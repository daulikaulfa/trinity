package jp.co.blueship.tri.am.domain;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedReturnToPendingServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class FlowChangeApprovedReturnToPendingServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testPjtId = "PJT-1606140002";

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean( "generalService" );
			((IService) service).init();
			
			this.returnToPending(service);
			
		} catch ( BaseBusinessException e ) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable) e) ) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void returnToPending (IGenericTransactionService service) throws Exception {
		FlowChangeApprovedReturnToPendingServiceBean serviceBean = new FlowChangeApprovedReturnToPendingServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(testPjtId);
			serviceBean.getParam().setComment("test");
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);
		
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangeApprovedReturnToPendingService.value(), serviceDto);
			this.debug(serviceBean);
		}

		if ( serviceBean.getResult().isCompleted() ) {
			IPjtDao dao = (IPjtDao) this.getBean( "amPjtDao" );
			PjtCondition condition = new PjtCondition();
			condition.setPjtId( testPjtId );
			IPjtEntity pjtEntity = dao.findByPrimaryKey( condition.getCondition() );
			assertTrue( null != pjtEntity );
			assertTrue( AmPjtStatusId.ChangePropertyInProgress.equals(pjtEntity.getStsId()) );
		}
		
	}
	
	private void debug (FlowChangeApprovedReturnToPendingServiceBean serviceBean) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}
}
