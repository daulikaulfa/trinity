package jp.co.blueship.tri.am.domain;

import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterViewBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibAssetRegisterServiceBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

import org.junit.Test;

public class FlowChaLibAssetRegisterServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Cha-Report-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowChaLibAssetRegisterService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalWebService" );
			((IService)service).init();

			FlowChaLibAssetRegisterServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			service.execute(FLOW_ACTION_ID, serviceDto);

			for ( AssetRegisterViewBean view: paramBean.getAssetRegisterViewList() ) {
				System.out.println(
						" MdlNm:=" + view.getMdlNm() +
						", FilePath:=" + view.getFilePath() +
						", LotId:=" + view.getLotId() +
						", PjtId:=" + view.getPjtId() +
						", AreqId:=" + view.getAreqId()
				);
			}

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private FlowChaLibAssetRegisterServiceBean getParamBean() {
		FlowChaLibAssetRegisterServiceBean paramBean = new FlowChaLibAssetRegisterServiceBean();
		AssetRegisterCondition condition = new AssetRegisterCondition();
		paramBean.setAssetRegisterCondition( condition );
		condition.setTargetMW( true );

		paramBean.setUserId( getAuthUserId() );
		paramBean.setUserName( getAuthUserName() );

		condition.setLotId( "LOT-1409290001" );

		return paramBean;
	}

}
