package jp.co.blueship.tri.am.domain;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangeApprovalPendingRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangeApprovedRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowChangeApprovalPendingDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-CheckoutRequestDetail-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String pjtId = null;

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			this.detailsInit(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void detailsInit(IGenericTransactionService service) throws Exception {

		pjtId = "PJT-1508210002";

		// Param
		FlowChangeApprovalPendingDetailsServiceBean serviceBean = new FlowChangeApprovalPendingDetailsServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setLanguage("en");
			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		serviceBean.getParam().setRequestType(RequestType.init);

		service.execute(ServiceId.AmChangeApprovalPendingDetailsService.value(), serviceDto);

		System.out.println("" + RequestType.init.value());

		ChangePropertyDetailsView view = serviceBean.getDetailsView();
		System.out.println("--------Pjt Information--------");
		System.out.println(" Pjt Id			: " + view.getPjtId());
		System.out.println(" ReferenceId Id		: " + view.getChgFactorId());
		System.out.println(" Status Id		: " + view.getStsId());
		System.out.println(" Status			: " + view.getStatus());
		System.out.println(" Change Facotr Id	: " + view.getChgFactorId());
		System.out.println(" Summary		: " + view.getSummary());
		System.out.println(" Content		: " + view.getContents());
		System.out.println(" Submitter name		: " + view.getSubmitterNm());
		System.out.println(" Assignee name		: " + view.getAssigneeNm());
		System.out.println(" Category name		: " + view.getCategoryNm());
		System.out.println(" Milestone name		: " + view.getMstoneNm());
		System.out.println(" Created Date		: " + view.getCreatedTime());
		System.out.println(" Update Date		: " + view.getUpdTime());

		List<ChangeApprovalPendingRequestView> cAreqView = serviceBean.getApprovalPendingRequestViews();
		System.out.println();
		System.out.println("--------CURRENT Areq Information--------");
		for(ChangeApprovalPendingRequestView current : cAreqView){

			System.out.println("Areq Id 		: " + current.getAreqId());
			System.out.println("Subject 		: " + current.getSubject());
			System.out.println("RequtestTime	: " + current.getRequestTime());
			System.out.println("Submitter 		: " + current.getSubmitterNm());
			System.out.println("GroupNm			: " + current.getGroupNm());
			System.out.println("AreqType		: "	+ current.getAreqType().value());
			System.out.println("Status Id     	: " + current.getStsId());
			System.out.println("Status			: " + current.getStatus());
			System.out.println();
		}

		List<ChangeApprovedRequestView> pAreqView = serviceBean.getApprovedRequestViews();
		System.out.println();
		System.out.println("--------PAST Areq Information--------");
		for(ChangeApprovedRequestView past : pAreqView){

			System.out.println("Areq Id 		: " + past.getAreqId());
			System.out.println("Subject 		: " + past.getSubject());
			System.out.println("RequestTime		: " + past.getRequestTime());
			System.out.println("Submitter	 	: " + past.getSubmitterNm());
			System.out.println("GroupNm			: " + past.getGroupNm());
			System.out.println("AreqType		: "	+ past.getAreqType().value());
			System.out.println("Status id		: " + past.getStsId());
			System.out.println("Status			: " + past.getStatus());
			System.out.println();
		}
	}
}

