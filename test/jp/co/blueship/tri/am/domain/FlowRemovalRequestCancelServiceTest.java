package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowRemovalRequestCancelServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-CheckoutRequestDetail-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String areqId = null;
	private String cancelReason = null;

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			this.cancelInit(service);
			this.cancelSubmitChanges(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}



	private void cancelInit(IGenericTransactionService service) throws Exception {

		areqId = "MSD-1604050002";

		// Param
		FlowRemovalRequestApprovalPendingCancellationServiceBean serviceBean = new FlowRemovalRequestApprovalPendingCancellationServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.getParam().setSelectedAreqId(areqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		serviceBean.getParam().setRequestType(RequestType.init);
		service.execute(ServiceId.AmRemovalRequestApprovalPendingCancellationService.value(), serviceDto);

	}

	private void cancelSubmitChanges(IGenericTransactionService service) throws Exception {

		areqId = "MSD-1604050002";
		cancelReason = "コメント こめんと ";
		FlowRemovalRequestApprovalPendingCancellationServiceBean serviceBean = new FlowRemovalRequestApprovalPendingCancellationServiceBean();

		serviceBean.setUserId(getAuthUserId());
		serviceBean.setUserName(getAuthUserName());
		serviceBean.getParam().setSelectedAreqId(areqId);
		serviceBean.getParam().getInputInfo().setComment(cancelReason);
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		serviceBean.getParam().setRequestType(RequestType.submitChanges);
		service.execute(ServiceId.AmRemovalRequestApprovalPendingCancellationService.value(), serviceDto);

	}
}

