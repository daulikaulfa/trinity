package jp.co.blueship.tri.am.domain;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.ChangeApprovedRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowChangeApprovedDetailServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-CheckoutRequestDetail-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String pjtId = null;

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			this.detailsInit(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void detailsInit(IGenericTransactionService service) throws Exception {

		pjtId = "PJT-1604220001";

		// Param
		FlowChangeApprovedDetailsServiceBean serviceBean = new FlowChangeApprovedDetailsServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setLanguage("en");
			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		serviceBean.getParam().setRequestType(RequestType.init);

		service.execute(ServiceId.AmChangeApprovedDetailsService.value(), serviceDto);

		System.out.println("" + RequestType.init.value());

		ChangePropertyDetailsView view = serviceBean.getDetailsView();
		System.out.println("--------Pjt Information--------");
		System.out.println(" Pjt Id			: " + view.getPjtId());
		System.out.println(" ReferenceId Id		: " + view.getChgFactorId());
		System.out.println(" Status			: " + view.getStatus());
		System.out.println(" Category		: " + view.getReferenceId());
		System.out.println(" Summary		: " + view.getSummary());
		System.out.println(" Content		: " + view.getContents());
		System.out.println(" Submitter name		: " + view.getSubmitterNm());
		System.out.println(" Assignee name		: " + view.getAssigneeNm());
		System.out.println(" Created Date		: " + view.getCreatedTime());
		System.out.println(" Update Date		: " + view.getUpdTime());
		System.out.println(" Category name		: " + view.getCategoryNm());
		System.out.println(" Milestone name		: " + view.getMstoneNm());

		List<ChangeApprovedRequestView> approvedAreqDetailsList = serviceBean.getApprovedRequestViews();
		System.out.println();
		System.out.println("--------Approved Areq Information--------");
		for(ChangeApprovedRequestView areqDetails : approvedAreqDetailsList){

			System.out.println("AreqType		: "	+ areqDetails.getAreqType());
			System.out.println("Areq Id 		: " + areqDetails.getAreqId());
			System.out.println("Subject 		: " + areqDetails.getSubject());
			System.out.println("RequtestTime		: " + areqDetails.getRequestTime());
			System.out.println("Submitter 		: " + areqDetails.getSubmitterNm());
			System.out.println("GroupNm			: " + areqDetails.getGroupNm());
			System.out.println("Status			: " + areqDetails.getStatus());
			System.out.println();
		}
//
//		List<ChangeApprovedHistoryViewBean> approvedHistoryList = serviceBean.getApprovedHistoryViews();
//		System.out.println();
//		System.out.println("--------Approved History Information--------");
//		for(PjtApproveHistoryViewBean approvedHistory : approvedHistoryList){
//
//			System.out.println("Approved date : " + approvedHistory.getApproveTime() );
//			System.out.println("Approved Category : " +approvedHistory.getStatus());
//			System.out.println("Approved User : " + approvedHistory.getApproveUser());
//			System.out.println("Approved Comment : " +approvedHistory.getApproveComment());
//			System.out.println();
//		}
	}
}

