package jp.co.blueship.tri.am.domain;

import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

import org.junit.Test;

public class FlowChaLibConflictCheckServiceCompTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Cha-Merge-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowChaLibConflictCheckService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowChaLibConflictCheckServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			service.execute(FLOW_ACTION_ID, serviceDto);

			this.threadWait( paramBean );

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private FlowChaLibConflictCheckServiceBean getParamBean() {
		FlowChaLibConflictCheckServiceBean paramBean = new FlowChaLibConflictCheckServiceBean();

		paramBean.setUserId( getAuthUserId() );
		paramBean.setUserName( getAuthUserName() );

		paramBean.setReferer( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
		paramBean.setForward( ChaLibScreenID.COMP_CONFLICTCHECK );
		paramBean.setScreenType( ScreenType.next );

		paramBean.setLockLotNo( "LOT-1407200031" );
		paramBean.setSelectedLotId( "LOT-1407200031" );
		paramBean.setSelectedLotVersionTag( "PJT-1407200011" );

		{
			MergeEditInputBean inputBean = new MergeEditInputBean();
			inputBean.setConflictCheckComment( "test用バッチからコンフリクト" );

			paramBean.setMergeEditInputBean( inputBean );
		}

		return paramBean;
	}

}
