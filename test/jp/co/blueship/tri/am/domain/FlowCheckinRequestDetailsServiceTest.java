package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckinRequestDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowCheckinRequestDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-CheckoutRequestDetail-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;

	}

	private String areqId = null;

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			this.detailsInit(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void detailsInit(IGenericTransactionService service) throws Exception {

		areqId = "LND-1604220004";

		// Param
		FlowCheckinRequestDetailsServiceBean serviceBean = new FlowCheckinRequestDetailsServiceBean();
		{
			serviceBean.setUserId("admin");
			serviceBean.getParam().setSelectedAreqId(areqId);
			serviceBean.setLanguage("en");
		}
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		{//init

			serviceDto.setServiceBean(serviceBean);
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmCheckinRequestDetailsService.value(), serviceDto);
			debugInfo(serviceBean);
		}
		// onChange
		{
			serviceBean.getParam().getResourceSelection().setPath("/lib");
	    	serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.selectFolder);
			service.execute(ServiceId.AmCheckinRequestDetailsService.value(), serviceDto);
			debugInfo( serviceBean );

			serviceBean.getParam().getResourceSelection().setPath("/lib");
	    	serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.openFolder);
			service.execute(ServiceId.AmCheckinRequestDetailsService.value(), serviceDto);
			debugInfo( serviceBean );

			serviceBean.getParam().getResourceSelection().setPath("/lib/configuration");
	    	serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.selectFolder);
			service.execute(ServiceId.AmCheckinRequestDetailsService.value(), serviceDto);
			debugInfo( serviceBean );
		}
	}

	private void debugInfo(FlowCheckinRequestDetailsServiceBean serviceBean){

		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("  ResourceSelection.type:= " + serviceBean.getParam().getResourceSelection().getType());
		System.out.println("  ResourceSelection.path:= " + serviceBean.getParam().getResourceSelection().getPath());
		System.out.println("--------------------");

		CheckinRequestDetailsViewBean detailView = serviceBean.getDetailsView();
		System.out.println("Lot id : " + detailView.getLotId());
		System.out.println("Pjt id : " + detailView.getPjtId());
		System.out.println("Areq id : " + detailView.getAreqId());
		System.out.println("Reference no : " + detailView.getReferenceId());
		System.out.println("Assignee Name : " + detailView.getAssigneeNm());
		System.out.println("Submitter Name : " + detailView.getSubmitterNm());
		System.out.println("Group Name : " + detailView.getGroupNm());
		System.out.println("Subject : " + detailView.getSubject());
		System.out.println("Content : " + detailView.getContents());
		System.out.println("Checkin Due Date : " + detailView.getCheckinDueDate());
		System.out.println("Checkout Date : " + detailView.getCheckoutTime());
		System.out.println("Milestone Name : " + detailView.getMstoneNm());
		System.out.println("Category Name : " + detailView.getCtgNm());
		System.out.println("Attachment file :" + detailView.getAttachmentFileNms());
		System.out.println("Checkout path : " + detailView.getCheckoutPath() );
		System.out.println("Checkin path : " + detailView.getCheckinPath() );

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderView());
        System.out.println("toJson: " + json);

        System.out.println("\nRequestViews");
		for ( ICheckinResourceDetailsViewBean bean: serviceBean.getResourceSelectionFolderView().getRequestViews()) {
			System.out.print("  Path: " + bean.getPath() + " ");
			System.out.print("  Name: " + bean.getName() + " ");
			System.out.print("  Size: " + bean.getSize() + " ");
			System.out.print("  Count: " + bean.getCount() + " ");
			System.out.println("Differ : " + bean.isDiff());
			System.out.println();
		}
	}
}