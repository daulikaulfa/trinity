package jp.co.blueship.tri.am.domain;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestServiceTest extends TestDomainSupport {

	private static final String testLotId = "LOT-1604140002";
	private static final String areqId = "LND-1604150004";
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);

			this.validateCheckinSuccessOrNot(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create(IGenericTransactionService service) throws Exception{
		FlowCheckinRequestServiceBean serviceBean = new FlowCheckinRequestServiceBean();

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedAreqId(areqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);

			serviceBean.getParam().getResourceSelection().setPath("");
			service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// on-change
		{
			ResourceSelection resource = serviceBean.getParam().getResourceSelection();
			serviceBean.getParam().setRequestType(RequestType.onChange);

			// open folder
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.openFolder);
				resource.setPath("lib2");
				service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}

			// select folder
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.selectFolder);
				resource.setPath("lib2/src");
				service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}

			// select folder
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.selectFolder);
				resource.setPath("lib2");
				service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}

			// refresh
			{
				serviceBean.getParam().setRequestOption(RequestOption.refresh);
				service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}

			// file upload
			{
				serviceBean.getParam().setRequestOption(RequestOption.fileUpload);
				serviceBean.getParam().getInputInfo().setAttachmentFileNm("checkout_resources.txt");
				serviceBean.getParam().getInputInfo().setAttachmentInputStreamBytes( this.readResources() );
				try {
					service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
				} catch (Exception e) {
					e.printStackTrace();
				}
				this.debug(serviceBean);
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmCheckinRequestService.value(), serviceDto);
			this.threadWait( serviceBean );
		}

		System.out.println(serviceBean.getParam().getSelectedAreqId());
	}

	private void validateCheckinSuccessOrNot(IGenericTransactionService service) throws Exception{

		// Call legacy system to check new check-in record exists in approval pending list
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		{
			FlowChaLibPjtApproveWaitListServiceBean waitServiceBean = new FlowChaLibPjtApproveWaitListServiceBean();
			waitServiceBean.setUserId(getAuthUserId());
			waitServiceBean.setUserName(getAuthUserName());
			waitServiceBean.setScreenType( ScreenType.Type.next.value() );
			waitServiceBean.setLanguage("en");

			waitServiceBean.setSelectedLotNo(testLotId);

			serviceDto.setServiceBean( waitServiceBean );

			service.execute("FlowChaLibPjtApproveWaitListService", serviceDto);

			boolean exist = false;
			for (ApplyInfoViewPjtApproveBean bean: waitServiceBean.getApplyInfoViewBeanList()) {
				if (bean.getApplyNo().equals(areqId)) {
					exist = true;
				}
			}

			if (!exist) {
				System.out.println("Checkin process is not finished or incorrect!");
			} else {
				System.out.println("Checkin process is finished!");
			}
		}
	}

	private byte[] readResources() throws IOException {

		InputStream inputStream = null;
		ByteArrayOutputStream boutStream = null;

		try {
			inputStream = TestDomainSupport.class.getClassLoader().getResourceAsStream("./data/checkout_resources.txt");
			boutStream = new ByteArrayOutputStream();

			byte [] buffer = new byte[1024];
			while(true) {
				int len = inputStream.read(buffer);
				if(len < 0) {
					break;
				}
				boutStream.write(buffer, 0, len);
			}
		} finally {
			if ( null != inputStream ) {
				inputStream.close();
			}
			if (null != boutStream ) {
				boutStream.close();
			}
		}

		return boutStream.toByteArray();
	}

	private void debug( FlowCheckinRequestServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("  ResourceSelection.type:= " + serviceBean.getParam().getResourceSelection().getType());
		System.out.println("  ResourceSelection.path:= " + serviceBean.getParam().getResourceSelection().getPath());
		System.out.println("--------------------");

		{
			System.out.println("\nResourceSelectionFolderView ");
			ResourceSelectionFolderView<ICheckinResourceViewBean> view = serviceBean.getResourceSelectionFolderView();
			System.out.println("  SelectedPath:= " + view.getSelectedPath());
			System.out.println("  ITreeFolderViewBean:= ");

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(view.getFolderView());
	        System.out.println(json);

			System.out.println("\nRequestViewBean ");
	        for ( ICheckinResourceViewBean row: view.getRequestViews() ) {
	        	System.out.print("  Path:= " + row.getPath());
	        	System.out.print("  Name:= " + row.getName());
	        	System.out.print("  Size:= " + row.getSize());
	        	System.out.print("  SubmitterNm:= " + row.getSubmitterNm());
	        	System.out.print("  CheckoutDate:= " + row.getCheckoutDate());
	        	System.out.print("  CheckinDueDate:= " + row.getCheckinDueDate());
	        	System.out.print("  AreqId:= " + row.getAreqId());
	        	System.out.print("  diff:= " + row.isDiff() );
	    		System.out.println("");

	        }
		}

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("\nRequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}

	}
}
