package jp.co.blueship.tri.am.domain;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

import org.junit.Test;

public class FlowChaLibLotModifyServiceCompTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Cha-Pjt-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowChaLibLotModifyService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowChaLibLotModifyServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			service.execute(FLOW_ACTION_ID, serviceDto);

			this.threadWait( paramBean );

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private FlowChaLibLotModifyServiceBean getParamBean() {
		FlowChaLibLotModifyServiceBean paramBean = new FlowChaLibLotModifyServiceBean();

		paramBean.setUserId( getAuthUserId() );
		paramBean.setUserName( getAuthUserName() );

		paramBean.setReferer( ChaLibScreenID.LOT_MODIFY_CONFIRM );
		paramBean.setForward( ChaLibScreenID.COMP_LOT_MODIFY );
		paramBean.setScreenType( ScreenType.next );

		//***********************************************************
		paramBean.setLockLotNo( "LOT-1504280002" );
		paramBean.setLotNo(paramBean.getLockLotNo());
		//***********************************************************

		{
			LotEditInputV3Bean inputBean = new LotEditInputV3Bean();

			inputBean.setLotNo(paramBean.getLockLotNo());
			inputBean.setLotName("JUnitTest-Modify");
			inputBean.setLotSummary("JUnitTest-Modify");
			inputBean.setLotContent("JUnitTest-Modify");

			inputBean.setInputUserId( getAuthUserId() );
			inputBean.setInputUserName( getAuthUserName() );

			inputBean.setBaseLineTag( "BASE-TAG" );
			inputBean.setEditUseMerge( true );

			inputBean.setDirectoryPathPublic( "c:/user" );
			inputBean.setDirectoryPathPrivate( "c:/sys" );

			List<ModuleViewBean> mdls = new ArrayList<ModuleViewBean>();
			ModuleViewBean mdl = new ModuleViewBean();
			mdl.setModuleName( "trinity-schemaCompiler" );
			mdl.setRepository( "file:///C:/csvn/data/repositories/repos/" );
			mdls.add( mdl );

			inputBean.setModuleViewBeanList( mdls );
			inputBean.setSelectedModuleViewBeanList( mdls );

			// グループ情報
			List<GroupViewBean> groups = new ArrayList<GroupViewBean>();
			GroupViewBean group = new GroupViewBean();
			group.setGroupId( "14090001" );
			group.setGroupName( "サポート" );
			groups.add( group );
			inputBean.setSelectedGroupViewBeanList( groups );

			List<GroupViewBean> mailGroups = new ArrayList<GroupViewBean>();
			inputBean.setSelectedSpecifiedGroupList( mailGroups );


			// ビルド環境情報
			inputBean.setSelectedBuildEnvNo( "BM_DIFF" );
			inputBean.setSelectedFullBuildEnvNo( "BM_FULL" );

			// リリース環境情報
			List<RelEnvViewBean> rpEnvs = new ArrayList<RelEnvViewBean>();
			RelEnvViewBean rpEnv = new RelEnvViewBean();
			rpEnv.setEnvNo( "RM_IT" );
			rpEnv.setEnvName( "リリース（開発環境）" );
			rpEnv.setEnvSummary( "" );
			rpEnvs.add( rpEnv );
			inputBean.setSelectedRelEnvViewBeanList( rpEnvs );

			paramBean.setLotEditInputBean(inputBean);
		}

		return paramBean;
	}

}
