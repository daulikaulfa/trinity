package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.CurrentMergeLotView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.OtherMergeLotView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class FlowMergeLotListServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-MergeDetails-Context.xml"};
	
	private static final String testLotId = "LOT-1605200009";
	
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}
	
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();
			
			this.mergeLotList(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void mergeLotList(IGenericTransactionService service) throws Exception {
		FlowMergeLotListServiceBean serviceBean = new FlowMergeLotListServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.getParam().setSelectedLotId( testLotId );

		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		int selectedPageNo = 1;
		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmMergeLotListService.value(), serviceDto);
			
			if (serviceBean.getPage().isNext()) {
				selectedPageNo++;
			}
			
			this.debug( serviceBean );
		}
		
		// onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo( selectedPageNo );
			service.execute(ServiceId.AmMergeLotListService.value(), serviceDto);
			this.debug( serviceBean );
		}
		
	}
	
	private void debug( FlowMergeLotListServiceBean serviceBean ) {
		{
			System.out.println("========CurrentLotView========");
			CurrentMergeLotView currentLotView = serviceBean.getCurrentLotView();
			System.out.println( "LotId 				:= " + currentLotView.getLotId() );
			System.out.println( "LotNm 				:= " + currentLotView.getLotNm() );
			System.out.println( "noOfMerges 		:= " + currentLotView.getNoOfMerges() );
			System.out.println( "noOfChangeProperty := " + currentLotView.getNoOfChangePropertys() );
			System.out.println( "startTime 			:= " + currentLotView.getStartTime() );
			System.out.println( "endTime 			:= " + currentLotView.getEndTime() );
			System.out.println( "stsId 				:= " + currentLotView.getStsId() );
			System.out.println( "status 			:= " + currentLotView.getStatus() );
			System.out.println( "warningOfConflict 	:= " + currentLotView.getWarningOfConflict() );
			System.out.println( "warningOfMerge 	:= " + currentLotView.getWarningOfMerge() );
			System.out.println("==============================");
			
		}
		
		System.out.println("========OtherLotView========");
		
		for ( OtherMergeLotView view : serviceBean.getOtherLotViews() ) {
			System.out.println( "LotId 				:= " + view.getLotId() );
			System.out.println( "LotNm 				:= " + view.getLotNm() );
			System.out.println( "noOfMerges 		:= " + view.getNoOfMerges() );
			System.out.println( "noOfChangeProperty := " + view.getNoOfChangePropertys() );
			System.out.println( "startTime 			:= " + view.getStartTime() );
			System.out.println( "endTime 			:= " + view.getEndTime() );
			System.out.println( "stsId 				:= " + view.getStsId() );
			System.out.println( "status 			:= " + view.getStatus() );
			System.out.println( "warningOfConflict 	:= " + view.getWarningOfConflict() );
			System.out.println( "warningOfMerge 	:= " + view.getWarningOfMerge() );
			System.out.println();
		}
		
		System.out.println("============================");
	}
}
