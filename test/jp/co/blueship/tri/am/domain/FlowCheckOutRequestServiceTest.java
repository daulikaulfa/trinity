package jp.co.blueship.tri.am.domain;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckInOutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestEditServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowCheckOutRequestServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};

	private static final String testLotId = "LOT-1603240008";//"LOT-1603220008";
	private static String testAreqIdDraftMode = null;
	//private static String testAreqIdSubmitMode = null;

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
			this.edit(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create(IGenericTransactionService service) throws Exception {
		FlowCheckoutRequestServiceBean serviceBean = new FlowCheckoutRequestServiceBean();

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		CheckoutRequestEditInputBean inputInfo = serviceBean.getParam().getInputInfo();

		String assigneeId = null;
		String groupId = null;
		String pjtId =  null;
		String ctgId = null;
		String mstoneId = null;

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// check pjt views
			if (0 < serviceBean.getParam().getInputInfo().getPjtViews().size()) {
				pjtId = serviceBean.getParam().getInputInfo().getPjtViews().get(0).getValue();
			}
			for (ItemLabelsBean pjtItem: serviceBean.getParam().getInputInfo().getPjtViews()) {
				System.out.println("PJT_ID: " + pjtItem.getValue());
			}

			// check assignee views
			if ( 0 < serviceBean.getParam().getInputInfo().getAssigneeViews().size() ) {
				assigneeId = serviceBean.getParam().getInputInfo().getAssigneeViews().get(0).getValue();
			};

			for (ItemLabelsBean assigneeItem: serviceBean.getParam().getInputInfo().getAssigneeViews()) {
				System.out.println("ASSIGNEE_ID: " + assigneeItem.getValue());
			}

			// check group views

			if ( 0 < serviceBean.getParam().getInputInfo().getGroupViews().size() ) {
				groupId = serviceBean.getParam().getInputInfo().getGroupViews().get(0).getValue();
			};

			for (ItemLabelsBean groupItem: serviceBean.getParam().getInputInfo().getGroupViews()) {
				System.out.println("GROUP_ID: " + groupItem.getValue());
			}


			// check category views
			if (0 < serviceBean.getParam().getInputInfo().getCategoryViews().size()) {
				ctgId = serviceBean.getParam().getInputInfo().getCategoryViews().get(0).getValue();
			}

			for (ItemLabelsBean categoryItem: serviceBean.getParam().getInputInfo().getCategoryViews()) {
				System.out.println("CTG_ID: " + categoryItem.getValue());
			}

			// check milestone views
			if (0 < serviceBean.getParam().getInputInfo().getCategoryViews().size()) {
				mstoneId = serviceBean.getParam().getInputInfo().getMstoneViews().get(0).getValue();
			}

			for (ItemLabelsBean mstoneItem: serviceBean.getParam().getInputInfo().getMstoneViews()) {
				System.out.println("MSTONE_ID: " + mstoneItem.getValue());
			}

			// check TreeFolder
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderView());
	        System.out.println("toJson: " + json);

			System.out.println("init finish!");
		}

		// refresh assignee
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectGroup);
			inputInfo.setGroupId(groupId);
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// check assignee views
			if ( 0 < serviceBean.getParam().getInputInfo().getAssigneeViews().size()) {
				assigneeId = serviceBean.getParam().getInputInfo().getAssigneeViews().get(0).getValue();
			}

			for (ItemLabelsBean assigneeItem: serviceBean.getParam().getInputInfo().getAssigneeViews()) {
				System.out.println("ASSIGNEE_ID: " + assigneeItem.getValue());
			}
		}

		// refresh group name
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectAssignee);
			inputInfo.setAssigneeId(assigneeId);
			System.out.println("input assigneeId: " + assigneeId);
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// check group name views
			if (0 < serviceBean.getParam().getInputInfo().getGroupViews().size()) {
				groupId = serviceBean.getParam().getInputInfo().getGroupViews().get(0).getValue();
			}

			for (ItemLabelsBean groupItem: serviceBean.getParam().getInputInfo().getGroupViews()) {
				System.out.println("GROUP_ID: " + groupItem.getValue());
			}
		}

		// open folder
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.openFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");//"lib2/src");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
		}

		// tree folder open -- selec folder
		List<String> selectedFiles = new ArrayList<String>();
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.selectFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");///No40000/No30000/No1000/No800/No10
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// check file in folder
			for (ICheckoutResourceViewBean viewBean : serviceBean.getResourceSelectionFolderView().getRequestViews()) {
				CheckInOutResourceViewBean viewBean1 = (CheckInOutResourceViewBean) viewBean;
				System.out.println("Path: " + viewBean1.getPath());
				System.out.println("Name: " + viewBean1.getName());
				System.out.println("Size: " + viewBean1.getSize());
				System.out.println("AreqId: " + viewBean1.getAreqId());
				System.out.println("PjtId: " + viewBean1.getPjtId());
				System.out.println();
				if (viewBean1.isFile() && TriStringUtils.isEmpty(viewBean1.getAreqId())) {
					selectedFiles.add(viewBean1.getPath());
				}
			}
		}


		// open folder
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.openFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");//"lib2/src");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
		}

		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.selectFolder);
			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2/No40000");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// check file in folder
			for (IResourceViewBean viewBean : serviceBean.getResourceSelectionFolderView().getRequestViews()) {
				if (viewBean instanceof CheckInOutResourceViewBean) {
					CheckInOutResourceViewBean viewBean1 = (CheckInOutResourceViewBean) viewBean;
					System.out.println("Path: " + viewBean1.getPath());
					System.out.println("Name: " + viewBean1.getName());
					System.out.println("Size: " + viewBean1.getSize());
					System.out.println("AreqId: " + viewBean1.getAreqId());
					System.out.println("PjtId: " + viewBean1.getPjtId());
					System.out.println();
					if (viewBean1.isFile() && TriStringUtils.isEmpty(viewBean1.getAreqId())) {
						selectedFiles.add(viewBean1.getPath());
					}
				}
			}
		}

		/*{
			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderViewBean().getFolder("lib2/src");
			serviceBean.getResourceSelectionFolderView().setSelectedPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
			List<String> newSelectedFiles = new ArrayList<String>();
			// check file in folder
			for (ICheckoutResourceViewBean viewBean : serviceBean.getResourceSelectionFolderView().getRequestViewBean()) {
				if (!selectedFiles.contains(viewBean.getPath())) {
					newSelectedFiles.add(viewBean.getPath());
				}
			}
			serviceBean.getParam().getResourceSelection().setSelectedFiles(newSelectedFiles.toArray(new String[0]));
		}*/

		// file upload or drag and drop file
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(RequestOption.fileUpload);
			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
//			File file = new File("C:\\work\\pg_dummy_data.sql"); --> check file extension
			File file = new File("C:\\work\\fileUpload.txt");
			InputStream targetStream = new FileInputStream(file);
			inputInfo.setCsvFileNm(file.getName());
			inputInfo.setCsvFilePath(file.getPath());
			inputInfo.setCsvInputStreamBytes(StreamUtils.convertInputStreamToBytes(targetStream));
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);

			// Check selected file
			for (String selectedFile : serviceBean.getParam().getResourceSelection().getSelectedFileSet()) {
				System.out.println("Path: " + selectedFile);
			}
			targetStream.close();
		}

		Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderView());
        System.out.println("toJson: " + json);

		// Choose /lib2 again and un-select some files
		/*{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getResourceSelection().setType(Type.openFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderViewBean().getFolder("lib2");
			serviceBean.getResourceSelectionFolderView().setSelectedPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
			List<String> newSelectedFiles = new ArrayList<String>();
			// check file in folder
			for (ICheckoutResourceViewBean viewBean : serviceBean.getResourceSelectionFolderView().getRequestViewBean()) {
				if (!selectedFiles.contains(viewBean.getPath())) {
					newSelectedFiles.add(viewBean.getPath());
				}
			}
			serviceBean.getParam().getResourceSelection().setSelectedFiles(newSelectedFiles.toArray(new String[0]));

		}*/


		// save draft
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
			inputInfo.setSubmitMode(SubmitMode.draft);
			inputInfo.setPjtId(pjtId);
			inputInfo.setGroupId(groupId);
			inputInfo.setSubject("subject");
			inputInfo.setContents("content");
			inputInfo.setAssigneeId(assigneeId);
			inputInfo.setCheckinDueDate("2016-03-22");
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
//			testAreqIdDraftMode = serviceBean.getParam().getSelectedAreqId();
		}

		// submit changes
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputInfo.setSubmitMode(SubmitMode.request);
			inputInfo.setPjtId(pjtId);
			inputInfo.setGroupId(groupId);
			inputInfo.setSubject("subject submit");
			inputInfo.setContents("content submit");
			inputInfo.setAssigneeId(assigneeId);
			inputInfo.setCheckinDueDate("2016-03-25");
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
//			testAreqIdSubmitMode = serviceBean.getParam().getSelectedAreqId();
		}

	}

	private void edit(IGenericTransactionService service) throws Exception {
		FlowCheckoutRequestEditServiceBean serviceBean = new FlowCheckoutRequestEditServiceBean();
		testAreqIdDraftMode = "null1604070001";
//		testAreqIdSubmitMode = "LND-1603230005";

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		String assigneeId = null;
		//String groupId = null;
		//String pjtId =  null;
		String ctgId = null;
		String mstoneId = null;

		// init: areq with status_id != CHECKOUT_DRAFT
		/*{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedAreqId(testAreqIdSubmitMode);
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);

			// check TreeFolder
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderViewBean());
	        System.out.println("toJson: " + json);

			// check selected folder
			for (String selectedFile: serviceBean.getSelectedFileSet()) {
				System.out.println("File Path: " + selectedFile);
			}

		}*/

		// init: areq with status_id = CHECKOUT_DRAFT
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedAreqId(testAreqIdDraftMode);
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);

		}

		CheckoutRequestEditInputBean inputInfo = serviceBean.getParam().getInputInfo();
		{
			// check pjt views
			for (ItemLabelsBean pjtItem: serviceBean.getParam().getInputInfo().getPjtViews()) {
				System.out.println("PJT_ID: " + pjtItem.getValue());
			}

			for (ItemLabelsBean assigneeItem: serviceBean.getParam().getInputInfo().getAssigneeViews()) {
				System.out.println("ASSIGNEE_ID: " + assigneeItem.getValue());
				if (null == mstoneId && !assigneeItem.getValue().equals(inputInfo.getAssigneeId())) {
					assigneeId = assigneeItem.getValue();
				}
			}

			// check group views
			for (ItemLabelsBean groupItem: serviceBean.getParam().getInputInfo().getGroupViews()) {
				System.out.println("GROUP_ID: " + groupItem.getValue());
			}


			// check category views
			for (ItemLabelsBean categoryItem: serviceBean.getParam().getInputInfo().getCategoryViews()) {
				System.out.println("CTG_ID: " + categoryItem.getValue());
				if (null == ctgId && !categoryItem.getValue().equals(inputInfo.getCtgId())) {
					ctgId = categoryItem.getValue();
				}
			}

			// check milestone views
			for (ItemLabelsBean mstoneItem: serviceBean.getParam().getInputInfo().getMstoneViews()) {
				System.out.println("MSTONE_ID: " + mstoneItem.getValue());
				if (null == mstoneId && !mstoneItem.getValue().equals(inputInfo.getMstoneId())) {
					mstoneId = mstoneItem.getValue();
				}
			}

			// check TreeFolder
			Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderView());
	        System.out.println("toJson: " + json);


			// check selected folder
			for (String selectedFile: serviceBean.getParam().getResourceSelection().getSelectedFileSet()) {
				System.out.println("File Path: " + selectedFile);
			}
			System.out.println("========");
		}

		// onchange

		// open folder
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(FlowCheckoutRequestEditServiceBean.RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.openFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");//"lib2/src");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// open folder
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(FlowCheckoutRequestEditServiceBean.RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.openFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2/No40000");//"lib2/src");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// close folder
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(FlowCheckoutRequestEditServiceBean.RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.closeFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");//"lib2/src");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// tree folder open
		List<String> selectedFiles = new ArrayList<String>();
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(FlowCheckoutRequestEditServiceBean.RequestOption.selectResource);
			serviceBean.getParam().getResourceSelection().setType(ResourceRequestType.selectFolder);
			ITreeFolderViewBean selectedFolder = serviceBean.getResourceSelectionFolderView().getFolderView().getFolder("lib2");
			serviceBean.getParam().getResourceSelection().setPath(selectedFolder.getPath());
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);

			// check file in folder
			for (IResourceViewBean viewBean : serviceBean.getResourceSelectionFolderView().getRequestViews()) {
				if (viewBean instanceof CheckInOutResourceViewBean) {
					CheckInOutResourceViewBean viewBean1 = (CheckInOutResourceViewBean) viewBean;
					if (serviceBean.getParam().getResourceSelection().getSelectedFileSet().contains(viewBean1.getPath())) {
						selectedFiles.add(viewBean1.getPath());
					}
				}
			}
			// check file in folder
			this.debug(serviceBean);
		}

		// file upload or drag and drop file
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOption(FlowCheckoutRequestEditServiceBean.RequestOption.fileUpload);
			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
//			serviceBean.getParam().getResourceSelection().setSelectedFileSet(new HashSet<String>(selectedFiles));
			File file = new File("C:\\work\\fileUpload.txt");
			InputStream targetStream = new FileInputStream(file);
			inputInfo.setCsvFileNm(file.getName());
			inputInfo.setCsvFilePath(file.getPath());
			inputInfo.setCsvInputStreamBytes(StreamUtils.convertInputStreamToBytes(targetStream));
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);

			// Check selected file
			for (String selectedFile : serviceBean.getParam().getResourceSelection().getSelectedFileSet()) {
				System.out.println("Path: " + selectedFile);
			}
			targetStream.close();
			this.debug(serviceBean);
		}

		/*for (String file : serviceBean.getSelectedFileSet()) {
			System.out.println("Selected path: " + file);
		}*/

		// save draft
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
//			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
			inputInfo.setSubmitMode(SubmitMode.changes);
			inputInfo.setSubject("subject changed");
			inputInfo.setContents("content changed");
			inputInfo.setAssigneeId(assigneeId);
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);
			System.out.println("finish");
		}

		// submit changes
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
//			serviceBean.getParam().getResourceSelection().setSelectedFiles(selectedFiles.toArray(new String[0]));
			inputInfo.setSubmitMode(SubmitMode.request);
			inputInfo.setSubject("subject changed the second time");
			inputInfo.setContents("content changed the second time");
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);
			service.execute(ServiceId.AmCheckoutRequestEditService.value(), serviceDto);
		}
	}

	private void debug( FlowCheckoutRequestEditServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("  ResourceSelection.type:= " + serviceBean.getParam().getResourceSelection().getType());
		System.out.println("  ResourceSelection.path:= " + serviceBean.getParam().getResourceSelection().getPath());
		System.out.println("--------------------");

		{
			System.out.println("\nResourceSelectionFolderView ");
			ResourceSelectionFolderView<ICheckoutResourceViewBean> view = serviceBean.getResourceSelectionFolderView();
			System.out.println("  SelectedPath:= " + view.getSelectedPath());
			System.out.println("  ITreeFolderViewBean:= ");

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(view.getFolderView());
	        System.out.println(json);

			System.out.println("\nRequestViewBean ");
	        for ( ICheckoutResourceViewBean row: view.getRequestViews() ) {
	        	System.out.print("  Path:= " + row.getPath());
	        	System.out.print("  Name:= " + row.getName());
	        	System.out.print("  Size:= " + row.getSize());
	        	System.out.print("  SubmitterNm:= " + row.getSubmitterNm());
	        	System.out.print("  CheckoutDate:= " + row.getCheckoutDate());
	        	System.out.print("  CheckinDueDate:= " + row.getCheckinDueDate());
	        	System.out.print("  AreqId:= " + row.getAreqId());
	    		System.out.println("");

	        }
		}

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("\nRequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}

	}
}
