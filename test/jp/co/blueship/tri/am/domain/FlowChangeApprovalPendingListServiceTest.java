package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowChangeApprovalPendingListServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1604010002";

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService) service).init();
			this.list(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void list(IGenericTransactionService service) throws Exception {
		String testCtgId = "9";
		String testMstoneId = "9";
		String testKeyword = "z";

		FlowChangeApprovalPendingListServiceBean serviceBean = new FlowChangeApprovalPendingListServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		//onChange: change Page
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);

			serviceBean.getParam().getListSelection().setSelectedIds("PJT-1604080002", "PJT-1604200002", "PJT-1604200004");
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
		
			serviceBean.getParam().getListSelection().setSelectedIds("PJT-1604200005");
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(1);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
			
			serviceBean.getParam().getListSelection().setSelectedIds("PJT-1604080005", "PJT-1604200003");
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
			
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(1);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
		}
		
		// onChange: search condition
		{
			{
				int testCase = 0;

				switch (testCase) {
					case 1:
						//Category Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						break;

					case 2:
						//Milestone Search
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 3:
						//CtgId + MstoneId Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 4:
						//keyword search
						serviceBean.getParam().getSearchCondition().setKeyword(testKeyword);
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					default:
						break;
				}
			}

			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
		}
		
		//submit Change
		{
			serviceBean.getParam().getListSelection().setSelectedIds("PJT-1604080002");
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangeApprovalPendingListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return;
	}

	private void debug( FlowChangeApprovalPendingListServiceBean serviceBean ) {
		System.out.println("================================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("================================");
		System.out.println("");

		System.out.println("Search Condition ");
		System.out.print("  CtgId:= " + serviceBean.getParam().getSearchCondition().getCtgId() + "   ");
		System.out.print("  MstoneId:= " + serviceBean.getParam().getSearchCondition().getMstoneId());
		System.out.println("");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getCtgViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Keyword " + serviceBean.getParam().getSearchCondition().getKeyword());

		System.out.println("");
		
		System.out.println("Selected Change Property: ");
		for ( String selectedPjtId : serviceBean.getParam().getListSelection().getSelectedIdSet() ) {
			System.out.print("    " + selectedPjtId );
		}
		System.out.println("");
		System.out.println("");

		System.out.println("Pjt Approval Pending List");
		for ( ChangePropertyView view: serviceBean.getChangePropertyViews()) {
			System.out.print("  PjtId: " + view.getPjtId());
			System.out.print("  ReferenceId: " + view.getReferenceId());
			System.out.print("  Select: " + view.isSelected());
			System.out.println("");
			for ( AreqView areqView : view.getAreqViews() ) {
				System.out.print("  	AreqId: " + areqView.getAreqId());
				System.out.print("  	SubmitterNm: " + areqView.getSubmitterNm());
				System.out.print("  	AssigneeNm: " + areqView.getAssigneeNm());
				System.out.print("  	RequestDate: " + areqView.getRequestDate());
				System.out.print("  	Status: " + areqView.getStatus());
				System.out.println("");

			}

		}
		System.out.println("");

		System.out.println("Page List Info");
		System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
		System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
		System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
		System.out.print("  ViewRows:= " + serviceBean.getPage().getViewRows());
		System.out.print("  ViewFrom:= " + serviceBean.getPage().getViewRangeFrom());
		System.out.println("  ViewTo:= " + serviceBean.getPage().getViewRangeTo());
		System.out.println("");

	}
}
