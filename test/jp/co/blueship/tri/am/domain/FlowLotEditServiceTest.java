package jp.co.blueship.tri.am.domain;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.ILotGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.AuthorizedGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.BuildEnvironment;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.MailRecipientGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.Module;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.ReleaseEnvironment;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotEditServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotEditServiceBean.LotDetailsView;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowLotEditServiceTest {

	public void edit(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {

		// Set Param Bean
		FlowLotEditServiceBean serviceBean = new FlowLotEditServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.setLanguage("en");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		LotEditInputBean inputInfo = serviceBean.getParam().getInputInfo();

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmLotEditService.value(), serviceDto);
			this.debug(serviceBean);

			FlowChaLibLotModifyServiceBean src =  serviceBean.getInnerService();
			LotEditInputV3Bean srcInfo = src.getLotEditInputBean();
			LotDetailsView lotView = serviceBean.getDetailsView();
			System.out.println("Lot no 				: " + lotView.getLotId());
			System.out.println("Status Id           		: " + lotView.getStsId());
			System.out.println("Status 				: " + lotView.getStatus());
			System.out.println("Created time			: " + lotView.getCreatedTime());
			System.out.println("Lot name 			: " + inputInfo.getLotSubject());
			System.out.println("Lot Summary 			: " + inputInfo.getLotSummary());
			System.out.println("Lot content 			: " + inputInfo.getLotContents());
			System.out.println("Base Tag Line 			: " + lotView.getBranchBaselineTag());
			System.out.println("Latest base line tag		: " + lotView.getLatestBaselineTag());
			System.out.println("Is use merge			: " + lotView.isUseMerge());
			System.out.println("Public path 			: " + lotView.getPublicPath());
			System.out.println("Private path 			: " + lotView.getPrivatePath());
			System.out.println("Is allow list view 		: " + lotView.isEnableUnauthorizedGroup());
			System.out.println();

			System.out.println("------------------Authorized Group------------------------------------");
			List<AuthorizedGroup> authgrp = inputInfo.getAuthorizedGroups();
			System.out.printf("%-15s%-12.5s%s" ,"Selected", "Group Id" , "Group Name");
			System.out.println();
			for(AuthorizedGroup auth : authgrp){
				System.out.printf("%-15s%-12.5s%s" ,auth.isSelected(), auth.getGroupId() , auth.getGroupNm());
				System.out.println();
			}
			System.out.println("-----------------------------------------------------------------------");



			System.out.println("--------------------Mail Group-----------------------------------------");
			List<MailRecipientGroup> mailgrp = inputInfo.getMailRecipientGroups();
			System.out.printf("%-15s%-12.5s%s" ,"Selected", "Mail1 Id" , "Mail1 Name");
			System.out.println();
			for(MailRecipientGroup mail : mailgrp){
				System.out.printf("%-15s%-12.5s%s", mail.isSelected(), mail.getGroupId() ,  mail.getGroupNm());
				System.out.println();
			}
			System.out.println("------------------------------------------------------------------------");



			System.out.println("-----------------------Module-------------------------------------------");
			List<Module> modulegrp = inputInfo.getModules();
			System.out.printf("%-15s%-12.5s%s" ,"Selected", "モジュール名" , " リポジトリ");
			System.out.println();
			for(Module mdl : modulegrp){
				System.out.printf("%-15s%-12.5s%s" ,mdl.isSelected(), mdl.getModuleNm() , mdl.getRepository());
				System.out.println();
			}
			System.out.println("-------------------------------------------------------------------------");



			System.out.println("-----------------------------Build Env-----------------------------------");

			List<BuildEnvironment> bldgrp = inputInfo.getBuildEnvs();
			System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%s" , "Build No" , "Full Build No" ,"Build Id", " Build Name" , "Summary" , "StatusId" , "Created time" );
			System.out.println();
			for (BuildEnvironment bld : bldgrp ){
				System.out.printf("%-20s%-20s%-20s%-20s%-20s%-20s%s" , bld.isSelectedBuild() , bld.isSelectedFullBuild(),
				 bld.getEnvId() , bld.getEnvironmentNm() , bld.getSummary() , bld.getStsId() , bld.getUpdTime());
				System.out.println();
			}
			System.out.println("--------------------------------------------------------------------------");



			System.out.println("--------------------------Release Environment------------------------------");
			String selectedRelId = srcInfo.getSelectedRelEnvNoString();
			System.out.println(selectedRelId);
			List<ReleaseEnvironment> rlsgrp = inputInfo.getReleaseEnvs();

			System.out.printf("%-20s%-20s%-20s%-20s%-20s%s" ,"Selected", "Release Env No" , "Release Env Name"  , " Release Summary" , "StatusId" , "Created time");
			System.out.println();
			for(ReleaseEnvironment rel : rlsgrp){
				System.out.printf("%-20s%-20s%-24s%-24s%-20s%s" , rel.isSelected() , rel.getEnvId() , rel.getEnvironmentNm() , rel.getSummary()
						, rel.getStsId(), rel.getUpdTime() );
				System.out.println();
			}
			System.out.println("---------------------------------------------------------------------------");


		}

		// RequestType = submitChages
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			inputInfo.setLotSubject		("yyy1112");
			inputInfo.setLotSummary		("yyy TESTIN123G TESTING SUMMARY 123");
			inputInfo.setLotContents	("yyyy TESTING12321 TESTING CONTENT 456");

			{ //Authorized group
				AuthorizedGroup group1 = inputInfo.new AuthorizedGroup().setGroupId("1").setGroupNm("").setSelected(true);
				AuthorizedGroup group2 = inputInfo.new AuthorizedGroup().setGroupId("2").setGroupNm("").setSelected(true);
				AuthorizedGroup group3 = inputInfo.new AuthorizedGroup().setGroupId("3").setGroupNm("").setSelected(true);
				inputInfo.setAuthorizedGroups(FluentList.from(new AuthorizedGroup[] { group1, group2, group3 }).asList());
			}
			{ // Mail group
				MailRecipientGroup mail1 = inputInfo.new MailRecipientGroup().setGroupId("1").setGroupNm("").setSelected(true);
				MailRecipientGroup mail2 = inputInfo.new MailRecipientGroup().setGroupId("2").setGroupNm("").setSelected(true);
				MailRecipientGroup mail3 = inputInfo.new MailRecipientGroup().setGroupId("3").setGroupNm("").setSelected(true);
				inputInfo.setMailRecipientGroups(FluentList.from(new MailRecipientGroup[]{mail1, mail2, mail3}).asList());
			}
			{ // Build Environment
				BuildEnvironment diff = inputInfo.new BuildEnvironment().setEnvId("BPENV_TRINITY").setSelectedBuild(true);
				BuildEnvironment full = inputInfo.new BuildEnvironment().setEnvId("BPENV_TRINITY_F").setSelectedFullBuild(true);
				inputInfo.setBuildEnvs(FluentList.from(new BuildEnvironment[] { diff, full }).asList());
			}
			service.execute(ServiceId.AmLotEditService.value(), serviceDto);
			this.debug(serviceBean);
			support.threadWait( serviceBean );
		}

		ILotDao dao = (ILotDao)support.getBean( "amLotDao" );
		ILotMailGrpLnkDao mDao = (ILotMailGrpLnkDao)support.getBean("amLotMailGrpLnkDao");
		ILotGrpLnkDao grpDao = (ILotGrpLnkDao)support.getBean("amLotGrpLnkDao");
		LotCondition condition = new LotCondition();
		condition.setLotId(lotId);
		ILotEntity lotEntity = dao.findByPrimaryKey(condition.getCondition());
		List<ILotGrpLnkEntity> grpEntity = grpDao.find(condition.getCondition());
		List<ILotMailGrpLnkEntity> mailEntity = mDao.find(condition.getCondition());

		System.out.println(lotEntity.getLotNm());
		System.out.println(lotEntity.getSummary());
		System.out.println(lotEntity.getContent());

		for(ILotMailGrpLnkEntity mail : mailEntity){
			System.out.println(mail.getGrpId());
			System.out.println(mail.getGrpNm());
		}

		System.out.println("Authorized group:");
		for(ILotGrpLnkEntity grp : grpEntity){
			System.out.println(grp.getGrpId());
			System.out.println(grp.getGrpNm());
		}
	}

	private void debug( FlowLotEditServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

        if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {

        	System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
