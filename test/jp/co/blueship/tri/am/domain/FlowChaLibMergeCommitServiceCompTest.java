package jp.co.blueship.tri.am.domain;

import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

import org.junit.Test;

public class FlowChaLibMergeCommitServiceCompTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Cha-Merge-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowChaLibMergeCommitService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowChaLibMergeCommitServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			service.execute(FLOW_ACTION_ID, serviceDto);

			this.threadWait( paramBean );

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private FlowChaLibMergeCommitServiceBean getParamBean() {
		FlowChaLibMergeCommitServiceBean paramBean = new FlowChaLibMergeCommitServiceBean();

		paramBean.setUserId( getAuthUserId() );
		paramBean.setUserName( getAuthUserName() );

		paramBean.setReferer( ChaLibScreenID.MERGECOMMIT_CONFIRM );
		paramBean.setForward( ChaLibScreenID.COMP_MERGECOMMIT );
		paramBean.setScreenType( ScreenType.next );

		paramBean.setLockLotNo( "LOT-1407170029" );
		paramBean.setSelectedLotId( "LOT-1407170029" );
		paramBean.setSelectedLotVersionTag( "BLD-1407170031" );

		{
			MergeEditInputBean inputBean = new MergeEditInputBean();
			inputBean.setMergeComment( "test用バッチからマージ" );

			paramBean.setMergeEditInputBean( inputBean );
		}

		return paramBean;
	}

}
