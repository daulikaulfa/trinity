package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceChangePasswordBean;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowLoginServiceChangePasswordTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml",
			"Test-Tri-CheckoutRequestDetail-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String currentPassword = null;
	private String newPassword = null;
	private String confirmPassword = null;

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			this.detailsOnChange(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void detailsOnChange(IGenericTransactionService service) throws Exception {

		FlowLogInServiceChangePasswordBean serviceBean = new FlowLogInServiceChangePasswordBean();
		{
			serviceBean.setUserId("admin");
			serviceBean.setLanguage("en");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		currentPassword = "aaatrinity2016";

		{	//onchange
			newPassword = "NGZZHHENSANM1G2345";
			confirmPassword = "NGZZHHENSANM1G2345";

			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setNewPassword(newPassword);
			service.execute(ServiceId.SmLogInServiceChangePassword.value(), serviceDto);
			debugInfo( serviceBean );
		}

		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().setCurrentPassword(currentPassword);
			serviceBean.getParam().setNewPassword(newPassword);
			serviceBean.getParam().setConfirmPassword(confirmPassword);
			service.execute(ServiceId.SmLogInServiceChangePassword.value(), serviceDto);
		}
	}

	private void debugInfo( FlowLogInServiceChangePasswordBean serviceBean ){

		boolean sameCharacter = serviceBean.getLoginDetailsView().getPassword().isSameCharacterLessThan3Times();
		boolean upLowerCase = serviceBean.getLoginDetailsView().getPassword().containsUppercaseAndLowercase();
		boolean containNo = serviceBean.getLoginDetailsView().getPassword().containsNumerals();
		boolean morethan8ch = serviceBean.getLoginDetailsView().getPassword().isMoreThan8characters();

		System.out.println("Password contains more than 8 characters 	:" + morethan8ch);
		System.out.println("Password contains numeric 			:" + containNo);
		System.out.println("Password contains uppercase and lowercase 	:" + upLowerCase);
		System.out.println("Same character less than 3 times		: " + sameCharacter);

		System.out.print("The complexity level :");
		System.out.println(serviceBean.getLoginDetailsView().getPassword().getPasswordComplexityLevel());

	}
}

