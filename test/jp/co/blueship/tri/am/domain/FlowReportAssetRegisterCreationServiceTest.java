package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowReportAssetRegisterCreationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Report-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		//final String FLOW_ACTION_ID = "FlowChaLibAssetRegisterService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service,"LOT-1605220001");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create(IGenericTransactionService service , String lotId ) throws Exception{

		// Set Param Bean
		FlowReportAssetRegisterCreationServiceBean serviceBean = new FlowReportAssetRegisterCreationServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.setScreenType( ScreenType.Type.next.value() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		serviceBean.getParam().setSelectedLotId(lotId);

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.DcmReportAssetRegisterCreationService.value(), serviceDto);
			this.debug(serviceBean);
		}

		System.out.println("Success");
	}

	private void debug( FlowReportAssetRegisterCreationServiceBean serviceBean ){

		System.out.println( serviceBean.getDetailsView().getLatestBaselineTag() );

		SearchCondition condition = serviceBean.getParam().getSearchCondition();

		System.out.println("変更管理番号");
		for( String pjtId : condition.getPjtViews() ){
			System.out.println(pjtId);
		}

		System.out.println("申請ユーザー");
		for( ItemLabelsBean user : condition.getSubmitterViews() ){
			System.out.println(user.getValue());
		}

	}

}
