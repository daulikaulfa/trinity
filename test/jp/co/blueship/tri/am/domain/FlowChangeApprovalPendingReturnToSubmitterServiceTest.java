package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingReturnToSubmitterServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class FlowChangeApprovalPendingReturnToSubmitterServiceTest extends TestDomainSupport {

	private static final String checkinAreqId = "LND-1604150007"; //
	private static final String removalRequestAreqId = "MSD-1604150023"; //

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.checkinReject(service);
			this.removalRequestReject(service);
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void checkinReject(IGenericTransactionService service) throws Exception{

		FlowChangeApprovalPendingReturnToSubmitterServiceBean serviceBean = new FlowChangeApprovalPendingReturnToSubmitterServiceBean();

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedAreqId(checkinAreqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangeApprovalReturnToSubmitterService.value(), serviceDto);
		}


		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getInputInfo().setComment("Some reasons");
			service.execute(ServiceId.AmChangeApprovalReturnToSubmitterService.value(), serviceDto);
		}

		if (serviceBean.getResult().isCompleted()) {
			System.out.println("finished!");
		}

	}

	private void removalRequestReject(IGenericTransactionService service) throws Exception{

		FlowChangeApprovalPendingReturnToSubmitterServiceBean serviceBean = new FlowChangeApprovalPendingReturnToSubmitterServiceBean();

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedAreqId(removalRequestAreqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangeApprovalReturnToSubmitterService.value(), serviceDto);
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getInputInfo().setComment("Some reasons");
			service.execute(ServiceId.AmChangeApprovalReturnToSubmitterService.value(), serviceDto);
		}

		if (serviceBean.getResult().isCompleted()) {
			System.out.println("finished!");
		}
	}

}
