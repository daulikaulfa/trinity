package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean.MergeHistoryView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class FlowMergeHistoryDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-MergeDetails-Context.xml"};
	
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}
	
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();
			
			this.mergeHistoryList(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void mergeHistoryList(IGenericTransactionService service) throws Exception {
		FlowMergeHistoryDetailsServiceBean serviceBean = new FlowMergeHistoryDetailsServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());

		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		int selectedPageNo = 1;
		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmMergeHistoryDetailsService.value(), serviceDto);
			
			if (serviceBean.getPage().isNext()) {
				selectedPageNo++;
			}
			
			this.debug( serviceBean );
		}
		
		// onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo( selectedPageNo );
			service.execute(ServiceId.AmMergeHistoryDetailsService.value(), serviceDto);
			this.debug( serviceBean );
		}
	}
	
	private void debug( FlowMergeHistoryDetailsServiceBean serviceBean ) {
		for ( MergeHistoryView view : serviceBean.getMergeHistoryViews() ) {
			System.out.println( "LotId 				:= " + view.getLotId() );
			System.out.println( "LotNm 				:= " + view.getLotNm() );
			System.out.println( "headBlId 			:= " + view.getHeadBlId() );
			System.out.println( "noOfChangeProperty := " + view.getNoOfChangePropertys() );
			System.out.println( "startTime 			:= " + view.getStartTime() );
			System.out.println( "endTime 			:= " + view.getEndTime() );
			System.out.println( "stsId 				:= " + view.getStsId() );
			System.out.println( "status 			:= " + view.getStatus() );
			System.out.println();
		}
	}
}
