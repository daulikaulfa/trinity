package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean.BaselineView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowConflictCheckServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-MergeDetails-Context.xml"};

	private static final String testLotId = "LOT-1605200009";

	private static String lotVerTag = "";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.conflictCheck(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void conflictCheck(IGenericTransactionService service) throws Exception {
		FlowConflictCheckServiceBean serviceBean = new FlowConflictCheckServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmConflictCheckService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// submitChanges validate only
		{
			serviceBean.getParam().setRequestType(RequestType.validate);
			serviceBean.getParam().getInputInfo().setSelectedLotVerTag(lotVerTag);
			service.execute(ServiceId.AmConflictCheckService.value(), serviceDto);
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmConflictCheckService.value(), serviceDto);
			this.threadWait(serviceBean);
			this.debug(serviceBean);
		}
	}

	private void debug(FlowConflictCheckServiceBean serviceBean) {
		int count = 0;
		if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
			for (BaselineView view : serviceBean.getBaselineViews()) {

				if (count == 0) {
					lotVerTag = view.getLotVerTag();
				}
				System.out.println("lotVerTag                   := " + view.getLotVerTag());
				System.out.println("lotBlTag                    := " + view.getLotBlTag());
				System.out.println("bpIds                       := " + view.getBpIds());
				System.out.println("rpIds                       := " + view.getRpIds());
				System.out.println("lotCheckinSubmitterNm       := " + view.getLotCheckinSubmitterNm());
				System.out.println("lotCheckinSubmitterIconPath := " + view.getLotCheckinSubmitterIconPath());
				System.out.println("lotCheckinTime              := " + view.getLotCheckinTime());
				count++;
			}
		} else if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
		}
	}
}
