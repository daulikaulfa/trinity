package jp.co.blueship.tri.am.domain;


import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCloseServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowLotCloseServiceTest {

	public void close(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {

		// Set Param Bean
		FlowLotCloseServiceBean serviceBean = new FlowLotCloseServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.setScreenType( ScreenType.Type.next.value() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		serviceBean.getParam().setSelectedLotId(lotId);

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmLotCloseService.value(), serviceDto);
			this.debug(serviceBean);

		}

		/*// RequestType = submitChages
		if (!serviceBean.getResult().isCompleted() ) {
			serviceBean.getParam().getInputInfo().setReason("For backend test");
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmLotCloseService.value(), serviceDto);
			this.debug(serviceBean);

			ILotDao dao = (ILotDao)support.getBean( "amLotDao" );
			LotCondition condition = new LotCondition();
			condition.setLotId(lotId);
			ILotEntity lotEntity = dao.findByPrimaryKey(condition.getCondition());

			if(serviceBean.getResult().isCompleted()){
				System.out.println("Submit changes is done");
			}

			if(null!= lotEntity){

				if(AmLotStatusId.LOT_CLOSE.equals(lotEntity.getStsId())){
					System.out.println(lotEntity.getStsId());
				}
			}

		}*/
	}

	private void debug( FlowLotCloseServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
