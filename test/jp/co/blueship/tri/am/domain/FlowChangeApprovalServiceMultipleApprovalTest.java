package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalServiceMultipleApprovalBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 *
 */

public class FlowChangeApprovalServiceMultipleApprovalTest extends TestDomainSupport {
	
	private static final String selectedLotId = "LOT-1604140002";
	private static final String[] selectedAreqIds = new String[] {"LND-1604150013", "LND-1604150014"};
//	private static final String[] selectedPjtIds = new String[] {"PJT-1604140001", "PJT-1604140003"};
	
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};
	
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.pjtMultipleApprove(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void pjtMultipleApprove(IGenericTransactionService service) throws Exception{
		
		FlowChangeApprovalServiceMultipleApprovalBean serviceBean = new FlowChangeApprovalServiceMultipleApprovalBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().getInputInfo().setSelectedAreqIds(selectedAreqIds);
			serviceBean.getParam().setSelectedLotId(selectedLotId);
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangeApprovalServiceMultipleApproval.value(), serviceDto);
		}
		
		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangeApprovalServiceMultipleApproval.value(), serviceDto);
			this.threadWait( serviceBean );
		}
				
	}
}
