package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean.Module;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean.ConflictCheckResultOverview;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean.LotDetailsViewBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMergeServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-MergeDetails-Context.xml"};

	private static final String testLotId = "LOT-1605200009";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.merge(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void merge(IGenericTransactionService service) throws Exception {
		FlowMergeServiceBean serviceBean = new FlowMergeServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmMergeService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// submitChanges validate only
		{
			serviceBean.getParam().setRequestType(RequestType.validate);
			service.execute(ServiceId.AmMergeService.value(), serviceDto);
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmMergeService.value(), serviceDto);
			this.threadWait(serviceBean);
			this.debug(serviceBean);
		}
	}

	private void debug(FlowMergeServiceBean serviceBean) {
		if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {

			// Lot Detail View
			{
				LotDetailsViewBean lotViewBean = serviceBean.getLotDetailsView();
				System.out.println("========LotDetailView=======");
				System.out.println( "LotId             := " + lotViewBean.getLotId() );
				System.out.println( "stsId             := " + lotViewBean.getStsId() );
				System.out.println( "status            := " + lotViewBean.getStatus() );
				System.out.println( "createdTime       := " + lotViewBean.getCreatedTime() );
				System.out.println( "summary           := " + lotViewBean.getSummary() );
				System.out.println( "contents          := " + lotViewBean.getContents() );
				System.out.println( "latestBaselineTag := " + lotViewBean.getLatestBaselineTag() );
				System.out.println( "latestVerTag      := " + lotViewBean.getLatestVerTag() );
				System.out.println( "branchBaselineTag := " + lotViewBean.getBranchBaselineTag() );
				for ( Module mdl : lotViewBean.getModules() ) {
					System.out.print  ( "Module Name       := " + mdl.getModuleNm() );
					System.out.println( "	Repository     := " + mdl.getRepository() );
				}
				System.out.println("============================");
			}

			// Baseline Detail View
			{
				ConflictCheckResultOverview baselineView = serviceBean.getBaselineView();
				System.out.println("========BaselineView=======");
				System.out.println( "headBlId             := " + baselineView.getHeadBlId() );
				System.out.println( "stsId                := " + baselineView.getStsId() );
				System.out.println( "status               := " + baselineView.getStatus() );
				System.out.println( "conflictCheckComment := " + baselineView.getConflictCheckComment() );
				System.out.println( "startTime            := " + baselineView.getStartTime() );
				System.out.println( "endTime              := " + baselineView.getEndTime() );
				System.out.println( "mergeEnabled         := " + baselineView.isMergeEnabled() );
				System.out.println("============================");
			}

			// Change property view
			{
				for (ChangePropertyDetailsView chgView : serviceBean.getChangePropertyViews()) {
					System.out.println("========ChangePropertyView=======");
					System.out.println("pjtId 				:= " + chgView.getPjtId());
					System.out.println("referenceId			:= " + chgView.getReferenceId() );
					System.out.println("referenceCategoryNm := " + chgView.getReferenceCategoryNm());
					System.out.println("summary				:= " + chgView.getSummary());
					System.out.println("submitterNm			:= " + chgView.getSubmitterNm());
					System.out.println("submitterIconPath	:= " + chgView.getSubmitterIconPath());
					System.out.println("assigneeNm			:= " + chgView.getAssigneeNm());
					System.out.println("assigneeIconPath	:= " + chgView.getAssigneeIconPath());
					System.out.println("createdTime			:= " + chgView.getCreatedTime());
					System.out.println("updTime				:= " + chgView.getUpdTime());
					System.out.println("stsId				:= " + chgView.getStsId());
					System.out.println("status				:= " + chgView.getStatus());
					System.out.println("============================");
				}
			}
		} else if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
		}
	}
}
