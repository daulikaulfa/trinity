package jp.co.blueship.tri.am.svc.areq;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.am.dao.areq.LendingReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.areq.beans.ActionNumberingApplyNo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link ActionNumberingApplyNo}のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class ActionNumberingApplyNoTest extends TriMockTestSupport {

	private static final String EXPECTED_NUMBER = "expected_number";
	private ActionNumberingApplyNo testee;
	private LendingReqNumberingDao mockNumberingDao;
	private IAreqEntity assetApplyEntity;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		testee = new ActionNumberingApplyNo();
		mockNumberingDao = createMock(LendingReqNumberingDao.class);
		testee.setNumberingDao(mockNumberingDao);
		assetApplyEntity = new AreqEntity();

	}

	/**
	 * {@link ActionNumberingApplyNo#execute(java.util.List)}メソッドのユニットテスト。
	 * 入力パラメタかAssetApplyEntityが抽出され、かつ同Entityに採番された申請番号が設定されることを確認する。
	 */
	@Test
	public void testExecute() {

		IServiceDto<FlowChaLibLendEntryServiceBean> serviceDto = new ServiceDto<FlowChaLibLendEntryServiceBean>();
		List<Object> inputParam = Arrays.asList(new Object(), new Object(), assetApplyEntity, new Object());
		serviceDto.setParamList( inputParam );

		mockNumberingDao.nextval();
		expectLastCall().andReturn(EXPECTED_NUMBER);

		replayAll();
		testee.execute(serviceDto);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(inputParam), is(true));
		assertThat(inputParam.size(), is(4));

		Object actual = inputParam.get(2);
		assertThat(actual, instanceOf(IAreqEntity.class));
		assertThat(((IAreqEntity) actual).getAreqId(), is(EXPECTED_NUMBER));

	}
}
