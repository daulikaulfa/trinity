package jp.co.blueship.tri.am.svc.areq;

import static org.easymock.EasyMock.*;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.am.domain.areq.beans.EhRecorderExtensionCheckReturnAsset;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EhRecorderExtensionCheckReturnAssetTest extends TriMockTestSupport {

	private EhRecorderExtensionCheckReturnAsset testee;
	private IDomain<IGeneralServiceBean> mockBeforeAfterAction;
	private IDomain<IGeneralServiceBean> mockAction;

	@SuppressWarnings("unused")
	private static final String EXPECTED_MESSAGE = "メソッドの使用方法に誤り : 業務コード ,// CODE1 サーバ名 ,// CODE2 ユーザ名 ,// CODE3";
	@SuppressWarnings("unused")
	private static final String EXPECTED_MESSAGE_ID = "SM005019S";

	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {
		testee = new EhRecorderExtensionCheckReturnAsset();
		mockBeforeAfterAction = createMock(IDomain.class);
		testee.setBeforeAction(mockBeforeAfterAction);
		testee.setAfterAction(mockBeforeAfterAction);
		mockAction = createMock(IDomain.class);
		testee.setAction(mockAction);
	}

	@Test
	private void expectCallsOnBeforeAfterAction(IServiceDto<IGeneralServiceBean> inputDto) {
		mockBeforeAfterAction.execute(inputDto);
		expectLastCall().andReturn(null).times(2);
	}

}
