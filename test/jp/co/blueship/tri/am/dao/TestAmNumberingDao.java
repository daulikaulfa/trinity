package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;

import org.junit.Test;

public class TestAmNumberingDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void nextval() {
		String[] beans = new String[]{
				"amLotNumberingDao",
				"amPjtNumberingDao",
				"amLendingReqNumberingDao",
		};

		for ( String bean: beans ) {
			INumberingDao dao = (INumberingDao)this.getBean( bean );
			String id = dao.nextval();

			assertTrue( null != id );
			System.out.println( bean + " := " + id );
			assertTrue( ! id.equals( dao.nextval() ) );
		}

	}

	//@Ignore
	@Test
	public void nextval2() {
		String[] beans = new String[]{
				"amPjtAvlNumberingDao",
		};

		for ( String bean: beans ) {
			INumberingDao dao = (INumberingDao)this.getBean( bean );
			String id = dao.nextval();

			assertTrue( null != id );
			System.out.println( bean + " := " + id );
		}

	}

}
