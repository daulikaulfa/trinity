package jp.co.blueship.tri.am.dao;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.IViewAssetRegisterDao;
import jp.co.blueship.tri.am.dao.areq.eb.IViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterCondition;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;

import org.junit.Test;

public class TestViewAssetRegisterDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		try {
			IViewAssetRegisterDao dao = (IViewAssetRegisterDao)this.getBean( "amViewAssetRegisterDao" );

			ViewAssetRegisterCondition condition = new ViewAssetRegisterCondition();
			//condition.setLotId( "LOT-1409290001" );
			condition.setLotNm( "動作" );
			condition.setMdlNm( "trinity-schemaCompiler" );
			condition.setFilePath( "sample.txt" );

			List<IViewAssetRegisterEntity> entities = dao.find(condition);

			System.out.println( "size:=" + entities.size() );

			for ( IViewAssetRegisterEntity entity: entities ) {
				System.out.println( "LotId:=" + entity.getLotId() );
				System.out.println( "AreqId:=" + entity.getAreqId() );
				System.out.println( "AreqCtgCd:=" + entity.getAreqCtgCd() );
				System.out.println( "FilePath:=" + entity.getFilePath() );
			}
		} catch ( Exception e ) {
			e.printStackTrace();
		}

	}

}
