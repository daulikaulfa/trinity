package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;

public class TestLotDao extends TestTriJdbcDaoSupport {

	@Ignore
	@Test
	public void find() {
		ILotDao dao = (ILotDao)this.getBean( "amLotDao" );

		LotCondition condition = new LotCondition();
		condition.setLotId( "LOT-1505080002" );
		//condition.setStsIds( new String[]{"1"} );
		//condition.setProcStsIds( new String[]{"10"} );
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.lotId, TriSortOrder.Asc, 1);

		IEntityLimit<ILotEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		System.out.println( "size:=" + home.getEntities().size() );
		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
		}

	}

	@Ignore
	@Test
	public void findWithAccsHist() {
		ILotDao dao = (ILotDao)this.getBean( "amLotDao" );

		LotCondition condition = new LotCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(AccsHistItems.accsTimestamp, TriSortOrder.Desc, 1);
		sort.setElement(AreqItems.lotId, TriSortOrder.Asc, 2);

		IEntityLimit<ILotEntity> home = dao.find( condition.getCondition(), sort, 1, 5 );

		System.out.println( "size:=" + home.getEntities().size() );
		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
		}

	}

	//@Ignore
	@Test
	public void findWithAccsHistByLotIdOrLotNm() {
		ILotDao dao = (ILotDao)this.getBean( "amLotDao" );

		LotCondition condition = new LotCondition();
		condition.setContainsByKeyword( "8000" );
		condition.setJoinAccsHist(true);

		ISqlSort sort = new SortBuilder();
		sort.setElement(AccsHistItems.accsTimestamp, TriSortOrder.Desc, 1);
		sort.setElement(AreqItems.lotId, TriSortOrder.Asc, 2);

		List<ILotEntity> entities = dao.find( condition.getCondition(), sort );

		System.out.println( "size:=" + entities.size() );
		assertTrue( null != entities );
		assertTrue( 0 < entities.size());

		for ( ILotEntity entity: entities ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
		}
	}

}
