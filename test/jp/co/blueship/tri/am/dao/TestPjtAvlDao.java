package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlDao;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlItems;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

public class TestPjtAvlDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		IPjtAvlDao dao = (IPjtAvlDao)this.getBean( "amPjtAvlDao" );

		PjtAvlCondition condition = new PjtAvlCondition();
		//condition.setLotId( "LOT-1" );
		ISqlSort sort = new SortBuilder();
		sort.setElement(PjtAvlItems.lotId, TriSortOrder.Desc, 1);

		IEntityLimit<IPjtAvlEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IPjtAvlEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "pjtId:=" + entity.getPjtId() );
			System.out.println( "stsId:=" + entity.getStsId() );
		}

	}

}
