package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

public class TestAreqDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		try {
			IAreqDao dao = (IAreqDao)this.getBean( "amAreqDao" );

			AreqCondition condition = new AreqCondition();
			//condition.setJoinAccsHist(true);
			//condition.setLotId( "LOT-1406280023" );
			//condition.setPjtId( "PJT-1406280001" );
			//condition.setStsIds( new String[]{"5200"} );
			//condition.setDelStsIds(StatusFlg.on);
			//condition.setProcStsIds( new String[]{"5200"} );
			//condition.setMstoneId("16030001");
			ISqlSort sort = new SortBuilder();
			sort.setElement(AreqItems.areqId, TriSortOrder.Desc, 1);

			IEntityLimit<IAreqEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

			assertTrue( null != home );
			assertTrue( null != home.getEntities() );
			assertTrue( 0 < home.getEntities().size());

			for ( IAreqEntity entity: home.getEntities() ) {
				System.out.print( " lotId:=" + entity.getLotId() );
				System.out.print( " areqId:=" + entity.getAreqId() );
				System.out.print( " stsId:=" + entity.getStsId() );
				System.out.print( " procStsId:=" + entity.getProcStsId() );
				System.out.print( " reqTimestamp:=" + entity.getReqTimestamp() );
				System.out.print( " pjtSubject:=" + entity.getPjtSubject() );
				System.out.print( " assineeId:=" + entity.getAssigneeId() );
				System.out.print( " assineeNm:=" + entity.getAssigneeNm() );
				System.out.print( " ctgId:=" + entity.getCtgId() );
				System.out.print( " ctgNm:=" + entity.getCtgNm() );
				System.out.print( " mstoneId:=" + entity.getMstoneId() );
				System.out.print( " mstoneNm:=" + entity.getMstoneNm() );
				System.out.print( " rtnReqDueDate:=" + entity.getRtnReqDueDate() );
				System.out.println( "" );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
