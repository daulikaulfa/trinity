package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.lot.ILotGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.constants.LotGrpLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

public class TestLotGrpLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		ILotGrpLnkDao dao = (ILotGrpLnkDao)this.getBean( "amLotGrpLnkDao" );

		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setLotId( "LOT-1606220001" );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotGrpLnkItems.grpId, TriSortOrder.Desc, 1);

		IEntityLimit<ILotGrpLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotGrpLnkEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "grpId:=" + entity.getGrpId() );
			System.out.println( "grpNm:=" + entity.getGrpNm() );
			System.out.println( "lotUpdTimestamp:=" + entity.getLotUpdTimestamp() );
		}

	}

}
