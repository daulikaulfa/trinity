package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.am.dao.lot.ILotMdlLnkDao;
import jp.co.blueship.tri.am.dao.lot.constants.LotMdlLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

import org.junit.Test;

public class TestLotMdlLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		ILotMdlLnkDao dao = (ILotMdlLnkDao)this.getBean( "amLotMdlLnkDao" );

		LotMdlLnkCondition condition = new LotMdlLnkCondition();
		condition.setLotId( "LOT-1406280018" );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotMdlLnkItems.mdlNm, TriSortOrder.Desc, 1);

		IEntityLimit<ILotMdlLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotMdlLnkEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "mdlNm:=" + entity.getMdlNm() );
		}

	}

}
