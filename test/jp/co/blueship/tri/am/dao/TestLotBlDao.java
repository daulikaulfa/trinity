package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.am.dao.baseline.ILotBlDao;
import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

import org.junit.Test;

public class TestLotBlDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		ILotBlDao dao = (ILotBlDao)this.getBean( "amLotBlDao" );

		LotBlCondition condition = new LotBlCondition();
		condition.setLotBlId("1");
		//condition.setStsIds( new String[]{"1"} );
		//condition.setProcStsIds( new String[]{"10"} );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotBlItems.lotId, TriSortOrder.Desc, 1);

		IEntityLimit<ILotBlEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		System.out.println( "size:=" + home.getEntities().size() );
		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotBlEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
		}

	}

}
