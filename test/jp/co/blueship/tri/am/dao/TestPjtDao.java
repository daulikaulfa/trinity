package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

public class TestPjtDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		IPjtDao dao = (IPjtDao)this.getBean( "amPjtDao" );

		PjtCondition condition = new PjtCondition();
		//condition.setCtgId("16030011");
		//condition.setPjtId("PJT-1507300001");
		//condition.setPjtId("PJT-1505150001");
		ISqlSort sort = new SortBuilder();
		sort.setElement(PjtItems.pjtId, TriSortOrder.Desc, 1);

		IEntityLimit<IPjtEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IPjtEntity entity: home.getEntities() ) {
			System.out.print( " lotId:=" + entity.getLotId() );
			System.out.print( " pjtId:=" + entity.getPjtId() );
			System.out.print( " stsId:=" + entity.getStsId() );
			System.out.print( " procStsId:=" + entity.getProcStsId() );
			System.out.print( " ctgId:=" + entity.getCtgId() );
			System.out.print( " ctgNM:=" + entity.getCtgNm() );
			System.out.print( " mstoneId:=" + entity.getMstoneId() );
			System.out.print( " mstoneNm:=" + entity.getMstoneNm() );
			System.out.print( " AssigneeIconPath:=" + entity.getAssigneeIconPath() );
			System.out.print( " RegUserIconPath:=" + entity.getRegUserIconPath() );
			System.out.println("");
		}

	}

}
