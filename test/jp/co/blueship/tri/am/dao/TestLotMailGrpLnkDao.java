package jp.co.blueship.tri.am.dao;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.constants.LotMailGrpLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

public class TestLotMailGrpLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		ILotMailGrpLnkDao dao = (ILotMailGrpLnkDao)this.getBean( "amLotMailGrpLnkDao" );

		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setLotId( "LOT-1603210001" );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotMailGrpLnkItems.grpId, TriSortOrder.Desc, 1);

		IEntityLimit<ILotMailGrpLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotMailGrpLnkEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "grpId:=" + entity.getGrpId() );
			System.out.println( "grpNm:=" + entity.getGrpNm() );
		}

	}

}
