package jp.co.blueship.tri.am.ws;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.am.ws.vo.LotPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.LotSearchConditions;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.test.support.TriMockTestSupport;
import jp.co.blueship.tri.test.util.DefaultValueGenerator;

import org.junit.Test;

/**
 * {@link LotSqlConditionBuilder}クラスのユニットテストクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class LotSqlConditionBuilderTest extends TriMockTestSupport {

	private static final String LOT_ID = "lotId";

	@Test
	public void testBuildByPrimaryKey() {

		LotPrimaryKey primaryKey = new LotPrimaryKey();
		primaryKey.setLotId(LOT_ID);

		LotSqlConditionBuilder testee = new LotSqlConditionBuilder(primaryKey);
		ISqlCondition actual = testee.buildByPrimaryKey().getCondition();
		String queryString = actual.toQueryString();

		assertThat(queryString, is(notNullValue()));
		assertThat(queryString, is(not("")));
	}

	@Test
	public void testBuild() throws Exception {

		LotSearchConditions conditions = DefaultValueGenerator.createBeanWithDefaultValue(LotSearchConditions.class);
		conditions.setCloseTimestamp("2014/05/20 12:30:50");
		conditions.setDelTimestamp("2014/05/20 12:30:50");
		conditions.setAllowListView(1);
		conditions.setAmProductActivate(1);
		conditions.setDelStsId(1);
		conditions.setIsAssetDup(1);
		conditions.setRmProductActivate(1);

		LotSqlConditionBuilder testee = new LotSqlConditionBuilder(conditions);
		ISqlCondition actual = testee.build().getCondition();
		String queryString = actual.toQueryString();
		assertThat(queryString, is(notNullValue()));
		assertThat(queryString, is(not("")));
	}

}
