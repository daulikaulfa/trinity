package jp.co.blueship.tri.am.ws;

import static java.util.Arrays.asList;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expectLastCall;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.ws.rs.core.Response;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.ws.vo.PjtPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.PjtSearchConditions;
import jp.co.blueship.tri.am.ws.vo.PjtVo;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionTemplate;
import jp.co.blueship.tri.test.support.TriMockTestSupport;
import jp.co.blueship.tri.test.util.DefaultValueGenerator;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link PjtWebService}クラスのユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class PjtWebServiceTest extends TriMockTestSupport {

	private PjtWebService testee;
	private IPjtDao mockIPjtDao;
	private Capture<ConditionTemplate> capturedConditionTemplate;

	@Before
	public void setUp() throws Exception {

		testee = new PjtWebService();
		mockIPjtDao = createMock(IPjtDao.class);
		testee.setPjtDao(mockIPjtDao);
		capturedConditionTemplate = new Capture<ConditionTemplate>();
	}

	@Test
	public void testGet() {

		IPjtEntity expecteResult = new PjtEntity();

		mockIPjtDao.findByPrimaryKey(capture(capturedConditionTemplate));
		expectLastCall().andReturn(expecteResult);

		PjtPrimaryKey primaryKey = new PjtPrimaryKey();
		primaryKey.setPjtId("PjtId");

		replayAll();
		Response actual = testee.get(primaryKey);
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(actual.getEntity(), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_PJT));
	}

	@Test
	public void testList() {

		IPjtEntity expecteResult1 = new PjtEntity();
		IPjtEntity expecteResult2 = new PjtEntity();

		mockIPjtDao.find(capture(capturedConditionTemplate));
		expectLastCall().andReturn(asList(expecteResult1, expecteResult2));

		replayAll();
		Response actual = testee.list();
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		@SuppressWarnings("unchecked")
		List<PjtVo> actualList = (List<PjtVo>) actual.getEntity();
		assertThat(actualList.size(), is(2));
		assertThat(actualList.get(0), is(notNullValue()));
		assertThat(actualList.get(1), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_PJT));
	}

	@Test
	public void testFind() throws Exception {

		IPjtEntity expecteResult1 = new PjtEntity();
		IPjtEntity expecteResult2 = new PjtEntity();

		mockIPjtDao.find(capture(capturedConditionTemplate));
		expectLastCall().andReturn(asList(expecteResult1, expecteResult2));

		PjtSearchConditions conditions = DefaultValueGenerator.createBeanWithDefaultValue(PjtSearchConditions.class);
		conditions.setCloseTimestamp("2014/05/20 12:30:50");
		conditions.setTestCompTimestamp("2014/05/20 12:30:50");
		conditions.setDelTimestamp("2014/05/20 12:30:50");
		conditions.setDelStsId(1);

		replayAll();
		Response actual = testee.find(conditions);
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		@SuppressWarnings("unchecked")
		List<PjtVo> actualList = (List<PjtVo>) actual.getEntity();
		assertThat(actualList.size(), is(2));
		assertThat(actualList.get(0), is(notNullValue()));
		assertThat(actualList.get(1), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_PJT));
	}

}
