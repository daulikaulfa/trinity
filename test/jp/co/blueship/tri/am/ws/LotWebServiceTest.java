package jp.co.blueship.tri.am.ws;

import static java.util.Arrays.asList;
import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.expectLastCall;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import javax.ws.rs.core.Response;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.eb.LotEntity;
import jp.co.blueship.tri.am.ws.vo.LotPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.LotSearchConditions;
import jp.co.blueship.tri.am.ws.vo.LotVo;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionTemplate;
import jp.co.blueship.tri.test.support.TriMockTestSupport;
import jp.co.blueship.tri.test.util.DefaultValueGenerator;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link LotWebService}クラスのユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class LotWebServiceTest extends TriMockTestSupport {

	private LotWebService testee;
	private ILotDao mockILotDao;
	private Capture<ConditionTemplate> capturedConditionTemplate;

	@Before
	public void setUp() throws Exception {

		testee = new LotWebService();
		mockILotDao = createMock(ILotDao.class);
		testee.setLotDao(mockILotDao);
		capturedConditionTemplate = new Capture<ConditionTemplate>();
	}

	@Test
	public void testGet() throws Exception {

		LotEntity expecteResult = createLotEntity();

		mockILotDao.findByPrimaryKey(capture(capturedConditionTemplate));
		expectLastCall().andReturn(expecteResult);

		LotPrimaryKey primaryKey = new LotPrimaryKey();
		primaryKey.setLotId("lotId");

		replayAll();
		Response actual = testee.get(primaryKey);
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		assertThat(actual.getEntity(), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_LOT));
	}

	@Test
	public void testList() throws Exception {

		LotEntity expecteResult1 = createLotEntity();
		LotEntity expecteResult2 = createLotEntity();

		mockILotDao.find(capture(capturedConditionTemplate));
		expectLastCall().andReturn(asList(expecteResult1, expecteResult2));

		replayAll();
		Response actual = testee.list();
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		@SuppressWarnings("unchecked")
		List<LotVo> actualList = (List<LotVo>) actual.getEntity();
		assertThat(actualList.size(), is(2));
		assertThat(actualList.get(0), is(notNullValue()));
		assertThat(actualList.get(1), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_LOT));
	}

	@Test
	public void testFind() throws Exception {

		LotEntity expecteResult1 = createLotEntity();
		LotEntity expecteResult2 = createLotEntity();

		mockILotDao.find(capture(capturedConditionTemplate));
		expectLastCall().andReturn(asList(expecteResult1, expecteResult2));

		LotSearchConditions conditions = DefaultValueGenerator.createBeanWithDefaultValue(LotSearchConditions.class);
		conditions.setCloseTimestamp("2014/05/20 12:30:50");
		conditions.setDelTimestamp("2014/05/20 12:30:50");
		conditions.setAllowListView(1);
		conditions.setAmProductActivate(1);
		conditions.setDelStsId(1);
		conditions.setIsAssetDup(1);
		conditions.setRmProductActivate(1);

		replayAll();
		Response actual = testee.find(conditions);
		verifyAll();

		assertThat(actual.getStatus(), is(Response.Status.OK.getStatusCode()));
		@SuppressWarnings("unchecked")
		List<LotVo> actualList = (List<LotVo>) actual.getEntity();
		assertThat(actualList.size(), is(2));
		assertThat(actualList.get(0), is(notNullValue()));
		assertThat(actualList.get(1), is(notNullValue()));

		ConditionTemplate template = capturedConditionTemplate.getValue();
		assertThat((AmTables) template.getTableAttribute(), is(AmTables.AM_LOT));
	}

	private LotEntity createLotEntity() throws Exception {

		LotEntity result = DefaultValueGenerator.createBeanWithDefaultValue(LotEntity.class);
		return result;
	}
}
