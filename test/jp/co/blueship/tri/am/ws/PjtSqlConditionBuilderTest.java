package jp.co.blueship.tri.am.ws;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.am.ws.vo.PjtPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.PjtSearchConditions;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.test.support.TriMockTestSupport;
import jp.co.blueship.tri.test.util.DefaultValueGenerator;

import org.junit.Test;

/**
 * {@link PjtSqlConditionBuilder}クラスのユニットテストクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class PjtSqlConditionBuilderTest extends TriMockTestSupport {

	private static final String PJT_ID = "pjtId";

	@Test
	public void testBuildByPrimaryKey() {

		PjtPrimaryKey primaryKey = new PjtPrimaryKey();
		primaryKey.setPjtId(PJT_ID);

		PjtSqlConditionBuilder testee = new PjtSqlConditionBuilder(primaryKey);
		ISqlCondition actual = testee.buildByPrimaryKey().getCondition();
		String queryString = actual.toQueryString();

		assertThat(queryString, is(notNullValue()));
		assertThat(queryString, is(not("")));
	}

	@Test
	public void testBuild() throws Exception {

		PjtSearchConditions conditions = DefaultValueGenerator.createBeanWithDefaultValue(PjtSearchConditions.class);
		conditions.setCloseTimestamp("2014/05/20 12:30:50");
		conditions.setDelTimestamp("2014/05/20 12:30:50");
		conditions.setTestCompTimestamp("2014/05/20 12:30:50");
		conditions.setDelStsId(1);

		PjtSqlConditionBuilder testee = new PjtSqlConditionBuilder(conditions);
		ISqlCondition actual = testee.build().getCondition();
		String queryString = actual.toQueryString();
		assertThat(queryString, is(notNullValue()));
		assertThat(queryString, is(not("")));
	}

}
