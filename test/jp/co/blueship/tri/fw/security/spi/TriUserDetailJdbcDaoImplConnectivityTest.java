package jp.co.blueship.tri.fw.security.spi;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.test.support.TriWithDataBaseTestCase;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Test-TriUserDetailJdbcDaoImpl.xml", "classpath:Test-DataSource-Context.xml" })
@TransactionConfiguration
@Transactional
public class TriUserDetailJdbcDaoImplConnectivityTest extends TriWithDataBaseTestCase {

	@Autowired
	@Qualifier("userDetailsService")
	private TriUserDetailJdbcDaoImpl testee;

	@Autowired
	private ApplicationContext context;

	@Before
	public void setUp() throws Exception {
		super.setUp();
		Contexts.getInstance().setApplicationContext(context);
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new InputSource("testresources\\data\\TriUserDetailJdbcDaoImplConnectivityTest.xml"));
	}

	@Test
	public void testLoadUsersByUsername() throws Exception {

		List<UserDetails> actualList = testee.loadUsersByUsername("TestUser");

		assertThat(TriCollectionUtils.isNotEmpty(actualList), is(true));
		assertThat(actualList.size(), is(1));

		TriAuthUser actual = (TriAuthUser) actualList.get(0);
		assertThat(actual.getUserID(), is("TestUser"));
		assertThat(actual.getPassword(), is("TestPass"));
		assertThat(actual.getUserNm(), is("トリニティ"));
		assertThat(actual.isEnabled(), is(true));
	}

	@Test
	public void testLoadUsersByUsernameCaseDisableUser() throws Exception {

		List<UserDetails> actualList = testee.loadUsersByUsername("Test-Disable-User");

		assertThat(TriCollectionUtils.isNotEmpty(actualList), is(true));
		assertThat(actualList.size(), is(1));

		TriAuthUser actual = (TriAuthUser) actualList.get(0);
		assertThat(actual.getUserID(), is("Test-Disable-User"));
		assertThat(actual.getPassword(), is("TestPass-Disable"));
		assertThat(actual.getUserNm(), is("無効ユーザ"));
		assertThat(actual.isEnabled(), is(false));
	}

	@Test
	public void testLoadUserByUsername() throws Exception {

		UserDetails actualUserDetails = testee.loadUserByUsername("TestUser");

		assertThat(actualUserDetails, is(notNullValue()));

		TriAuthUser actual = (TriAuthUser) actualUserDetails;
		assertThat(actual.getUserID(), is("TestUser"));
		assertThat(actual.getPassword(), is("TestPass"));
		assertThat(actual.getUserNm(), is("トリニティ"));
		assertThat(actual.isEnabled(), is(true));
		assertThat(TriCollectionUtils.isNotEmpty(actual.getAuthorities()), is(true));
		assertThat(actual.getAuthorities().size(), is(1));
	}
}
