package jp.co.blueship.tri.fw.security.spi;

import static org.easymock.EasyMock.expectLastCall;

import org.easymock.EasyMockSupport;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;

public class TrinityDaoAuthenticationProviderTest extends EasyMockSupport {

	private TrinityDaoAuthenticationProvider testee;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {
		testee = new TrinityDaoAuthenticationProvider();
	}

	/**
	 * {@link TrinityDaoAuthenticationProvider#additionalAuthenticationChecks(UserDetails, UsernamePasswordAuthenticationToken)}
	 * のテストケース。<br/>
	 * 正しいパスワードが入力された場合、エラーが発生しないことを確認する。
	 */
	@Test
	public void testAdditionalAuthenticationChecks() {

		UserDetails userDetails = createUserDetails("b8da4f8870e7024fb8c2bdf45e9f8f29");
		UsernamePasswordAuthenticationToken authentication = createAuthenticationToken("userID", "password");
		replayAll();
		try {
			testee.additionalAuthenticationChecks(userDetails, authentication);
		} catch (Exception e) {
			Assert.fail("期待しない例外が発生した。");
		}
	}

	/**
	 * {@link TrinityDaoAuthenticationProvider#additionalAuthenticationChecks(UserDetails, UsernamePasswordAuthenticationToken)}
	 * のテストケース。<br/>
	 * パスワードがnullの場合、エラーが発生することを確認する。
	 */
	@Test(expected=BadCredentialsException.class)
	public void testAdditionalAuthenticationChecksCasePasswordIsNull() {

		UserDetails userDetails = createUserDetails("b8da4f8870e7024fb8c2bdf45e9f8f29");
		UsernamePasswordAuthenticationToken authentication = createAuthenticationToken("userID", null);
		replayAll();
		testee.additionalAuthenticationChecks(userDetails, authentication);
	}

	/**
	 * {@link TrinityDaoAuthenticationProvider#additionalAuthenticationChecks(UserDetails, UsernamePasswordAuthenticationToken)}
	 * のテストケース。<br/>
	 * パスワードが異なる場合、エラーが発生することを確認する。
	 */
	@Test(expected=BadCredentialsException.class)
	public void testAdditionalAuthenticationChecksCaseInValidPassword() {

		UserDetails userDetails = createUserDetails("b8da4f8870e7024fb8c2bdf45e9f8f29");
		UsernamePasswordAuthenticationToken authentication = createAuthenticationToken("userID", "InValidPassword");
		replayAll();
		testee.additionalAuthenticationChecks(userDetails, authentication);
	}

	private UsernamePasswordAuthenticationToken createAuthenticationToken(String userID, String password) {

		return new UsernamePasswordAuthenticationToken(userID, password);
	}

	private UserDetails createUserDetails(String password) {

		UserDetails mockUserDetails = createMock(UserDetails.class);
		mockUserDetails.getPassword();
		expectLastCall().andReturn(password);

		return mockUserDetails;
	}

}
