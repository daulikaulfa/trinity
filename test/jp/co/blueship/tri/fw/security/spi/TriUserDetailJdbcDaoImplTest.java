package jp.co.blueship.tri.fw.security.spi;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

public class TriUserDetailJdbcDaoImplTest extends TriMockTestSupport {

	private TriUserDetailJdbcDaoImpl testee;

	private JdbcTemplate mockJdbcTemplate;

	//private Capture<String[]> capturedArgs;

	private static final String USER_ID = "userID";
	private static final String USER_NAME = "userName";
	private static final String PASSWORD = "password";
	private static final String QUERY = "query";

	@Before
	public void setUp() throws Exception {

		testee = new TriUserDetailJdbcDaoImpl();
		mockJdbcTemplate = createMock(JdbcTemplate.class);
		testee.setJdbcTemplate(mockJdbcTemplate);
		testee.setUsersByUsernameQuery(QUERY);
		//capturedArgs = new Capture<String[]>();
	}
//
//	@SuppressWarnings("unchecked")
//	@Test
//	public void testLoadUsersByUsername() {
//
//		mockJdbcTemplate.query(eq(QUERY), capture(capturedArgs), isA(RowMapper.class));
//		expectLastCall().andReturn(createTriAuthUsers());
//
//		replayAll();
//		List<UserDetails> actual = testee.loadUsersByUsername(USER_ID);
//		verifyAll();
//
//		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
//		assertThat(actual.size(), is(1));
//
//		UserDetails userDetails = actual.get(0);
//		assertThat(TriAuthUser.class.isInstance(userDetails), is(true));
//
//		TriAuthUser authUser = (TriAuthUser) userDetails;
//		assertThat(authUser.getUserID(), is(USER_ID));
//		assertThat(authUser.getPassword(), is(PASSWORD));
//		assertThat(authUser.getUserNm(), is(USER_NAME));
//		assertThat(authUser.isEnabled(), is(true));
//	}

	@SuppressWarnings("unused")
	private List<UserDetails> createTriAuthUsers() {

		List<UserDetails> result = new ArrayList<UserDetails>();
		TriAuthUser triAuthUser = new TriAuthUser(USER_ID, USER_NAME, PASSWORD, true);
		result.add(triAuthUser);

		return result;
	}

	@Test
	public void testCreateUserDetails() {

		UserDetails actual = testee.createUserDetails(USER_ID, createTriAuthUser(), Arrays.asList(createGrantedAuthority()));

		assertThat(TriAuthUser.class.isInstance(actual), is(true));

		TriAuthUser triAuthUser = (TriAuthUser) actual;

		assertThat(triAuthUser.getUserID(), is(USER_ID));
		assertThat(triAuthUser.getUserNm(), is(USER_NAME));
		assertThat(triAuthUser.getPassword(), is(PASSWORD));
		assertThat(triAuthUser.isEnabled(), is(true));
		assertThat(triAuthUser.isAccountNonExpired(), is(true));
		assertThat(triAuthUser.isAccountNonLocked(), is(true));
		assertThat(triAuthUser.isCredentialsNonExpired(), is(true));

		assertThat(TriCollectionUtils.isNotEmpty(triAuthUser.getAuthorities()), is(true));
		assertThat(triAuthUser.getAuthorities().size(), is(1));
	}

	@Test
	public void testCreateUserDetailsCaseUserDetailIsNotTriAuthUserInstance() {

		UserDetails actual = testee.createUserDetails(USER_ID, new User(USER_ID, PASSWORD, AuthorityUtils.NO_AUTHORITIES),
				Arrays.asList(createGrantedAuthority()));

		assertThat(TriAuthUser.class.isInstance(actual), is(false));
	}

	private TriAuthUser createTriAuthUser() {

		return new TriAuthUser(USER_ID, USER_NAME, PASSWORD, true, true, true, true, AuthorityUtils.NO_AUTHORITIES);
	}

	private GrantedAuthority createGrantedAuthority() {

		return createMock(GrantedAuthority.class);
	}
}
