package jp.co.blueship.tri.fw.security;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.fw.security.spi.TriAuthUser;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

public class TriAuthenticationContextTest {

	private static final String USER_ID = "userID";
	private static final String USER_NAME = "userName";
	private TriAuthenticationContext testee;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		testee = new TriAuthenticationContext();

	}

	/**
	 * 設定した認証済みユーザIDが取得できることを確認する。
	 */
	@Test
	public void testSetAuthUserIDOnAgent() {

		testee.setAuthUser(USER_ID, USER_NAME);
		String actual = testee.getAuthUserID();

		assertThat(actual, is(USER_ID));
	}

	/**
	 * 設定した認証済みユーザIDがクリアされることを確認する。<br/>
	 * クリアされているかの確認はIllegalStateExceptionが発生するか否かで確認する。
	 */
	@Test
	public void testClearAuthenticationContext() {

		testee.setAuthUser(USER_ID, USER_NAME);
		String actual = testee.getAuthUserID();
		assertThat(actual, is(USER_ID));

		testee.clearAuthenticationContext();

		try {
			testee.getAuthUserID();
			Assert.fail("期待した例外が発生しなかった");
		} catch (IllegalStateException e) {
		}

	}

	/**
	 * setAuthUserIDOnAgent()が呼び出されていない状態で、ユーザIDクリアを行った場合、<br/>
	 * 正常終了することを確認する。
	 */
	@Test
	public void testClearAuthenticationContextWithNotSetAuthUserID() {

		testee.clearAuthenticationContext();
	}

	/**
	 * 認証済みの場合、trueが返却されることを確認する。
	 */
	@Test
	public void testIsAuthenticated() {

		testee.setAuthUser(USER_ID, USER_NAME);
		boolean actual = testee.isAuthenticated();

		assertThat(actual, is(true));
	}

	/**
	 * 認証済みでない場合、falseが返却されることを確認する。
	 */
	@Test
	public void testIsAuthenticatedCaseFalse() {

		boolean actual = testee.isAuthenticated();

		assertThat(actual, is(false));
	}

	/**
	 * セキュリティ情報が子スレッドへ伝播されることを確認する。
	 *
	 * @throws Exception 予期せぬ例外が発生した場合
	 */
	@Test
	public void testInheritableThreadLocal() throws Exception {

		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
		testee.setAuthUser(USER_ID, USER_NAME);
		TestRunnable child = new TestRunnable();
		Thread t = new Thread(child);
		t.start();
		t.join();

		String actual = child.getAuthUser();
		assertThat(actual, is(USER_ID));

	}

	/**
	 * ログイン中ユーザIDが取得できることを確認する。
	 *
	 * @throws Exception 予期せぬ例外が発生した場合
	 */
	@Test
	public void testGetAuthUserID() {

		testee.setAuthUser(USER_ID, USER_NAME);
		String actual = testee.getAuthUserID();

		assertThat(actual, is(USER_ID));
	}

	/**
	 * ログイン中ユーザ名が取得できることを確認する。
	 *
	 * @throws Exception 予期せぬ例外が発生した場合
	 */
	@Test
	public void testGetAuthUserName() {

		testee.setAuthUser(USER_ID, USER_NAME);
		String actual = testee.getAuthUserName();

		assertThat(actual, is(USER_NAME));
	}

	/**
	 * 後処理
	 */
	@After
	public void tearDown() {
		SecurityContextHolder.clearContext();
	}

	private class TestRunnable implements Runnable {

		private SecurityContext context;

		@Override
		public void run() {
			context = SecurityContextHolder.getContext();
		}

		private String getAuthUser() {
			TriAuthUser principal = (TriAuthUser) context.getAuthentication().getPrincipal();
			return principal.getUserID();
		}
	}

}
