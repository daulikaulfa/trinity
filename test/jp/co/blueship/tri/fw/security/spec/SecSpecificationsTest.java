package jp.co.blueship.tri.fw.security.spec;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class SecSpecificationsTest {

	/** "hoge" */
	private static final String VALID_SHA_PASSWORD = "dbb50237ad3fa5b818b8eeca9ca25a047e0f29517db2b25f4a8db5f717ff90bf0b7e94ef4f5c4e313dfb06e48fbd9a2e40795906a75c470cdb619cf9c2d4f6d9";
	/** "usetID:trinityRealm:password" */
	private static final String VALID_A1FORMATPASSWORD = "b8da4f8870e7024fb8c2bdf45e9f8f29";

	@Test
	public void testSpecOfPasswordInSHA() {

		boolean actual = SecSpecifications.specOfMatchingPasswordInSHAWith(VALID_SHA_PASSWORD).isSpecified("hoge");
		assertThat(actual, is(true));
	}

	@Test
	public void testSpecOfPasswordInSHACaseInvalidPassword() {

		boolean actual = SecSpecifications.specOfMatchingPasswordInSHAWith(VALID_SHA_PASSWORD).isSpecified("fuga");
		assertThat(actual, is(false));

		actual = SecSpecifications.specOfMatchingPasswordInSHAWith(VALID_SHA_PASSWORD).isSpecified("");
		assertThat(actual, is(false));

		actual = SecSpecifications.specOfMatchingPasswordInSHAWith(VALID_SHA_PASSWORD).isSpecified(null);
		assertThat(actual, is(false));
	}

	@Test
	public void testSpecOfPasswordInMD5A1Format() {

		boolean actual = SecSpecifications.specOfMatchingPasswordInMD5A1FormatWith(VALID_A1FORMATPASSWORD, "userID").isSpecified("password");
		assertThat(actual, is(true));
	}

	@Test
	public void testSpecOfPasswordInMD5A1FormatCaseInvalidPassword() {

		boolean actual = SecSpecifications.specOfMatchingPasswordInMD5A1FormatWith(VALID_A1FORMATPASSWORD, "userID").isSpecified("fuga");
		assertThat(actual, is(false));

		actual = SecSpecifications.specOfMatchingPasswordInMD5A1FormatWith(VALID_A1FORMATPASSWORD, "userID").isSpecified("");
		assertThat(actual, is(false));

		actual = SecSpecifications.specOfMatchingPasswordInMD5A1FormatWith(VALID_A1FORMATPASSWORD, "userID").isSpecified(null);
		assertThat(actual, is(false));
	}
}
