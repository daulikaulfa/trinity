package jp.co.blueship.tri.fw.security;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class SecurityServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void isValidAccess() {

		try {
			SecurityService service = (SecurityService)this.getBean( "targetService" );

			assertTrue( service.isValidAccess(this.getAuthUserId(), ServiceId.UmCategoryCreationService.value()) );
			//assertTrue( service.isValidAccess(this.getAuthUserId(), ServiceId.AmChangePropertyListService.value()) );

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
