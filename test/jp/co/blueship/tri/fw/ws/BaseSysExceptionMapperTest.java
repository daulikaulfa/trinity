package jp.co.blueship.tri.fw.ws;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

public class BaseSysExceptionMapperTest extends TriMockTestSupport {

	private static final String MESSAGE = "Test Message";
	private BaseSysExceptionMapper testee;

	@Before
	public void setUp() throws Exception {
		testee = new BaseSysExceptionMapper();
	}

	@Test
	public void testToResponseCaseTriSystemException() {

		TriSystemException e = new TriSystemException(SmMessageId.SM001001E);
		expectCallsOnGetMessage(e);

		replayAll();
		Response actual = testee.toResponse(e);
		verifyAll();

		assertThat(actual.getStatus(), is(500));

		@SuppressWarnings("unchecked")
		List<String> messageList = (List<String>) actual.getEntity();
		assertThat(messageList.get(0), is(MESSAGE));
	}

	@Test
	public void testToResponseCaseContinuableBusinessException() {

		ContinuableBusinessException e = new ContinuableBusinessException(SmMessageId.SM001001E);

		messageManager().getMessage("jp", eq(SmMessageId.SM001001E));
		expectLastCall().andReturn(MESSAGE);

		replayAll();
		Response actual = testee.toResponse(e);
		verifyAll();

		assertThat(actual.getStatus(), is(400));

		@SuppressWarnings("unchecked")
		List<String> messageList = (List<String>) actual.getEntity();
		assertThat(messageList.get(0), is(MESSAGE));
	}

	@Test
	public void testToResponseCaseBusinessException() {

		BusinessException e = new BusinessException(SmMessageId.SM001001E);
		expectCallsOnGetMessage(e);

		replayAll();
		Response actual = testee.toResponse(e);
		verifyAll();

		assertThat(actual.getStatus(), is(400));

		@SuppressWarnings("unchecked")
		List<String> messageList = (List<String>) actual.getEntity();
		assertThat(messageList.get(0), is(MESSAGE));
	}

	private void expectCallsOnGetMessage(ITranslatable e) {

		messageManager().getMessage("jp", e.getMessageID(), e.getMessageArgs());
		expectLastCall().andReturn(MESSAGE);
	}
}
