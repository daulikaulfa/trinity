package jp.co.blueship.tri.fw.ws;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import javax.ws.rs.core.Response;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link TriWebService}クラスのユニットテストクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class TriWebServiceTest extends TriMockTestSupport {

	@SuppressWarnings("rawtypes")
	private TriWebService testee;

	@Before
	public void setUp() throws Exception {

		testee = createMockBuilder(TriWebService.class).//
				createMock();

	}

	@Test
	public void testContext() {

		IContextAdapter mockContextAdapter = createMock(IContextAdapter.class);
		testee.setContextAdapter(mockContextAdapter);

		assertThat(testee.context(), sameInstance(mockContextAdapter));
	}

	@Test
	public void testResponseAsSingleVo() {

		IValueObject mockResultVo = createMock(IValueObject.class);
		@SuppressWarnings("unchecked")
		Response actual = testee.responseAsSingleVo(mockResultVo);

		assertThat(actual.getStatus(), is(200));
		assertThat(IValueObject.class.isInstance(actual.getEntity()), is(true));
		IValueObject entity = (IValueObject) actual.getEntity();
		assertThat(entity, is(mockResultVo));
	}

	@Test
	public void testResponseAsList() {

		IValueObject mockResultVo1 = createMock(IValueObject.class);
		IValueObject mockResultVo2 = createMock(IValueObject.class);
		List<IValueObject> resultList = Arrays.asList(mockResultVo1, mockResultVo2);
		@SuppressWarnings("unchecked")
		Response actual = testee.responseAsList(resultList);

		assertThat(actual.getStatus(), is(200));
		assertThat(List.class.isInstance(actual.getEntity()), is(true));
		@SuppressWarnings("unchecked")
		List<IValueObject> entity = (List<IValueObject>) actual.getEntity();
		assertThat(entity.size(), is(2));
		assertThat(entity.get(0), is(mockResultVo1));
		assertThat(entity.get(1), is(mockResultVo2));
	}

	@Test
	public void testResponseAsInteger() {

		Response actual = testee.responseAsInteger(100);
		assertThat(actual.getStatus(), is(200));
		assertThat(Integer.class.isInstance(actual.getEntity()), is(true));
		Integer entity = (Integer) actual.getEntity();
		assertThat(entity, is(100));
	}

}
