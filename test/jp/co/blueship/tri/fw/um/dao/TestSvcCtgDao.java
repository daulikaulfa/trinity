package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgEntity;

import org.junit.Test;

public class TestSvcCtgDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		ISvcCtgEntity entity = new SvcCtgEntity();
		entity.setSvcCtgId("svcctg_T1");
		entity.setSvcCtgNm("SvcCtg_T1");
		entity.setSvcCtgContent("T1 contents");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		List<ISvcCtgEntity> list = new ArrayList<ISvcCtgEntity>();
		{
			ISvcCtgEntity entity = new SvcCtgEntity();
			entity.setSvcCtgId("svcctg_T2");
			entity.setSvcCtgNm("SvcCtg_T2");
			entity.setSvcCtgContent("T2 contents");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			ISvcCtgEntity entity = new SvcCtgEntity();
			entity.setSvcCtgId("svcctg_T3");
			entity.setSvcCtgNm("SvcCtg_T3");
			entity.setSvcCtgContent("T3 contents");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		SvcCtgCondition condition = new SvcCtgCondition();
		condition.setSvcCtgId( "svcctg_T1" );
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );
		ISvcCtgEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getSvcCtgNm() );
		System.out.println( entity.getSvcCtgContent() );

	}
	//@Ignore
	@Test
	public void find() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		SvcCtgCondition condition = new SvcCtgCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(SvcCtgItems.svcCtgId, TriSortOrder.Desc, 1);

		IEntityLimit<ISvcCtgEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setSvcCtgIds( new String[] {"svcctg_T1", "svcctg_T2"} );
		List<ISvcCtgEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		ISvcCtgEntity entity = new SvcCtgEntity();
		entity.setSvcCtgId("svcctg_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		List<ISvcCtgEntity> list = new ArrayList<ISvcCtgEntity>();

		{
			ISvcCtgEntity entity = new SvcCtgEntity();
			entity.setSvcCtgId("svcctg_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			ISvcCtgEntity entity = new SvcCtgEntity();
			entity.setSvcCtgId("svcctg_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		ISvcCtgDao dao = (ISvcCtgDao)this.getBean( "umSvcCtgDao" );

		SvcCtgCondition condition = new SvcCtgCondition();

		condition.setSvcCtgId("svcctg_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcCtgId("svcctg_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcCtgId("svcctg_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
