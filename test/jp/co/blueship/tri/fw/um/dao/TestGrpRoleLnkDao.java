package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpRoleLnkItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestGrpRoleLnkDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
		entity.setGrpId("grp_T1");
		entity.setRoleId("role_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		List<IGrpRoleLnkEntity> list = new ArrayList<IGrpRoleLnkEntity>();
		{
			IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
			entity.setGrpId("grp_T2");
			entity.setRoleId("role_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
			entity.setGrpId("grp_T3");
			entity.setRoleId("role_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		condition.setGrpId( "grp_T1" );
		condition.setRoleId( "role_T1" );
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );
		IGrpRoleLnkEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity );

	}
	//@Ignore
	@Test
	public void find() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(GrpRoleLnkItems.grpId, TriSortOrder.Desc, 1);

		IEntityLimit<IGrpRoleLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setGrpIds( new String[] {"grp_T1", "grp_T2"} );
		condition.setRoleIds( new String[] {"role_T1", "role_T2"} );
		List<IGrpRoleLnkEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
		entity.setGrpId("grp_T1");
		entity.setRoleId("role_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		List<IGrpRoleLnkEntity> list = new ArrayList<IGrpRoleLnkEntity>();

		{
			IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
			entity.setGrpId("grp_T2");
			entity.setRoleId("role_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
			entity.setGrpId("grp_T3");
			entity.setRoleId("role_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IGrpRoleLnkDao dao = (IGrpRoleLnkDao)this.getBean( "umGrpRoleLnkDao" );

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();

		condition.setGrpId("grp_T1");
		condition.setRoleId("role_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T2");
		condition.setRoleId("role_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T3");
		condition.setRoleId("role_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
