package jp.co.blueship.tri.fw.um.dao;


import static org.junit.Assert.*;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgLnkItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;

public class TestCategoryLnkDao extends TestTriJdbcDaoSupport{
	//@Ignore
	@Test
	public void insert() {
		ICtgLnkDao dao = (ICtgLnkDao) this.getBean( "umCategoryLnkDao" );

		ICtgLnkEntity entity = new CtgLnkEntity();
		entity.setCtgId("1");
		entity.setDataCtgCd("categoryLink_T1");
		entity.setDataId("2");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );
	}

	@Ignore
	@Test
	public void find() {
		ICtgLnkDao dao = (ICtgLnkDao) this.getBean( "umCategoryLnkDao" );

		CtgLnkCondition condition = new CtgLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(CtgLnkItems.ctgId, TriSortOrder.Desc, 1);

		condition.setCtgId("1");
		condition.setDataCtgCd("categoryLink_T1");
		condition.setDataId("2");
		List<ICtgLnkEntity> entity = dao.find( condition.getCondition(), sort );
		assertTrue( 1 == entity.size() );
	}
}
