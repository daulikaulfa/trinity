package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.*;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.um.dao.accshist.IAccsHistDao;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistCondition;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;

public class TestAccsHistDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		try {
			IAccsHistDao dao = (IAccsHistDao)this.getBean( "umAccsHistDao" );

			IAccsHistEntity entity = new AccsHistEntity();
			entity.setUserId("userId");
			entity.setDataCtgCd("dataCtg");
			entity.setDataId("dataId");
			entity.setAccsTimestamp(TriDateUtils.getSystemTimestamp());
			entity.setUserNm("userName");
			entity.setDelStsId(StatusFlg.off);

			int count = dao.insert( entity );

			assertTrue( 1 == count );
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Ignore
	@Test
	public void findByPrimaryKey() {
		AccsHistCondition condition = new AccsHistCondition();
		condition.setUserId( "userId" );
		condition.setDataCtgCd( "dataCtg" );
		condition.setDataId( "dataId" );
		IAccsHistDao dao = (IAccsHistDao)this.getBean( "umAccsHistDao" );
		IAccsHistEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

	}

	@Ignore
	@Test
	public void delete() {
		IAccsHistDao dao = (IAccsHistDao)this.getBean( "umAccsHistDao" );

		AccsHistCondition condition = new AccsHistCondition();

		condition.setUserId("userId");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
