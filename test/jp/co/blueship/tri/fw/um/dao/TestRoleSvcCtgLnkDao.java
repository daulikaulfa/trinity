package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleSvcCtgLnkItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;

import org.junit.Test;

public class TestRoleSvcCtgLnkDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
		entity.setRoleId("role_T1");
		entity.setSvcCtgId("svcctg_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		List<IRoleSvcCtgLnkEntity> list = new ArrayList<IRoleSvcCtgLnkEntity>();
		{
			IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
			entity.setRoleId("role_T2");
			entity.setSvcCtgId("svcctg_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
			entity.setRoleId("role_T3");
			entity.setSvcCtgId("svcctg_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		condition.setRoleId( "role_T1" );
		condition.setSvcCtgId( "svcctg_T1" );
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );
		IRoleSvcCtgLnkEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity );

	}
	//@Ignore
	@Test
	public void find() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(RoleSvcCtgLnkItems.roleId, TriSortOrder.Desc, 1);

		IEntityLimit<IRoleSvcCtgLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setRoleIds( new String[] {"role_T1", "role_T2"} );
		condition.setSvcCtgIds( new String[] {"svcctg_T1", "svcctg_T2"} );
		List<IRoleSvcCtgLnkEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
		entity.setRoleId("role_T1");
		entity.setSvcCtgId("svcctg_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		List<IRoleSvcCtgLnkEntity> list = new ArrayList<IRoleSvcCtgLnkEntity>();

		{
			IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
			entity.setRoleId("role_T2");
			entity.setSvcCtgId("svcctg_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IRoleSvcCtgLnkEntity entity = new RoleSvcCtgLnkEntity();
			entity.setRoleId("role_T3");
			entity.setSvcCtgId("svcctg_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IRoleSvcCtgLnkDao dao = (IRoleSvcCtgLnkDao)this.getBean( "umRoleSvcCtgLnkDao" );

		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();

		condition.setRoleId("role_T1");
		condition.setSvcCtgId("svcctg_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setRoleId("role_T2");
		condition.setSvcCtgId("svcctg_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setRoleId("role_T3");
		condition.setSvcCtgId("svcctg_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
