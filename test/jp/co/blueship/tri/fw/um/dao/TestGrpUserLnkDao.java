package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpUserLnkItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestGrpUserLnkDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		IGrpUserLnkEntity entity = new GrpUserLnkEntity();
		entity.setGrpId("grp_T1");
		entity.setUserId("blueship_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		List<IGrpUserLnkEntity> list = new ArrayList<IGrpUserLnkEntity>();
		{
			IGrpUserLnkEntity entity = new GrpUserLnkEntity();
			entity.setGrpId("grp_T2");
			entity.setUserId("blueship_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IGrpUserLnkEntity entity = new GrpUserLnkEntity();
			entity.setGrpId("grp_T3");
			entity.setUserId("blueship_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId( "grp_T1" );
		condition.setUserId( "blueship_T1" );
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );
		IGrpUserLnkEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity );

	}
	//@Ignore
	@Test
	public void find() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(GrpUserLnkItems.grpId, TriSortOrder.Desc, 1);

		IEntityLimit<IGrpUserLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setGrpIds( new String[] {"grp_T1", "grp_T2"} );
		condition.setUserIds( new String[] {"blueship_T1", "blueship_T2"} );
		List<IGrpUserLnkEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	@Ignore
	@Test
	public void update() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		IGrpUserLnkEntity entity = new GrpUserLnkEntity();
		entity.setGrpId("grp_T1");
		entity.setUserId("blueship_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		List<IGrpUserLnkEntity> list = new ArrayList<IGrpUserLnkEntity>();

		{
			IGrpUserLnkEntity entity = new GrpUserLnkEntity();
			entity.setGrpId("grp_T2");
			entity.setUserId("blueship_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IGrpUserLnkEntity entity = new GrpUserLnkEntity();
			entity.setGrpId("grp_T3");
			entity.setUserId("blueship_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IGrpUserLnkDao dao = (IGrpUserLnkDao)this.getBean( "umGrpUserLnkDao" );

		GrpUserLnkCondition condition = new GrpUserLnkCondition();

		condition.setGrpId("grp_T1");
		condition.setUserId("blueship_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T2");
		condition.setUserId("blueship_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T3");
		condition.setUserId("blueship_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
