package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgSvcLnkDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgSvcLnkItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgSvcLnkCondition;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgSvcLnkEntity;

import org.junit.Test;

public class TestSvcCtgSvcLnkDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
		entity.setSvcId("svc_T1");
		entity.setSvcCtgId("svcctg_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		List<ISvcCtgSvcLnkEntity> list = new ArrayList<ISvcCtgSvcLnkEntity>();
		{
			ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
			entity.setSvcId("svc_T2");
			entity.setSvcCtgId("svcctg_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
			entity.setSvcId("svc_T3");
			entity.setSvcCtgId("svcctg_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		SvcCtgSvcLnkCondition condition = new SvcCtgSvcLnkCondition();
		condition.setSvcId( "svc_T1" );
		condition.setSvcCtgId( "svcctg_T1" );
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );
		ISvcCtgSvcLnkEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity );

	}
	//@Ignore
	@Test
	public void find() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		SvcCtgSvcLnkCondition condition = new SvcCtgSvcLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(SvcCtgSvcLnkItems.svcId, TriSortOrder.Desc, 1);

		IEntityLimit<ISvcCtgSvcLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setSvcIds( new String[] {"svc_T1", "svc_T2"} );
		condition.setSvcCtgIds( new String[] {"svcctg_T1", "svcctg_T2"} );
		List<ISvcCtgSvcLnkEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
		entity.setSvcId("svc_T1");
		entity.setSvcCtgId("svcctg_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		List<ISvcCtgSvcLnkEntity> list = new ArrayList<ISvcCtgSvcLnkEntity>();

		{
			ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
			entity.setSvcId("svc_T2");
			entity.setSvcCtgId("svcctg_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			ISvcCtgSvcLnkEntity entity = new SvcCtgSvcLnkEntity();
			entity.setSvcId("svc_T3");
			entity.setSvcCtgId("svcctg_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		ISvcCtgSvcLnkDao dao = (ISvcCtgSvcLnkDao)this.getBean( "umSvcCtgSvcLnkDao" );

		SvcCtgSvcLnkCondition condition = new SvcCtgSvcLnkCondition();

		condition.setSvcId("svc_T1");
		condition.setSvcCtgId("svcctg_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T2");
		condition.setSvcCtgId("svcctg_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T3");
		condition.setSvcCtgId("svcctg_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
