package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqRecDocument;
import jp.co.blueship.tri.fw.schema.beans.areq.AreqType;
import jp.co.blueship.tri.fw.schema.beans.bp.BpRecDocument;
import jp.co.blueship.tri.fw.schema.beans.bp.BpType;
import jp.co.blueship.tri.fw.schema.beans.lot.LotRecDocument;
import jp.co.blueship.tri.fw.schema.beans.lot.LotType;
import jp.co.blueship.tri.fw.schema.beans.ra.RaRecDocument;
import jp.co.blueship.tri.fw.schema.beans.ra.RaType;
import jp.co.blueship.tri.fw.schema.beans.rep.RepRecDocument;
import jp.co.blueship.tri.fw.schema.beans.rep.RepType;
import jp.co.blueship.tri.fw.schema.beans.rp.RpRecDocument;
import jp.co.blueship.tri.fw.schema.beans.rp.RpType;
import jp.co.blueship.tri.fw.um.dao.hist.IHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistCondition;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.utils.HistMappingUtils;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.RpEntity;

public class TestHistDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		// Areq Xml Test
		IAreqDao areqDao = (IAreqDao)this.getBean( "amAreqDao" );
		AreqCondition areqCondition = new AreqCondition();
		areqCondition.setAreqId("LND-1409120001");
		IAreqDto areqDto = new AreqDto();
		IAreqEntity areqEntity = areqDao.findByPrimaryKey(areqCondition.getCondition());
		areqDto.setAreqEntity(areqEntity);
		// AreqFile
		List<IAreqFileEntity> areqFileEntites = new ArrayList<IAreqFileEntity>();
		IAreqFileEntity areqFileEntity = new AreqFileEntity();
		areqFileEntity.setAreqId("Areq001");
		areqFileEntity.setAreqFileSeqNo("0001");
		areqFileEntity.setAreqCtgCd("40");
		areqFileEntity.setFilePath("c:/aaa");
		areqFileEntity.setFileRev("123");
		areqFileEntity.setFileUpdTimestamp(TriDateUtils.getSystemTimestamp());
		areqFileEntites.add(areqFileEntity);
		areqFileEntity = new AreqFileEntity();
		areqFileEntity.setAreqId("Areq001");
		areqFileEntity.setAreqFileSeqNo("0002");
		areqFileEntity.setAreqCtgCd("40");
		areqFileEntity.setFilePath("c:/bbb");
		areqFileEntity.setFileRev("124");
		areqFileEntity.setFileUpdTimestamp(TriDateUtils.getSystemTimestamp());
		areqFileEntites.add(areqFileEntity);
		areqDto.setAreqFileEntities(areqFileEntites);
		// AreqBinaryFile
		List<IAreqBinaryFileEntity> areqBinaryFileEntites = new ArrayList<IAreqBinaryFileEntity>();
		AreqBinaryFileEntity areqBinaryFileEntity = new AreqBinaryFileEntity();
		areqBinaryFileEntity.setAreqId("Areq001");
		areqBinaryFileEntity.setAreqFileSeqNo("0003");
		areqBinaryFileEntity.setAreqCtgCd("40");
		areqBinaryFileEntity.setFilePath("c:/cccc");
		areqBinaryFileEntites.add(areqBinaryFileEntity);
		areqBinaryFileEntity = new AreqBinaryFileEntity();
		areqBinaryFileEntity.setAreqId("Areq001");
		areqBinaryFileEntity.setAreqFileSeqNo("0004");
		areqBinaryFileEntity.setAreqCtgCd("40");
		areqBinaryFileEntity.setFilePath("c:/ddd");
		areqBinaryFileEntites.add(areqBinaryFileEntity);
		areqDto.setAreqBinaryFileEntities(areqBinaryFileEntites);
		// AttachedFile
		List<IAreqAttachedFileEntity> areqAttachedFileEntites = new ArrayList<IAreqAttachedFileEntity>();
		IAreqAttachedFileEntity areqAttachedFileEntity = new AreqAttachedFileEntity();
		areqAttachedFileEntity.setAreqId("Areq001");
		areqAttachedFileEntity.setAttachedFileSeqNo("0005");
		areqAttachedFileEntity.setFilePath("c:/eee");
		areqAttachedFileEntites.add(areqAttachedFileEntity);
		areqAttachedFileEntity = new AreqAttachedFileEntity();
		areqAttachedFileEntity.setAreqId("Areq001");
		areqAttachedFileEntity.setAttachedFileSeqNo("0006");
		areqAttachedFileEntity.setFilePath("c:/fff");
		areqAttachedFileEntites.add(areqAttachedFileEntity);
		areqDto.setAreqAttachedFileEntities(areqAttachedFileEntites);
		// make Areq xml
		AreqRecDocument doc = AreqRecDocument.Factory.newInstance();
		AreqType xml = doc.addNewAreqRec();
		xml = new HistMappingUtils().mapDB2XMLBeans(areqDto, xml);
		System.out.println(TriXmlUtils.replaceExcessString(doc));

		// Lot Xml Test
		ILotDao lotDao = (ILotDao)this.getBean( "amLotDao" );
		ILotDto lotDto = new LotDto();
		LotCondition lotCondition = new LotCondition();
		lotCondition.setLotId("LOT-1407290050");
		lotDto.setLotEntity(lotDao.findByPrimaryKey(lotCondition.getCondition()));
		// LotMdl
		List<ILotMdlLnkEntity> lotMdlEntities = new ArrayList<ILotMdlLnkEntity>();
		ILotMdlLnkEntity lotMdlEntity = new LotMdlLnkEntity();
		lotMdlEntity.setLotId("Lot0001");
		lotMdlEntity.setMdlNm("MdlName001");
		lotMdlEntity.setMdlVerTag("MdlVerTag");
		lotMdlEntity.setMergeHeadVerTag("MergeTargetVerTag");
		lotMdlEntity.setIsLotInclude(StatusFlg.on);
		lotMdlEntities.add(lotMdlEntity);
		lotMdlEntity = new LotMdlLnkEntity();
		lotMdlEntity.setLotId("Lot0001");
		lotMdlEntity.setMdlNm("MdlName002");
		lotMdlEntity.setMdlVerTag("MdlVerTag02");
		lotMdlEntity.setMergeHeadVerTag("MergeTargetVerTag02");
		lotMdlEntity.setIsLotInclude(StatusFlg.off);
		lotMdlEntities.add(lotMdlEntity);
		lotDto.setLotMdlLnkEntities(lotMdlEntities);
		// RelEnv
		List<ILotRelEnvLnkEntity> lotEnvEntities = new ArrayList<ILotRelEnvLnkEntity>();
		ILotRelEnvLnkEntity lotEnvEntity = new LotRelEnvLnkEntity();
		lotEnvEntity.setLotId("Lot0001");
		lotEnvEntity.setBldEnvId("BldEnv001");
		lotEnvEntities.add(lotEnvEntity);
		lotEnvEntity = new LotRelEnvLnkEntity();
		lotEnvEntity.setLotId("Lot0001");
		lotEnvEntity.setBldEnvId("BldEnv002");
		lotEnvEntities.add(lotEnvEntity);
		lotDto.setLotRelEnvLnkEntities(lotEnvEntities);

		// make Lot xml
		LotRecDocument lotDoc = LotRecDocument.Factory.newInstance();
		LotType lotXml = lotDoc.addNewLotRec();
		lotXml = new HistMappingUtils().mapDB2XMLBeans(lotDto, lotXml);
		System.out.println(TriXmlUtils.replaceExcessString(lotDoc));

		// Bp Xml Test
		IBpDao bpDao = (IBpDao)this.getBean( "bmBpDao" );
		BpCondition bpCondition = new BpCondition();
		bpCondition.setBpId("BLD-1408070001");
		IBpDto bpDto = new BpDto();
		bpDto.setBpEntity(bpDao.findByPrimaryKey(bpCondition.getCondition()));
		//BpAreq
		List<IBpAreqLnkEntity> bpAreqEntities = new ArrayList<IBpAreqLnkEntity>();
		IBpAreqLnkEntity bpAreqEntity = new BpAreqLnkEntity();
		bpAreqEntity.setBpId("Bp0001");
		bpAreqEntity.setAreqId("Areq0001");
		bpAreqEntities.add(bpAreqEntity);
		bpAreqEntity = new BpAreqLnkEntity();
		bpAreqEntity.setBpId("Bp0001");
		bpAreqEntity.setAreqId("Areq0002");
		bpAreqEntities.add(bpAreqEntity);
		bpDto.setBpAreqLnkEntities(bpAreqEntities);
		//BpMdl
		List<IBpMdlLnkEntity> bpMdlEntities = new ArrayList<IBpMdlLnkEntity>();
		IBpMdlLnkEntity bpMdlEntity = new BpMdlLnkEntity();
		bpMdlEntity.setBpId("Bp0001");
		bpMdlEntity.setMdlNm("Mdl0001");
		bpMdlEntity.setBpCloseRev("123");
		bpMdlEntities.add(bpMdlEntity);
		bpMdlEntity = new BpMdlLnkEntity();
		bpMdlEntity.setBpId("Bp0001");
		bpMdlEntity.setMdlNm("Mdl0002");
		bpMdlEntity.setBpCloseRev("124");
		bpMdlEntities.add(bpMdlEntity);
		bpDto.setBpMdlLnkEntities(bpMdlEntities);
		// make Bp xml
		BpRecDocument bpDoc = BpRecDocument.Factory.newInstance();
		BpType bpXml = bpDoc.addNewBpRec();
		bpXml = new HistMappingUtils().mapDB2XMLBeans(bpDto, bpXml);
		System.out.println(TriXmlUtils.replaceExcessString(bpDoc));


		// Ra Xml Test
		IRaDto raDto = new RaDto();
		IRaEntity raEntity = new RaEntity();
		raEntity.setRaId("Ra0001");
		raEntity.setLotId("Lot0001");
		raEntity.setBldEnvId("BldEnv001");
		raEntity.setReqGrpNm("grp001");
		raEntity.setRegUserId("user001");
		raEntity.setRegTimestamp(TriDateUtils.getSystemTimestamp());
		raEntity.setReqResponsibility("Respon");
		raEntity.setPreferredRelDate("2014/06/16");
		raEntity.setRelationId("Rel001");
		raDto.setRaEntity(raEntity);
		//RaBp
		List<IRaBpLnkEntity> raBpEntities = new ArrayList<IRaBpLnkEntity>();
		RaBpLnkEntity raBpEntity = new RaBpLnkEntity();
		raBpEntity.setBpId("Bp0001");
		raBpEntity.setBpNm("BpName001");
		raBpEntity.setSummary("Summary 001");
		raBpEntity.setContent("001 content");
		raBpEntities.add(raBpEntity);
		raBpEntity = new RaBpLnkEntity();
		raBpEntity.setBpId("Bp0002");
		raBpEntity.setBpNm("BpName002");
		raBpEntity.setSummary("Summary 002");
		raBpEntity.setContent("002 content");
		raBpEntities.add(raBpEntity);
		raDto.setRaBpLnkEntities(raBpEntities);
		// RaRp
		List<IRaRpLnkEntity> raRpEntities = new ArrayList<IRaRpLnkEntity>();
		RaRpLnkEntity raRpEntity = new RaRpLnkEntity();
		raRpEntity.setRpId("Rp0001");
		raRpEntity.setRelTimestamp(TriDateUtils.getSystemTimestamp());
		raRpEntities.add(raRpEntity);
		raRpEntity = new RaRpLnkEntity();
		raRpEntity.setRpId("Rp0002");
		raRpEntity.setRelTimestamp(TriDateUtils.getSystemTimestamp());
		raRpEntities.add(raRpEntity);
		raDto.setRaRpLnkEntities(raRpEntities);
		//RaAttachedFile
		List<IRaAttachedFileEntity> raAttachedFileEntities = new ArrayList<IRaAttachedFileEntity>();
		IRaAttachedFileEntity raAttachedFileEntity = new RaAttachedFileEntity();
		raAttachedFileEntity.setRaId("Ra0001");
		raAttachedFileEntity.setAttachedFileSeqNo("0001");
		raAttachedFileEntity.setFilePath("c:/aaaaa");
		raAttachedFileEntities.add(raAttachedFileEntity);
		raAttachedFileEntity = new RaAttachedFileEntity();
		raAttachedFileEntity.setRaId("Ra0001");
		raAttachedFileEntity.setAttachedFileSeqNo("0002");
		raAttachedFileEntity.setFilePath("c:/abcdefg");
		raAttachedFileEntities.add(raAttachedFileEntity);
		raDto.setRaAttachedFileEntities(raAttachedFileEntities);
		// make Ra xml
		RaRecDocument raDoc = RaRecDocument.Factory.newInstance();
		RaType raXml = raDoc.addNewRaRec();
		raXml = new HistMappingUtils().mapDB2XMLBeans(raDto, raXml);
		System.out.println(TriXmlUtils.replaceExcessString(raDoc));


		// Rp Xml Test
		IRpDto rpDto = new RpDto();
		IRpEntity rpEntity = new RpEntity();
		rpEntity.setRpId("Rp0001");
		rpEntity.setRpNm("RpName0001");
		rpEntity.setLotId("Lot0001");
		rpEntity.setSummary("Summary");
		rpEntity.setContent("Content");
		rpEntity.setBldEnvId("BldEnvId001");
		rpEntity.setExecUserId("User001");
		rpEntity.setExecUserNm("UserName001");
		rpEntity.setProcStTimestamp(TriDateUtils.getSystemTimestamp());
		rpEntity.setProcEndTimestamp(TriDateUtils.getSystemTimestamp());
		rpEntity.setRelTimestamp(TriDateUtils.getSystemTimestamp());
		rpEntity.setStsId("StsId001");
		rpEntity.setDelCmt("Delete Comment");
		rpEntity.setProcId("9000");
		rpDto.setRpEntity(rpEntity);
		//RpBpLnk
		List<IRpBpLnkEntity> rpBpEntities = new ArrayList<IRpBpLnkEntity>();
		IRpBpLnkEntity rpBpEntity = new RpBpLnkEntity();
		rpBpEntity.setRpId("Rp0001");
		rpBpEntity.setBpId("Bp0001");
		rpBpEntity.setMergeOdr("ASC");
		rpBpEntities.add(rpBpEntity);
		rpBpEntity = new RpBpLnkEntity();
		rpBpEntity.setRpId("Rp0001");
		rpBpEntity.setBpId("Bp0002");
		rpBpEntity.setMergeOdr("DESC");
		rpBpEntities.add(rpBpEntity);
		rpDto.setRpBpLnkEntities(rpBpEntities);
		//RpDo
		List<IDmDoEntity> dmDoEntities = new ArrayList<IDmDoEntity>();
		IDmDoEntity dmDoEntity = new DmDoEntity();
		dmDoEntity.setMgtVer("MgVer001");
		dmDoEntity.setRpId("Rp0001");
		dmDoEntity.setSummary("DoSummary");
		dmDoEntity.setContent("DoContent");
		dmDoEntity.setTimerSettingSummary("DoTimerSummary");
		dmDoEntity.setTimerSettingContent("DoTimerContent");
		dmDoEntity.setTimerSettingDate("2016/06/16");
		dmDoEntity.setTimerSettingTime("16:50");
		dmDoEntity.setStsId("DoSts001");
		dmDoEntities.add(dmDoEntity);
		//RpRa
		List<String> raIds = new ArrayList<String>();
		raIds.add( "Ra001" );
		// make Rp xml
		RpRecDocument rpDoc = RpRecDocument.Factory.newInstance();
		RpType rpXml = rpDoc.addNewRpRec();
		rpXml = new HistMappingUtils().mapDB2XMLBeans(rpDto, dmDoEntities, raIds, rpXml);
		System.out.println(TriXmlUtils.replaceExcessString(rpDoc));


		// Rep Xml Test
		IRepEntity repEntity = new RepEntity();
		repEntity.setRepId("Rep0001");
		repEntity.setLotId("Lot0001");
		repEntity.setRepCtgCd("RM-RP-001");
		repEntity.setExecUserId("User0001");
		repEntity.setExecUserNm("UserName0001");
		repEntity.setProcEndTimestamp(TriDateUtils.getSystemTimestamp());
		repEntity.setProcStTimestamp(TriDateUtils.getSystemTimestamp());
		repEntity.setLatestDlUserId("User0001");
		repEntity.setLatestDlUserNm("UserName0001");
		repEntity.setLatestDlTimestamp(TriDateUtils.getSystemTimestamp());
		repEntity.setStsId("StsId001");
		repEntity.setDelCmt("DeleteComment");
		// make Rep xml
		RepRecDocument repDoc = RepRecDocument.Factory.newInstance();
		RepType repXml = repDoc.addNewRepRec();
		repXml = new HistMappingUtils().mapDB2XMLBeans(repEntity,repXml);
		System.out.println(TriXmlUtils.replaceExcessString(repDoc));


		IHistEntity entity = new HistEntity();
		entity.setHistId("hist0001");
		entity.setDataCtgCd("AM_AREQ");
		entity.setDataId("Areq001");
		entity.setHistData(TriXmlUtils.replaceExcessString(doc));
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		List<IHistEntity> list = new ArrayList<IHistEntity>();
		{
			IHistEntity entity = new HistEntity();
			entity.setHistId("hist_T2");
			entity.setDataCtgCd("DataCtgCd_T2");
			entity.setDataId("data_id_T2");
			entity.setHistData("<test_T2 />");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IHistEntity entity = new HistEntity();
			entity.setHistId("hist_T3");
			entity.setDataCtgCd("DataCtgCd_T3");
			entity.setDataId("data_id_T3");
			entity.setHistData("<test_T3 />");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		HistCondition condition = new HistCondition();
		condition.setHistId( "hist_T1" );
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );
		IHistEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println(entity.getDataCtgCd());
		System.out.println(entity.getDataId());
		System.out.println(entity.getHistData());

	}
	//@Ignore
	@Test
	public void find() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		HistCondition condition = new HistCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(HistItems.histId, TriSortOrder.Desc, 1);

		IEntityLimit<IHistEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setHistIds( new String[] {"hist_T2", "hist_T3"} );
		List<IHistEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		IHistEntity entity = new HistEntity();
		entity.setHistId("hist_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		List<IHistEntity> list = new ArrayList<IHistEntity>();

		{
			IHistEntity entity = new HistEntity();
			entity.setHistId("hist_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IHistEntity entity = new HistEntity();
			entity.setHistId("hist_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IHistDao dao = (IHistDao)this.getBean( "umHistDao" );

		HistCondition condition = new HistCondition();

		condition.setHistId("hist_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setHistId("hist_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setHistId("hist_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
