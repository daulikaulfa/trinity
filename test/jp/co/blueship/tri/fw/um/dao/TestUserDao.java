package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;

public class TestUserDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		IUserEntity entity = new UserEntity();
		entity.setUserId("blueship_T1");
		entity.setUserPass("pass");
		entity.setUserNm("User_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		List<IUserEntity> list = new ArrayList<IUserEntity>();
		{
			IUserEntity entity = new UserEntity();
			entity.setUserId("blueship_T2");
			entity.setUserPass("pass");
			entity.setUserNm("User_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IUserEntity entity = new UserEntity();
			entity.setUserId("blueship_T3");
			entity.setUserPass("pass");
			entity.setUserNm("User_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	@Ignore
	@Test
	public void findByPrimaryKey() {
		UserCondition condition = new UserCondition();
		condition.setUserId( "blueship1" );
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );
		IUserEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

	}
	//@Ignore
	@Test
	public void find() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		UserCondition condition = new UserCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(UserItems.userId, TriSortOrder.Desc, 1);

		IEntityLimit<IUserEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		System.out.println("");
		for ( IUserEntity entity: home.getEntities() ) {
			System.out.print( "  UserId := " + entity.getUserId() );
			System.out.print( ", UserNm := " + entity.getUserNm() );
			System.out.print( ", LastLoginTimestamp := " + entity.getLastLoginTimestamp() );
			System.out.println( "" );
		}

		condition.setUserIds( new String[] {"blueship1", "blueship2"} );
		List<IUserEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	@Ignore
	@Test
	public void update() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		IUserEntity entity = new UserEntity();
		entity.setUserId("blueship_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		List<IUserEntity> list = new ArrayList<IUserEntity>();

		{
			IUserEntity entity = new UserEntity();
			entity.setUserId("blueship_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IUserEntity entity = new UserEntity();
			entity.setUserId("blueship_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IUserDao dao = (IUserDao)this.getBean( "umUserDao" );

		UserCondition condition = new UserCondition();

		condition.setUserId("blueship_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
