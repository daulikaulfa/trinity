package jp.co.blueship.tri.fw.um.dao;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.um.dao.wiki.IViewWikiTagsDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;

public class TestViewWikiTagsDao extends TestTriJdbcDaoSupport {

	@Test
	public void find(){
		try{
			IViewWikiTagsDao dao = (IViewWikiTagsDao)this.getBean( "umViewWikiTagsDao" );

			ViewWikiTagsCondition condition = new ViewWikiTagsCondition();
			condition.setLotId("LOT-1605180007");

			List<IViewWikiTagsEntity> entityList = dao.find(condition);

			for(IViewWikiTagsEntity entity : entityList){
				System.out.print(" TagNm : " + entity.getTagNm() );
				System.out.print(" LotId : " + entity.getLotId() );
				System.out.print(" Count : " + entity.count() );
				System.out.println("");
			}

			System.out.println("finish!!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
