package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneLnkDao;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneLnkItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkEntity;

public class TestMilestoneLnkDao extends TestTriJdbcDaoSupport{
	@Ignore
	@Test
	public void insert() {
		IMstoneLnkDao dao = (IMstoneLnkDao) this.getBean( "umMilestoneLnkDao" );
		
		IMstoneLnkEntity entity = new MstoneLnkEntity();
		entity.setMstoneId("1");
		entity.setDataCtgCd("milestoneLink_T1");
		entity.setDataId("2");
		entity.setDelStsId(StatusFlg.off);
		
		int count = dao.insert( entity );
		
		assertTrue( 1 == count );
	}

	//@Ignore
	@Test
	public void find() {
		IMstoneLnkDao dao = (IMstoneLnkDao) this.getBean( "umMilestoneLnkDao" );
		
		MstoneLnkCondition condition = new MstoneLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneLnkItems.mstoneId, TriSortOrder.Desc, 1);
		
		condition.setMstoneId("1");
		condition.setDataCtgCd("milestoneLink_T1");
		condition.setDataId("2");
		List<IMstoneLnkEntity> entity = dao.find( condition.getCondition(), sort );
		assertTrue( 1 == entity.size() );
	}

}
