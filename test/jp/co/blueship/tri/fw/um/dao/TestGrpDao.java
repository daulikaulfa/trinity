package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestGrpDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		IGrpEntity entity = new GrpEntity();
		entity.setGrpId("grp_T1");
		entity.setGrpNm("Grp_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		List<IGrpEntity> list = new ArrayList<IGrpEntity>();
		{
			IGrpEntity entity = new GrpEntity();
			entity.setGrpId("grp_T2");
			entity.setGrpNm("Grp_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IGrpEntity entity = new GrpEntity();
			entity.setGrpId("grp_T3");
			entity.setGrpNm("Grp_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		GrpCondition condition = new GrpCondition();
		condition.setGrpId( "grp_T1" );
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );
		IGrpEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getGrpNm() );

	}
	//@Ignore
	@Test
	public void find() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		GrpCondition condition = new GrpCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(GrpItems.grpId, TriSortOrder.Desc, 1);

		IEntityLimit<IGrpEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setGrpIds( new String[] {"grp_T1", "grp_T2"} );
		List<IGrpEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	@Ignore
	@Test
	public void update() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		IGrpEntity entity = new GrpEntity();
		entity.setGrpId("grp_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		List<IGrpEntity> list = new ArrayList<IGrpEntity>();

		{
			IGrpEntity entity = new GrpEntity();
			entity.setGrpId("grp_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IGrpEntity entity = new GrpEntity();
			entity.setGrpId("grp_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IGrpDao dao = (IGrpDao)this.getBean( "umGrpDao" );

		GrpCondition condition = new GrpCondition();

		condition.setGrpId("grp_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setGrpId("grp_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
