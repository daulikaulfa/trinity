package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.dept.constants.DeptItems;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestDeptDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		IDeptEntity entity = new DeptEntity();
		entity.setDeptId("dept_T1");
		entity.setDeptNm("Dept_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		List<IDeptEntity> list = new ArrayList<IDeptEntity>();
		{
			IDeptEntity entity = new DeptEntity();
			entity.setDeptId("dept_T2");
			entity.setDeptNm("Dept_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IDeptEntity entity = new DeptEntity();
			entity.setDeptId("dept_T3");
			entity.setDeptNm("Dept_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		DeptCondition condition = new DeptCondition();
		condition.setDeptId( "develop" );
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );
		IDeptEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

	}
	//@Ignore
	@Test
	public void find() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		DeptCondition condition = new DeptCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(DeptItems.deptId, TriSortOrder.Desc, 1);

		IEntityLimit<IDeptEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setDeptIds( new String[] {"dept_T1", "dept_T2"} );
		List<IDeptEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	@Ignore
	@Test
	public void update() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		IDeptEntity entity = new DeptEntity();
		entity.setDeptId("dept_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		List<IDeptEntity> list = new ArrayList<IDeptEntity>();

		{
			IDeptEntity entity = new DeptEntity();
			entity.setDeptId("dept_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IDeptEntity entity = new DeptEntity();
			entity.setDeptId("dept_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IDeptDao dao = (IDeptDao)this.getBean( "umDeptDao" );

		DeptCondition condition = new DeptCondition();

		condition.setDeptId("dept_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setDeptId("dept_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setDeptId("dept_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
