package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.user.IUserPassHistDao;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserPassHistItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistEntity;

import org.junit.Test;

public class TestUserPassHistDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		IUserPassHistEntity entity = new UserPassHistEntity();
		entity.setUserId("blueship_T1");
		entity.setPassHistSeqNo("000000");
		entity.setUserPass("pass00");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		List<IUserPassHistEntity> list = new ArrayList<IUserPassHistEntity>();
		{
			IUserPassHistEntity entity = new UserPassHistEntity();
			entity.setUserId("blueship_T2");
			entity.setPassHistSeqNo("000001");
			entity.setUserPass("pass00");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IUserPassHistEntity entity = new UserPassHistEntity();
			entity.setUserId("blueship_T3");
			entity.setPassHistSeqNo("000002");
			entity.setUserPass("pass00");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		UserPassHistCondition condition = new UserPassHistCondition();
		condition.setUserId( "blueship_T1" );
		condition.setPassHistSeqNo( "000000" );
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );
		IUserPassHistEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getUserPass() );

	}
	//@Ignore
	@Test
	public void find() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		UserPassHistCondition condition = new UserPassHistCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(UserPassHistItems.passHistSeqNo, TriSortOrder.Desc, 1);

		IEntityLimit<IUserPassHistEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setUserIds( new String[] {"blueship_T1", "blueship_T2"} );
		List<IUserPassHistEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( IUserPassHistEntity entity: entities ) {
			System.out.println( entity.getPassHistSeqNo() + ":" + entity.getUserPass() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		IUserPassHistEntity entity = new UserPassHistEntity();
		entity.setUserId("blueship_T1");
		entity.setPassHistSeqNo("000000");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		List<IUserPassHistEntity> list = new ArrayList<IUserPassHistEntity>();

		{
			IUserPassHistEntity entity = new UserPassHistEntity();
			entity.setUserId("blueship_T2");
			entity.setPassHistSeqNo("000001");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IUserPassHistEntity entity = new UserPassHistEntity();
			entity.setUserId("blueship_T3");
			entity.setPassHistSeqNo("000002");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IUserPassHistDao dao = (IUserPassHistDao)this.getBean( "umUserPassHistDao" );

		UserPassHistCondition condition = new UserPassHistCondition();

		condition.setUserId("blueship_T1");
		condition.setPassHistSeqNo("000000");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T2");
		condition.setPassHistSeqNo("000001");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T3");
		condition.setPassHistSeqNo("000002");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
