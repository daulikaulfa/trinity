package jp.co.blueship.tri.fw.um.dao;

import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiDao;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;

public class TestWikiDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void inser(){

		System.out.println("start");

		IWikiDao dao = (IWikiDao)this.getBean( "umWikiDao" );

		IWikiEntity entity = new WikiEntity();
		entity.setWikiId("WIKI-00000");
		entity.setContent("CONTENTTTTTTTTTTT");
		entity.setLotId("LOT-1605180007");
		entity.setPageNm("DaoTEST");


		dao.insert(entity);

		System.out.println("finsh!!!");
	}

	@Test
	public void updata(){
		System.out.println("start");

		IWikiDao dao = (IWikiDao)this.getBean( "umWikiDao" );

		IWikiEntity entity = new WikiEntity();
		entity.setWikiId("WIKI-00000");
		entity.setContent("CHANGE");
		dao.update(entity);

		System.out.println("finsh!!!");
	}
	@Ignore
	@Test
	public void find(){

		System.out.println("start");

		IWikiDao dao = (IWikiDao)this.getBean( "umWikiDao" );

		WikiCondition condition = new WikiCondition();

		List<IWikiEntity> entityList =  dao.find(condition.getCondition());

		System.out.println(entityList.size() + "");

		System.out.println("finsh!!!");
	}


}
