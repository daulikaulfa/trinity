package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.locksvc.ILockSvcDao;
import jp.co.blueship.tri.fw.um.dao.locksvc.constants.LockSvcItems;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.ILockSvcEntity;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.LockSvcCondition;
import jp.co.blueship.tri.fw.um.dao.locksvc.eb.LockSvcEntity;

import org.junit.Test;

public class TestLockSvcDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		ILockSvcEntity entity = new LockSvcEntity();
		entity.setSvcId("svc_T1");
		entity.setLockSvcId("locksvc_T1");
		entity.setLockForLot(StatusFlg.off);
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		List<ILockSvcEntity> list = new ArrayList<ILockSvcEntity>();
		{
			ILockSvcEntity entity = new LockSvcEntity();
			entity.setSvcId("svc_T2");
			entity.setLockSvcId("locksvc_T2");
			entity.setLockForLot(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			ILockSvcEntity entity = new LockSvcEntity();
			entity.setSvcId("svc_T3");
			entity.setLockSvcId("locksvc_T3");
			entity.setLockForLot(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		LockSvcCondition condition = new LockSvcCondition();
		condition.setSvcId( "svc_T1" );
		condition.setLockSvcId( "locksvc_T1" );
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );
		ILockSvcEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getLockForLot() );

	}
	//@Ignore
	@Test
	public void find() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		LockSvcCondition condition = new LockSvcCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(LockSvcItems.svcId, TriSortOrder.Desc, 1);

		IEntityLimit<ILockSvcEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setSvcIds( new String[] {"svc_T1", "svc_T2"} );
		condition.setLockSvcIds( new String[] {"locksvc_T1", "locksvc_T2"} );
		List<ILockSvcEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	//@Ignore
	@Test
	public void update() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		ILockSvcEntity entity = new LockSvcEntity();
		entity.setSvcId("svc_T1");
		entity.setLockSvcId("locksvc_T1");
		entity.setLockForLot(StatusFlg.on);
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		List<ILockSvcEntity> list = new ArrayList<ILockSvcEntity>();

		{
			ILockSvcEntity entity = new LockSvcEntity();
			entity.setSvcId("svc_T2");
			entity.setLockSvcId("locksvc_T2");
			entity.setLockForLot(StatusFlg.on);
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			ILockSvcEntity entity = new LockSvcEntity();
			entity.setSvcId("svc_T3");
			entity.setLockSvcId("locksvc_T3");
			entity.setLockForLot(StatusFlg.on);
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		ILockSvcDao dao = (ILockSvcDao)this.getBean( "umLockSvcDao" );

		LockSvcCondition condition = new LockSvcCondition();

		condition.setSvcId("svc_T1");
		condition.setLockSvcId("locksvc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T2");
		condition.setLockSvcId("locksvc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T3");
		condition.setLockSvcId("locksvc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
