package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestRoleDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		IRoleEntity entity = new RoleEntity();
		entity.setRoleId("role_T1");
		entity.setRoleName("Role_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		List<IRoleEntity> list = new ArrayList<IRoleEntity>();
		{
			IRoleEntity entity = new RoleEntity();
			entity.setRoleId("role_T2");
			entity.setRoleName("Role_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IRoleEntity entity = new RoleEntity();
			entity.setRoleId("role_T3");
			entity.setRoleName("Role_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		RoleCondition condition = new RoleCondition();
		condition.setRoleId( "role_T1" );
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );
		IRoleEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

	}
	//@Ignore
	@Test
	public void find() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		RoleCondition condition = new RoleCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(RoleItems.roleId, TriSortOrder.Desc, 1);

		IEntityLimit<IRoleEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setRoleIds( new String[] {"role_T1", "role_T2"} );
		List<IRoleEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

	}
	@Ignore
	@Test
	public void update() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		IRoleEntity entity = new RoleEntity();
		entity.setRoleId("role_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		List<IRoleEntity> list = new ArrayList<IRoleEntity>();

		{
			IRoleEntity entity = new RoleEntity();
			entity.setRoleId("role_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IRoleEntity entity = new RoleEntity();
			entity.setRoleId("role_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IRoleDao dao = (IRoleDao)this.getBean( "umRoleDao" );

		RoleCondition condition = new RoleCondition();

		condition.setRoleId("role_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setRoleId("role_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setRoleId("role_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
