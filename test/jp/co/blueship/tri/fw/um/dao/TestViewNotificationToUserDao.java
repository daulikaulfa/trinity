package jp.co.blueship.tri.fw.um.dao;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.hist.IViewNotificationToUserDao;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.ViewNotificationToUserCondition;

public class TestViewNotificationToUserDao extends TestTriJdbcDaoSupport {

	@Test
	public void find(){
		try{

			IViewNotificationToUserDao dao = (IViewNotificationToUserDao)this.getBean( "umViewNotificationToUserDao" );

			ViewNotificationToUserCondition condition = new ViewNotificationToUserCondition();
			ISqlSort sort = new SortBuilder();

			List<IHistEntity> entityList = dao.find(condition, sort);

			for( IHistEntity entity : entityList ){
				System.out.print("  HistId : "		+ entity.getHistId());
				System.out.print("  DataId : "		+ entity.getHistId());
				System.out.print("  DataCtgCd : "	+ entity.getDataCtgCd());
				System.out.print("  LotId : "		+ entity.getLotId());
				System.out.print("  StsId : "		+ entity.getStsId());
				System.out.print("  ActStsId : "	+ entity.getActSts());
				System.out.print("  regUserNm : "	+ entity.getRegUserNm());
				System.out.print("  regTimestamp : "+ entity.getRegTimestamp());

			}
			System.out.println("finish!!");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
