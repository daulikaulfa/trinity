package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgDao;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;

public class TestCategoryDao extends TestTriJdbcDaoSupport  {
	@Ignore
	@Test
	public void insert() {
		ICtgDao dao = (ICtgDao) this.getBean( "umCategoryDao" );
		
		ICtgEntity entity = new CtgEntity();
		entity.setCtgId("1");
		entity.setCtgNm("Category_T1");
		entity.setSortOdr(1);
		entity.setDelStsId(StatusFlg.off);
		
		int count = dao.insert( entity );
	
		assertTrue( 1 == count );
		
	}

	@Ignore
	@Test
	public void batchInsert() {
		ICtgDao dao = (ICtgDao) this.getBean( "umCategoryDao" );
		
		List<ICtgEntity> list = new ArrayList<ICtgEntity>();
		{
			ICtgEntity entity = new CtgEntity();
			entity.setCtgId("1");
			entity.setCtgNm("Category_T1");
			entity.setSortOdr(1);
			entity.setDelStsId(StatusFlg.off);
		
			list.add( entity );
		}
		
		{
			ICtgEntity entity = new CtgEntity();
			entity.setCtgId("2");
			entity.setCtgNm("Category_T2");
			entity.setSortOdr(2);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}
		int count[] = dao.insert( list );
	
		assertTrue( 2 == count.length );
		
	}
	
	@Ignore
	@Test
	public void update() {
		ICtgDao dao = (ICtgDao) this.getBean( "umCategoryDao" );
		
		ICtgEntity entity = new CtgEntity();
		entity.setCtgId("1");
		entity.setCtgNm("Category_T1");
		entity.setSortOdr(1);
		
		int count = dao.update( entity );
		
		assertTrue( 1 == count );
		
	}
	
	@Ignore
	@Test
	public void batchUpdate() {
		ICtgDao dao = (ICtgDao) this.getBean( "umCategoryDao" );
		
		List<ICtgEntity> list = new ArrayList<ICtgEntity>();
		
		{
			ICtgEntity entity = new CtgEntity();
			entity.setCtgId("1");
			entity.setCtgNm("Category_A");
			entity.setSortOdr(2);
			
			list.add( entity );
		}
		
		{
			ICtgEntity entity = new CtgEntity();
			entity.setCtgId("2");
			entity.setCtgNm("Category_B");
			entity.setSortOdr(1);
			
			list.add( entity );
		}
		
		int count[] = dao.update( list );
		
		assertTrue( 2 == count.length );
		
	}
	
	//@Ignore
	@Test
	public void find() {
		ICtgDao dao = (ICtgDao)this.getBean( "umCategoryDao" ); 
		
		CtgCondition condition = new CtgCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(CtgItems.ctgId, TriSortOrder.Desc, 1);
		
		condition.setCtgId("1");
		List<ICtgEntity> entity = dao.find( condition.getCondition(), sort);
		assertTrue( 1 == entity.size() );
	}
	
	@Ignore
	@Test
	public void findByPrimaryKey() {
		CtgCondition condition = new CtgCondition();
		condition.setCtgId( "1" );
		ICtgDao dao = (ICtgDao)this.getBean( "umCategoryDao" );
		ICtgEntity entity = dao.findByPrimaryKey( condition.getCondition() );
		
		assertTrue( null != entity );
		
	}
	
	@Ignore
	@Test
	public void delete() {
		ICtgDao dao = (ICtgDao)this.getBean( "umCategoryDao" );
		
		CtgCondition condition = new CtgCondition();
		
		condition.setCtgId("1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );
	}

}
