package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneDao;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;

public class TestMilestoneDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		IMstoneDao dao = (IMstoneDao) this.getBean( "umMilestoneDao" );
		
		IMstoneEntity entity = new MstoneEntity();
		entity.setMstoneId("1");
		entity.setMstoneNm("Milestone_T1");
		entity.setLotId("LotMilestone_T1");
		entity.setSortOdr(1);
		
		int count = dao.insert(entity);
	
		assertTrue( 1 == count );
		
	}
	
	@Ignore
	@Test
	public void batchInsert() {
		IMstoneDao dao = (IMstoneDao) this.getBean( "umMilestoneDao" );
		
		List<IMstoneEntity> list = new ArrayList<IMstoneEntity>();
		{
			IMstoneEntity entity = new MstoneEntity();
			entity.setMstoneId("2");
			entity.setMstoneNm("Milestone_T2");
			entity.setLotId("LotMilestone_T2");
			entity.setSortOdr(2);
			
			list.add( entity );
		}
		
		{
			IMstoneEntity entity = new MstoneEntity();
			entity.setMstoneId("3");
			entity.setMstoneNm("Milestone_T3");
			entity.setLotId("LotMilestone_T3");
			entity.setSortOdr(3);
			
			list.add( entity );
		}
		int count[] = dao.insert( list );
	
		assertTrue( 2 == count.length );
	}
	
	@Ignore
	@Test
	public void update() {
		IMstoneDao dao = (IMstoneDao) this.getBean( "umMilestoneDao" );
		
		IMstoneEntity entity = new MstoneEntity();
		entity.setMstoneId("1");
		entity.setMstoneNm("Milestone_A");
		entity.setLotId("LotMilestone_A");
		entity.setSortOdr(1);
		
		int count = dao.update(entity);
	
		assertTrue( 1 == count );
		
	}
	
	@Ignore
	@Test
	public void batchUpdate() {
		IMstoneDao dao = (IMstoneDao) this.getBean( "umMilestoneDao" );
		
		List<IMstoneEntity> list = new ArrayList<IMstoneEntity>();
		
		{
			IMstoneEntity entity = new MstoneEntity();
			entity.setMstoneId("2");
			entity.setMstoneNm("Milestone_B");
			entity.setLotId("LotMilestone_B");
			entity.setSortOdr(4);
			
			list.add( entity );
		}
		
		{
			IMstoneEntity entity = new MstoneEntity();
			entity.setMstoneId("3");
			entity.setMstoneNm("Milestone_C");
			entity.setLotId("LotMilestone_C");
			entity.setSortOdr(3);
			
			list.add( entity );
		}
		
		int count[] = dao.update( list );
		
		assertTrue( 2 == count.length );
		
	}
	
	//@Ignore
	@Test
	public void find() {
		IMstoneDao dao = (IMstoneDao)this.getBean( "umMilestoneDao" );
		
		MstoneCondition condition = new MstoneCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.mstoneId, TriSortOrder.Desc, 1);
		
		condition.setMstoneId("3");
		List<IMstoneEntity> entity = dao.find( condition.getCondition(), sort);
		assertTrue( 1 == entity.size() );
	}

	@Ignore
	@Test
	public void findByPrimaryKey() {
		MstoneCondition condition = new MstoneCondition();
		condition.setMstoneId( "1" );
		IMstoneDao dao = (IMstoneDao)this.getBean( "umMilestoneDao" );
		IMstoneEntity entity = dao.findByPrimaryKey( condition.getCondition() );
	
		assertTrue( null != entity );
	}
	
	@Ignore
	@Test
	public void delete() {
		IMstoneDao dao = (IMstoneDao)this.getBean( "umMilestoneDao" );
		
		MstoneCondition condition = new MstoneCondition();
		
		condition.setMstoneId("1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );
	}
	
}
