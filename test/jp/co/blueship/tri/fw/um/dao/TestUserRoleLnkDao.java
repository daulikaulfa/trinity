package jp.co.blueship.tri.fw.um.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserRoleLnkItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;

import org.junit.Test;

public class TestUserRoleLnkDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		IUserRoleLnkEntity entity = new UserRoleLnkEntity();
		entity.setUserId("blueship_T1");
		entity.setRoleId("role_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		List<IUserRoleLnkEntity> list = new ArrayList<IUserRoleLnkEntity>();
		{
			IUserRoleLnkEntity entity = new UserRoleLnkEntity();
			entity.setUserId("blueship_T2");
			entity.setRoleId("role_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IUserRoleLnkEntity entity = new UserRoleLnkEntity();
			entity.setUserId("blueship_T3");
			entity.setRoleId("role_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId( "blueship_T1" );
		condition.setRoleId( "role_T1" );
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );
		IUserRoleLnkEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getDelStsId() );

	}
	//@Ignore
	@Test
	public void find() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(UserRoleLnkItems.userId, TriSortOrder.Desc, 1);

		IEntityLimit<IUserRoleLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setUserIds( new String[] {"blueship_T1", "blueship_T2"} );
		List<IUserRoleLnkEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( IUserRoleLnkEntity entity: entities ) {
			System.out.println( entity.getUserId() + ":" + entity.getRoleId() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		IUserRoleLnkEntity entity = new UserRoleLnkEntity();
		entity.setUserId("blueship_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		List<IUserRoleLnkEntity> list = new ArrayList<IUserRoleLnkEntity>();

		{
			IUserRoleLnkEntity entity = new UserRoleLnkEntity();
			entity.setUserId("blueship_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IUserRoleLnkEntity entity = new UserRoleLnkEntity();
			entity.setUserId("blueship_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IUserRoleLnkDao dao = (IUserRoleLnkDao)this.getBean( "umUserRoleLnkDao" );

		UserRoleLnkCondition condition = new UserRoleLnkCondition();

		condition.setUserId("blueship_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setUserId("blueship_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
