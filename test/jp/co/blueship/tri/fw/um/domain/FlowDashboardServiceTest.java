package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.IDashboardLotViewBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowDashboardServiceLotListBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean.NotificationToUserView;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;

public class FlowDashboardServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowDashboardService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowDashboardServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			{
				paramBean.getRecentUpdates().getParam().setRequestOption(RequestOption.none);

				service.execute(FLOW_ACTION_ID, serviceDto);
				this.debug(paramBean);
			}


			{
				paramBean.getRecentUpdates().getParam().setRequestOption(RequestOption.more);
				paramBean.getNotificationToUser().getParam().setRequestOption(FlowDashboardServiceNotificationToUserBean.RequestOption.more);

				service.execute(FLOW_ACTION_ID, serviceDto);
				this.debug(paramBean);
			}

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private FlowDashboardServiceBean getParamBean() {
		FlowDashboardServiceBean serviceBean = new FlowDashboardServiceBean();

		serviceBean.setUserId( getAuthUserId() );
		serviceBean.setUserName( getAuthUserName() );

		FlowDashboardServiceLotListBean lotListBean = new FlowDashboardServiceLotListBean();
		//lotListBean.getParam().setIncrementalCondition("80002");
		FlowCheckInOutRequestListServiceBean requestBean = new FlowCheckInOutRequestListServiceBean();

		FlowDashboardServiceRecentUpdatesBean recentUpDatesBean = new FlowDashboardServiceRecentUpdatesBean();

		FlowDashboardServiceNotificationToUserBean notificationToUser = new FlowDashboardServiceNotificationToUserBean();

		serviceBean.setLotListBean(lotListBean);
		serviceBean.setCheckInOutRequestListBean(requestBean);
		serviceBean.setRecentUpdates(recentUpDatesBean);
		serviceBean.setNotificationToUser(notificationToUser);

		return serviceBean;
	}


	private void debug(FlowDashboardServiceBean paramBean){
		System.out.println("LotList");
		for ( IDashboardLotViewBean bean: paramBean.getLotListBean().getLotViews() ) {
			System.out.println("lotID:=" + bean.getLotId() + " lotSubject:=" + bean.getLotSubject());
		}

		this.showRecentUpDatesBean(paramBean.getRecentUpdates());

		this.showNotificationToUserBean( paramBean.getNotificationToUser() );

		System.out.println("");
	}

	private void showRecentUpDatesBean(FlowDashboardServiceRecentUpdatesBean recentUpDatesBean){

		System.out.println("\nRecentUpDates");

		for(RecentUpdateViewBean view : recentUpDatesBean.getRecentUpdateViews()){

			System.out.print("  HistId : "			+ view.getHistId());
			System.out.print("  SubmitterId : "		+ view.getSubmitterId());
			System.out.print("  SubmitterNm : "		+ view.getSubmitterNm());
			System.out.print("  DataAttribute : "	+ ((view.getDataAttribute()!=null)? view.getDataAttribute().value():""));
			System.out.print("  ActStsId : " 		+ view.getActStsId());
			System.out.print("  LotId : " 			+ view.getLotId());
			System.out.print("  LotNm : " 			+ view.getLotNm());
			System.out.print("  DataId : " 			+ view.getDataId());
			System.out.print("  DataSubject : " 	+ view.getDataSubject());
			System.out.print("  Comment : " 		+ view.getComment());
			System.out.print("  MilestoneNm : "		+ view.getMstoneNm());
			System.out.print("  CategoryNm : " 		+ view.getCtgNm());
			System.out.print("  ElapsedTime : "		+ view.getElapsedTime());
			System.out.println("");
		}

		System.out.println("HasNext : " + recentUpDatesBean.hasNext());
	}


	private void showNotificationToUserBean( FlowDashboardServiceNotificationToUserBean notificationToUserBean ){

		System.out.println("\nNotificationToUser\n");

		for(NotificationToUserView view : notificationToUserBean.getNotificationViews()){

			System.out.println( " "		+ view.getNotificationDate() );
			System.out.println( "   "	+ view.getSubject() );
			System.out.println( "   "	+ view.getMessage() );
			System.out.println( "   "	+ view.isNewsFromAdmin() );
			System.out.println();
		}

		System.out.println("HasNext : " + notificationToUserBean.hasNext() );
	}
}
