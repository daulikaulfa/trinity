package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleGroupLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleListServiceBean.RoleView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowRoleServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-AdminSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String createdRoleId = null;
	private Set<String> groupIdSet = new LinkedHashSet<String>();
	private Set<String> actionIdSet = new LinkedHashSet<String>();

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

//			this.create(service);
			this.edit(service);
//			this.list(service);
//			this.remove(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void create( IGenericTransactionService service ) throws Exception {
		FlowRoleCreationServiceBean serviceBean = new FlowRoleCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		RoleEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmRoleCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
			this.debugGroupViews(inputBean);
			System.out.println(serviceBean.getActionSelectionView().toJsonFromActionSelection());

			{
				actionIdSet.add("FlowBuildPackageCloseService");
				actionIdSet.add("FlowChaLibCompLendCancelService");
				actionIdSet.add("FlowCategoryCreationService");
				actionIdSet.add("FlowCategoryEditingService");
				actionIdSet.add("FlowCategoryListService");
				actionIdSet.add("FlowCategoryRemovalService");
			}

			{
				groupIdSet.add("15060004");
//				groupIdSet.add("16050021");
			}
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setActionIdSet(actionIdSet);
			inputBean.setGroupIdSet(groupIdSet);
			inputBean.setRoleNm("Administrator 1");
			service.execute(ServiceId.UmRoleCreationService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			createdRoleId = serviceBean.getResult().getRoleId();
		}

	}

	private void edit( IGenericTransactionService service ) throws Exception {
		FlowRoleEditServiceBean serviceBean = new FlowRoleEditServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
			//serviceBean.getParam().setSelectedRoleId(createdRoleId);
			serviceBean.getParam().setSelectedRoleId("1");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		RoleEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmRoleEditService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
			this.debugGroupViews(inputBean);
			this.selectedActions(inputBean);
			this.selectedGroups(inputBean);

			System.out.println(serviceBean.getActionSelectionView().toJsonFromActionSelection());

			{
				actionIdSet.add("FlowBuildPackageCloseService");
				actionIdSet.add("FlowChaLibCompLendCancelService");
				actionIdSet.add("FlowDepartmentCreationService");
				actionIdSet.add("FlowDepartmentEditService");
			}

			{
				groupIdSet.add("16050021");
			}
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setActionIdSet(actionIdSet);
			inputBean.setGroupIdSet(groupIdSet);
			inputBean.setRoleNm("Administrator FSU1.BU1");
			service.execute(ServiceId.UmRoleEditService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {
		FlowRoleRemovalServiceBean serviceBean = new FlowRoleRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
			serviceBean.getParam().setSelectedRoleId(createdRoleId);
//			serviceBean.getParam().setSelectedRoleId("16050015");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmRoleRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {
		FlowRoleListServiceBean serviceBean = new FlowRoleListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		String selectedRoleId = "";
		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmRoleListService.value(), serviceDto);
			for ( RoleView view: serviceBean.getRoleViews() ) {
				System.out.println("=======");
				System.out.println("Role ID:=   " + view.getRoleId());
				System.out.println("Role Name:= " + view.getRoleNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("=======");
				System.out.println("");
			}
			if ( null != serviceBean.getRoleViews()
					&& serviceBean.getRoleViews().size() > 1) {
				selectedRoleId = serviceBean.getRoleViews().get(1).getRoleId();
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			RoleListServiceSortOrderEditBean sortOrder = new RoleListServiceSortOrderEditBean();
			sortOrder.setRoleId(selectedRoleId);
			sortOrder.setNewSortOrder(1);
			serviceBean.getParam().setSortOrderEdit(sortOrder);

			service.execute(ServiceId.UmRoleListService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );

			for ( RoleView view: serviceBean.getRoleViews() ) {
				System.out.println("=======");
				System.out.println("Role ID:=   " + view.getRoleId());
				System.out.println("Role Name:= " + view.getRoleNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("=======");
				System.out.println("");
			}
		}
	}

	private void selectedGroups(RoleEditInputBean inputBean) {
		for (String grpId : inputBean.getGroupIdSet()) {
			System.out.println("Group ID: " + grpId);
			System.out.println("");
		}
	}

	private void selectedActions(RoleEditInputBean inputBean) {
		for (String svcId : inputBean.getActionIdSet()) {
			System.out.println("Svc ID: " + svcId);
			System.out.println("");
		}
	}
	private void debugGroupViews(RoleEditInputBean inputBean) {
		System.out.println("Number of groups: " + inputBean.getGroupViews().size());
		for (RoleGroupLinkViewBean viewBean : inputBean.getGroupViews() ) {
			System.out.println("Group ID   =: " + viewBean.getGrpId());
			System.out.println("Group Name =: " + viewBean.getGrpNm());
			System.out.println("Count      =: " + viewBean.getCount());
			System.out.println("");
		}
	}
}
