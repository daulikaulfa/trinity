package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.WikiView;

public class FlowWikiListServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.list(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void list( IGenericTransactionService service ) throws Exception {
		FlowWikiListServiceBean serviceBean = new FlowWikiListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId("LOT-1605180007");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType( RequestType.init );
			serviceBean.getParam().setSearchPageNm("TEST");
			serviceBean.getParam().getSearchCondition().setKeyword("List");
			service.execute( ServiceId.UmWikiListService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}


	private void debug( FlowWikiListServiceBean serviceBean ){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		for(WikiView view: serviceBean.getWikiViews()){
			System.out.println("WikiId : " + view.getWikiId());
			System.out.println("WikiNm : " + view.getWikiNm());
			System.out.println("Content : " + view.getContent());
			System.out.println("UpdDate : " + view.getUpdDate());
			for( String tag : view.getTags() )
				System.out.println(tag);

			System.out.println("\n");
		}
	}
}
