package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean.Notification;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowProgressNotificationServiceBean.RequestOption;

public class FlowProgressNotificationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.notice(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void notice( IGenericTransactionService service ) throws Exception {
		FlowProgressNotificationServiceBean serviceBean = new FlowProgressNotificationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//count
		{
			serviceBean.getParam().setRequestType( RequestType.init );
			serviceBean.getParam().setRequestOption(RequestOption.count);
			service.execute( ServiceId.UmProgressNotificationService.value(), serviceDto);
			this.debug(serviceBean);
		}

		//views
		{
			serviceBean.getParam().setRequestType( RequestType.init );
			serviceBean.getParam().setRequestOption(RequestOption.views);
			service.execute( ServiceId.UmProgressNotificationService.value(), serviceDto);
			this.debug(serviceBean);
		}

		//refresh
		{
			serviceBean.getParam().setRequestType( RequestType.onChange );
			serviceBean.getParam().setRequestOption(RequestOption.refreshViews);
			service.execute( ServiceId.UmProgressNotificationService.value(), serviceDto);
			this.debug(serviceBean);
		}

	}

	private void debug(FlowProgressNotificationServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		System.out.println("RequestOption : " + serviceBean.getParam().getRequestOption().value() + "\n");

		for(Notification view : serviceBean.getNotificationViews()){
			System.out.print("  |  InfoMessage : "	+ view.getInfoMessage());
			System.out.print("  |  Subject : "		+ view.getSubject());
			System.out.print("  |  ResultMessage : "+ view.getResultMessage());
//			System.out.print("  |  Result : "		+ view.getResult().value());
//			System.out.print("  |  IProgressBar : "	+ view.getBar().getPercentComplete());
			System.out.print("  |  RemainingTime : "+ view.getRemainingTime());
			System.out.print("  |  StartDate : "	+ view.getStartDate());
			System.out.print("  |  EndDate : "		+ view.getEndDate());
			System.out.print("  |  ServiceId : "	+ view.getServiceId());
			System.out.print("  |  LotId : "		+ view.getLotId() +"\n");
		}

		System.out.println("\nCountOfUnRead : " + serviceBean.getCountOfUnread());
		System.out.println("\n");

	}
}
