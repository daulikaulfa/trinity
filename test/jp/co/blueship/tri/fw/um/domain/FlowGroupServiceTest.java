package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupRoleLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupUserLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean.GroupRequestOption;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupListServiceBean.GroupView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowGroupServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-AdminSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	//private String createdGrpId = null;
	private List<String> selectedUserIds = new ArrayList<String>();
	private List<String> selectedRoleIds = new ArrayList<String>();

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
//			this.edit(service);
//			this.list(service);
//			this.remove(service);

			System.out.println("finish.");
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create( IGenericTransactionService service ) throws Exception {
		FlowGroupCreationServiceBean serviceBean = new FlowGroupCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		GroupEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmGroupCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );

			this.debugUserViews(inputBean.getUserViews());
			this.debugRoleViews(inputBean.getRoleViews());

			if (TriStringUtils.isNotEmpty(inputBean.getRoleViews())) {
				selectedRoleIds.add(inputBean.getRoleViews().get(2).getRoleId());
				selectedRoleIds.add(inputBean.getRoleViews().get(1).getRoleId());
				selectedRoleIds.add(inputBean.getRoleViews().get(3).getRoleId());
			}

			if (TriStringUtils.isNotEmpty(inputBean.getUserViews())) {
				selectedUserIds.add(inputBean.getUserViews().get(0).getUserId());
				selectedUserIds.add(inputBean.getUserViews().get(1).getUserId());
				selectedUserIds.add(inputBean.getUserViews().get(2).getUserId());
				selectedUserIds.add(inputBean.getUserViews().get(3).getUserId());
				selectedUserIds.add("admin");
				selectedUserIds.add("tanaka");
			}
		}

		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setRoleIds(selectedRoleIds.toArray(new String[0]));
			inputBean.setUserIds(selectedUserIds.toArray(new String[0]));
			inputBean.setGroupNm("Developer11");
			service.execute(ServiceId.UmGroupCreationService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			//createdGrpId = serviceBean.getResult().getGroupId();
		}

	}

	@SuppressWarnings("unused")
	private void edit( IGenericTransactionService service ) throws Exception {
		FlowGroupEditServiceBean serviceBean = new FlowGroupEditServiceBean();

		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
//			serviceBean.getParam().setSelectedGroupId(createdGrpId);
			serviceBean.getParam().setSelectedGroupId("16050014");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		GroupEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmGroupEditService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );

			// selected users
			this.debugUserViews(serviceBean.getUserViews());

			// selected roles
			this.debugSelectedRoles(inputBean.getRoleIdSet());

			this.debugUserViews(inputBean.getUserViews());
			this.debugRoleViews(inputBean.getRoleViews());

		}

		//onChange: remove user from views
		{
			serviceBean.getParam().setRequestOption(GroupRequestOption.ExcludeUser);
			serviceBean.getParam().getInputInfo().setExcludeUserId(serviceBean.getUserViews().get(0).getUserId());
			System.out.println("Removal User ID: " + serviceBean.getUserViews().get(0).getUserId());
			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.UmGroupEditService.value(), serviceDto);

			// selected users
			this.debugUserViews(serviceBean.getUserViews());

			// selected roles
			this.debugSelectedRoles(inputBean.getRoleIdSet());

			this.debugUserViews(inputBean.getUserViews());
			this.debugRoleViews(inputBean.getRoleViews());
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getInputInfo().setGroupNm("Developer");
			serviceBean.getParam().getInputInfo().getUserIdSet().addAll(selectedUserIds);
			serviceBean.getParam().getInputInfo().getRoleIdSet().addAll(selectedUserIds);
			service.execute(ServiceId.UmGroupEditService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {
		FlowGroupListServiceBean serviceBean = new FlowGroupListServiceBean();

		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		String selectedGrpId = "";
		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmGroupListService.value(), serviceDto);
			for ( GroupView view: serviceBean.getGroupViews() ) {
				System.out.println("============================");
				System.out.println("Group ID:=   " + view.getGroupId());
				System.out.println("Group Name:= " + view.getGroupNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("============================");
				System.out.println("");
			}
			if ( null != serviceBean.getGroupViews()
					&& serviceBean.getGroupViews().size() > 1) {
				selectedGrpId = serviceBean.getGroupViews().get(1).getGroupId();
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			GroupListServiceSortOrderEditBean sortOrder = new GroupListServiceSortOrderEditBean();
			sortOrder.setGroupId(selectedGrpId);
			sortOrder.setNewSortOrder(1);
			serviceBean.getParam().setSortOrderEdit(sortOrder);

			service.execute(ServiceId.UmGroupListService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			for ( GroupView view: serviceBean.getGroupViews() ) {
				System.out.println("============================");
				System.out.println("Group ID:=   " + view.getGroupId());
				System.out.println("Group Name:= " + view.getGroupNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("============================");
				System.out.println("");
			}
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {
		FlowGroupRemovalServiceBean serviceBean = new FlowGroupRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedGroupId("16050022");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmGroupRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}

	}

	private void debugUserViews(List<GroupUserLinkViewBean> userViews) {
		System.out.println("Number of users: " + userViews.size());
		for (GroupUserLinkViewBean view : userViews) {
			System.out.println("============================");
			System.out.println("User ID=:       " + view.getUserId());
			System.out.println("User Name=:     " + view.getUserNm());
			System.out.println("Email Address=: " + view.getMailAddress());
			System.out.println("============================");
			System.out.println("");
		}
	}

	private void debugRoleViews(List<GroupRoleLinkViewBean> roleViews) {
		System.out.println("Number of roles: " + roleViews.size());
		for (GroupRoleLinkViewBean view : roleViews) {
			System.out.println("============================");
			System.out.println("Role ID=:       " + view.getRoleId());
			System.out.println("Role Name=:     " + view.getRoleNm());
			System.out.println("Count=:         " + view.getCount());
			System.out.println("============================");
			System.out.println("");
		}
	}

	private void debugSelectedRoles(Set<String> roleIdSet) {
		System.out.println("Number of roles: " + roleIdSet.size());
		for (String role : roleIdSet) {
			System.out.println("Role ID=:       " + role );
		}
	}
}
