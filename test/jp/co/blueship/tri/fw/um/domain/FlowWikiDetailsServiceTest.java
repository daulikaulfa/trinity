package jp.co.blueship.tri.fw.um.domain;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean.WikiView;

public class FlowWikiDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void details( IGenericTransactionService service ) throws Exception {
		FlowWikiDetailsServiceBean serviceBean = new FlowWikiDetailsServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedWikiId("16070002");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType( RequestType.init );
			//serviceBean.getParam().setSearchPageNm("TEST");
			service.execute( ServiceId.UmWikiDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}

	private void debug(FlowWikiDetailsServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		this.showDetailsView(serviceBean.getDetailsView());

		this.showPageViews(serviceBean.getPageViews());

		this.showTagViews(serviceBean.getTagViews());

	}

	private void showDetailsView(WikiView view){

		System.out.println("WikiId : " + view.getWikiId());
		System.out.println("WikiNm : " + view.getWikiNm());
		System.out.println("Content : " + view.getContent());
		System.out.println("isWikiHome : " + view.isWikiHome());
		System.out.println("CreateBy : " + view.getCreatedBy());
		System.out.println("CreateDate : " + view.getCreatedDate());
		System.out.println("UpdUser : " + view.getUpdUserNm());
		System.out.println("UpdDate : " + view.getUpdDate());

		for( String tag : view.getTagViews() )
			System.out.println(tag);
	}

	private void showTagViews(List<WikiTagViewBean> tagViews){
		for(WikiTagViewBean tagView:tagViews){
			System.out.print("  TagNm : " + tagView.getTagNm());
			System.out.print("  Qty : " + tagView.getQuantity());
			System.out.println("");
		}
	}

	private void showPageViews(List<WikiPageViewBean> pageViews){
		for(WikiPageViewBean pageView:pageViews){
			System.out.print("  WikiId : " + pageView.getWikiId());
			System.out.print("  WikiNm : " + pageView.getWikiNm());
			System.out.println("");
		}
	}
}
