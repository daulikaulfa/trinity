package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean.ProfileEditInputBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowProfileEditServiceTest extends TestDomainSupport {

private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-PersonalSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowProfileEditServiceBean serviceBean = new FlowProfileEditServiceBean();
			{
				serviceBean.setUserId( getAuthUserId() );
				serviceBean.setUserName( getAuthUserName() );
				serviceBean.setLanguage( "en" );
			}

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( serviceBean );

			ProfileEditInputBean inputBean = serviceBean.getParam().getInputInfo();

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setGenderType(GenderType.Male);
			inputBean.setBiography("This is Haru!!!!");
			inputBean.setUserLocation("Vietnam");
			inputBean.setUrls("http://gmail.com");

			service.execute(ServiceId.UmProfileEditService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
