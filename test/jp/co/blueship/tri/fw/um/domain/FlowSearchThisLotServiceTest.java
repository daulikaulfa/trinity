package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchServiceType;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchThisSiteView;

public class FlowSearchThisLotServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	private static final String testLotId = "LOT-1506240004";

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void list( IGenericTransactionService service ) throws Exception {
		FlowSearchThisSiteServiceBean serviceBean = new FlowSearchThisSiteServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		SearchCondition condition = serviceBean.getParam().getSearchCondition();
		// Init
		{
			condition.setLotIds( new String[] { testLotId } );
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSearchServiceType( SearchServiceType.SearchLot );
			service.execute(ServiceId.UmSearchThisLotService.value(), serviceDto);
			this.debugDropBox(serviceBean);
			this.debugDataViews(serviceBean);
		}

		// Onchange - ChangeProperty
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.ChangeProperty );
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// CheckInOut Request
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.CheckInRequest );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// Removal Request
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.RemovalRequest );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// Build Package
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.BuildPackage );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// Release Package
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.ReleasePackage );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// Release Request
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.ReleaseRequest );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}

		// Deployment Job
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			condition.setTableAttribute( DataAttribute.DeploymentJob );
			condition.setMstoneStartDate("2016/05/06");
			condition.setMstoneDueDate("2016/07/12");
			service.execute(ServiceId.UmSearchThisSiteService.value(), serviceDto);
			this.debugDataViews(serviceBean);
		}
	}

	private void debugDropBox(FlowSearchThisSiteServiceBean serviceBean) {
		SearchCondition condition = serviceBean.getParam().getSearchCondition();

		System.out.println("===Lot===");
		for (ItemLabelsBean lotLabelBean : condition.getLotViews()) {
			System.out.println("LotID := " + lotLabelBean.getValue());
			System.out.println("LotNm := " + lotLabelBean.getLabel());
			System.out.println();
		}
		System.out.println("=========");

		System.out.println("===Status===");
		for (ItemLabelsBean statusLabelBean : condition.getStatusViews()) {
			System.out.println("StatusId 		 := " + statusLabelBean.getValue());
			System.out.println("StatusNm 		 := " + statusLabelBean.getLabel());
			System.out.println("Status Sub Value := " + statusLabelBean.getSubValue());
			System.out.println();
		}
		System.out.println("============");

		System.out.println("===Assignee===");
		for (ItemLabelsBean assigneeLabelBean : condition.getAssigneeViews()) {
			System.out.println("AssigneeId := " + assigneeLabelBean.getValue());
			System.out.println("AssigneeNm := " + assigneeLabelBean.getLabel());
			System.out.println();
		}
		System.out.println("==============");

		System.out.println("===Group===");
		for (ItemLabelsBean grpLabelBean : condition.getGroupViews()) {
			System.out.println("GroupId := " + grpLabelBean.getValue());
			System.out.println("GroupNm := " + grpLabelBean.getLabel());
			System.out.println();
		}
		System.out.println("===========");

		System.out.println("===Category===");
		for (ItemLabelsBean ctgLabelBean : condition.getCtgViews()) {
			System.out.println("CtgId 			:= " + ctgLabelBean.getValue());
			System.out.println("CtgNm 			:= " + ctgLabelBean.getLabel());
			System.out.println("Ctg Sub Value 	:= " + ctgLabelBean.getSubValue());
		}
		System.out.println("=============");

		System.out.println("===Milestone===");
		for (ItemLabelsBean mstoneLabelBean : condition.getMstoneViews()) {
			System.out.println("MstoneId 		 := " + mstoneLabelBean.getValue());
			System.out.println("MstoneNm 		 := " + mstoneLabelBean.getLabel());
			System.out.println("Mstone Sub Value := " + mstoneLabelBean.getSubValue());
			System.out.println();
		}
		System.out.println("==============");
	}

	private void debugDataViews(FlowSearchThisSiteServiceBean serviceBean) {

		for (SearchThisSiteView view : serviceBean.getSearchSiteViews()) {
			System.out.println("LotId			 := " + view.getLotId());
			System.out.println("LotNm			 := " + view.getLotNm());
			System.out.println("DataAttribute    := " + view.getDataAttribute());
			System.out.println("Id				 := " + view.getId());
			System.out.println("Subject			 := " + view.getSubject());
			System.out.println("StsId			 := " + view.getStsId());
			System.out.println("Status			 := " + view.getStatus());
			System.out.println("AssigneeNm		 := " + view.getAssigneeNm());
			System.out.println("AssigneeIconPath := " + view.getAssigneeIconPath());
			System.out.println("AssigneeGroup	 := " + view.getAssigneeGroup());
			System.out.println("LatestUpdDate	 := " + view.getLatestUpdDate());
			System.out.println("AttachedFile	 := " + view.isAttachedFile());
			System.out.println("===========================");
		}
	}
}
