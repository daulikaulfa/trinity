package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryListServiceBean.CategoryView;

public class FlowCategoryServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-LotSettings-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1603150001";
	private String createdCtgId = null;

	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
			//this.edit(service);
			//this.remove(service);
			//this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create( IGenericTransactionService service ) throws Exception {

		//Param
		FlowCategoryCreationServiceBean serviceBean = new FlowCategoryCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
			serviceBean.getParam().getInputInfo().setSubject("testA");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmCategoryCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmCategoryCreationService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			createdCtgId = serviceBean.getResult().getCtgId();
		}

	}

	@SuppressWarnings("unused")
	private void edit( IGenericTransactionService service ) throws Exception {

		//Param
		FlowCategoryEditServiceBean serviceBean = new FlowCategoryEditServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
			serviceBean.getParam().setSelectedCategoryId(createdCtgId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmCategoryEditService.value(), serviceDto);
			assertTrue( "testA".equals(serviceBean.getParam().getInputInfo().getSubject()) );
			assertTrue( ! serviceBean.getResult().isCompleted() );
		}

		//submitChanges
		{
			serviceBean.getParam().getInputInfo().setSubject("testB");

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmCategoryEditService.value(), serviceDto);
			assertTrue( "testB".equals(serviceBean.getParam().getInputInfo().getSubject()) );
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {

		//Param
		FlowCategoryRemovalServiceBean serviceBean = new FlowCategoryRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedCategoryId(createdCtgId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmCategoryRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {

		//Param
		FlowCategoryListServiceBean serviceBean = new FlowCategoryListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmCategoryListService.value(), serviceDto);
			for ( CategoryView view: serviceBean.getCategoryViews() ) {
				System.out.println("ID:=" + view.getCategoryId());
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			CategoryListServiceSortOrderEditBean sortOrder = new CategoryListServiceSortOrderEditBean();
			sortOrder.setCategoryId("16030010");
			sortOrder.setNewSortOrder(4);
			serviceBean.getParam().setSortOrderEdit(sortOrder);

			service.execute(ServiceId.UmCategoryListService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			for ( CategoryView view: serviceBean.getCategoryViews() ) {
				System.out.println("ID:=" + view.getCategoryId());
			}
		}

	}



}
