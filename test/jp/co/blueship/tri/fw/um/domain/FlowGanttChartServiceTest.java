package jp.co.blueship.tri.fw.um.domain;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttContent;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttDay;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttHeader;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttPeriod;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.Span;

public class FlowGanttChartServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-LotSettings-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void execute() {
		final String FLOW_ACTION_ID = "FlowGanttChartService";

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowGanttChartServiceBean paramBean = this.getParamBean();

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( paramBean );

			{
				service.execute(FLOW_ACTION_ID, serviceDto);
				this.debug(paramBean);
			}

			System.out.println("finish!!!");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private FlowGanttChartServiceBean getParamBean(){
		FlowGanttChartServiceBean serviceBean = new FlowGanttChartServiceBean();

		serviceBean.setUserId( getAuthUserId() );
		serviceBean.setUserName( getAuthUserName() );
		serviceBean.getParam().setRequestType(RequestType.init);

		serviceBean.getParam().setSelectedLotId("LOT-1605180007");
		serviceBean.getParam().getSearchCondition()
			.setSpan(Span.OneMonth)
			.setGrouping(Grouping.none)
			.setStatus(Status.All)
			//.setStartDate("2016/04/01")
			;

		return serviceBean;
	}


	private void debug(FlowGanttChartServiceBean serviceBean){

		System.out.println("RequestGrouping : " + serviceBean.getParam().getSearchCondition().getGrouping().value());
		System.out.println("\n");

		for(GanttViewBean gantt : serviceBean.getGroupingViews()){
			System.out.println("Grouping : " + gantt.getGrouping().value());
			System.out.println("GroupingNm : " + gantt.getGroupingNm());

			this.showGanttHeader(gantt.getHeader());
			this.showGanttContent(gantt.getContents());

			System.out.println("");
		}
	}


	private void showGanttHeader(GanttHeader ganttHeader){

		for(GanttPeriod ganttPeriod : ganttHeader.getPeriods()){
			System.out.println("<< " + ganttPeriod.getPeriod() + " >>");

			for(GanttDay ganttDay : ganttPeriod.getDays()){
				System.out.print(ganttDay.getSeq() + "  " + ganttDay.getDay());

				if(ganttDay.isToday())
					System.out.print("  Today!");

				if(ganttDay.isHoliday())
					System.out.print("  Holiday!!!");

				if(ganttDay.isWeekBeginning())
					System.out.print("  Monday...");

				if(ganttDay.isFlag())
					System.out.print("  ▷  " + ganttDay.getFlagNm());

				System.out.println("");
			}
			System.out.println("");
		}
	}

	private void showGanttContent(List<GanttContent> contents){

		for(GanttContent content : contents){
			System.out.print("  Attribute : "	+ content.getDataAttribute().value());
			System.out.print("  DataId : "		+ content.getDataId());
			System.out.print("  Subject : "		+ content.getSubject());
			System.out.print("  AssigneeNm : "	+ content.getAssigneeNm());
			System.out.print("  StartDate : "	+ content.getStartDate());
			System.out.print("  EndDate : "		+ content.getEndDate());
			System.out.print("  Seq : "			+ content.getSeq());
			System.out.print("  CountDays : "	+ content.getCountDays());
			System.out.print("  Status : "		+ content.getStatus().value());
			System.out.println("");
		}
	}

}
