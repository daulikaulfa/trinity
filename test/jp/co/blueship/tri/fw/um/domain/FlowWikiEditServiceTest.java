package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiEditServiceBean.WikiCreationInputInfo;

public class FlowWikiEditServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void details( IGenericTransactionService service ) throws Exception {
		FlowWikiEditServiceBean serviceBean = new FlowWikiEditServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedWikiId("16070002");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//restore
		{
			serviceBean.getParam().setRequestType( RequestType.submitChanges );
			service.execute( ServiceId.UmWikiEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		/*
		{
			serviceBean.getParam().setRequestType( RequestType.init );
			service.execute( ServiceId.UmWikiEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		//edit
		{
			serviceBean.getParam().setRequestType( RequestType.submitChanges );

			serviceBean.getParam().getInputInfo().setContent("EditTestChange")
												 .setPageNm("[tag2][tag4]eidtTestPageNm")
												 ;

			service.execute( ServiceId.UmWikiEditService.value(), serviceDto);
			this.debug(serviceBean);
		}
		*/
	}

	private void debug( FlowWikiEditServiceBean serviceBean ){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		for(String tag : serviceBean.getTagViews())
			System.out.println(tag);

		WikiCreationInputInfo inputInfo = serviceBean.getParam().getInputInfo();

		System.out.println("PageNm : " + inputInfo.getPageNm());
		System.out.println("Content : " + inputInfo.getContent());

	}
}
