package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedMemberView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedReleaseView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedRequestView;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class FlowGlobalSearchServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void list( IGenericTransactionService service ) throws Exception {
		FlowGlobalSearchServiceBean serviceBean = new FlowGlobalSearchServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		// onChange
		{
			serviceBean.getParam().setRequestType( RequestType.onChange );
			serviceBean.getParam().getSearchCondition().setKeyword("dev");
			service.execute( ServiceId.UmGlobalSearchService.value(), serviceDto);
			this.debug(serviceBean);
		}
		
	}
	
	private void debug( FlowGlobalSearchServiceBean serviceBean ) {
		
		System.out.println("=====RequestView=====");
		for (RecentlyViewedRequestView requestView : serviceBean.getRequestViews()) {
			System.out.println("AreqId 				:= " + requestView.getAreqId());
			System.out.println("PjtId 				:= " + requestView.getPjtId());
			System.out.println("referenceId 		:= " + requestView.getReferenceId());
			System.out.println("CheckinFilePath 	:= " + requestView.getCheckinFilePath());
			System.out.println("CheckoutFilePath	:= " + requestView.getCheckoutFilePath());
			System.out.println();
		}
		System.out.println("=====================");
		
		System.out.println("=====ReleaseView=====");
		for (RecentlyViewedReleaseView releaseView : serviceBean.getReleaseViews()) {
			System.out.println("DataId 		:= " + releaseView.getInternalId());
			System.out.println("Subject		:= " + releaseView.getSubject());
			System.out.println("ReleaseType	:= " + releaseView.getReleaseType());
			System.out.println();
		}
		System.out.println("=====================");
		
		System.out.println("=====MemberView======");
		for (RecentlyViewedMemberView memberView : serviceBean.getMemberViews()) {
			System.out.println("UserId		:= " + memberView.getUserId());
			System.out.println("UserNm		:= " + memberView.getUserNm());
			System.out.println("IconPath 	:= " + memberView.getIconPath());
			System.out.println();
		}
		System.out.println("====================");
	}
}
