package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowMemberListServiceBean.MemberView;

public class FlowMemberListServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testUserId = "admin";
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean( "generalService" );
			((IService) service).init();
			
			FlowMemberListServiceBean serviceBean = new FlowMemberListServiceBean();
			{
				serviceBean.setUserId( testUserId );
				serviceBean.setUserName( getAuthUserName() );
			}
			
			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean(serviceBean);
			
			{
				serviceBean.getParam().setRequestType(RequestType.init);
				service.execute(ServiceId.UmMemberListService.value(), serviceDto);
				this.debug( serviceBean );
			}
			
			{
				serviceBean.getParam().setRequestType(RequestType.submitChanges);
				serviceBean.getParam().getSearchCondition().setKeyword("abc");
				service.execute(ServiceId.UmMemberListService.value(), serviceDto);
				this.debug( serviceBean );
			}
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for ( String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void debug (FlowMemberListServiceBean serviceBean) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("	Keyword:= " + serviceBean.getParam().getSearchCondition().getKeyword());
		
		System.out.println("Result: ");
		for ( MemberView member : serviceBean.getMemberViews() ) {
			System.out.print("	UserId:= " + member.getUserId());
			System.out.print("	UserNm:= " + member.getUserNm());
			System.out.print("	LastLogInTime:= " + member.getLastLogInTime());
			System.out.print("	IconPath:= " + member.getIconPath());
			System.out.print("	CurrentTime:= " + member.getCurrentTime());
			System.out.println();
		}
		System.out.println();
	}
}
