package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.LanguageAndTimezoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.PasswordEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.UserEditInputBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowPersonalSettingsEditServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-PersonalSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowPersonalSettingsEditServiceBean serviceBean = new FlowPersonalSettingsEditServiceBean();
			{
				serviceBean.setUserId( getAuthUserId() );
				serviceBean.setUserName( getAuthUserName() );
				serviceBean.setLanguage( "en" );
			}

			this.userChanges(service, serviceBean);

//			this.passwordChanges(service, serviceBean);

//			this.languageTimezoneChanges(service, serviceBean);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void userChanges( IGenericTransactionService service, FlowPersonalSettingsEditServiceBean serviceBean ) throws Exception {

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		serviceBean.getParam().setRequestOptions(RequestOption.UserChanges);
		UserEditInputBean inputBean = serviceBean.getParam().getUserInputInfo();

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
			this.debugUserChanges(inputBean);
		}

		// onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			File iconFile = new File("C:/Users/Public/Pictures/Sample Pictures/Chrysanthemum.jpg");
			InputStream targetStream = new FileInputStream(iconFile);
			inputBean.setIconFileNm(iconFile.getName());
			inputBean.setIconFilePath(iconFile.getPath());
			inputBean.setIconInputStreamBytes(StreamUtils.convertInputStreamToBytes(targetStream));
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
			System.out.println("Icon File Path: " + inputBean.getIconFilePath());
			System.out.println("Icon File Name: " + inputBean.getIconFileNm());
		}

		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setUserNm("Developer");
			inputBean.setMailAddress("xuanlt@gmail.com");
			inputBean.setGenderType(GenderType.Female);
//			inputBean.setIconOption(IconSelectionOption.DefaultImage);
//			inputBean.setIconPath("default-1.png");
			inputBean.setLocation("Viet Nam");
			inputBean.setBiography("My name is Xuan. I am Java Web Developer");
			inputBean.setUrl("https://mail.fsoft.com.vn/owa", "https://facebook.com.vn");
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
		}
	}

	@SuppressWarnings("unused")
	private void passwordChanges( IGenericTransactionService service, FlowPersonalSettingsEditServiceBean serviceBean ) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		serviceBean.getParam().setRequestOptions(RequestOption.PasswordChanges);
		PasswordEditInputBean inputBean = serviceBean.getParam().getPasswordInputInfo();

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
			System.out.println("Expiration Message: " + serviceBean.getDetailsView().getPassword().getExpirationMsg());
			this.debugPasswordChanges(inputBean);
		}

		// onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			inputBean.setNewPassword("xuan12345");
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
			assertTrue( serviceBean.getDetailsView().getPassword().isMoreThan8characters() );
			assertTrue( !serviceBean.getDetailsView().getPassword().containsUppercaseAndLowercase() );
			assertTrue( !serviceBean.getDetailsView().getPassword().isSameCharacterLessThan3Times() );
			assertTrue( serviceBean.getDetailsView().getPassword().containsNumerals() );
		}

		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setCurrentPassword("trinity12345");
			inputBean.setConfirmPassword("xuan12345");
			inputBean.setPasswordExpiration(inputBean.getPasswordExpirationViews().get(0).getValue());
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
		}
	}

	@SuppressWarnings("unused")
	private void languageTimezoneChanges( IGenericTransactionService service, FlowPersonalSettingsEditServiceBean serviceBean ) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		serviceBean.getParam().setRequestOptions(RequestOption.LanguageAndTimezoneChanges);
		LanguageAndTimezoneEditInputBean inputBean = serviceBean.getParam().getLanguageTimezoneInputInfo();

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
			System.out.println("====Language====");
			for (ItemLabelsBean bean: inputBean.getLanguageViews()) {
				System.out.println("Language value: " + bean.getValue());
				System.out.println("Language label: " + bean.getLabel());
				System.out.println("");
			}

			System.out.println("====TimeZone====");
			for (ItemLabelsBean bean : inputBean.getTimezoneViews()) {
				System.out.println("Timezone value: " + bean.getValue());
				System.out.println("Timezone label: " + bean.getLabel());
				System.out.println("");
			}
		}

		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputBean.setLanguage("en");
			inputBean.setTimezone("VN");
			service.execute(ServiceId.UmPersonalSettingsEditService.value(), serviceDto);
		}
	}

	private void debugUserChanges(UserEditInputBean inputBean) {
		System.out.println("User Name         : " + inputBean.getUserNm());
		System.out.println("Mail Address      : " + inputBean.getMailAddress());
		System.out.println("Icon Type         : " + (TriStringUtils.isEmpty(inputBean.getIconOption()) ? null : inputBean.getIconOption().value()));
		System.out.println("Default Icon Path : " + inputBean.getIconPath());
		System.out.println("Custome Icon Path : " + inputBean.getIconFilePath());
		System.out.println("Gender            : " + (TriStringUtils.isEmpty(inputBean.getGenderType()) ? null : inputBean.getGenderType().value()));
		System.out.println("User Location     : " + inputBean.getLocation());
		System.out.println("Biography         : " + inputBean.getBiography());
		System.out.println("URL               : ");
		if ( TriStringUtils.isNotEmpty(inputBean.getUrl()) ) {
			for ( String url: inputBean.getUrl() ) {
				System.out.println("                  : " + url);
			}
		}
	}


	private void debugPasswordChanges(PasswordEditInputBean inputBean) {
		for (ItemLabelsBean bean : inputBean.getPasswordExpirationViews()) {
			System.out.println("Value : " + bean.getValue());
			System.out.println("Label : " + bean.getLabel());
		}
	}
}
