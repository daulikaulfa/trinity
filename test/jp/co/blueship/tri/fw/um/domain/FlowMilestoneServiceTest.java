package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneListServiceBean.MilestoneView;

public class FlowMilestoneServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-LotSettings-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1603150001";
	private String createdMstoneId = null;

	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
			//this.edit(service);
			//this.remove(service);
			//this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create( IGenericTransactionService service ) throws Exception {

		//Param
		FlowMilestoneCreationServiceBean serviceBean = new FlowMilestoneCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
			serviceBean.getParam().getInputInfo().setSubject("testA");
			serviceBean.getParam().getInputInfo().setStartDate("2016/3/11");
			serviceBean.getParam().getInputInfo().setDueDate("2016/3/12");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmMilestoneCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmMilestoneCreationService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			createdMstoneId = serviceBean.getResult().getMstoneId();
		}

	}

	@SuppressWarnings("unused")
	private void edit( IGenericTransactionService service ) throws Exception {

		//Param
		FlowMilestoneEditServiceBean serviceBean = new FlowMilestoneEditServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
			serviceBean.getParam().setSelectedMstoneId(createdMstoneId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmMilestoneEditService.value(), serviceDto);
			assertTrue( "testA".equals(serviceBean.getParam().getInputInfo().getSubject()) );
			assertTrue( ! serviceBean.getResult().isCompleted() );
		}

		//submitChanges
		{
			serviceBean.getParam().getInputInfo().setSubject("testB");
			serviceBean.getParam().getInputInfo().setStartDate("2016/3/14");
			serviceBean.getParam().getInputInfo().setDueDate("2016/3/17");

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmMilestoneEditService.value(), serviceDto);
			assertTrue( "testB".equals(serviceBean.getParam().getInputInfo().getSubject()) );
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {

		//Param
		FlowMilestoneRemovalServiceBean serviceBean = new FlowMilestoneRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedMstoneId(createdMstoneId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmMilestoneRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {

		//Param
		FlowMilestoneListServiceBean serviceBean = new FlowMilestoneListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmMilestoneListService.value(), serviceDto);
			System.out.println(" " + RequestType.init.value());

			for ( MilestoneView view: serviceBean.getMilestoneViews() ) {
				System.out.print(" ID:=" + view.getMstoneId());
				System.out.print(" Description:=" + view.getDescription());
				System.out.print(" Start Date:=" + view.getStartDate());
				System.out.print(" Due Date:=" + view.getDueDate());
				System.out.print(" Count:=" + view.getCount());
				System.out.println("");
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			MilestoneListServiceSortOrderEditBean sortOrder = new MilestoneListServiceSortOrderEditBean();
			sortOrder.setMstoneId("16030010");
			sortOrder.setNewSortOrder(4);
			serviceBean.getParam().setSortOrderEdit(sortOrder);

			service.execute(ServiceId.UmMilestoneListService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			System.out.println(" " + RequestType.submitChanges.value());

			for ( MilestoneView view: serviceBean.getMilestoneViews() ) {
				System.out.print(" ID:=" + view.getMstoneId());
				System.out.print(" Description:=" + view.getDescription());
				System.out.print(" Start Date:=" + view.getStartDate());
				System.out.print(" Due Date:=" + view.getDueDate());
				System.out.print(" Count:=" + view.getCount());
				System.out.println("");
			}
		}

	}

}
