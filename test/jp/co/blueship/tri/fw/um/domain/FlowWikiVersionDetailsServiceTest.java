package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean.WikiVersionView;

public class FlowWikiVersionDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void details(IGenericTransactionService service ) throws Exception {
		FlowWikiVersionDetailsServiceBean serviceBean = new FlowWikiVersionDetailsServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedWikiId("16070002");
			serviceBean.getParam().setWikiVerNo(2);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType( RequestType.init );
			service.execute( ServiceId.UmWikiVersionDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}


	private void debug(FlowWikiVersionDetailsServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		System.out.println("Details");

		System.out.println("WikiVerNo : " + serviceBean.getVersionDetailsView().getWikiVerNo());
		System.out.println("Content : " + serviceBean.getVersionDetailsView().getContent());
		System.out.println("UpdUser : " + serviceBean.getVersionDetailsView().getUpdUserNm());
		System.out.println("UpdDate : "+ serviceBean.getVersionDetailsView().getUpdDate());
		System.out.println("");

		System.out.println("WikiVersionViews");

		for(WikiVersionView view : serviceBean.getWikiVersionViews()){
			System.out.print("  WikiVerNo : " + view.getWikiVerNo());
			System.out.print("  UpdUser : " + view.getUpdUserNm());
			System.out.print("  UpdDate : " + view.getUpdDate());
			System.out.println("");
		}

	}
}
