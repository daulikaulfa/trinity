package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterServiceBean.SearchFilterView;
/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowSearchFilterServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			//this.create(service);
			//this.edit(service);
			//this.remove(service);
			this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void list( IGenericTransactionService service ) throws Exception {

		//Param
		FlowSearchFilterServiceBean serviceBean = new FlowSearchFilterServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );


		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().setSelectedUserId("admin");

			service.execute(ServiceId.UmSearchFilterService.value(), serviceDto);

			for ( SearchFilterView view: serviceBean.getFilterViews() ) {
				System.out.println("filterID:=" + view.getFilterId());
				System.out.println("filterNm:=" + view.getFilterNm());
				System.out.println("serviceId:=" + view.getServiceId());
			}
		}

	}


}
