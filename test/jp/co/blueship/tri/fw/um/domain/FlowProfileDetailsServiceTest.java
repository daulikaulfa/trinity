package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttContent;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsViewBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean.Day;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean.LatestActivityRequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean.RequestOption;;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowProfileDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-PersonalSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void init(){

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			FlowProfileDetailsServiceBean serviceBean =  new FlowProfileDetailsServiceBean();
			{
				serviceBean.setUserId( getAuthUserId() );
				serviceBean.setUserName( getAuthUserName() );
			}

			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			serviceDto.setServiceBean( serviceBean );

			//init
			{
				serviceBean.getParam().setSelectedUserId("admin");
				serviceBean.getParam().setRequestType(RequestType.init);
				serviceBean.getParam().setRequestOption(RequestOption.profile);

				service.execute(ServiceId.UmProfileDetailsService.value(), serviceDto);
				this.debug(serviceBean);
			}

			//onChange latestActivity
			{
				serviceBean.getParam().setRequestType(RequestType.onChange);
				serviceBean.getParam().setRequestOption(RequestOption.latestActivity);
				serviceBean.getLatestActivityView().setRequestOption(LatestActivityRequestOption.none);

				service.execute(ServiceId.UmProfileDetailsService.value(), serviceDto);
				this.debug(serviceBean);
			}

			//onChange latestActivity more
			{
				serviceBean.getParam().setRequestType(RequestType.onChange);
				serviceBean.getParam().setRequestOption(RequestOption.latestActivity);
				serviceBean.getLatestActivityView().setRequestOption(LatestActivityRequestOption.more);

				service.execute(ServiceId.UmProfileDetailsService.value(), serviceDto);
				this.debug(serviceBean);
			}

			//onChange Gantt Chart
			{
				serviceBean.getParam().setRequestType(RequestType.onChange);
				serviceBean.getParam().setRequestOption(RequestOption.ganttChart);

				service.execute(ServiceId.UmProfileDetailsService.value(), serviceDto);
				this.debug(serviceBean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void debug(FlowProfileDetailsServiceBean serviceBean){
		System.out.println("==========================");
		System.out.println("  RequestType : " + serviceBean.getParam().getRequestType().value());
		System.out.println("  RequestOption : " + serviceBean.getParam().getRequestOption().name());


		if( RequestOption.none.equals(serviceBean.getParam().getRequestOption().name()) )
			this.showDetailsView( serviceBean.getDetailsView() );

		if( RequestOption.latestActivity.equals(serviceBean.getParam().getRequestOption().name()) )
			this.showLatestActivity(serviceBean.getLatestActivityView());

		if( RequestOption.ganttChart.equals(serviceBean.getParam().getRequestOption().name()) )
			this.showGanttChart( serviceBean.getGanttChartView() );

		System.out.println("\n");

	}


	private void showDetailsView(ProfileDetailsViewBean detailsView){
		System.out.println("==========================\n");

		System.out.println("UserId : "			+ detailsView.getUserId());
		System.out.println("UserNm : "			+ detailsView.getUserNm());
		System.out.println("IconPath : "		+ detailsView.getIconPath());
		System.out.println("Biography : "		+ detailsView.getBiography());
		System.out.println("UserLocation : "	+ detailsView.getUserLocation());

		for(String url : detailsView.getUrls())
			System.out.println("URL : " 		+ url);
	}


	private void showLatestActivity(ProfileDetailsLatestActivityBean paramBean){

		System.out.println("  LatestActivityRequestOption : " + paramBean.getRequestOption().name());
		System.out.println("==========================\n");


		for(Day day : paramBean.getDays()){
			System.out.println("Date : " + day.getDate());
			System.out.println("DateSubject : " + day.getDateSubject());
			System.out.println("");

			for( RecentUpdateViewBean view : day.getRecentUpdateViews() ){

				System.out.print("  DataAttributte : "	+ ((view.getDataAttribute() != null)? view.getDataAttribute().value():"") );
				System.out.print("  ActStsId  : "		+ view.getActStsId());
				System.out.print("  LotId  : "			+ view.getLotId());
				System.out.print("  LotNm  : "			+ view.getLotNm());
				System.out.print("  DataId  : "			+ view.getDataId());
				System.out.print("  DataSubject  : "	+ view.getDataSubject());
				System.out.print("  Comment  : "		+ view.getComment());
				System.out.print("  Time  : "			+ view.getUpdTime());
				System.out.println("");
			}
			System.out.println("");
		}
		System.out.println("HasNext : " + paramBean.hasNext());
	}

	private void showGanttChart( ProfileDetailsGanttChartBean paramBean ){

		System.out.println("  GanttChartRequestOption : " + paramBean.getRequestOption().name());
		System.out.println("==========================\n");


		System.out.println(" StartDate :" + paramBean.getSearchCondition().getStartDate());
		System.out.println(" Span :" + paramBean.getSearchCondition().getSpan().value());
		System.out.println(" Grouping :" + paramBean.getSearchCondition().getGrouping().name());
		System.out.println(" Status :" + paramBean.getSearchCondition().getStatus().name());

		System.out.println("");

		for( GanttViewBean ganttView : paramBean.getGroupingViews() ){
			System.out.println(" Grouping : " + ganttView.getGrouping().name() );
			System.out.println(" GroupingNm : " + ganttView.getGroupingNm() );

			for( GanttContent content: ganttView.getContents() ){
				System.out.println(" dataId " + content.getDataId());
			}
		}
	}
}
