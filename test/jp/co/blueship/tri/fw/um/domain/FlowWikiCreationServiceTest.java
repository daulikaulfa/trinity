package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiCreationServiceBean.WikiCreationInputInfo;


public class FlowWikiCreationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.creation(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void creation(IGenericTransactionService service ) throws Exception {
		FlowWikiCreationServiceBean serviceBean = new FlowWikiCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			//serviceBean.getParam().setSelectedLotId("LOT-1605180007");
			serviceBean.getParam().setCopyWikiId("16070002");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType( RequestType.init );
			service.execute( ServiceId.UmWikiCreationService.value(), serviceDto);
			this.debug(serviceBean);
		}

		{
			serviceBean.getParam().setRequestType( RequestType.submitChanges );
			/*
			serviceBean.getParam().getInputInfo().setPageNm("[tag1][tag2][tag3]TagTestWIkiNm")
												 .setContent("TAG TEST")
												 ;
			 */
			service.execute( ServiceId.UmWikiCreationService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}

	private void debug( FlowWikiCreationServiceBean serviceBean ){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		for(String tag : serviceBean.getTagViews())
			System.out.println(tag);

		WikiCreationInputInfo inputInfo = serviceBean.getParam().getInputInfo();

		System.out.println("PageNm : " + inputInfo.getPageNm());
		System.out.println("Content : " + inputInfo.getContent());
	}
}
