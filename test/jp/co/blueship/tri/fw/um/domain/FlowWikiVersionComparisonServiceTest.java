package jp.co.blueship.tri.fw.um.domain;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.VersionComparisonView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.WikiVersionView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.VersionComparisonView.VersionLineView;

public class FlowWikiVersionComparisonServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Wiki-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.diff(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void diff(IGenericTransactionService service ) throws Exception {
		FlowWikiVersionComparisonServiceBean serviceBean = new FlowWikiVersionComparisonServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedWikiId("16070002");
			serviceBean.getParam().setDestVersion(2);
			serviceBean.getParam().setSrcVersion(3);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType( RequestType.init );
			service.execute( ServiceId.UmWikiVersionComparisonService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}


	private void debug( FlowWikiVersionComparisonServiceBean serviceBean ){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================\n");

		System.out.println("WikiVersionViews");

		for(WikiVersionView view : serviceBean.getVersionViews()){
			System.out.print("  WikiVerNo : " + view.getWikiVerNo());
			System.out.print("  UpdUser : " + view.getUpdUserNm());
			System.out.print("  UpdDate : " + view.getUpdDate());
			System.out.println("");
		}

		System.out.println("");

		VersionComparisonView diffView = serviceBean.getVersionComparisonView();

		System.out.println("Src");
		System.out.println("  WikiVerNo : " + diffView.getSrcVersionView().getWikiVerNo());
		System.out.println("  content : " + diffView.getSrcVersionView().getContent());
		System.out.println("  UpdUser : " + diffView.getSrcVersionView().getUpdUserNm());
		System.out.println("  UpdDate : " + diffView.getSrcVersionView().getUpdDate());

		System.out.println("");

		System.out.println("Dest");
		System.out.println("  WikiVerNo : " + diffView.getDestVersionView().getWikiVerNo());
		System.out.println("  content : " + diffView.getDestVersionView().getContent());
		System.out.println("  UpdUser : " + diffView.getDestVersionView().getUpdUserNm());
		System.out.println("  UpdDate : " + diffView.getDestVersionView().getUpdDate());

		System.out.println("");

		System.out.println("DiffResult");
		for(VersionLineView lineView : diffView.getLines()){
			System.out.print("  Status : "	+ lineView.getStatus().getStatusId());
			System.out.print("  dest : "	+ lineView.getDestLineNo());
			System.out.print("  "			+ lineView.getDest());
			System.out.print("  src : "		+ lineView.getSrcLineNo());
			System.out.print("  "			+ lineView.getSrc());

			System.out.println("");

		}

	}

}
