package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import java.util.LinkedHashSet;
import java.util.Set;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean.GroupView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean.UserView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-AdminSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

//			this.create(service);
			this.edit(service);
//			this.list(service);
//			this.remove(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@SuppressWarnings("unused")
	private void create( IGenericTransactionService service ) throws Exception {
		FlowUserCreationServiceBean serviceBean = new FlowUserCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		UserEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmUserCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
			this.debugGroupViews(inputBean);
			this.debugDepartmentViews(inputBean);
			this.debugPasswordExpiration(inputBean);
			this.debugLanguageViews(inputBean);
			this.debugTimezoneViews(inputBean);
		}

		// onChange
		{
			inputBean.setPassword("trinity@12345");
			serviceBean.getParam().setRequestType(RequestType.onChange);
//			serviceBean.getParam().setRequestOptions(RequestOption.PasswordChanges);
			service.execute(ServiceId.UmUserCreationService.value(), serviceDto);

			assertTrue( serviceBean.getDetailsView().getPassword().isMoreThan8characters() );
			assertTrue( !serviceBean.getDetailsView().getPassword().containsUppercaseAndLowercase() );
			assertTrue( !serviceBean.getDetailsView().getPassword().isSameCharacterLessThan3Times() );
			assertTrue( serviceBean.getDetailsView().getPassword().containsNumerals() );

			System.out.println("Complexity: " + serviceBean.getDetailsView().getPassword().getPasswordComplexityLevel()); // 2 (Middle)

		}

		// submitChanges
		{
			Set<String> grpIdSet = new LinkedHashSet<String>();
			grpIdSet.add("1");
			grpIdSet.add("15060004");

			inputBean.setUserId("xuan1");
			inputBean.setUserNm("xuan1");
			inputBean.setMailAddress("xuanlt1@fsoft.com.vn");
			inputBean.setDeptId("16050009");
			inputBean.setIconPath("/icon/default.png");
			inputBean.setReceiveMail(true);
			inputBean.setPassword("trinity");
			inputBean.setConfirmPassword("trinity");
			inputBean.setPasswordExpiration(inputBean.getPasswordExpirationViews().get(2).getValue());
			inputBean.setLanguage("vn");
			inputBean.setTimeZone("Etc/GMT+11");
			inputBean.setGroupIdSet(grpIdSet);

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
//			serviceBean.getParam().setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges});
			service.execute(ServiceId.UmUserCreationService.value(), serviceDto);
		}
	}

	private void edit( IGenericTransactionService service ) throws Exception {
		FlowUserEditServiceBean serviceBean = new FlowUserEditServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );

		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		UserEditInputBean inputBean = serviceBean.getParam().getInputInfo();

		inputBean.setUserId("xuan1");

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmUserEditService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
		}

		// onChange
		{
			inputBean.setPassword("trinity@12345");
			serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().setRequestOptions(RequestOption.PasswordChanges);
			service.execute(ServiceId.UmUserEditService.value(), serviceDto);

			assertTrue( serviceBean.getDetailsView().getPassword().isMoreThan8characters() );
			assertTrue( !serviceBean.getDetailsView().getPassword().containsUppercaseAndLowercase() );
			assertTrue( !serviceBean.getDetailsView().getPassword().isSameCharacterLessThan3Times() );
			assertTrue( serviceBean.getDetailsView().getPassword().containsNumerals() );

			System.out.println("Complexity: " + serviceBean.getDetailsView().getPassword().getPasswordComplexityLevel()); // 2 (Middle)

		}

		// submitChange
		{
//			Set<String> grpIdSet = new LinkedHashSet<String>();
//			grpIdSet.add("16050021");
//
//			inputBean.setUserNm("xuan haru");
//			inputBean.setMailAddress("xuanlt1@fsoft.com.vn");
//			inputBean.setDeptId("16050011");
//			inputBean.setIconPath("/icon/default.png");
//			inputBean.setReceiveMail(false);
			inputBean.setPassword("haru12345");
			inputBean.setConfirmPassword("haru12345");
			inputBean.setPasswordExpiration(inputBean.getPasswordExpirationViews().get(2).getValue());
			inputBean.setLanguage("vn");
			inputBean.setTimeZone("Etc/GMT+11");
//			inputBean.setGroupIdSet(grpIdSet);

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().setRequestOptions(new RequestOption[] {RequestOption.PasswordChanges, RequestOption.UserChanges});
			service.execute(ServiceId.UmUserEditService.value(), serviceDto);
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {
		FlowUserListServiceBean serviceBean = new FlowUserListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmUserListService.value(), serviceDto);
			this.listDebug(serviceBean);
		}

		// onchange
		{
			String testDeptId = "16050013";//16050013
			String testRoleId = "16050013";//15070002, 16050013
			String testGrpId = "16050021";//16050021, 15060004
			String testKeyword = "you";
			{
				int testCase = 8;
				switch (testCase) {
				case 1:
					//Department Search
					serviceBean.getParam().getSearchCondition().setDeptId(testDeptId);
					break;

				case 2:
					//Role Search
					serviceBean.getParam().getSearchCondition().setRoleId(testRoleId);
					break;

				case 3:
					//Group Search
					serviceBean.getParam().getSearchCondition().setGroupId(testGrpId);
					break;

				case 4:
					//keyword search
					serviceBean.getParam().getSearchCondition().setKeyword(testKeyword);
					break;
				case 5:
					// Department + Role search
					serviceBean.getParam().getSearchCondition().setDeptId(testDeptId);
					serviceBean.getParam().getSearchCondition().setRoleId(testRoleId);
					break;
				case 6:
					// Role + Group Search
					serviceBean.getParam().getSearchCondition().setRoleId(testRoleId);
					serviceBean.getParam().getSearchCondition().setGroupId(testGrpId);
					break;
				case 7:
					// Department + Role + Group search
					serviceBean.getParam().getSearchCondition().setDeptId(testDeptId);
					serviceBean.getParam().getSearchCondition().setRoleId(testRoleId);
					serviceBean.getParam().getSearchCondition().setGroupId(testGrpId);
					break;
				case 8:
					// Department + Role + Group + Keyword search
					serviceBean.getParam().getSearchCondition().setDeptId(testDeptId);
					serviceBean.getParam().getSearchCondition().setRoleId(testRoleId);
					serviceBean.getParam().getSearchCondition().setGroupId(testGrpId);
					serviceBean.getParam().getSearchCondition().setKeyword(testKeyword);
					break;

				default:
					break;
			}
			}
			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.UmUserListService.value(), serviceDto);
			this.listDebug(serviceBean);
		}
	}

	private void listDebug( FlowUserListServiceBean serviceBean ) {
		System.out.println("================================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("================================");
		System.out.println("");

		System.out.println("Search Condition ");
		System.out.print("  DeptId:= " + serviceBean.getParam().getSearchCondition().getDeptId() + "   ");

		System.out.println("Department View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getDepartmentViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());
			System.out.println("");

		}
		System.out.println("");

		System.out.print("  RoleId:= " + serviceBean.getParam().getSearchCondition().getRoleId());
		System.out.println("Role View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getRoleViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());
			System.out.println("");

		}
		System.out.println("");

		System.out.print("  GrpId:= " + serviceBean.getParam().getSearchCondition().getGroupId());
		System.out.println("Group View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getGroupViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());
			System.out.println("");

		}
		System.out.println("");
		System.out.println("Keyword " + serviceBean.getParam().getSearchCondition().getKeyword());

		System.out.println("User List");
		System.out.println("Number of users: " + serviceBean.getUserViews().size());
		for ( UserView view: serviceBean.getUserViews() ) {
			System.out.print("  User Id:         " + view.getUserId());
			System.out.print("  User Name:       " + view.getUserNm());
			System.out.print("  Group Name:      " );
			for (String grpNm: view.getGroupNm()) {
				System.out.print(grpNm);
				System.out.println("");
			}
			System.out.print("  Email Address:   " + view.getMailAddress());
			System.out.print("  Language:        " + view.getLanguage());
			System.out.print("  Timezone:        " + view.getTimezone());
			System.out.print("  Last Time Login: " + view.getLastLogInTime());
			System.out.print("  Create Date:     " + view.getCreatedDate());
			System.out.print("  Updated Date:    " + view.getUpdDate());
			System.out.println("");
		}

		System.out.println("");
	}

	private void debugGroupViews(UserEditInputBean inputBean) {
		System.out.println("Number of groups: " + inputBean.getGroupViews().size());
		for (GroupView viewBean : inputBean.getGroupViews() ) {
			System.out.println("Group ID   =: " + viewBean.getGroupId());
			System.out.println("Group Name =: " + viewBean.getGroupNm());
			System.out.println("Count      =: " + viewBean.getCount());
			System.out.println("");
		}
	}

	private void debugDepartmentViews(UserEditInputBean inputBean) {
		System.out.println("Number of departments: " + inputBean.getDeptViews().size());
		for (ItemLabelsBean viewBean : inputBean.getDeptViews() ) {
			System.out.println("Dept ID   =: " + viewBean.getValue());
			System.out.println("Dept Name =: " + viewBean.getLabel());
			System.out.println("");
		}
	}

	private void debugPasswordExpiration(UserEditInputBean inputBean) {
		System.out.println("===== Password Expiration =====");
		for (ItemLabelsBean viewBean : inputBean.getPasswordExpirationViews() ) {
			System.out.println("Value   =: " + viewBean.getValue());
			System.out.println("Label =: " + viewBean.getLabel());
			System.out.println("");
		}
	}

	private void debugTimezoneViews(UserEditInputBean inputBean) {
		System.out.println("===== Timezone Views =====");
		for (ItemLabelsBean viewBean : inputBean.getTimeZoneViews() ) {
			System.out.println("Value   =: " + viewBean.getValue());
			System.out.println("Label =: " + viewBean.getLabel());
			System.out.println("");
		}
	}

	private void debugLanguageViews(UserEditInputBean inputBean) {
		System.out.println("===== Language Views =====");
		for (ItemLabelsBean viewBean : inputBean.getLanguageViews() ) {
			System.out.println("Value   =: " + viewBean.getValue());
			System.out.println("Label =: " + viewBean.getLabel());
			System.out.println("");
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {
		FlowUserRemovalServiceBean serviceBean = new FlowUserRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
			serviceBean.getParam().setSelectedUserId("xuan1");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmUserRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

}
