package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentUserViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentRemovalServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean.DepartmentRequestOption;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentListServiceBean.DepartmentView;


/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowDepartmentServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-AdminSettings-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String createdDeptId = null;
	private List<String> selectedUserId = new ArrayList<String>();

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
//			this.edit(service);
//			this.list(service);
//			this.remove(service);

			System.out.println("finish.");
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create( IGenericTransactionService service ) throws Exception {
		FlowDepartmentCreationServiceBean serviceBean = new FlowDepartmentCreationServiceBean();

		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage("en");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmDepartmentCreationService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );
			this.debugUserView(serviceBean.getParam().getInputInfo().getUserViews());
		}


		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getInputInfo().setUserIds(selectedUserId.toArray(new String[0]));
			serviceBean.getParam().getInputInfo().setDeptNm("FSU1.BU3");
			service.execute(ServiceId.UmDepartmentCreationService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			createdDeptId = serviceBean.getResult().getDeptId();
		}

	}

	@SuppressWarnings("unused")
	private void edit( IGenericTransactionService service ) throws Exception {
		FlowDepartmentEditServiceBean serviceBean = new FlowDepartmentEditServiceBean();

		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedDeptId("16050013");
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		List<DepartmentUserViewBean> userViews = null;
		List<DepartmentUserViewBean> selectedUserViews = null;

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmDepartmentEditService.value(), serviceDto);
			assertTrue( ! serviceBean.getResult().isCompleted() );

			userViews = serviceBean.getParam().getInputInfo().getUserViews();
			selectedUserViews = serviceBean.getUserViews();

			this.debugUserView(userViews);
			this.debugSelectedUsers(selectedUserViews);
		}

		// onChange
		{
			// Remove User
			serviceBean.getParam().setRequestOption(DepartmentRequestOption.ExcludeUser);
			serviceBean.getParam().getInputInfo().setExcludeUserId(selectedUserViews.get(0).getUserId());
			System.out.println("Removal User ID: " + selectedUserViews.get(0).getUserId());
			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.UmDepartmentEditService.value(), serviceDto);

			userViews = serviceBean.getParam().getInputInfo().getUserViews();
			selectedUserViews = serviceBean.getUserViews();

			this.debugUserView(userViews);
			this.debugSelectedUsers(selectedUserViews);
		}

		// submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().getInputInfo().setDeptNm("FSU1.GNC");
			serviceBean.getParam().getInputInfo().getUserIdSet().addAll(selectedUserId);
			service.execute(ServiceId.UmDepartmentEditService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}

	@SuppressWarnings("unused")
	private void list( IGenericTransactionService service ) throws Exception {
		FlowDepartmentListServiceBean serviceBean = new FlowDepartmentListServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
			serviceBean.setLanguage( "en" );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		String selectedDeptId = "";
		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.UmDepartmentListService.value(), serviceDto);
			for ( DepartmentView view: serviceBean.getDepartmentViews() ) {
				System.out.println("=====================================");
				System.out.println("Department ID:=   " + view.getDeptId());
				System.out.println("Department Name:= " + view.getDeptNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("=====================================");
				System.out.println("");
			}
			if ( null != serviceBean.getDepartmentViews()
					&& serviceBean.getDepartmentViews().size() > 1) {
				selectedDeptId = serviceBean.getDepartmentViews().get(1).getDeptId();
			}
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			DepartmentListServiceSortOrderEditBean sortOrder = new DepartmentListServiceSortOrderEditBean();
			sortOrder.setDeptId(selectedDeptId);
			sortOrder.setNewSortOrder(1);
			serviceBean.getParam().setSortOrderEdit(sortOrder);

			service.execute(ServiceId.UmDepartmentListService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
			for ( DepartmentView view: serviceBean.getDepartmentViews() ) {
				System.out.println("=====================================");
				System.out.println("Department ID:=   " + view.getDeptId());
				System.out.println("Department Name:= " + view.getDeptNm());
				System.out.println("Sort Order:=      " + view.getSortOrder());
				System.out.println("=====================================");
				System.out.println("");
			}
		}
	}

	@SuppressWarnings("unused")
	private void remove( IGenericTransactionService service ) throws Exception {
		FlowDepartmentRemovalServiceBean serviceBean = new FlowDepartmentRemovalServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );

			serviceBean.getParam().setSelectedDeptId(createdDeptId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmDepartmentRemovalService.value(), serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}

	}

	private void debugUserView(List<DepartmentUserViewBean> userViews) {
		int count = 1;
		for (DepartmentUserViewBean view : userViews) {
			System.out.println("============================");
			System.out.println("Number=:        " + count);
			System.out.println("User ID=:       " + view.getUserId());
			System.out.println("User Name=:     " + view.getUserNm());
			System.out.println("Email Address=: " + view.getMailAddress());
			System.out.println("============================");
			System.out.println("");
			if (count % 2 == 0) {
				selectedUserId.add(view.getUserId());
			}
			count++;
		}
	}

	private void debugSelectedUsers(List<DepartmentUserViewBean> selectedUsers) {
		for (DepartmentUserViewBean selectedUser : selectedUsers) {
			System.out.println("============================");
			System.out.println("User ID=:       " + selectedUser.getUserId());
			System.out.println("User Name=:     " + selectedUser.getUserNm());
			System.out.println("Email Address=: " + selectedUser.getMailAddress());
			System.out.println("============================");
			System.out.println("");
		}
	}
}
