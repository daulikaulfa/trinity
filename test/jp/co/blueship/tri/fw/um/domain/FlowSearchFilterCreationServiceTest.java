package jp.co.blueship.tri.fw.um.domain;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

public class FlowSearchFilterCreationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-Dashboard-Context.xml"};

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);
			//this.edit(service);
			//this.remove(service);
			//this.list(service);

			System.out.println("finish.");

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create( IGenericTransactionService service ) throws Exception {

		//Param
		FlowSearchFilterCreationServiceBean serviceBean = new FlowSearchFilterCreationServiceBean();
		{
			serviceBean.setUserId( getAuthUserId() );
			serviceBean.setUserName( getAuthUserName() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChanges
		{
			FlowChangePropertyListServiceBean listBean = new FlowChangePropertyListServiceBean();
			SearchCondition searchCond = listBean.getParam().getSearchCondition();
			searchCond.setCtgId("Category1");

			serviceBean.getParam().getInputInfo().setLotId( "LOT-1111" );
			serviceBean.getParam().getInputInfo().setFilterNm( "sample1" );
			serviceBean.getParam().getInputInfo().setServiceId( ServiceId.AmChangePropertyListService.value() );
			serviceBean.getParam().getInputInfo().setSearchFilter( searchCond  );

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.UmSearchFilterCreationService.value(),serviceDto);
			assertTrue( serviceBean.getResult().isCompleted() );
		}
	}


}
