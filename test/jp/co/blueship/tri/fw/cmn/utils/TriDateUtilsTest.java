package jp.co.blueship.tri.fw.cmn.utils;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.util.Date;
import java.util.TimeZone;

import org.junit.Ignore;
import org.junit.Test;

public class TriDateUtilsTest {

	@Ignore
	@Test
	public void timeZones() {
		TimeZone tz = TimeZone.getDefault();
		System.out.println("ID:=" + tz.getID());

		int max = 0;
		for ( String id: TimeZone.getAvailableIDs( ) ) {
			if (max < id.length()) {
				max = id.length();
			};
			TimeZone innerTz = TimeZone.getTimeZone(id);
			System.out.println(id + " :" + innerTz.getDisplayName());
		}
		System.out.println("max_size:=" + max);
	}

	@Ignore
	@Test
	public void convertTimestampToDate() {
		final String dt = "2015-11-13 16:12:51.83";
		Timestamp time = Timestamp.valueOf(dt);

		Date date = TriDateUtils.convertTimestampToDate( time );
		assertEquals( "2015/11/13 16:12:51.830", TriDateUtils.getDate(date) );
		System.out.println( TriDateUtils.getDate(date) );
	}

	@Test
	public void getDefaultDateFormatOfTimeZone() {
		TimeZone defaultZone = TriDateUtils.getDefaultTimeZone();
		TimeZone singapore = TriDateUtils.getTimeZone("Asia/Singapore");
		TimeZone japan = TriDateUtils.getTimeZone("Japan");

		Date date = new Date();

		System.out.println( TriDateUtils.getDefaultDateFormat(defaultZone).format( date ) + " (" + defaultZone.getID() + ")" );
		System.out.println( TriDateUtils.getDefaultDateFormat(singapore).format( date ) + " (" + singapore.getID() + ")" );
		System.out.println( TriDateUtils.getDefaultDateFormat(japan).format( date ) + " (" + japan.getID() + ")" );
	}

}
