package jp.co.blueship.tri.fw.cmn.utils.expression;


public class TriResolverMockBean {

	private String id = null;
	private String name = null;

	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
