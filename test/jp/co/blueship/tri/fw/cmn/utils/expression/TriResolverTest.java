package jp.co.blueship.tri.fw.cmn.utils.expression;

import static org.junit.Assert.assertTrue;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.junit.Before;
import org.junit.Test;

public class TriResolverTest {

	@Before
	public void setUp() throws Exception {
		TriResolver resolver = new TriResolver();
		BeanUtilsBean.getInstance().getPropertyUtils().setResolver(resolver);
	}

	@Test
    public void testMapToBean() {
    	TriResolverMockBean dest = new TriResolverMockBean();
    	Map<String, String> src = new HashMap<String, String>();

    	{
    		src.put("id", "ID-0001");
    		src.put("name", "yamada tarou");
    	}


    	try {
			BeanUtils.populate(dest, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		assertTrue( "ID-0001".equals(dest.getId()) );
		assertTrue( "yamada tarou".equals(dest.getName()) );

    }

	@Test
    public void testMapToBean2() {
    	TriResolverMockBean dest = new TriResolverMockBean();
    	Map<String, String> src = new HashMap<String, String>();

    	{
    		src.put("id", "ID-0001");
    		src.put("name", "yamada tarou");
    		src.put("name[]", "yamada tarou");
    		src.put("name[].subName", "yamada tarou");
    	}


    	try {
			BeanUtils.populate(dest, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		assertTrue( "ID-0001".equals(dest.getId()) );
		assertTrue( "yamada tarou".equals(dest.getName()) );

    }

	@Test
    public void testMapToBean3() {
    	TriResolverMockBean dest = new TriResolverMockBean();
    	Map<String, String> src = new HashMap<String, String>();

    	{
    		src.put("id", "ID-0001");
    		src.put("name", "yamada tarou");
    		//src.put("class.classLoader", "");
     	}


    	try {
			BeanUtils.populate(dest, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		assertTrue( "ID-0001".equals(dest.getId()) );
		assertTrue( "yamada tarou".equals(dest.getName()) );

    }

	@Test
    public void testMapToMap() {
    	Map<String, String> dest = new HashMap<String, String>();
    	Map<String, String> src = new HashMap<String, String>();

    	{
    		src.put("id", "ID-0001");
    		src.put("name", "yamada tarou");
    		src.put("name[]", "yamada jirou");
     	}


    	try {
			BeanUtils.populate(dest, src);

		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}

		assertTrue( "ID-0001".equals(dest.get("id")) );
		assertTrue( "yamada tarou".equals(dest.get("name")) );
		assertTrue( "yamada jirou".equals(dest.get("name[]")) );

    }

}
