package jp.co.blueship.tri.fw.cmn.utils.collections;

import java.util.Arrays;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.function.TriClojureEx;

public class TriCollectionUtilsForEachTest {

	private static final TriClojureEx<Input, ClojureTestException> TEST_CLOJURE_EX = new TriClojureEx<TriCollectionUtilsForEachTest.Input, ClojureTestException>() {

		@Override
		public boolean apply(Input input) throws ClojureTestException {

			if (input.getAtter() == 0L) {
				throw new ClojureTestException();
			}

			return true;
		}
	};

	@Test
	public void testForEachEx() throws Exception {

		TriCollectionUtils.forEachEx(Arrays.asList(new Input(1L), new Input(2L)), TEST_CLOJURE_EX);
	}

	@Test(expected = ClojureTestException.class)
	public void testForEachExCaseThrowException() throws Exception {

		TriCollectionUtils.forEachEx(Arrays.asList(new Input(1L), new Input(0L)), TEST_CLOJURE_EX);
	}

	private class Input {

		private Long atter;

		Input(Long atter) {
			this.atter = atter;
		}

		Long getAtter() {
			return atter;
		}

	}
}

class ClojureTestException extends Exception {

	private static final long serialVersionUID = -5686626588253089936L;

	public ClojureTestException() {
	}

}