package jp.co.blueship.tri.fw.cmn.utils.collections;

import static jp.co.blueship.tri.fw.cmn.utils.collections.FluentList.*;
import static jp.co.blueship.tri.fw.cmn.utils.monoid.Monoids.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;

public class FluentListTest {

	@Test
	public void testSum() {

		List<Long> inputList = Arrays.asList(10L, 20L, 30L);
		Long actual = from(inputList).sum(LONG_MONOID);

		assertThat(actual, is(10L + 20L + 30L));
	}

	@Test
	public void testSumCaseIncludingNull() {

		List<Long> inputList = Arrays.asList(10L, null, 30L);
		Long actual = from(inputList).sum(LONG_MONOID);

		assertThat(actual, is(10L + 30L));
	}

	@Test
	public void testMap() {

		Long actual = from(Arrays.asList("10", "20", "30")).map(
				new TriFunction<String, Long>() {

					public Long apply(String input) {
						return Long.valueOf(input);
					}
				}).sum(LONG_MONOID);

		assertThat(actual, is(10L + 20L + 30L));
	}

	@Test
	public void testSort() {

		List<Long> actualList = from(Arrays.asList(10L, 5L, 11L, 1L, 7L)).sort(
				new Comparator<Long>() {

					public int compare(Long data1, Long data2) {
						return data1.compareTo(data2);
					}
				}).asList();

		assertThat(actualList.size(), is(5));
		assertThat(actualList.get(0), is(1L));
		assertThat(actualList.get(1), is(5L));
		assertThat(actualList.get(2), is(7L));
		assertThat(actualList.get(3), is(10L));
		assertThat(actualList.get(4), is(11L));
	}

	@Test
	public void testIsEmpty() {

		assertThat(from(Collections.emptyList()).isEmpty(), is(true));
	}

	@Test
	public void testAtFirst() {

		Long actual = from(Arrays.asList(10L, 20L, 30L)).atFirst();

		assertThat(actual, is(10L));
	}

	@Test
	public void testAtFirstCaseEmpty() {

		Long actual = from(Collections.<Long> emptyList()).atFirst();

		assertThat(actual, is(nullValue()));
	}

	@Test
	public void testLimit() {

		List<Long> actual = from(Arrays.asList(10L, 20L, 30L, 40L, 50L)).limit(
				3).asList();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(3));
		assertThat(actual.get(0), is(10L));
		assertThat(actual.get(1), is(20L));
		assertThat(actual.get(2), is(30L));
	}

	@Test
	public void testLimitLargeSize() {

		List<Long> actual = from(Arrays.asList(10L)).limit(
				3).asList();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat(actual.get(0), is(10L));
	}

	@Test
	public void testLimitSameSize() {

		List<Long> actual = from(Arrays.asList(10L, 20L, 30L)).limit(
				3).asList();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(3));
		assertThat(actual.get(0), is(10L));
		assertThat(actual.get(1), is(20L));
		assertThat(actual.get(2), is(30L));
	}
}
