package jp.co.blueship.tri.fw.cmn.utils.collections;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

/**
 * {@link ComparableSet} クラスの単体テスト
 *
 * @author Takayuki Kubo
 *
 */
public class ComparableSetTest {

	private List<TestElement> list;
	private static final Comparator<TestElement> COMPARATOR = new Comparator<TestElement>() {

		@Override
		public int compare(TestElement elm1, TestElement elm2) {

			if (elm1.getValue().equals(elm2.getValue())) {
				return 0;
			}

			return -1;
		}
	};

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		list = new ArrayList<TestElement>();
		list.add(new TestElement("AAAAA"));
		list.add(new TestElement("BBBBB"));
		list.add(new TestElement("CCCCC"));
		list.add(new TestElement("DDDDD"));
		list.add(new TestElement("EEEEE"));

	}

	/**
	 * {@link ComparableSet#add(Object)}メソッドの単体テスト<br/>
	 * 重複した要素は追加できないことを確認する.
	 */
	@Test
	public void testAddCaseDuplicatedElement() {

		ComparableSet<TestElement> testee = new ComparableSet<ComparableSetTest.TestElement>(COMPARATOR);
		for (TestElement elm : list) {
			testee.add(elm);
		}

		testee.add(new TestElement("AAAAA"));

		assertThat(testee.size(), is(5));
	}

	/**
	 * {@link ComparableSet#add(Object)}メソッドの単体テスト<br/>
	 * 重複した要素が無い場合はすべて追加されることを確認する.
	 */
	@Test
	public void testAddCaseNotDuplicatedElement() {

		ComparableSet<TestElement> testee = new ComparableSet<ComparableSetTest.TestElement>(COMPARATOR);
		for (TestElement elm : list) {
			testee.add(elm);
		}

		assertThat(testee.size(), is(5));

	}

	/**
	 *
	 */
	static class TestElement {

		private String value;

		TestElement(String value) {
			this.value = value;
		}

		String getValue() {
			return value;
		}

	}

}
