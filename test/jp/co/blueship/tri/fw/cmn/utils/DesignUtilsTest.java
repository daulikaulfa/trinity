package jp.co.blueship.tri.fw.cmn.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.fw.msg.MessageParameter;

import org.junit.Test;

/**
 * {@link DesignUtil}のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class DesignUtilsTest {

	/**
	 * {@link DesignUtil#getMessageParameter(String)}のユニットテスト
	 */
	@Test
	public void testGetMessageParameter() {

		assertThat(TriStringUtils.isEmpty(MessageParameter.values()), is(false));

		for (MessageParameter messageParameter : MessageParameter.values()) {
			String actual = DesignUtils.getMessageParameter(messageParameter.getKey());
			assertThat(actual, is(notNullValue()));
		}
	}

}
