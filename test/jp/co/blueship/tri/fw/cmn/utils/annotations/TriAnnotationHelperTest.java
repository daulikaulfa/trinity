package jp.co.blueship.tri.fw.cmn.utils.annotations;

import org.junit.Test;

public class TriAnnotationHelperTest {

	//@Ignore
	@Test
	public void valueOf() {
		System.out.println("start");

		try {
			TestView view = new TestView();
			view.setStsId("Dummy");

			String value = TriAnnotationHelper.valueOf(view, TriSelectedId.class);

			System.out.println("value:=" + value);

		} finally {
			System.out.println("end");
		}
	}

	class TestView {
		private String stsId = null;

		@TriSelectedId
		public String getStsId() {
			return stsId;
		}
		public TestView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
	}

}
