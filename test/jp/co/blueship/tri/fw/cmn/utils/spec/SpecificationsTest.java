package jp.co.blueship.tri.fw.cmn.utils.spec;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class SpecificationsTest {

	@Test
	public void testNotEmptyListCaseTrue() {

		List<String> list = new ArrayList<String>();
		list.add("test");

		assertThat(Specifications.NOT_EMPTY_LIST.isSpecified(list), is(true));
	}

	@Test
	public void testNotEmptyListCaseFalse() {

		List<String> list = new ArrayList<String>();

		assertThat(Specifications.NOT_EMPTY_LIST.isSpecified(list), is(false));
		assertThat(Specifications.NOT_EMPTY_LIST.isSpecified(null), is(false));
	}

	@Test
	public void testHasSingleElementCaseTrue() {

		List<String> list = new ArrayList<String>();
		list.add("test");

		assertThat(Specifications.HAS_SINGLE_ELEMENT.isSpecified(list), is(true));
	}

	@Test
	public void testHasSingleElementCaseFalse() {

		List<String> list = new ArrayList<String>();
		list.add("test1");
		list.add("test2");

		assertThat(Specifications.HAS_SINGLE_ELEMENT.isSpecified(list), is(false));

		list = new ArrayList<String>();
		assertThat(Specifications.HAS_SINGLE_ELEMENT.isSpecified(list), is(false));
		assertThat(Specifications.HAS_SINGLE_ELEMENT.isSpecified(null), is(false));
	}
}
