package jp.co.blueship.tri.fw.cmn.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class EncryptionUtilsTest {
	/**
	 * getHashStringEncodedSha512()の動作確認テスト
	 *
	 */
	@Test
	public void testGetHashStringEncodedSha512() {
		String actual = EncryptionUtils.getHashStringEncodedSha512("ABCDEFG");
		assertThat(actual,is(notNullValue()));
		System.out.println(actual);
	}

}
