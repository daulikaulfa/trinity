package jp.co.blueship.tri.fw.cmn.utils.collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;

public class TriCollectionUtilsCollectTest {

    private static final TriFunction<Input, Long> TO_LONG = new TriFunction<Input, Long>() {

        public Long apply( Input input ) {
            return input.getAtter();
        }
    };

    @Test
    public void testCollectByList() {

        List<Input> input = Arrays.asList(createInput(10L), //
                createInput(20L), createInput(30L));

        List<Long> actual = TriCollectionUtils.collect(input, TO_LONG);
        assertThat(actual.size(), is(3));
        assertThat(actual.get(0), is(10L));
        assertThat(actual.get(1), is(20L));
        assertThat(actual.get(2), is(30L));
    }

    @Test
    public void testCollectByCollection() {

        Collection<Input> input = Arrays.asList(createInput(10L), //
                createInput(20L), createInput(30L));

        Collection<Long> actuals = TriCollectionUtils.collect(input, TO_LONG);
        assertThat(actuals.size(), is(3));
        assertThat(actuals.contains(10L), is(true));
        assertThat(actuals.contains(20L), is(true));
        assertThat(actuals.contains(30L), is(true));

    }

    @Test
    public void testCollectByListWithOutput() {

        List<Input> input = Arrays.asList(createInput(10L), //
                createInput(20L), createInput(30L));

        List<Long> actual = new ArrayList<Long>();

        TriCollectionUtils.collect(input, TO_LONG, actual);
        assertThat(actual.size(), is(3));
        assertThat(actual.get(0), is(10L));
        assertThat(actual.get(1), is(20L));
        assertThat(actual.get(2), is(30L));
    }

    @Test
    public void testCollectByCollectionWithOutput() {

        Collection<Input> input = Arrays.asList(createInput(10L), //
                createInput(20L), createInput(30L));

        Collection<Long> actual = new ArrayList<Long>();

        TriCollectionUtils.collect(input, TO_LONG, actual);
        assertThat(actual.size(), is(3));
        assertThat(actual.contains(10L), is(true));
        assertThat(actual.contains(20L), is(true));
        assertThat(actual.contains(30L), is(true));
    }

    private Input createInput( Long atter ) {
        return new Input(atter);
    }

}

class Input {

    private Long atter;

    Input( Long atter ) {
        this.atter = atter;
    }

    Long getAtter() {
        return atter;
    }

}
