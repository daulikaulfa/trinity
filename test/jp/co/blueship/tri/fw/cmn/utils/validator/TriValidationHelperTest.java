package jp.co.blueship.tri.fw.cmn.utils.validator;

import static jp.co.blueship.tri.fw.msg.MessageParameter.REPOSITORY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.lang.annotation.ElementType;

import javax.validation.ConstraintViolation;

import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link TriValidationHelper}のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class TriValidationHelperTest extends TriMockTestSupport {

	private TriValidationHelper testee;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		testee = new TriValidationHelper();
	}

	@Test
	public void testGetMessageParametersAsStringFrom() {

		String[] actual = testee.getMessageParametersAsStringFrom(createConstraintViolation());

		assertThat(TriStringUtils.isEmpty(actual), is(false));
		assertThat(actual.length, is(1));

		assertThat(actual[0], is(expectedParamValueOn(MessageParameter.REPOSITORY)));
	}

	private String expectedParamValueOn(MessageParameter param) {
		return DesignUtils.getMessageParameter(param.getKey());
	}

	@Test
	public void testGetMessageIDFrom() {

		IMessageId actual = testee.getMessageIDFrom(createConstraintViolation());

		assertThat((SmMessageId) actual, is(SmMessageId.SM004096F));
	}

	/**
	 * {@link TriValidationHelper#getMessageParamValue(jp.co.blueship.tri.fw.msg.IMessageParameter)
	 * のユニットテスト。 指定したメッセージパラメタIDに対応するメッセージパラメタ値が返却されことを確認する。
	 */
	@Test
	public void testMessageParamValue() {

		assertThat(TriStringUtils.isEmpty(MessageParameter.values()), is(false));

		for (MessageParameter messageParameter : MessageParameter.values()) {
			String actual = testee.getMessageParamValue(messageParameter);
			assertThat(actual, is(notNullValue()));
		}
	}

	private ConstraintViolation<TestBean> createConstraintViolation() {

		TestBean bean = new TestBean();
		return ConstraintViolationImpl.forBeanValidation(//
				null,//
				null,//
				TestBean.class,//
				bean,//
				bean,//
				null,//
				PathImpl.createPathFromString("TestBean.testProp"),//
				null,//
				ElementType.FIELD);
	}

	public static class TestBean {

		@NotBlank
		@TriMessage(smMessageID = SmMessageId.SM004096F, args = { REPOSITORY })
		private String testProp;

		public void setTestProp(String testProp) {
			this.testProp = testProp;
		}

		public String getTestProp() {
			return testProp;
		}

	}

}
