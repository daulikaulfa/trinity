package jp.co.blueship.tri.fw.cmn.utils;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class PathBuilderTest {

	@Test
	public void testFrom() {

		String actual = PathBuilder.from("path").buildBySlash();
		assertThat(actual, is("path"));
	}

	@Test
	public void test() {
		String actual = PathBuilder.//
				from("path0").//
				with("\\path1").//
				buildBySlash();
		assertThat(actual, is("path0/path1"));
	}

}
