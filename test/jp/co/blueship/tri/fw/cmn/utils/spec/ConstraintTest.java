package jp.co.blueship.tri.fw.cmn.utils.spec;

import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

import org.junit.Test;

public class ConstraintTest {

	@Test
	public void testValidateCaseTrue() {

		List<String> input = new ArrayList<String>();
		input.add("test");
		Constraint.validate(input, Specifications.NOT_EMPTY_LIST, SmMessageId.SM005001S,null,null);

	}

	@Test
	public void testValidateCaseFalse() {

		List<String> input = new ArrayList<String>();
		try {
			Constraint.validate(input, Specifications.NOT_EMPTY_LIST, SmMessageId.SM005001S,null,null);
			fail("期待した例外が発生しなかった。");
		} catch (TriSystemException e) {
		}

	}

}
