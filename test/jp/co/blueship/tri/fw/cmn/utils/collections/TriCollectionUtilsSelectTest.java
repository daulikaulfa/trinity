package jp.co.blueship.tri.fw.cmn.utils.collections;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;

public class TriCollectionUtilsSelectTest {

    @Test
    public void testSelectCaseList() {

        TriPredicate<Long> predicate = new TriPredicate<Long>() {

            public boolean evalute(Long obj) {

                return obj.equals(777L);
            }
        };

        List<Long> input = Arrays.asList(100L, 777L, 111L, 777L, 222L);

        List<Long> actual = TriCollectionUtils.select(input, predicate);

        assertThat(actual, is(notNullValue()));
        assertThat(actual.size(), is(2));
        assertThat(actual.get(0), is(777L));
        assertThat(actual.get(1), is(777L));

    }

    @Test
    public void testSelectCaseCollection() {

        TriPredicate<Long> predicate = new TriPredicate<Long>() {

            public boolean evalute(Long obj) {

                return obj.equals(777L);
            }
        };

        Collection<Long> input = Arrays.asList(100L, 777L, 111L, 777L, 222L);

        Collection<Long> actual = TriCollectionUtils.select(input, predicate);

        assertThat(actual, is(notNullValue()));
        assertThat(actual.size(), is(2));

        for( Long value : actual ) {
            assertThat(value, is(777L));
        }

    }

    @Test
    public void testSelectRejectedCaseList() {

        TriPredicate<Long> predicate = new TriPredicate<Long>() {

            public boolean evalute(Long obj) {

                return obj.equals(777L);
            }
        };

        List<Long> input = Arrays.asList(100L, 777L, 111L, 777L, 222L);

        List<Long> actual = TriCollectionUtils.selectRejected(input, predicate);

        assertThat(actual, is(notNullValue()));
        assertThat(actual.size(), is(3));
        assertThat(actual.get(0), is(100L));
        assertThat(actual.get(1), is(111L));
        assertThat(actual.get(2), is(222L));

    }

    @Test
    public void testSelectRejectedCaseCollection() {

        TriPredicate<Long> predicate = new TriPredicate<Long>() {

            public boolean evalute(Long obj) {

                return obj.equals(777L);
            }
        };

        Collection<Long> input = Arrays.asList(100L, 777L, 111L, 777L, 222L);

        Collection<Long> actual = TriCollectionUtils.selectRejected(input, predicate);

        assertThat(actual, is(notNullValue()));
        assertThat(actual.size(), is(3));

    }
}
