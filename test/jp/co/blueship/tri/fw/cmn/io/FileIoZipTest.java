package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.util.HashSet;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;

import org.junit.Test;

public class FileIoZipTest {
	/**
	 * getHashStringEncodedSha512()の動作確認テスト
	 *
	 */
	@Test
	public void testUnCompressZip() {
		String masterPath = "C:/user/atf.zip";
		String srcPath = "C:/user/atf2.zip";
		String dstPath = "C:/user/test";

		for (int i = 0; i < 10; i++) {
			try {
				TriFileUtils.copyFileToFile(new File(masterPath), new File(srcPath));
				FileIoZip.UnCompressZip(srcPath, dstPath, Charset.WINDOWS_31J, new HashSet<String>());
				TriFileUtils.delete(new File(srcPath));
				TriFileUtils.delete(new File(dstPath+ "/atf"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

}
