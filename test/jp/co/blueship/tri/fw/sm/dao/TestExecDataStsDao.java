package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.IExecDataStsDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsCondition;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsEntity;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.IExecDataStsEntity;

import org.junit.Test;

public class TestExecDataStsDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		IExecDataStsEntity entity = new ExecDataStsEntity();
		entity.setProcId("proc_T1");
		entity.setDataCtgCd("data_ctg_cd_T1");
		entity.setDataId("data_T1");
		entity.setStsId("sts_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		List<IExecDataStsEntity> list = new ArrayList<IExecDataStsEntity>();
		{
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setProcId("proc_T2");
			entity.setDataCtgCd("data_ctg_cd_T2");
			entity.setDataId("data_T2");
			entity.setStsId("sts_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setProcId("proc_T3");
			entity.setDataCtgCd("data_ctg_cd_T3");
			entity.setDataId("data_T3");
			entity.setStsId("sts_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		ExecDataStsCondition condition = new ExecDataStsCondition();
		condition.setProcId( "proc_T1" );
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );
		IExecDataStsEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getProcId() + ":" + entity.getDataCtgCd() + ":" + entity.getDataId() + ":" + entity.getStsId() );

	}
	//@Ignore
	@Test
	public void find() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		ExecDataStsCondition condition = new ExecDataStsCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(ExecDataStsItems.procId, TriSortOrder.Desc, 1);

		IEntityLimit<IExecDataStsEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setProcIds( new String[] {"proc_T1", "proc_T2"} );
		List<IExecDataStsEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( IExecDataStsEntity entity: entities ) {
			System.out.println( entity.getProcId() + ":" + entity.getDataCtgCd() + ":" + entity.getDataId() + ":" + entity.getStsId() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		IExecDataStsEntity entity = new ExecDataStsEntity();
		entity.setProcId("proc_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		List<IExecDataStsEntity> list = new ArrayList<IExecDataStsEntity>();

		{
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setProcId("proc_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IExecDataStsEntity entity = new ExecDataStsEntity();
			entity.setProcId("proc_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IExecDataStsDao dao = (IExecDataStsDao)this.getBean( "smExecDataStsDao" );

		ExecDataStsCondition condition = new ExecDataStsCondition();

		condition.setProcId("proc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
