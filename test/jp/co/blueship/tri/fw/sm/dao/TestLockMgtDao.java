package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.ILockMgtDao;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.ILockMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.lockmgt.eb.LockMgtEntity;

import org.junit.Ignore;
import org.junit.Test;

public class TestLockMgtDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		ILockMgtEntity entity = new LockMgtEntity();
		entity.setProcId("proc_T1");
		entity.setLockSvcId("locksvc_T1");
		entity.setSvcId("svc_T1");
		entity.setUserId("blueship_T1");
		entity.setLotId("lotid_T1");
		entity.setLockForLot(StatusFlg.off);
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		List<ILockMgtEntity> list = new ArrayList<ILockMgtEntity>();
		{
			ILockMgtEntity entity = new LockMgtEntity();
			entity.setProcId("proc_T2");
			entity.setLockSvcId("locksvc_T2");
			entity.setSvcId("svc_T2");
			entity.setUserId("blueship_T2");
			entity.setLotId("lotid_T2");
			entity.setLockForLot(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			ILockMgtEntity entity = new LockMgtEntity();
			entity.setProcId("proc_T3");
			entity.setLockSvcId("locksvc_T3");
			entity.setSvcId("svc_T3");
			entity.setUserId("blueship_T3");
			entity.setLotId("lotid_T3");
			entity.setLockForLot(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	@Ignore
	@Test
	public void findByPrimaryKey() {
		LockMgtCondition condition = new LockMgtCondition();
		condition.setProcId( "proc_T1" );
		condition.setLockSvcId( "locksvc_T1" );
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );
		ILockMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getProcId() + ":" + entity.getLockSvcId() + ":" + entity.getSvcId() + ":" + entity.getUserId() );

	}
//	@Test
//	public void find() {
//		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );
//		List<ILockMgtEntity> entities = dao.lock( "Svc001" );
//		assertTrue( null != entities );
//		assertTrue( 0 < entities.size());
//
//		for( ILockMgtEntity entity: entities ) {
//			System.out.println( entity.getProcId() + ":" + entity.getLockSvcId() + ":" + entity.getSvcId() + ":" + entity.getUserId() );
//		}
//
//	}
/*
	@Ignore
	@Test
	public void find() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		LockMgtCondition condition = new LockMgtCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(LockMgtItems.procId, TriSortOrder.Desc, 1);

		IEntityLimit<ILockMgtEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setProcIds( new String[] {"proc_T1", "proc_T2"} );
		condition.setLockSvcIds( new String[] {"locksvc_T1", "locksvc_T2"} );
		List<ILockMgtEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( ILockMgtEntity entity: entities ) {
			System.out.println( entity.getProcId() + ":" + entity.getLockSvcId() + ":" + entity.getSvcId() + ":" + entity.getUserId() );
		}

	}
*/
	@Ignore
	@Test
	public void update() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		ILockMgtEntity entity = new LockMgtEntity();
		entity.setProcId("proc_T1");
		entity.setLockForLot(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		List<ILockMgtEntity> list = new ArrayList<ILockMgtEntity>();

		{
			ILockMgtEntity entity = new LockMgtEntity();
			entity.setProcId("proc_T2");
			entity.setLockForLot(StatusFlg.on);

			list.add( entity );
		}

		{
			ILockMgtEntity entity = new LockMgtEntity();
			entity.setProcId("proc_T3");
			entity.setLockForLot(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		ILockMgtDao dao = (ILockMgtDao)this.getBean( "smLockMgtDao" );

		LockMgtCondition condition = new LockMgtCondition();

		condition.setProcId("proc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
