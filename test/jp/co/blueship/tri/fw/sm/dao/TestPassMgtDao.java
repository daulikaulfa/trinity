package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.passmgt.IPassMgtDao;
import jp.co.blueship.tri.fw.sm.dao.passmgt.constants.PassMgtItems;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.PassMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.PassMgtEntity;

public class TestPassMgtDao extends TestTriJdbcDaoSupport {
	@Ignore
	@Test
	public void insert() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		IPassMgtEntity entity = new PassMgtEntity();
		entity.setPassCtgCd("pass_ctg_cd_T1");
		entity.setHostNm("192.168.0.1");
		entity.setUserId("blueship_T1");
		entity.setEpass("epass_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchInsert() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		List<IPassMgtEntity> list = new ArrayList<IPassMgtEntity>();
		{
			IPassMgtEntity entity = new PassMgtEntity();
			entity.setPassCtgCd("pass_ctg_cd_T2");
			entity.setHostNm("192.168.0.2");
			entity.setUserId("blueship_T2");
			entity.setEpass("epass_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IPassMgtEntity entity = new PassMgtEntity();
			entity.setPassCtgCd("pass_ctg_cd_T3");
			entity.setHostNm("192.168.0.3");
			entity.setUserId("blueship_T3");
			entity.setEpass("epass_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		PassMgtCondition condition = new PassMgtCondition();
		condition.setPassCtgCd( "svn" );
		condition.setHostNm( "TRI_CONTROLLER" );
		condition.setUserId( "trinity" );
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );
		IPassMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getPassCtgCd() + ":" + entity.getHostNm() + ":" + entity.getUserId() + ":" + entity.getEpass() );

	}
	@Ignore
	@Test
	public void find() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		PassMgtCondition condition = new PassMgtCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(PassMgtItems.passCtgCd, TriSortOrder.Desc, 1);

		IEntityLimit<IPassMgtEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setPassCtgCd( "pass_ctg_cd_T2" );
		List<IPassMgtEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 1 == entities.size());

		for( IPassMgtEntity entity: entities ) {
			System.out.println( entity.getPassCtgCd() + ":" + entity.getHostNm() + ":" + entity.getUserId() + ":" + entity.getEpass() );
		}

	}
	@Ignore
	@Test
	public void update() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		IPassMgtEntity entity = new PassMgtEntity();
		entity.setPassCtgCd("pass_ctg_cd_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	@Ignore
	@Test
	public void batchUpdate() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		List<IPassMgtEntity> list = new ArrayList<IPassMgtEntity>();

		{
			IPassMgtEntity entity = new PassMgtEntity();
			entity.setPassCtgCd("pass_ctg_cd_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IPassMgtEntity entity = new PassMgtEntity();
			entity.setPassCtgCd("pass_ctg_cd_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	@Ignore
	@Test
	public void delete() {
		IPassMgtDao dao = (IPassMgtDao)this.getBean( "smPassMgtDao" );

		PassMgtCondition condition = new PassMgtCondition();

		condition.setPassCtgCd("pass_ctg_cd_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setPassCtgCd("pass_ctg_cd_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setPassCtgCd("pass_ctg_cd_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
