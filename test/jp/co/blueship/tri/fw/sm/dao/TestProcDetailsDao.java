package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcDetailsItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsCondition;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsEntity;

import org.junit.Test;

public class TestProcDetailsDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		IProcDetailsEntity entity = new ProcDetailsEntity();
		entity.setProcId("proc_T1");
		entity.setProcCtgCd("ctg");
		entity.setStsId("sts_T1");
		entity.setMsgId("msg_T1");
		entity.setCompStsId(StatusFlg.off);
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		List<IProcDetailsEntity> list = new ArrayList<IProcDetailsEntity>();
		{
			IProcDetailsEntity entity = new ProcDetailsEntity();
			entity.setProcId("proc_T2");
			entity.setProcCtgCd("ctg");
			entity.setStsId("sts_T2");
			entity.setMsgId("msg_T2");
			entity.setCompStsId(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IProcDetailsEntity entity = new ProcDetailsEntity();
			entity.setProcId("proc_T3");
			entity.setProcCtgCd("ctg");
			entity.setStsId("sts_T3");
			entity.setMsgId("msg_T3");
			entity.setCompStsId(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		ProcDetailsCondition condition = new ProcDetailsCondition();
		condition.setProcId("proc_T1");
		condition.setProcCtgCd("ctg");
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );
		IProcDetailsEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getProcId() + ":" + entity.getProcStTimestamp() + ":" + entity.getStsId() );

	}
	//@Ignore
	@Test
	public void find() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		ProcDetailsCondition condition = new ProcDetailsCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcDetailsItems.procId, TriSortOrder.Desc, 1);

		IEntityLimit<IProcDetailsEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setProcCtgCd("ctg");
		List<IProcDetailsEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 3 == entities.size());

		for( IProcDetailsEntity entity: entities ) {
			System.out.println( entity.getProcId() + ":" + entity.getProcStTimestamp() + ":" + entity.getStsId() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		IProcDetailsEntity entity = new ProcDetailsEntity();
		entity.setProcId("proc_T1");
		entity.setCompStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		List<IProcDetailsEntity> list = new ArrayList<IProcDetailsEntity>();

		{
			IProcDetailsEntity entity = new ProcDetailsEntity();
			entity.setProcId("proc_T2");
			entity.setCompStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IProcDetailsEntity entity = new ProcDetailsEntity();
			entity.setProcId("proc_T3");
			entity.setCompStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IProcDetailsDao dao = (IProcDetailsDao)this.getBean( "smProcDetailsDao" );

		ProcDetailsCondition condition = new ProcDetailsCondition();

		condition.setProcId("proc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
