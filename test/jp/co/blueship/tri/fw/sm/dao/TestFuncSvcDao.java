package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.IFuncSvcDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.constants.FuncSvcItems;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcEntity;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;

import org.junit.Test;

public class TestFuncSvcDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		IFuncSvcEntity entity = new FuncSvcEntity();
		entity.setSvcId("svc_T1");
		entity.setSvcNm("Svc_T1");
		entity.setSvcContent("Svc Content T1");
		entity.setIsConcurrent(StatusFlg.off);
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		List<IFuncSvcEntity> list = new ArrayList<IFuncSvcEntity>();
		{
			IFuncSvcEntity entity = new FuncSvcEntity();
			entity.setSvcId("svc_T2");
			entity.setSvcNm("Svc_T2");
			entity.setSvcContent("Svc Content T2");
			entity.setIsConcurrent(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IFuncSvcEntity entity = new FuncSvcEntity();
			entity.setSvcId("svc_T3");
			entity.setSvcNm("Svc_T3");
			entity.setSvcContent("Svc Content T3");
			entity.setIsConcurrent(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		FuncSvcCondition condition = new FuncSvcCondition();
		condition.setSvcId( "svc_T1" );
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );
		IFuncSvcEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getSvcNm() );

	}
	//@Ignore
	@Test
	public void find() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		FuncSvcCondition condition = new FuncSvcCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(FuncSvcItems.svcId, TriSortOrder.Desc, 1);

		IEntityLimit<IFuncSvcEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setSvcIds( new String[] {"svc_T1", "svc_T2"} );
		List<IFuncSvcEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( IFuncSvcEntity entity: entities ) {
			System.out.println( entity.getSvcId() + ":" + entity.getSvcNm() + ":" + entity.getIsConcurrent() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		IFuncSvcEntity entity = new FuncSvcEntity();
		entity.setSvcId("svc_T1");
		entity.setIsConcurrent(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		List<IFuncSvcEntity> list = new ArrayList<IFuncSvcEntity>();

		{
			IFuncSvcEntity entity = new FuncSvcEntity();
			entity.setSvcId("svc_T2");
			entity.setIsConcurrent(StatusFlg.on);

			list.add( entity );
		}

		{
			IFuncSvcEntity entity = new FuncSvcEntity();
			entity.setSvcId("svc_T3");
			entity.setIsConcurrent(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IFuncSvcDao dao = (IFuncSvcDao)this.getBean( "smFuncSvcDao" );

		FuncSvcCondition condition = new FuncSvcCondition();

		condition.setSvcId("svc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setSvcId("svc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
