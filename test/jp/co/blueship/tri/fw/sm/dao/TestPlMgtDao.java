package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.plmgt.IPlMgtDao;
import jp.co.blueship.tri.fw.sm.dao.plmgt.constants.PlMgtItems;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.PlMgtEntity;

import org.junit.Test;

public class TestPlMgtDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		IPlMgtEntity entity = new PlMgtEntity();
		entity.setProductId("product_T1");
		entity.setCpNm("host_cp_T1");
		entity.setSerialNo("0123456789_T1");
		entity.setPlKey("plkey_T1");
		entity.setAgentPlKey("agent_T1");
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		List<IPlMgtEntity> list = new ArrayList<IPlMgtEntity>();
		{
			IPlMgtEntity entity = new PlMgtEntity();
			entity.setProductId("product_T2");
			entity.setCpNm("host_cp_T2");
			entity.setSerialNo("0123456789_T2");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IPlMgtEntity entity = new PlMgtEntity();
			entity.setProductId("product_T3");
			entity.setCpNm("host_cp_T3");
			entity.setSerialNo("0123456789_T3");
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		PlMgtCondition condition = new PlMgtCondition();
		condition.setProductId( "product_T1" );
		condition.setCpNm( "host_cp_T1" );
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );
		IPlMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getProductId() + ":" + entity.getCpNm() + ":" + entity.getSerialNo() + ":" + entity.getPlKey() );

	}
	//@Ignore
	@Test
	public void find() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		PlMgtCondition condition = new PlMgtCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(PlMgtItems.productId, TriSortOrder.Desc, 1);

		IEntityLimit<IPlMgtEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setProductId( "product_T2" );
		List<IPlMgtEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 1 == entities.size());

		for( IPlMgtEntity entity: entities ) {
			System.out.println( entity.getProductId() + ":" + entity.getCpNm() + ":" + entity.getSerialNo() + ":" + entity.getPlKey() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		IPlMgtEntity entity = new PlMgtEntity();
		entity.setProductId( "product_T1" );
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		List<IPlMgtEntity> list = new ArrayList<IPlMgtEntity>();

		{
			IPlMgtEntity entity = new PlMgtEntity();
			entity.setProductId( "product_T2" );
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IPlMgtEntity entity = new PlMgtEntity();
			entity.setProductId( "product_T3" );
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IPlMgtDao dao = (IPlMgtDao)this.getBean( "smPlMgtDao" );

		PlMgtCondition condition = new PlMgtCondition();

		condition.setProductId( "product_T1" );
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProductId( "product_T2" );
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProductId( "product_T3" );
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
