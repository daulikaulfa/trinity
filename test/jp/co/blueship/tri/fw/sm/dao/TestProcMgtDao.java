package jp.co.blueship.tri.fw.sm.dao;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcMgtDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtCondition;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtEntity;

import org.junit.Test;

public class TestProcMgtDao extends TestTriJdbcDaoSupport {
	//@Ignore
	@Test
	public void insert() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		IProcMgtEntity entity = new ProcMgtEntity();
		entity.setProcId("proc_T1");
		entity.setSvcId("svc_T1");
		entity.setLotId("lot_T1");
		entity.setUserId("blueship_T1");
		entity.setUserNm("Blueship_T1");
		entity.setStsId("sts_T1");
		entity.setCompStsId(StatusFlg.off);
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchInsert() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		List<IProcMgtEntity> list = new ArrayList<IProcMgtEntity>();
		{
			IProcMgtEntity entity = new ProcMgtEntity();
			entity.setProcId("proc_T2");
			entity.setSvcId("svc_T2");
			entity.setLotId("lot_T2");
			entity.setUserId("blueship_T2");
			entity.setUserNm("Blueship_T2");
			entity.setStsId("sts_T2");
			entity.setCompStsId(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		{
			IProcMgtEntity entity = new ProcMgtEntity();
			entity.setProcId("proc_T3");
			entity.setSvcId("svc_T3");
			entity.setLotId("lot_T3");
			entity.setUserId("blueship_T3");
			entity.setUserNm("Blueship_T3");
			entity.setStsId("sts_T3");
			entity.setCompStsId(StatusFlg.off);
			entity.setDelStsId(StatusFlg.off);

			list.add( entity );
		}

		int[] count = dao.insert( list );

		assertTrue( 2 == count.length );

	}
	//@Ignore
	@Test
	public void findByPrimaryKey() {
		ProcMgtCondition condition = new ProcMgtCondition();
		condition.setProcId( "proc_T1" );
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );
		IProcMgtEntity entity = dao.findByPrimaryKey( condition.getCondition() );

		assertTrue( null != entity );

		System.out.println( entity.getUserId() );

	}
	//@Ignore
	@Test
	public void find() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		ProcMgtCondition condition = new ProcMgtCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcMgtItems.procId, TriSortOrder.Desc, 1);

		IEntityLimit<IProcMgtEntity> home = dao.find( condition.getCondition(), sort, 1, 1 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		condition.setProcIds( new String[] {"proc_T1", "proc_T2"} );
		List<IProcMgtEntity> entities = dao.find( condition.getCondition(), sort );
		assertTrue( 2 == entities.size());

		for( IProcMgtEntity entity: entities ) {
			System.out.println( entity.getProcId() + ":" + entity.getUserId() + ":" + entity.getSvcId() + ":" + entity.getCompStsId() );
		}

	}
	//@Ignore
	@Test
	public void update() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		IProcMgtEntity entity = new ProcMgtEntity();
		entity.setProcId("proc_T1");
		entity.setDelStsId(StatusFlg.on);

		int count = dao.update( entity );

		assertTrue( 1 == count );

	}
	//@Ignore
	@Test
	public void batchUpdate() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		List<IProcMgtEntity> list = new ArrayList<IProcMgtEntity>();

		{
			IProcMgtEntity entity = new ProcMgtEntity();
			entity.setProcId("proc_T2");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		{
			IProcMgtEntity entity = new ProcMgtEntity();
			entity.setProcId("proc_T3");
			entity.setDelStsId(StatusFlg.on);

			list.add( entity );
		}

		int[] count = dao.update( list );

		assertTrue( 2 == count.length );

	}

	//@Ignore
	@Test
	public void delete() {
		IProcMgtDao dao = (IProcMgtDao)this.getBean( "smProcMgtDao" );

		ProcMgtCondition condition = new ProcMgtCondition();

		condition.setProcId("proc_T1");
		int count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T2");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

		condition.setProcId("proc_T3");
		count = dao.delete( condition.getCondition() );
		assertTrue( 1 == count );

	}


}
