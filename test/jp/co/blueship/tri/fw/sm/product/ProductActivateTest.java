package jp.co.blueship.tri.fw.sm.product;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.Set;

import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

import org.junit.Test;

/**
 * ProductActivateのTestコード
 *
 *
 * @author ono
 *
 */
public class ProductActivateTest {

	/**
	 *
	 * getMACAddressをテストする。 結果は空でなく、一件取得、Nullではない。
	 * MACアドレスが表示されること
	 */
	@Test
	public void testGetMACAddress() {

		Set<String> actual = ProductActivate.getMACAddress();
		assertThat(actual.isEmpty(), is(false));

		assertThat(actual.size(), is(1));

		for (String elem : actual) {
			assertThat(elem, is(notNullValue()));
		}

		Object[] array = actual.toArray();
		System.out.println((String) array[0]);
		assertThat( (String)array[0] , is(notNullValue()));
	}
	/**
	 *
	 * getMACAddressStringをテストする。 結果は空でなく、一件取得、Nullではない。
	 * MACアドレスが表示されること
	 */
	@Test
	public void testGetMACAddressString(){
		String actual = ProductActivate.getMACAddressString();
		assertThat(actual.isEmpty(),is(false));
		assertThat(actual,is(notNullValue()));
		System.out.println(actual);
	}
}
