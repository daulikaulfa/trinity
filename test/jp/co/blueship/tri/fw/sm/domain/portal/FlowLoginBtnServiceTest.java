package jp.co.blueship.tri.fw.sm.domain.portal;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowLoginBtnServiceBean;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

public class FlowLoginBtnServiceTest extends TriMockTestSupport {

	private FlowLoginBtnService testee;

	private IUmFinderSupport mockUmFinderSupport;
	private IDesignSheet mockDesignSheet;

	private static final String USER_ID = "UserId";
	private static final String USER_NAME = "UserName";
	private static final String PROJECT_NAME = "ProjectName";
	private static final String VERSION = "Version";

	@Before
	public void setUp() {

		testee = new FlowLoginBtnService();
		mockDesignSheet = createMock(IDesignSheet.class);
		mockUmFinderSupport = createMock(IUmFinderSupport.class);

		setMockDesignSheet(mockDesignSheet);
		testee.setUmFinderSupport(mockUmFinderSupport);

	}

	@Test
	public void testExecute() throws Exception {

		expectCallsOnFindUserByUserId(createUserEntity());
		expectCallsOnIDesignSheet();

		replayAll();

		IServiceDto<FlowLoginBtnServiceBean> inputDto = createInputDto();
		testee.execute(inputDto);

		verifyAll();

		FlowLoginBtnServiceBean actualParam = inputDto.getServiceBean();

		assertThat(TriCollectionUtils.isNotEmpty(actualParam.getInfoCommentIdList()), is(true));
		assertThat(actualParam.getInfoCommentIdList().size(), is(1));
		assertThat((SmMessageId) actualParam.getInfoCommentIdList().get(0), is(SmMessageId.SM003010I));

		assertThat(TriCollectionUtils.isNotEmpty(actualParam.getInfoCommentArgsList()), is(true));
		assertThat(actualParam.getInfoCommentArgsList().size(), is(1));

		assertThat(actualParam.getUserName(), is(USER_NAME));
		assertThat(actualParam.getProjectName(), is(PROJECT_NAME));
		assertThat(actualParam.getVersion(), is(VERSION));
		assertThat(actualParam.isPasswordEffect(), is(true));

	}

	private IServiceDto<FlowLoginBtnServiceBean> createInputDto() {

		IServiceDto<FlowLoginBtnServiceBean> result = new ServiceDto<FlowLoginBtnServiceBean>();
		FlowLoginBtnServiceBean inputParam = new FlowLoginBtnServiceBean();
		inputParam.setUserId(USER_ID);
		inputParam.setUserName(USER_NAME);
		result.setServiceBean(inputParam);

		return result;
	}

	private void expectCallsOnFindUserByUserId(IUserEntity entity) {

		mockUmFinderSupport.findUserByUserId(USER_ID);
		expectLastCall().andReturn(entity);
	}

	private void expectCallsOnIDesignSheet() {

		mockDesignSheet.getValue(UmDesignEntryKeyByCommon.projectName);
		expectLastCall().andReturn(PROJECT_NAME);

		mockDesignSheet.getValue(UmDesignEntryKeyByCommon.version);
		expectLastCall().andReturn(VERSION);
	}

	private IUserEntity createUserEntity() throws Exception {

		IUserEntity result = new UserEntity();
		result.setPassTimeLimit(TriDateUtils.convertStringToTimestampWithDateTime("2050/12/31 12:00:00"));
		result.setUserNm(USER_NAME);

		return result;
	}


}
