package jp.co.blueship.tri.fw.sm.domain.portal;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.sm.domain.portal.dto.FlowLoginBtnServiceBean;
import jp.co.blueship.tri.test.support.TriWithDataBaseTestCase;

import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import org.xml.sax.InputSource;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Fw-Module-Context.xml", "classpath:Rel-LoginService-Context.xml", "classpath:Test-DataSource-Context.xml" })
@TransactionConfiguration
@Transactional
public class FlowLoginBtnWithAgentCheckServiceConnectivityTest extends TriWithDataBaseTestCase {

	@Autowired
	@Qualifier("flowLoginBtnService")
	private FlowLoginBtnWithAgentCheckService testee;

	private static final String USER_ID = "TestUser";
	private static final String USER_NAME = "トリニティ";

	@Before
	public void setUp() throws Exception {
		super.setUp();
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new FlatXmlDataSetBuilder().build(new InputSource("testresources\\data\\FlowLoginBtnWithAgentCheckServiceConnectivityTest.xml"));
	}

	/**
	 * Agentライセンス数の取得を伴ったログインサービスのテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合
	 */
	@Test
	public void testExecute() throws Exception {

		IServiceDto<FlowLoginBtnServiceBean> inputServiceDto = createInputDto();
		testee.execute(inputServiceDto);

		assertThat(inputServiceDto.getServiceBean().getNeedLicenseKeyCount(), is(3));
	}

	private IServiceDto<FlowLoginBtnServiceBean> createInputDto() {

		IServiceDto<FlowLoginBtnServiceBean> result = new ServiceDto<FlowLoginBtnServiceBean>();
		FlowLoginBtnServiceBean inputParam = new FlowLoginBtnServiceBean();
		inputParam.setUserId(USER_ID);
		inputParam.setUserName(USER_NAME);
		result.setServiceBean(inputParam);

		return result;
	}

	@Override
	protected String getAuthUserID() {
		return USER_ID;
	}

}
