package jp.co.blueship.tri.fw.domain;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.junit.Before;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtCondition;
import jp.co.blueship.tri.fw.sm.support.SmFinderSupport;

/**
 * DAO(JDBC) Framework Operation check
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public abstract class TestDomainSupport {
	private Contexts ac;
	private Properties props;

	@Before
	public void setUp() throws Exception {
		ac = Contexts.getInstance();
		props = this.getProperty( "./conf/user.properties" );

		Contexts.getInstance().initBeanFactory( getConfigLocations() );

		TriAuthenticationContext authContext = (TriAuthenticationContext) getBean(TriAuthenticationContext.BEAN_NAME);
		authContext.setAuthUser( getAuthUserId(), getAuthUserName() );

	}

	public final Object getBean( String beanName ) {
		return ac.getBeanFactory().getBean( beanName );
	}

	protected abstract String[] getConfigLocations();

	public String getAuthUserId() {
		return props.getProperty("userid");
	}

	public String getAuthUserName() {
		return props.getProperty("user_nm");
	}

	public String getLotId() {
		return props.getProperty("lotId");
	}

	public void threadWait( IGeneralServiceBean paramBean ) {
		SmFinderSupport support = (SmFinderSupport)this.getBean( "smFinderSupport" );

		ProcMgtCondition condition = new ProcMgtCondition();
		condition.setProcId( paramBean.getProcId() );
		condition.setCompStsId( StatusFlg.on );
		condition.setDelStsId( null );

		boolean isWait = true;

		while ( isWait ) {
			int count = support.getProcMgtDao().count( condition.getCondition() );
			if ( 1 == count )
				break;

			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}

	/**
	 * Propertiesを取得します。
	 *
	 * @param filePath property file path
	 * @throws IOException
	 */
	public Properties getProperty( String filePath ) throws IOException {

		Properties props = new Properties();

		InputStream inputStream = null;
		InputStreamReader inputStreamReader = null;

		try {
			 inputStream = TestDomainSupport.class.getClassLoader().getResourceAsStream(filePath);
			 inputStreamReader = new InputStreamReader(inputStream, Charset.UTF_8.value());
	         props.load(inputStreamReader);
		} finally {
			if ( null != inputStream ) {
				inputStream.close();
			}
			if (null != inputStreamReader ) {
				inputStreamReader.close();
			}
		}

		return props;
	}

}
