package jp.co.blueship.tri.fw.ui;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import javax.servlet.http.HttpServletRequest;

import org.easymock.Capture;
import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link BaseStrutsAction}のユニットテストクラス。
 *
 * @version V3L10.02
 * @author Takayuki Kubo
 *
 */
public class BaseStrutsActionPopulateErrorMessageToRequestTest extends TriMockTestSupport {

	private static final String TEST_MESSAGE1 = "Test Message Hoge";
	private static final String[] EMPTY_ARGS = new String[] {};

	private MessageManager mockMessageManager;
	private Capture<SCRE0000ResponseBean> capturedResponseBean;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		mockMessageManager = createMock(MessageManager.class);
		addMockBeanByName(MessageManager.BEAN_NAME, mockMessageManager);

		capturedResponseBean = new Capture<SCRE0000ResponseBean>();
	}

	/**
	 * {@link BaseStrutsAction#populateErrorMessageToRequest(Throwable, javax.servlet.http.HttpServletRequest)}
	 * のユニットテスト<br/>
	 * サービス層からメッセージIDを保持した例外がスローされた場合レスポンス情報にメッセージが設定されることを確認する。
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testPopulateErrorMessageToRequestCaseTranslatableException() throws Exception {

		mockMessageManager.getMessage("jp", SmMessageId.SM001002E, EMPTY_ARGS);
		expectLastCall().andReturn(TEST_MESSAGE1);

		Throwable input = new TriSystemException(SmMessageId.SM001002E, EMPTY_ARGS);
		HttpServletRequest request = createMockHttpServletRequest();

		replayAll();
		BaseStrutsAction.populateErrorMessageToRequest(input, request);
		verifyAll();

		SCRE0000ResponseBean actual = capturedResponseBean.getValue();
		assertThat(TriCollectionUtils.isNotEmpty(actual.getMessageList()), is(true));
		assertThat(actual.getMessageList().size(), is(1));
		assertThat(actual.getMessageList().get(0), is(TEST_MESSAGE1));
	}

	/**
	 * {@link BaseStrutsAction#populateErrorMessageToRequest(Throwable, javax.servlet.http.HttpServletRequest)}
	 * のユニットテスト<br/>
	 * サービス層からtrinity以外の例外がスローされた場合、trinityのシステム例外にラッピングされ、<br/>
	 * レポンス情報にSM001019Eのメッセージが設定されることを確認する。
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testPopulateErrorMessageToRequestCaseNoTranslatableException() throws Exception {

		//mockMessageManager.getMessage("jp", SmMessageId.SM001019E, null);
		expectLastCall().andReturn(TEST_MESSAGE1);

		Throwable input = new IllegalArgumentException();
		HttpServletRequest request = createMockHttpServletRequest();

		replayAll();
		BaseStrutsAction.populateErrorMessageToRequest(input, request);
		verifyAll();

		SCRE0000ResponseBean actual = capturedResponseBean.getValue();
		assertThat(TriCollectionUtils.isNotEmpty(actual.getMessageList()), is(true));
		assertThat(actual.getMessageList().size(), is(1));
		assertThat(actual.getMessageList().get(0), is(TEST_MESSAGE1));
	}

	private HttpServletRequest createMockHttpServletRequest() {

		HttpServletRequest result = createMock(HttpServletRequest.class);
		result.setAttribute(eq("responseBean"), capture(capturedResponseBean));
		expectLastCall();

		return result;
	}

}
