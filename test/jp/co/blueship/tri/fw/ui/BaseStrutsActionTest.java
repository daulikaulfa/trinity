package jp.co.blueship.tri.fw.ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * {@link BaseStrutsAction}のテストスイートクラス。
 *
 * @author Takayuki Kubo
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ BaseStrutsActionExecuteTest.class, BaseStrutsActionPopulateErrorMessageToRequestTest.class })
public class BaseStrutsActionTest {
}
