package jp.co.blueship.tri.fw.ui;

import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import java.io.InputStream;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts.AppendFileEnum;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.upload.FormFile;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link BaseStrutsAction#execute(HttpServletRequest, javax.servlet.http.HttpServletResponse)}
 * のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class BaseStrutsActionExecuteTest extends TriMockTestSupport {

	private static final String TEST_MESSAGE1 = "Test Message Hoge";
	private static final String[] EMPTY_ARGS = new String[] {};
	private static final String ATTR_NAME_RESPONSE_BEAN = "responseBean";
	private static final String FORWARD_NAME = "ForwardName";
	private static final String ERROR_BUSSINESS = "error_bussiness";
	private static final String FILE_NAME = "fileName";
	private static final Integer FILE_SIZE = 100;

	private BaseStrutsAction partialTestee;
	private MessageManager mockMessageManager;
	private HttpServletRequest mockHttpServletRequest;
	private HttpSession mockHttpSession;
	private ServletContext mockServletContext;
	private ActionMapping mockActionMapping;
	private ActionForward actionForward;
	private InputStream mockInputStream;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		partialTestee = createMockBuilder(BaseStrutsAction.class).//
				addMockedMethod("execute", IApplicationInfo.class, ISessionInfo.class, IRequestInfo.class).//
				createMock();

		mockMessageManager = createMock(MessageManager.class);
		addMockBeanByName(MessageManager.BEAN_NAME, mockMessageManager);

		mockServletContext = createMock(ServletContext.class);
		mockHttpSession = createMock(HttpSession.class);
		mockHttpServletRequest = createMock(HttpServletRequest.class);
		mockActionMapping = createMock(ActionMapping.class);
		actionForward = new ActionForward();

	}

	/**
	 * {@link BaseStrutsAction#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecute() throws Exception {

		TriBaseActionForm form = new TriBaseActionForm();
		form.setAppendFile1(createFormFile());
		expectCallsOnHttpRequestSetAttribute(AppendFileEnum.AppendFile1.getFileName(), FILE_NAME);
		expectCallsOnHttpRequestSetAttribute(AppendFileEnum.AppendFile1.getFileInputStream(), mockInputStream);
		expectCallsOnHttpRequestSetAttribute(AppendFileEnum.AppendFile1.getFileSize(), FILE_SIZE);

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();

		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall().andReturn(FORWARD_NAME);

		expectCallsOnFindForward();

		replayAll();
		ActionForward actual = partialTestee.execute(mockActionMapping, form, mockHttpServletRequest, null);
		verifyAll();

		assertThat(actual, sameInstance(actionForward));
	}

	/**
	 * {@link BaseStrutsAction#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト<br/>
	 * ダウンロードのケース。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecuteCaseDownload() throws Exception {

		expectCallsOnFindForward(null);
		TriBaseActionForm form = new TriBaseActionForm();
		form.setMessage("download");

		replayAll();
		ActionForward actual = partialTestee.execute(mockActionMapping, form, mockHttpServletRequest, null);
		verifyAll();

		assertThat(actual, sameInstance(actionForward));
	}

	/**
	 * {@link BaseStrutsAction#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト<br/>
	 * BusinessExceptuonが発生した場合、リクエスト情報にエラーメッセージが設定されることを確認する。、
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecuteCaseBusinessException() throws Exception {

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();

		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall().andThrow(new BusinessException(SmMessageId.SM001002E, EMPTY_ARGS));

		expectCallsOnFindForward(ERROR_BUSSINESS);

		replayAll();
		ActionForward actual = partialTestee.execute(mockActionMapping, new TriBaseActionForm(), mockHttpServletRequest, null);
		verifyAll();

		assertThat(actual, sameInstance(actionForward));
	}

	/**
	 * {@link BaseStrutsAction#execute(org.apache.struts.action.ActionMapping, org.apache.struts.action.ActionForm, HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト<br/>
	 * BusinessExceptuon以外が発生した場合、Exceptionがスローされることを確認する。、
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecuteCaseThrowable() throws Exception {

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();
		TriSystemException expectedEx = new TriSystemException(SmMessageId.SM001002E, EMPTY_ARGS);
		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall().andThrow(expectedEx);

		expectCallsOnGetMessage(SmMessageId.SM001002E, EMPTY_ARGS);
		mockHttpServletRequest.setAttribute(eq(ATTR_NAME_RESPONSE_BEAN), isA(SCRE0000ResponseBean.class));
		expectLastCall();
		expectCallsOnGetSession();
		mockHttpSession.invalidate();
		expectLastCall();

		replayAll();
		try {
			partialTestee.execute(mockActionMapping, new TriBaseActionForm(), mockHttpServletRequest, null);
			Assert.fail("期待したエラーが発生しなかった");
		} catch (Exception e) {
			assertThat(TriSystemException.class.isInstance(e.getCause()), is(true));
			assertThat((TriSystemException) e.getCause(), sameInstance(expectedEx));
		}

		verifyAll();
	}

	private void expectCallsOnGetSession() {

		mockHttpServletRequest.getSession();
		expectLastCall().andReturn(mockHttpSession);
	}

	private void expectCallsOnGetServletContext() {

		expectCallsOnGetSession();
		mockHttpSession.getServletContext();
		expectLastCall().andReturn(mockServletContext);
	}

	private void expectCallsOnHttpRequestSetAttribute(String name, Object value) {

		mockHttpServletRequest.setAttribute(name, value);
		expectLastCall();
	}

	private void expectCallsOnGetMessage(IMessageId id, String[] args) {

		mockMessageManager.getMessage("jp", id, args);
		expectLastCall().andReturn(TEST_MESSAGE1);
	}

	private void expectCallsOnFindForward() {
		expectCallsOnFindForward(FORWARD_NAME);
	}

	private void expectCallsOnFindForward(String forwardName) {

		mockActionMapping.findForward(forwardName);
		expectLastCall().andReturn(actionForward);
	}

	private FormFile createFormFile() throws Exception {

		FormFile mockFormFile = createMock(FormFile.class);
		mockFormFile.getFileName();
		expectLastCall().andReturn(FILE_NAME);
		mockInputStream = createMock(InputStream.class);
		mockFormFile.getInputStream();
		expectLastCall().andReturn(mockInputStream);
		mockFormFile.getFileSize();
		expectLastCall().andReturn(FILE_SIZE);

		return mockFormFile;
	}

}
