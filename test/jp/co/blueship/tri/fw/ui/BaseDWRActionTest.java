package jp.co.blueship.tri.fw.ui;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * {@link BaseDWRAction}のテストスイートクラス。
 *
 * @author Takayuki Kubo
 *
 */
@RunWith(Suite.class)
@SuiteClasses({ BaseDWRActionExecuteTest.class, BaseDWRActionInvalidateTest.class, BaseDWRActionPopulateErrorMessageToRequestTest.class })
public class BaseDWRActionTest {
}
