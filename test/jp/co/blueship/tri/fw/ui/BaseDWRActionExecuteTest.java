package jp.co.blueship.tri.fw.ui;

import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.isA;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * {@link BaseDWRAction#execute(HttpServletRequest, javax.servlet.http.HttpServletResponse)}のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class BaseDWRActionExecuteTest extends TriMockTestSupport {

	private static final String TEST_MESSAGE1 = "Test Message Hoge";
	private static final String[] EMPTY_ARGS = new String[] {};
	private static final String ATTR_NAME_RESPONSE_BEAN = "responseBean";
	private static final String ATTR_NAME_MESSAGE = "message";

	private BaseDWRAction partialTestee;
	private MessageManager mockMessageManager;
	private HttpServletRequest mockHttpServletRequest;
	private HttpSession mockHttpSession;
	private ServletContext mockServletContext;
	private IBaseResponseBean mockIBaseResponceBean;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		partialTestee = createMockBuilder(BaseDWRAction.class).//
				addMockedMethod("execute", IApplicationInfo.class, ISessionInfo.class, IRequestInfo.class).//
				createMock();

		mockMessageManager = createMock(MessageManager.class);
		addMockBeanByName(MessageManager.BEAN_NAME, mockMessageManager);

		mockServletContext = createMock(ServletContext.class);
		mockHttpSession = createMock(HttpSession.class);
		mockHttpServletRequest = createMock(HttpServletRequest.class);
		mockIBaseResponceBean = createMock(IBaseResponseBean.class);

	}

	/**
	 * {@link BaseDWRAction#execute(HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecute() throws Exception {

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();
		expectCallsOnHttpServletRequestGetAttribute(ATTR_NAME_RESPONSE_BEAN);

		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall();

		replayAll();
		IBaseResponseBean actual = partialTestee.execute(mockHttpServletRequest, null);
		verifyAll();

		assertThat(actual, sameInstance(mockIBaseResponceBean));
	}

	/**
	 * {@link BaseDWRAction#execute(HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト<br/>
	 * BusinessExceptuonが発生した場合、リクエスト情報にエラーメッセージが設定されることを確認する。、
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecuteCaseBusinessException() throws Exception {

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();
		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall().andThrow(new BusinessException(SmMessageId.SM001002E, EMPTY_ARGS));

		expectCallsOnGetMessage(SmMessageId.SM001002E, EMPTY_ARGS);
		expectCallsOnHttpRequestSetAttribute(ATTR_NAME_MESSAGE, TEST_MESSAGE1);
		expectCallsOnHttpServletRequestGetAttribute(ATTR_NAME_RESPONSE_BEAN);

		replayAll();
		IBaseResponseBean actual = partialTestee.execute(mockHttpServletRequest, null);
		verifyAll();

		assertThat(actual, sameInstance(mockIBaseResponceBean));

	}

	/**
	 * {@link BaseDWRAction#execute(HttpServletRequest, javax.servlet.http.HttpServletResponse)}
	 * のユニットテスト<br/>
	 * BusinessExceptuon以外が発生した場合、Exceptionがスローされることを確認する。、
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testExecuteCaseThrowable() throws Exception {

		expectCallsOnGetSession();
		expectCallsOnGetServletContext();
		TriSystemException expectedEx = new TriSystemException(SmMessageId.SM001002E, EMPTY_ARGS);
		partialTestee.execute(isA(IApplicationInfo.class), isA(ISessionInfo.class), isA(IRequestInfo.class));
		expectLastCall().andThrow(expectedEx);

		expectCallsOnGetMessage(SmMessageId.SM001002E, EMPTY_ARGS);
		mockHttpServletRequest.setAttribute(eq(ATTR_NAME_RESPONSE_BEAN), isA(SCRE0000ResponseBean.class));
		expectLastCall();
		expectCallsOnGetSession();
		mockHttpSession.invalidate();
		expectLastCall();

		replayAll();
		try {
			partialTestee.execute(mockHttpServletRequest, null);
			Assert.fail("期待したエラーが発生しなかった");
		} catch (Exception e) {
			assertThat(TriSystemException.class.isInstance(e.getCause()), is(true));
			assertThat((TriSystemException) e.getCause(), sameInstance(expectedEx));
		}

		verifyAll();
	}

	private void expectCallsOnHttpServletRequestGetAttribute(String name) {

		mockHttpServletRequest.getAttribute(name);
		expectLastCall().andReturn(mockIBaseResponceBean);
	}

	private void expectCallsOnGetSession() {

		mockHttpServletRequest.getSession();
		expectLastCall().andReturn(mockHttpSession);
	}

	private void expectCallsOnGetServletContext() {

		expectCallsOnGetSession();
		mockHttpSession.getServletContext();
		expectLastCall().andReturn(mockServletContext);
	}

	private void expectCallsOnHttpRequestSetAttribute(String name, Object value) {

		mockHttpServletRequest.setAttribute(name, value);
		expectLastCall();
	}

	private void expectCallsOnGetMessage(IMessageId id, String[] args) {

		mockMessageManager.getMessage("jp", id, args);
		expectLastCall().andReturn(TEST_MESSAGE1);
	}

}
