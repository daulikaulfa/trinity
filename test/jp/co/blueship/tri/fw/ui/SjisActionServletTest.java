package jp.co.blueship.tri.fw.ui;

import static org.easymock.EasyMock.expectLastCall;
import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Before;
import org.junit.Test;

public class SjisActionServletTest extends TriMockTestSupport {

	private SjisActionServlet testee;

	private TriTaskExecutor mockTriTaskExecutor;

	@Before
	public void setUp() {

		testee = new SjisActionServlet();

		mockTriTaskExecutor = createMock(TriTaskExecutor.class);
		addMockBeanByName(TriTaskExecutor.BEAN_NAME, mockTriTaskExecutor);

	}

	@Test
	public void testShutDownTriTaskExecutor() {

		mockTriTaskExecutor.isActive();
		expectLastCall().andReturn(true);

		mockTriTaskExecutor.shutDown();
		expectLastCall();

		replayAll();
		testee.shutDownTriTaskExecutor();
		verifyAll();
	}

	@Test
	public void testShutDownTriTaskExecutorCaseExecutorIsNull() {

		super.setUpMockTest();

		replayAll();
		testee.shutDownTriTaskExecutor();
		verifyAll();
	}

	@Test
	public void testShutDownTriTaskExecutorCaseExecutorIsDeActive() {

		mockTriTaskExecutor.isActive();
		expectLastCall().andReturn(false);

		replayAll();
		testee.shutDownTriTaskExecutor();
		verifyAll();
	}

}
