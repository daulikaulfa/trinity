package jp.co.blueship.tri.fw.ex;

import java.io.IOException;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ftp.FtpIoException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;

import org.junit.Test;

public class ExceptionUtilsTest {

	/**
	 * reThrowIfTrinityExceptionのテスト用
	 * @param e
	 */
	public static <T> void reThrowTest(Throwable e) {
		if ( ITranslatable.class.isInstance(e) ) {
			if (TriRuntimeException.class.isInstance(e)) {
				throw (TriRuntimeException) e;
			} else if (TriException.class.isInstance(e)) {
				TriException be = (TriException) e;
				throw new BusinessException(SmMessageId.SM001006E, be, "1","2");
			}
		}
	}

	@Test(expected = BusinessException.class)
	public void testReThrowIfBusinessException() {
		ExceptionUtils.reThrowIfBusinessException(new BusinessException(AmMessageId.AM001002E));
	}

	@Test
	public void testReThrowIfBusinessExceptionCaseNoReThrow() {
		ExceptionUtils.reThrowIfBusinessException(new Exception());
	}

	@Test(expected = ContinuableBusinessException.class)
	public void testReThrowIfContinuableBusinessException() {
		ExceptionUtils.reThrowIfContinuableBusinessException(new ContinuableBusinessException(AmMessageId.AM001002E));
	}

	@Test
	public void testReThrowIfContinuableBusinessExceptionCaseNoReThrow() {
		ExceptionUtils.reThrowIfContinuableBusinessException(new Exception());
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseNoReThrow() {
		try {
			reThrowTest(new Exception());
			System.out.print("OK0");
		}catch(Exception e){
			System.out.print("例外がthrowされた:NG");
		}
		System.out.print(" 終了0\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseReThrowBaseException() {
		try {
			reThrowTest(new TriException(BmMessageId.BM001011E));
			System.out.print("NG1");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			System.out.print("OK1 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesArgs[0] + "=1 not null" );
		} catch(Exception e) {
			System.out.print("NG1");
		}
		System.out.print(" 終了1\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseNoReThrowIOException() {
		try {
			reThrowTest(new IOException() );
			System.out.print("OK2");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			System.out.print("NG2 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesArgs[0] + "=1 not null" );
		} catch(Exception e) {
			System.out.print("NG2");
		}
		System.out.print(" 終了2\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseReThrowScmSysException() {
		try {
			reThrowTest(new ScmSysException(BmMessageId.BM001013E));
			System.out.print("NG3");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			String mesNull;
			if (TriStringUtils.isEmpty(mesArgs)) {
				mesNull = "null";
			}else{
				mesNull = mesArgs[0];
			}
			System.out.print("OK3 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesNull + "=null"  );
		} catch(Exception e) {
			System.out.print("NG3");
		}
		System.out.print(" 終了3\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseReThrowBaseBusinessException() {
		try {
			ExceptionUtils.reThrowIfTrinityException(new BaseBusinessException(BmMessageId.BM001014E,"4"));
			System.out.print("NG4");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			System.out.print("OK4 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesArgs[0] + "=4"  );
		} catch(Exception e) {
			System.out.print("NG4");
		}
		System.out.print(" 終了4\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseReThrowScmException() {
		try {
			reThrowTest(new ScmException(BmMessageId.BM001015E,"5"));
			System.out.print("NG5");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			System.out.print("OK5 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesArgs[0] + "=1 not 5"  );
		} catch(Exception e) {
			System.out.print("NG5");
		}
		System.out.print(" 終了5\n");
	}

	@Test
	public void testReThrowIfImplementsITraslatableCaseReThrowFtpIoException() {
		try {
			reThrowTest(new FtpIoException(BmMessageId.BM001016E,"6"));
			System.out.print("NG6");
		} catch(TriRuntimeException e) {
			String[] mesArgs = e.getMessageArgs();
			System.out.print("OK6 MessageID :"+ e.getMessageID() +" MessageArgs :" + mesArgs[0] + "=1 not 6"  );
		} catch(Exception e) {
			System.out.print("NG6");
		}
		System.out.print(" 終了6\n");
	}

	@Test
	public void testThrowBaseBusinessExceptionCaseTriException() {
		try{
			ExceptionUtils.throwBaseBusinessException(new FtpIoException(BmMessageId.BM001016E,"7"));
			System.out.print("NG7");
		} catch( Exception e ) {
			if ( BaseBusinessException.class.isInstance(e) ) {
				BaseBusinessException be = (BaseBusinessException)e;
				String[] mesArgs = be.getMessageArgs();
				System.out.print("OK7 MessageID :"+ be.getMessageID() +" MessageArgs :" + mesArgs[0] + "=7"  );
			} else {
				System.out.print("NG7");
			}
		}
		System.out.print("終了7\n");
	}

	@Test
	public void testThrowBaseBusinessExceptionCaseScmException() {
		try{
			ExceptionUtils.throwBaseBusinessException(new ScmException(BmMessageId.BM001016E,"8"));
			System.out.print("NG8");
		} catch( Exception e ) {
			if ( BaseBusinessException.class.isInstance(e) ) {
				BaseBusinessException be = (BaseBusinessException)e;
				String[] mesArgs = be.getMessageArgs();
				System.out.print("OK8 MessageID :"+ be.getMessageID() +" MessageArgs :" + mesArgs[0] + "=8"  );
			} else {
				System.out.print("NG8");
			}
		}
		System.out.print("終了8\n");
	}

}
