package jp.co.blueship.tri.fw.session;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;

import org.junit.Before;
import org.junit.Test;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.userdetails.User;

import jp.co.blueship.tri.fw.security.spi.TriAuthUser;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

public class LoginSessionTest extends TriMockTestSupport {

	private LoginSession testee;
	private SessionRegistry mockSessionRegistry;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {
		testee = new LoginSession();

		mockSessionRegistry = createMock(SessionRegistry.class);
		addMockBeanByName(LoginSession.SESSION_REGISTORY_BEAN_NAME, mockSessionRegistry);
	}

	/**
	 * {@link LoginSession#isLoggedInUser(String)} のテスト。
	 *
	 * 指定したユーザがSessionRepository内に存在する場合、trueが返却されることを確認する。
	 */
	@Test
	public void testIsLoggedInUser() {

		mockSessionRegistry.getAllPrincipals();
		expectLastCall().andReturn(Arrays.asList(//
				createUserTriAuthUser("logoutUser"),//
				createUserTriAuthUser("loginUser")));

		replayAll();
		boolean actual = testee.isLoggedInUser("loginUser");
		verifyAll();

		assertThat(actual, is(true));
	}

	/**
	 * {@link LoginSession#isLoggedInUser(String)} のテスト。
	 *
	 * 指定したユーザがSessionRepository内に存在しない場合、trueが返却されることを確認する。
	 */
	@Test
	public void testIsLoggedInUserCaseFalse() {

		mockSessionRegistry.getAllPrincipals();
		expectLastCall().andReturn(Arrays.asList(//
				createUserTriAuthUser("goodUser1"),//
				createUserTriAuthUser("goodUser2")));

		replayAll();
		boolean actual = testee.isLoggedInUser("badUser");
		verifyAll();

		assertThat(actual, is(false));
	}

	/**
	 * {@link LoginSession#isLoggedInUser(String)} のテスト。
	 *
	 * SessionRepository内にPrincipalが1件も存在しない場合(空リスト)、falseが返却されることを確認する。
	 */
	@Test
	public void testIsLoggedInUserCasePrincipalIsEmpty() {

		mockSessionRegistry.getAllPrincipals();
		expectLastCall().andReturn(Collections.emptyList());

		replayAll();
		boolean actual = testee.isLoggedInUser("userID");
		verifyAll();

		assertThat(actual, is(false));
	}

	/**
	 * {@link LoginSession#isLoggedInUser(String)} のテスト。
	 *
	 * SessionRepository内にPrincipalが1件も存在しない場合(NULL)、falseが返却されることを確認する。
	 */
	@Test
	public void testIsLoggedInUserCasePrincipalIsNull() {

		mockSessionRegistry.getAllPrincipals();
		expectLastCall().andReturn(null);

		replayAll();
		boolean actual = testee.isLoggedInUser("userID");
		verifyAll();

		assertThat(actual, is(false));
	}

	/**
	 * {@link LoginSession#isLoggedInUser(String)} のテスト。
	 *
	 * SessionRepository内のPrincipalがTriAuthUserでない場合、falseが返却されることを確認する。
	 */
	@Test
	public void testIsLoggedInUserCaseNotTriAuthUser() {

		mockSessionRegistry.getAllPrincipals();
		expectLastCall().andReturn(Arrays.asList(createUser()));

		replayAll();
		boolean actual = testee.isLoggedInUser("");
		verifyAll();

		assertThat(actual, is(false));
	}

	private TriAuthUser createUserTriAuthUser(String userID) {

		TriAuthUser mockUser = createMock(TriAuthUser.class);
		mockUser.getUserID();
		expectLastCall().andReturn(userID);

		return mockUser;
	}

	private User createUser() {

		User mockUser = createMock(User.class);

		return mockUser;
	}

}
