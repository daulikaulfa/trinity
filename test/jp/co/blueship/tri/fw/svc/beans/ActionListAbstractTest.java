package jp.co.blueship.tri.fw.svc.beans;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link ActionListAbstract}のユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class ActionListAbstractTest extends TriMockTestSupport {

	private ActionListAbstract<IGeneralServiceBean> partialTestee;
	private IDomain<IGeneralServiceBean> mockActionPojo1;
	private IDomain<IGeneralServiceBean> mockActionPojo2;

	/**
	 * 事前準備
	 */
	@SuppressWarnings("unchecked")
	@Before
	public void setUp() {

		partialTestee = createMockBuilder(ActionListAbstract.class).createMock();
		mockActionPojo1 = createMock(IDomain.class);
		mockActionPojo2 = createMock(IDomain.class);
	}

	/**
	 * {@link ActionListAbstract#actions(List, boolean)}のユニットテスト。<br/>
	 * 各ActionPojoで例外が発生した場合、処理を中断せず、発生したすべてに例外情報をまとめた<br/>
	 * ContinuableBusinessExceptionがスローされることを確認する。
	 */
	@Test
	public void testActionsCaseContinuableBusinessExceptionIsTrue() {

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>()
			.addAll(Collections.<Object> emptyList());

		mockActionPojo1.execute(serviceDto);
		expectLastCall().andThrow(new BusinessException(SmMessageId.SM001002E, ""));

		mockActionPojo2.execute(serviceDto);
		expectLastCall().andThrow(new ContinuableBusinessException(BmMessageId.BM001000E, ""));

		partialTestee.setActions(Arrays.asList(mockActionPojo1, mockActionPojo2));

		replayAll();
		try {
			partialTestee.actions(serviceDto, true);
			Assert.fail("期待した例外が発生しなかった。");
		} catch (ContinuableBusinessException e) {

			assertThat(TriCollectionUtils.isNotEmpty(e.getMessageIdList()), is(true));
			assertThat(e.getMessageIdList().size(), is(2));
			assertThat((SmMessageId) e.getMessageIdList().get(0), is(SmMessageId.SM001002E));
			assertThat((BmMessageId) e.getMessageIdList().get(1), is(BmMessageId.BM001000E));

			assertThat(TriCollectionUtils.isNotEmpty(e.getMessageArgsList()), is(true));
			assertThat(e.getMessageArgsList().size(), is(2));
			assertThat(e.getMessageArgsList().get(0).length, is(1));
			assertThat(e.getMessageArgsList().get(1).length, is(1));
		}
		verifyAll();

	}

}
