package jp.co.blueship.tri.fw.act.rb;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean.MessageUtility;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link BaseResponseBean.MessageUtility}のユニットテストクラス
 *
 * @author Takayuki Kubo
 *
 */
public class MessageUtilityTest extends TriMockTestSupport {

	private static final String TEST_MESSAGE1 = "Test Message Hoge";
	private static final String[] EMPTY_ARGS = new String[] {};

	private MessageUtility testee;
	private BaseResponseBean baseResponseBean;
	private MessageManager mockMessageManager;

	@Before
	public void setUp() {

		baseResponseBean = new BaseResponseBean();
		testee = baseResponseBean.new MessageUtility();

		mockMessageManager = createMock(MessageManager.class);
		addMockBeanByName(MessageManager.BEAN_NAME, mockMessageManager);

	}

	/**
	 * {@link BaseResponseBean.MessageUtility#reflectMessage(ITranslatable)}
	 * のユニットテスト<br/>
	 *
	 * 例外が保持するメッセージIDを基に導出したメッセージ文言がBaseResponseBeanクラスに設定されることを確認する。
	 */
	@Test
	public void testReflectMessageTranslatable() {

		mockMessageManager.getMessage("jp", SmMessageId.SM001002E, EMPTY_ARGS);
		expectLastCall().andReturn(TEST_MESSAGE1);

		ITranslatable input = createTranslatableException(SmMessageId.SM001002E, EMPTY_ARGS);

		replayAll();
		testee.reflectMessage(input);
		verifyAll();

		List<String> actual = baseResponseBean.getMessageList();
		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat(actual.get(0), is(TEST_MESSAGE1));
	}

	/**
	 * {@link BaseResponseBean.MessageUtility#reflectMessage(ITranslatable)}
	 * のユニットテスト<br/>
	 *
	 * 例外がnullの場合、BaseResponseBeanクラスにメッセージが設定されないことを確認する。
	 */
	@Test
	public void testReflectMessageTranslatableCaseExceptionIsNull() {

		replayAll();
		testee.reflectMessage((ITranslatable) null);
		verifyAll();

		List<String> actual = baseResponseBean.getMessageList();
		assertThat(TriCollectionUtils.isEmpty(actual), is(true));
	}

	private ITranslatable createTranslatableException(IMessageId id, String[] args) {

		ITranslatable mockTranslatable = createMock(ITranslatable.class);
		mockTranslatable.getMessageID();
		expectLastCall().andReturn(id);

		mockTranslatable.getMessageArgs();
		expectLastCall().andReturn(args);

		return mockTranslatable;
	}
}
