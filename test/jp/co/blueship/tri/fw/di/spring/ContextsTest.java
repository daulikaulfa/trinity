package jp.co.blueship.tri.fw.di.spring;

import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;

import org.easymock.EasyMockSupport;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;

public class ContextsTest extends EasyMockSupport {

	/**
	 * setApplicationContext()をテストする
	 * ApplicationContext型を引数とする
	 * setした内容がgetBeanFactoryで得たinstanceと一致すること
	 */
	@Test
	public void testSetBeanFactoryFrom() {

		ApplicationContext mockAppContext = createAppContext();

		replayAll();
		Contexts.getInstance().setApplicationContext(mockAppContext);
		verifyAll();

		BeanFactory actual = Contexts.getInstance().getBeanFactory();

		assertThat(actual, sameInstance((BeanFactory) mockAppContext));
	}

	private ApplicationContext createAppContext() {

		ApplicationContext result = createMock(ApplicationContext.class);

		return result;
	}

}
