package jp.co.blueship.tri.fw.di.spring;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;

/**
 * Spring定義ファイルの読み込み確認を行うための疎通テスト。
 *
 * @author Takayuki Kubo
 *
 */
public class ContextsConnectivityTest {

	private Contexts testee;

	@Before
	public void setUp() {
		testee = Contexts.getInstance();
	}

	/**
	 * trinity Webアプリケーションの最低限の構成において、Spring定義ファイルがロードできることを確認する。
	 */
	@Test
	public void testInitBeanFactoryCaseOnlyFrameworkContext() {

		String[] configs = { "classpath:Test-Trinity-WebApp-Context.xml", "classpath:Fw-Module-Context.xml" };
		try {
			testee.initBeanFactory(configs);
		} catch (Exception e) {
			e.printStackTrace();
		}

		BeanFactory actual = testee.getBeanFactory();

		assertThat(actual, is(notNullValue()));

		for (String beanName : ((ListableBeanFactory) actual).getBeanDefinitionNames()) {
			Object bean = actual.getBean(beanName);
			assertThat(bean, is(notNullValue()));
		}

	}

	/**
	 * trinity 構成管理Webアプリケーション構成において、Spring定義ファイルがロードできることを確認する。
	 */
	@Test
	public void testInitBeanFactoryCaseChangecConfig() {

		String[] configs = { "classpath:Test-Trinity-WebApp-Context.xml", "classpath:Fw-Module-Context.xml", "classpath:Cha-WebApp-Context.xml" };
		testee.initBeanFactory(configs);
		BeanFactory actual = testee.getBeanFactory();

		assertThat(actual, is(notNullValue()));

		for (String beanName : ((ListableBeanFactory) actual).getBeanDefinitionNames()) {
			Object bean = actual.getBean(beanName);
			assertThat(bean, is(notNullValue()));
		}
	}

	/**
	 * trinity リリース管理Webアプリケーション構成において、Spring定義ファイルがロードできることを確認する。
	 */
	@Test
	public void testInitBeanFactoryCaseReleaseConfig() {

		String[] configs = { "classpath:Test-DealAssetRemoteService-Context.xml", "classpath:Test-Trinity-WebApp-Context.xml", "classpath:Fw-Module-Context.xml", "classpath:Rel-WebApp-Context.xml" };
		testee.initBeanFactory(configs);
		BeanFactory actual = testee.getBeanFactory();

		assertThat(actual, is(notNullValue()));

		for (String beanName : ((ListableBeanFactory) actual).getBeanDefinitionNames()) {
			Object bean = actual.getBean(beanName);
			assertThat(bean, is(notNullValue()));
		}
	}

	@After
	public void tearDown() {
		testee.setApplicationContext(null);
	}

}
