package jp.co.blueship.tri.fw.di.spring;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.NoSuchMessageException;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link ContextAdapter}のユニットテストクラス
 *
 * @author Takayuki Kubo
 *
 */
public class ContextAdapterTest extends TriMockTestSupport {

	private static final String TEST_MESSAGE1 = "Test Message Hoge";
	private static final String TEST_MESSAGE2 = "Test Message Fuga";
	private IContextAdapter partialTestee;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		partialTestee = createMockBuilder(ContextAdapter.class).//
				addMockedMethod("getMessage", IMessageId.class, String[].class).//
				createMock();
	}

	/**
	 * {@link ContextAdapter#getMessage(jp.co.blueship.tri.fw.ex.ITranslatable)}
	 * のユニットテスト。<br/>
	 * 指定された例外がContinuableBusinessException以外の場合、メッセージが１件だけ返却されることを確認する。
	 */
	@Test
	public void testGetMessageByTranslatable() {

		partialTestee.getMessage(SmMessageId.SM001002E, new String[] {});
		expectLastCall().andReturn(TEST_MESSAGE1);

		ITranslatable input = createTranslatableException(SmMessageId.SM001002E, new String[] {});

		replayAll();
		List<String> actual = partialTestee.getMessage(input);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat(actual.get(0), is(TEST_MESSAGE1));
	}

	/**
	 * {@link ContextAdapter#getMessage(jp.co.blueship.tri.fw.ex.ITranslatable)}
	 * のユニットテスト。<br/>
	 * 指定された例外がContinuableBusinessExceptionの場合、
	 * 同Exception内に保持された複数件のメッセージがすべて返却されることを確認する。
	 */
	@Test
	public void testGetMessageByContinuableBusinessException() {

		partialTestee.getMessage(SmMessageId.SM001002E, new String[] {});
		expectLastCall().andReturn(TEST_MESSAGE1);

		partialTestee.getMessage(BmMessageId.BM001000E, new String[] {});
		expectLastCall().andReturn(TEST_MESSAGE2);

		ITranslatable input = new ContinuableBusinessException(//
				Arrays.<IMessageId> asList(SmMessageId.SM001002E, BmMessageId.BM001000E),//
				Arrays.asList(new String[] {}, new String[] {}));

		replayAll();
		List<String> actual = partialTestee.getMessage(input);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(2));
		assertThat(actual.get(0), is(TEST_MESSAGE1));
		assertThat(actual.get(1), is(TEST_MESSAGE2));
	}

	/**
	 * {@link ContextAdapter#getMessage(jp.co.blueship.tri.fw.ex.ITranslatable)}
	 * のユニットテスト。<br/>
	 * 指定された例外がnullの場合、空のリストが返却されることを確認する。
	 */
	@Test
	public void testGetMessageByTranslatableCaseExceptionIsNull() {

		List<String> actual = partialTestee.getMessage((ITranslatable) null);
		assertThat(actual, is(notNullValue()));
		assertThat(actual.size(), is(0));

	}

	/**
	 * {@link ContextAdapter#getMessage(jp.co.blueship.tri.fw.ex.ITranslatable)}
	 * のユニットテスト。<br/>
	 * メッセージIDからメッセージ文言への変換でNoSuchMessageExceptionがスローされた場合、<br/>
	 * メッセージ文言にメッセージIDの文字列表現がそのまま設定されることを確認する。
	 */
	@Test
	public void testGetMessageByTranslatableCaseError() {

		partialTestee.getMessage(SmMessageId.SM001002E, new String[] {});
		expectLastCall().andThrow(new NoSuchMessageException("Any Code"));

		ITranslatable input = createTranslatableException(SmMessageId.SM001002E, new String[] {});

		replayAll();
		List<String> actual = partialTestee.getMessage(input);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat(actual.get(0), is(SmMessageId.SM001002E.getMessageId()));
	}

	private ITranslatable createTranslatableException(IMessageId id, String[] args) {

		ITranslatable mockTranslatable = createMock(ITranslatable.class);
		mockTranslatable.getMessageID();
		expectLastCall().andReturn(id);

		mockTranslatable.getMessageArgs();
		expectLastCall().andReturn(args);

		return mockTranslatable;
	}

}
