package jp.co.blueship.tri.fw.di;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * {@link MessageManager}の疎通テスト。<br/>
 * 実際にメッセージリソースの読み込みを行い、メッセージIDよりメッセージ文言が取得できることを確認するためのテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Fw-MessageContents-Context.xml" })
public class MessageManagerConnectivityTest {

	@Autowired
	private MessageManager testee;

	private static final IMessageId TEST_MESSAGE_ID = AmMessageId.AM004045F;
	private static final String EXPECTED_MESSAGE_CONTENTS_1 = "[AM004045F] : リポジトリ情報にデータが存在しません。 テーブル名 = HOGE";
	private static final String EXPECTED_MESSAGE_CONTENTS_2 = "リポジトリ情報にデータが存在しません。 テーブル名 = HOGE";
	private static final String[] MESSAGE_ARGS = { "HOGE" };

	private static final TriLogMessage TEST_LOG_MESSAGE_ID = TriLogMessage.LSM0001;
	private static final String EXPECTED_LOG_MESSAGE_CONTENTS = "ビルド番号：12345 登録・更新者：FUGA";
	private static final String[] LOG_ARGS = { "12345", "FUGA" };

	@Test
	public void testGetMessageWithDecorate() {

		String actual = testee.getMessage("jp", TEST_MESSAGE_ID, MESSAGE_ARGS);
		assertThat(TriStringUtils.isNotEmpty(actual), is(true));
		assertThat(actual, is(EXPECTED_MESSAGE_CONTENTS_1));
	}

	@Test
	public void testGetMessageWithNoDecorate() {

		String actual = testee.getMessage("jp", TEST_MESSAGE_ID, MESSAGE_ARGS, false);
		assertThat(TriStringUtils.isNotEmpty(actual), is(true));
		assertThat(actual, is(EXPECTED_MESSAGE_CONTENTS_2));
	}

	@Test
	public void testGetLogMessage() {

		String actual = testee.getLogMessage(TEST_LOG_MESSAGE_ID, LOG_ARGS);
		assertThat(TriStringUtils.isNotEmpty(actual), is(true));
		assertThat(actual, is(EXPECTED_LOG_MESSAGE_CONTENTS));
	}

}
