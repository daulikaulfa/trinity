/**
 *
 */
package jp.co.blueship.tri.fw.vcs.svn;

import static org.easymock.EasyMock.*;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.easymock.Capture;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNCopySource;
import org.tmatesoft.svn.core.wc.SVNRevision;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.vcs.svn.constants.SvnSubDir;

/**
 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore}のユニットテストクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class SubversionCoreTest extends EasyMockSupport {

	private SubversionCore testee;

	private SvnKitProxy mockSvnKitProxy;
	private DirEntryHandler mockHandler;

	private Capture<SVNURL> capturedSVNURL;
	private Capture<SVNURL> capturedSVNURL2;
	private Capture<File> capturedFile;
	private Capture<String> capturedStr;
	private Capture<SVNCopySource[]> capturedSVNCopySources;
	private Capture<File[]> capturedFileList;

	/**
	 * 事前準備を行います。
	 */
	@Before
	public void setUp() {

		mockHandler = createMock(DirEntryHandler.class);
		mockSvnKitProxy = createMock(SvnKitProxy.class);
		testee = new SubversionCore();
		testee.setSvnKitProxy(mockSvnKitProxy);

		capturedSVNURL = new Capture<SVNURL>();
		capturedSVNURL2 = new Capture<SVNURL>();
		capturedStr = new Capture<String>();
		capturedFile = new Capture<File>();
		capturedSVNCopySources = new Capture<SVNCopySource[]>();
		capturedFileList = new Capture<File[]>();
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#checkoutTrunkHead(String, String, String)}
	 * のためのテスト・メソッド。 SVNURLが正しいこと ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testCheckoutTrunkHead() throws Exception {

		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";

		mockSvnKitProxy.checkout(capture(capturedSVNURL), capture(capturedFile), eq(SVNRevision.HEAD), eq(SVNRevision.HEAD));
		expectLastCall();

		replayAll();
		testee.checkoutTrunkHead(repository, moduleName, pathDest);
		verifyAll();

		assertThat(capturedSVNURL.getValue().toString(), is("svn://repository/trunk/module"));
		assertThat(capturedFile.getValue().getPath(), is("c:\\dest\\module"));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#checkoutBranchHead(String, String, String, String)}
	 * のためのテスト・メソッド。 SVNURLが正しいこと ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testCheckoutBranchHead() throws Exception {

		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		String labelBranch = "branch";

		mockSvnKitProxy.checkout(capture(capturedSVNURL), capture(capturedFile), eq(SVNRevision.parse("branch")), eq(SVNRevision.parse("branch")));
		expectLastCall();

		replayAll();
		testee.checkoutBranchHead(repository, moduleName, pathDest, labelBranch);
		verifyAll();

		assertThat(capturedSVNURL.getValue().toString(), is("svn://repository/branches/branch/module"));
		assertThat(capturedFile.getValue().getPath(), is("c:\\dest\\module"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#checkoutBranchRevision(String, String, String, String, Long)}
	 * のためのテスト・メソッド。 SVNURLが正しいこと ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testCheckoutBranchRevision() throws Exception {

		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		String labelBranch = "branch";
		Long revisionNo = 1L;

		mockSvnKitProxy.isValidRevisionNumber(revisionNo);
		expectLastCall().andReturn(true);

		mockSvnKitProxy.checkout(capture(capturedSVNURL), capture(capturedFile), eq(SVNRevision.create(revisionNo)),
				eq(SVNRevision.create(revisionNo)));
		expectLastCall();

		replayAll();
		testee.checkoutBranchRevision(repository, moduleName, pathDest, labelBranch, revisionNo);
		verifyAll();

		assertThat(capturedSVNURL.getValue().toString(), is("svn://repository/branches/branch/module"));
		assertThat(capturedFile.getValue().getPath(), is("c:\\dest\\module"));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#checkoutBranchRevision(String, String, String, String, Long)}
	 * のためのテスト・メソッド。 リビジョン番号が不当な場合、SystemExceptionがスローすることを確認する。
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test(expected = TriSystemException.class)
	public void testCheckoutBranchRevisionCaseInvalidRevision() throws Exception {

		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		String labelBranch = "branch";
		Long revisionNo = 1L;

		mockSvnKitProxy.isValidRevisionNumber(revisionNo);
		expectLastCall().andReturn(false);

		replayAll();
		testee.checkoutBranchRevision(repository, moduleName, pathDest, labelBranch, revisionNo);
		verifyAll();
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#branch(String, String, String)}
	 * のためのテスト・メソッド。 commitCommentが等しいこと URLが等しいこと infoとactualのインスタンスが同じであること
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testBranch() throws Exception {

		String repository = "svn://repository\\";
		String labeBranch = "branch";
		String commitComment = "commit comment";
		SVNCommitInfo info = new SVNCommitInfo(0, null, null);

		mockSvnKitProxy.copyUrlToUrl(capture(capturedSVNCopySources), eq("svn://repository/branches/branch"), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		SVNCommitInfo actual = testee.branch(repository, labeBranch, commitComment);
		verifyAll();
		assertThat(actual, sameInstance(info));
		SVNCopySource[] value = capturedSVNCopySources.getValue();
		assertThat(value.length, is(1));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#tagToTrunk(String, String, String)}
	 * のためのテスト・メソッド。 commitCommentが等しいこと URLが等しいこと infoとactualのインスタンスが同じであること
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testTagToTrunk() throws Exception {

		String repository = "svn://repository\\";
		String labelTag = "tag";
		String commitComment = "commit comment";

		SVNCommitInfo info = new SVNCommitInfo(0, null, null);

		mockSvnKitProxy.copyUrlToUrl(capture(capturedSVNCopySources), eq("svn://repository/tags/tag"), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		SVNCommitInfo actual = testee.tagToTrunk(repository, labelTag, commitComment);
		verifyAll();
		assertThat(actual, sameInstance(info));
		SVNCopySource[] value = capturedSVNCopySources.getValue();
		assertThat(value.length, is(1));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#tagToBranch(String, String, String, String)}
	 * のためのテスト・メソッド。 commitCommentが等しいこと URLが等しいこと infoとactualのインスタンスが同じであること
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testTagToBranch() throws Exception {

		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String labelTag = "tag";
		String commitComment = "commit comment";

		SVNCommitInfo info = new SVNCommitInfo(0, null, null);

		mockSvnKitProxy.copyUrlToUrl(capture(capturedSVNCopySources), eq("svn://repository/tags/tag"), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		SVNCommitInfo actual = testee.tagToBranch(repository, labelBranch, labelTag, commitComment);
		verifyAll();
		assertThat(actual, sameInstance(info));
		SVNCopySource[] value = capturedSVNCopySources.getValue();
		assertThat(value.length, is(1));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#listSubDir(String, SvnSubDir, SVNDepth, DirEntryHandler)}
	 * のためのテスト・メソッド。 SVNURLが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testListSubDir() throws Exception {

		String repository = "svn://repository\\";
		SvnSubDir svnSubDir = SvnSubDir.trunk;
		DirEntryHandler handler = new DirEntryHandler();
		SVNDepth svnDepth = SVNDepth.EMPTY;

		mockSvnKitProxy.listRemote(capture(capturedSVNURL), eq(SVNRevision.HEAD), eq(SVNRevision.HEAD), eq(SVNDepth.EMPTY), eq(handler));
		expectLastCall();

		replayAll();
		testee.listSubDir(repository, svnSubDir, svnDepth, handler);
		verifyAll();
		assertThat(capturedSVNURL.getValue().toString(), is("svn://repository/trunk"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#statusAll(String, String, StatusHandler)}
	 * のためのテスト・メソッド。 ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testStatusAll() throws Exception {

		String pathDest = "c:\\dest\\";
		String moduleName = "module";
		StatusHandler handler = new StatusHandler(true);

		mockSvnKitProxy.status(capture(capturedFile), eq(handler), eq(SVNDepth.INFINITY));
		expectLastCall();

		replayAll();
		testee.statusAll(pathDest, moduleName, handler);
		verifyAll();
		assertThat(capturedFile.getValue().toString(), is("c:\\dest\\module"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#statusSingle(String, String, StatusHandler)}
	 * のためのテスト・メソッド。 ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testStatusSingle() throws Exception {

		String pathDest = "c:\\dest\\";
		String objectPath = "object";
		StatusHandler handler = new StatusHandler(true);

		mockSvnKitProxy.status(capture(capturedFile), eq(handler), eq(SVNDepth.EMPTY));
		expectLastCall();

		replayAll();
		testee.statusSingle(pathDest, objectPath, handler);
		verifyAll();

		assertThat(capturedFile.getValue().getPath(), is("c:\\dest\\object"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#update(String, String)}
	 * のためのテスト・メソッド。 actualがrevisionNoと等しいこと ファイルパスが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testUpdate() throws Exception {

		String pathDest = "c:\\dest\\";
		String moduleName = "module\\";
		Long revisionNo = 1L;

		mockSvnKitProxy.update(capture(capturedStr), eq(SVNRevision.HEAD));
		expectLastCall().andReturn(revisionNo);

		replayAll();
		long actual = testee.update(pathDest, moduleName);
		verifyAll();
		assertThat(actual, is(revisionNo));
		assertThat(capturedStr.getValue(), is("c:/dest/module"));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#commit(String, String[], String)}
	 * のためのテスト・メソッド。 commitCommentが等しいこと ファイルリストのサイズが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testCommit() throws Exception {

		String pathDest = "c:\\dest\\";
		String[] moduleNameArray = { "module1", "module2" };
		String commitComment = "commit comment";

		SVNCommitInfo info = new SVNCommitInfo(0, null, null);
		String[] changeListArray = null;
		SVNProperties svnProperties = null;
		mockSvnKitProxy.commit(capture(capturedFileList), eq(commitComment), eq(changeListArray), eq(svnProperties));

		expectLastCall().andReturn(info);

		replayAll();
		SVNCommitInfo actual = testee.commit(pathDest, moduleNameArray, commitComment);
		verifyAll();
		assertThat(actual, sameInstance(info));
		File[] actualPathArray = capturedFileList.getValue();
		assertThat(actualPathArray.length, is(2));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#add(String[])}
	 * のためのテスト・メソッド。 ファイルリストのサイズが正しいこと
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testAdd() throws Exception {

		String[] pathArray = { "add1", "add2" };

		mockSvnKitProxy.add(capture(capturedFileList));
		expectLastCall();

		replayAll();
		testee.add(pathArray);
		verifyAll();
		assertThat(capturedFileList.getValue().length, is(2));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#delete(String)}
	 * のためのテスト・メソッド。 ファイルパスが正しいか
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testDelete() throws Exception {

		String pathDeleteAsset = "c:\\delete\\asset";

		mockSvnKitProxy.delete(capture(capturedFile));
		expectLastCall();

		replayAll();
		testee.delete(pathDeleteAsset);
		verifyAll();

		assertThat(capturedFile.getValue().getPath(), is("c:\\delete\\asset"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SubversionCore#mergeBranch(String, String, String, SVNRevision, SVNRevision, String)}
	 * のためのテスト・メソッド。 SVNURLが正しいか SVNURL2が正しいか ファイルパスが正しいか
	 *
	 * @throws Exception 例外が発生した場合
	 */
	@Test
	public void testMergeBranch() throws Exception {

		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String moduleName = "module\\";
		SVNRevision mergeFrom = SVNRevision.BASE;
		SVNRevision mergeTo = SVNRevision.HEAD;
		String pathDest = "c:\\dest\\";

		mockSvnKitProxy.merge(capture(capturedSVNURL), eq(mergeFrom), capture(capturedSVNURL2), eq(mergeTo), capture(capturedFile));
		expectLastCall();

		replayAll();
		testee.mergeFromBranch(repository, labelBranch, moduleName, mergeFrom, mergeTo, pathDest);
		verifyAll();

		assertThat(capturedSVNURL.getValue().toString(), is("svn://repository/branches/branch/module"));
		assertThat(capturedSVNURL2.getValue().toString(), is("svn://repository/branches/branch/module"));
		assertThat(capturedFile.getValue().getPath(), is("c:\\dest\\module"));

	}

	@Test
	public void testCheckTag() throws Exception {

		String versionTag = "tag";
		List<SVNDirEntry> list = extracted();
		DirEntryHandler handler = createMock(DirEntryHandler.class);
		handler.getSvnDirEntryList();
		expectLastCall().andReturn(list);

		replayAll();
		boolean actual = testee.checkTag(versionTag, handler);
		verifyAll();

		assertThat(actual, is(true));

	}

	@Test
	public void testCheckTagCaseFalse() throws Exception {

		String versionTag = "tag";
		DirEntryHandler handler = new DirEntryHandler();

		replayAll();
		boolean actual = testee.checkTag(versionTag, handler);
		verifyAll();

		assertThat(actual, is(false));

	}

	@Test(expected = TriSystemException.class)
	public void testCheckTagCaseException() throws Exception {

		String versionTag = "tag";
		expectCallsOnGetSvnDirEntry(extracted2());

		replayAll();
		testee.checkTag(versionTag, mockHandler);
		verifyAll();

	}

	@Test
	public void testRevisionFromHandler() throws Exception {

		expectCallsOnGetSvnDirEntry(extracted());

		replayAll();
		long actual = testee.revisionFromHandler(mockHandler);
		verifyAll();

		assertThat(actual, is(0L));

	}

	@Test(expected = TriSystemException.class)
	public void testRevisionFromHandlerCaseDirEntryListIsNull() throws Exception {

		expectCallsOnGetSvnDirEntry(null);

		replayAll();
		testee.revisionFromHandler(mockHandler);
		verifyAll();
	}

	@Test(expected = TriSystemException.class)
	public void testRevisionFromHandlerCaseDirEntryListIsEmpty() throws Exception {

		expectCallsOnGetSvnDirEntry(Collections.<SVNDirEntry> emptyList());

		replayAll();
		testee.revisionFromHandler(mockHandler);
		verifyAll();
	}

	private void expectCallsOnGetSvnDirEntry(List<SVNDirEntry> list) {
		mockHandler.getSvnDirEntryList();
		expectLastCall().andReturn(list);
	}

	private List<SVNDirEntry> extracted() {
		return Arrays.asList(new SVNDirEntry(null, null, "tag", SVNNodeKind.DIR, 0, false, 0, null, null));
	}

	private List<SVNDirEntry> extracted2() {
		return Arrays.asList(new SVNDirEntry(null, null, "tag", null, 0, false, 0, null, null));
	}

	@SuppressWarnings("deprecation")
	@Test
	public void testModuleNameSet() throws Exception {

		List<SVNDirEntry> list = extracted();
		DirEntryHandler handler = createMock(DirEntryHandler.class);
		handler.getSvnDirEntryList();
		expectLastCall().andReturn(list);

		replayAll();
		Set<String> actual = testee.moduleNameSet(handler);
		verifyAll();

		assertThat(actual, is(Set.class));

	}

}
