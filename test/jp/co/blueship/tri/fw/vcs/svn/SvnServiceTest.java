package jp.co.blueship.tri.fw.vcs.svn;

import static org.easymock.EasyMock.capture;
import static org.easymock.EasyMock.eq;
import static org.easymock.EasyMock.expectLastCall;
import static org.easymock.EasyMock.getCurrentArguments;
import static org.easymock.EasyMock.isA;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;

import java.io.File;
import java.text.DateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.validator.TriValidationHelper;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsCommitInfo;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.svn.constants.SvnSubDir;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.MergeVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;
import jp.co.blueship.tri.fw.vcs.vo.UpdateVO;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.easymock.Capture;
import org.easymock.IAnswer;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNErrorCode;
import org.tmatesoft.svn.core.SVNErrorMessage;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;

/**
 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService}のユニットテストクラスです。
 *
 * @author Takashi Ono
 *
 */
public class SvnServiceTest extends TriMockTestSupport {

	private IVcsService testSS;
	private SubversionCore mockSubversionCore;
	private TriValidationHelper mockTriValidationHelper;
	private Capture<String> capturedStr1;
	private Capture<String[]> capturedStrAry;

	/**
	 * 事前準備を行います。
	 */
	@Before
	public void setUp() {
		testSS = new SvnService();
		mockSubversionCore = createMock(SubversionCore.class);
		ReflectionTestUtils.setField(testSS, "subversionCore", mockSubversionCore);
		mockTriValidationHelper = createMock(TriValidationHelper.class);
		addMockBeanByName(TriValidationHelper.BEAN_NAME, mockTriValidationHelper);
		capturedStr1 = new Capture<String>();
		capturedStrAry = new Capture<String[]>();
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#checkoutHead(CheckOutVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testCheckoutHead() throws Exception {

		CheckOutVO vo = new CheckOutVO();
		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		vo.setRepository(repository);
		vo.setModuleName(moduleName);
		vo.setPathDest(pathDest);

		mockTriValidationHelper.validate(vo, CheckOutVO.ChkHeadChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.checkoutTrunkHead(eq(repository), eq(moduleName), eq(pathDest));
		expectLastCall();

		replayAll();
		testSS.checkoutHead(vo);
		verifyAll();

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#checkoutBranch(CheckOutVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testCheckoutBranch() throws Exception {

		CheckOutVO vo = new CheckOutVO();
		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		String labelBranch = "branch";
		vo.setRepository(repository);
		vo.setModuleName(moduleName);
		vo.setPathDest(pathDest);
		vo.setLabelBranch(labelBranch);

		mockTriValidationHelper.validate(vo, CheckOutVO.ChkBranchChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.checkoutBranchHead(eq(repository), eq(moduleName), eq(pathDest), eq(labelBranch));
		expectLastCall();

		replayAll();
		testSS.checkoutBranch(vo);
		verifyAll();

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#checkoutBranchRevision(CheckOutVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testCheckoutBranchRevision() throws Exception {

		CheckOutVO vo = new CheckOutVO();
		String repository = "svn://repository\\";
		String moduleName = "module\\";
		String pathDest = "c:\\dest\\";
		String labelBranch = "branch";
		Long revisionNo = 1L;
		vo.setRepository(repository);
		vo.setModuleName(moduleName);
		vo.setPathDest(pathDest);
		vo.setLabelBranch(labelBranch);
		vo.setRevisionNo(revisionNo);

		mockTriValidationHelper.validate(vo, CheckOutVO.RevisionChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.checkoutBranchRevision(eq(repository), eq(moduleName), eq(pathDest), eq(labelBranch), eq(revisionNo));
		expectLastCall();

		replayAll();
		testSS.checkoutBranchRevision(vo);
		verifyAll();
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#merge(MergeVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testMerge() throws Exception {

		MergeVO vo = new MergeVO();
		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String moduleName = "module\\";
		SVNRevision mergeFrom = SVNRevision.BASE;
		SVNRevision mergeTo = SVNRevision.HEAD;
		String pathDest = "c:\\dest\\";

		vo.setRepository(repository);
		vo.setLabelBranch(labelBranch);
		vo.setModuleName(moduleName);
		vo.setMergeFrom(mergeFrom);
		vo.setMergeTo(mergeTo);
		vo.setPathDest(pathDest);

		mockTriValidationHelper.validate(vo);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.mergeFromBranch(eq(repository), eq(labelBranch), eq(moduleName), eq(mergeFrom), eq(mergeTo), eq(pathDest));
		expectLastCall();

		replayAll();
		testSS.merge(vo);
		verifyAll();
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#branch(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの3つのパラメータがsubversionCoreメソッドの戻り値と等しいこと、ErrorMessageがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testBranch() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelBranch(labelBranch);
		vo.setCommitComment(commitComment);

		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision = 1L;
		String author = "testAuthor";
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date);

		mockTriValidationHelper.validate(vo, CommitVO.BranchChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.branch(eq(repository), eq(labelBranch), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.branch(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#branch(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの4つのパラメータがsubversionCoreメソッドの戻り値と等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testBranchCaseError() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelBranch(labelBranch);
		vo.setCommitComment(commitComment);

		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision =1L;
		String author = "testAuthor";
		SVNErrorMessage error = SVNErrorMessage.create(SVNErrorCode.UNSUPPORTED_FEATURE, "サポートなし");
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date, error);

		mockTriValidationHelper.validate(vo, CommitVO.BranchChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.branch(eq(repository), eq(labelBranch), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.branch(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is("svn: サポートなし"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#tagToBranch(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの3つのパラメータがsubversionCoreメソッドの戻り値と等しいこと、ErrorMessageがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testTagToBranch() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String labelTag = "tag";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelBranch(labelBranch);
		vo.setLabelTag(labelTag);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision = 1L;
		String author = "testAuthor";
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date);

		mockTriValidationHelper.validate(vo, CommitVO.TagToBChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.tagToBranch(eq(repository), eq(labelBranch), eq(labelTag), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.tagToBranch(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#tagToBranch(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの4つのパラメータがsubversionCoreメソッドの戻り値と等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testTagToBranchCaseError() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelBranch = "branch";
		String labelTag = "tag";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelBranch(labelBranch);
		vo.setLabelTag(labelTag);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision = 1L;
		String author = "testAuthor";
		SVNErrorMessage error = SVNErrorMessage.create(SVNErrorCode.ENTRY_EXISTS, "エントリーすでにあり");
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date, error);

		mockTriValidationHelper.validate(vo, CommitVO.TagToBChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.tagToBranch(eq(repository), eq(labelBranch), eq(labelTag), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.tagToBranch(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is("svn: エントリーすでにあり"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#tagToTrunk(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの3つのパラメータがsubversionCoreメソッドの戻り値と等しいこと、ErrorMessageがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testTagToTrunk() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelTag = "tag";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelTag(labelTag);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision = 1L;
		String author = "testAuthor";
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date);

		mockTriValidationHelper.validate(vo, CommitVO.TagToTChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.tagToTrunk(eq(repository), eq(labelTag), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.tagToTrunk(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#tagToTrunk(commitVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの4つのパラメータがsubversionCoreメソッドの戻り値と等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testTagToTrunkCaseError() throws Exception {

		CommitVO vo = new CommitVO();
		String repository = "svn://repository\\";
		String labelTag = "tag";
		String commitComment = "commit comment";

		vo.setRepository(repository);
		vo.setLabelTag(labelTag);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/2/28");
		Long revision = 1L;
		String author = "testAuthor";

		SVNErrorMessage error = SVNErrorMessage.create(SVNErrorCode.WC_INVALID_SCHEDULE, "無効なスケジュール");
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date, error);

		mockTriValidationHelper.validate(vo, CommitVO.TagToTChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.tagToTrunk(eq(repository), eq(labelTag), eq(commitComment));
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.tagToTrunk(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is("svn: 無効なスケジュール"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#statusSingle(StatusVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの4つのパラメータがsubversionCoreメソッドの戻り値と等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testStatusSingle() throws Exception {

		StatusVO vo = new StatusVO();
		String pathDest = "c:\\dest\\";
		String objectPath = "object";

		vo.setPathDest(pathDest);
		vo.setObjectPath(objectPath);

		mockTriValidationHelper.validate(vo, StatusVO.SingleChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.statusSingle(eq(pathDest), eq(objectPath), isA(StatusHandler.class));

		expectLastCall().andAnswer(new IAnswer<Void>() {

			@Override
			public Void answer() throws Throwable {
				Object[] args = getCurrentArguments();
				StatusHandler handler = (StatusHandler) args[2];
				handler.handleStatus(crteateSVNStatus());
				return null;
			}

		});

		replayAll();
		SVNStatus actual = testSS.statusSingle(vo);
		verifyAll();
		assertThat(actual, is(notNullValue()));
	}

	private SVNStatus crteateSVNStatus() {
		File myFile = new File("c:\\java", "file.txt");
		return new SVNStatus(null, myFile, null, null, null, null, null, null, null, null, null, false, false, false, false, null, null, null, null,
				null, null, null, null, null, null, 0, null);
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#statusSingle(StatusVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testStatusSingleCaseStatusListNull() throws Exception {

		StatusVO vo = new StatusVO();
		String pathDest = "c:\\dest\\";
		String objectPath = "object";

		vo.setPathDest(pathDest);
		vo.setObjectPath(objectPath);

		mockTriValidationHelper.validate(vo, StatusVO.SingleChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.statusSingle(eq(pathDest), eq(objectPath), isA(StatusHandler.class));
		expectLastCall();

		replayAll();
		SVNStatus actual = testSS.statusSingle(vo);
		verifyAll();
		assertThat(actual, is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#statusSingle(StatusVO)}
	 * のためのテスト・メソッド。ScmExceptionのテストです
	 * subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 *
	 * @throws Exception
	 */
	@Test(expected = ScmException.class)
	public void testStatusSingleCaseException() throws Exception {

		StatusVO vo = new StatusVO();
		String pathDest = "c:\\dest\\";
		String objectPath = "object";

		vo.setPathDest(pathDest);
		vo.setObjectPath(objectPath);

		mockTriValidationHelper.validate(vo, StatusVO.SingleChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.statusSingle(eq(pathDest), eq(objectPath), isA(StatusHandler.class));
		expectLastCall().andThrow(new SVNException(SVNErrorMessage.create(SVNErrorCode.APMOD_ACTIVITY_NOT_FOUND)));

		replayAll();
		testSS.statusSingle(vo);
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#statusAll(StatusVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがnullでないこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testStatusAll() throws Exception {

		StatusVO vo = new StatusVO();
		String pathDest = "c:\\dest\\";
		String moduleName = "module";

		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);

		mockTriValidationHelper.validate(vo, StatusVO.AllChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.statusAll(eq(pathDest), eq(moduleName), isA(StatusHandler.class));
		expectLastCall().andAnswer(new IAnswer<Void>() {

			@Override
			public Void answer() throws Throwable {
				Object[] args = getCurrentArguments();
				StatusHandler handler = (StatusHandler) args[2];
				handler.handleStatus(crteateSVNStatus());
				return null;
			}

		});

		replayAll();
		Map<String, SVNStatus> actual = testSS.statusAll(vo);
		verifyAll();
		assertThat(actual, is(notNullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#statusAll(StatusVO)}
	 * のためのテスト・メソッド。戻り値がnullのテスト subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testStatusAllCaseReturnNull() throws Exception {

		StatusVO vo = new StatusVO();
		String pathDest = "c:\\dest\\";
		String moduleName = "module";

		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);

		mockTriValidationHelper.validate(vo, StatusVO.AllChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.statusAll(eq(pathDest), eq(moduleName), isA(StatusHandler.class));
		expectLastCall();

		replayAll();
		Map<String, SVNStatus> actual = testSS.statusAll(vo);
		verifyAll();
		assertThat(actual, is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#add(StatusVO)}
	 * のためのテスト・メソッド。 subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualのlengthがfilePathArrayのlengthと等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testAdd() throws Exception {

		AddVO vo = new AddVO();
		String pathDest = "c:\\dest\\";
		String moduleName = "module\\";
		String[] filePathArray = { "\\file\\Path\\Array" };
		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);
		vo.setFilePathArray(filePathArray);

		mockTriValidationHelper.validate(vo);
		expectLastCall().andReturn(Collections.emptySet());

		mockTriValidationHelper.getMessageParamValue(MessageParameter.PATH_ADD_ASSETS);
		expectLastCall().andReturn("");

		mockSubversionCore.add(capture(capturedStrAry));
		expectLastCall();

		replayAll();
		testSS.add(vo);
		verifyAll();
		assertThat(capturedStrAry.getValue().length, is(filePathArray.length));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#remove(DeleteVO)}
	 * のためのテスト・メソッド。 deleteの引数が正しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testRemove() throws Exception {

		DeleteVO vo = new DeleteVO();
		String pathDest = "c:\\dest\\";
		String moduleName = "module\\";
		String[] filePathArray = { "\\file\\Path\\Array" };
		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);
		vo.setFilePathArray(filePathArray);

		mockTriValidationHelper.validate(vo);
		expectLastCall().andReturn(Collections.emptySet());

		mockTriValidationHelper.getMessageParamValue(MessageParameter.PATH_DELETE_ASSETS);
		expectLastCall().andReturn("");

		mockSubversionCore.delete(capture(capturedStr1));
		expectLastCall();

		replayAll();
		testSS.remove(vo);
		verifyAll();
		assertThat(capturedStr1.getValue(), is("c:/dest/module/file/Path/Array"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#commit(CommitVO)}
	 * のためのテスト・メソッド。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの3つのパラメータがsubversionCoreメソッドの戻り値と等しいこと、ErrorMessageがnullであること
	 *
	 * @throws Exception
	 */
	@Test
	public void testCommit() throws Exception {

		CommitVO vo = new CommitVO();
		String pathDest = "c:\\dest\\";
		String[] moduleNameArray = { "moduleName" };
		String commitComment = "commit comment";
		vo.setPathDest(pathDest);
		vo.setModuleNameArray(moduleNameArray);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/3/3");
		Long revision = 1L;
		String author = "testAuthor";
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date);

		mockTriValidationHelper.validate(vo,CommitVO.CommitChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockTriValidationHelper.getMessageParamValue(MessageParameter.MODULE_NAME_ARRAY);
		expectLastCall().andReturn("");

		mockSubversionCore.commit(pathDest, moduleNameArray, commitComment);
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.commit(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is(nullValue()));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#commit(CommitVO)}
	 * のためのテスト・メソッド。エラーメッセージのテストです。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualの4つのパラメータがsubversionCoreメソッドの戻り値と等しいこと
	 *
	 * @throws Exception
	 */
	@Test
	public void testCommitCaseErrorMessege() throws Exception {

		CommitVO vo = new CommitVO();
		String pathDest = "c:\\dest\\";
		String[] moduleNameArray = { "moduleName" };
		String commitComment = "commit comment";
		vo.setPathDest(pathDest);
		vo.setModuleNameArray(moduleNameArray);
		vo.setCommitComment(commitComment);
		Date date = DateFormat.getDateInstance().parse("2014/3/3");
		Long revision = 1L;
		String author = "testAuthor";
		SVNErrorMessage error = SVNErrorMessage.create(SVNErrorCode.ENTRY_EXISTS, "エントリーすでにあり");
		SVNCommitInfo info = new SVNCommitInfo(revision, author, date, error);

		mockTriValidationHelper.validate(vo,CommitVO.CommitChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockTriValidationHelper.getMessageParamValue(MessageParameter.MODULE_NAME_ARRAY);
		expectLastCall().andReturn("");

		mockSubversionCore.commit(pathDest, moduleNameArray, commitComment);
		expectLastCall().andReturn(info);

		replayAll();
		VcsCommitInfo actual = testSS.commit(vo);
		verifyAll();
		assertThat(actual.getNewRevision(), is(revision));
		assertThat(actual.getAuthor(), is(author));
		assertThat(actual.getDate(), is(date));
		assertThat(actual.getErrorMessage(), is("svn: エントリーすでにあり"));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#update(UpdateVO)}
	 * のためのテスト・メソッド。エラーメッセージのテストです。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがrevisionNoと一致すること
	 *
	 * @throws Exception
	 */
	@Test
	public void testUpdate() throws Exception {

		UpdateVO vo = new UpdateVO();
		String pathDest = "c:\\dest\\";
		String moduleName = "module\\";
		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);
		Long revisionNo = 1L;

		mockTriValidationHelper.validate(vo);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.update(pathDest, moduleName);
		expectLastCall().andReturn(revisionNo);

		replayAll();
		long actual = testSS.update(vo);
		verifyAll();
		assertThat(actual, is(revisionNo));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#chkExistsModuleInTrunk(ListSubDirVO)}
	 * のためのテスト・メソッド。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがmoduleNameSetと一致すること
	 *
	 * @throws Exception
	 */
	@Test
	public void testChkExistsModuleInTrunk() throws Exception {

		ListSubDirVO vo = new ListSubDirVO();
		String repository = "svn://repository\\";
		Set<String> moduleNameSet = new LinkedHashSet<String>();
		moduleNameSet.add("module1");
		moduleNameSet.add("module2");
		vo.setRepository(repository);

		mockTriValidationHelper.validate(vo,ListSubDirVO.ListChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.listSubDir(eq(repository), eq(SvnSubDir.trunk), eq(SVNDepth.IMMEDIATES), isA(DirEntryHandler.class));
		mockSubversionCore.moduleNameSet(isA(DirEntryHandler.class));
		expectLastCall().andReturn(moduleNameSet);

		replayAll();
		Set<String> actual = testSS.chkExistsModuleInTrunk(vo);
		verifyAll();
		assertThat(actual, is(moduleNameSet));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#retriveTrunkRevision(ListSubDirVO)}
	 * のためのテスト・メソッド。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがrevisionNoと一致すること
	 *
	 * @throws Exception
	 */
	@Test
	public void testRetriveTrunkRevision() throws Exception {

		ListSubDirVO vo = new ListSubDirVO();
		String repository = "svn://repository\\";
		Long revisionNo = 1L;
		vo.setRepository(repository);

		mockTriValidationHelper.validate(vo,ListSubDirVO.ListChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.listSubDir(eq(repository), eq(SvnSubDir.trunk), eq(SVNDepth.EMPTY), isA(DirEntryHandler.class));
		mockSubversionCore.revisionFromHandler(isA(DirEntryHandler.class));
		expectLastCall().andReturn(revisionNo);

		replayAll();
		long actual = testSS.retriveTrunkRevision(vo);
		verifyAll();
		assertThat(actual, is(revisionNo));
	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#chkExistsTag(ListSubDirVO)}
	 * のためのテスト・メソッド。subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがcheckResultと一致すること
	 *
	 * @throws Exception
	 */
	@Test
	public void testChkExistsTag() throws Exception {

		ListSubDirVO vo = new ListSubDirVO();
		String repository = "svn://repository\\";
		String versionTag = "tag";
		boolean checkResult = true;
		vo.setRepository(repository);
		vo.setVersionTag(versionTag);

		mockTriValidationHelper.validate(vo,ListSubDirVO.ChkTagChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.listSubDir(eq(repository), eq(SvnSubDir.tags), eq(SVNDepth.IMMEDIATES), isA(DirEntryHandler.class));
		mockSubversionCore.checkTag(eq(versionTag), isA(DirEntryHandler.class));
		expectLastCall().andReturn(checkResult);

		replayAll();
		boolean actual = testSS.chkExistsTag(vo);
		verifyAll();
		assertThat(actual, is(checkResult));

	}

	/**
	 * {@link jp.co.blueship.tri.fw.vcs.svn.SvnService#chkExistsTag(ListSubDirVO)}
	 * のためのテスト・メソッド。falseを返すテスト subversionCoreメソッドの引数とValueObjectの配置関係が正しいこと
	 * actualがcheckResultと一致すること
	 *
	 * @throws Exception
	 */
	@Test
	public void testChkExistsTagCaseFalse() throws Exception {

		ListSubDirVO vo = new ListSubDirVO();
		String repository = "svn://repository\\";
		String versionTag = "tag";
		boolean checkResult = false;
		vo.setRepository(repository);
		vo.setVersionTag(versionTag);

		mockTriValidationHelper.validate(vo,ListSubDirVO.ChkTagChecks.class);
		expectLastCall().andReturn(Collections.emptySet());

		mockSubversionCore.listSubDir(eq(repository), eq(SvnSubDir.tags), eq(SVNDepth.IMMEDIATES), isA(DirEntryHandler.class));
		mockSubversionCore.checkTag(eq(versionTag), isA(DirEntryHandler.class));
		expectLastCall().andReturn(checkResult);

		replayAll();
		boolean actual = testSS.chkExistsTag(vo);
		verifyAll();
		assertThat(actual, is(checkResult));

	}

}
