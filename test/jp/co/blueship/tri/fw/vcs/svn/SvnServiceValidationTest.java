package jp.co.blueship.tri.fw.vcs.svn;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.Scope;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.context.support.SimpleThreadScope;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tmatesoft.svn.core.wc.SVNRevision;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.MergeVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;
import jp.co.blueship.tri.fw.vcs.vo.UpdateVO;
import jp.co.blueship.tri.test.util.BeanValidationTestHelper;

/**
 * VCSアクセスで使用するVOのValidationテストクラスです。
 *
 * @author Takayuki Kubo
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Fw-VcsService-Context.xml", "classpath:Fw-MessageContents-Context.xml" })
public class SvnServiceValidationTest {

	private IVcsService testee;

	@Autowired
	private GenericApplicationContext applicationContext;

	/**
	 * 事前準備を行います。
	 */
	@Before
	public void setUp() {

		Contexts.getInstance().setApplicationContext(applicationContext);

		ConfigurableListableBeanFactory beanFactory = applicationContext.getBeanFactory();
		Scope requestScope = new SimpleThreadScope();
		beanFactory.registerScope("request", requestScope);

		testee = (IVcsService) ContextAdapterFactory.getContextAdapter().getBean("svnService");
	}

	/**
	 * {@link SvnService#checkoutHead(CheckOutVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testCheckoutHead() throws Exception {

		BeanValidationTestHelper<CheckOutVO, ScmException> bvth = new BeanValidationTestHelper<CheckOutVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CheckOutVO inputVo) throws Exception {
				testee.checkoutHead(inputVo);
			}
		};

		CheckOutVO input = createCheckOutVOCaseDefault();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseDefault();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);
	}

	private CheckOutVO createCheckOutVOCaseDefault() {

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository("repository");
		vo.setModuleName("moduleName");
		vo.setPathDest("path");

		return vo;
	}

	/**
	 * {@link SvnService#checkoutBranch(CheckOutVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testCheckoutBranch() throws Exception {

		BeanValidationTestHelper<CheckOutVO, ScmException> bvth = new BeanValidationTestHelper<CheckOutVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CheckOutVO inputVo) throws Exception {
				testee.checkoutBranch(inputVo);
			}
		};

		CheckOutVO input = createCheckOutVOCaseChkBranch();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// LabelBranchを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setLabelBranch(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelBranchが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseChkBranch();
		input.setLabelBranch("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);
	}

	private CheckOutVO createCheckOutVOCaseChkBranch() {

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository("repository");
		vo.setModuleName("moduleName");
		vo.setPathDest("path");
		vo.setLabelBranch("branch");

		return vo;
	}

	/**
	 * {@link SvnService#checkoutBranchRevision(CheckOutVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testCheckoutBranchRevision() throws Exception {

		BeanValidationTestHelper<CheckOutVO, ScmException> bvth = new BeanValidationTestHelper<CheckOutVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CheckOutVO inputVo) throws Exception {
				testee.checkoutBranchRevision(inputVo);
			}
		};

		CheckOutVO input = createCheckOutVOCaseRevision();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// LabelBranchを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setLabelBranch(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelBranchが空文字の場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setLabelBranch("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// RevisionNoを省略した場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setRevisionNo(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004097F, MessageParameter.REVISION_NO);

		// RevisionNoが0以下の場合、エラーとなることを確認する
		input = createCheckOutVOCaseRevision();
		input.setRevisionNo(0L);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004097F, MessageParameter.REVISION_NO);
	}

	private CheckOutVO createCheckOutVOCaseRevision() {

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository("repository");
		vo.setModuleName("moduleName");
		vo.setPathDest("path");
		vo.setLabelBranch("branch");
		vo.setRevisionNo(1L);
		return vo;
	}

	/**
	 * {@link SvnService#merge(MergeVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testMerge() throws Exception {

		BeanValidationTestHelper<MergeVO, ScmException> bvth = new BeanValidationTestHelper<MergeVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(MergeVO inputVo) throws Exception {
				testee.merge(inputVo);
			}
		};

		MergeVO input = createMergeVO();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createMergeVO();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// LabelBranchを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setLabelBranch(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelBranchが空文字の場合、エラーとなることを確認する
		input = createMergeVO();
		input.setLabelBranch("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createMergeVO();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// MergeFromを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setMergeFrom(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MERGE_FROM);

		// MergeToを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setMergeTo(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MERGE_TO);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createMergeVO();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createMergeVO();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

	}

	private MergeVO createMergeVO() {

		MergeVO vo = new MergeVO();
		vo.setRepository("repository");
		vo.setLabelBranch("branch");
		vo.setModuleName("moduleName");
		vo.setMergeFrom(SVNRevision.BASE);
		vo.setMergeTo(SVNRevision.HEAD);
		vo.setPathDest("path");
		return vo;
	}

	/**
	 * {@link SvnService#branch(CommitVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testBranch() throws Exception {

		BeanValidationTestHelper<CommitVO, ScmException> bvth = new BeanValidationTestHelper<CommitVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CommitVO inputVo) throws Exception {
				testee.branch(inputVo);
			}
		};

		CommitVO input = createCommitVOCaseBranch();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseBranch();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseBranch();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// LabelBranchを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseBranch();
		input.setLabelBranch(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelBranchが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseBranch();
		input.setLabelBranch("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

	}

	private CommitVO createCommitVOCaseBranch() {

		CommitVO vo = new CommitVO();
		vo.setRepository("repository");
		vo.setLabelBranch("branch");
		return vo;
	}

	/**
	 * {@link SvnService#tagToBranch(CommitVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testTagToBranch() throws Exception {

		BeanValidationTestHelper<CommitVO, ScmException> bvth = new BeanValidationTestHelper<CommitVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CommitVO inputVo) throws Exception {
				testee.tagToBranch(inputVo);
			}
		};

		CommitVO input = createCommitVOCaseTagToB();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// LabelBranchを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setLabelBranch(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelBranchが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setLabelBranch("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_BRANCH);

		// LabelTagを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setLabelTag(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_TAG);

		// LabelTagが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseTagToB();
		input.setLabelTag("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_TAG);

	}

	private CommitVO createCommitVOCaseTagToB() {

		CommitVO vo = new CommitVO();
		vo.setRepository("repository");
		vo.setLabelBranch("branch");
		vo.setLabelTag("tag");
		return vo;
	}

	/**
	 * {@link SvnService#tagToTrunk(CommitVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testTagToTrunk() throws Exception {

		BeanValidationTestHelper<CommitVO, ScmException> bvth = new BeanValidationTestHelper<CommitVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CommitVO inputVo) throws Exception {
				testee.tagToTrunk(inputVo);
			}
		};

		CommitVO input = createCommitVOCaseTagToT();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseTagToT();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseTagToT();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// LabelTagを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseTagToT();
		input.setLabelTag(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_TAG);

		// LabelTagが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseTagToT();
		input.setLabelTag("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.LABEL_TAG);

	}

	private CommitVO createCommitVOCaseTagToT() {

		CommitVO vo = new CommitVO();
		vo.setRepository("repository");
		vo.setLabelTag("tag");
		return vo;
	}

	/**
	 * {@link SvnService#statusSingle(StatusVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testStatsuSingle() throws Exception {

		BeanValidationTestHelper<StatusVO, ScmException> bvth = new BeanValidationTestHelper<StatusVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(StatusVO inputVo) throws Exception {
				testee.statusSingle(inputVo);
			}
		};

		// エラーとならないことを確認する
		StatusVO input = createStatusVOCaseSingle();
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseSingle();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createStatusVOCaseSingle();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseSingle();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createStatusVOCaseSingle();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

	}

	private StatusVO createStatusVOCaseSingle() {

		StatusVO vo = new StatusVO();
		vo.setPathDest("pathDest");
		vo.setModuleName("module");
		return vo;
	}

	/**
	 * {@link SvnService#statusAll(StatusVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testStatusAll() throws Exception {

		BeanValidationTestHelper<StatusVO, ScmException> bvth = new BeanValidationTestHelper<StatusVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(StatusVO inputVo) throws Exception {
				testee.statusAll(inputVo);
			}
		};

		// エラーとならないことを確認する
		StatusVO input = createStatusVOCaseAll();
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

	}

	private StatusVO createStatusVOCaseAll() {

		StatusVO vo = new StatusVO();
		vo.setPathDest("pathDest");
		vo.setModuleName("moduleName");
		return vo;
	}

	/**
	 * {@link SvnService#statusAll(StatusVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testMergeStatusAll() throws Exception {

		BeanValidationTestHelper<StatusVO, ScmException> bvth = new BeanValidationTestHelper<StatusVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(StatusVO inputVo) throws Exception {
				testee.mergeStatusAll(inputVo);
			}
		};

		// エラーとならないことを確認する
		StatusVO input = createStatusVOCaseAll();
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ObjectPathを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ObjectPathを省略した場合、エラーとなることを確認する
		input = createStatusVOCaseAll();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

	}

	/**
	 * {@link SvnService#add(AddVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testAdd() throws Exception {

		BeanValidationTestHelper<AddVO, ScmException> bvth = new BeanValidationTestHelper<AddVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(AddVO inputVo) throws Exception {
				testee.add(inputVo);
			}
		};

		AddVO input = createAddVO();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createAddVO();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createAddVO();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createAddVO();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createAddVO();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// FilePathArrayを省略した場合、エラーとなることを確認する
		input = createAddVO();
		input.setFilePathArray(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_ADD_ASSETS);

		// FilePathArrayがEmptyの場合、エラーとなることを確認する
		input = createAddVO();
		String[] moduleNameArray = new String[0];
		input.setFilePathArray(moduleNameArray);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_ADD_ASSETS);

		// FilePathArrayが空文字の場合、エラーとなることを確認する
		input = createAddVO();
		String[] moduleNameArray1 = { "" };
		input.setFilePathArray(moduleNameArray1);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_ADD_ASSETS);

	}

	private AddVO createAddVO() {

		AddVO vo = new AddVO();
		vo.setPathDest("pathDest");
		vo.setModuleName("module");
		String[] file = { "file\\path" };
		vo.setFilePathArray(file);
		return vo;
	}

	/**
	 * {@link SvnService#remove(DeleteVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testRemove() throws Exception {

		BeanValidationTestHelper<DeleteVO, ScmException> bvth = new BeanValidationTestHelper<DeleteVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(DeleteVO inputVo) throws Exception {
				testee.remove(inputVo);
			}
		};

		DeleteVO input = createDeleteVO();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createDeleteVO();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createDeleteVO();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createDeleteVO();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createDeleteVO();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// FilePathArrayを省略した場合、エラーとなることを確認する
		input = createDeleteVO();
		//input.setFilePathArray(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DELETE_ASSETS);

		// FilePathArrayがEmptyの場合、エラーとなることを確認する
		input = createDeleteVO();
		String[] moduleNameArray = new String[0];
		input.setFilePathArray(moduleNameArray);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DELETE_ASSETS);

		// FilePathArrayが空文字の場合、エラーとなることを確認する
		input = createDeleteVO();
		String[] moduleNameArray1 = { "" };
		input.setFilePathArray(moduleNameArray1);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DELETE_ASSETS);

	}

	private DeleteVO createDeleteVO() {

		DeleteVO vo = new DeleteVO();
		vo.setPathDest("pathDest");
		vo.setModuleName("module");
		String[] file = { "file\\path" };
		vo.setFilePathArray(file);
		return vo;
	}

	/**
	 * {@link SvnService#commit(CommitVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testCommit() throws Exception {

		BeanValidationTestHelper<CommitVO, ScmException> bvth = new BeanValidationTestHelper<CommitVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(CommitVO inputVo) throws Exception {
				testee.commit(inputVo);
			}
		};

		CommitVO input = createCommitVOCaseCommit();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseCommit();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseCommit();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameArrayを省略した場合、エラーとなることを確認する
		input = createCommitVOCaseCommit();
		input.setModuleNameArray(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME_ARRAY);

		// ModuleNameArrayがEmptyの場合、エラーとなることを確認する
		input = createCommitVOCaseCommit();
		String[] moduleNameArray = new String[0];
		input.setModuleNameArray(moduleNameArray);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME_ARRAY);

		// ModuleNameArrayが空文字の場合、エラーとなることを確認する
		input = createCommitVOCaseCommit();
		String[] moduleNameArray1 = { "" };
		input.setModuleNameArray(moduleNameArray1);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME_ARRAY);
	}

	private CommitVO createCommitVOCaseCommit() {

		CommitVO vo = new CommitVO();
		vo.setPathDest("pathDest");
		String[] nameArray = { "moduleName" };
		vo.setModuleNameArray(nameArray);
		return vo;
	}

	/**
	 * {@link SvnService#update(UpdateVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testUpdate() throws Exception {

		BeanValidationTestHelper<UpdateVO, ScmException> bvth = new BeanValidationTestHelper<UpdateVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(UpdateVO inputVo) throws Exception {
				testee.update(inputVo);
			}
		};

		UpdateVO input = createUpdateVO();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// PathDestを省略した場合、エラーとなることを確認する
		input = createUpdateVO();
		input.setPathDest(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// PathDestが空文字の場合、エラーとなることを確認する
		input = createUpdateVO();
		input.setPathDest("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.PATH_DEST);

		// ModuleNameを省略した場合、エラーとなることを確認する
		input = createUpdateVO();
		input.setModuleName(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

		// ModuleNameが空文字の場合、エラーとなることを確認する
		input = createUpdateVO();
		input.setModuleName("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.MODULE_NAME);

	}

	private UpdateVO createUpdateVO() {

		UpdateVO vo = new UpdateVO();
		vo.setPathDest("pathDest");
		vo.setModuleName("module");
		return vo;
	}

	/**
	 * {@link SvnService#chkExistsModuleInTrunk(ListSubDirVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testChkExistsModuleInTrunk() throws Exception {

		BeanValidationTestHelper<ListSubDirVO, ScmException> bvth = new BeanValidationTestHelper<ListSubDirVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(ListSubDirVO inputVo) throws Exception {
				testee.chkExistsModuleInTrunk(inputVo);
			}
		};

		ListSubDirVO input = createListSubDirVOCaseList();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createListSubDirVOCaseList();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createListSubDirVOCaseList();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);
	}

	private ListSubDirVO createListSubDirVOCaseList() {

		ListSubDirVO vo = new ListSubDirVO();
		vo.setRepository("repository");
		return vo;
	}

	/**
	 * {@link SvnService#retriveTrunkRevision(ListSubDirVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testRetriveTrunkRevision() throws Exception {

		BeanValidationTestHelper<ListSubDirVO, ScmException> bvth = new BeanValidationTestHelper<ListSubDirVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(ListSubDirVO inputVo) throws Exception {
				testee.retriveTrunkRevision(inputVo);
			}
		};

		ListSubDirVO input = createListSubDirVOCaseList();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createListSubDirVOCaseList();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createListSubDirVOCaseList();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);
	}

	/**
	 * {@link SvnService#chkExistsTag(ListSubDirVO)}メソッドのバリデーションテスト。
	 *
	 * @throws Exception 予期しない例外が発生した場合。
	 */
	@Test
	public void testChkExistsTag() throws Exception {

		BeanValidationTestHelper<ListSubDirVO, ScmException> bvth = new BeanValidationTestHelper<ListSubDirVO, ScmException>() {

			@Override
			public void callTesteeOnValidation(ListSubDirVO inputVo) throws Exception {
				testee.chkExistsTag(inputVo);
			}
		};

		ListSubDirVO input = createListSubDirVOCaseChkTag();

		// エラーとならないことを確認する
		bvth.verifyNoValidationError(input);

		// Repositoryを省略した場合、エラーとなることを確認する
		input = createListSubDirVOCaseChkTag();
		input.setRepository(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// Repositoryが空文字の場合、エラーとなることを確認する
		input = createListSubDirVOCaseChkTag();
		input.setRepository("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.REPOSITORY);

		// VersionTagを省略した場合、エラーとなることを確認する
		input = createListSubDirVOCaseChkTag();
		input.setVersionTag(null);
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.VERSION_TAG);

		// VersitonTagが空文字の場合、エラーとなることを確認する
		input = createListSubDirVOCaseChkTag();
		input.setVersionTag("");
		bvth.verifyValidation(input, ScmException.class, SmMessageId.SM004096F, MessageParameter.VERSION_TAG);
	}

	private ListSubDirVO createListSubDirVOCaseChkTag() {

		ListSubDirVO vo = new ListSubDirVO();
		vo.setRepository("repository");
		vo.setVersionTag("tag");
		return vo;
	}
}
