package jp.co.blueship.tri.fw.vcs.svn;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.vcs.svn.constants.SvnSubDir;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.wc.SVNStatus;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Test-Trinity-WebApp-Context.xml", "classpath:Fw-Module-Context.xml",
		"classpath:Cha-WebApp-Context.xml" })
public class SubversionCoreConnectivityTest {

	private SubversionCore testee;

	@Before
	public void setUp() {

		SvnKitProxy proxy = new SvnKitProxy("admin", "admin");
		testee = new SubversionCore();
		testee.setSvnKitProxy(proxy);
	}

	@Ignore
	@Test
	public void testStatusSingle() throws Exception {

		StatusHandler statusHandler = new StatusHandler(true);
		testee.statusSingle("C:\\trinity-work-folder\\trunk", "testResources\\test.txt", statusHandler);

		List<SVNStatus> actual = statusHandler.getSvnStatusList();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));

	}

	@Ignore
	@Test
	public void testCheckoutHead() throws Exception {

		testee.checkoutTrunkHead("http://localhost:18080/svn/trinity-test", "testResources", "C:\\trinity-work-folder");
	}

	@Ignore
	@Test
	public void testListSubDir() throws Exception {

		DirEntryHandler handler = new DirEntryHandler();
		testee.listSubDir("http://localhost:18080/svn/trinity-test", SvnSubDir.trunk, SVNDepth.IMMEDIATES, handler);

	}
}
