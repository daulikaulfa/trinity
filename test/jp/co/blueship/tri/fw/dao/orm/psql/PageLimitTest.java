package jp.co.blueship.tri.fw.dao.orm.psql;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

public class PageLimitTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-Design-Context.xml" };

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	//@Ignore
	@Test
	public void test() {
		List<String> testList = new ArrayList<String>();
		Collections.addAll(testList, new String[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25"});

		ILimit limit = new PageLimit();

		limit.setMaxRows( 25 );
		limit.setViewRows( 10 );

		for ( int selectedPageNo: new Integer[]{1,2,3,4} ) {
			limit.getPageBar().setValue( selectedPageNo );

			System.out.println("(" + selectedPageNo + " page)");
			for ( String value: limit.getLimit(testList)  ) {
				System.out.println("  " + value);
			}

			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit);
			System.out.print("  page info ");
			System.out.print("  MaxPageNo:= " + page.getMaxPageNo());
			System.out.print("  MaxRows:= " + page.getMaxRows());
			System.out.print("  ViewRows:= " + page.getViewRows());
			System.out.print("  SelectPageNo:= " + page.getSelectPageNo());
			System.out.print("  ViewRangeFrom:= " + page.getViewRangeFrom());
			System.out.print("  ViewRangeTo:= " + page.getViewRangeTo());

			System.out.println("");
			System.out.println("=================================");
		}

	}

}
