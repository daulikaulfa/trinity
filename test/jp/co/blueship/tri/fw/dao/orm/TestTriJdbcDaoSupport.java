package jp.co.blueship.tri.fw.dao.orm;

import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;

import org.junit.Before;

/**
 * DAO(JDBC) Framework Operation check
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class TestTriJdbcDaoSupport {

	private Contexts ac;

	@Before
	public void setUp() throws Exception {
		ac = Contexts.getInstance();
		String[] configs = { "classpath:TestDao.xml" };
		ac.initBeanFactory(configs);


		TriAuthenticationContext authContext = (TriAuthenticationContext) getBean(TriAuthenticationContext.BEAN_NAME);
		authContext.setAuthUser("trinity", "trinity");

	}

	protected final Object getBean( String beanName ) {
		return ac.getBeanFactory().getBean( beanName );
	}
}
