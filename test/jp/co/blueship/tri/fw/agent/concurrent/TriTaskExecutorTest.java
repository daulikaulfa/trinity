package jp.co.blueship.tri.fw.agent.concurrent;

import static org.easymock.EasyMock.expectLastCall;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.fw.constants.SmDesignEntryKeyByTask;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Before;
import org.junit.Test;

public class TriTaskExecutorTest extends TriMockTestSupport {

	private TriTaskExecutor testee;

	@Before
	public void setUp() {

		testee = new TriTaskExecutor();

		setMockDesignSheet(new MockDesignSheet() {
			@Override
			public String getValue(IDesignEntryKey key) {

				if (key == SmDesignEntryKeyByTask.maxConcurrentBusinessSequenceNum) {
					return "2";
				}

				throw new UnsupportedOperationException();
			}
		});
	}

	@Test
	public void testSubmit() throws Exception {

		IASyncTask mockASyncTask = createMock(IASyncTask.class);

		mockASyncTask.run();
		expectLastCall();

		replayAll();
		testee.init();
		testee.submit(mockASyncTask);
		Thread.sleep(5000L);
		verifyAll();

		testee.shutDown();

	}

	@Test
	public void testIsActiveCaseTrue() throws Exception {

		IASyncTask mockASyncTask = createMock(IASyncTask.class);

		mockASyncTask.run();
		expectLastCall();

		replayAll();
		testee.init();
		testee.submit(mockASyncTask);
		Thread.sleep(5000L);
		boolean actual = testee.isActive();
		verifyAll();

		assertThat(actual, is(true));

		testee.shutDown();
	}

	@Test
	public void testIsActiveCaseFalse() throws Exception {

		IASyncTask mockASyncTask = createMock(IASyncTask.class);

		mockASyncTask.run();
		expectLastCall();

		replayAll();
		testee.init();
		testee.submit(mockASyncTask);
		Thread.sleep(5000L);
		testee.shutDown();
		boolean actual = testee.isActive();
		verifyAll();

		assertThat(actual, is(false));

	}

}
