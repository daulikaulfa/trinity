package jp.co.blueship.tri.fw.agent.concurrent;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.BuildTaskBean;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.test.support.TriWithDataBaseTestCase;

import org.dbunit.dataset.DefaultDataSet;
import org.dbunit.dataset.IDataSet;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Fw-Module-Context.xml", "classpath:Test-DataSource-Context.xml" })
@TransactionConfiguration
@Transactional
public class TriTaskExecutorConnectivityTest extends TriWithDataBaseTestCase {

	private static final String USER_ID = "admin";
	private static final String USER_NAME = "trinity管理ユーザ";

	@Autowired
	private ApplicationContext applicationContext;

	@Autowired
	@Qualifier("triTaskExecutor")
	private TriTaskExecutor testee;

	@Autowired
	@Qualifier("triAuthenticationContext")
	private TriAuthenticationContext authContext;

	@Before
	public void setUp() throws Exception {

		super.setUp();

		testee = new TriTaskExecutor();
		testee.init();
		authContext.setAuthUser(USER_ID, USER_NAME);
		Contexts.getInstance().setApplicationContext(applicationContext);
	}

	@Override
	protected IDataSet getDataSet() throws Exception {
		return new DefaultDataSet();
	}

	/**
	 * TaskExecuter経由で生成したスレッド配下で動作する非同期タスクに認証情報が伝播することを確認する。
	 *
	 * @throws Exception 予期しない例外発生した。
	 */
	@Test
	public void testInheritableSecurity() throws Exception {

		for (int i = 0; i < 50; i++) {

			BuildTaskBean param = new BuildTaskBean();
			param.setInsertUpdateUserId(USER_ID);

			TestTask task = new TestTask();
			task.setParam(param);
			task.setContext(ContextAdapterFactory.getContextAdapter());

			testee.submit(task);

			Thread.sleep(500);

			String actual = task.getActualUserId();
			assertThat(actual, is(USER_ID));
		}
	}

	/**
	 * 後処理
	 */
	@After
	public void tearDown() throws Exception {

		super.tearDown();
		authContext.clearAuthenticationContext();
		testee.shutDown();
	}

	@Override
	protected String getAuthUserID() {
		return USER_ID;
	}

	private class TestTask extends ASyncTask<IBldTimelineEntity, IBuildTaskBean, IBldTimelineAgentEntity> {

		private String actualUserId;

		@Override
		protected void execute(IBldTimelineEntity timeLine, IBuildTaskBean param) throws Exception {

			TriAuthenticationContext authContext = (TriAuthenticationContext) getBean(TriAuthenticationContext.BEAN_NAME);
			actualUserId = authContext.getAuthUserID();
		}

		@Override
		protected void execute(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line) throws Exception {
		}

		@Override
		protected void writeProcessByIrregular(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) {
		}

		String getActualUserId() {
			return actualUserId;
		}

	}

}
