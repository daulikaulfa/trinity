package jp.co.blueship.tri.fw.agent.concurrent;

import static org.easymock.EasyMock.expectLastCall;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.sameInstance;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.fw.agent.dto.ITaskBean;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.task.ITaskService;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.test.support.TriMockTestSupport;
import jp.co.blueship.tri.test.util.ThrowExceptionTestHelper;

import org.junit.Before;
import org.junit.Test;

/**
 * [{@link ASyncTask}クラスのユニットテスト
 *
 * @author trinity
 *
 */
public class ASyncTaskTest extends TriMockTestSupport {

	@SuppressWarnings("rawtypes")
	private ASyncTask testee;
	private static final String USER_ID = "userID";
	private static final String USER_NAME = "userName";
	private TriAuthenticationContext mockTriAuthenticationContext;
	private IUmFinderSupport mockUmFinderSupport;

	@Before
	public void setUp() {

		mockTriAuthenticationContext = createMock(TriAuthenticationContext.class);
		addMockBeanByName(TriAuthenticationContext.BEAN_NAME, mockTriAuthenticationContext);
		mockUmFinderSupport = createMock(IUmFinderSupport.class);
		addMockBeanByName("umFinderSupport", mockUmFinderSupport);

		testee = createMockBuilder(ASyncTask.class).//
				addMockedMethod("execute", IEntity.class, ITaskBean.class).//
				addMockedMethod("execute", IEntity.class, ITaskBean.class, IEntity.class).//
				addMockedMethod("writeProcessByIrregular", IEntity.class, ITaskBean.class, IEntity.class, Exception.class).//
				createMock();
	}

	@Test
	public void testContext() {

		IContextAdapter mockIContextAdapter = createMock(IContextAdapter.class);
		testee.setContext(mockIContextAdapter);

		assertThat(testee.context(), is(sameInstance(mockIContextAdapter)));
	}

	@Test
	public void testGetBean() {

		IContextAdapter mockIContextAdapter = createMock(IContextAdapter.class);
		Object mockBeanObject = createMock(Object.class);
		mockIContextAdapter.getBean("beanName");
		expectLastCall().andReturn(mockBeanObject);

		testee.setContext(mockIContextAdapter);

		replayAll();
		Object actual = testee.getBean("beanName");
		verifyAll();

		assertThat(actual, is(sameInstance(mockBeanObject)));
	}

	@Test
	public void testService() {

		ITaskService mockService = createMock(ITaskService.class);
		testee.setService(mockService);

		assertThat(testee.service(), is(sameInstance(mockService)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testTimeLine() {

		IEntity mockTimeLine = createMockOfIEntity();
		testee.setTimeLine(mockTimeLine);

		assertThat(testee.timeLine(), is(sameInstance(mockTimeLine)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testParam() {

		ITaskBean param = createITaskBean();
		testee.setParam(param);

		assertThat(testee.param(), is(sameInstance(param)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testLine() {

		IEntity mockLine = createMockOfIEntity();
		testee.setLine(mockLine);

		assertThat(testee.line(), is(sameInstance(mockLine)));
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRunCaseAllLine() throws Exception {

		IEntity mockTimeLine = createMockOfIEntity();
		ITaskBean param = createITaskBean();
		testee.setTimeLine(mockTimeLine);
		testee.setParam(param);
		testee.setContext(ContextAdapterFactory.getContextAdapter());

		expectCallsOnAuthentication();

		testee.execute(mockTimeLine, param);
		expectLastCall();

		replayAll();
		testee.run();
		verifyAll();

	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRunCaseSingleLine() throws Exception {

		IEntity mockTimeLine = createMockOfIEntity();
		ITaskBean param = createITaskBean();
		IEntity mockLine = createMockOfIEntity();
		testee.setTimeLine(mockTimeLine);
		testee.setParam(param);
		testee.setLine(mockLine);
		testee.setContext(ContextAdapterFactory.getContextAdapter());

		expectCallsOnAuthentication();

		testee.execute(mockTimeLine, param, mockLine);
		expectLastCall();

		replayAll();
		testee.run();
		verifyAll();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRunCaseError() throws Exception {

		IEntity mockTimeLine = createMockOfIEntity();
		ITaskBean param = createITaskBean();
		IEntity mockLine = createMockOfIEntity();
		Exception mockException = createMock(Exception.class);

		testee.setTimeLine(mockTimeLine);
		testee.setParam(param);
		testee.setLine(mockLine);
		testee.setContext(ContextAdapterFactory.getContextAdapter());

		expectCallsOnAuthentication();

		testee.execute(mockTimeLine, param, mockLine);
		expectLastCall().andThrow(mockException);

		testee.writeProcessByIrregular(mockTimeLine, param, mockLine, mockException);
		expectLastCall();

		replayAll();
		testee.run();
		verifyAll();
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRunCaseInsertUpdateUserIdIsNull() throws Exception {

		testee.setParam(createITaskBean(null));
		testee.setContext(ContextAdapterFactory.getContextAdapter());

		mockTriAuthenticationContext.clearAuthenticationContext();
		expectLastCall();

		replayAll();

		ThrowExceptionTestHelper<Void, AssertionError> teth = new ThrowExceptionTestHelper<Void, AssertionError>() {

			@Override
			public void callTesteeOnException(Void input) throws Exception {
				testee.run();
			}
		};

		teth.verifyThrowError(null, AssertionError.class, "UserID is not specified.");

	}

	private void expectCallsOnAuthentication() {

		mockTriAuthenticationContext.setAuthUser(USER_ID, USER_NAME);
		expectLastCall();

		mockUmFinderSupport.findUserByUserId(USER_ID);
		expectLastCall().andReturn(createUserEntity());

		mockTriAuthenticationContext.clearAuthenticationContext();
		expectLastCall();

	}

	private IEntity createMockOfIEntity() {
		return createMock(IEntity.class);
	}

	private ITaskBean createITaskBean() {

		return createITaskBean(USER_ID);
	}

	private ITaskBean createITaskBean(String userID) {

		ITaskBean result = new ITaskBeanStub(userID, null);
		return result;
	}

	private UserEntity createUserEntity() {

		UserEntity result = new UserEntity();
		result.setUserNm(USER_NAME);

		return result;
	}

	private static class ITaskBeanStub implements ITaskBean {

		private static final long serialVersionUID = -1659203212301359412L;

		private String user;
		private String userID;

		private ITaskBeanStub(String userID, String user) {
			this.user = user;
			this.userID = userID;
		}

		@Override
		public String getInsertUpdateUser() {
			return user;
		}

		@Override
		public String getInsertUpdateUserId() {
			return userID;
		}

	}

}
