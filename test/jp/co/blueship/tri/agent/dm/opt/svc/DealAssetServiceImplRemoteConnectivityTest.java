package jp.co.blueship.tri.agent.dm.opt.svc;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.agent.dm.opt.DealAssetServiceController;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetFtpBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * リモートエージェントとして配備されている配布連携に対して各機能を実行するためのテスト。
 *
 * @author Takayuki Kubo
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Test-Release-All-Context.xml", "classpath:Test-Rel-RemoteServiceClientDistribute-Context.xml" })
public class DealAssetServiceImplRemoteConnectivityTest {

	@Autowired
	private DealAssetServiceController testee;

	private static final String ENV = "ＩＴ環境";
	private static final String DRMSMNGNO = "REL-0001";
	private static final String DATE = "20091231120101";

	/**
	 * 「資源登録依頼」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetRegistration() {

		DealAssetServiceBean bean = new DealAssetServiceBean();
		bean.setTargetEnvironment(ENV);
		bean.setDrmsManagementNo(DRMSMNGNO);
		bean.setPath("ccc");
		DealAssetFtpBean ftpBean = new DealAssetFtpBean();
		ftpBean.setFtpServer(StatusFlg.off.value());
		bean.setFtpBean(ftpBean);
		DealAssetServiceResponseBean actual = testee.execute("DealAssetRegistrationService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

	/**
	 * 「資源削除依頼」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetDelete() {

		DealAssetServiceBean bean = new DealAssetServiceBean();
		bean.setTargetEnvironment(ENV);
		bean.setDrmsManagementNo(DRMSMNGNO);

		DealAssetServiceResponseBean actual = testee.execute("DealAssetDeleteService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

	/**
	 * 「登録履歴確認」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetRegistrationHistoryCheck() {

		DealAssetServiceBean bean = new DealAssetServiceBean();
		bean.setTargetEnvironment(ENV);

		DealAssetServiceResponseBean actual = testee.execute("DealAssetRegistrationHistoryCheckService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

	/**
	 * 「タイマー設定」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetDealTimerSet() {

		DealAssetServiceBean bean = new DealAssetServiceBean();
		bean.setTargetEnvironment(ENV);
		bean.setDrmsManagementNo(DRMSMNGNO);
		bean.setTimerDate(DATE);
		bean.setNewOrUpdateFlg("0");
		DealAssetServiceResponseBean actual = testee.execute("DealAssetTimerSetService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

	/**
	 * 「タイマー取消」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetDealTimerCancel() {

		DealAssetServiceBean bean = new DealAssetServiceBean();
		bean.setTargetEnvironment(ENV);
		bean.setDrmsManagementNo(DRMSMNGNO);

		DealAssetServiceResponseBean actual = testee.execute("DealAssetTimerCancelService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

	/**
	 * 「配付状況確認」クライアントのAgent実行テストを行う。
	 */
	@Test
	public void testAssetDealStatusCheck() {

		DealAssetServiceBean bean = new DealAssetServiceBean();

		DealAssetServiceResponseBean actual = testee.execute("DealAssetStatusCheckService", bean);
		assertThat(actual.getReturnCode(), is(0));
	}

}
