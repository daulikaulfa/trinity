package jp.co.blueship.tri.agent.rm.svc.flow;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Before;
import org.junit.Test;

public class RelProcessEntityBuilderTest extends TriMockTestSupport {

	private static final String PROC_ID = "procId";
	private static final String SERVER_ID = "ServerID";
	private static final String ENV_ID = "EnvID";
	private static final Integer LINE_NO = 100;
	private static final String TASK_FLOW_ID = "TaskFlowID";
	private static final Integer TARGET_SEQ_NO = 200;
	private static final String PROC_STS_ID = "ProcStsId";
	private static final String EMPTY = "";

	private RelProcessEntityBuilder testee;

	private IBldTimelineEntity bldTimelineEntity;
	private IBldTimelineAgentEntity line;

	@Before
	public void setUp() {

		bldTimelineEntity = createBuildTimeLineEntity();
		line = createLineEntity();

	}

	@Test
	public void testGetEntity() {

		testee = new RelProcessEntityBuilder(bldTimelineEntity, line);
		testee.setProcId(PROC_ID);
		testee.setMessage("Message");
		testee.setProcStatusId(PROC_STS_ID);
		testee.setCompStsId(StatusFlg.on);

		replayAll();
		ITaskFlowProcEntity actual = testee.getEntity();
		verifyAll();

		assertThat(actual.getProcId(), is(PROC_ID));
		assertThat(actual.getBldSrvId(), is(SERVER_ID));
		assertThat(actual.getBldEnvId(), is(ENV_ID));
		assertThat(actual.getBldLineNo(), is(LINE_NO));
		assertThat(actual.getTaskFlowId(), is(TASK_FLOW_ID));
		assertThat(actual.getTargetSeqNo(), is(TARGET_SEQ_NO));
		assertThat(actual.getStsId(), is(PROC_STS_ID));
		assertThat(actual.getMsgId(), is(EMPTY));
		assertThat(actual.getMsg(), is("Message"));
		assertThat(actual.getCompStsId(), is(StatusFlg.on));
		assertThat(actual.getProcStTimestamp(), is(notNullValue()));
		assertThat(actual.getProcEndTimestamp(), is(notNullValue()));

	}

	@Test
	public void testGetEntityCaseLineIsNull() {

		testee = new RelProcessEntityBuilder(bldTimelineEntity, null);
		testee.setProcId(PROC_ID);
		testee.setMessage("Message");
		testee.setProcStatusId(PROC_STS_ID);
		testee.setCompStsId(StatusFlg.on);

		replayAll();
		ITaskFlowProcEntity actual = testee.getEntity();
		verifyAll();

		assertThat(actual.getProcId(), is(PROC_ID));
		assertThat(actual.getBldSrvId(), is(EMPTY));
		assertThat(actual.getBldEnvId(), is(ENV_ID));
		assertThat(actual.getBldLineNo(), is(LINE_NO));
		assertThat(actual.getTaskFlowId(), is(EMPTY));
		assertThat(actual.getTargetSeqNo(), is(0));
		assertThat(actual.getStsId(), is(PROC_STS_ID));
		assertThat(actual.getMsgId(), is(EMPTY));
		assertThat(actual.getMsg(), is("Message"));
		assertThat(actual.getCompStsId(), is(StatusFlg.on));
		assertThat(actual.getProcStTimestamp(), is(notNullValue()));
		assertThat(actual.getProcEndTimestamp(), is(notNullValue()));
	}

	@Test
	public void testGetEntityCaseNoMessage() {

		testee = new RelProcessEntityBuilder(bldTimelineEntity, null);

		replayAll();
		ITaskFlowProcEntity actual = testee.getEntity();
		verifyAll();

		assertThat(actual.getMsg(), is(EMPTY));
	}

	private IBldTimelineEntity createBuildTimeLineEntity() {

		IBldTimelineEntity result = new BldTimelineEntity();
		result.setBldEnvId(ENV_ID);
		result.setBldLineNo(LINE_NO);

		return result;
	}

	private IBldTimelineAgentEntity createLineEntity() {

		IBldTimelineAgentEntity result = bldTimelineEntity.newLineEntity();

		result.setBldSrvId(SERVER_ID);
		result.setTaskFlowId(TASK_FLOW_ID);
		result.setTargetSeqNo(TARGET_SEQ_NO);

		return result;
	}

}
