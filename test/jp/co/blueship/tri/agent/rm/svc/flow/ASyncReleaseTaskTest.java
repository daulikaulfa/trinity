package jp.co.blueship.tri.agent.rm.svc.flow;

import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link ASyncReleaseTask}クラスのユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class ASyncReleaseTaskTest extends TriMockTestSupport {
//
//	private static final String PROC_ID = "procId";
//	private static final String SERVER_ID = "ServerID";
//	private static final String ENV_ID = "EnvID";
//	private static final Integer LINE_NO = 100;
//	private static final String TASK_FLOW_ID = "TaskFlowID";
//	private static final Integer TARGET_SEQ_NO = 200;
//
//	private ASyncReleaseTask testee;
//
//	private IContextAdapter mockContextAdapter;
//	private IReleaseTaskService mockReleaseTaskService;
//	private ITaskFlowProcDao mockRelProcessDao;
//	private IBldTimelineEntity bldTimelineEntity;
//	private IReleaseTaskBean releaseTaskBean;
//	private IBldTimelineAgentEntity line;
//
//	private Capture<TaskFlowProcEntity> capturedRelProcessEntity;
//
//	@Before
//	public void setUp() {
//
//		testee = new ASyncReleaseTask();
//
//		mockContextAdapter = createMock(IContextAdapter.class);
//		mockReleaseTaskService = createMock(IReleaseTaskService.class);
//		testee.setService(mockReleaseTaskService);
//		mockRelProcessDao = createMock(ITaskFlowProcDao.class);
//		//testee.setReleaseProcessDao(mockRelProcessDao);
//		bldTimelineEntity = createBuildTimeLineEntity();
//		releaseTaskBean = createBuildTaskBean();
//		line = createLineEntity();
//
//		capturedRelProcessEntity = new Capture<TaskFlowProcEntity>();
//	}
//
//	@Test
//	public void testExecute() throws Exception {
//
//		mockReleaseTaskService.execute(bldTimelineEntity, releaseTaskBean);
//		expectLastCall();
//
//		replayAll();
//		testee.execute(bldTimelineEntity, releaseTaskBean);
//		verifyAll();
//
//	}
//
//	@Test
//	public void testExecuteWithLineEntity() throws Exception {
//
//		mockReleaseTaskService.execute(bldTimelineEntity, releaseTaskBean, line);
//		expectLastCall();
//
//		replayAll();
//		testee.execute(bldTimelineEntity, releaseTaskBean, line);
//		verifyAll();
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseInsert() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockRelProcessDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(null);
//
//		mockRelProcessDao.insert(capture(capturedRelProcessEntity));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, releaseTaskBean, line, new Exception("Test Message"));
//		verifyAll();
//
//		TaskFlowProcEntity actualRelProcessEntity = capturedRelProcessEntity.getValue();
//		assertThat(actualRelProcessEntity.getProcId(), is(PROC_ID));
//		assertThat(actualRelProcessEntity.getMsg(), is(notNullValue()));
//		assertThat(actualRelProcessEntity.getMsg().indexOf("Test Message"), is(0));
//		assertThat(actualRelProcessEntity.getStsId(), is(BmBpStatusIdForExecData.BUILD_ERROR.getStatusId()));
//
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseUpdate() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockRelProcessDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(Arrays.asList(new TaskFlowProcEntity()));
//
//		mockRelProcessDao.update(capture(capturedRelProcessEntity));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, releaseTaskBean, line, new Exception("Test Message"));
//		verifyAll();
//
//		TaskFlowProcEntity actualRelProcessEntity = capturedRelProcessEntity.getValue();
//		assertThat(actualRelProcessEntity.getProcId(), is(PROC_ID));
//		assertThat(actualRelProcessEntity.getMsg(), is(notNullValue()));
//		assertThat(actualRelProcessEntity.getMsg().indexOf("Test Message"), is(0));
//		assertThat(actualRelProcessEntity.getStsId(), is(BmBpStatusIdForExecData.BUILD_ERROR.getStatusId()));
//
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseLineIsNull() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockRelProcessDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(null);
//
//		mockRelProcessDao.insert(isA(ITaskFlowProcEntity.class));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, releaseTaskBean, null, new Exception("Test Message"));
//		verifyAll();
//
//	}
//
//	@Test
//	public void testExecuteCaseBuildServiceIsNotInjected() throws Exception {
//
//		IBuildTaskService mockInjectedService = createMock(IBuildTaskService.class);
//		testee.setService(mockInjectedService);
//
//		try {
//			testee.execute(bldTimelineEntity, releaseTaskBean);
//			Assert.fail("期待したエラーが発生しなかった");
//		} catch (AssertionError e) {
//			assertThat(e.getMessage(), is("IReleaseTaskService is not injected."));
//		}
//
//	}
//
//	private IBldTimelineEntity createBuildTimeLineEntity() {
//
//		IBldTimelineEntity result = new BldTimelineEntity();
//		result.setBldEnvId(ENV_ID);
//		result.setBldLineNo(LINE_NO);
//
//		return result;
//	}
//
//	private IReleaseTaskBean createBuildTaskBean() {
//
//		ReleaseTaskBean result = new ReleaseTaskBean();
//		result.setProcId(PROC_ID);
//
//		return result;
//	}
//
//	private IBldTimelineAgentEntity createLineEntity() {
//		IBldTimelineAgentEntity result = bldTimelineEntity.newLineEntity();
//		result.setBldSrvId(SERVER_ID);
//		result.setTaskFlowId(TASK_FLOW_ID);
//		result.setTargetSeqNo(TARGET_SEQ_NO);
//
//		return result;
//	}
}
