package jp.co.blueship.tri.agent.bm.svc.flow;

import jp.co.blueship.tri.test.support.TriMockTestSupport;

/**
 * {@link ASyncBuildTask}クラスのユニットテストクラス。
 *
 * @author Takayuki Kubo
 *
 */
public class ASyncBuildTaskTest extends TriMockTestSupport {
//
//	private static final String PROC_ID = "procId";
//	private static final String SERVER_ID = "ServerID";
//	private static final String ENV_ID = "EnvID";
//	private static final Integer LINE_NO = 100;
//	private static final String TASK_FLOW_ID = "TaskFlowID";
//	private static final Integer TARGET_SEQ_NO = 200;
//
//	private ASyncBuildTask testee;
//
//	private IContextAdapter mockContextAdapter;
//	private IBuildTaskService mockBuildService;
//	private ITaskFlowProcDao mockTaskFlowProcDao;
//	private IBldTimelineEntity bldTimelineEntity;
//	private IBuildTaskBean buildTaskBean;
//	private IBldTimelineAgentEntity line;
//
//	private Capture<TaskFlowProcEntity> capturedTaskFlowProcEntity;
//
//	@Before
//	public void setUp() {
//
//		testee = new ASyncBuildTask();
//
//		mockContextAdapter = createMock(IContextAdapter.class);
//		mockBuildService = createMock(IBuildTaskService.class);
//		testee.setService(mockBuildService);
//		mockTaskFlowProcDao = createMock(ITaskFlowProcDao.class);
//		//testee.setTaskFlowProcDao(mockTaskFlowProcDao);
//		bldTimelineEntity = createBuildTimeLineEntity();
//		buildTaskBean = createBuildTaskBean();
//		line = createLineEntity();
//
//		capturedTaskFlowProcEntity = new Capture<TaskFlowProcEntity>();
//	}
//
//	@Test
//	public void testExecute() throws Exception {
//
//		mockBuildService.execute(bldTimelineEntity, buildTaskBean);
//		expectLastCall();
//
//		replayAll();
//		testee.execute(bldTimelineEntity, buildTaskBean);
//		verifyAll();
//
//	}
//
//	@Test
//	public void testExecuteWithLineEntity() throws Exception {
//
//		mockBuildService.execute(bldTimelineEntity, buildTaskBean, line);
//		expectLastCall();
//
//		replayAll();
//		testee.execute(bldTimelineEntity, buildTaskBean, line);
//		verifyAll();
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseInsert() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockTaskFlowProcDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(null);
//
//		mockTaskFlowProcDao.insert(capture(capturedTaskFlowProcEntity));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, buildTaskBean, line, new Exception("Test Message"));
//		verifyAll();
//
//		TaskFlowProcEntity actualRelProcessEntity = capturedTaskFlowProcEntity.getValue();
//		assertThat(actualRelProcessEntity.getProcId(), is(PROC_ID));
//		assertThat(actualRelProcessEntity.getMsg(), is(notNullValue()));
//		assertThat(actualRelProcessEntity.getMsg().indexOf("Test Message"), is(0));
//		assertThat(actualRelProcessEntity.getStsId(), is(BmBpStatusIdForExecData.BUILD_ERROR.getStatusId()));
//
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseUpdate() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockTaskFlowProcDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(Arrays.asList(new TaskFlowProcEntity()));
//
//		mockTaskFlowProcDao.update(capture(capturedTaskFlowProcEntity));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, buildTaskBean, line, new Exception("Test Message"));
//		verifyAll();
//
//		TaskFlowProcEntity actualRelProcessEntity = capturedTaskFlowProcEntity.getValue();
//		assertThat(actualRelProcessEntity.getProcId(), is(PROC_ID));
//		assertThat(actualRelProcessEntity.getMsg(), is(notNullValue()));
//		assertThat(actualRelProcessEntity.getMsg().indexOf("Test Message"), is(0));
//		assertThat(actualRelProcessEntity.getStsId(), is(BmBpStatusIdForExecData.BUILD_ERROR.getStatusId()));
//
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseTranslatableException() throws Exception {
//
//		mockContextAdapter.getMessage(SmMessageId.SM004214F, "A", "B", "C");
//		expectLastCall().andReturn("Test Message");
//
//		testee.setContext(mockContextAdapter);
//
//		mockTaskFlowProcDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(Arrays.asList(new TaskFlowProcEntity()));
//
//		mockTaskFlowProcDao.update(capture(capturedTaskFlowProcEntity));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, buildTaskBean, line, new TriSystemException(SmMessageId.SM004214F, "A", "B", "C"));
//		verifyAll();
//
//		TaskFlowProcEntity actualRelProcessEntity = capturedTaskFlowProcEntity.getValue();
//		assertThat(actualRelProcessEntity.getProcId(), is(PROC_ID));
//		assertThat(actualRelProcessEntity.getMsg(), is(notNullValue()));
//		assertThat(actualRelProcessEntity.getMsg().indexOf("Test Message"), is(0));
//		assertThat(actualRelProcessEntity.getStsId(), is(BmBpStatusIdForExecData.BUILD_ERROR.getStatusId()));
//
//	}
//
//	@Test
//	public void testWriteProcessByIrregularCaseLineIsNull() throws Exception {
//
//		testee.setContext(mockContextAdapter);
//
//		mockTaskFlowProcDao.find(isA(ISqlCondition.class));
//		expectLastCall().andReturn(null);
//
//		mockTaskFlowProcDao.insert(isA(ITaskFlowProcEntity.class));
//		expectLastCall().andReturn(1);
//
//		replayAll();
//		testee.writeProcessByIrregular(bldTimelineEntity, buildTaskBean, null, new Exception("Test Message"));
//		verifyAll();
//
//	}
//
//	@Test
//	public void testExecuteCaseBuildServiceIsNotInjected() throws Exception {
//
//		IReleaseTaskService mockInjectedService = createMock(IReleaseTaskService.class);
//		testee.setService(mockInjectedService);
//
//		try {
//			testee.execute(bldTimelineEntity, buildTaskBean);
//			Assert.fail("期待したエラーが発生しなかった");
//		} catch (AssertionError e) {
//			assertThat(e.getMessage(), is("IBuildTaskService is not injected."));
//		}
//
//	}
//
//	private IBldTimelineEntity createBuildTimeLineEntity() {
//
//		IBldTimelineEntity result = new BldTimelineEntity();
//		result.setBldEnvId(ENV_ID);
//		result.setBldLineNo(LINE_NO);
//
//		return result;
//	}
//
//	private IBuildTaskBean createBuildTaskBean() {
//
//		BuildTaskBean result = new BuildTaskBean();
//		result.setProcId(PROC_ID);
//
//		return result;
//	}
//
//	private IBldTimelineAgentEntity createLineEntity() {
//		IBldTimelineAgentEntity result = bldTimelineEntity.newLineEntity();
//		result.setBldSrvId(SERVER_ID);
//		result.setTaskFlowId(TASK_FLOW_ID);
//		result.setTargetSeqNo(TARGET_SEQ_NO);
//
//		return result;
//	}
}
