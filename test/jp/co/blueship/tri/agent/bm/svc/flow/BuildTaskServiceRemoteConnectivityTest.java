package jp.co.blueship.tri.agent.bm.svc.flow;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.agent.service.rpc.RmiProxyFactoryWrapperBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates;

import org.junit.Before;
import org.junit.Test;

public class BuildTaskServiceRemoteConnectivityTest {

	private IBuildTaskService testee;

	private static final String RMI_HOST_NM = "localhost";
	private static final String RMI_REG_PORT = "45031";
	private static final String RMI_SVC_NAME = "BuildTaskService";

	@Before
	public void setUp() throws Exception {

		RmiProxyFactoryWrapperBean rmiProxyFactoryBean = new RmiProxyFactoryWrapperBean(null, null, new IBldEnvSrvEntity[] {});

		String url = "rmi://" + RMI_HOST_NM + ":" + RMI_REG_PORT + "/" + RMI_SVC_NAME;

		rmiProxyFactoryBean.setServiceUrl(url);
		rmiProxyFactoryBean.setServiceInterface(IBuildTaskService.class);
		rmiProxyFactoryBean.afterPropertiesSet();
		testee = (IBuildTaskService) rmiProxyFactoryBean.getObject();
	}

	@Test
	public void testExecuteIBldTimelineEntityIBuildTaskBean() {
	}

	@Test
	public void testExecuteIBldTimelineEntityIBuildTaskBeanIBldTimelineAgentEntity() {
	}

	@Test
	public void testLinkCheck() throws Exception {

		testee.executeLinkCheck();
	}

	@Test
	public void testLinkCheckByService() throws Exception {

		IRmiSvcDto inputDto = new RmiSvcDto();
		List<Object> input = new ArrayList<Object>();
		input.add(inputDto);
		List<Object> actualList = testee.executeLinkCheckByService(input);
		assertThat(TriCollectionUtils.isNotEmpty(actualList), is(true));
		assertThat(actualList.size(), is(2));

		List<Object> statusList = FluentList.from(actualList).select(TriPredicates.isInstanceOf(AgentStatus.class)).asList();
		assertThat(TriCollectionUtils.isNotEmpty(statusList), is(true));
		assertThat(statusList.size(), is(1));

		AgentStatus actual = (AgentStatus) statusList.get(0);
		assertThat(actual.getRemoteServiceEntity(), is(notNullValue()));
		assertThat(actual.isStatus(), is(true));
	}

}
