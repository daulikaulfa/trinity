package jp.co.blueship.tri.agent.cmn.service.flow;

import static org.easymock.EasyMock.expectLastCall;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.ASyncBuildTask;
import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.BuildTaskBean;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.ASyncReleaseTask;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.ReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Before;
import org.junit.Test;

public class ThreadTaskServiceTest extends TriMockTestSupport {

	private ThreadTaskService testee;

	private TriTaskExecutor mockTriTaskExecutor;
	private ASyncBuildTask mockASyncBuildTask;
	private ASyncReleaseTask mockASyncReleaseTask;
	private IBuildTaskService mockIBuildTaskService;
	private IReleaseTaskService mockIReleaseTaskService;

	@Before
	public void setUp() {

		testee = new ThreadTaskService();

		mockIBuildTaskService = createMock(IBuildTaskService.class);
		mockIReleaseTaskService = createMock(IReleaseTaskService.class);
		mockTriTaskExecutor = createMock(TriTaskExecutor.class);
		addMockBeanByName(TriTaskExecutor.BEAN_NAME, mockTriTaskExecutor);

		testee.setContext(contextAdapter());

	}

	@Test
	public void testExecuteOnBuild() throws Exception {

		IBldTimelineEntity timeLine = new BldTimelineEntity();
		IBuildTaskBean param = new BuildTaskBean();

		mockASyncBuildTask = createMock(ASyncBuildTask.class);
		mockASyncBuildTask.setTimeLine(timeLine);
		expectLastCall();
		mockASyncBuildTask.setParam(param);
		expectLastCall();

		mockTriTaskExecutor.submit(mockASyncBuildTask);
		expectLastCall();

		testee.setAsyncTask(mockASyncBuildTask);

		replayAll();
		testee.execute(timeLine, param);
		verifyAll();

	}

	@Test
	public void testExecuteOnBuildWithSingleLine() throws Exception {

		IBldTimelineEntity timeLine = new BldTimelineEntity();
		IBuildTaskBean param = new BuildTaskBean();
		IBldTimelineAgentEntity lineEntity = timeLine.newLineEntity();

		mockASyncBuildTask = createMock(ASyncBuildTask.class);
		mockASyncBuildTask.setTimeLine(timeLine);
		expectLastCall();
		mockASyncBuildTask.setParam(param);
		expectLastCall();
		mockASyncBuildTask.setLine(lineEntity);
		expectLastCall();

		mockTriTaskExecutor.submit(mockASyncBuildTask);
		expectLastCall();

		testee.setAsyncTask(mockASyncBuildTask);

		replayAll();
		testee.execute(timeLine, param, lineEntity);
		verifyAll();
	}

	@Test
	public void testExecuteOnRelease() throws Exception {

		IBldTimelineEntity timeLine = new BldTimelineEntity();
		IReleaseTaskBean param = new ReleaseTaskBean();

		mockASyncReleaseTask = createMock(ASyncReleaseTask.class);
		mockASyncReleaseTask.setTimeLine(timeLine);
		expectLastCall();
		mockASyncReleaseTask.setParam(param);
		expectLastCall();

		mockTriTaskExecutor.submit(mockASyncReleaseTask);
		expectLastCall();

		testee.setAsyncTask(mockASyncReleaseTask);

		replayAll();
		testee.execute(timeLine, param);
		verifyAll();
	}

	@Test
	public void testExecuteOnReleaseWithSingleLine() throws Exception {

		IBldTimelineEntity timeLine = new BldTimelineEntity();
		IReleaseTaskBean param = new ReleaseTaskBean();
		IBldTimelineAgentEntity lineEntity = timeLine.newLineEntity();

		mockASyncReleaseTask = createMock(ASyncReleaseTask.class);
		mockASyncReleaseTask.setTimeLine(timeLine);
		expectLastCall();
		mockASyncReleaseTask.setParam(param);
		expectLastCall();
		mockASyncReleaseTask.setLine(lineEntity);
		expectLastCall();

		mockTriTaskExecutor.submit(mockASyncReleaseTask);
		expectLastCall();

		testee.setAsyncTask(mockASyncReleaseTask);

		replayAll();
		testee.execute(timeLine, param, lineEntity);
		verifyAll();
	}

	@Test
	public void testLinkCheckCaseBuildTaskService() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheck();
		expectLastCall().andReturn(null);

		replayAll();
		List<AgentStatusTask> actual = testee.executeLinkCheck();
		verifyAll();

		assertThat(actual, is(nullValue()));
	}

	@Test
	public void testLinkCheckCaseReleaseTaskService() throws Exception {

		testee.setTaskService(mockIReleaseTaskService);

		mockIReleaseTaskService.executeLinkCheck();
		expectLastCall().andReturn(null);

		replayAll();
		List<AgentStatusTask> actual = testee.executeLinkCheck();
		verifyAll();

		assertThat(actual, is(nullValue()));
	}

	@Test
	public void testLinkCheckCaseBusinessException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheck();
		expectLastCall().andThrow(new BusinessException(BmMessageId.BM004002F));

		replayAll();
		try {
			testee.executeLinkCheck();
			fail("期待した例外が発生しなかった");
		} catch (BusinessException e) {
			// MessageIDの検証
		}
		verifyAll();
	}

	@Test
	public void testLinkCheckCaseSystemException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheck();
		expectLastCall().andThrow(new TriSystemException(BmMessageId.BM004002F));

		replayAll();
		try {
			testee.executeLinkCheck();
			fail("期待した例外が発生しなかった");
		} catch (TriSystemException e) {
			// MessageIDの検証
		}
		verifyAll();
	}

	@Test
	public void testLinkCheckCaseException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheck();
		expectLastCall().andThrow(new Exception());

		replayAll();
		try {
			testee.executeLinkCheck();
			fail("期待した例外が発生しなかった");
		} catch (TriSystemException e) {
			// MessageIDの検証
		}
		verifyAll();
	}

	@Test
	public void testLinkCheckServiceCaseBuildTaskService() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		List<Object> retList = new ArrayList<Object>();
		retList.add("element");
		List<Object> paramList = Collections.emptyList();
		mockIBuildTaskService.executeLinkCheckByService(paramList);
		expectLastCall().andReturn(retList);

		replayAll();
		List<Object> actual = testee.executeLinkCheckByService(paramList);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat((String) actual.get(0), is("element"));
	}

	@Test
	public void testLinkCheckServiceCaseReleaseTaskService() throws Exception {

		testee.setTaskService(mockIReleaseTaskService);

		List<Object> retList = new ArrayList<Object>();
		retList.add("element");
		List<Object> paramList = Collections.emptyList();
		mockIReleaseTaskService.executeLinkCheckByService(paramList);
		expectLastCall().andReturn(retList);

		replayAll();
		List<Object> actual = testee.executeLinkCheckByService(paramList);
		verifyAll();

		assertThat(TriCollectionUtils.isNotEmpty(actual), is(true));
		assertThat(actual.size(), is(1));
		assertThat((String) actual.get(0), is("element"));
	}

	@Test
	public void testLinkCheckByServiceCaseBusinessException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheckByService(null);
		expectLastCall().andThrow(new BusinessException(BmMessageId.BM004002F));

		replayAll();
		try {
			testee.executeLinkCheckByService(null);
			fail("期待した例外が発生しなかった");
		} catch (BusinessException e) {
			// MessageIDの検証
		}
		verifyAll();
	}

	@Test
	public void testLinkCheckByServiceCaseSystemException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheckByService(null);
		expectLastCall().andThrow(new TriSystemException(BmMessageId.BM004002F));

		replayAll();
		try {
			testee.executeLinkCheckByService(null);
			fail("期待した例外が発生しなかった");
		} catch (TriSystemException e) {
			// MessageIDの検証
		}
		verifyAll();
	}

	@Test
	public void testLinkCheckByServiceCaseException() throws Exception {

		testee.setTaskService(mockIBuildTaskService);

		mockIBuildTaskService.executeLinkCheckByService(null);
		expectLastCall().andThrow(new Exception());

		replayAll();
		try {
			testee.executeLinkCheckByService(null);
			fail("期待した例外が発生しなかった");
		} catch (TriSystemException e) {
			// MessageIDの検証
		}
		verifyAll();
	}
}
