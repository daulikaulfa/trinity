package jp.co.blueship.tri.agent.cmn.service.rpc;

import static org.easymock.EasyMock.expectLastCall;
import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.test.support.TriMockTestSupport;

import org.junit.Before;
import org.junit.Test;

/**
 * {@link TaskServiceExporter} クラスのユニットテスト
 *
 * @author Takayuki Kubo
 *
 */
public class TaskServiceExporterTest extends TriMockTestSupport {

	private static final String BUILD_SERVICE_NAME = "BuildServiceName";
	private static final String RELEASE_SERVICE_NAME = "ReleaseServiceName";

	private TaskServiceExporter testee;
	private IBuildTaskService mockIBuildTaskService;
	private IReleaseTaskService mockIReleaseTaskService;

	/**
	 * 事前準備
	 */
	@Before
	public void setUp() {

		testee = new TaskServiceExporter();

		mockIBuildTaskService = createMock(IBuildTaskService.class);
		mockIReleaseTaskService = createMock(IReleaseTaskService.class);
		addMockBeanByName(BUILD_SERVICE_NAME, mockIBuildTaskService);
		addMockBeanByName(RELEASE_SERVICE_NAME, mockIReleaseTaskService);
	}

	/**
	 * {@linkplain TaskServiceExporter#execute(IBldTimeLineEntity, IBuildTaskBean)}
	 * メソッドのテスト
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testExecuteIBuildTimeLineEntityIBuildTaskBean() throws Exception {

		IBldTimelineEntity mockBuildTimeLineEntity = createMock(IBldTimelineEntity.class);
		IBuildTaskBean mockIBuildTaskBean = createMock(IBuildTaskBean.class);

		mockIBuildTaskService.execute(mockBuildTimeLineEntity, mockIBuildTaskBean);
		expectLastCall();

		replayAll();
		testee.setServiceName(BUILD_SERVICE_NAME);
		testee.execute(mockBuildTimeLineEntity, mockIBuildTaskBean);
		verifyAll();

	}

	/**
	 * {@linkplain TaskServiceExporter#execute(IBldTimeLineEntity, IBuildTaskBean, IBldTimelineAgentEntity)}
	 * メソッドのテスト
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testExecuteIBuildTimeLineEntityIBuildTaskBeanILineEntity() throws Exception {

		IBldTimelineEntity mockBuildTimeLineEntity = createMock(IBldTimelineEntity.class);
		IBuildTaskBean mockIBuildTaskBean = createMock(IBuildTaskBean.class);
		IBldTimelineAgentEntity mockILineEntity = createMock(IBldTimelineAgentEntity.class);

		mockIBuildTaskService.execute(mockBuildTimeLineEntity, mockIBuildTaskBean, mockILineEntity);
		expectLastCall();

		replayAll();
		testee.setServiceName(BUILD_SERVICE_NAME);
		testee.execute(mockBuildTimeLineEntity, mockIBuildTaskBean, mockILineEntity);
		verifyAll();
	}

	/**
	 * {@linkplain TaskServiceExporter#execute(IBldTimelineEntity, IReleaseTaskBean)}
	 * メソッドのテスト
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testExecuteIBldTimelineEntityIReleaseTaskBean() throws Exception {

		IBldTimelineEntity mockBldTimelineEntity = createMock(IBldTimelineEntity.class);
		IReleaseTaskBean mockIReleaseTaskBean = createMock(IReleaseTaskBean.class);

		mockIReleaseTaskService.execute(mockBldTimelineEntity, mockIReleaseTaskBean);
		expectLastCall();

		replayAll();
		testee.setServiceName(RELEASE_SERVICE_NAME);
		testee.execute(mockBldTimelineEntity, mockIReleaseTaskBean);
		verifyAll();

	}

	/**
	 * {@linkplain TaskServiceExporter#execute(IBldTimelineEntity, IReleaseTaskBean, jp.co.blueship.tri.rm.dao.v2.relTimeLine.eb.IBldTimelineEntity.ILineEntity)}
	 * メソッドのテスト
	 *
	 * @throws Exception 予期しない例外が発生した
	 */
	@Test
	public void testExecuteIBldTimelineEntityIReleaseTaskBeanILineEntity() throws Exception {

		IBldTimelineEntity mockRelTimeLineEntity = createMock(IBldTimelineEntity.class);
		IReleaseTaskBean mockIReleaseTaskBean = createMock(IReleaseTaskBean.class);
		IBldTimelineAgentEntity mockILineEntity = createMock(IBldTimelineAgentEntity.class);

		mockIReleaseTaskService.execute(mockRelTimeLineEntity, mockIReleaseTaskBean, mockILineEntity);
		expectLastCall();

		replayAll();
		testee.setServiceName(RELEASE_SERVICE_NAME);
		testee.execute(mockRelTimeLineEntity, mockIReleaseTaskBean, mockILineEntity);
		verifyAll();

	}

}
