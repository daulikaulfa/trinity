package jp.co.blueship.tri.dm.domain;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class FlowDeploymentJobDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	@SuppressWarnings("unused")
	private static final String mgtVer = "16060022";


	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	/*

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void details(IGenericTransactionService service) throws Exception {
		FlowDeploymentJobDetailsServiceBean serviceBean = new FlowDeploymentJobDetailsServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedMgtVer(mgtVer);

			service.execute(ServiceId.DmFlowDeploymentJobDetailsService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}
	}


	private void debug( FlowDeploymentJobDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println(" MgtVer : "					 + serviceBean.getDetailsView().getMgtVer());
		System.out.println(" RpId : "					 + serviceBean.getDetailsView().getRpId());
		System.out.println(" AutoJob : "				 + serviceBean.getDetailsView().isAutoJob());
		System.out.println(" Summary : "				 + serviceBean.getDetailsView().getSummary());
		System.out.println(" Contents : "				 + serviceBean.getDetailsView().getContents());
		System.out.println(" EnvId : "					 + serviceBean.getDetailsView().getEnvId());
		System.out.println(" EnvironmentNm : "			 + serviceBean.getDetailsView().getEnvironmentNm());
		System.out.println(" Category : "				 + serviceBean.getDetailsView().getCategoryNm());
		System.out.println(" Milestone : "				 + serviceBean.getDetailsView().getMstoneNm());
		System.out.println(" StsId : "					 + serviceBean.getDetailsView().getStsId());
		System.out.println(" Status : "					 + serviceBean.getDetailsView().getStatus());
		System.out.println(" TimerSettings : "			 + serviceBean.getDetailsView().getTimerSettings().value());
		System.out.println(" TimerSettingsTime : "		 + serviceBean.getDetailsView().getTimerSettingsTime());
		System.out.println(" LatestUpdateUserNm : "		 + serviceBean.getDetailsView().getLatestUpdateUserNm());
		System.out.println(" LatestUpdateUserIconParh : "+ serviceBean.getDetailsView().getLatestUpdateUserIconPath());
		System.out.println(" DeploymentRegistedTime : "	 + serviceBean.getDetailsView().getDeploymentRegistedTime());
		System.out.println(" DeploymentAppliedTime : "	 + serviceBean.getDetailsView().getDeploymentAppliedTime());
		System.out.println(" DeploymentStsId : "		 + serviceBean.getDetailsView().getDeploymentStsId());
		System.out.println(" DeploymentStatus : "		 + serviceBean.getDetailsView().getStatus());
		System.out.println("");

		ReleasePackageViewBean rpView = serviceBean.getDetailsView().getReleasePackage();

		System.out.println("ReleasePackageView");
		System.out.println(" RpId : "				+ rpView.getRpId());
		System.out.println(" BpId : "				+ rpView.getBpId());
		System.out.println(" EnvironmentNm : "		+ rpView.getEnvironmentNm());
		System.out.println(" CreateBy : "			+ rpView.getCreatedBy());
		System.out.println(" CreateByIconPath : "	+ rpView.getCreatedByIconPath());
		System.out.println(" StartTime : "			+ rpView.getStartTime());
		System.out.println(" EndTime : "			+ rpView.getEndTime());
	}

	*/
}
