package jp.co.blueship.tri.dm.domain;

import org.junit.Test;

import jp.co.blueship.tri.dm.domainx.deploy.dto.FlowDeploymentJobCancellationServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class FlowDeploymentJobCancellationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	private static final String mgtVer = "16120004";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.cancel(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void cancel(IGenericTransactionService service) throws Exception {
		FlowDeploymentJobCancellationServiceBean serviceBean = new FlowDeploymentJobCancellationServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			serviceBean.getParam().setSelectedMgtVer(mgtVer);

			service.execute(ServiceId.DmFlowDeploymentJobCancellationService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}

	private void debug(FlowDeploymentJobCancellationServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");


		if(serviceBean.getResult().isCompleted())
			System.out.println("Completed !!!");
		else
			System.out.println("NotCompleted....");
	}
}
