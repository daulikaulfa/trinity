package jp.co.blueship.tri.dm.domain;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class FlowDeploymentJobEditServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	@SuppressWarnings("unused")
	private static final String mgtVer = "16060030";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	/*

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.edit(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void edit(IGenericTransactionService service) throws Exception {
		FlowDeploymentJobEditServiceBean serviceBean = new FlowDeploymentJobEditServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);

			serviceBean.getParam().setSelectedMgtVer(mgtVer);

			service.execute(ServiceId.DmFlowDeploymentJobEditService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}

		//submitChange
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			serviceBean.getParam().getInputInfo().setSummary("TESTEDIT");
			serviceBean.getParam().getInputInfo().setContents("EDITTEST");

			serviceBean.getParam().getInputInfo().setTimerSettings(TimerSettings.Cancel);


			service.execute(ServiceId.DmFlowDeploymentJobEditService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}
	}


	private void debug(FlowDeploymentJobEditServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if(RequestType.init.equals(serviceBean.getParam().getRequestType()))
			this.showInitResult(serviceBean);

		if(RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()))
			this.showSubmitChangesResult(serviceBean);


	}

	private void showInitResult(FlowDeploymentJobEditServiceBean serviceBean){
		System.out.println("Summary : " + serviceBean.getParam().getInputInfo().getSummary());
		System.out.println("Conrents : " + serviceBean.getParam().getInputInfo().getContents());
		System.out.println("TimerSettings : " + serviceBean.getParam().getInputInfo().getTimerSettings().value());
		System.out.println("TimerDate : " + serviceBean.getParam().getInputInfo().getTimerDate());
		System.out.println("TimerHour : " + serviceBean.getParam().getInputInfo().getTimerHour());
		System.out.println("TimerMuinute : " + serviceBean.getParam().getInputInfo().getTimerMinute());
		System.out.println("AutoJob : " + serviceBean.getParam().getInputInfo().isAutoJob());
		System.out.println("");


		System.out.println("MgtVer : " + serviceBean.getDetailsView().getMgtVer());
		System.out.println("RpId : " + serviceBean.getDetailsView().getRpId());
		System.out.println("EnvId : " + serviceBean.getDetailsView().getEnvId());
		System.out.println("EnvNm : " + serviceBean.getDetailsView().getEnvironmentNm());
		System.out.println("CtgNm : " + serviceBean.getDetailsView().getCategoryNm());
		System.out.println("MstoneNm : " + serviceBean.getDetailsView().getMstoneNm());

		System.out.println("RpId : " + serviceBean.getDetailsView().getReleasePackage().getRpId());
		System.out.println("BpId : " + serviceBean.getDetailsView().getReleasePackage().getBpId());
		System.out.println("EnvNm : " + serviceBean.getDetailsView().getReleasePackage().getEnvironmentNm());
		System.out.println("CreateBy : " + serviceBean.getDetailsView().getReleasePackage().getCreatedBy());
	}

	private void showSubmitChangesResult(FlowDeploymentJobEditServiceBean serviceBean){
		System.out.println(serviceBean.getParam().getInputInfo().getSummary());
		System.out.println(serviceBean.getParam().getInputInfo().getContents());

	}
	*/
}
