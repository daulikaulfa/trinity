package jp.co.blueship.tri.dm.domain;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class FlowDeploymentJobCreationServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	@SuppressWarnings("unused")
	private static final String testLotId = "LOT-1605180007";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

/*

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.careation(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void careation(IGenericTransactionService service) throws Exception {
		FlowDeploymentJobCreationServiceBean serviceBean = new FlowDeploymentJobCreationServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);

			serviceBean.getParam().setSelectedLotId(testLotId);

			service.execute(ServiceId.DmFlowDeploymentJobCreationService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}

		//submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			//inputParam
			String summary	= "domainTest summary";
			String contents	= "domainTest contents";
			String relEnvId	= "RPENV_TRINITY_PDT";
			String ctgId	= "";
			String mstoneId	= "";
			String rpId		= "REL-1606170034";
			boolean autoJob	= false;

			DeploymentJobEditInputBean inputInfo = serviceBean.getParam().getInputInfo()
					.setSummary		(summary)
					.setContents	(contents)
					.setRelEnvId	(relEnvId)
					.setCtgId		(ctgId)
					.setMstoneId	(mstoneId)
					.setRpId		(rpId)
//					.setAutoJob		( autoJob )
					;
			serviceBean.getParam().setInputInfo(inputInfo);

			service.execute(ServiceId.DmFlowDeploymentJobCreationService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}

		//submitChanegs timerSetting
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			//inputParam
			TimerSettings timerSettings = TimerSettings.Immediately;
			String date		= "2020/06/14";
			String hour		= "00";
			String minute	= "00";

			/*
			String relEnvId	= "RPENV_TRINITY_PDT";
			String rpId		= "REL-1606160026";
			boolean autoJob	= false;
			* /

			DeploymentJobEditInputBean inputInfo = serviceBean.getParam().getInputInfo()
					/*
					.setRelEnvId	(relEnvId)
					.setRpId		(rpId)
					.setAutoJob		( autoJob )
					* /
					.setTimerSettings	(timerSettings)
					.setTimerDate		(date)
					.setTimerHour		(hour)
					.setTimerMinute		(minute)
					;
			serviceBean.getParam().setInputInfo(inputInfo);

			//serviceBean.getResult().setMgtVer("aaaa");

			service.execute(ServiceId.DmFlowDeploymentJobCreationService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}
	}

	private void debug( FlowDeploymentJobCreationServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if(RequestType.init.equals(serviceBean.getParam().getRequestType()))
			this.showInitResult(serviceBean);

		if(RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()))
			this.showSubmitChangesResult(serviceBean);
	}


	private void showInitResult(FlowDeploymentJobCreationServiceBean serviceBean){

		System.out.println("ReleaseEnv");
		this.showItemLabelsBean(serviceBean.getParam().getInputInfo().getReleaseEnvViews());

		System.out.println("Ctegory");
		this.showItemLabelsBean(serviceBean.getParam().getInputInfo().getCtgViews());

		System.out.println("Milestone");
		this.showItemLabelsBean(serviceBean.getParam().getInputInfo().getMstoneViews());
		System.out.println("");

		System.out.println("Release Package List");
		for(ReleasePackageViewBean view : serviceBean.getParam().getInputInfo().getReleasePackageViews()){
			System.out.print("  RpId : "		+ view.getRpId());
			System.out.print("  BpId : "		+ view.getBpId());
			System.out.print("  EnvNm : "		+ view.getEnvironmentNm());
			System.out.print("  CreateBy : "	+ view.getCreatedBy());
			System.out.print("  StartTime : "	+ view.getStartTime());
			System.out.print("  EndTime : "		+ view.getEndTime() + "\n");
		}
		System.out.println("");
	}


	private void showSubmitChangesResult(FlowDeploymentJobCreationServiceBean serviceBean){

		if(serviceBean.getResult().isCompleted())
			System.out.println("completed!! \n MgtVer : " + serviceBean.getResult().getMgtVer());
		else
			System.out.println("not completed");
	}

	private void showItemLabelsBean(List<ItemLabelsBean> views){
		for(ItemLabelsBean view : views)
			System.out.println("   Label : " + view.getLabel() + "   Value : " + view.getValue());

		System.out.println("");
	}

	*/
}
