package jp.co.blueship.tri.dm.domain;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class FlowDeploymentJobLatestUpdatesServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	@SuppressWarnings("unused")
	private static final String testLotId = "LOT-1605180007";


	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	/*

	@Test
	public void init(){
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.latestUpdates(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void latestUpdates(IGenericTransactionService service) throws Exception {

		FlowDeploymentJobLatestUpdatesServiceBean serviceBean = new FlowDeploymentJobLatestUpdatesServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(testLotId);

			service.execute(ServiceId.DmFlowDeploymentJobLatestUpdatesService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}
	}

	private void debug(FlowDeploymentJobLatestUpdatesServiceBean serviceBean){
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("LotId : " + serviceBean.getParam().getSelectedLotId()+"\n");

		for(DeloymentAutomaticJobViewBean view : serviceBean.getJobViews()){

			System.out.println("EnvName : " + view.getEnvironmentNm() + "  EnvId : " + view.getEnvId());

			System.out.println(" MgtVer : "					+ view.getMgtVer());
			System.out.println(" RpId : "					+ view.getRpId());
			System.out.println(" AutoJob : "				+ view.isAutoJob());
			System.out.println(" StsId : "					+ view.getStsId());
			System.out.println(" Status : "					+ view.getStatus());
			System.out.println(" TimerSettings : "			+ view.getTimerSettings().value());
			System.out.println(" TimerSettingsTime : "		+ view.getTimerSettingsTime());
			System.out.println(" LatestUpDateUserNm : "		+ view.getLatestUpdateUserNm());
			System.out.println(" DeploymentRegistedTime : "	+ view.getDeploymentRegistedTime());
			System.out.println(" DeploymentAppliedTime : "	+ view.getDeploymentAppliedTime());
			System.out.println(" DeploymentStsId : "		+ view.getDeploymentStsId());
			System.out.println(" DeploymentStatus : "		+ view.getDeploymentStatus());
			System.out.println("");

		}
	}

	*/
}
