package jp.co.blueship.tri.dm.domain;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class FlowDeploymentJobListServiceTest extends TestDomainSupport{

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-DeploymentJob-Context.xml"};

	@SuppressWarnings("unused")
	private static final String testLotId = "LOT-1605180007";


	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}


	/*

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.listview(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	private void listview(IGenericTransactionService service) throws Exception {



		FlowDeploymentJobListServiceBean serviceBean = new FlowDeploymentJobListServiceBean();


		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);

			serviceBean.getParam().setSelectedLotId(testLotId);

			service.execute(ServiceId.DmFlowDeploymentJobListService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}


		//onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);

			//inputParam
			Boolean	isAutomatic		= false;
			String	envId			= "";
			String	stsId			= "3100";
			String	deploymentStsId	= "";
			String	ctgId			= "";
			String	mstoneId		= "";
			String	keyword			= "";
			int		selectedPageNo	= 1;

			SearchCondition condition = (isAutomatic)? serviceBean.getParam().getSearchAutomaticCondition()
													 :	serviceBean.getParam().getSearchManualCondition();

			condition.setEnvId			 ( envId );
			condition.setStsId			 ( stsId );
			condition.setDeploymentStsId ( deploymentStsId );
			condition.setCtgId			 ( ctgId );
			condition.setMstoneId		 ( mstoneId );
			condition.setKeyword		 ( keyword );
			condition.setSelectedPageNo	 ( selectedPageNo );

			if(isAutomatic){
				serviceBean.getParam().setSearchAutomaticCondition(condition);

			}else{
				serviceBean.getParam().setSearchManualCondition(condition);
			}

			service.execute(ServiceId.DmFlowDeploymentJobListService.value(), serviceDto);
			//this.threadWait( serviceBean );
			this.debug(serviceBean);
		}

	}


	private void debug( FlowDeploymentJobListServiceBean serviceBean ) {

		SearchCondition searchCondition = serviceBean.getParam().getSearchAutomaticCondition();

		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("ReleaseEnv");
		this.showItemLabelsBean( searchCondition.getReleaseEnvViews() );

		System.out.println("Request Status");
		this.showItemLabelsBean( searchCondition.getStatusViews() );

		System.out.println("DeploymentStatus");
		this.showItemLabelsBean( searchCondition.getDeploymentStatusViews() );

		System.out.println("Category");
		this.showItemLabelsBean( searchCondition.getCtgViews() );

		System.out.println("Milestone");
		this.showItemLabelsBean( searchCondition.getMstoneViews() );
		System.out.println("");


		System.out.println("Automatic List");
		for(DeloymentAutomaticJobViewBean view : serviceBean.getAutomaticJobViews()){
			System.out.print("   DeploymentRegistedTime : "	 + view.getDeploymentRegistedTime());
			System.out.print( "  DeploymentAppliedTime : "	 + view.getDeploymentAppliedTime());
			System.out.print( "  DeploymentStsId : "		 + view.getDeploymentStsId());
			System.out.print( "  DeploymentStatus : "		 + view.getDeploymentStatus());
			System.out.print( "  MgtVer : "					 + view.getMgtVer());
			System.out.print( "  RpId : "					 + view.getRpId());
			System.out.print( "  EnvId : "					 + view.getEnvId());
			System.out.print( "  EnvironmentNm : "			 + view.getEnvironmentNm());
			System.out.print( "  StsId : "					 + view.getStsId());
			System.out.print( "  Status : "					 + view.getStatus());
			System.out.print( "  TimerSettings : "			 + view.getTimerSettings().value());
			System.out.print( "  TimerSettingsTime : "		 + view.getTimerSettingsTime());
			System.out.print( "  LatestUpdateUserNm : "		 + view.getLatestUpdateUserNm());
			System.out.print( "  LatestUpdateUserIconPath : "+ view.getLatestUpdateUserIconPath() + "\n");
		}
		System.out.println("");

		System.out.println("Manual List");
		for(DeploymentManualJobViewBean view : serviceBean.getManualJobViews()){
			System.out.print("   MgtVer : "						+ view.getMgtVer());
			System.out.print("   RpId : "						+ view.getRpId());
			System.out.print("   EnvId : "						+ view.getEnvId());
			System.out.print("   EnvironmentNm : "				+ view.getEnvironmentNm());
			System.out.print("   StsId : "						+ view.getStsId());
			System.out.print("   Status : "						+ view.getStatus());
			System.out.print("   TimerSettings : "				+ view.getTimerSettings().value());
			System.out.print("   TimerSettingsTime : "			+ view.getTimerSettingsTime());
			System.out.print("   LatestUpdateUserNm : "			+ view.getLatestUpdateUserNm());
			System.out.print("   LatestUpdateUserIconPath : "	+ view.getLatestUpdateUserIconPath() +"\n");
		}
		System.out.println("");
	}


	private void showItemLabelsBean(List<ItemLabelsBean> views){
		for(ItemLabelsBean view : views)
			System.out.println("   Label : " + view.getLabel() + "   Value : " + view.getValue());

		System.out.println("");
	}
	*/
}
