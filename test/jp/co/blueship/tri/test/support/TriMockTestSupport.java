package jp.co.blueship.tri.test.support;

import java.io.IOException;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId.Sheet;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.di.spring.Contexts;

import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.MessageSourceResolvable;
import org.springframework.context.NoSuchMessageException;
import org.springframework.core.env.Environment;
import org.springframework.core.io.Resource;

/**
 * ユニットテストサポートクラス。<br/>
 * ユニットテストにおいて、SpringDIコンテナのgetBean()メソッドを使用して依存性を注入するケースで、<br/>
 * SpringDIコンテナよりモック化されたBeanを取得することが可能となる。<br/>
 *
 * <pre>
 * 使用例
 * <code>
 * Foo mockFoo = createMock(Foo.class);
 * addMockBeanByName("foo", mockFoo);
 * </code>
 *
 * </pre>
 *
 * @author Takayuki Kubo
 *
 */
public class TriMockTestSupport extends EasyMockSupport {

	private ApplicationContextStub applicationContext;
	private IContextAdapter contextAdapter;
	private MessageManager mockMessageManager;
	private MockDesignSheetWrapper mockDesignSheetWrapper;

	/**
	 * 事前準備。<br/>
	 */
	@Before
	public final void setUpMockTest() {
		applicationContext = new ApplicationContextStub();
		Contexts.getInstance().setApplicationContext(applicationContext);
		contextAdapter = ContextAdapterFactory.getContextAdapter();
		mockDesignSheetWrapper = new MockDesignSheetWrapper();
		new DesignSheetFactory().setDesignSheet(mockDesignSheetWrapper);
		mockMessageManager = createNiceMock(MessageManager.class);
		addMockBeanByName(MessageManager.BEAN_NAME, mockMessageManager);
	}

	/**
	 * ApplicationContextへモック化されたBeanを追加する。
	 *
	 * @param beanName bean名
	 * @param bean モック化されたBean
	 */
	protected void addMockBeanByName(String beanName, Object bean) {
		applicationContext.addBeanByName(beanName, bean);
	}

	protected BeanFactory beanFactory() {
		return applicationContext;
	}

	protected ApplicationContext applicationContext() {
		return applicationContext;
	}

	protected IContextAdapter contextAdapter() {
		return contextAdapter;
	}

	protected MessageManager messageManager() {
		return mockMessageManager;
	}

	protected void setMockDesignSheet(IDesignSheet designSheet) {
		mockDesignSheetWrapper = new MockDesignSheetWrapper(designSheet);
		new DesignSheetFactory().setDesignSheet(mockDesignSheetWrapper);
	}

	protected void setDesignValue(IDesignEntryKey key, String value) {
		mockDesignSheetWrapper.setDesignValue(key, value);
	}

	private static class ApplicationContextStub implements ApplicationContext {

		private Map<String, Object> beanMap = new HashMap<String, Object>();

		private void addBeanByName(String name, Object bean) {
			beanMap.put(name, bean);
		}

		@Override
		public Environment getEnvironment() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsBeanDefinition(String beanName) {
			throw new UnsupportedOperationException();
		}

		@Override
		public int getBeanDefinitionCount() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String[] getBeanDefinitionNames() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String[] getBeanNamesForType(Class<?> type) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String[] getBeanNamesForType(Class<?> type, boolean includeNonSingletons, boolean allowEagerInit) {
			throw new UnsupportedOperationException();
		}

		@Override
		public <T> Map<String, T> getBeansOfType(Class<T> type) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public <T> Map<String, T> getBeansOfType(Class<T> type, boolean includeNonSingletons, boolean allowEagerInit) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public Map<String, Object> getBeansWithAnnotation(Class<? extends Annotation> annotationType) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public <A extends Annotation> A findAnnotationOnBean(String beanName, Class<A> annotationType) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Object getBean(String name) throws BeansException {
			return beanMap.get(name);
		}

		@Override
		public <T> T getBean(String name, Class<T> requiredType) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public <T> T getBean(Class<T> requiredType) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public Object getBean(String name, Object... args) throws BeansException {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsBean(String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isSingleton(String name) throws NoSuchBeanDefinitionException {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isPrototype(String name) throws NoSuchBeanDefinitionException {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean isTypeMatch(String name, Class<?> targetType) throws NoSuchBeanDefinitionException {
			throw new UnsupportedOperationException();
		}

		@Override
		public Class<?> getType(String name) throws NoSuchBeanDefinitionException {
			throw new UnsupportedOperationException();
		}

		@Override
		public String[] getAliases(String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public BeanFactory getParentBeanFactory() {
			throw new UnsupportedOperationException();
		}

		@Override
		public boolean containsLocalBean(String name) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getMessage(String code, Object[] args, String defaultMessage, Locale locale) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getMessage(String code, Object[] args, Locale locale) throws NoSuchMessageException {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getMessage(MessageSourceResolvable resolvable, Locale locale) throws NoSuchMessageException {
			throw new UnsupportedOperationException();
		}

		@Override
		public void publishEvent(ApplicationEvent event) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Resource[] getResources(String locationPattern) throws IOException {
			throw new UnsupportedOperationException();
		}

		@Override
		public Resource getResource(String location) {
			throw new UnsupportedOperationException();
		}

		@Override
		public ClassLoader getClassLoader() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getId() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getApplicationName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getDisplayName() {
			throw new UnsupportedOperationException();
		}

		@Override
		public long getStartupDate() {
			throw new UnsupportedOperationException();
		}

		@Override
		public ApplicationContext getParent() {
			throw new UnsupportedOperationException();
		}

		@Override
		public AutowireCapableBeanFactory getAutowireCapableBeanFactory() throws IllegalStateException {
			throw new UnsupportedOperationException();
		}

	}

	private static class MockDesignSheetWrapper extends MockDesignSheet {

		private IDesignSheet delegate;
		private Map<IDesignEntryKey, String> designMap = new HashMap<IDesignEntryKey, String>();

		private MockDesignSheetWrapper() {
			setDesignValue(UmDesignEntryKeyByCommon.viewDateFormat, "yyyy/MM/dd HH:mm:ss");
		}

		private MockDesignSheetWrapper(IDesignSheet delegate) {
			this();
			this.delegate = delegate;
		}

		@Override
		public Map<String, String> getKeyMap(IDesignBeanId beanId) {
			return delegate.getKeyMap(beanId);
		}

		@Override
		@Deprecated
		public Map<String, String> getKeyMap(Sheet sheet, String beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Map<String, String> getValueMap(IDesignBeanId beanId) {
			return delegate.getValueMap(beanId);
		}

		@Override
		public List<String> getKeyList(IDesignBeanId beanId) {
			return delegate.getKeyList(beanId);
		}

		@Override
		public List<String> getValueList(IDesignBeanId beanId) {
			return delegate.getValueList(beanId);
		}

		@Override
		public String getKey(IDesignBeanId beanId, String value) {
			return delegate.getKey(beanId, value);
		}

		@Override
		public String getValue(IDesignBeanId beanId, String key) {
			return delegate.getValue(beanId, key);
		}

		@Override
		public Integer intValue(IDesignBeanId beanId, String key) {
			return delegate.intValue(beanId, key);
		}

		@Override
		public String getValue(IDesignEntryKey key) {
			if (designMap.containsKey(key)) {
				return designMap.get(key);
			}

			return delegate.getValue(key);
		}

		@Override
		public Integer intValue(IDesignEntryKey key) {
			return delegate.intValue(key);
		}

		public void setDesignValue(IDesignEntryKey key, String value) {
			designMap.put(key, value);
		}

	}

	protected static class MockDesignSheet implements IDesignSheet {

		protected MockDesignSheet() {
		}

		@Override
		public void init() {
		}

		@Override
		public Map<String, String> getKeyMap(IDesignBeanId beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Map<String, String> getKeyMap(Sheet sheet, String beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Map<String, String> getValueMap(IDesignBeanId beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<String> getKeyList(IDesignBeanId beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public List<String> getValueList(IDesignBeanId beanId) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getKey(IDesignBeanId beanId, String value) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getValue(IDesignBeanId beanId, String key) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Integer intValue(IDesignBeanId beanId, String key) {
			throw new UnsupportedOperationException();
		}

		@Override
		public String getValue(IDesignEntryKey key) {
			throw new UnsupportedOperationException();
		}

		@Override
		public Integer intValue(IDesignEntryKey key) {
			throw new UnsupportedOperationException();
		}

	}

}
