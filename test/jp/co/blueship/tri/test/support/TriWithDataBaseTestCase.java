package jp.co.blueship.tri.test.support;

import javax.sql.DataSource;

import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;

import org.dbunit.DataSourceBasedDBTestCase;
import org.junit.Before;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

/**
 * DBアクセスを伴った結合テストケース用の基底クラス。
 *
 * 注意：現時点ではトランザクションの制御がうまくゆかず、DBの内容がロールバックされないため使用しない方がよい。
 *
 * @author Takayuki Kubo
 *
 */
public abstract class TriWithDataBaseTestCase extends DataSourceBasedDBTestCase implements ApplicationContextAware {

	private static final String DEFAULT_USER_ID = "trinity";
	private static final String DEFAULT_USER_NAME = "trinityUserName";

	@Autowired
	private DataSource dataSource;
	private ApplicationContext applicationContext;

	@Before
	public final void setUpTrinity() {
		Contexts.getInstance().setApplicationContext(applicationContext);
		TriAuthenticationContext triAuthenticationContext = (TriAuthenticationContext) applicationContext.getBean(TriAuthenticationContext.BEAN_NAME);
		triAuthenticationContext.setAuthUser(getAuthUserID(), getAuthUserName());
	}

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	protected ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	/**
	 * DataSourceを取得する。
	 *
	 * @return DataSource
	 *
	 * @see org.dbunit.DataSourceBasedDBTestCase#getDataSource()
	 */
	@Override
	protected final DataSource getDataSource() {
		return dataSource;
	}

	/**
	 * Trinity内部で必要となるログインユーザIDを返す。<br/>
	 * 実際にユーザテーブルに登録しているものでなければならない場合は、子クラスにてオーバライドすること。
	 *
	 * @return ログインユーザID"trinity"を返す。
	 */
	protected String getAuthUserID() {
		return DEFAULT_USER_ID;
	}

	/**
	 * Trinity内部で必要となるログインユーザ名を返す。<br/>
	 * 実際にユーザテーブルに登録しているものでなければならない場合は、子クラスにてオーバライドすること。
	 *
	 * @return ログインユーザ名"trinityUserName"を返す。
	 */
	protected String getAuthUserName() {
		return DEFAULT_USER_NAME;
	}

}
