package jp.co.blueship.tri.test.util;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.collect;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;

import org.junit.Assert;

/**
 * スローされた例外の妥当性をチェックするテスト用ヘルパクラス。
 *
 * @author Takayuki Kubo
 *
 */
public abstract class ThrowExceptionTestHelper<T, E extends Throwable> {

	private static final TriFunction<MessageParameter, String> TO_STRING_PARAM = new TriFunction<MessageParameter, String>() {

		@Override
		public String apply(MessageParameter mParam) {
			return DesignUtils.getMessageParameter(mParam.getKey());
		}
	};

	public void verifyThrowException(T input, Class<E> expectedException, IMessageId expectedMessageID, MessageParameter... expectedArgs) {

		verifyThrowException(input, expectedException, expectedMessageID, collect(expectedArgs, TO_STRING_PARAM).toArray(new String[] {}));
	}

	public void verifyThrowException(T input, Class<E> expectedException, IMessageId expectedMessageID, String... expectedArgs) {

		try {
			callTesteeOnException(input);
			Assert.fail("期待した例外が発生しなかった。");
		} catch (Throwable e) {

			if (!(expectedException.isInstance(e)) || !(ITranslatable.class.isInstance(e))) {
				Assert.fail("期待した例外が発生しなかった。\n" + ExceptionUtils.getStackTraceString(e));
			}

			assertMessageId((ITranslatable) e, expectedMessageID);
			assertMessageParameters((ITranslatable) e, expectedArgs);
		}
	}

	public void verifyThrowError(T input, Class<E> expectedError, String expectedMessage) {

		try {
			callTesteeOnException(input);
			Assert.fail("期待したエラーが発生しなかった。");
		} catch (Throwable e) {

			if (!(expectedError.isInstance(e))) {
				Assert.fail("期待したエラーが発生しなかった。\n" + ExceptionUtils.getStackTraceString(e));
			}

			assertMessage(e, expectedMessage);
		}
	}

	private void assertMessageId(ITranslatable actual, IMessageId expectedMessageID) {
		assertThat(actual.getMessageID(), is(expectedMessageID));
	}

	private void assertMessageParameters(ITranslatable actual, String... expectedArgs) {
		if (TriStringUtils.isEmpty(expectedArgs)) {
			return;
		}

		assertThat("期待するメッセージパラメタの数と実績値が一致しない", actual.getMessageArgs().length, is(expectedArgs.length));

		for (int i = 0; i < expectedArgs.length; i++) {
			assertThat("期待するメッセージパラメタが設定されていない", actual.getMessageArgs()[i], is(expectedArgs[i]));
		}

	}

	private void assertMessage(Throwable actual, String expectedMessage) {
		assertThat(actual.getMessage(), is(expectedMessage));
	}

	public abstract void callTesteeOnException(T input) throws Exception;
}
