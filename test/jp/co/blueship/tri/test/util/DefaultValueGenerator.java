package jp.co.blueship.tri.test.util;

import java.lang.reflect.Field;
import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.lang.RandomStringUtils;

public final class DefaultValueGenerator {

	private static final Boolean[] BOOLEAN_VALUES = { true, false };

	private DefaultValueGenerator() {
	}

	public static <T> T createBeanWithDefaultValue(Class<T> beanClass) throws Exception {

		T bean = beanClass.newInstance();

		Field[] fields = bean.getClass().getDeclaredFields();
		for (Field field : fields) {
			@SuppressWarnings("unchecked")
			Class<T> type = (Class<T>) field.getType();
			if (type == Integer.class || type == Long.class || type == Double.class || type == Float.class) {
				BeanUtils.setProperty(bean, field.getName(), ConvertUtils.convert(RandomStringUtils.randomNumeric(5), type));
			}
			if (type == Boolean.class) {
				int index = Integer.parseInt(RandomStringUtils.random(1, "01"));
				BeanUtils.setProperty(bean, field.getName(), BOOLEAN_VALUES[index]);
			} else if (type == String.class) {
				BeanUtils.setProperty(bean, field.getName(), RandomStringUtils.randomAlphanumeric(10));
			} else if (type.isEnum()) {
				T[] enumConstants = type.getEnumConstants();
				BeanUtils.setProperty(bean, field.getName(), enumConstants[0]);
			} else if (type == Timestamp.class) {
				BeanUtils.setProperty(bean, field.getName(), new Timestamp(System.currentTimeMillis()));
			} else if (type == StatusFlg.class) {
				BeanUtils.setProperty(bean, field.getName(), StatusFlg.on);
			} else {
				// throw new
				// UnsupportedAttributeException("UnSupported Class Type:",
				// type.getSimpleName());
			}
		}

		return bean;
	}

}
