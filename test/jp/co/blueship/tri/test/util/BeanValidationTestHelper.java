package jp.co.blueship.tri.test.util;

import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;

import org.junit.Assert;

/**
 * BeanValidationテスト用ヘルパクラス。
 *
 * @author Takayuki Kubo
 *
 */
public abstract class BeanValidationTestHelper<T, E extends Throwable> extends ThrowExceptionTestHelper<T, E> {

	/**
	 * 期待したValidationエラーが発生することをテストし、検証する。
	 *
	 * @param input 入力値
	 * @param expectedMessageID 期待するエラーメッセージのID
	 * @param expectedArgs 期待するエラーメッセージのメッセージパラメタ。無い場合は省略。
	 * @throws Exception 予期しない例外が発生した場合
	 */
	public void verifyValidation(T input, Class<E> expectedException, IMessageId expectedMessageID, MessageParameter... expectedArgs) throws Exception {
		verifyThrowException(input, expectedException, expectedMessageID, expectedArgs);
	}

	/**
	 * 指定された入力値でバリデーションエラーが発生ししないことをテストし、検証する。
	 *
	 * @param input 入力値
	 * @throws Exception 予期しない例外が発生した場合
	 */
	public void verifyNoValidationError(T input) throws Exception {

		try {
			callTesteeOnValidation(input);
		} catch (NullPointerException e) {
		} catch (Exception e) {
			if (ITranslatable.class.isInstance(e)) {
				Assert.fail("期待しないエラーが発生してしまった。" + e.getMessage() + "\n" + ExceptionUtils.getStackTraceString(e));
			}
		}
	}

	@Override
	public void callTesteeOnException(T input) throws Exception {
		callTesteeOnValidation(input);
	}

	/**
	 * Validationテストを実行する
	 *
	 * @param input 入力値
	 * @throws Exception 予期しない例外が発生した場合
	 */
	public abstract void callTesteeOnValidation(T input) throws Exception;

}
