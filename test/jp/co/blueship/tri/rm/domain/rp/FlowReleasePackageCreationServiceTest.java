package jp.co.blueship.tri.rm.domain.rp;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean.ReleasePackageCreationInputInfo;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageCreationServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ReleasePackage-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1604010002";

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create(IGenericTransactionService service) throws Exception {
		FlowReleasePackageCreationServiceBean serviceBean = new FlowReleasePackageCreationServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		ReleasePackageCreationInputInfo inputInfo = serviceBean.getParam().getInputInfo();

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.RmReleasePackageCreationService.value(), serviceDto);
			this.debug(serviceBean);
			assertTrue(null == serviceBean.getResult().getRpId());
		}

		String ctgId = "8";
		String mstoneId = "8";

		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputInfo.setSubject("Test Tanka");
			inputInfo.setSummary("Rp Tanaka");
			inputInfo.setEnvId("RPENV_TRINITY_PDT");
			inputInfo.setBpId("BLD-1604130005");
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);

			service.execute(ServiceId.RmReleasePackageCreationService.value(), serviceDto);
			this.threadWait( serviceBean );
			this.debug(serviceBean);
		}
	}

	private void debug( FlowReleasePackageCreationServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("Release Environment View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getReleaseEnvViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Build Package View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getBuildPackageViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getCtgViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {

			System.out.println("RequestsCompletion ");
			System.out.println("   isCompleted: " + serviceBean.getResult().isCompleted());
			System.out.println("   BpId: " + serviceBean.getResult().getRpId());
		}
		System.out.println("");
	}
}
