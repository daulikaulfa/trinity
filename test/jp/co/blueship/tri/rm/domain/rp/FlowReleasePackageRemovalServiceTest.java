package jp.co.blueship.tri.rm.domain.rp;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.rm.dao.rp.IRpDao;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageRemovalServiceBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageRemovalServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ReleasePackage-Context.xml"};

	private static final String testRpId = "REL-1604250035";
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}
	
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean( "generalService" );
			((IService)service).init();
			
			this.remove(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void remove(IGenericTransactionService service) throws Exception {
		
		FlowReleasePackageRemovalServiceBean serviceBean = new FlowReleasePackageRemovalServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
			serviceBean.setLanguage("en");
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			
			serviceBean.getParam().setSelectedRpId(testRpId);
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.RmReleasePackageRemovalService.value(), serviceDto);
			this.debug(serviceBean);
			
			IRpDao rpDao = (IRpDao) this.getBean("rmRpDao");
			RpCondition condition = new RpCondition();
			condition.setRpId(testRpId);
			IRpEntity rpEntity = rpDao.findByPrimaryKey(condition.getCondition());
			
			assertTrue( serviceBean.getResult().isCompleted());
			assertTrue( null == rpEntity );
		}
	}
	
	private void debug( FlowReleasePackageRemovalServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
			
		}
	}
}
