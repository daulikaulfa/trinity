package jp.co.blueship.tri.rm.domain.rp;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean.ReleasePackageView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageDetailsServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ReleasePackage-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testRpId = "REL-1509180001";
	
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable) e)) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void details(IGenericTransactionService service) throws Exception {
		FlowReleasePackageDetailsServiceBean serviceBean = new FlowReleasePackageDetailsServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());

			serviceBean.getParam().setSelectedRpId(testRpId);
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);
		
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.setLanguage("en");
			
			service.execute(ServiceId.RmFlowReleasePackageDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}
	
	private void debug( FlowReleasePackageDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		ReleasePackageView view = serviceBean.getDetailsView();
		System.out.println("ReleasePackageDetailsView ");
		System.out.print("  RpId: " + view.getRpId());
		System.out.print("  StsId: " + view.getStsId());
		System.out.print("  Status: " + view.getStatus());
		System.out.print("  Name: " + view.getSubject());
		System.out.print("  Summary: " + view.getSummary());
		System.out.print("  RaId: " + view.getRaId());
		System.out.println("  EnvId: " + view.getEnvId());
		System.out.print("  EnvNm: " + view.getEnvNm());
		System.out.print("  BpId: " + view.getBpId());
		System.out.print("  CategoryNm: " + view.getCtgNm());
		System.out.print("  MstoneNm: " + view.getMstoneNm());
		System.out.print("  Created By: " + view.getCreatedBy());
		System.out.print("  CreatedTime: " + view.getStartTime());
		System.out.print("  EndTime: " + view.getEndTime());
		System.out.println("");
	}
}
