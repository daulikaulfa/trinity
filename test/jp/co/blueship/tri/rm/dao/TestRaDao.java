package jp.co.blueship.tri.rm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.rm.dao.ra.IRaDao;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;

import org.junit.Test;

public class TestRaDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() throws Exception {
		IRaDao dao = (IRaDao)this.getBean( "rmRaDao" );

		RaCondition condition = new RaCondition();
		condition.setBldEnvId("RM_IT");

		System.out.println( "condition : " + condition.getCondition().toQueryString() );
		IEntityLimit<IRaEntity> home = dao.find( condition.getCondition(), null, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IRaEntity entity: home.getEntities() ) {
			System.out.println( "raId:=" + entity.getRaId() );
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
			System.out.println( "bldEnvNm:=" + entity.getBldEnvNm().toString() );
		}

	}

}
