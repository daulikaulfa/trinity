package jp.co.blueship.tri.rm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.rm.dao.rp.IRpBpLnkDao;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;

import org.junit.Test;

public class TestRpBpLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		IRpBpLnkDao dao = (IRpBpLnkDao)this.getBean( "rmRpBpLnkDao" );

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setRpId( "REL-1408120001" );
		condition.setRpStsIds( RmRpStatusId.ReleasePackageRemoved.getStatusId() );
		condition.setDelStsId( null );

		IEntityLimit<IRpBpLnkEntity> home = dao.find( condition.getCondition(), null, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IRpBpLnkEntity entity: home.getEntities() ) {
			System.out.println( "rpId:=" + entity.getRpId() );
			System.out.println( "rpStsId:=" + entity.getRpStsId() );
		}

	}

}
