package jp.co.blueship.tri.rm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.rm.dao.ra.IRaRpLnkDao;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkCondition;

import org.junit.Test;

public class TestRaRpLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		IRaRpLnkDao dao = (IRaRpLnkDao)this.getBean( "rmRaRpLnkDao" );

		RaRpLnkCondition condition = new RaRpLnkCondition();
		condition.setRaId( "RA-1408110002" );
		condition.setRaStsIds( RmRaStatusId.ReleaseRequested.getStatusId() );
		condition.setDelStsId( null );

		IEntityLimit<IRaRpLnkEntity> home = dao.find( condition.getCondition(), null, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IRaRpLnkEntity entity: home.getEntities() ) {
			System.out.println( "raId:=" + entity.getRaId() );
			System.out.println( "raStsId:=" + entity.getRaStsId() );
		}

	}

}
