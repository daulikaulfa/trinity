package jp.co.blueship.tri.rm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.rm.dao.ra.IRaBpLnkDao;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkCondition;

import org.junit.Test;

public class TestRaBpLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		IRaBpLnkDao dao = (IRaBpLnkDao)this.getBean( "rmRaBpLnkDao" );

		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setRaId( "RA-1408110002" );
		condition.setRaStsIds( RmRaStatusId.ReleaseRequested.getStatusId() );
		condition.setDelStsId( null );

		IEntityLimit<IRaBpLnkEntity> home = dao.find( condition.getCondition(), null, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IRaBpLnkEntity entity: home.getEntities() ) {
			System.out.println( "raId:=" + entity.getRaId() );
			System.out.println( "raStsId:=" + entity.getRaStsId() );
		}

	}

}
