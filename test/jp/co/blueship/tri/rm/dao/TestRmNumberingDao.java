package jp.co.blueship.tri.rm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;

import org.junit.Test;

public class TestRmNumberingDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void nextval() {
		String[] beans = new String[]{
				"rmRaNumberingDao",
				"rmRaTempNumberingDao",
				"rmRpNumberingDao",
		};

		for ( String bean: beans ) {
			INumberingDao dao = (INumberingDao)this.getBean( bean );
			String id = dao.nextval();

			assertTrue( null != id );
			System.out.println( bean + " := " + id );
			assertTrue( ! id.equals( dao.nextval() ) );
		}

	}

}
