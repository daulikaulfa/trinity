package jp.co.blueship.tri.dcm.domain.dl;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusIdForExecData;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.test.util.BeanValidationTestHelper;

import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;

public class StatusCheckAddonUtilTest extends EasyMockSupport {

	private StatusCheckAddonUtil testee;
	/**
	 * 事前準備を行います。
	 */
	@Before
	public void setUp() {
		testee = new StatusCheckAddonUtil();
	}

	@SuppressWarnings("static-access")
	@Test
	public void testCheckReportAssetDownload() throws Exception {

		BeanValidationTestHelper<IRepEntity, ContinuableBusinessException> bvth = new BeanValidationTestHelper<IRepEntity, ContinuableBusinessException>() {

			@Override
			public void callTesteeOnValidation(IRepEntity entity) throws Exception {
				testee.checkReportAssetDownload(entity);
			}
		};

		// StsIdが正規の場合、エラーが発生しないこと
		IRepEntity entity = new RepEntity();
		entity.setStsId( DcmRepStatusId.ReportCreated.getStatusId() );
		bvth.verifyNoValidationError(entity);

		// StsIdがnullの場合、ContinuableBusinessExceptionとなることを確認する。(exceptionのmessageは発生しない)
		entity = new RepEntity();
		entity.setStsId( null );
		bvth.verifyValidation(entity, ContinuableBusinessException.class , null );

		// StsIdがReportComplete以外の場合、ContinuableBusinessExceptionとなることを確認する。(exceptionのmessageは発生しない)
		entity = new RepEntity();
		entity.setStsId( DcmRepStatusIdForExecData.CreatingReport.getStatusId() );
		bvth.verifyValidation(entity, ContinuableBusinessException.class , null );
	}

}
