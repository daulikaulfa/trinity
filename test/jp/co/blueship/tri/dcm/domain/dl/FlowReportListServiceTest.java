package jp.co.blueship.tri.dcm.domain.dl;

import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ReportView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class FlowReportListServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-Report-Context.xml" };
	
	private static final String testLotId = "LOT-1508070002";

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();
			this.reportList(service);
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void reportList(IGenericTransactionService service) throws Exception {
		FlowReportListServiceBean serviceBean = new FlowReportListServiceBean();

		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			
			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.DcmReportListService.value(), serviceDto);

			this.debug(serviceBean);

		}
		
		// OnChange
		{
//			serviceBean.getParam().getSearchCondition().setStsId("1003");
//			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
			serviceBean.getParam().getSearchCondition().setKeyword("002");
			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.DcmReportListService.value(), serviceDto);

			this.debug(serviceBean);
		}
	}

	private void debug(FlowReportListServiceBean serviceBean) {
		{
			System.out.println(String.format("========ReportView page #%d========",
					serviceBean.getParam().getSearchCondition().getSelectedPageNo()));
			List<ReportView> reportView = serviceBean.getReportViews();
			int i = 0;
			for (ReportView view : reportView) {
				System.out.println(String.format("==============reportView #%d================", ++i));

				System.out.println("reportId 				:= " + view.getReportId());
				System.out.println("type	 				:= " + view.getType());
				System.out.println("type value				:= " + view.getType().value());
				System.out.println("submitterName				:= " + view.getSubmitterNm());
				System.out.println("createdDate				:= " + view.getCreatedDate());
				System.out.println("stsId	 				:= " + view.getStsId());
				System.out.println("status	 				:= " + view.getStatus());
				System.out.println("isDownloadEnable			:= " + view.isDownloadEnable());

				System.out.println("==============================");
			}
			System.out.println("pagination				:= " + serviceBean.getPage().getViewRangeFrom());
			System.out.println("pagination				:= " + serviceBean.getPage().getViewRangeTo());
			System.out.println("pagination				:= " + serviceBean.getPage().getMaxPageNo());
			System.out.println("pagination				:= " + serviceBean.getPage().getSelectPageNo());
		}
	}

}
