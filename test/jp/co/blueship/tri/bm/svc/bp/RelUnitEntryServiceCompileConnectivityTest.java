package jp.co.blueship.tri.bm.svc.bp;

import jp.co.blueship.tri.bm.domain.bp.beans.RelUnitEntryServiceCompile;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.ITranslatable;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Test-Release-All-Context.xml", "classpath:Test-RelUnitEntryServiceCompile.xml" })
@TransactionConfiguration
@Transactional
@Ignore("障害再現用テスト。DB書き込みを伴うため使用する際は熟慮すること")
public class RelUnitEntryServiceCompileConnectivityTest {

	@Autowired
	@Qualifier("relUnitEntryServiceCompile")
	private RelUnitEntryServiceCompile testee;

	@Autowired
	@Qualifier("smProcMgtNumberingDao")
	private INumberingDao numberingDao;

	@Autowired
	private ApplicationContext context;

	@Before
	public void setUp() {
		Contexts.getInstance().setApplicationContext(context);
	}

	@Test
	public void testExecute() throws Exception {

		IServiceDto<FlowRelUnitEntryServiceBean> serviceDto = new ServiceDto<FlowRelUnitEntryServiceBean>();
		FlowRelUnitEntryServiceBean paramBean = new FlowRelUnitEntryServiceBean();
		paramBean.setUserId("test01");
		paramBean.setUserName("権限-あり01");
		paramBean.setBuildNo("BLD-1407250099");
		paramBean.setProcId(numberingDao.nextval());
		serviceDto.setServiceBean(paramBean);

		try {
			testee.execute(serviceDto);
		} catch (Exception e) {
			System.out.println(messageOf(e));
			throw e;
		}

	}

	private String messageOf(Exception e) {

		if(ITranslatable.class.isInstance(e)) {
			return ContextAdapterFactory.getContextAdapter().getMessage(((ITranslatable)e).getMessageID(), ((ITranslatable)e).getMessageArgs());
		}

		return e.getMessage();
	}
}
