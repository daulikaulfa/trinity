package jp.co.blueship.tri.bm.beans;

import static org.junit.Assert.assertThat;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:Test-FlowRelUnitSupport.xml", "classpath:Fw-Module-Context.xml" })
public class FlowRelUnitEditSupportConnectivityTest extends FlowRelUnitEditSupport {

	@Autowired
	private FlowRelUnitEditSupport testee;

	@Test
	public void testGetTaskFlowEntity() throws Exception {

		ITaskFlowEntity[] actual = testee.getTaskFlowEntity();

		assertThat(TriStringUtils.isNotEmpty(actual), CoreMatchers.is(true));

	}

}
