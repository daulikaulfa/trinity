package jp.co.blueship.tri.bm.domain.bp;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class FlowBuildPackageRemovalServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-BuildPackage-Context.xml"};

	private static final String testBpId = "BLD-1604210004";
	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}
	
	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean( "generalService" );
			((IService)service).init();
			
			this.remove(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private void remove(IGenericTransactionService service) throws Exception {
		
		FlowBuildPackageRemovalServiceBean serviceBean = new FlowBuildPackageRemovalServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
			serviceBean.setLanguage("en");
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			
			serviceBean.getParam().setSelectedBpId(testBpId);
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.BmBuildPackageRemovalService.value(), serviceDto);
			this.debug(serviceBean);
			
			IBpDao bpDao = (IBpDao) this.getBean("bmBpDao");
			BpCondition condition = new BpCondition();
			condition.setBpId(testBpId);
			IBpEntity bpEntity = bpDao.findByPrimaryKey(condition.getCondition());
			
			assertTrue( serviceBean.getResult().isCompleted());
			assertTrue( null == bpEntity );
		}
	}
	
	private void debug( FlowBuildPackageRemovalServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
			
		}
	}
}
