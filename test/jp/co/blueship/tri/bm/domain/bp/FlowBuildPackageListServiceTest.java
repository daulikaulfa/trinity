package jp.co.blueship.tri.bm.domain.bp;

import org.junit.Test;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.BuildPackageView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowBuildPackageListServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-BuildPackage-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1604010002";

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService) service).init();
			this.list(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void list(IGenericTransactionService service) throws Exception {
		String testCtgId = "8";
		String testMstoneId = "8";
		String testStsId = "2001";
		String testKeyword = "tanaka";

		FlowBuildPackageListServiceBean serviceBean = new FlowBuildPackageListServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedLotId(testLotId);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(1);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.BmBuildPackageListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		//onChange
		{
			{
				int testCase = 4;

				switch (testCase) {
					case 1:
						//Category Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						break;

					case 2:
						//Milestone Search
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 3:
						//Status Search
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						break;

					case 4:
						//keyword search
						serviceBean.getParam().getSearchCondition().setKeyword(testKeyword);
						break;

					case 5:
						//Combine search
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						break;

					default:
						break;
				}
			}

			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.BmBuildPackageListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return;
	}

	private void debug( FlowBuildPackageListServiceBean serviceBean ) {
		System.out.println("================================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("================================");
		System.out.println("");

		System.out.println("Search Condition ");
		System.out.print("  CtgId:= " + serviceBean.getParam().getSearchCondition().getCtgId() + "   ");
		System.out.print("  MstoneId:= " + serviceBean.getParam().getSearchCondition().getMstoneId());
		System.out.print("  StsId:= " + serviceBean.getParam().getSearchCondition().getStsId() + "   ");
		System.out.println("  Keyword " + serviceBean.getParam().getSearchCondition().getKeyword());
		System.out.println("");

		System.out.println("Status View " );
		for ( ItemLabelsBean status: serviceBean.getParam().getSearchCondition().getStatusViews() ) {
			System.out.print("  label: " + status.getLabel());
			System.out.print("  value: " + status.getValue());

		}
		System.out.println("");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getCtgViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");
		System.out.println("");

		System.out.println("Build Package List");
		for ( BuildPackageView view : serviceBean.getBuildPackageViews() ) {
			System.out.print("   BpId: " + view.getBpId() );
			System.out.print("   Bp Subject: " + view.getSubject() );
			System.out.print("   Created By: " + view.getCreatedBy() );
			System.out.print("   Created Date: " + view.getCreatedDate() );
			System.out.print("   Status: " + view.getStatus() );
			System.out.print("   isError: " + view.isError() );
			System.out.println("");
		}
		System.out.println("");

		System.out.println("Page List Info");
		System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
		System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
		System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
		System.out.print("  ViewRows:= " + serviceBean.getPage().getViewRows());
		System.out.print("  ViewFrom:= " + serviceBean.getPage().getViewRangeFrom());
		System.out.println("  ViewTo:= " + serviceBean.getPage().getViewRangeTo());
		System.out.println("");
	}
}
