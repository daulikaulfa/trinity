package jp.co.blueship.tri.bm.domain.bp;

import org.junit.Test;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.BuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.ChangePropertyView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.ProcessingResultView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.RequestResourcesView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageDetailsServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-BuildPackage-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testBpId = "BLD-1509230010";

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable) e)) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void details(IGenericTransactionService service) throws Exception {
		FlowBuildPackageDetailsServiceBean serviceBean = new FlowBuildPackageDetailsServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());

			serviceBean.getParam().setSelectedBpId(testBpId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.setLanguage("en");

			service.execute(ServiceId.BmBuildPackageDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}

	}

	private void debug( FlowBuildPackageDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		BuildPackageView view = serviceBean.getDetailsView();
		System.out.println("BuildPackageDetailsView ");
		System.out.print("  BpId: " + view.getBpId());
		System.out.print("  StsId: " + view.getStsId());
		System.out.print("  Status: " + view.getStatus());
		System.out.print("  Name: " + view.getSubject());
		System.out.print("  SubmitterNm: " + view.getCreatedBy());
		System.out.println("  CreatedTime: " + view.getStartTime());
		System.out.print("  EndTime: " + view.getEndTime());
		System.out.print("  Summary: " + view.getSummary());
		System.out.print("  BuildDiff: " + view.isBuildDiff());
		System.out.print("  BuildFull: " + view.isBuildFull());
		System.out.print("  CategoryNm: " + view.getCtgNm());
		System.out.print("  MstoneNm: " + view.getMstoneNm());
		System.out.println("");

		System.out.println("TotalResources ");
		System.out.print("  TotalCheckout " + view.getTotalResources().getCheckoutResources());
		System.out.print("  TotalCheckinChanged " + view.getTotalResources().getChangedResources());
		System.out.print("  TotalCheckinRemovedAscii " + view.getTotalResources().getDeletedAsciiResources());
		System.out.print("  TotalCheckinRemovedBinary " + view.getTotalResources().getDeletedBinaryResources());
		System.out.print("  TotalCheckinCreated " + view.getTotalResources().getAddedResources());
		System.out.println("");

		System.out.println("ChangePropertyList ");
		for (ChangePropertyView changeProperty : view.getChangeProperties()) {
			System.out.println("  PjtId " + changeProperty.getPjtId());
			System.out.println("  ReferenceId " + changeProperty.getReferenceId());
			for (RequestResourcesView resource : changeProperty.getResources()) {
				System.out.print("  RequestType " + resource.getAreqType().toString());
				System.out.print("  AreqId " + resource.getAreqId());
				System.out.print("  Checkout " + resource.getCheckoutResources());
				System.out.print("  CheckinChanged " + resource.getChangedResources());
				System.out.print("  CheckinRemoved " + resource.getDeletedAsciiResources());
				System.out.print("  CheckinCreated " + resource.getAddedResources());

			}
		}
		System.out.println("");

		System.out.println("Process Result");
		for (ProcessingResultView process : view.getProcessResults()) {
			System.out.print("  SequenceNumber " + process.getSequenceNo());
			System.out.print("  ProcessName " + process.getProcessNm());
			System.out.print("  Completed: " + process.isCompleted());
			System.out.println("  Error: " + process.isError());
		}
		System.out.println("");
	}
}
