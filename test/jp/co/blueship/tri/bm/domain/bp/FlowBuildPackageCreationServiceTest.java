package jp.co.blueship.tri.bm.domain.bp;

import static org.junit.Assert.*;

import org.junit.Test;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.BuildPackageCreationInputInfo;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.ChangePropertyView;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageCreationServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-BuildPackage-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private static final String testLotId = "LOT-1605230001";

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.create(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void create(IGenericTransactionService service) throws Exception {
		FlowBuildPackageCreationServiceBean serviceBean = new FlowBuildPackageCreationServiceBean();
		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		BuildPackageCreationInputInfo inputInfo = serviceBean.getParam().getInputInfo();

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.BmBuildPackageCreationService.value(), serviceDto);
			this.debug(serviceBean);
			assertTrue(null == serviceBean.getResult().getBpId());
		}

		String ctgId = null;
		String mstoneId = null;
		boolean testFullBuild = false;

		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputInfo.setSubject("Test BP");
			inputInfo.setSummary("Bp Summary");
			inputInfo.setCtgId(ctgId);
			inputInfo.setMstoneId(mstoneId);

			if ( !testFullBuild ) {
				String[] pjtID = {"PJT-1605230008"};
				inputInfo.setSelectedPjtIds(pjtID);
			}
			inputInfo.setBuildDiff( !testFullBuild );
			inputInfo.setBuildFull( testFullBuild );

			service.execute(ServiceId.BmBuildPackageCreationService.value(), serviceDto);
			this.threadWait(serviceBean);
			this.debug(serviceBean);
		}
	}

	private void debug( FlowBuildPackageCreationServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getCtgViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		if ( RequestType.init.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("ChangePropertyView ");
			for ( ChangePropertyView pjtView : serviceBean.getChangePropertyViews() ) {
				System.out.print("  PjtId: " + pjtView.getPjtId() );
				System.out.print("  Subject: " + pjtView.getSummary() );
				System.out.print("  Reference Id: " + pjtView.getReferenceId() );
				System.out.print("  Created By: " + pjtView.getSubmitterNm() );
				System.out.print("  Request Approved Date: " + pjtView.getApprovedTime() );
				System.out.println("");
			}
		}

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("");
			for ( String pjtView : serviceBean.getParam().getInputInfo().getSelectedPjtIds() ) {
				System.out.print("PjtId: " + pjtView );
				System.out.println("");
			}

			System.out.println("RequestsCompletion ");
			System.out.println("   isCompleted: " + serviceBean.getResult().isCompleted());
			System.out.println("   BpId: " + serviceBean.getResult().getBpId());
		}
		System.out.println("");
	}
}
