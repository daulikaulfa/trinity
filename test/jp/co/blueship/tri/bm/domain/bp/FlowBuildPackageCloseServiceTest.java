package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.ClosableBuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.FlowBuildPackageCloseInputInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */

public class FlowBuildPackageCloseServiceTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-BuildPackage-Context.xml"};

	private static final String testLotId = "LOT-1604140002";
	private List<String> selectedBpIds = new ArrayList<String>();

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.buildPackageInit(service);

			this.buildPackageClose(service);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void buildPackageInit(IGenericTransactionService service) throws Exception {
		FlowBuildPackageCloseServiceBean serviceBean = new FlowBuildPackageCloseServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// Init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.BmBuildPackageCloseService.value(), serviceDto);

			if (TriStringUtils.isNotEmpty(serviceBean.getClosableViews())) {
				System.out.println("=================================");
				for (ClosableBuildPackageView view: serviceBean.getClosableViews()) {
					System.out.println("bpId: " + view.getBpId());
					System.out.println("bpName: " + view.getSubject());
					System.out.println("createdBy: " + view.getCreatedBy());
					System.out.println("createdDate: " + view.getCreatedTime());
					if (TriStringUtils.isNotEmpty(view.getRpIds())) {
						System.out.println("rpId: ");
						for (String rpId: view.getRpIds()) {
							System.out.println("      " + rpId);
						}
					}
					selectedBpIds.add(view.getBpId());
					System.out.println("===================================");
				}
			} else {
				return;
			}

		}
	}

	private void buildPackageClose(IGenericTransactionService service) throws Exception {

		FlowBuildPackageCloseServiceBean serviceBean = new FlowBuildPackageCloseServiceBean();

		{
			serviceBean.setUserId(this.getAuthUserId());
			serviceBean.setUserName(this.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedLotId(testLotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		FlowBuildPackageCloseInputInfo inputInfo = serviceBean.getParam().getInputInfo();

		// init before submitchanges
		{
			inputInfo.setSelectedBpIds(selectedBpIds.toArray(new String[0]));

			serviceBean.getParam().setRequestType(RequestType.validate);
			service.execute(ServiceId.BmBuildPackageCloseService.value(), serviceDto);
		}

		// submitChanges
		{
			inputInfo.setBaseLineTag("20160413-Test");
			inputInfo.setComment("Some Reason");

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.BmBuildPackageCloseService.value(), serviceDto);

			this.threadWait( serviceBean );
		}
	}
}
