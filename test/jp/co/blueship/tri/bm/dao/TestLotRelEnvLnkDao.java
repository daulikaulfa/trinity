package jp.co.blueship.tri.bm.dao;

import static org.junit.Assert.assertTrue;
import jp.co.blueship.tri.am.dao.lot.ILotRelEnvLnkDao;
import jp.co.blueship.tri.am.dao.lot.constants.LotRelEnvLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkCondition;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

import org.junit.Test;

public class TestLotRelEnvLnkDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() {
		ILotRelEnvLnkDao dao = (ILotRelEnvLnkDao)this.getBean( "amLotRelEnvLnkDao" );

		LotRelEnvLnkCondition condition = new LotRelEnvLnkCondition();
		condition.setLotId( "LOT-1" );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotRelEnvLnkItems.bldEnvId, TriSortOrder.Desc, 1);

		IEntityLimit<ILotRelEnvLnkEntity> home = dao.find( condition.getCondition(), sort, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( ILotRelEnvLnkEntity entity: home.getEntities() ) {
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "bldRelEnvId:=" + entity.getBldEnvId() );
			System.out.println( "bldRelEnvNm:=" + entity.getBldEnvNm() );
		}

	}

}
