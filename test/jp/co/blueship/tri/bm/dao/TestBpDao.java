package jp.co.blueship.tri.bm.dao;

import static org.junit.Assert.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import org.junit.Ignore;
import org.junit.Test;

import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.TestTriJdbcDaoSupport;

public class TestBpDao extends TestTriJdbcDaoSupport {

	//@Ignore
	@Test
	public void find() throws Exception {
		IBpDao dao = (IBpDao)this.getBean( "bmBpDao" );

		BpCondition condition = new BpCondition();
		condition.setBpId( "BLD-1508040001" );
		Timestamp tm1 = new Timestamp( new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" ).parse( "2014/06/24 10:00:00" ).getTime() );
		Timestamp tm2 = new Timestamp( new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" ).parse( "2014/06/25 10:00:00" ).getTime() );
		condition.setCloseTimestamp( tm1, tm2 );
		condition.setCtgId("16030002");

		System.out.println( "condition : " + condition.getCondition().toQueryString() );
		IEntityLimit<IBpEntity> home = dao.find( condition.getCondition(), null, 1, 0 );

		assertTrue( null != home );
		assertTrue( null != home.getEntities() );
		assertTrue( 0 < home.getEntities().size());

		for ( IBpEntity entity: home.getEntities() ) {
			System.out.println( "bpId:=" + entity.getBpId() );
			System.out.println( "lotId:=" + entity.getLotId() );
			System.out.println( "stsId:=" + entity.getStsId() );
			System.out.println( "procStsId:=" + entity.getProcStsId() );
			System.out.println( "closeTimestamp:=" + entity.getCloseTimestamp().toString() );
		}

	}
	@Ignore
	@Test
	public void insert() {
		IBpDao dao = (IBpDao)this.getBean( "bmBpDao" );

		IBpEntity entity = new BpEntity();
		entity.setBpId("Bp0001");
		entity.setBpNm("Bp0001 name");
		entity.setCloseTimestamp(TriDateUtils.getSystemTimestamp());
		entity.setDelStsId(StatusFlg.off);

		int count = dao.insert( entity );

		assertTrue( 1 == count );

	}

}
