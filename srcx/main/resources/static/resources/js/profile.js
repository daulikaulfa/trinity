var app = app || {};

app.member = {
  search: function() {
    $(".clearSearchFieldButton").click(function() {
      $(this).closest("form").trigger("reset");
      return false;
    });
  }
};

app.profile = {
  prepare: function() {
    $( "#tabs" ).tabs();

    $(".profileEditForm .editProfileLink").on("click", function() {
      $(".profileEditForm").toggleClass("editing");
      $(".profileEditForm")[0].reset();
      return false;
    });

    $(".editProfileAddURLButton").on("click", function() {
	  var template = '<div class="form-group urlFieldWrapper" style="display: block;">' + 
          		  '<input type="url" class="form-control input-sm urlField validation validationMessageId validationMessagePosition validationCustomPattern" ' +
          		  	'data-validationMessageId="formError_url"' + 
    				'data-validationMessagePosition="url_invalid_message_location"' + 
    				'data-validationCustomPattern="url"' +
          		  ' name="urls" value="" maxlength="256" /> </div> '; 
	  $(".urlFieldsInsertPosition").before(template);

      return false;
    });
  }
};
