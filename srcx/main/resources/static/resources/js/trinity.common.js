/**
 * trinity.common.js
 *
 * @version V4.00.00
 * @author Blueship
 *
 * @version V4.01.00
 * @author Blueship
 *
 * This file is fragmentated as below:
 *   form/foundation
 *     addMilestone/Category
 *     Band Popup
 *       clipboard
 *       comment
 *     Folder Tree
 *     globalnav
 *     Folder Tree
 *     lotMenuSearchLot
 *     Search Filter
 *     popup window
 *   ajax
 *   CheckInFileDifferences
 *   error message
 *   message
 *   secret hacking
 *   validation
 *   wiki
 * so when you create, update, delete some scripts,
 * follow this fragment for now.
 *
 * tab rule: soft tab (use 2 spaces)
 */


// =============== form/foundation(function) ===============

/**
 * return context path like this "/trinity/"
 */
function getContextPath() {
  return $("#contextPath").val();
}


/**
 * Do not call this function from User Interface
 * Use safeSubmit()
 * @param formId
 */
function submit(formId) {
  var formObj = $('#'+formId);
  appendWindowId(formObj);
  formObj.submit();
}


/**
 * Flag for is SubmitForm running.
 */
var submitFlag = false;
function clearSubmitFlag() {
  submitFlag = false;
}
function setSubmitFlag() {
  submitFlag = true;
}


/**
 * submit with validation and prevention double sending
 * @param formId
 * @param param.validation = true
 * @returns false
 */
function safeSubmit(formId, param) {
  if (submitFlag || !formId) return;
  setSubmitFlag();	// never clear

  if (param) {
    if (param.validation) {
      if (!validateOnSubmit()) {
        clearSubmitFlag();
        return;
      }
    }
  }

  submit(formId);
  //	clearSubmitFlag();	//never clear
  return false;
}


var logoutFlag = false;
var loginFlag = false;
function logout(formId, param) {
  logoutFlag = true;
  safeSubmit(formId, param);
}
function login(formId, param) {
  loginFlag = true;
  safeSubmit(formId, param);
}


/**
 * set WindowId: set WindowId to manage session
 * @param formObj
 */
function appendWindowId(formObj) {
  $('#WindowsId').remove();
  var windowId = $('#windowId').val();
  $("<input type='hidden' />")
  .attr("value",windowId )
  .attr("id","WindowsId" )
  .attr("name", "WindowsId")
  .appendTo(formObj);
}


function clearFormWindowId(formObj) {
  $(formObj).remove('.WindowsId');
}


/**
 * set RequestType:submitChanges to form, and after safeSubmit()
 * @param formId
 */
function submitChanges(formId) {
  setParamsToForm(formId, {'RequestType': 'submitChanges'});
  safeSubmit(formId);
}


/**
 * set parameters{} to form
 * @param formId
 * @param params
 */
function setParamsToForm(formId, params) {
  $.each(params, function(key, val) {
    $('#' + formId + ' > input[name="' + key +'"]').val(val);
  });
}


/**
 * set action to form
 * @param formId
 * @param action
 */
function setActionToForm(formId, action) {
  $('#' + formId).attr('action', action);
}


function cloneWindow(formId) {
  // remove redirect window id from session
  removeRedirectWindowsId();
  // open new window
  var windowName = new Date().getTime();
  window.open('', windowName);
  var formObj = document.getElementById(formId);
  $(formObj).find('input[name="RequestType"]').val('init');
  formObj.target = windowName;
  formObj.submit();
}


function openOverviewInNewWindow(formId, param) {
  setParamsToForm(formId, param);
  cloneWindow(formId);
}


/*
 * Flag for is Dialog opening.
 */
var dialogFlag = false;
function clearDialogFlag() {
  dialogFlag = false;
}
function setDialogFlag() {
  dialogFlag = true;
}


/**
 * - javascript class -<br>
 * This class is for using mask layer.<br>
 */
MaskLayer = function() {
};
MaskLayer.prototype = {
  show: function() {
    $("#bandPopupBackground").height(document.body.clientHeight);
    $("#bandPopupBackground").show();
  },
  visible: function( visible ) {
    if( visible ) {
      $("#bandPopupBackground").show();
    }else{
      $("#bandPopupBackground").hide();
    }
  },
  startWaiting: function(targetElementId) {
    this.visible(true);

    var width = $("#"+targetElementId).outerWidth();
    var height = $("#"+targetElementId).outerHeight();
    var top = $("#"+targetElementId).offset().top + 6;
    var left = $("#"+targetElementId).offset().left;
    var html = '<div id="' + targetElementId + '_waitingMask" style="width: ' + width + 'px; height: ' + height + 'px; left: ' + left + 'px; top: ' + top + 'px; position:absolute; background-color: #FFFFFF; opacity: 0.7; text-align: center; display: table-cell; vertical-align: middle; z-index: 1000;"><div><i class="fa fa-refresh fa-spin fa-4x" aria-hidden="true"></i></div></div>';
    $("body").append($(html));
  },
  endWaiting: function(targetElementId) {
    this.visible(false);
    $("#"+targetElementId+"_waitingMask").remove();
  },
};


/**
 * - javascript class -<br>
 * This class is for using flash message layer.<br>
 */
FlashMessageLayer = function() {
  this.flashMessageObj = $("#infoDialog").find(".bandPopup");
};
FlashMessageLayer.prototype = {
  show: function() {
    $(this.flashMessageObj).fadeIn( popupFadeTime );
    $(this.flashMessageObj).delay( popupDisplayTime ).fadeOut(popupFadeTime);
    return this;
  },
};


/*
 * Return a layer object by using argument.
 * Return null if argument is layer that do not exist.
 *
 * 引数に応じたレイヤーオブジェクトが返されます。
 * 存在しないレイヤーを指定した場合はnullが返されます。
 */
function getTriLayer( layerName ) {
  if( layerName == "flashMessage" ) {
    return new FlashMessageLayer();
  }else if(layerName == "mask") {
    return new MaskLayer();
  }
  return null;
}


function goBack() {
  window.history.back();
}


function removeRedirectWindowsId() {
  var urlRedirect = getContextPath() + 'common/windows-id/remove';
  var formData = new FormData();

  $.ajax({
    url: urlRedirect,
    type: "POST",
    data: formData,
    contentType: false,
    processData: false,
    success: function(data, textStatus) {
      console.log('success');
    },
    fail: function(data, textStatus) {
      console.log("remove fail");
    }
  });
}


// =============== form/foundation(event) ===============

$(window).on('beforeunload', function() {
  if(!logoutFlag && !loginFlag ) {
    var urlRedirect = getContextPath() + 'common/windows-id/save';
    var formData = new FormData();
    formData.append("WindowsId", $('#windowId').val());
    $.ajax({
      url: urlRedirect,
      type: "POST",
      data: formData,
      contentType: false,
      processData: false,
      success: function(data, textStatus) {
        console.log('success');
      },
      fail: function(data, textStatus) {
        console.log("beforeunload fail");
      }
    });
  }
});


// =============== addMilestone/Category(function) ===============

function serializeWindowId(formId) {
  var formObj = $('#'+formId);
  appendWindowId(formObj);
  return formObj.serialize();
}


function addMilestone(formId) {
  $("#mstoneButtonWaiting").show();
  var form = document.getElementById(formId);
  var submitForm = document.getElementById('FlowMilestoneCreationService');
  submitForm.milestoneSubject.value = form.milestoneSubject.value;
  appendWindowId(form)
  var url = getContextPath() + 'lot/settings/milestone/create';
  timeOut = setTimeout(function() {
    $.post(url, serializeWindowId('FlowMilestoneCreationService')).done(
      function(data, status, xhr) {
        $("#message").html($(data).find("#message").html());

        if ($(data).find("#mstoneId").html() != undefined) {
        $("#mstoneId").html($(data).find("#mstoneId").html());
        $("#mstoneList").html($(data).find("#mstoneList").html());
        $("#addMilestone .windowOpenBtn .faBtnChange").trigger("click");
        $("#infoDialog").html($(data).find("#infoDialog").html());
        form.milestoneSubject.value = '';
        getTriLayer("flashMessage").show();
        }
        if ($(".errorMessage")[0] != null) {
        var timeOut = setTimeout(function() {
          $("html,body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
          $(".sectionErrorMessage").show();
        }, 200);
        }
        $("#mstoneButtonWaiting").hide();
      }).fail(function(XMLHttpRequest, status, errorThrown) {
        $("#mstoneButtonWaiting").hide();
      });
  }, 500);
}


function addCategory(formId) {
  $("#ctgButtonWaiting").show();
  var form = document.getElementById(formId);
  var submitForm = document.getElementById('FlowCategoryCreationService');
  submitForm.categorySubject.value = form.categorySubject.value;
  var url = getContextPath() + 'lot/settings/category/create';
  timeOut = setTimeout(function() {
    $.post(url, serializeWindowId('FlowCategoryCreationService')).done(
      function(data, status, xhr) {
        $("#message").html($(data).find("#message").html());
        if ($(data).find("#ctgId").html() != undefined) {
          $("#ctgId").html($(data).find("#ctgId").html());
          $("#categoryList").html($(data).find("#categoryList").html());
          $("#addCategory .windowOpenBtn .faBtnChange").trigger("click");
          $("#infoDialog").html($(data).find("#infoDialog").html());
          form.categorySubject.value = '';
          getTriLayer("flashMessage").show();
        }
        if ($(".errorMessage")[0] != null) {
          var timeOut = setTimeout(function() {
            $("html,body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
            $(".sectionErrorMessage").show();
          }, 200);
        }
        $("#ctgButtonWaiting").hide();
    }).fail(function(XMLHttpRequest, status, errorThrown) {
      $("#ctgButtonWaiting").hide();
    });
  }, 500);
}


// =============== addMilestone/Category(event) ===============


// =============== Band Popup(function) ===============

var popupFadeTime = 500;
var popupDisplayTime = 2500;
var popupParams;


function openBandPopup(params){
  setDialogFlag();
  $("#bandPopupBackground").height(document.body.clientHeight);
  $("#bandPopupBackground").show();

  popupParams = params;
  var bandPopup = $("#confirmDialog .bandPopup");
  var bandPopupMsg = $("#confirmDialog").find(".bandPopupMessage");
  var bandPopupBtn = $("#confirmDialog").find(".bandPopupButton");

  $(bandPopup).fadeIn(popupFadeTime);
  $(bandPopupMsg).html(params.message.split('&lt;br /&gt;').join('<br />'));


  if(params.btn1Text==null && params.btn2Text==null) {
    $(bandPopupBtn).html('');
    $(bandPopup).delay(params.displayTime).fadeOut(popupFadeTime);
    $("#bandPopupBackground").hide();
    clearDialogFlag();
  } else if (params.btn2Text==null && params.btn1Text!=null) {
    $(bandPopupBtn).html('<span><a href="javascript:void(0)" class="btn btn-default" onClick="popupBtnFunc('+"'btn1'"+')">'+params.btn1Text+'</a></span>');
  } else{
    $(bandPopupBtn).html('<span><a href="javascript:void(0)" class="btn btn-default" onClick="popupBtnFunc('+"'btn1'"+')">'+params.btn1Text+'</a></span>'+
    '<span><a href="javascript:void(0)" class="btn btn-default" onClick="popupBtnFunc('+"'btn2'"+')">'+params.btn2Text+'</a></span>');
  }
}


function popupBtnFunc(btn) {
  if(btn == "btn1"){
    popupParams.btn1Func();

  }else if(btn == "btn2") {
    popupParams.btn2Func();
  }
  $("#confirmDialog .bandPopup").fadeOut(popupFadeTime);
  $("#bandPopupBackground").hide();
  clearDialogFlag();
}


function fadeoutBandPopup() {
  $('.bandPopup').fadeOut(popupFadeTime);
}


// =============== Band Popup(event) ===============

$('#bandPopupBackground, .bandPopup').click(function(event) {
  event.stopPropagation();
});


// =============== clipboard(function) ===============


// =============== clipboard(event) ===============

var globalheader_clipboard_checkout = new Clipboard('.globalheader_clipboard_checkout');
var globalheader_clipboard_checkin = new Clipboard('.globalheader_clipboard_checkin');


globalheader_clipboard_checkout.on('success', function(e) {
  openBandPopup({
    // message:"貸出フォルダをクリップボードにコピーしました",
    message: messages.ChkInOutReqList_messageCopypathChkOut_text,
    btn1Text: null,
    btn2Text: null,
    displayTime: 2500
  });
  e.clearSelection();
});


globalheader_clipboard_checkin.on('success', function(e) {
  openBandPopup({
    message: messages.ChkInOutReqList_messageCopypathChkIn_text,
    btn1Text: null,
    btn2Text: null,
    displayTime: 2500
  });
  e.clearSelection();
});


// =============== comment(function) ===============

var isCommentChangedFlag = false;
function clearIsCommentChangedFlagFlag() {
  isCommentChangedFlag = false;
}
function setIsCommentChangedFlagFlag() {
  isCommentChangedFlag = true;
}


function hideComment() {
  $(".fukidashiHeader").hide();
  $("#replace_reason").fadeOut(200);
  $("#replace_reason").html("");
  $(".fukidashiHeader").removeClass("opened");
}


function removeCommentContainer() {
  $("#comment_container_temp").remove();
  $("#comment_container_after").remove();
}


function closeComment() {
  if (xhrFlag) return;
  if( !closeCommentBox() ){
    return false;
  }
  if ($(".fukidashiHeader").hasClass("opened")) {
    hideComment();
  }
  if ($("#replace_reason .actionCheck").length == 1) {
    hideComment();
  }
  removeCommentContainer();
  return;
}


function closeCommentBox() {
  if (!$(".fukidashiHeader").hasClass("opened") && !$(".actionCheck")) {
    return true;
  }
  if (isCommentChangedFlag) {
    if (!confirm(messages.commentDialog_warningMessageOnCloseDialog_text)) {
      return false;
    }
    clearIsCommentChangedFlagFlag();
  }
  hideComment();
  return true;
}


function toggleComment(formId, id_prefix) {

  if( !closeCommentBox() ) {
    return false;
  }

  // if self is opened return
  if ($("#" + id_prefix + "FukidashiHeader").hasClass("opened")) {
    return;
  }

  $("#" + id_prefix + "CommentButtonWaiting").show();

  $("#message").html("");

  var form = $("#" + formId);
  var _url = form.attr("action");
  appendWindowId(form);
  var _data = form.serialize();
  var _timeout = 3000;

  $.ajax({
    type: "POST",
    url: _url,
    data: _data,
    timeout: _timeout,
    success: function(data) {
      $("#message").html($(data).find("#message").html());
      $("#replace_reason").html($(data).find("#replace_reason").html());
      $("#" + id_prefix + "FukidashiHeader").addClass("opened");
      $("#" + id_prefix + "FukidashiHeader").fadeIn(200);
      $("#replace_reason").fadeIn(200);
      var message = $(data).find("#message").html();
      if ( $(message).find(".dl-systemErrorHeader").length > 0 ) {
        $('body').html(data);
        $('body').attr('class','loginBody');
      }

      if ($(".errorMessage")[0] != null && !$("#commentMessages").length ) {
        $("#" + id_prefix + "FukidashiHeader").removeClass("opened");
        $("#" + id_prefix + "FukidashiHeader").hide();
        $("#replace_reason").html("");
        removeCommentContainer();

        timeOut = setTimeout(function() {
        $("html,body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
        $(".sectionErrorMessage").show();
        }, 200);
      } else if ($("#replace_reason .actionCheck")[0] != null) {
        $("html,body").animate({scrollTop:$('#replace_reason').offset().top-$("nav#globalNav").height()-100 });
      }
    },
    error: function(XMLHttpRequest, status, errorThrown) {
      console.log("fail");
      var html = '<div class="errorMessage"><dl class="dl-horizontal dl-errormsg"><dt class="zero"></dt><dd>';
      html += XMLHttpRequest.responseText;
      html += '</dd></dl></div>';
      console.log(html);
      $("#message").html(html);
      $("#" + id_prefix + "FukidashiHeader").removeClass("opened");
      $("#" + id_prefix + "FukidashiHeader").hide();
      removeCommentContainer();
      timeOut = setTimeout(function() {
        $("html,body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
        $(".sectionErrorMessage").show();
      }, 200);
    },
    complete: function() {
      $("#" + id_prefix + "CommentButtonWaiting").hide();
    },
  });
}


function submitReasonDialog(formId, message, btn1Text, btn2Text) {
  openBandPopup({
    message: message,
    btn1Text: btn1Text,
    btn2Text: btn2Text,
    btn1Func: function() {
      safeSubmit(formId);
    },
    btn2Func: function() {
      fadeoutBandPopup();
    },
  });
}


// =============== comment(event) =============

$(document).on("change", "textarea[name='reason']", function() {
  setIsCommentChangedFlagFlag();
});


$('#replace_reason').click(function(event) {
  event.stopPropagation();
});
$('.commentButtons').click(function(event) {
  event.stopPropagation();
});
$('.commentSubmitButtons').click(function(event) {
  event.stopPropagation();
});
$('.bandPopupButton').click(function(event) {
  event.stopPropagation();
});
$(document).click(function() {
  if (!dialogFlag) {
    closeComment();
  }
});


// =============== Folder Tree(function) ===============

function makeFolderTreeView(url, formId) {
  var json = $.parseJSON($('#hiddenJson').val());
  var selectedPath = $('#hiddenSelectedPath').val();
  var html = '<ul class="treeviewContainer">';
  html += folderTreeViewParser(url, formId, json, selectedPath);
  html += '\n</ul>';
  $(html).appendTo("#treeview");
  $('.matchHeight').matchHeight();
}


function refreshFolderTreeView(url, formId) {
  var json = $.parseJSON($('#hiddenJson').val());
  var selectedPath = $('#hiddenSelectedPath').val();
  var html = '<ul class="treeviewContainer">';
  html += folderTreeViewParser(url, formId, json, selectedPath);
  html += '\n</ul>';
  $("#treeview").html(html);
  $('.matchHeight').matchHeight();
}

function folderTreeViewParser(url, formId, json, selectedPath, closeAtPath, loop) {
  if(typeof loop === "undefined") {
    loop = 0;
  }
  var isRootFolder = false;
  var html="";

  if (typeof json === "object") {
    if(json.name == "") isRootFolder = true;

    if(json.path == selectedPath) {
      html += '\n<li class="folderItem select">';
    }else{
      html += '\n<li class="folderItem">';
    }

    if(json.count > 0) {
      html += '\n<span class="treeviewCount">' + json.count + '</span>';
    }

    for(var i=0; i<loop; i++) {
      html += '\n<span class="treeviewExpander"></span>';
    }

    if(json.openFolder == true && isRootFolder == false) {
      html += '\n<a href="javascript:void(0);"><span class="fa fa-caret-down" onClick="submitFormCloseFolder(\'' + url + '\', \'' + formId + '\', \'' + json.path + '\')"></span></a><span class="treeviewIcon fa fa-folder-open-o"></span>';
    }else if(isRootFolder == true) {
      html += '\n<a href="javascript:void(0);"><span class="fa fa-caret-down"></span></a><span class="treeviewIcon fa fa-folder-open-o"></span>';
    }else{
      html += '\n<a href="javascript:void(0);"><span class="fa fa-caret-right" onClick="submitFormOpenFolder(\'' + url + '\', \'' + formId + '\', \'' + json.path + '\')"></span></a><span class="treeviewIcon fa fa-folder-o"></span>';
    }

    if(isRootFolder) json.name="[ROOT]";
    html += '\n<span class="treeviewTitle" onClick="submitFormSelectFolder(\'' + url + '\', \'' + formId + '\', \'' + json.path + '\')" >'+ json.name+ '</span>';
    html += '\n</li>';

    if(json.openFolder == false) return html;

    html += '\n<li>';

    loop++;
    $.each(json.folders,function(index, obj) {
      html += '\n<ul style="display:block;">';
      html += folderTreeViewParser(url, formId, obj, selectedPath, closeAtPath, loop);	// recursive call
      html += '\n</ul>';
    });
        html += '\n</li>';
    }
    return html;
};

var startIndex = 30;
scrollMore = true;

var sourceTreeStructure;
var isTreeviewTitle_TriggerOnLoad = false;
function submitFormSelectFolder(url, formId, path, closeAtPath) {
  scrollMore = true;
  startIndex = 30;
  var form = document.getElementById(formId);
  form.path.value = path;
  form.resourceRequestType.value = 'selectFolder';
  form.RequestType.value = 'onChange';
  form.requestOption.value = 'selectResource';
  $(form.selectedFiles).remove();
  getTriLayer("mask").startWaiting('fileListUnit');
  appendWindowId(form);
  $.post(url, $(form).serialize()).done(
    function(data, status, xhr) {
    	
      $("#hiddenJson").val($(data).find("#hiddenJson").val());
      $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

      var json = $.parseJSON($('#hiddenJson').val());
      var selectedPath = $("#hiddenSelectedPath").val();
      if(typeof closeAtPath === "undefined") {
        closeAtPath = selectedPath;
      }

      var tree = '<ul class="treeviewContainer">';
      tree += folderTreeViewParser(url, formId, json, selectedPath, closeAtPath);
      tree += '\n</ul>';

      $("#treeview").html(tree);
      $("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
      $("#fileViewTable thead").html($(data).find("#fileViewTable thead").html());
      updateHeadCheckbox('allCheckCheckout','selectedFile');
      var sortHeaders = {headers: {}};
      $('.tableSorter th.checkboxHead').each(function (index) {
          sortHeaders.headers[index] = {sorter: false};
      });
      $('.tableSorter th.sorter-false').each(function (index) {
      	var headerIndex = $(this).index();
          sortHeaders.headers[headerIndex] = {sorter: false};
      });
      
      $(".tableSorter").tablesorter(sortHeaders);
      $('#fileViewTable').trigger('update');
      if (!isTreeviewTitle_TriggerOnLoad || $(data).find("#message").html().trim() != "") {
        $("#message").html($(data).find("#message").html());
      }
      
      getTriLayer("mask").endWaiting('fileListUnit');
      
      isTreeviewTitle_TriggerOnLoad = false;
      $('.matchHeight').matchHeight();
    }).fail(function(XMLHttpRequest, status, errorThrown) {
  });
  if ( form.closeAtPath ) {
    form.closeAtPath.value = closeAtPath;
  }
  if ( form.url ) {
    form.url.value = url;
  }
  sourceTreeStructure = JSON.stringify($(form).serializeArray());
}



function initScrollTable(url){
	if(scrollMore) {
		var form = document.getElementById("FlowResourceListDisplay");
		form.startIndex.value = startIndex;
		form.endIndex.value = startIndex + 30;
		appendWindowId(form);
		var urlListFile = url + "/listfile";
		$.ajax({
	            type: "POST",
	            url: urlListFile,
	            data: $(form).serialize(),
	            success: function(data) {
	            	var htmlObject = document.createElement('div');
	            	htmlObject.innerHTML = data;
	            	var rowCount = $(htmlObject).children('table').children('tbody').children('tr').length;
	            	if(rowCount < 30) scrollMore = false;
	            	$("#fileViewTable tbody").append($(htmlObject).find("#fileViewTable tbody").html());
	            	var sortHeaders = {headers: {}};
	                $('.tableSorter th.checkboxHead').each(function (index) {
	                    sortHeaders.headers[index] = {sorter: false};
	                });
	                $('.tableSorter th.sorter-false').each(function (index) {
	                	var headerIndex = $(this).index();
	                    sortHeaders.headers[headerIndex] = {sorter: false};
	                });
	                
	                $(".tableSorter").tablesorter(sortHeaders);
	                 $('#fileViewTable').trigger('update');
	            	startIndex += rowCount;
	            },
	        });
	}
}

function postListFile(url,form){
	
	  $.post(url, $(form).serialize()).done(
		      function(data, status, xhr) {
		        $("#hiddenJson").val($(data).find("#hiddenJson").val());
		        $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

		        var json = $.parseJSON($('#hiddenJson').val());
		        var selectedPath = $("#hiddenSelectedPath").val();
		        if(typeof closeAtPath === "undefined") {
		        closeAtPath = selectedPath;
		        }

		        var tree = '<ul class="treeviewContainer">';
		        tree += folderTreeViewParser(url, formId, json, selectedPath, closeAtPath);
		        tree += '\n</ul>';

		        $("#treeview").html(tree);
		        $("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
		        $("#message").html($(data).find("#message").html());
		        $('.matchHeight').matchHeight();
		      }).fail(function(XMLHttpRequest, status, errorThrown) {
		    });
	
}



function reloadSourceTree() {
  if ( !sourceTreeStructure ) {
    var url = sourceTreeStructure.url.value;
    var closeAtPath = sourceTreeStructure.closeAtPath.value;
    $.post(url, $(form).serialize()).done(
      function(data, status, xhr) {
        $("#hiddenJson").val($(data).find("#hiddenJson").val());
        $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

        var json = $.parseJSON($('#hiddenJson').val());
        var selectedPath = $("#hiddenSelectedPath").val();
        if(typeof closeAtPath === "undefined") {
        closeAtPath = selectedPath;
        }

        var tree = '<ul class="treeviewContainer">';
        tree += folderTreeViewParser(url, formId, json, selectedPath, closeAtPath);
        tree += '\n</ul>';

        $("#treeview").html(tree);
        $("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
        $("#message").html($(data).find("#message").html());
        $('.matchHeight').matchHeight();
      }).fail(function(XMLHttpRequest, status, errorThrown) {
    });
  }
}

function submitFormOpenFolder(url, formId, path) {
  var form = document.getElementById(formId);
  form.path.value = path;
  form.resourceRequestType.value = 'open';
  form.RequestType.value = 'onChange';
  form.requestOption.value = 'selectResource';
  $(form.selectedFiles).remove();
  appendWindowId(form);
  getTriLayer("mask").startWaiting('fileListUnit');
  $.post(url, $(form).serialize()).done(
    function(data, status, xhr) {

      $("#hiddenJson").val($(data).find("#hiddenJson").val());
      $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

      var json = $.parseJSON($('#hiddenJson').val());
      var selectedPath = $("#hiddenSelectedPath").val();
      var tree = '<ul class="treeviewContainer">';
      tree += folderTreeViewParser(url, formId, json, selectedPath);
      tree += '\n</ul>';
      $("#treeview").html(tree);
      $("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
      getTriLayer("mask").endWaiting('fileListUnit');
      $("#message").html($(data).find("#message").html());
      $('.matchHeight').matchHeight();
    }).fail(function(XMLHttpRequest, status, errorThrown) {
  });
}

function submitFormOpenAllFolder(url, formId){
	var form = document.getElementById(formId);
	form.resourceRequestType.value = 'openAll';
	form.RequestType.value = 'onChange';
	form.requestOption.value = 'selectResource';
	$(form.selectedFiles).remove();
	updateHeadCheckbox('allCheckCheckout','selectedFile');
	appendWindowId(form);
	getTriLayer("mask").startWaiting('fileListUnit');
	$.post(url, $(form).serialize()).done(
		 function(data, status, xhr) {

			$("#hiddenJson").val($(data).find("#hiddenJson").val());
			$("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

			var json = $.parseJSON($('#hiddenJson').val());
			var selectedPath = $("#hiddenSelectedPath").val();
			var tree = '<ul class="treeviewContainer">';
			tree += folderTreeViewParser(url, formId, json, selectedPath);
			tree += '\n</ul>';
			$("#treeview").html(tree);
			$("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
			getTriLayer("mask").endWaiting('fileListUnit');
			$("#message").html($(data).find("#message").html());
			$('.matchHeight').matchHeight();
		 }).fail(function(XMLHttpRequest, status, errorThrown) {
	});
}

function updateHeadCheckbox(headCheckboxClass,subCheckboxClass){
	
	if($('.'+subCheckboxClass).length == 0) {
		$('.'+headCheckboxClass).remove();
	}else if ($('.'+subCheckboxClass+':checked').length == $('.'+subCheckboxClass).length){
		$('.'+headCheckboxClass).prop('checked', true);
	}else {
		$('.'+headCheckboxClass).prop('checked', false);
	}
	
}

function submitFormCloseFolder(url, formId, path) {
  var form = document.getElementById(formId);
  form.path.value = path;
  form.resourceRequestType.value = 'close';
  form.RequestType.value = 'onChange';
  form.requestOption.value = 'selectResource';
  $(form.selectedFiles).remove();
  updateHeadCheckbox('allCheckCheckout','selectedFile');
  appendWindowId(form);
  getTriLayer("mask").startWaiting('fileListUnit');
  $.post(url, $(form).serialize()).done(
    function(data, status, xhr) {
      $("#hiddenJson").val($(data).find("#hiddenJson").val());
      $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

      var json = $.parseJSON($('#hiddenJson').val());
      var selectedPath = $("#hiddenSelectedPath").val();
      var tree = '<ul class="treeviewContainer">';
      tree += folderTreeViewParser(url, formId, json, selectedPath, path);
      tree += '\n</ul>';

      $("#treeview").html(tree);
      $("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
      getTriLayer("mask").endWaiting('fileListUnit');
      $("#message").html($(data).find("#message").html());
      $('.matchHeight').matchHeight();
    }).fail(function(XMLHttpRequest, status, errorThrown) {
  });
}

function submitFormCloseAllFolder(url, formId, path){
	var form = document.getElementById(formId);
	form.path.value = path;
	form.resourceRequestType.value = 'closeAll';
	form.RequestType.value = 'onChange';
	form.requestOption.value = 'selectResource';
	$(form.selectedFiles).remove();
	updateHeadCheckbox('allCheckCheckout','selectedFile');
	appendWindowId(form);
	getTriLayer("mask").startWaiting('fileListUnit');
	$.post(url, $(form).serialize()).done(
		function(data, status, xhr) {
			$("#hiddenJson").val($(data).find("#hiddenJson").val());
			$("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

			var json = $.parseJSON($('#hiddenJson').val());
			var selectedPath = $("#hiddenSelectedPath").val();
			var tree = '<ul class="treeviewContainer">';
			tree += folderTreeViewParser(url, formId, json, selectedPath, path);
			tree += '\n</ul>';
			$("#treeview").html(tree);
			$("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
			getTriLayer("mask").endWaiting('fileListUnit');
			$("#message").html($(data).find("#message").html());
			$('.matchHeight').matchHeight();
		}).fail(function(XMLHttpRequest, status, errorThrown) {
	});
}

function reloadFolderTreeViewNumber() {
  $("#treeview .folderItem.select .treeviewTitle").click();
}

function submitFormSelectedSingleFile(url, formId,thisObj){
	var form = document.getElementById(formId);
	form.path.value = $(thisObj).val();
	form.resourceRequestType.value = 'selectSingleResource';
	form.RequestType.value = 'onChange';
	form.requestOption.value = 'selectResource';
	form.isSelected.value = $(thisObj).prop("checked");
	form.selectedResource.value =  $(thisObj).val();
	appendWindowId(form);
	getTriLayer("mask").startWaiting('fileListUnit');
	$.post(url, $(form).serialize()).done(
			function(data, status, xhr) {
                if ($(thisObj).data('lock')) {
                    $(thisObj).remove();
                }
				getTriLayer("mask").endWaiting('fileListUnit');
				updateHeadCheckbox('allCheckCheckout','selectedFile');
			}).fail(function(XMLHttpRequest, status, errorThrown) {
				getTriLayer("mask").endWaiting('fileListUnit');
		});
	
}

function submitFormSelectedAllFile(url, formId,thisObj){
	var form = document.getElementById(formId);
	form.path.value = $(thisObj).val();
	form.resourceRequestType.value = 'selectAllResource';
	form.RequestType.value = 'onChange';
	form.requestOption.value = 'selectResource';
	form.isSelected.value = $(thisObj).prop("checked");
	form.selectedPath.value =  $(thisObj).val();
	appendWindowId(form);
	getTriLayer("mask").startWaiting('fileListUnit');
	$.post(url, $(form).serialize()).done(
			function(data, status, xhr) {
				getTriLayer("mask").endWaiting('fileListUnit');
				$("#fileViewTable tbody").html($(data).find("#fileViewTable tbody").html());
				startIndex = 30;
				updateHeadCheckbox('allCheckCheckout','selectedFile');
			}).fail(function(XMLHttpRequest, status, errorThrown) {
				getTriLayer("mask").endWaiting('fileListUnit');
		});
	
}



$(document).on("change", ".selectedFile", function(){
	 submitFormSelectedSingleFile(singleResourceUrl, formSingleId,this);
	 return false;
});

$(document).on("change", ".allCheckCheckout", function(){
	submitFormSelectedAllFile(url, formAllId,this);
	 return false;
});



function submitFormRefreshFolder(url, formId){
  var form = document.getElementById(formId);
  form.RequestType.value = 'onChange';
  form.requestOption.value = 'refresh';
  appendWindowId(form);
  $.post(url, $(form).serialize()).done(
    function(data, status, xhr) {
      $("#hiddenJson").val($(data).find("#hiddenJson").val());
      $("#hiddenSelectedPath").val($(data).find("#hiddenSelectedPath").val());

      var json = $.parseJSON($('#hiddenJson').val());
      var selectedPath = $("#hiddenSelectedPath").val();
      if(typeof closeAtPath === "undefined"){
        closeAtPath = selectedPath;
      }

      var tree = '<ul class="treeviewContainer">';
      tree += folderTreeViewParser(url, formId, json, selectedPath, closeAtPath);
      tree += '\n</ul>';

      $("#treeview").html(tree);
      $("#fileViewTable").html($(data).find("#fileViewTable").html());
      $("#message").html($(data).find("#message").html());
      $('.matchHeight').matchHeight();
    }).fail(function(XMLHttpRequest, status, errorThrown) {
  });
}


// =============== Folder Tree(event) =============

// =============== globalnav(function) ===============

function openGlobalMenu(formId, replaceId) {
  var form = document.getElementById(formId);
  var timeOut = null;
  var url = form.action;
  var keyword = $('#' + replaceId + ' .globalNavDropdownSearchField').val();

  if(typeof form.keyword !== "undefined") {
    form.keyword.value = keyword;
  }
  $('#'+replaceId+' .globalNavSearchNoResultMessage').hide();
  $('#'+replaceId+' .globalNavSearchResultContainer').hide();
  $('#'+replaceId+' .globalNavDropdownPagination').hide();
  $('#'+replaceId+' .globalNavSearchResultLoading').show();

  appendWindowId(form);
  // send
    $.post(url, $(form).serialize()).done(
      function(data, status, xhr) {
        //close all other open tab
        $('.globalNavDropdownContainer').hide();
        $('#'+replaceId).replaceWith($(data).find('#'+replaceId).parent().html());
        $('#' + replaceId + ' .globalNavDropdownSearchField').val(keyword);
        $('#'+replaceId).find(".globalNavDropdownSearchField").focus();
        var form = document.getElementById(formId);
        if(form && typeof form.keyword !== "undefined") {
            form.keyword.value = keyword;
        }
        calculateJointWidths();
      }
    ).fail(function(XMLHttpRequest, status, errorThrown) {
      $('#'+replaceId+' .globalNavSearchNoResultMessage').show();
      $('#'+replaceId+' .globalNavSearchResultContainer').hide();
      $('#'+replaceId+' .globalNavDropdownPagination').hide();
      $('#'+replaceId+' .globalNavSearchResultLoading').hide();
    });
}


function pagingGlobalMenu(formId, replaceId, page) {
  var form = document.getElementById(formId);
  form.selectedPageNo.value=page;
  openGlobalMenu(formId, replaceId);
}


function submitSearchFilter(filterId) {
  var formObj = document.getElementById("SubmitSearchFilter");
    formObj.dataId.value = filterId;

    appendWindowId(formObj);
  formObj.submit();
}

function editSearchFilter(url, filterId, filterNm) {
  filterNm = $.trim(filterNm);

  if(filterNm == "") {
    alert( messages.GlobalBarDropDown_alert_text );
    return;
  }

  var html="";
  html += '<input type="hidden" name="filterId" value="' + filterId + '" />';
  html += '<input type="hidden" name="filterNm" value="' + filterNm + '" />';
  html += '<input type="hidden" name="RequestType" value="submitChanges" />';

  var form = $('<form>', {
        "id": "form",
        "method": "POST",
        "action": url,
        "html": html,
    });
  appendWindowId(form);
  var timeOut = null;
  var _data = $(form).serialize();
  $.ajax({
      type: "POST",
      url: url,
      data: _data,
      global: false, //disable xhr check for this ajax
      success: function(data) {
    	  console.log(data);

          openGlobalMenu('SearchFilterService', 'GlobalMenu_SearchFilter');
          $("#infoDialog").html($(data).find("#infoDialog").html());
          getTriLayer("flashMessage").show();
      },
      error: function(XMLHttpRequest, status, errorThrown) {
        console.log("fail");
      },
    });
  
}


function removeSearchFilter(url, filterId, confirmMsg) {
  var result = confirm(confirmMsg);
  if (!result) {
    return;
  }

  var html="";
  html += '<input type="hidden" name="filterId" value="' + filterId + '" />';
  html += '<input type="hidden" name="RequestType" value="submitChanges" />';

  var form = $('<form>', {
    "id": "form",
    "method": "POST",
    "action": url,
    "html": html,
  });
  appendWindowId(form)
  var _data = $(form).serialize();
  $.ajax({
      type: "POST",
      url: url,
      data: _data,
      global: false, //disable xhr check for this ajax
      success: function(data) {
    	  console.log(data);

          openGlobalMenu('SearchFilterService', 'GlobalMenu_SearchFilter');
          $("#infoDialog").html($(data).find("#infoDialog").html());
          getTriLayer("flashMessage").show();
      },
      error: function(XMLHttpRequest, status, errorThrown) {
        console.log("fail");
      },
    });
  
}


function IncrementalSearchRequest() {}

IncrementalSearchRequest.prototype = {
  url_: null,
  data_: null,
  callback_: {
    done: null,
    fail: null,
    always: null,
  },
  setUrl: function(url) {
    this.url_ = url;
    return this;
  },
  getUrl: function() {
    return this.url_;
  },
  setData: function(data) {
    this.data_ = data;
    return this;
  },
  getData: function() {
    return this.data_;
  },
  setCallbackIfDone: function(done) {
    this.callback_.done = done;
    return this;
  },
  setCallbackIfFail: function(fail) {
    this.callback_.fail = fail;
    return this;
  },
  setCallbackAlways: function(always) {
    this.callback_.always = always;
    return this;
  },
  getCallback: function() {
    return this.callback_;
  }
};


var IncrementalSearch = {
  /** @const */
  INTERVAL_TIME_: 500,
  isProcessing_: false,
  hasNextSearch_: false,
  request_: null,

  search: function(request) {
    IncrementalSearch.request_ = request;
    IncrementalSearch.hasNextSearch_ = true;

    if( !IncrementalSearch.isProcessing_ ) {
      IncrementalSearch.isProcessing_ = true;
      setTimeout(IncrementalSearch.ajax_, IncrementalSearch.INTERVAL_TIME_);
    }
  },
  ajax_: function() {
    var url = IncrementalSearch.request_.getUrl();
    var data = IncrementalSearch.request_.getData();
    var callback = IncrementalSearch.request_.getCallback();

    IncrementalSearch.hasNextSearch_ = false;
    var startTime = new Date();

    $.ajax({type: "POST", url: url, data: data})
    .done( function(data, status, xhr) {
      if(typeof callback.done == "function") {
        callback.done(data, status, xhr);
      }
    })
    .fail( function(data, status, xhr) {
      if(typeof callback.fail == "function") {
        callback.fail(data, status, xhr);
      }
    })
    .always( function(data, status, xhr) {
      if(typeof callback.always == "function") {
        callback.always(data, status, xhr);
      }

      var endTime = new Date();
      var waitTime = IncrementalSearch.INTERVAL_TIME_ - (endTime - startTime);
      waitTime = ( waitTime < 0 )? 0 : waitTime;

      if( IncrementalSearch.hasNextSearch_ ) {
        setTimeout( IncrementalSearch.ajax_, waitTime );
      }else{
        IncrementalSearch.isProcessing_ = false;
      }
    });
  },
};


function searchBoxGlobalMenu(url, replaceId, clear) {
  if(clear == 1) {
    var keyword = "";
    $('#' + replaceId + ' .globalNavDropdownSearchField').val('');
  }else{
    var keyword = $('#' + replaceId + ' .globalNavDropdownSearchField').val();
  }

  var html = "";
  html += '<input type="hidden" name="keyword" value="' + keyword + '" />';
  html += '<input type="hidden" name="selectedPageNo" value="1" />';
  html += '<input type="hidden" name="RequestType" value="onChange" />';

  var form = $('<form>', {
    "id": "form",
    "method": "POST",
    "action": url,
    "html": html,
  });

  appendWindowId(form);

  var request = new IncrementalSearchRequest()
  .setUrl( url )
  .setData( $(form).serialize() )
  .setCallbackIfDone(function(data, status, xhr) {
    $('#'+replaceId+' .globalNavSearchResult').html($(data).find('#'+replaceId+' .globalNavSearchResult').html());
    $('#'+replaceId).find(".globalNavDropdownSearchField").focus();
  });
  IncrementalSearch.search( request );
}


function searchBox(searchBoxId, formId) {
  var form = document.getElementById(formId);
  appendWindowId(form);

  var request = new IncrementalSearchRequest()
  .setUrl( form.action )
  .setData( $(form).serialize() )
  ;

  if(searchBoxId=="searchRecentlyViewLot") {
    request.setCallbackIfDone( function( data, status, xhr ) {
      $("#globalNavSearchResult_lot").html($(data).find("#globalNavSearchResult_lot").html());
    });
  }else if(searchBoxId=="searchRecentlyViewWiki") {
    request.setCallbackIfDone( function( data, status, xhr ) {
      $("#globalNavSearchResult_wiki").html($(data).find("#globalNavSearchResult_wiki").html());
    });
  }else{
    request.setCallbackIfDone( function( data, status, xhr ) {
      $("#globalNavSearchResult_request").html($(data).find("#globalNavSearchResult_request").html());
    });
  }
  IncrementalSearch.search( request );
}


function clearTextThenFocus(searchBoxId, formId) {
  $('#'+searchBoxId+'.globalNavDropdownSearchField').val("");
  $('#'+searchBoxId+'.globalNavDropdownSearchField').focus();
  searchBox(searchBoxId, formId);
}


function submitLotDetailsService(lotId) {
  var form = document.getElementById('FlowLotDetailsService');
  form.lotId.value = lotId;
  submit('FlowLotDetailsService');
}


function submitWikiDetailsService(lotId, wikiId) {
  var form = document.getElementById('RecentlyViewedWikiService');
  form.lotId.value = lotId;
  form.wikiId.value = wikiId;
  submit('RecentlyViewedWikiService');
}


function submitCheckoutRequestDetailsService(areqId) {
  var form = document.getElementById('FlowCheckoutRequestDetailsService');
  form.areqId.value = areqId;
  submit('FlowCheckoutRequestDetailsService');
}


function submitCheckinRequestDetailsService(areqId) {
  var form = document.getElementById('FlowCheckinRequestDetailsService');
  form.areqId.value = areqId;
  submit('FlowCheckinRequestDetailsService');
}


function updateCountOfUnread() {
  if( submitFlag ) {
    return;
  }

  var form = document.getElementById("FlowProgressNotificationService_countOfUnread");
  if(form) {
    $('#WindowsId').remove();
    appendWindowId(form);
    var _url = form.action;
    var _data = $(form).serialize();

    setSubmitFlag();

    $.ajax({
      type: "POST",
      url: _url,
      data: _data,
      global: false, //disable xhr check for this ajax
      success: function(data) {
        if (isLoginHtml(data)) {
          window.location.assign(getContextPath() + "logout");
        }
        if(data != "0"){
          $("#countOfUnread").html(data);
        $("#countOfUnread").show();
        }
        if(data == "" || data == "0") {
        $("#countOfUnread").html(data);
        $("#countOfUnread").hide();
        }
        clearSubmitFlag();
      },
      error: function(XMLHttpRequest, status, errorThrown) {
        console.log("fail");
        clearSubmitFlag();
      },
    });
  }
}


function isLoginHtml(html) {
  var hasLoginForm = $(html).find("#FlowLogInService").length > 0;
  var hasUserInput = $(html).find("#userid").length > 0;
  var hasPasswordInput = $(html).find("#password").length > 0;

  return hasLoginForm && hasUserInput && hasPasswordInput;
}


var isProgressNotificationWindowOpen = 0;

function updateProgressNotificationWindow() {

  var form = document.getElementById("FlowProgressNotificationService");
  if( isProgressNotificationWindowOpen == 1 ) {
    form.requestOption.value="refresh";
  }else{
    form.requestOption.value="views";
    isProgressNotificationWindowOpen = 1;
    $(".globalNavIcon.count .popupWindow").slideDown(100);
  }
  appendWindowId(form);
  var url = form.action;

  if(submitFlag){
    return;
  }

  setSubmitFlag();

  $.post(url, $(form).serialize()).done(
    function(data, status, xhr) {
      var scrollVal = $("#progressNotificationWindow_popupWindowBody").scrollTop();
      $("#FlowProgressNotificationService").html($(data).find("#FlowProgressNotificationService").html());
      $("#progressNotificationWindow").html($(data).find("#progressNotificationWindow").html());
      $("#progressNotificationWindow_popupWindowBody").scrollTop(scrollVal);
      clearSubmitFlag();
      updateCountOfUnread();
    }
  ).fail(function(XMLHttpRequest, status, errorThrown) {
    console.log("fail");
    clearSubmitFlag();
  });
}


function submitProgressNotificationLink(url, lotId, serviceId) {
  var windowId = $('#windowId').val();
  url = url + "?WindowsId=" + windowId + "&submitMode=list&lotId=" + lotId + "&serviceId=" + serviceId;
  window.location.assign(url);
}


// =============== globalnav(event) =============

var progressNotification = null;
var timer_progressNotification = null;
var reloadInterval = null;
var timer_reloadInterval = null;

$(document).ready(function() {
  progressNotification = $("#progressNotification").html();
  reloadInterval = $("#reloadInterval").html();
  updateCountOfUnread();
  if (progressNotification != null && progressNotification >= 1000) {
    timer_progressNotification = setInterval(updateCountOfUnread, progressNotification);
  }
});


$(document).on("click", ".globalNavSearchResult .searchFilterResultItem .editButton", function() {
  if($(".resultItemEditForm:visible").html() != 'undefined'){
	  $(".resultItemEditForm:visible").closest(".searchFilterResultItem").find(".cancelButton").trigger( "click" );  
  }
  $(this).closest(".searchFilterResultItem").find(".resultItemTitle .resultItemTitleText").hide();
  $(this).closest(".searchFilterResultItem").find(".defaultActions").hide();
  $(this).closest(".searchFilterResultItem").find(".resultItemEditForm").show();
  $(this).closest(".searchFilterResultItem").find(".cancelActions").show();
  return false;
});


$(document).on("click", ".globalNavSearchResult .searchFilterResultItem .cancelButton", function() {
  $(this).closest(".searchFilterResultItem").find(".resultItemTitle .resultItemTitleText").show();
  $(this).closest(".searchFilterResultItem").find(".defaultActions").show();
  $(this).closest(".searchFilterResultItem").find(".resultItemEditForm").hide();
  $(this).closest(".searchFilterResultItem").find(".deleteActions").hide();
  $(this).closest(".searchFilterResultItem").find(".cancelActions").hide();
  return false;
});


$(".globalNavIcon.count").click(function(e) {
  if ( isProgressNotificationWindowOpen == 1 ) {
    clearInterval(timer_reloadInterval);
    isProgressNotificationWindowOpen = 0;
    $(".globalNavIcon.count .popupWindow").slideUp(100);
  } else {
    updateProgressNotificationWindow();
    if (reloadInterval != null && reloadInterval > 1000) {
      timer_reloadInterval = setInterval(updateProgressNotificationWindow, reloadInterval);
    }
  }
  e.stopPropagation();
});


$(document).click(function() {
  if ( isProgressNotificationWindowOpen == 1 ) {
    clearInterval(timer_reloadInterval);
    isProgressNotificationWindowOpen = 0;
    $(".globalNavIcon.count .popupWindow").slideUp(100);
  }
});


// =============== lotMenuSearchLot(function) ===============


// =============== lotMenuSearchLot(event) =============

function clearSearchLotInput(){
    $("#lotMenuSearchLot").val("");
    $("#lotMenuSearchLot").keyup();
    $("#lotMenuSearchLot").focus();
}


$(function() {
  $("#lotMenuSearchLot").keydown(function( e ) {
    var code = e.which;
    console.log(code);
    if(code == 13) {
      var path = window.location.pathname;
      if(path == getContextPath() + 'lot/search') {
        $("input[name=keyword]").val($(this).val());
        ajax_refresh();
        e.preventDefault();
      }else{
        $("#lotMenuSearchForm").attr("action", getContextPath() + "lot/search");
        var formObj = $("#lotMenuSearchForm");
        appendWindowId(formObj);
        formObj.submit();
      }
    }
  });
});

//prevent enter key
$(document).on("keypress", "input.searchFilterEditField", function(event) {
    return event.which !== 13;
});


// =============== Search Filter(function) ===============

var searchFilterQuery = {};
var condNames = {};
if(typeof messages != 'undefined') {
    condNames = {
      'requestJob': messages.DeploymentList_requestJob_text,
      'condSpan': 'condSpan',
      'condTarget': messages.ChangePropertyList_condTarget_text,
      'condRelEnv' : messages.ReleaseRequestList_releaseEnvironment_text,
      'condGrouping': 'condGrouping',
      'condType': messages.ReportList_condType_text,
      'condStatus': messages.SearchThisLot_status_text,
      'condCategory': messages.SearchThisLot_category_text,
      'condMilestone': messages.SearchThisLot_milestone_text,
      'searchKeyword': messages.SearchThisLot_keyword_text,
      'draft_condRelEnv' : messages.ReleaseRequestList_releaseEnvironment_text,
      'draft_condTarget': messages.ChangePropertyList_condTarget_text,
      'draft_condStatus': messages.SearchThisLot_status_text,
      'draft_condCategory': messages.SearchThisLot_category_text,
      'draft_condMilestone': messages.SearchThisLot_milestone_text,
      'draft_searchKeyword': messages.SearchThisLot_keyword_text,
      'pending_condRelEnv' : messages.ReleaseRequestList_releaseEnvironment_text,
      'pending_condStatus' : messages.SearchThisLot_status_text,
      'pending_condTarget': messages.ChangePropertyList_condTarget_text,
      'pending_condCategory': messages.SearchThisLot_category_text,
      'pending_condMilestone': messages.SearchThisLot_milestone_text,
      'pending_searchKeyword': messages.SearchThisLot_keyword_text,
      'approved_condRelEnv': messages.ReleaseRequestList_releaseEnvironment_text,
      'approved_condTarget': messages.ChangePropertyList_condTarget_text,
      'approved_condStatus': messages.SearchThisLot_status_text,
      'approved_condCategory': messages.SearchThisLot_category_text,
      'approved_condMilestone': messages.SearchThisLot_milestone_text,
      'approved_searchKeyword': messages.SearchThisLot_keyword_text,
      'envId': messages.ReleaseRequestList_releaseEnvironment_text,
      'stsId': messages.DeploymentList_condDeploymentStatus_text,
      'ctgId': messages.SearchThisLot_category_text,
      'mstoneId': messages.SearchThisLot_milestone_text,
      'keyword': messages.SearchThisLot_keyword_text,
      'selectedPageNo': messages.selectedPageNo_text,
      'condDepStatus': messages.DeploymentList_condDeploymentStatus_text,
      'approved': messages.selectedTab_text,
      'pending': messages.selectedTab_text,
      'draft': messages.selectedTab_text,
    };
}


var serviceIdName = 'serviceId';
function updateSearchFilterQuery(formId) {
  searchFilterQuery = {};
  $.each(condNames, function(key, val) {
    addSearchFilterQuery(formId, key);
  });

  updateSaveDialogQueryInfo();

  addSearchFilterQuery(formId, serviceIdName);
  searchFilterQuery.serialize = $.param(searchFilterQuery);
}


function addSearchFilterQuery(formId, name) {
  var data_value = $('#'+formId).find('input[name='+name+']').val();
  if(!data_value) return;
  if(name.indexOf("eyword") > -1) {
    searchFilterQuery[name] = data_value;
  } else if(name === "approved" || name === "pending" || name === "draft") {
    if(data_value=== "false") {
        searchFilterQuery[name] = $("#listTab").text().trim();
    } else {
        searchFilterQuery[name] = $("#"+ name + "Tab").text().trim();
    }
  } else {
    var display_value = $('[data-key="'+name+'"][data-value="'+ data_value +'"]').text().trim();
    searchFilterQuery[name] = display_value;
  }

}


function updateSearchConditionView(formId) {
  $.each(condNames, function(key, val) {
    var formVal = $('#'+formId).find('input[name='+key+']').val();
    if(typeof formVal === "undefined") return true;
    $('[data-key="'+key+'"].select').removeClass("select");
    $('[data-key="'+key+'"][data-value="'+formVal+'"]').addClass("select");
  });
}


function updateSaveDialogQueryInfo() {
  var html = "";
  $.each(condNames, function(key, val) {
    if(typeof searchFilterQuery[key] === "undefined" || searchFilterQuery[key] == "") return true;
    html += '<tr>\n';
    html += '<td><i class="fa fa-check"></i>'+val+'</td>\n';
    html += '<td>='+searchFilterQuery[key]+'</td>\n';
    html += '</tr>\n';
  });
  $('#searchQueryInfoTableBody').html(html);
}


function saveSearchFilter(url) {
  $(".modal-error.searchFilterNameError").fadeOut(200);

  if($("#searchFilterName").val() == '') {
    $(".modal-error.searchFilterNameError").text(searchFilterNameErrorText);
    $(".modal-error.searchFilterNameError").fadeIn(200);
    return;
  }
  var windowId =  $('#windowId').val();

  var data = {
    'filterNm': $("#searchFilterName").val(),
    'RequestType': 'submitChanges',
    'WindowsId':	windowId,
  }
  var timeOut = null;
  timeOut = setTimeout(function() {
    $.post(url, $.param(data)).done(
      function(data, status, xhr) {
        console.log(data);

        if($(data).find(".dl-systemErrorHeader").length > 0) {
          $(".modal-error.searchFilterNameError").html($(data).find(".dl-systemErrorHeader dd").html());
        $(".modal-error.searchFilterNameError").fadeIn(200);
        return;
        }

        $('#saveFilterDialog').modal('hide');
        $("#searchFilterName").val('');

        $("#infoDialog").html($(data).find("#infoDialog").html());
        getTriLayer("flashMessage").show();
      }
    ).fail(function(XMLHttpRequest, status, errorThrown) {
      console.log("fail");
    });
  }, 500);
}


// =============== Search Filter(event) =============


$(document).ready(function(){
  searchFilterNameErrorText = $(".modal-error.searchFilterNameError").text();
});


$(".saveFilterButton").click(function(e) {
  e.preventDefault();
});


// =============== popup window(function) ===============


// =============== popup window(event) =============

$(".windowOpenBtn .faBtnChange,.popupWindowClose").click(function() {
  $(this).nextAll(".popupWindow").slideToggle(100);
  if ($(this).hasClass("fa-plus-square")) {
    $(this).removeClass("fa-plus-square").addClass("fa-minus-square");
    var timeOut = setTimeout(function() {
      setDialogFlag();
    }, 100);
  } else {
    $(this).removeClass("fa-minus-square").addClass("fa-plus-square");
    var timeOut = setTimeout(function() {
      clearDialogFlag();
    }, 100);
  }
});


$(document).click(function() {
  if (dialogFlag) {
    $(".windowOpenBtn .faBtnChange.fa-minus-square").nextAll(".popupWindow").slideUp(100);
    $(".windowOpenBtn .faBtnChange.fa-minus-square").removeClass("fa-minus-square").addClass("fa-plus-square");
    clearDialogFlag();
  }
});


$('.windowOpenBtn > .popupWindow').click(function(event) {
  event.stopPropagation();
});


// =============== ajax(function) ===============

/*
 * Flag for is XHR using.
 */
var xhrFlag = false;
function clearXhrFlag() {
  xhrFlag = false;
}
function setXhrFlag() {
  xhrFlag = true;
}


/**
 * @param params = {formId, timeout, beforeFunc(), successFunc(data),
 * errorFunc(XMLHttpRequest, status, errorThrown), competeFunc(), afterFunc()}
 */
function ajaxXHRSafeSend(params) {
  if(params.beforeFunc != null) params.beforeFunc();
  var form = null;
  if(params.formId != null) form = $("#" + params.formId);
  var _url = form.attr("action");
  var _data = form.serialize();
  var _timeout = 1000;
  if(params.timeout != null) _timeout = params.timeout;

  $.ajax({
    type: "POST",
    url: _url,
    data: _data,
    timeout: _timeout,
    success: function(data) {
      if(params.successFunc != null) params.successFunc(data);
    },
    error: function(XMLHttpRequest, status, errorThrown) {
      if(params.errorFunc != null) params.errorFunc(XMLHttpRequest, status, errorThrown);
    },
    complete: function() {
      if(params.completeFunc != null) params.completeFunc();
    },
  });
  if(params.afterFunc != null) params.afterFunc();
}


// =============== ajax(event) =============


$(document).ajaxError(
  function(e, x, settings, exception) {
    var message;
    var statusErrorMap = {
      '400': "Server understood the request, but request content was invalid.",
      '401': "Unauthorized access.",
      '403': "Forbidden resource can't be accessed.",
      '404': "Unknow Address",
      '500': "Internal server error.",
      '503': "Service unavailable."
    };
    if (x.status) {
      message = statusErrorMap[x.status];
      if(!message) {
        message = "Unknown Error \n.";
      }
    }else if(exception == 'parsererror') {
      message = "Error.\nParsing JSON Request failed.";
    }else if(exception == 'timeout') {
      message = "Request Time out.";
    }else{
      message = exception;
    }
    if('abort' != exception && message != "") {
      var html = '<div class="errorMessage"><dl class="dl-horizontal dl-errormsg"><dt class="zero"></dt><dd>';
      html += message;
      html += '</dd></dl></div>';
      console.log(html);
      $("#message").html(html);
      timeOut = setTimeout(function() {
        $("html,body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
        $(".sectionErrorMessage").show();
      }, 200);
    }
  }
);


$( document ).ajaxSend(function(event, request, settings) {
  if (xhrFlag) {
    request.abort();
  }
  setXhrFlag();
});


$( document ).ajaxComplete(function(event, xhr, settings) {
  clearXhrFlag();
  if( xhr.getResponseHeader('content-type') && xhr.getResponseHeader('content-type').indexOf('text/html') >= 0 ) {
    var data = xhr.responseText;
      var message = $(data).find("#message").html();
      if ( $(message).find(".dl-systemErrorHeader").length > 0 ) {
        $('body').html(data);
        $('body').attr('class', 'loginBody');
      }
      var sessionError = $(data).find("#isSessionError").val();
      if ( sessionError ){
    	  $('body').html(data);
    	  $('body').addClass( "loginBody" );
      }
  }

});


// =============== CheckInFileDifferences(function) ===============

function fromJsonToForm(formObj, formData) {
  data = $.parseJSON(formData);
  $.each(data, function(i, item) {
    $('<input>').attr({
      type: 'hidden',
      id: item.name,
      name: item.name,
      value: item.value
    }).appendTo($(formObj));
  });
}


// =============== CheckInFileDifferences(event) =============


// =============== error message(function) ===============


// =============== error message(event) =============

$(document).ready(function() {
  var messageVar = document.getElementById("message");
  if ($(messageVar) && !$(messageVar).find(".dl-systemErrorHeader").length > 0) {
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    var observer = new MutationObserver(function(mutations, observer) {
      // fired when a mutation occurs
      if ($(".errorMessage")[0] != null) {
        timeOut = setTimeout(function() {
        $("html, body").animate({scrollTop:$('#message').offset().top-$("nav#globalNav").height()});
        $(".sectionErrorMessage").show();
        }, 200);
      }
      // ...
    });
    // define what element should be observed by the observer
    // and what types of mutations trigger the callback
    if ( document.getElementById("message") ) {
      observer.observe(document.getElementById("message"), {
        subtree: true,
        attributes: true,
        childList: true
      });
    }
  }
});


$(document).on("click", ".showMoreErrorMessages", function() {
  $('.dl-errormsg dd.more').each( function(index) {
      $(this).css('display', 'block');
  });
  $('.showMoreErrorMessages').hide();
  $('.hideMoreErrorMessages').show();
  if(typeof changeWindowHeight === 'function'){
    changeWindowHeight();
  }
});


$(document).on("click", ".hideMoreErrorMessages", function() {
  $('.dl-errormsg dd.more').each( function(index) {
    $(this).css('display', 'none');
  });
  $('.hideMoreErrorMessages').hide();
  $('.showMoreErrorMessages').show();
  if(typeof changeWindowHeight === 'function'){
    changeWindowHeight();
  }
});


// =============== message(function) ===============

function messageReplace(message) {
  for(var i = 0; i < arguments.length-1; i++) {
    message = message.replace("{"+i+"}",arguments[i+1]);
  }
  return message;
}


// =============== message(event) =============

// =============== secret hacking(function) ===============

/**
 * this is demo.
 */
function fullWidth() {
  $("article.contents").addClass("fullWidth");
}


// =============== secret hacking(event) =============

// =============== validation(function) ===============

var isFirstOnSubmitValidation = true;


function validateOnSubmit() {
  var valid = true;
  isFirstOnSubmitValidation = false;
  $("input.validation,select.validation,textarea.validation").each( function(index, element) {
    if ( !validate(element, true) ) valid = false;
  });

  if ( !valid ) {
    $("html,body").animate({scrollTop:$("input.validation.invalidStat,select.validation.invalidStat,textarea.validation.invalidStat").eq(0).offset().top-$("nav#globalNav").height()-20});
    $("input.validation.invalidStat,select.validation.invalidStat,textarea.validation.invalidStat").eq(0).focus();
  }

  return valid;
}


function validateOnchange() {
  $("input.validation,select.validation,textarea.validation").each( function(index, element) {
    validate(element, !isFirstOnSubmitValidation);
  });
}


var colorValid = "#FFFFFF";
var colorInvalid = "#FFF0F0";

/**
 * validate
 * @param element that want to validate
 * @param isHighlight set true if use Highlight when invalid
 * @returns {Boolean} is valid
 */
function validate(element, isHighlight) {
  var valid = true;

  var formError = $(element).parent().children("div.formError");
  if ( $(element).hasClass("validationMessageId") ) {
    var messageId = $(element).attr("data-validationMessageId");
    formError = $("#"+messageId);
  }
  if ( $(element).hasClass("validationSwitch") ) {
    var switchId = $(element).attr("data-validationSwitch");
    if( !$("#"+switchId).prop("checked") ) {
      removeInvalidState(formError, element);
      return true;
    }
  }
  if ( $(element).hasClass("validationCustomPattern") ) {
    var pattern = $(element).attr("data-validationCustomPattern");
    if ( pattern == "checkbox_atLeastOne" ) {
      valid = false;
      var elements = $("input[type='checkbox'][name='" + $(element).attr("name") + "']");
      elements.each( function(index, element) {
        valid = valid || $(element).prop("checked") || $(element).prop("indeterminate");	//evaluate
      });
      if( !valid ) {
    	  addInvalidState(formError, elements[0]);
      } else {
    	  removeInvalidState(formError, elements[0]);
      }
    }
    if ( pattern == "radio_selectOne" ) {
      valid = false;
      var elements = $("input[type='radio'][name='" + $(element).attr("name") + "']");
      elements.each( function(index, element) {
        valid = valid || $(element).prop("checked");	//evaluate
      });
      if( !valid ) {
        elements.each( function(index, element) {
        addInvalidState(formError, element);
        });
      } else {
        elements.each( function(index, element) {
        removeInvalidState(formError, element);
        });
      }
    }
    if ( pattern == "url" ) {
      var elements = $("input[name='" + $(element).attr("name") + "']");
      var regex = new RegExp("^(http[s]?:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?");
      var invalidElement;
      elements.each( function(index, element) {
        if ( $(element).val().trim() ) {
        if ( $(element).val().match(regex) ) {	// evaluate
          clearInvalidState(element);
        } else {
          showInvalidState(element);
          invalidElement = element;
          valid = false;
        }
        }
      });
      if( !valid ) {
        showFormError(formError, invalidElement);
      } else {
        hideFormError(formError);
      }
    }

    if ( pattern == "date-hour" ) {
        var elements = $("input[name='" + $(element).attr("name") + "']");
        var regex = /(2[0-3]|[0-1][0-9])/;
        var invalidElement;
        elements.each( function(index, element) {
          if ( $(element).val().trim() ) {
          if ( $(element).val().match(regex) ) {	// evaluate
            clearInvalidState(element);
          } else {
            showInvalidState(element);
            invalidElement = element;
            valid = false;
          }
          }else {
        	  showInvalidState(element);
              invalidElement = element;
              valid = false;
          }
        });

        if( !valid ) {
          showFormError(formError, invalidElement);
        } else {
          hideFormError(formError);
        }
      }
    if ( pattern == "date-hour-minute" ) {
        var elements = $("input[name='" + $(element).attr("name") + "']");
        var regex = /[0-5][0-9]/;
        var invalidElement;
        elements.each( function(index, element) {
          if ( $(element).val().trim() ) {
          if ( $(element).val().match(regex) ) {	// evaluate
            clearInvalidState(element);
          } else {
            showInvalidState(element);
            invalidElement = element;
            valid = false;
          }
          }else {
        	  showInvalidState(element);
              invalidElement = element;
              valid = false;
          }
        });
        if( !valid ) {
          showFormError(formError, invalidElement);
        } else {
          hideFormError(formError);
        }
      }

  } else {
    $(element).val( $(element).val().trim() );
    if ( $(element).val() == "" ) {	// evaluate
      addInvalidState(formError, element);
      valid = false;
    } else {
      removeInvalidState(formError, element);
    }
  }
  return valid;
}

function showFormError(formError, element) {
  formError.css("position", "absolute");
  formError.css("top", 0);
  formError.css("left", 0);

  formError.show();

  var offset = formError.offset();
  var formErrorLeft = 0;
  var formErrorTop = 0;
  formError.hide();

  if( $(element).hasClass("validationMessagePosition") ) {
    var validationMessagePositionObjectId = "#" + $(element).attr("data-validationMessagePosition");
    formErrorLeft = ($(validationMessagePositionObjectId).offset().left + $(validationMessagePositionObjectId).outerWidth()) - offset.left + 10;
    var childPos = $(validationMessagePositionObjectId).offset();
    var parentPos = $(validationMessagePositionObjectId).parent().offset();
    formErrorTop = childPos.top - parentPos.top;
  }else{
    formErrorLeft = ($(element).offset().left + $(element).outerWidth()) - offset.left +  + 10;
  }

  formError.css("left", formErrorLeft);
  formError.css("top", formErrorTop);
  formError.fadeIn(100);
}


function hideFormError(formError) {
  $(formError).fadeOut(100);
}


function showInvalidState(element) {
  $(element).removeClass("validStat");
  $(element).addClass("invalidStat");
  if(!isFirstOnSubmitValidation){
    if( $(element).prop("type") == "checkbox" || $(element).prop("type") == "radio" ) {
      $(element).parent().addClass("validationInvalidBackground");
    }else{
      $(element).addClass("validationInvalidBackground");
    }
  }else{
    $(element).removeClass("validationInvalidBackground");
  }
}


function clearInvalidState(element) {
  $(element).removeClass("invalidStat");
  $(element).addClass("validStat");
  if( $(element).prop("type") == "checkbox" || $(element).prop("type") == "radio" ) {
    $(element).parent().removeClass("validationInvalidBackground");
  }else{
    $(element).removeClass("validationInvalidBackground");
  }
}


function addInvalidState(formError, element) {
  showFormError(formError, element);
  showInvalidState(element);
}


function removeInvalidState(formError, element) {
  hideFormError(formError);
  clearInvalidState(element);
}


// =============== validation(event) =============

$(document).ready(function() {
  $("input.validation,select.validation,textarea.validation").addClass("invalidStat");
});


$(document).on("focusout", "input.validation,select.validation,textarea.validation", function() {
  validate(this, false);
});


$(document).on("change", "input.validation,select.validation,textarea.validation", function() {
  validate(this, false);
});


$(document).on("change", "input.validationTrigger:radio", function() {
  validateOnchange();
});


// =============== wiki(function) ===============

function findAddonLineBreak(wiki_content_id) {
  var line_breaks = [];
  if ( $(wiki_content_id).val() && $(wiki_content_id).val().match(/[\n]{2,}/g) ) {
    line_breaks = $(wiki_content_id).val().match(/[\n]{2,}/g)
    .reduce(function(acc, p, i, all) {
      acc.push(p.substring(2));
      return acc
    }, [])
  }
  return line_breaks;
}


function replaceLineBreak(wiki_content_id) {
  var addon_line_breaks = findAddonLineBreak(wiki_content_id);
  addon_line_breaks = [""].concat(addon_line_breaks);

  // skip 2 linebreak before p tag
  var tmp = $(".editor-preview").html().split("\n\n<p>")
  .reduce(function(acc, p, i, all) {
    var addon = addon_line_breaks ? addon_line_breaks[i] : "";
    acc.push( addon + p);
    return acc;
  }, [])
  .join("<p>");
  // replace with <br> tag
  tmp = tmp.split("\n").join("<br>");

  $(".editor-preview").html(tmp);
}


// =============== wiki(event) =============


// =============== (function) ===============
// =============== (event) =============

//==================List View Function ===============

function submitFormOpenAllFolderListView(url, formId){
	var form = document.getElementById(formId);
	form.RequestType.value = 'onChange';
	form.viewMode.value = 'list';
	appendWindowId(form);

	$.post(url, $(form).serialize()).done(
		 function(data, status, xhr) {

			$("#fileViewTableListView").html($(data).find("#fileViewTableListView").html());
			$("#listViewFileTable").show();
			$("#treeviewFileTable").hide();

		 }).fail(function(XMLHttpRequest, status, errorThrown) {
	});
}

function displayTreeFileTable(){
	var form = document.getElementById(formId);
	form.viewMode.value = '';
	$("#treeviewFileTable").show();
	$("#listViewFileTable").hide();
}

function transform(element, value) {
	$(element).css('cssText','-ms-transform:'+ value);

}

