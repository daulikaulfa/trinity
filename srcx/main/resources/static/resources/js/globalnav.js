var app = app || {};

//Calculate joints' widths
function calculateJointWidths(){
	$(".globalNavJoint").each(function() {
		$(this).css("width", $(this).closest("li").width() + "px");
	});
}

app.globalNavigate = function() {
//  $(".globalNavIcon.count").click(function() {
//    $(".globalNavIcon.count .popupWindow").slideToggle(100);
//  });

	calculateJointWidths();


  $("#globalNavUl01").on("click", "li a", function(e) {
    var $list = $(this).closest("li");
    if ($list.data("role") !== "dropdown") {
      return;
    }
    if( $(this).hasClass("ajax")){
    	return;
    }

    e.preventDefault();
    e.stopPropagation();

    var active = $list.hasClass("active");
    $(".globalNavDropdownContainer").hide();
    $("#globalNavUl01").find("li.active").removeClass("active");

    if (active) {
      $(this).parent().find(".globalNavDropdownContainer").hide();
    } else {
      $list.addClass("active");
      $(this).parent().find(".globalNavDropdownContainer").show();
      $(this).parent().find(".globalNavDropdownSearchField").focus();
    }
  });

  var dismissDropdown = function() {
    $(".globalNavDropdownContainer:visible").hide();
    $("#globalNavUl01").find("li.active").removeClass("active");
  };

  var dismissSearchResults = function() {
    $("#globalNavSearchContainer").hide();
  };

  $(document).click(function(event) {
    if (
      !$(event.target).closest(".globalNavDropdownContainer").length &&
      !$(event.target).hasClass("globalNavDropdownContainer") &&
      !$(event.target).hasClass("globalNavSearchField")
    ) {
      dismissDropdown();
    }
  })

  $(".resultItem").click(function(e) {
    var href = $(this).attr("href");
    if (href.length > 1) {
      e.preventDefault();
      location.href = href;
    }
  });

  $(".clearSearchButton").click(function(e) {
    e.preventDefault();
    $(this).closest("form").trigger("reset");
    dismissSearchResults();
  });

  $(".globalNavSearchField").keyup(function() {
    dismissDropdown();

    var query = $(this).val().trim();
    if (query.length == 0) {
      dismissSearchResults();

    } else {
    	var request = new IncrementalSearchRequest()
                                .setUrl( getContextPath()+'project/globalSearch' )
                                .setData( $.param({'keyword' : query}) )
                                .setCallbackIfDone( function(data, status, xhr){
                                        $("#globalNavSearchContainer .globalNavSearchResult").html($(data).find("#assetsResults").parent().html()); // ("#assetsResults").parent() == .globalNavSearchResult
                                        $(".globalNavSearchResultSwitch.clearfix").html($(data).find("#assetsResults").parent().prev().html()); // ("#assetsResults").parent().prev() == .globalNavSearchResultSwitch
                                        $("#globalNavSearchContainer").show();
                                        $(".globalNavSearchResultSwitch").find("li").first().trigger("click");
                                   });
        IncrementalSearch.search( request );
    }
  });

  // Search results
  $(".globalNavSearchResultSwitch").on("click", "li", function() {
    $(".globalNavSearchResultSwitch li").removeClass("active");
    $(this).addClass("active");
    $("#globalNavSearchContainer .globalNavSearchResultContainer").hide();
    $($(this).data("target")).show();
  });

  // Filter
  $(document).on("click", ".clearFilterButton", function(e) {
    e.preventDefault();
    $(this).closest("form").trigger("reset");
    // TODO: filtered results
  });

};

app.submitUserEditing = function(userId) {
	form = document.getElementById('globalSearchFlowUserEditingService');
	form.memberId.value = userId;
	form.windowId.value = $('#windowId').val();
	submit(form.id);
}

app.submitReleaseResults = function(id, releaseType) {
	var form;
	
	if(releaseType == 'BP') {
		form = document.getElementById('globalSearchFlowBuildPackageDetails');
		form.dataId.value = id;
	} else if (releaseType == 'RP') {
		form = document.getElementById('globalSearchFlowReleasePackageDetails');
		form.dataId.value = id;
	}
	// don't implement ReleaseRequest, DeploymentJob
	submit(form.id);
}

app.submitAssetsResults = function(id, requestType) {
	var form;
	if(requestType == '40') {
		form = document.getElementById('globalSearchFlowCheckoutRequestDetails');
	} else if (requestType == '50') {
		form = document.getElementById('globalSearchFlowCheckInRequestDetails');
	} else if (requestType == '60') {
		form = document.getElementById('globalSearchFlowRemovalRequestDetails');
	}
	form.dataId.value = id;
	submit(form.id);
}

app.submitWikiDetails = function(lotId, wikiId) {
	var form = document.getElementById('globalSearchFlowWikiDetails');
	form.dataId.value = wikiId;
	submit(form.id);
}

