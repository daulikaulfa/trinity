var app = app || {};

app.search = {
  query: {},
  resultColumns: [],
  allColumns: [],
  resultLimit: 20,
  prepare: function() {
    $(".searchSection").on("click", ".clearSearchFormButton", function() {
      $(this).closest("form").trigger("reset");
      $("select[data-label='Data'] > option:first").prop("selected", true);
      $("select[name='dataProcessing']").val("").change();
      $("input[name='attachment']").prop("checked", false);
      $("input[name='keyword']").attr("value", "");
      $("input[name='dateStart']").attr("value", "");
      $("input[name='dateEnd']").attr("value", "");
      $("input[name='keyword']").attr("value", "");
      
      $("#resultBody").css("display", "none");
      $("#emptyResultsMessage").css("display", "block");
      $("#submitSearchCondition").prop('disabled', true);
      updateSearchQuery();
      return false;
    });

    var isArray = function(obj) {
      return Object.prototype.toString.call(obj) === '[object Array]'
    };

    var buildSearchQuery = function() {
      query = {};
      var isArrayEmpty = function(array) {
        if (array == []) {
          return true;
        }

        if (array.length == 1 && array[0] === "") {
          return true;
        }

        return false;
      };

      $(".globalSearchForm .filterField").each(function(index) {
        var $field = $(this);
        var label = $field.data("label");
        var name = $field.attr("name");
        var value = $field.val();

        if ($field.attr("type") === "checkbox") {
          if ($field.prop("checked")) {
            query[name] = { value: value, text: "yes", label: label };
          }
        } else if ($field.prop("tagName").toLowerCase() === "select") {
          if (isArray(value)) {
            if (!isArrayEmpty(value)) {
              var text = $field.find("option:selected").map(function() {
                return $(this).text().trim();
              }).get();
              query[name] = { value: value, text: text, label: label };
            }
          } else if (value !== null && value !== "") {
            query[name] = { value: value, text: $field.find("option:selected").text().trim(), label: label };
          }
        } else if (value !== null && value !== "") {
          query[name] = { value: value, text: value, label: label };
        }
      });

      return query;
    };

    var showSearchQueryInfo = function(queryInfo) {
      var info = "";
      var htmlStart = "<span class='filter'><i class='fa fa-check'></i>";
      var htmlEnd = "</span>"
      for (key in queryInfo) {
        var field = queryInfo[key];
        var title = field.label + "=" + field.text;
        info += htmlStart + title + htmlEnd;
      }

      $(".searchQueryDetail").html(info);
      
      var $sectionHeader = $(".sectionHeader");
      var $row = $(".sectionHeader .row");
      if($sectionHeader.outerHeight() != $row.outerHeight()) {
    	  $sectionHeader.height($row.height()).animate({ height: $row.height()}, 500);
      }
    };

    var updateSaveDialogQueryInfo = function(queryInfo) {
      var info = "";
      var htmlStart = "<tr><td><i class='fa fa-check'></i>";
      var htmlMiddle = "</td><td>=";
      var htmlEnd = "</td></tr>"
      for (key in queryInfo) {
        var field = queryInfo[key];
        var value = field.text;
        if (isArray(value)) {
          value = value.join("<br><span style='color:white'>=</span>");
        }
        info += htmlStart + field.label + htmlMiddle + value + htmlEnd;
      }

      $(".searchQueryInfoTable tbody").html(info);
    };

    var searchQueryInfo = function(query) {
      var info = []; // UI label: value

      // Special handling for date
      var dateRange = [];
      var dateLabel = $("input[name=dateStart]").closest(".filterFieldGroup").data("label");

      for (key in query) {
        var field = query[key];
        if (key === "dateStart") {
          if (query["dateEnd"] === undefined) {
            info.push({ label: dateLabel, value: field.value, text: field.value });
          } else {
            dateRange.unshift(field.value);
          }
        } else if (key === "dateEnd") {
          if (query["dateStart"] === undefined) {
            info.push({ label: dateLabel, value: field.value, text: field.value });
          } else {
            dateRange.push(field.value);
            info.push({ label: dateLabel, value: dateRange.join("~"), text: dateRange.join("~") });
          }
        } else {
          info.push({ label: field.label, value: field.value, text: field.text });
        }
      }

      return info;
    };

    var updateSearchQuery = function() {
      var query = buildSearchQuery();
      var queryInfo = searchQueryInfo(query);
      showSearchQueryInfo(queryInfo);
      updateSaveDialogQueryInfo(queryInfo);

      app.search.query = query;
    };

    $(".globalSearchForm .filterField").on("change", function() {
      updateSearchQuery();
    });

    $(".globalSearchDateWrapper").css("padding-top", $(".globalSearchLotWrapper").height() - $(".globalSearchDateWrapper").height() + "px");

    // Search Filter Behavior
    $(".searchFilterForm").on("submit", function() {
      if ($(this).find(".searchFilterName").val().trim() === "") {
        $(this).find(".searchFilterNameError").show();
        return false;
      } else {
        $(this).find(".searchFilterNameError").hide();
        alert("TODO: save with app.search.query object.");
        return false;
      }
    });

    $(".datePicker").datepicker({
      format: "yyyy/mm/dd",
      language: "ja",
      autoclose: true,
      todayHighlight: true,
      clearBtn: true
    }).on("clearDate", function(e) {
      updateSearchQuery();
    });

    // Quick select links
    $(".quickOptionLink").unbind().click(function(e) {
    	var $selectContact = $("select[name='contact']");
    	$("option:selected", $selectContact).prop("selected", false);
    	
    	var $selectedOption = $("option[value='" + $(this).data("values") + "']", $selectContact);
    	$selectedOption.prop("selected", true);
    	$selectContact.change();
    	
    	return false;
    });

    // Search Result Setting
    $(function() {
    	var allColumns = $(".resultSettingForm input[type=checkbox]");
        var columns = allColumns.map(function() {
          return { name: $(this).attr("name"), title: $(this).parent().text().trim() };
        }).get();
    	app.search.allColumns = columns;
    });
    
    var updateResultSetting = function() {
    	for(key in app.search.allColumns) {
    		var name = app.search.allColumns[key].name;
    		var isShow = false;
    		
    		for(key in app.search.resultColumns) {
    			var showName = app.search.resultColumns[key].name;
    			if(name == showName) {
    				$("th[data-column='" +name+"']").show();
    				$("td[data-column='" +name+"']").show();
    				isShow = true;
    				break;
    			}
    			isShow = false;
    		}
    		
    		if(!isShow) {
    			$("th[data-column='" +name+"']").hide();
    			$("td[data-column='" +name+"']").hide();
    		}
    	}
    };

    var updateResultColumns = function() {
      var checked = $(".resultSettingForm input[type=checkbox]:checked");
      var columns = checked.map(function() {
        return { name: $(this).attr("name"), title: $(this).parent().text().trim() };
      }).get();
      app.search.resultColumns = columns;
    };

    $(".resultLimitLink").click(function() {
      if ($(this).hasClass("current")) {
        return false;
      }

      $(".resultLimitLink").removeClass("current");
      $(this).addClass("current");
      $(this).closest("form").find(".limitField").val($(this).data("value"));

      app.search.resultLimit = $(this).data("value");
      
      $("input[name=linesPerPage").val(app.search.resultLimit);
      ajax_refresh();

      return false;
    });

    $(".resultSettingForm input[type=checkbox]").on("change", function() {
      updateResultColumns();
      updateResultSetting();
    });

    $("#resultSettingDialog").on("show.bs.modal", function() {
      $(".resultSettingForm input[type=checkbox]").prop("checked", false);
      for (key in app.search.resultColumns) {
        column = app.search.resultColumns[key];
        $(".resultSettingForm input[name=" + column.name + "]").prop("checked", true);
      }

      $(".limitField").val(app.search.resultLimit);
      $(".resultLimitLink").each(function() {
        if ($(this).data("value") == app.search.resultLimit) {
          $(this).addClass("current");
        } else {
          $(this).removeClass("current");
        }
      });

    });
  }
};
