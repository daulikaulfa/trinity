// Japanese
jQuery.timeago.settings.strings = {
  prefixAgo: "",
  prefixFromNow: "今から",
  suffixAgo: "前",
  suffixFromNow: "後",
  seconds: "たった今",
  minute: "1 分",
  minutes: "%d 分",
  hour: "1 時間",
  hours: "%d 時間",
  day: "1 日",
  days: "%d 日",
  month: "1 ヶ月",
  months: "%d ヶ月",
  year: "1 年",
  years: "%d 年",
  wordSeparator: ""
};
