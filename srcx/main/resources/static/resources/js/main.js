// JavaScript Document
//@codekit-prepend "_lib/js-markdown-extra.js"
//@codekit-prepend "_lib/jquery.markdown-live.js"
//@codekit-prepend "_lib/jquery.matchHeight.js"
//@codekit-prepend "_lib/jquery.tablesorter.js"
//@codekit-prepend "_lib/jquery.timeago.js"
//@codekit-prepend "_lib/jquery.timeago.ja.js"
//@codekit-prepend "_lib/jquery.repeater.min.js"
//@codekit-prepend "_lib/magicsuggest-min.js"
//@codekit-prepend "globalnav.js"
//@codekit-prepend "search.js"
//@codekit-prepend "profile.js"


$(function () {
	'use strict';
  $('[data-toggle="tooltip"]').tooltip();

$('.twoColumnSectionBodyTab li a').click(function() {

	var index = $('.twoColumnSectionBodyTab li a').index(this);
	$('.twoColumnSectionBodyTabContent .tabPanel').removeClass('active');
	$('.twoColumnSectionBodyTabContent .tabPanel').eq(index).addClass('active');
	$('.twoColumnSectionBodyTab li a').removeClass('select');
	$(this).addClass('select');
});
$('.lotList').hover(function() {

	$(this).addClass('select');
},function(){
	$(this).removeClass('select');

});

$('.treeviewBtn').click(function () {//add 2016.03.22 @tkr ... Treeview関連。下部にも有り
	$(this).siblings("ul").slideToggle();
	if($(this).hasClass("fa-caret-right")){

        $(this).removeClass("fa-caret-right").addClass("fa-caret-down");

    }else{
        $(this).removeClass("fa-caret-down").addClass("fa-caret-right");}
		if($(this).siblings(".treeviewIcon").hasClass("fa-folder-o")){

        $(this).siblings(".treeviewIcon").removeClass("fa-folder-o").addClass("fa-folder-open-o");

    }else{
        $(this).siblings(".treeviewIcon").removeClass("fa-folder-open-o").addClass("fa-folder-o");}



        return false;
    });


$('.newPasswordPopover').hover(function(){
	$(this).popover({
      html : true,
	  placement : 'bottom',
      content: function() {
        return $('#popoverContents').html();
      }
	  });
});





  $('.popover-top-hover').popover({
    trigger: 'hover' // click,hover,focus,manualを選択出来る
  });

  //$('.popover-click-html').popover();

  $('h2.faBtn').click(function () {
	  $(this).parents(".section").children(".faBtnContents").slideToggle();
	  if($(this).children(".fa").hasClass("fa-minus-square")){

        $(this).children(".fa").removeClass("fa-minus-square").addClass("fa-plus-square");

    }else{
        $(this).children(".fa").removeClass("fa-plus-square").addClass("fa-minus-square");}



        return false;
    });

  $('.popover-click-html').popover({
    html : true,
    //title: function() {
    //  return $(".popover-click-html#popover-head").html();
    //},
    content: function() {
      return $(".popover-click-html-contents").html();
    }
});

    //http://www.paulund.co.uk/fixed-width-sortable-tables
    $('.sortable tbody').sortable({
        helper: fixWidthHelper,
        handle: ".handle",
        cursor: "move"
    }).disableSelection();

    function fixWidthHelper(e, ui) {
        ui.children().each(function() {
            $(this).width($(this).width());
        });
        return ui;
    }

  $('.buildBox').matchHeight();

  $('.matchHeight').matchHeight();

    //edit 2017.05.11 @GiAnG
    $.tablesorter.addParser({
        id: 'fileSizeSorter',
        is: function (s) {
            return /^\d+\.?\d*[KMGT]?B$/.test(s);
        },
        format: function (s) {
            var sizeDict = {"B": 1, "KB": 1e3, "MB": 1e6, "GB": 1e9, "TB": 1e12};
            for (var i = 0; i < s.length; i++) {
                if (!/[\d.]/.test(s[i])) {
                    return s.substring(0, i) * sizeDict[s.substring(i)];
                }
            }
        },
        type: 'numeric'
    });
    var sortHeaders = {headers: {}};
    $('.tableSorter th.checkboxHead').each(function (index) {
        sortHeaders.headers[index] = {sorter: false};
    });
    $('.tableSorter th.sorter-false').each(function (index) {
    	var headerIndex = $(this).index();
        sortHeaders.headers[headerIndex] = {sorter: false};
    });
    
    $(".tableSorter").tablesorter(sortHeaders);


    $('a.cloneWindow').click(function () {
        window.open(document.URL);
    });//add 2016.03.15 @yuhei.suzuki

    $('table td.btns a.removeRow').click(function () {
        $(this).parent().parent().fadeOut("1500").queue(function() {
            this.remove();
        });
        //$(this).parent().parent().remove();
    });//add 2016.03.15 @yuhei.suzuki
/*
    $(".datepicker").datepicker();
    $(".datepicker").datepicker("option", "showOn", 'button');
    $(".datepicker").datepicker("option", "dateFormat", 'yy/mm/dd');
    //$(".datepicker").datepicker("option", "buttonText", 'カレンダーから入力');
    $(".datepicker").datepicker("option", "buttonImageOnly", true);
    $(".datepicker").datepicker("option", "buttonImage", 'images/common/icon_calender_mini_28px.png');

    $('[data-toggle=datepicker]').each(function () {
        var target = $(this).data('target') || '';
        if (target) {
            $(target).datepicker();
            $(this).bind("click", function () {
                $(target).datepicker("show");
            });
        }
    });*/


  app.globalNavigate();
//  app.copyToClipboard();
  app.search.prepare();

  jQuery("time.timeago").timeago();




    $('.fileAddUnitBtn').click(function(){
		$('.fileAddUnit').slideToggle('fast');
		$('.folderAddUnit').hide('fast');
	}); // add @tkr wiki, file関連
	$('.folderAddUnitBtn').click(function(){
		$('.folderAddUnit').slideToggle('fast');
		$('.fileAddUnit').hide('fast');
	}); // add @tkr file関連

	 $('.subContentsCloseUnit span i').click(function(){
		$('.subContents').toggleClass("width0");
		$('.mainContents').toggleClass("width100");
		$(this).toggleClass("fa-caret-square-o-right").toggleClass("fa-caret-square-o-left");
	}); // add @tkr wiki関連
	$('.subMenuBtn').click(function(){
		$(this).children("ul").slideToggle('fast');
		$(this).children("span").children("i").toggleClass("fa-caret-down").toggleClass("fa-caret-up");
	}); // add @tkr wiki関連

	$('#markDownArea').markdownlive(); // add @tkr wiki関連

	$('#markDownTitle').markdownlive(); // add @tkr wiki関連

	$('.mainContentsTab ul li').click(function() {
		var index = $('.mainContentsTab ul li').index(this);
		$('.mainContentsTabContent').removeClass('active');
		$('.mainContentsTabContent').eq(index).addClass('active');
		$('.mainContentsTab ul li').removeClass('select');
		$(this).addClass('select');
	}); // add @tkr wiki関連

    $('.sectionConfigContentsTab ul li').click(function() {
        var index = $('.sectionConfigContentsTab ul li').index(this);
        $('.sectionConfigContentsTabContent').removeClass('active');
        $('.sectionConfigContentsTabContent').eq(index).addClass('active');
        $('.sectionConfigContentsTab ul li').removeClass('select');
        $(this).addClass('select');
    }); // copy @yuhei お知らせ


});


$(document).ready( function(){
	$('input.first').focus();   //add 2016.03.18 @suzuki.yuhei
    var selectedThemeSession = $("#selected-theme");
    var currentThemeSession = $("html").attr("class");
	if (selectedThemeSession && currentThemeSession != selectedThemeSession.val()) {
    	$("html").removeClass();
    	$("html").addClass(selectedThemeSession.val());
    }
});


$('.allCheck').change(function () {  // add @tkr checkboxを一括でチェック入れられるようにする
	'use strict';
	$('tbody tr th input[type="checkbox"]').prop('checked', $(this).prop('checked'));
	$('tbody tr td input[type="checkbox"]').prop('checked', $(this).prop('checked'));
});



$(".treeview").scroll(function() { //add 2016.03.22 @tkr ... 何か別な方法があればいいけど...
	'use strict';
	var fromLeft = $(".treeview").scrollLeft() +175;
    $(".treeviewCount").css('left', fromLeft + 'px');
});



//add 2016.04.20 @tkr ... toggleのみ
//fix 2016.06.28 y.suzuki change event to document.on
$(document).on('click', '.calendarTableLeftHeaderClose', function(){
	'use strict';
	$(this).parents('.calendarTable').toggleClass("closeTableLeft");
});

$(document).on('click', '.calendarTableUnitHeader', function(){
	'use strict';
	$(this).parents('.calendarTableUnit').toggleClass("closeTable");
});

$(document).on('click', '.chart', function(){
	'use strict';
	$(this).siblings('.chartPopUp').toggleClass("chartPopUpOn");
});

$(document).on('click', '.popupWindowHeaderRight', function(){
	'use strict';
	$(this).parents('.chartPopUp').toggleClass("chartPopUpOn");
});
