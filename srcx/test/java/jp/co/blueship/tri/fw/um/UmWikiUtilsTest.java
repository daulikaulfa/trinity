package jp.co.blueship.tri.fw.um;

import org.junit.Test;

public class UmWikiUtilsTest {

	//@Ignore
	@Test
	public void extractTags() {
		String pageName = "[tag1][tag2](tag3)(tag4) Wiki Sample [tag5][tag6]";

		for ( String tag: UmWikiUtils.extractTags(pageName) ) {
			System.out.println("tag: " + tag);
		}
	}

	//@Ignore
	@Test
	public void formatPageName() {
		String pageName = "[tag1][tag2](tag3)(tag4) Wiki Sample [tag5][tag6]";

		System.out.println("before: " + pageName);
		System.out.println("after: " + UmWikiUtils.formatPageName(pageName));
	}

}
