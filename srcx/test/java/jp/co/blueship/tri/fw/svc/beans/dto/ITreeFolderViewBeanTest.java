package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean.CheckedState;

public class ITreeFolderViewBeanTest {

	@Ignore
	@Test
	public void testToTreeFolderViewBean() {
		System.out.println("TreeFolderViewBean");

		ITreeFolderViewBean root = new TreeFolderViewBean().setOwner().setName("");
		ITreeFolderViewBean module1 = new TreeFolderViewBean().setName("module1");
		ITreeFolderViewBean module2 = new TreeFolderViewBean().setName("module2");
		root.addFolder(module1).addFolder(module2);
		module1.addFolder(new TreeFolderViewBean().setName("xxx"));

		ITreeFolderViewBean module1_sub1 = new TreeFolderViewBean().setName("sub1");
		module1.addFolder(module1_sub1);
		module1_sub1.addFolder(new TreeFolderViewBean().setName("bbb"));

		System.out.println(root.getFolder("module1").getName());
		//Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		//for debug - PrettyPrinting
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(root);
        System.out.println("toJson: " + json);
	}

	//@Ignore
	@Test
	public void testToTreeFolderViewBeanFromPath() {
		System.out.println("TreeFolderViewBean");
		String[] paths = this.getSamplePath();

		List<String> folders = new ArrayList<String>();

		for ( String path: paths ) {
			String[] split = TriStringUtils.splitPath( path );
			folders.add( split[0] );
		}

		ITreeFolderViewBean root = TreeFolderViewBean.toBean( folders );

		//Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		//for debug - PrettyPrinting
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(root);
        System.out.println("toJson: " + json);

        for (String path: root.getAllPath() ) {
            System.out.println("path: " + path);
        };
	}

	@Ignore
	@Test
	public void testCheckedStateFromPath() {
		System.out.println("TreeFolderViewBean");
		String[] paths = this.getSamplePath();

		List<String> folders = new ArrayList<String>();

		for ( String path: paths ) {
			String[] split = TriStringUtils.splitPath( path );
			folders.add( split[0] );
		}

		ITreeFolderViewBean root = TreeFolderViewBean.toBean( folders );

		root.getFolder("module_A").setCheckedState(CheckedState.Checked);

		//Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
		//for debug - PrettyPrinting
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

		System.out.println("\nmodule_A is Checked, module_B is UnChecked ");
		System.out.println("toJson: " + gson.toJson(root));

		root.getFolder("module_A/apl/comment").setCheckedState(CheckedState.UnChecked);

		System.out.println("\nmodule_A is Mixed: ");
		System.out.println("toJson: " + gson.toJson(root));

		root.getFolder("module_A").setCheckedState(CheckedState.Checked);
		root.getFolder("module_B").setCheckedState(CheckedState.Checked);

		System.out.println("\nAll Checked: ");
		System.out.println("toJson: " + gson.toJson(root));
	}

	private String[] getSamplePath() {
		return new String[]{
				"module_A/apl/comment/goal.txt",
				"module_A/apl/comment/hint.txt",
				"module_A/apl/debug/important.txt",
				"module_A/apl/debug/jump.txt",
				"module_A/apl/folder_manage.txt",
				"module_A/book/memo/none.txt",
				"module_A/book/memo/open_source.txt",
				"module_B/questionnaires/user/dummy.txt",
				"module_B/questionnaires/user/dummy01.txt",
				"module_B/questionnaires/user/dummy02.txt",
				"module_B/result/venture/dummy_1.txt",
				"module_B/result/venture/dummy_2.txt",
				"module_B/result/venture/dummy_3.txt",
				"module_B/result/xperience/dummy.txt",
				"module_B/statistics/dummy.txt",
				"module_B/statistics/error.txt",
				"module_B/statistics/ytinirt/trinity.txt",
				};
	}

}
