package jp.co.blueship.tri.fw.svc.beans.dto;

import org.junit.Ignore;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;

public class ISearchFilterTest {

	@Ignore
	@Test
	public void test1() {
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

		System.out.println("toJson: ");

		ISearchFilter filter = new TestSearchCondition()
				.setStsId("1000")
				.setPjtId("PJT-0001")
				.setKeyword("keyword")
		;

		String json = gson.toJson(filter);
		System.out.println(json);

		System.out.println("\ntoBean: ");
		TestSearchCondition condition = gson.fromJson(json, TestSearchCondition.class);

		System.out.println("StsId: " + condition.getStsId());
		System.out.println("PjtId: " + condition.getPjtId());
		System.out.println("CtgId: " + condition.getCtgId());
		System.out.println("MstoneId: " + condition.getMstoneId());
		System.out.println("Keyword: " + condition.getKeyword());
		System.out.println("SelectedPageNo: " + condition.getSelectedPageNo());
	}

	//@Ignore
	@Test
	public void test2() {
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

		System.out.println("toJson: ");

		SearchCondition filter = new FlowCheckInOutRequestListServiceBean().getParam().getSearchCondition();
		filter
			.setAreqCtgCd( AreqCtgCd.LendingRequest.value() )
		;

		String json = gson.toJson(filter);
		System.out.println(json);

		System.out.println("\ntoBean: ");
		SearchCondition condition = gson.fromJson(json, SearchCondition.class);
		System.out.println("isDraft: " + condition.isDraft());
		System.out.println("AreqCtgCd: " + condition.getAreqCtgCd());
	}

	class TestSearchCondition  implements ISearchFilter {
		@Expose private String stsId = null;
		@Expose private String pjtId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		@Expose private boolean exsists = false;
		private Integer selectedPageNo = 1;

		public String getStsId() {
			return stsId;
		}
		public TestSearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getPjtId() {
			return pjtId;
		}
		public TestSearchCondition setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getCtgId() {
			return ctgId;
		}
		public TestSearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public TestSearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}
		public String getKeyword() {
			return keyword;
		}
		public TestSearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public TestSearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

	}

}
