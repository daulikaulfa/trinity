package jp.co.blueship.tri.fw.um;

import org.junit.Test;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;

public class UmDesignBusinessRuleUtilsTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-Design-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	//@Ignore
	@Test
	public void testGetUserIconSharePath() {
		IUserEntity defaultUser = this.getDefaultUser();
		IUserEntity customUser = this.getCustomUser();

		System.out.println( "IconType(DEFAULT-EMPTY) := " + UmDesignBusinessRuleUtils.getUserIconSharePath(getDefaultUserEmpty()) );
		System.out.println( "IconType(DEFAULT) := " + UmDesignBusinessRuleUtils.getUserIconSharePath(defaultUser) );
		System.out.println( "IconType(CUSTOM) := " + UmDesignBusinessRuleUtils.getUserIconSharePath(customUser) );

	}

	//@Ignore
	@Test
	public void testGetUserIconPath() {

		System.out.println( "parent path := " + UmDesignBusinessRuleUtils.getUserCustomIconParentLocalPath("dev1") );
		System.out.println( "local path := " + UmDesignBusinessRuleUtils.getUserCustomIconLocalPath("dev1", "custom.png") );
		System.out.println( "relative path := " + UmDesignBusinessRuleUtils.getUserCustomIconSharePath("admin", "download.png"));

	}
	
	//@Ignore
	@Test
	public void testProjectIconPath() {
		System.out.println( "ProjectIconSharePath(DEFAULT) := " + UmDesignBusinessRuleUtils.getProjectIconParentSharePath( IconSelectionOption.DefaultImage ) );
		System.out.println( "ProjectIconSharePath(CUSTOM)  := " + UmDesignBusinessRuleUtils.getProjectIconParentSharePath( IconSelectionOption.CustomImage ) );
		
		System.out.println( "ProjectIconLocalPath := " + UmDesignBusinessRuleUtils.getProjectCustomIconLocalPath("project.png") );
	}
	
	//@Ignore
	@Test
	public void testLotIconPath() {
		System.out.println( "LotIconSharePath(DEFAULT) := " + UmDesignBusinessRuleUtils.getLotIconParentSharePath("LOT-10002",IconSelectionOption.DefaultImage));
		System.out.println( "LotIconSharePath(CUSTON) := " + UmDesignBusinessRuleUtils.getLotIconParentSharePath("LOT-10002",IconSelectionOption.CustomImage));

		System.out.println( "LotIconLocalPath :=" + UmDesignBusinessRuleUtils.getLotCustomIconLocalPath("LOT-10002","custom.png"));
	}

	private IUserEntity getDefaultUserEmpty() {
		IUserEntity user = new UserEntity();

		user.setUserId( this.getAuthUserId() );

		return user;
	}

	private IUserEntity getDefaultUser() {
		IUserEntity user = new UserEntity();

		user.setUserId( this.getAuthUserId() );
		user.setIconTyp( "DEFAULT" );
		user.setDefaultIconPath( "/trinity/resources/images/user-icons/default.png" );

		return user;
	}

	private IUserEntity getCustomUser() {
		IUserEntity user = new UserEntity();

		user.setUserId( this.getAuthUserId() );
		user.setIconTyp( "CUSTOM" );
		user.setCustomIconPath( "/resources/users/dev1/images/user-icons/custom.png" );

		return user;
	}


}
