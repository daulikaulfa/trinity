package jp.co.blueship.tri.dcm;

import org.junit.Test;

import jp.co.blueship.tri.fw.domain.TestDomainSupport;

public class DcmDesignBusinessRuleUtilsTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-Design-Context.xml" };

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	//@Ignore
	@Test
	public void testGetDownloadPath() {
		System.out.println ( DcmDesignBusinessRuleUtils.getDownloadTempPath( getAuthUserId() ) );
	}
}
