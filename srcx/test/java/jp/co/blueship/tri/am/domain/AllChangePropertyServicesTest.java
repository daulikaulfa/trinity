package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domain.pjt.FlowChangePropertyOverviewServiceTest;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class AllChangePropertyServicesTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ChangeProperty-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			//String lotId = this.getLotId();
			String pjtId = "PJT-1605250001";

			//pjtId = new FlowChangePropertyCreationServiceTest().create( service, this, lotId );
			//new FlowChangePropertyEditServiceTest().edit(service, this, pjtId);
			//new FlowChangePropertyCloseServiceTest().remove(service, this, pjtId);;
			//new FlowChangePropertyCloseServiceMultipleCloseTest().close(service, this, new String[]{"PJT-1603250001","PJT-1603300001"});;
			//new FlowChangePropertyTestCompletionServiceTest().testComplete( service, this, pjtId );
			//new FlowChangePropertyRemovalServiceTest().remove(service, this, pjtId);
			//new FlowChangePropertyDetailsServiceTest().details( service, this, pjtId );
			//new FlowChangePropertyListServiceTest().list( service, this, lotId );
			new FlowChangePropertyOverviewServiceTest().overview(service, this, pjtId);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
