package jp.co.blueship.tri.am.domain.pjt;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowChangePropertyCloseServiceTest {

	public void close(IGenericTransactionService service, TestDomainSupport support, String pjtId) throws Exception {

		// Set Param Bean
		FlowChangePropertyCloseServiceBean serviceBean = new FlowChangePropertyCloseServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		serviceBean.getParam().setSelectedPjtId(pjtId);

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangePropertyCloseService.value(), serviceDto);
			this.debug(serviceBean);
			assertTrue( serviceBean.getResult().isCompleted() );
		}


		// RequestType = submitChages
		if ( serviceBean.getResult().isCompleted() ) {
			serviceBean.getParam().getInputInfo().setComment("For backend test");

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangePropertyCloseService.value(), serviceDto);
			this.debug(serviceBean);

			IPjtDao dao = (IPjtDao)support.getBean( "amPjtDao" );
			PjtCondition condition = new PjtCondition();
			condition.setPjtId(pjtId);
			IPjtEntity pjtEntity = dao.findByPrimaryKey(condition.getCondition());

			assertTrue( serviceBean.getResult().isCompleted() );
			assertTrue( null != pjtEntity );
			assertTrue( AmPjtStatusId.ChangePropertyClosed.equals(pjtEntity.getStsId()) );

		}
	}

	private void debug( FlowChangePropertyCloseServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
