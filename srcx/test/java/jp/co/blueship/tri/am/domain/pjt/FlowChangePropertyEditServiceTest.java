package jp.co.blueship.tri.am.domain.pjt;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyEditServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowChangePropertyEditServiceTest {

	public void edit(IGenericTransactionService service, TestDomainSupport support, String pjtId) throws Exception {

		// Set Param Bean
		FlowChangePropertyEditServiceBean serviceBean = new FlowChangePropertyEditServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		ChangePropertyEditInputBean inputInfo = serviceBean.getParam().getInputInfo();

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangePropertyEditService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// RequestType = submitChages
		{
			inputInfo.setReferenceId("HOTFIX5");
			inputInfo.setReferenceCategoryId("20");
			inputInfo.setSummary("xuan haru summary - edit");
			inputInfo.setContents("xuan haru content -edit");
			//inputInfo.setCtgId("16030043");
			//inputInfo.setMstoneId("2");
			inputInfo.setAssigneeId("dev1");
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangePropertyEditService.value(), serviceDto);
			this.debug(serviceBean);

			IPjtDao dao = (IPjtDao)support.getBean( "amPjtDao" );
			PjtCondition condition = new PjtCondition();
			condition.setPjtId(pjtId);
			IPjtEntity pjtEntity = dao.findByPrimaryKey(condition.getCondition());

			assertTrue( inputInfo.getReferenceId().equals(pjtEntity.getChgFactorNo()));
			assertTrue( inputInfo.getReferenceCategoryId().equals(pjtEntity.getChgFactorId()));
			assertTrue( inputInfo.getSummary().equals(pjtEntity.getSummary()));
			assertTrue( inputInfo.getContents().equals(pjtEntity.getContent()));
			assertTrue( inputInfo.getCtgId().equals(inputInfo.getCtgId()));
			assertTrue( inputInfo.getMstoneId().equals(inputInfo.getMstoneId()));

		}
	}

	private void debug( FlowChangePropertyEditServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getCategoryViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Assignee View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getAssigneeViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("ChangePropertyView ");
			System.out.println("  submitterNm: " + serviceBean.getDetailsView().getSubmitterNm());

			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
