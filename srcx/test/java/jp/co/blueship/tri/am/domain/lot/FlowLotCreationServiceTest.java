package jp.co.blueship.tri.am.domain.lot;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.AuthorizedGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.BuildEnvironment;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.Module;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCreationServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowLotCreationServiceTest {

	public String create(IGenericTransactionService service, TestDomainSupport support) throws Exception {
		String createdId = null;

		// Param
		FlowLotCreationServiceBean serviceBean = new FlowLotCreationServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmLotCreationService.value(), serviceDto);
			assertTrue(!serviceBean.getResult().isCompleted());
		}

		// submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(serviceBean);
			service.execute(ServiceId.AmLotCreationService.value(), serviceDto);
			assertTrue(serviceBean.getResult().isCompleted());
			createdId = serviceBean.getResult().getLotId();
			support.threadWait( serviceBean );
		}

		return createdId;
	}

	private void mapping(FlowLotCreationServiceBean bean) {

		LotEditInputBean info = bean.getParam().getInputInfo();

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

			info.setLotSubject("subject").setLotSummary("summary").setLotContents("contents")
					.setBaselineTag("baselineTagName").setUseMerge(true).setPublicPath("C:/user")
					.setPrivatePath("C:/sys").setUseDefaultPublicPath(true).setUseDefaultPrivatePath(true)
					.setEnableUnauthorizedGroup(false);

			{ // Authorized Group
				AuthorizedGroup group = info.new AuthorizedGroup().setGroupId("15060001").setGroupNm("").setSelected(true);
				AuthorizedGroup group1 = info.new AuthorizedGroup().setGroupId("1").setGroupNm("").setSelected(true);
				AuthorizedGroup group2 = info.new AuthorizedGroup().setGroupId("2").setGroupNm("").setSelected(true);
				AuthorizedGroup group3 = info.new AuthorizedGroup().setGroupId("3").setGroupNm("").setSelected(true);
				info.setAuthorizedGroups(FluentList.from(new AuthorizedGroup[] { group, group1, group2, group3 }).asList());
			}
			{ // E-mail Recipient Group
			}
			{ // Module
				Module module = info.new Module().setRepository("file:///C:/csvn/data/repositories/repos/").setModuleNm("trinity-schemaCompiler").setSelected(true);
				info.setModules(FluentList.from(new Module[] { module }).asList());
			}
			{ // Build Environment
				BuildEnvironment diff = info.new BuildEnvironment().setEnvId("BM_DIFF").setSelectedBuild(true);
				BuildEnvironment full = info.new BuildEnvironment().setEnvId("BM_FULL").setSelectedFullBuild(true);
				info.setBuildEnvs(FluentList.from(new BuildEnvironment[] { diff, full }).asList());
			}
			{ // Release Environment
			}
		}
	}

}
