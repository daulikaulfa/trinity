package jp.co.blueship.tri.am.domain.pjt;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCreationServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowChangePropertyCreationServiceTest {

	public String create(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {

		// Set Param Bean
		FlowChangePropertyCreationServiceBean serviceBean = new FlowChangePropertyCreationServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedLotId(lotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		ChangePropertyEditInputBean inputInfo = serviceBean.getParam().getInputInfo();

		String ctgId = null;
		String mstoneId = null;
		String assigneeId = null;
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangePropertyCreationService.value(), serviceDto);
			this.debug(serviceBean);
			assertTrue(null == serviceBean.getResult().getPjtId());
			if ( 0 < serviceBean.getParam().getInputInfo().getCategoryViews().size() ) {
				ctgId = serviceBean.getParam().getInputInfo().getCategoryViews().get(0).getValue();
			};
			if ( 0 < serviceBean.getParam().getInputInfo().getMstoneViews().size() ) {
				mstoneId = serviceBean.getParam().getInputInfo().getMstoneViews().get(0).getValue();
			};
			if ( 0 < serviceBean.getParam().getInputInfo().getAssigneeViews().size() ) {
				assigneeId = serviceBean.getParam().getInputInfo().getAssigneeViews().get(0).getValue();
			};
		}

		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			inputInfo.setReferenceId("HOTFIX4");
			inputInfo.setReferenceCategoryId("50");
			inputInfo.setSummary("xuan_summary");
			inputInfo.setContents("xuan_content");
			if ( null != ctgId ) inputInfo.setCtgId(ctgId);
			if ( null != mstoneId ) inputInfo.setMstoneId(mstoneId);
			if ( null != assigneeId ) inputInfo.setAssigneeId(assigneeId);
			service.execute(ServiceId.AmChangePropertyCreationService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return serviceBean.getResult().getPjtId();
	}

	private void debug( FlowChangePropertyCreationServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getCategoryViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Assignee View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getInputInfo().getAssigneeViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("ChangePropertyView ");
			System.out.println("  submitterNm: " + serviceBean.getDetailsView().getSubmitterNm());

			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
			System.out.println("  pjtId: " + serviceBean.getResult().getPjtId());
		}
	}

}
