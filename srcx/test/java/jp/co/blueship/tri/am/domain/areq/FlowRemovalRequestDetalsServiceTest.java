package jp.co.blueship.tri.am.domain.areq;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowRemovalRequestDetalsServiceTest {

	public void details(IGenericTransactionService service, TestDomainSupport support, String areqId) throws Exception {

		FlowRemovalRequestDetailsServiceBean serviceBean = new FlowRemovalRequestDetailsServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedAreqId(areqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmRemovalRequestDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// onChange
		{
			serviceBean.getParam().getResourceSelection().setPath("/trinity-schemaCompiler");
	    	serviceBean.getParam().setRequestType(RequestType.onChange);
			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.selectFolder);
			service.execute(ServiceId.AmRemovalRequestDetailsService.value(), serviceDto);
			debug( serviceBean );

			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.openFolder);
			service.execute(ServiceId.AmRemovalRequestDetailsService.value(), serviceDto);
			debug( serviceBean );

			serviceBean.getParam().getResourceSelection().setPath("/trinity-schemaCompiler/test2");
			serviceBean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.selectFolder);
			service.execute(ServiceId.AmRemovalRequestDetailsService.value(), serviceDto);
			debug( serviceBean );
		}

		return;
	}

	private void debug( FlowRemovalRequestDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("  ResourceSelection.type:= " + serviceBean.getParam().getResourceSelection().getType());
		System.out.println("  ResourceSelection.path:= " + serviceBean.getParam().getResourceSelection().getPath());
		System.out.println("--------------------");

		RemovalRequestDetailsViewBean view = serviceBean.getDetailsView();
		System.out.println("\nAreqViews");
		System.out.println("  LotId:" 		+ view.getLotId());
		System.out.println("  AreqId:" 		+ view.getAreqId());
		System.out.println("  PjtId:" 		+ view.getPjtId());
		System.out.println("  ReferenceId:" + view.getReferenceId());
		System.out.println("  GroupNm:" + view.getGroupNm());
		System.out.println("  SubmitterNm:" + view.getSubmitterNm());
		System.out.println("  AssigneeNm:" + view.getAssigneeNm());
		System.out.println("  Subject:" + view.getSubject());
		System.out.println("  Contents:" + view.getContents());
		System.out.println("  RemovalRequestDate:" + view.getRemovalRequestTime());
		System.out.println("  CtgNm:" + view.getCtgNm());
		System.out.println("  MstoneNm:" + view.getMstoneNm());
		System.out.println("  UpdDate:" + view.getUpdTime());
		System.out.println("  StsId:" + view.getStsId());
		System.out.println("  Status:" + view.getStatus());

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		String json = gson.toJson(serviceBean.getResourceSelectionFolderView().getFolderView());
        System.out.println("\ntoJson: " + json);

        System.out.println("\nRequestViews");
		for ( IRemovalResourceDetailsViewBean bean: serviceBean.getResourceSelectionFolderView().getRequestViews()) {
			System.out.print("  Path: " + bean.getPath() + " ");
			System.out.print("  Name: " + bean.getName() + " ");
			System.out.print("  Size: " + bean.getSize() + " ");
			System.out.print("  Count: " + bean.getCount() + " ");
			System.out.println();
		}
	}
}
