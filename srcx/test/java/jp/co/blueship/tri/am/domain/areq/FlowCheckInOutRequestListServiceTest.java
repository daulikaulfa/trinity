package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowCheckInOutRequestListServiceTest {

	public void list(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {
		String testCtgId = "1";
		//String testStsId = AmAreqStatusId.LEND_APPLY.getStatusId();
		String testStsId = AmAreqStatusId.Merged.getStatusId();
		String testMstoneId = "1";

		FlowCheckInOutRequestListServiceBean serviceBean = new FlowCheckInOutRequestListServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
			serviceBean.getParam().getSearchDraftCondition().setSelectedPageNo(2);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmCheckinOutRequestListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// onChange
		{

			{
				int testCase = 9;

				switch (testCase) {
					case 1:
						//Status + Category + Milestone Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						serviceBean.getParam().getSearchCondition().setAreqCtgCd(AreqCtgCd.ReturningRequest.value());
						break;

					case 2:
						//Category + Milestone Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 3:
						//Status + Category Search
						serviceBean.getParam().getSearchCondition().setAreqCtgCd(null);
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						break;

					case 4:
						//Target Search
						serviceBean.getParam().getSearchCondition().setAreqCtgCd(AreqCtgCd.LendingRequest.value());
						break;

					case 5:
						//Target Search
						serviceBean.getParam().getSearchCondition().setAreqCtgCd(AreqCtgCd.ReturningRequest.value());
						break;

					case 6:
						//Category Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						break;

					case 7:
						//Milestone Search
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 8:
						//Status Search
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						break;

					case 9:
						//Status Draft
						serviceBean.getParam().setSelectedDraft(true);
						break;

					default:
						break;
				}
			}

			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.AmCheckinOutRequestListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return;
	}

	private void debug( FlowCheckInOutRequestListServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("\nParam");
		System.out.println("  isSelectedDraft:= " + serviceBean.getParam().isSelectedDraft());
		System.out.println("  OrderBy( Dashboard Only ) ");
		System.out.println("    OrderBy.AccsTimestamp:= " + serviceBean.getParam().getOrderBy().getAccsTimestamp());
		System.out.println("    OrderBy.AreqId:= " + serviceBean.getParam().getOrderBy().getAreqId());
		System.out.println("    OrderBy.PjtId:= " + serviceBean.getParam().getOrderBy().getPjtId());
		System.out.println("    OrderBy.Status:= " + serviceBean.getParam().getOrderBy().getStatus());
		System.out.println("  isShowAll:= " + serviceBean.getParam().isShowAll());

		System.out.println("\nSearchCondition");
		SearchCondition condition = serviceBean.getParam().getSearchCondition();
		System.out.println("  AreqCtgCd:= " + condition.getAreqCtgCd());
		System.out.println("  StsId:= " + condition.getStsId());
		System.out.println("  CtgId:= " + condition.getCtgId());
		System.out.println("  MstoneId:= " + condition.getMstoneId());
		System.out.println("  SelectedPageNo:= " + condition.getSelectedPageNo());
		System.out.println("  Keyword:= " + condition.getKeyword());
		System.out.println("");

		System.out.println("\n  TargetViews ");
		for ( String value: condition.getTargetViews() ) {
			System.out.print("    value: " + value);
		}
		System.out.println("");

		System.out.println("\n  CategoryViews " );
		for ( ItemLabelsBean label: condition.getCtgViews() ) {
			System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

		}
		System.out.println("");

		System.out.println("\n  MstoneViews " );
		for ( ItemLabelsBean label: condition.getMstoneViews() ) {
			System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

		}
		System.out.println("");

		System.out.println("\n  StatusViews " );
		for ( ItemLabelsBean label: condition.getStatusViews() ) {
			System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

		}
		System.out.println("");
		System.out.println("");

		System.out.println("\nAreqViews");
		for ( AreqView view: serviceBean.getAreqViews()) {
			System.out.print("  AreqId: " + view.getAreqId());
			System.out.print("  AreqCtgCd: " + view.getAreqType());
			System.out.print("  PjtId: " + view.getPjtId());
			System.out.print("  PjtSubject: " + view.getPjtSubject());
			System.out.print("  ReferenceId: " + view.getReferenceId());
			System.out.print("  Subject: " + view.getSubject());
			System.out.print("  SubmitterId: " + view.getSubmitterId());
			System.out.print("  SubmitterNm: " + view.getSubmitterNm());
			System.out.print("  AssigneeId: " + view.getAssigneeId());
			System.out.print("  AssigneeNm: " + view.getAssigneeNm());
			System.out.print("  Content: " + view.getContents());
			System.out.print("  CheckoutDate: " + view.getCheckoutDate());
			System.out.print("  CheckinDate: " + view.getCheckinDate());
			System.out.print("  CheckinDueDate: " + view.getCheckinDueDate());
			System.out.print("  UpdDate: " + view.getUpdDate());
			System.out.print("  StsId: " + view.getStsId());
			System.out.print("  Status: " + view.getStatus());
			System.out.print("  CheckoutPath: " + view.getCheckoutPath());
			System.out.print("  CheckinPath: " + view.getCheckinPath());
			System.out.print("  isCheckinEnabled: " + view.isCheckinEnabled());
			System.out.println("");
		}
		System.out.println("");

		System.out.println("\nDraftAreqViews");
		for ( DraftAreqView view: serviceBean.getDraftAreqViews()) {
			System.out.print("  AreqId: " + view.getAreqId());
			System.out.print("  PjtId: " + view.getPjtId());
			System.out.print("  Subject: " + view.getSubject());
			System.out.print("  SubmitterId: " + view.getSubmitterId());
			System.out.print("  SubmitterNm: " + view.getSubmitterNm());
			System.out.print("  AssigneeId: " + view.getAssigneeId());
			System.out.print("  AssigneeNm: " + view.getAssigneeNm());
			System.out.print("  CheckoutDate: " + view.getCheckoutDate());
			System.out.print("  CheckinDueDate: " + view.getCheckinDueDate());
			System.out.print("  UpdDate: " + view.getUpdDate());
			System.out.print("  StsId: " + view.getStsId());
			System.out.print("  Status: " + view.getStatus());
		}
		System.out.println("");

		System.out.println("\nPage");
		System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
		System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
		System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
		System.out.println("  ViewRows:= " + serviceBean.getPage().getViewRows());
		System.out.println("");

		System.out.println("\nDraftPage");
		System.out.print("  MaxDraftPageNo:= " + serviceBean.getDraftPage().getMaxPageNo());
		System.out.print("  MaxDraftRows:= " + serviceBean.getDraftPage().getMaxRows());
		System.out.print("  SelectDraftPageNo:= " + serviceBean.getDraftPage().getSelectPageNo());
		System.out.print("  ViewDraftRows:= " + serviceBean.getDraftPage().getViewRows());
		System.out.println("");
		System.out.println("");
	}
}
