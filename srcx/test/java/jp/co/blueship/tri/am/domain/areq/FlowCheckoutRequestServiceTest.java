package jp.co.blueship.tri.am.domain.areq;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowCheckoutRequestServiceTest {

	public String create(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {

		// Set Param Bean
		FlowCheckoutRequestServiceBean serviceBean = new FlowCheckoutRequestServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedLotId(lotId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );
		CheckoutRequestEditInputBean inputInfo = serviceBean.getParam().getInputInfo();
		ResourceSelection selection = serviceBean.getParam().getResourceSelection();

		String id = null;

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);

			serviceBean.getParam().getResourceSelection().setPath("");
			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// onChange
		{
			ResourceSelection resource = serviceBean.getParam().getResourceSelection();
			serviceBean.getParam().setRequestType(RequestType.onChange);

			resource.setPath("/trinity-schemaCompiler");
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.openFolder);
				service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.selectFolder);
				resource.setPath("trinity-schemaCompiler/test2/");
				service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}
			{
				serviceBean.getParam().setRequestOption(RequestOption.fileUpload);
				selection.setSelectedFiles(this.getSelectedFiles(serviceBean.getResourceSelectionFolderView().getRequestViews()));
				serviceBean.getParam().getInputInfo().setCsvFileNm("checkout_resources.txt");
				serviceBean.getParam().getInputInfo().setCsvInputStreamBytes( this.readResources() );
				service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}
			{
				serviceBean.getParam().setRequestOption(RequestOption.selectResource);
				resource.setType(ResourceSelection.ResourceRequestType.selectFolder);
				selection.setSelectedFiles(this.getSelectedFiles(serviceBean.getResourceSelectionFolderView().getRequestViews()));
				resource.setPath("/trinity-schemaCompiler");
				service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
				this.debug(serviceBean);
			}
		}

		boolean isSubmitChanges = true;

		// submitChanges
		if ( isSubmitChanges ) {
			selection.setSelectedFiles(this.getSelectedFiles(serviceBean.getResourceSelectionFolderView().getRequestViews()));

			serviceBean.getParam().setRequestType(RequestType.submitChanges);

			inputInfo
				.setPjtId("PJT-1603310001")
				.setSubject("Subject")
				.setContents("Contents")
				.setGroupId("14090001")
				.setSubmitMode(SubmitMode.request)
			;

			service.execute(ServiceId.AmCheckoutRequestService.value(), serviceDto);
			this.debug(serviceBean);

			id = serviceBean.getResult().getAreqId();

		}

		return id;
	}

	private byte[] readResources() throws IOException {

		InputStream inputStream = null;
		ByteArrayOutputStream boutStream = null;

		try {
			inputStream = TestDomainSupport.class.getClassLoader().getResourceAsStream("./data/checkout_resources.txt");
			boutStream = new ByteArrayOutputStream();

			byte [] buffer = new byte[1024];
			while(true) {
				int len = inputStream.read(buffer);
				if(len < 0) {
					break;
				}
				boutStream.write(buffer, 0, len);
			}
		} finally {
			if ( null != inputStream ) {
				inputStream.close();
			}
			if (null != boutStream ) {
				boutStream.close();
			}
		}

		return boutStream.toByteArray();
	}

	private String[] getSelectedFiles( List<ICheckoutResourceViewBean> views ) {
		List<String> files = new ArrayList<String>();

		for ( ICheckoutResourceViewBean view: views ) {
			if ( ! view.isSelected() )
				continue;

			files.add( view.getPath() );
		}

		return files.toArray(new String[0]);
	}

	private void debug( FlowCheckoutRequestServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("  RequestCtg:= " + serviceBean.getParam().getRequestOption());
		System.out.println("  ResourceSelection.type:= " + serviceBean.getParam().getResourceSelection().getType());
		System.out.println("  ResourceSelection.path:= " + serviceBean.getParam().getResourceSelection().getPath());
		System.out.println("--------------------");

		{
			System.out.println("\nResourceSelectionFolderView ");
			ResourceSelectionFolderView<ICheckoutResourceViewBean> view = serviceBean.getResourceSelectionFolderView();
			System.out.println("  SelectedPath:= " + view.getSelectedPath());
			System.out.println("  ITreeFolderViewBean:= ");

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			String json = gson.toJson(view.getFolderView());
	        System.out.println(json);

			System.out.println("\nRequestViewBean ");
	        for ( ICheckoutResourceViewBean row: view.getRequestViews() ) {
	        	System.out.print("  Path:= " + row.getPath());
	        	System.out.print("  Name:= " + row.getName());
	        	if ( row.isDirectory() ) {
	        		System.out.print("  Count:= " + row.getCount());
	        	}
	        	if ( row.isFile() ) {
		        	System.out.print("  isSelected:= " + row.isSelected());
		        	System.out.print("  isLocked:= " + row.isLocked());
		        	System.out.print("  Size:= " + row.getSize());
		        	System.out.print("  SubmitterNm:= " + row.getSubmitterNm());
		        	System.out.print("  CheckoutDate:= " + row.getCheckoutDate());
		        	System.out.print("  CheckinDueDate:= " + row.getCheckinDueDate());
		        	System.out.print("  PjtId:= " + row.getPjtId());
		        	System.out.print("  AreqId:= " + row.getAreqId());
	        	}
	    		System.out.println("");
	        }
		}

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("\nRequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
			System.out.println("  AreqId: " + serviceBean.getResult().getAreqId());
		}

	}

}
