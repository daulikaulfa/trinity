package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyDetailsViewBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowChangePropertyDetailsServiceTest {

	public void details(IGenericTransactionService service, TestDomainSupport support, String pjtId) throws Exception {

		// Set Param Bean
		FlowChangePropertyDetailsServiceBean serviceBean = new FlowChangePropertyDetailsServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.setLanguage("en");

			service.execute(ServiceId.AmChangePropertyDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}

	private void debug( FlowChangePropertyDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		ChangePropertyDetailsViewBean view = serviceBean.getChangePropertyDetailsView();

		System.out.print("ChangePropertyDetailsView ");
		System.out.print("  PjtId: " + view.getPjtId());
		System.out.print("  StsId: " + view.getStsId());
		System.out.print("  Status: " + view.getStatus());
		System.out.print("  ReferenceId: " + view.getReferenceId());
		System.out.print("  ReferenceCategory: " + view.getReferenceCategoryNm());
		System.out.print("  Summary: " + view.getSummary());
		System.out.print("  Contents: " + view.getContents());
		System.out.print("  SubmitterNm: " + view.getSubmitterNm());
		System.out.print("  AssigneeNm: " + view.getAssigneeNm());
		System.out.print("  CategoryNm: " + view.getCategoryNm());
		System.out.print("  MstoneNm: " + view.getMstoneNm());
		System.out.print("  CreatedDate: " + view.getCreatedTime());
		System.out.print("  UpdDate: " + view.getUpdTime());
		System.out.print(" isClosed: " + view.isClosed());
		System.out.print(" isTestCompletionEnabled: " + view.isTestCompletionEnabled());
		System.out.println("");
	}

}
