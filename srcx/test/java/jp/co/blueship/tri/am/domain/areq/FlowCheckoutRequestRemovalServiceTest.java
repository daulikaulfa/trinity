package jp.co.blueship.tri.am.domain.areq;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowCheckoutRequestRemovalServiceTest {

	public void remove(IGenericTransactionService service, TestDomainSupport support, String areqId) throws Exception {

		// Set Param Bean
		FlowCheckoutRequestRemovalServiceBean serviceBean = new FlowCheckoutRequestRemovalServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedAreqId(areqId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );


		// RequestType = submitChages
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmCheckoutRequestRemovalService.value(), serviceDto);
			this.debug(serviceBean);

			IAreqDao dao = (IAreqDao)support.getBean( "amAreqDao" );
			AreqCondition condition = new AreqCondition();
			condition.setAreqId(areqId);
			IAreqEntity entity = dao.findByPrimaryKey(condition.getCondition());

			assertTrue( serviceBean.getResult().isCompleted() );
			assertTrue( null == entity );

		}
	}

	private void debug( FlowCheckoutRequestRemovalServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
