package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowChangePropertyListServiceTest {

	public void list(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {
		String testCtgId = "1";
		String testStsId = AmPjtStatusId.ChangePropertyClosable.getStatusId();
		String testMstoneId = "1";

		// Param
		FlowChangePropertyListServiceBean serviceBean = new FlowChangePropertyListServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangePropertyListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// onChange
		{
			SearchCondition condition = serviceBean.getParam().getSearchCondition();
			{
				int testCase = 1;

				switch (testCase) {
					case 1:
						//All
						break;

					case 2:
						//Status + Category + Milestone Search
						condition.setCtgId(testCtgId);
						condition.setMstoneId(testMstoneId);
						condition.setStsId(testStsId);
						break;

					case 3:
						//Category + Milestone Search
						condition.setCtgId(testCtgId);
						condition.setMstoneId(testMstoneId);
						break;

					case 4:
						//Status + Category Search
						condition.setCtgId(testCtgId);
						condition.setStsId(testStsId);
						break;

					case 5:
						//Status + Milestone Search
						condition.setMstoneId(testMstoneId);
						condition.setStsId(testStsId);
						break;

					case 6:
						//Category Search
						condition.setCtgId(testCtgId);
						break;

					case 7:
						//Milestone Search
						condition.setMstoneId(testMstoneId);
						break;

					case 8:
						//Status Search
						condition.setStsId(testStsId);
						break;

					default:
						break;
				}
			}

			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.AmChangePropertyListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return;
	}

	private void debug( FlowChangePropertyListServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		System.out.println("  CtgId:= " + serviceBean.getParam().getSearchCondition().getCtgId());
		System.out.println("  MstoneId:= " + serviceBean.getParam().getSearchCondition().getMstoneId());
		System.out.println("  StsId:= " + serviceBean.getParam().getSearchCondition().getStsId());

		System.out.println("Category View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getCategoryViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Mstone View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getMstoneViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Status View " );
		for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getStatusViews() ) {
			System.out.print("  label: " + label.getLabel());
			System.out.print("  value: " + label.getValue());

		}
		System.out.println("");

		System.out.println("Results");
		for ( ChangePropertyView view: serviceBean.getChangePropertyViews()) {
			System.out.print("  CloseEnabled: " + view.isCloseEnabled());
			System.out.print("  PjtId: " + view.getPjtId());
			System.out.print("  ReferenceId: " + view.getReferenceId());
			System.out.print("  ReferenceCategory: " + view.getReferenceCategoryNm());
			System.out.print("  Summary: " + view.getSummary());
			System.out.print("  SubmitterNm: " + view.getSubmitterNm());
			System.out.print("  AssigneeNm: " + view.getAssigneeNm());
			System.out.print("  Created Date: " + view.getCreatedDate());
			System.out.print("  Updated Date: " + view.getUpdDate());
			System.out.print("  Status: " + view.getStatus());
			System.out.println("");
		}

		System.out.println("PageInfo");
		System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
		System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
		System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
		System.out.print("  ViewRows:= " + serviceBean.getPage().getViewRows());
		System.out.println("");
	}

}
