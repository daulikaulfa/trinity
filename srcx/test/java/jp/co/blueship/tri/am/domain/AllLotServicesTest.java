package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domain.lot.FlowLotCreationServiceTest;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

public class AllLotServicesTest extends TestDomainSupport {

	private static final String[] DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Test-Tri-Lot-Context.xml" };

	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	private String createdId = null;

	@Test
	public void submitChanges() {

		try {
			IGenericTransactionService service = (IGenericTransactionService) this.getBean("generalService");
			((IService) service).init();

			createdId = new FlowLotCreationServiceTest().create( service, this );

			new FlowLotCloseServiceTest().close(service, this, "LOT-1604120004");
			new FlowLotEditServiceTest().edit(service, this, "LOT-1604080001");

			System.out.println("finish. createdId := " + createdId);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg : ContextAdapterFactory.getContextAdapter().getMessage((ITranslatable) e)) {
				System.out.println(msg);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
