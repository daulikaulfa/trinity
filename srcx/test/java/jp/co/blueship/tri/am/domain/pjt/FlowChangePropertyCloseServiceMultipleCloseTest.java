package jp.co.blueship.tri.am.domain.pjt;

import static org.junit.Assert.*;

import java.util.List;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceMultipleCloseBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowChangePropertyCloseServiceMultipleCloseTest {

	public void close(IGenericTransactionService service, TestDomainSupport support, String... pjtIds) throws Exception {

		// Set Param Bean
		FlowChangePropertyCloseServiceMultipleCloseBean serviceBean = new FlowChangePropertyCloseServiceMultipleCloseBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtIds(pjtIds);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmChangePropertyCloseServiceMultipleClose.value(), serviceDto);
			this.debug(serviceBean);
			assertTrue( serviceBean.getResult().isCompleted() );
		}


		// RequestType = submitChages
		if ( serviceBean.getResult().isCompleted() ) {
			serviceBean.getParam().getInputInfo().setComment("For backend test");

			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangePropertyCloseServiceMultipleClose.value(), serviceDto);
			this.debug(serviceBean);

			IPjtDao dao = (IPjtDao)support.getBean( "amPjtDao" );
			PjtCondition condition = new PjtCondition();
			condition.setPjtIds(pjtIds);
			List<IPjtEntity> pjtEntities = dao.find(condition.getCondition());

			assertTrue( serviceBean.getResult().isCompleted() );
			assertTrue( pjtEntities.size() == pjtIds.length );

			for ( IPjtEntity entity: pjtEntities ) {
				assertTrue( AmPjtStatusId.ChangePropertyClosed.equals(entity.getStsId()) );
			}

		}
	}

	private void debug( FlowChangePropertyCloseServiceMultipleCloseBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
