package jp.co.blueship.tri.am.domain;

import org.junit.Test;

import jp.co.blueship.tri.am.domain.areq.FlowCheckInOutRequestListServiceTest;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class AllCheckInOutRequestServicesTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-CheckInOutRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			String lotId = this.getLotId();

			new FlowCheckInOutRequestListServiceTest().list( service, this, lotId );
			//String areqId = "LND-1604210001";
			//areqId = new FlowCheckoutRequestServiceTest().create(service, this, lotId);
			//new FlowCheckoutRequestRemovalServiceTest().remove(service, this, areqId);
			//new FlowCheckoutRequestDetalsServiceTest().details(service, this, areqId);;
			//new FlowRemovalRequestDetalsServiceTest().details(service, this, areqId);;
			//new FlowRemovalRequestListServiceTest().list(service, this, lotId);

		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


}
