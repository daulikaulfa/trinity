package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyOverviewServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyOverviewServiceBean.ChangePropertyOverview;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowChangePropertyOverviewServiceTest {

	public void overview(IGenericTransactionService service, TestDomainSupport support, String pjtId) throws Exception {

		// Set Param Bean
		FlowChangePropertyOverviewServiceBean serviceBean = new FlowChangePropertyOverviewServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// RequestType = submitChanges
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.setLanguage("en");

			service.execute(ServiceId.AmChangePropertyOverviewService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}
	
	private void debug( FlowChangePropertyOverviewServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		ChangePropertyOverview view = serviceBean.getDetailsView();

		System.out.println("ChangePropertyOverView ");
		System.out.println("  PjtId: " + serviceBean.getParam().getSelectedPjtId());
		System.out.println("  StsId: " + view.getStsId());
		System.out.println("  Status: " + view.getStatus());
		System.out.println("  ReferenceId: " + view.getReferenceId());
		System.out.println("  ReferenceCategory: " + view.getReferenceCategoryNm());
		System.out.println("  Summary: " + view.getSummary());
		System.out.println("  SubmitterNm: " + view.getSubmitterNm());
		System.out.println("  AssigneeNm: " + view.getAssigneeNm());
		System.out.println("  CreatedDate: " + view.getCreatedTime());
		System.out.println("");
	}
}
