package jp.co.blueship.tri.am.domain.areq;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class FlowRemovalRequestListServiceTest {

	public void list(IGenericTransactionService service, TestDomainSupport support, String lotId) throws Exception {
		String testCtgId = "1";
		String testStsId = AmAreqStatusIdForExecData.RemovalRequestCancelled.getStatusId();
		String testMstoneId = "1";

		FlowRemovalRequestListServiceBean serviceBean = new FlowRemovalRequestListServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			serviceBean.setLanguage("en");

			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(2);
			serviceBean.getParam().getSearchDraftCondition().setSelectedPageNo(2);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);

		// init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.AmRemovalRequestListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		// onChange
		{
			SearchCondition draftCondition = serviceBean.getParam().getSearchDraftCondition();

			{
				int testCase = 1;

				switch (testCase) {
					case 1:
						//All
						break;

					case 2:
						//Category Search
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						break;

					case 3:
						//Milestone Search
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 4:
						//Status Search
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						break;

					case 5:
						//Draft + Category Search
						serviceBean.getParam().setSelectedDraft(true);
						draftCondition.setCtgId(testCtgId);
						break;

					case 6:
						//Draft + Milestone Search
						serviceBean.getParam().setSelectedDraft(true);
						draftCondition.setMstoneId(testMstoneId);
						break;

					case 7:
						//All Draft Search
						serviceBean.getParam().setSelectedDraft(true);
						break;

					case 8:
						//Status + Milestone Search
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						serviceBean.getParam().getSearchCondition().setMstoneId(testMstoneId);
						break;

					case 9:
						//Status + Category Search
						serviceBean.getParam().getSearchCondition().setStsId(testStsId);
						serviceBean.getParam().getSearchCondition().setCtgId(testCtgId);
						break;

					default:
						break;
				}
			}

			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.AmRemovalRequestListService.value(), serviceDto);
			this.debug(serviceBean);
		}

		return;
	}

	private void debug( FlowRemovalRequestListServiceBean serviceBean ) {
		System.out.println("================================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("================================");
		System.out.println("");

		System.out.println("--------------------------------");
		System.out.println("Request List");
		System.out.println("--------------------------------");
		System.out.println("");

		if ( ! serviceBean.getParam().isSelectedDraft() ) {
			System.out.println("Search Condition ");
			System.out.print("  StsId:= " + serviceBean.getParam().getSearchCondition().getStsId() + "   ");
			System.out.print("  CtgId:= " + serviceBean.getParam().getSearchCondition().getCtgId() + "   ");
			System.out.println("  MstoneId:= " + serviceBean.getParam().getSearchCondition().getMstoneId());

			System.out.println("\nCategoryViews " );
			for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getCtgViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			System.out.println("");

			System.out.println("\nMstoneViews " );
			for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getMstoneViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			System.out.println("");

			System.out.println("\nStatusViews " );
			for ( ItemLabelsBean label: serviceBean.getParam().getSearchCondition().getStatusViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			System.out.println("");

			System.out.println("\nRemoval Request List");
			for ( AreqView view: serviceBean.getAreqViewList()) {
				System.out.print("  AreqId: " + view.getAreqId());
				System.out.print("  PjtId: " + view.getPjtId());
				System.out.print("  PjtSubject: " + view.getPjtSubject());
				System.out.print("  ReferenceId: " + view.getReferenceId());
				System.out.print("  Subject: " + view.getSubject());
				System.out.print("  SubmitterId: " + view.getSubmitterId());
				System.out.print("  SubmitterNm: " + view.getSubmitterNm());
				System.out.print("  AssigneeId: " + view.getAssigneeId());
				System.out.print("  AssigneeNm: " + view.getAssigneeNm());
				System.out.print("  RequestDate: " + view.getRequestDate());
				System.out.print("  UpdDate: " + view.getUpdDate());
				System.out.print("  StsId: " + view.getStsId());
				System.out.print("  Status: " + view.getStatus());
				System.out.println("");
			}
			System.out.println("");

			System.out.println("\nPage List Info");
			System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
			System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
			System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
			System.out.println("  ViewRows:= " + serviceBean.getPage().getViewRows());
			System.out.println("");
		} else {
			System.out.println("--------------------------------");
			System.out.println("Draft List");
			System.out.println("--------------------------------");
			System.out.println("");
			SearchCondition draftCondition = serviceBean.getParam().getSearchDraftCondition();

			System.out.println("\nDraft Search Condition ");
			System.out.print("  Draft StsId:= " + draftCondition.getStsId() + "   ");
			System.out.print("  Draft CtgId:= " + draftCondition.getCtgId() + "   ");
			System.out.println("  Draft MstoneId:= " + draftCondition.getMstoneId());
			System.out.println("");

			System.out.println("\nDraft Category View " );
			for ( ItemLabelsBean label: draftCondition.getCtgViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			System.out.println("");

			System.out.println("\nDraft Mstone View " );
			for ( ItemLabelsBean label: draftCondition.getMstoneViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			System.out.println("");

			System.out.println("\nDraft Status View " );
			for ( ItemLabelsBean label: draftCondition.getStatusViews() ) {
				System.out.print("    [value: " + label.getValue() + " label: " + label.getLabel() + "]");

			}
			assertTrue( 0 == draftCondition.getStatusViews().size() );
			System.out.println("");

			System.out.println("\nDraft removal request list");
			for ( DraftAreqView view: serviceBean.getDraftAreqViewList()) {
				System.out.print("  AreqId: " + view.getAreqId());
				System.out.print("  PjtId: " + view.getPjtId());
				System.out.print("  Subject: " + view.getSubject());
				System.out.print("  SubmitterId: " + view.getSubmitterId());
				System.out.print("  SubmitterNm: " + view.getSubmitterNm());
				System.out.print("  AssigneeId: " + view.getAssigneeId());
				System.out.print("  AssigneeNm: " + view.getAssigneeNm());
				System.out.print("  RequestDate: " + view.getRequestDate());
				System.out.print("  UpdDate: " + view.getUpdDate());
				System.out.println("");
			}
			System.out.println("");

			System.out.println("\nPage Draft Info");
			System.out.print("  MaxDraftPageNo:= " + serviceBean.getDraftPage().getMaxPageNo());
			System.out.print("  MaxDraftRows:= " + serviceBean.getDraftPage().getMaxRows());
			System.out.print("  SelectDraftPageNo:= " + serviceBean.getDraftPage().getSelectPageNo());
			System.out.print("  ViewDraftRows:= " + serviceBean.getDraftPage().getViewRows());
			System.out.println("");
			System.out.println("");
		}
	}

}
