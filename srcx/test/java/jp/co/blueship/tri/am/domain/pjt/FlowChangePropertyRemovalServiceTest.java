package jp.co.blueship.tri.am.domain.pjt;

import static org.junit.Assert.*;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;

public class FlowChangePropertyRemovalServiceTest {

	public void remove(IGenericTransactionService service, TestDomainSupport support, String pjtId) throws Exception {

		// Set Param Bean
		FlowChangePropertyRemovalServiceBean serviceBean = new FlowChangePropertyRemovalServiceBean();
		{
			serviceBean.setUserId(support.getAuthUserId());
			serviceBean.setUserName(support.getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedPjtId(pjtId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );


		// RequestType = submitChages
		{
			serviceBean.getParam().setRequestType(RequestType.submitChanges);
			service.execute(ServiceId.AmChangePropertyRemovalService.value(), serviceDto);
			this.debug(serviceBean);

			IPjtDao dao = (IPjtDao)support.getBean( "amPjtDao" );
			PjtCondition condition = new PjtCondition();
			condition.setPjtId(pjtId);
			IPjtEntity pjtEntity = dao.findByPrimaryKey(condition.getCondition());

			assertTrue( serviceBean.getResult().isCompleted() );
			assertTrue( null == pjtEntity );

		}
	}

	private void debug( FlowChangePropertyRemovalServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		if ( RequestType.submitChanges.equals(serviceBean.getParam().getRequestType()) ) {
			System.out.println("RequestsCompletion ");
			System.out.println("  isCompleted: " + serviceBean.getResult().isCompleted());
		}
	}

}
