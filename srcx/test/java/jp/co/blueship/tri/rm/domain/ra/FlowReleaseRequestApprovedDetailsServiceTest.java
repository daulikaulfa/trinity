package jp.co.blueship.tri.rm.domain.ra;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestDetailsViewBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedDetailsServiceBean;

public class FlowReleaseRequestApprovedDetailsServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ReleaseRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void submitChanges() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();

			this.details(service);
				
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void details(IGenericTransactionService service) throws Exception {
		String raId = "RA-1605250001";
		
		// Set Param Bean
		FlowReleaseRequestApprovedDetailsServiceBean serviceBean = new FlowReleaseRequestApprovedDetailsServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );

			serviceBean.getParam().setSelectedRaId(raId);
		}

		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( serviceBean );

		// RequestType = init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.setLanguage("en");

			service.execute(ServiceId.RmReleaseRequestApprovedDetailsService.value(), serviceDto);
			this.debug(serviceBean);
		}
	}

	private void debug( FlowReleaseRequestApprovedDetailsServiceBean serviceBean ) {
		System.out.println("====================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("====================");

		ReleaseRequestDetailsViewBean view = serviceBean.getDetailsView();

		System.out.print("ReleaseRequestDetailsView ");
		System.out.print("  SubmitterNm: " + view.getSubmitterNm());
		System.out.print("  Submitter Icon Path: " + view.getSubmitterIconPath());
		System.out.print("  Submitter Group: " + view.getSubmitterGroup());
		System.out.print("  Supervisor: " + view.getSupervisor());
		System.out.print("  Supervisor Icon: " + view.getSupervisorIcon());
		System.out.print("  Environment Name: " + view.getRelEnvNm());
		System.out.print("  Preferred Date: " + view.getPreferredDate());
		System.out.print("  BpId: " + view.getBpId());
		System.out.print("  Remarks: " + view.getRemarks());
		System.out.print("  File Names: ");
		for (String fileName : view.getAttachmentFileNms()) {
			System.out.print( fileName + "  ");
		}
		System.out.print("  Ctg Name: " + view.getCtgNm());
		System.out.print("  Mstone Name: " + view.getMstoneNm());
		
		System.out.println("");
	}
}
