package jp.co.blueship.tri.rm.domain.ra;

import org.junit.Test;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.TestDomainSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.ApprovedReleaseRequestView;

public class FlowReleaseRequestApprovedListServiceTest extends TestDomainSupport {
	private static final String[] DI_CONTEXT_FILES = {"Fw-Module-Context.xml", "Test-Tri-ReleaseRequest-Context.xml"};

	@Override
	protected String[] getConfigLocations() {
		return DI_CONTEXT_FILES;
	}

	@Test
	public void init() {
		try {
			IGenericTransactionService service = (IGenericTransactionService)this.getBean( "generalService" );
			((IService)service).init();
			this.list(service);
			
		} catch (BaseBusinessException e) {
			e.printStackTrace();
			for (String msg: ContextAdapterFactory.getContextAdapter().getMessage( (ITranslatable)e ) ) {
				System.out.println( msg );
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void list (IGenericTransactionService service) throws Exception {
		String lotId = this.getLotId();
		
		FlowReleaseRequestApprovedListServiceBean serviceBean = new FlowReleaseRequestApprovedListServiceBean();
		{
			serviceBean.setUserId(getAuthUserId());
			serviceBean.setUserName(getAuthUserName());
			serviceBean.setScreenType( ScreenType.Type.next.value() );
			
			serviceBean.getParam().setSelectedLotId(lotId);
			serviceBean.getParam().getSearchCondition().setSelectedPageNo(1);
		}
		
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean(serviceBean);
		
		//init
		{
			serviceBean.getParam().setRequestType(RequestType.init);
			service.execute(ServiceId.RmReleaseRequestApprovedListService.value(), serviceDto);
			this.debug(serviceBean);
		}
		
		//onChange
		{
			serviceBean.getParam().setRequestType(RequestType.onChange);
			service.execute(ServiceId.RmReleaseRequestApprovedListService.value(), serviceDto);
			this.debug(serviceBean);
		}
		
		return;
	}
	
	private void debug (FlowReleaseRequestApprovedListServiceBean serviceBean) {
		System.out.println("================================");
		System.out.println("Request:= " + serviceBean.getParam().getRequestType());
		System.out.println("================================");
		System.out.println("");

		System.out.println("-------NORMAL LIST-------");
		System.out.println("Search Condition ");
		System.out.print("	BldEnvId:= " + serviceBean.getParam().getSearchCondition().getBldEnvId());
		System.out.print("  StsId:= " + serviceBean.getParam().getSearchCondition().getStsId() + "   ");
		System.out.print("  CtgId:= " + serviceBean.getParam().getSearchCondition().getCtgId() + "   ");
		System.out.print("  MstoneId:= " + serviceBean.getParam().getSearchCondition().getMstoneId());
		System.out.println("  Keyword " + serviceBean.getParam().getSearchCondition().getKeyword());
		System.out.println("");
		
		System.out.println("Release Environment View " );
		for ( ItemLabelsBean status: serviceBean.getParam().getSearchCondition().getReleaseEnvViews()) {
			System.out.print("  label: " + status.getLabel());
			System.out.print("  value: " + status.getValue());

		}
		System.out.println("");
		
		System.out.println("Status View " );
		for ( ItemLabelsBean status: serviceBean.getParam().getSearchCondition().getStatusViews() ) {
			System.out.print("  label: " + status.getLabel());
			System.out.print("  value: " + status.getValue());

		}
		System.out.println("");

		System.out.println("Ctg View " );
		for ( ItemLabelsBean status: serviceBean.getParam().getSearchCondition().getCtgViews()) {
			System.out.print("  label: " + status.getLabel());
			System.out.print("  value: " + status.getValue());

		}
		System.out.println("");
		
		System.out.println("Mstone View " );
		for ( ItemLabelsBean status: serviceBean.getParam().getSearchCondition().getMstoneViews()) {
			System.out.print("  label: " + status.getLabel());
			System.out.print("  value: " + status.getValue());

		}
		System.out.println("");
		
		System.out.println("Release Request List");
		for (ApprovedReleaseRequestView view : serviceBean.getReleaseRequestViews()) {
			System.out.print("	RaId: " + view.getRaId() );
			System.out.print("	BpId: " + view.getBpId() );
			System.out.print("	EnvId: " + view.getEnvId() );
			System.out.print("	EnvNm: " + view.getEnvNm() );
			System.out.print("	SubmitterNm: " + view.getSubmitterNm() );
			System.out.print("	SubmitterIconPath: " + view.getSubmitterIconPath() );
			System.out.print("	Request Date: " + view.getRequestDate() );
			System.out.print("	Status: " + view.getStatus() );
			System.out.print("	StsId: " + view.getStsId() );

			System.out.println("");
		}
		System.out.println("");
		
		System.out.println("\nPage");
		System.out.print("  MaxPageNo:= " + serviceBean.getPage().getMaxPageNo());
		System.out.print("  MaxRows:= " + serviceBean.getPage().getMaxRows());
		System.out.print("  SelectPageNo:= " + serviceBean.getPage().getSelectPageNo());
		System.out.println("  ViewRows:= " + serviceBean.getPage().getViewRows());
		System.out.println("------------------------");
		
	}
}
