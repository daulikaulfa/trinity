@echo off

rem 実行したディレクトリにログが出力されます

rem tasklist | find /n "rmiregistry"
rem start rmiregistry

rem .\lib下のjarを環境変数tmpcpへ連結する。
for /R .\lib %%a in (*.jar) do call .\catcp %%~fa

java -Xms128m -Xmx768m -Djdk.java.home="%JAVA_HOME%" -classpath .\;.\trinity_dealAsset.jar;%tmpcp% jp.co.blueship.tri.agent.dm.opt.DmAgentService >> stdout.log 2>&1

echo %tmpcp%
