#!/bin/sh

exec_start()
{
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            echo ""
            echo "[WARNING] trinity Deployment Job has already started."
            echo ""
            exit 0
        else
            rm -f .agent_pid.bs
        fi
    fi

    nohup ./boot.sh &
    sleep 5
    if [ $? -ne 0 ]
    then
        echo ""
        echo "[Error] trinity Deployment Job failed to start."
        rm -f .agent_pid.bs
        exit
    fi

    PIDSTR=`cat .agent_pid.bs`
    ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
    if [ $? -eq 0 ]
    then
        echo ""
        echo "[Info] trinity Deployment Job has started."
        exit
    else
        echo ""
        echo "[Error] trinity Deployment Job failed to start."
        rm -f .agent_pid.bs
        exit
    fi
}

exec_stop()
{
    sleep 5
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            kill -9 $PIDSTR >/dev/null
            if [ $? -eq 0 ]
            then
                echo ""
                echo "[Info] trinity Deployment Job has been stopped."
                rm -f .agent_pid.bs
                exit
            else
                echo ""
                echo "[Error]trinity Deployment Job failed to stop."
                exit
            fi
        else
            echo ""
            echo "[Error] trinity Deployment Job isnot running."
            rm -f .agent_pid.bs
            exit
        fi
    else
        echo ""
        echo "[Error] trinity Deployment Job is not running, or failed to stop."
    fi
}

func_title()
{
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
    echo /_
    echo /_  Start/Stop Script for trinity Deployment Job
    echo /_
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
}

func_start()
{
    clear
    func_title
    echo ""
    echo -n "Are you sure you want to start trinity Deployment Job? (y/n) > "
    read ANS

    case $ANS in
        y) exec_start;;
        Y) exec_start;;
        *) echo ""
           echo "[Info] Cancelled execution.";;
    esac
    exit
}

func_stop()
{
    clear
    func_title
    echo ""
    echo -n "trinity Deployment Job will be stopped. Are you sure? (y/n) > "
    read ANS

    case $ANS in
        y) exec_stop;;
        Y) exec_stop;;
        *) echo ""
           echo "[Info] Cancelled termination.";;
    esac
    exit
}

func_end()
{
    exit
}

clear

while [ 0 ]
do 
    func_title
    echo "[menu]"
    echo "  1:Start trinity Deployment Job"
    echo "  9:Stop trinity Deployment Job"
    echo "  Z:End script"
    echo ""
    echo -n "Please select from [menu]> "
    read SELECTNO

    case $SELECTNO in
        1) func_start;;
        9) func_stop;;
        Z) func_end;;
        *)
            clear
            continue
    esac
done
