#!/bin/sh
#-----------------------------

#実行したディレクトリにログが出力されます

#rmiregistry &

# ./WEB-INF/lib下のjarを環境変数tmpcpへ連結する。
tmpcp=
find ./lib -type f -name '*.jar' > ./dirList.txt
while read dir_name
do
tmpcp=$tmpcp:$dir_name
done <./dirList.txt

# XXX.XXX.XXX.XXXには、agent自身のIPアドレスまたはホスト名を設定してください。
${JAVA_HOME}/bin/java -Xms128m -Xmx768m -Djdk.java.home=${JAVA_HOME} -Djava.rmi.server.hostname=XXX.XXX.XXX.XXX -classpath ./:./trinity_dealAsset.jar:${tmpcp} jp.co.blueship.tri.agent.dm.opt.DmAgentService | pgrep -P $$ > .agent_pid.bs

unset tmpcp
