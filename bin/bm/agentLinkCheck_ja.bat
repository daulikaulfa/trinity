@echo off

rem 実行したディレクトリにログが出力されます

java -Djdk.java.home="%JAVA_HOME%" -classpath .\;..\WEB-INF\classes;..\WEB-INF\lib\*;.\lib\* jp.co.blueship.tri.agent.bm.AgentLinkCheckMain >> agentLinkCheck.log 2>&1

if %ERRORLEVEL% equ 0 echo agentLinkCheckが完了しました。
if %ERRORLEVEL% geq 1 echo agentLinkCheckの起動に失敗しました。