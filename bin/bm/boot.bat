@echo off

rem 実行したディレクトリにログが出力されます
rem
rem 【実行時引数について】
rem 　　　　第１引数：　serverId（省略不可）
rem 　　　　第２引数：　serviceId（省略不可:IBuildTaskServiceまたはIReleaseTaskService固定）


rem tasklist | find /n "rmiregistry"
start rmiregistry

rem .\lib下のjarを環境変数tmpcpへ連結する。
for /R .\lib %%a in (*.jar) do call .\catcp %%~fa

java -Xms128m -Xmx768m -Djdk.java.home="%JAVA_HOME%" -classpath .\;.\trinity_agent.jar;%tmpcp% jp.co.blueship.tri.agent.bm.BmAgentService %1 %2 >> stdout.log 2>&1

echo %tmpcp%
