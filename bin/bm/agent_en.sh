#!/bin/sh

exec_start()
{
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            echo ""
            echo "[WARNING]trinityAgent has already started."
            echo ""
            exit 0
        else
            rm -f .agent_pid.bs
        fi
    fi

    nohup ./boot.sh $1 $2 &
    sleep 5
    
    if [ $? -ne 0 ]
    then
        echo ""
        echo "[Error]trinityAgent failed to start."
        rm -f .agent_pid.bs
        exit
    fi

    PIDSTR=`cat .agent_pid.bs`
    ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
    if [ $? -eq 0 ]
    then
        echo ""
        echo "[Info]trinityAgent has started."
        exit
    else
        echo ""
        echo "[Error]trinityAgent failed to start."
        rm -f .agent_pid.bs
        exit
    fi
}

exec_stop()
{
    sleep 5
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            kill -9 $PIDSTR >/dev/null
            if [ $? -eq 0 ]
            then
                echo ""
                echo "[Info]trinityAgent has been stopped."
                rm -f .agent_pid.bs
                exit
            else
                echo ""
                echo "[Error]trinityAgent failed to stop."
                exit
            fi
        else
            echo ""
            echo "[Error]trinityAgent is not running."
            rm -f .agent_pid.bs
            exit
        fi
    else
        echo ""
        echo "[Error]trinityAgent is not running, or failed to stop."
    fi
}

func_title()
{
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
    echo /_
    echo /_  Start/Stop Script for trinityAgent
    echo /_
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
}

func_start()
{
    clear
    
    func_title
    echo ""
    echo -n "Please input Build Server ID. > "
    read BLDSRVID
    
    echo ""    
    echo "[menu]"
    echo ""  
    echo "  1:IBuildTaskService"
    echo "  9:IReleaseTaskService"
    echo ""  
    echo -n "Please select RMI Service to be used. > "
    read SELECTRMISVCNO

    case $SELECTRMISVCNO in
        1) SELECTRMISVCNO=IBuildTaskService;;
        9) SELECTRMISVCNO=IReleaseTaskService;;
        *)
            clear
            continue
    esac
        
    echo ""
    echo "Will start trinityAgent with the following IDs. Please confirm (y/n)"
    echo -n "Build Server ID "[ $BLDSRVID ]", RMI Service ID "[ $SELECTRMISVCNO ]" "
    read ANS

    case $ANS in
        y) exec_start $BLDSRVID $SELECTRMISVCNO;;
        Y) exec_start $BLDSRVID $SELECTRMISVCNO;;
        *) echo ""
           echo "[Info] Cancelled execution.";;
    esac
    exit
}

func_stop()
{
    clear
    func_title
    echo ""
    echo -n "trinityAgent will be stopped. Are you sure? (y/n) > "
    read ANS

    case $ANS in
        y) exec_stop;;
        Y) exec_stop;;
        *) echo ""
           echo "[Info] Cancelled termination.";;
    esac
    exit
}

func_end()
{
    exit
}

clear

while [ 0 ]
do 
    func_title
    echo "[menu]"
    echo "  1:Start trinityAgent"
    echo "  9:Stop trinityAgent"
    echo "  Z:End script"
    echo ""
    echo -n "Please select from [menu]> "
    read SELECTNO

    case $SELECTNO in
        1) func_start;;
        9) func_stop;;
        Z) func_end;;
        *)
            clear
            continue
    esac
done
