@echo off

rem Log outputs will be saved in the execution directory.

java -Djdk.java.home="%JAVA_HOME%" -classpath .\;..\WEB-INF\classes;..\WEB-INF\lib\*;.\lib\* jp.co.blueship.tri.agent.bm.AgentLinkCheckMain >> agentLinkCheck.log 2>&1

if %ERRORLEVEL% equ 0 echo agentLinkCheck has been completed.
if %ERRORLEVEL% geq 1 echo agentLinkCheck failed to start.