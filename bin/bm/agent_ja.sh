#!/bin/sh

exec_start()
{
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            echo ""
            echo "[WARNING]trinityAgentは既に起動されています。"
            echo ""
            exit 0
        else
            rm -f .agent_pid.bs
        fi
    fi

    nohup ./boot.sh $1 $2 &
    sleep 5
    
    if [ $? -ne 0 ]
    then
        echo ""
        echo "[Error]trinityAgentの起動に失敗しました。"
        rm -f .agent_pid.bs
        exit
    fi

    PIDSTR=`cat .agent_pid.bs`
    ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
    if [ $? -eq 0 ]
    then
        echo ""
        echo "[Info]trinityAgentを起動しました。"
        exit
    else
        echo ""
        echo "[Error]trinityAgentの起動に失敗しました。"
        rm -f .agent_pid.bs
        exit
    fi
}

exec_stop()
{
    sleep 5
    if [ -e .agent_pid.bs ]
    then
        PIDSTR=`cat .agent_pid.bs`
        ls -l /proc/$PIDSTR/cwd 2>/dev/null | grep trinity >/dev/null
        if [ $? -eq 0 ]
        then
            kill -9 $PIDSTR >/dev/null
            if [ $? -eq 0 ]
            then
                echo ""
                echo "[Info]trinityAgentを停止しました。"
                rm -f .agent_pid.bs
                exit
            else
                echo ""
                echo "[Error]trinityAgentの停止に失敗しました。"
                exit
            fi
        else
            echo ""
            echo "[Error]trinityAgentは起動されていません。"
            rm -f .agent_pid.bs
            exit
        fi
    else
        echo ""
        echo "[Error]trinityAgentが起動されていないか、停止に失敗しました。"
    fi
}

func_title()
{
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
    echo /_
    echo /_  trinityAgent起動／停止スクリプト
    echo /_
    echo /_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_/_
}

func_start()
{
    clear
    
    func_title
    echo ""
    echo -n "ビルドサーバーIDを入力してください。 > "
    read BLDSRVID
    
    echo ""    
    echo "[menu]"
    echo ""  
    echo "  1:IBuildTaskService"
    echo "  9:IReleaseTaskService"
    echo ""  
    echo -n "使用するRMIサービスを選択してください。 > "
    read SELECTRMISVCNO

    case $SELECTRMISVCNO in
        1) SELECTRMISVCNO=IBuildTaskService;;
        9) SELECTRMISVCNO=IReleaseTaskService;;
        *)
            clear
            continue
    esac
        
    echo ""
    echo "ビルドサーバーID "[ $BLDSRVID ]"、RMIサービスID "[ $SELECTRMISVCNO ]"で、"
    echo -n "trinityAgentを起動します。よろしいですか？(y/n)"
    read ANS

    case $ANS in
        y) exec_start $BLDSRVID $SELECTRMISVCNO;;
        Y) exec_start $BLDSRVID $SELECTRMISVCNO;;
        *) echo ""
           echo "[Info] 起動をキャンセルしました。";;
    esac
    exit
}

func_stop()
{
    clear
    func_title
    echo ""
    echo -n "trinityAgentを停止します。よろしいですか？(y/n) > "
    read ANS

    case $ANS in
        y) exec_stop;;
        Y) exec_stop;;
        *) echo ""
           echo "[Info] 停止をキャンセルしました。";;
    esac
    exit
}

func_end()
{
    exit
}

clear

while [ 0 ]
do 
    func_title
    echo "[menu]"
    echo "  1:trinityAgentの起動"
    echo "  9:trinityAgentの停止"
    echo "  Z:スクリプトの終了"
    echo ""
    echo -n "[menu]から選択してください。> "
    read SELECTNO

    case $SELECTNO in
        1) func_start;;
        9) func_stop;;
        Z) func_end;;
        *)
            clear
            continue
    esac
done
