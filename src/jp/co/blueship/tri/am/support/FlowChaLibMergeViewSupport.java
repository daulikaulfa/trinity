package jp.co.blueship.tri.am.support;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadLotBlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * HEAD原本マージ処理のサポートClass<br>
 *
 * <br>
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMergeViewSupport extends AmFinderSupport {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 *
	 * 変更情報に紐付く申請情報を取得する
	 *
	 * @param sort ソート条件
	 * @param pjtIds 変更情報ID
	 * @return 変更管理情報エンティティの配列
	 */
	public final IAreqEntity[] getAreqEntitiesFromPjtIds( ISqlSort sort, String... pjtIds  ) {

		AreqCondition condition = new AreqCondition();

		if ( TriStringUtils.isEmpty( pjtIds ) ) {
			//全件検索を防止するため、ダミーを設定。
			pjtIds = new String[]{ "" };
		}
		condition.setPjtIds( pjtIds );

		List<IAreqEntity> assetApplyEntities = this.getAreqDao().find(condition.getCondition(), sort);

		return assetApplyEntities.toArray(new IAreqEntity[0]);
	}

	/**
	 *
	 * ロット・ベースラインに紐付く変更情報を取得する
	 *
	 * @param lotBlDtoList ロット・ベースラインDTOのList
	 * @param sort ソート条件
	 * @return 変更情報EntitのList
	 */
	public List<IPjtEntity> getPjtEntities( List<ILotBlDto> lotBlDtoList, ISqlSort sort ) {
		Set<String> pjtIdSet = new HashSet<String>();

		for( ILotBlDto dto: lotBlDtoList ) {
			List<ILotBlReqLnkEntity> lnkEntities = dto.getLotBlReqLnkEntities();
			for( ILotBlReqLnkEntity lnkEntity : lnkEntities ) {
				pjtIdSet.add( lnkEntity.getPjtId() );
			}
		}
		String[] pjtIds = pjtIdSet.toArray( new String[ 0 ] );

		return this.findPjtEntities(sort, pjtIds);
	}

	/**
	 * 指定されたVersion Tagに該当するベースラインのListを取得します。
	 * <br>
	 * @param lotNo ロット番号
	 * @param lotVersionTag ロットチェックイン時のバージョンタグ
	 * @param order ソート順
	 * @return ベースライン情報EntityのList
	 */
	public final List<ILotBlEntity> getLotBlEntityByVersionTag(
			String lotId,
			String lotVersionTag,
			TriSortOrder order ) {

		if( TriStringUtils.isEmpty( lotId ) ){
			throw new TriSystemException( AmMessageId.AM004011F , lotId);
		}
		if( TriStringUtils.isEmpty( lotVersionTag ) ){
			throw new TriSystemException( AmMessageId.AM004012F , lotVersionTag );
		}

		LotBlCondition condition = new LotBlCondition() ;
		condition.setLotId( lotId );
		condition.setLotVerTag( lotVersionTag ) ;

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		List<ILotBlEntity> entities = this.getLotBlDao().find( condition.getCondition(), sort );

		if( TriStringUtils.isEmpty( entities ) ) {
			throw new TriSystemException( AmMessageId.AM004013F , lotVersionTag );
		}

		return entities;
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlDto> getLotBlDtoByMergeEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		List<ILotBlEntity> entities = this.getLotBlEntitiesByMergeEnable( lotId , lotCheckInTimestamp , order ) ;
		return this.findLotBlDto( entities );
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したヘッド・ベースラインDTOのList
	 */
	public List<IHeadBlEntity> getHeadBlEntitiesByMergeEnable(
			String lotId,
			TriSortOrder order ) {

		HeadBlCondition condition = new HeadBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmHeadBlStatusId.ConflictChecked.getStatusId()} );

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(HeadBlItems.mergeEndTimestamp, order, 0);
		}

		return this.getHeadBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したロッド・ベースラインDTOのList
	 */
	public List<ILotBlEntity> getLotBlEntitiesByMergeEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return this.getLotBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したロッド・ベースラインDTOのList
	 */
	public List<ILotBlEntity> getLotBlEntitiesByMergeEnable(
			IHeadBlEntity headBlEntity,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		// 指定されたヘッドベースラインより、該当するロットベースライン一覧を取得
		HeadLotBlLnkCondition headLotBlCondition = new HeadLotBlLnkCondition();
		headLotBlCondition.setHeadBlId( headBlEntity.getHeadBlId() );
		List<IHeadLotBlLnkEntity> headLotBlEntities = this.getHeadLotBlLnkDao().find(headLotBlCondition.getCondition());
		List<String> lotBlIds = new ArrayList<String>();
		for ( IHeadLotBlLnkEntity headLotBlEntity : headLotBlEntities ) {
			lotBlIds.add( headLotBlEntity.getLotBlId() );
		}

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotBlIds( lotBlIds.toArray( new String[0]) ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return this.getLotBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ可能なベースラインの先頭１件を取得します
	 * <br>
	 * @param lotNo ロット番号
	 * @return ビルドパッケージビルド情報レコードの配列
	 */
	public final ILotBlEntity getLotBlEntityByMergeEnableAtFirst( String lotId ) {

		ILotBlEntity lotBlEntity = null;
		// マージチェック済みのヘッドベースラインを取得
		List<IHeadBlEntity> headBlEntity = this.getHeadBlEntitiesByMergeEnable( lotId, TriSortOrder.Desc );
		if( null!=headBlEntity && 0<headBlEntity.size() ) {
			// あればロットベースライン情報を取得
			lotBlEntity = this.getLotBlEntitiesByMergeEnable( headBlEntity.get(0), null, TriSortOrder.Desc ).get(0);
		}

		return lotBlEntity;
	}

	/**
	 * マージ済みの変更情報をカウントする
	 * @param lotId ロット番号
	 * @return 完了の変更情報の件数
	 */
	public int countMergedByPjt( String lotId ) {
		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getMergedCountPjtCondition( lotId );

		return this.getPjtDao().count( condition.getCondition() );
	}

	/**
	 * 未クローズの変更情報をカウントする
	 * @param lotId ロット番号
	 * @return クローズされていない変更情報の件数
	 */
	public int countAliveByPjt( String lotId ) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getAliveCountPjtCondition( lotId );

		return this.getPjtDao().count( condition.getCondition() );

	}

	/**
	 * ロット詳細情報（画面表示）を取得します。
	 * @param lotDto ロットDTO
	 * @return ロット詳細情報
	 */
	public LotDetailViewBean getLotDetailViewBean( IGeneralServiceBean paramBean, ILotDto lotDto ) {

		LotDetailViewBean lotDetailViewBean = new LotDetailViewBean();
		IBldSrvEntity srvEntity = this.getBmFinderSupport().findBldSrvEntityByController();

		AmViewInfoAddonUtils.setLotDetailViewBeanPjtLotEntity(paramBean,lotDetailViewBean, lotDto, srvEntity.getBldSrvNm(), srvEntity.getBldSrvNm() );

		return lotDetailViewBean;
	}

	/**
	 * /全ビルド（全ロットで）の中でマージエラーのビルドパッケージエンティティを取得する。
	 *
	 * @return ビルドパッケージエンティティの配列
	 */
	public ILotBlEntity[] getLotBlEntitiesByMergeError() {
		LotBlCondition lotBlCondition = new LotBlCondition();

		lotBlCondition.setStsIds( new String[]{ AmLotBlStatusId.CommittedToLot.getStatusId() } );
		lotBlCondition.setProcStsIds( new String[]{ AmLotBlStatusIdForExecData.MergeError.getStatusId() } );

		ISqlSort sort = new SortBuilder();
		sort.setElement(LotBlItems.lotId, TriSortOrder.Asc, 0);
		sort.setElement(LotBlItems.lotChkinTimestamp, TriSortOrder.Desc, 1);

		List<ILotBlEntity> lotBlEntities = this.getLotBlDao().find(lotBlCondition.getCondition(), sort);
		ILotBlEntity[] lotBlEntityArray =  lotBlEntities.toArray(new ILotBlEntity[0]);

		return lotBlEntityArray;
	}

	/**
	 * ビルドエンティティから変更管理画面用ビーンを作成します。
	 * @param lotBlDtoList ビルドエンティティ
	 * @return 変更管理画面用ビーンのリスト
	 */
	public List<LotBaselineViewBean> getLotBaselineViewBean( List<ILotBlDto> lotBlDtoList ) {

		List<LotBaselineViewBean> lotBlViewBeanList = new ArrayList<LotBaselineViewBean>();

		AmViewInfoAddonUtils.setBuildViewBeanBuildEntity( lotBlViewBeanList, lotBlDtoList );
		// BlTag,VerTagは基本的に同じはずなので以下のチェックはコメントアウト
/*		String prevBaseLine = null;

		for( LotBaselineViewBean lotBlViewBean : lotBlViewBeanList ) {
			StringBuilder baseLine = new StringBuilder();

			baseLine.append( ( null == lotBlViewBean.getBaselineTag() )? "": lotBlViewBean.getBaselineTag() );
			baseLine.append( "(" );
			baseLine.append( ( null == lotBlViewBean.getVersionTag() )? "": lotBlViewBean.getVersionTag() );
			baseLine.append( ")" );

			//前レコードと同一のベースラインタグ＆バージョンタグの場合、表示しなくするため消す
			if( null != prevBaseLine ) {
				if ( baseLine.toString().equals( prevBaseLine ) ) {
					lotBlViewBean.setBaselineTag( null ) ;
					lotBlViewBean.setVersionTag( null ) ;
				}
			}
			prevBaseLine = baseLine.toString() ;
		}
*/
		return lotBlViewBeanList;
	}
	/**
	 * ビルドエンティティから変更管理画面用ビーンを作成します。
	 * (複数ベースライン処理対応)
	 * @param lotBlDtoList ビルドエンティティ
	 * @return 変更管理画面用ビーンのリスト
	 */
	public List<LotBaselineViewGroupByVersionTag> getLotBaselineGroupViewBean( List<ILotBlDto> lotBlDtoList ) {

		List<LotBaselineViewGroupByVersionTag> lotBlViewBeanList = new ArrayList<LotBaselineViewGroupByVersionTag>();

		for ( ILotBlDto lotBlDto : lotBlDtoList ) {
			LotBaselineViewGroupByVersionTag viewBeanGroup = new LotBaselineViewGroupByVersionTag();
			LotBaselineViewBean viewBean = AmViewInfoAddonUtils.setBuildViewBeanBuildEntity( lotBlDto.getLotBlEntity() );
			viewBeanGroup.getBuildInfoList().add( viewBean );
			viewBeanGroup.setBaselineTag( lotBlDto.getLotBlEntity().getLotBlTag() );
			viewBeanGroup.setVersionTag( lotBlDto.getLotBlEntity().getLotVerTag() );
			lotBlViewBeanList.add( viewBeanGroup );
		}

		String prevBaseLine = null;

		for( LotBaselineViewGroupByVersionTag lotBlViewBean : lotBlViewBeanList ) {
			StringBuilder baseLine = new StringBuilder();

			baseLine.append( ( null == lotBlViewBean.getBaselineTag() )? "": lotBlViewBean.getBaselineTag() );
			baseLine.append( "(" );
			baseLine.append( ( null == lotBlViewBean.getVersionTag() )? "": lotBlViewBean.getVersionTag() );
			baseLine.append( ")" );

			//前レコードと同一のベースラインタグ＆バージョンタグの場合、表示しなくするため消す
			if( null != prevBaseLine ) {
				if ( baseLine.toString().equals( prevBaseLine ) ) {
					lotBlViewBean.setBaselineTag( null ) ;
					lotBlViewBean.setVersionTag( null ) ;
				}
			}
			prevBaseLine = baseLine.toString() ;
		}
		return lotBlViewBeanList;
	}
	/**
	 * ビルドパッケージDtoから変更管理画面用ビーンを作成します。
	 * @param entities ビルドパッケージDto
	 * @return 変更管理画面用ビーンのリスト
	 */
	public List<LotBaselineViewGroupByVersionTag> getLotBaselineViewGroupByVersionTag( List<ILotBlDto> lotBlDtoList ) {

		List<ILotBlEntity> lotBlEntities = new ArrayList<ILotBlEntity>() ;
		for ( ILotBlDto entity : lotBlDtoList ) {
			lotBlEntities.add(entity.getLotBlEntity());
		}

		return getLotBaselineViewGroupByVersionTag( lotBlEntities.toArray(new ILotBlEntity[0]) );
	}

	/**
	 * ベースライン一覧情報を変更管理情報エンティティから設定する
	 * @param baselineViewBeanList ベースライン一覧情報
	 * @param entitys ベースライン一覧情報のエンティティ
	 */
	public List<LotBaselineViewGroupByVersionTag> getLotBaselineViewGroupByVersionTag( ILotBlEntity[] entityList ) {

		List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = new ArrayList<LotBaselineViewGroupByVersionTag>();

		//クローズ日時の降順ソート
		Arrays.sort( entityList,
				new Comparator<ILotBlEntity>() {
					public int compare( ILotBlEntity o1, ILotBlEntity o2 ) {
						ILotBlEntity src1 = (ILotBlEntity)o1;
						ILotBlEntity src2 = (ILotBlEntity)o2;

						return src2.getLotChkinTimestamp().compareTo( src1.getLotChkinTimestamp() );
					}
				} );

		LotBaselineViewGroupByVersionTag viewBean = null ;

		for ( ILotBlEntity entity : entityList ) {

			if( null == viewBean ||
				true != viewBean.getVersionTag().equals( entity.getLotVerTag() ) ) {

				if( null != viewBean ) {
					baselineViewBeanList.add( viewBean );
				}
				viewBean = new LotBaselineViewGroupByVersionTag();
				viewBean.setBaselineTag( entity.getLotBlTag() ) ;
				viewBean.setVersionTag( entity.getLotVerTag() ) ;
				viewBean.setCheckInDate( TriDateUtils.convertViewDateFormat( entity.getLotChkinTimestamp() ) );
				viewBean.getBuildInfoList().add( AmViewInfoAddonUtils.setBuildViewBeanBuildEntity( entity ) ) ;

				// 前回のマージチェック・マージ結果を取得する為に最新のヘッドベースライン情報を取得する
				List<IHeadBlEntity> headBlEntities = this.getHeadBaselineList( entity );
				if( 0<headBlEntities.size() ) {
					IHeadBlEntity headBlEntity = headBlEntities.get(0); // 最新のヘッドベースライン
					if( headBlEntity.getStsId().equals( AmHeadBlStatusId.ConflictChecked.getStatusId() ) ) {
						viewBean.setMergeCheckStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getStsId() ) ) ;
						viewBean.setMergeCheckStatusId( AmHeadBlStatusId.value( headBlEntity.getStsId() ).name() );
					} else {
						if( headBlEntity.getStsId().equals( AmHeadBlStatusId.Merged.getStatusId() ) ) {
							viewBean.setMergeStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getStsId() ) ) ;
							viewBean.setMergeStatusId( AmHeadBlStatusId.value( headBlEntity.getStsId() ).name() );
						} else {
							viewBean.setMergeCheckStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getProcStsId() ) ) ;
							viewBean.setMergeCheckStatusId( AmHeadBlStatusIdForExecData.value( headBlEntity.getProcStsId() ).name() );
							viewBean.setMergeStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getProcStsId() ) ) ;
							viewBean.setMergeStatusId( AmHeadBlStatusIdForExecData.value( headBlEntity.getProcStsId() ).name() );
						}
					}
					viewBean.setMergeCheckStartDate( TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkStTimestamp() ) ) ;
					viewBean.setMergeCheckEndDate( TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkEndTimestamp() ) ) ;
					viewBean.setMergeStartDate( TriDateUtils.convertViewDateFormat( headBlEntity.getMergeStTimestamp() ) ) ;
					viewBean.setMergeEndDate( TriDateUtils.convertViewDateFormat( headBlEntity.getMergeEndTimestamp() ) ) ;
				}
				/*
				viewBean.setMergeStatus( sheet.getValue( RmDesignBeanId.statusId ,entity.getProcStsId() ) ) ;
				viewBean.setMergeStartDate(  TriDateUtils.convertViewDateFormat( entity.getMergeStTimestamp() ) ) ;
				viewBean.setMergeEndDate(  TriDateUtils.convertViewDateFormat( entity.getMergeEndTimestamp() ) ) ;*/
			} else {//同じタイミングでパッケージクローズされた
				viewBean.getBuildInfoList().add( AmViewInfoAddonUtils.setBuildViewBeanBuildEntity( entity ) ) ;
			}
		}
		if( null != viewBean ) {
			baselineViewBeanList.add( viewBean );
		}

		return baselineViewBeanList;
	}

	/**
	 * 引数で与えられた、マージ済みのベースラインとなるHeadBlEntityに含まれて、一緒にマージされた<br>
	 * LotBlエンティティのリストを取得する<br>
	 * @param lotId ロット番号
	 * @param headBlEntity ベースラインのビルドエンティティ
	 * @param isOrderByASC ソート方向
	 * <pre>
	 * 		true	:	クローズ日時の昇順
	 * 		false	:	クローズ日時の降順
	 * </pre>
	 * @return ベースラインと一緒にマージされたＲＵLotBlエンティティ一覧
	 */
	public List<ILotBlDto> getLotBlDtoByMergedBaseline( String lotId , IHeadBlEntity headBlEntity , boolean isOrderByASC ) {

		// HeadBlに紐ずくLotBlId一覧取得
		HeadLotBlLnkCondition condition = new HeadLotBlLnkCondition();
		condition.setHeadBlId( headBlEntity.getHeadBlId() );
		List<IHeadLotBlLnkEntity> headLotBlEntities = this.getHeadLotBlLnkDao().find( condition.getCondition() );

		// LotBlId一覧抽出
		List<String> lotBlIds = new ArrayList<String>();
		for ( IHeadLotBlLnkEntity headLotBlEntity : headLotBlEntities) {
			lotBlIds.add( headLotBlEntity.getLotBlId() );
		}

		// LotBlId一覧からLotBl情報取得
		LotBlCondition lotBlCondition = new LotBlCondition();
		lotBlCondition.setLotBlIds( lotBlIds.toArray( new String[0]) );
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotBlItems.lotChkinTimestamp, isOrderByASC ? TriSortOrder.Asc : TriSortOrder.Desc, 0);
		List<ILotBlEntity> lotBlEntities = this.getLotBlDao().find( lotBlCondition.getCondition(), sort );
		List<ILotBlDto> lotBlDtos = this.findLotBlDto(lotBlEntities, AmTables.AM_LOT_BL_MDL_LNK, AmTables.AM_LOT_BL_REQ_LNK );

		return lotBlDtos;
	}

	/**
	 * コンフリクトチェック可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>チェックイン済
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのビルドパッケージビルドが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlDto> getLotBlDtoByMergeCheckEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		List<ILotBlEntity> entities = this.getLotBlEntitiesByMergeCheckEnable( lotId , lotCheckInTimestamp , order ) ;
		return this.findLotBlDto( entities );
	}

	/**
	 * コンフリクトチェック可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>チェックイン済
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのビルドパッケージビルドが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlEntity> getLotBlEntitiesByMergeCheckEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return this.getLotBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ済みまたはマージエラーのビルドエンティティのリストを取得する<br>
	 * @param lotId ロット番号
	 * @param isOrderByASC ソート方向
	 * <pre>
	 * 		true	:	マージ終了日時・クローズ日時の昇順
	 * 		false	:	マージ終了日時・クローズ日時の降順
	 * </pre>
	 * @return ベースライン以前のＲＵビルドエンティティ
	 * @deprecated
	 */
	public List<ILotBlEntity> getMergedBuildEntity( String lotId , boolean isOrderByASC ) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsId(AmLotBlStatusId.CommittedToLot.getStatusId());	//RUクローズ済み
		condition.setProcStsIds( new String[]{
				AmLotBlStatusId.Merged.getStatusId() ,
				AmLotBlStatusIdForExecData.MergeError.getStatusId() ,
				/*RelStatusId.PJT_MERGE_ACTIVE*/ } ) ;

		//int viewRows = DesignProjectUtil.getIntValue( DesignProjectChangecDefineId.maxPageNumberByTopMenu ) ;
		ISqlSort sort = new SortBuilder();
		sort.setElement(LotBlItems.updTimestamp, isOrderByASC ? TriSortOrder.Asc : TriSortOrder.Desc, 0);
		sort.setElement(LotBlItems.lotChkinTimestamp, isOrderByASC ? TriSortOrder.Asc : TriSortOrder.Desc, 1);

		List<ILotBlEntity> lotBlEntities = this.getLotBlDao().find(condition.getCondition(), sort);

		return lotBlEntities ;
	}

	/**
	 * LotBl情報より、HeadBl情報を取得する
	 * @param lotBlEntity ロットベースラインEntity
	 * @return ヘッドベースライン情報レコードの配列
	 */
	public List<IHeadBlEntity> getHeadBaselineList( ILotBlEntity lotBlEntity ) {

		List<IHeadBlEntity> headBlEntities = new ArrayList<IHeadBlEntity>();
		// HeadLotBlLnkテーブルよりLotBlId一覧を取得
		HeadLotBlLnkCondition condition = new HeadLotBlLnkCondition();
		condition.setLotBlId( lotBlEntity.getLotBlId() );
		List<IHeadLotBlLnkEntity> headLotBlEntities = this.getHeadLotBlLnkDao().find(condition.getCondition());
		List<String> headBlIds = new ArrayList<String>();
		for ( IHeadLotBlLnkEntity headLotBlEntity : headLotBlEntities ) {
			headBlIds.add( headLotBlEntity.getHeadBlId() );
		}

		if( 0<headBlIds.size() ) {
			// LotBl情報一覧を取得
			HeadBlCondition headBlCondition = new HeadBlCondition();
			headBlCondition.setHeadBlIds( headBlIds.toArray( new String[0] ) );
			ISqlSort sort = new SortBuilder();
			sort.setElement(HeadBlItems.mergeEndTimestamp, TriSortOrder.Desc, 0);
			sort.setElement(HeadBlItems.mergechkEndTimestamp, TriSortOrder.Desc, 1);
			headBlEntities = this.getHeadBlDao().find( headBlCondition.getCondition(), sort );
		}
		return headBlEntities;
	}
	/**
	 * LotBl情報一覧より、HeadBl情報を取得する
	 * @param lotBlEntity ロットベースラインEntity
	 * @return ヘッドベースライン情報レコードの配列
	 */
	public List<IHeadBlEntity> getHeadBaselineList( List<ILotBlEntity> lotBlEntities ) {

		List<IHeadBlEntity> headBlEntities = new ArrayList<IHeadBlEntity>();
		for ( ILotBlEntity lotBlEntity : lotBlEntities ) {
			// HeadLotBlLnkテーブルよりLotBlId一覧を取得
			HeadLotBlLnkCondition condition = new HeadLotBlLnkCondition();
			condition.setLotBlId( lotBlEntity.getLotBlId() );
			List<IHeadLotBlLnkEntity> headLotBlEntities = this.getHeadLotBlLnkDao().find(condition.getCondition());
			if( 0==headLotBlEntities.size() ) {
				continue;
			}
			List<String> headBlIds = new ArrayList<String>();
			for ( IHeadLotBlLnkEntity headLotBlEntity : headLotBlEntities ) {
				headBlIds.add( headLotBlEntity.getHeadBlId() );
			}
			if( 0<headBlIds.size() ) {
				// LotBl情報一覧を取得
				HeadBlCondition headBlCondition = new HeadBlCondition();
				headBlCondition.setHeadBlIds( headBlIds.toArray( new String[0] ) );
				ISqlSort sort = new SortBuilder();
				sort.setElement(HeadBlItems.mergeEndTimestamp, TriSortOrder.Desc, 0);
				sort.setElement(HeadBlItems.mergechkEndTimestamp, TriSortOrder.Desc, 1);
				headBlEntities = this.getHeadBlDao().find( headBlCondition.getCondition(), sort );
			}
			break;
		}
		return headBlEntities;
	}

	/**
	 * マージ済みの変更情報をカウントする
	 * @param lotId ロット番号
	 * @return 完了の変更情報の件数
	 */
	public int countNoOfMerges( String lotId ) {
		HeadBlCondition condition = new HeadBlCondition();
		String[] mergeStsIds = new String[] { AmHeadBlStatusId.Merged.getStatusId() };
		condition.setLotId( lotId );
		condition.setStsIds( mergeStsIds );

		return this.getHeadBlDao().count( condition.getCondition() );
	}
}
