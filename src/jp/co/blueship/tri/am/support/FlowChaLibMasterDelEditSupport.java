package jp.co.blueship.tri.am.support;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.constants.ChaLibScreenItemID;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;

/**
 * 削除申請系イベントのサポートClass <br>
 * <p>
 * 削除申請／削除編集を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChaLibMasterDelEditSupport extends AmFinderSupport /* extends FinderSupport */ {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 表示に一貫性を持たせるため、ファイルセパレータを"/"に変換します。
	 *
	 * @param src 対象となるディレクトリパス
	 * @return 変換後のメッセージを戻します。
	 */
	public static final String convertPath(String src) {
		String relativePath = TriStringUtils.convertPath(src);

		if (relativePath.startsWith("/"))
			relativePath = StringUtils.substringAfter(relativePath, "/");

		return relativePath;
	}

	/**
	 * 選択可能なロットを取得します。
	 *
	 * @return
	 */

	public final List<LotViewBean> getLotViewBeanForPullDown(GenericServiceBean retBean) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		LotCondition lotCondition = AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		ISqlSort lotSort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibTop();
		List<ILotEntity> lotList = this.getLotDao().find(lotCondition.getCondition(), lotSort);
		List<ILotEntity> lotEntityList = lotList;
		List<ILotDto> lotDto = this.findLotDto(lotEntityList);

		if (TriCollectionUtils.isEmpty(lotEntityList)) {
			retBean.setInfoMessage( AmMessageId.AM001007E );
		}

		AmViewInfoAddonUtils.populateLotViewBeanPjtLotEntityForPullDown(//
				lotViewBeanList,//
				lotDto,//
				this.getUmFinderSupport().findGroupByUserId(retBean.getUserId()),
				this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

		if (TriCollectionUtils.isNotEmpty(lotEntityList) && TriCollectionUtils.isEmpty(lotViewBeanList)) {
			retBean.setInfoMessage(AmMessageId.AM001122E);
		}

		return lotViewBeanList;
	}

	/**
	 * 選択可能な変更管理を取得します。
	 *
	 * @return
	 */
	public final List<PjtViewBean> getSelectPjtViewList() {
		List<IPjtEntity> pjtList = this.getPjtDao().find(AmDBSearchConditionAddonUtils.getPjtSelectCondition().getCondition(),
				AmDBSearchSortAddonUtils.getPjtSelectSortFromDesignDefine());

		IPjtEntity[] pjtEntitys = pjtList.toArray(new IPjtEntity[0]);

		return AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( pjtEntitys );
	}

	/**
	 * ロットを指定して選択可能な変更管理を取得します。
	 *
	 * @return
	 */
	public final List<PjtViewBean> getSelectPjtViewList(String lotId) {
		PjtCondition condition = new PjtCondition();
		condition = AmDBSearchConditionAddonUtils.getPjtSelectCondition();
		condition.setLotId(lotId);
		List<IPjtEntity> pjtList = this.getPjtDao().find(condition.getCondition(),
				AmDBSearchSortAddonUtils.getPjtSelectSortFromDesignDefine());

		IPjtEntity[] pjtEntitys = pjtList.toArray(new IPjtEntity[0]);

		return AmViewInfoAddonUtils.setPjtViewBeanPjtEntity(pjtEntitys);
	}

	/**
	 * 申請情報の配列から、変更管理番号を取得し、配列で返す<br>
	 *
	 * @param assetApplyEntityArray 申請情報の配列
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNoArrayFromAssetApplyArray(IAreqEntity[] assetApplyEntityArray) {
		List<String> pjtNoList = new ArrayList<String>();
		for (IAreqEntity assetApplyEntity : assetApplyEntityArray) {
			pjtNoList.add(assetApplyEntity.getPjtId());
		}
		return pjtNoList.toArray(new String[0]);
	}

	/**
	 * 変更管理番号の配列から、変更管理情報を取得し、変更管理番号をキーとしたMapで返す<br>
	 *
	 * @param pjtNoArray 変更管理番号の配列
	 * @return 変更管理番号をキーとしたMap
	 */
	public Map<String, IPjtEntity> getPjtEntityPjtNoMap(String[] pjtNoArray) {
		PjtCondition condition = new PjtCondition();

		String[] conditionPjtNo = pjtNoArray;

		if (TriStringUtils.isEmpty(conditionPjtNo)) {
			// ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[] { "" };
		}

		condition.setPjtIds(conditionPjtNo);

		List<IPjtEntity> list = this.getPjtDao().find(condition.getCondition());

		IPjtEntity[] pjtEntityArray = list.toArray(new IPjtEntity[0]);
		Map<String, IPjtEntity> lhMap = new LinkedHashMap<String, IPjtEntity>();
		for (IPjtEntity pjtEntity : pjtEntityArray) {
			lhMap.put(pjtEntity.getPjtId(), pjtEntity);
		}
		return lhMap;
	}

	/**
	 * 選択可能なグループ情報を取得します。 <br>
	 * ユーザの所属するグループが対象になります。
	 *
	 * @param lotId
	 * @param userId
	 * @return
	 */
	public final List<String> getSelectGroupNameList(String lotId, String userId) {

		List<String> userGroupList = new ArrayList<String>();
		ILotDto lotDto = this.findLotDto( lotId, AmTables.AM_LOT_GRP_LNK );

		List<IGrpEntity> grpEntities = this.getUmFinderSupport().findGroupByUserId(userId);

		AmViewInfoAddonUtils.getSelectGroupNameList(userGroupList, lotDto, grpEntities);

		return userGroupList;
	}

	public List<Object> setParamList(ResourceSelectionFolderView<IRemovalResourceViewBean> folderView,
			ResourceSelection resourceSelection, String selectedLotId, String selectedAreqId) {
		List<Object> paramList = new ArrayList<Object>();

		ILotDto lotDto = this.findLotDto(selectedLotId, AmTables.AM_LOT_MDL_LNK);
		List<IAreqDto> areqDtoList = new ArrayList<IAreqDto>();

		AreqCondition condition = AmResourceSelectionUtils.setAreqConditionForLockedResource(selectedLotId);

		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> assetAreqEntities =
				this.getAreqDao().find( condition.getCondition(), sort );
		for (IAreqEntity areqEntity: assetAreqEntities) {
			if (areqEntity.getAreqId().equals(selectedAreqId)) {
				continue;
			}
			IAreqDto areqDto = this.findAreqDto(areqEntity, new AmTables[]{AmTables.AM_AREQ_FILE, AmTables.AM_AREQ_BINARY_FILE});
			areqDtoList.add(areqDto);
		}

		paramList.add(lotDto);
		paramList.add(areqDtoList.toArray(new IAreqDto[0]));
		paramList.add(resourceSelection);
		paramList.add(folderView);

		return paramList;
	}

	public static final void saveDelApplyDefUploadFile(RemovalRequestEditInputBean inputBean, String userId) {
		if (null == inputBean.getCsvInputStreamBytes()) {
			return;
		}
		BusinessFileUtils.saveAppendFile(
				TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
						sheet.getValue(AmDesignEntryKeyByChangec.delApplyDefUploadRelativePath)), userId,
				StreamUtils.convertBytesToInputStreamToBytes(inputBean.getCsvInputStreamBytes()), inputBean.getCsvFileNm());

		String filePath = new File(TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
				sheet.getValue(AmDesignEntryKeyByChangec.delApplyDefUploadRelativePath)), TriStringUtils.linkPath(userId,
						inputBean.getCsvFileNm())).getPath();
		if (log.isDebugEnabled()) {
			LogHandler.debug(log, "★★★:filePath:=" + filePath);
		}
		if (new File(filePath).exists()) {
			inputBean.setCsvInputStreamBytes(null);
			inputBean.setCsvFilePath(filePath);
		}
	}

	public final void setRemovalRequestEntity(IGeneralServiceBean serviceBean,
			RemovalRequestEditInputBean inputBean, IAreqEntity entity, String statusId) {

		entity.setAreqCtgCd			( AreqCtgCd.RemovalRequest.value() );
		entity.setSummary			( inputBean.getSubject() );
		entity.setContent			( inputBean.getContents() );
		entity.setStsId				( statusId );
		entity.setAssigneeId(inputBean.getAssigneeId());

		if (TriStringUtils.isNotEmpty(inputBean.getAssigneeId())) {
			entity.setAssigneeNm(this.getUmFinderSupport().findUserByUserId(inputBean.getAssigneeId()).getUserNm());
		} else {
			entity.setAssigneeNm("");
		}
		entity.setDelStsId			( StatusFlg.off);
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( serviceBean.getUserName() );
		entity.setUpdUserId			( serviceBean.getUserId() );

		if (TriStringUtils.isEmpty(entity.getAreqId())) {
			entity.setDelReqUserNm		( serviceBean.getUserName() );
			entity.setDelReqUserId		( serviceBean.getUserId() );
			entity.setDelReqTimestamp	( TriDateUtils.getSystemTimestamp() );
			entity.setPjtId				( inputBean.getPjtId() );
			entity.setGrpId				( inputBean.getGroupId() );
			if (TriStringUtils.isNotEmpty(inputBean.getGroupId())) {
				entity.setGrpNm			( this.getUmFinderSupport().findGroupById(inputBean.getGroupId()).getGrpNm() );
			}

		}
	}

	public static IDelApplyBinaryFileResult[] getBinaryDelFiles(List<Object> paramList) {
		try {
			File binaryFile = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
					sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile ) );
			if (!binaryFile.exists()) {
				return new IDelApplyBinaryFileResult[] {};
			}
			FileInputStream fisBin = null ;
			InputStreamReader isrBin = null ;
			String[] binaryFiles = null ;
			try {
				fisBin = new FileInputStream( binaryFile ) ;
				isrBin = new InputStreamReader( fisBin ) ;
				binaryFiles = TriFileUtils.readFileLine( isrBin, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isrBin);
				TriFileUtils.close(fisBin);
			}


			List<IDelApplyBinaryFileResult> binaryParsePath = new ArrayList<IDelApplyBinaryFileResult>();
			for ( String binaryPath : binaryFiles ) {

				// Binary file result
				String binaryRelativePath = FlowChaLibMasterDelEditSupport.convertPath( binaryPath );
				IDelApplyBinaryFileResult result = new DelApplyBinaryFileResult();
				result.setFilePath( binaryRelativePath );
				binaryParsePath.add( result );
			}

			return binaryParsePath.toArray( new IDelApplyBinaryFileResult[] {} );
		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		}
	}

	public static IDelApplyFileResult[] getDelFiles(Set<String> selectedFileSet) {
		List<IDelApplyFileResult> parsePath = new ArrayList<IDelApplyFileResult>();

		for (String selectedFile : selectedFileSet) {
			String relativePath = FlowChaLibMasterDelEditSupport.convertPath( selectedFile );
			IDelApplyFileResult result = new DelApplyFileResult();
			result.setFilePath( relativePath );
			parsePath.add(result);

		}
		return parsePath.toArray(new IDelApplyFileResult[]{});
	}

	/**
	 * 削除申請レコードに、申請ファイル情報を設定します。
	 * @param paramList
	 * @param statusMap
	 * @return
	 * @throws ScmException
	 * @throws ScmSysException
	 */
	public final IAreqDto setAssetFileEntity(
									List<Object> paramList,
									Map<String,SVNStatus> statusMap,
									IAreqDto areqDto
									 ) throws IOException , ScmException, ScmSysException {

		IAreqEntity areqEntity = areqDto.getAreqEntity();
		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( paramList );

		if ( null != files ) {

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			int fileSeq = 0;
			for ( IFileResult path : files ) {
				File file = new File( masterPath, path.getFilePath() );
				if ( !file.isFile() ){
					throw new TriSystemException( AmMessageId.AM004000F , path.getFilePath());
				}

				IAreqFileEntity fileEntity = new AreqFileEntity();
				fileEntity.setAreqId( areqEntity.getAreqId() );//
				fileEntity.setAreqCtgCd( areqEntity.getAreqCtgCd() );//
				fileEntity.setAreqFileSeqNo( this.getAreqFileNumberingDao().nextval( ++fileSeq ) );
				fileEntity.setFilePath( TriStringUtils.convertPath( path.getFilePath() ) ) ;//
				fileEntity.setFileByteSize( new Long(file.length()).intValue() );
				fileEntity.setFileUpdTimestamp( new Timestamp( file.lastModified() ) );
				fileEntity.setFileStsId(FileStatus.Delete.getStatusId());

				String absolutePath = TriStringUtils.convertPath( file.getCanonicalPath() ) ;
				if ( statusMap.containsKey( absolutePath ) ) {
					SVNStatus svnStatus = statusMap.get( absolutePath ) ;

					long revisionNo = svnStatus.getCommittedRevision().getNumber() ;
					if( -1 == revisionNo ) {
						throw new TriSystemException( AmMessageId.AM004001F , path.getFilePath());
					}

					fileEntity.setFileRev( String.valueOf( revisionNo ) ) ;
				} else {
					throw new TriSystemException( AmMessageId.AM004000F , path.getFilePath());
				}
				areqDto.getAreqFileEntities().add( fileEntity );
			}
		}

		IFileResult[] binaryFiles = AmExtractMessageAddonUtils.extractDelApplyBinaryFileResults( paramList );

		if ( null != binaryFiles ) {

			int binaryFileSeq = 0;
			for ( IFileResult binaryPath : binaryFiles ) {

				IAreqBinaryFileEntity fileEntity = new AreqBinaryFileEntity();
				fileEntity.setAreqId( areqEntity.getAreqId() );
				fileEntity.setAreqCtgCd( areqEntity.getAreqCtgCd() );
				fileEntity.setAreqFileSeqNo( this.getAreqFileNumberingDao().nextval( ++binaryFileSeq ) );
				fileEntity.setFilePath( TriStringUtils.convertPath( binaryPath.getFilePath() ) ) ;

				areqDto.getAreqBinaryFileEntities().add( fileEntity );
			}
		}

		return areqDto;
	}

	public final void validateRemovalRequestInput(RemovalRequestEditInputBean inputBean) throws ContinuableBusinessException {
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();
		try {
			if (TriStringUtils.isEmpty(inputBean.getPjtId())) {
				messageList.add(AmMessageId.AM001038E);
				messageArgsList.add(new String[] {});
			}

			if (TriStringUtils.isEmpty(inputBean.getGroupId())) {
				messageList.add(AmMessageId.AM001077E);
				messageArgsList.add(new String[] {});
			}

			if (!StatusFlg.off.value().equals(
					sheet.getValue(AmDesignBeanId.flowChaLibMasterDelEntryCheck, ChaLibScreenItemID.FlowChaLibMasterDelEntryCheck.SUMMARY.toString()))) {
				if (inputBean.getSubmitMode() != RemovalRequestEditInputBean.SubmitMode.draft && TriStringUtils.isEmpty(inputBean.getSubject())) {
					messageList.add(AmMessageId.AM001040E);
					messageArgsList.add(new String[] {});
				}
			}

			if (!StatusFlg.off.value().equals(
					sheet.getValue(AmDesignBeanId.flowChaLibMasterDelEntryCheck, ChaLibScreenItemID.FlowChaLibMasterDelEntryCheck.CONTENT.toString()))) {
				if (inputBean.getSubmitMode() != RemovalRequestEditInputBean.SubmitMode.draft && TriStringUtils.isEmpty(inputBean.getContents())) {
					messageList.add(AmMessageId.AM001076E);
					messageArgsList.add(new String[] {});
				}
			}
			if (TriStringUtils.isNotEmpty(inputBean.getCtgId())) {
				ICtgEntity ctgEntity = this.getUmFinderSupport().findCtgByPrimaryKey(inputBean.getCtgId());
				if (null == ctgEntity) {
					messageList.add		( AmMessageId.AM001130E );
					messageArgsList.add	( new String[] {inputBean.getCtgId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(inputBean.getMstoneId())) {
				IMstoneEntity mstoneEntity = this.getUmFinderSupport().findMstoneByPrimaryKey(inputBean.getMstoneId());
				if (null == mstoneEntity) {
					messageList.add		( AmMessageId.AM001131E );
					messageArgsList.add	( new String[] {inputBean.getMstoneId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(inputBean.getAssigneeId())) {
				String groupId = inputBean.getGroupId();
				if (TriStringUtils.isNotEmpty(groupId)) {
					IGrpUserLnkEntity grpUserLnkEntity = this.getUmFinderSupport().findGrpUserLnkByPrimaryKey(groupId, inputBean.getAssigneeId());

					if (null == grpUserLnkEntity) {
						messageList.add		( AmMessageId.AM001133E );
						messageArgsList.add	( new String[] {groupId} );
					}
				}

			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}
}
