package jp.co.blueship.tri.am.support;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * 変更管理起票系のサポートClass<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibPjtEditSupport extends AmFinderSupport /* extends FinderSupport */ {

	private static TriFunction<IGrpEntity, String> TO_GROUP_ID = new TriFunction<IGrpEntity, String>() {

		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	/**
	 * ロットエンティティからロット画面用ビーンを作成します。
	 *
	 * @param entities ロットエンティティ
	 * @return ロット画面用ビーンのリスト
	 */
	public List<LotViewBean> getLotViewBean(List<ILotDto> lotDto, List<String> disableLotNoList) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();
		AmViewInfoAddonUtils.setLotViewBeanPjtLotEntity(lotViewBeanList, lotDto, disableLotNoList, this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

		for (LotViewBean viewBean : lotViewBeanList) {

			viewBean.setCompPjtCount(getCompPjtCount(viewBean.getLotNo()));
			viewBean.setProgressPjtCount(getProgressPjtCount(viewBean.getLotNo()));
		}

		return lotViewBeanList;
	}

	/**
	 * ロットに属する変更管理のうち、完了した件数を取得する。
	 *
	 * @param lotId ロット番号
	 * @return 完了の変更管理の件数
	 */
	private int getCompPjtCount(String lotId) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getCompleteCountPjtCondition(lotId);

		return this.getPjtDao().count(condition.getCondition());

	}

	/**
	 * ロットに属する変更管理のうち、進行中の件数を取得する。
	 *
	 * @param lotId ロット番号
	 * @return 完了の変更管理の件数
	 */
	private int getProgressPjtCount(String lotId) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getProgressCountPjtCondition(lotId);

		return this.getPjtDao().count(condition.getCondition());

	}

	/**
	 * ロットエンティティからロット詳細画面用ビーンを作成します。
	 *
	 * @param entity ロットエンティティ
	 * @return ロット詳細画面用ビーン
	 */
	public LotDetailViewBean getLotDetailViewBean(IGeneralServiceBean paramBean, ILotDto entity) {

		LotDetailViewBean lotDetailViewBean = new LotDetailViewBean();
		IBldSrvEntity srvEntity = this.getBmFinderSupport().findBldSrvEntityByController();

		AmViewInfoAddonUtils.setLotDetailViewBeanPjtLotEntity(paramBean,lotDetailViewBean, entity, srvEntity.getBldSrvId(), srvEntity.getBldSrvNm() );

		return lotDetailViewBean;
	}

	/**
	 * ロット番号から変更管理エンティティを取得する。
	 *
	 * @param lotId ロット番号
	 * @param selectPageNo 選択ページ番号
	 * @param viewRows ページあたりの行数
	 * @param sort ソート条件
	 * @return 変更管理エンティティ
	 */
	public IEntityLimit<IPjtEntity> getPjtEntityLimit(String lotId, int selectPageNo, int viewRows, ISqlSort sort) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getPjtCondition(lotId);

		IEntityLimit<IPjtEntity> entityLimit = this.getPjtDao().find(condition.getCondition(), sort, selectPageNo, viewRows);

		return entityLimit;
	}

	/**
	 * 変更管理番号から申請情報エンティティを取得する。
	 *
	 * @param pjtNo 変更管理番号
	 * @param selectPageNo 選択ページ番号
	 * @param viewRows ページあたりの行数
	 * @param sort ソート条件
	 * @return 申請情報エンティティ
	 */
	public IEntityLimit<IAreqEntity> getAssetApplyEntityLimit(String pjtNo, int selectPageNo, int viewRows, ISqlSort sort) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getAreqCondition(pjtNo);

		IEntityLimit<IAreqEntity> entityLimit = this.getAreqDao().find(condition.getCondition(), sort, selectPageNo, viewRows);

		return entityLimit;
	}

	/**
	 * 申請情報エンティティから申請情報詳細画面用ビーンを作成します。
	 *
	 * @param dtoList 変更管理エンティティ
	 * @return 申請情報詳細画面用ビーンのリスト
	 */
	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewPjtDetailBean(String userId, AreqDtoList dtoList) {

		List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();

		AmViewInfoAddonUtils.setApplyInfoViewBeanAssetApplyEntity(userId, applyInfoViewBeanList, dtoList);

		return applyInfoViewBeanList;
	}

	/**
	 * 変更管理エンティティを変更管理番号の配列に変換します。
	 *
	 * @param entities 変更管理エンティティ
	 * @return
	 */
	public final String[] getPjtNo(IPjtEntity[] entities) {
		if (null == entities)
			return new String[0];

		List<String> value = new ArrayList<String>();

		for (IPjtEntity pjtEntity : entities) {
			value.add(pjtEntity.getPjtId());
		}

		return value.toArray(new String[0]);
	}

	/**
	 * 申請エンティティを申請番号の配列に変換します。
	 *
	 * @param entities 申請情報エンティティ
	 * @return
	 */
	public final String[] getAssetApplyNo(IAreqEntity[] entities) {
		if (null == entities)
			return new String[0];

		List<String> value = new ArrayList<String>();

		for (IAreqEntity assetApplyEntity : entities) {
			value.add(assetApplyEntity.getAreqId());
		}

		return value.toArray(new String[0]);
	}

	/**
	 * アクセス可能なロット番号一覧を検索条件に追加する
	 *
	 * @param condition ロットの検索条件
	 * @param retBean GenericServiceBean
	 * @param isSetInfo 検索結果が０件の場合にメッセージを設定するかどうか
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @return
	 */

	public boolean setAccessableLotNumbers(LotCondition condition, GenericServiceBean retBean, boolean isSetInfo, List<String> disableLinkLotNumbers) {

		List<String> accessableLotNumbers = new ArrayList<String>();

		// いったん全部取得
		List<ILotEntity> entities = this.getLotDao().find(condition.getCondition());
		if (isSetInfo && TriStringUtils.isEmpty(entities)) {
			retBean.setInfoMessage( AmMessageId.AM001007E );
		}
		List<ILotDto> lotDto = this.findLotDto( entities );

		// ユーザが所属しているグループ
		List<String> groupIdList = TriCollectionUtils.collect(groupFrom(retBean.getUserId()), TO_GROUP_ID);

		for (ILotDto lot : lotDto) {

			// アクセス許可グループ指定なし＝全員アクセス可能
			if (TriStringUtils.isEmpty(lot.getLotGrpLnkEntities())) {
				accessableLotNumbers.add(lot.getLotEntity().getLotId());

				// グループ指定あり
			} else {
				// 自分の所属グループがロットにアクセス許可があるかチェック
				for (ILotGrpLnkEntity group : lot.getLotGrpLnkEntities()) {

					if (groupIdList.contains(group.getGrpId())) {
						accessableLotNumbers.add(lot.getLotEntity().getLotId());
						break;
					}
				}
			}

			// 一覧表示許可あり
			if (!accessableLotNumbers.contains(lot.getLotEntity().getLotId()) && StatusFlg.on == lot.getLotEntity().getAllowListView()) {

				accessableLotNumbers.add(lot.getLotEntity().getLotId());
				disableLinkLotNumbers.add(lot.getLotEntity().getLotId());
			}
		}

		if (0 == accessableLotNumbers.size()) {
			return false;
		}

		condition.setLotIds(accessableLotNumbers.toArray(new String[0]));
		return true;
	}

	private List<IGrpEntity> groupFrom(String userId) {
		return this.getUmFinderSupport().findGroupByUserId(userId);
	}
}
