package jp.co.blueship.tri.am.support;

import jp.co.blueship.tri.am.dao.areq.IAreqAttachedFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqBinaryFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.IAreqFileDao;
import jp.co.blueship.tri.am.dao.areq.IViewAssetRegisterDao;
import jp.co.blueship.tri.am.dao.baseline.IHeadBlDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlMdlLnkDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlReqLnkDao;
import jp.co.blueship.tri.am.dao.ext.IExtDao;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.ILotGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotMdlLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotRelEnvLnkDao;
import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.fw.um.dao.hist.IAreqHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IPjtHistDao;

/**
 * AM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public interface IAmDaoFinder {
	/**
	 * 拡張子DAOを取得します。
	 * @return 拡張子DAO
	 */
	public IExtDao getExtDao();
	/**
	 * ロット・ベースラインDAOを取得します。
	 * @return ロット・ベースラインDAO
	 */
	public ILotBlDao getLotBlDao();
	/**
	 * ロット・ベースライン・モジュールDAOを取得します。
	 * @return ロット・ベースライン・モジュールDAO
	 */
	public ILotBlMdlLnkDao getLotBlMdlLnkDao();
	/**
	 * ロット・ベースライン・資産申請DAOを取得します。
	 * @return ロット・ベースライン・資産申請DAO
	 */
	public ILotBlReqLnkDao getLotBlReqLnkDao() ;
	/**
	 * ロットDAOを取得します。
	 * @return ロットDAO
	 */
	public ILotDao getLotDao();

	/**
	 * ロット・モジュールDAOを取得します。
	 * @return ロット・モジュールDAO
	 */
	public ILotMdlLnkDao getLotMdlLnkDao();
	/**
	 * ロット・グループDAOを取得します。
	 * @return ロット・グループDAO
	 */
	public ILotGrpLnkDao getLotGrpLnkDao();
	/**
	 * ロット・メールグループDAOを取得します。
	 * @return ロット・メールグループDAO
	 */
	public ILotMailGrpLnkDao getLotMailGrpLnkDao();
	/**
	 * ロット・リリース環境DAOを取得します。
	 * @return ロット・リリース環境DAO
	 */
	public ILotRelEnvLnkDao getLotRelEnvLnkDao();
	/**
	 * 変更情報DAOを取得します。
	 * @return 変更情報DAO
	 */
	public IPjtDao getPjtDao();
	/**
	 * 資産申請DAOを取得します。
	 * @return 資産申請DAO
	 */
	public IAreqDao getAreqDao();
	/**
	 * 資産申請履歴DAOを取得します。
	 * @return areqHistDao
	 */
	public IAreqHistDao getAreqHistDao();

	/**
	 * 資産申請ファイルDAOを取得します。
	 * @return 資産申請ファイルDAO
	 */
	public IAreqFileDao getAreqFileDao();

	public IAreqAttachedFileDao getAreqAttachedFileDao();

	/**
	 * 資産申請バイナリファイルDAOを取得します。
	 * @return 資産申請ファイルDAO
	 */
	public IAreqBinaryFileDao getAreqBinaryFileDao();
	/**
	 * viewAssetRegisterDaoを取得します。
	 * @return viewAssetRegisterDao
	 */
	public IViewAssetRegisterDao getViewAssetRegisterDao();
	/**
	 * viewAssetRegisterDaoを設定します。
	 * @param viewAssetRegisterDao viewAssetRegisterDao
	 */
	public void setViewAssetRegisterDao(IViewAssetRegisterDao viewAssetRegisterDao);

	/**
	 * 変更管理履歴DAOを取得します。<br>
	 * Getting the PjtHistory DAO.
	 *
	 * @return pjtHistDao
	 */
	public IPjtHistDao getPjtHistDao();
	
	public IHeadBlDao getHeadBlDao();
}
