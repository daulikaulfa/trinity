package jp.co.blueship.tri.am.support;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.lot.eb.LotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.BuildEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * ロット作成系のサポートClass <br>
 * <br>
 *
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public class FlowChaLibLotEditSupport
		extends AmFinderSupport /*
								 * extends FinderSupport
								 */ {
	private static final Comparator<ModuleViewBean> IS_MODULE_NAME_EQUAL = new Comparator<ModuleViewBean>() {

		@Override
		public int compare(ModuleViewBean dto1, ModuleViewBean dto2) {

			if (dto1.getModuleName().equals(dto2.getModuleName())) {
				return 0;
			}

			return -1;
		}

	};

	private static final TriFunction<IVcsReposDto, ModuleViewBean> TO_MODULE_VIEW_BEAN = new TriFunction<IVcsReposDto, ModuleViewBean>() {

		@Override
		public ModuleViewBean apply(IVcsReposDto reposDto) {

			ModuleViewBean moduleViewBean = new ModuleViewBean();
			moduleViewBean.setModuleName(reposDto.getVcsMdlEntity().getMdlNm());
			moduleViewBean.setRepository(reposDto.getVcsReposEntity().getVcsReposUrl());

			return moduleViewBean;
		}
	};

	private static final TriFunction<IGrpEntity, GroupViewBean> TO_GROUP_VIEW_BEAN_FROM_GRP_ENTITY = new TriFunction<IGrpEntity, GroupViewBean>() {

		@Override
		public GroupViewBean apply(IGrpEntity groupEntity) {

			GroupViewBean groupViewBean = new GroupViewBean();
			groupViewBean.setGroupId(groupEntity.getGrpId());
			groupViewBean.setGroupName(groupEntity.getGrpNm());

			return groupViewBean;
		}
	};

	/**
	 * リリース環境エンティティから環境番号の配列を取得する
	 *
	 * @param envEntityArray
	 * @return
	 */
	public String[] convertToEnvNo(IBldEnvEntity[] envEntityArray) {
		return AmViewInfoAddonUtils.convertToEnvNo(envEntityArray);
	}

	/**
	 * リリース環境情報エンティティからリリース環境を取得する
	 *
	 * @param envList
	 * @return
	 */
	public Map<String, IBldEnvEntity> convertToEnvMap(IBldEnvEntity[] envArray) {
		return AmViewInfoAddonUtils.convertToEnvMap(FluentList.from(envArray).asList());
	}

	/**
	 * ロット情報エンティティからアクセス可能なリリース環境を取得する
	 *
	 * @param envList
	 * @return
	 */
	public Map<String, ILotRelEnvLnkEntity> convertToLotEnvMap(List<ILotRelEnvLnkEntity> envList) {
		return AmViewInfoAddonUtils.convertToLotEnvMap(envList);
	}

	/**
	 * 選択リリース環境情報から環境番号のセットを取得する
	 *
	 * @param envEntityArray
	 * @return
	 */
	public Set<String> convertToEnvNoSet(List<RelEnvViewBean> selectedViewBeanList) {
		return AmViewInfoAddonUtils.convertToEnvNoSet(selectedViewBeanList);
	}

	/**
	 * UcfScmレコードを取得し、ModuleViewBeanのリストで返す <br>
	 *
	 * @return ModuleViewBeanのリスト
	 */
	public List<ModuleViewBean> getModuleViewBeanList() {

		List<IVcsReposDto> reposDtoList = findVcsReposDto(//
				AmDBSearchSortAddonUtils.getUcfScmSortFromDesignDefineByChaLibLotEntryModuleList());

		return toModuleViewBeanList(reposDtoList);
	}

	/**
	 * UcfRepositoryレコードを取得し、ModuleViewBeanのリストで返す <br>
	 *
	 * @return ModuleViewBeanのリスト
	 */
	public List<ModuleViewBean> getModuleViewBeanListForSvn(String serverId) {

		List<IVcsReposDto> reposDtoList = this.findVcsReposDto();
		if (TriCollectionUtils.isEmpty(reposDtoList)) {
			throw new TriSystemException(AmMessageId.AM004002F, serverId);
		}

		return toModuleViewBeanList(reposDtoList);
	}

	/**
	 * VcsReposDtoをModuleViewBeanへ変換します。<br/>
	 * その際、モジュール名をキーにしてModuleViewBeanの重複を取り除きます。
	 *
	 * @param reposDtoList
	 *            VcsReposDtoのリスト
	 * @return 重複を取り除いた、ModuleViewBeanのリスト
	 */
	private List<ModuleViewBean> toModuleViewBeanList(List<IVcsReposDto> reposDtoList) {

		ComparableSet<ModuleViewBean> moduleViewBeanSet = new ComparableSet<ModuleViewBean>(IS_MODULE_NAME_EQUAL);
		TriCollectionUtils.collect(//
				reposDtoList, //
				TO_MODULE_VIEW_BEAN, //
				moduleViewBeanSet);

		return new ArrayList<ModuleViewBean>(moduleViewBeanSet);
	}

	/**
	 * ビルド環境レコード（選択可能なステータスのもののみ）を取得し、BuildEnvViewBeanのリストで返す <br>
	 *
	 * @param serviceBean
	 */
	public List<BuildEnvViewBean> getBuildEnvViewBeanList(IGeneralServiceBean serviceBean) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getActiveBuildEnvCondition();
		ISqlSort sort = (ISqlSort) AmDBSearchSortAddonUtils
				.getBuildEnvSortFromDesignDefineByChaLibLotEntryBuildEnvList();

		List<IBldEnvEntity> envList = getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		List<BuildEnvViewBean> envViews = new ArrayList<BuildEnvViewBean>();

		for (IBldEnvEntity entity : envList) {
			BuildEnvViewBean buildEnvViewBean = new BuildEnvViewBean();
			buildEnvViewBean.setEnvNo(entity.getBldEnvId());
			buildEnvViewBean.setEnvName(entity.getBldEnvNm());
			buildEnvViewBean.setContent(entity.getRemarks());
			buildEnvViewBean.setStatusId(entity.getStsId());
			buildEnvViewBean.setInsertUpdateDate(
					TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), serviceBean.getTimeZone()));
			envViews.add(buildEnvViewBean);
		}

		return envViews;
	}

	/**
	 * リリース環境レコード（選択可能なステータスのもののみ）を取得する <br>
	 *
	 * @return リリース環境エンティティ
	 */
	public IBldEnvEntity[] getRelEnvEntityArray() {
		return getRelEnvEntityArray(null);
	}

	/**
	 * リリース環境レコード（選択可能なステータスのもののみ）を取得する <br>
	 *
	 * @param 対象となるリリース環境番号の配列
	 * @return リリース環境エンティティ
	 */
	public IBldEnvEntity[] getRelEnvEntityArray(String[] envNo) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getActiveRelEnvCondition(envNo);
		ISqlSort sort = AmDBSearchSortAddonUtils.getRelEnvSortFromDesignDefineByChaLibLotEntryRelEnvList();

		List<IBldEnvEntity> list = getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);

		return list.toArray(new IBldEnvEntity[0]);
	}

	/**
	 * リリース環境レコード（選択可能なステータスのもののみ）を取得し、RelEnvViewBeanのリストで返す <br>
	 *
	 * @param serviceBean
	 * @return BuildEnvViewBeanのリスト
	 */
	public List<RelEnvViewBean> getRelEnvViewBeanList(IGeneralServiceBean serviceBean) {

		IBldEnvEntity[] envList = getRelEnvEntityArray();
		List<RelEnvViewBean> envViews = new ArrayList<RelEnvViewBean>();

		for (IBldEnvEntity entity : envList) {
			RelEnvViewBean viewBean = new RelEnvViewBean();
			viewBean.setEnvNo(entity.getBldEnvId());
			viewBean.setEnvName(entity.getBldEnvNm());
			viewBean.setEnvSummary(entity.getRemarks());
			viewBean.setStatusId(entity.getStsId());
			viewBean.setLatestUpdateDate(TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), serviceBean.getTimeZone()));
			envViews.add(viewBean);
		}

		return envViews;
	}

	/**
	 * ロット編集入力情報をもとに、ロット情報パラメタオブジェクトを生成します。
	 *
	 * @param lotEditInputBean
	 *            ロット編集入力情報
	 *
	 * @return ロット情報パラメタオブジェクト
	 */
	public LotParamInfo createLotParamInfo(LotEditInputV3Bean lotEditInputBean) {

		LotParamInfo lotParamInfo = new LotParamInfo();

		ILotDto lotDto = new LotDto();
		ILotEntity lotEntity = new LotEntity();

		// 有効化
		lotParamInfo.setProcessResult(true);

		// ユーザ情報
		lotParamInfo.setUser(lotEditInputBean.getInputUserName());
		lotParamInfo.setUserId(lotEditInputBean.getInputUserId());

		// ＨＥＡＤバージョンタグの発番（タイムスタンプを含むため、共通で使用する）
		String headVersionTag = AmDesignBusinessRuleUtils.getLotBaseVersionTag();
		lotParamInfo.setHeadVersionTag(headVersionTag);
		// ブランチタグの発番（タイムスタンプを含むため、共通で使用する）
		String lotBranchTag = AmDesignBusinessRuleUtils.getLotBranchTag();
		lotEntity.setLotBranchTag(lotBranchTag);
		// ロットバージョンタグの発番（タイムスタンプを含むため、共通で使用する）
		String lotVersionTag = AmDesignBusinessRuleUtils.getLotVersionTag();
		lotEntity.setLotBranchBlTag(lotEditInputBean.getBaseLineTag());
		lotEntity.setLotLatestVerTag(lotVersionTag);
		// 新規lotNoの発番（タイムスタンプを含むため、共通で使用する）
		String lotId = this.getLotNumberingDao().nextval();
		lotEntity.setLotId(lotId);

		// ベースラインタグ名
		lotEntity.setLotLatestBlTag(lotEditInputBean.getBaseLineTag());

		// 基本情報
		lotEntity.setLotNm(lotEditInputBean.getLotName());
		lotEntity.setSummary(lotEditInputBean.getLotSummary());
		lotEntity.setContent(lotEditInputBean.getLotContent());
		lotEntity.setUseMerge(deriveUseMergeStatus(lotEditInputBean));
		lotEntity.setIsAssetDup(deriveAllowAssetDuplicationStatus(lotEditInputBean));
		lotEntity.setAllowListView(deriveAllowListViewStatus(lotEditInputBean));

		// ディレクトリパス
		populateDirectoryPathToLotEntity(lotEntity, lotEditInputBean, lotEntity.getLotId());
		// モジュール情報
		lotDto.setLotMdlLnkEntities(TriCollectionUtils.collect(lotEditInputBean.getModuleViewBeanList(),
				toLotMdlLnkEntity(lotEntity.getLotId(), lotEntity.getLotBranchTag())));

		// 今回追加するモジュール（既存選択済みモジュールを取り除く）
		lotParamInfo.setTargetModuleList(//
				TriCollectionUtils.collect(lotEditInputBean.getSelectedModuleViewBeanList(),
						populateLotIncludeFlagFunc(lotDto)));

		// グループ情報
		populateGroupInfo(lotEditInputBean, lotEntity.getLotId(), lotParamInfo, lotDto);

		// メール送信先 特定グループ
		populateMailGroup(lotEditInputBean, lotEntity.getLotId(), lotParamInfo, lotDto);

		// ビルド環境情報
		populateBuildEnvInfo(lotEditInputBean, lotEntity);

		// リリース環境情報
		populateReleaseEnvInfo(lotEditInputBean, lotEntity.getLotId(), lotDto);

		lotEntity.setVcsCtgCd(DesignSheetFactory.getDesignSheet().getValue(AmDesignEntryKeyByScm.useApplication));
		lotEntity.setStsId(AmLotStatusId.Unspecified.getStatusId());
		lotEntity.setDelStsId(StatusFlg.off);// OFF固定

		lotDto.setLotEntity(lotEntity);
		lotParamInfo.setLotDto(lotDto);

		return lotParamInfo;
	}

	/**
	 * ロット編集入力情報、既存のロット情報をもとにロット更新用パラメタオブジェクトを生成します。
	 *
	 * @param lotEditInputBean
	 *            ロット編集入力情報
	 * @param lotDto
	 *            既存ロット情報
	 *
	 * @return ロット情報パラメタオブジェクト
	 */
	public LotParamInfo populateLotParamInfo(LotEditInputV3Bean lotEditInputBean, ILotDto lotDto) {

		LotParamInfo lotParamInfo = new LotParamInfo();

		// 有効化
		lotParamInfo.setProcessResult(true);

		// ユーザ情報
		lotParamInfo.setUser(lotEditInputBean.getInputUserName());
		lotParamInfo.setUserId(lotEditInputBean.getInputUserId());

		// ロット編集
		lotParamInfo.setHeadVersionTag(null);

		ILotEntity lotEntity = lotDto.getLotEntity();

		// 基本情報
		lotEntity.setLotNm(lotEditInputBean.getLotName());
		lotEntity.setSummary(lotEditInputBean.getLotSummary());
		lotEntity.setContent(lotEditInputBean.getLotContent());
		lotEntity.setUseMerge(deriveUseMergeStatus(lotEditInputBean));
		lotEntity.setIsAssetDup(deriveAllowAssetDuplicationStatus(lotEditInputBean));
		lotEntity.setAllowListView(deriveAllowListViewStatus(lotEditInputBean));

		// ロット編集の場合、モジュール情報は更新対象から外す。
		lotDto.setLotMdlLnkEntities(Collections.<ILotMdlLnkEntity> emptyList());
		lotParamInfo.setTargetModuleList(new ArrayList<ModuleViewBean>());

		// グループ情報
		populateGroupInfo(lotEditInputBean, lotEntity.getLotId(), lotParamInfo, lotDto);

		// メール送信先 特定グループ
		populateMailGroup(lotEditInputBean, lotEntity.getLotId(), lotParamInfo, lotDto);

		// ビルド環境情報
		populateBuildEnvInfo(lotEditInputBean, lotEntity);

		// リリース環境情報
		populateReleaseEnvInfo(lotEditInputBean, lotEntity.getLotId(), lotDto);

		lotParamInfo.setLotDto(lotDto);

		return lotParamInfo;
	}

	private void populateDirectoryPathToLotEntity(ILotEntity lotEntity, LotEditInputV3Bean lotEditInputBean,
			String lotId) {

		String inOutBox = AmDesignBusinessRuleUtils.getLotDirectory(lotId, lotEditInputBean.getDirectoryPathPublic(),
				lotEditInputBean.getDirectoryPathPrivate(), AmDesignEntryKeyByChangec.inOutBox);
		String workPath = AmDesignBusinessRuleUtils.getLotDirectory(lotId, lotEditInputBean.getDirectoryPathPublic(),
				lotEditInputBean.getDirectoryPathPrivate(), AmDesignEntryKeyByChangec.workPath);
		String historyPath = AmDesignBusinessRuleUtils.getLotDirectory(lotId, lotEditInputBean.getDirectoryPathPublic(),
				lotEditInputBean.getDirectoryPathPrivate(), AmDesignEntryKeyByChangec.historyPath);
		String workspace = AmDesignBusinessRuleUtils.getLotDirectory(lotId, lotEditInputBean.getDirectoryPathPublic(),
				lotEditInputBean.getDirectoryPathPrivate(), AmDesignEntryKeyByChangec.workspace);
		String systemInBox = AmDesignBusinessRuleUtils.getLotDirectory(lotId, lotEditInputBean.getDirectoryPathPublic(),
				lotEditInputBean.getDirectoryPathPrivate(), AmDesignEntryKeyByChangec.systemInBox);

		lotEntity.setInOutBox(inOutBox);
		lotEntity.setLotMwPath(workPath);
		lotEntity.setHistPath(historyPath);
		lotEntity.setWsPath(workspace);
		lotEntity.setSysInBox(systemInBox);
	}

	private void populateGroupInfo(LotEditInputV3Bean lotEditInputBean, String lotId, LotParamInfo lotParamInfo,
			ILotDto lotDto) {

		lotDto.setLotGrpLnkEntities(//
				TriCollectionUtils.collect(lotEditInputBean.getSelectedGroupViewBeanList(), toLotGrpLnkEntity(lotId)));
		lotParamInfo.setGroupViewBeanList(lotEditInputBean.getGroupViewBeanList());
	}

	private void populateMailGroup(LotEditInputV3Bean lotEditInputBean, String lotId, LotParamInfo lotParamInfo,
			ILotDto lotDto) {

		lotDto.setLotMailGrpLnkEntities(//
				TriCollectionUtils.collect(lotEditInputBean.getSelectedSpecifiedGroupList(),
						toLotMailGrpLnkEntity(lotId)));
		lotParamInfo.setSpecifiedGroupList(lotEditInputBean.getSpecifiedGroupList());
	}

	private void populateBuildEnvInfo(LotEditInputV3Bean lotEditInputBean, ILotEntity lotEntity) {

		lotEntity.setBldEnvId(lotEditInputBean.getSelectedBuildEnvNo());
		lotEntity.setFullBldEnvId(lotEditInputBean.getSelectedFullBuildEnvNo());
	}

	private void populateReleaseEnvInfo(LotEditInputV3Bean lotEditInputBean, String lotId, ILotDto lotDto) {
		lotDto.setLotRelEnvLnkEntities(//
				TriCollectionUtils.collect(lotEditInputBean.getSelectedRelEnvViewBeanList(),
						toLotRelEnvLnkEntity(lotId)));
	}

	private TriFunction<ModuleViewBean, ILotMdlLnkEntity> toLotMdlLnkEntity(final String lotId,
			final String lotBranchTag) {

		return new TriFunction<ModuleViewBean, ILotMdlLnkEntity>() {

			@Override
			public ILotMdlLnkEntity apply(ModuleViewBean moduleViewBean) {

				ILotMdlLnkEntity mdlEntity = new LotMdlLnkEntity();
				mdlEntity.setLotId(lotId);
				mdlEntity.setMdlNm(moduleViewBean.getModuleName());
				mdlEntity.setMdlVerTag(lotBranchTag);
				mdlEntity.setIsLotInclude(StatusFlg.off);

				return mdlEntity;
			}
		};

	}

	private TriFunction<ModuleViewBean, ModuleViewBean> populateLotIncludeFlagFunc(final ILotDto lotDto) {

		return new TriFunction<ModuleViewBean, ModuleViewBean>() {

			@Override
			public ModuleViewBean apply(ModuleViewBean viewBean) {

				ILotMdlLnkEntity mdlEntity = lotDto.getMdlEntity(viewBean.getModuleName());
				if (null != mdlEntity) {
					mdlEntity.setIsLotInclude(StatusFlg.on);
				}

				return viewBean;
			}
		};
	}

	private TriFunction<GroupViewBean, ILotGrpLnkEntity> toLotGrpLnkEntity(final String lotId) {

		return new TriFunction<GroupViewBean, ILotGrpLnkEntity>() {

			@Override
			public ILotGrpLnkEntity apply(GroupViewBean viewBean) {

				ILotGrpLnkEntity entity = new LotGrpLnkEntity();
				entity.setLotId(lotId);
				entity.setGrpId(viewBean.getGroupId());

				return entity;
			}
		};
	}

	private TriFunction<GroupViewBean, ILotMailGrpLnkEntity> toLotMailGrpLnkEntity(final String lotId) {

		return new TriFunction<GroupViewBean, ILotMailGrpLnkEntity>() {

			@Override
			public ILotMailGrpLnkEntity apply(GroupViewBean viewBean) {
				ILotMailGrpLnkEntity mailGrpEntity = new LotMailGrpLnkEntity();

				mailGrpEntity.setLotId(lotId);
				mailGrpEntity.setGrpId(viewBean.getGroupId());

				return mailGrpEntity;
			}
		};
	}

	private TriFunction<RelEnvViewBean, ILotRelEnvLnkEntity> toLotRelEnvLnkEntity(final String lotId) {

		return new TriFunction<RelEnvViewBean, ILotRelEnvLnkEntity>() {

			@Override
			public ILotRelEnvLnkEntity apply(RelEnvViewBean viewBean) {

				ILotRelEnvLnkEntity entity = new LotRelEnvLnkEntity();
				entity.setLotId(lotId);
				entity.setBldEnvId(viewBean.getEnvNo());

				return entity;
			}
		};
	}

	/**
	 * 二重貸出チェックのステータス値を導出します。
	 *
	 * @param lotEditInputBean
	 *            ロット編集入力オブジェクト
	 *
	 * @return 二重貸出チェックのステータス値
	 */
	private StatusFlg deriveAllowAssetDuplicationStatus(LotEditInputV3Bean lotEditInputBean) {

		return lotEditInputBean.isAllowAssetDuplication() ? StatusFlg.on : StatusFlg.off;
	}

	/**
	 * UseMergeのステータス値を導出します。
	 *
	 * @param lotEditInputBean
	 *            ロット編集入力オブジェクト
	 *
	 * @return UseMergeのステータス値
	 */
	private StatusFlg deriveUseMergeStatus(LotEditInputV3Bean lotEditInputBean) {

		return lotEditInputBean.isEditUseMerge() ? StatusFlg.on : StatusFlg.off;
	}

	/**
	 * 一覧表示許可のステータス値を導出します。
	 *
	 * @param lotEditInputBean
	 *            ロット編集入力オブジェクト
	 *
	 * @return 一覧表示許可のステータス値
	 */
	private StatusFlg deriveAllowListViewStatus(LotEditInputV3Bean lotEditInputBean) {
		return lotEditInputBean.isAllowListView() ? StatusFlg.on : StatusFlg.off;
	}

	/**
	 * 選択可能なグループを取得します。
	 *
	 * @return
	 */
	public final List<GroupViewBean> getGroupViewBean() {

		GrpCondition condition = new GrpCondition();
		List<IGrpEntity> grpEntitys = this.getUmFinderSupport().getGrpDao().find(condition.getCondition());

		return TriCollectionUtils.collect(grpEntitys, TO_GROUP_VIEW_BEAN_FROM_GRP_ENTITY);
	}

	/**
	 * デフォルト選択済みとなるグループを取得します。
	 *
	 * @return
	 */
	public final List<GroupViewBean> getDefaultSelectedGroupViewBean(String userId) {

		return TriCollectionUtils.collect(groupsFrom(userId), TO_GROUP_VIEW_BEAN_FROM_GRP_ENTITY);
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * 有効（「貸出申請取消」「削除申請取消」「パッケージクローズ」「ロットマージ済」以外）な資産申請情報を取得します。
	 *
	 * @param lotId
	 *            対象ロット
	 * @param groups
	 *            対象グループ名
	 *
	 * @return 取得した資産申請情報エンティティを戻します。
	 */
	public final List<IAreqEntity> getAssetApplyByAssetApplyProgress(String lotId, String[] groups) {

		AreqCondition condition = AmDBSearchConditionAddonUtils.getAreqConditionByAssetApplyProgress(lotId);
		condition.setGrpNms(groups);

		List<IAreqEntity> list = getAreqDao().find(condition.getCondition());

		List<IAreqEntity> entitys = new ArrayList<IAreqEntity>();
		for (IAreqEntity entity : list) {
			entitys.add(entity);
		}

		return entitys;
	}
}
