package jp.co.blueship.tri.am.support;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqAttachedFileItems;
import jp.co.blueship.tri.am.dao.areq.constants.AreqFileItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlMdlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadLotBlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlMdlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlReqLnkCondition;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.ext.constants.ExtItems;
import jp.co.blueship.tri.am.dao.ext.eb.ExtCondition;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.head.eb.HeadCondition;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlAreqLnkCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlDto;
import jp.co.blueship.tri.am.dao.vcsrepos.constants.VcsMdlItems;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.VcsMdlCondition;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.VcsReposCondition;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.VcsReposDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * AM関連情報を検索するためのサービス機能を提供するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L12.01
 * @author Norheda
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Sam
 */
public class AmFinderSupport extends AmDaoFinder implements IAmFinderSupport {

//	private IContextAdapter context = null;

	private ISmFinderSupport smFinderSupport = null;
	private IUmFinderSupport umFinderSupport = null;
	private IBmFinderSupport bmFinderSupport = null;

//
//	/**
//	 * ビーンを生成するコンテキストが設定されます。
//	 * @param context コンテキスト
//	 */
//	public final void setContext( IContextAdapter context ) {
//		this.context = context;
//	}
//
//	/**
//	 * ビーンを生成するコンテキストを取得します。
//	 * @return コンテキスト
//	 */
//	protected final IContextAdapter getContext() {
//		if ( null == context )
//			context = ContextAdapterFactory.getContextAdapter();
//
//		return this.context;
//	}

	@Override
	public ISmFinderSupport getSmFinderSupport() {
	    return smFinderSupport;
	}
	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
	    this.smFinderSupport = smFinderSupport;
	}
	public IUmFinderSupport getUmFinderSupport() {
	    return umFinderSupport;
	}
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
	    this.umFinderSupport = umFinderSupport;
	}

	@Override
	public final IBmFinderSupport getBmFinderSupport() {
	    return bmFinderSupport;
	}
	public void setBmFinderSupport(IBmFinderSupport bmFinderSupport) {
	    this.bmFinderSupport = bmFinderSupport;
	}


	@Override
	public final String nextvalByHeadBlId() {
		return this.getHeadBlNumberingDao().nextval();
	}

	@Override
	public final String nextvalByLotBlId() {
		return this.getLotBlNumberingDao().nextval();
	}

	@Override
	public final String nextvalByLotId() {
		return this.getLotNumberingDao().nextval();
	}
	@Override
	public final String nextvalByPjtId() {
		return this.getPjtNumberingDao().nextval();
	}
	@Override
	public final String nextvalByPjtAvlSeqNo() {
		return this.getPjtAvlNumberingDao().nextval();
	}
	@Override
	public final String nextvalByLendingReqId() {
		return this.getLendingReqNumberingDao().nextval();
	}
	@Override
	public final String nextvalByAttachedFileSeqNo(int seq) {
		return this.getAreqAttachedFileNumberingDao().nextval(seq);
	}
	@Override
	public final String nextvalByMultipleAttachedFileSeqNo(String areqId) {
		int seq = 1;
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqAttachedFileItems.attachedFileSeqNo, TriSortOrder.Desc, 1);
		AreqAttachedFileCondition fileCondition = new AreqAttachedFileCondition();
		fileCondition.setAreqId(areqId);

		List<IAreqAttachedFileEntity> attachedFileList = this.getAreqAttachedFileDao().find(fileCondition.getCondition(), sort);
		if (TriStringUtils.isNotEmpty(attachedFileList)) {
			seq = Integer.parseInt(attachedFileList.get(0).getAttachedFileSeqNo());
			seq++;
		}

		DecimalFormat format = new DecimalFormat("0000");
		return format.format(seq);
	}

	@Override
	public Map<String, IExtEntity> getExtMap() {
		Map<String, IExtEntity> map = new LinkedHashMap<String, IExtEntity>();

		ExtCondition condition = new ExtCondition() ;
		ISqlSort sort = new SortBuilder() ;
		sort.setElement( ExtItems.ext, TriSortOrder.Asc , 1 ) ;

		List<IExtEntity> entities = this.getExtDao().find( condition.getCondition(), sort );

		for ( IExtEntity entity : entities ) {
			map.put( ( null == entity.getExt() )? "": entity.getExt(), entity );
		}

		return map;
	}

	@Override
	public IBldEnvEntity[] getAccessableRelEnvEntity( IBldEnvEntity[] envAllEntities, List<ILotDto> lotList ) {
		Map<String, IBldEnvEntity> envMap = new HashMap<String, IBldEnvEntity>();
		List<IBldEnvEntity> envList = new ArrayList<IBldEnvEntity>();

		for ( IBldEnvEntity env : envAllEntities ) {
			envMap.put(env.getBldEnvId(), env);
		}

		for ( String envNo: convertToEnvNo( envAllEntities, lotList ) ) {
			envList.add( envMap.get( envNo ) );
		}

		return envList.toArray( new IBldEnvEntity[0] );
	}

	@Override
	public List<ILotEntity> getAccessableLotNumbers(
			IJdbcCondition condition,
			ISqlSort sort,
			GenericServiceBean retBean,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage ) {
		return this.getAccessableLotNumbers(condition, sort, retBean, true, disableLinkLotNumbers, isSetMessage);
	}
	@Override
	public boolean setAccessableLotNumbers(
			IJdbcCondition condition, GenericServiceBean retBean, List<String> disableLinkLotNumbers, boolean isSetMessage ) {
		return this.setAccessableLotNumbers(condition, retBean, true, disableLinkLotNumbers, isSetMessage);
	}

	@Override
	public boolean setAccessableLotNumbers(
			IJdbcCondition condition,
			GenericServiceBean retBean,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage ) {

		List<ILotEntity> lotArrays = this.getAccessableLotNumbers(condition, null, retBean, isIncludeAllowList, disableLinkLotNumbers, isSetMessage);

		if ( TriStringUtils.isEmpty( lotArrays ) ) {
			//ダミーを設定して、全件検索を防止する。
			((LotCondition)condition).setLotIds( new String[]{ "" });
			return false;
		} else {

			((LotCondition)condition).setLotIds( this.convertToLotNo( lotArrays ) );
			return true;
		}
	}

	@Override
	public List<ILotEntity> getAccessableLotNumbers(
			IJdbcCondition condition,
			ISqlSort sort,
			GenericServiceBean retBean,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage ) {

		List<ILotEntity> lotEntities = this.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
		if ( ! ScreenType.bussinessException.equals( retBean.getScreenType() ) ) {
			if ( isSetMessage && TriStringUtils.isEmpty( lotEntities )) {
				retBean.setInfoMessage( AmMessageId.AM001123E );
			}
		}
		List<ILotDto> lotDto = this.getAmFinderSupport().findLotDto(lotEntities);

		// ユーザが所属しているグループ
		List<String> groupIdList = new ArrayList<String>();
		List<IGrpUserLnkEntity> groupUserEntities = this.getUmFinderSupport().findGrpUserLnkByUserId( retBean.getUserId() );
		for ( IGrpUserLnkEntity groupUserEntity : groupUserEntities ) {
			groupIdList.add( groupUserEntity.getGrpId() );
		}


		List<ILotEntity> accessableLotList = new ArrayList<ILotEntity>();
		List<String> accessableLotNumbers = new ArrayList<String>();

		for ( ILotDto lot : lotDto ) {

			// アクセス許可グループ指定なし＝全員アクセス可能
			if ( TriStringUtils.isEmpty( lot.getLotGrpLnkEntities() )) {
				accessableLotList.add( lot.getLotEntity() );
				accessableLotNumbers.add( lot.getLotEntity().getLotId() );

			// グループ指定あり
			} else {
				// 自分の所属グループがロットにアクセス許可があるかチェック
				for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

					if ( groupIdList.contains( group.getGrpId() )) {
						accessableLotList.add( lot.getLotEntity() );
						accessableLotNumbers.add( lot.getLotEntity().getLotId() );
						break;
					}
				}
			}


			// 一覧表示許可あり
			if ( isIncludeAllowList ) {
				if ( !accessableLotNumbers.contains( lot.getLotEntity().getLotId() ) && StatusFlg.on.value().equals( lot.getLotEntity().getAllowListView().value() ) ) {

					accessableLotList.add( lot.getLotEntity() );
					accessableLotNumbers.add( lot.getLotEntity().getLotId() );
					disableLinkLotNumbers.add( lot.getLotEntity().getLotId() );
				}
			}
		}

		return accessableLotList;
	}

	@Override
	public String[] convertToLotNo( List<ILotEntity> pjtLotEntityArray ) {
		Set<String> lotNoSet = new LinkedHashSet<String>();

		for ( ILotEntity lot : pjtLotEntityArray ) {
			lotNoSet.add( lot.getLotId() );
		}

		return lotNoSet.toArray( new String[0] );
	}

	private String[] convertToEnvNo( IBldEnvEntity[] envAllEntities, List<ILotDto> lotList ) {
		Set<String> envNo = new HashSet<String>();
		List<String> envNoList = new ArrayList<String>();

		String[] envArray = convertToEnvNo( envAllEntities );

		for ( ILotDto lot : lotList ) {
			if ( TriStringUtils.isEmpty(lot.getLotRelEnvLnkEntities()) ) {
				envNo.addAll( FluentList.from(envArray).asList() );
			} else {
				for ( ILotRelEnvLnkEntity env : lot.getLotRelEnvLnkEntities() ) {
					envNo.add( env.getBldEnvId() );
				}
			}
		}

		//ソート順を保障する
		for ( IBldEnvEntity env: envAllEntities ) {
			if ( envNo.contains( env.getBldEnvId() ) ) {
				envNoList.add( env.getBldEnvId() );
			}
		}

		return envNoList.toArray( new String[0] );
	}
	/**
	 * ビルド環境エンティティからビルド環境IDの配列を取得する
	 *
	 * @param envEntities ビルド環境情報
	 * @return
	 */
	private static String[] convertToEnvNo( IBldEnvEntity[] envEntities ) {
		Set<String> envNo = new LinkedHashSet<String>();

		//ソート順を保障する
		for ( IBldEnvEntity env : envEntities ) {
			envNo.add( env.getBldEnvId() );
		}

		return envNo.toArray( new String[0] );
	}

	@Override
	public IHeadEntity findHeadEntity( VcsCategory vcsCategory ) throws TriSystemException {
		String vcsCtgCd = (null == vcsCategory)?
				DesignSheetFactory.getDesignSheet().getValue(AmDesignEntryKeyByScm.useApplication )
				: vcsCategory.value();

		HeadCondition condition = new HeadCondition();
		condition.setVcsCtgCd( vcsCtgCd );

		IHeadEntity entity = this.getHeadDao().findByPrimaryKey( condition.getCondition() );

		if ( null == entity )
			throw new TriSystemException(AmMessageId.AM004050F, AmTables.AM_HEAD.name() ,vcsCtgCd);

		return entity;
	}

	@Override
	public IVcsReposEntity findVcsReposEntity( String vcsReposId ) throws TriSystemException {
		if ( TriStringUtils.isEmpty(vcsReposId) ){
			throw new TriSystemException(AmMessageId.AM004051F, AmTables.AM_VCS_REPOS.name(), "empty");
		}

		VcsReposCondition reposCondition = new VcsReposCondition();
		reposCondition.setVcsReposId( vcsReposId );

		IVcsReposEntity reposEntity = this.getVcsReposDao().findByPrimaryKey( reposCondition.getCondition() );
		if ( null == reposEntity ) {
			throw new TriSystemException(AmMessageId.AM004051F, AmTables.AM_VCS_REPOS.name(), vcsReposId );
		}

		return reposEntity;
	}

	@Override
	public IVcsReposEntity findVcsReposEntityFromModuleNames( String... moduleNames ) throws TriSystemException {
		if ( TriStringUtils.isEmpty( moduleNames ) ) {
			throw new TriSystemException(AmMessageId.AM004052F, AmTables.AM_VCS_REPOS.name(), "empty" );
		}

		VcsMdlCondition mdlCondition = new VcsMdlCondition();
		if ( 1 == moduleNames.length ) {
			mdlCondition.setMdlNm( moduleNames[0] );
		} else {
			mdlCondition.setMdlNms( moduleNames );
		}

		List<IVcsMdlEntity> mdlEntities = this.getVcsMdlDao().find(mdlCondition.getCondition());

		if ( TriCollectionUtils.isEmpty(mdlEntities) )
			throw new TriSystemException(AmMessageId.AM004052F, AmTables.AM_VCS_REPOS.name(), moduleNames[0] );

		if ( 1 < mdlEntities.size() )
			throw new TriSystemException(AmMessageId.AM004053F, AmTables.AM_VCS_REPOS.name(), moduleNames[0] );

		return this.findVcsReposEntity( mdlEntities.get(0).getVcsReposId() );
	}

	@Override
	public final List<IVcsReposEntity> findVcsReposEntities() throws TriSystemException {
		VcsReposCondition reposCondition = new VcsReposCondition();

		List<IVcsReposEntity> reposEntities = this.getVcsReposDao().find( reposCondition.getCondition() );
		if ( 0 == reposEntities.size() ) {
			throw new TriSystemException( AmMessageId.AM004045F, AmTables.AM_VCS_MDL.name() );
		}

		return reposEntities;
	}

	/**
	 * VCS Module Entityを取得します。
	 * <br>モジュール名の昇順にソートされます。
	 *
	 * @param sort ソート順
	 * @param moduleNames モジュール名
	 * @return 取得したVCS Module Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	private final List<IVcsMdlEntity> findVcsMdlEntities( ISqlSort sort, String... moduleNames ) throws TriSystemException {
		VcsMdlCondition mdlCondition = new VcsMdlCondition();
		mdlCondition.setMdlNms(moduleNames);

		List<IVcsMdlEntity> mdlEntities = this.getVcsMdlDao().find(mdlCondition.getCondition(), sort);

		if ( TriCollectionUtils.isEmpty(mdlEntities) ) {
			throw new TriSystemException( AmMessageId.AM004054F, AmTables.AM_VCS_MDL.name() );
		}

		return mdlEntities;
	}

	/**
	 * VCS Module Entityを取得します。
	 *
	 * @param sort ソート順
	 * @return 取得したVCS Module Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public final List<IVcsMdlEntity> findVcsMdlEntities( ISqlSort sort ) throws TriSystemException {
		VcsMdlCondition mdlCondition = new VcsMdlCondition();

		List<IVcsMdlEntity> mdlEntities = this.getVcsMdlDao().find(mdlCondition.getCondition(), sort);

		if ( TriCollectionUtils.isEmpty(mdlEntities) ) {
			throw new TriSystemException( AmMessageId.AM004054F, AmTables.AM_VCS_MDL.name() );
		}

		return mdlEntities;
	}

	@Override
	public final IPassMgtEntity findPassMgtEntityBySVN() throws TriSystemException {
		String serverId = this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId();
		List<IVcsReposEntity> reposEntities = this.findVcsReposEntities();
		return  this.getSmFinderSupport().findPasswordEntity(PasswordCategory.SVN, serverId, reposEntities.get(0).getVcsUserNm());
	}

	@Override
	public final IHeadBlEntity findHeadBlEntity( String headBlId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(headBlId) ){
			throw new TriSystemException(AmMessageId.AM005152S, "empty");
		}
		HeadBlCondition condition = new HeadBlCondition();
		condition.setHeadBlId(headBlId);
		IHeadBlEntity headBlEntity = this.getHeadBlDao().findByPrimaryKey(condition.getCondition()) ;

		if ( null == headBlEntity ){
			throw new TriSystemException(AmMessageId.AM005152S , headBlId);
		}
		return headBlEntity;
	}

	/**
	 * HEAD・ベースライン・モジュール情報を取得します。
	 *
	 * @param headBlId HEAD・ベースラインID
	 * @return 取得したHEAD・ベースライン・モジュールエンティティを戻します。
	 */
	private final List<IHeadBlMdlLnkEntity> findHeadBlMdlLnkEntities( String headBlId ) {
		if ( TriStringUtils.isEmpty(headBlId) ){
			throw new TriSystemException(AmMessageId.AM005152S, "empty");
		}
		HeadBlMdlLnkCondition condition = new HeadBlMdlLnkCondition();
		condition.setHeadBlId(headBlId);
		List<IHeadBlMdlLnkEntity> entities = this.getHeadBlMdlLnkDao().find( condition.getCondition() );

		return entities;
	}
	/**
	 * HEAD・ベースライン・資産申請を取得します。
	 *
	 * @param headBlId HEAD・ベースラインID
	 * @return 取得したHEAD・ベースライン・資産申請エンティティを戻します。
	 */
	private final List<IHeadLotBlLnkEntity> findHeadLotBlLnkEntities( String headBlId ) {
		if ( TriStringUtils.isEmpty(headBlId) ){
			throw new TriSystemException(AmMessageId.AM005152S, "empty");
		}
		HeadLotBlLnkCondition condition = new HeadLotBlLnkCondition();
		condition.setHeadBlId(headBlId);
		List<IHeadLotBlLnkEntity> entities = this.getHeadLotBlLnkDao().find( condition.getCondition() );

		return entities;
	}

	@Override
	public final ILotBlEntity findLotBlEntity( String lotBlId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(lotBlId) ){
			throw new TriSystemException(AmMessageId.AM005147S, "empty");
		}
		LotBlCondition condition = new LotBlCondition();
		condition.setLotBlId(lotBlId);
		ILotBlEntity lotBlEntity = this.getLotBlDao().findByPrimaryKey(condition.getCondition()) ;

		if ( null == lotBlEntity ){
			throw new TriSystemException(AmMessageId.AM005147S , lotBlId);
		}
		return lotBlEntity;
	}

	@Override
	public List<ILotBlEntity> findLotBlEntities( String... lotBlIds ) {

		if ( TriStringUtils.isEmpty(lotBlIds) ){
			throw new TriSystemException(AmMessageId.AM005147S, "empty");
		}

		LotBlCondition condition = new LotBlCondition();
		if ( 1 == lotBlIds.length ) {
			condition.setLotBlId( lotBlIds[0] );
		} else {
			condition.setLotBlIds( lotBlIds );
		}

		List<ILotBlEntity> entities = this.getLotBlDao().find( condition.getCondition() );

		return entities;
	}

	/**
	 * ロット・ベースライン・モジュール情報を取得します。
	 *
	 * @param lotBlId ロット・ベースラインID
	 * @return 取得したロット・ベースライン・モジュールエンティティを戻します。
	 */
	private final List<ILotBlMdlLnkEntity> findLotBlMdlLnkEntities( String lotBlId ) {
		if ( TriStringUtils.isEmpty(lotBlId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotBlMdlLnkCondition condition = new LotBlMdlLnkCondition();
		condition.setLotBlId(lotBlId);
		List<ILotBlMdlLnkEntity> entities = this.getLotBlMdlLnkDao().find( condition.getCondition() );

		return entities;
	}
	/**
	 * ロット・ベースライン・資産申請を取得します。
	 *
	 * @param lotBlId ロット・ベースラインID
	 * @return 取得したロット・ベースライン・資産申請エンティティを戻します。
	 */
	private final List<ILotBlReqLnkEntity> findLotBlReqLnkEntities( String lotBlId ) {
		if ( TriStringUtils.isEmpty(lotBlId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotBlReqLnkCondition condition = new LotBlReqLnkCondition();
		condition.setLotBlId(lotBlId);
		List<ILotBlReqLnkEntity> entities = this.getLotBlReqLnkDao().find( condition.getCondition() );

		return entities;
	}

	@Override
	public final ILotEntity findLotEntity( String lotId ) throws TriSystemException {

		return this.findLotEntity(lotId, StatusFlg.off);
	}

	@Override
	public final ILotEntity findLotEntity( Map<String, ILotEntity> cache, String lotId ) throws TriSystemException {
		if ( null == cache || TriStringUtils.isEmpty(lotId) ) {
			return this.findLotEntity( lotId );
		}

		if ( cache.containsKey( lotId ) ) {
			return cache.get( lotId );
		}

		ILotEntity entity = this.findLotEntity( lotId );
		cache.put(lotId, entity);

		return entity;
	}

	@Override
	public final ILotEntity findLotEntity( String lotId, StatusFlg delStsId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(lotId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotCondition condition = new LotCondition();
		condition.setLotId(lotId);
		condition.setDelStsId(delStsId);
		ILotEntity lotEntity = this.getLotDao().findByPrimaryKey(condition.getCondition()) ;

		if ( null == lotEntity ){
			throw new TriSystemException(AmMessageId.AM005146S , lotId);
		}
		return lotEntity;
	}

	/**
	 * ロット・グループ情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したロット・グループエンティティを戻します。
	 */
	private final List<ILotGrpLnkEntity> findLotGrpLnkEntities( String lotId ) {
		if ( TriStringUtils.isEmpty(lotId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setLotId(lotId);
		List<ILotGrpLnkEntity> entities = this.getLotGrpLnkDao().find( condition.getCondition() );

		return entities;
	}
	/**
	 * ロット・メールグループ情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したロット・メールグループエンティティを戻します。
	 */
	private final List<ILotMailGrpLnkEntity> findLotMailGrpLnkEntities( String lotId ) {
		if ( TriStringUtils.isEmpty(lotId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotMailGrpLnkCondition condition = new LotMailGrpLnkCondition();
		condition.setLotId(lotId);
		List<ILotMailGrpLnkEntity> entities = this.getLotMailGrpLnkDao().find( condition.getCondition() );

		return entities;
	}
	/**
	 * ロット・モジュール情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したロットエンティティを戻します。
	 */
	private final List<ILotMdlLnkEntity> findLotMdlLnkEntities( String lotId ) {
		if ( TriStringUtils.isEmpty(lotId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotMdlLnkCondition condition = new LotMdlLnkCondition();
		condition.setLotId(lotId);
		List<ILotMdlLnkEntity> entities = this.getLotMdlLnkDao().find( condition.getCondition() );

		return entities;
	}
	/**
	 * ロット・リリース環境情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したロット・リリース環境エンティティを戻します。
	 */
	private final List<ILotRelEnvLnkEntity> findLotRelEnvLnkEntities( String lotId ) {
		if ( TriStringUtils.isEmpty(lotId) ){
			throw new TriSystemException(AmMessageId.AM005146S, "empty");
		}
		LotRelEnvLnkCondition condition = new LotRelEnvLnkCondition();
		condition.setLotId(lotId);
		List<ILotRelEnvLnkEntity> entities = this.getLotRelEnvLnkDao().find( condition.getCondition() );
		return entities;
	}

	@Override
	public final IPjtEntity findPjtEntity( String pjtId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(pjtId) ){
			throw new TriSystemException(AmMessageId.AM005145S, "empty");
		}
		PjtCondition condition = new PjtCondition();
		condition.setPjtId(pjtId);
		IPjtEntity pjtEntity = this.getPjtDao().findByPrimaryKey(condition.getCondition()) ;

		if ( null == pjtEntity ){
			throw new TriSystemException(AmMessageId.AM005145S , pjtId);
		}
		return pjtEntity;
	}

	@Override
	public final List<IPjtEntity> findPjtEntities( String... pjtIds ) {
		return this.findPjtEntities((ISqlSort)null, pjtIds);
	}

	@Override
	public final List<IPjtEntity> findPjtEntities( ISqlSort sort, String... pjtIds ) {

		if ( TriStringUtils.isEmpty( pjtIds ) ) {
			//ダミーの変更管理を設定して、全件検索を防止する。
			pjtIds = new String[]{ "" };
		}

		PjtCondition condition = new PjtCondition();
		condition.setPjtIds( pjtIds );

		List<IPjtEntity> pjtEntities = this.getPjtDao().find(condition.getCondition(), sort);

		return pjtEntities;
	}

	@Override
	public final IPjtAvlEntity findPjtAvlEntity( String pjtId, String seqNo ) {

		return this.findPjtAvlEntity(pjtId, seqNo, StatusFlg.off);
	}

	@Override
	public final IPjtAvlEntity findPjtAvlEntity( String pjtId, String seqNo, StatusFlg delStsId ) {

		if ( TriStringUtils.isEmpty( pjtId ) ){
			throw new TriSystemException(AmMessageId.AM005145S, "empty");
		}

		if ( TriStringUtils.isEmpty(seqNo) ) {
			return null;
		}

		PjtAvlCondition condition = new PjtAvlCondition();
		condition.setPjtId(pjtId);
		condition.setPjtAvlSeqNo(seqNo);
		condition.setDelStsId(delStsId);

		return this.getPjtAvlDao().findByPrimaryKey( condition.getCondition() );
	}

	@Override
	public final List<IPjtAvlEntity> findPjtAvlEntities( String pjtId ) {

		if ( TriStringUtils.isEmpty( pjtId ) ){
			throw new TriSystemException(AmMessageId.AM005145S, "empty");
		}

		PjtAvlCondition condition = new PjtAvlCondition();
		condition.setPjtId(pjtId);

		return this.getPjtAvlDao().find( condition.getCondition() );
	}

	@Override
	public final List<IPjtAvlAreqLnkEntity> findPjtAvlAreqLnkEntities( String pjtId, String seqNo, String... areqIds ) {

		if ( TriStringUtils.isEmpty( pjtId ) ){
			throw new TriSystemException(AmMessageId.AM005145S, "empty");
		}

		if ( TriStringUtils.isEmpty(seqNo) ) {
			return new ArrayList<IPjtAvlAreqLnkEntity>();
		}

		PjtAvlAreqLnkCondition condition = new PjtAvlAreqLnkCondition();

		condition.setPjtId(pjtId);
		condition.setPjtAvlSeqNo(seqNo);

		if ( ! TriStringUtils.isNotEmpty(areqIds) ) {
			if ( 1 == areqIds.length ) {
				condition.setAreqId( areqIds[0] );
			} else {
				condition.setAreqIds( areqIds );
			}
		}

		return this.getPjtAvlAreqLnkDao().find( condition.getCondition() );
	}

	@Override
	public final IAreqEntity findAreqEntity( String areqId ) throws TriSystemException {

		return this.findAreqEntity(areqId, StatusFlg.off);
	}

	@Override
	public final IAreqEntity findAreqEntity( String areqId, StatusFlg delStsId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty( areqId ) ){
			throw new TriSystemException(AmMessageId.AM005144S, "empty");
		}
		AreqCondition condition = new AreqCondition();
		condition.setAreqId(areqId);
		condition.setDelStsId(delStsId);
		IAreqEntity entity = this.getAreqDao().findByPrimaryKey( condition.getCondition() );

		if ( null == entity ){
			throw new TriSystemException(AmMessageId.AM005144S , areqId );
		}
		return entity;
	}

	@Override
	public final List<IAreqEntity> findAreqEntities( String... areqIds ) {
		return this.findAreqEntities((ISqlSort)null, areqIds);
	}

	@Override
	public final List<IAreqEntity> findAreqEntities( ISqlSort sort, String... areqIds ) {

		AreqCondition condition = new AreqCondition();

		if ( TriStringUtils.isEmpty( areqIds ) ) {
			//全件検索を防止するため、ダミーを設定。
			areqIds = new String[]{ "" };
		}
		condition.setAreqIds( areqIds );

		List<IAreqEntity> entities = this.getAreqDao().find( condition.getCondition() );

		return entities;
	}

	@Override
	public final List<IAreqFileEntity> findAreqFileEntities( String areqId, AreqCtgCd... areqCtgCds ) {
		return this.findAreqFileEntities( areqId, StatusFlg.off, areqCtgCds );
	}

	@Override
	public final List<IAreqFileEntity> findAreqFileEntities( String areqId, StatusFlg delStsId, AreqCtgCd... areqCtgCds ) {

		if ( TriStringUtils.isEmpty( areqId ) ){
			throw new TriSystemException(AmMessageId.AM005144S, "empty");
		}

		AreqFileCondition condition = new AreqFileCondition();
		condition.setAreqId(areqId);
		condition.setDelStsId(delStsId);

		if ( ! TriStringUtils.isEmpty(areqCtgCds) ) {
			if ( 1 == areqCtgCds.length ) {
				condition.setAreqCtgCd( areqCtgCds[0].value() );
			} else {
				condition.setAreqCtgCds(
						FluentList.from(areqCtgCds).map( AmFluentFunctionUtils.toValueFromAreqCtgCd ).toArray(new String[0]) );
			}
		}

		ISqlSort sortBuilder = new SortBuilder();
		sortBuilder.setElement(AreqFileItems.areqCtgCd, TriSortOrder.Asc, 1);
		sortBuilder.setElement(AreqFileItems.areqFileSeqNo, TriSortOrder.Asc, 2);

		return this.getAreqFileDao().find( condition.getCondition(), sortBuilder );
	}

	@Override
	public final List<IAreqBinaryFileEntity> findAreqBinaryFileEntities( String areqId, AreqCtgCd... areqCtgCds ) {
		return this.findAreqBinaryFileEntities( areqId, StatusFlg.off, areqCtgCds );
	}

	@Override
	public final List<IAreqBinaryFileEntity> findAreqBinaryFileEntities( String areqId, StatusFlg delStsId, AreqCtgCd... areqCtgCds ) {

		if ( TriStringUtils.isEmpty( areqId ) ){
			throw new TriSystemException(AmMessageId.AM005144S, "empty");
		}

		AreqBinaryFileCondition condition = new AreqBinaryFileCondition();
		condition.setAreqId(areqId);
		condition.setDelStsId(delStsId);

		if ( ! TriStringUtils.isEmpty(areqCtgCds) ) {
			if ( 1 == areqCtgCds.length ) {
				condition.setAreqCtgCd( areqCtgCds[0].value() );
			} else {
				condition.setAreqCtgCds(
						FluentList.from(areqCtgCds).map( AmFluentFunctionUtils.toValueFromAreqCtgCd ).toArray(new String[0]) );
			}
		}

		return this.getAreqBinaryFileDao().find( condition.getCondition() );
	}

	@Override
	public final List<IAreqAttachedFileEntity> findAreqAttachedFileEntities( String areqId ) {
		return this.findAreqAttachedFileEntities( areqId, StatusFlg.off );
	}

	@Override
	public final List<IAreqAttachedFileEntity> findAreqAttachedFileEntities( String areqId, StatusFlg delStsId ) {

		if ( TriStringUtils.isEmpty( areqId ) ){
			throw new TriSystemException(AmMessageId.AM005144S, "empty");
		}

		AreqAttachedFileCondition condition = new AreqAttachedFileCondition();
		condition.setAreqId(areqId);
		condition.setDelStsId(delStsId);

		return this.getAreqAttachedFileDao().find( condition.getCondition() );
	}

	/**
	 *HEAD・ベースラインEntityの子Entityを取得しDTOに設定します。
	 *
	 * @param headBlDto ロット・ベースラインDTO
	 * @param headBlEntity ロット・ベースラインEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IHeadBlDto setHeadBlDto( IHeadBlDto headBlDto, IHeadBlEntity headBLEntity, AmTables... tables ) {
		headBlDto.setHeadBlEntity( headBLEntity );

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_HEAD_BL_MDL_LNK,
					AmTables.AM_HEAD_LOT_BL_LNK,
			};
		}

		String headBlId = headBLEntity.getHeadBlId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_HEAD_BL_MDL_LNK ) ) {
				headBlDto.setHeadBlMdlLnkEntities(this.findHeadBlMdlLnkEntities( headBlId ));
			}
			if ( table.equals( AmTables.AM_HEAD_LOT_BL_LNK ) ) {
				headBlDto.setHeadLotBlLnkEntities(this.findHeadLotBlLnkEntities( headBlId ));
			}
		}

		return headBlDto;
	}

	@Override
	public List<IVcsReposDto> findVcsReposDto() {
		SortBuilder sort = new SortBuilder();
		sort.setElement(VcsMdlItems.mdlNm, TriSortOrder.Asc, 1);

		return findVcsReposDto( sort );
	}

	@Override
	public List<IVcsReposDto> findVcsReposDto( ISqlSort sort ) {
		List<IVcsReposDto> reposDtoList = new ArrayList<IVcsReposDto>();

		for ( IVcsMdlEntity mdlEntity: this.findVcsMdlEntities( sort ) ) {
			IVcsReposDto vcsReposDto = new VcsReposDto();
			vcsReposDto.setVcsReposEntity( this.findVcsReposEntity( mdlEntity.getVcsReposId() ) );
			vcsReposDto.setVcsMdlEntity( mdlEntity );
			reposDtoList.add( vcsReposDto );
		}

		return reposDtoList;
	}

	@Override
	public List<IVcsReposDto> findVcsReposDtoFromLotMdlLnkEntities( List<ILotMdlLnkEntity> mdlEntities ) {
		List<IVcsReposDto> reposDtoList = new ArrayList<IVcsReposDto>();

		SortBuilder sort = new SortBuilder();
		sort.setElement(VcsMdlItems.mdlNm, TriSortOrder.Asc, 1);

		if ( TriCollectionUtils.isEmpty(mdlEntities) ) {
			throw new TriSystemException(AmMessageId.AM004052F, AmTables.AM_VCS_MDL.name(), "empty");
		}

		String[] mdlNms = FluentList.from( mdlEntities ).map( AmFluentFunctionUtils.toMdlNmsFromLotMdlLnk ).toArray(new String[0]);

		for ( IVcsMdlEntity mdlEntity: this.findVcsMdlEntities( sort, mdlNms ) ) {
			IVcsReposDto vcsReposDto = new VcsReposDto();
			vcsReposDto.setVcsReposEntity( this.findVcsReposEntity( mdlEntity.getVcsReposId() ) );
			vcsReposDto.setVcsMdlEntity( mdlEntity );
			reposDtoList.add( vcsReposDto );
		}

		return reposDtoList;
	}

	@Override
	public final IHeadBlDto findHeadBlDto( String headBlId, AmTables... tables ) throws TriSystemException {
		IHeadBlEntity headBlEntity = findHeadBlEntity( headBlId );
		return this.setHeadBlDto(new HeadBlDto(), headBlEntity, tables);
	}

	@Override
	public final IHeadBlDto findHeadBlDto( IHeadBlEntity headBlEntity, AmTables... tables ) throws TriSystemException {
		return this.setHeadBlDto(new HeadBlDto(), headBlEntity, tables);
	}

	/**
	 *ロット・ベースラインEntityの子Entityを取得しDTOに設定します。
	 *
	 * @param lotBlDto ロット・ベースラインDTO
	 * @param lotBlEntity ロット・ベースラインEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final ILotBlDto setLotBlDto( ILotBlDto lotBlDto, ILotBlEntity lotBlEntity, AmTables... tables ) {
		lotBlDto.setLotBlEntity( lotBlEntity );

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_LOT_BL_MDL_LNK,
					AmTables.AM_LOT_BL_REQ_LNK,
			};
		}

		String lotBlId = lotBlEntity.getLotBlId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_LOT_BL_MDL_LNK ) ) {
				lotBlDto.setLotBlMdlLnkEntities(this.findLotBlMdlLnkEntities( lotBlId ));
			}
			if ( table.equals( AmTables.AM_LOT_BL_REQ_LNK ) ) {
				lotBlDto.setLotBlReqLnkEntities(this.findLotBlReqLnkEntities( lotBlId ));
			}
		}

		return lotBlDto;
	}

	@Override
	public final ILotBlDto findLotBlDto( String lotBlId, AmTables... tables ) throws TriSystemException {
		ILotBlEntity lotBLEntity = findLotBlEntity( lotBlId );
		return this.setLotBlDto(new LotBlDto(), lotBLEntity, tables);
	}

	@Override
	public final List<ILotBlDto> findLotBlDto( List<ILotBlEntity> entities, AmTables... tables ) {
		List<ILotBlDto> lotBlDtoList = new ArrayList<ILotBlDto>();

		for ( ILotBlEntity entity: entities ) {
			lotBlDtoList.add( this.setLotBlDto( new LotBlDto(), entity, tables) );
		}

		return lotBlDtoList;
	}

	/**
	 *ロットEntityの子Entityを取得しDTOに設定します。
	 *
	 * @param lotDto ロットDTO
	 * @param lotEntity ロットEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final ILotDto setLotDto( ILotDto lotDto, ILotEntity lotEntity, AmTables... tables ) {
		lotDto.setLotEntity( lotEntity );

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_LOT_GRP_LNK,
					AmTables.AM_LOT_MAIL_GRP_LNK,
					AmTables.AM_LOT_MDL_LNK,
					AmTables.AM_LOT_REL_ENV_LNK,
			};
		}

		String lotId = lotEntity.getLotId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_LOT_GRP_LNK ) ) {
				lotDto.setLotGrpLnkEntities(this.findLotGrpLnkEntities( lotId ));
			}
			if ( table.equals( AmTables.AM_LOT_MAIL_GRP_LNK ) ) {
				lotDto.setLotMailGrpLnkEntities(this.findLotMailGrpLnkEntities( lotId ));
			}
			if ( table.equals( AmTables.AM_LOT_MDL_LNK ) ) {
				lotDto.setLotMdlLnkEntities(this.findLotMdlLnkEntities( lotId ));
			}
			if ( table.equals( AmTables.AM_LOT_REL_ENV_LNK ) ) {
				lotDto.setLotRelEnvLnkEntities(this.findLotRelEnvLnkEntities( lotId ));
			}
		}

		return lotDto;
	}

	@Override
	public final ILotDto findLotDto( String lotId, AmTables... tables ) throws TriSystemException {
		ILotEntity lotEntity = findLotEntity( lotId );
		return this.setLotDto(new LotDto(), lotEntity, tables);
	}

	@Override
	public final ILotDto findLotDto( ILotEntity lotEntity, AmTables... tables ) {
		return this.setLotDto(new LotDto(), lotEntity, tables);
	}

	@Override
	public final ILotDto findLotDto( ILotDto lotDto, AmTables... tables ) {
		return this.setLotDto(lotDto, lotDto.getLotEntity(), tables);
	}

	@Override
	public final List<ILotDto> findLotDto( List<ILotEntity> entities, AmTables... tables ) {

		List<ILotDto> lotDtoList = new ArrayList<ILotDto>();

		for ( ILotEntity lotEntity : entities ) {
			lotDtoList.add( this.findLotDto(lotEntity, tables) );
		}

		return lotDtoList;
	}

	/**
	 * 資産申請Entityの子Entityを取得しDTOに設定します。
	 *
	 * @param areqDto  資産申請DTO
	 * @param areqEntity 資産申請Entity
	 * @param delStsId 削除ステータスID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IAreqDto setAreqDto( IAreqDto areqDto, IAreqEntity areqEntity, StatusFlg delStsId, AmTables... tables ) {
		areqDto.setAreqEntity( areqEntity );

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_AREQ_FILE,
					AmTables.AM_AREQ_BINARY_FILE,
					AmTables.AM_AREQ_ATTACHED_FILE,
			};
		}

		String areqId = areqEntity.getAreqId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_AREQ_FILE ) ) {
				areqDto.setAreqFileEntities(this.findAreqFileEntities( areqId, delStsId ));
			}
			if ( table.equals( AmTables.AM_AREQ_BINARY_FILE ) ) {
				areqDto.setAreqBinaryFileEntities(this.findAreqBinaryFileEntities( areqId, delStsId ));
			}
			if ( table.equals( AmTables.AM_AREQ_ATTACHED_FILE ) ) {
				areqDto.setAreqAttachedFileEntities(this.findAreqAttachedFileEntities( areqId, delStsId ));
			}
		}

		return areqDto;
	}

	@Override
	public final IAreqDto findAreqDto( String areqId, AmTables... tables ) throws TriSystemException {
		IAreqEntity areqEntity = findAreqEntity( areqId );
		return this.setAreqDto(new AreqDto(), areqEntity, StatusFlg.off, tables);
	}

	@Override
	public final IAreqDto findAreqDto( String areqId, StatusFlg delStsId, AmTables... tables ) throws TriSystemException {
		IAreqEntity areqEntity = findAreqEntity( areqId, delStsId );
		return this.setAreqDto(new AreqDto(), areqEntity, delStsId, tables);
	}

	@Override
	public final IAreqDto findAreqDto( IAreqEntity areqEntity, AmTables... tables ) throws TriSystemException {
		return this.setAreqDto(new AreqDto(), areqEntity, StatusFlg.off, tables);
	}

	@Override
	public List<IAreqDto> findAreqDto( List<IAreqEntity> entities, AmTables... tables ) throws TriSystemException {

		List<IAreqDto> areqDtoList = new ArrayList<IAreqDto>();

		for ( IAreqEntity areqEntity : entities ) {
			areqDtoList.add( this.findAreqDto(areqEntity, tables) );
		}

		return areqDtoList;
	}

	public IAreqDto findAreqDto( String areqId ,AreqCtgCd ctgCd ) {

		IAreqDto areqDto = new AreqDto();
		areqDto.setAreqEntity( this.findAreqEntity( areqId ) );
		areqDto.setAreqFileEntities( this.findAreqFileEntities( areqId, ctgCd ) );
		areqDto.setAreqBinaryFileEntities (this.findAreqBinaryFileEntities( areqId, ctgCd ) );
		areqDto.setAreqAttachedFileEntities( this.findAreqAttachedFileEntities( areqId ) );

		return areqDto;
	}

	/**
	 * 資産申請Dtoのリストを取得します。
	 * @param areqEntities
	 * @return 資産申請Dtoのリスト
	 */
	public AreqDtoList findAreqDtoList( List<IAreqEntity> areqEntities ) {

		AreqDtoList areqDtoList = new AreqDtoList();

		for (IAreqEntity entity : areqEntities) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( entity );
			areqDto.setAreqFileEntities( this.findAreqFileEntities( entity.getAreqId() ) );
			areqDto.setAreqBinaryFileEntities (this.findAreqBinaryFileEntities( entity.getAreqId() ) );
			areqDto.setAreqAttachedFileEntities( this.findAreqAttachedFileEntities( entity.getAreqId() ) );
			areqDtoList.add( areqDto );
		}

		return areqDtoList;
	}

	/**
	 * 変更承認Entityの子Entityを取得しDTOに設定します。
	 *
	 * @param pjtAvlDto  変更承認DTO
	 * @param pjtEntity 変更情報Entity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IPjtAvlDto setPjtAvlDto( IPjtAvlDto pjtAvlDto, IPjtEntity pjtEntity, AmTables... tables ) {
		pjtAvlDto.setPjtEntity( pjtEntity );

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_PJT_AVL,
					AmTables.AM_PJT_AVL_AREQ_LNK,
			};
		}

		String pjtId = pjtEntity.getPjtId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_PJT_AVL ) ) {
				pjtAvlDto.setPjtAvlEntity(this.findPjtAvlEntity( pjtId, pjtEntity.getPjtLatestAvlSeqNo() ));
			}
			if ( table.equals( AmTables.AM_PJT_AVL_AREQ_LNK ) ) {
				pjtAvlDto.setPjtAvlAreqLnkEntityList(this.findPjtAvlAreqLnkEntities( pjtId, pjtEntity.getPjtLatestAvlSeqNo() ));
			}
		}

		return pjtAvlDto;
	}

	/**
	 * 変更承認Entityの子Entityを取得しDTOに設定します。
	 *
	 * @param pjtAvlDto  変更承認DTO
	 * @param pjtEntity 変更情報Entity
	 * @param pjtAvlEntity 変承認更情報Entity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IPjtAvlDto setPjtAvlDto( IPjtAvlDto pjtAvlDto, IPjtEntity pjtEntity, IPjtAvlEntity pjtAvlEntity, AmTables... tables ) {
		pjtAvlDto.setPjtEntity( pjtEntity );
		pjtAvlDto.setPjtAvlEntity( pjtAvlEntity );

		if ( null == pjtAvlEntity ) {
			return pjtAvlDto;
		}

		AmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new AmTables[]{
					AmTables.AM_PJT_AVL_AREQ_LNK,
			};
		}

		String pjtId = pjtEntity.getPjtId();

		for ( AmTables table: findTables ) {
			if ( table.equals( AmTables.AM_PJT_AVL_AREQ_LNK ) ) {
				pjtAvlDto.setPjtAvlAreqLnkEntityList(this.findPjtAvlAreqLnkEntities( pjtId, pjtAvlEntity.getPjtAvlSeqNo() ));
			}
		}

		return pjtAvlDto;
	}

	@Override
	public final IPjtAvlDto findPjtAvlDtoByLatest( String pjtId, AmTables... tables ) throws TriSystemException {
		IPjtEntity pjtEntity = findPjtEntity( pjtId );
		return this.setPjtAvlDto(new PjtAvlDto(), pjtEntity, tables);
	}

	@Override
	public final IPjtAvlDto findPjtAvlDtoByLatest( IPjtEntity pjtEntity, AmTables... tables ) throws TriSystemException {
		return this.setPjtAvlDto(new PjtAvlDto(), pjtEntity, tables);
	}

	@Override
	public final IPjtAvlDto findPjtAvlDto( String pjtId, String pjtAvlSeqNo, AmTables... tables ) throws TriSystemException {
		IPjtEntity pjtEntity = findPjtEntity( pjtId );
		IPjtAvlEntity pjtAvlEntity = this.findPjtAvlEntity(pjtId, pjtAvlSeqNo);

		return this.setPjtAvlDto(new PjtAvlDto(), pjtEntity, pjtAvlEntity, tables);
	}

	@Override
	public final List<IPjtAvlDto> findPjtAvlDto( String pjtId, AmTables... tables ) throws TriSystemException {
		List<IPjtAvlDto> pjtAvlDtoList = new ArrayList<IPjtAvlDto>();

		IPjtEntity pjtEntity = findPjtEntity( pjtId );

		for ( IPjtAvlEntity avlEntity: this.findPjtAvlEntities(pjtId) ) {
			pjtAvlDtoList.add( this.setPjtAvlDto(new PjtAvlDto(), pjtEntity, avlEntity, tables) );
		}

		return pjtAvlDtoList;
	}

	@Override
	public List<IPjtAvlDto> findPjtAvlDto( List<IPjtAvlEntity> pjtAvlEntities, AmTables... tables ) throws TriSystemException {
		List<IPjtAvlDto> pjtAvlDtoList = new ArrayList<IPjtAvlDto>();

		for ( IPjtAvlEntity avlEntity: pjtAvlEntities ) {
			IPjtEntity pjtEntity = findPjtEntity( avlEntity.getPjtId(), (StatusFlg)null );
			pjtAvlDtoList.add( this.setPjtAvlDto(new PjtAvlDto(), pjtEntity, avlEntity, tables) );
		}

		return pjtAvlDtoList;
	}

	public final IPjtEntity findPjtEntity( String pjtId, StatusFlg delStsId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(pjtId) ){
			throw new TriSystemException(AmMessageId.AM005145S, "empty");
		}
		PjtCondition condition = new PjtCondition();
		condition.setPjtId(pjtId);
		condition.setDelStsId(delStsId);
		IPjtEntity pjtEntity = this.getPjtDao().findByPrimaryKey(condition.getCondition()) ;

		if ( null == pjtEntity ){
			throw new TriSystemException(AmMessageId.AM005145S , pjtId);
		}

		return pjtEntity;
	}

	@Override
	public void cleaningAreq( String areqId) {
		AreqCondition condition = new AreqCondition();
		condition.setAreqId( areqId );

		IAreqEntity areqEntity = new AreqEntity();
		areqEntity.setAreqId(areqId);
		areqEntity.setDelStsId(StatusFlg.on);

		this.getAreqDao().update(condition.getCondition(), areqEntity);

		this.cleaningAreqFile(areqId);
		this.cleaningAreqBinaryFile(areqId);
		this.cleaningAreqAttachedFile(areqId);
	}


	private void cleaningAreqFile( String areqId ) {
		AreqFileCondition condition = new AreqFileCondition();
		condition.setAreqId( areqId );

		IAreqFileEntity areqFileEntity = new AreqFileEntity();
		areqFileEntity.setAreqId(areqId);
		areqFileEntity.setDelStsId(StatusFlg.on);

		this.getAreqFileDao().update(condition.getCondition(), areqFileEntity);
	}

	private void cleaningAreqBinaryFile( String areqId ) {
		AreqBinaryFileCondition condition = new AreqBinaryFileCondition();
		condition.setAreqId( areqId );

		IAreqBinaryFileEntity areqBinaryFileEntity = new AreqBinaryFileEntity();
		areqBinaryFileEntity.setAreqId(areqId);
		areqBinaryFileEntity.setDelStsId(StatusFlg.on);

		this.getAreqBinaryFileDao().update(condition.getCondition(), areqBinaryFileEntity);
	}

	private void cleaningAreqAttachedFile( String areqId) {
		AreqAttachedFileCondition condition = new AreqAttachedFileCondition();
		condition.setAreqId( areqId );

		IAreqAttachedFileEntity areqAttachedFileEntity = new AreqAttachedFileEntity();
		areqAttachedFileEntity.setAreqId(areqId);
		areqAttachedFileEntity.setDelStsId(StatusFlg.on);

		this.getAreqAttachedFileDao().update(condition.getCondition(), areqAttachedFileEntity);
	}

	public List<String> findBpIdsByLotBlId( String lotBlId ) {
		LotBlCondition condition = new LotBlCondition();
		condition.setLotBlId( lotBlId );

		List<ILotBlEntity> lotBlEntities = this.getLotBlDao().find( condition.getCondition() );

		if (TriStringUtils.isEmpty( lotBlEntities )) {
			return null;
		}

		return FluentList.from( lotBlEntities ).map( AmFluentFunctionUtils.toBpIdsFromLotBl).asList();
	}

}
