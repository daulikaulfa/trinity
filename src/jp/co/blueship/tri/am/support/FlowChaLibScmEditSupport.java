package jp.co.blueship.tri.am.support;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.ScmParamFile;
import jp.co.blueship.tri.fw.vcs.ScmParamFile.ObjType;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;


/**
 * Ｓｃｍ系イベントのサポートClass
 * <br>
 * <p>
 * Ｓｃｍ系の操作を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibScmEditSupport extends AmFinderSupport /* extends FinderSupport */ {

//	private static final String SEP = SystemProps.LineSeparator.getProperty();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * SVNのワーキングコピーに対し、ステータス取得を行う。
	 * @param svnUtil
	 * @param workPath
	 * @param results
	 * @param statusMap
	 * @throws ScmException
	 * @throws ScmSysException
	 */
	public final static void getStatus( IVcsService vcsService ,
											String workPath ,
											IFileResult[] results ,
											Map<String, SVNStatus> statusMap ) throws IOException , ScmException, ScmSysException {

		String warterMark = sheet.getValue( AmDesignEntryKeyByScm.waterMarkStatusMget );
		if( ! TriStringUtils.isEmpty( warterMark ) &&
				Integer.parseInt( warterMark ) < results.length ) {//モジュール一括で取得：少量資産の場合は遅くなる

			Set<String> moduleNameSet = new LinkedHashSet<String>() ;
			for( IFileResult result : results ) {
				moduleNameSet.add( StringUtils.substringBefore( TriStringUtils.convertPath( result.getFilePath() ) , "/" ) ) ;
			}
			Iterator<String> iter = moduleNameSet.iterator() ;
			while( iter.hasNext() ) {
				statusMap.putAll( vcsService.statusAll(setStatusAllVO(workPath,iter.next())) ) ;
			}

		} else {//資産１件ごとに取得：大量資産貸出時は一括より遅くなる

			List<ScmParamFile> extractFileList	= new ArrayList<ScmParamFile>() ;
			for ( IFileResult result : results ) {
				ScmParamFile scmParamFile = new ScmParamFile() ;
				scmParamFile.setObjName( result.getFilePath() ) ;
				scmParamFile.setObjType( ObjType.unknown ) ;
				extractFileList.add( scmParamFile );
			}
			for ( ScmParamFile scmParamFile : extractFileList ) {
				String filePath = scmParamFile.getObjName() ;
				SVNStatus status = null;
				try {
					status = vcsService.statusSingle(setStatusSingleVO(workPath,filePath));
				} catch ( ScmException e ) {
					//throw new ScmSysException ( "SvnKitによる処理実行時にエラーが発生しました。" , e ) ;
					//存在しないフォルダ下の資産のリビジョンを取得しようとした場合に例外を発生するため、除外。
					//新規→削除を一度もHEAD原本化せずに行うことにより、ロットもHEADも資産なしの場合に起こる。
					continue ;
				}
				String absolutePath = TriStringUtils.convertPath( status.getFile().getCanonicalPath() ) ;
				statusMap.put( absolutePath , status ) ;
			}
		}
	}
	/*
	 * ValueObjectにパラメータをセットするprivateメソッド
	 */
	private static StatusVO setStatusAllVO(String pathDest,String moduleName){

		StatusVO vo = new StatusVO();
		vo.setPathDest(pathDest);
		vo.setModuleName(moduleName);
		return vo;
	}

	/*
	 * ValueObjectにパラメータをセットするprivateメソッド
	 */
	private static StatusVO setStatusSingleVO(String pathDest,String filePath){

		StatusVO vo = new StatusVO();
		vo.setPathDest(pathDest);
		vo.setObjectPath(filePath);
		return vo;
	}
}
