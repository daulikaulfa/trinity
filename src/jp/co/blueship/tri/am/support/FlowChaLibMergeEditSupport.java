package jp.co.blueship.tri.am.support;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * HEAD原本マージ処理のサポートClass<br>
 *
 * <br>
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMergeEditSupport extends AmFinderSupport {

	/**
	 *
	 * 変更管理に紐付く申請情報を取得する
	 *
	 * @param pjtIds 変更情報ID
	 * @param sort ソート条件
	 * @return 資産申請情報EntityのList
	 */
	public final List<IAreqEntity> getAreqEntitiesFromPjtIds( String[] pjtIds, ISqlSort sort ) {

		AreqCondition condition = new AreqCondition();

		if ( TriStringUtils.isEmpty( pjtIds ) ) {
			//全件検索を防止するため、ダミーを設定。
			pjtIds = new String[]{ "" };
		}
		condition.setPjtIds( pjtIds );

		return this.getAreqDao().find(condition.getCondition(), sort);
	}

	/**
	 *
	 * ロット・ベースラインに紐付く変更情報を取得する
	 *
	 * @param lotBlDtoList ロット・ベースラインDTOのList
	 * @param sort ソート条件
	 * @return 変更情報EntitのList
	 */
	public List<IPjtEntity> getPjtEntities( List<ILotBlDto> lotBlDtoList, ISqlSort sort ) {
		Set<String> pjtIdSet = new HashSet<String>();

		for( ILotBlDto dto: lotBlDtoList ) {
			List<ILotBlReqLnkEntity> lnkEntities = dto.getLotBlReqLnkEntities();
			for( ILotBlReqLnkEntity lnkEntity : lnkEntities ) {
				pjtIdSet.add( lnkEntity.getPjtId() );
			}
		}
		String[] pjtIds = pjtIdSet.toArray( new String[ 0 ] );

		return this.findPjtEntities(sort, pjtIds);
	}

	/**
	 * 指定されたVersion Tagに該当するベースラインの先頭１件を取得します
	 * <br>
	 * @param lotNo ロット番号
	 * @param lotVersionTag ロットチェックイン時のバージョンタグ
	 * @return ベースライン情報Entityの先頭1件
	 */
	public final ILotBlEntity getLotBlEntityByVersionTagAtFirst(
			String lotId,
			String lotVersionTag ) {

		return this.getLotBlEntityByVersionTag(lotId, lotVersionTag, TriSortOrder.Desc).get(0);
	}

	/**
	 * 指定されたVersion Tagに該当するベースラインのListを取得します。
	 * <br>
	 * @param lotNo ロット番号
	 * @param lotVersionTag ロットチェックイン時のバージョンタグ
	 * @param order ソート順
	 * @return ベースライン情報EntityのList
	 */
	public final List<ILotBlEntity> getLotBlEntityByVersionTag(
			String lotId,
			String lotVersionTag,
			TriSortOrder order ) {

		if( TriStringUtils.isEmpty( lotId ) ){
			throw new TriSystemException( AmMessageId.AM004011F , lotId);
		}
		if( TriStringUtils.isEmpty( lotVersionTag ) ){
			throw new TriSystemException( AmMessageId.AM004012F , lotVersionTag );
		}

		LotBlCondition condition = new LotBlCondition() ;
		condition.setLotId( lotId );
		condition.setLotVerTag( lotVersionTag ) ;

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		List<ILotBlEntity> entities = this.getLotBlDao().find( condition.getCondition(), sort );

		if( TriStringUtils.isEmpty( entities ) ) {
			throw new TriSystemException( AmMessageId.AM004013F , lotVersionTag );
		}

		return entities;
	}

	/**
	 * 指定されたVersion Tagに該当するベースラインのListを取得します。
	 * <br>
	 * @param lotNo ロット番号
	 * @param lotVersionTag ロットチェックイン時のバージョンタグ
	 * @param order ソート順
	 * @return ベースライン情報EntityのList
	 */
	public final List<ILotBlDto> getLotBlDtoByVersionTag(
			String lotId,
			String lotVersionTag,
			TriSortOrder order ) {

		List<ILotBlEntity> entities = this.getLotBlEntityByVersionTag(lotId, lotVersionTag, order);
		return this.findLotBlDto( entities );
	}

	/**
	 * コンフリクトチェック可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>チェックイン済
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのビルドパッケージビルドが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlDto> getLotBlDtoByMergeCheckEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		List<ILotBlEntity> entities = this.getLotBlEntitiesByMergeCheckEnable( lotId , lotCheckInTimestamp , order ) ;
		return this.findLotBlDto( entities );
	}

	/**
	 * コンフリクトチェック可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>チェックイン済
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのビルドパッケージビルドが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlEntity> getLotBlEntitiesByMergeCheckEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return this.getLotBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public final List<ILotBlDto> getLotBlDtoByMergeEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order ) {

		List<ILotBlEntity> entities = this.getLotBlEntitiesByMergeEnable( lotId , lotCheckInTimestamp , order, new ArrayList<String>() ) ;
		return this.findLotBlDto( entities );
	}

	/**
	 * マージ可能なベースラインを取得します。
	 * <br>指定されたチェックイン日時を含むそれ以前のベースラインが対象となります。
	 * <br>対象となるステータスは以下の通りです。
	 * <li>コンフリクトチェック済
	 *
	 * @param lotId ロット番号
	 * @param lotCheckInTimestamp ベースラインのチェックイン日時。null時はすべてのベースラインが対象
	 * @param order ソート順
	 * @return 取得したロット・ベースラインDTOのList
	 */
	public List<ILotBlEntity> getLotBlEntitiesByMergeEnable(
			String lotId,
			Timestamp lotCheckInTimestamp,
			TriSortOrder order,
			List<String> lotBlIds) {

		LotBlCondition condition = new LotBlCondition() ;

		condition.setLotId( lotId ) ;
		condition.setStsIds( new String[]{
				AmLotBlStatusId.CommittedToLot.getStatusId()} );

		if( null != lotCheckInTimestamp ) {
			condition.setLotChkinTimestamp( null, lotCheckInTimestamp  ) ;
		}
		
		if ( TriCollectionUtils.isNotEmpty(lotBlIds) ){
			condition.setLotBlIds(lotBlIds.toArray(new String[0]));
		}

		ISqlSort sort = null;

		if ( null != order ) {
			sort = new SortBuilder();
			sort.setElement(LotBlItems.lotChkinTimestamp, order, 0);
		}

		return this.getLotBlDao().find( condition.getCondition(), sort );
	}

	/**
	 * マージ可能なベースラインの先頭１件を取得します
	 * <br>
	 * @param lotNo ロット番号
	 * @return ビルドパッケージビルド情報レコードの配列
	 */
	public final ILotBlEntity getLotBlEntityByMergeEnableAtFirst( String lotId , IHeadBlDto headBlDto) {
			
		List<IHeadLotBlLnkEntity> headLotBlLink= headBlDto.getHeadLotBlLnkEntities();
		List<String> lotBlIds = new ArrayList<String>();
			for (IHeadLotBlLnkEntity lotHeadLink : headLotBlLink) {
				String lotBlId = lotHeadLink.getLotBlId();
				if ( TriStringUtils.isNotEmpty(lotBlId) ){
					lotBlIds.add(lotBlId);
				}
			}
		return this.getLotBlEntitiesByMergeEnable( lotId, null, TriSortOrder.Desc, lotBlIds  ).get(0);
	}

	/**
	 * HEAD原本の状態チェックを行います。
	 *
	 * @param lotDto ロット情報DTO
	 * @param headMwPath HEAD原本のMWパス
	 */
	public void checkHeadMasterAsset( ILotDto lotDto , String headMwPath ) {

		ILotEntity lotEntity = lotDto.getLotEntity();
		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true );

		if( TriStringUtils.isEmpty( mdlEntities ) ) {
			throw new TriSystemException( AmMessageId.AM004003F , lotEntity.getLotId() , lotEntity.getLotLatestVerTag());
		}

		File fileMaster = new File( headMwPath ) ;
		if( true != fileMaster.exists() ) {
			throw new TriSystemException( AmMessageId.AM004004F , headMwPath);
		}
		for( ILotMdlLnkEntity moduleEntity : mdlEntities ) {
			String pathModule = TriStringUtils.linkPathBySlash( headMwPath , moduleEntity.getMdlNm() ) ;
			File fileModule = new File( pathModule ) ;
			if( true != fileModule.exists() ) {
				throw new TriSystemException( AmMessageId.AM004005F , pathModule);
			}
		}
	}

	/**
	 * 資産申請番IDに対応する資産申請情報を取得する。
	 * <br>変更承認日時の昇順でSortされます。
	 *
	 * @param areqIds 資産申請ID
	 * @return 取得した申請情報DTOのListを戻します。
	 */
	public List<IAreqDto> getAreqEntitiesSortByPjtAvlTimestamp( String[] areqIds ) throws Exception {

		AreqCondition condition = new AreqCondition() ;

		if ( TriStringUtils.isEmpty( areqIds ) ) {
			//全件検索とならないようダミーを設定。
			condition.setAreqIds( new String[]{ "" } ) ;
		} else {
			condition.setAreqIds( areqIds ) ;
		}

		condition.setAreqCtgCds( AreqCtgCd.ReturningRequest.value(), AreqCtgCd.RemovalRequest.value() );

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.pjtAvlTimestamp, TriSortOrder.Asc, 1);

		List<IAreqEntity> areqEntities = this.getAreqDao().find( condition.getCondition() , sort);

		//パッケージ作成（全資産）の場合等、パッケージに紐付く資産情報は存在しない場合がある。
		if ( 0 == areqEntities.size() ) {
			return new ArrayList<IAreqDto>();
		}

		return this.findAreqDto( areqEntities, AmTables.AM_AREQ_FILE );
	}

	/**
	 * コンフリクトチェック対象のHEAD・ベースライン情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したHEADベースラインDTO
	 */
	public final IHeadBlDto getTargetHeadBlDtoForMergeCheck( String lotId ) {
		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );
		condition.setProcStsIds( new String[]{
				AmHeadBlStatusIdForExecData.ConflictChecking.getStatusId(),
				} );

		List<IHeadBlEntity> headBlEntities = this.getHeadBlDao().find(condition.getCondition());

		if ( 0 == headBlEntities.size() ) {
			throw new TriSystemException( AmMessageId.AM005154S, lotId );
		}

		IHeadBlEntity headBlEntity = headBlEntities.get(0);
		return this.findHeadBlDto( headBlEntity );
	}

	/**
	 * マージ対象のHEAD・ベースライン情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したHEADベースラインDTO
	 */
	public final IHeadBlDto getTargetHeadBlDtoForMerge( String lotId ) {
		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );
		condition.setStsIds( new String[]{
				AmHeadBlStatusId.ConflictChecked.getStatusId(),
				} );

		List<IHeadBlEntity> headBlEntities = this.getHeadBlDao().find(condition.getCondition());

		if ( 0 == headBlEntities.size() ) {
			throw new TriSystemException( AmMessageId.AM005155S, lotId );
		}

		IHeadBlEntity headBlEntity = headBlEntities.get(0);
		return this.findHeadBlDto( headBlEntity );
	}
	
	public final ILotBlEntity  getLotBlEntityAtFirst( String lotId ){
		return this.getLotBlEntitiesByMergeEnable( lotId, null, TriSortOrder.Desc, null ).get(0);
	}

}
