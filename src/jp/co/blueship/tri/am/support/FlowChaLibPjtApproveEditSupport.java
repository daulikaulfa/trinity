package jp.co.blueship.tri.am.support;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 変更管理承認系のサポートClass<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChaLibPjtApproveEditSupport extends AmFinderSupport /*
																	 * extends
																	 * FinderSupport
																	 */{

	IFilesDiffer differ = null;

	public final void setDiffer(IFilesDiffer diff) {
		differ = diff;
	}

	/**
	 * 申請情報エンティティから変更管理番号を取得します。
	 *
	 * @param entities 申請情報エンティティ
	 * @return 変更管理番号の配列
	 */
	public List<IPjtAvlAreqLnkEntity> getPjtNo(AreqDtoList entities) {

		List<IPjtAvlAreqLnkEntity> pjtNoList = new ArrayList<IPjtAvlAreqLnkEntity>();

		for (IAreqDto entity : entities) {
			IPjtAvlAreqLnkEntity pjtAreq = new PjtAvlAreqLnkEntity();
			pjtAreq.setAreqId(entity.getAreqEntity().getAreqId());
			pjtAreq.setPjtAvlSeqNo(entity.getAreqEntity().getPjtAvlSeqNo());
			pjtAreq.setPjtId(entity.getAreqEntity().getPjtId());
			pjtNoList.add(pjtAreq);
		}

		return pjtNoList;
	}

	/**
	 * 変更管理エンティティから変更管理番号を取得します。
	 *
	 * @param entities 変更管理エンティティ
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNo(IPjtEntity[] entities) {

		List<String> pjtNoList = new ArrayList<String>();

		for (IPjtEntity entity : entities) {
			pjtNoList.add(entity.getPjtId());
		}

		return (String[]) pjtNoList.toArray(new String[0]);
	}

	/**
	 * 申請情報エンティティから申請情報番号を取得します。
	 *
	 * @param entities 申請情報エンティティ
	 * @return 申請情報番号の配列
	 */
	public String[] getAssetApplyNo(AreqDtoList entities) {

		List<String> assetApplyNoList = new ArrayList<String>();

		for (IAreqDto entity : entities) {
			assetApplyNoList.add(entity.getAreqEntity().getAreqId());
		}

		return (String[]) assetApplyNoList.toArray(new String[0]);
	}

	/**
	 * ビルドパッケージ情報エンティティからビルド番号を取得します。
	 *
	 * @param entities ビルドパッケージ情報エンティティ
	 * @return ビルド番号の配列
	 */
	public String[] getBuildNo(IBpEntity[] entities) {

		List<String> buildEntityList = new ArrayList<String>();

		for (IBpEntity entity : entities) {
			buildEntityList.add(entity.getBpId());
		}

		return (String[]) buildEntityList.toArray(new String[0]);
	}

	/**
	 * 申請情報エンティティから申請情報画面用ビーンを作成します。
	 *
	 * @param entities 申請情報エンティティ
	 * @return 申請情報画面用ビーンのリスト
	 */
	public List<ApplyInfoViewPjtApproveBean> getApplyInfoViewBeanList(AreqDtoList entities) {

		List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtApproveBean>();

		Map<String, IPjtEntity> pjtEntitiesMap = new HashMap<String, IPjtEntity>();
		for (IPjtEntity pjtEntity : this.getPjtEntity(entities)) {
			pjtEntitiesMap.put(pjtEntity.getPjtId(), pjtEntity);
		}

		AmViewInfoAddonUtils.setApplyInfoViewPjtApproveBeanAssetApplyEntity(applyInfoViewBeanList, entities, pjtEntitiesMap);

		return applyInfoViewBeanList;
	}

	/**
	 * 申請情報から、申請情報エンティティを取得する
	 *
	 * @param applyNoArray 対象の変更管理番号
	 * @return 申請情報エンティティ
	 */
	public IAreqEntity[] getAssetApplyEntity(String[] applyNoArray) {

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getAreqConditionByApplyNo(applyNoArray);

		List<IAreqEntity> entities = this.getAreqDao().find(condition.getCondition());

		return entities.toArray(new IAreqEntity[0]);
	}

	/**
	 * 申請情報エンティティから申請情報画面用ビーンを作成します。
	 *
	 * @param dtoList 申請情報エンティティ
	 * @return 申請情報画面用ビーンのリスト
	 */
	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewPjtDetailBean(String userId, AreqDtoList dtoList) {

		List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();

		AmViewInfoAddonUtils.setApplyInfoViewBeanAssetApplyEntity(userId, applyInfoViewBeanList, dtoList);

		return applyInfoViewBeanList;

	}

	/**
	 * 申請情報エンティティから変更管理承認履歴画面用ビーンを作成します。
	 *
	 * @param pjtDto 変更管理エンティティ(履歴)
	 * @return 変更管理承認履歴画面用ビーンのリスト
	 */
	public List<PjtApproveHistoryViewBean> getPjtApproveHistoryViewBean(List<IPjtAvlDto> pjtDto) {

		List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList = new ArrayList<PjtApproveHistoryViewBean>();

		AmViewInfoAddonUtils.setPjtApproveHistoryViewBeanPjtEntity(pjtApproveHistoryViewBeanList, pjtDto);

		return pjtApproveHistoryViewBeanList;

	}

	/**
	 * 指定された返却申請で返却された資産ファイルのリストを取得する。
	 *
	 * @param lotDto ロットエンティティ
	 * @param retEntity 返却申請エンティティ
	 * @return 相対パスのファイルリスト
	 */
	public List<String> getReturnAssetList(ILotDto lotDto, IAreqEntity retEntity) throws TriSystemException {

		List<String> fileList = new ArrayList<String>();

		try {
			if (AmBusinessJudgUtils.isReturnApply(retEntity)) {

				List<Object> paramList = new ArrayList<Object>();
				paramList.add(lotDto);

				File targetDir = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath(paramList, retEntity);
				if (!targetDir.exists()) {
					throw new TriSystemException(AmMessageId.AM004009F, retEntity.getAreqId(), targetDir.getAbsolutePath());
				}

				fileList = TriFileUtils.getFilePaths(targetDir, TriFileUtils.TYPE_FILE);
				if (null == fileList || 0 == fileList.size()) {
					throw new TriSystemException(AmMessageId.AM004014F, retEntity.getAreqId(), targetDir.getAbsolutePath());
				}
			}
		} catch (IOException ioe) {
			throw new TriSystemException(AmMessageId.AM005053S, ioe);
		}

		return fileList;
	}

	/**
	 * 申請情報を取得する
	 *
	 * @param applyInfoViewBeanList 申請情報のリスト
	 * @return 申請情報
	 */
	public AreqDtoList getAssetApplyEntities(List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList) {

		List<String> applyNoList = new ArrayList<String>();
		for (ApplyInfoViewPjtDetailBean viewBean : applyInfoViewBeanList) {
			applyNoList.add(viewBean.getApplyNo());
		}

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getAreqConditionByApplyNo((String[]) applyNoList.toArray(new String[0]));

		List<IAreqEntity> entities = this.getAreqDao().find(condition.getCondition());
		return this.findAreqDtoList(entities);

	}

	/**
	 * 変更管理情報を取得する
	 *
	 * @param entities 申請情報エンティティ
	 * @return 変更管理情報
	 */
	public IPjtEntity[] getPjtEntity(AreqDtoList entities) {

		List<IPjtAvlAreqLnkEntity> pjtAvlAreqArray = this.getPjtNo(entities);
		Set<String> pjtIdSet = new TreeSet<String>();
		for (IPjtAvlAreqLnkEntity pjtNo : pjtAvlAreqArray) {
			pjtIdSet.add(pjtNo.getPjtId());
		}

		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getPjtCondition(pjtIdSet.toArray(new String[0]));

		List<IPjtEntity> pjtEntities = this.getPjtDao().find(condition.getCondition());

		if (pjtEntities.size() != pjtIdSet.size()) {

			String pjtNoLabel = TriStringUtils.convertArrayToString(pjtIdSet.toArray(new String[0]));
			throw new TriSystemException(AmMessageId.AM004015F, pjtNoLabel);

		}

		return pjtEntities.toArray(new IPjtEntity[0]);
	}

	/**
	 * 選択可能なロットを取得します。
	 *
	 * @return
	 */
	public final List<LotViewBean> getLotViewBeanForPullDown(GenericServiceBean retBean) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		LotCondition lotCondition = AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		ISqlSort lotSort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibTop();
		List<ILotEntity> lotEntityList = this.getLotDao().find(lotCondition.getCondition(), lotSort);
		List<ILotDto> lotDto = this.findLotDto(lotEntityList);

		if (TriStringUtils.isEmpty(lotEntityList)) {
			retBean.setInfoMessage(AmMessageId.AM001007E);
		}

		AmViewInfoAddonUtils.populateLotViewBeanPjtLotEntityForPullDown(//
				lotViewBeanList,//
				lotDto,//
				this.getUmFinderSupport().findGroupByUserId(retBean.getUserId()), this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId());

		if (!TriStringUtils.isEmpty(lotEntityList) && TriStringUtils.isEmpty(lotViewBeanList)) {
			retBean.setInfoMessage(AmMessageId.AM001122E);
		}

		return lotViewBeanList;
	}

	/**
	 * 参照元と参照先の資産を比較し、「diff」ボタン表示/非表示の判定を行う。
	 *
	 * @param assetResource 比較する資産
	 */
	public final void setDiffEnabled(ChaLibEntryAssetResourceInfoBean assetResource) {
		Map<String, IExtEntity> ucfExtensionMap = this.getExtMap();

		for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
			// バイナリ資産or拡張子未登録の資産はDiffボタンを非表示にする
			viewBean.setDiffEnabled(!AmBusinessFileUtils.isBinary(viewBean.getRelativeResourcePath(), ucfExtensionMap));
		}
	}

	/**
	 * 「新規」「変更」「未変更」の判定を行う。 <br>
	 * <br>
	 * 申請が承認前か、承認後かによって判定ルールが異なります。 <br>
	 * <br>
	 * 承認後であれば、常にnullを戻します。
	 *
	 * @param assetResource 比較する資産
	 * @param paramList
	 * @param assetApplyEntity 変更管理情報
	 * @param diffSrcMode 比較元資産の参照先モード
	 *
	 *            <pre>
	 * 		true	:	内部返却フォルダ
	 * 		false	:	返却フォルダ
	 * </pre>
	 */
	@SuppressWarnings("deprecation")
	public final void setFileStatusByAssetResourceInfoBean(ChaLibEntryAssetResourceInfoBean assetResource, List<Object> paramList, IAreqDto areqDto,
			boolean diffSrcMode) {

		IAreqEntity areqEntity = areqDto.getAreqEntity();
		
		if (!AmBusinessJudgUtils.isPjtApproveEnabled(areqEntity)) {
			for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
				viewBean.setFileStatus(null);
			}

			return;
		}

		if (!AmBusinessJudgUtils.isDeleteApply(areqEntity)) {

			File srcPath = null;
			if (true == diffSrcMode) {
				srcPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath(paramList, areqEntity);
			} else {
				srcPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath(paramList, areqEntity);
			}
			File diffPath = AmDesignBusinessRuleUtils.getMasterWorkPath(paramList);

			IFileDiffResult[] results = differ.execute(srcPath, diffPath);

			for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
				for (IFileDiffResult result : results) {
					if (viewBean.getResourcePath().endsWith(result.getFilePath())) {
						if (!result.isMasterExists()) {
							viewBean.setFileStatus(FileStatus.Add.getStatus());
							viewBean.setFileStatusId(FileStatus.Add.getStatusId());
						} else if (result.isDiff()) {
							viewBean.setFileStatus(FileStatus.Edit.getStatus());
							viewBean.setFileStatusId(FileStatus.Edit.getStatusId());
						} else {
							viewBean.setFileStatus(FileStatus.Same.getStatus());
							viewBean.setFileStatusId(FileStatus.Same.getStatusId());
						}
					}
				}
			}
		} else {
			List<IAreqFileEntity> removalFileEntities = areqDto.getAreqFileEntities(AreqCtgCd.RemovalRequest);
			if (null != removalFileEntities) {
				for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
					for (IAreqFileEntity assetFileEntity : removalFileEntities) {
						if (viewBean.getResourcePath().endsWith(assetFileEntity.getFilePath())) {
							viewBean.setFileStatus(FileStatus.Delete.getStatus());
							viewBean.setFileStatusId(FileStatus.Delete.getStatusId());
						}
					}
				}
			}

			List<IAreqBinaryFileEntity> removalBinaryFileEntities = areqDto.getAreqBinaryFileEntities(AreqCtgCd.RemovalRequest);
			if (null != removalBinaryFileEntities) {
				for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetBinaryResourceViewBeanList()) {
					for (IAreqBinaryFileEntity assetFileEntity : removalBinaryFileEntities) {
						if (viewBean.getResourcePath().endsWith(assetFileEntity.getFilePath())) {
							viewBean.setFileStatus(FileStatus.Delete.getStatus());
							viewBean.setFileStatusId(FileStatus.Delete.getStatusId());
						}
					}
				}
			}
		}
	}

	// Get all possible areq_ids to approve
	public String[] getTargetApplyNo(FlowChaLibPjtApproveServiceBean paramBean) {
		List<String> targetApplyNo = new ArrayList<String>();
		if (TriStringUtils.isNotEmpty(paramBean.getApplyInfoViewBeanList())) {
			for (ApplyInfoViewPjtApproveBean infoBean : paramBean.getApplyInfoViewBeanList()) {
				targetApplyNo.add(infoBean.getApplyNo());
			}
		}

		return targetApplyNo.toArray(new String[0]);
	}

	public String[] getPjtIdsFromAreqIds(String[] areqIds) {
		Set<String> pjtIds = new HashSet<String>();
		for (String areqId : areqIds) {
			IAreqEntity areqEntity = this.findAreqEntity(areqId);

			if (null != areqEntity) {
				pjtIds.add(areqEntity.getPjtId());
			}
		}
		return pjtIds.toArray(new String[pjtIds.size()]);
	}
}
