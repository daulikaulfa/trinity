package jp.co.blueship.tri.am.support;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.PathInfoViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * 貸出申請系イベントのサポートClass <br>
 * <p>
 * 貸出申請／貸出編集を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChaLibLendEditSupport extends AmFinderSupport /* extends FinderSupport */ {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 選択可能な変更管理を取得します。
	 *
	 * @return
	 */
	/*
	 * public final List<String> getSelectPjtNoList() { List<String> prjNoList =
	 * new ArrayList<String>();
	 *
	 * IPjtEntityLimit pjtEntLimit = this.getPjtDao().find(
	 * DBSearchConditionAddonUtil.getPjtSelectCondition(),
	 * DBSearchSortAddonUtil.getPjtSelectSortFromDesignDefine(), 1, 0);
	 *
	 * IPjtEntity[] pjtEntitys = pjtEntLimit.getEntity(); for ( IPjtEntity
	 * entity : pjtEntitys ) { prjNoList.add(entity.getPjtNo()); }
	 *
	 * return prjNoList; }
	 */

	/**
	 * 貸出排他中のファイルの非表示設定を行います。
	 *
	 * @param applyNo
	 * @param inLotDto
	 * @param assetBean 原本資産のリソース情報
	 */
	public final void checkAssetLock(String applyNo, ILotDto inLotDto, ChaLibEntryAssetResourceInfoBean assetBean) {

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath(inLotDto.getLotEntity());

		List<IAreqEntity> assetEntity = this.getAssetApplyByAssetLock(inLotDto.getLotEntity().getLotId(), applyNo);

		List<Object> list = new ArrayList<Object>();
		list.add(inLotDto);

		for (IAreqEntity entity : assetEntity) {

			File lendAssetNoPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath(list, entity);
			File returnAssetNoPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath(list, entity);

			for (ChaLibAssetResourceViewBean bean : assetBean.getAssetResourceViewBeanList()) {
				if (bean.isFile()) {
					String relativePath = TriStringUtils.convertRelativePath(masterPath, bean.getResourcePath());

					if (new File(lendAssetNoPath, relativePath).exists()) {
						bean.setHidden(true);
					} else if (new File(returnAssetNoPath, relativePath).exists()) {
						bean.setHidden(true);
					}
				}
			}
		}
	}

	/**
	 * 貸出中の資産申請情報を取得します。
	 *
	 * @param lotId 対象ロット
	 * @param applyNo 対象外となる申請番号（自分自身）
	 *
	 * @return 取得した資産申請情報エンティティを戻します。
	 */
	public final List<IAreqEntity> getAssetApplyByAssetLock(String lotId, String applyNo) {
		List<IAreqEntity> entitys = new ArrayList<IAreqEntity>();

		AreqCondition condition = AmDBSearchConditionAddonUtils.getAreqConditionByAssetLock(lotId);

		List<IAreqEntity> list = this.getAreqDao().find(condition.getCondition());

		for (IAreqEntity entity : list) {
			if (entity.getAreqId().equals(applyNo))
				continue;

			entitys.add(entity);
		}

		return entitys;
	}

	/**
	 * 貸出中の申請が前回貸出した資産のパスを取得します。 （資産選択画面を初期表示した際、貸出中の資産を画面に覚えこませるため）
	 *
	 * @param lotDto 対象ロットDTO
	 * @param assetApplyEntity 対象申請エンティティ
	 * @return 取得した相対パスを戻します。
	 */
	public final String[] getSelectedAssetResourcePath(ILotDto lotDto, IAreqEntity assetApplyEntity) {
		List<Object> param = new ArrayList<Object>();
		param.add(lotDto);

		ILotEntity lotEntity = lotDto.getLotEntity();

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath(lotEntity);
		File rootPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath(param, assetApplyEntity);

		List<String> resources = new ArrayList<String>();

		List<File> moduleFiles = BusinessFileUtils.getFiles(AmDesignBusinessRuleUtils.getLendAssetApplyNoPath(param, assetApplyEntity),
				AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));

		for (File file : moduleFiles) {
			if (file.isDirectory())
				continue;

			String relativePath = TriStringUtils.convertRelativePath(rootPath, TriStringUtils.convertPath(file.getPath()));
			resources.add(TriStringUtils.convertPath(new File(masterPath, relativePath)));
		}

		return resources.toArray(new String[0]);
	}

	/**
	 * 貸出申請パラメタに値を設定します。
	 *
	 * @param inBaseInfoBean
	 * @param inApplyNo
	 * @param paramInfo
	 * @return
	 */
	public final void setLendParamInfo(ChaLibEntryInputBaseInfoBean inBaseInfoBean, String inApplyNo, IEhLendParamInfo retParam) {

		retParam.setApplyNo(inApplyNo);
		retParam.setPjtNo(inBaseInfoBean.getInPrjNo());
		retParam.setGroupName(inBaseInfoBean.getInGroupName());
		retParam.setSummary(inBaseInfoBean.getInSubject());
		retParam.setContent(inBaseInfoBean.getInContent());
		retParam.setLendApplyUser(inBaseInfoBean.getInUserName());
		retParam.setLendApplyUserId(inBaseInfoBean.getInUserId());
		retParam.setGroupId(inBaseInfoBean.getInGroupId());
		retParam.setAssigneeId(inBaseInfoBean.getAssigneeId());
		retParam.setCtgId(inBaseInfoBean.getCtgId());
		retParam.setMstoneId(inBaseInfoBean.getMstoneId());
		retParam.setDraff(inBaseInfoBean.isDraff());
	}

	/**
	 * 貸出申請パラメタに値を設定します。
	 *
	 * @param paramBean
	 * @param inMasterPath
	 * @param paramInfo
	 * @return
	 */
	public final void setLendParamInfo(ChaLibInputLendEntryAssetSelectBean inAssetSelectBean, File inMasterPath, IEhLendParamInfo retParam) {

		List<String> lendFiles = new ArrayList<String>();
		for (String resource : inAssetSelectBean.getInAssetResourcePath()) {
			lendFiles.add(TriStringUtils.convertRelativePath(inMasterPath, resource));
		}

		retParam.setLendFiles(lendFiles.toArray(new String[0]));
	}

	/**
	 * 貸出申請レコードに、申請ファイル情報を設定します。
	 *
	 * @param paramList
	 * @param statusMap
	 * @return
	 */
	public final List<IAreqFileEntity> setAssetFileEntity(List<Object> paramList, Map<String, SVNStatus> statusMap) throws IOException {

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractLendApply(paramList);
		if (null == areqDto) {
			throw new TriSystemException(AmMessageId.AM005003S);
		}

		IAreqEntity areqEntity = areqDto.getAreqEntity();
		File frontMasterPath = AmDesignBusinessRuleUtils.getMasterWorkPath(paramList);

		IFileDiffResult[] results = AmExtractMessageAddonUtils.getFileDiffResult(paramList);
		List<IAreqFileEntity> fileEntities = new ArrayList<IAreqFileEntity>();

		int seqNo = 0;
		for (IFileDiffResult path : results) {
			File file = new File(frontMasterPath, path.getFilePath());
			if (!file.isFile()) {
				throw new TriSystemException(AmMessageId.AM004000F, path.getFilePath());
			}
			IAreqFileEntity fileEntity = new AreqFileEntity();
			fileEntity.setAreqId(areqEntity.getAreqId());
			fileEntity.setAreqCtgCd(areqEntity.getAreqCtgCd());
			fileEntity.setAreqFileSeqNo(this.getAreqFileNumberingDao().nextval(++seqNo));
			fileEntity.setFilePath(TriStringUtils.convertPath(path.getFilePath()));
			fileEntity.setFileByteSize(new Long(file.length()).intValue());
			fileEntity.setFileUpdTimestamp(new Timestamp(file.lastModified()));
			fileEntity.setDelStsId(areqEntity.getDelStsId());

			String absolutePath = TriStringUtils.convertPath(file.getCanonicalPath());
			if (statusMap.containsKey(absolutePath)) {
				SVNStatus svnStatus = statusMap.get(absolutePath);

				long revisionNo = svnStatus.getCommittedRevision().getNumber();
				if (-1 == revisionNo) {
					throw new TriSystemException(AmMessageId.AM004001F, path.getFilePath());
				}

				fileEntity.setFileRev(String.valueOf(revisionNo));
			} else {
				throw new TriSystemException(AmMessageId.AM004000F, path.getFilePath());
			}

			fileEntities.add(fileEntity);
		}

		return fileEntities;
	}

	/**
	 * 選択可能なロットを取得します。
	 *
	 * @return
	 */

	public final List<LotViewBean> getLotViewBeanForPullDown(GenericServiceBean retBean) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		LotCondition lotCondition = AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		ISqlSort lotSort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibTop();
		List<ILotEntity> lotList = this.getLotDao().find(lotCondition.getCondition(), lotSort);
		List<ILotEntity> lotEntityList = lotList;
		List<ILotDto> lotDto = this.findLotDto(lotEntityList);

		if (TriCollectionUtils.isEmpty(lotEntityList)) {
			retBean.setInfoMessage(AmMessageId.AM001007E);
		}

		AmViewInfoAddonUtils.populateLotViewBeanPjtLotEntityForPullDown(lotViewBeanList, lotDto,
				this.getUmFinderSupport().findGroupByUserId(retBean.getUserId()),
				this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

		if (TriCollectionUtils.isNotEmpty(lotEntityList) && TriCollectionUtils.isEmpty(lotViewBeanList)) {
			retBean.setInfoMessage(AmMessageId.AM001122E);
		}

		return lotViewBeanList;
	}

	/**
	 * 選択可能な変更管理を取得します。
	 *
	 * @return
	 */
	public final List<PjtViewBean> getSelectPjtViewList() {
		List<IPjtEntity> pjtList =
				this.getPjtDao().find(
						AmDBSearchConditionAddonUtils.getPjtSelectCondition().getCondition(),
						AmDBSearchSortAddonUtils.getPjtSelectSortFromDesignDefine());

		IPjtEntity[] pjtEntitys = pjtList.toArray(new IPjtEntity[0]);

		return AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( pjtEntitys );
	}

	/**
	 * ロットを指定して選択可能な変更管理を取得します。
	 *
	 * @return
	 */
	public final List<PjtViewBean> getSelectPjtViewList(String lotId) {
		PjtCondition condition = new PjtCondition();
		condition = AmDBSearchConditionAddonUtils.getPjtSelectCondition();
		condition.setLotId(lotId);

		List<IPjtEntity> pjtList = this.getPjtDao().find(condition.getCondition(),
				AmDBSearchSortAddonUtils.getPjtSelectSortFromDesignDefine());

		IPjtEntity[] pjtEntitys = pjtList.toArray(new IPjtEntity[0]);

		return AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( pjtEntitys );
	}

	/**
	 * ディレクトリパスを階層ごとに分割し、PathInfoViewBeanのリストに編集して返す<br>
	 *
	 * @param masterPath 絶対パスでの基準パス
	 * @param selectedDir 絶対パスでの選択中パス
	 * @return PathInfoViewBeanのリスト
	 */
	public List<PathInfoViewBean> makePathInfoViewBeanList(String masterPath, String selectedDir) {

		final String ROOT_LABEL = "[ROOT]";

		List<PathInfoViewBean> arrlst = new ArrayList<PathInfoViewBean>();

		PathInfoViewBean pathInfoViewBean = new PathInfoViewBean();
		// ルートを登録
		pathInfoViewBean.setDirName(ROOT_LABEL);
		pathInfoViewBean.setFullPath(masterPath);
		arrlst.add(pathInfoViewBean);

		//
		String convertedSelectedDir = TriStringUtils.convertPath(selectedDir);

		String curAbsolutePath = StringUtils.replace(convertedSelectedDir, TriStringUtils.convertPath(masterPath), "");// 絶対パス部分を除去

		String[] dirArray = curAbsolutePath.split("/");// パス区切り文字で分割
		int posOffset = 0;
		for (String dir : dirArray) {
			if (TriStringUtils.isEmpty(dir)) {
				continue;
			}
			pathInfoViewBean = new PathInfoViewBean();
			pathInfoViewBean.setDirName(dir);

			posOffset = convertedSelectedDir.indexOf(dir, posOffset) + dir.length();
			pathInfoViewBean.setFullPath(convertedSelectedDir.substring(0, posOffset));

			arrlst.add(pathInfoViewBean);
		}

		return arrlst;
	}

	/**
	 * 申請情報の配列から、変更管理番号を取得し、配列で返す<br>
	 *
	 * @param assetApplyEntityArray 申請情報の配列
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNoArrayFromAssetApplyArray(IAreqEntity[] assetApplyEntityArray) {
		List<String> pjtNoList = new ArrayList<String>();
		for (IAreqEntity assetApplyEntity : assetApplyEntityArray) {
			pjtNoList.add(assetApplyEntity.getPjtId());
		}
		return pjtNoList.toArray(new String[0]);
	}

	/**
	 * 変更管理番号の配列から、変更管理情報を取得し、変更管理番号をキーとしたMapで返す<br>
	 *
	 * @param pjtNoArray 変更管理番号の配列
	 * @return 変更管理番号をキーとしたMap
	 */
	public Map<String, IPjtEntity> getPjtEntityPjtNoMap(String[] pjtNoArray) {
		PjtCondition condition = new PjtCondition();

		String[] conditionPjtNo = pjtNoArray;

		if (TriStringUtils.isEmpty(conditionPjtNo)) {
			// ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[] { "" };
		}

		condition.setPjtIds(conditionPjtNo);

		List<IPjtEntity> list = this.getPjtDao().find(condition.getCondition());

		IPjtEntity[] pjtEntityArray = list.toArray(new IPjtEntity[0]);
		Map<String, IPjtEntity> lhMap = new LinkedHashMap<String, IPjtEntity>();
		for (IPjtEntity pjtEntity : pjtEntityArray) {
			lhMap.put(pjtEntity.getPjtId(), pjtEntity);
		}
		return lhMap;
	}

	/**
	 * 表示に一貫性を持たせるため、ファイルセパレータを"/"に変換します。
	 *
	 * @param src 対象となるディレクトリパス
	 * @return 変換後のメッセージを戻します。
	 */
	public static final String convertPath(String src) {
		String relativePath = TriStringUtils.convertPath(src);

		if (relativePath.startsWith("/"))
			relativePath = StringUtils.substringAfter(relativePath, "/");

		return relativePath;
	}

	/**
	 * 指定された申請アップロードファイルをサーバに保存します。
	 *
	 * @param inBean
	 */
	public static final void saveLendApplyDefUploadFile(ChaLibEntryInputBaseInfoBean inputBean, String usrId) {

		if (null == inputBean.getLendApplyInputStreamBytes()) {
			return;
		}

		BusinessFileUtils.saveAppendFile(
				TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
						sheet.getValue(AmDesignEntryKeyByChangec.lendApplyDefUploadRelativePath)), usrId,
				StreamUtils.convertBytesToInputStreamToBytes(inputBean.getLendApplyInputStreamBytes()), inputBean.getLendApplyFileName());

		String filePath = new File(TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
				sheet.getValue(AmDesignEntryKeyByChangec.lendApplyDefUploadRelativePath)), TriStringUtils.linkPath(usrId,
				inputBean.getLendApplyFileName())).getPath();

		if (log.isDebugEnabled()) {
			LogHandler.debug(log, "★★★:filePath:=" + filePath);
		}

		if (new File(filePath).exists()) {
			inputBean.setLendApplyInputStreamBytes(null);
			inputBean.setLendApplyFilePath(filePath);
		}
	}

	public static final void check4InputFile(ChaLibEntryInputBaseInfoBean inBean) throws ContinuableBusinessException {

		if (TriStringUtils.isEmpty(inBean.getLendApplyFilePath()) || !new File(inBean.getLendApplyFilePath()).isFile()) {
			throw new ContinuableBusinessException(AmMessageId.AM001075E);
		}
	}

	public static final String[] getLendAssetPath(File masterPath, List<Object> paramList) {

		String[] assetFiles = AmExtractMessageAddonUtils.getLendParamInfo(paramList).getLendFiles();
		List<String> assetFileList = new ArrayList<String>();

		for (String assetFile : assetFiles) {
			assetFileList.add(TriStringUtils.linkPathBySlash(masterPath.getAbsolutePath(), assetFile));
		}

		return (String[]) assetFileList.toArray(new String[0]);
	}

	/**
	 * 選択可能なグループ情報を取得します。 <br>
	 * ユーザの所属するグループが対象になります。
	 *
	 * @param paramBean
	 * @param retBean
	 * @return
	 */
	public final List<String> getSelectGroupNameList(String lotId, String userId) {

		List<String> userGroupList = new ArrayList<String>();
		ILotDto lotDto = this.findLotDto( lotId );
		List<IGrpEntity> grpEntities = this.getUmFinderSupport().findGroupByUserId( userId);

		AmViewInfoAddonUtils.getSelectGroupNameList(userGroupList, lotDto, grpEntities);

		return userGroupList;
	}

	public List<Object> setParamList(ResourceSelectionFolderView<ICheckoutResourceViewBean> folderView,
			ResourceSelection resourceSelection, String selectedLotId, String selectedAreqId) {
		List<Object> paramList = new ArrayList<Object>();

		ILotDto lotDto = this.findLotDto(selectedLotId, AmTables.AM_LOT_GRP_LNK, AmTables.AM_LOT_MDL_LNK);
		List<IAreqDto> areqDtoList = new ArrayList<IAreqDto>();

		AreqCondition condition = AmResourceSelectionUtils.setAreqConditionForLockedResource(selectedLotId);

		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> assetAreqEntities =
				this.getAreqDao().find( condition.getCondition(), sort );
		for (IAreqEntity areqEntity: assetAreqEntities) {
			if (areqEntity.getAreqId().equals(selectedAreqId)) {
				continue;
			}
			IAreqDto areqDto = this.findAreqDto(areqEntity, AmTables.AM_AREQ_FILE);
			areqDtoList.add(areqDto);
		}

		paramList.add(lotDto);
		paramList.add(areqDtoList.toArray(new IAreqDto[0]));
		paramList.add(resourceSelection);
		paramList.add(folderView);

		return paramList;
	}

	public String[] getCheckedOutFile(IAreqEntity areqEntity) {

		List<IAreqFileEntity> areqFileEntities = this.findAreqFileEntities(areqEntity.getAreqId(), AreqCtgCd.LendingRequest);

		List<String> resourcesPath = new ArrayList<String>();

		for (IAreqFileEntity entity: areqFileEntities) {
			String relativePath = entity.getFilePath();
			resourcesPath.add(TriStringUtils.convertPath(relativePath, true));
		}
		return resourcesPath.toArray(new String[0]);
	}

	public static void setAssetApplyEntityBySaveDraftMode(IAreqEntity entity,
			ChaLibEntryInputBaseInfoBean inputBaseInfoBean) {

		entity.setAreqCtgCd(AreqCtgCd.LendingRequest.value());
		entity.setSummary(inputBaseInfoBean.getInSubject());
		entity.setContent(inputBaseInfoBean.getInContent());
		entity.setStsId(AmAreqStatusId.DraftCheckoutRequest.getStatusId());
		entity.setUpdTimestamp(TriDateUtils.getSystemTimestamp());
		entity.setUpdUserNm(inputBaseInfoBean.getInUserName());
		entity.setUpdUserId(inputBaseInfoBean.getInUserId());
		entity.setDelStsId(StatusFlg.off);
		entity.setRtnReqDueDate(inputBaseInfoBean.getCheckinDueDate());
		entity.setAssigneeId(inputBaseInfoBean.getAssigneeId());
		entity.setAssigneeNm(inputBaseInfoBean.getAssigneeNm());

		if (TriStringUtils.isEmpty(entity.getAreqId())) {
			entity.setPjtId(inputBaseInfoBean.getInPrjNo());
			entity.setGrpId(inputBaseInfoBean.getInGroupId());
			entity.setGrpNm(inputBaseInfoBean.getInGroupName());
			entity.setLendReqTimestamp(TriDateUtils.getSystemTimestamp());
			entity.setLendReqUserNm(inputBaseInfoBean.getInUserName());
			entity.setLendReqUserId(inputBaseInfoBean.getInUserId());
		}
	}
}
