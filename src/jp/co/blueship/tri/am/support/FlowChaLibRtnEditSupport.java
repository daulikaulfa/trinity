package jp.co.blueship.tri.am.support;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却申請系イベントのサポートClass <br>
 * <p>
 * 返却受付を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChaLibRtnEditSupport extends AmFinderSupport /*
															 * extends
															 * FinderSupport
															 */{

	IFilesDiffer differ = null;

	public final void setDiffer(IFilesDiffer diff) {
		differ = diff;
	}

	/**
	 * 参照元と参照先の資産を比較し、「新規」「変更」「未変更」の判定を行う。
	 *
	 * @param assetResource 比較する資産
	 * @param srcPath 参照元のパス
	 * @param diffPath 参照先のパス
	 */
	@SuppressWarnings("deprecation")
	public final void setFileStatusByAssetResourceInfoBean(ChaLibEntryAssetResourceInfoBean assetResource, File srcPath, File diffPath) {

		IFileDiffResult[] results = differ.execute(srcPath, diffPath);
		for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
			for (int i = 0; i < results.length; i++) {
				if (viewBean.getResourcePath().endsWith(results[i].getFilePath())) {
					if (!results[i].isMasterExists()) {
						viewBean.setFileStatus(FileStatus.Add.getStatus());
						viewBean.setFileStatusId(FileStatus.Add.getStatusId());
					} else if (results[i].isDiff()) {
						viewBean.setFileStatus(FileStatus.Edit.getStatus());
						viewBean.setFileStatusId(FileStatus.Edit.getStatusId());
					} else {
						viewBean.setFileStatus(FileStatus.Same.getStatus());
						viewBean.setFileStatusId(FileStatus.Same.getStatusId());
					}
				}
			}
		}
	}

	/**
	 * 参照元と参照先の資産を比較し、「diff」ボタン表示/非表示の判定を行う。
	 *
	 * @param assetResource 比較する資産
	 */
	public final void setDiffEnabledByUcfExtension(ChaLibEntryAssetResourceInfoBean assetResource) {
		Map<String, IExtEntity> ucfExtensionMap = this.getExtMap();

		for (ChaLibAssetResourceViewBean viewBean : assetResource.getAssetResourceViewBeanList()) {
			// バイナリ資産or拡張子未登録の資産はDiffボタンを非表示にする
			viewBean.setDiffEnabled(!AmBusinessFileUtils.isBinary(viewBean.getResourcePath(), ucfExtensionMap));
		}
	}

	/**
	 * 選択可能なロットを取得します。
	 *
	 * @return
	 */

	public final List<LotViewBean> getLotViewBeanForPullDown(GenericServiceBean retBean) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		LotCondition lotCondition = AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		ISqlSort lotSort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibTop();
		List<ILotEntity> lotEntities = this.getLotDao().find(lotCondition.getCondition(), lotSort);
		List<ILotEntity> lotEntitys = lotEntities;
		List<ILotDto> lotDto = this.findLotDto(lotEntitys);

		if (TriCollectionUtils.isEmpty(lotEntitys)) {
			retBean.setInfoMessage(AmMessageId.AM001007E);
		}

		AmViewInfoAddonUtils.populateLotViewBeanPjtLotEntityForPullDown(//
				lotViewBeanList,//
				lotDto,//
				this.getUmFinderSupport().findGroupByUserId(retBean.getUserId()), this.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId());

		if (TriCollectionUtils.isNotEmpty(lotEntitys) && TriCollectionUtils.isEmpty(lotViewBeanList)) {
			retBean.setInfoMessage(AmMessageId.AM001122E);
		}

		return lotViewBeanList;
	}

	/**
	 * 返却時に資産の重複チェックを行うかどうかを取得する。
	 *
	 * @param applyNo 申請番号
	 * @return 返却時に資産の重複チェックを行うのであればtrue、そうでなければfalse
	 */
	public boolean isAllowAssetDuplicationCheck(String applyNo) {

		IAreqEntity areqEntity = this.findAreqEntity(applyNo);
		IPjtEntity pjtEntity = this.findPjtEntity(areqEntity.getPjtId());

		ILotEntity lotEntity = this.findLotEntity(pjtEntity.getLotId());

		return lotEntity.getIsAssetDup().parseBoolean();
	}

	/**
	 * 申請情報の配列から、変更管理番号を取得し、配列で返す<br>
	 *
	 * @param assetApplyEntityArray 申請情報の配列
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNoArrayFromAssetApplyArray(IAreqEntity[] assetApplyEntityArray) {
		List<String> pjtNoList = new ArrayList<String>();
		for (IAreqEntity assetApplyEntity : assetApplyEntityArray) {
			pjtNoList.add(assetApplyEntity.getPjtId());
		}
		return pjtNoList.toArray(new String[0]);
	}

	/**
	 * 変更管理番号の配列から、変更管理情報を取得し、変更管理番号をキーとしたMapで返す<br>
	 *
	 * @param pjtNoArray 変更管理番号の配列
	 * @return 変更管理番号をキーとしたMap
	 */
	public Map<String, IPjtEntity> getPjtEntityPjtNoMap(String[] pjtNoArray) {
		PjtCondition condition = new PjtCondition();

		String[] conditionPjtNo = pjtNoArray;

		if (TriStringUtils.isEmpty(conditionPjtNo)) {
			// ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[] { "" };
		}

		condition.setPjtIds(conditionPjtNo);

		ISqlSort sort = new SortBuilder();
		List<IPjtEntity> entities = this.getPjtDao().find(condition.getCondition(), sort);

		IPjtEntity[] pjtEntityArray = entities.toArray(new IPjtEntity[0]);
		Map<String, IPjtEntity> lhMap = new LinkedHashMap<String, IPjtEntity>();
		for (IPjtEntity pjtEntity : pjtEntityArray) {
			lhMap.put(pjtEntity.getPjtId(), pjtEntity);
		}
		return lhMap;
	}

	public List<Object> setParamList(ResourceSelectionFolderView<ICheckinResourceViewBean> folderView,
			ResourceSelection resourceSelection, String selectedAreqId) {
		List<Object> paramList = new ArrayList<Object>();

		IAreqDto areqDto = new AreqDto();
		IAreqEntity areqEntity = this.findAreqEntity(selectedAreqId);
		areqDto.setAreqEntity(areqEntity);
		IPjtEntity pjtEntity = this.findPjtEntity(areqDto.getAreqEntity().getPjtId());
		ILotDto lotDto = new LotDto();
		ILotEntity lotEntity = this.findLotEntity(pjtEntity.getLotId());
		lotDto.setLotEntity(lotEntity);
		Map<String, IExtEntity> ucfExtensionMap = this.getExtMap();

		paramList.add( lotDto );
		paramList.add( areqDto );
		paramList.add( resourceSelection );
		paramList.add( folderView );
		paramList.add( ucfExtensionMap );

		return paramList;
	}
}
