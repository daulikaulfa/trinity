package jp.co.blueship.tri.am.support;

import jp.co.blueship.tri.am.dao.areq.AreqAttachedFileNumberingDao;
import jp.co.blueship.tri.am.dao.areq.AreqFileNumberingDao;
import jp.co.blueship.tri.am.dao.areq.IAreqAttachedFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqBinaryFileDao;
import jp.co.blueship.tri.am.dao.areq.IAreqDao;
import jp.co.blueship.tri.am.dao.areq.IAreqFileDao;
import jp.co.blueship.tri.am.dao.areq.IViewAssetRegisterDao;
import jp.co.blueship.tri.am.dao.areq.LendingReqNumberingDao;
import jp.co.blueship.tri.am.dao.baseline.HeadBlNumberingDao;
import jp.co.blueship.tri.am.dao.baseline.IHeadBlDao;
import jp.co.blueship.tri.am.dao.baseline.IHeadBlMdlLnkDao;
import jp.co.blueship.tri.am.dao.baseline.IHeadLotBlLnkDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlMdlLnkDao;
import jp.co.blueship.tri.am.dao.baseline.ILotBlReqLnkDao;
import jp.co.blueship.tri.am.dao.baseline.LotBlNumberingDao;
import jp.co.blueship.tri.am.dao.ext.IExtDao;
import jp.co.blueship.tri.am.dao.head.IHeadDao;
import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.ILotGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotMdlLnkDao;
import jp.co.blueship.tri.am.dao.lot.ILotRelEnvLnkDao;
import jp.co.blueship.tri.am.dao.lot.LotNumberingDao;
import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.PjtNumberingDao;
import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlAreqLnkDao;
import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlDao;
import jp.co.blueship.tri.am.dao.pjtavl.PjtAvlNumberingDao;
import jp.co.blueship.tri.am.dao.vcsrepos.IVcsMdlDao;
import jp.co.blueship.tri.am.dao.vcsrepos.IVcsReposDao;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.um.dao.hist.IAreqHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IPjtHistDao;

/**
 * AM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 */
public abstract class AmDaoFinder extends FinderSupport implements IAmDaoFinder {

	private HeadBlNumberingDao headBlNumberingDao = null;
	private LotBlNumberingDao lotBlNumberingDao = null;
	private LotNumberingDao lotNumberingDao = null;
	private PjtNumberingDao pjtNumberingDao = null;
	private PjtAvlNumberingDao pjtAvlNumberingDao = null;
	private LendingReqNumberingDao lendingReqNumberingDao = null;
	private AreqFileNumberingDao areqFileNumberingDao = null;
	private AreqAttachedFileNumberingDao areqAttachedFileNumberingDao = null;

	private IHeadDao headDao = null;
	private IHeadBlDao headBlDao = null;
	private IHeadBlMdlLnkDao headBlMdlLnkDao = null;
	private IHeadLotBlLnkDao headLotBlLnkDao = null;
	private ILotBlDao lotBlDao = null;
	private ILotBlMdlLnkDao lotBlMdlLnkDao = null;
	private ILotBlReqLnkDao lotBlReqLnkDao = null;
	private ILotDao lotDao = null;
	private ILotMdlLnkDao lotMdlLnkDao = null;
	private ILotGrpLnkDao lotGrpLnkDao = null;
	private ILotMailGrpLnkDao lotMailGrpLnkDao = null;
	private ILotRelEnvLnkDao lotRelEnvLnkDao = null;
	private IPjtDao pjtDao = null;
	private IPjtHistDao pjtHistDao = null;
	private IPjtAvlDao pjtAvlDao = null;
	private IPjtAvlAreqLnkDao pjtAvlAreqLnkDao = null;
	private IAreqDao areqDao = null;
	private IAreqFileDao areqFileDao = null;
	private IAreqBinaryFileDao areqBinaryFileDao = null;
	private IAreqAttachedFileDao areqAttachedFileDao = null;
	private IAreqHistDao areqHistDao = null;

	private IVcsReposDao vcsReposDao = null;
	private IVcsMdlDao vcsMdlDao = null;
	private IExtDao extDao = null;

	private IViewAssetRegisterDao viewAssetRegisterDao = null;


	public void setLotNumberingDao(LotNumberingDao lotNumberingDao) {
		this.lotNumberingDao = lotNumberingDao;
	}
	public HeadBlNumberingDao getHeadBlNumberingDao() {
		return headBlNumberingDao;
	}
	public void setHeadBlNumberingDao(HeadBlNumberingDao headBlNumberingDao) {
		this.headBlNumberingDao = headBlNumberingDao;
	}
	public LotBlNumberingDao getLotBlNumberingDao() {
		return lotBlNumberingDao;
	}
	public void setLotBlNumberingDao(LotBlNumberingDao lotBlNumberingDao) {
		this.lotBlNumberingDao = lotBlNumberingDao;
	}
	public LotNumberingDao getLotNumberingDao() {
		return lotNumberingDao;
	}
	public void setPjtNumberingDao(PjtNumberingDao pjtNumberingDao) {
		this.pjtNumberingDao = pjtNumberingDao;
	}
	public PjtNumberingDao getPjtNumberingDao() {
		return pjtNumberingDao;
	}
	public void setPjtAvlNumberingDao(PjtAvlNumberingDao pjtAvlNumberingDao) {
		this.pjtAvlNumberingDao = pjtAvlNumberingDao;
	}
	public PjtAvlNumberingDao getPjtAvlNumberingDao() {
		return pjtAvlNumberingDao;
	}
	public LendingReqNumberingDao getLendingReqNumberingDao() {
		return lendingReqNumberingDao;
	}
	public void setLendingReqNumberingDao(LendingReqNumberingDao lendingNumberingDao) {
		this.lendingReqNumberingDao = lendingNumberingDao;
	}
	public AreqFileNumberingDao getAreqFileNumberingDao() {
		return areqFileNumberingDao;
	}
	public void setAreqFileNumberingDao(AreqFileNumberingDao areqFileNumberingDao) {
		this.areqFileNumberingDao = areqFileNumberingDao;
	}
	public AreqAttachedFileNumberingDao getAreqAttachedFileNumberingDao() {
		return areqAttachedFileNumberingDao;
	}
	public void setAreqAttachedFileNumberingDao(AreqAttachedFileNumberingDao areqAttachedFileNumberingDao) {
		this.areqAttachedFileNumberingDao = areqAttachedFileNumberingDao;
	}
	public IHeadDao getHeadDao() {
		return headDao;
	}
	public void setHeadDao(IHeadDao headDao) {
		this.headDao = headDao;
	}
	
	@Override
	public IHeadBlDao getHeadBlDao() {
		return headBlDao;
	}
	public void setHeadBlDao(IHeadBlDao headBlDao) {
		this.headBlDao = headBlDao;
	}
	
	public IHeadBlMdlLnkDao getHeadBlMdlLnkDao() {
		return headBlMdlLnkDao;
	}
	public void setHeadBlMdlLnkDao(IHeadBlMdlLnkDao headBlMdlLnkDao) {
		this.headBlMdlLnkDao = headBlMdlLnkDao;
	}
	public IHeadLotBlLnkDao getHeadLotBlLnkDao() {
		return headLotBlLnkDao;
	}
	public void setHeadLotBlLnkDao(IHeadLotBlLnkDao headLotBlLnkDao) {
		this.headLotBlLnkDao = headLotBlLnkDao;
	}

	@Override
	public ILotBlDao getLotBlDao() {
		return lotBlDao;
	}
	public void setLotBlDao(ILotBlDao lotBlDao) {
		this.lotBlDao = lotBlDao;
	}
	@Override
	public ILotBlMdlLnkDao getLotBlMdlLnkDao() {
		return lotBlMdlLnkDao;
	}
	public void setLotBlMdlLnkDao(ILotBlMdlLnkDao lotBlMdlLnkDao) {
		this.lotBlMdlLnkDao = lotBlMdlLnkDao;
	}
	@Override
	public ILotBlReqLnkDao getLotBlReqLnkDao() {
		return lotBlReqLnkDao;
	}
	public void setLotBlReqLnkDao(ILotBlReqLnkDao lotBlReqLnkDao) {
		this.lotBlReqLnkDao = lotBlReqLnkDao;
	}
	public ILotDao getLotDao() {
		return lotDao;
	}
	public void setLotDao(ILotDao lotDao) {
		this.lotDao = lotDao;
	}
	@Override
	public ILotMdlLnkDao getLotMdlLnkDao() {
		return lotMdlLnkDao;
	}
	public void setLotMdlLnkDao(ILotMdlLnkDao lotMdlLnkDao) {
		this.lotMdlLnkDao = lotMdlLnkDao;
	}
	public ILotGrpLnkDao getLotGrpLnkDao() {
		return lotGrpLnkDao;
	}
	public void setLotGrpLnkDao(ILotGrpLnkDao lotGrpLnkDao) {
		this.lotGrpLnkDao = lotGrpLnkDao;
	}
	public ILotMailGrpLnkDao getLotMailGrpLnkDao() {
		return lotMailGrpLnkDao;
	}
	public void setLotMailGrpLnkDao(ILotMailGrpLnkDao lotMailGrpLnkDao) {
		this.lotMailGrpLnkDao = lotMailGrpLnkDao;
	}
	public ILotRelEnvLnkDao getLotRelEnvLnkDao() {
		return lotRelEnvLnkDao;
	}
	public void setLotRelEnvLnkDao(ILotRelEnvLnkDao lotRelEnvLnkDao) {
		this.lotRelEnvLnkDao = lotRelEnvLnkDao;
	}
	@Override
	public IPjtDao getPjtDao() {
		return pjtDao;
	}
	public void setPjtDao(IPjtDao pjtDao) {
		this.pjtDao = pjtDao;
	}

	@Override
	public IPjtHistDao getPjtHistDao(){
		return this.pjtHistDao;
	}
	public void setPjtHistDao(IPjtHistDao pjtHistDao){
		this.pjtHistDao = pjtHistDao;
	}

	public IPjtAvlDao getPjtAvlDao() {
		return pjtAvlDao;
	}
	public void setPjtAvlDao(IPjtAvlDao pjtAvlDao) {
		this.pjtAvlDao = pjtAvlDao;
	}
	public IPjtAvlAreqLnkDao getPjtAvlAreqLnkDao() {
		return pjtAvlAreqLnkDao;
	}
	public void setPjtAvlAreqLnkDao(IPjtAvlAreqLnkDao pjtAvlAreqLnkDao) {
		this.pjtAvlAreqLnkDao = pjtAvlAreqLnkDao;
	}
	@Override
	public IAreqDao getAreqDao() {
		return areqDao;
	}
	public void setAreqDao(IAreqDao areqDao) {
		this.areqDao = areqDao;
	}
	@Override
	public IAreqFileDao getAreqFileDao() {
		return areqFileDao;
	}
	public void setAreqFileDao(IAreqFileDao areqFileDao) {
		this.areqFileDao = areqFileDao;
	}
	@Override
	public IAreqBinaryFileDao getAreqBinaryFileDao() {
		return areqBinaryFileDao;
	}
	public void setAreqBinaryFileDao(IAreqBinaryFileDao areqBinaryFileDao) {
		this.areqBinaryFileDao = areqBinaryFileDao;
	}
	public IAreqAttachedFileDao getAreqAttachedFileDao() {
		return areqAttachedFileDao;
	}
	public void setAreqAttachedFileDao(IAreqAttachedFileDao areqAttachedFileDao) {
		this.areqAttachedFileDao = areqAttachedFileDao;
	}
	@Override
	public IAreqHistDao getAreqHistDao() {
		return areqHistDao;
	}
	public void setAreqHistDao(IAreqHistDao areqHistDao) {
		this.areqHistDao = areqHistDao;
	}
	public IVcsReposDao getVcsReposDao() {
		return vcsReposDao;
	}
	public void setVcsReposDao(IVcsReposDao vcsReposDao) {
		this.vcsReposDao = vcsReposDao;
	}
	public IVcsMdlDao getVcsMdlDao() {
		return vcsMdlDao;
	}
	public void setVcsMdlDao(IVcsMdlDao vcsMdlDao) {
		this.vcsMdlDao = vcsMdlDao;
	}
	@Override
	public IExtDao getExtDao() {
		return extDao;
	}
	public void setExtDao(IExtDao extDao) {
		this.extDao = extDao;
	}
	/**
	 * viewAssetRegisterDaoを取得します。
	 * @return viewAssetRegisterDao
	 */
	public IViewAssetRegisterDao getViewAssetRegisterDao() {
		return viewAssetRegisterDao;
	}
	/**
	 * viewAssetRegisterDaoを設定します。
	 * @param viewAssetRegisterDao viewAssetRegisterDao
	 */
	public void setViewAssetRegisterDao(IViewAssetRegisterDao viewAssetRegisterDao) {
		this.viewAssetRegisterDao = viewAssetRegisterDao;
	}
}
