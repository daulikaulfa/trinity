package jp.co.blueship.tri.am.support;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * AM関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Sam
 */
public interface IAmFinderSupport extends IAmDaoFinder {
	/**
	 * smFinderSupportを取得します。
	 * @return smFinderSupport
	 */
	public ISmFinderSupport getSmFinderSupport();
	/**
	 * umFinderSupportを取得します。
	 * @return umFinderSupport
	 */
	public IUmFinderSupport getUmFinderSupport();

	/**
	 * bmFinderSupportを取得します。
	 * @return bmFinderSupport
	 */
	public IBmFinderSupport getBmFinderSupport();

	/**
	 * HEAD・ベースラインIDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByHeadBlId();

	/**
	 * ロット・ベースラインIDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByLotBlId();

	/**
	 * ロットIDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByLotId();

	/**
	 * 変更情報IDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByPjtId();

	/**
	 * 変更承認連番を採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByPjtAvlSeqNo();
	/**
	 * 貸出資産申請IDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByLendingReqId();
	/**
	 * 資産申請IDを採番します。
	 *
	 * param seq 採番編集するシーケンス番号
	 * @return 採番したユニークID
	 */
	public String nextvalByAttachedFileSeqNo( int seq );
	/**
	 * 資産申請IDを採番します。
	 *
	 * param areqId
	 * @return 採番したユニークID
	 */
	public String nextvalByMultipleAttachedFileSeqNo( String areqId );
	/**
	 * すべての拡張子情報を取得します。
	 * <br>nullの拡張子があれば、""（空文字）に変換します。
	 *
	 * @return 取得したマップエンティティを戻します。
	 */
	public Map<String, IExtEntity> getExtMap();

	/**
	 * ロット情報エンティティからアクセス可能なリリース環境エンティティの配列を取得する
	 * <br>旧データ互換のため、ロットにリリース環境が設定されてないデータでも動作を保障する。
	 * <br>ロットにリリース環境が設定されていない場合、進行中の全リリース環境を取得する。
	 *
	 * @param envAllEntities 全リリース環境情報
	 * @param lotList
	 * @return
	 */
	public IBldEnvEntity[] getAccessableRelEnvEntity( IBldEnvEntity[] envAllEntities, List<ILotDto> lotList );

	/**
	 * ロットの検索条件にアクセス可能なロット番号を取得する
	 *
	 * @param condition ロット検索条件
	 * @param sort ソート条件
	 * @param retBean GenericServiceBean オブジェクト
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return アクセス可能なロット
	 */
	public List<ILotEntity> getAccessableLotNumbers(
			IJdbcCondition condition,
			ISqlSort sort,
			GenericServiceBean retBean,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage );

	/**
	 * ロットの検索条件にアクセス可能なロット番号を設定する
	 * <br>処理効率が良くないため、以後は原則使用しない。
	 *
	 * @param condition ロット検索条件
	 * @param retBean GenericServiceBean オブジェクト
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return アクセス可能なロットが１件もない場合、falseを戻す。
	 * @deprecated
	 */
	public boolean setAccessableLotNumbers(
			IJdbcCondition condition,GenericServiceBean retBean, List<String> disableLinkLotNumbers, boolean isSetMessage );
	/**
	 * ロットの検索条件にアクセス可能なロット番号を設定する
	 * <br>処理効率が良くないため、以後は原則使用しない。
	 *
	 * @param condition ロット検索条件
	 * @param retBean GenericServiceBean オブジェクト
	 * @param isIncludeAllowList 表示のみのロットを含めるかどうか
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return アクセス可能なロットが１件もない場合、falseを戻す。
	 * @deprecated
	 */
	public boolean setAccessableLotNumbers(
			IJdbcCondition condition,
			GenericServiceBean retBean,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage );

	/**
	 * ロットの検索条件にアクセス可能なロット番号を取得する
	 *
	 * @param condition ロット検索条件
	 * @param sort ソート条件
	 * @param retBean GenericServiceBean オブジェクト
	 * @param isIncludeAllowList 表示のみのロットを含めるかどうか
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return アクセス可能なロット
	 */
	public List<ILotEntity> getAccessableLotNumbers(
			IJdbcCondition condition,
			ISqlSort sort,
			GenericServiceBean retBean,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage );

	/**
	 * ロット情報エンティティからロット番号の配列を取得する
	 *
	 * @param pjtLotEntityArray
	 * @return
	 */
	public String[] convertToLotNo( List<ILotEntity> pjtLotEntityArray );

	/**
	 * パスワード管理情報よりSVNのユーザパスワード情報を取得します。
	 *
	 * @return 取得したパスワード管理エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPassMgtEntity findPassMgtEntityBySVN() throws TriSystemException;

	/**
	 * HEAD Entityを取得します。
	 *
	 * @param vcsCategory VCS分類コード
	 * @return 取得したHEAD Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IHeadEntity findHeadEntity( VcsCategory vcsCategory ) throws TriSystemException;

	/**
	 * VCS Repository Entityを取得します。
	 *
	 * @param vcsReposId VCSレポジトリID
	 * @return 取得したVCS Repository Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IVcsReposEntity findVcsReposEntity( String vcsReposId ) throws TriSystemException;

	/**
	 * モジュール名に対応するVCS Repository Entityを取得します。
	 *
	 * @param moduleNames モジュール名
	 * @return 取得したVCS Repository Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IVcsReposEntity findVcsReposEntityFromModuleNames( String... moduleNames ) throws TriSystemException;

	/**
	 * VCS Repository Entityを取得します。
	 *
	 * @return 取得したVCS Repository Entityを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IVcsReposEntity> findVcsReposEntities() throws TriSystemException;

	/**
	 * HEAD・ベースライン情報を取得します。
	 *
	 * @param headBlId HEAD・ベースラインID
	 * @return 取得したHEAD・ベースライン情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IHeadBlEntity findHeadBlEntity( String headBlId ) throws TriSystemException;

	/**
	 * ロット・ベースライン情報を取得します。
	 *
	 * @param lotBlId ロット・ベースラインID
	 * @return 取得したロット・ベースライン情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotBlEntity findLotBlEntity( String lotBlId ) throws TriSystemException;

	/**
	 * ロット・ベースライン情報のListを取得します。
	 *
	 * @param lotBlIds ロット・ベースラインID
	 * @return 取得したロット・ベースライン情報エンティティのListを戻します。
	 */
	public List<ILotBlEntity> findLotBlEntities( String... lotBlIds );

	/**
	 * ロット情報を取得します。
	 *
	 * @param lotId ロットID
	 * @return 取得したロット情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotEntity findLotEntity( String lotId ) throws TriSystemException;

	/**
	 * ロット情報を取得します。
	 *
	 * @param cache map for cache
	 * @param lotId ロットID
	 * @return 取得したロット情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotEntity findLotEntity( Map<String, ILotEntity> cache, String lotId ) throws TriSystemException;

	/**
	 * ロット情報を取得します。
	 *
	 * @param lotId ロットID
	 * @param delStsId 削除ステータスID
	 * @return 取得したロット情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotEntity findLotEntity( String lotId, StatusFlg delStsId ) throws TriSystemException;

	/**
	 * 変更情報を取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @return 取得した変更情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPjtEntity findPjtEntity( String pjtId ) throws TriSystemException;

	/**
	 * 変更情報のListを取得します。
	 *
	 * @param pjtIds 変更情報ID
	 * @return 取得した変更情報エンティティのListを戻します。
	 */
	public List<IPjtEntity> findPjtEntities( String... pjtIds );

	/**
	 * 変更情報のListを取得します。
	 *
	 * @param sort ソート条件
	 * @param pjtIds 変更情報ID
	 * @return 取得した変更情報エンティティのListを戻します。
	 */
	public List<IPjtEntity> findPjtEntities( ISqlSort sort, String... pjtIds );

	/**
	 * 変更承認情報を取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param seqNo 変更承認連番
	 * @return 取得した変更承認情報エンティティを戻します。
	 */
	public IPjtAvlEntity findPjtAvlEntity( String pjtId, String seqNo );

	/**
	 * 変更承認情報を取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param seqNo 変更承認連番
	 * @param delStsId 削除ステータスID
	 * @return 取得した変更承認情報エンティティを戻します。
	 */
	public IPjtAvlEntity findPjtAvlEntity( String pjtId, String seqNo, StatusFlg delStsId );

	/**
	 * 変更承認のListを取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param seqNo 変更承認連番
	 * @return 取得した変更承認情報エンティティのListを戻します。
	 */
	public List<IPjtAvlEntity> findPjtAvlEntities( String pjtId ) throws TriSystemException;

	/**
	 * 変更承認・申請情報のListを取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param seqNo 変更承認連番
	 * @return 取得した変更承認・申請情報エンティティのListを戻します。
	 */
	public List<IPjtAvlAreqLnkEntity> findPjtAvlAreqLnkEntities( String pjtId, String seqNo, String... areqIds );

	/**
	 * 資産申請情報を取得します。
	 *
	 * @param areqId 資産申請ID
	 * @return 取得した資産申請情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 *
	 */
	public IAreqEntity findAreqEntity( String areqId ) throws TriSystemException;

	/**
	 * 資産申請情報を取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param delStsId 削除ステータスID
	 * @return 取得した資産申請情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 *
	 */
	public IAreqEntity findAreqEntity( String areqId, StatusFlg delStsId ) throws TriSystemException;

	/**
	 * 資産申請情報のListを取得します。
	 *
	 * @param areqIds 資産申請ID
	 * @return 取得した資産申請情報エンティティのListを戻します。
	 *
	 */
	public List<IAreqEntity> findAreqEntities( String... areqIds );

	/**
	 * 資産申請情報のListを取得します。
	 *
	 * @param sort ソート条件
	 * @param areqIds 資産申請ID
	 * @return 取得した資産申請情報エンティティのListを戻します。
	 *
	 */
	public List<IAreqEntity> findAreqEntities( ISqlSort sort, String... areqIds );

	/**
	 * 資産申請ファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param areqCtgCds 資産申請分類コード
	 * @return 取得した資産申請ファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqFileEntity> findAreqFileEntities( String areqId, AreqCtgCd... areqCtgCds );

	/**
	 * 資産申請ファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param delStsId 削除ステータスID
	 * @param areqCtgCds 資産申請分類コード
	 * @return 取得した資産申請ファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqFileEntity> findAreqFileEntities( String areqId, StatusFlg delStsId, AreqCtgCd... areqCtgCds );

	/**
	 * 資産申請バイナリファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param areqCtgCds 資産申請分類コード
	 * @return 取得した資産申請バイナリファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqBinaryFileEntity> findAreqBinaryFileEntities( String areqId, AreqCtgCd... areqCtgCds );

	/**
	 * 資産申請バイナリファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param delStsId 削除ステータスID
	 * @param areqCtgCds 資産申請分類コード
	 * @return 取得した資産申請バイナリファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqBinaryFileEntity> findAreqBinaryFileEntities( String areqId, StatusFlg delStsId, AreqCtgCd... areqCtgCds );

	/**
	 * 申請添付ファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @return 取得した申請添付ファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqAttachedFileEntity> findAreqAttachedFileEntities( String areqId );

	/**
	 * 申請添付ファイルエンティティのListを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param delStsId 削除ステータスID
	 * @return 取得した申請添付ファイルエンティティのListを戻します。
	 *
	 */
	public List<IAreqAttachedFileEntity> findAreqAttachedFileEntities( String areqId, StatusFlg delStsId );

	/**
	 * VCS・リポジトリDTOのListを取得します。
	 * <br>VCS・モジュールはモジュール名の昇順でソートされます。
	 *
	 * @return 取得したVCS・リポジトリDTOのListを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IVcsReposDto> findVcsReposDto();

	/**
	 * VCS・リポジトリDTOのListを取得します。
	 * <br>VCS・モジュールはモジュール名の昇順でソートされます。
	 *
	 * @param sort VCS・モジュールEntityのソート順
	 * @return 取得したVCS・リポジトリDTOのListを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IVcsReposDto> findVcsReposDto( ISqlSort sort );

	/**
	 * VCS・リポジトリDTOのListを取得します。
	 * <br>VCS・モジュールはモジュール名の昇順でソートされます。
	 *
	 * @param mdlEntities ロット・モジュールEntityのリスト
	 * @return 取得したVCS・リポジトリDTOのListを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IVcsReposDto> findVcsReposDtoFromLotMdlLnkEntities( List<ILotMdlLnkEntity> mdlEntities );

	/**
	 * HEAD・ベースラインDTOを取得します。
	 *
	 * @param headBlId HEAD・ベースラインID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_HEAD_BL_MDL_LNK}、
	 * 					{@link AmTables#AM_HEAD_LOT_BL_LNK}、
	 * @return 取得したHEAD・ベースラインDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IHeadBlDto findHeadBlDto( String headBlId, AmTables... tables ) throws TriSystemException;

	/**
	 * HEAD・ベースラインDTOを取得します。
	 *
	 * @param headBlEntity HEAD・ベースラインEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_HEAD_BL_MDL_LNK}、
	 * 					{@link AmTables#AM_HEAD_LOT_BL_LNK}、
	 * @return 取得したHEAD・ベースラインDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IHeadBlDto findHeadBlDto( IHeadBlEntity headBlEntity, AmTables... tables ) throws TriSystemException;

	/**
	 * ロット・ベースラインDTOを取得します。
	 *
	 * @param lotBlId ロット・ベースラインID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_BL_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_BL_REQ_LNK}、
	 * @return 取得したロット・ベースラインDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotBlDto findLotBlDto( String lotBlId, AmTables... tables ) throws TriSystemException;

	/**
	 * ロット・ベースラインDTOを取得します。
	 *
	 * @param entities ロット・ベースラインEntityのList
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_BL_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_BL_REQ_LNK}、
	 * @return 取得したロット・ベースラインDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<ILotBlDto> findLotBlDto( List<ILotBlEntity> entities, AmTables... tables ) throws TriSystemException;

	/**
	 * ロットDTOを取得します。
	 *
	 * @param lotId ロットID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MAIL_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_REL_ENV_LNK}
	 * @return 取得したロットDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public ILotDto findLotDto( String lotId, AmTables... tables ) throws TriSystemException;

	/**
	 * ロットDTOを取得します。
	 *
	 * @param lotEntity ロットEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MAIL_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_REL_ENV_LNK}
	 * @return 取得したロットDTOを戻します。
	 */
	public ILotDto findLotDto( ILotEntity lotEntity, AmTables... tables );

	/**
	 * ロットDTOを取得します。
	 *
	 * @param lotDto ロットDTO（ILotEntityの設定は必須）
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MAIL_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_REL_ENV_LNK}
	 * @return 取得したロットDTOを戻します。
	 */
	public ILotDto findLotDto( ILotDto lotDto, AmTables... tables );

	/**
	 * ロットDTOを取得します。
	 *
	 * @param entities ロットEntityのリスト
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_LOT_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MAIL_GRP_LNK}、
	 * 					{@link AmTables#AM_LOT_MDL_LNK}、
	 * 					{@link AmTables#AM_LOT_REL_ENV_LNK}
	 * @return 取得したロットDTOを戻します。
	 */
	public List<ILotDto> findLotDto( List<ILotEntity> entities, AmTables... tables );

	/**
	 * 最新の変更承認DTOを取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_PJT_AVL}、
	 * 					{@link AmTables#AM_PJT_AVL_AREQ_LNK}、
	 * @return 取得した変更承認DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPjtAvlDto findPjtAvlDtoByLatest( String pjtId, AmTables... tables ) throws TriSystemException;

	/**
	 * 変更承認DTOを取得します。
	 *
	 * @param pjtEntity 変更情報Entity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_PJT_AVL}、
	 * 					{@link AmTables#AM_PJT_AVL_AREQ_LNK}、
	 * @return 取得した変更承認DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPjtAvlDto findPjtAvlDtoByLatest( IPjtEntity pjtEntity, AmTables... tables ) throws TriSystemException;

	/**
	 * 変更承認DTOを取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param pjtAvlSeqNo 変更承認連番
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_PJT_AVL}、
	 * 					{@link AmTables#AM_PJT_AVL_AREQ_LNK}、
	 * @return 取得した変更承認DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IPjtAvlDto findPjtAvlDto( String pjtId, String pjtAvlSeqNo, AmTables... tables ) throws TriSystemException;

	/**
	 * 変更承認DTOのListを取得します。
	 *
	 * @param pjtId 変更情報ID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_PJT_AVL}、
	 * 					{@link AmTables#AM_PJT_AVL_AREQ_LNK}、
	 * @return 取得した変更承認DTOのListを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IPjtAvlDto> findPjtAvlDto( String pjtId, AmTables... tables ) throws TriSystemException;

	/**
	 * 変更承認DTOのListを取得します。
	 *
	 * @param pjtAvlEntities 変更承認情報のList
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_PJT_AVL}、
	 * 					{@link AmTables#AM_PJT_AVL_AREQ_LNK}、
	 * @return 取得した変更承認DTOのListを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IPjtAvlDto> findPjtAvlDto( List<IPjtAvlEntity> pjtAvlEntities, AmTables... tables ) throws TriSystemException;

	/**
	 * 資産申請DTOを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_AREQ_FILE}、
	 * 					{@link AmTables#AM_AREQ_BINARY_FILE}、
	 * 					{@link AmTables#AM_AREQ_ATTACHED_FILE}
	 * @return 取得した資産申請DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IAreqDto findAreqDto( String areqId, AmTables... tables ) throws TriSystemException;

	/**
	 * 資産申請DTOを取得します。
	 *
	 * @param areqId 資産申請ID
	 * @param delStsId 削除ステータスID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_AREQ_FILE}、
	 * 					{@link AmTables#AM_AREQ_BINARY_FILE}、
	 * 					{@link AmTables#AM_AREQ_ATTACHED_FILE}
	 * @return 取得した資産申請DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IAreqDto findAreqDto( String areqId, StatusFlg delStsId, AmTables... tables ) throws TriSystemException;

	/**
	 * 資産申請DTOを取得します。
	 *
	 * @param areqEntity 資産申請Entity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_AREQ_FILE}、
	 * 					{@link AmTables#AM_AREQ_BINARY_FILE}、
	 * 					{@link AmTables#AM_AREQ_ATTACHED_FILE}
	 * @return 取得した資産申請DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IAreqDto findAreqDto( IAreqEntity areqEntity, AmTables... tables ) throws TriSystemException;

	/**
	 * 資産申請DTOを取得します。
	 *
	 * @param entities 資産申請EntityのList
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link AmTables#AM_AREQ_FILE}、
	 * 					{@link AmTables#AM_AREQ_BINARY_FILE}、
	 * 					{@link AmTables#AM_AREQ_ATTACHED_FILE}
	 * @return 取得した資産申請DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public List<IAreqDto> findAreqDto( List<IAreqEntity> entities, AmTables... tables ) throws TriSystemException;

	/**
	 * Cleaning a record of am_areq table
	 *
	 * @param areqId
	 */
	public void cleaningAreq( String areqId);

	public List<String> findBpIdsByLotBlId ( String lotBlId );
}
