package jp.co.blueship.tri.am;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckInOutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.CheckinRequestInputInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 *
 * @version V4.03.00
 * @author anh.nguyenduc
 */
public class AmResourceSelectionUtils {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final String treeMode = "tree";
	public static final String fileMode = "file";

	/**
	 * Returns the folder string of the resource entities.
	 *
	 * @param entities resource entities.
	 * @return Top Returns the relative path to the folder that begins with a "/".
	 */
	public static String[] foldersPathOf( List<IAreqFileEntity> entities ) {

		return foldersPathOf( entities, null, null );
	}

	/**
	 * Returns the folder string of the resource entities.
	 *
	 * @param entities resource entities.
	 * @param selectedPath Returns the folder in the specified folder.
	 * @param tree Please set object to store the folder attribute.
	 * @return Top Returns the relative path to the folder that begins with a "/".
	 */
	public static String[] foldersPathOf(
			List<IAreqFileEntity> entities,
			String selectedPath,
			ITreeFolderViewBean tree ) {

		Set<String> folderSet = new LinkedHashSet<String>();

		if ( null != selectedPath ) {
			selectedPath = TriStringUtils.convertPath(selectedPath, true);
		}

		for (IAreqFileEntity entity : entities) {
			String[] split = TriStringUtils.splitPath( entity.getFilePath() );
			String folderPath = TriStringUtils.convertPath( split[0], true );

			if ( TriStringUtils.isNotEmpty(selectedPath) ) {
				String ownerPath = tree.getOwner().getPath();

				if ( ownerPath.equals(selectedPath) ) {

					String[] folderSplit = TriStringUtils.split( TriStringUtils.trimHeadSeparator(folderPath), "/" );

					if (TriStringUtils.isEmpty( folderSplit )) {
						continue;
					}

					folderPath = folderSplit[0];

				} else {

					String[] folderSplit = TriStringUtils.split( TriStringUtils.trimHeadSeparator(TriStringUtils.convertRelativePath(selectedPath, folderPath)), "/" );

					if (TriStringUtils.isEmpty( folderSplit )) {
						continue;
					}

					folderPath = TriStringUtils.linkPathBySlash(selectedPath, folderSplit[0]);

					ITreeFolderViewBean tempTree = tree.getFolder(folderPath);

					if ( null == tempTree || ! selectedPath.equals(tempTree.getParent().getPath()) )
						continue;
				}
			}

			folderSet.add( folderPath );
		}

		return folderSet.toArray(new String[0]);
	}

	/**
	 * Convert the set entities in the parameter to ITreeFolderViewBean.
	 *
	 * @param entities resource entities.
	 * @return
	 */
	public static ITreeFolderViewBean getTreeFolderView( List<IAreqFileEntity> entities ) {
		ITreeFolderViewBean tree = TreeFolderViewBean.toBean( AmResourceSelectionUtils.foldersPathOf(entities) );

		Set<String> selectedFileSet = new HashSet<String>();
		for ( IAreqFileEntity entity: entities ) {
			selectedFileSet.add( TriStringUtils.convertPath( entity.getFilePath(), true ));
		}

		if (null != tree) {
			int rootCount = 0;
			if (TriStringUtils.isNotEmpty(tree.getFolders())) {
				rootCount = setCountFolder(tree, selectedFileSet);
			}
			tree.setCount(rootCount);
		}

		return tree;
	}

	public static void buildTreeFolderViewByInit( List<Object> paramList, File masterPath, boolean isCheckin ) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(paramList);
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		List<File> files = new ArrayList<File>();

		if ( isCheckin ) {
			List<File> folders = BusinessFileUtils.getListFiles(masterPath,
					AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
			for (File folder : folders) {
				if (folder.isDirectory()) {
					files.add(folder);
				}
			}

		} else {
			for (ILotMdlLnkEntity module : lotDto.getIncludeMdlEntities(true)) {
				File modulePath = new File(masterPath, module.getMdlNm());
				if (modulePath.exists())
					files.add(modulePath);
			}
		}

		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();

		ITreeFolderViewBean root = new TreeFolderViewBean().setOwner().setName("");

		File[] fileArray = (File[]) files.toArray(new File[0]);
		BusinessFileUtils.sortDefaultFileList(fileArray);
		int rootCount = 0;
		for (File file : fileArray) {

			// If the file is directory --> add to tree folder view
			int count = 0;
			if (file.isDirectory()) {
				TreeFolderViewBean subFolder = new TreeFolderViewBean().setName(file.getName());
				root.addFolder(subFolder);
				if (TriStringUtils.isNotEmpty(selectedFileSet)) {
					for (String selectedFile: selectedFileSet) {
						if (selectedFile.matches(subFolder.getPath() + "(/.*)")) {
							count++;
						}
					}
					subFolder.setCount(count);
				}
				requestViewBean.add(setFolderInformation(resourceSelection, file.isDirectory(), file.getName(), subFolder.getPath(), count));
			}
			rootCount += count;
		}
		root.setCount(rootCount);
		resourceSelection.setPath("/");
		folderView.setSelectedPath("/");
		folderView.setFolderView(root);
		folderView.setRequestViews(requestViewBean);
	}

	public static void rebuildTreeFolderView(List<Object> paramList) {
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(paramList);
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		ITreeFolderViewBean root = folderView.getFolderView();
		if (null != root) {
			int rootCount = 0;
			if (TriStringUtils.isNotEmpty(root.getFolders())) {
				rootCount = setCountFolder(root, selectedFileSet);
			}
			root.setCount(rootCount);
		}

	}

	public static int setCountFolder(ITreeFolderViewBean folderView, Set<String> selectedFileSet) {
		int parentCount = 0;
		if (TriStringUtils.isNotEmpty(folderView.getFolders())) {
			for (ITreeFolderViewBean bean: folderView.getFolders()) {
				if (bean.isOpenFolder() || TriStringUtils.isNotEmpty(bean.getFolders())) {
					setCountFolder(bean, selectedFileSet);
				}
				int count = getCountFile(bean.getPath(), selectedFileSet);
				((TreeFolderViewBean) bean).setCount(count);
				parentCount += count;
			}
		}
		return parentCount;
	}

	public static int getCountFile(String folderPath, Set<String> selectedFileSet) {
		int count = 0;
		for (String file : selectedFileSet) {
			if (file.matches(folderPath + "(/.*)")) {
				count++;
			}
		}
		return count;
	}

	public static void closeFolder(List<Object> paramList) {
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(paramList);
		String selectedPath = resourceSelection.getPath();
		ITreeFolderViewBean rootFolder = folderView.getFolderView();
		if (TriStringUtils.isNotEmpty(selectedPath)) {
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(false);
		}
	}

	public static void openFolder(List<Object> paramList, File masterPath) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(paramList);

		String selectedPath = resourceSelection.getPath();
		ITreeFolderViewBean rootFolder = folderView.getFolderView();

		List<ITreeFolderViewBean> listFolders = new ArrayList<ITreeFolderViewBean>();

		if (TriStringUtils.isNotEmpty(selectedPath)) {
			File selectedFile = new File(masterPath, selectedPath);
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(true);
			List<File> folders = BusinessFileUtils.getListFiles(selectedFile,
					AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
			for (File subFolder: folders) {
				if (subFolder.isDirectory()) {
					String subFolderRelativePath = TriStringUtils.convertRelativePath(masterPath, subFolder.getPath(), true);
					ITreeFolderViewBean subFileBean = rootFolder.getFolder(subFolderRelativePath);
					if (null == subFileBean) {
						ITreeFolderViewBean newFolder = new TreeFolderViewBean().setName(subFolder.getName());
						listFolders.add(newFolder);
					} else {
						if (subFileBean.isOpenFolder()
								|| TriStringUtils.isNotEmpty(subFileBean.getFolders())) {
							refreshFolderStructure(rootFolder, subFileBean, masterPath, lotDto);
						}
						listFolders.add(subFileBean);
					}
				}
			}
			viewBean.setFolders(listFolders);
		}
	}

	public static void refreshTreeFolder(List<Object> paramList, File masterPath) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);

		ITreeFolderViewBean rootFolder = folderView.getFolderView();

		List<File> subModules = BusinessFileUtils.getListFiles(masterPath,
				AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));

		List<ITreeFolderViewBean> listModules = new ArrayList<ITreeFolderViewBean>();

		for (File subModule: subModules) {
			if (subModule.isDirectory()) {
				String subModuleRelativePath = TriStringUtils.convertRelativePath(masterPath, subModule.getPath(), true);
				ITreeFolderViewBean subFolderBean = rootFolder.getFolder(subModuleRelativePath);
				if (null == subFolderBean) {
					ITreeFolderViewBean newFolder = new TreeFolderViewBean().setName(subModule.getName());
					listModules.add(newFolder);
				} else {
					if (subFolderBean.isOpenFolder()
							|| TriStringUtils.isNotEmpty(subFolderBean.getFolders())) {
						refreshFolderStructure(rootFolder, subFolderBean, masterPath, lotDto);
					}
					listModules.add(subFolderBean);
				}
			}
		}

		rootFolder.setFolders(listModules);
	}

	public static ITreeFolderViewBean refreshFolderStructure(ITreeFolderViewBean rootFolder, ITreeFolderViewBean currentFolderBean, File masterPath, ILotDto lotDto) {

		File currentFolder = new File(masterPath, TriStringUtils.convertPath(currentFolderBean.getPath()));

		if (currentFolderBean.isOpenFolder()
				|| TriStringUtils.isNotEmpty(currentFolderBean.getFolders())) {
			List<ITreeFolderViewBean> listFolders = new ArrayList<ITreeFolderViewBean>();
			List<File> folders = BusinessFileUtils.getListFiles(currentFolder,
					AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
			for (File subFolder: folders) {
				if (subFolder.isDirectory()) {
					String subFolderRelativePath = TriStringUtils.convertRelativePath(masterPath, subFolder.getPath(), true);
					ITreeFolderViewBean subFileBean = rootFolder.getFolder(subFolderRelativePath);
					if (null == subFileBean) {
						ITreeFolderViewBean newFolder = new TreeFolderViewBean().setName(subFolder.getName());
						listFolders.add(newFolder);
					} else {
						if (subFileBean.isOpenFolder()) {
							refreshFolderStructure(rootFolder, subFileBean, masterPath, lotDto);
						}
						listFolders.add(subFileBean);
					}
				}
			}
			currentFolderBean.setFolders(listFolders);
		}
		return currentFolderBean;
	}

	public static IResourceViewBean convertFile2CheckoutRemovalResourceBean(IGeneralServiceBean serviceBean, List<Object> paramList, ResourceSelection resourceSelection, File file, File masterPath) {

		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		CheckInOutResourceViewBean outResourceBean = new CheckInOutResourceViewBean();
		if (file.isFile()) {
			outResourceBean = (CheckInOutResourceViewBean) getResourceInformation(paramList, masterPath, file.getPath(), serviceBean.getLanguage(), serviceBean.getTimeZone());
			String relativePath = TriStringUtils.convertRelativePath(masterPath, file.getPath(), true);
			outResourceBean.setPath(relativePath);
			if (selectedFileSet.contains(relativePath)) {
				outResourceBean.setSelected(true);
			} else {
				outResourceBean.setSelected(false);
			}
			outResourceBean.setSize(TriFileUtils.fileSizeOf(new Long(file.length())));
			outResourceBean.setName(file.getName());
		}
		return outResourceBean;
	}


	public static IResourceViewBean convertFile2CheckinResourceBean( Map<String, IExtEntity> ucfExtensionMap, File srcFile, File srcPath, File destPath ) {

		try {

			CheckInOutResourceViewBean outResourceBean = new CheckInOutResourceViewBean();
			if (srcFile.isFile()) {
				String relativePath = TriStringUtils.convertRelativePath(srcPath, srcFile.getPath());
				File destFile = new File(TriStringUtils.convertPath(destPath), relativePath);
				boolean diff = false;
				FileStatus status = null;
				if (destFile.exists()) {
					if (destFile.isFile()) {
						boolean isSame = TriFileUtils.isSame(srcFile, destFile);
						if (isSame) {
							diff = false;
							status = FileStatus.Same;
						} else {
							diff = true;
							status = FileStatus.Edit;
						}
					} else {
						diff = false;
						status = FileStatus.Add;
					}

				} else {
					diff = false;
					status = FileStatus.Add;
				}

				if (diff && null != ucfExtensionMap ) {
					diff = !AmBusinessFileUtils.isBinary(relativePath, ucfExtensionMap);
				}

				outResourceBean.setDiff(diff)
								.setStsId( (null == status)? FileStatus.none.getStatusId(): status.getStatusId() )
								.setPath(TriStringUtils.convertPath(relativePath, true))
								.setSelected(true)
								.setName(srcFile.getName())
								.setSize(TriFileUtils.fileSizeOf(new Long(srcFile.length())));
			}
			return outResourceBean;
		} catch (IllegalArgumentException ex) {

			LogHandler.debug( log , "exception:=" + ex.getMessage());
			throw new IllegalArgumentException();

		} catch (IOException ex) {

			LogHandler.debug( log , "exception:=" + ex.getMessage());
			throw new TriSystemException( SmMessageId.SM005053S , ex );

		}
	}

	public static void setAssetSelectList(IGeneralServiceBean serviceBean, List<Object> paramList, File checkinInOutBoxPath, boolean isCheckin) {

		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection( paramList );
		ResourceSelectionFolderView<IResourceViewBean> resourceFolderView = AmExtractEntityAddonUtils.extractResourceFolderView( paramList );
		if (TriStringUtils.isEmpty(resourceSelection.getPath())) {
			return;
		}
		File destPath = null;
		Map<String, IExtEntity> ucfExtEntity = null;
		if (isCheckin) {
			destPath = AmDesignBusinessRuleUtils.getMasterWorkPath(lotDto.getLotEntity());
			ucfExtEntity = AmExtractEntityAddonUtils.extractExtEntity(paramList);

		}

		String selectedDir = TriStringUtils.convertRelativePath(checkinInOutBoxPath, resourceSelection.getPath(), true);
		ITreeFolderViewBean root = resourceFolderView.getFolderView();
		TreeFolderViewBean selectedFolderBean = (TreeFolderViewBean)root.getFolder(selectedDir);

		String selectedPath = TriStringUtils.linkPath(TriStringUtils.convertPath(checkinInOutBoxPath.getAbsolutePath()), selectedDir);
		File selectedFolder = new File(selectedPath);
		int count = 0;
		List<File> selectedFolderFiles = BusinessFileUtils.getListFiles(selectedFolder,
				AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
		for (File subFile : selectedFolderFiles) {
			if (subFile.isFile()) {
				count++;
				if ( isCheckin ) {
					requestViewBean.add(convertFile2CheckinResourceBean( ucfExtEntity, subFile, checkinInOutBoxPath, destPath ));
				} else {
					requestViewBean.add(convertFile2CheckoutRemovalResourceBean(serviceBean, paramList, resourceSelection, subFile, checkinInOutBoxPath));
				}
			} else if (subFile.isDirectory()) {
				count = 0;
				String folderName = subFile.getName();
				String folderPath = TriStringUtils.convertPath(subFile.getPath());

				String relativeFilePath = TriStringUtils.convertRelativePath(checkinInOutBoxPath, folderPath, true);

				for (String selectedFile: resourceSelection.getSelectedFileSet()) {
					if (selectedFile.matches(relativeFilePath + "(/.*)")) {
						count++;
					}
				}
				requestViewBean.add(setFolderInformation(resourceSelection, subFile.isDirectory(), folderName, relativeFilePath, count));
				TreeFolderViewBean subFolder = (TreeFolderViewBean) root.getFolder(relativeFilePath);
				if (null == subFolder) {
					subFolder = new TreeFolderViewBean().setName(folderName);
					subFolder.setCount(count);
					selectedFolderBean.addFolder(subFolder);
				}
			}

		}
		selectedFolderBean.setCount(count);

		resourceFolderView.setRequestViews(requestViewBean);

		if (selectedFolderBean.isOpenFolder()) {
			openFolder(paramList, checkinInOutBoxPath);
		}
	}

	public static void setSelectedFiles(List<Object> paramList) {
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(paramList);
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		String beforeDir = TriStringUtils.convertPath(folderView.getSelectedPath(), true);
		String afterDir = TriStringUtils.isNotEmpty(resourceSelection.getPath())
										? TriStringUtils.convertPath(resourceSelection.getPath(), true) : "";

		afterDir = (TriStringUtils.isEmpty(afterDir)) ? beforeDir : afterDir;
		Set<String> selectResourceList = new HashSet<String>();
		if (TriStringUtils.isNotEmpty(selectedFileSet)) {
			if (TriStringUtils.isNotEmpty(beforeDir)) {
				for (String resource : selectedFileSet) {
					if ( TriStringUtils.isEmpty(resource) ) {
						continue;
					}
						selectResourceList.add(resource);
					
				}
			} else {
				for (String resource : selectedFileSet) {
					selectResourceList.add(resource);
				}
			}
		}
		
		if (TriStringUtils.isNotEmpty(folderView.getRequestViews())) {
			for ( IResourceViewBean resource : folderView.getRequestViews()) {
				if (resource.isFile()) {

					if (selectedFileSet.contains(TriStringUtils.convertPath(resource.getPath(), true))) {
						((CheckInOutResourceViewBean)resource).setSelected(true);
					} else {
						((CheckInOutResourceViewBean)resource).setSelected(false);
					}
				}
			}
		}


		resourceSelection.setSelectedFileSet(selectResourceList);

		if (ResourceRequestType.selectFolder.equals(resourceSelection.getType())) {
			folderView.setSelectedPath(afterDir);
		}
	}

	public static IResourceViewBean getResourceInformation(List<Object> paramList, File masterPath, String filePath, String language, TimeZone timezone) {

		IAreqDto[] areqDtos = AmExtractEntityAddonUtils.extractAssetApplyArray( paramList );

		for ( IAreqDto dto : areqDtos ) {
			IAreqEntity entity = dto.getAreqEntity();

			if ( !AmBusinessJudgUtils.isResourceLock( entity.getStsId() ) ) {
				continue;
			}

			String relativePath = TriStringUtils.convertRelativePath(masterPath, filePath, true);
			Set<String> requestPath = getRequestFilePathSet( dto );

			if ( requestPath.contains( relativePath )) {
				boolean locked = true;
				return getCheckInOutResource( entity, locked, language, timezone );
			}
		}

		return new CheckInOutResourceViewBean();
	}

	public static void setResourceSelectedBySubmit(List<Object> paramList, Set<String> selectedFileSet) {
		ResourceSelectionFolderView<IResourceViewBean> foldeView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		if (TriStringUtils.isNotEmpty(foldeView.getRequestViews())) {
			for ( IResourceViewBean resource : foldeView.getRequestViews()) {
				if (resource.isFile()) {

					if (selectedFileSet.contains(TriStringUtils.convertPath(resource.getPath(), true))) {
						((CheckInOutResourceViewBean)resource).setSelected(true);
					} else {
						((CheckInOutResourceViewBean)resource).setSelected(false);
					}
				}
			}
		}

	}

	public static CheckInOutResourceViewBean getCheckInOutResource(IAreqEntity areqEntity, boolean isLocked, String language, TimeZone timezone) {
		CheckInOutResourceViewBean viewBean = new CheckInOutResourceViewBean();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( language, timezone );

		viewBean.setAreqId(areqEntity.getAreqId())
				.setPjtId(areqEntity.getPjtId())
				.setLocked(isLocked)
				.setReferenceId(areqEntity.getChgFactorNo());

		if ( isLocked ) {
			viewBean.setAreqType			( AreqCtgCd.value(areqEntity.getAreqCtgCd()) )
					.setCheckoutDate		( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(),formatYMD) )
					.setCheckinDueDate		( areqEntity.getRtnReqDueDate() )
					.setCheckinDate			( TriDateUtils.convertViewDateFormat(areqEntity.getRtnReqTimestamp(),formatYMD) )
					.setRemovalRequestDate	( TriDateUtils.convertViewDateFormat(areqEntity.getDelReqTimestamp(),formatYMD) )
					;

			if ( AreqCtgCd.RemovalRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getDelReqUserId() )
						.setSubmitterNm( areqEntity.getDelReqUserNm() )
						;

			} else if ( AreqCtgCd.LendingRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getLendReqUserId() )
						.setSubmitterNm( areqEntity.getLendReqUserNm() )
						;

			} else if ( AreqCtgCd.ReturningRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getRtnReqUserId() )
						.setSubmitterNm( areqEntity.getRtnReqUserNm() )
						;
			}
		}

		return viewBean;
	}

	public static Set<String> getRequestFilePathSet(IAreqDto areqDto) {
		Set<String> filePathSet = new HashSet<String>();
		
		List<AreqCtgCd> ctgCds = new ArrayList<AreqCtgCd>();
		AreqCtgCd ctgCd = AreqCtgCd.value( areqDto.getAreqEntity().getAreqCtgCd());
		ctgCds.add(ctgCd);
		if ( AreqCtgCd.ReturningRequest.equals(ctgCd) ) {
			ctgCds.add( AreqCtgCd.LendingRequest );
		}

		List<IAreqFileEntity> fileEntities =  areqDto.getAreqFileEntities( ctgCds.toArray( new AreqCtgCd[0] ) );

		List<IAreqBinaryFileEntity> binaryFileEntities = areqDto.getAreqBinaryFileEntities( ctgCds.toArray( new AreqCtgCd[0] ) );

		for ( IAreqFileEntity filePath : fileEntities) {
			filePathSet.add(TriStringUtils.linkPathBySlash("/", filePath.getFilePath()));
		}
		for ( IAreqBinaryFileEntity binaryFilePath : binaryFileEntities ) {
			filePathSet.add(TriStringUtils.linkPathBySlash("/", binaryFilePath.getFilePath()));
		}

		return filePathSet;
	}

	public static IResourceViewBean setFolderInformation(ResourceSelection resourceSeletion, boolean isDirectory, String folderName, String relativePath, int countFile) {
		return new CheckInOutResourceViewBean()
			.setDirectory(isDirectory)
			.setPath(relativePath)
			.setName(folderName)
			.setCount(countFile)
			.setLocked(true)
			;
	}

	public static void checkInOutFileUpload(IGeneralServiceBean serviceBean, ChaLibEntryInputBaseInfoBean inputBean,
			List<Object> list, String areqId, List<IDomain<IGeneralServiceBean>> actions,
			List<IDomain<IGeneralServiceBean>> actions4InputFile) {

		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(list);
		ResourceSelectionFolderView<IResourceViewBean> foldeView = AmExtractEntityAddonUtils.extractResourceFolderView(list);
		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		FlowChaLibLendEditSupport.saveLendApplyDefUploadFile(inputBean, serviceBean.getUserId());

		List<Object> paramList = new ArrayList<Object>();

		IEhLendParamInfo param = new EhLendParamInfo();
		param.setPjtNo(inputBean.getInPrjNo());
		param.setGroupName(inputBean.getInGroupName());
		param.setSummary(inputBean.getInSubject());
		param.setContent(inputBean.getInContent());
		param.setApplyNo(areqId);
		param.setLendApplyUser(serviceBean.getUserName());
		param.setLendApplyUserId(serviceBean.getUserId());

		paramList.add(param);

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean(serviceBean).setParamList(paramList);
		param.setInApplyFilePath(inputBean.getLendApplyFilePath());
		if (TriStringUtils.isEmpty(inputBean.getLendApplyFilePath()) || !new File(inputBean.getLendApplyFilePath()).isFile()) {
			throw new ContinuableBusinessException(AmMessageId.AM001075E);
		}

		// Validate Lend File
		if (TriStringUtils.isNotEmpty(actions)) {
			for (IDomain<IGeneralServiceBean> action : actions) {
				action.execute(innerServiceDto);
			}
		}

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(list);
		ILotEntity lotEntity = lotDto.getLotEntity();
		paramList.add(lotDto);

		param.setDuplicationCheck(!lotEntity.getIsAssetDup().parseBoolean());
		param.setUserId(serviceBean.getUserId());

		// Validate and save file into temp folder
		for (IDomain<IGeneralServiceBean> action : actions4InputFile) {
			action.execute(innerServiceDto);
		}

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		String[] listFiles = AmExtractMessageAddonUtils.getLendParamInfo(paramList).getLendFiles();
		for (String filePath : listFiles) {
			selectedFileSet.add(TriStringUtils.convertPath(filePath, true));
		}

		String currentPath = foldeView.getSelectedPath();
		if (TriStringUtils.isNotEmpty(currentPath)) {
			setAssetSelectList(serviceBean, list, masterPath, false);
		}
	}

	public static void removalRequestFileUpload(IGeneralServiceBean serviceBean, RemovalRequestEditInputBean inputBean,
			List<Object> list, List<IDomain<IGeneralServiceBean>> fileUploadActions) {

		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection(list);
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(list);
		Set<String> selectedFileSet = resourceSelection.getSelectedFileSet();

		FlowChaLibMasterDelEditSupport.saveDelApplyDefUploadFile(inputBean, serviceBean.getUserId());

		List<Object> paramList = new ArrayList<Object>();

		IDelApplyParamInfo actionParam = new DelApplyParamInfo();
		{
			actionParam.setUserId(serviceBean.getUserId());
			actionParam.setDelApplyUser(serviceBean.getUserName());
			actionParam.setDelApplyUserId(serviceBean.getUserId());
			actionParam.setInApplyFilePath(inputBean.getCsvFilePath());

			paramList.add(lotDto);
			paramList.add(actionParam);

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( serviceBean )
					.setParamList( paramList );

			for (IDomain<IGeneralServiceBean> action : fileUploadActions) {
				action.execute(innerServiceDto);
			}


			IDelApplyFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( paramList );
			for (IDelApplyFileResult file: files) {
				selectedFileSet.add(TriStringUtils.convertPath(file.getFilePath(), true));
			}

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			String currentPath = resourceSelection.getPath();
			if (TriStringUtils.isNotEmpty(currentPath)) {
				setAssetSelectList(serviceBean, list, masterPath, false);
			}

		}
	}

	public static void checkinFileUpload(FlowCheckinRequestServiceBean paramBean) {
		CheckinRequestInputInfo inputInfo = paramBean.getParam().getInputInfo();
		if ( null == inputInfo.getAttachmentInputStreamBytes()
				|| 0 == inputInfo.getAttachmentInputStreamBytes().length) {
			return;
		}

		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		String applyNo = paramBean.getParam().getSelectedAreqId() ;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		String uniqueKey = paramBean.getParam().getInputInfo().getAttachmentFileSeqNo();
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

		String append1 = "";
		String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

		BusinessFileUtils.saveAppendFile	(
				basePath , applyNo , uniqueKey , append1 ,
				StreamUtils.convertBytesToInputStreamToBytes( inputInfo.getAttachmentInputStreamBytes() ),
				inputInfo.getAttachmentFileNm() );

		String filePath = new File(
				appendPath1 ,
				inputInfo.getAttachmentFileNm() ).getPath();

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "filePath:=" + filePath );
		}

		if ( new File(filePath).exists() ) {
			inputInfo.setAttachmentInputStreamBytes(null);
			inputInfo.setAttachmentFilePath(filePath);
		}
	}

	public static String getCheckinFileAttachmentPath(FlowCheckinRequestServiceBean paramBean) {
		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		String applyNo = paramBean.getParam().getSelectedAreqId() ;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		String uniqueKey = paramBean.getParam().getInputInfo().getAttachmentFileSeqNo();
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

		return uniquePath;
	}

	public static String getCheckinFileAttachmentPath(IAreqAttachedFileEntity attachmentEntity) {
		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		String applyNo = attachmentEntity.getAreqId() ;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		String uniqueKey = attachmentEntity.getAttachedFileSeqNo();
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

		return uniquePath;
	}
	
	public static IDelApplyBinaryFileResult[] getBinaryDelFiles(List<Object> paramList) {
		try {
			File binaryFile = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
					sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile ) );

			FileInputStream fisBin = null ;
			InputStreamReader isrBin = null ;
			String[] binaryFiles = null ;
			try {
				fisBin = new FileInputStream( binaryFile ) ;
				isrBin = new InputStreamReader( fisBin ) ;
				binaryFiles = TriFileUtils.readFileLine( isrBin, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isrBin);
				TriFileUtils.close(fisBin);
			}


			List<IDelApplyBinaryFileResult> binaryParsePath = new ArrayList<IDelApplyBinaryFileResult>();
			for ( String binaryPath : binaryFiles ) {
				IDelApplyBinaryFileResult result = new DelApplyBinaryFileResult();
				result.setFilePath( FlowChaLibMasterDelEditSupport.convertPath( binaryPath ));
				binaryParsePath.add( result );
			}

			return binaryParsePath.toArray( new IDelApplyBinaryFileResult[] {} );
		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		}
	}

	public static List<String> getSelectedGroupList(ILotDto lotDto, List<IGrpEntity> userEntityList) {
		List<String> groupIdList = new ArrayList<String>();
		List<String> allowGroupIdList = new ArrayList<String>();

		if (null != lotDto.getLotGrpLnkEntities()) {
			for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {
				groupIdList.add(group.getGrpId());
			}
		}
		for (IGrpEntity entity : userEntityList) {

			if (0 == groupIdList.size() || groupIdList.contains(entity.getGrpId())) {
				allowGroupIdList.add(entity.getGrpId());
			}
		}
		return allowGroupIdList;
	}

	public static AreqCondition setAreqConditionForLockedResource(String selectedLotId) {
		String[] baseStatusId = {
				AmAreqStatusId.RemovalRequested.getStatusId(),
				AmAreqStatusId.RemovalRequestApproved.getStatusId(),
				AmAreqStatusId.CheckoutRequested.getStatusId(),
				AmAreqStatusId.CheckinRequested.getStatusId(),
				AmAreqStatusId.CheckinRequestApproved.getStatusId() };

		AreqCondition condition = new AreqCondition();
		condition.setLotId( selectedLotId );
		condition.setStsIds(baseStatusId);

		return condition;
	}
	
	
	public static void openAllFolder(List<Object> paramList, File masterPath) {
		ResourceSelectionFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractResourceFolderView(paramList);
		ITreeFolderViewBean rootFolder = folderView.getFolderView();
		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();
		rootFolder.setFolders(new ArrayList<ITreeFolderViewBean>());
		folderView.setRequestViews(requestViewBean);
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		File destPath = null;
		Map<String, IExtEntity> ucfExtEntity = null;
		destPath = AmDesignBusinessRuleUtils.getMasterWorkPath(lotDto.getLotEntity());
		ucfExtEntity = AmExtractEntityAddonUtils.extractExtEntity(paramList);
		setTreeFolderView(masterPath, masterPath, rootFolder,destPath,ucfExtEntity,folderView);
	}
	
	private static int setTreeFolderView(File currentFolder, File masterPath, ITreeFolderViewBean parentFolder, 
			File destPath, Map<String, IExtEntity> ucfExtEntity,  ResourceSelectionFolderView<IResourceViewBean> folderView) {
		TreeFolderViewBean currentFolderBean;
	

		if (!currentFolder.getPath().equals(masterPath.getPath())) {
			currentFolderBean = new TreeFolderViewBean().setName(currentFolder.getName());
			parentFolder.addFolder(currentFolderBean);
			currentFolderBean.setOpenFolder(true);
		} else {
			currentFolderBean = (TreeFolderViewBean)parentFolder;
		}

		int count = 0;
		List<File> subFolders = BusinessFileUtils.getListFiles(currentFolder);
		for (File subFolder: subFolders) {
			if (subFolder.isDirectory()) {
				count += setTreeFolderView(subFolder, masterPath, currentFolderBean, destPath, ucfExtEntity,folderView);
			} else {
				folderView.getRequestViews().add(convertFile2CheckinResourceBean(ucfExtEntity, subFolder, masterPath, destPath));
				count ++;
			}
		}
		currentFolderBean.setCount(count);
		return count;
	}

	/**
	 * Validate: check-in needs attach file or not
	 * @param paramBean
	 * @return
	 */
	public static boolean attachFileRequired() {

		if (StatusFlg.on.value().equals( sheet.getValue( AmDesignEntryKeyByChangec.needRtnAttachFile ) )) {
			return true;
		}
		return false;
	}
	
	public static Set<String> unselectResource(String unselectedResource, Set<String> selectedResources){
		if ( TriStringUtils.isNotEmpty( unselectedResource ) && selectedResources.contains(unselectedResource)){
			selectedResources.remove(unselectedResource);
		}
		return selectedResources;
	}
	
	public static void selectFilesInFolder(IGeneralServiceBean serviceBean, List<Object> paramList, File checkinInOutBoxPath, boolean isCheckin) {

		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();
		ResourceSelectionFolderView<IResourceViewBean> resourceFolderView = AmExtractEntityAddonUtils.extractResourceFolderView( paramList );
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractResourceSelection( paramList );
		Set<String> selectedResources = resourceSelection.getSelectedFileSet();
		if (TriStringUtils.isEmpty(resourceSelection.getPath())) {
			return;
		}
		File destPath = null;
		Map<String, IExtEntity> ucfExtEntity = null;
		if (isCheckin) {
			destPath = AmDesignBusinessRuleUtils.getMasterWorkPath(lotDto.getLotEntity());
			ucfExtEntity = AmExtractEntityAddonUtils.extractExtEntity(paramList);
		}
		String selectedDir = TriStringUtils.convertRelativePath(checkinInOutBoxPath, resourceSelection.getPath(), true);
		ITreeFolderViewBean root = resourceFolderView.getFolderView();
		TreeFolderViewBean selectedFolderBean = (TreeFolderViewBean)root.getFolder(selectedDir);
		String selectedPath = TriStringUtils.linkPath(TriStringUtils.convertPath(checkinInOutBoxPath.getAbsolutePath()), selectedDir);
		File selectedFolder = new File(selectedPath);
		List<File> selectedFolderFiles = BusinessFileUtils.getListFiles(selectedFolder,
				AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
		for (File subFile : selectedFolderFiles) {
			
			if (subFile.isFile()) {

				if ( isCheckin ) {
					requestViewBean.add(convertFile2CheckinResourceBean( ucfExtEntity, subFile, checkinInOutBoxPath, destPath ));
				} else {
					CheckInOutResourceViewBean fileResource = (CheckInOutResourceViewBean) convertFile2CheckoutRemovalResourceBean(serviceBean, paramList, resourceSelection, subFile, checkinInOutBoxPath);
					requestViewBean.add(fileResource);
					String relativePath = TriStringUtils.convertRelativePath(checkinInOutBoxPath, subFile.getPath(), true);
					if (resourceSelection.isSelectAllFilesInFolder()) {
						if (!fileResource.isLocked()) {
							selectedResources.add(relativePath);
							fileResource.setSelected(true);
						}
					} else {
						selectedResources = AmResourceSelectionUtils.unselectResource(relativePath, selectedResources);
						fileResource.setSelected(false);
					}
				}
		
			} else if (subFile.isDirectory()) {
				String folderName = subFile.getName();
				String folderPath = TriStringUtils.convertPath(subFile.getPath());

				String relativeFilePath = TriStringUtils.convertRelativePath(checkinInOutBoxPath, folderPath, true);

				for (String selectedFile: resourceSelection.getSelectedFileSet()) {
					if (selectedFile.matches(relativeFilePath + "(/.*)")) {
					}
				}
				requestViewBean.add(setFolderInformation(resourceSelection, subFile.isDirectory(), folderName, relativeFilePath, 0));
				TreeFolderViewBean subFolder = (TreeFolderViewBean) root.getFolder(relativeFilePath);
				if (null == subFolder) {
					subFolder = new TreeFolderViewBean().setName(folderName);
					selectedFolderBean.addFolder(subFolder);
				}
			}
		}
		resourceFolderView.setRequestViews(requestViewBean);
		
	}
}
