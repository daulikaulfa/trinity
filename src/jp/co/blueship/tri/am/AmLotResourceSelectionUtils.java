package jp.co.blueship.tri.am;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TimeZone;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.IResourceViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotResourceViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceFolderView;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */

public class AmLotResourceSelectionUtils {

	public static void buildTreeFolderViewByInit( List<Object> paramList, File masterPath ) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractLotResourceSelection(paramList);
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);

		List<File> files = new ArrayList<File>();
		for (ILotMdlLnkEntity module : lotDto.getIncludeMdlEntities(true)) {
			File modulePath = new File(masterPath, module.getMdlNm());
			if (modulePath.exists())
				files.add(modulePath);
		}

		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();

		ITreeFolderViewBean root = new TreeFolderViewBean().setOwner().setName("");

		File[] fileArray = (File[]) files.toArray(new File[0]);
		BusinessFileUtils.sortDefaultFileList(fileArray);
		int rootCount = 0;
		boolean recursiveCount = true;
		for (File file : fileArray) {
			// If the file is directory --> add to tree folder view
			if (file.isDirectory()) {
				TreeFolderViewBean subFolder = new TreeFolderViewBean().setName(file.getName());
				int subFolderCount = countFilesInFolder(file, recursiveCount);
				subFolder.setCount(subFolderCount);
				root.addFolder(subFolder);
				requestViewBean.add(setFolderInformation(resourceSelection, file.isDirectory(), file.getName(), subFolder.getPath(), subFolderCount));
				rootCount += subFolderCount;
			}
		}

		root.setCount(rootCount);
		resourceSelection.setPath("/");
		folderView.setSelectedPath("/");
		folderView.setFolderView(root);
		folderView.setRequestViews(requestViewBean);
	}

	public static void setTotalResourceDetail(FlowLotResourceListServiceBean serviceBean, List<Object> paramList, File folderPath, IUmFinderSupport support) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		List<File> lotFiles = new ArrayList<File>();
		boolean includeFolderCount = true;

		for (ILotMdlLnkEntity module : lotDto.getIncludeMdlEntities(true)) {
			File modulePath = new File(folderPath, module.getMdlNm());
			if (modulePath.exists()) {
				List<File> moduleFiles = getTotalFiles(modulePath, includeFolderCount);
				lotFiles.addAll(moduleFiles);
			}
		}
		long totalSize = 0;
		int totalFile = 0;
		int totalFolder = 0;

		for (File file : lotFiles) {
			if(file.isFile()) {
				totalFile++;
				totalSize += new Long(file.length());

			} else if(file.isDirectory()) {
				totalFolder ++;
			}
		}

		serviceBean.getLotResourceDetailsView()
				.setSize	( TriFileUtils.fileSizeOf(new Long(totalSize)) )
				.setFolders	( totalFolder )
				.setFiles	( totalFile )
				;
	}

	public static List<File> getTotalFiles(File src, boolean includeFolder) {
		List<File> retList = new ArrayList<File>();
		List<File> listOfFiles = BusinessFileUtils.getListFiles(src);
		for (int i = 0; i < listOfFiles.size(); i++) {
			File file = listOfFiles.get(i);
			retList.add(file);
			if (file.isDirectory()) {
				retList.addAll(getTotalFiles(file, includeFolder));
			}
		}

		return retList;
	}

	public static int countFilesInFolder(File src, boolean recursive) {
		List<File> listOfFiles = BusinessFileUtils.getListFiles(src);
		int count = 0;
		for ( File file : listOfFiles ) {
			if ( file.isDirectory() ) {
				if( recursive ) {
					count += countFilesInFolder(file, recursive);
				}
			} else {
				count++;
			}
		}

		return count;
	}

	public static void closeFolder( List<Object> paramList ) {
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractLotResourceSelection(paramList);
		String selectedPath = resourceSelection.getPath();
		ITreeFolderViewBean rootFolder = folderView.getFolderView();
		if (TriStringUtils.isNotEmpty(selectedPath)) {
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(false);
		}
	}

	public static void openFolder(List<Object> paramList, File masterPath) {
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractLotResourceSelection(paramList);

		String selectedPath = resourceSelection.getPath();
		ITreeFolderViewBean rootFolder = folderView.getFolderView();

		List<ITreeFolderViewBean> listFolders = new ArrayList<ITreeFolderViewBean>();

		if (TriStringUtils.isNotEmpty(selectedPath)) {
			File selectedFile = new File(masterPath, selectedPath);
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(true);
			List<File> folders = BusinessFileUtils.getListFiles(selectedFile,
					AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
			for (File subFolder: folders) {
				if (subFolder.isDirectory()) {
					int subFolderCount = countFilesInFolder(subFolder, true);
					String subFolderRelativePath = TriStringUtils.convertRelativePath(masterPath, subFolder.getPath(), true);
					ITreeFolderViewBean subFileBean = rootFolder.getFolder(subFolderRelativePath);
					if (null == subFileBean) {
						TreeFolderViewBean newFolder = new TreeFolderViewBean().setName(subFolder.getName());
						newFolder.setCount(subFolderCount);
						listFolders.add(newFolder);
					} else {
						if (subFileBean.isOpenFolder()
								|| TriStringUtils.isNotEmpty(subFileBean.getFolders())) {
							AmResourceSelectionUtils.refreshFolderStructure(rootFolder, subFileBean, masterPath, lotDto);
						}
						subFileBean.setCount(subFolderCount);
						listFolders.add(subFileBean);
					}
				}
			}
			viewBean.setFolders(listFolders);
		}
	}

	public static void closeAllFolder(List<Object> paramList) {
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		TreeFolderViewBean rootFolder = (TreeFolderViewBean)folderView.getFolderView();
		for (ITreeFolderViewBean subFolder : rootFolder.getFolders()) {
			( (TreeFolderViewBean)subFolder ).setOpenFolder( false );
		}
		rootFolder.setOpenFolder(false);
	}

	public static void openAllFolder(List<Object> paramList, File masterPath) {
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		ITreeFolderViewBean rootFolder = folderView.getFolderView();
		rootFolder.setFolders(new ArrayList<ITreeFolderViewBean>());
		setTreeFolderView(masterPath, masterPath, rootFolder);
	}

	private static int setTreeFolderView(File currentFolder, File masterPath, ITreeFolderViewBean parentFolder) {
		TreeFolderViewBean currentFolderBean;
		if (!currentFolder.getPath().equals(masterPath.getPath())) {
			currentFolderBean = new TreeFolderViewBean().setName(currentFolder.getName());
			parentFolder.addFolder(currentFolderBean);
			currentFolderBean.setOpenFolder(true);
		} else {
			currentFolderBean = (TreeFolderViewBean)parentFolder;
		}

		int count = 0;
		List<File> subFolders = BusinessFileUtils.getListFiles(currentFolder);
		for (File subFolder: subFolders) {
			if (subFolder.isDirectory()) {
				count += setTreeFolderView(subFolder, masterPath, currentFolderBean);
			} else {
				count ++;
			}
		}
		currentFolderBean.setCount(count);
		return count;
	}

	public static void setAssetSelectList(IGeneralServiceBean serviceBean, List<Object> paramList, File checkinInOutBoxPath, IUmFinderSupport support) {

		List<IResourceViewBean> requestViewBean = new ArrayList<IResourceViewBean>();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractLotResourceSelection( paramList );
		ResourceFolderView<IResourceViewBean> resourceFolderView = AmExtractEntityAddonUtils.extractLotResourceFolderView( paramList );
		if (TriStringUtils.isEmpty(resourceSelection.getPath())) {
			return;
		}

		String selectedDir = TriStringUtils.convertRelativePath(checkinInOutBoxPath, resourceSelection.getPath(), true);
		ITreeFolderViewBean root = resourceFolderView.getFolderView();
		TreeFolderViewBean selectedFolderBean = (TreeFolderViewBean)root.getFolder(selectedDir);

		String selectedPath = TriStringUtils.linkPath(TriStringUtils.convertPath(checkinInOutBoxPath.getAbsolutePath()), selectedDir);
		File selectedFolder = new File(selectedPath);
		List<File> selectedFolderFiles = BusinessFileUtils.getListFiles(selectedFolder,
				AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto));
		for (File subFile : selectedFolderFiles) {
			if (subFile.isFile()) {
				requestViewBean.add(convertFile2LotResourceBean(serviceBean, paramList, subFile, checkinInOutBoxPath, support));
			} else if (subFile.isDirectory()) {
				String folderName = subFile.getName();
				String folderPath = TriStringUtils.convertPath(subFile.getPath());

				String relativeFilePath = TriStringUtils.convertRelativePath(checkinInOutBoxPath, folderPath, true);
				int subFileCount = countFilesInFolder(subFile, true);
				requestViewBean.add(setFolderInformation(resourceSelection, subFile.isDirectory(), folderName, relativeFilePath, subFileCount));
				TreeFolderViewBean subFolder = (TreeFolderViewBean) root.getFolder(relativeFilePath);
				if (null == subFolder) {
					subFolder = new TreeFolderViewBean().setName(folderName);
					selectedFolderBean.addFolder(subFolder);
				}
			}

		}

		resourceFolderView.setRequestViews(requestViewBean);

		if (selectedFolderBean.isOpenFolder()) {
			openFolder(paramList, checkinInOutBoxPath);
		}
	}

	public static LotResourceViewBean convertFile2LotResourceBean(IGeneralServiceBean serviceBean, List<Object> paramList, File file, File masterPath, IUmFinderSupport support) {

		LotResourceViewBean lotResourceView = new LotResourceViewBean();
		if (file.isFile()) {
			lotResourceView = (LotResourceViewBean) getResourceInformation(paramList, masterPath, file.getPath(), serviceBean.getLanguage(), serviceBean.getTimeZone(), support);
			String relativePath = TriStringUtils.convertRelativePath(masterPath, file.getPath(), true);
			lotResourceView.setPath(relativePath);
			lotResourceView.setSize(TriFileUtils.fileSizeOf(new Long(file.length())));
			lotResourceView.setName(file.getName());
		}
		return lotResourceView;
	}

	public static IResourceViewBean getResourceInformation(List<Object> paramList, File masterPath, String filePath, String language, TimeZone timezone, IUmFinderSupport support) {

		IAreqDto[] areqDtos = AmExtractEntityAddonUtils.extractAssetApplyArray( paramList );

		for ( IAreqDto dto : areqDtos ) {
			IAreqEntity entity = dto.getAreqEntity();

			if ( !AmBusinessJudgUtils.isResourceLock( entity.getStsId() ) ) {
				continue;
			}

			String relativePath = TriStringUtils.convertRelativePath(masterPath, filePath, true);
			Set<String> requestPath = AmResourceSelectionUtils.getRequestFilePathSet( dto );

			if ( requestPath.contains( relativePath )) {
				boolean locked = true;
				return getCheckInOutResource( entity, locked, language, timezone, support);
			}
		}

		return new LotResourceViewBean();
	}

	public static IResourceViewBean getCheckInOutResource(IAreqEntity areqEntity, boolean isLocked, String language, TimeZone timezone, IUmFinderSupport support) {
		LotResourceViewBean viewBean = new LotResourceViewBean();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( language, timezone );

		viewBean.setLocked(isLocked);

		if ( isLocked ) {
			viewBean.setAreqType( AreqCtgCd.value(areqEntity.getAreqCtgCd()) );

			if ( AreqCtgCd.RemovalRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getDelReqUserId() )
						.setSubmitterNm( areqEntity.getDelReqUserNm() )
						.setSubmitterIconPath(support.getIconPath(areqEntity.getDelReqUserId()))
						.setLockedDate( TriDateUtils.convertViewDateFormat(areqEntity.getDelReqTimestamp(),formatYMD) )
						;

			} else if ( AreqCtgCd.LendingRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getLendReqUserId() )
						.setSubmitterNm( areqEntity.getLendReqUserNm() )
						.setSubmitterIconPath(support.getIconPath(areqEntity.getLendReqUserId()))
						.setLockedDate( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(),formatYMD) )
						;

			} else if ( AreqCtgCd.ReturningRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
				viewBean.setSubmitterId( areqEntity.getRtnReqUserId() )
						.setSubmitterNm( areqEntity.getRtnReqUserNm() )
						.setSubmitterIconPath(support.getIconPath(areqEntity.getRtnReqUserId()))
						.setLockedDate( ( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(),formatYMD) ) )
						;
			}
		}

		return viewBean;
	}

	public static void rebuildTreeFolderView(List<Object> paramList) {
		File masterPath =  AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		ITreeFolderViewBean root = folderView.getFolderView();
		if (null != root) {
			int rootCount = countFilesInFolder(masterPath, true);
			root.setCount(rootCount);
		}
	}

	public static IResourceViewBean setFolderInformation(ResourceSelection resourceSeletion, boolean isDirectory, String folderName, String relativePath, int countFile) {
		return new LotResourceViewBean()
			.setDirectory(isDirectory)
			.setPath(relativePath)
			.setName(folderName)
			.setCount(countFile)
			.setLocked(true)
			;
	}
}
