package jp.co.blueship.tri.am;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.RelatedApplyInfoViewBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnListServiceBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopViewServiceBean.TopMenuViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * 変更管理View層のAddonUtilです。
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public class AmViewInfoAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static TriFunction<IGrpEntity, String> TO_GROUP_ID = new TriFunction<IGrpEntity, String>() {

		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	/**
	 * リリース環境エンティティから環境番号の配列を取得する
	 *
	 * @param pjtLotEntityArray
	 * @return envEntityArray
	 */
	public static String[] convertToEnvNo(IBldEnvEntity[] envEntityArray) {
		Set<String> envNoSet = new LinkedHashSet<String>();

		for (IBldEnvEntity env : envEntityArray) {
			envNoSet.add(env.getBldEnvId());
		}

		return envNoSet.toArray(new String[0]);
	}

	/**
	 * リリース環境情報エンティティからリリース環境を取得する
	 *
	 * @param envList
	 * @return
	 */
	public static Map<String, IBldEnvEntity> convertToEnvMap(List<IBldEnvEntity> envList) {
		Map<String, IBldEnvEntity> envMap = new LinkedHashMap<String, IBldEnvEntity>();

		for (IBldEnvEntity env : envList) {
			envMap.put(env.getBldEnvId(), env);
		}

		return envMap;
	}

	/**
	 * ロット情報エンティティからアクセス可能なリリース環境を取得する
	 *
	 * @param envList
	 * @return
	 */
	public static Map<String, ILotRelEnvLnkEntity> convertToLotEnvMap(List<ILotRelEnvLnkEntity> envList) {
		Map<String, ILotRelEnvLnkEntity> envMap = new LinkedHashMap<String, ILotRelEnvLnkEntity>();

		for (ILotRelEnvLnkEntity env : envList) {
			envMap.put(env.getBldEnvId(), env);
		}

		return envMap;
	}

	/**
	 * 選択リリース環境情報から環境番号のセットを取得する
	 *
	 * @param envEntityArray
	 * @return
	 */
	public static Set<String> convertToEnvNoSet(List<RelEnvViewBean> selectedViewBeanList) {
		Set<String> selectedEnvNoSet = new LinkedHashSet<String>();

		for (RelEnvViewBean view : selectedViewBeanList) {
			selectedEnvNoSet.add(view.getEnvNo());
		}

		return selectedEnvNoSet;
	}

	/**
	 * ロット詳細情報をロット情報エンティティから設定する
	 *
	 * @param lotDetailViewBean ロット詳細情報
	 * @param entity ロット情報のエンティティ
	 */
	public static void setLotDetailViewBeanPjtLotEntity(
			IGeneralServiceBean paramBean,
			LotDetailViewBean lotDetailViewBean,
			ILotDto lotDto,
			String srvId,
			String srvNm) {

		ILotEntity entity = lotDto.getLotEntity();
		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities(true);
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		lotDetailViewBean.setLotNo(entity.getLotId());
		lotDetailViewBean.setLotName(entity.getLotNm());
		lotDetailViewBean.setLotSummary(entity.getSummary());
		lotDetailViewBean.setLotContent(entity.getContent());
		lotDetailViewBean.setAllowAssetDuplicationVisible(entity.getIsAssetDup().parseBoolean() || isSelectableAllowAssetDuplication());
		lotDetailViewBean.setAllowAssetDuplicationName(sheet.getValue(AmDesignBeanId.allowAssetDuplication,
				(entity.getIsAssetDup().parseBoolean()) ? StatusFlg.on.value() : StatusFlg.off.value()));
		lotDetailViewBean.setUseMerge( entity.isUseMerge().parseBoolean() );

		lotDetailViewBean.setLotStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()));
		lotDetailViewBean.setInputDate(TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(),format));
		lotDetailViewBean.setBranchBaselineTag(entity.getLotBranchBlTag());
		lotDetailViewBean.setRecentTag(entity.getLotLatestBlTag()); // @最新ベースライン
		lotDetailViewBean.setRecentVersion(entity.getLotLatestVerTag()); // @最新モジュールのバージョン

		List<String> moduleNameList = new ArrayList<String>();
		for (ILotMdlLnkEntity moduleEntity : mdlEntities) {
			moduleNameList.add(moduleEntity.getMdlNm());
		}
		lotDetailViewBean.setModuleName((String[]) moduleNameList.toArray(new String[0]));

		lotDetailViewBean.setAllowListView(entity.getAllowListView().parseBoolean());
		if ( entity.getAllowListView().parseBoolean() ) {
			lotDetailViewBean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.ALLOW_LIST_VIEW_CONTENT.getKey()));
		} else {
			lotDetailViewBean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.DENY_LIST_VIEW_CONTENT.getKey()));
		}
		List<String> groupNameList = AmLibraryAddonUtils.getAccessableGroupNameList(lotDto);
		lotDetailViewBean.setGroupName((String[]) groupNameList.toArray(new String[0]));

		List<String> specifiedGroupNameNameList = AmLibraryAddonUtils.getSpecifiedGroupNameList(lotDto);
		lotDetailViewBean.setSpecifiedGroupName((String[]) specifiedGroupNameNameList.toArray(new String[0]));

		lotDetailViewBean.setServerNo( srvId );
		lotDetailViewBean.setServerName( srvNm );

		lotDetailViewBean.setPublicFolder(AmLibraryAddonUtils.getPublicDirectoryPath(entity));
		lotDetailViewBean.setPrivateFolder(AmLibraryAddonUtils.getPrivateDirectoryPath(entity));

		List<String> relEnvNameList = AmLibraryAddonUtils.getRelEnvNameList(lotDto);
		lotDetailViewBean.setRelEnvName((String[]) relEnvNameList.toArray(new String[0]));
		lotDetailViewBean.setThemeColor(entity.getThemeColor());
		if(IconSelectionOption.DefaultImage.value().equals(entity.getIconTyp())) {
			lotDetailViewBean.setIcon(entity.getDefaultIconPath());
		} else {
			lotDetailViewBean.setIcon(entity.getCustomIconPath());
		}
	}

	/**
	 * ロット作成／編集時に資産の重複の許可／不許可を選択可能とするかを取得する。
	 *
	 * @return 資産の重複の許可／不許可を選択可能であればtrue、そうでなければfalse
	 */
	public static boolean isSelectableAllowAssetDuplication() {

		boolean duplicationCheck = true;
		if (!StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByChangec.selectableAllowAssetDuplication))) {
			duplicationCheck = false;
		}

		return duplicationCheck;
	}

	/**
	 * 変更管理一覧情報を取得します。
	 *
	 * @param entitys 変更情報のエンティティ
	 * @return 設定した変更情報一覧
	 */
	public static List<PjtViewBean> setPjtViewBeanPjtEntity(IPjtEntity[] entitys) {
		List<PjtViewBean> pjtViewList = new ArrayList<PjtViewBean>();

		for (IPjtEntity entity : entitys) {

			PjtViewBean viewBean = new PjtViewBean();

			viewBean.setPjtNo(entity.getPjtId());
			viewBean.setPjtSummary(entity.getSummary());
			viewBean.setChangeCauseNo(entity.getChgFactorNo());
			viewBean.setPjtStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()));

			pjtViewList.add(viewBean);
		}

		return pjtViewList;
	}

	/**
	 * 変更管理一覧情報を変更管理情報エンティティから設定する
	 *
	 * @param pjtViewBeanList 変更管理一覧情報
	 * @param entitys 変更管理情報のエンティティ
	 */
	public static LotBaselineViewBean setBuildViewBeanBuildEntity(ILotBlEntity entity) {

		LotBaselineViewBean viewBean = new LotBaselineViewBean();

		viewBean.setDataId(entity.getDataId()); // BpId
		viewBean.setBaselineName(entity.getLotBlNm());
		viewBean.setCheckInDate(TriDateUtils.convertViewDateFormat(entity.getLotChkinTimestamp()));

		return viewBean;
	}

	/**
	 * 変更管理一覧情報を変更管理情報エンティティから設定する
	 *
	 * @param pjtViewBeanList 変更管理一覧情報
	 * @param entitys 変更管理情報のエンティティ
	 */
	public static void setBuildViewBeanBuildEntity(List<LotBaselineViewBean> buildViewBeanList, List<ILotBlDto> entitys) {

		for (ILotBlDto entity : entitys) {

			LotBaselineViewBean viewBean = setBuildViewBeanBuildEntity(entity.getLotBlEntity());
			buildViewBeanList.add(viewBean);
		}
	}

	/**
	 * ロット一覧情報をロットエンティティから設定する
	 *
	 * @param lotViewBeanList ロット一覧情報
	 * @param lotDto ロットのエンティティ
	 * @param groupUserEntitys グループユーザのエンティティ
	 * @param isPullDown プルダウン用のリストかどうか
	 */
	public static void populateLotViewBeanPjtLotEntity(List<LotViewBean> lotViewBeanList, List<ILotDto> lotDto, List<IGrpEntity> groupEntities, String srvId) {

		populateLotViewBeanPjtLotEntity(lotViewBeanList, lotDto, groupEntities, srvId, false);
	}

	/**
	 * ロット一覧情報をロットエンティティから設定する
	 *
	 * @param lotViewBeanList ロット一覧情報
	 * @param lotDto ロットのエンティティ
	 * @param groupUserEntitys グループユーザのエンティティ
	 * @param isPullDown プルダウン用のリストかどうか
	 */
	public static void populateLotViewBeanPjtLotEntityForPullDown(List<LotViewBean> lotViewBeanList, List<ILotDto> lotDto,List<IGrpEntity> groupEntities, String srvId) {

		populateLotViewBeanPjtLotEntity(lotViewBeanList, lotDto, groupEntities, srvId, true);
	}

	/**
	 * ロット一覧情報をロットエンティティから設定する
	 *
	 * @param lotViewBeanList ロット一覧情報
	 * @param lotDto ロットのエンティティ
	 * @param groupEntities グループユーザのエンティティリスト
	 * @param isPullDown プルダウン用のリストかどうか
	 */
	private static void populateLotViewBeanPjtLotEntity(List<LotViewBean> lotViewBeanList, List<ILotDto> lotDto, List<IGrpEntity> groupEntities, String srvId,
			boolean isPullDown) {

		List<String> groupIdList = TriCollectionUtils.collect(groupEntities, TO_GROUP_ID);

		List<ILotDto> lotList = new ArrayList<ILotDto>();
		List<String> disableLotNoList = new ArrayList<String>();

		for (ILotDto lot : lotDto) {

			// グループの設定なし＝全グループがアクセス可能
			if (null == lot.getLotGrpLnkEntities() || 0 == lot.getLotGrpLnkEntities().size()) {
				lotList.add(lot);
				continue;
			}

			boolean listView = false;
			for (ILotGrpLnkEntity group : lot.getLotGrpLnkEntities()) {

				if (groupIdList.contains(group.getGrpId())) {
					lotList.add(lot);
					listView = true;
					break;
				}
			}

			if (!listView && !isPullDown && lot.getLotEntity().getIsAssetDup().parseBoolean()) {
				lotList.add(lot);
				disableLotNoList.add(lot.getLotEntity().getLotId());
			}
		}
		setLotViewBeanPjtLotEntity(lotViewBeanList, lotList, disableLotNoList, srvId);
	}

	/**
	 * ロット一覧情報をロットエンティティから設定する
	 *
	 * @param lotViewBeanList ロット一覧情報
	 * @param lotEntitys ロットのエンティティ
	 * @param disableLotNoList リンクを切断するロット番号のリスト
	 */
	public static void setLotViewBeanPjtLotEntity(List<LotViewBean> lotViewBeanList, List<ILotDto> lotDto, List<String> disableLotNoList, String srvId) {

		for (ILotDto dto : lotDto) {

			ILotEntity entity = dto.getLotEntity();
			LotViewBean viewBean = new LotViewBean();

			viewBean.setLotNo(entity.getLotId());
			viewBean.setLotName(entity.getLotNm());
			viewBean.setServerNo( srvId );
			viewBean.setLotStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()));
			viewBean.setEditLinkEnabled(isLotEnabled(entity));
			if ( AmLotStatusIdForExecData.LotEditError.equals(entity.getProcStsId())) {
				viewBean.isError(true);
			} else {
				viewBean.isError(false);
			}
			if ( AmLotStatusIdForExecData.EditingLot.equals(entity.getProcStsId())) {
				viewBean.isProcessing(true);
			} else {
				viewBean.isProcessing(false);
			}
			if (viewBean.isEditLinkEnabled() && disableLotNoList.contains(viewBean.getLotNo())) {
				viewBean.setEditLinkEnabled(false);
			}
			if (disableLotNoList.contains(viewBean.getLotNo())) {
				viewBean.setViewLinkEnabled(false);
			} else {
				viewBean.setViewLinkEnabled(true);
			}
			viewBean.setLotIconPath(UmDesignBusinessRuleUtils.getLotIconSharePath(entity));
			lotViewBeanList.add(viewBean);
		}
	}

	/**
	 * ロットの編集か可能かどうかを判定する。
	 *
	 * @param entity ロットのエンティティ
	 * @return ロットの編集が可能であれば、true。それ以外はfalseを戻します。
	 */
	private static boolean isLotEnabled(ILotEntity entity) {
		if (null == entity)
			return false;

		if (!AmLotStatusId.LotInProgress.equals(entity.getStsId()))
			return false;

		if (!AmLotStatusId.LotInProgress.equals(entity.getProcStsId()) && !AmLotStatusIdForExecData.LotEditError.equals(entity.getProcStsId()))
			return false;

		return true;
	}

	/**
	 * 申請情報一覧情報を申請情報エンティティから設定する
	 *
	 * @param applyInfoViewBeanList 申請情報一覧情報
	 * @param dtoList 申請情報のエンティティ
	 */
	public static void setApplyInfoViewBeanAssetApplyEntity(String userId, List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList,
			AreqDtoList dtoList) {

		for (IAreqDto dto : dtoList) {

			ApplyInfoViewPjtDetailBean viewBean = new ApplyInfoViewPjtDetailBean();

			setApplyInfoViewBeanAssetApplyEntity(userId, viewBean, dto );

			applyInfoViewBeanList.add(viewBean);
		}

	}

	/**
	 * 関連申請情報一覧情報を申請情報エンティティから設定する
	 *
	 * @param applyInfoViewBeanList 申請情報一覧情報
	 * @param applyNoAssetRelatedEntityMap 申請番号-関連申請情報のエンティティ
	 */
	public static void setRelatedApplyInfoViewBeanAssetApplyEntity(String userId, List<RelatedApplyInfoViewBean> relatedApplyInfoViewlBean,
			Map<String, IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap) {

		for (Map.Entry<String, IEhAssetRelatedEntity> entry : applyNoAssetRelatedEntityMap.entrySet()) {

			for (IAreqDto dto : entry.getValue().getAssetRelatedReturnEntity()) {

				RelatedApplyInfoViewBean viewBean = new RelatedApplyInfoViewBean();

				viewBean.setOrigApplyNo(entry.getKey());
				setApplyInfoViewBeanAssetApplyEntity(userId, viewBean, dto);

				relatedApplyInfoViewlBean.add(viewBean);
			}
		}

	}

	/**
	 * 申請情報一覧情報を申請情報エンティティから設定する
	 *
	 * @param viewBean 申請情報一覧情報
	 * @param dto 申請情報のエンティティ
	 */
	private static void setApplyInfoViewBeanAssetApplyEntity(String userId, ApplyInfoViewPjtDetailBean viewBean, IAreqDto dto) {

		viewBean.setApplyNo(dto.getAreqEntity().getAreqId());
		viewBean.setApplyId(dto.getAreqEntity().getAreqCtgCd());
		viewBean.setApplySubject(dto.getAreqEntity().getSummary());
		viewBean.setLendApplyDate(TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getLendReqTimestamp()));
		viewBean.setReturnApplyDate(TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getRtnReqTimestamp()));
		viewBean.setDelApplyDate(TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getDelReqTimestamp()));
		viewBean.setReturnApproveDate(TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getPjtAvlTimestamp()));

		viewBean.setEnableView(userId.equals(dto.getAreqEntity().getLendReqUserId()) ? true : false);

		if (AmBusinessJudgUtils.isLendApply(dto.getAreqEntity())) {
			viewBean.setApplyUser(dto.getAreqEntity().getLendReqUserNm());
			viewBean.setApplyUserId(dto.getAreqEntity().getLendReqUserId());
		} else if (AmBusinessJudgUtils.isReturnApply(dto.getAreqEntity())) {
			viewBean.setApplyUser(dto.getAreqEntity().getRtnReqUserNm());
			viewBean.setApplyUserId(dto.getAreqEntity().getRtnReqUserId());
		} else if (AmBusinessJudgUtils.isDeleteApply(dto.getAreqEntity())) {
			viewBean.setApplyUser(dto.getAreqEntity().getDelReqUserNm());
			viewBean.setApplyUserId(dto.getAreqEntity().getDelReqUserId());
		}

		viewBean.setApplyStatus(sheet.getValue(AmDesignBeanId.statusId, dto.getAreqEntity().getProcStsId()));
	}

	/**
	 * 申請情報一覧情報を申請情報エンティティから設定する
	 *
	 * @param applyInfoViewBeanList 申請情報一覧情報
	 * @param entityies 申請情報のエンティティ
	 */
	public static void setApplyInfoViewPjtApproveBeanAssetApplyEntity(List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList,
			AreqDtoList entityies, Map<String, IPjtEntity> pjtEntitiesMap) {

		// 変更管理に、承認のチェックボックスを出すかどうかの情報を格納するマップ
		Map<String, Boolean> approveViewMap = new HashMap<String, Boolean>();

		for (IAreqDto entity : entityies) {

			ApplyInfoViewPjtApproveBean viewBean = new ApplyInfoViewPjtApproveBean();

			String pjtNo = entity.getAreqEntity().getPjtId();
			IPjtEntity pjtEntity = pjtEntitiesMap.get(pjtNo);
			if (approveViewMap.containsKey(pjtNo)) {
				// 同じ変更管理番号がある場合、一番先頭の番号だけ表示する。
				pjtNo = "";
				pjtEntity = null;
			} else {
				// とりあえずチェックボックスを出さない、をデフォルトにする
				approveViewMap.put(pjtNo, new Boolean(false));
			}

			viewBean.setPjtNo(pjtNo);
			viewBean.setChangeCauseNo((String) ((null == pjtEntity) ? null : pjtEntity.getChgFactorNo()));
			viewBean.setApplyNo(entity.getAreqEntity().getAreqId());
			viewBean.setApplySubject(entity.getAreqEntity().getSummary());
			if (AmBusinessJudgUtils.isLendApply(entity.getAreqEntity())) {
				viewBean.setApplyUser(entity.getAreqEntity().getLendReqUserNm());
				viewBean.setApplyUserId(entity.getAreqEntity().getLendReqUserId());
				viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(entity.getAreqEntity().getLendReqTimestamp()));
			} else if (AmBusinessJudgUtils.isReturnApply(entity.getAreqEntity())) {
				viewBean.setApplyUser(entity.getAreqEntity().getRtnReqUserNm());
				viewBean.setApplyUserId(entity.getAreqEntity().getRtnReqUserId());
				viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(entity.getAreqEntity().getRtnReqTimestamp()));
			} else if (AmBusinessJudgUtils.isDeleteApply(entity.getAreqEntity())) {
				viewBean.setApplyUser(entity.getAreqEntity().getDelReqUserNm());
				viewBean.setApplyUserId(entity.getAreqEntity().getDelReqUserId());
				viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(entity.getAreqEntity().getDelReqTimestamp()));
			} else {
				viewBean.setApplyUser("");
				viewBean.setApplyUserId("");
				viewBean.setApplyDate("");
			}
			viewBean.setApplyStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getAreqEntity().getProcStsId()));

			// この申請が承認対象かどうかを判定する
			viewBean.setPjtApproveEnabled(AmBusinessJudgUtils.isPjtApproveEnabled(entity.getAreqEntity()));
			if (viewBean.isPjtApproveEnabled()) {
				approveViewMap.put(entity.getAreqEntity().getPjtId(), new Boolean(true));
				viewBean.setPjtApproveRejectEnabled(true);

				viewBean.setPjtApproveColor(sheet.getValue(UmDesignEntryKeyByCommon.bgColor));
			}

			applyInfoViewBeanList.add(viewBean);
		}

		// 承認チェックボックスの出しわけ
		for (ApplyInfoViewPjtApproveBean viewBean : applyInfoViewBeanList) {

			String pjtNo = viewBean.getPjtNo();

			if (!pjtNo.equals("") && approveViewMap.get(pjtNo).booleanValue()) {
				viewBean.setApproveView(true);
			}
		}
	}

	/**
	 * 変更管理承認履歴一覧情報を変更管理エンティティから設定する
	 *
	 * @param pjtApproveHistoryViewBeanList 変更管理承認履歴一覧情報
	 * @param pjtDto 変更管理のエンティティ
	 */
	public static void setPjtApproveHistoryViewBeanPjtEntity(List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList, List<IPjtAvlDto> pjtDto) {

		for (IPjtAvlDto dto : pjtDto) {

			IPjtEntity entity = dto.getPjtEntity();
			PjtApproveHistoryViewBean viewBean = new PjtApproveHistoryViewBean();

			viewBean.setPjtNo(entity.getPjtId());
			viewBean.setChangeCauseNo(entity.getChgFactorNo());
			viewBean.setApproveComment(dto.getPjtAvlEntity().getPjtAvlCmt());
			viewBean.setApproveTime(TriDateUtils.convertViewDateFormat(dto.getPjtAvlEntity().getPjtAvlTimestamp()));

			// 承認履歴のキーは、変更管理番号＋登録日時
//			viewBean.setApproveDate4Search(TriDateUtils.getDate(dto.getPjtAvlEntity().getPjtAvlTimestamp())); // キー情報のため、編集してはいけない
			viewBean.setApproveDate4Search(dto.getPjtAvlEntity().getPjtAvlSeqNo()); // V3でキー変更
			viewBean.setApproveUser(dto.getPjtAvlEntity().getPjtAvlUserNm());
			viewBean.setApproveUserId(dto.getPjtAvlEntity().getPjtAvlUserId());
			viewBean.setApproveCancelDate(TriDateUtils.convertViewDateFormat(dto.getPjtAvlEntity().getPjtAvlCancelTimestamp()));
			viewBean.setApproveCancelUser(dto.getPjtAvlEntity().getPjtAvlCancelUserNm());
			viewBean.setApproveCancelUserId(dto.getPjtAvlEntity().getPjtAvlCancelUserId());
			viewBean.setStatus(sheet.getValue(AmDesignBeanId.statusId, dto.getPjtAvlEntity().getStsId()));

			// 申請番号
			// カンマ編集
			List<IPjtAvlAreqLnkEntity> pjtAvlAreqEntities = dto.getPjtAvlAreqLnkEntityList();

			if (0 < pjtAvlAreqEntities.size()) {

				StringBuilder stb = new StringBuilder();
				for (IPjtAvlAreqLnkEntity pjtApplyEntity : pjtAvlAreqEntities) {
					if (0 < stb.length()) {
						stb.append(",");
					}
					stb.append(pjtApplyEntity.getAreqId());
				}
				viewBean.setApproveApplyNo(stb.toString());
			}

			// 取消ボタンを表示するのは、「承認」ステータスのデータだけ
			if (AmPjtStatusId.ChangePropertyApproved.equals(dto.getPjtAvlEntity().getStsId())) {
				viewBean.setApproveCancelView(true);
			}

			pjtApproveHistoryViewBeanList.add(viewBean);
		}

	}

	/**
	 * 変更管理情報の選択項目の設定を行う。
	 *
	 * @param pjtLotEntity ロット管理エンティティ
	 * @return PjtDetailInputBeanオブジェクト
	 */
	public static PjtDetailInputBean getPjtDetailInputBean(ILotEntity pjtLotEntity) {

		return getPjtDetailInputBean(pjtLotEntity, null);
	}

	/**
	 * 変更管理情報の選択項目の設定を行う。
	 *
	 * @param pjtLotEntity ロット管理エンティティ
	 * @param pjtEntity 変更管理エンティティ
	 * @return PjtDetailInputBeanオブジェクト
	 */
	public static PjtDetailInputBean getPjtDetailInputBean(ILotEntity pjtLotEntity, IPjtEntity pjtEntity) {

		PjtDetailInputBean pjtDetailInputBean = new PjtDetailInputBean();

		pjtDetailInputBean.setChangeCauseClassifyList(sheet.getValueList(AmDesignBeanId.changeCauseClassifyId));

		PjtDetailBean pjtDetailBean = getPjtDetailBean(pjtLotEntity, pjtEntity);

		pjtDetailInputBean.setPjtDetailBean(pjtDetailBean);

		return pjtDetailInputBean;
	}

	/**
	 * 変更管理情報の選択項目の設定を行う。
	 *
	 * @param pjtLotEntity ロット管理エンティティ
	 * @param pjtEntity 変更管理エンティティ
	 * @return PjtDetailViewBeanオブジェクト
	 */
	public static PjtDetailViewBean getPjtDetailViewBean(ILotEntity pjtLotEntity, IPjtEntity pjtEntity) {

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();

		PjtDetailBean pjtDetailBean = getPjtDetailBean(pjtLotEntity, pjtEntity);

		pjtDetailViewBean.setPjtDetailBean(pjtDetailBean);

		return pjtDetailViewBean;
	}

	/**
	 * 変更管理情報の選択項目の設定を行う。
	 *
	 * @param pjtDetailBean 変更管理情報
	 * @param pjtLotEntity ロット管理エンティティ
	 * @param pjtEntity 変更管理エンティティ
	 * @return PjtDetailViewBeanオブジェクト
	 */
	private static PjtDetailBean getPjtDetailBean(ILotEntity pjtLotEntity, IPjtEntity pjtEntity) {

		PjtDetailBean pjtDetailBean = new PjtDetailBean();

		pjtDetailBean.setLotNo(pjtLotEntity.getLotId());
		pjtDetailBean.setLotName(pjtLotEntity.getLotNm());

		if (null != pjtEntity) {

			pjtDetailBean.setPjtNo(pjtEntity.getPjtId());
			pjtDetailBean.setChangeCauseNo(pjtEntity.getChgFactorNo());
			pjtDetailBean.setChangeCauseClassify(sheet.getValue(AmDesignBeanId.changeCauseClassifyId, pjtEntity.getChgFactorId()));
			pjtDetailBean.setPjtStatus(sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()));

			pjtDetailBean.setPjtContent(pjtEntity.getContent());
			pjtDetailBean.setPjtSummary(pjtEntity.getSummary());
			pjtDetailBean.setSubmitterNm(pjtEntity.getRegUserNm());
			pjtDetailBean.setAssigneeId(pjtEntity.getAssigneeId());
			pjtDetailBean.setAssigneeNm(pjtEntity.getAssigneeNm());
			pjtDetailBean.setCtgId(pjtEntity.getCtgId());
			pjtDetailBean.setCtgNm(pjtEntity.getCtgNm());
			pjtDetailBean.setMstoneId(pjtEntity.getMstoneId());
			pjtDetailBean.setMstoneNm(pjtEntity.getMstoneNm());
			pjtDetailBean.setEnableTestComplete(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete));
		}

		return pjtDetailBean;
	}

	public static void setChaLibApplyViewBeanFromAssetApplyEntity(FlowChaLibLendListServiceBean.ChaLibApplyViewBean viewBean, IAreqEntity entity) {

		viewBean.setApplyNo(entity.getAreqId());
		viewBean.setApplyUser(entity.getLendReqUserNm());
		viewBean.setApplyUserId(entity.getLendReqUserId());
		viewBean.setPjtNo(entity.getPjtId());
		viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(entity.getLendReqTimestamp()));

		viewBean.setStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()));

		viewBean.setApplySubject(entity.getSummary());
		viewBean.setApplyReason(entity.getContent());
	}

	public static void setChaLibApplyViewBeanFromAssetApplyEntity(FlowChaLibRtnListServiceBean.ChaLibApplyViewBean viewBean, IAreqEntity entity) {

		viewBean.setApplyNo(entity.getAreqId());
		viewBean.setApplyUser(entity.getLendReqUserNm());
		viewBean.setApplyUserId(entity.getLendReqUserId());
		viewBean.setPjtNo(entity.getPjtId());
		viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(entity.getLendReqTimestamp()));

		viewBean.setStatus(sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()));

		viewBean.setApplySubject(entity.getSummary());
		viewBean.setApplyReason(entity.getContent());
	}

	public static void setChaLibListViewBeanAssetDelApplyEntity(TopMenuViewBean viewBean, IPjtEntity pjtEntity, IAreqEntity assetApplyEntity) {

		viewBean.setApplyNo(assetApplyEntity.getAreqId());
		viewBean.setApplyUser(assetApplyEntity.getDelReqUserNm());
		viewBean.setApplyUserId(assetApplyEntity.getDelReqUserId());
		viewBean.setPjtNo(assetApplyEntity.getPjtId());
		viewBean.setChangeCauseNo(pjtEntity.getChgFactorNo());
		viewBean.setApplyDate(TriDateUtils.convertViewDateFormat(assetApplyEntity.getDelReqTimestamp()));

		viewBean.setStatus(sheet.getValue(AmDesignBeanId.statusId, assetApplyEntity.getProcStsId()));

		viewBean.setApplySubject(assetApplyEntity.getSummary());
		viewBean.setApplyReason(assetApplyEntity.getContent());
	}

	/**
	 * 選択可能なグループ情報を取得します。
	 *
	 * @param userGroupList グループ名のリスト
	 * @param lotDto ロット
	 * @param entitys グループユーザエンティティ
	 */
	public static void getSelectGroupNameList(List<String> userGroupList, ILotDto lotDto, List<IGrpEntity> entitys) {

		List<String> allowGroupIdList = new ArrayList<String>();

		if (null != lotDto.getLotGrpLnkEntities()) {
			for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {
				allowGroupIdList.add(group.getGrpId());
			}
		}

		for (IGrpEntity entity : entitys) {

			// アクセス許可グループなし＝全グループがアクセス可能
			if (0 == allowGroupIdList.size() || allowGroupIdList.contains(entity.getGrpId())) {
				userGroupList.add(entity.getGrpNm());
			}
		}
	}
}
