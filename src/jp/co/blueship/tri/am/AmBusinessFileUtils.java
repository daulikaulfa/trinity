package jp.co.blueship.tri.am;

import java.io.File;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.dao.constants.TriExtensionType;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * AMで利用されるファイルアクセスに関連する機能を提供するUtilities
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public class AmBusinessFileUtils {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * バイナリdiff比較を行う
	 * <br>バイナリにスキップワードの概念はない。
	 * @param srcPath	比較元ファイルパス
	 * @param dstPath	比較先ファイルパス
	 * @return 判定結果
	 * @throws Exception
	 */
	public static Status checkBinaryDiff( String srcPath , String dstPath ) throws Exception {
		File srcFile = new File( srcPath ) ;
		File dstFile = new File( dstPath ) ;
		if( true != dstFile.exists() ) {//比較先ファイルが存在しない
			return Status.ADD ;
		}

		if( true == TriFileUtils.isSame( srcFile, dstFile ) ) {//一致
			return Status.EQUAL ;
		} else {//不一致
			return Status.CHANGE ;//コピーする
		}
	}

	/**
	 * 削除資産のdiff比較を行う
	 * @param dstPath	比較先ファイルパス
	 * @return 判定結果
	 * @throws Exception
	 */
	public static Status checkDeleteDiff( String dstPath ) throws Exception {
		File dstFile = new File( dstPath ) ;

		if ( ! dstFile.exists() )
			return Status.EQUAL;

		return Status.DELETE;
	}

	/**
	 * 引数で与えられたフォルダパス以下の内容をすべて削除する
	 * @param path フォルダのパス
	 * @throws Exception
	 */
	public static void cleaningDirectory( String path ) throws Exception {

		File file = new File( path ) ;

		if( true == file.exists() ) {
			TriFileUtils.delete( file ) ;
		}

		file.mkdirs() ;
	}

	/**
	 * if argument file is binary file, return <code>true</code><br>
	 * <br>
	 * 引数で与えられたファイルがバイナリファイルだった場合に<code>true</code>を返します<br>
	 *
	 * @param fileName
	 * @param ucfExtensionMap all extension info
	 * @return return <code>true</code> if file is binary file. Retrun <code>false</code> if file is not binary file.
	 */
	public static boolean isBinary( String fileName, Map<String, IExtEntity> ucfExtensionMap ){
		try {
			String extension = TriFileUtils.getExtension(fileName);

			extension = (null == extension) ? "" : extension;

			if (ucfExtensionMap.containsKey(extension)) {
				IExtEntity extEntity = ucfExtensionMap.get(extension);

				if (TriExtensionType.binary.equals(extEntity.getExtTyp())){
					return true;
				}
			}

			return false;

		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005004S, e, fileName);
		}
	}

	/**
	 * 貸出／返却フォルダ配下の作成を行う
	 *
	 * @param inOutBox
	 * @param id
	 * @param groupNameList
	 */
	public static final void createInOutBoxDirectory(LotParamInfo lotParamInfo, ILotDto lotDto) {

		// アクセス可能なグループ名一覧
		List<String> groupNameList = AmLibraryAddonUtils.getAccessableGroupNameList(lotDto);
		if (TriStringUtils.isEmpty(groupNameList)) {

			// 全グループ
			for (GroupViewBean group : lotParamInfo.getGroupViewBeanList()) {
				groupNameList.add(group.getGroupName());
			}
		}

		File dir = null;

		for (String groupName : groupNameList) {

			dir = new File(lotDto.getLotEntity().getInOutBox(), TriStringUtils
					.linkPath(sheet.getValue(AmDesignEntryKeyByDirectory.lendAssetRelationPath), groupName));
			dir.mkdirs();

			dir = new File(lotDto.getLotEntity().getInOutBox(), TriStringUtils
					.linkPath(sheet.getValue(AmDesignEntryKeyByDirectory.returnInfoRelationPath), groupName));
			dir.mkdirs();

			dir = new File(lotDto.getLotEntity().getInOutBox(), TriStringUtils
					.linkPath(sheet.getValue(AmDesignEntryKeyByDirectory.returnAssetBackupRelationPath), groupName));
			dir.mkdirs();

			/*
			 * dir = new File( lotEntity.getInOutBox(),
			 * StringAddonUtil.linkPath( DesignProjectUtil.getValue(
			 * DesignProjectCmnDirDefineId.sendBackRelationPath ), groupName )
			 * ); dir.mkdirs();
			 */
		}
	}
}
