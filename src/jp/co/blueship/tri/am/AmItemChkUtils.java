package jp.co.blueship.tri.am;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.constants.ChaLibScreenItemID;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 画面入力項目の必須チェック、形式チェックの共通処理Class
 *
 * @version V3L10.02
 *
 * @version SP-20150622_V3L13R01
 * @author Norheda Zulkipeli
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */

public class AmItemChkUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static TriFunction<IGrpEntity, String> TO_GROUP_ID = new TriFunction<IGrpEntity, String>() {

		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	/**
	 * ロット番号が選択されているかどうかチェックする
	 *
	 * @param lotId ロット番号
	 */

	public static void checkLotNo( String lotId ) {

		if ( TriStringUtils.isEmpty(lotId) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001003E, new String[] {} );
		}

	}

	/**
	 * 変更管理の入力情報をチェックする
	 * @param pjtDetailInputBean 変更管理入力情報
	 */

	public static void checkPjtInput( PjtDetailInputBean pjtDetailInputBean, FlowChaLibPjtEditSupport support)
		throws BaseBusinessException {

		PjtDetailBean bean = pjtDetailInputBean.getPjtDetailBean();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();
		IUmFinderSupport umSupport = support.getUmFinderSupport();

		try {

			//変更要因番号
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibPjtEntryCheck,
							ChaLibScreenItemID.FlowChaLibPjtEntryCheck.CHANGE_CAUSE_NO.toString() )) ) {

				if ( TriStringUtils.isEmpty( bean.getChangeCauseNo() )) {
					messageList.add		( AmMessageId.AM001009E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//変更要因分類
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibPjtEntryCheck,
							ChaLibScreenItemID.FlowChaLibPjtEntryCheck.CHANGE_CAUSE_CLASSIFY.toString() )) ) {

				if ( TriStringUtils.isEmpty( bean.getChangeCauseClassify() )) {
					messageList.add		( AmMessageId.AM001010E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//概要
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibPjtEntryCheck,
							ChaLibScreenItemID.FlowChaLibPjtEntryCheck.SUMMARY.toString() )) ) {

				if ( TriStringUtils.isEmpty( bean.getPjtSummary() )) {
					messageList.add		( AmMessageId.AM001011E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//内容
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibPjtEntryCheck,
							ChaLibScreenItemID.FlowChaLibPjtEntryCheck.CONTENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( bean.getPjtContent() )) {
					messageList.add		( AmMessageId.AM001012E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if (TriStringUtils.isNotEmpty(bean.getCtgId())) {
				ICtgEntity ctgEntity = umSupport.findCtgByPrimaryKey(bean.getCtgId());
				if (null == ctgEntity) {
					messageList.add		( AmMessageId.AM001130E );
					messageArgsList.add	( new String[] {bean.getCtgId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(bean.getMstoneId())) {
				IMstoneEntity mstoneEntity = umSupport.findMstoneByPrimaryKey(bean.getMstoneId());
				if (null == mstoneEntity) {
					messageList.add		( AmMessageId.AM001131E );
					messageArgsList.add	( new String[] {bean.getMstoneId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(bean.getAssigneeId())) {
				ILotDto lotDto = support.findLotDto(bean.getLotNo());
				List<ILotGrpLnkEntity> groupEntityArray = lotDto.getLotGrpLnkEntities();
				if (TriStringUtils.isNotEmpty(groupEntityArray)) {
					List<IGrpEntity> groups = umSupport.findGroupByUserId(bean.getAssigneeId());
					IGrpDao grpDao = support.getUmFinderSupport().getGrpDao();

					Set<String> myGroupIdSet = new HashSet<String>();
					TriCollectionUtils.collect(groups, TO_GROUP_ID, myGroupIdSet);
					boolean isBelong = false;

					for (ILotGrpLnkEntity grpEntity : groupEntityArray) {
						if (myGroupIdSet.contains(grpEntity.getGrpId())) {

							GrpCondition condition = new GrpCondition();
							condition.setGrpId(grpEntity.getGrpId());
							IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
							if (null == gEntity) {
								messageList.add		( AmMessageId.AM004039F );
								messageArgsList.add	( new String[] {grpEntity.getGrpId(), grpEntity.getGrpNm()} );
							}

							isBelong = true;
						}
					}
					if (!isBelong) {
						messageList.add		( AmMessageId.AM001132E );
						messageArgsList.add	( new String[] {bean.getLotNo()} );
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}

	/**
	 * 変更管理が承認可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 */

	public static void checkPjtApprove( PjtEditInputBean pjtEditInputBean ) throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibPjtApproveCheck,
						ChaLibScreenItemID.FlowChaLibPjtApproveCheck.APPROVE_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( pjtEditInputBean.getApproveComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001102E );
			}
		}
	}

	/**
	 * 変更管理が承認取消可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 */

	public static void checkPjtApproveCancel( PjtEditInputBean pjtEditInputBean )
		throws BaseBusinessException {
		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibPjtApproveCancelCheck,
						ChaLibScreenItemID.FlowChaLibPjtApproveCancelCheck.APPROVE_CANCEL_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( pjtEditInputBean.getApproveCancelComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001103E );
			}
		}
	}

	/**
	 * 変更管理が承認却下可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 */

	public static void checkPjtApproveReject( PjtEditInputBean pjtEditInputBean )  throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibPjtApproveRejectCheck,
						ChaLibScreenItemID.FlowChaLibPjtApproveRejectCheck.APPROVE_REJECT_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( pjtEditInputBean.getApproveRejectComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001104E );
			}
		}
	}


	/**
	 * 変更管理がクローズ可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 * @param applyList 変更管理に紐づく申請情報
	 */

	public static void checkPjtClose( PjtEditInputBean pjtEditInputBean, IAreqEntity[] applyList ) throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibPjtCloseCheck,
							ChaLibScreenItemID.FlowChaLibPjtCloseCheck.CLOSE_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( pjtEditInputBean.getCloseComment() )) {
					messageList.add		( AmMessageId.AM001013E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( 0 == applyList.length ) {
				messageList.add		( AmMessageId.AM001008E );
				messageArgsList.add	( new String[] {} );
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 変更管理が取消可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 */

	public static void checkPjtCancel( PjtEditInputBean pjtEditInputBean )  throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibPjtCancelCheck,
						ChaLibScreenItemID.FlowChaLibPjtCancelCheck.CANCEL_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( pjtEditInputBean.getDelComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001014E );
			}
		}

	}

	/**
	 * 変更管理がテスト完了可能かをチェックする
	 * @param pjtEditInputBean 画面入力情報
	 */

	public static void checkPjtTestComplete ( PjtEditInputBean pjtEditInputBean, IAreqEntity[] applyList )  throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			if ( !StatusFlg.off.value().equals(
				sheet.getValue(
				AmDesignBeanId.flowChaLibPjtTestCompleteCheck,
				ChaLibScreenItemID.FlowChaLibPjtTestCompleteCheck.TEST_COMPLETE_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( pjtEditInputBean.getTestCompleteComment() )) {
					messageList.add		( AmMessageId.AM001015E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( 0 == applyList.length ) {
				messageList.add		( AmMessageId.AM001129E );
				messageArgsList.add	( new String[] {} );
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ロットがクローズ可能かをチェックする
	 * @param lotEditInputBean 画面入力情報
	 */

	public static void checkLotClose( LotEditInputV3Bean lotEditInputBean ) throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibLotCloseCheck,
						ChaLibScreenItemID.FlowChaLibLotCloseCheck.CLOSE_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( lotEditInputBean.getCloseComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001013E );
			}
		}
	}

	/**
	 * ロットが取消可能かをチェックする
	 * @param lotEditInputBean 画面入力情報
	 */

	public static void checkLotCancel( LotEditInputV3Bean lotEditInputBean )  throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibLotCancelCheck,
						ChaLibScreenItemID.FlowChaLibLotCancelCheck.CANCEL_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( lotEditInputBean.getDelComment() )) {
				throw new ContinuableBusinessException( AmMessageId.AM001014E );
			}
		}
	}

	/**
	 * 変更管理番号が選択されているかをチェックする
	 * @param pjtNo 変更管理番号
	 */

	public static void checkPjtNo( String[] pjtNo ) throws BaseBusinessException {

		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001087E, new String[] {} );
		}
	}

	/**
	 * 変更管理番号が選択されているかをチェックする
	 * @param pjtNo 変更管理番号
	 */

	public static void checkPjtNo( String pjtNo ) throws BaseBusinessException {

		String[] pjtNoArray = { pjtNo };

		checkPjtNo( pjtNoArray );
	}

	/**
	 * 申請番号が選択されているかをチェックする
	 * @param applyNo 申請番号
	 */

	public static void checkApplyNo( String applyNo ) throws BaseBusinessException {

		if ( TriStringUtils.isEmpty( applyNo ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001097E, new String[] {} );
		}
	}

	/**
	 * 申請番号が選択されているかをチェックする
	 * @param applyNo 申請番号
	 */

	public static void checkApplyNo( String[] applyNo ) throws BaseBusinessException {

		if ( TriStringUtils.isEmpty( applyNo ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001097E, new String[] {} );
		}
	}

	/**
	 * 承認日時が選択されているかをチェックする
	 * @param approveDate 変更管理承認日時
	 */

	public static void checkApproveDate( String approveDate ) throws BaseBusinessException {

		if ( TriStringUtils.isEmpty( approveDate ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001100E, new String[] {} );
		}
	}
	/**
	 * ロット作成の基本情報がセットされているかをチェックする
	 * @param lotEditInputBean
	 * @param isCreate 登録の場合true。それ以外はfalseを設定する
	 */

	public static void checkLotBasicInfo( LotEditInputV3Bean lotEditInputBean , boolean isCreate ) throws Exception {

		List<IMessageId> messageList= new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		//ロット名
		if( TriStringUtils.isEmpty( lotEditInputBean.getLotName() ) ) {
			messageList.add		( AmMessageId.AM001017E );
			messageArgsList.add	( new String[] { } ) ;
		}

		//ロット概要
		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibLotEntryCheck,
						ChaLibScreenItemID.FlowChaLibLotEntryCheck.LOT_SUMMARY.toString() )) ) {

			if( TriStringUtils.isEmpty( lotEditInputBean.getLotSummary() ) ) {
				messageList.add		( AmMessageId.AM001018E );
				messageArgsList.add	( new String[] { } ) ;
			}
		}

		//ロット内容
		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						AmDesignBeanId.flowChaLibLotEntryCheck,
						ChaLibScreenItemID.FlowChaLibLotEntryCheck.LOT_CONTENT.toString() )) ) {

			if( TriStringUtils.isEmpty( lotEditInputBean.getLotContent() ) ) {
				messageList.add		( AmMessageId.AM001019E );
				messageArgsList.add	( new String[] { } ) ;
			}
		}

		if( true == isCreate ) {
			//ベースラインタグ
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibLotEntryCheck,
							ChaLibScreenItemID.FlowChaLibLotEntryCheck.BASELINE_TAG.toString() )) ) {

				if( TriStringUtils.isEmpty( lotEditInputBean.getBaseLineTag() ) ) {
					messageList.add		( AmMessageId.AM001028E );
					messageArgsList.add	( new String[] { } ) ;
				}
			}

			//公開ディレクトリパス
			if( TriStringUtils.isEmpty( lotEditInputBean.getDirectoryPathPublic() ) ) {
				messageList.add		( AmMessageId.AM001020E );
				messageArgsList.add	( new String[] { } ) ;
			} else {
				//パス文字列をチェック、実在パスがあれば大文字/小文字を実パスにあわせる
				String chkPath = BusinessFileUtils.toUpperPathDriveLetter( lotEditInputBean.getDirectoryPathPublic() ) ;
				if( null != chkPath ) {
					chkPath = BusinessFileUtils.chkPartialPathIsCanonical( chkPath ) ;
					if( null != chkPath ) {
						lotEditInputBean.setDirectoryPathPublic( chkPath ) ;
						if ( false == BusinessFileUtils.isMkdirs( new File( chkPath ) ) ) {
							chkPath = null ;
						}
					}
				}
				if( null == chkPath ) {
					messageList.add		( AmMessageId.AM001026E );
					messageArgsList.add	( new String[] { lotEditInputBean.getDirectoryPathPublic() } ) ;
				}
			}

			//非公開ディレクトリパス
			if( TriStringUtils.isEmpty( lotEditInputBean.getDirectoryPathPrivate() ) ) {
				messageList.add		( AmMessageId.AM001021E );
				messageArgsList.add	( new String[] { } ) ;
			} else {
				//パス文字列をチェック、実在パスがあれば大文字/小文字を実パスにあわせる
				String chkPath = BusinessFileUtils.toUpperPathDriveLetter( lotEditInputBean.getDirectoryPathPrivate() ) ;
				if( null != chkPath ) {
					chkPath = BusinessFileUtils.chkPartialPathIsCanonical( chkPath ) ;
					if( null != chkPath ) {
						lotEditInputBean.setDirectoryPathPrivate( chkPath ) ;
						if ( false == BusinessFileUtils.isMkdirs( new File( chkPath ) ) ) {
							chkPath = null ;
						}
					}
				}
				if( null == chkPath ) {
					messageList.add		( AmMessageId.AM001027E );
					messageArgsList.add	( new String[] { lotEditInputBean.getDirectoryPathPrivate() } ) ;
				}
			}
		}

		if ( 0 != messageList.size() )
			throw new ContinuableBusinessException( messageList , messageArgsList ) ;
	}
	/**
	 * ロット作成時のモジュール選択がされているかをチェックする
	 * @param lotEditInputBean
	 */

	public static void checkLotModuleInfo( LotEditInputV3Bean lotEditInputBean ) {

		if( null == lotEditInputBean.getSelectedModuleViewBeanList() ||
				0 == lotEditInputBean.getSelectedModuleViewBeanList().size() ) {
			throw new ContinuableBusinessException( AmMessageId.AM001022E ) ;
		}
	}
	/**
	 * ロット作成時のビルド環境が選択されているかをチェックする
	 * @param lotEditInputBean
	 */

	public static void checkLotBuildEnvInfo( LotEditInputV3Bean lotEditInputBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		if( TriStringUtils.isEmpty( lotEditInputBean.getSelectedBuildEnvNo() ) ) {
			messageList.add		( AmMessageId.AM001023E );
			messageArgsList.add	( new String[] { } ) ;
		}
		if( TriStringUtils.isEmpty( lotEditInputBean.getSelectedFullBuildEnvNo() ) ) {
			messageList.add		( AmMessageId.AM001024E );
			messageArgsList.add	( new String[] { } ) ;
		}
		if( true != TriStringUtils.isEmpty( lotEditInputBean.getSelectedBuildEnvNo() ) &&
			true != TriStringUtils.isEmpty( lotEditInputBean.getSelectedFullBuildEnvNo() ) &&
			lotEditInputBean.getSelectedBuildEnvNo().equals( lotEditInputBean.getSelectedFullBuildEnvNo() ) ) {
			messageList.add		( AmMessageId.AM001025E );
			messageArgsList.add	( new String[] { } ) ;
		}

		if ( 0 != messageList.size() )
			throw new ContinuableBusinessException( messageList , messageArgsList ) ;
	}
	/**
	 * ロット編集が可能かをチェックする
	 * @param paramBean フロー情報
	 */

	public static void checkLotModifyCheck( FlowChaLibLotModifyServiceBean paramBean )  throws BaseBusinessException {
		//マージエラーチェック
		WarningCheckBean warningCheckError = paramBean.getWarningCheckError() ;
		if ( warningCheckError.isExistWarning() ) {

			if ( !warningCheckError.isConfirmCheck() ) {
				throw new ContinuableBusinessException( AmMessageId.AM001036E );
			}
		}
	}

	/**
	 * コンフリクトチェックが可能かをチェックする
	 * @param paramBean フロー情報
	 */

	public static void checkConflictCheck( FlowChaLibConflictCheckServiceBean paramBean )  throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList	= new ArrayList<String[]>();

		try {

			MergeEditInputBean mergeEditInputBean = paramBean.getMergeEditInputBean() ;

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibConflictCheckCheck,
							ChaLibScreenItemID.FlowChaLibConflictCheckCheck.CONFLICT_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( mergeEditInputBean.getConflictCheckComment() )) {
					messageList.add		( AmMessageId.AM001108E );
					messageArgsList.add	( new String[] {} );
				}
			}

//			マージエラーチェック
			WarningCheckBean warningCheckAssetMergeCommitError = paramBean.getWarningCheckMergeCommitError() ;
			if ( warningCheckAssetMergeCommitError.isExistWarning() ) {

				if ( !warningCheckAssetMergeCommitError.isConfirmCheck() ) {
					messageList.add		( AmMessageId.AM001115E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}

	/**
	 * マージコミットが可能かをチェックする
	 * @param paramBean フロー情報
	 */

	public static void checkMergeCommitCheck( FlowChaLibMergeCommitServiceBean paramBean )  throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			MergeEditInputBean mergeEditInputBean = paramBean.getMergeEditInputBean() ;

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibMergeCommitCheck,
							ChaLibScreenItemID.FlowChaLibMergeCommitCheck.MERGE_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( mergeEditInputBean.getConflictCheckComment() )) {
					messageList.add		( AmMessageId.AM001109E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//マージエラーチェック
			WarningCheckBean warningCheckAssetMergeCommitError = paramBean.getWarningCheckMergeCommitError() ;
			if ( warningCheckAssetMergeCommitError.isExistWarning() ) {

				if ( !warningCheckAssetMergeCommitError.isConfirmCheck() ) {
					messageList.add		( AmMessageId.AM001115E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}

	public static void checkCheckinDueDate(String checkinDueDate) throws ParseException {

		if (TriStringUtils.isEmpty(checkinDueDate)) {
			return;
		}

		if (!TriDateUtils.checkYMD(checkinDueDate)) {
			throw new ContinuableBusinessException( AmMessageId.AM001140E );
		}

		checkinDueDate = TriDateUtils.fillYMD(checkinDueDate);

		String today = TriDateUtils.getYMDDateFormat().format( TriDateUtils.getSystemTimestamp() );

		if (!TriDateUtils.before(today, checkinDueDate)) {
			throw new ContinuableBusinessException( AmMessageId.AM001139E );
		}
	}
}
