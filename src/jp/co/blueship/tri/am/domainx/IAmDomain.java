package jp.co.blueship.tri.am.domainx;

import jp.co.blueship.tri.fw.domainx.IDomain;

/**
 * The interface of the domain layer.
 * <br>If the service function being managed by the AM is used by another module (BM, etc.),
 * <br>You need to always implement this interface.
 * <br>
 * <br>ドメイン層のインタフェースです。
 * <br>AMで管理されているサービス機能が別モジュール(BM等)から利用される場合、
 * 必ずこのインタフェースを実装する必要があります。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IAmDomain<E> extends IDomain<E> {

}
