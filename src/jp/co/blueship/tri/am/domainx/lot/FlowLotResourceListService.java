package jp.co.blueship.tri.am.domainx.lot;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmLotResourceSelectionUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ILotResourceViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.IResourceViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceFolderView;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */

public class FlowLotResourceListService implements IDomain<FlowLotResourceListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowLotResourceListServiceBean> execute(IServiceDto<FlowLotResourceListServiceBean> serviceDto) {

		FlowLotResourceListServiceBean paramBean = serviceDto.getServiceBean();
		try {
			paramBean = serviceDto.getServiceBean();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			} else if (RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowLotResourceListServiceBean paramBean) {

		// Build Tree Folder Views
		{
			List<Object> paramList = this.setParamList(paramBean.getResourceFolderView(),
					paramBean.getParam().getResourceSelection(), paramBean.getParam().getSelectedLotId(), null);
			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
			AmLotResourceSelectionUtils.setTotalResourceDetail(paramBean, paramList, masterPath, this.support.getUmFinderSupport() );
			AmLotResourceSelectionUtils.buildTreeFolderViewByInit(paramList, masterPath);
		}

	}

	private void onChange(FlowLotResourceListServiceBean paramBean) {
		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		String selectedLotId = paramBean.getParam().getSelectedLotId();

		List<Object> paramList = this.setParamList(paramBean.getResourceFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, null);
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {
			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmLotResourceSelectionUtils.openFolder(paramList, masterPath);
				return;
			}

			else if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmLotResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			else if (ResourceRequestType.openAllFolder.equals(selectedType)) {
				AmLotResourceSelectionUtils.openAllFolder( paramList, masterPath );
				return;
			}

			else if (ResourceRequestType.closeAllFolder.equals(selectedType)) {
				AmLotResourceSelectionUtils.closeAllFolder( paramList );
				return;
			}

			else if (ResourceRequestType.selectFolder.equals(selectedType)) {
				this.selectFolder(paramBean, paramList);
				return;
			}
		}
	}


	private void selectFolder(FlowLotResourceListServiceBean paramBean, List<Object> paramList) {
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		setSelectedPath(paramList);

		// Set Asset Selection List
		AmLotResourceSelectionUtils.setAssetSelectList(paramBean, paramList, masterPath, this.support.getUmFinderSupport() );

		// Build tree folder view again
		AmLotResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}

	private void setSelectedPath(List<Object> paramList) {
		ResourceSelection resourceSelection = AmExtractEntityAddonUtils.extractLotResourceSelection(paramList);
		ResourceFolderView<IResourceViewBean> folderView = AmExtractEntityAddonUtils.extractLotResourceFolderView(paramList);
		// Set all selected files
		String beforeDir = TriStringUtils.convertPath(folderView.getSelectedPath(), true);
		String afterDir = TriStringUtils.convertPath(resourceSelection.getPath(), true);

		afterDir = (TriStringUtils.isEmpty(afterDir)) ? beforeDir : afterDir;
		folderView.setSelectedPath(afterDir);
	}

	private List<Object> setParamList(ResourceFolderView<ILotResourceViewBean> folderView,
			ResourceSelection resourceSelection, String selectedLotId, String selectedAreqId) {
		List<Object> paramList = new ArrayList<Object>();

		ILotDto lotDto = this.support.findLotDto(selectedLotId, AmTables.AM_LOT_MDL_LNK);
		List<IAreqDto> areqDtoList = new ArrayList<IAreqDto>();

		AreqCondition condition = AmResourceSelectionUtils.setAreqConditionForLockedResource(selectedLotId);

		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> assetAreqEntities =
				this.support.getAreqDao().find( condition.getCondition(), sort );
		for (IAreqEntity areqEntity: assetAreqEntities) {
			if (areqEntity.getAreqId().equals(selectedAreqId)) {
				continue;
			}
			IAreqDto areqDto = this.support.findAreqDto(areqEntity, new AmTables[]{AmTables.AM_AREQ_FILE, AmTables.AM_AREQ_BINARY_FILE});
			areqDtoList.add(areqDto);
		}

		paramList.add(lotDto);
		paramList.add(areqDtoList.toArray(new IAreqDto[0]));
		paramList.add(resourceSelection);
		paramList.add(folderView);
		return paramList;
	}

}
