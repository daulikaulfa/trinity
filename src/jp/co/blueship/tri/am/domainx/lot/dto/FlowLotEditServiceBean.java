package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private LotDetailsView detailsView = new LotDetailsView();

	{
		this.setInnerService( new FlowChaLibLotModifyServiceBean() );
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowLotEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	public LotDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowLotEditServiceBean setDetailsView(LotDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private LotEditInputBean inputInfo = new LotEditInputBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public LotEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(LotEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Lot Information
	 */
	public class LotDetailsView {
		private String lotId = null;
		private String stsId = null;
		private String status = null;
		private String createdTime = null;
		private String latestBaselineTag = null;
		private String latestVerTag = null;
		private String branchBaselineTag = null;
		private String publicPath = null;
		private String privatePath = null;
		private boolean useMerge;
		private boolean enableUnauthorizedGroup;

		public String getLotId() {
			return lotId;
		}
		public LotDetailsView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public LotDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public LotDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getCreatedTime() {
			return createdTime;
		}
		public LotDetailsView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}
		public String getLatestBaselineTag() {
			return latestBaselineTag;
		}
		public LotDetailsView setLatestBaselineTag(String latestBaselineTag) {
			this.latestBaselineTag = latestBaselineTag;
			return this;
		}
		public String getLatestVerTag() {
			return latestVerTag;
		}
		public LotDetailsView setLatestVerTag(String latestVerTag) {
			this.latestVerTag = latestVerTag;
			return this;
		}
		public String getBranchBaselineTag() {
			return branchBaselineTag;
		}
		public LotDetailsView setBranchBaselineTag(String branchBaselineTag) {
			this.branchBaselineTag = branchBaselineTag;
			return this;
		}
		public String getPublicPath() {
			return publicPath;
		}
		public LotDetailsView setPublicPath(String publicPath) {
			this.publicPath = publicPath;
			return this;
		}
		public String getPrivatePath() {
			return privatePath;
		}
		public LotDetailsView setPrivatePath(String privatePath) {
			this.privatePath = privatePath;
			return this;
		}

		public boolean isUseMerge() {
			return useMerge;
		}

		public LotDetailsView setUseMerge(boolean useMerge) {
			this.useMerge = useMerge;
			return this;
		}


		public boolean isEnableUnauthorizedGroup() {
			return enableUnauthorizedGroup;
		}

		public LotDetailsView setEnableUnauthorizedGroup(boolean enableUnauthorizedGroup) {
			this.enableUnauthorizedGroup = enableUnauthorizedGroup;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
