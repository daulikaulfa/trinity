package jp.co.blueship.tri.am.domainx.lot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.IDashboardLotViewBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowDashboardServiceLotListBean;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;

/**
 * Provide the following backend services.
 * <br> - Dashboard List
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowDashboardServiceLotList implements IAmDomain<FlowDashboardServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IAmFinderSupport amSupport = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
	}

	@Override
	public IServiceDto<FlowDashboardServiceBean> execute( IServiceDto<FlowDashboardServiceBean> serviceDto ) {

		FlowDashboardServiceBean paramBean = serviceDto.getServiceBean();

		try {
			FlowDashboardServiceLotListBean lotListBean = paramBean.getLotListBean();
			if ( null == lotListBean ) {
				return serviceDto;
			}

			LotCondition condition = this.getLotCondition(lotListBean);

			List<String> disableLinkLotNumbers = new ArrayList<String>();
			boolean isIncludeAllowList = true;
			if(ServiceId.UmRecentlyViewedLotsService.value().equals(paramBean.getFlowAction())) {
				isIncludeAllowList = false;
				condition.setJoinAccsHist(true, JoinType.RIGHT );
			}
			boolean isSearch = this.amSupport.setAccessableLotNumbers(condition, paramBean, isIncludeAllowList, disableLinkLotNumbers, true);

			IEntityLimit<ILotEntity> entityLimit = null;

			if (isSearch) {
				ISqlSort sort = this.getLotSortFromDesignDefineByChaLibTop();

				int maxPage = (lotListBean.getParam().isShowAll())? 0: lotListBean.getParam().getLinesPerPage();

				entityLimit =
					this.amSupport.getLotDao().find(
						condition.getCondition(),
						sort,
						lotListBean.getParam().getSearchCondition().getSelectedPageNo(),
						maxPage);

				ILimit limit = entityLimit.getLimit();
				lotListBean.getPage()
					.setSelectPageNo(limit.getPageBar().getValue())
					.setMaxPageNo(limit.getPageBar().getMaximum())
					.setViewRows(limit.getViewRows())
					.setMaxRows(limit.getMaxRows())
				;
			} else {
				entityLimit = new EntityLimit<ILotEntity>();
				entityLimit.setEntities(FluentList.from(new ILotEntity[0]).asList());
			}

			if (TriStringUtils.isEmpty(entityLimit.getEntities()) && TriStringUtils.isEmpty(paramBean.getInfoMessageId())) {
				paramBean.setInfoMessage(AmMessageId.AM001122E);
			}

			List<ILotDto> lotDto = this.amSupport.findLotDto(entityLimit.getEntities(), AmTables.AM_LOT);

			List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();
			AmViewInfoAddonUtils.setLotViewBeanPjtLotEntity(lotViewBeanList, lotDto, disableLinkLotNumbers, null );

			lotListBean.setLotViews(
					FluentList.from(lotViewBeanList).map(
						new TriFunction<LotViewBean, IDashboardLotViewBean>() {
							public IDashboardLotViewBean apply(LotViewBean input) {
								return input;
							}
						} ).asList());

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * @param lotListBean
	 * @return
	 */
	private LotCondition getLotCondition( FlowDashboardServiceLotListBean lotListBean ) {
		LotCondition condition = new LotCondition();
		List<String> stsIds = new ArrayList<String>();
		if ( lotListBean.getParam().getSearchCondition().isSelectedLotInProgress() ) {
			stsIds.add( AmLotStatusId.LotInProgress.getStatusId() );
		}
		if ( lotListBean.getParam().getSearchCondition().isSelectedLotClosed() ) {
			stsIds.add( AmLotStatusId.LotClosed.getStatusId() );
		}
		condition.setStsIds( FluentList.from(stsIds).toArray(new String[0]) );
		condition.setJoinAccsHist( true );
		condition.setContainsByKeyword( lotListBean.getParam().getIncrementalCondition() );

		return condition;
	}

	/**
	 * @return
	 */
	private ISqlSort getLotSortFromDesignDefineByChaLibTop() {

		ISqlSort sort = new SortBuilder();

		sort.setElement(AccsHistItems.accsTimestamp, TriSortOrder.Desc, 1);
		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.topListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.topListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

}
