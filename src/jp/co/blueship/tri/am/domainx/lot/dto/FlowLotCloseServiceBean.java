package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCloseServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowLotCloseServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibLotCloseServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowLotCloseServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private LotCloseInputInfo inputInfo = new LotCloseInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public LotCloseInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(LotCloseInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Lot close input information
	 */
	public class LotCloseInputInfo {
		private String comment;

		public String getComment(){
			return comment;
		}
		public LotCloseInputInfo setComment(String comment){
			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
