package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibLotCancelServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowLotRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private LotRemovalInputInfo inputInfo = new LotRemovalInputInfo();
		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		
		public LotRemovalInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(LotRemovalInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
	
	/**
	 * Lot Removal Input Information
	 */
	public class LotRemovalInputInfo {
		private String comment;

		public String getComment(){
			return comment;
		}
		public LotRemovalInputInfo setComment(String comment){
			this.comment = comment;
			return this;
		}
	}
}
