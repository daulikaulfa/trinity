package jp.co.blueship.tri.am.domainx.lot.beans.dto;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public interface IResourceViewBean {
	public boolean isFile();
	public boolean isDirectory();
	public String getPath();
	public String getName();
	public String getSize();
	public int getCount();
}
