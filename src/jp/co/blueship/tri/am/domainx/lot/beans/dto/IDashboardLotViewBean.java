package jp.co.blueship.tri.am.domainx.lot.beans.dto;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Thang Vu
 */
public interface IDashboardLotViewBean {
	public String getLotId();
	public String getLotSubject();
	public String getLotIconPath();
}
