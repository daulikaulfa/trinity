package jp.co.blueship.tri.am.domainx.lot;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCloseServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCloseServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCloseServiceBean.LotCloseInputInfo;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * Provide the following backend services.
 * <br> - Close
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Sam
 *
 */
public class FlowLotCloseService implements IDomain<FlowLotCloseServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibPjtEditSupport support = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}
	public void setConfirmationService(IDomain<IGeneralServiceBean> service) {
		this.confirmationService = service;
	}
	public void setCompleteService(IDomain<IGeneralServiceBean> service) {
		this.completeService = service;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}
	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}
	
	@Override
	public IServiceDto<FlowLotCloseServiceBean> execute( IServiceDto<FlowLotCloseServiceBean> serviceDto ) {

		FlowLotCloseServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibLotCloseServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
				this.validate(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void validate( FlowLotCloseServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );
		
		// Status Matrix Check
		{
			IPjtEntity[] entities = this.support.getPjtEntityLimit( paramBean.getParam().getSelectedLotId(), 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( this.support.getPjtNo( entities ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer( ChaLibScreenID.LOT_CLOSE_CONFIRM);

		//closeLotConfirmation
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CLOSE);
			confirmationService.execute(dto);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowLotCloseServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotCloseServiceBean serviceBean = paramBean.getInnerService();
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			IPjtEntity[] entities = this.support.getPjtEntityLimit( paramBean.getParam().getSelectedLotId(), 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( this.support.getPjtNo( entities ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer( ChaLibScreenID.LOT_CLOSE_CONFIRM );

		//closeLotConfirmation
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CLOSE );
			confirmationService.execute(dto);
		}
		//editLotComplete
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CLOSE );
			completeService.execute(dto);
		}
		
		//mail Lot Close 
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CLOSE );
			mailService.execute(dto);
			
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003012I, serviceBean.getLotDetailViewBean().getLotName() );
	}

	private void beforeExecution(FlowLotCloseServiceBean src, FlowChaLibLotCloseServiceBean dest) {
		LotCloseInputInfo srcInfo = src.getParam().getInputInfo();
		LotEditInputV3Bean destInfo = dest.getLotEditInputBean();
		src.getResult().setCompleted(false);

		if (RequestType.validate.equals(src.getParam().getRequestType())) {
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
			dest.setUserId(src.getUserId());
			dest.setUserName(src.getUserName());
			destInfo.setCloseComment(srcInfo.getComment());
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
			dest.setUserId(src.getUserId());
			dest.setUserName(src.getUserName());
			destInfo.setCloseComment(srcInfo.getComment());
		}
	}

}
