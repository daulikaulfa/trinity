package jp.co.blueship.tri.am.domainx.lot;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotRemovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * Provide the following backend services.
 * <br> - Remove
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotRemovalService implements IDomain<FlowLotRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibPjtEditSupport support = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}
	public void setConfirmationService(IDomain<IGeneralServiceBean> service) {
		this.confirmationService = service;
	}
	public void setCompleteService(IDomain<IGeneralServiceBean> service) {
		this.completeService = service;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}
	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}
	
	@Override
	public IServiceDto<FlowLotRemovalServiceBean> execute( IServiceDto<FlowLotRemovalServiceBean> serviceDto ) {

		FlowLotRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibLotCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
				this.validate(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void validate( FlowLotRemovalServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );
		// Status Matrix Check
		{
			IPjtEntity[] entities = this.support.getPjtEntityLimit( paramBean.getParam().getSelectedLotId(), 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( this.support.getPjtNo( entities ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);
		serviceBean.setReferer( ChaLibScreenID.LOT_DETAIL_VIEW );

		//cancelLotConfirmation
		{
			serviceBean.setForward( ChaLibScreenID.LOT_CANCEL_CONFIRM );
			confirmationService.execute(dto);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowLotRemovalServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			IPjtEntity[] entities = this.support.getPjtEntityLimit( paramBean.getParam().getSelectedLotId(), 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( this.support.getPjtNo( entities ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer( ChaLibScreenID.LOT_DETAIL_VIEW );

		//editLotConfirmation
		{
			serviceBean.setForward( ChaLibScreenID.LOT_CANCEL_CONFIRM );
			confirmationService.execute(dto);
		}
		//editLotComplete
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CANCEL );
			completeService.execute(dto);
		}
		//mailLotCancel
		
		{
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_CANCEL );
			mailService.execute(dto);
		}
		
		
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003013I, serviceBean.getLotDetailViewBean().getLotName() );
	}

	private void beforeExecution(FlowLotRemovalServiceBean src, FlowChaLibLotCancelServiceBean dest) {

		src.getResult().setCompleted(false);

		if (RequestType.validate.equals(src.getParam().getRequestType())) {
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
			dest.getLotEditInputBean().setDelComment(src.getParam().getInputInfo().getComment());
		}
	}

}
