package jp.co.blueship.tri.am.domainx.lot.beans.dto;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public interface ILotResourceViewBean extends IResourceViewBean {
	public boolean isLocked();
	public String getLockedDate();
	public boolean isCheckoutRequest();
	public boolean isCheckinRequest();
	public boolean isRemovalRequest();
	public String getSubmitterId();
	public String getSubmitterNm();
	public String getSubmitterIconPath();
}
