package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.BuildPackageCreationResultViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private LotDetailsViewBean detailsView = new LotDetailsViewBean();
	private BuildPackageCreationResultViewBean buildResult = new BuildPackageCreationResultViewBean();

	public RequestParam getParam() {
		return param;
	}

	public LotDetailsViewBean getDetailsView() {
		return detailsView;
	}

	public FlowLotDetailsServiceBean setDetailsView(LotDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public BuildPackageCreationResultViewBean getBuildResult() {
		return buildResult;
	}
	public FlowLotDetailsServiceBean setBuildResult(BuildPackageCreationResultViewBean buildResult) {
		this.buildResult = buildResult;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
	}

}
