package jp.co.blueship.tri.am.domainx.lot.beans.dto;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public class LotResourceViewBean implements ILotResourceViewBean {
	@Expose	private boolean directory;
	@Expose	private String path;
	@Expose	private String name;
	@Expose	private String size;
	@Expose private int count = 0;
	@Expose private boolean locked = false;
	@Expose private String lockedDate;
	@Expose private AreqCtgCd areqType;
	@Expose private String submitterId;
	@Expose private String submitterNm;
	@Expose private String submitterIconPath;

	@Override
	public boolean isFile() {
		return !this.directory;
	}
	@Override
	public boolean isDirectory() {
		return this.directory;
	}
	public LotResourceViewBean setDirectory(boolean directory) {
		this.directory = directory;
		return this;
	}

	@Override
	public String getPath() {
		return this.path;
	}
	public LotResourceViewBean setPath( String path ) {
		this.path = path;
		return this;
	}

	@Override
	public String getName() {
		return this.name;
	}
	public LotResourceViewBean setName( String name ) {
		this.name = name;
		return this;
	}

	@Override
	public String getSize() {
		return this.size;
	}
	public LotResourceViewBean setSize( String size ) {
		this.size = size;
		return this;
	}

	@Override
	public int getCount() {
		return this.count;
	}
	public LotResourceViewBean setCount( int count ) {
		this.count = count;
		return this;
	}

	@Override
	public boolean isLocked() {
		return this.locked;
	}
	public LotResourceViewBean setLocked( boolean locked ) {
		this.locked = locked;
		return this;
	}

	@Override
	public String getLockedDate() {
		return this.lockedDate;
	}
	public LotResourceViewBean setLockedDate( String lockedDate ) {
		this.lockedDate = lockedDate;
		return this;
	}

	@Override
	public boolean isCheckoutRequest() {
		return AmBusinessJudgUtils.isLendApply(this.areqType);
	}
	@Override
	public boolean isCheckinRequest() {
		return AmBusinessJudgUtils.isReturnApply(this.areqType);
	}
	@Override
	public boolean isRemovalRequest() {
		return AmBusinessJudgUtils.isDeleteApply(this.areqType);
	}

	public LotResourceViewBean setAreqType( AreqCtgCd areqType ){
		this.areqType = areqType;
		return this;
	}

	@Override
	public String getSubmitterId() {
		return this.submitterId;
	}
	public LotResourceViewBean setSubmitterId( String submitterId ) {
		this.submitterId = submitterId;
		return this;
	}

	@Override
	public String getSubmitterNm() {
		return this.submitterNm;
	}
	public LotResourceViewBean setSubmitterNm( String submitterNm ) {
		this.submitterNm = submitterNm;
		return this;
	}

	@Override
	public String getSubmitterIconPath() {
		return this.submitterIconPath;
	}
	public LotResourceViewBean setSubmitterIconPath( String submitterIconPath ) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}
}
