package jp.co.blueship.tri.am.domainx.lot.beans.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public class ResourceSelection {
	private String path = null;
	private ResourceRequestType type = ResourceRequestType.none;

	public String getPath() {
		return path;
	}
	public ResourceSelection setPath(String path) {
		this.path = path;
		return this;
	}
	public ResourceRequestType getType() {
		return type;
	}
	public ResourceSelection setType(ResourceRequestType type) {
		this.type = type;
		return this;
	}

	public enum ResourceRequestType {
		none			( "" ),
		openFolder		( "open" ),
		closeFolder		( "close" ),
		selectFolder	( "selectFolder" ),

		openAllFolder	( "openAll" ),
		closeAllFolder	( "closeAll" ),
		;

		private String value = null;

		private ResourceRequestType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			ResourceRequestType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static ResourceRequestType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( ResourceRequestType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
