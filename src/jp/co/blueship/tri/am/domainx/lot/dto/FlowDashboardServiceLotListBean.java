package jp.co.blueship.tri.am.domainx.lot.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.IDashboardLotViewBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceLotListBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private List<IDashboardLotViewBean> lotViews = new ArrayList<IDashboardLotViewBean>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<IDashboardLotViewBean> getLotViews() {
		return lotViews;
	}
	public FlowDashboardServiceLotListBean setLotViews( List<IDashboardLotViewBean> lotViews ) {
		this.lotViews = lotViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowDashboardServiceLotListBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam {
		private String incrementalCondition = null;
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 5;
		private boolean showAll = false;

		public String getIncrementalCondition() {
			return incrementalCondition;
		}
		public RequestParam setIncrementalCondition(String incrementalCondition) {
			this.incrementalCondition = incrementalCondition;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public boolean isShowAll() {
			return showAll;
		}
		public RequestParam setShowAll(boolean showAll) {
			this.showAll = showAll;
			return this;
		}
	}
	/**
	 * Search Condition
	 */
	public class SearchCondition {
		private boolean selectedLotInProgress = true;
		private boolean selectedLotClosed = false;
		private Integer selectedPageNo = 1;

		public boolean isSelectedLotInProgress() {
			if ( ! selectedLotInProgress && ! selectedLotClosed ) {
				return true;
			}
			return selectedLotInProgress;
		}
		public SearchCondition setSelectedLotInProgress(boolean selectedLotInProgress) {
			this.selectedLotInProgress = selectedLotInProgress;
			return this;
		}
		public boolean isSelectedLotClosed() {
			return selectedLotClosed;
		}
		public SearchCondition setSelectedLotClosed(boolean selectedLotClosed) {
			this.selectedLotClosed = selectedLotClosed;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

	}

}
