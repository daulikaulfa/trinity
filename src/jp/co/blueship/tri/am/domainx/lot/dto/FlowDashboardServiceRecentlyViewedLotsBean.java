package jp.co.blueship.tri.am.domainx.lot.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.IDashboardLotViewBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceRecentlyViewedLotsBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private IPageNoInfo page = new PageNoInfo();

	private List<IDashboardLotViewBean> lotViews = new ArrayList<IDashboardLotViewBean>();

	public RequestParam getParam() {
		return param;
	}

	public List<IDashboardLotViewBean> getLotViews() {
		return lotViews;
	}
	public FlowDashboardServiceRecentlyViewedLotsBean setLotViews( List<IDashboardLotViewBean> lotViews ) {
		this.lotViews = lotViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowDashboardServiceRecentlyViewedLotsBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam {
		private String incrementalCondition = null;
		private final int linesPerPage = 6;
		private Integer selectedPageNo = 1;

		public String getIncrementalCondition() {
			return incrementalCondition;
		}
		public RequestParam setIncrementalCondition(String incrementalCondition) {
			this.incrementalCondition = incrementalCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public RequestParam setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
	}

}
