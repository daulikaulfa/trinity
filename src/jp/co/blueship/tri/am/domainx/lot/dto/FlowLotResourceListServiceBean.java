package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.ILotResourceViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceFolderView;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public class FlowLotResourceListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private LotResourceDetailsView detailsView = new LotResourceDetailsView();
	private ResourceFolderView<ILotResourceViewBean> resourceFolderView = new ResourceFolderView<ILotResourceViewBean>();

	public RequestParam getParam(){
		return this.param;
	}

	public LotResourceDetailsView getLotResourceDetailsView(){
		return this.detailsView;
	}
	public FlowLotResourceListServiceBean setLotResourceDetailsView( LotResourceDetailsView detailsView ){
		this.detailsView = detailsView;
		return this;
	}

	public ResourceFolderView<ILotResourceViewBean> getResourceFolderView(){
		return this.resourceFolderView;
	}
	public FlowLotResourceListServiceBean setResourceFolderView( ResourceFolderView<ILotResourceViewBean> resourceFolderView ){
		this.resourceFolderView = resourceFolderView;
		return this;
	}


	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private RequestOption requestOption = RequestOption.none;
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedLotId(){
			return this.lotId;
		}
		public RequestParam setSelectedLotId( String lotId ){
			this.lotId = lotId;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}
		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}
	}

	public class LotResourceDetailsView {
		private String size;
		private int folders;
		private int files;

		public String getSize(){
			return this.size;
		}
		public LotResourceDetailsView setSize( String size ){
			this.size = size;
			return this;
		}

		public int getFolders(){
			return this.folders;
		}
		public LotResourceDetailsView setFolders( int folders ){
			this.folders = folders;
			return this;
		}

		public int getFiles(){
			return this.files;
		}
		public LotResourceDetailsView setFiles( int files ){
			this.files = files;
			return this;
		}
	}

	public enum RequestOption {
		none (""),
		selectResource ("selectResource"),
		refreshResource ("refreshResource"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
