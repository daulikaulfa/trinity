package jp.co.blueship.tri.am.domainx.lot.beans.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Tang vu
 */
public class LotDetailsViewBean {
	private boolean closed = false;
	private String lotId = null;
	private String lotSubject = null;
	private String lotSummary = null;
	private String lotContents = null;
	private String stsId = null;
	private String status = null;
	private boolean useMerge = true;
	private String createdTime = null;
	private String latestBaselineTag = null;
	private String latestVerTag = null;
	private String branchBaselineTag = null;
	private String publicPath = null;
	private String privatePath = null;
	private String themeColor = null;
	private String lotIconPath = null;
	private List<AuthorizedGroup> authorizedGroups = new ArrayList<AuthorizedGroup>();
	private boolean enableUnauthorizedGroup = false;
	private List<MailRecipientGroup> mailRecipientGroups = new ArrayList<MailRecipientGroup>();
	private List<Module> modules = new ArrayList<Module>();
	private List<BuildEnvironment> buildEnvs = new ArrayList<BuildEnvironment>();
	private List<ReleaseEnvironment> releaseEnvs = new ArrayList<ReleaseEnvironment>();

	public boolean isClosed() {
		return closed;
	}
	public LotDetailsViewBean setClosed(boolean closed) {
		this.closed = closed;
		return this;
	}

	public String getLotId() {
		return lotId;
	}
	public LotDetailsViewBean setLotId(String lotId) {
		this.lotId = lotId;
		return this;
	}

	public String getLotSubject() {
		return lotSubject;
	}
	public LotDetailsViewBean setLotSubject(String lotSubject) {
		this.lotSubject = lotSubject;
		return this;
	}

	public String getLotSummary() {
		return lotSummary;
	}
	public LotDetailsViewBean setLotSummary(String lotSummary) {
		this.lotSummary = lotSummary;
		return this;
	}

	public String getLotContents() {
		return lotContents;
	}
	public LotDetailsViewBean setLotContents(String lotContents) {
		this.lotContents = lotContents;
		return this;
	}

	public String getStsId() {
		return stsId;
	}
	public LotDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}

	public String getStatus() {
		return status;
	}
	public LotDetailsViewBean setStatus(String status) {
		this.status = status;
		return this;
	}

	public boolean isUseMerge() {
		return useMerge;
	}
	public LotDetailsViewBean setUseMerge(boolean useMerge) {
		this.useMerge = useMerge;
		return this;
	}

	public String getCreatedTime() {
		return createdTime;
	}
	public LotDetailsViewBean setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
		return this;
	}

	public String getLatestBaselineTag() {
		return latestBaselineTag;
	}
	public LotDetailsViewBean setLatestBaselineTag(String latestBaselineTag) {
		this.latestBaselineTag = latestBaselineTag;
		return this;
	}

	public String getLatestVerTag() {
		return latestVerTag;
	}
	public LotDetailsViewBean setLatestVerTag(String latestVerTag) {
		this.latestVerTag = latestVerTag;
		return this;
	}

	public String getBranchBaselineTag() {
		return branchBaselineTag;
	}
	public LotDetailsViewBean setBranchBaselineTag(String branchBaselineTag) {
		this.branchBaselineTag = branchBaselineTag;
		return this;
	}

	public String getPublicPath() {
		return publicPath;
	}
	public LotDetailsViewBean setPublicPath(String publicPath) {
		this.publicPath = publicPath;
		return this;
	}

	public String getPrivatePath() {
		return privatePath;
	}
	public LotDetailsViewBean setPrivatePath(String privatePath) {
		this.privatePath = privatePath;
		return this;
	}

	public String getThemeColor() {
		return themeColor;
	}
	public LotDetailsViewBean setThemeColor(String themeColor) {
		this.themeColor = themeColor;
		return this;
	}

	public String getLotIconPath() {
		return lotIconPath;
	}
	public LotDetailsViewBean setLotIconPath(String lotIconPath) {
		this.lotIconPath = lotIconPath;
		return this;
	}

	public List<AuthorizedGroup> getAuthorizedGroups() {
		return authorizedGroups;
	}
	public LotDetailsViewBean setAuthorizedGroups(List<AuthorizedGroup> authorizedGroups) {
		this.authorizedGroups = authorizedGroups;
		return this;
	}

	public boolean isEnableUnauthorizedGroup() {
		return enableUnauthorizedGroup;
	}
	public LotDetailsViewBean setEnableUnauthorizedGroup(boolean enableUnauthorizedGroup) {
		this.enableUnauthorizedGroup = enableUnauthorizedGroup;
		return this;
	}

	public List<MailRecipientGroup> getMailRecipientGroups() {
		return mailRecipientGroups;
	}
	public LotDetailsViewBean setMailRecipientGroups(List<MailRecipientGroup> mailRecipientGroups) {
		this.mailRecipientGroups = mailRecipientGroups;
		return this;
	}

	public List<Module> getModules() {
		return modules;
	}
	public LotDetailsViewBean setModules(List<Module> modules) {
		this.modules = modules;
		return this;
	}

	public List<BuildEnvironment> getBuildEnvs() {
		return buildEnvs;
	}
	public LotDetailsViewBean setBuildEnvs(List<BuildEnvironment> buildEnvs) {
		this.buildEnvs = buildEnvs;
		return this;
	}

	public List<ReleaseEnvironment> getReleaseEnvs() {
		return releaseEnvs;
	}
	public LotDetailsViewBean setReleaseEnvs(List<ReleaseEnvironment> releaseEnvs) {
		this.releaseEnvs = releaseEnvs;
		return this;
	}

	/**
	 * Authorized Group
	 */
	public class AuthorizedGroup {
		private String groupNm;

		public String getGroupNm() {
			return groupNm;
		}
		public AuthorizedGroup setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
	}

	/**
	 * E-mail Recipient Group
	 */
	public class MailRecipientGroup {
		private String groupNm;

		public String getGroupNm() {
			return groupNm;
		}
		public MailRecipientGroup setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
	}

	/**
	 * Module
	 */
	public class Module {
		private String moduleNm;
		private String repository;

		public String getModuleNm() {
			return moduleNm;
		}
		public Module setModuleNm(String moduleNm) {
			this.moduleNm = moduleNm;
			return this;
		}

		public String getRepository() {
			return repository;
		}
		public Module setRepository(String repository) {
			this.repository = repository;
			return this;
		}
	}

	/**
	 * Build Environment
	 */
	public class BuildEnvironment {
		private boolean selectedBuild = false;
		private boolean selectedFullBuild = false;
		private String envId;
		private String environmentNm;
		private String summary;
		private String statusId;
		private String updTime = null;

		public boolean isSelectedBuild() {
			return selectedBuild;
		}
		public BuildEnvironment setSelectedBuild(boolean selectedBuild) {
			this.selectedBuild = selectedBuild;
			return this;
		}

		public boolean isSelectedFullBuild() {
			return selectedFullBuild;
		}
		public BuildEnvironment setSelectedFullBuild(boolean selectedFullBuild) {
			this.selectedFullBuild = selectedFullBuild;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public BuildEnvironment setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}
		public BuildEnvironment setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public BuildEnvironment setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getStatusId() {
			return statusId;
		}
		public BuildEnvironment setStatusId(String statusId) {
			this.statusId = statusId;
			return this;
		}

		public String getUpdTime() {
			return updTime;
		}
		public BuildEnvironment setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
	}

	/**
	 * Release Environment
	 */
	public class ReleaseEnvironment {
		private String envId;
		private String environmentNm;
		private String summary;
		private String updTime = null;

		public String getEnvId() {
			return envId;
		}
		public ReleaseEnvironment setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}
		public ReleaseEnvironment setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public ReleaseEnvironment setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getUpdTime() {
			return updTime;
		}
		public ReleaseEnvironment setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
	}
}
