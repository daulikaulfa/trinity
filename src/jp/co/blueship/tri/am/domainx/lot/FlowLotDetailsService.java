package jp.co.blueship.tri.am.domainx.lot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean.Module;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotDetailsServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;

/**
 * Provide the following backend services.
 * <br> - Details
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotDetailsService implements IDomain<FlowLotDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

    private FlowChaLibLotEditSupport support = null;

	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowLotDetailsServiceBean> execute( IServiceDto<FlowLotDetailsServiceBean> serviceDto ) {

		FlowLotDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try {
			paramBean = serviceDto.getServiceBean();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {

				String selectedLotNo = paramBean.getParam().getSelectedLotId();
				AmItemChkUtils.checkLotNo( selectedLotNo );

				ILotDto lotDto = this.support.findLotDto( selectedLotNo );

				List<IMessageId> messageList = new ArrayList<IMessageId>();
				List<String[]> messageArgsList = new ArrayList<String[]>();

				AmLibraryAddonUtils.checkAccessableGroup(
						lotDto,
						this.support.getUmFinderSupport().getGrpDao(),
						support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()),
						messageList,
						messageArgsList
						);

				AmLibraryAddonUtils.checkSpecifiedGroup(
						lotDto,
						this.support.getUmFinderSupport().getGrpDao(),
						messageList,
						messageArgsList
						);

				if ( 0 < messageList.size() ) {
					paramBean.setInfoMessageIdList( messageList );
					paramBean.setInfoMessageArgsList( messageArgsList );
				}

				LotDetailViewBean lotDetailViewBean = new LotDetailViewBean();
				AmViewInfoAddonUtils.setLotDetailViewBeanPjtLotEntity(paramBean,lotDetailViewBean, lotDto, null, null );
				//display all groups when user select no group
				if (TriStringUtils.isEmpty(lotDetailViewBean.getGroupName())) {
					List<String> groupNames = AmLibraryAddonUtils.getAllGroupName(this.support.getUmFinderSupport().getGrpDao());
					lotDetailViewBean.setGroupName(groupNames.toArray(new String[0]));
				}
				paramBean.setDetailsView(
						this.getLotDetails(lotDetailViewBean,
						lotDto,
						paramBean) );
				{
					IJdbcCondition successCountCondition =
							AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition( lotDto.getLotEntity().getLotId() );
					IJdbcCondition failureCountCondition =
							AmDBSearchConditionAddonUtils.getFailedRelUnitCondition( lotDto.getLotEntity().getLotId() );

					paramBean.getBuildResult()
						.setSuccessful( this.support.getBmFinderSupport().getBpDao().count( successCountCondition.getCondition() ) )
						.setFailed( this.support.getBmFinderSupport().getBpDao().count( failureCountCondition.getCondition() ) );
				}

				support.getUmFinderSupport().updateAccsHist(paramBean, AmTables.AM_LOT, selectedLotNo);

			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * @param src the bean to copy.
	 * @param lotDto
	 * @return the bean to copy to.
	 */
	private LotDetailsViewBean getLotDetails( LotDetailViewBean src, ILotDto lotDto, FlowLotDetailsServiceBean paramBean) {

		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		LotDetailsViewBean dest = new LotDetailsViewBean()
			.setClosed( AmLotStatusId.LotClosed.getStatusId().equals(lotDto.getLotEntity().getStsId()) )
			.setLotId( src.getLotNo() )
			.setLotSubject( src.getLotName() )
			.setLotSummary( src.getLotSummary() )
			.setLotContents( src.getLotContent() )
			.setStsId( lotDto.getLotEntity().getProcStsId() )
			.setStatus( sheet.getValue(AmDesignBeanId.statusId, lotDto.getLotEntity().getProcStsId()) )
			.setUseMerge( src.isUseMerge() )
			.setCreatedTime( src.getInputDate() )
			.setBranchBaselineTag( src.getBranchBaselineTag() )
			.setLatestBaselineTag( src.getRecentTag() )
			.setLatestVerTag( src.getRecentVersion() )
			.setPublicPath( src.getPublicFolder() )
			.setPrivatePath( src.getPrivateFolder() )
			.setEnableUnauthorizedGroup( src.isAllowListView() )
			.setThemeColor(src.getThemeColor())
			.setLotIconPath(UmDesignBusinessRuleUtils.getLotIconSharePath(lotDto.getLotEntity()));

		{
			for (String value: FluentList.from(src.getGroupName()).asList()) {
				dest.getAuthorizedGroups().add(
						dest.new AuthorizedGroup().setGroupNm(value));
			}
		}
		{
			for (String value: FluentList.from(src.getSpecifiedGroupName()).asList()) {
				dest.getMailRecipientGroups().add(
						dest.new MailRecipientGroup().setGroupNm(value));
			}
		}
		dest.setModules( this.getModules( dest, src.getModuleName() ) );
		{
			IBldEnvEntity bldEnv = support.getBmFinderSupport().findBldEnvEntity( lotDto.getLotEntity().getBldEnvId() );
			dest.getBuildEnvs().add( dest.new BuildEnvironment()
				.setSelectedBuild(true).setSelectedFullBuild(false)
				.setEnvId( bldEnv.getBldEnvId() )
				.setEnvironmentNm( bldEnv.getBldEnvNm() )
				.setSummary( bldEnv.getRemarks() )
				.setUpdTime(TriDateUtils.convertViewDateFormat(bldEnv.getUpdTimestamp(), formatYMDHM).toString())
			);

			IBldEnvEntity fullBldEnv = support.getBmFinderSupport().findBldEnvEntity( lotDto.getLotEntity().getFullBldEnvId() );
			dest.getBuildEnvs().add( dest.new BuildEnvironment()
				.setSelectedBuild(false).setSelectedFullBuild(true)
				.setEnvId(fullBldEnv.getBldEnvId())
				.setEnvironmentNm( fullBldEnv.getBldEnvNm() )
				.setSummary( fullBldEnv.getRemarks() )
				.setUpdTime(TriDateUtils.convertViewDateFormat(fullBldEnv.getUpdTimestamp(), formatYMDHM).toString())
			);
		}
		{
			String[] bldEnvIds = FluentList.from( lotDto.getLotRelEnvLnkEntities() ).map( AmFluentFunctionUtils.toBldEnvIdsFromLotRelEnvLnkEntity).toArray(new String[0]);
			IBldEnvEntity[] relEnvViewBeanList = this.support.getRelEnvEntityArray( bldEnvIds );

			for( IBldEnvEntity bldEnv : relEnvViewBeanList ){
				dest.getReleaseEnvs().add( dest.new ReleaseEnvironment()
						.setEnvId( bldEnv.getBldEnvId() )
						.setEnvironmentNm( bldEnv.getBldEnvNm() )
						.setSummary( bldEnv.getRemarks() )
						.setUpdTime(TriDateUtils.convertViewDateFormat(bldEnv.getUpdTimestamp(), formatYMDHM).toString())
					);
			}
		}

		return dest;
	}

	/**
	 * @param view
	 * @param moduleNames
	 * @return
	 */
	private List<Module> getModules( LotDetailsViewBean view, String[] moduleNames ) {
		List<Module> modules = new ArrayList<Module>();

		List<IVcsReposDto> reposDtoList = this.support.findVcsReposDto();

		ComparableSet<ModuleViewBean> moduleViewBeanSet =
				new ComparableSet<ModuleViewBean>(new Comparator<ModuleViewBean>() {
					@Override
					public int compare(ModuleViewBean dto1, ModuleViewBean dto2) {
						if (dto1.getModuleName().equals(dto2.getModuleName())) {
							return 0;
						}
						return -1;}
				});
		TriCollectionUtils.collect(//
				reposDtoList,//
				new TriFunction<IVcsReposDto, ModuleViewBean>() {
					@Override
					public ModuleViewBean apply(IVcsReposDto reposDto) {

						ModuleViewBean module = new ModuleViewBean();
						module.setModuleName(reposDto.getVcsMdlEntity().getMdlNm());
						module.setRepository(reposDto.getVcsReposEntity().getVcsReposUrl());
						return module;
					}
				},
				moduleViewBeanSet);

		Set<String> moduleSet = new HashSet<String>( FluentList.from(moduleNames).asList() );
		for ( ModuleViewBean moduleBean: moduleViewBeanSet ) {
			if ( ! moduleSet.contains(moduleBean.getModuleName()) ) {
				continue;
			}
			modules.add( view.new Module()
				.setModuleNm(moduleBean.getModuleName())
				.setRepository(moduleBean.getRepository())
			);
		}

		return modules;
	}

}
