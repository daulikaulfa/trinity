package jp.co.blueship.tri.am.domainx.lot.dto;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowLotCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibLotEntryServiceBean() );
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowLotCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private LotEditInputBean inputInfo = new LotEditInputBean();

		public LotEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(LotEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private String lotId;

		public String getLotId() {
			return lotId;
		}

		public RequestsCompletion setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
