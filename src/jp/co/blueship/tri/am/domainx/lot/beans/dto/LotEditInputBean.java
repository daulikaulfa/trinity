package jp.co.blueship.tri.am.domainx.lot.beans.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LotEditInputBean {
	private String lotSubject;
	private String lotSummary;
	private String lotContents;
	private String baselineTag;
	private boolean useMerge = true;
	private String publicPath;
	private String privatePath;
	private boolean useDefaultPublicPath = true;
	private String defaultPublicPath;
	private boolean useDefaultPrivatePath = true;
	private String defaultPrivatePath;
	private List<AuthorizedGroup> authorizedGroups = new ArrayList<AuthorizedGroup>();
	private boolean enableUnauthorizedGroup = false;
	private List<MailRecipientGroup> mailRecipientGroups = new ArrayList<MailRecipientGroup>();
	private List<Module> modules = new ArrayList<Module>();
	private List<BuildEnvironment> buildEnvs = new ArrayList<BuildEnvironment>();
	private List<ReleaseEnvironment> releaseEnvs = new ArrayList<ReleaseEnvironment>();

	public String getLotSubject() {
		return lotSubject;
	}

	public LotEditInputBean setLotSubject(String lotSubject) {
		this.lotSubject = lotSubject;
		return this;
	}

	public String getLotSummary() {
		return lotSummary;
	}

	public LotEditInputBean setLotSummary(String lotSummary) {
		this.lotSummary = lotSummary;
		return this;
	}

	public String getLotContents() {
		return lotContents;
	}

	public LotEditInputBean setLotContents(String lotContents) {
		this.lotContents = lotContents;
		return this;
	}

	public String getBaselineTag() {
		return baselineTag;
	}

	public LotEditInputBean setBaselineTag(String baselineTag) {
		this.baselineTag = baselineTag;
		return this;
	}

	public boolean isUseMerge() {
		return useMerge;
	}

	public LotEditInputBean setUseMerge(boolean useMerge) {
		this.useMerge = useMerge;
		return this;
	}

	public String getPublicPath() {
		return publicPath;
	}

	public LotEditInputBean setPublicPath(String publicPath) {
		this.publicPath = publicPath;
		return this;
	}

	public String getPrivatePath() {
		return privatePath;
	}

	public LotEditInputBean setPrivatePath(String privatePath) {
		this.privatePath = privatePath;
		return this;
	}

	public String getDefaultPublicPath() {
		return defaultPublicPath;
	}

	public LotEditInputBean setDefaultPublicPath(String defaultPublicPath) {
		this.defaultPublicPath = defaultPublicPath;
		return this;
	}

	public boolean isUseDefaultPrivatePath() {
		return useDefaultPrivatePath;
	}

	public LotEditInputBean setUseDefaultPrivatePath(boolean useDefaultPrivatePath) {
		this.useDefaultPrivatePath = useDefaultPrivatePath;
		return this;
	}

	public String getDefaultPrivatePath() {
		return defaultPrivatePath;
	}

	public LotEditInputBean setDefaultPrivatePath(String defaultPrivatePath) {
		this.defaultPrivatePath = defaultPrivatePath;
		return this;
	}

	public boolean isUseDefaultPublicPath() {
		return useDefaultPublicPath;
	}

	public LotEditInputBean setUseDefaultPublicPath(boolean useDefaultPublicPath) {
		this.useDefaultPublicPath = useDefaultPublicPath;
		return this;
	}

	public List<AuthorizedGroup> getAuthorizedGroups() {
		return authorizedGroups;
	}

	public LotEditInputBean setAuthorizedGroups(List<AuthorizedGroup> authorizedGroups) {
		this.authorizedGroups = authorizedGroups;
		return this;
	}

	public boolean isEnableUnauthorizedGroup() {
		return enableUnauthorizedGroup;
	}

	public LotEditInputBean setEnableUnauthorizedGroup(boolean enableUnauthorizedGroup) {
		this.enableUnauthorizedGroup = enableUnauthorizedGroup;
		return this;
	}

	public List<MailRecipientGroup> getMailRecipientGroups() {
		return mailRecipientGroups;
	}

	public LotEditInputBean setMailRecipientGroups(List<MailRecipientGroup> mailRecipientGroups) {
		this.mailRecipientGroups = mailRecipientGroups;
		return this;
	}

	public List<Module> getModules() {
		return modules;
	}

	public LotEditInputBean setModules(List<Module> modules) {
		this.modules = modules;
		return this;
	}

	public List<BuildEnvironment> getBuildEnvs() {
		return buildEnvs;
	}

	public LotEditInputBean setBuildEnvs(List<BuildEnvironment> buildEnvs) {
		this.buildEnvs = buildEnvs;
		return this;
	}

	public List<ReleaseEnvironment> getReleaseEnvs() {
		return releaseEnvs;
	}

	public LotEditInputBean setReleaseEnvs(List<ReleaseEnvironment> releaseEnvs) {
		this.releaseEnvs = releaseEnvs;
		return this;
	}

	/**
	 * Authorized Group
	 */
	public class AuthorizedGroup {
		private boolean selected = false;
		private String groupId;
		private String groupNm;

		public boolean isSelected() {
			return selected;
		}

		public AuthorizedGroup setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		public String getGroupId() {
			return groupId;
		}

		public AuthorizedGroup setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		public String getGroupNm() {
			return groupNm;
		}

		public AuthorizedGroup setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
	}

	/**
	 * E-mail Recipient Group
	 */
	public class MailRecipientGroup {
		private boolean selected = false;
		private String groupId;
		private String groupNm;

		public boolean isSelected() {
			return selected;
		}

		public MailRecipientGroup setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		public String getGroupId() {
			return groupId;
		}

		public MailRecipientGroup setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		public String getGroupNm() {
			return groupNm;
		}

		public MailRecipientGroup setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
	}

	/**
	 * Module
	 */
	public class Module {
		private boolean selected = false;
		private String moduleNm;
		private String repository;

		public boolean isSelected() {
			return selected;
		}

		public Module setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		public String getModuleNm() {
			return moduleNm;
		}

		public Module setModuleNm(String moduleNm) {
			this.moduleNm = moduleNm;
			return this;
		}

		public String getRepository() {
			return repository;
		}

		public Module setRepository(String repository) {
			this.repository = repository;
			return this;
		}
	}

	/**
	 * Build Environment
	 */
	public class BuildEnvironment {
		private boolean selectedBuild = false;
		private boolean selectedFullBuild = false;
		private String envId;
		private String environmentNm;
		private String summary;
		private String stsId;
		private String updTime = null;

		public boolean isSelectedBuild() {
			return selectedBuild;
		}

		public BuildEnvironment setSelectedBuild(boolean selectedBuild) {
			this.selectedBuild = selectedBuild;
			return this;
		}

		public boolean isSelectedFullBuild() {
			return selectedFullBuild;
		}

		public BuildEnvironment setSelectedFullBuild(boolean selectedFullBuild) {
			this.selectedFullBuild = selectedFullBuild;
			return this;
		}

		public String getEnvId() {
			return envId;
		}

		public BuildEnvironment setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}

		public BuildEnvironment setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getSummary() {
			return summary;
		}

		public BuildEnvironment setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getStsId() {
			return stsId;
		}

		public BuildEnvironment setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getUpdTime() {
			return updTime;
		}

		public BuildEnvironment setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
	}

	/**
	 * Release Environment
	 */
	public class ReleaseEnvironment {
		private boolean selected = false;
		private String envId;
		private String environmentNm;
		private String summary;
		private String stsId;
		private String updTime = null;

		public boolean isSelected() {
			return selected;
		}

		public ReleaseEnvironment setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		public String getEnvId() {
			return envId;
		}

		public ReleaseEnvironment setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return environmentNm;
		}

		public ReleaseEnvironment setEnvironmentNm(String environmentNm) {
			this.environmentNm = environmentNm;
			return this;
		}

		public String getSummary() {
			return summary;
		}

		public ReleaseEnvironment setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getStsId() {
			return stsId;
		}

		public ReleaseEnvironment setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}


		public String getUpdTime() {
			return updTime;
		}

		public ReleaseEnvironment setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
	}

}
