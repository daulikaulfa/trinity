package jp.co.blueship.tri.am.domainx.lot.beans.dto;

/**
 * Build Package Creation Result View
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class BuildPackageCreationResultViewBean {
	private int Successful;
	private int Failed;

	public int getSuccessful() {
		return Successful;
	}
	public BuildPackageCreationResultViewBean setSuccessful(int successful) {
		Successful = successful;
		return this;
	}
	public int getFailed() {
		return Failed;
	}
	public BuildPackageCreationResultViewBean setFailed(int failed) {
		Failed = failed;
		return this;
	}

}
