package jp.co.blueship.tri.am.domainx.lot.beans.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;

/**
 *
 * @author Akahoshi
 * @version V4.01.00
 */
public class ResourceFolderView<E extends IResourceViewBean> {
	private ITreeFolderViewBean folderView = new TreeFolderViewBean().setName("");
	private String selectedPath = null;
	private List<E> requestViews = new ArrayList<E>();

	public String getSelectedPath() {
		return selectedPath;
	}
	public ResourceFolderView<E> setSelectedPath(String selectedPath) {
		this.selectedPath = selectedPath;
		return this;
	}

	public ITreeFolderViewBean getFolderView() {
		return folderView;
	}
	public ResourceFolderView<E> setFolderView(ITreeFolderViewBean folderView) {
		this.folderView = folderView;
		return this;
	}

	public List<E> getRequestViews() {
		return requestViews;
	}
	public ResourceFolderView<E> setRequestViews(List<E> requestViews) {
		this.requestViews = requestViews;
		return this;
	}

	public String toJsonFromFolderView() {
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(folderView);
	}
}
