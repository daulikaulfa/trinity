package jp.co.blueship.tri.am.domainx.lot;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceFileHistoryDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceFileHistoryDetailsServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceFileHistoryDetailsServiceBean.LotResourceFileHistoryDetailsView;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */

public class FlowLotResourceFileHistoryDetailsService implements IDomain<FlowLotResourceFileHistoryDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	
	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}
	
	@Override
	public IServiceDto<FlowLotResourceFileHistoryDetailsServiceBean> execute(
			IServiceDto<FlowLotResourceFileHistoryDetailsServiceBean> serviceDto) {
		FlowLotResourceFileHistoryDetailsServiceBean paramBean = serviceDto.getServiceBean();
		try {
			paramBean = serviceDto.getServiceBean();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowLotResourceFileHistoryDetailsServiceBean paramBean) throws IOException {
		
		String relativePath = paramBean.getParam().getSelectedResourcePath();
		LotResourceFileHistoryDetailsView detailsView = paramBean.getDetailsView();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		
		ILotEntity lotEntity = this.support.findLotEntity( paramBean.getParam().getSelectedLotId() );
		
		this.setBasicAttribute( lotEntity, detailsView, relativePath, formatYMDHM);
		
		List<IAreqEntity> areqEntities = this.getAreqList( paramBean.getParam().getSelectedLotId() );
		
		for ( IAreqEntity areqEntity : areqEntities ) {
			
			IAreqDto areqDto = this.support.findAreqDto(areqEntity, new AmTables[]{AmTables.AM_AREQ_FILE, AmTables.AM_AREQ_BINARY_FILE});
			Set<String> requestPath = AmResourceSelectionUtils.getRequestFilePathSet( areqDto );
			
			if ( AmBusinessJudgUtils.isResourceLock( areqEntity.getStsId() ) ) {
				if ( requestPath.contains( relativePath )) {
					detailsView.setLocked(true)
							.setAreqType( AreqCtgCd.value(areqEntity.getAreqCtgCd()) )
							;
					if ( AreqCtgCd.RemovalRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
						detailsView.setSubmitterId( areqEntity.getDelReqUserId() )
								.setSubmitterNm( areqEntity.getDelReqUserNm() )
								.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getDelReqUserId() ) )
								.setLockedDate( TriDateUtils.convertViewDateFormat(areqEntity.getDelReqTimestamp(),formatYMD) )
								;
					} else if ( AreqCtgCd.LendingRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
						detailsView.setSubmitterId( areqEntity.getLendReqUserId() )
								.setSubmitterNm( areqEntity.getLendReqUserNm() )
								.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getLendReqUserId() ) )
								.setLockedDate( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(),formatYMD) )
								;
					} else if ( AreqCtgCd.ReturningRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
						detailsView.setSubmitterId( areqEntity.getRtnReqUserId() )
								.setSubmitterNm( areqEntity.getRtnReqUserNm() )
								.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getRtnReqUserId() ) )
								.setLockedDate( ( TriDateUtils.convertViewDateFormat(areqEntity.getRtnReqTimestamp(),formatYMD) ) )
								;
					}
				}
			}
			
			if ( requestPath.contains( relativePath )) {
				IPjtEntity pjtEntity = this.support.findPjtEntity( areqEntity.getPjtId() );
				
				AreqView areqView = new FlowLotResourceFileHistoryDetailsServiceBean().new AreqView();
				areqView.setAreqType( AreqCtgCd.value(areqEntity.getAreqCtgCd()) )
						.setPjtId( pjtEntity.getPjtId() )
						.setReferenceId( pjtEntity.getChgFactorNo() );

				if ( AreqCtgCd.RemovalRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
					areqView.setSubmitterNm( areqEntity.getDelReqUserNm() )
							.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getDelReqUserId() ) )
							;
				} else if ( AreqCtgCd.LendingRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
					areqView.setSubmitterNm( areqEntity.getLendReqUserNm() )
							.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getLendReqUserId() ) )
							;
				} else if ( AreqCtgCd.ReturningRequest.value().equals(areqEntity.getAreqCtgCd()) ) {
					areqView.setSubmitterNm( areqEntity.getRtnReqUserNm() )
							.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getRtnReqUserId() ) )
							;
				}
				areqView
					.setAreqId( areqEntity.getAreqId() )
					.setAssigneeNm( areqEntity.getAssigneeNm() )
					.setAssigneeIconPath( this.support.getUmFinderSupport().getIconPath( areqEntity.getAssigneeId() ) )
					.setApprovedDate( TriDateUtils.convertViewDateFormat( areqEntity.getPjtAvlTimestamp(),formatYMD ) )
					.setRequestDate( TriDateUtils.convertViewDateFormat( areqEntity.getReqTimestamp(),formatYMD ) )
					;
				paramBean.getAreqViews().add( areqView );
			}	
		}
	}
	
	private List<IAreqEntity> getAreqList(String lotId) {
		AreqCondition condition = new AreqCondition();
		
		String[] areqStsIds ={ AmAreqStatusId.CheckoutRequested.getStatusId(),
				   AmAreqStatusId.CheckinRequested.getStatusId(), AmAreqStatusId.CheckinRequestApproved.getStatusId(),
				   AmAreqStatusId.RemovalRequested.getStatusId(), AmAreqStatusId.RemovalRequestApproved.getStatusId(),
				   AmAreqStatusId.Merged.getStatusId(),
				   AmAreqStatusId.BuildPackageClosed.getStatusId()};
		condition.setStsIds		( areqStsIds );
		condition.setLotId( lotId );
		
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.updTimestamp, TriSortOrder.Desc, 1);
		List<IAreqEntity> areqEntityList = this.support.getAreqDao().find(condition.getCondition(), sort);
		return areqEntityList;
	}

	private void setBasicAttribute(ILotEntity lotEntity, LotResourceFileHistoryDetailsView detailsView, String relativePath, SimpleDateFormat formatYMDHM) throws IOException {
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );	
		
		File currentFile = new File( masterPath.getAbsolutePath() + File.separator + relativePath );
		BasicFileAttributes attr = Files.readAttributes(currentFile.toPath(), BasicFileAttributes.class);
		
		detailsView.setExtension( FilenameUtils.getExtension( currentFile.getName() ) );
		detailsView.setCreatedTime( formatYMDHM.format( attr.creationTime().toMillis() ) );
		detailsView.setLatestUpdTime( formatYMDHM.format( attr.lastModifiedTime().toMillis() ) );
		detailsView.setSize( TriFileUtils.fileSizeOf( new Long(currentFile.length() ) ) );
		detailsView.setName( relativePath );
	}
}
