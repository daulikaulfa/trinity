package jp.co.blueship.tri.am.domainx.lot.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public class FlowLotResourceFileHistoryDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private LotResourceFileHistoryDetailsView detailsView = new LotResourceFileHistoryDetailsView();
	private List<AreqView> areqViews = new ArrayList<AreqView>();

	public RequestParam getParam(){
		return this.param;
	}

	public LotResourceFileHistoryDetailsView getDetailsView(){
		return this.detailsView;
	}
	public FlowLotResourceFileHistoryDetailsServiceBean setDetailsView( LotResourceFileHistoryDetailsView detailsView ){
		this.detailsView = detailsView;
		return this;
	}

	public List<AreqView> getAreqViews() {
		return areqViews;
	}
	public FlowLotResourceFileHistoryDetailsServiceBean setAreqViews(List<AreqView> areqViews) {
		this.areqViews = areqViews;
		return this;
	}


	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private String resourcePath = null;

		public String getSelectedLotId(){
			return this.lotId;
		}
		public RequestParam setSelectedLotId( String lotId ){
			this.lotId = lotId;
			return this;
		}

		public String getSelectedResourcePath() {
			return resourcePath;
		}
		public RequestParam setSelectedResourcePath(String selectedResourcePath) {
			this.resourcePath = selectedResourcePath;
			return this;
		}
	}

	public class LotResourceFileHistoryDetailsView {
		private String name;
		private String extension;
		private String size;
		private String createdTime;
		private String latestUpdTime;
		private boolean locked = false;
		private String lockedDate;
		private AreqCtgCd areqType;
		private String submitterId;
		private String submitterNm;
		private String submitterIconPath;

		public String getName(){
			return this.name;
		}
		public LotResourceFileHistoryDetailsView setName( String name ){
			this.name = name;
			return this;
		}

		public String getSize(){
			return this.size;
		}
		public LotResourceFileHistoryDetailsView setSize( String size ){
			this.size = size;
			return this;
		}

		public String getLatestUpdTime(){
			return this.latestUpdTime;
		}
		public LotResourceFileHistoryDetailsView setLatestUpdTime( String latestUpdTime ){
			this.latestUpdTime = latestUpdTime;
			return this;
		}

		public String getCreatedTime(){
			return this.createdTime;
		}
		public LotResourceFileHistoryDetailsView setCreatedTime( String createdTime ){
			this.createdTime = createdTime;
			return this;
		}

		public boolean isLocked() {
			return this.locked;
		}
		public LotResourceFileHistoryDetailsView setLocked( boolean locked ) {
			this.locked = locked;
			return this;
		}

		public String getLockedDate() {
			return this.lockedDate;
		}
		public LotResourceFileHistoryDetailsView setLockedDate( String lockedDate ) {
			this.lockedDate = lockedDate;
			return this;
		}

		public String getExtension(){
			return this.extension;
		}
		public LotResourceFileHistoryDetailsView setExtension( String extension ){
			this.extension = extension;
			return this;
		}

		public boolean isCheckoutRequest() {
			return AmBusinessJudgUtils.isLendApply(this.areqType);
		}
		public boolean isCheckinRequest() {
			return AmBusinessJudgUtils.isReturnApply(this.areqType);
		}
		public boolean isRemovalRequest() {
			return AmBusinessJudgUtils.isDeleteApply(this.areqType);
		}
		public LotResourceFileHistoryDetailsView setAreqType( AreqCtgCd areqType ){
			this.areqType = areqType;
			return this;
		}

		public String getSubmitterId() {
			return this.submitterId;
		}
		public LotResourceFileHistoryDetailsView setSubmitterId( String submitterId ) {
			this.submitterId = submitterId;
			return this;
		}

		public String getSubmitterNm() {
			return this.submitterNm;
		}
		public LotResourceFileHistoryDetailsView setSubmitterNm( String submitterNm ) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return this.submitterIconPath;
		}
		public LotResourceFileHistoryDetailsView setSubmitterIconPath( String submitterIconPath ) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
	}

	public class AreqView {
		private String pjtId;
		private String referenceId;
		private AreqCtgCd areqType;
		private String areqId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String requestDate;
		private String approvedDate;
		
		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public AreqView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public boolean isCheckoutRequest() {
			return AmBusinessJudgUtils.isLendApply(this.areqType);
		}
		public boolean isCheckinRequest() {
			return AmBusinessJudgUtils.isReturnApply(this.areqType);
		}
		public boolean isRemovalRequest() {
			return AmBusinessJudgUtils.isDeleteApply(this.areqType);
		}

		public String getAreqId() {
			return areqId;
		}
		public AreqView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public AreqView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public AreqView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public AreqView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public AreqView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getRequestDate() {
			return requestDate;
		}
		public AreqView setRequestDate(String requestDate) {
			this.requestDate = requestDate;
			return this;
		}
		
		public String getApprovedDate(){
			return this.approvedDate;
		}
		public AreqView setApprovedDate( String approvedDate ){
			this.approvedDate = approvedDate;
			return this;
		}
		
		public String getPjtId(){
			return this.pjtId;
		}
		public AreqView setPjtId( String pjtId ){
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId(){
			return this.referenceId;
		}
		public AreqView setReferenceId( String referenceId ){
			this.referenceId = referenceId;
			return this;
		}
		
		public boolean isPending() {
			return TriStringUtils.isEmpty(this.approvedDate);
		}
	}
}
