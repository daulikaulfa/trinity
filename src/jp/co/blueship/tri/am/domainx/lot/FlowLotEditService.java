package jp.co.blueship.tri.am.domainx.lot;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.BuildEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.AuthorizedGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.BuildEnvironment;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.MailRecipientGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.Module;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.ReleaseEnvironment;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotEditServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotEditServiceBean.LotDetailsView;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;

/**
 * Provide the following backend services.
 * <br> - Edit
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class FlowLotEditService implements IDomain<FlowLotEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> moduleSelectionService = null;
	private IDomain<IGeneralServiceBean> buildEnvSelectionService = null;
	private IDomain<IGeneralServiceBean> relEnvSelectionService = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}
	public void setInputService(IDomain<IGeneralServiceBean> service) {
		this.inputService = service;
	}
	public void setModuleSelectionService(IDomain<IGeneralServiceBean> service) {
		this.moduleSelectionService = service;
	}
	public void setBuildEnvSelectionService(IDomain<IGeneralServiceBean> service) {
		this.buildEnvSelectionService = service;
	}
	public void setRelEnvSelectionService(IDomain<IGeneralServiceBean> service) {
		this.relEnvSelectionService = service;
	}
	public void setConfirmationService(IDomain<IGeneralServiceBean> service) {
		this.confirmationService = service;
	}
	public void setCompleteService(IDomain<IGeneralServiceBean> service) {
		this.completeService = service;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowLotEditServiceBean> execute( IServiceDto<FlowLotEditServiceBean> serviceDto ) {

		FlowLotEditServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibLotModifyServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void init( FlowLotEditServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		this.beforeExecution(paramBean, serviceBean);
		serviceBean.setReferer( ChaLibScreenID.LIB_TOP );

		//basic information input
		{
			serviceBean.setForward( ChaLibScreenID.LOT_MODIFY );
			inputService.execute(dto);
		}
		//moduleSelection
		{
			serviceBean.setForward( ChaLibScreenID.LOT_MODULE_SELECT );
			moduleSelectionService.execute(dto);
		}
		//buildEnvSelection
		{
			serviceBean.setForward( ChaLibScreenID.LOT_BUILDENV_SELECT );
			buildEnvSelectionService.execute(dto);
		}
		//relEnvSelection
		{
			serviceBean.setForward( ChaLibScreenID.LOT_REL_ENV_SELECT );
			relEnvSelectionService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowLotEditServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLotModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			String lotId = paramBean.getParam().getSelectedLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( paramBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotId )
					.setHeadBls		( this.getHeadBlIds(lotId) )
					.setPjtIds		( this.getPjtIds( lotId ) )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		//basic information input
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_MODIFY );
			serviceBean.setForward( ChaLibScreenID.LOT_MODULE_SELECT );
			inputService.execute(dto);
		}
		//moduleSelection
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_MODULE_SELECT );
			serviceBean.setForward( ChaLibScreenID.LOT_BUILDENV_SELECT );
			moduleSelectionService.execute(dto);
		}
		//buildEnvSelection
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_BUILDENV_SELECT );
			serviceBean.setForward( ChaLibScreenID.LOT_REL_ENV_SELECT );
			buildEnvSelectionService.execute(dto);
		}
		//relEnvSelection
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_REL_ENV_SELECT );
			serviceBean.setForward( ChaLibScreenID.LOT_MODIFY_CONFIRM );
			relEnvSelectionService.execute(dto);
		}
		//editLotConfirmation
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_MODIFY );
			confirmationService.execute(dto);
		}
		//editLotComplete
		{
			serviceBean.setReferer( ChaLibScreenID.LOT_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LOT_MODIFY );
			completeService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void beforeExecution( FlowLotEditServiceBean src, FlowChaLibLotModifyServiceBean dest ) {
		LotEditInputBean srcInfo = src.getParam().getInputInfo();
		LotEditInputV3Bean destInfo = dest.getLotEditInputBean();

		if ( RequestType.init.equals(src.getParam().getRequestType()) ) {
			if( null == dest.getLotEditInputBean() ) {
				dest.setLotEditInputBean( new LotEditInputV3Bean() );
			}

			dest.setLotNo( src.getParam().getSelectedLotId() );
		}

		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			dest.setProcId( src.getProcId() );

			destInfo.setLotName( srcInfo.getLotSubject() );
			destInfo.setLotSummary( srcInfo.getLotSummary() );
			destInfo.setLotContent( srcInfo.getLotContents() );
			destInfo.setBaseLineTag( srcInfo.getBaselineTag() );
			destInfo.setEditUseMerge( srcInfo.isUseMerge() );
			destInfo.setAllowListView( srcInfo.isEnableUnauthorizedGroup() );

			{	//Authorized Group
				Set<String> selectedIds = new LinkedHashSet<String>();
				for ( AuthorizedGroup grp: srcInfo.getAuthorizedGroups() ) {
					if ( grp.isSelected() ) {
						selectedIds.add(grp.getGroupId());
					}
				}
				List<GroupViewBean> groups = new ArrayList<GroupViewBean>();
				for ( GroupViewBean destView: destInfo.getGroupViewBeanList() ) {
					if ( selectedIds.contains(destView.getGroupId()) ) {
						groups.add(destView);
					}
				}
				destInfo.setSelectedGroupIdString( selectedIds.toArray(new String[0]) );
				destInfo.setSelectedGroupViewBeanList( groups );
			}
			{	//E-mail Recipient Group
				Set<String> selectedIds = new LinkedHashSet<String>();
				for ( MailRecipientGroup grp: srcInfo.getMailRecipientGroups() ) {
					if ( grp.isSelected() ) {
						selectedIds.add(grp.getGroupId());
					}
				}
				List<GroupViewBean> groups = new ArrayList<GroupViewBean>();
				for ( GroupViewBean destView: destInfo.getSpecifiedGroupList() ) {
					if ( selectedIds.contains(destView.getGroupId()) ) {
						groups.add(destView);
					}
				}
				destInfo.setSelectedSpecifiedGroupIdString( selectedIds.toArray(new String[0]) );
				destInfo.setSelectedSpecifiedGroupList( groups );
			}
			{	//Build Environment
				for ( BuildEnvironment env: srcInfo.getBuildEnvs() ) {
					if ( env.isSelectedBuild() ) {
						destInfo.setSelectedBuildEnvNo( env.getEnvId() );
					}
					if ( env.isSelectedFullBuild() ) {
						destInfo.setSelectedFullBuildEnvNo( env.getEnvId() );
					}
				}
				BuildEnvViewBean selectedBuild = null;
				BuildEnvViewBean selectedFullBuild = null;
				for ( BuildEnvViewBean env: destInfo.getBuildEnvViewBeanEnableList() ) {
					if ( env.getEnvNo().equals(destInfo.getSelectedBuildEnvNo()) ) {
						selectedBuild = env;
					}
					if ( env.getEnvNo().equals(destInfo.getSelectedFullBuildEnvNo()) ) {
						selectedFullBuild = env;
					}
				}
				destInfo.setBuildEnvBean( selectedBuild );
				destInfo.setFullBuildEnvBean( selectedFullBuild );
			}
			{	//Release Environment
				Set<String> selectedIds = new LinkedHashSet<String>();
				for ( ReleaseEnvironment env: srcInfo.getReleaseEnvs() ) {
					if ( env.isSelected() ) {
						selectedIds.add(env.getEnvId());
					}
				}
				List<RelEnvViewBean> envs = new ArrayList<RelEnvViewBean>();
				for ( RelEnvViewBean destView: destInfo.getRelEnvViewBeanList() ) {
					if ( selectedIds.contains(destView.getEnvNo()) ) {
						envs.add(destView);
					}
				}
				destInfo.setSelectedRelEnvNoString( selectedIds.toArray(new String[0]) );
				destInfo.setSelectedRelEnvViewBeanList( envs );
			}
		}
	}

	/**
	 * @param src Inner Service Bean
	 * @param dest Service Bean
	 */
	private void afterExecution( FlowChaLibLotModifyServiceBean src, FlowLotEditServiceBean dest ) {
		LotDetailsView lotView = dest.getDetailsView();
		LotEditInputBean destInfo = dest.getParam().getInputInfo();
		LotEditInputV3Bean srcInfo = src.getLotEditInputBean();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( dest.getLanguage(), dest.getTimeZone() );

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {
			destInfo
			.setLotSubject( srcInfo.getLotName() )
			.setLotSummary( srcInfo.getLotSummary() )
			.setLotContents( srcInfo.getLotContent() );

			lotView
			.setLotId(srcInfo.getLotNo())
			.setStsId(srcInfo.getStatusId())
			.setStatus(AmLotStatusId.value(srcInfo.getStatusId()).toString())
			.setCreatedTime(TriDateUtils.convertViewDateFormat(srcInfo.getCreatedTime(), formatYMDHM))
			.setLatestBaselineTag(srcInfo.getBaseLineTag())
			.setBranchBaselineTag(srcInfo.getBranchBaseLineTag())
			.setPublicPath( srcInfo.getDirectoryPathPublic() )
			.setPrivatePath( srcInfo.getDirectoryPathPrivate() )
			.setUseMerge( srcInfo.isEditUseMerge() )
			.setEnableUnauthorizedGroup( srcInfo.isAllowListView() );

			{	//Authorized Group
				List<AuthorizedGroup> groups = new ArrayList<AuthorizedGroup>();
				for ( GroupViewBean srcView: srcInfo.getGroupViewBeanList() ) {
					groups.add( destInfo.new AuthorizedGroup()
						.setSelected( FluentList.from(srcInfo.getSelectedGroupIds()).asList().contains(srcView.getGroupId()) )
						.setGroupId( srcView.getGroupId() )
						.setGroupNm( srcView.getGroupName() )
					);
				}
				destInfo.setAuthorizedGroups( groups );
			}
			{	//E-mail Recipient Group
				List<MailRecipientGroup> groups = new ArrayList<MailRecipientGroup>();
				for ( GroupViewBean srcView: srcInfo.getSpecifiedGroupList() ) {
					groups.add( destInfo.new MailRecipientGroup()
						.setSelected( FluentList.from(srcInfo.getSelectedSpecifiedGroupIds()).asList().contains(srcView.getGroupId()) )
						.setGroupId( srcView.getGroupId() )
						.setGroupNm( srcView.getGroupName() )
					);
				}
				destInfo.setMailRecipientGroups( groups );
			}
			{ // Module
				List<Module> modules = new ArrayList<Module>();
				for (ModuleViewBean srcView : srcInfo.getModuleViewBeanList()) {
					modules.add(destInfo.new Module()
							.setSelected(FluentList.from(srcInfo.getSelectedModuleNames()).asList()
									.contains(srcView.getModuleName()))
							.setModuleNm(srcView.getModuleName()).setRepository(srcView.getRepository()));
				}
				destInfo.setModules(modules);
			}
			{	//Build Environment
				List<BuildEnvironment> envs = new ArrayList<BuildEnvironment>();
				for ( BuildEnvViewBean srcView: srcInfo.getBuildEnvViewBeanList() ) {
					envs.add( destInfo.new BuildEnvironment()
						.setSelectedBuild( TriStringUtils.equals(srcView.getEnvNo(), srcInfo.getSelectedBuildEnvNo()) )
						.setSelectedFullBuild( TriStringUtils.equals(srcView.getEnvNo(), srcInfo.getSelectedFullBuildEnvNo()) )
						.setEnvId( srcView.getEnvNo() )
						.setEnvironmentNm( srcView.getEnvName() )
						.setSummary( srcView.getSummary() )
						.setStsId(srcView.getStatusId())
						.setUpdTime(srcView.getInsertUpdateDate())
					);
				}
				destInfo.setBuildEnvs( envs );
			}
			{	//Release Environment
				List<ReleaseEnvironment> envs = new ArrayList<ReleaseEnvironment>();
				for ( RelEnvViewBean srcView: srcInfo.getRelEnvViewBeanList() ) {
					envs.add( destInfo.new ReleaseEnvironment()
						.setSelected(  FluentList.from(srcInfo.getSelectedRelEnvNos()).asList().contains(srcView.getEnvNo()) )
						.setEnvId( srcView.getEnvNo() )
						.setEnvironmentNm( srcView.getEnvName() )
						.setSummary( srcView.getEnvSummary() )
						.setStsId(srcView.getStatusId())
						.setUpdTime(srcView.getLatestUpdateDate())
					);
				}
				destInfo.setReleaseEnvs( envs );
			}

			IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
			destInfo.setDefaultPublicPath( sheet.getValue(UmDesignEntryKeyByCommon.defaultPublicPath) );
			destInfo.setDefaultPrivatePath( sheet.getValue(UmDesignEntryKeyByCommon.defaultPrivatePath) );
	   }

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003027I, destInfo.getLotSubject());
		}
	}

	private String[] getPjtIds(String lotId){

		PjtCondition condition = new PjtCondition();
		condition.setLotId( lotId );

		List<IPjtEntity> entityList = this.support.getPjtDao().find( condition.getCondition() );
		List<String> pjtIdList = new ArrayList<String>();

		for( IPjtEntity entity : entityList ){
			pjtIdList.add(entity.getPjtId());
		}

		return pjtIdList.toArray(new String[0]);
	}

	private String[] getHeadBlIds(String lotId){

		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( HistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IHeadBlEntity> entityList =  this.support.getHeadBlDao().find(condition.getCondition(), sort);
		List<String> headBlIdList = new ArrayList<String>();

		if( entityList.size() != 0){
			headBlIdList.add( entityList.get(0).getHeadBlId() );
		}

		return headBlIdList.toArray(new String[]{});
	}
}
