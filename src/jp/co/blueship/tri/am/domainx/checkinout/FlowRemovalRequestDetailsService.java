package jp.co.blueship.tri.am.domainx.checkinout;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowRemovalRequestDetailsService implements IDomain<FlowRemovalRequestDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRemovalRequestDetailsServiceBean> execute(
			IServiceDto<FlowRemovalRequestDetailsServiceBean> serviceDto) {

		FlowRemovalRequestDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 *
	 * @param serviceBean Service Bean
	 */
	private void init(FlowRemovalRequestDetailsServiceBean serviceBean) {
		String areqId = serviceBean.getParam().getSelectedAreqId();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);
		serviceBean.setCacheAreqFileEntities( this.support.findAreqFileEntities(areqId, AreqCtgCd.value(areqEntity.getAreqCtgCd())) );

		ILotEntity lotEntity = this.support.findLotEntity( areqEntity.getLotId() );
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqEntity.getPjtId() );

		serviceBean.getDetailsView()
			.setLotId			( areqEntity.getLotId() )
			.setLotStsId		( lotEntity.getProcStsId() )
			.setLotStatus		( sheet.getValue(AmDesignBeanId.statusId, lotEntity.getProcStsId()) )
			.setAreqId			( areqEntity.getAreqId() )
			.setPjtId			( areqEntity.getPjtId() )
			.setPjtStsId		( pjtEntity.getProcStsId() )
			.setPjtStatus		( sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()) )
			.setReferenceId		( areqEntity.getChgFactorNo() )
			.setGroupNm			( areqEntity.getGrpNm() )
			.setSubmitterNm		( areqEntity.getDelReqUserNm() )
			.setSubmitterIconPath
								( support.getUmFinderSupport().getIconPath(areqEntity.getDelReqUserId()))
			.setAssigneeNm		( areqEntity.getAssigneeNm() )
			.setAssigneeIconPath( support.getUmFinderSupport().getIconPath(areqEntity.getAssigneeId()))
			.setSubject			( areqEntity.getSummary() )
			.setContents		( areqEntity.getContent() )
			.setRemovalRequestTime
								( TriDateUtils.convertViewDateFormat(areqEntity.getDelReqTimestamp(), formatYMDHM) )
			.setCtgNm			( areqEntity.getCtgNm() )
			.setMstoneNm		( areqEntity.getMstoneNm() )
			.setUpdTime			( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMDHM) )
			.setStsId			( areqEntity.getProcStsId() )
			.setStatus			( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) )
			;

		ITreeFolderViewBean tree = AmResourceSelectionUtils.getTreeFolderView( serviceBean.getCacheAreqFileEntities() );
		serviceBean.getResourceSelectionFolderView().setFolderView( tree );
		serviceBean.getResourceSelectionFolderView().setSelectedPath( tree.getOwner().getPath() );

		List<IRemovalResourceDetailsViewBean> resourceViews = this.getResourceViews( serviceBean );
		serviceBean.getResourceSelectionFolderView().setRequestViews( resourceViews );
		
		support.getUmFinderSupport().updateAccsHist(serviceBean, AmTables.AM_AREQ, areqId);
	}

	/**
	 * @param serviceBean
	 */
	private void onChange( FlowRemovalRequestDetailsServiceBean serviceBean ) {
		ResourceRequestType selectionType = serviceBean.getParam().getResourceSelection().getType();
		String selectedPath = serviceBean.getParam().getResourceSelection().getPath();

		if ( ResourceRequestType.selectFolder.equals(selectionType) ) {
			serviceBean.getResourceSelectionFolderView().setRequestViews( this.getResourceViews( serviceBean ) );
			serviceBean.getResourceSelectionFolderView().setSelectedPath( selectedPath );

		} else if ( ResourceRequestType.openFolder.equals(selectionType) ) {
			ITreeFolderViewBean tree = serviceBean.getResourceSelectionFolderView().getFolderView();
			TreeFolderViewBean viewBean = (TreeFolderViewBean) tree.getFolder(selectedPath);
			viewBean.setOpenFolder(true);

		} else if ( ResourceRequestType.closeFolder.equals(selectionType) ) {
			ITreeFolderViewBean rootFolder = serviceBean.getResourceSelectionFolderView().getFolderView();
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(false);
		}
		
		if ( !serviceBean.getParam().getInputInfo().isFolderTreeFormat() ) {
			serviceBean.getResourceSelectionFolderView().setRequestViews( this.getResourceAllViews( serviceBean ) );
		}
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private List<IRemovalResourceDetailsViewBean> getResourceViews( FlowRemovalRequestDetailsServiceBean serviceBean ) {

		String selectedPath = serviceBean.getParam().getResourceSelection().getPath();

		ITreeFolderViewBean tree = serviceBean.getResourceSelectionFolderView().getFolderView();

		if (TriStringUtils.isEmpty(selectedPath)){
			selectedPath = tree.getOwner().getPath();
		}

		List<IRemovalResourceDetailsViewBean> detailsViews = new ArrayList<IRemovalResourceDetailsViewBean>();

		for ( String path: AmResourceSelectionUtils.foldersPathOf(serviceBean.getCacheAreqFileEntities(), selectedPath, tree) ) {
			ITreeFolderViewBean treeView = tree.getFolder(path);

			ResourceViewBean resource = new ResourceViewBean()
				.setPath( treeView.getPath() )
				.setName( treeView.getName() )
				.setCount( treeView.getCount() )
				.setDirectory( true )
				.setSize( null )
				;

			detailsViews.add( resource );
		}

		for( IAreqFileEntity entity : serviceBean.getCacheAreqFileEntities() ){
			String[] split = TriStringUtils.splitPath( entity.getFilePath() );
			String resourcePath = TriStringUtils.convertPath( entity.getFilePath(), true );

			if ( tree.getOwner().getPath().equals(selectedPath) ) {
				if  ( 1 != TriStringUtils.split( TriStringUtils.trimHeadSeparator(resourcePath), "/" ).length )
					continue;
			} else {
				if ( ! selectedPath.equals(TriStringUtils.convertPath(split[0], true)) )
					continue;
			}

			ResourceViewBean resource = new ResourceViewBean()
				.setPath( TriStringUtils.convertPath(entity.getFilePath(), true) )
				.setName( split[1] )
				.setCount( 0 )
				.setDirectory( false )
				.setSize( TriFileUtils.fileSizeOf( entity.getFileByteSize() ) )
				;

			detailsViews.add( resource );
		}

		return detailsViews;
	}
	
	private List<IRemovalResourceDetailsViewBean> getResourceAllViews( FlowRemovalRequestDetailsServiceBean serviceBean ) {
		List<IRemovalResourceDetailsViewBean> detailsViews = new ArrayList<IRemovalResourceDetailsViewBean>();
		
		for( IAreqFileEntity entity : serviceBean.getCacheAreqFileEntities() ){
			String[] split = TriStringUtils.splitPath( entity.getFilePath() );
			ResourceViewBean resource = new ResourceViewBean()
				.setPath( TriStringUtils.convertPath(entity.getFilePath(), true) )
				.setName( split[1] )
				.setCount( 0 )
				.setDirectory( false )
				.setSize( TriFileUtils.fileSizeOf( entity.getFileByteSize() ) )
				;

			detailsViews.add( resource );
		}

		return detailsViews;
	}
}