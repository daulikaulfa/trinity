package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowRequestResourceFileComparisonServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ResourceComparisonViewBean resourceComparisonView = new ResourceComparisonViewBean();

	public RequestParam getParam() {
		return param;
	}

	public ResourceComparisonViewBean getResourceComparisonView() {
		return resourceComparisonView;
	}
	public FlowRequestResourceFileComparisonServiceBean setResourceComparisonView(
			ResourceComparisonViewBean resourceComparisonView) {
		this.resourceComparisonView = resourceComparisonView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private String resourcePath = null;

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSelectedResourcePath() {
			return resourcePath;
		}
		public RequestParam setSelectedResourcePath(String selectedResourcePath) {
			this.resourcePath = selectedResourcePath;
			return this;
		}
	}
}
