package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckinRequestDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowCheckinRequestDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CheckinRequestDetailsViewBean detailsView = new CheckinRequestDetailsViewBean();
	private ResourceSelectionFolderView<ICheckinResourceDetailsViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<ICheckinResourceDetailsViewBean>();

	private List<IAreqFileEntity> cacheAreqFileEntities = new ArrayList<IAreqFileEntity>();

	public RequestParam getParam() {
		return param;
	}

	public ResourceSelectionFolderView<ICheckinResourceDetailsViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}
	public FlowCheckinRequestDetailsServiceBean setResourceSelectionFolderView(ResourceSelectionFolderView<ICheckinResourceDetailsViewBean> folderView) {
		this.resourceSelectionFolderView = folderView;
		return this;
	}

	public CheckinRequestDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowCheckinRequestDetailsServiceBean setDetailsView(CheckinRequestDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public List<IAreqFileEntity> getCacheAreqFileEntities() {
		return cacheAreqFileEntities;
	}

	public FlowCheckinRequestDetailsServiceBean setCacheAreqFileEntities(List<IAreqFileEntity> cacheAreqFileEntities) {
		this.cacheAreqFileEntities = cacheAreqFileEntities;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private CheckinRequestDetailsInputInfo inputInfo = new CheckinRequestDetailsInputInfo();
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public CheckinRequestDetailsInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckinRequestDetailsInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}

		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}
	}

	public class CheckinRequestDetailsInputInfo {
		private boolean isFolderTreeFormat = true;

		public boolean isFolderTreeFormat() {
			return this.isFolderTreeFormat;
		}
		public CheckinRequestDetailsInputInfo setFolderTreeFormat( boolean isFolderTreeFormat ) {
			this.isFolderTreeFormat = isFolderTreeFormat;
			return this;
		}
	}
}
