package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRemovalRequestService implements IDomain<FlowRemovalRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support;
	private IUmFinderSupport umSupport;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private List<IDomain<IGeneralServiceBean>> fileUploadActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> saveActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> handleUploadedFileActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setInputService(IDomain<IGeneralServiceBean> inputService) {
		this.inputService = inputService;
	}


	public void setFileUploadActions(List<IDomain<IGeneralServiceBean>> fileUploadActions) {
		this.fileUploadActions = fileUploadActions;
	}

	public void setSaveActions(List<IDomain<IGeneralServiceBean>> saveActions) {
		this.saveActions = saveActions;
	}

	public void setHandleUploadedFileActions(
			List<IDomain<IGeneralServiceBean>> handleUploadedFileActions) {
		this.handleUploadedFileActions = handleUploadedFileActions;
	}


	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowRemovalRequestServiceBean> execute(IServiceDto<FlowRemovalRequestServiceBean> serviceDto) {
		FlowRemovalRequestServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibMasterDelEntryServiceBean serviceBean = paramBean.getInnerService();

		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				SubmitMode submitMode = paramBean.getParam().getInputInfo().getSubmitMode();

				if (SubmitMode.draft.equals(submitMode)) {

					this.saveAsDraft(paramBean);

				} else if (SubmitMode.request.equals(submitMode)) {

					this.submitChanges(paramBean);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, serviceBean);
		}
	}

	private void init(FlowRemovalRequestServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMasterDelEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		this.beforeExecution(paramBean, serviceBean);

		// Call input service
		{
			serviceBean.setForward(ChaLibScreenID.MASTER_DEL_ENTRY);
			serviceBean.setReferer(ChaLibScreenID.MASTER_DEL_DETAIL_VIEW);
			inputService.execute(dto);
		}

		// Build Tree Folder View
		{
			List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), paramBean.getParam().getSelectedLotId(), null);
			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			AmResourceSelectionUtils.buildTreeFolderViewByInit(paramList, masterPath, false);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void onChange(FlowRemovalRequestServiceBean paramBean) {
		PreConditions.assertOf(paramBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {

			// Set Category Views
			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {

			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}

		// Re-render group name when select assignee
		if (RequestOption.selectAssignee.equals(selectedRequestCtg)) {
			this.refreshGroup(paramBean);
			return;
		}

		// Re-render assignee name when select group
		if (RequestOption.selectGroup.equals(selectedRequestCtg)) {
			this.refreshAssignee(paramBean);
			return;
		}

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), paramBean.getParam().getSelectedLotId(), null);
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {
			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmResourceSelectionUtils.openFolder(paramList, masterPath);
				return;
			}

			if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			if (ResourceRequestType.selectFolder.equals(selectedType)) {

				this.selectFolder(paramBean, paramList);
				return;
			}
			if (ResourceRequestType.selectSingleResource.equals(selectedType)) {

				this.selectSingleResource(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectAllResource.equals(selectedType)) {

				this.selectAllResource(paramBean, paramList);
				return;
			}
		}

		// Select Tree Mode or File/Drag and Drop mode
		if (RequestOption.fileUpload.equals(selectedRequestCtg)) {
			this.uploadFile(paramBean, paramList);
			return;
		}

	}

	private void selectFolder(FlowRemovalRequestServiceBean paramBean, List<Object> paramList) {
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// Set Asset Selection List
		AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, masterPath, false);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}
	
	private void selectSingleResource(FlowRemovalRequestServiceBean paramBean, List<Object> paramList){

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);
	}
	
	private void selectAllResource(FlowRemovalRequestServiceBean paramBean, List<Object> paramList){
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		// Set all selected files
		AmResourceSelectionUtils.selectFilesInFolder(paramBean, paramList, masterPath, false);
	}

	private void uploadFile(FlowRemovalRequestServiceBean paramBean, List<Object> paramList) {
		if( TriStringUtils.isEmpty( paramBean.getParam().getInputInfo().getCsvInputStreamBytes() ) ) {
			throw new ContinuableBusinessException(AmMessageId.AM001134E);
		}
		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// File Upload or Drag and Drop Selection
		AmResourceSelectionUtils.removalRequestFileUpload(paramBean, paramBean.getParam().getInputInfo(), paramList, fileUploadActions);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}

	/**
	 * @param paramBean
	 */
	private void saveAsDraft(FlowRemovalRequestServiceBean paramBean) {
		String status = AmAreqStatusId.DraftRemovalRequest.getStatusId();
		this.executeRemovalRequest(paramBean, status);

	}

	private void submitChanges(FlowRemovalRequestServiceBean paramBean) {
		String status = AmAreqStatusId.RemovalRequested.getStatusId();
		this.executeRemovalRequest(paramBean, status);
	}

	private void executeRemovalRequest(FlowRemovalRequestServiceBean paramBean, String status) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMasterDelEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( paramBean.getParam().getInputInfo().getPjtId() );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		List<Object> paramForSelectedFiles = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), paramBean.getParam().getSelectedLotId(), null);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramForSelectedFiles);

		{
			List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), paramBean.getParam().getSelectedLotId(), null);

			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		RemovalRequestEditInputBean inputBean = paramBean.getParam().getInputInfo();

		// Validate input information
		this.support.validateRemovalRequestInput(inputBean);

		Set<String> selectedFileSet = paramBean.getParam().getResourceSelection().getSelectedFileSet();

		if ( TriCollectionUtils.isEmpty( selectedFileSet )
				&& !( AmBusinessJudgUtils.isDraft( AmAreqStatusId.value(status) )
						|| AmBusinessJudgUtils.isPendingRequest( AmAreqStatusId.value(status) ) )) {

			throw new ContinuableBusinessException(AmMessageId.AM001141E);
		}

		IAreqDto areqDto = new AreqDto();
		IAreqEntity areqEntity = new AreqEntity();
		IPjtEntity pjtEntity = this.support.findPjtEntity( inputBean.getPjtId() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK, AmTables.AM_LOT_MDL_LNK );
		List<IAreqFileEntity> fileEntities = new ArrayList<IAreqFileEntity>();
		List<IAreqBinaryFileEntity> binaryFileEntities = new ArrayList<IAreqBinaryFileEntity>();

		// Set Areq Entity
		this.support.setRemovalRequestEntity(paramBean, inputBean, areqEntity, status);

		// グループの存在チェック
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		List<Object> paramList = new ArrayList<Object>();

		IDelApplyParamInfo actionParam = new DelApplyParamInfo();
		{

			actionParam.setUserId( paramBean.getUserId() );
			actionParam.setDelApplyUser( paramBean.getUserName() );
			actionParam.setDelApplyUserId( paramBean.getUserId() );
			actionParam.setCtgId( inputBean.getCtgId() );
			actionParam.setMstoneId( inputBean.getMstoneId() );
			paramList.add( pjtEntity );
			paramList.add( lotDto );
			paramList.add( actionParam );
		}


		IDelApplyFileResult[] files = new IDelApplyFileResult[]{};

		// Get all files
		if (TriStringUtils.isNotEmpty(selectedFileSet)) {
			files = FlowChaLibMasterDelEditSupport.getDelFiles(selectedFileSet);
		}

		IDelApplyBinaryFileResult[] binaryFiles = new IDelApplyBinaryFileResult[]{};

		// Get all binary files
		if (TriStringUtils.isNotEmpty(inputBean.getCsvFilePath())) {
			binaryFiles = FlowChaLibMasterDelEditSupport.getBinaryDelFiles(paramList);
		}

		{
			areqDto.setAreqEntity(areqEntity);
			areqDto.setAreqFileEntities(fileEntities);
			areqDto.setAreqBinaryFileEntities(binaryFileEntities);

			paramList.add( areqEntity );
			paramList.add( areqDto );
			paramList.add( fileEntities );
			paramList.add( binaryFileEntities );
			paramList.add( files );
			paramList.add( binaryFiles );
		}

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( serviceBean )
				.setParamList( paramList );

		VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
		if( VcsCategory.SVN.equals( scmType ) ) {

			List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
			paramList.add( reposEntities.toArray( new IVcsReposEntity[0] ) ) ;

			IPassMgtEntity passwordEntity = this.support.findPassMgtEntityBySVN();
			paramList.add( passwordEntity ) ;

		} else {
			throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
		}

		for (IDomain<IGeneralServiceBean> action : saveActions) {
			action.execute(innerServiceDto);
		}

		if (TriStringUtils.isNotEmpty(inputBean.getCsvFilePath())) {
			for (IDomain<IGeneralServiceBean> action : handleUploadedFileActions) {
				action.execute(innerServiceDto);
			}
		}

		{
			serviceBean.setReferer( ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_MASTER_DEL_ENTRY );
			mailService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}


	private void beforeExecution(FlowRemovalRequestServiceBean src, FlowChaLibMasterDelEntryServiceBean dest) {

		dest.setSelectedLotNo(src.getParam().getSelectedLotId());

		if ( RequestType.init.equals(src.getParam().getRequestType()) ) {
			dest.setUserId(src.getUserId());
		}
	}

	private void afterExecution(FlowChaLibMasterDelEntryServiceBean src, FlowRemovalRequestServiceBean dest) {

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {
			this.generateDropBox(src, dest);
		}
		if ( RequestType.submitChanges.equals((dest.getParam().getRequestType()))) {
			dest.getResult().setCompleted(true);
			dest.getResult().setAreqId(src.getApplyNo());

			SubmitMode submitMode = dest.getParam().getInputInfo().getSubmitMode();

			if (SubmitMode.draft.equals(submitMode)) {
				dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003023I);
			} else if (SubmitMode.request.equals(submitMode)) {
				dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003022I);
			}
		}
	}

	private void generateDropBox(FlowChaLibMasterDelEntryServiceBean src, FlowRemovalRequestServiceBean dest) {

		RemovalRequestEditInputBean destInfo = dest.getParam().getInputInfo();
		dest.getDetailsView().setSubmitterNm(dest.getHeader().getLogInUserName());

		// Set Pjt Views
		destInfo.setPjtViews(FluentList.from( src.getSelectPjtViewList() ).map( AmFluentFunctionUtils.toItemLabelsFromPjtViewBean ).asList());

		// Set Group Views
		ILotDto lotDto = this.support.findLotDto(dest.getParam().getSelectedLotId(), AmTables.AM_LOT_GRP_LNK);
		List<IGrpEntity> grpEntity = this.support.getUmFinderSupport().findGroupByUserId(dest.getUserId());

		String[] groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto, grpEntity).toArray(new String[0]);
		destInfo.setGroupViews(FluentList.from(this.support.getUmFinderSupport().findGroupByGroupIds(groupIds))
				.map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		// Set Assignee Views

		List<IUserEntity> userEntityList = this.umSupport.findUserByGroups(groupIds);
		destInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

		// Set Category Views
		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(dest.getParam().getSelectedLotId());
		destInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		// Set Milestone Views
		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(dest.getParam().getSelectedLotId());
		destInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
	}

	private void refreshGroup(FlowRemovalRequestServiceBean paramBean) {
		RemovalRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getAssigneeId())) {
			List<String> groupNameList = this.support.getSelectGroupNameList(paramBean.getParam().getSelectedLotId(), paramBean.getUserId());
			Set<String> groupNameSet = new HashSet<String>(groupNameList);

			List<IGrpEntity> grpEntityList = this.support.getUmFinderSupport().findGroupByUserId(inputInfo.getAssigneeId());
			List<IGrpEntity> selectedGrpEntity = new ArrayList<IGrpEntity>();
			for (IGrpEntity grpEntity : grpEntityList) {
				if (groupNameSet.contains(grpEntity.getGrpNm())) {
					selectedGrpEntity.add(grpEntity);
				}
			}
			inputInfo.setGroupViews(FluentList.from(selectedGrpEntity).map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());
		}
	}

	private void refreshAssignee(FlowRemovalRequestServiceBean paramBean) {
		RemovalRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getGroupId())) {
			List<IUserEntity> userEntityList = this.support.getUmFinderSupport().findUserByGroup(inputInfo.getGroupId());
			inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());
		}
	}

}
