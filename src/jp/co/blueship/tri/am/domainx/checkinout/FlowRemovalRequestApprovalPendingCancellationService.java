package jp.co.blueship.tri.am.domainx.checkinout;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V4.00.00
 * @author Sam
 */

public class FlowRemovalRequestApprovalPendingCancellationService implements IDomain<FlowRemovalRequestApprovalPendingCancellationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRemovalRequestApprovalPendingCancellationServiceBean> execute( IServiceDto<FlowRemovalRequestApprovalPendingCancellationServiceBean> serviceDto ) {

		FlowRemovalRequestApprovalPendingCancellationServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibCompMasterDelCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );
			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty( areqId ) , "SelectedAreqId is not specified" );

			if (RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);

			}
            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
            	this.submitChanges(paramBean);
            }

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}
		return serviceDto;
	}

	private void validate(FlowRemovalRequestApprovalPendingCancellationServiceBean paramBean){

		String areqId = paramBean.getParam().getSelectedAreqId();
		IAreqDto areqDto = support.findAreqDto( areqId );
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( pjtEntity.getLotId() )
				.setAreqIds( areqId );

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

	}

	private void submitChanges(FlowRemovalRequestApprovalPendingCancellationServiceBean paramBean){

		String areqId = paramBean.getParam().getSelectedAreqId();
		IAreqDto areqDto = support.findAreqDto( areqId );
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( pjtEntity.getLotId() )
				.setAreqIds( areqId );

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibCompMasterDelCancelServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		innerServiceBean.setForward(ChaLibScreenID.COMP_MASTER_DEL_CANCEL);
		{
			this.beforeExecution(paramBean, innerServiceBean);
			this.setEntity( areqDto, innerServiceBean, paramBean);

			{
				List<Object> paramList = new ArrayList<Object>();

				paramList.add( areqDto );
				paramList.add( lotDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

			for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}
			}

			support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqId);

			support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.RemovalRequestCancelled, areqId);

		}
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003025I);
	}

	private void beforeExecution(FlowRemovalRequestApprovalPendingCancellationServiceBean src, FlowChaLibCompMasterDelCancelServiceBean dest) {

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setDelApplyNo(src.getParam().getSelectedAreqId());
		}
	}

	private void  setEntity(IAreqDto dto, FlowChaLibCompMasterDelCancelServiceBean innerServiceBean, FlowRemovalRequestApprovalPendingCancellationServiceBean paramBean) {

		String cancelReason = paramBean.getParam().getInputInfo().getComment();

		innerServiceBean.setSystemDate( TriDateUtils.getSystemTimestamp() );
		innerServiceBean.setUserId(innerServiceBean.getUserId());
		innerServiceBean.setUserName(innerServiceBean.getUserName());

		IAreqEntity entity = dto.getAreqEntity();
		entity.setStsId			( AmAreqStatusId.PendingRemovalRequest.getStatusId() );
		entity.setDelStsId		( StatusFlg.off );
		entity.setDelTimestamp	( innerServiceBean.getSystemDate());
		entity.setDelUserId		( innerServiceBean.getUserId());
		entity.setDelUserNm		( innerServiceBean.getUserName());
		entity.setDelCmt		( cancelReason );

		// am_areq_fileレコード論理削除
		for( IAreqFileEntity fileEntity : dto.getAreqFileEntities() ) {
			fileEntity.setDelStsId( StatusFlg.off );
		}
		// am_areq_binary_fileレコード論理削除
		for( IAreqBinaryFileEntity binaryFileEntity : dto.getAreqBinaryFileEntities() ) {
			binaryFileEntity.setDelStsId( StatusFlg.off );
		}
	}
}


