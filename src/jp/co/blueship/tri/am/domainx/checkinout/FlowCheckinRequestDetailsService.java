package jp.co.blueship.tri.am.domainx.checkinout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckinRequestDetailsViewBean.AttachFile;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckinResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestDetailsServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowCheckinRequestDetailsService implements IDomain<FlowCheckinRequestDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowCheckinRequestDetailsServiceBean> execute(
			IServiceDto<FlowCheckinRequestDetailsServiceBean> serviceDto) {

		FlowCheckinRequestDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean, null);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 *
	 * @param serviceBean
	 *            Service Bean
	 */
	private void init(FlowCheckinRequestDetailsServiceBean serviceBean) {
		String areqId = serviceBean.getParam().getSelectedAreqId();

		IAreqDto areqDto = this.support.findAreqDto(areqId, AmTables.AM_AREQ_ATTACHED_FILE);
		IAreqEntity areqEntity = areqDto.getAreqEntity();
		serviceBean.setCacheAreqFileEntities( this.support.findAreqFileEntities(areqId, AreqCtgCd.value(areqEntity.getAreqCtgCd())));

		List<AttachFile> attachmentfiles = serviceBean.getDetailsView().getAttachmentFileNms();
		for (IAreqAttachedFileEntity fileEntity : areqDto.getAreqAttachedFileEntities()) {
			AttachFile attachFile = serviceBean.getDetailsView().new AttachFile();
			attachFile.setFileName(fileEntity.getFilePath());
			attachFile.setSeqNo(fileEntity.getAttachedFileSeqNo());
			attachmentfiles.add(attachFile);
		}

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		serviceBean.getDetailsView()
			.setLotId			( areqEntity.getLotId() )
			.setAreqId			( areqEntity.getAreqId() )
			.setPjtId			( areqEntity.getPjtId() )
			.setReferenceId		( areqEntity.getChgFactorNo() )
			.setGroupNm			( areqEntity.getGrpNm() )
			.setSubmitterNm		( areqEntity.getRegUserNm() )
			.setSubmitterIconPath
								( support.getUmFinderSupport().getIconPath( areqEntity.getRegUserId() ) )
			.setAssigneeNm		( areqEntity.getAssigneeNm() )
			.setAssigneeIconPath( support.getUmFinderSupport().getIconPath( areqEntity.getAssigneeId() ) )
			.setSubject			( areqEntity.getSummary() )
			.setContents		( areqEntity.getContent())
			.setCheckoutTime	( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(), formatYMDHM))
			.setCheckinDueDate	( TriDateUtils.convertViewDateFormat(areqEntity.getRtnReqDueDate(), TriDateUtils.getYMDDateFormat(), formatYMD) )
			.setCtgNm			( areqEntity.getCtgNm() )
			.setMstoneNm		( areqEntity.getMstoneNm() )
			.setUpdTime			( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMDHM))
			.setStsId			( areqEntity.getProcStsId())
			.setStatus			( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()))
			.setCheckoutPath	( AmDesignBusinessRuleUtils.getSharePublicPathOfCheckoutRequest(areqEntity))
			.setCheckinPath		( AmDesignBusinessRuleUtils.getSharePublicPathOfCheckinRequest(areqEntity));
		;

		ITreeFolderViewBean tree = AmResourceSelectionUtils.getTreeFolderView(serviceBean.getCacheAreqFileEntities());
		serviceBean.getResourceSelectionFolderView().setFolderView(tree);
		serviceBean.getResourceSelectionFolderView().setSelectedPath(tree.getOwner().getPath());

		List<ICheckinResourceDetailsViewBean> resourceViews = this.getResourceViews(serviceBean);
		serviceBean.getResourceSelectionFolderView().setRequestViews(resourceViews);

		support.getUmFinderSupport().updateAccsHist(serviceBean, AmTables.AM_AREQ, areqId);
	}

	private void onChange(FlowCheckinRequestDetailsServiceBean serviceBean,
			FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean innerService) {
		ResourceRequestType selectionType = serviceBean.getParam().getResourceSelection().getType();
		String selectedPath = serviceBean.getParam().getResourceSelection().getPath();

		if (ResourceRequestType.selectFolder.equals(selectionType)) {
			serviceBean.getResourceSelectionFolderView().setRequestViews(this.getResourceViews(serviceBean));
			serviceBean.getResourceSelectionFolderView().setSelectedPath(selectedPath);

		} else if (ResourceRequestType.openFolder.equals(selectionType)) {
			ITreeFolderViewBean tree = serviceBean.getResourceSelectionFolderView().getFolderView();
			TreeFolderViewBean viewBean = (TreeFolderViewBean) tree.getFolder(selectedPath);
			viewBean.setOpenFolder(true);

		} else if (ResourceRequestType.closeFolder.equals(selectionType)) {
			ITreeFolderViewBean rootFolder = serviceBean.getResourceSelectionFolderView().getFolderView();
			TreeFolderViewBean viewBean = (TreeFolderViewBean) rootFolder.getFolder(selectedPath);
			viewBean.setOpenFolder(false);

		}

		if (!serviceBean.getParam().getInputInfo().isFolderTreeFormat()) {
			serviceBean.getResourceSelectionFolderView().setRequestViews(this.getResourceAllViews(serviceBean));
		}
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private List<ICheckinResourceDetailsViewBean> getResourceViews(FlowCheckinRequestDetailsServiceBean serviceBean) {

		String selectedPath = serviceBean.getParam().getResourceSelection().getPath();

		ITreeFolderViewBean tree = serviceBean.getResourceSelectionFolderView().getFolderView();

		if (TriStringUtils.isEmpty(selectedPath)) {
			selectedPath = tree.getOwner().getPath();
		}

		List<ICheckinResourceDetailsViewBean> detailsViews = new ArrayList<ICheckinResourceDetailsViewBean>();

		for (String path : AmResourceSelectionUtils.foldersPathOf(serviceBean.getCacheAreqFileEntities(), selectedPath,
				tree)) {
			ITreeFolderViewBean treeView = tree.getFolder(path);
			CheckinResourceDetailsViewBean resource = new CheckinResourceDetailsViewBean().setPath(treeView.getPath())
					.setName		( treeView.getName())
					.setCount		( treeView.getCount())
					.setDirectory	( true )
					.setSize		( null )
					;

			detailsViews.add(resource);
		}

		IAreqDto areqDto = new AreqDto();
		IAreqEntity areqEntity = this.support.findAreqEntity(serviceBean.getParam().getSelectedAreqId());
		areqDto.setAreqEntity(areqEntity);

		ILotDto lotDto = new LotDto();
		ILotEntity lotEntity = this.support.findLotEntity(areqEntity.getLotId());
		lotDto.setLotEntity(lotEntity);

		List<Object> paramList = new ArrayList<Object>();
		paramList.add(lotDto);
		paramList.add(areqDto);

		Map<String, IExtEntity> ucfExtensionMap = support.getExtMap();

		for (IAreqFileEntity entity : serviceBean.getCacheAreqFileEntities()) {
			String[] split = TriStringUtils.splitPath(entity.getFilePath());
			String resourcePath = TriStringUtils.convertPath(entity.getFilePath(), true);

			if (tree.getOwner().getPath().equals(selectedPath)) {
				if (1 != TriStringUtils.split(TriStringUtils.trimHeadSeparator(resourcePath), "/").length)
					continue;
			} else {
				if (!selectedPath.equals(TriStringUtils.convertPath(split[0], true)))
					continue;
			}

			FileStatus status = FileStatus.value(entity.getFileStsId());
			boolean isDiff = false;

			if( FileStatus.Edit.equals(entity.getFileStsId())
					&& !AmBusinessFileUtils.isBinary(entity.getFilePath(), ucfExtensionMap) ){

				isDiff = true;
			}

			CheckinResourceDetailsViewBean resource = new CheckinResourceDetailsViewBean()
					.setPath		( TriStringUtils.convertPath(entity.getFilePath(), true))
					.setName		( split[1] )
					.setCount		( 0 )
					.setDirectory	( false )
					.setSize		( TriFileUtils.fileSizeOf(entity.getFileByteSize()))
					.setDiff		( isDiff )
					.setStsId		( status.getStatusId())
					;

			detailsViews.add( resource );
		}

		return detailsViews;
	}

	private List<ICheckinResourceDetailsViewBean> getResourceAllViews(FlowCheckinRequestDetailsServiceBean serviceBean){
		List<ICheckinResourceDetailsViewBean> detailsViews = new ArrayList<ICheckinResourceDetailsViewBean>();
		Map<String, IExtEntity> ucfExtensionMap = support.getExtMap();

		for (IAreqFileEntity entity : serviceBean.getCacheAreqFileEntities()) {
			String[] split = TriStringUtils.splitPath(entity.getFilePath());
			FileStatus status = FileStatus.value(entity.getFileStsId());
			boolean isDiff = false;

			if( FileStatus.Edit.equals(entity.getFileStsId())
					&& !AmBusinessFileUtils.isBinary(entity.getFilePath(), ucfExtensionMap) ){

				isDiff = true;
			}

			CheckinResourceDetailsViewBean resource = new CheckinResourceDetailsViewBean()
					.setPath		( TriStringUtils.convertPath(entity.getFilePath(), true))
					.setName		( split[1] )
					.setCount		( 0 )
					.setDirectory	( false )
					.setSize		( TriFileUtils.fileSizeOf(entity.getFileByteSize()))
					.setDiff		( isDiff )
					.setStsId		( status.getStatusId())
					;

			detailsViews.add( resource );
		}

		return detailsViews;

	}
}
