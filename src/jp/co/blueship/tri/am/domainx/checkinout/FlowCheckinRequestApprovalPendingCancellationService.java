package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestApprovalPendingCancellationService implements IDomain<FlowCheckinRequestApprovalPendingCancellationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support;

	public void setSupport( FlowChaLibRtnEditSupport support ) {
		this.support = support;
	}

	private ActionStatusMatrixList statusMatrixAction = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowCheckinRequestApprovalPendingCancellationServiceBean> execute(IServiceDto<FlowCheckinRequestApprovalPendingCancellationServiceBean> serviceDto) {
		FlowCheckinRequestApprovalPendingCancellationServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );
			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty( areqId ) , "SelectedAreqId is not specified" );

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
            	this.submitChanges(paramBean);
            }

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void validate(FlowCheckinRequestApprovalPendingCancellationServiceBean paramBean) {

		String areqId = paramBean.getParam().getSelectedAreqId();
		AmItemChkUtils.checkApplyNo(areqId);

		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqEntity.getPjtId() );

		IAreqDto areqDto = new AreqDto();
		areqDto.setAreqEntity(areqEntity);

		AreqDtoList areqDtoList = new AreqDtoList();
		areqDtoList.add(areqDto);

		String selectedLotId = pjtEntity.getLotId();

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean	( paramBean )
				.setFinder		( support )
				.setActionList	( statusMatrixAction )
				.setLotIds		( selectedLotId )
				.setAreqIds		( areqEntity.getAreqId() )
				;

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
	}

	private void submitChanges(FlowCheckinRequestApprovalPendingCancellationServiceBean paramBean)throws IOException {
		String areqId = paramBean.getParam().getSelectedAreqId();
		AmItemChkUtils.checkApplyNo(areqId);

		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqEntity.getPjtId() );

		IAreqDto areqDto = new AreqDto();
		areqDto.setAreqEntity(areqEntity);

		AreqDtoList areqDtoList = new AreqDtoList();
		areqDtoList.add(areqDto);

		String selectedLotId = pjtEntity.getLotId();

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean	( paramBean )
				.setFinder		( support )
				.setActionList	( statusMatrixAction )
				.setLotIds		( selectedLotId )
				.setAreqIds		( areqEntity.getAreqId() )
				;

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

		IApproveParamInfo paramInfo = new ApproveParamInfo();
		paramInfo.setAssetApplyEntities	( areqDtoList );
		paramInfo.setUser				( paramBean.getUserName() );
		paramInfo.setUserId				( paramBean.getUserId() );

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotDto );
		paramList.add( areqDto );
		paramList.add( paramInfo );

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		// Actions:
		// 1: backup check-in files
		// 2: remove System_in_out folder
		// 3: update am_areq: sts_id (LEND_APPLY), data_ctg_cd: LendingRequest (40)
		for ( IDomain<IGeneralServiceBean> action : actions ) {
			action.execute( innerServiceDto );
		}

		this.removeAttachment(paramBean);
		// Update SM_EXEC_DATA_STS: sts_id = RETURN_CANCEL
		support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.CheckinRequestCancelled, areqId);
		
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003010I);

	}
	
	
	private void removeAttachment(FlowCheckinRequestApprovalPendingCancellationServiceBean paramBean) throws IOException {
		List<IAreqAttachedFileEntity> areqAttachedFileEntities	= support.findAreqAttachedFileEntities(paramBean.getParam().getSelectedAreqId());
		for (IAreqAttachedFileEntity iAreqAttachedFileEntity : areqAttachedFileEntities) {
			int row = this.removeAttachmentFromDB(iAreqAttachedFileEntity);
			if (row > 0) {
				this.removeAttachmentFromFileSystem(iAreqAttachedFileEntity);
			}
		}	
	}

	private int removeAttachmentFromDB(IAreqAttachedFileEntity attachmentEntity) {
		AreqAttachedFileCondition fileCondition = new AreqAttachedFileCondition();
		fileCondition.setAreqId(attachmentEntity.getAreqId());
		fileCondition.setAttachedFileSeqNo(attachmentEntity.getAttachedFileSeqNo());
		fileCondition.setDelStsId(StatusFlg.off);

		return this.support.getAreqAttachedFileDao().delete(fileCondition.getCondition());
	}

	private void removeAttachmentFromFileSystem(IAreqAttachedFileEntity attachmentEntity) throws IOException {
		String filePath = AmResourceSelectionUtils.getCheckinFileAttachmentPath(attachmentEntity);
		File deleteFile = new File(filePath);

		if(deleteFile.exists()) {
			TriFileUtils.delete(deleteFile);
		}
	}
}
