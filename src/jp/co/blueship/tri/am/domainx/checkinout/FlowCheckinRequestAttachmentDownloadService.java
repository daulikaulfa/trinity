package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestAttachmentDownloadServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

public class FlowCheckinRequestAttachmentDownloadService implements IDomain<FlowCheckinRequestAttachmentDownloadServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowCheckinRequestAttachmentDownloadServiceBean> execute(
			IServiceDto<FlowCheckinRequestAttachmentDownloadServiceBean> serviceDto) {

		FlowCheckinRequestAttachmentDownloadServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				String areqId = paramBean.getParam().getSelectedAreqId();
				PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}



	private void  init(FlowCheckinRequestAttachmentDownloadServiceBean serviceBean){
		String areqId = serviceBean.getParam().getSelectedAreqId();
		String selectedFileNm = serviceBean.getParam().getSelectedFileNm();
		String seqNo = serviceBean.getParam().getSeqNo();


		IAreqDto areqDto = this.support.findAreqDto(areqId, AmTables.AM_AREQ_ATTACHED_FILE);

		IAreqAttachedFileEntity selectedFileEntity = null;
		for (IAreqAttachedFileEntity fileEntity : areqDto.getAreqAttachedFileEntities()) {
			if(fileEntity.getFilePath().equals(selectedFileNm)){
				selectedFileEntity = fileEntity;
			}
		}

		if(selectedFileEntity == null){
			serviceBean.getResult().setCompleted(false);
			return;
		}

		File attachmentFilePath = AmDesignBusinessRuleUtils.getAttachmentFilePath(areqId,selectedFileNm, seqNo);

		serviceBean.getFileResponse().setFile(attachmentFilePath);
		serviceBean.getResult().setCompleted(true);
	}
}
