package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public class CheckInOutResourceViewBean implements ICheckoutResourceViewBean, ICheckinResourceViewBean, IRemovalResourceViewBean {
	@Expose	private boolean directory;
	@Expose	private String path;
	@Expose	private String name;
	@Expose	private String size;
	@Expose private int count = 0;
	@Expose	private boolean selected = false;
	@Expose private boolean locked = false;
	@Expose	private String areqId;
	@Expose	private String pjtId;
	@Expose	private AreqCtgCd areqType;
	@Expose	private String referenceId;
	@Expose	private String submitterId;
	@Expose	private String submitterNm;
	@Expose	private String checkoutDate;
	@Expose	private String checkinDueDate;
	@Expose	private String checkinDate;
	@Expose	private String removalRequestDate;
	@Expose	private String stsId;
	@Expose	private boolean diff = false;

	@Override
	public boolean isFile() {
		return !directory;
	}
	@Override
	public boolean isDirectory() {
		return directory;
	}
	public CheckInOutResourceViewBean setDirectory(boolean directory) {
		this.directory = directory;
		return this;
	}
	@Override
	public String getPath() {
		return this.path;
	}
	public CheckInOutResourceViewBean setPath(String path) {
		this.path = path;
		return this;
	}
	@Override
	public String getName() {
		return this.name;
	}
	public CheckInOutResourceViewBean setName(String name) {
		this.name = name;
		return this;
	}
	@Override
	public String getSize() {
		return this.size;
	}
	public CheckInOutResourceViewBean setSize(String size) {
		this.size = size;
		return this;
	}
	@Override
	public int getCount() {
		return count;
	}
	public CheckInOutResourceViewBean setCount(int count) {
		this.count = count;
		return this;
	}
	@Override
	public boolean isSelected() {
		return selected;
	}
	public CheckInOutResourceViewBean setSelected(boolean selected) {
		this.selected = selected;
		return this;
	}
	@Override
	public boolean isLocked() {
		return locked;
	}
	public CheckInOutResourceViewBean setLocked(boolean locked) {
		this.locked = locked;
		return this;
	}

	@Override
	public String getLockedDate(){
		if( AmBusinessJudgUtils.isLendApply(this.areqType)
				|| AmBusinessJudgUtils.isReturnApply(this.areqType)){

			return this.checkoutDate;

		}else if( AmBusinessJudgUtils.isDeleteApply(this.areqType) ){
			return this.removalRequestDate;
		}

		return null;
	}

	@Override
	public String getAreqId() {
		return areqId;
	}
	public CheckInOutResourceViewBean setAreqId(String areqId) {
		this.areqId = areqId;
		return this;
	}
	@Override
	public String getPjtId() {
		return this.pjtId;
	}
	public CheckInOutResourceViewBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}
	@Override
	public boolean isCheckoutRequest() {
		return AmBusinessJudgUtils.isLendApply(this.areqType);
	}
	@Override
	public boolean isCheckinRequest() {
		return AmBusinessJudgUtils.isReturnApply(this.areqType);
	}
	@Override
	public boolean isRemovalRequest() {
		return AmBusinessJudgUtils.isDeleteApply(this.areqType);
	}
	public CheckInOutResourceViewBean setAreqType(AreqCtgCd areqType) {
		this.areqType = areqType;
		return this;
	}
	
	@Override
	public AreqCtgCd getAreqType() {
		return this.areqType;
	}
	
	@Override
	public String getReferenceId() {
		return this.referenceId;
	}
	public CheckInOutResourceViewBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}
	@Override
	public String getSubmitterId() {
		return this.submitterId;
	}
	public CheckInOutResourceViewBean setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
		return this;
	}
	@Override
	public String getSubmitterNm() {
		return this.submitterNm;
	}
	public CheckInOutResourceViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}
	@Override
	public String getCheckoutDate() {
		return this.checkoutDate;
	}
	public CheckInOutResourceViewBean setCheckoutDate(String checkoutDate) {
		this.checkoutDate = checkoutDate;
		return this;
	}
	@Override
	public String getCheckinDueDate() {
		return this.checkinDueDate;
	}
	public CheckInOutResourceViewBean setCheckinDueDate(String checkinDueDate) {
		this.checkinDueDate = checkinDueDate;
		return this;
	}
	@Override
	public String getCheckinDate() {
		return this.checkinDate;
	}
	public CheckInOutResourceViewBean setCheckinDate(String checkinDate) {
		this.checkinDate = checkinDate;
		return this;
	}
	@Override
	public String getRemovalRequestDate() {
		return removalRequestDate;
	}
	public CheckInOutResourceViewBean setRemovalRequestDate(String removalRequestDate) {
		this.removalRequestDate = removalRequestDate;
		return this;
	}

	@Override
	public String getStsId() {
		return stsId;
	}
	public CheckInOutResourceViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}
	@Override
	public boolean isDiff() {
		return this.diff;
	}
	public CheckInOutResourceViewBean setDiff(boolean diff) {
		this.diff = diff;
		return this;
	}

}
