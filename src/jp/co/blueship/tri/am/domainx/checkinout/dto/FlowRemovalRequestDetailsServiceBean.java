package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRemovalRequestDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RemovalRequestDetailsViewBean detailsView = new RemovalRequestDetailsViewBean();
	private ResourceSelectionFolderView<IRemovalResourceDetailsViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<IRemovalResourceDetailsViewBean>();

	private List<IAreqFileEntity> cacheAreqFileEntities = new ArrayList<IAreqFileEntity>();

	public List<IAreqFileEntity> getCacheAreqFileEntities() {
		return cacheAreqFileEntities;
	}

	public FlowRemovalRequestDetailsServiceBean setCacheAreqFileEntities(List<IAreqFileEntity> cacheAreqFileEntities) {
		this.cacheAreqFileEntities = cacheAreqFileEntities;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	public ResourceSelectionFolderView<IRemovalResourceDetailsViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}
	public FlowRemovalRequestDetailsServiceBean setResourceSelectionFolderView(ResourceSelectionFolderView<IRemovalResourceDetailsViewBean> folderView) {
		this.resourceSelectionFolderView = folderView;
		return this;
	}

	public RemovalRequestDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowRemovalRequestDetailsServiceBean setDetailsView(RemovalRequestDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private RemovalRequestDetailsInputInfo inputInfo = new RemovalRequestDetailsInputInfo();
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public RemovalRequestDetailsInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(RemovalRequestDetailsInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}

		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}
	}

	public class RemovalRequestDetailsInputInfo {
		private boolean isFolderTreeFormat = true;

		public boolean isFolderTreeFormat() {
			return this.isFolderTreeFormat;
		}
		public RemovalRequestDetailsInputInfo setFolderTreeFormat( boolean isFolderTreeFormat ) {
			this.isFolderTreeFormat = isFolderTreeFormat;
			return this;
		}
	}
}
