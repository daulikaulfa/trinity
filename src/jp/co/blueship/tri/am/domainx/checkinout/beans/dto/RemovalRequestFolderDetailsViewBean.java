package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class RemovalRequestFolderDetailsViewBean  implements IRemovalRequestDetailsViewBean{
	
	private int count;
	private String folderName;
	private int size;

	public int getCount() {
		return this.count;
	}
	
	public RemovalRequestFolderDetailsViewBean setCount(int count) {
		this.count = count;
		return this;
	}
	
	public String getPath() {
		return "";
	}

	public String getName() {
		return this.folderName;
	}
	public RemovalRequestFolderDetailsViewBean setName(String folderName) {
		this.folderName = folderName;
		return this;
	}

	public int getSize() {
		return this.size;
	}
	public RemovalRequestFolderDetailsViewBean setSize(int size) {
		this.size = size;
		return this;
	}

	public String getSubmitter() {
		return "";
	}
	
	public String getStatus() {
		return "";
	}
	
	public String getRemovalDate() {
		return "";
	}
	
}
