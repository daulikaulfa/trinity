package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowRemovalRequestEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RemovalRequestDetailsView detailsView = new RemovalRequestDetailsView();
	private ResourceSelectionFolderView<IRemovalResourceViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<IRemovalResourceViewBean>();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RemovalRequestDetailsView getDetailsView() {
		return detailsView;
	}

	public FlowRemovalRequestEditServiceBean setDetailsView(RemovalRequestDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public ResourceSelectionFolderView<IRemovalResourceViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}
	public FlowRemovalRequestEditServiceBean setResourceSelectionFolderView(ResourceSelectionFolderView<IRemovalResourceViewBean> folderView) {
		this.resourceSelectionFolderView = folderView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowRemovalRequestEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private RemovalRequestEditInputBean inputInfo = new RemovalRequestEditInputBean();
		private RequestOption requestOption = RequestOption.none;
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public RemovalRequestEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(RemovalRequestEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}
		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}

	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		selectResource( "selectResource" ),
		fileUpload( "fileUpload" ),
		selectAssignee( "selectAssignee" ),
		selectGroup( "selectGroup" ),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone")
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class RemovalRequestDetailsView {
		private String lotId;
		private String submitterNm;

		public String getLotId() {
			return lotId;
		}
		public RemovalRequestDetailsView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public RemovalRequestDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String areqId;
		private boolean completed = false;

		public String getAreqId() {
			return areqId;
		}

		public RequestsCompletion setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
