package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibSelectLendEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowCheckoutRequestService implements IDomain<FlowCheckoutRequestServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support;
	private IUmFinderSupport umSupport;

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private List<IDomain<IGeneralServiceBean>> actions4InputFile = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> saveDraftActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibLendEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	public void setInputService(IDomain<IGeneralServiceBean> service) {
		this.inputService = service;
	}

	public void setConfirmationService(IDomain<IGeneralServiceBean> service) {
		this.confirmationService = service;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> service) {
		this.completeService = service;
	}

	public void setActions4InputFile(List<IDomain<IGeneralServiceBean>> actions4InputFile) {
		this.actions4InputFile = actions4InputFile;
	}

	public void setSaveDraftActions(List<IDomain<IGeneralServiceBean>> saveDraftActions) {
		this.saveDraftActions = saveDraftActions;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowCheckoutRequestServiceBean> execute( IServiceDto<FlowCheckoutRequestServiceBean> serviceDto ) {

		FlowCheckoutRequestServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibLendEntryServiceBean innerServiceBean = paramBean.getInnerService();

		try {

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				SubmitMode submitMode = paramBean.getParam().getInputInfo().getSubmitMode();

				if (SubmitMode.draft.equals(submitMode)) {
					this.saveAsDraft(paramBean);

				} else if (SubmitMode.request.equals(submitMode)) {
					this.submitChanges(paramBean);
				} else {
					PreConditions.assertOf(false, "SubmitMode is not specified.");
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void init( FlowCheckoutRequestServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		String selectedLotId = paramBean.getParam().getSelectedLotId();

		serviceBean.setLotNo( selectedLotId );

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer( ChaLibScreenID.LEND_LIST );

		//basic information input
		{
			serviceBean.setForward( ChaLibScreenID.LEND_ENTRY );
			inputService.execute(dto);
			this.setInputParamByInit( serviceBean, paramBean );
		}

		// Build Tree Folder
		{
			List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), selectedLotId, null);
			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			AmResourceSelectionUtils.buildTreeFolderViewByInit(paramList, masterPath, false);
		}
	}

	private void onChange(FlowCheckoutRequestServiceBean paramBean) {
		PreConditions.assertOf(paramBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		// Re-render group name when select assignee
		if (RequestOption.selectAssignee.equals(selectedRequestCtg)) {
			this.refreshGroup(paramBean);
			return;
		}

		// Re-render assignee name when select group
		if (RequestOption.selectGroup.equals(selectedRequestCtg)) {
			this.refreshAssignee(paramBean);
			return;
		}

		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {
			// Set Category Views
			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {
			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}

		String selectedLotId = paramBean.getParam().getSelectedLotId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, null);
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {
			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmResourceSelectionUtils.openFolder(paramList, masterPath);
				return;
			}

			if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			if (ResourceRequestType.selectFolder.equals(selectedType)) {

				this.selectFolder(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectSingleResource.equals(selectedType)) {

				this.selectSingleResource(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectAllResource.equals(selectedType)) {

				this.selectAllResource(paramBean, paramList);
				return;
			}
		}

		if (RequestOption.fileUpload.equals(selectedRequestCtg)) {
			this.uploadFile(paramBean, paramList);
			return;
		}
	}

	private void selectFolder(FlowCheckoutRequestServiceBean paramBean, List<Object> paramList) {
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// Set Asset Selection List
		AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, masterPath, false);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
		
	}
	
	private void selectSingleResource(FlowCheckoutRequestServiceBean paramBean, List<Object> paramList){
		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);
		
	}
	
	private void selectAllResource(FlowCheckoutRequestServiceBean paramBean, List<Object> paramList){
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		// Set all selected files
		AmResourceSelectionUtils.selectFilesInFolder(paramBean, paramList, masterPath, false);
	}

	private void uploadFile(FlowCheckoutRequestServiceBean paramBean, List<Object> paramList) {
		if( TriStringUtils.isEmpty( paramBean.getParam().getInputInfo().getCsvInputStreamBytes() ) ) {
			throw new ContinuableBusinessException(AmMessageId.AM001134E);
		}
		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		CheckoutRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		// Set input param
		ChaLibEntryInputBaseInfoBean inputBean = new ChaLibEntryInputBaseInfoBean();
		{
			inputBean.setInPrjNo(inputInfo.getPjtId());
			if (TriStringUtils.isNotEmpty(inputInfo.getGroupId())) {
				inputBean.setInGroupName(this.support.getUmFinderSupport().
						findGroupById(inputInfo.getGroupId()).getGrpNm());
			}
			inputBean.setInSubject(inputInfo.getSubject());
			inputBean.setInContent(inputInfo.getContents());
			inputBean.setLendApplyFilePath(inputInfo.getCsvFilePath());
			inputBean.setLendApplyFileName(inputInfo.getCsvFileNm());
			inputBean.setLendApplyInputStreamBytes(inputInfo.getCsvInputStreamBytes());
		}

		// File Upload or Drag and Drop Selection
		AmResourceSelectionUtils.checkInOutFileUpload(paramBean, inputBean, paramList, null, null, actions4InputFile);


		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}

	/**
	 * @param paramBean Service Bean
	 * @throws ParseException
	 */
	private void submitChanges( FlowCheckoutRequestServiceBean paramBean ) throws ParseException {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		serviceBean.setScreenType( ScreenType.next );
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( paramBean.getParam().getInputInfo().getPjtId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		AmItemChkUtils.checkCheckinDueDate(paramBean.getParam().getInputInfo().getCheckinDueDate());

		String selectedLotId = paramBean.getParam().getSelectedLotId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, null);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		{
			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		this.setInputParamBySubmit(paramBean, serviceBean);

		// Call Lend Create Entry
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY);
			serviceBean.setForward(ChaLibScreenID.LEND_DETAIL_VIEW);
			inputService.execute(dto);

		}

		// Call Lend Create Confirmation
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_ENTRY);
			confirmationService.execute(dto);
		}

		// Call Lend Create Completion
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_ENTRY);
			completeService.execute(dto);

		}

		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_ENTRY);
			mailService.execute(dto);

		}


		this.afterExecution(serviceBean, paramBean);
	}

	private void saveAsDraft(FlowCheckoutRequestServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		serviceBean.setScreenType( ScreenType.next );
		dto.setServiceBean( serviceBean );

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setPjtIds( paramBean.getParam().getInputInfo().getPjtId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		String selectedLotId = paramBean.getParam().getSelectedLotId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, null);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);
		{
			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		this.setInputParamBySubmit(paramBean, serviceBean);

		// Call Lend Entry
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY);
			serviceBean.setForward(ChaLibScreenID.LEND_DETAIL_VIEW);
			inputService.execute(dto);

		}

		// Validate file and insert lend - equivalent to lend confirmation
		{
			ChaLibEntryInputBaseInfoBean inBean = serviceBean.getInBaseInfoBean();
			ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);

			IAreqEntity assetApplyEntity = new AreqEntity();

			FlowChaLibLendEditSupport.setAssetApplyEntityBySaveDraftMode(assetApplyEntity, inBean);


			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity(assetApplyEntity);

			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );
			{
				List<Object> list = new ArrayList<Object>();
				IEhLendParamInfo param = new EhLendParamInfo();
				support.setLendParamInfo(serviceBean.getInBaseInfoBean(), serviceBean.getApplyNo(), param);
				support.setLendParamInfo(serviceBean.getInAssetSelectBean(), masterPath, param);
				// ２重貸出チェック
				param.setDuplicationCheck(true) ;
				param.setUserId( paramBean.getUserId() );

				list.add( param );
				list.add( lotDto );
				list.add( areqDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( serviceBean )
						.setParamList( list );

				VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
				if( VcsCategory.SVN.equals(  scmType ) ) {
					List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
					list.add( reposEntities.toArray( new IVcsReposEntity[0] ) );
					list.add( this.support.findPassMgtEntityBySVN() ) ;

				} else {
					throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
				}

				for ( IDomain<IGeneralServiceBean> action : saveDraftActions ) {
					action.execute( innerServiceDto );
				}

			}
		}

		// Call Lend Completion
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_ENTRY);
			completeService.execute(dto);

		}

		this.afterExecution(serviceBean, paramBean);
	}

	/**
	 * @param src Inner Service Bean
	 * @param dest Request Parameter
	 */
	private void setInputParamByInit( FlowChaLibLendEntryServiceBean src, FlowCheckoutRequestServiceBean dest ) {
		ChaLibSelectLendEntryBaseInfoBean baseInfo = src.getSelectBaseInfoViewBean();
		CheckoutRequestEditInputBean destInfo = dest.getParam().getInputInfo();

		destInfo.setPjtViews(FluentList.from( baseInfo.getSelectPjtViewBeanList() ).map( AmFluentFunctionUtils.toItemLabelsFromPjtViewBean ).asList());

		ILotDto lotDto = this.support.findLotDto(dest.getParam().getSelectedLotId(), AmTables.AM_LOT_GRP_LNK);
		List<IGrpEntity> grpEntity = this.support.getUmFinderSupport().findGroupByUserId(dest.getUserId());

		String[] groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto, grpEntity).toArray(new String[0]);
		destInfo.setGroupViews(FluentList.from(this.support.getUmFinderSupport().findGroupByGroupIds(groupIds))
				.map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		List<IUserEntity> userEntityList = this.umSupport.findUserByGroups(groupIds);
		destInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(dest.getParam().getSelectedLotId());
		destInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(dest.getParam().getSelectedLotId());
		destInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
	}

	/**
	 * @param src Request Parameter
	 * @param dest Inner Service Bean
	 */
	private void setInputParamBySubmit( FlowCheckoutRequestServiceBean src,  FlowChaLibLendEntryServiceBean dest  ) {
		ChaLibEntryInputBaseInfoBean inBean = new ChaLibEntryInputBaseInfoBean();

		if (TriStringUtils.isNotEmpty(dest.getInBaseInfoBean())) {
			inBean = dest.getInBaseInfoBean();
		} else {
			dest.setInBaseInfoBean(inBean);
		}

		CheckoutRequestEditInputBean srcInfo = src.getParam().getInputInfo();
		inBean.setInLotNo(src.getParam().getSelectedLotId());
		inBean.setInPrjNo(srcInfo.getPjtId());

		IPjtEntity pjtEntity = null;

		if ( ! TriStringUtils.isEmpty( inBean.getInPrjNo() )) {
			pjtEntity = this.support.findPjtEntity( inBean.getInPrjNo() ) ;
			inBean.setInChangeCauseNo( pjtEntity.getChgFactorNo() ) ;
		}

		inBean.setInSubject(srcInfo.getSubject());
		inBean.setInContent(srcInfo.getContents());
		inBean.setInUserName(src.getUserName());
		inBean.setInUserId(src.getUserId());

		if ( srcInfo.getSubmitMode() == CheckoutRequestEditInputBean.SubmitMode.draft ) {
			inBean.setDraff( true );
		}

		if ( ! TriStringUtils.isEmpty( srcInfo.getGroupId() )) {
			inBean.setInGroupId(srcInfo.getGroupId());
			inBean.setInGroupName(this.support.getUmFinderSupport().findGroupById(srcInfo.getGroupId()).getGrpNm());
		}

		if (srcInfo.isResourceSelection()) {
			inBean.setLendApplyMode(AmResourceSelectionUtils.treeMode);
		} else {
			inBean.setLendApplyMode(AmResourceSelectionUtils.fileMode);
		}

		inBean.setLendApplyFileName(srcInfo.getCsvFileNm());
		inBean.setLendApplyFilePath(srcInfo.getCsvFilePath());
		inBean.setLendApplyInputStreamBytes(srcInfo.getCsvInputStreamBytes());
		inBean.setAssigneeId(srcInfo.getAssigneeId());

		if ( TriStringUtils.isNotEmpty(srcInfo.getAssigneeId()) ) {
			inBean.setAssigneeNm( this.support.getUmFinderSupport().findUserByUserId(srcInfo.getAssigneeId()).getUserNm() );
		} else {
			inBean.setAssigneeNm( "" );
		}

		inBean.setCheckinDueDate( TriStringUtils.defaultIfEmpty(srcInfo.getCheckinDueDate(), ""));
		inBean.setCtgId(srcInfo.getCtgId());
		inBean.setMstoneId(srcInfo.getMstoneId());

		ChaLibInputLendEntryAssetSelectBean inAssetSelectBean = new ChaLibInputLendEntryAssetSelectBean();

		if (TriStringUtils.isNotEmpty(dest.getInAssetSelectBean())) {
			inAssetSelectBean = dest.getInAssetSelectBean();
		} else {
			dest.setInAssetSelectBean(inAssetSelectBean);
		}

		Set<String> selectedFileSet = src.getParam().getResourceSelection().getSelectedFileSet();
		String[] applyLendFiles = selectedFileSet.toArray(new String[selectedFileSet.size()]);
		inAssetSelectBean.setInAssetResourcePath(applyLendFiles);
		dest.setApplyNo(null);
	}

	private void refreshAssignee(FlowCheckoutRequestServiceBean serviceBean) {
		CheckoutRequestEditInputBean inputInfo = serviceBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getGroupId())) {
			List<IUserEntity> userEntityList = this.umSupport.findUserByGroup(inputInfo.getGroupId());
			inputInfo.setAssigneeViews(
					FluentList.from(userEntityList).map(UmFluentFunctionUtils.toItemLabelsFromUserEntity).asList());
		}
	}

	private void refreshGroup(FlowCheckoutRequestServiceBean serviceBean) {
		CheckoutRequestEditInputBean inputInfo = serviceBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getAssigneeId())) {
			ILotDto lotDto = this.support.findLotDto(serviceBean.getParam().getSelectedLotId(), AmTables.AM_LOT_GRP_LNK);
			List<IGrpEntity> userEntityList = this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId());
			List<String> groupIdList = AmResourceSelectionUtils.getSelectedGroupList(lotDto, userEntityList);
			Set<String> groupIdSet = new HashSet<String>(groupIdList);

			List<IGrpEntity> grpEntityList = this.umSupport.findGroupByUserId(inputInfo.getAssigneeId());
			List<IGrpEntity> selectedGrpEntity = new ArrayList<IGrpEntity>();
			for (IGrpEntity grpEntity : grpEntityList) {
				if (groupIdSet.contains(grpEntity.getGrpId())) {
					selectedGrpEntity.add(grpEntity);
				}
			}
			inputInfo.setGroupViews(
					FluentList.from(selectedGrpEntity).map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());
		}
	}

	private void beforeExecution(FlowCheckoutRequestServiceBean src, FlowChaLibLendEntryServiceBean dest) {
		dest.setLotNo(src.getParam().getSelectedLotId());
		ChaLibEntryInputBaseInfoBean baseInfoBean = new ChaLibEntryInputBaseInfoBean();
		if (TriStringUtils.isNotEmpty(dest.getInBaseInfoBean())) {
			baseInfoBean = dest.getInBaseInfoBean();
		} else {
			dest.setInBaseInfoBean(baseInfoBean);
		}
		if ( RequestType.init.equals(src.getParam().getRequestType()) ) {

			baseInfoBean.setInLotNo(src.getParam().getSelectedLotId());
		}
	}

	private void afterExecution(FlowChaLibLendEntryServiceBean src, FlowCheckoutRequestServiceBean dest) {
		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.getResult().setAreqId(src.getApplyNo());
			dest.getResult().setCompleted(true);

			SubmitMode submitMode = dest.getParam().getInputInfo().getSubmitMode();
			if (SubmitMode.draft.equals(submitMode)) {
				dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003017I);
			} else if (SubmitMode.request.equals(submitMode)) {
				dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003008I);
			}
		}
	}
}
