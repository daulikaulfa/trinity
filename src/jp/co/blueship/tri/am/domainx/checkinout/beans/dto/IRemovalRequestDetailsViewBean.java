package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Sam
 */
public interface IRemovalRequestDetailsViewBean{
	public String getPath();
	public int getCount();
	public String getName();
	public int getSize();
	public String getSubmitter();
	public String getRemovalDate();
	public String getStatus();
}
