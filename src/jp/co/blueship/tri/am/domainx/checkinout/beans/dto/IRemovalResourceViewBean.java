package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public interface IRemovalResourceViewBean extends IResourceViewBean {
	public boolean isSelected();
	public boolean isLocked();
	public String getLockedDate();
	public String getAreqId();
	public String getPjtId();
	public boolean isCheckoutRequest();
	public boolean isCheckinRequest();
	public boolean isRemovalRequest();
	public String getReferenceId();
	public String getSubmitterId();
	public String getSubmitterNm();
	public String getCheckoutDate();
	public String getCheckinDueDate();
	public String getRemovalRequestDate();
	public AreqCtgCd getAreqType();
}
