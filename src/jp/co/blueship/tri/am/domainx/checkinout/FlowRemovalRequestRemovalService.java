package jp.co.blueship.tri.am.domainx.checkinout;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestRemovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V4.00.00
 * @author Sam
 */

public class FlowRemovalRequestRemovalService implements IDomain<FlowRemovalRequestRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> removalService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private FlowChaLibMasterDelEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setRemovalService(IDomain<IGeneralServiceBean> removalService) {
		this.removalService = removalService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowRemovalRequestRemovalServiceBean> execute( IServiceDto<FlowRemovalRequestRemovalServiceBean> serviceDto ) {

		FlowRemovalRequestRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibCompMasterDelCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );

			String areqId = paramBean.getParam().getSelectedAreqId();

			PreConditions.assertOf( TriStringUtils.isNotEmpty( areqId ) , "SelectedAreqId is not specified" );
            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
                this.submitChanges(paramBean);
            }

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}
		return serviceDto;
	}

	private void submitChanges(FlowRemovalRequestRemovalServiceBean paramBean){

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibCompMasterDelCancelServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		String areqId = paramBean.getParam().getSelectedAreqId();

		// Status Matrix Check
		{
			IAreqDto areqDto = support.findAreqDto( areqId );
			IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() );
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( pjtEntity.getLotId() )
			.setAreqIds( areqId );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		innerServiceBean.setForward(ChaLibScreenID.COMP_MASTER_DEL_CANCEL);
		{
			this.beforeExecution(paramBean, innerServiceBean);
			removalService.execute(dto);
			mailService.execute(dto);
			support.getUmFinderSupport().cleaningCtgLnk(AmTables.AM_AREQ, areqId);
			support.getUmFinderSupport().cleaningMstoneLnk(AmTables.AM_AREQ, areqId);
		}
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003021I);
	}

	private void beforeExecution(FlowRemovalRequestRemovalServiceBean src, FlowChaLibCompMasterDelCancelServiceBean dest) {

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setDelApplyNo(src.getParam().getSelectedAreqId());
		}
	}

}


