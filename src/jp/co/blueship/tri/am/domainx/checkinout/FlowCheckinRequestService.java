package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.*;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.CheckinRequestInputInfo;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowCheckinRequestService implements IDomain<FlowCheckinRequestServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support;

	public void setSupport( FlowChaLibRtnEditSupport support ) {
		this.support = support;
	}

	private IDomain<IGeneralServiceBean> checkinDetailService = null;
	private IDomain<IGeneralServiceBean> checkinConfirmationService = null;
	private IDomain<IGeneralServiceBean> checkinCheckService = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setCheckinDetailService(
			IDomain<IGeneralServiceBean> checkinDetailService) {
		this.checkinDetailService = checkinDetailService;
	}

	public void setCheckinConfirmationService(
			IDomain<IGeneralServiceBean> checkinConfirmationService) {
		this.checkinConfirmationService = checkinConfirmationService;
	}

	public void setCheckinCheckService(
			IDomain<IGeneralServiceBean> checkinCheckService) {
		this.checkinCheckService = checkinCheckService;
	}

	@Override
	public IServiceDto<FlowCheckinRequestServiceBean> execute(
			IServiceDto<FlowCheckinRequestServiceBean> serviceDto) {
		FlowCheckinRequestServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibRtnEntryServiceBean serviceBean = paramBean.getInnerService();
		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, serviceBean);
		}
	}

	private void init(FlowCheckinRequestServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibRtnDetailViewServiceBean serviceBean = paramBean.getInnerViewService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, null, serviceBean);

		// Call return details view service
		{
			checkinDetailService.execute(dto);
		}

		this.afterExecution(null, serviceBean, paramBean);

		// Build Tree Folder View Bean
		{
			List<Object> paramList =
					this.support.setParamList(
							paramBean.getResourceSelectionFolderView(),
							paramBean.getParam().getResourceSelection(),
							paramBean.getParam().getSelectedAreqId());

			AmResourceSelectionUtils.buildTreeFolderViewByInit(paramList, AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList ), true);
		}

	}

	private void onChange(FlowCheckinRequestServiceBean paramBean) throws IOException {
		PreConditions.assertOf(paramBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		List<Object> paramList =
				this.support.setParamList(
						paramBean.getResourceSelectionFolderView(),
						paramBean.getParam().getResourceSelection(),
						selectedAreqId);

		File checkinInOutBoxPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {

			if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmResourceSelectionUtils.openFolder(paramList, checkinInOutBoxPath);
				return;
			}

			if (ResourceRequestType.selectFolder.equals(selectedType)) {
				this.selectFolder(paramBean, paramList);
			}
		}

		if (RequestOption.refresh.equals(selectedRequestCtg)) {
			this.refreshScreen(paramBean, paramList);
			return;
		}

		// Attachment File Upload
		if (RequestOption.fileUpload.equals(selectedRequestCtg)) {
			this.uploadFile(paramBean, paramList);
		}

		// Attachment File Remove
		if (RequestOption.deleteUplodedFile.equals(selectedRequestCtg)) {
			this.removeAttachment(paramBean);
		}
		
		if (!paramBean.getParam().getInputInfo().isFolderTreeFormat()) {
			AmResourceSelectionUtils.openAllFolder(paramList, checkinInOutBoxPath);
			return;
		}

	}

	private void refreshScreen(FlowCheckinRequestServiceBean paramBean, List<Object> paramList) {
		try {
			ResourceSelection resourceSelection = paramBean.getParam().getResourceSelection();
			ResourceSelectionFolderView<ICheckinResourceViewBean> folderView = paramBean.getResourceSelectionFolderView();

			File checkinInOutBoxPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

			if (TriStringUtils.isNotEmpty(folderView.getSelectedPath())) {
				resourceSelection.setPath(folderView.getSelectedPath());
			}

			List<String> files = TriFileUtils.getFilePaths(checkinInOutBoxPath, TriFileUtils.TYPE_FILE, BusinessFileUtils.getFileFilter());

			Set<String> fileSet = new HashSet<String>( files );

			paramBean.getParam().getResourceSelection().setSelectedFileSet(fileSet);


			// Refresh Tree Folder View Bean
			AmResourceSelectionUtils.refreshTreeFolder(paramList, checkinInOutBoxPath);

			// Refresh Request View Bean
			AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, checkinInOutBoxPath, true);
		} catch (IOException e) {
			throw new TriSystemException( SmMessageId.SM005053S , e );

		} finally {
		}
	}

	private void selectFolder(FlowCheckinRequestServiceBean paramBean, List<Object> paramList) {

		ResourceSelection resourceSelection = paramBean.getParam().getResourceSelection();
		ResourceSelectionFolderView<ICheckinResourceViewBean> folderView = paramBean.getResourceSelectionFolderView();

		File checkinInOutBoxPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

		String beforeDir = TriStringUtils.convertPath(folderView.getSelectedPath(), true);
		String afterDir = TriStringUtils.convertPath(resourceSelection.getPath(), true);

		afterDir = (TriStringUtils.isEmpty(afterDir)) ? beforeDir : afterDir;

		// Set Asset Resource Selection List
		AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, checkinInOutBoxPath, true);

		AmResourceSelectionUtils.openFolder(paramList, checkinInOutBoxPath);
		
		folderView.setSelectedPath(afterDir);
	}

	private void uploadFile(FlowCheckinRequestServiceBean paramBean, List<Object> paramList) {

		// Save the attachment file
		if (TriStringUtils.isEmpty(paramBean.getParam().getInputInfo().getAttachmentInputStreamBytes())) {
			return;
		}
		String attachedFileSeqNo = this.support.nextvalByMultipleAttachedFileSeqNo(paramBean.getParam().getSelectedAreqId());
		paramBean.getParam().getInputInfo().setAttachmentFileSeqNo(attachedFileSeqNo);

		int row = this.insertAttachment(paramBean);
		if (row > 0) {
			AmResourceSelectionUtils.checkinFileUpload(paramBean);
		}

	}

	private void removeAttachment(FlowCheckinRequestServiceBean paramBean) throws IOException {
		int row = this.removeAttachmentFromDB(paramBean);
		if (row > 0) {
			this.removeAttachmentFromFileSystem(paramBean);
		}
	}

	private int removeAttachmentFromDB(FlowCheckinRequestServiceBean paramBean) {
		AreqAttachedFileCondition fileCondition = new AreqAttachedFileCondition();
		fileCondition.setAreqId(paramBean.getParam().getSelectedAreqId());
		fileCondition.setFilePath(paramBean.getParam().getInputInfo().getAttachmentFileNm());
		fileCondition.setAttachedFileSeqNo(paramBean.getParam().getInputInfo().getAttachmentFileSeqNo());
		fileCondition.setDelStsId(StatusFlg.off);

		return this.support.getAreqAttachedFileDao().delete(fileCondition.getCondition());
	}

	private void removeAttachmentFromFileSystem(FlowCheckinRequestServiceBean paramBean) throws IOException {
		String filePath = AmResourceSelectionUtils.getCheckinFileAttachmentPath(paramBean);
		File deleteFile = new File(filePath);

		if(deleteFile.exists()) {
			TriFileUtils.delete(deleteFile);
		}
	}

	private void submitChanges(FlowCheckinRequestServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibRtnEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String areqId = paramBean.getParam().getSelectedAreqId();
			IAreqEntity areqEntity = support.findAreqEntity( areqId );
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( areqEntity.getLotId() )
			.setPjtIds( areqEntity.getPjtId() )
			.setAreqIds( areqId );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		CheckinRequestInputInfo inputInfo = paramBean.getParam().getInputInfo();

		boolean needAttachFile = AmResourceSelectionUtils.attachFileRequired();

		// If check-in attach file is required, but no uploaded file => return error
		if (TriStringUtils.isEmpty(inputInfo.getAttachmentFilePath())
				&& needAttachFile) {
			throw new ContinuableBusinessException( AmMessageId.AM001074E  );
		}

		this.beforeExecution(paramBean, serviceBean, null);

		// Call check-in confirmation service
		{
			serviceBean.setForward(ChaLibScreenID.RTN_ENTRY_CHECK);
			serviceBean.setReferer(ChaLibScreenID.RTN_ENTRY_CONFIRM);
			checkinConfirmationService.execute(dto);
		}

		this.afterExecution(serviceBean, null, paramBean);
	}

	private void beforeExecution(FlowCheckinRequestServiceBean src, FlowChaLibRtnEntryServiceBean entryDst, FlowChaLibRtnDetailViewServiceBean detailDst) {

		if ( RequestType.init.equals(src.getParam().getRequestType()) ) {
			detailDst.setApplyNo(src.getParam().getSelectedAreqId());
		}

		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			entryDst.setProcId(src.getProcId());
			entryDst.setApplyNo(src.getParam().getSelectedAreqId());
			entryDst.setDuplicationCheck(true);
		}
	}

	private void afterExecution(FlowChaLibRtnEntryServiceBean entrySrc, FlowChaLibRtnDetailViewServiceBean detailSrc, FlowCheckinRequestServiceBean dest) {

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {

			Set<String> selectedFileSet = dest.getParam().getResourceSelection().getSelectedFileSet();
			//ChaLibEntryBaseInfoBean infoBean = detailSrc.getBaseInfoViewBean();

			// Get selected files
			List<Object> paramList = new ArrayList<Object>();

			IAreqDto areqDto = new AreqDto();
			IAreqEntity areqEntity = this.support.findAreqEntity(dest.getParam().getSelectedAreqId());
			areqDto.setAreqEntity(areqEntity);
			IPjtEntity pjtEntity = this.support.findPjtEntity(areqDto.getAreqEntity().getPjtId());
			ILotDto lotDto = new LotDto();
			ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
			lotDto.setLotEntity(lotEntity);

			// Set basic information
			SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( dest.getLanguage(), dest.getTimeZone() );
			SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( dest.getLanguage(), dest.getTimeZone() );

			dest.getDetailsView()
				.setLotId			( areqEntity.getLotId() )
				.setAreqId			( areqEntity.getAreqId() )
				.setPjtId			( areqEntity.getPjtId() )
				.setReferenceId		( areqEntity.getChgFactorNo() )
				.setReferenceCategoryNm(pjtEntity.getChgFactorId())
				.setSubmitterNm		( areqEntity.getLendReqUserNm() )
				.setSubmitterIconPath( support.getUmFinderSupport().getIconPath( areqEntity.getLendReqUserId() ) )
				.setAssigneeNm		( areqEntity.getAssigneeNm() )
				.setAssigneeIconPath( support.getUmFinderSupport().getIconPath( areqEntity.getAssigneeId() ) )
				.setGroupNm			( areqEntity.getGrpNm() )
				.setSubject			( areqEntity.getSummary() )
				.setContents		( areqEntity.getContent() )
				.setCheckoutTime	( TriDateUtils.convertViewDateFormat(areqEntity.getLendReqTimestamp(), formatYMDHM) )
				.setCheckinDueDate	( TriDateUtils.convertViewDateFormat(areqEntity.getRtnReqDueDate(), TriDateUtils.getYMDDateFormat(), formatYMD) )
				.setCtgNm			( areqEntity.getCtgNm() )
				.setMstoneNm		( areqEntity.getMstoneNm() )
				.setUpdTime			( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMDHM) )
				.setStsId			( areqEntity.getProcStsId() )
				.setStatus			( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) )
				.setCheckoutPath	( AmDesignBusinessRuleUtils.getSharePublicPathOfCheckoutRequest(areqEntity) )
				.setCheckinPath		( AmDesignBusinessRuleUtils.getSharePublicPathOfCheckinRequest(areqEntity) )
				;

			paramList.add( lotDto );
			paramList.add( areqDto );
			File masterPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

			ChaLibEntryAssetResourceInfoBean assetInfoBean = detailSrc.getAssetSelectViewBean();

			if (TriStringUtils.isNotEmpty(assetInfoBean.getAssetResourceViewBeanList())) {
				for (ChaLibAssetResourceViewBean viewBean : assetInfoBean.getAssetResourceViewBeanList()) {
					if (viewBean.isFile()) {
						selectedFileSet.add(TriStringUtils.convertRelativePath(masterPath, viewBean.getResourcePath(), true));
					}
				}
			}
		}

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003019I);
		}
	}

	private int insertAttachment(FlowCheckinRequestServiceBean paramBean) {
		String areqId = paramBean.getParam().getSelectedAreqId();
		String filePath = paramBean.getParam().getInputInfo().getAttachmentFileNm();
		String attachedFileSeqNo = paramBean.getParam().getInputInfo().getAttachmentFileSeqNo();

		IAreqAttachedFileEntity entity = new AreqAttachedFileEntity();
		entity.setAreqId(areqId);
		entity.setAttachedFileSeqNo(attachedFileSeqNo);
		entity.setFilePath(filePath);

		return this.support.getAreqAttachedFileDao().insert(entity);
	}

}
