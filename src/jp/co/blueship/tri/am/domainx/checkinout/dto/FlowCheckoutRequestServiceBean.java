package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckoutRequestServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CheckoutRequestDetailsView detailsView = new CheckoutRequestDetailsView();
	private ResourceSelectionFolderView<ICheckoutResourceViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<ICheckoutResourceViewBean>();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibLendEntryServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public CheckoutRequestDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowCheckoutRequestServiceBean setDetailsView(CheckoutRequestDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public ResourceSelectionFolderView<ICheckoutResourceViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}
	public FlowCheckoutRequestServiceBean setResourceSelectionFolderView(ResourceSelectionFolderView<ICheckoutResourceViewBean> folderView) {
		this.resourceSelectionFolderView = folderView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckoutRequestServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private CheckoutRequestEditInputBean inputInfo = new CheckoutRequestEditInputBean();
		private RequestOption requestOption = RequestOption.none;
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public CheckoutRequestEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckoutRequestEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}
		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		selectResource( "selectResource" ),
		fileUpload( "fileUpload" ),
		selectAssignee( "selectAssignee" ),
		selectGroup( "selectGroup" ),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone")
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}

	public class CheckoutRequestDetailsView {
		private String groupNm;
		private String submitterNm;
		private String assigneeNm;
		private String ctgNm;
		private String mstoneNm;

		public String getGroupNm() {
			return groupNm;
		}
		public CheckoutRequestDetailsView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public CheckoutRequestDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
		public String getAssigneeNm() {
			return assigneeNm;
		}
		public CheckoutRequestDetailsView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}
		public String getCtgNm() {
			return ctgNm;
		}
		public CheckoutRequestDetailsView setCtgNm(String ctgNm) {
			this.ctgNm = ctgNm;
			return this;
		}
		public String getMstoneNm() {
			return mstoneNm;
		}
		public CheckoutRequestDetailsView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String areqId;
		private boolean completed = false;

		public String getAreqId() {
			return areqId;
		}
		public RequestsCompletion setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
