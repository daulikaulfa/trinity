package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRemovalRequestOverviewServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CheckinOutRequestOverview detailsView = new CheckinOutRequestOverview();

	public RequestParam getParam() {
		return param;
	}

	public CheckinOutRequestOverview getDetailsView() {
		return detailsView;
	}
	public FlowRemovalRequestOverviewServiceBean setDetailsView(CheckinOutRequestOverview detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}
	}

	public class CheckinOutRequestOverview {
		private String subject;
		private String stsId;
		private String status;
		private String submitterNm;
		private String assigneeNm;
		private String requestTime;

		public String getSubject() {
			return subject;
		}
		public CheckinOutRequestOverview setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public CheckinOutRequestOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public CheckinOutRequestOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public CheckinOutRequestOverview setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public CheckinOutRequestOverview setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getRequestTime() {
			return requestTime;
		}
		public CheckinOutRequestOverview setRequestTime(String requestTime) {
			this.requestTime = requestTime;
			return this;
		}
	}
}
