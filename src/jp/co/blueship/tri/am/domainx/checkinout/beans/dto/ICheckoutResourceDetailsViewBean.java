package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 * @version V4.00.00
 * @author Sam
 */
public interface ICheckoutResourceDetailsViewBean extends IResourceViewBean {
}
