package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean.ResourceLineView;
import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean.ResourcePropertyView;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRequestResourceFileComparisonServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

public class FlowRequestResourceFileComparisonService implements IDomain<FlowRequestResourceFileComparisonServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support = null;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}


	@Override
	public IServiceDto<FlowRequestResourceFileComparisonServiceBean> execute(
			IServiceDto<FlowRequestResourceFileComparisonServiceBean> serviceDto) {

		FlowRequestResourceFileComparisonServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}



	private void init(FlowRequestResourceFileComparisonServiceBean serviceBean){

		String areqId = serviceBean.getParam().getSelectedAreqId();
		String resourcePath = serviceBean.getParam().getSelectedResourcePath();

		IAreqEntity assetApplyEntity = this.support.findAreqEntity( areqId );
		IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() ) ;
		ILotDto lotDto = this.support.findLotDto(pjtEntity.getLotId() );

		List<Object> paramList = new ArrayList<Object>() ;
		paramList.add( lotDto);

		File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath(paramList);
		File returnInfoPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList , assetApplyEntity );

		File srcFile = new File( TriStringUtils.linkPathBySlash( masterWorkPath.getAbsolutePath() , resourcePath ) ) ;
		File dstFile = new File( TriStringUtils.linkPathBySlash( returnInfoPath.getAbsolutePath() , resourcePath ) ) ;


		//input ResourceProperty
		ResourcePropertyView dstResource =  serviceBean.getResourceComparisonView().getDestResource()
			.setResourcePath		(resourcePath)
			.setSubmitterNm			(assetApplyEntity.getRegUserNm())
			.setResourceUpdTime		(TriDateUtils.convertViewDateFormat(
										(new Timestamp( dstFile.lastModified() )),
										TriDateUtils.getYMDHMSDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
										).toString() )
			;

		serviceBean.getResourceComparisonView().setDestResource(dstResource);

		ResourcePropertyView srcResource =  serviceBean.getResourceComparisonView().getSrcResource()
			.setResourcePath		(resourcePath)
			.setSubmitterNm			("")
			.setResourceUpdTime		(TriDateUtils.convertViewDateFormat(
										(new Timestamp( srcFile.lastModified() )),
										TriDateUtils.getYMDHMSDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
										).toString() )
			;

		serviceBean.getResourceComparisonView().setSrcResource(srcResource);


		//diff
		EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer( srcFile , dstFile ) ;

		differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

		DiffResult diffResult = differ.diff() ;

		List<ResourceLineView> lines = serviceBean.getResourceComparisonView().getLine();
		for(IDiffElement resultElement : diffResult.getDiffElementList()){

			FileStatus fileStatus = null;
			Status status  = resultElement.getStatus();

			if		(Status.ADD.equals(status))		fileStatus = FileStatus.Add;
			else if (Status.DELETE.equals(status))	fileStatus = FileStatus.Delete;
			else if (Status.CHANGE.equals(status))	fileStatus = FileStatus.Edit;
			else if (Status.EQUAL.equals(status))	fileStatus = FileStatus.Same;


			ResourceLineView lineView = serviceBean.getResourceComparisonView().new ResourceLineView()
					.setStatus			(fileStatus)
					.setDestLineNo		(resultElement.getDstLineNumber()+"")
					.setSrcLineNo		(resultElement.getSrcLineNumber()+"")
					.setDest			(resultElement.getDstContents())
					.setSrc				(resultElement.getSrcContents())
					;

			lines.add(lineView);
		}

		serviceBean.getResourceComparisonView().setLine(lines);
	}
}
