package jp.co.blueship.tri.am.domainx.checkinout;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestRemovalServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestRemovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowCheckinRequestRemovalService implements IDomain<FlowCheckinRequestRemovalServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support;

	public void setSupport( FlowChaLibRtnEditSupport support ) {
		this.support = support;
	}

	private ActionStatusMatrixList statusMatrixAction = null;
	private IDomain<IGeneralServiceBean> checkinCancelService = null;
	private IDomain<IGeneralServiceBean> checkoutRemovalService = null;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setCheckinCancelService(IDomain<IGeneralServiceBean> checkinCancelService) {
		this.checkinCancelService = checkinCancelService;
	}

	public void setCheckoutRemovalService(IDomain<IGeneralServiceBean> checkoutRemovalService) {
		this.checkoutRemovalService = checkoutRemovalService;
	}

	@Override
	public IServiceDto<FlowCheckinRequestRemovalServiceBean> execute(
			IServiceDto<FlowCheckinRequestRemovalServiceBean> serviceDto) {

		FlowCheckinRequestRemovalServiceBean paramBean = serviceDto.getServiceBean();

		try {

			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty( areqId ) , "SelectedAreaId is not specified" );

            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
                this.submitChanges(paramBean);
            }

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void submitChanges(FlowCheckinRequestRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowCheckinRequestApprovalPendingCancellationServiceBean cancelServiceBean = paramBean.getCheckinCancelServiceBean();
		FlowCheckoutRequestRemovalServiceBean removalServiceBean = paramBean.getCheckoutRemovalServiceBean();

		TriPropertyUtils.copyProperties(cancelServiceBean, paramBean);
		GenericServiceBean innerBeanTemp = 	removalServiceBean.getInnerService();
		TriPropertyUtils.copyProperties(removalServiceBean, paramBean);
		removalServiceBean.setInnerService(innerBeanTemp);

		String areqId = paramBean.getParam().getSelectedAreqId();
		AmItemChkUtils.checkApplyNo(areqId);

		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqEntity.getPjtId() );

		{
			String selectedLotId = pjtEntity.getLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( selectedLotId )
					.setAreqIds( areqEntity.getAreqId() )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

			ILotDto lotDto = this.support.findLotDto( selectedLotId, AmTables.AM_LOT_GRP_LNK);

			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
		}

		this.beforeExecution(paramBean, cancelServiceBean, removalServiceBean);

		// Call Check-in Cancellation
		{
			dto.setServiceBean(cancelServiceBean);
			checkinCancelService.execute(dto);
		}

		// Call Check-out Removal
		{
			dto.setServiceBean(removalServiceBean);
			checkoutRemovalService.execute(dto);
		}

		this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqEntity.getAreqId());

		this.afterExecution(cancelServiceBean, removalServiceBean, paramBean);
	}

	private void beforeExecution(FlowCheckinRequestRemovalServiceBean src,
			FlowCheckinRequestApprovalPendingCancellationServiceBean cancelDest,
			FlowCheckoutRequestRemovalServiceBean removalDest) {

		// Cancellation Service Bean
		cancelDest.getParam().setSelectedAreqId(src.getParam().getSelectedAreqId());
		cancelDest.getParam().setRequestType(RequestType.submitChanges);
		cancelDest.setProcId(src.getProcId());

		// Check-out Removal Service Bean
		removalDest.getParam().setSelectedAreqId(src.getParam().getSelectedAreqId());
		removalDest.getParam().setRequestType(RequestType.submitChanges);
		removalDest.setProcId(src.getProcId());
	}

	private void afterExecution(FlowCheckinRequestApprovalPendingCancellationServiceBean cancelSrc,
			FlowCheckoutRequestRemovalServiceBean removalSrc,
			FlowCheckinRequestRemovalServiceBean dest) {

		dest.getResult().setCompleted(true);
		dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003011I);
	}

}
