package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import com.google.gson.annotations.Expose;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class CheckinResourceDetailsViewBean implements ICheckinResourceDetailsViewBean {
	@Expose	private boolean directory;
	@Expose	private String path;
	@Expose	private String name;
	@Expose	private String size;
	@Expose private int count = 0;
	@Expose	private String stsId;
	@Expose	private boolean diff = false;

	@Override
	public boolean isFile() {
		return !directory;
	}
	@Override
	public boolean isDirectory() {
		return directory;
	}
	public CheckinResourceDetailsViewBean setDirectory(boolean directory) {
		this.directory = directory;
		return this;
	}
	@Override
	public String getPath() {
		return this.path;
	}
	public CheckinResourceDetailsViewBean setPath(String path) {
		this.path = path;
		return this;
	}
	@Override
	public String getName() {
		return this.name;
	}
	public CheckinResourceDetailsViewBean setName(String name) {
		this.name = name;
		return this;
	}
	@Override
	public String getSize() {
		return this.size;
	}
	public CheckinResourceDetailsViewBean setSize(String size) {
		this.size = size;
		return this;
	}
	@Override
	public int getCount() {
		return count;
	}
	public CheckinResourceDetailsViewBean setCount(int count) {
		this.count = count;
		return this;
	}
	@Override
	public String getStsId() {
		return stsId;
	}
	public CheckinResourceDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}
	@Override
	public boolean isDiff() {
		return this.diff;
	}
	public CheckinResourceDetailsViewBean setDiff(boolean diff) {
		this.diff = diff;
		return this;
	}
}
