package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestApprovalPendingCancellationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckinRequestApprovalPendingCancellationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private CheckinRequestCancellationInputBean inputInfo = new CheckinRequestCancellationInputBean();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String selectedAreqId) {
			this.areqId = selectedAreqId;
			return this;
		}

		public CheckinRequestCancellationInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckinRequestCancellationInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

	}

	/**
	 *
	 * Input Information
	 *
	 */
	public class CheckinRequestCancellationInputBean {
		private String comment;

		public String getComment() {
			return comment;
		}
		public CheckinRequestCancellationInputBean setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
