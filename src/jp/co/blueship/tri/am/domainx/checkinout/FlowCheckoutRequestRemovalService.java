package jp.co.blueship.tri.am.domainx.checkinout;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompLendCancelServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestRemovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */

public class FlowCheckoutRequestRemovalService implements IDomain<FlowCheckoutRequestRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService){
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService){
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowCheckoutRequestRemovalServiceBean> execute( IServiceDto<FlowCheckoutRequestRemovalServiceBean> serviceDto ) {

		FlowCheckoutRequestRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibCompLendCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty( areqId ) , "SelectedAreaId is not specified" );

            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
                this.submitChanges(paramBean);
            }

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}
		return serviceDto;
	}

	private void submitChanges(FlowCheckoutRequestRemovalServiceBean paramBean){

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibCompLendCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		String areqId = paramBean.getParam().getSelectedAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( areqEntity.getLotId() )
				.setAreqIds( areqId );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer(ChaLibScreenID.LEND_DETAIL_VIEW);
		{
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_CANCEL);
			completeService.execute(dto);

			if ( !AmAreqStatusId.DraftCheckoutRequest.equals(areqEntity.getStsId()) ) {
				mailService.execute(dto);
			}
		}

		if(TriView.CheckinOutRequestList.value().equals(paramBean.getReferer()) && AmAreqStatusId.DraftCheckoutRequest.equals(areqEntity.getStsId())) {
			paramBean.getResult().setRedirectToDraft(true);
		}
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003016I);
	}

	private void beforeExecution(FlowCheckoutRequestRemovalServiceBean src, FlowChaLibCompLendCancelServiceBean dest) {
		src.getResult().setCompleted(false);

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setApplyNo(src.getParam().getSelectedAreqId());
		}
	}
}

