package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceExportToFileBean;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;



public class FlowCheckinOutRequestListServiceExportToFile implements IDomain<FlowCheckInOutRequestListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> checkinoutRequestList = null;

	public void setCheckinoutRequestList(IDomain<IGeneralServiceBean> checkinoutRequestList) {
		this.checkinoutRequestList = checkinoutRequestList;
	}


	@Override
	public IServiceDto<FlowCheckInOutRequestListServiceExportToFileBean> execute(
			IServiceDto<FlowCheckInOutRequestListServiceExportToFileBean> serviceDto) {
		FlowCheckInOutRequestListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowCheckInOutRequestListServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if(RequestType.onChange.equals(paramBean.getParam().getRequestType())){
				this.onChange(paramBean);
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowCheckInOutRequestListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowCheckInOutRequestListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowCheckInOutRequestListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			checkinoutRequestList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		checkinoutRequestList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowCheckInOutRequestListServiceExportToFileBean src, FlowCheckInOutRequestListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		SearchCondition srcDraftSearchCondition = src.getParam().getSearchDraftCondition();
		SearchCondition destDraftSearchCondition = dest.getParam().getSearchDraftCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);
			if(src.getParam().isSelectedDraft()){
				dest.getParam().setSelectedDraft(true);
				destDraftSearchCondition.setStsId(srcDraftSearchCondition.getStsId());
				destDraftSearchCondition.setAreqCtgCd(srcDraftSearchCondition.getAreqCtgCd());
				destDraftSearchCondition.setCtgId(srcDraftSearchCondition.getCtgId());
				destDraftSearchCondition.setMstoneId(srcDraftSearchCondition.getMstoneId());
				destDraftSearchCondition.setKeyword(srcDraftSearchCondition.getKeyword());
				destDraftSearchCondition.setSelectedPageNo(1);
			} else{
				dest.getParam().setSelectedDraft(false);
				destSearchCondition.setStsId(srcSearchConditon.getStsId());
				destSearchCondition.setAreqCtgCd(srcSearchConditon.getAreqCtgCd());
				destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
				destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
				destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
				destSearchCondition.setSelectedPageNo(1);
			}
				dest.getParam().setLinesPerPage(0);
		}
	}

	private void afterExecution(FlowCheckInOutRequestListServiceBean src, FlowCheckInOutRequestListServiceExportToFileBean dest) {
		List<AreqView> srcAreqViews = src.getAreqViews();
		List<AreqView> destAreqViews = new ArrayList<AreqView>();

		List<DraftAreqView> srcDraftAreqView = src.getDraftAreqViews();
		List<DraftAreqView> destDraftAreqView = new ArrayList<DraftAreqView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			if(dest.getParam().isSelectedDraft()){
				for (int i = 0; i < srcDraftAreqView.size(); i++) {
					DraftAreqView view = srcDraftAreqView.get(i);
					destDraftAreqView.add(view);
				}
				dest.setDraftAreqViews(destDraftAreqView);
			}else{
				for (int i = 0; i < srcAreqViews.size(); i++) {
					AreqView view = srcAreqViews.get(i);
					destAreqViews.add(view);
				}
				dest.setAreqViews(destAreqViews);
			}
		}
	}
}
