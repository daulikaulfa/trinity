package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowCheckoutRequestDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CheckoutRequestDetailsViewBean detailsView = new CheckoutRequestDetailsViewBean();
	private ResourceSelectionFolderView<ICheckoutResourceDetailsViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<ICheckoutResourceDetailsViewBean>();

	private List<IAreqFileEntity> cacheAreqFileEntities = new ArrayList<IAreqFileEntity>();

	public List<IAreqFileEntity> getCacheAreqFileEntities() {
		return cacheAreqFileEntities;
	}

	public FlowCheckoutRequestDetailsServiceBean setCacheAreqFileEntities(List<IAreqFileEntity> cacheAreqFileEntities) {
		this.cacheAreqFileEntities = cacheAreqFileEntities;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	public ResourceSelectionFolderView<ICheckoutResourceDetailsViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}
	public FlowCheckoutRequestDetailsServiceBean setResourceSelectionFolderView(ResourceSelectionFolderView<ICheckoutResourceDetailsViewBean> folderView) {
		this.resourceSelectionFolderView = folderView;
		return this;
	}

	public CheckoutRequestDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowCheckoutRequestDetailsServiceBean setDetailsView(CheckoutRequestDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private CheckoutRequestDetailsInputInfo inputInfo = new CheckoutRequestDetailsInputInfo();
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public CheckoutRequestDetailsInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckoutRequestDetailsInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}

		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}
	}

	public class CheckoutRequestDetailsInputInfo {
		private boolean isFolderTreeFormat = true;

		public boolean isFolderTreeFormat() {
			return this.isFolderTreeFormat;
		}
		public CheckoutRequestDetailsInputInfo setFolderTreeFormat( boolean isFolderTreeFormat ) {
			this.isFolderTreeFormat = isFolderTreeFormat;
			return this;
		}
	}
}
