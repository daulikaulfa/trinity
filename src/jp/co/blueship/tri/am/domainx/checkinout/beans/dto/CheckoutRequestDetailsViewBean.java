package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Sam
 */
public class CheckoutRequestDetailsViewBean {

	private String lotId;
	private String lotStsId;
	private String lotStatus;
	private String areqId;
	private String pjtId;
	private String pjtStsId;
	private String pjtStatus;
	private String referenceId;
	private String groupNm;
	private String submitterNm;
	private String submitterIconPath;
	private String assigneeNm;
	private String assigneeIconPath;
	private String subject;
	private String contents;
	private String checkoutTime;
	private String checkinDueDate;
	private String ctgNm;
	private String mstoneNm;
	private String updTime;
	private String stsId;
	private String status;
	private String checkoutPath;
	private String checkinPath;
	private boolean isCheckinEnabled = false;

	public String getLotId(){
		return lotId;
	}
	public CheckoutRequestDetailsViewBean setLotId(String lotId){
		this.lotId = lotId;
		return this;
	}

	public String getLotStsId() {
		return lotStsId;
	}
	public CheckoutRequestDetailsViewBean setLotStsId(String lotStsId) {
		this.lotStsId = lotStsId;
		return this;
	}
	public String getLotStatus() {
		return lotStatus;
	}
	public CheckoutRequestDetailsViewBean setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
		return this;
	}

	public String getAreqId() {
		return areqId;
	}
	public CheckoutRequestDetailsViewBean setAreqId(String areqId) {
		this.areqId = areqId;
		return this;
	}

	public String getPjtId() {
		return pjtId;
	}
	public CheckoutRequestDetailsViewBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}

	public String getPjtStsId() {
		return pjtStsId;
	}
	public CheckoutRequestDetailsViewBean setPjtStsId(String pjtStsId) {
		this.pjtStsId = pjtStsId;
		return this;
	}
	public String getPjtStatus() {
		return pjtStatus;
	}
	public CheckoutRequestDetailsViewBean setPjtStatus(String pjtStatus) {
		this.pjtStatus = pjtStatus;
		return this;
	}

	public String getReferenceId() {
		return referenceId;
	}
	public CheckoutRequestDetailsViewBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}

	public String getGroupNm() {
		return groupNm;
	}
	public CheckoutRequestDetailsViewBean setGroupNm(String groupNm) {
		this.groupNm = groupNm;
		return this;
	}

	public String getSubmitterNm() {
		return submitterNm;
	}
	public CheckoutRequestDetailsViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}
	public CheckoutRequestDetailsViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}

	public String getAssigneeNm() {
		return assigneeNm;
	}
	public CheckoutRequestDetailsViewBean setAssigneeNm(String AssigneeNm) {
		this.assigneeNm = AssigneeNm;
		return this;
	}

	public String getAssigneeIconPath() {
		return assigneeIconPath;
	}
	public CheckoutRequestDetailsViewBean setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
		return this;
	}

	public String getSubject() {
		return subject;
	}

	public CheckoutRequestDetailsViewBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getContents() {
		return contents;
	}
	public CheckoutRequestDetailsViewBean setContents(String contents) {
		this.contents = contents;
		return this;
	}

	public String getCheckoutTime() {
		return checkoutTime;
	}
	public CheckoutRequestDetailsViewBean setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
		return this;
	}

	public String getCheckinDueDate() {
		return checkinDueDate;
	}
	public CheckoutRequestDetailsViewBean setCheckinDueDate(String checkinDueDate) {
		this.checkinDueDate = checkinDueDate;
		return this;
	}

	public String getCtgNm() {
		return ctgNm;
	}
	public CheckoutRequestDetailsViewBean setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		return this;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}
	public CheckoutRequestDetailsViewBean setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		return this;
	}

	public String getUpdTime() {
		return updTime;
	}
	public CheckoutRequestDetailsViewBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}

	public String getStsId() {
		return stsId;
	}
	public CheckoutRequestDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}

	public String getStatus() {
		return status;
	}
	public CheckoutRequestDetailsViewBean setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getCheckoutPath() {
		return checkoutPath;
	}
	public CheckoutRequestDetailsViewBean setCheckoutPath(String checkoutPath) {
		this.checkoutPath = checkoutPath;
		return this;
	}

	public String getCheckinPath() {
		return checkinPath;
	}
	public CheckoutRequestDetailsViewBean setCheckinPath(String checkinPath) {
		this.checkinPath = checkinPath;
		return this;
	}

	public boolean isCheckinEnabled() {
		return isCheckinEnabled;
	}
	public CheckoutRequestDetailsViewBean setCheckinEnabled(boolean isCheckinEnabled) {
		this.isCheckinEnabled = isCheckinEnabled;
		return this;
	}

}
