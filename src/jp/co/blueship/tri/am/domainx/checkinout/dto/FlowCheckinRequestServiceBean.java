package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CheckinRequestDetailsView detailsView = new CheckinRequestDetailsView();
	private ResourceSelectionFolderView<ICheckinResourceViewBean> resourceSelectionFolderView = new ResourceSelectionFolderView<ICheckinResourceViewBean>();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibRtnEntryServiceBean() );
		this.setInnerViewService( new FlowChaLibRtnDetailViewServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public CheckinRequestDetailsView getDetailsView() {
		return detailsView;
	}

	public FlowCheckinRequestServiceBean setDetailsView(CheckinRequestDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public ResourceSelectionFolderView<ICheckinResourceViewBean> getResourceSelectionFolderView() {
		return resourceSelectionFolderView;
	}

	public FlowCheckinRequestServiceBean setResourceSelectionFolderView(
			ResourceSelectionFolderView<ICheckinResourceViewBean> resourceSelectionFolderView) {
		this.resourceSelectionFolderView = resourceSelectionFolderView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckinRequestServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private CheckinRequestInputInfo inputInfo = new CheckinRequestInputInfo();
		private RequestOption requestOption = RequestOption.none;
		private ResourceSelection resourceSelection = new ResourceSelection();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public CheckinRequestInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckinRequestInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public ResourceSelection getResourceSelection() {
			return resourceSelection;
		}
		public RequestParam setResourceSelection(ResourceSelection resourceSelection) {
			this.resourceSelection = resourceSelection;
			return this;
		}

	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		selectResource( "selectResource" ),
		fileUpload( "fileUpload" ),
		deleteUplodedFile( "deleteFile" ),
		refresh( "refresh" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}

	/**
	 * Input Information
	 */
	public class CheckinRequestInputInfo {
		private String attachmentFilePath;
		private String attachmentFileNm;
		private String attachmentFileSeqNo;
		private byte[] attachmentInputStreamBytes;

		private boolean isFolderTreeFormat = true;

		public String getAttachmentFilePath() {
			return attachmentFilePath;
		}
		public CheckinRequestInputInfo setAttachmentFilePath(String attachmentFilePath) {
			this.attachmentFilePath = attachmentFilePath;
			return this;
		}
		public String getAttachmentFileNm() {
			return attachmentFileNm;
		}
		public CheckinRequestInputInfo setAttachmentFileNm(String attachmentFileNm) {
			this.attachmentFileNm = attachmentFileNm;
			return this;
		}
		public String getAttachmentFileSeqNo() {
			return attachmentFileSeqNo;
		}
		public CheckinRequestInputInfo setAttachmentFileSeqNo(String attachmentFileSeqNo) {
			this.attachmentFileSeqNo = attachmentFileSeqNo;
			return this;
		}
		public byte[] getAttachmentInputStreamBytes() {
			return attachmentInputStreamBytes;
		}
		public CheckinRequestInputInfo setAttachmentInputStreamBytes(byte[] attachmentInputStreamBytes) {
			this.attachmentInputStreamBytes = attachmentInputStreamBytes;
			return this;
		}

		public boolean isFolderTreeFormat() {
			return this.isFolderTreeFormat;
		}
		public CheckinRequestInputInfo setFolderTreeFormat( boolean isFolderTreeFormat ) {
			this.isFolderTreeFormat = isFolderTreeFormat;
			return this;
		}
	}

	/**
	 * Check-in Request Information
	 */
	public class CheckinRequestDetailsView {
		private String lotId;
		private String areqId;
		private String pjtId;
		private String referenceId;
		private String referenceCategoryNm;
		private String submitterNm;
		private String submitterIconPath;
		private String groupId;
		private String groupNm;
		private String subject;
		private String contents;
		private String assigneeNm;
		private String assigneeIconPath;
		private String ctgNm;
		private String mstoneNm;
		private String checkoutTime;
		private String checkinDueDate;
		private String updTime;
		private String stsId;
		private String status;
		private String checkoutPath;
		private String checkinPath;

		public String getLotId() {
			return lotId;
		}
		public CheckinRequestDetailsView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public String getAreqId() {
			return areqId;
		}
		public CheckinRequestDetailsView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}
		public String getPjtId() {
			return pjtId;
		}
		public CheckinRequestDetailsView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getReferenceId() {
			return referenceId;
		}
		public CheckinRequestDetailsView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}
		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public CheckinRequestDetailsView setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public CheckinRequestDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public CheckinRequestDetailsView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		public String getGroupId() {
			return groupId;
		}
		public CheckinRequestDetailsView setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}
		public String getGroupNm() {
			return groupNm;
		}
		public CheckinRequestDetailsView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public String getSubject() {
			return subject;
		}
		public CheckinRequestDetailsView setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		public String getContents() {
			return contents;
		}
		public CheckinRequestDetailsView setContents(String contents) {
			this.contents = contents;
			return this;
		}
		public String getAssigneeNm() {
			return assigneeNm;
		}
		public CheckinRequestDetailsView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}
		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public CheckinRequestDetailsView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}
		public String getCtgNm() {
			return ctgNm;
		}
		public CheckinRequestDetailsView setCtgNm(String ctgNm) {
			this.ctgNm = ctgNm;
			return this;
		}
		public String getMstoneNm() {
			return mstoneNm;
		}
		public CheckinRequestDetailsView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}
		public String getCheckoutTime() {
			return checkoutTime;
		}
		public CheckinRequestDetailsView setCheckoutTime(String checkoutTime) {
			this.checkoutTime = checkoutTime;
			return this;
		}
		public String getCheckinDueDate() {
			return checkinDueDate;
		}
		public CheckinRequestDetailsView setCheckinDueDate(String checkinDueDate) {
			this.checkinDueDate = checkinDueDate;
			return this;
		}
		public String getUpdTime() {
			return updTime;
		}
		public CheckinRequestDetailsView setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public CheckinRequestDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public CheckinRequestDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getCheckoutPath() {
			return checkoutPath;
		}
		public CheckinRequestDetailsView setCheckoutPath(String checkoutPath) {
			this.checkoutPath = checkoutPath;
			return this;
		}
		public String getCheckinPath() {
			return checkinPath;
		}
		public CheckinRequestDetailsView setCheckinPath(String checkinPath) {
			this.checkinPath = checkinPath;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
