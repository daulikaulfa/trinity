package jp.co.blueship.tri.am.domainx.checkinout;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.constants.ChaLibCallScriptItemID;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestProcessingStatusServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean.Task;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean.Timeline;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.contants.SmProcCtgCd;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcDetailsItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsCondition;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtCondition;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;
import jp.co.blueship.tri.fw.svc.beans.dto.ProgressBar;

/**
 *
 * Provide the following backend services.
 * <br> - ProcessingStatus
 *
 * @version V4.00.00
 * @author Satoshi Sasaki
 */
public class FlowCheckinRequestProcessingStatusService implements IDomain<FlowCheckinRequestProcessingStatusServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowCheckinRequestProcessingStatusServiceBean> execute(
			IServiceDto<FlowCheckinRequestProcessingStatusServiceBean> serviceDto) {
		FlowCheckinRequestProcessingStatusServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 *
	 * @param serviceBean
	 *            Service Bean
	 */
	private void init(FlowCheckinRequestProcessingStatusServiceBean serviceBean) {
		TaskDetailsViewBean detailsView = serviceBean.getDetailsView();
		String areqId = serviceBean.getParam().getAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity( areqId );

		ILotDto lotDto = this.support.findLotDto(areqEntity.getLotId(), AmTables.AM_LOT);
		ILotEntity lotEntity = lotDto.getLotEntity();

		serviceBean.getOverview()
			.setLotId			( lotEntity.getLotId() )
			.setLotStsId		( lotEntity.getStsId() )
			.setLotStatus		( sheet.getValue(AmDesignBeanId.statusId, lotEntity.getProcStsId()) )
			.setAreqId			( areqId )
			.setStsId			( areqEntity.getProcStsId() )
			.setStatus			( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) )
			.setSubmitterNm		( areqEntity.getRegUserNm() )
			.setSubmitterIconPath( support.getUmFinderSupport().getIconPath(areqEntity.getRegUserId()) )
			.setGroupNm			( areqEntity.getGrpNm() )
		;

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		List<Timeline> timeLines = new ArrayList<Timeline>();
		{
			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				task.setTaskNm( ca.getValue( SmProcCtgCd.DUPLICATION_FILE_CHECK.getMessageKey(), serviceBean.getLanguage() ) );
				task.setResult(ProcessStatus.Active);
				IProgressBar bar = new ProgressBar();
				bar.setValue(100);
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(1);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);
		}
		{
			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				task.setTaskNm( ca.getValue( SmProcCtgCd.RETURN_FILE_COPY.getMessageKey(), serviceBean.getLanguage() ) );
				task.setResult(ProcessStatus.none);
				IProgressBar bar = new ProgressBar();
				bar.setValue(0);
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(2);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);
		}
		{
			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				task.setTaskNm( ca.getValue( SmProcCtgCd.EXTENSION_CHECK.getMessageKey(), serviceBean.getLanguage() ) );
				task.setResult(ProcessStatus.none);
				IProgressBar bar = new ProgressBar();
				bar.setValue(0);
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(3);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);
		}
		if( !StatusFlg.off.value().equals(sheet.getValue(AmDesignBeanId.callScriptAtReturnApply, ChaLibCallScriptItemID.SummaryAtReturnApply.DO_EXEC.toString()))) {
			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				task.setTaskNm( ca.getValue( SmProcCtgCd.CALL_SCRIPT.getMessageKey(), serviceBean.getLanguage() ) );
				task.setResult(ProcessStatus.none);
				IProgressBar bar = new ProgressBar();
				bar.setValue(0);
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(4);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);

		}
		serviceBean.getDetailsView().setTimeLines(timeLines);

	}

	private void onChange(FlowCheckinRequestProcessingStatusServiceBean serviceBean) {
		String areqId = serviceBean.getParam().getAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity( areqId );

		ILotDto lotDto = this.support.findLotDto(areqEntity.getLotId(), AmTables.AM_LOT);
		ILotEntity lotEntity = lotDto.getLotEntity();

		List<Timeline> timeLines = new ArrayList<Timeline>();
		List<IProcDetailsEntity> entitys = new ArrayList<IProcDetailsEntity>();

		String procId = "";

		ProcMgtCondition procMgtCondition = new ProcMgtCondition();
		procMgtCondition.setLotId( lotEntity.getLotId() );
		procMgtCondition.setServiceId(ServiceId.AmCheckinRequestService.value());
		procMgtCondition.setUserId(serviceBean.getUserId());
		procMgtCondition.setCompStsId((StatusFlg)null);
		procMgtCondition.setDelStsId((StatusFlg)null);

		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcMgtItems.procStTimestamp, TriSortOrder.Desc, 0);

		List<IProcMgtEntity> procMgtEntityList = this.support.getSmFinderSupport().getProcMgtDao().find(procMgtCondition.getCondition() , sort, 1, 1).getEntities();

		if( !procMgtEntityList.isEmpty() )
		{
			procId = procMgtEntityList.get(0).getProcId();
		}

		ProcDetailsCondition condition = new ProcDetailsCondition();
		condition.setProcId(procId);
		sort = new SortBuilder();
		sort.setElement(ProcDetailsItems.procStTimestamp, TriSortOrder.Asc, 0);
		entitys = this.support.getSmFinderSupport().getProcDetailsDao().find(condition.getCondition(), sort);
		TaskDetailsViewBean detailsView = serviceBean.getDetailsView();
		int procMax = 3;
		String enableCallScript = sheet.getValue(AmDesignBeanId.callScriptAtReturnApply, ChaLibCallScriptItemID.SummaryAtReturnApply.DO_EXEC.toString());
		if( !StatusFlg.off.value().equals(enableCallScript))
		{
			procMax = 4;
		}

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		int lineCnt = 1;
		for( IProcDetailsEntity entity : entitys )
		{
			if( lineCnt > procMax )
			{
				break;
			}
			ProcessStatus status = ProcessStatus.Active;

			if ( TriStringUtils.isNotEmpty( entity.getMsgId() ) ) {
				status = ProcessStatus.Error;
				serviceBean.getMessages().add(entity.getMsg());
			} else if ( entity.getCompStsId().parseBoolean() ) {
				status = ProcessStatus.Completed;
			}

			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				task.setTaskNm( ca.getValue( SmProcCtgCd.value( entity.getProcCtgCd() ).getMessageKey(), serviceBean.getLanguage() ) );
				task.setResult( status );
				IProgressBar bar = new ProgressBar();
				if( status.equals(ProcessStatus.none))
				{
					bar.setValue(0);
				} else {
					bar.setValue(100);
				}
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(lineCnt++);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);
		}
		for( int procLp = entitys.size(); procLp<procMax; procLp++ )
		{
			Timeline timeLine = detailsView.new Timeline();
			List<Task> tasks = new ArrayList<Task>();
			{
				Task task = detailsView.new Task();
				String taskName = "";
				switch( procLp )
				{
					case 0:
						taskName = ca.getValue( SmProcCtgCd.DUPLICATION_FILE_CHECK.getMessageKey(), serviceBean.getLanguage() );
						break;
					case 1:
						taskName = ca.getValue( SmProcCtgCd.RETURN_FILE_COPY.getMessageKey(), serviceBean.getLanguage() );
						break;
					case 2:
						taskName = ca.getValue( SmProcCtgCd.EXTENSION_CHECK.getMessageKey(), serviceBean.getLanguage() );
						break;
					case 3:
						taskName = ca.getValue( SmProcCtgCd.CALL_SCRIPT.getMessageKey(), serviceBean.getLanguage() );
						break;
				}
				task.setTaskNm( taskName );
				task.setResult( ProcessStatus.none );
				IProgressBar bar = new ProgressBar();
				bar.setValue(0);
				task.setBar(bar);
				tasks.add(task);
			}
			timeLine.setLineNo(procLp+1);
			timeLine.setTasks(tasks);
			timeLines.add(timeLine);
		}

		if( timeLines.size() > 0 )
		{
			serviceBean.getDetailsView().setTimeLines(timeLines);

		}
	}
}
