package jp.co.blueship.tri.am.domainx.checkinout;

import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceCheckInOutRequestList implements IAmDomain<FlowDashboardServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> service = null;

	public void setService(IDomain<IGeneralServiceBean> service) {
		this.service = service;
	}

	@Override
	public IServiceDto<FlowDashboardServiceBean> execute( IServiceDto<FlowDashboardServiceBean> serviceDto ) {

		FlowDashboardServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( null == serviceDto.getServiceBean().getCheckInOutRequestListBean() ) {
				return serviceDto;
			}

			IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
			dto.setServiceBean( serviceDto.getServiceBean().getCheckInOutRequestListBean() );
			service.execute( dto );
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

}
