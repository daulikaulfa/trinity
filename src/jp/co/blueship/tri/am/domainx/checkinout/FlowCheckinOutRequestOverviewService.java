package jp.co.blueship.tri.am.domainx.checkinout;

import java.text.SimpleDateFormat;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinOutRequestOverviewServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinOutRequestOverviewServiceBean.CheckinOutRequestOverview;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowCheckinOutRequestOverviewService implements IDomain<FlowCheckinOutRequestOverviewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowCheckinOutRequestOverviewServiceBean> execute(
			IServiceDto<FlowCheckinOutRequestOverviewServiceBean> serviceDto) {


		FlowCheckinOutRequestOverviewServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowCheckinOutRequestOverviewServiceBean serviceBean) {
		String areqId = serviceBean.getParam().getSelectedAreqId();

		IAreqDto areqDto = this.support.findAreqDto(areqId, AmTables.AM_AREQ_ATTACHED_FILE);
		IAreqEntity areqEntity = areqDto.getAreqEntity();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(serviceBean.getLanguage(),
				serviceBean.getTimeZone());

		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(),
				serviceBean.getTimeZone());

		CheckinOutRequestOverview view = serviceBean.new CheckinOutRequestOverview()
				.setSubject			( areqEntity.getSummary() )
				.setStsId			( areqEntity.getProcStsId())
				.setStatus			( sheet.getValue( AmDesignBeanId.statusId, areqEntity.getProcStsId() ) )
				.setSubmitterNm		( areqEntity.getLendReqUserNm() )
				.setAssigneeNm		( areqEntity.getAssigneeNm() )
				.setCheckinDueDate	( TriDateUtils.convertViewDateFormat( areqEntity.getRtnReqDueDate(),
										TriDateUtils.getYMDDateFormat(), formatYMD))
				.setCheckoutTime	( TriDateUtils.convertViewDateFormat(areqEntity.getReqTimestamp(), formatYMDHM) )
				.setCheckinTime		( TriDateUtils.convertViewDateFormat(areqEntity.getReqTimestamp(), formatYMDHM) )
				;

		serviceBean.setDetailsView(view);
	}
}
