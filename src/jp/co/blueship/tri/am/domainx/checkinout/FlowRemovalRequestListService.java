package jp.co.blueship.tri.am.domainx.checkinout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.OrderBy;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowRemovalRequestListService implements IDomain<FlowRemovalRequestListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRemovalRequestListServiceBean>
				execute( IServiceDto<FlowRemovalRequestListServiceBean> serviceDto) {

		FlowRemovalRequestListServiceBean paramBean = serviceDto.getServiceBean();

		PreConditions.assertOf(TriStringUtils.isNotEmpty(paramBean.getParam().getSelectedLotId()), "SelectedLotId is not specified");

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified!");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * Call when initializing by service
	 *
	 * @param serviceBean
	 */
	private void init ( FlowRemovalRequestListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		ILotDto lotDto = this.support.findLotDto( lotId, AmTables.AM_LOT_GRP_LNK );
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

		this.generateDropBox( lotId, serviceBean );
		this.onChange( serviceBean );
	}

	/**
	 * @param lotId Target lot-ID
	 * @param serviceBean Set the generated search conditions
	 */
	private void generateDropBox( String lotId, FlowRemovalRequestListServiceBean serviceBean  ) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = serviceBean.getParam().getSearchDraftCondition();

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchDraftCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());
		searchCondition.setCtgViews( searchDraftCondition.getCtgViews() );

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchDraftCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());
		searchCondition.setMstoneViews( searchDraftCondition.getMstoneViews() );


		{
			List<IStatusId> areqStatuses = new ArrayList<IStatusId>();
			areqStatuses.add(AmAreqStatusId.RemovalRequested);
			areqStatuses.add(AmAreqStatusId.RemovalRequestApproved);
			areqStatuses.add(AmAreqStatusIdForExecData.RemovalRequestError);
			areqStatuses.add(AmAreqStatusIdForExecData.ReturnToPendingRemovalRequest);
			areqStatuses.add(AmAreqStatusIdForExecData.RemovalRequestCancelled);
			areqStatuses.add(AmAreqStatusIdForExecData.BuildPackageClosing);
			areqStatuses.add(AmAreqStatusId.BuildPackageClosed);
			areqStatuses.add(AmAreqStatusIdForExecData.BuildPackageCloseError);
			areqStatuses.add(AmAreqStatusIdForExecData.Merging);
			areqStatuses.add(AmAreqStatusId.Merged);
			areqStatuses.add(AmAreqStatusIdForExecData.MergeError);
			searchCondition.setStatusViews(FluentList.from( areqStatuses ).map( AmFluentFunctionUtils.toItemLabelsFromStatusId).asList());
		}
	}

	/**
	 * Call when request by service
	 *
	 * @param serviceBean
	 */
	private void onChange( FlowRemovalRequestListServiceBean serviceBean ) {
		ISqlSort sort = getSortOrder( serviceBean.getParam() );

		SearchCondition searchCondition = (serviceBean.getParam().isSelectedDraft()) ?
				serviceBean.getParam().getSearchDraftCondition() : serviceBean.getParam().getSearchCondition();

		int linesPerPage = serviceBean.getParam().getLinesPerPage();

		int pageNo = 1;
		pageNo = searchCondition.getSelectedPageNo();

		IEntityLimit<IAreqEntity> limit = support.getAreqDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				pageNo,
				linesPerPage);

		IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());

		if ( serviceBean.getParam().isSelectedDraft() ) {
			serviceBean.setAreqViewList(new ArrayList<AreqView>());
			serviceBean.getParam().getSearchDraftCondition().setStatusViews(new ArrayList<ItemLabelsBean>());

			serviceBean.setDraftPage(page);
			serviceBean.setDraftAreqViewList( getDraftAreqViewBean(serviceBean, limit.getEntities() ) );

		} else {
			serviceBean.setPage(page);
			serviceBean.setAreqViewList( getAreqViewBean(serviceBean, limit.getEntities() ) );
		}
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private AreqCondition getCondition(
			FlowRemovalRequestListServiceBean serviceBean ) {

		FlowRemovalRequestListServiceBean.RequestParam param = serviceBean.getParam();
		String lotId = serviceBean.getParam().getSelectedLotId();

		SearchCondition searchParam = ( param.isSelectedDraft() )? param.getSearchDraftCondition() : param.getSearchCondition();

		AreqCondition condition = new AreqCondition();

		condition.setLotId(lotId);
		condition.setAreqCtgCd(AreqCtgCd.RemovalRequest.value());

		if ( param.isSelectedDraft() ) {
			condition.setProcStsIds( AmAreqStatusId.DraftRemovalRequest.getStatusId() );

		} else {
			if ( TriStringUtils.isNotEmpty(searchParam.getStsId()) ) {
				condition.setProcStsIds( searchParam.getStsId() );
			} else {
				condition.setStsIdsNotEquals(new String[] {
					AmAreqStatusId.DraftRemovalRequest.getStatusId()	
				});
			}
		}

		if (  TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId(searchParam.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId(searchParam.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		return condition;
	}

	/**
	 * @param param
	 * @return
	 */
	private ISqlSort getSortOrder( FlowRemovalRequestListServiceBean.RequestParam param ) {

		ISqlSort sort = new SortBuilder();

		if ( null == param.getOrderBy() )
			return sort;

		OrderBy orderBy = param.getOrderBy();

		if ( null != orderBy.getAreqId() ) {
			sort.setElement(AreqItems.areqId, orderBy.getAreqId(), sort.getLatestSeq() + 1);
		}

		return sort;
	}

	/**
	 * @param paramBean Service Bean
	 * @param areqList Check-in / Check-out Request Entities
	 * @return
	 */
	private List<AreqView> getAreqViewBean(
								FlowRemovalRequestListServiceBean paramBean,
								List<IAreqEntity> areqList) {

		List<AreqView> viewBeanList = new ArrayList<AreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		for (IAreqEntity entity : areqList) {
			AreqView view = paramBean.new AreqView()
					.setAreqId( entity.getAreqId() )
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setPjtSubject( entity.getPjtSubject() )
					.setReferenceId( entity.getChgFactorNo() )
					.setSubject( entity.getSummary() )
					.setSubmitterId( entity.getDelReqUserId() )
					.setSubmitterNm( entity.getDelReqUserNm() )
					.setSubmitterIconPath( support.getUmFinderSupport().getIconPath(entity.getDelReqUserId()))
					.setAssigneeId( entity.getAssigneeId() )
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
					.setRequestDate( TriDateUtils.convertViewDateFormat(entity.getDelReqTimestamp(), formatYMD) )
					.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
			;

			viewBeanList.add( view );
		}

		return viewBeanList;
	}

	/**
	 * @param paramBean Service Bean
	 * @param draftAreqList Check-in / Check-out Request Entities
	 * @return
	 */
	private List<DraftAreqView> getDraftAreqViewBean(
								FlowRemovalRequestListServiceBean paramBean,
								List<IAreqEntity> draftAreqList) {

		List<DraftAreqView> viewBeanList = new ArrayList<DraftAreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		for (IAreqEntity entity : draftAreqList) {
			DraftAreqView view = paramBean.new DraftAreqView()
					.setAreqId( entity.getAreqId() )
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setSubject( entity.getSummary() )
					.setSubmitterId( entity.getDelReqUserId() )
					.setSubmitterNm( entity.getDelReqUserNm() )
					.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(entity.getDelReqUserId()))
					.setAssigneeId( entity.getAssigneeId() )
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
					.setRequestDate( TriDateUtils.convertViewDateFormat(entity.getDelReqTimestamp(), formatYMD) )
					.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
			;

			viewBeanList.add( view );
		}

		return viewBeanList;
	}

}
