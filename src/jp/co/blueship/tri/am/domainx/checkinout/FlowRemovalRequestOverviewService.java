package jp.co.blueship.tri.am.domainx.checkinout;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestOverviewServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestOverviewServiceBean.CheckinOutRequestOverview;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowRemovalRequestOverviewService implements IDomain<FlowRemovalRequestOverviewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRemovalRequestOverviewServiceBean> execute(
			IServiceDto<FlowRemovalRequestOverviewServiceBean> serviceDto) {

		FlowRemovalRequestOverviewServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowRemovalRequestOverviewServiceBean serviceBean) {
		String areqId = serviceBean.getParam().getSelectedAreqId();

		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);

		CheckinOutRequestOverview view = serviceBean.new CheckinOutRequestOverview ()
				.setSubmitterNm		( areqEntity.getDelReqUserNm() )
				.setAssigneeNm		( areqEntity.getAssigneeNm() )
				.setSubject			( areqEntity.getSummary() )
				.setStsId			( areqEntity.getProcStsId() )
				.setStatus			( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) )
				.setRequestTime		( TriDateUtils.convertViewDateFormat(
										areqEntity.getReqTimestamp(),
										TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
										).toString() )
		;

		serviceBean.setDetailsView(view);
	}
}
