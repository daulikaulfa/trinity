package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private FlowCheckinRequestApprovalPendingCancellationServiceBean checkinCancelServiceBean = new FlowCheckinRequestApprovalPendingCancellationServiceBean();
	private FlowCheckoutRequestRemovalServiceBean checkoutRemovalServiceBean = new FlowCheckoutRequestRemovalServiceBean();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckinRequestApprovalPendingCancellationServiceBean getCheckinCancelServiceBean() {
		return checkinCancelServiceBean;
	}

	public FlowCheckinRequestRemovalServiceBean setCheckinCancelServiceBean(FlowCheckinRequestApprovalPendingCancellationServiceBean checkinCancelServiceBean) {
		this.checkinCancelServiceBean = checkinCancelServiceBean;
		return this;
	}

	public FlowCheckoutRequestRemovalServiceBean getCheckoutRemovalServiceBean() {
		return checkoutRemovalServiceBean;
	}

	public FlowCheckinRequestRemovalServiceBean setCheckoutRemovalServiceBean(FlowCheckoutRequestRemovalServiceBean checkoutRemovalServiceBean) {
		this.checkoutRemovalServiceBean = checkoutRemovalServiceBean;
		return this;
	}

	public FlowCheckinRequestRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private CheckinRequestRemovalInputBean inputInfo = new CheckinRequestRemovalInputBean();

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String selectedAreqId) {
			this.areqId = selectedAreqId;
			return this;
		}

		public CheckinRequestRemovalInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CheckinRequestRemovalInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 *
	 * Input Information
	 *
	 */
	public class CheckinRequestRemovalInputBean {
		private String comment;

		public String getComment() {
			return comment;
		}
		public CheckinRequestRemovalInputBean setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
