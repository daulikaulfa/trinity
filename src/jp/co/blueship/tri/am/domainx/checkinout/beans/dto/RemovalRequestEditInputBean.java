package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class RemovalRequestEditInputBean {
	private String pjtId;
	private String referenceId = null;
	private List<ItemLabelsBean> pjtViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
	private String groupId;
	private String subject;
	private String contents;
	private String assigneeId;
	private List<ItemLabelsBean> assigneeViews = new ArrayList<ItemLabelsBean>();
	private String ctgId;
	private List<ItemLabelsBean> categoryViews = new ArrayList<ItemLabelsBean>();
	private String mstoneId;
	private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();
	private boolean isResourceSelection = true;
	private String csvFilePath;
	private String csvFileNm;
	private byte[] csvInputStreamBytes;
	private SubmitMode submitMode = SubmitMode.none;

	public String getPjtId() {
		return pjtId;
	}
	public RemovalRequestEditInputBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}
	public List<ItemLabelsBean> getPjtViews() {
		return pjtViews;
	}
	public RemovalRequestEditInputBean setPjtViews(List<ItemLabelsBean> pjtViews) {
		this.pjtViews = pjtViews;
		return this;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public RemovalRequestEditInputBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}
	public String getGroupId() {
		return groupId;
	}
	public RemovalRequestEditInputBean setGroupId(String groupId) {
		this.groupId = groupId;
		return this;
	}
	public List<ItemLabelsBean> getGroupViews() {
		return groupViews;
	}
	public void setGroupViews(List<ItemLabelsBean> groupViews) {
		this.groupViews = groupViews;
	}
	public String getSubject() {
		return subject;
	}
	public RemovalRequestEditInputBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}
	public String getContents() {
		return contents;
	}
	public RemovalRequestEditInputBean setContents(String contents) {
		this.contents = contents;
		return this;
	}
	public boolean isResourceSelection() {
		return isResourceSelection;
	}
	public RemovalRequestEditInputBean setResourceSelection(boolean isResourceSelection) {
		this.isResourceSelection = isResourceSelection;
		return this;
	}
	public String getCsvFilePath() {
		return csvFilePath;
	}
	public RemovalRequestEditInputBean setCsvFilePath(String csvFilePath) {
		this.csvFilePath = csvFilePath;
		return this;
	}
	public String getCsvFileNm() {
		return csvFileNm;
	}
	public RemovalRequestEditInputBean setCsvFileNm(String csvFileNm) {
		this.csvFileNm = csvFileNm;
		return this;
	}
	public byte[] getCsvInputStreamBytes() {
		return csvInputStreamBytes;
	}
	public RemovalRequestEditInputBean setCsvInputStreamBytes(byte[] csvInputStreamBytes) {
		this.csvInputStreamBytes = csvInputStreamBytes;
		return this;
	}
	public String getAssigneeId() {
		return assigneeId;
	}
	public RemovalRequestEditInputBean setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
		return this;
	}
	public List<ItemLabelsBean> getAssigneeViews() {
		return assigneeViews;
	}
	public RemovalRequestEditInputBean setAssigneeViews(List<ItemLabelsBean> assigneeViews) {
		this.assigneeViews = assigneeViews;
		return this;
	}
	public String getCtgId() {
		return ctgId;
	}
	public RemovalRequestEditInputBean setCtgId(String ctgId) {
		this.ctgId = ctgId;
		return this;
	}
	public List<ItemLabelsBean> getCategoryViews() {
		return categoryViews;
	}
	public RemovalRequestEditInputBean setCategoryViews(List<ItemLabelsBean> categoryViews) {
		this.categoryViews = categoryViews;
		return this;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public RemovalRequestEditInputBean setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		return this;
	}
	public List<ItemLabelsBean> getMstoneViews() {
		return mstoneViews;
	}
	public RemovalRequestEditInputBean setMstoneViews(List<ItemLabelsBean> mstoneViews) {
		this.mstoneViews = mstoneViews;
		return this;
	}
	public SubmitMode getSubmitMode() {
		return submitMode;
	}
	public RemovalRequestEditInputBean setSubmitMode(SubmitMode submitMode) {
		this.submitMode = submitMode;
		return this;
	}


	public enum SubmitMode {
		none( "" ),
		/**
		 * CreateService Only
		 */
		draft( "draft" ),
		/**
		 * CreateService / EditService
		 */
		request( "request" ),
		/**
		 * EditService Only
		 */
		changes ( "changes" );

		private String value = null;

		private SubmitMode( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SubmitMode mode = value( value );

			if ( null == mode ) return false;
			if ( ! this.equals(mode) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SubmitMode value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SubmitMode mode : values() ) {
				if ( mode.value().equals( value ) ) {
					return mode;
				}
			}

			return none;
		}
	}
}
