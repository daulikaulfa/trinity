package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.*;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestEditServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestEditServiceBean.CheckoutRequestDetailsView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestEditServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowCheckoutRequestEditService implements IDomain<FlowCheckoutRequestEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibLendEditSupport support;
	private IUmFinderSupport umSupport;

	public void setSupport( FlowChaLibLendEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> actions4InputFile = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> saveDraftActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> validateSubmitActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> submitActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setInputService(IDomain<IGeneralServiceBean> service) {
		this.inputService = service;
	}

	public void setConfirmationService(IDomain<IGeneralServiceBean> service) {
		this.confirmationService = service;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> service) {
		this.completeService = service;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setActions4InputFile( List<IDomain<IGeneralServiceBean>> actions4InputFile ) {
		this.actions4InputFile = actions4InputFile;
	}

	public void setSaveDraftActions(List<IDomain<IGeneralServiceBean>> saveDraftActions) {
		this.saveDraftActions = saveDraftActions;
	}

	public List<IDomain<IGeneralServiceBean>> getSubmitActions() {
		return submitActions;
	}

	public void setSubmitActions(List<IDomain<IGeneralServiceBean>> submitActions) {
		this.submitActions = submitActions;
	}

	public void setValidateSubmitActions(List<IDomain<IGeneralServiceBean>> validateSubmitActions) {
		this.validateSubmitActions = validateSubmitActions;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowCheckoutRequestEditServiceBean> execute( IServiceDto<FlowCheckoutRequestEditServiceBean> serviceDto ) {

		FlowCheckoutRequestEditServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibLendModifyServiceBean innerServiceBean = paramBean.getInnerService();

		try {

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				SubmitMode submitMode = paramBean.getParam().getInputInfo().getSubmitMode();

				if (SubmitMode.changes.equals(submitMode)) {

					this.changes(paramBean);

				} else if (SubmitMode.request.equals(submitMode)) {

					this.submitChanges(paramBean);
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void init( FlowCheckoutRequestEditServiceBean paramBean ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean( serviceBean );

		IAreqEntity areqEntity = this.support.findAreqEntity(paramBean.getParam().getSelectedAreqId());

		String selectedLotId = areqEntity.getLotId();
		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		serviceBean.setLotNo( selectedLotId );
		serviceBean.setApplyNo( selectedAreqId );
		paramBean.getResult().setAreqId( paramBean.getParam().getSelectedAreqId() );

		serviceBean.setReferer( ChaLibScreenID.LEND_LIST );

		this.beforeExecution(paramBean, serviceBean, areqEntity.getLotId());

		//basic information input
		{
			serviceBean.setForward( ChaLibScreenID.LEND_MODIFY );
			inputService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean, areqEntity);

		// Get all checked out files
		{
			this.setCheckedoutFilesByInit(paramBean);
		}

		// Build Tree Folder
		{
			List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			AmResourceSelectionUtils.buildTreeFolderViewByInit(paramList, masterPath, false);
		}

		// Generate drop box
		{
			this.generateDropBox(paramBean, areqEntity.getLotId());
		}
	}

	private void onChange(FlowCheckoutRequestEditServiceBean paramBean) {
		PreConditions.assertOf(paramBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		IAreqEntity areqEntity = this.support.findAreqEntity(paramBean.getParam().getSelectedAreqId());

		String selectedLotId = areqEntity.getLotId();
		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {

			// Set Category Views
			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(selectedLotId);
			paramBean.getParam().getInputInfo().setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {

			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(selectedLotId);
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}

		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {
			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmResourceSelectionUtils.openFolder(paramList, masterPath);
				return;
			}

			if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			if (ResourceRequestType.selectFolder.equals(selectedType)) {

				this.selectFolder(paramBean, paramList);
				return;
			}
			

			if (ResourceRequestType.selectSingleResource.equals(selectedType)) {

				this.selectSingleResource(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectAllResource.equals(selectedType)) {

				this.selectAllResource(paramBean, paramList);
				return;
			}
		}

		// File Upload
		if (RequestOption.fileUpload.equals(selectedRequestCtg)) {
			this.uploadFile(paramBean, paramList);
			return;
		}

	}

	private void selectFolder(FlowCheckoutRequestEditServiceBean paramBean, List<Object> paramList) {

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// Set Asset Selection List
		AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, masterPath, false);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}
	
	private void selectSingleResource(FlowCheckoutRequestEditServiceBean paramBean, List<Object> paramList){

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);
		
	}
	
	private void selectAllResource(FlowCheckoutRequestEditServiceBean paramBean, List<Object> paramList){
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		// Set all selected files
		AmResourceSelectionUtils.selectFilesInFolder(paramBean, paramList, masterPath, false);
	}

	private void uploadFile(FlowCheckoutRequestEditServiceBean paramBean, List<Object> paramList) {
		if( TriStringUtils.isEmpty( paramBean.getParam().getInputInfo().getCsvInputStreamBytes() ) ) {
			throw new ContinuableBusinessException(AmMessageId.AM001134E);
		}
		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		CheckoutRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		// Set input param
		ChaLibEntryInputBaseInfoBean inputBean = new ChaLibEntryInputBaseInfoBean();
		{
			inputBean.setInPrjNo(inputInfo.getPjtId());
			if (TriStringUtils.isNotEmpty(inputInfo.getGroupId())) {
				inputBean.setInGroupName(this.support.getUmFinderSupport().
						findGroupById(inputInfo.getGroupId()).getGrpNm());
			}
			inputBean.setInSubject(inputInfo.getSubject());
			inputBean.setInContent(inputInfo.getContents());
			inputBean.setLendApplyFilePath(inputInfo.getCsvFilePath());
			inputBean.setLendApplyFileName(inputInfo.getCsvFileNm());
			inputBean.setLendApplyInputStreamBytes(inputInfo.getCsvInputStreamBytes());
		}

		// File Upload or Drag and Drop Selection
		AmResourceSelectionUtils.checkInOutFileUpload(paramBean, inputBean, paramList, selectedAreqId, actions, actions4InputFile);


		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}

	/**
	 * @param paramBean Service Bean
	 */
	private void submitChanges( FlowCheckoutRequestEditServiceBean paramBean ) throws ParseException {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		serviceBean.setScreenType( ScreenType.next );
		dto.setServiceBean( serviceBean );

		String selectedAreqId = paramBean.getParam().getSelectedAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity(selectedAreqId);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( areqEntity.getLotId() )
			.setAreqIds( selectedAreqId );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		AmItemChkUtils.checkCheckinDueDate(paramBean.getParam().getInputInfo().getCheckinDueDate());

		String selectedLotId = areqEntity.getLotId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		{
			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		// Set input param before execution
		this.setInputParamBySubmit(paramBean, serviceBean, areqEntity.getLotId(), areqEntity);

		IAreqDto areqDto = this.support.findAreqDto( paramBean.getParam().getSelectedAreqId(), AmTables.AM_AREQ_FILE );
		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);

		// Validate checkout draft selected files
		{
			this.validateCheckoutDraftSelectedFiles(serviceBean, lotDto, areqDto, selectedAreqId);
		}

		// Call Lend Modify Input Service
		{
			this.callModifyInputService(serviceBean, lotDto, areqDto);
		}

		// Call Lend Modify Confirmation
		{
			this.callModifyConfirmation(serviceBean, lotDto, areqDto, false);
		}

		// Call Lend Modify Completion
		{
			serviceBean.setReferer( ChaLibScreenID.LEND_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LEND_MODIFY );
			completeService.execute(dto);
		}
		{
			serviceBean.setReferer( ChaLibScreenID.LEND_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LEND_MODIFY );
			mailService.execute(dto);
		}
		
		this.afterExecution(serviceBean, paramBean, areqEntity);
	}

	private void changes(FlowCheckoutRequestEditServiceBean paramBean) throws ParseException {

		IAreqEntity areqEntity = this.support.findAreqEntity(paramBean.getParam().getSelectedAreqId());

		if (AmAreqStatusId.DraftCheckoutRequest.equals(areqEntity.getStsId())) {

			this.saveAsDraft(paramBean, areqEntity.getLotId(), areqEntity);

		} else {

			this.save(paramBean, areqEntity.getLotId(), areqEntity);

		}

	}

	private void save(FlowCheckoutRequestEditServiceBean paramBean, String lotId, IAreqEntity areqEntity) throws ParseException {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		serviceBean.setScreenType( ScreenType.next );
		dto.setServiceBean( serviceBean );

		String selectedLotId = lotId;
		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( areqEntity.getLotId() )
			.setAreqIds( selectedAreqId );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		AmItemChkUtils.checkCheckinDueDate(paramBean.getParam().getInputInfo().getCheckinDueDate());

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		{
			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		// Set input param before execution
		this.setInputParamBySubmit(paramBean, serviceBean, lotId, areqEntity);

		// Call Lend Modify Input Service
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_MODIFY);
			serviceBean.setForward(ChaLibScreenID.LEND_MODIFY);
			inputService.execute(dto);
		}


		// Call Lend Modify Confirmation
		{
			serviceBean.setReferer( ChaLibScreenID.LEND_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LEND_MODIFY );
			confirmationService.execute(dto);
		}

		// Call Lend Modify Completion
		{
			serviceBean.setReferer( ChaLibScreenID.LEND_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LEND_MODIFY );
			completeService.execute(dto);
		}
		
		// Call Lend Modify Completion
		{
			serviceBean.setReferer( ChaLibScreenID.LEND_MODIFY_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_LEND_MODIFY );
			mailService.execute(dto);
		}
		this.afterExecution(serviceBean, paramBean, areqEntity);
		paramBean.getParam().setRequestType(RequestType.init);
		this.init(paramBean);
	}

	private void saveAsDraft(FlowCheckoutRequestEditServiceBean paramBean, String lotId, IAreqEntity areqEntity) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibLendModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		serviceBean.setScreenType( ScreenType.next );
		dto.setServiceBean( serviceBean );

		String selectedLotId = lotId;
		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( areqEntity.getLotId() )
			.setAreqIds( selectedAreqId );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		{
			AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
					paramBean.getParam().getResourceSelection().getSelectedFileSet());
		}

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot(paramList);
		IAreqDto areqDto = this.support.findAreqDto( paramBean.getParam().getSelectedAreqId(), AmTables.AM_AREQ_FILE );

		// Set input param before execution
		this.setInputParamBySubmit(paramBean, serviceBean, lotId, areqDto.getAreqEntity());

		// Validate checkout draft selected files
		{
			this.validateCheckoutDraftSelectedFiles(serviceBean, lotDto, areqDto, selectedAreqId);
		}

		// Call Lend Modify Input Service
		{
			this.callModifyInputService(serviceBean, lotDto, areqDto);

		}

		// Call Lend Modify Confirmation Service
		{
			this.callModifyConfirmation(serviceBean, lotDto, areqDto, true);
		}

		// Call Lend Modify Completion
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_MODIFY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_MODIFY);
			completeService.execute(dto);

		}
		
		// Call Lend Modify Mail Service
		
		{
			serviceBean.setReferer(ChaLibScreenID.LEND_MODIFY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_LEND_MODIFY);
			mailService.execute(dto);

		}

		this.afterExecution(serviceBean, paramBean, areqEntity);
		paramBean.getParam().setRequestType(RequestType.init);
		this.init(paramBean);

	}

	private void beforeExecution(FlowCheckoutRequestEditServiceBean src, FlowChaLibLendModifyServiceBean dest, String lotId) {
		dest.setApplyNo(src.getParam().getSelectedAreqId());
		dest.setLotNo(lotId);

		if (RequestType.init.equals(src.getParam().getRequestType())) {
			dest.setScreenType(ScreenType.next);
		}
		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {

		}
	}

	private void afterExecution(FlowChaLibLendModifyServiceBean src, FlowCheckoutRequestEditServiceBean dest, IAreqEntity areqEntity) {
		CheckoutRequestEditInputBean inputInfo = dest.getParam().getInputInfo();
		CheckoutRequestDetailsView detailsView = dest.getDetailsView();

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {

			ChaLibEntryBaseInfoBean v3Info = src.getBaseInfoViewBean();

			inputInfo.setPjtId(v3Info.getPrjNo());
			IPjtEntity pjtEntity = null;
			if ( ! TriStringUtils.isEmpty( v3Info.getPrjNo() )) {
				pjtEntity = this.support.findPjtEntity( v3Info.getPrjNo() ) ;
				inputInfo.setReferenceId( pjtEntity.getChgFactorNo() ) ;
				detailsView.setLotId(pjtEntity.getLotId());
			}

			inputInfo
				.setGroupId(this.support.getUmFinderSupport().findGroupByName(v3Info.getGroupName()).getGrpId())
				.setSubject			( v3Info.getSubject() )
				.setContents		( areqEntity.getContent() )
				.setAssigneeId		( v3Info.getAssigneeId() )
				.setCheckinDueDate	( v3Info.getCheckinDueDate() )
				.setCtgId			( v3Info.getCtgId() )
				.setMstoneId		( v3Info.getMstoneId() )
			;

			detailsView
				.setDraft			( AmBusinessJudgUtils.isDraft(areqEntity) )
				.setSubmitterNm		( v3Info.getApplyUser() )
				.setGroupNm			( src.getBaseInfoViewBean().getGroupName() )
				.setStatus			( areqEntity.getStsId() )
			;

		}

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {

			dest.getParam().setSelectedAreqId( src.getApplyNo());
			boolean isDraft = AmBusinessJudgUtils.isDraft(areqEntity);
			dest.getDetailsView()
				.setDraft( isDraft )
				.setStsId( areqEntity.getProcStsId())
				.setStatus( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) );
			dest.getResult().setCompleted(true);
			SubmitMode submitMode = dest.getParam().getInputInfo().getSubmitMode();
			if (SubmitMode.changes.equals(submitMode)) {
				if (isDraft) {
					dest.getMessageInfo().addFlashTranslatable( AmMessageId.AM003017I );
				} else {
					dest.getMessageInfo().addFlashTranslatable( AmMessageId.AM003018I );
				}
			} else if (SubmitMode.request.equals(submitMode)) {
				dest.getMessageInfo().addFlashTranslatable( AmMessageId.AM003008I );
			}
		}
	}

	/**
	 * @param src
	 * @param dest
	 * @param lotId
	 * @param areqEntity
	 */
	private void setInputParamBySubmit(
			FlowCheckoutRequestEditServiceBean src,
			FlowChaLibLendModifyServiceBean dest,
			String lotId,
			IAreqEntity areqEntity) {

		ChaLibEntryInputBaseInfoBean v3Info = new ChaLibEntryInputBaseInfoBean();

		if (TriStringUtils.isNotEmpty(dest.getInBaseInfoBean())) {
			v3Info = dest.getInBaseInfoBean();
		} else {
			dest.setInBaseInfoBean(v3Info);
		}

		CheckoutRequestEditInputBean srcInfo = src.getParam().getInputInfo();

		v3Info.setInLotNo(lotId);
		v3Info.setInPrjNo(srcInfo.getPjtId());

		IPjtEntity pjtEntity = null;
		if ( ! TriStringUtils.isEmpty( v3Info.getInPrjNo() )) {
			pjtEntity = this.support.findPjtEntity( v3Info.getInPrjNo() ) ;
			v3Info.setInChangeCauseNo( pjtEntity.getChgFactorNo() ) ;
		}

		v3Info.setInSubject(srcInfo.getSubject());
		v3Info.setInContent(srcInfo.getContents());
		v3Info.setInUserName(src.getUserName());
		v3Info.setInUserId(src.getUserId());
		v3Info.setInGroupId(areqEntity.getGrpId());
		v3Info.setInGroupName(areqEntity.getGrpNm());
		
		if (src.getParam().getInputInfo().getSubmitMode() == SubmitMode.changes && AmAreqStatusId.DraftCheckoutRequest.equals(areqEntity.getStsId())) {
			v3Info.setDraff(true);
		}

		if (srcInfo.isResourceSelection()) {
			v3Info.setLendApplyMode(AmResourceSelectionUtils.treeMode);
		} else {
			v3Info.setLendApplyMode(AmResourceSelectionUtils.fileMode);
		}

		v3Info.setLendApplyFileName(srcInfo.getCsvFileNm());
		v3Info.setLendApplyFilePath(srcInfo.getCsvFilePath());
		v3Info.setLendApplyInputStreamBytes(srcInfo.getCsvInputStreamBytes());
		v3Info.setAssigneeId(srcInfo.getAssigneeId());
		if(TriStringUtils.isNotEmpty(srcInfo.getAssigneeId())) {
            v3Info.setAssigneeNm(this.support.getUmFinderSupport().findUserByUserId(srcInfo.getAssigneeId()).getUserNm());
        } else {
		    v3Info.setAssigneeNm( "" );
        }
		v3Info.setCheckinDueDate(srcInfo.getCheckinDueDate());
		v3Info.setCtgId(srcInfo.getCtgId());
		v3Info.setMstoneId(srcInfo.getMstoneId());

		ChaLibInputLendEntryAssetSelectBean inAssetSelectBean = new ChaLibInputLendEntryAssetSelectBean();

		if (TriStringUtils.isNotEmpty(dest.getInAssetSelectBean())) {
			inAssetSelectBean = dest.getInAssetSelectBean();
		} else {
			dest.setInAssetSelectBean(inAssetSelectBean);
		}

		Set<String> selectedFileSet = src.getParam().getResourceSelection().getSelectedFileSet();
		String[] applyLendFiles = selectedFileSet.toArray(new String[selectedFileSet.size()]);
		inAssetSelectBean.setInAssetResourcePath(applyLendFiles);
		dest.setApplyNo(src.getParam().getSelectedAreqId());
	}

	private void setCheckedoutFilesByInit(FlowCheckoutRequestEditServiceBean paramBean) {

		IAreqEntity assetApplyEntity = this.support.findAreqEntity( paramBean.getParam().getSelectedAreqId() );

		String[] selectedFileList = new String[]{};
		selectedFileList = this.support.getCheckedOutFile(assetApplyEntity);

		Set<String> selectedFileSet = new HashSet<String>(FluentList.from(selectedFileList).asList());

		paramBean.getParam().getResourceSelection().setSelectedFileSet(selectedFileSet);
	}

	private void validateCheckoutDraftSelectedFiles( FlowChaLibLendModifyServiceBean paramBean, ILotDto lotDto, IAreqDto areqDto, String areqId) {
		List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(areqId, new AreqCtgCd[]{AreqCtgCd.LendingRequest});

		if (null == areqFileEntities) {
			return;
		}

		List<Object> paramList = new ArrayList<Object>();
		IEhLendParamInfo param = new EhLendParamInfo();
		support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );
		support.setLendParamInfo(paramBean.getInAssetSelectBean(), masterPath, param);

		param.setDuplicationCheck(true) ;
		param.setUserId( paramBean.getUserId() );

		paramList.add( param );
		paramList.add( lotDto );
		paramList.add( areqDto );

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		// Validate checkout draft selected files
		for ( IDomain<IGeneralServiceBean> action : validateSubmitActions ) {

			action.execute( innerServiceDto );
		}
	}

	private void callModifyInputService(FlowChaLibLendModifyServiceBean paramBean, ILotDto lotDto, IAreqDto areqDto) {
		List<Object> paramList = new ArrayList<Object>();

		IEhLendParamInfo param = new EhLendParamInfo();
		this.support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);
		paramList.add( param );
		paramList.add( lotDto );
		paramList.add( areqDto );

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		for ( IDomain<IGeneralServiceBean> action : this.actions ) {
			action.execute( innerServiceDto );
		}
	}

	private void callModifyConfirmation(FlowChaLibLendModifyServiceBean paramBean, ILotDto lotDto, IAreqDto areqDto, boolean isSaveDraft) {
		try {
			IAreqEntity areqEntity = areqDto.getAreqEntity();

			FlowChaLibLendEditSupport.setAssetApplyEntityBySaveDraftMode(areqEntity, paramBean.getInBaseInfoBean());

			if (!isSaveDraft) {
				areqEntity.setStsId(AmAreqStatusId.CheckoutRequested.getStatusId());
			}
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );
			{
				List<Object> paramList = new ArrayList<Object>();
				IEhLendParamInfo param = new EhLendParamInfo();
				support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);
				support.setLendParamInfo(paramBean.getInAssetSelectBean(), masterPath, param);

				param.setDuplicationCheck(true) ;
				param.setUserId( paramBean.getUserId() );

				paramList.add( param );
				paramList.add( lotDto );
				paramList.add( areqDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
				if( VcsCategory.SVN.equals(  scmType ) ) {
					List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
					paramList.add( reposEntities.toArray( new IVcsReposEntity[0] ) );
					paramList.add( this.support.findPassMgtEntityBySVN() ) ;

				} else {
					throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
				}


				if (!isSaveDraft) {

					// Validate input information
					for ( IDomain<IGeneralServiceBean> action : validateSubmitActions ) {
						action.execute( innerServiceDto );
					}

					String areqId =  areqEntity.getAreqId();

					// Logical delete Draft Records
					{
						this.support.cleaningAreq(areqId);
					}

					// Logical delete Category Link
					{
						this.support.getUmFinderSupport().cleaningCtgLnk(AmTables.AM_AREQ, areqId);
					}

					// Logical delete Milestone Link
					{
						this.support.getUmFinderSupport().cleaningMstoneLnk(AmTables.AM_AREQ, areqId);

					}

					for ( IDomain<IGeneralServiceBean> action : submitActions ) {
						action.execute( innerServiceDto );
					}

				} else {

					for ( IDomain<IGeneralServiceBean> action : saveDraftActions ) {
						action.execute( innerServiceDto );
					}
				}

			}
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}
	}

	private void generateDropBox(FlowCheckoutRequestEditServiceBean paramBean, String lotId) {

		CheckoutRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		// Set Assignee Views

		List<IUserEntity> userEntityList = this.umSupport.findUserByGroup(inputInfo.getGroupId());
		inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

		// Set Category Views
		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
		inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		// Set Milestone Views
		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		ILotDto lotDto = this.support.findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
		List<IGrpEntity> grpEntity = this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId());

		String[] groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto, grpEntity).toArray(new String[0]);
		inputInfo.setGroupViews(FluentList.from(this.support.getUmFinderSupport().findGroupByGroupIds(groupIds))
				.map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		List<PjtViewBean> pjtViewList = this.support.getSelectPjtViewList(lotId);
		inputInfo.setPjtViews(FluentList.from( pjtViewList ).map( AmFluentFunctionUtils.toItemLabelsFromPjtViewBean ).asList());

	}

}
