package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class CheckinRequestDetailsViewBean {

	private String lotId;
	private String lotStsId;
	private String lotStatus;
	private String areqId;
	private String pjtId;
	private String pjtStsId;
	private String pjtStatus;
	private String referenceId;
	private String groupNm;
	private String submitterNm;
	private String submitterIconPath;
	private String assigneeNm;
	private String assigneeIconPath;
	private String subject;
	private String contents;
	private String checkoutTime;
	private String checkinDueDate;
	private String ctgNm;
	private String mstoneNm;
	private String updTime;
	private String stsId;
	private String status;
	private List<AttachFile> attachmentFileNms = new ArrayList<AttachFile>();
	private String checkoutPath;
	private String checkinPath;

	public String getLotId(){
		return lotId;
	}
	public CheckinRequestDetailsViewBean setLotId(String lotId){
		this.lotId = lotId;
		return this;
	}

	public String getLotStsId() {
		return lotStsId;
	}
	public CheckinRequestDetailsViewBean setLotStsId(String lotStsId) {
		this.lotStsId = lotStsId;
		return this;
	}
	public String getLotStatus() {
		return lotStatus;
	}
	public CheckinRequestDetailsViewBean setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
		return this;
	}
	public String getAreqId() {
		return areqId;
	}
	public CheckinRequestDetailsViewBean setAreqId(String areqId) {
		this.areqId = areqId;
		return this;
	}

	public String getPjtId() {
		return pjtId;
	}
	public CheckinRequestDetailsViewBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}

	public String getPjtStsId() {
		return pjtStsId;
	}
	public CheckinRequestDetailsViewBean setPjtStsId(String pjtStsId) {
		this.pjtStsId = pjtStsId;
		return this;
	}
	public String getPjtStatus() {
		return pjtStatus;
	}
	public CheckinRequestDetailsViewBean setPjtStatus(String pjtStatus) {
		this.pjtStatus = pjtStatus;
		return this;
	}

	public String getReferenceId() {
		return referenceId;
	}
	public CheckinRequestDetailsViewBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}

	public String getGroupNm() {
		return groupNm;
	}
	public CheckinRequestDetailsViewBean setGroupNm(String groupNm) {
		this.groupNm = groupNm;
		return this;
	}

	public String getSubmitterNm() {
		return submitterNm;
	}
	public CheckinRequestDetailsViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}
	public CheckinRequestDetailsViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}
	
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public CheckinRequestDetailsViewBean setAssigneeNm(String AssigneeNm) {
		this.assigneeNm = AssigneeNm;
		return this;
	}

	public String getAssigneeIconPath() {
		return assigneeIconPath;
	}
	public CheckinRequestDetailsViewBean setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
		return this;
	}
	
	public String getSubject() {
		return subject;
	}

	public CheckinRequestDetailsViewBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getContents() {
		return contents;
	}
	public CheckinRequestDetailsViewBean setContents(String contents) {
		this.contents = contents;
		return this;
	}

	public String getCheckoutTime() {
		return checkoutTime;
	}
	public CheckinRequestDetailsViewBean setCheckoutTime(String checkoutTime) {
		this.checkoutTime = checkoutTime;
		return this;
	}

	public String getCheckinDueDate() {
		return checkinDueDate;
	}
	public CheckinRequestDetailsViewBean setCheckinDueDate(String checkinDueDate) {
		this.checkinDueDate = checkinDueDate;
		return this;
	}

	public String getCtgNm() {
		return ctgNm;
	}
	public CheckinRequestDetailsViewBean setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		return this;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}
	public CheckinRequestDetailsViewBean setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		return this;
	}

	public String getUpdTime() {
		return updTime;
	}
	public CheckinRequestDetailsViewBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}

	public List<AttachFile> getAttachmentFileNms() {
		return attachmentFileNms;
	}
	public CheckinRequestDetailsViewBean setAttachmentFileNms(List<AttachFile> attachmentFileNms) {
		this.attachmentFileNms = attachmentFileNms;
		return this;
	}

	public String getStsId() {
		return stsId;
	}
	public CheckinRequestDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}

	public String getStatus() {
		return status;
	}
	public CheckinRequestDetailsViewBean setStatus(String status) {
		this.status = status;
		return this;
	}

	public String getCheckoutPath() {
		return checkoutPath;
	}
	public CheckinRequestDetailsViewBean setCheckoutPath(String checkoutPath) {
		this.checkoutPath = checkoutPath;
		return this;
	}

	public String getCheckinPath() {
		return checkinPath;
	}
	public CheckinRequestDetailsViewBean setCheckinPath(String checkinPath) {
		this.checkinPath = checkinPath;
		return this;
	}
	
	public class AttachFile {
		private String fileNm;
		private String seqNo;

		public String getFileNm() {
			return fileNm;
		}
		public AttachFile setFileName(String fileNm) {
			this.fileNm = fileNm;
			return this;
		}

		public String getSeqNo() {
			return seqNo;
		}
		public AttachFile setSeqNo(String seqNo) {
			this.seqNo = seqNo;
			return this;
		}
	}
}
