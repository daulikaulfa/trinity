package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class RemovalRequestDetailsViewBean {

	private String lotId;
	private String lotStsId;
	private String lotStatus;
	private String areqId;
	private String pjtId;
	private String pjtStsId;
	private String pjtStatus;
	private String referenceId;
	private String groupNm;
	private String submitterNm;
	private String submitterIconPath;
	private String assigneeNm;
	private String assigneeIconPath;
	private String subject;
	private String contents;
	private String removalRequestTime;
	private String ctgNm;
	private String mstoneNm;
	private String updTime;
	private String stsId;
	private String status;

	public String getLotId(){
		return lotId;
	}
	public RemovalRequestDetailsViewBean setLotId(String lotId){
		this.lotId = lotId;
		return this;
	}

	public String getLotStsId() {
		return lotStsId;
	}
	public RemovalRequestDetailsViewBean setLotStsId(String lotStsId) {
		this.lotStsId = lotStsId;
		return this;
	}
	public String getLotStatus() {
		return lotStatus;
	}
	public RemovalRequestDetailsViewBean setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
		return this;
	}

	public String getAreqId() {
		return areqId;
	}
	public RemovalRequestDetailsViewBean setAreqId(String areqId) {
		this.areqId = areqId;
		return this;
	}

	public String getPjtId() {
		return pjtId;
	}
	public RemovalRequestDetailsViewBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}

	public String getPjtStsId() {
		return pjtStsId;
	}
	public RemovalRequestDetailsViewBean setPjtStsId(String pjtStsId) {
		this.pjtStsId = pjtStsId;
		return this;
	}
	public String getPjtStatus() {
		return pjtStatus;
	}
	public RemovalRequestDetailsViewBean setPjtStatus(String pjtStatus) {
		this.pjtStatus = pjtStatus;
		return this;
	}

	public String getReferenceId() {
		return referenceId;
	}
	public RemovalRequestDetailsViewBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}

	public String getGroupNm() {
		return groupNm;
	}
	public RemovalRequestDetailsViewBean setGroupNm(String groupNm) {
		this.groupNm = groupNm;
		return this;
	}

	public String getSubmitterNm() {
		return submitterNm;
	}
	public RemovalRequestDetailsViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}
	public RemovalRequestDetailsViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}
	
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public RemovalRequestDetailsViewBean setAssigneeNm(String AssigneeNm) {
		this.assigneeNm = AssigneeNm;
		return this;
	}

	public String getAssigneeIconPath() {
		return assigneeIconPath;
	}
	public RemovalRequestDetailsViewBean setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
		return this;
	}
	
	public String getSubject() {
		return subject;
	}

	public RemovalRequestDetailsViewBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}

	public String getContents() {
		return contents;
	}
	public RemovalRequestDetailsViewBean setContents(String contents) {
		this.contents = contents;
		return this;
	}

	public String getRemovalRequestTime() {
		return removalRequestTime;
	}
	public RemovalRequestDetailsViewBean setRemovalRequestTime(String removalRequestTime) {
		this.removalRequestTime = removalRequestTime;
		return this;
	}

	public String getCtgNm() {
		return ctgNm;
	}
	public RemovalRequestDetailsViewBean setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		return this;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}
	public RemovalRequestDetailsViewBean setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		return this;
	}

	public String getUpdTime() {
		return updTime;
	}
	public RemovalRequestDetailsViewBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}

	public String getStsId() {
		return stsId;
	}
	public RemovalRequestDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}

	public String getStatus() {
		return status;
	}
	public RemovalRequestDetailsViewBean setStatus(String status) {
		this.status = status;
		return this;
	}

}
