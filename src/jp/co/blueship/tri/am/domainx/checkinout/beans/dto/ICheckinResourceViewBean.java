package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface ICheckinResourceViewBean extends IResourceViewBean {
	public String getAreqId();
	public String getSubmitterId();
	public String getSubmitterNm();
	public String getCheckoutDate();
	public String getCheckinDueDate();
	public String getCheckinDate();
	public String getStsId();
	public boolean isDiff();
}
