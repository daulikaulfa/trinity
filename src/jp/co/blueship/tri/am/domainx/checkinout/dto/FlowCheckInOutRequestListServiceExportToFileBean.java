package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowCheckInOutRequestListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private CheckInOutRequestListFileResponse fileResponse = new CheckInOutRequestListFileResponse();
	private List<AreqView> areqViews = new ArrayList<AreqView>();
	private List<DraftAreqView> draftAreqViews = new ArrayList<DraftAreqView>();

	{
		this.setInnerService( new FlowCheckInOutRequestListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckInOutRequestListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public CheckInOutRequestListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowCheckInOutRequestListServiceExportToFileBean setFileResponse(CheckInOutRequestListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<AreqView> getAreqViews() {
		return areqViews;
	}
	public FlowCheckInOutRequestListServiceExportToFileBean setAreqViews( List<AreqView> areqViews ) {
		this.areqViews = areqViews;
		return this;
	}

	public List<DraftAreqView> getDraftAreqViews() {
		return draftAreqViews;
	}
	public FlowCheckInOutRequestListServiceExportToFileBean setDraftAreqViews( List<DraftAreqView> draftAreqViews ) {
		this.draftAreqViews = draftAreqViews;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private boolean draft = false;
		private SearchCondition searchCondition = new FlowCheckInOutRequestListServiceBean().new SearchCondition( false );
		private SearchCondition searchDraftCondition = new FlowCheckInOutRequestListServiceBean().new SearchCondition( true );
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;
		private ExportToFileBean exportBean = null;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public boolean isSelectedDraft() {
			return draft;
		}
		public RequestParam setSelectedDraft(boolean draft) {
			this.draft = draft;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public SearchCondition getSearchDraftCondition() {
			return searchDraftCondition;
		}
		public RequestParam setSearchDraftCondition(SearchCondition searchDraftCondition) {
			this.searchDraftCondition = searchDraftCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class CheckInOutRequestListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
