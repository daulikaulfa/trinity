package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRemovalRequestListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private FlowRemovalRequestListFileResponse fileResponse = new FlowRemovalRequestListFileResponse();
	private List<AreqView> areqView = new ArrayList<AreqView>();
	private List<DraftAreqView> draftAreqView = new ArrayList<DraftAreqView>();
	{
		this.setInnerService( new FlowRemovalRequestListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowRemovalRequestListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public FlowRemovalRequestListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowRemovalRequestListServiceExportToFileBean setFileResponse(FlowRemovalRequestListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<AreqView> getAreqViewList(){
		return areqView;
	}

	public void setAreqViewList(List<AreqView> areqView){
		this.areqView = areqView;
	}

	public List<DraftAreqView> getDraftAreqViewList(){
		return draftAreqView;
	}

	public void setDraftAreqViewList(List<DraftAreqView> draftAreqView){
		this.draftAreqView = draftAreqView;
	}
	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private boolean draft = false;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowRemovalRequestListServiceBean().new SearchCondition( false );
		private SearchCondition searchDraftCondition = new FlowRemovalRequestListServiceBean().new SearchCondition( true );
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public boolean isSelectedDraft() {
			return draft;
		}
		public RequestParam setSelectedDraft(boolean draft) {
			this.draft = draft;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public SearchCondition getSearchDraftCondition() {
			return searchDraftCondition;
		}
		public RequestParam setSearchDraftCondition(SearchCondition searchDraftCondition) {
			this.searchDraftCondition = searchDraftCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class FlowRemovalRequestListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
