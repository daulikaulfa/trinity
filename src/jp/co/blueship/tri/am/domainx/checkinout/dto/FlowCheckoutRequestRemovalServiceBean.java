package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompLendCancelServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowCheckoutRequestRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibCompLendCancelServiceBean() );
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCheckoutRequestRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;

		public String getSelectedAreqId() {
			return areqId;
		}
		public RequestParam setSelectedAreqId(String selectedAreqId) {
			this.areqId = selectedAreqId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private boolean redirectToDraft = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

		public boolean isRedirectToDraft() {
			return redirectToDraft;
		}

		public void setRedirectToDraft(boolean redirectToDraft) {
			this.redirectToDraft = redirectToDraft;
		}
	}
}
