package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import com.google.gson.annotations.Expose;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class ResourceViewBean  implements ICheckoutResourceDetailsViewBean, IRemovalResourceDetailsViewBean{
	@Expose	private boolean directory;
	@Expose	private String path;
	@Expose	private String name;
	@Expose	private String size;
	@Expose private int count = 0;

	public boolean isFile() {
		return !directory;
	}
	public boolean isDirectory() {
		return directory;
	}
	public ResourceViewBean setDirectory(boolean directory) {
		this.directory = directory;
		return this;
	}

	public String getPath() {
		return this.path;
	}
	public ResourceViewBean setPath(String path) {
		this.path = path;
		return this;
	}
	public String getName() {
		return this.name;
	}
	public ResourceViewBean setName(String name) {
		this.name = name;
		return this;
	}
	public String getSize() {
		return this.size;
	}
	public ResourceViewBean setSize(String size) {
		this.size = size;
		return this;
	}
	public int getCount() {
		return count;
	}
	public ResourceViewBean setCount(int count) {
		this.count = count;
		return this;
	}

}
