package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

import java.util.LinkedHashSet;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class ResourceSelection {
	private String path = null;
	private ResourceRequestType type = ResourceRequestType.none;
	private boolean selectAllFilesInFolder = false;
	private String[] selectedFiles;
	private Set<String> selectedFileSet = new LinkedHashSet<String>();

	public String getPath() {
		return path;
	}
	public ResourceSelection setPath(String path) {
		this.path = path;
		return this;
	}
	public ResourceRequestType getType() {
		return type;
	}
	public ResourceSelection setType(ResourceRequestType type) {
		this.type = type;
		return this;
	}

	/**
	 *
	 * @return
	 */
	public String[] getSelectedFiles() {
		if ( null == selectedFiles )
			return new String[0];

		return selectedFiles;
	}

	/**
	 *
	 * @param selectedFiles
	 * @return
	 */
	public ResourceSelection setSelectedFiles(String... selectedFiles) {
		this.selectedFiles = selectedFiles;
		return this;
	}

	public Set<String> getSelectedFileSet() {
		return selectedFileSet;
	}
	public void setSelectedFileSet(Set<String> selectedFileSet) {
		this.selectedFileSet = selectedFileSet;
	}

	public boolean isSelectAllFilesInFolder() {
		return selectAllFilesInFolder;
	}
	public void setSelectAllFilesInFolder(boolean selectAllFilesInFolder) {
		this.selectAllFilesInFolder = selectAllFilesInFolder;
	}




	public enum ResourceRequestType {
		none( "" ),
		openFolder( "open" ),
		closeFolder( "close" ),
		selectFolder( "selectFolder" ),
		selectSingleResource( "selectSingleResource" ),
		selectAllResource( "selectAllResource" ),
		;

		private String value = null;

		private ResourceRequestType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			ResourceRequestType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static ResourceRequestType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( ResourceRequestType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
