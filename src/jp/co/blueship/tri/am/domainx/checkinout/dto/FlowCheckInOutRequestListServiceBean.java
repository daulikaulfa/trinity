package jp.co.blueship.tri.am.domainx.checkinout.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowCheckInOutRequestListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<AreqView> areqViews = new ArrayList<AreqView>();
	private List<DraftAreqView> draftAreqViews = new ArrayList<DraftAreqView>();
	private IPageNoInfo page = new PageNoInfo();
	private IPageNoInfo draftPage = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<AreqView> getAreqViews() {
		return areqViews;
	}
	public FlowCheckInOutRequestListServiceBean setAreqViews( List<AreqView> areqViews ) {
		this.areqViews = areqViews;
		return this;
	}

	public List<DraftAreqView> getDraftAreqViews() {
		return draftAreqViews;
	}
	public FlowCheckInOutRequestListServiceBean setDraftAreqViews( List<DraftAreqView> draftAreqViews ) {
		this.draftAreqViews = draftAreqViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowCheckInOutRequestListServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	public IPageNoInfo getDraftPage() {
		return draftPage;
	}

	public FlowCheckInOutRequestListServiceBean setDraftPage(IPageNoInfo draftPage) {
		this.draftPage = draftPage;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String userId = null;
		private String lotId = null;
		private boolean draft = false;
		private SearchCondition searchCondition = new SearchCondition( false );
		private SearchCondition searchDraftCondition = new SearchCondition( true );
		private OrderBy orderBy = new OrderBy();
		private int linesPerPage = 20;
		private boolean showAll = false;

		public String getUserId() {
			return userId;
		}
		public RequestParam setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public boolean isSelectedDraft() {
			return draft;
		}
		public RequestParam setSelectedDraft(boolean draft) {
			this.draft = draft;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public SearchCondition getSearchDraftCondition() {
			return searchDraftCondition;
		}
		public RequestParam setSearchDraftCondition(SearchCondition searchDraftCondition) {
			this.searchDraftCondition = searchDraftCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public boolean isShowAll() {
			return showAll;
		}
		public RequestParam setShowAll(boolean showAll) {
			this.showAll = showAll;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}
	}

	/**
	 * Search Condition
	 */

	public class SearchCondition implements ISearchFilter {
		@Expose private boolean draft = false;
		private List<String> targetViews = new ArrayList<String>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();
		@Expose private String areqCtgCd = null;
		@Expose private String stsId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer pageNo = 1;

		public SearchCondition( boolean draft ) {
			this.draft = draft;
		}

		public boolean isDraft() {
			return draft;
		}

		public List<String> getTargetViews() {
			return targetViews;
		}
		public SearchCondition setTargetViews(List<String> targetViews) {
			this.targetViews = targetViews;
			return this;
		}
		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}
		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}
		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
		public String getAreqCtgCd() {
			return areqCtgCd;
		}
		public SearchCondition setAreqCtgCd(String areqCtgCd) {
			this.areqCtgCd = areqCtgCd;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return pageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.pageNo = selectedPageNo;
			return this;
		}

	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder accsTimestamp;
		private TriSortOrder pjtId;
		private TriSortOrder areqId = TriSortOrder.Desc;
		private TriSortOrder status;

		public TriSortOrder getAccsTimestamp() {
			return accsTimestamp;
		}
		public OrderBy setAccsTimestamp(TriSortOrder accsTimestamp) {
			this.accsTimestamp = accsTimestamp;
			return this;
		}
		public TriSortOrder getPjtId() {
			return pjtId;
		}
		public OrderBy setPjtId(TriSortOrder pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public TriSortOrder getAreqId() {
			return areqId;
		}
		public OrderBy setAreqId(TriSortOrder areqId) {
			this.areqId = areqId;
			return this;
		}
		public TriSortOrder getStatus() {
			return status;
		}
		public OrderBy setStatus(TriSortOrder status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Check-in / Checkout Request
	 */
	public class AreqView {
		private boolean isCheckinEnabled = false;
		private String checkoutPath;
		private String checkinPath;
		private AreqCtgCd areqType;

		private String areqId;
		private String pjtId;
		private String pjtSubject;
		private String referenceId;
		private String subject;
		private String submitterId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeId;
		private String assigneeNm;
		private String assigneeIconPath;
		private String contents;
		private String checkoutDate;
		private String checkinDate;
		private String checkinDueDate;
		private String updDate;
		private String stsId;
		private String status;

		public boolean isCheckinEnabled() {
			return isCheckinEnabled;
		}
		public AreqView setCheckinEnabled(boolean isCheckinEnabled) {
			this.isCheckinEnabled = isCheckinEnabled;
			return this;
		}
		public String getCheckoutPath() {
			return checkoutPath;
		}
		public AreqView setCheckoutPath(String checkoutPath) {
			this.checkoutPath = checkoutPath;
			return this;
		}
		public String getCheckinPath() {
			return checkinPath;
		}
		public AreqView setCheckinPath(String checkinPath) {
			this.checkinPath = checkinPath;
			return this;
		}
		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public AreqView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public boolean isCheckoutRequest() {
			return AmBusinessJudgUtils.isLendApply(this.areqType);
		}
		public boolean isCheckinRequest() {
			return AmBusinessJudgUtils.isReturnApply(this.areqType);
		}
		public boolean isRemovalRequest() {
			return AmBusinessJudgUtils.isDeleteApply(this.areqType);
		}

		public String getAreqId() {
			return areqId;
		}
		public AreqView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getPjtId() {
			return pjtId;
		}
		public AreqView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getPjtSubject() {
			return pjtSubject;
		}
		public AreqView setPjtSubject(String pjtSubject) {
			this.pjtSubject = pjtSubject;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public AreqView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public AreqView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getSubmitterId() {
			return submitterId;
		}
		public AreqView setSubmitterId(String submitterId) {
			this.submitterId = submitterId;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public AreqView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public AreqView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeId() {
			return assigneeId;
		}
		public AreqView setAssigneeId(String assigneeId) {
			this.assigneeId = assigneeId;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public AreqView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public AreqView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getContents() {
			return contents;
		}
		public AreqView setContents(String content) {
			this.contents = content;
			return this;
		}

		public String getCheckoutDate() {
			return checkoutDate;
		}
		public AreqView setCheckoutDate(String checkoutDate) {
			this.checkoutDate = checkoutDate;
			return this;
		}

		public String getCheckinDate() {
			return checkinDate;
		}
		public AreqView setCheckinDate(String checkinDate) {
			this.checkinDate = checkinDate;
			return this;
		}

		public String getCheckinDueDate() {
			return checkinDueDate;
		}
		public AreqView setCheckinDueDate(String checkinDueDate) {
			this.checkinDueDate = checkinDueDate;
			return this;
		}

		public String getUpdDate() {
			return updDate;
		}
		public AreqView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public AreqView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public AreqView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Draft for Check-in / Checkout Request
	 */
	public class DraftAreqView {
		private String areqId;
		private AreqCtgCd areqType;
		private String pjtId;
		private String referenceId;
		private String subject;
		private String submitterId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeId;
		private String assigneeNm;
		private String assigneeIconPath;
		private String checkoutDate;
		private String checkinDueDate;
		private String updDate;
		private String stsId;
		private String status;

		public String getAreqId() {
			return areqId;
		}
		public DraftAreqView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public DraftAreqView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public boolean isCheckoutRequest() {
			return AmBusinessJudgUtils.isLendApply(this.areqType);
		}
		public boolean isCheckinRequest() {
			return AmBusinessJudgUtils.isReturnApply(this.areqType);
		}
		public boolean isRemovalRequest() {
			return AmBusinessJudgUtils.isDeleteApply(this.areqType);
		}

		public String getPjtId() {
			return pjtId;
		}
		public DraftAreqView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public DraftAreqView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public DraftAreqView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getSubmitterId() {
			return submitterId;
		}
		public DraftAreqView setSubmitterId(String submitterId) {
			this.submitterId = submitterId;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public DraftAreqView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public DraftAreqView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeId() {
			return assigneeId;
		}
		public DraftAreqView setAssigneeId(String assigneeId) {
			this.assigneeId = assigneeId;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public DraftAreqView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public DraftAreqView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getCheckoutDate() {
			return checkoutDate;
		}
		public DraftAreqView setCheckoutDate(String checkoutDate) {
			this.checkoutDate = checkoutDate;
			return this;
		}

		public String getCheckinDueDate() {
			return checkinDueDate;
		}
		public DraftAreqView setCheckinDueDate(String checkinDueDate) {
			this.checkinDueDate = checkinDueDate;
			return this;
		}

		public String getUpdDate() {
			return updDate;
		}
		public DraftAreqView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public DraftAreqView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public DraftAreqView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

}
