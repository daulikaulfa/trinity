
package jp.co.blueship.tri.am.domainx.checkinout;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.DraftAreqView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.OrderBy;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowCheckInOutRequestListService implements IDomain<FlowCheckInOutRequestListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support = null;
	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowCheckInOutRequestListServiceBean> execute( IServiceDto<FlowCheckInOutRequestListServiceBean> serviceDto ) {

		FlowCheckInOutRequestListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * Call when initializing by service.
	 *
	 * @param serviceBean
	 */
	private void init ( FlowCheckInOutRequestListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		if ( TriStringUtils.isNotEmpty(lotId) ) {
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			ILotDto lotDto = this.support.findLotDto( lotId, AmTables.AM_LOT_GRP_LNK );
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

			this.generateDropBox( lotId, serviceBean );
		}

		this.onChange( serviceBean );
	}

	/**
	 * @param lotId Target lot-ID
	 * @param serviceBean Set the generated search conditions
	 */
	private void generateDropBox( String lotId, FlowCheckInOutRequestListServiceBean serviceBean) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = serviceBean.getParam().getSearchDraftCondition();

		searchDraftCondition.setTargetViews( FluentList.from( new String[]{ AreqCtgCd.LendingRequest.value(), AreqCtgCd.ReturningRequest.value() } ).asList() );
		searchCondition.setTargetViews( searchDraftCondition.getTargetViews() );


		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchDraftCondition.setCtgViews( FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );
		searchCondition.setCtgViews( searchDraftCondition.getCtgViews() );


		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchDraftCondition.setMstoneViews( FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );
		searchCondition.setMstoneViews( searchDraftCondition.getMstoneViews() );

		TriPropertyUtils.copyProperties(searchCondition, searchDraftCondition);

		{
			List<IStatusId> areqStatuses = this.getStatusList();
			searchCondition.setStatusViews(FluentList.from( areqStatuses ).map( AmFluentFunctionUtils.toItemLabelsFromStatusId).asList());
		}

	}

	/**
	 * Call when request by service
	 *
	 * @param serviceBean
	 */
	private void onChange( FlowCheckInOutRequestListServiceBean serviceBean ) {
		ISqlSort sort = getSortOrder( serviceBean.getParam() );

		SearchCondition searchCondition = (serviceBean.getParam().isSelectedDraft()) ?
				serviceBean.getParam().getSearchDraftCondition() : serviceBean.getParam().getSearchCondition();

		int linesPerPage = 0;
		if ( !serviceBean.getParam().isShowAll() ) {
			linesPerPage = serviceBean.getParam().getLinesPerPage();
		}

		IEntityLimit<IAreqEntity> limit = support.getAreqDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				searchCondition.getSelectedPageNo(),
				linesPerPage);

		IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());

		if ( serviceBean.getParam().isSelectedDraft() ) {
//			serviceBean.setAreqViews(new ArrayList<AreqView>());

			serviceBean.setDraftPage(page);
			serviceBean.setDraftAreqViews( getDraftAreqViewBean(serviceBean, limit.getEntities()));
		} else {
			serviceBean.setPage(page);
			serviceBean.setAreqViews( getAreqViewBean(serviceBean, limit.getEntities() ) );
		}
	}
	
	private List<IStatusId> getStatusList() {
		List<IStatusId> areqStatuses = new ArrayList<IStatusId>();
		areqStatuses.add(AmAreqStatusId.CheckoutRequested);
		areqStatuses.add(AmAreqStatusIdForExecData.CheckinRequestCancelled);
		areqStatuses.add(AmAreqStatusIdForExecData.CheckinRequestError);
		areqStatuses.add(AmAreqStatusId.CheckinRequested);
		areqStatuses.add(AmAreqStatusId.CheckinRequestApproved);
		areqStatuses.add(AmAreqStatusIdForExecData.CheckinRequestApprovalError);
		areqStatuses.add(AmAreqStatusIdForExecData.CheckinApprovalCancelled);
		areqStatuses.add(AmAreqStatusIdForExecData.ReturnToCheckoutRequest);
		areqStatuses.add(AmAreqStatusIdForExecData.BuildPackageClosing);
		areqStatuses.add(AmAreqStatusId.BuildPackageClosed);
		areqStatuses.add(AmAreqStatusIdForExecData.BuildPackageCloseError);
		areqStatuses.add(AmAreqStatusIdForExecData.Merging);
		areqStatuses.add(AmAreqStatusId.Merged);
		areqStatuses.add(AmAreqStatusIdForExecData.MergeError);
		return areqStatuses;
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private AreqCondition getCondition(
			FlowCheckInOutRequestListServiceBean serviceBean ) {

		FlowCheckInOutRequestListServiceBean.RequestParam param = serviceBean.getParam();
		String lotId = serviceBean.getParam().getSelectedLotId();
		SearchCondition searchParam = ( param.isSelectedDraft() )? param.getSearchDraftCondition() : param.getSearchCondition();

		AreqCondition condition = new AreqCondition();

		if ( TriStringUtils.isEmpty( lotId ) ) {
			if(ServiceId.UmRecentlyViewedRequestsService.value().equals(serviceBean.getFlowAction())) {
				condition.setJoinAccsHist( true, JoinType.RIGHT);
			}

			if( TriStringUtils.isNotEmpty( serviceBean.getParam().getUserId() ) ){
				condition.setContainsByMyRequest( serviceBean.getParam().getUserId() );
			}

		} else {
			condition.setLotId(lotId);

			if (  TriStringUtils.isNotEmpty(searchParam.getAreqCtgCd()) ) {
				condition.setAreqCtgCd(searchParam.getAreqCtgCd() );
			} else {
				condition.setAreqCtgCds( AreqCtgCd.LendingRequest.value(), AreqCtgCd.ReturningRequest.value() );
			}

			if ( param.isSelectedDraft() ) {
				condition.setProcStsIds( AmAreqStatusId.DraftCheckoutRequest.getStatusId(), AmAreqStatusId.DraftCheckinRequest.getStatusId() );
			} else {
				if ( TriStringUtils.isNotEmpty(searchParam.getStsId()) ) {
					condition.setProcStsIds( searchParam.getStsId() );
				} else {
					condition.setProcStsIdsByNotEquals(
							AmAreqStatusId.DraftCheckoutRequest.getStatusId(),
							AmAreqStatusId.DraftCheckinRequest.getStatusId());
				}
			}

			if ( TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
				condition.setCtgId(searchParam.getCtgId());
			}

			if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
				condition.setMstoneId(searchParam.getMstoneId());
			}

		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		return condition;
	}

	private LotCondition getLotCondition() {
		LotCondition condition = new LotCondition();
		List<String> stsIds = new ArrayList<String>();
		stsIds.add( AmLotStatusId.LotInProgress.getStatusId() );
		stsIds.add( AmLotStatusId.LotClosed.getStatusId() );
		condition.setStsIds( FluentList.from(stsIds).toArray(new String[0]) );


		return condition;
	}

	/**
	 * @param param
	 * @return
	 */
	private ISqlSort getSortOrder( FlowCheckInOutRequestListServiceBean.RequestParam param ) {

		ISqlSort sort = new SortBuilder();

		if ( null == param.getOrderBy() )
			return sort;

		OrderBy orderBy = param.getOrderBy();

		if ( null != orderBy.getAccsTimestamp() ) {
			sort.setElement(AccsHistItems.accsTimestamp, orderBy.getAccsTimestamp(), sort.getLatestSeq() + 1);
		}

		if ( null != orderBy.getPjtId() ) {
			sort.setElement(AreqItems.pjtId, orderBy.getPjtId(), sort.getLatestSeq() + 1);
		}
		if ( null != orderBy.getStatus() ) {
			sort.setElement(AreqItems.stsId, orderBy.getStatus(), sort.getLatestSeq() + 1);
			sort.setElement(AreqItems.procStsId, orderBy.getStatus(), sort.getLatestSeq() + 1);
		}
		if ( null != orderBy.getAreqId() ) {
			sort.setElement(AreqItems.areqId, orderBy.getAreqId(), sort.getLatestSeq() + 1);
		}

		return sort;
	}

	/**
	 * @param serviceBean Service Bean
	 * @param areqList Check-in / Check-out Request Entities
	 * @return
	 */
	private List<AreqView> getAreqViewBean(
								FlowCheckInOutRequestListServiceBean serviceBean,
								List<IAreqEntity> areqList) {

		List<AreqView> viewBeanList = new ArrayList<AreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IAreqEntity entity : areqList) {
			AreqView view = serviceBean.new AreqView()
				.setCheckoutPath(AmDesignBusinessRuleUtils.getSharePublicPathOfCheckoutRequest(entity))
				.setCheckinPath(AmDesignBusinessRuleUtils.getSharePublicPathOfCheckinRequest(entity))
				.setCheckinEnabled( AmBusinessJudgUtils.isReturnApplyEnabled(entity) )
				.setAreqType( AreqCtgCd.value(entity.getAreqCtgCd()) )
				.setAreqId( entity.getAreqId() )
				.setPjtId( entity.getPjtId() )
				.setPjtSubject( entity.getPjtSubject() )
				.setReferenceId( entity.getChgFactorNo() )
				.setSubject( entity.getSummary() )
				.setSubmitterId( entity.getRegUserId() )
				.setSubmitterNm( entity.getRegUserNm() )
				.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(entity.getRegUserId()))
				.setAssigneeId( entity.getAssigneeId() )
				.setAssigneeNm( entity.getAssigneeNm() )
				.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
				.setContents( entity.getContent() )
				.setCheckinDueDate( TriDateUtils.convertViewDateFormat(entity.getRtnReqDueDate(), TriDateUtils.getYMDDateFormat(), formatYMD) )
				.setCheckoutDate( TriDateUtils.convertViewDateFormat(entity.getLendReqTimestamp(), formatYMD) )
				.setCheckinDate( TriDateUtils.convertViewDateFormat(entity.getRtnReqTimestamp(), formatYMD) )
				.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
				.setStsId( entity.getProcStsId() )
				.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
			;
			viewBeanList.add( view );
		}

		return viewBeanList;
	}

	/**
	 * @param serviceBean Service Bean
	 * @param draftAreqList Check-in / Check-out Request Entities
	 * @return
	 */
	private List<DraftAreqView> getDraftAreqViewBean(
								FlowCheckInOutRequestListServiceBean serviceBean,
								List<IAreqEntity> draftAreqList) {

		List<DraftAreqView> viewBeanList = new ArrayList<DraftAreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IAreqEntity entity : draftAreqList) {
			DraftAreqView view = serviceBean.new DraftAreqView()
					.setAreqType( AreqCtgCd.value(entity.getAreqCtgCd()) )
					.setAreqId( entity.getAreqId() )
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setSubject( entity.getSummary() )
					.setSubmitterId( entity.getLendReqUserId() )
					.setSubmitterNm( entity.getLendReqUserNm() )
					.setSubmitterIconPath(support.getUmFinderSupport().getIconPath((entity.getLendReqUserId())))
					.setAssigneeId( entity.getAssigneeId() )
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath( support.getUmFinderSupport().getIconPath(entity.getAssigneeId() ))
					.setCheckinDueDate( TriDateUtils.convertViewDateFormat(entity.getRtnReqDueDate(), TriDateUtils.getYMDDateFormat(), formatYMD) )
					.setCheckoutDate( TriDateUtils.convertViewDateFormat(entity.getReqTimestamp(), formatYMD) )
					.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
			;

			viewBeanList.add( view );
		}

		return viewBeanList;
	}
}
