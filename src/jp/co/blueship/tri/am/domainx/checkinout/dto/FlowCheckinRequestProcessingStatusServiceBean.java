package jp.co.blueship.tri.am.domainx.checkinout.dto;


import jp.co.blueship.tri.bm.task.TaskDetailsViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCheckinRequestProcessingStatusServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;
	private RequestParam param = new RequestParam();
	private CheckinRequestProcessingStatusOverview overview = new CheckinRequestProcessingStatusOverview();
	private TaskDetailsViewBean detailsView = new TaskDetailsViewBean();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public CheckinRequestProcessingStatusOverview getOverview() {
		return overview;
	}
	public FlowCheckinRequestProcessingStatusServiceBean setOverview(CheckinRequestProcessingStatusOverview overview) {
		this.overview = overview;
		return this;
	}

	public TaskDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowCheckinRequestProcessingStatusServiceBean setDetailsView(TaskDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private RequestOption requestOption = RequestOption.none;

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public String getAreqId() {
			return areqId;
		}
		public RequestParam setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		refresh("refresh"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class CheckinRequestProcessingStatusOverview {
		private String lotId;
		private String lotStsId;
		private String lotStatus;
		private String areqId;
		private String stsId;
		private String status;
		private String submitterNm;
		private String submitterIconPath;
		private String groupNm;

		public String getLotId() {
			return lotId;
		}
		public CheckinRequestProcessingStatusOverview setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotStsId() {
			return lotStsId;
		}
		public CheckinRequestProcessingStatusOverview setLotStsId(String lotStsId) {
			this.lotStsId = lotStsId;
			return this;
		}

		public String getLotStatus() {
			return lotStatus;
		}
		public CheckinRequestProcessingStatusOverview setLotStatus(String lotStatus) {
			this.lotStatus = lotStatus;
			return this;
		}

		public String getAreqId() {
			return areqId;
		}
		public CheckinRequestProcessingStatusOverview setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public CheckinRequestProcessingStatusOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public CheckinRequestProcessingStatusOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public CheckinRequestProcessingStatusOverview setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public CheckinRequestProcessingStatusOverview setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getGroupNm() {
			return groupNm;
		}
		public CheckinRequestProcessingStatusOverview setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
	}
}
