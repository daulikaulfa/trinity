package jp.co.blueship.tri.am.domainx.checkinout;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.am.support.FlowChaLibScmEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRemovalRequestEditService implements IDomain<FlowRemovalRequestEditServiceBean> {

private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support;
	private IUmFinderSupport umSupport;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}
	private IVcsService vcsService = null;

	private List<IDomain<IGeneralServiceBean>> fileUploadActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> saveDraftActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> validateSubmitActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> submitActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> handleUploadedFileActions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setFileUploadActions(List<IDomain<IGeneralServiceBean>> fileUploadActions) {
		this.fileUploadActions = fileUploadActions;
	}



	public void setSaveDraftActions(List<IDomain<IGeneralServiceBean>> saveDraftActions) {
		this.saveDraftActions = saveDraftActions;
	}



	public void setValidateSubmitActions(List<IDomain<IGeneralServiceBean>> validateSubmitActions) {
		this.validateSubmitActions = validateSubmitActions;
	}



	public void setSubmitActions(List<IDomain<IGeneralServiceBean>> submitActions) {
		this.submitActions = submitActions;
	}



	public void setHandleUploadedFileActions(List<IDomain<IGeneralServiceBean>> handleUploadedFileActions) {
		this.handleUploadedFileActions = handleUploadedFileActions;
	}



	@Override
	public IServiceDto<FlowRemovalRequestEditServiceBean> execute(
			IServiceDto<FlowRemovalRequestEditServiceBean> serviceDto) {
		FlowRemovalRequestEditServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}
			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				SubmitMode submitMode = paramBean.getParam().getInputInfo().getSubmitMode();

				if (SubmitMode.draft.equals(submitMode)) {

					this.saveAsDraft(paramBean);

				} else if (SubmitMode.request.equals(submitMode)) {

					this.submitChanges(paramBean);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}

	}

	private void init(FlowRemovalRequestEditServiceBean paramBean) {

		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		IAreqEntity areqEntity = this.support.findAreqEntity(selectedAreqId);
		String selectedLotId = areqEntity.getLotId();

		// Set Entity Information
		this.setEntityByInit(paramBean, areqEntity);

		// Get all removal request files
		{
			Set<String> selectedFileSet = new HashSet<String>(FluentList.from(this.getRemovalRequestFiles(selectedAreqId)).asList());

			paramBean.getParam().getResourceSelection().setSelectedFileSet(selectedFileSet);
		}

		// Build Tree Folder
		{
			List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);
			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			AmResourceSelectionUtils.buildTreeFolderViewByInit(paramList, masterPath, false);
		}

		// Generate Drop box
		this.generateDropBox(paramBean, selectedLotId);

	}

	private void onChange(FlowRemovalRequestEditServiceBean paramBean) {
		PreConditions.assertOf(paramBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedRequestCtg = paramBean.getParam().getRequestOption();
		ResourceRequestType selectedType = paramBean.getParam().getResourceSelection().getType();

		String selectedAreqId = paramBean.getParam().getSelectedAreqId();

		IAreqEntity areqEntity = this.support.findAreqEntity(selectedAreqId);
		String selectedLotId = areqEntity.getLotId();

		List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
				paramBean.getParam().getResourceSelection(), selectedLotId, selectedAreqId);
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );


		if (RequestOption.refreshCategory.equals(selectedRequestCtg)) {

			// Set Category Views
			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(areqEntity.getLotId());
			paramBean.getParam().getInputInfo().setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());
		}

		if (RequestOption.refreshMilestone.equals(selectedRequestCtg)) {

			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(areqEntity.getLotId());
			paramBean.getParam().getInputInfo().setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}

		// Re-render group name when select assignee
		if (RequestOption.selectAssignee.equals(selectedRequestCtg)) {
			this.refreshGroup(paramBean, areqEntity.getLotId());
			return;
		}

		// Re-render assignee name when select group
		if (RequestOption.selectGroup.equals(selectedRequestCtg)) {
			this.refreshAssignee(paramBean);
			return;
		}


		if ( RequestOption.selectResource.equals(selectedRequestCtg) ) {
			if (ResourceRequestType.openFolder.equals(selectedType)) {
				AmResourceSelectionUtils.openFolder(paramList, masterPath);
				return;
			}

			if (ResourceRequestType.closeFolder.equals(selectedType)) {
				AmResourceSelectionUtils.closeFolder(paramList);
				return;
			}

			if (ResourceRequestType.selectFolder.equals(selectedType)) {
				this.selectFolder(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectSingleResource.equals(selectedType)) {

				this.selectSingleResource(paramBean, paramList);
				return;
			}
			
			if (ResourceRequestType.selectAllResource.equals(selectedType)) {

				this.selectAllResource(paramBean, paramList);
				return;
			}
		}

		// File Upload
		if (RequestOption.fileUpload.equals(selectedRequestCtg)) {
			this.uploadFile(paramBean, paramList);
			return;
		}
	}

	private void selectFolder(FlowRemovalRequestEditServiceBean paramBean, List<Object> paramList) {
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// Set Asset Selection List
		AmResourceSelectionUtils.setAssetSelectList(paramBean, paramList, masterPath, false);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}
	
	
	private void selectSingleResource(FlowRemovalRequestEditServiceBean paramBean, List<Object> paramList){

		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);
	}
	
	private void selectAllResource(FlowRemovalRequestEditServiceBean paramBean, List<Object> paramList){
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		// Set all selected files
		AmResourceSelectionUtils.selectFilesInFolder(paramBean, paramList, masterPath, false);
	}


	private void uploadFile(FlowRemovalRequestEditServiceBean paramBean, List<Object> paramList) {
		if( TriStringUtils.isEmpty( paramBean.getParam().getInputInfo().getCsvInputStreamBytes() ) ) {
			throw new ContinuableBusinessException(AmMessageId.AM001134E);
		}
		// Set all selected files
		AmResourceSelectionUtils.setSelectedFiles(paramList);

		// File Upload or Drag and Drop Selection
		AmResourceSelectionUtils.removalRequestFileUpload(paramBean, paramBean.getParam().getInputInfo(), paramList, fileUploadActions);

		// Build tree folder view again
		AmResourceSelectionUtils.rebuildTreeFolderView(paramList);
	}

	private void saveAsDraft(FlowRemovalRequestEditServiceBean paramBean) {
		String status = AmAreqStatusId.DraftRemovalRequest.getStatusId();

		this.executeRemovalRequest(paramBean, status);
	}

	private void submitChanges(FlowRemovalRequestEditServiceBean paramBean) {
		String status = AmAreqStatusId.RemovalRequested.getStatusId();

		this.executeRemovalRequest(paramBean, status);
	}

	private void executeRemovalRequest(FlowRemovalRequestEditServiceBean paramBean, String status) {

		try {

			String areqId = paramBean.getParam().getSelectedAreqId();
			IAreqEntity areqEntity = this.support.findAreqEntity(areqId);

			// Status Matrix Check
			{
				StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( areqEntity.getLotId() )
				.setAreqIds( areqId );
				StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			}

			String selectedLotId = areqEntity.getLotId();

			List<Object> paramForSelectedFiles = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
					paramBean.getParam().getResourceSelection(), selectedLotId, null);

			// Set all selected files
			AmResourceSelectionUtils.setSelectedFiles(paramForSelectedFiles);

			{
				List<Object> paramList = this.support.setParamList(paramBean.getResourceSelectionFolderView(),
						paramBean.getParam().getResourceSelection(), selectedLotId, null);

				AmResourceSelectionUtils.setResourceSelectedBySubmit(paramList,
						paramBean.getParam().getResourceSelection().getSelectedFileSet());
			}

			RemovalRequestEditInputBean inputBean = paramBean.getParam().getInputInfo();

			// Validate input information
			this.support.validateRemovalRequestInput(inputBean);

			Set<String> selectedFileSet = paramBean.getParam().getResourceSelection().getSelectedFileSet();

			if ( TriCollectionUtils.isEmpty( selectedFileSet )
					&& !( AmBusinessJudgUtils.isDraft( AmAreqStatusId.value( status ) )
							|| AmBusinessJudgUtils.isPendingRequest( AmAreqStatusId.value( status ) ) )) {

				throw new ContinuableBusinessException(AmMessageId.AM001141E);
			}

			IAreqDto areqDto = new AreqDto();
//			IAreqEntity areqEntity = this.support.findAreqEntity(paramBean.getParam().getSelectedAreqId());
			IPjtEntity pjtEntity = this.support.findPjtEntity( inputBean.getPjtId() );
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK, AmTables.AM_LOT_MDL_LNK );
			List<IAreqFileEntity> fileEntities = new ArrayList<IAreqFileEntity>();
			List<IAreqBinaryFileEntity> binaryFileEntities = new ArrayList<IAreqBinaryFileEntity>();

			// Set Areq Entity
			if(AmAreqStatusId.DraftRemovalRequest.getStatusId().equals(status)){
				this.support.setRemovalRequestEntity(paramBean, inputBean, areqEntity, areqEntity.getStsId());

			}else{
				this.support.setRemovalRequestEntity(paramBean, inputBean, areqEntity, status);
			}

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			List<Object> paramList = new ArrayList<Object>();

			IDelApplyParamInfo actionParam = new DelApplyParamInfo();
			{

				actionParam.setUserId( paramBean.getUserId() );
				actionParam.setDelApplyUser( paramBean.getUserName() );
				actionParam.setDelApplyUserId( paramBean.getUserId() );
				actionParam.setCtgId( inputBean.getCtgId() );
				actionParam.setMstoneId( inputBean.getMstoneId() );
				paramList.add( pjtEntity );
				paramList.add( lotDto );
				paramList.add( actionParam );
			}


			IDelApplyFileResult[] files = new IDelApplyFileResult[]{};

			// Get all files
			if (TriStringUtils.isNotEmpty(selectedFileSet)) {
				files = FlowChaLibMasterDelEditSupport.getDelFiles(selectedFileSet);
			}

			IDelApplyBinaryFileResult[] binaryFiles = new IDelApplyBinaryFileResult[]{};

			// Get all binary files
			if (TriStringUtils.isNotEmpty(inputBean.getCsvFilePath())) {
				binaryFiles = FlowChaLibMasterDelEditSupport.getBinaryDelFiles(paramList);
			} else {
				binaryFileEntities = this.support.findAreqBinaryFileEntities(paramBean.getParam().getSelectedAreqId());
				List<IDelApplyBinaryFileResult> binaryParsePath = new ArrayList<IDelApplyBinaryFileResult>();
				if (TriStringUtils.isNotEmpty(binaryFileEntities)) {
					for (IAreqBinaryFileEntity entity: binaryFileEntities) {
						IDelApplyBinaryFileResult binaryFile = new DelApplyBinaryFileResult();
						binaryFile.setFilePath(entity.getFilePath());
						binaryParsePath.add(binaryFile);
					}

					binaryFiles = binaryParsePath.toArray( new IDelApplyBinaryFileResult[] {} );
				}
			}

			{
				areqDto.setAreqEntity(areqEntity);

				paramList.add( areqEntity );
				paramList.add( areqDto );
				paramList.add( fileEntities );
				paramList.add( binaryFileEntities );
				paramList.add( files );
				paramList.add( binaryFiles );
			}

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
			if( VcsCategory.SVN.equals( scmType ) ) {

				List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
				paramList.add( reposEntities.toArray( new IVcsReposEntity[0] ) ) ;

				IPassMgtEntity passwordEntity = this.support.findPassMgtEntityBySVN();
				paramList.add( passwordEntity ) ;
				vcsService = factory.createService(scmType ,passwordEntity.getUserId(), passwordEntity.getPassword());

				Map<String,SVNStatus> statusMap = new HashMap<String,SVNStatus>();

				String workPath = lotDto.getLotEntity().getLotMwPath() ;

				FlowChaLibScmEditSupport.getStatus( vcsService , workPath , files , statusMap );
				areqDto = this.support.setAssetFileEntity(
						paramList,
						statusMap,
						areqDto );
				areqDto.getAreqEntity().setPjtId(pjtEntity.getPjtId());


			} else {
				throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
			}

			if (AmAreqStatusId.DraftRemovalRequest.getStatusId().equals(status)) {

				for (IDomain<IGeneralServiceBean> action : saveDraftActions) {
					action.execute(innerServiceDto);
				}
			} else {

				// Validate input information
				for (IDomain<IGeneralServiceBean> action : validateSubmitActions) {
					action.execute(innerServiceDto);
				}

				// Logical delete Draft Records
				{
					this.support.cleaningAreq(areqId);
				}

				// Logical delete Category Link
				{
					this.support.getUmFinderSupport().cleaningCtgLnk(AmTables.AM_AREQ, areqId);
				}

				// Logical delete Milestone Link
				{
					this.support.getUmFinderSupport().cleaningMstoneLnk(AmTables.AM_AREQ, areqId);

				}

				for (IDomain<IGeneralServiceBean> action : submitActions) {
					action.execute(innerServiceDto);
				}
			}

			if (TriStringUtils.isNotEmpty(inputBean.getCsvFilePath())) {
				File file = AmDesignBusinessRuleUtils.getDelApplyDefApplyNoPath( paramList );
				if (!file.exists()) {
					file.mkdirs();
				}
				for (IDomain<IGeneralServiceBean> action : handleUploadedFileActions) {
					action.execute(innerServiceDto);
				}
			}
			paramBean.getResult().setCompleted(true);
			if(AmAreqStatusId.DraftRemovalRequest.getStatusId().equals(status)){
				paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003024I);

			}else if (AmAreqStatusId.RemovalRequested.getStatusId().equals(status)){
				paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003022I);
			}
		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "IOError" );
		} catch (ScmException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "ScmError" );
		} catch (ScmSysException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "ScmSysError" );
		}

	}

	private void setEntityByInit(FlowRemovalRequestEditServiceBean paramBean, IAreqEntity areqEntity) {
		RemovalRequestEditInputBean inputInfo = new RemovalRequestEditInputBean();
		inputInfo.setPjtId(areqEntity.getPjtId());
		inputInfo.setReferenceId(areqEntity.getChgFactorNo());
		inputInfo.setGroupId(areqEntity.getGrpId());
		inputInfo.setSubject(areqEntity.getSummary());
		inputInfo.setContents(areqEntity.getContent());
		inputInfo.setAssigneeId(areqEntity.getAssigneeId());
		inputInfo.setCtgId(areqEntity.getCtgId());
		inputInfo.setMstoneId(areqEntity.getMstoneId());

		paramBean.getParam().setInputInfo(inputInfo);
	}

	public void generateDropBox(FlowRemovalRequestEditServiceBean paramBean, String lotId) {
		RemovalRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		// Set Pjt Views
		List<PjtViewBean> pjtViewList = this.support.getSelectPjtViewList(lotId);
		inputInfo.setPjtViews(FluentList.from( pjtViewList ).map( AmFluentFunctionUtils.toItemLabelsFromPjtViewBean ).asList());

		// Set Group Views
		ILotDto lotDto = this.support.findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
		List<IGrpEntity> grpEntity = this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId());

		String[] groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto, grpEntity).toArray(new String[0]);
		inputInfo.setGroupViews(FluentList.from(this.support.getUmFinderSupport().findGroupByGroupIds(groupIds))
				.map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		// Set Assignee Views
		List<IUserEntity> userEntityList = this.umSupport.findUserByGroup(inputInfo.getGroupId());
		inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

		// Set Category Views
		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
		inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		// Set Milestone Views
		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

	}

	private void refreshGroup(FlowRemovalRequestEditServiceBean paramBean, String lotId) {
		RemovalRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getAssigneeId())) {
			List<String> groupNameList = this.support.getSelectGroupNameList(lotId, paramBean.getUserId());
			Set<String> groupNameSet = new HashSet<String>(groupNameList);

			List<IGrpEntity> grpEntityList = this.support.getUmFinderSupport().findGroupByUserId(inputInfo.getAssigneeId());
			List<IGrpEntity> selectedGrpEntity = new ArrayList<IGrpEntity>();
			for (IGrpEntity grpEntity : grpEntityList) {
				if (groupNameSet.contains(grpEntity.getGrpNm())) {
					selectedGrpEntity.add(grpEntity);
				}
			}
			inputInfo.setGroupViews(FluentList.from(selectedGrpEntity).map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());
		}
	}

	private void refreshAssignee(FlowRemovalRequestEditServiceBean paramBean) {
		RemovalRequestEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		if (TriStringUtils.isNotEmpty(inputInfo.getGroupId())) {
			List<IUserEntity> userEntityList = this.support.getUmFinderSupport().findUserByGroup(inputInfo.getGroupId());
			inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());
		}
	}


	private String[] getRemovalRequestFiles(String selectedAreqId) {
		List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(selectedAreqId);

		List<String> resourcesPath = new ArrayList<String>();

		for (IAreqFileEntity entity: areqFileEntities) {
			String relativePath = entity.getFilePath();
			resourcesPath.add(TriStringUtils.convertPath(relativePath, true));
		}
		return resourcesPath.toArray(new String[0]);
	}
}
