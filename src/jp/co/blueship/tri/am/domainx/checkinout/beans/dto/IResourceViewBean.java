package jp.co.blueship.tri.am.domainx.checkinout.beans.dto;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface IResourceViewBean {
	public boolean isFile();
	public boolean isDirectory();
	public String getPath();
	public String getName();
	public String getSize();
	public int getCount();
}
