package jp.co.blueship.tri.am.domainx.checkinout.dto;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowRemovalRequestRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibCompMasterDelCancelServiceBean() );
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowRemovalRequestRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;

		public String getSelectedAreqId() {
			return areqId;
		}

		public RequestParam setSelectedAreqId(String selectedAreqId) {
			this.areqId = selectedAreqId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
