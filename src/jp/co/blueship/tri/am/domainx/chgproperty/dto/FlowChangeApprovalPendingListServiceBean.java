package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriSelectedId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowChangeApprovalPendingListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ChangePropertyView> changePropertyViews = new ArrayList<ChangePropertyView>();
	private IPageNoInfo page = new PageNoInfo();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public List<ChangePropertyView> getChangePropertyViews() {
		return changePropertyViews;
	}
	public FlowChangeApprovalPendingListServiceBean setChangePropertyViews(List<ChangePropertyView> changePropertyViews) {
		this.changePropertyViews = changePropertyViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowChangeApprovalPendingListServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangeApprovalPendingListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();
		private int linesPerPage = 20;
		private ListSelection listSelection = new ListSelection();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public ListSelection getListSelection() {
			return listSelection;
		}
		public RequestParam setListSelection(ListSelection listSelection) {
			this.listSelection = listSelection;
			return this;
		}

	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}
		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}
		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
	}

	/**
	 * List Selection
	 */
	public class ListSelection extends jp.co.blueship.tri.fw.svc.beans.dto.ListSelection {
	}

	/**
	 * Approval List
	 */
	public class ChangePropertyView {
		private boolean isSelected = false;
		private String pjtId;
		private String referenceId;
		private List<AreqView> areqViews;

		public boolean isSelected() {
			return isSelected;
		}
		public ChangePropertyView setSelected(boolean isSelected) {
			this.isSelected = isSelected;
			return this;
		}

		@TriSelectedId
		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public List<AreqView> getAreqViews() {
			return areqViews;
		}
		public ChangePropertyView setAreqViews(List<AreqView> areqViews) {
			this.areqViews = areqViews;
			return this;
		}
	}

	/**
	 * Pending Request List
	 */
	public class AreqView {
		private boolean isApprovalEnabled = false;

		private AreqCtgCd areqType;
		private String areqId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String requestDate;
		private String stsId;
		private String status;

		public boolean isApprovalEnabled() {
			return isApprovalEnabled;
		}
		public AreqView setApprovalEnabled(boolean isApprovalEnabled) {
			this.isApprovalEnabled = isApprovalEnabled;
			return this;
		}

		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public AreqView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public boolean isCheckoutRequest() {
			return AmBusinessJudgUtils.isLendApply(this.areqType);
		}
		public boolean isCheckinRequest() {
			return AmBusinessJudgUtils.isReturnApply(this.areqType);
		}
		public boolean isRemovalRequest() {
			return AmBusinessJudgUtils.isDeleteApply(this.areqType);
		}

		public String getAreqId() {
			return areqId;
		}
		public AreqView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public AreqView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public AreqView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public AreqView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public AreqView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getRequestDate() {
			return requestDate;
		}
		public AreqView setRequestDate(String requestDate) {
			this.requestDate = requestDate;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public AreqView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public AreqView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private List<String> selectedIds = new ArrayList<String>();

		public List<String> getSelectedIds() {
			return selectedIds;
		}
		public void setSelectedIds(List<String> selectedIds) {
			this.selectedIds = selectedIds;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
