package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyCloseServiceMultipleCloseBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	
	{
		this.setInnerService( new FlowChaLibPjtCloseServiceBean() );
	}
	
	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangePropertyCloseServiceMultipleCloseBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private String[] selectedPjtIds = new String[0];
		private ChangePropertyCloseInput inputInfo = new ChangePropertyCloseInput();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String[] getSelectedPjtIds() {
			return selectedPjtIds;
		}
		public RequestParam setSelectedPjtIds(String... selectedPjtIds) {
			this.selectedPjtIds = selectedPjtIds;
			return this;
		}

		public ChangePropertyCloseInput getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ChangePropertyCloseInput inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Close Input Information
	 */
	public class ChangePropertyCloseInput {
		private String comment;

		public String getComment() {
			return comment;
		}
		public ChangePropertyCloseInput setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
