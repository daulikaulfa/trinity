package jp.co.blueship.tri.am.domainx.chgproperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.RequestParam;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

public class FlowChangeApprovedListService implements IDomain<FlowChangeApprovedListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangeApprovedListServiceBean> execute(
			IServiceDto<FlowChangeApprovedListServiceBean> serviceDto) {
		FlowChangeApprovedListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified!");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
		return serviceDto;
	}

	/**
	 * Call when initializing by service
	 *
	 * @param serviceBean
	 */
	private void init ( FlowChangeApprovedListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

		ILotDto lotDto = this.support.findLotDto( lotId );
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
			this.support.getUmFinderSupport().getGrpDao(),//
			this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

		this.generateDropBox(lotId, serviceBean);
		this.onChange( serviceBean );
	}

	/**
	 * @param lotId Target lot-ID
	 * @param serviceBean Service bean
	 */
	private void generateDropBox( String lotId, FlowChangeApprovedListServiceBean serviceBean ) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
	}

	/**
	 * Call when request by service
	 *
	 * @param paramBean
	 */
	private void onChange( FlowChangeApprovedListServiceBean paramBean ) {
		ISqlSort sort = AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByPjtApproveList();
		int pageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		IEntityLimit<IPjtAvlEntity> limit = this.support.getPjtAvlDao().findListPjtAvl(
												this.getCondition(paramBean).getCondition(),
												sort,
												pageNo,
												linesPerPage);

		if ( (null != limit) && (null != limit.getEntities())) {
			List<IPjtAvlDto> pjtDtoList = this.support.findPjtAvlDto( limit.getEntities() );
			paramBean.setChangePropertyViews( this.getPjtViewBean(paramBean, pjtDtoList ) );

			ILimit viewLimit = new PageLimit();
			viewLimit.setViewRows(linesPerPage);
			viewLimit.setMaxRows(paramBean.getChangePropertyViews().size());
			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), viewLimit);
			paramBean.setPage(page);

		} else {
			paramBean.setPage(new PageNoInfo());
			paramBean.setChangePropertyViews(new ArrayList<ChangePropertyView>() );
		}

	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private PjtAvlCondition getCondition( FlowChangeApprovedListServiceBean serviceBean ) {

		RequestParam param = serviceBean.getParam();
		SearchCondition searchParam = param.getSearchCondition();
		PjtAvlCondition condition = new PjtAvlCondition();

		condition.setLotId( param.getSelectedLotId() );
		condition.setStsId( AmPjtStatusId.ChangePropertyApproved.getStatusId() );
		if (  TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId(searchParam.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId(searchParam.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		return condition;
	}

	/**
	 * @param paramBean Service Bean
	 * @param pjtDtoList Pending Change Property Approval DTO List
	 */
	private List<ChangePropertyView> getPjtViewBean (
							FlowChangeApprovedListServiceBean paramBean,
							List<IPjtAvlDto> pjtDtoList) {
		Map<String, ChangePropertyView> viewBeanMap = new LinkedHashMap<String, ChangePropertyView>();
		

		for (IPjtAvlDto pjtDto : pjtDtoList) {
			IPjtEntity entity = pjtDto.getPjtEntity();

			List<AreqView> areqViewList = this.getAreqViewBean(paramBean, pjtDto);

			if( areqViewList.size() > 0 ) {
				if ( !viewBeanMap.containsKey(entity.getPjtId()) ) {
					ChangePropertyView view = paramBean.new ChangePropertyView()
							.setPjtId( entity.getPjtId() )
							.setReferenceId( entity.getChgFactorNo() )
							.setAreqViews(areqViewList)
					;
					viewBeanMap.put(entity.getPjtId(), view);
				} else {
					viewBeanMap.get(entity.getPjtId()).getAreqViews().addAll(areqViewList);
				}
			}
		}
		
		List<ChangePropertyView> viewBeanList = new ArrayList<ChangePropertyView>(viewBeanMap.values());
		return viewBeanList;
	}

	/**
	 * @param serviceBean Service Bean
	 * @param pjtDto Change Property DTO
	 */
	private List<AreqView> getAreqViewBean(
							FlowChangeApprovedListServiceBean serviceBean,
							IPjtAvlDto pjtDto) {
		IPjtEntity pjtEntity = pjtDto.getPjtEntity();
		List<IPjtAvlAreqLnkEntity> pjtAreqLnk = pjtDto.getPjtAvlAreqLnkEntityList();

		AreqCondition condition = (AreqCondition) AmDBSearchConditionAddonUtils.getAreqCondition(pjtEntity.getPjtId());

		Set<String> areqIdSet = new TreeSet<String>();
		for (IPjtAvlAreqLnkEntity areqId : pjtAreqLnk) {
			areqIdSet.add(areqId.getAreqId());
		}
		condition.setAreqIds(areqIdSet.toArray(new String[0]));
		condition.setStsIds(ExtractStatusAddonUtils.getAssetApplyBaseStatusByOverPjtApprove());
		
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.areqId, TriSortOrder.Asc, 1);

		int viewRows = 0;
		int pageNo = 1;

		IEntityLimit<IAreqEntity> entityLimit = this.support.getAreqDao().find(
									condition.getCondition(),
									sort,
									pageNo,
									viewRows);

		List<AreqView> viewBeanList = new ArrayList<AreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		if ( !TriStringUtils.isEmpty( entityLimit.getEntities() ) ) {
			for (IAreqEntity entity : entityLimit.getEntities()) {
				AreqView view = serviceBean.new AreqView()
						.setAreqType(AreqCtgCd.value(entity.getAreqCtgCd()))
						.setAreqId( entity.getAreqId() )
						.setSubmitterNm( entity.getRegUserNm() )
						.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(entity.getRegUserId()))
						.setAssigneeNm( entity.getAssigneeNm() )
						.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
						.setRequestDate( TriDateUtils.convertViewDateFormat(entity.getReqTimestamp(), formatYMD) )
						.setApprovedDate( TriDateUtils.convertViewDateFormat(entity.getPjtAvlTimestamp(), formatYMD) )
						.setStsId( entity.getProcStsId() )
						.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
				;

				viewBeanList.add(view);
			}
		}
		return viewBeanList;
	}
}
