package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangeApprovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibPjtApproveServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ChangeApprovalInputInfo inputInfo = new ChangeApprovalInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}

		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ChangeApprovalInputInfo getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(ChangeApprovalInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 *
	 * Change Approval Input Information
	 *
	 */
	public class ChangeApprovalInputInfo {
		private String[] areqIds = null;
		private String comment;

		public String[] getSelectedAreqIds() {
			return areqIds;
		}
		public ChangeApprovalInputInfo setSelectedAreqIds(String... areqIds) {
			this.areqIds = areqIds;
			return this;
		}

		public String getComment() {
			return comment;
		}
		public ChangeApprovalInputInfo setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
