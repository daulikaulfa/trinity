package jp.co.blueship.tri.am.domainx.chgproperty;

import java.util.List;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedReturnToPendingServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.bm.BmFluentFunctionUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChangeApprovedReturnToPendingService implements IDomain<FlowChangeApprovedReturnToPendingServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;
	private IDomain<IGeneralServiceBean> confirmService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	public void setConfirmService(IDomain<IGeneralServiceBean> confirmService) {
		this.confirmService = confirmService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChangeApprovedReturnToPendingServiceBean> execute(
			IServiceDto<FlowChangeApprovedReturnToPendingServiceBean> serviceDto) {

		FlowChangeApprovedReturnToPendingServiceBean serviceBean = serviceDto.getServiceBean();
		FlowChaLibPjtApproveCancelServiceBean innerServiceBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String pjtId = serviceBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if (RequestType.validate.equals(serviceBean.getParam().getRequestType())) {
				this.validate(serviceBean);
			}

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, serviceBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerServiceBean);
		}

		return serviceDto;
	}

	private void validate(FlowChangeApprovedReturnToPendingServiceBean serviceBean) {
		String pjtId = serviceBean.getParam().getSelectedPjtId();
		IPjtEntity pjtEntity = this.support.findPjtEntity( pjtId );
		// Status Matrix Check
		{
			List<IAreqEntity> areqEntities = this.findAreqEntityByPjtId( pjtId );
			AreqDtoList areqDtoList = this.support.findAreqDtoList( areqEntities );
			String[] bpIds = this.getBpIds( areqDtoList );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( serviceBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( pjtEntity.getLotId() )
					.setPjtIds( pjtId )
					.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) )
					.setBpIds( bpIds );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtApproveCancelServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		dto.setServiceBean(innerBean);

		this.beforeExecution(serviceBean, innerBean);

		List<IPjtAvlEntity> pjtAvlEntities = this.support.findPjtAvlEntities(pjtId);
		for (IPjtAvlEntity pjtAvlEntity : pjtAvlEntities) {
			if(AmPjtStatusId.ChangePropertyApproved.getStatusId().equals(pjtAvlEntity.getStsId())) {
				innerBean.setPjtAvlSeqNo( pjtAvlEntity.getPjtAvlSeqNo() );
				innerBean.setReferer( ChaLibScreenID.PJT_APPROVE_LIST );
				innerBean.setForward( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM );
				confirmService.execute(dto);
			}

		}
	}

	private void submitChanges(FlowChangeApprovedReturnToPendingServiceBean serviceBean) {
		String pjtId = serviceBean.getParam().getSelectedPjtId();
		{
			IPjtEntity pjtEntity = this.support.findPjtEntity( pjtId );
			List<IAreqEntity> areqEntities = this.findAreqEntityByPjtId( pjtId );
			AreqDtoList areqDtoList = this.support.findAreqDtoList( areqEntities );
			String[] bpIds = this.getBpIds( areqDtoList );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( serviceBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( pjtEntity.getLotId() )
					.setPjtIds( pjtId )
					.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) )
					.setBpIds( bpIds )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtApproveCancelServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		dto.setServiceBean(innerBean);

		this.beforeExecution(serviceBean, innerBean);

		List<IPjtAvlEntity> pjtAvlEntities = this.support.findPjtAvlEntities(pjtId);
		for (IPjtAvlEntity pjtAvlEntity : pjtAvlEntities) {
			if(AmPjtStatusId.ChangePropertyApproved.getStatusId().equals(pjtAvlEntity.getStsId())) {
				innerBean.setPjtAvlSeqNo(pjtAvlEntity.getPjtAvlSeqNo());

				innerBean.setReferer( ChaLibScreenID.PJT_APPROVE_LIST );
				innerBean.setForward( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM );
				confirmService.execute(dto);

				innerBean.setReferer(ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM);
				innerBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE_CANCEL);
				confirmService.execute(dto);
				completeService.execute(dto);
				mailService.execute(dto);
			}
		}

		this.afterExecution(innerBean, serviceBean);
	}

	private void beforeExecution(FlowChangeApprovedReturnToPendingServiceBean src,
								 FlowChaLibPjtApproveCancelServiceBean dest) {
		String selectedPjtId = src.getParam().getSelectedPjtId();
		dest.setSelectedPjtNo( selectedPjtId );
		dest.setUserId		 ( src.getUserId() );
		dest.setUserName	 ( src.getUserName() );
		dest.setProcId		 ( src.getProcId() );
		dest.setFlowAction	 ( src.getFlowAction() );

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.getPjtEditInputBean().setApproveCancelComment( src.getParam().getComment() );
		}
	}

	private void afterExecution(FlowChaLibPjtApproveCancelServiceBean src,
								FlowChangeApprovedReturnToPendingServiceBean dest) {

		dest.getResult().setCompleted(true);
		dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003006I);
	}

	private String[] getBpIds(AreqDtoList areqDtoList) {

		String[] areqIds = FluentList.from(areqDtoList).map(AmFluentFunctionUtils.toAreqIdsFromAreqDto).toArray(new String[0]);

		BpAreqLnkCondition bpAreqCondition = new BpAreqLnkCondition();
		bpAreqCondition.setAreqIds( areqIds );
		List<IBpAreqLnkEntity> bpAreqEntities = this.support.getBmFinderSupport().getBpAreqLnkDao().find( bpAreqCondition.getCondition() );

		String[] bpIds = FluentList.from(bpAreqEntities).map(BmFluentFunctionUtils.toBpIdFromBpAreqLnk).toArray(new String[0]);

		return bpIds;

	}

	public List<IAreqEntity> findAreqEntityByPjtId( String pjtId ) {
		AreqCondition condition = new AreqCondition();
		condition.setPjtId(pjtId);
		condition.setStsIds(ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove());

		List<IAreqEntity> areqEntities = this.support.getAreqDao().find( condition.getCondition() );

		return areqEntities;
	}

}