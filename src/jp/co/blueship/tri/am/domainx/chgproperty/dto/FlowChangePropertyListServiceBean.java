package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriSelectedId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class FlowChangePropertyListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ChangePropertyView> changePropertyViews = new ArrayList<ChangePropertyView>();
	private IPageNoInfo page = new PageNoInfo();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public List<ChangePropertyView> getChangePropertyViews() {
		return changePropertyViews;
	}
	public FlowChangePropertyListServiceBean setChangePropertyViews( List<ChangePropertyView> changePropertyViews ) {
		this.changePropertyViews = changePropertyViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowChangePropertyListServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangePropertyListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();
		private int linesPerPage = 20;
		private ListSelection listSelection = new ListSelection();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public ListSelection getListSelection() {
			return listSelection;
		}
		public RequestParam setListSelection(ListSelection listSelection) {
			this.listSelection = listSelection;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		@Expose private String stsId = null;
		@Expose private String pjtId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		private List<ItemLabelsBean> categoryViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getPjtId() {
			return pjtId;
		}
		public SearchCondition setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}
		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
		public List<ItemLabelsBean> getCategoryViews() {
			return categoryViews;
		}
		public SearchCondition setCategoryViews(List<ItemLabelsBean> categoryViews) {
			this.categoryViews = categoryViews;
			return this;
		}
		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
	}

	/**
	 * List Selection
	 */
	public class ListSelection extends jp.co.blueship.tri.fw.svc.beans.dto.ListSelection {
	}

	/**
	 * Change Property
	 */
	public class ChangePropertyView {
		private boolean isCloseEnabled = false;

		private boolean selected;
		private String pjtId;
		private String summary;
		private String referenceId;
		private String referenceCategoryNm;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String createdDate;
		private String updDate;
		private String stsId;
		private String status;

		public boolean isCloseEnabled() {
			return isCloseEnabled;
		}
		public ChangePropertyView setCloseEnabled(boolean isCloseEnabled) {
			this.isCloseEnabled = isCloseEnabled;
			return this;
		}

		public boolean isSelected() {
			return selected;
		}
		public ChangePropertyView setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		@TriSelectedId
		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public ChangePropertyView setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public ChangePropertyView setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public ChangePropertyView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public ChangePropertyView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getCreatedDate() {
			return createdDate;
		}
		public ChangePropertyView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public String getUpdDate() {
			return updDate;
		}
		public ChangePropertyView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ChangePropertyView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangePropertyView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private List<String> selectedIds = new ArrayList<String>();

		public List<String> getSelectedIds() {
			return selectedIds;
		}
		public void setSelectedIds(List<String> selectedIds) {
			this.selectedIds = selectedIds;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
