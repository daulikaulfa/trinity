package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChangePropertyOverviewServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ChangePropertyOverview detailsView = new ChangePropertyOverview();

	public RequestParam getParam() {
		return param;
	}

	public ChangePropertyOverview getDetailsView() {
		return detailsView;
	}
	public FlowChangePropertyOverviewServiceBean setDetailsView(ChangePropertyOverview detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String pjtId;

		public String getSelectedPjtId() {
			return pjtId;
		}
		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
	}

	public class ChangePropertyOverview {
		private String summary;
		private String stsId;
		private String status;
		private String submitterNm;
		private String assigneeNm;
		private String createdTime;
		private String referenceId;
		private String referenceCategoryNm;

		public String getSummary() {
			return summary;
		}
		public ChangePropertyOverview setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ChangePropertyOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangePropertyOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyOverview setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public ChangePropertyOverview setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getCreatedTime() {
			return createdTime;
		}
		public ChangePropertyOverview setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyOverview setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public ChangePropertyOverview setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}

	}
}
