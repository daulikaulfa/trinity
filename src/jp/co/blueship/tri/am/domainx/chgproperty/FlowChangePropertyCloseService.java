package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceBean.ChangePropertyCloseInput;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * Provide the following backend services.
 * <br> - Close
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyCloseService implements IDomain<FlowChangePropertyCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeActionByTestComplete = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}
	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}
	public void setStatusIfNotMergeMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeAction = action;
	}
	public void setStatusIfNotMergeMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeActionByTestComplete = action;
	}
	public void setConfirmationService(IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}
	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}
	@Override
	public IServiceDto<FlowChangePropertyCloseServiceBean> execute(
			IServiceDto<FlowChangePropertyCloseServiceBean> serviceDto) {

		FlowChangePropertyCloseServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibPjtCloseServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
            	this.validate(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
            	this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}  finally {
            ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
		return serviceDto;
	}

	private void validate(FlowChangePropertyCloseServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String pjtId = paramBean.getParam().getSelectedPjtId();
			IPjtEntity pjtEntity = this.support.findPjtEntity( pjtId );
			ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit(pjtId, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
				.toArray(new IAreqEntity[0]);
			StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( pjtEntity.getLotId() )
				.setPjtIds( pjtId )
				.setAreqIds( support.getAssetApplyNo(applyList) );
			if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
				if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
					// テスト完了運用を行う
					statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
				} else {
					// テスト完了運用を行わない
					statusDto.setActionList( statusMatrixIfNotMergeAction );
				}
			}
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer(ChaLibScreenID.PJT_CLOSE_CONFIRM);
		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CLOSE);
			confirmationService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);

	}

	private void submitChanges(FlowChangePropertyCloseServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String pjtId = paramBean.getParam().getSelectedPjtId();
			IPjtEntity pjtEntity = this.support.findPjtEntity( pjtId );
			ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit(pjtId, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
				.toArray(new IAreqEntity[0]);
			StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( pjtEntity.getLotId() )
				.setPjtIds( pjtId )
				.setAreqIds( support.getAssetApplyNo(applyList) );
			if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
				if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
					// テスト完了運用を行う
					statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
				} else {
					// テスト完了運用を行わない
					statusDto.setActionList( statusMatrixIfNotMergeAction );
				}
			}
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			serviceBean.setSelectedLotNo( pjtEntity.getLotId());
		}

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer(ChaLibScreenID.PJT_CLOSE_CONFIRM);
		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CLOSE);
			confirmationService.execute(dto);
		}

		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CLOSE);
			completeService.execute(dto);
		}
		
		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CLOSE);
			mailService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003003I);

	}

	private void beforeExecution(FlowChangePropertyCloseServiceBean src, FlowChaLibPjtCloseServiceBean dest) {
		ChangePropertyCloseInput srcInfo = src.getParam().getInputInfo();
		PjtEditInputBean destInfo = dest.getPjtEditInputBean();
		src.getResult().setCompleted(false);

		if (RequestType.validate.equals(src.getParam().getRequestType())) {
			if (null == destInfo) {
				destInfo = new PjtEditInputBean();
				dest.setPjtEditInputBean(destInfo);
			}

			dest.setSelectedPjtNo(src.getParam().getSelectedPjtId());
			destInfo.setCloseComment(srcInfo.getComment());
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			if (null == destInfo) {
				destInfo = new PjtEditInputBean();
				dest.setPjtEditInputBean(destInfo);
			}

			dest.setSelectedPjtNo(src.getParam().getSelectedPjtId());
			destInfo.setCloseComment(srcInfo.getComment());
		}
	
	}

}
