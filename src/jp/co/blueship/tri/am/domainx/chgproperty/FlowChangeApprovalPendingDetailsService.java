package jp.co.blueship.tri.am.domainx.chgproperty;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangeApprovalPendingRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangeApprovedRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 *
 * @version V4.00.00
 * @author Sam
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChangeApprovalPendingDetailsService implements IDomain<FlowChangeApprovalPendingDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangeApprovalPendingDetailsServiceBean> execute(
			IServiceDto<FlowChangeApprovalPendingDetailsServiceBean> serviceDto) {

		FlowChangeApprovalPendingDetailsServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}else if(RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				paramBean.getApprovalPendingRequestViews().clear();
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * @param paramBean
	 *  Service Bean
	 */
	private void init(FlowChangeApprovalPendingDetailsServiceBean paramBean) {

		String pjtId = paramBean.getParam().getSelectedPjtId();
		IPjtEntity pjtEntity = this.support.findPjtEntity(pjtId);

		AreqCondition condition = new AreqCondition();
		condition.setPjtId(pjtId);
		List<IAreqEntity> areqEntities = this.support.getAreqDao().find( condition.getCondition() );

		setPjtDetails(paramBean,pjtEntity);
		setAreqEntities(paramBean,areqEntities);
	}

	private void setPjtDetails(FlowChangeApprovalPendingDetailsServiceBean serviceBean,IPjtEntity pjtEntity) {

		ChangePropertyDetailsView view = serviceBean.getDetailsView();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );
		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

		view.setPjtId(pjtEntity.getPjtId())
				.setChgFactorId(pjtEntity.getChgFactorId())
				.setReferenceCategoryNm(pjtEntity.getChgFactorId())
				.setSummary(pjtEntity.getSummary())
				.setContents(pjtEntity.getContent())
				.setStsId(pjtEntity.getProcStsId())
				.setStatus(sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()))
				.setSubmitterNm(pjtEntity.getRegUserNm())
				.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(pjtEntity.getRegUserId()))
				.setAssigneeNm(pjtEntity.getAssigneeNm())
				.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(pjtEntity.getAssigneeId()))
				.setCategoryNm(pjtEntity.getCtgNm())
				.setMstoneNm(pjtEntity.getMstoneNm())
				.setCreatedTime(TriDateUtils.convertViewDateFormat(pjtEntity.getRegTimestamp(),formatYMDHM))
				.setUpdTime(TriDateUtils.convertViewDateFormat(pjtEntity.getUpdTimestamp(),formatYMDHM));

		serviceBean.setDetailsView(view);
	}

	private void setAreqEntities(FlowChangeApprovalPendingDetailsServiceBean serviceBean, List<IAreqEntity> areqEntities){

		List<ChangeApprovalPendingRequestView> areqCurrentViewList = serviceBean.getApprovalPendingRequestViews();
		areqCurrentViewList = new ArrayList<ChangeApprovalPendingRequestView>();
		List<ChangeApprovedRequestView> areqPastViewList = serviceBean.getApprovedRequestViews();
		areqPastViewList = new ArrayList<ChangeApprovedRequestView>();

		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IAreqEntity areqEntity : areqEntities) {
			String stsId = areqEntity.getStsId();
			if (AmAreqStatusId.CheckinRequested.equals(stsId)||AmAreqStatusId.RemovalRequested.equals(stsId)) {

			ChangeApprovalPendingRequestView areqCurrentView = serviceBean.new ChangeApprovalPendingRequestView();
			areqCurrentView.setAreqType(AreqCtgCd.value(areqEntity.getAreqCtgCd()))
							.setAreqId(areqEntity.getAreqId())
							.setSubject(areqEntity.getSummary())
							.setRequestTime(TriDateUtils.convertViewDateFormat(areqEntity.getReqTimestamp(),formatYMDHM))
							.setSubmitterNm(areqEntity.getRegUserNm())
							.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(areqEntity.getRegUserId()))
							.setGroupNm(areqEntity.getGrpNm())
							.setStsId(areqEntity.getProcStsId())
							.setStatus(sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()));

				areqCurrentViewList.add(areqCurrentView);
			}
			if (AmAreqStatusId.CheckinRequestApproved.equals(stsId)||AmAreqStatusId.RemovalRequestApproved.equals(stsId)){

				ChangeApprovedRequestView areqPastView = serviceBean.new ChangeApprovedRequestView();
				areqPastView.setAreqType(AreqCtgCd.value(areqEntity.getAreqCtgCd()))
							.setAreqId(areqEntity.getAreqId())
							.setSubject(areqEntity.getSummary())
							.setRequestTime(TriDateUtils.convertViewDateFormat(areqEntity.getReqTimestamp(),formatYMDHM))
							.setSubmitterNm(areqEntity.getRegUserNm())
							.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(areqEntity.getRegUserId()))
							.setGroupNm(areqEntity.getGrpNm())
							.setStsId(areqEntity.getProcStsId())
							.setStatus(sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()));

				areqPastViewList.add(areqPastView);
			}
		}
		serviceBean.setApprovalPendingRequestViews(areqCurrentViewList);
		serviceBean.setApprovedRequestViews(areqPastViewList);
	}
}



