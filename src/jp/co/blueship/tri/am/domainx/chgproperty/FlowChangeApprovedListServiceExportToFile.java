package jp.co.blueship.tri.am.domainx.chgproperty;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceExportToFileBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.SearchCondition;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;


public class FlowChangeApprovedListServiceExportToFile implements IDomain<FlowChangeApprovedListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private IDomain<IGeneralServiceBean> changeApprovedList = null;

	public void setChangeApprovedList(IDomain<IGeneralServiceBean> changeApprovedList) {
		this.changeApprovedList = changeApprovedList;
	}

	@Override
	public IServiceDto<FlowChangeApprovedListServiceExportToFileBean> execute(
			IServiceDto<FlowChangeApprovedListServiceExportToFileBean> serviceDto) {
		FlowChangeApprovedListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowChangeApprovedListServiceBean innerServiceBean = paramBean.getInnerService();


		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowChangeApprovedListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowChangeApprovedListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChangeApprovedListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());

			changeApprovedList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		changeApprovedList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowChangeApprovedListServiceExportToFileBean src, FlowChangeApprovedListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);

			destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
			destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
		}
	}


	private void afterExecution(FlowChangeApprovedListServiceBean src, FlowChangeApprovedListServiceExportToFileBean dest) {
		List<ChangePropertyView> srcChangePropertyViews = src.getChangePropertyViews();
		List<ChangePropertyView> destChangePropertyViews = new ArrayList<ChangePropertyView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcChangePropertyViews.size(); i++) {
				ChangePropertyView view = srcChangePropertyViews.get(i);
				destChangePropertyViews.add(view);
			}
			dest.setChangePropertyViews(destChangePropertyViews);
		}
	}
}
