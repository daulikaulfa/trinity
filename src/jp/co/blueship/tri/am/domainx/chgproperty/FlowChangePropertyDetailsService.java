package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyDetailsViewBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyDetailsServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * Provide the following backend services.
 * <br> - Details
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowChangePropertyDetailsService implements IDomain<FlowChangePropertyDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangePropertyDetailsServiceBean> execute(
			IServiceDto<FlowChangePropertyDetailsServiceBean> serviceDto) {

		FlowChangePropertyDetailsServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String pjtId = serviceBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType()) ) {

				IPjtEntity pjtEntity = this.support.findPjtEntity(pjtId);
				ILotDto lotDto = this.support.findLotDto(pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

				ChangePropertyDetailsViewBean changePropertyDetailsViewBean =
						getChangePropertyDetails(serviceBean, pjtEntity);

				serviceBean.setChangePropertyDetailsView(changePropertyDetailsViewBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, serviceBean.getFlowAction());
		}
	}

	private ChangePropertyDetailsViewBean getChangePropertyDetails (
											FlowChangePropertyDetailsServiceBean serviceBean,
											IPjtEntity pjtEntity) {

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

		String checkStatusTestComplete = sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete);

		ChangePropertyDetailsViewBean view = new ChangePropertyDetailsViewBean()
			.setPjtId			( pjtEntity.getPjtId() )
			.setReferenceId		( pjtEntity.getChgFactorNo() )
			.setStsId			( pjtEntity.getProcStsId() )
			.setStatus			( sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()) )
			.setReferenceCategoryId(pjtEntity.getChgFactorId())
			.setReferenceCategoryNm
								( pjtEntity.getChgFactorId() )
			.setSummary		( pjtEntity.getSummary() )
			.setContents		( pjtEntity.getContent() )
			.setSubmitterNm		( pjtEntity.getRegUserNm() )
			.setSubmitterIconPath(this.support.getUmFinderSupport().getIconPath(pjtEntity.getRegUserId()))
			.setAssigneeNm		( pjtEntity.getAssigneeNm() )
			.setAssigneeIconPath(this.support.getUmFinderSupport().getIconPath(pjtEntity.getAssigneeId()))
			.setCategoryNm		( pjtEntity.getCtgNm() )
			.setMstoneNm		( pjtEntity.getMstoneNm() )
			.setClosed			( AmPjtStatusId.ChangePropertyClosed.getStatusId().equals(pjtEntity.getStsId()) )
			.setTestCompletionEnabled
								( StatusFlg.value(checkStatusTestComplete).parseBoolean() )
			;

		view.setCreatedTime(
				TriDateUtils.convertViewDateFormat(
						pjtEntity.getRegTimestamp(),
						TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
				).toString() );
		view.setUpdTime(
				TriDateUtils.convertViewDateFormat(
						pjtEntity.getUpdTimestamp(),
						TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
				).toString() );

			return view;
	}

}
