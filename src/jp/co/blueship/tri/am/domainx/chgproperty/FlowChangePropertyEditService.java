package jp.co.blueship.tri.am.domainx.chgproperty;

import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyEditServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyEditServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * Provide the following backend services.
 * <br> - Edit
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyEditService implements IDomain<FlowChangePropertyEditServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support;
	private IUmFinderSupport umSupport;

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	public void setInputService(IDomain<IGeneralServiceBean> inputService) {
		this.inputService = inputService;
	}

	public void setConfirmationService(
			IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowChangePropertyEditServiceBean> execute(
			IServiceDto<FlowChangePropertyEditServiceBean> serviceDto) {

		FlowChangePropertyEditServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibPjtModifyServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedPjtId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals( paramBean.getParam().getRequestType() )) {
				this.onChange( paramBean );
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}


		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
        } finally {
            ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
		return serviceDto;
	}

	private void init(FlowChangePropertyEditServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtModifyServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		this.beforeExecution(paramBean, innerServiceBean);
		innerServiceBean.setReferer(ChaLibScreenID.LIB_TOP);

		// Call modify input service
		{
			innerServiceBean.setForward(ChaLibScreenID.PJT_MODIFY_INPUT);
			inputService.execute(dto);
		}
		this.afterExecution(innerServiceBean, paramBean);

	}

	private void onChange( FlowChangePropertyEditServiceBean paramBean) {

		ChangePropertyEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		String lotId = support.findPjtEntity(paramBean.getParam().getSelectedPjtId()).getLotId();

		if ( RequestOption.refreshCategory.equals( paramBean.getParam().getRequestOption() )) {

			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
			inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());


		} else if ( RequestOption.refreshMilestone.equals( paramBean.getParam().getRequestOption() )) {
			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
			inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}
	}

	private void submitChanges(FlowChangePropertyEditServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtModifyServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		// Status Matrix Check
		{
			String pjtId = paramBean.getParam().getSelectedPjtId();
			String lotId = support.findPjtEntity( pjtId ).getLotId();
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit( pjtId, 1, 0, null ).getEntities().toArray(new IAreqEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setPjtIds( pjtId )
			.setAreqIds( support.getAssetApplyNo( applyList ) );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, innerServiceBean);
		// Call modify input service
		{
			innerServiceBean.setReferer(ChaLibScreenID.PJT_MODIFY_INPUT);
			innerServiceBean.setForward(ChaLibScreenID.PJT_MODIFY_CONFIRM);
			inputService.execute(dto);
		}

		// Call pjt modify confirmation service
		{
			innerServiceBean.setReferer(ChaLibScreenID.PJT_MODIFY_CONFIRM);
			innerServiceBean.setForward(ChaLibScreenID.COMP_PJT_MODIFY);
			confirmationService.execute(dto);
		}

		// Call pjt modify complete service
		{
			innerServiceBean.setReferer(ChaLibScreenID.PJT_MODIFY_CONFIRM);
			innerServiceBean.setForward(ChaLibScreenID.COMP_PJT_MODIFY);
			completeService.execute(dto);
		}
		
		// Call pjt modify mail service
		{
			innerServiceBean.setReferer(ChaLibScreenID.PJT_MODIFY_CONFIRM);
			innerServiceBean.setForward(ChaLibScreenID.COMP_PJT_MODIFY);
			mailService.execute(dto);
		}

		this.afterExecution(innerServiceBean, paramBean);
	}

	private void beforeExecution(FlowChangePropertyEditServiceBean src, FlowChaLibPjtModifyServiceBean dest) {
		ChangePropertyEditInputBean srcInfo = src.getParam().getInputInfo();
		PjtDetailInputBean pjtDetailInputBean = dest.getPjtDetailInputBean();
		PjtDetailBean destInfo = null;

		src.getResult().setCompleted(false);
		String lotId = support.findPjtEntity(src.getParam().getSelectedPjtId()).getLotId();

		if (RequestType.init.equals(src.getParam().getRequestType())) {
			if (null == pjtDetailInputBean) {
				pjtDetailInputBean = new PjtDetailInputBean();
			}
			destInfo = new PjtDetailBean();
			dest.setSelectedPjtNo(src.getParam().getSelectedPjtId());
			dest.setSelectedLotNo(lotId);
			destInfo.setPjtNo(src.getParam().getSelectedPjtId());
			destInfo.setLotNo(lotId);
			pjtDetailInputBean.setPjtDetailBean(destInfo);
		}

		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			destInfo = pjtDetailInputBean.getPjtDetailBean();
			dest.setSelectedPjtNo(src.getParam().getSelectedPjtId());
			dest.setSelectedLotNo(lotId);
			destInfo.setLotNo(lotId);
			destInfo.setPjtNo(src.getParam().getSelectedPjtId());
			destInfo.setChangeCauseClassify(
					DesignSheetFactory.getDesignSheet().getValue( AmDesignBeanId.changeCauseClassifyId, srcInfo.getReferenceCategoryId() ));
			destInfo.setChangeCauseNo(srcInfo.getReferenceId());
			destInfo.setPjtContent(srcInfo.getContents());
			destInfo.setPjtSummary(srcInfo.getSummary());

			if ( TriStringUtils.isEmpty(srcInfo.getAssigneeId()) ) {
				destInfo.setAssigneeId("");
				destInfo.setAssigneeNm("");
			} else {
				destInfo.setAssigneeId(srcInfo.getAssigneeId());
				destInfo.setAssigneeNm(umSupport.findUserByUserId(srcInfo.getAssigneeId()).getUserNm());
			}

			destInfo.setCtgId(srcInfo.getCtgId());
			destInfo.setMstoneId(srcInfo.getMstoneId());
		}
	}

	private void afterExecution(FlowChaLibPjtModifyServiceBean src, FlowChangePropertyEditServiceBean dest) {
		ChangePropertyEditInputBean destInfo = dest.getParam().getInputInfo();
		PjtDetailBean srcInfo = src.getPjtDetailInputBean().getPjtDetailBean();
		String pjtId = dest.getParam().getSelectedPjtId();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( dest.getLanguage(), dest.getTimeZone() );

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {
			IPjtEntity pjtEntity = support.findPjtEntity(pjtId);

			FlowChangePropertyEditServiceBean.ChangePropertyDetailsView destView = dest.getDetailsView();

			destInfo.setReferenceId(srcInfo.getChangeCauseNo());
			destInfo.setReferenceCategoryId(
				DesignSheetFactory.getDesignSheet().getKey( AmDesignBeanId.changeCauseClassifyId, srcInfo.getChangeCauseClassify() ));
			destInfo.setSummary(srcInfo.getPjtSummary());
			destInfo.setContents(srcInfo.getPjtContent());
			destInfo.setAssigneeId(srcInfo.getAssigneeId());
			destInfo.setCtgId(srcInfo.getCtgId());
			destInfo.setMstoneId(srcInfo.getMstoneId());

			destView.setSubmitterNm(dest.getHeader().getLogInUserName())
					.setSubmitterIconPath(this.support.getUmFinderSupport().getIconPath(dest.getHeader().getLogInUserId()))
					.setStsId( pjtEntity.getProcStsId())
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()) )
					.setCreatedTime(TriDateUtils.convertViewDateFormat( pjtEntity.getRegTimestamp(), formatYMD))
					.setUpdTime(TriDateUtils.convertViewDateFormat( pjtEntity.getUpdTimestamp(), formatYMD));

			this.generateDropBox(destInfo, src.getSelectedLotNo());
		}

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003015I);
		}

	}

	private void generateDropBox(ChangePropertyEditInputBean inputInfo, String lotId) {
		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

		inputInfo.setReferenceCategoryViews(FluentList.from(sheet.getKeyList(AmDesignBeanId.changeCauseClassifyId)).map( AmFluentFunctionUtils.toItemLabelsFromReferenceCategoryId ).asList());

		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
		inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		String[] groups = FluentList
			.from(support.findLotDto(lotId, AmTables.AM_LOT_GRP_LNK).getLotGrpLnkEntities())
			.map( AmFluentFunctionUtils.toGroupIdFromGrpUserLnkEntity ).toArray(new String[0]);

		List<IUserEntity> userEntityList = this.umSupport.findUserByGroups( groups );
		inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());
	}

}
