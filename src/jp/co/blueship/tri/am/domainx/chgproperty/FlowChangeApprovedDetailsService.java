package jp.co.blueship.tri.am.domainx.chgproperty;


import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.ChangeApprovedHistoryViewBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.ChangeApprovedRequestView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 *
 * @version V4.00.00
 * @author Sam
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChangeApprovedDetailsService implements IDomain<FlowChangeApprovedDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangeApprovedDetailsServiceBean> execute(
			IServiceDto<FlowChangeApprovedDetailsServiceBean> serviceDto) {

		FlowChangeApprovedDetailsServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * @param paramBean
	 *  Service Bean
	 */
	private void init(FlowChangeApprovedDetailsServiceBean paramBean) {

		String pjtId = paramBean.getParam().getSelectedPjtId();
		IPjtEntity pjtEntity = this.support.findPjtEntity(pjtId);

		AreqCondition condition = new AreqCondition();
		condition.setPjtId(pjtId);
		condition.setStsIds(ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove());
		List<IAreqEntity> areqEntities = this.support.getAreqDao().find( condition.getCondition() );

		List<IPjtAvlEntity> pjtAvlEntities = this.support.findPjtAvlEntities(pjtId);

		setPjtDetails(paramBean,pjtEntity);
		setAreqEntities(paramBean,areqEntities);
		setPjtAvlEntities(paramBean,pjtAvlEntities );
	}

	private void setPjtDetails(FlowChangeApprovedDetailsServiceBean serviceBean,IPjtEntity pjtEntity) {

		ChangePropertyDetailsView view = serviceBean.getDetailsView();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

			view
				.setPjtId		( pjtEntity.getPjtId() )
				.setChgFactorId	( pjtEntity.getChgFactorId())
				.setReferenceId ( pjtEntity.getChgFactorNo() )
				.setReferenceCategoryNm(pjtEntity.getChgFactorId())
				.setSummary		( pjtEntity.getSummary() )
				.setContents	( pjtEntity.getContent() )
				.setStsId		( pjtEntity.getProcStsId() )
				.setStatus		( sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()) )
				.setSubmitterNm ( pjtEntity.getRegUserNm() )
				.setSubmitterIconPath(support.getUmFinderSupport().getIconPath(pjtEntity.getRegUserId()))
				.setAssigneeNm	( pjtEntity.getAssigneeId())
				.setAssigneeIconPath(support.getUmFinderSupport().getIconPath(pjtEntity.getAssigneeId()))
				.setCategoryNm(pjtEntity.getCtgNm())
				.setMstoneNm(pjtEntity.getMstoneNm() )
				.setCreatedTime	( TriDateUtils.convertViewDateFormat(pjtEntity.getRegTimestamp(),formatYMDHM) )
				.setUpdTime		( TriDateUtils.convertViewDateFormat(pjtEntity.getUpdTimestamp(),formatYMDHM) )
				.setCategoryNm( pjtEntity.getCtgNm())
				.setMstoneNm(pjtEntity.getMstoneNm())
				;

		serviceBean.setDetailsView(view);
	}

	private void setAreqEntities(FlowChangeApprovedDetailsServiceBean serviceBean, List<IAreqEntity> areqEntities){


		List<ChangeApprovedRequestView> areqViewList = serviceBean.getApprovedRequestViews();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for(IAreqEntity areqEntity : areqEntities){

			ChangeApprovedRequestView areqCurrentView = serviceBean.new ChangeApprovedRequestView();
				areqCurrentView
						.setAreqType	( AreqCtgCd.value(areqEntity.getAreqCtgCd()) )
						.setAreqId		( areqEntity.getAreqId() )
						.setSubject		( areqEntity.getSummary() )
						.setRequestTime	( TriDateUtils.convertViewDateFormat(areqEntity.getReqTimestamp(),formatYMDHM) )
						.setSubmitterNm	( areqEntity.getRegUserNm() )
						.setSubmitterIconPath( support.getUmFinderSupport().getIconPath(areqEntity.getRegUserId()))
						.setGroupNm		( areqEntity.getGrpNm() )
						.setStsId		( areqEntity.getProcStsId() )
						.setStatus		( sheet.getValue(AmDesignBeanId.statusId, areqEntity.getProcStsId()) );

					areqViewList.add(areqCurrentView);
			}
		serviceBean.setApprovedRequestViews(areqViewList);
	}

	private void setPjtAvlEntities(FlowChangeApprovedDetailsServiceBean serviceBean,List<IPjtAvlEntity> pjtAvlEntities) {

		List<ChangeApprovedHistoryViewBean> approvedHistoryList = serviceBean.getApprovedHistoryViews();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for(IPjtAvlEntity pjtAvlEntity : pjtAvlEntities){

			ChangeApprovedHistoryViewBean approvedHistory = serviceBean.new ChangeApprovedHistoryViewBean()
				.setUpdTime( TriDateUtils.convertViewDateFormat(pjtAvlEntity.getPjtAvlTimestamp(),formatYMD) )
				.setStsId( pjtAvlEntity.getProcStsId() )
				.setStatus( sheet.getValue(AmDesignBeanId.statusId, pjtAvlEntity.getProcStsId()) )
				.setSubmitterNm( pjtAvlEntity.getUpdUserNm() )
				.setSubmitterIconPath( support.getUmFinderSupport().getIconPath(pjtAvlEntity.getUpdUserId()))
				.setComment( pjtAvlEntity.getPjtAvlCmt() )
			;

			approvedHistoryList.add(approvedHistory);
		}

		serviceBean.setApprovedHistoryViews(approvedHistoryList);
	}

}
