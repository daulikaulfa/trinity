package jp.co.blueship.tri.am.domainx.chgproperty.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ChangePropertyEditInputBean {
	private String referenceId;
	private List<ItemLabelsBean> referenceCategoryViews = new ArrayList<ItemLabelsBean>();
	private String referenceCategoryId;
	private String summary;
	private String contents;
	private String assigneeId;
	private List<ItemLabelsBean> assigneeViews = new ArrayList<ItemLabelsBean>();
	private String ctgId;
	private List<ItemLabelsBean> categoryViews = new ArrayList<ItemLabelsBean>();
	private String mstoneId;
	private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();
	private String createdTime;
	private String updTime;
	
	public String getReferenceId() {
		return referenceId;
	}
	public ChangePropertyEditInputBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}
	public List<ItemLabelsBean> getReferenceCategoryViews() {
		return referenceCategoryViews;
	}
	public ChangePropertyEditInputBean setReferenceCategoryViews(List<ItemLabelsBean> referenceCategoryViews) {
		this.referenceCategoryViews = referenceCategoryViews;
		return this;
	}
	public String getReferenceCategoryId() {
		return referenceCategoryId;
	}
	public ChangePropertyEditInputBean setReferenceCategoryId(String referenceCategoryId) {
		this.referenceCategoryId = referenceCategoryId;
		return this;
	}
	public String getContents() {
		return contents;
	}
	public ChangePropertyEditInputBean setContents(String contents) {
		this.contents = contents;
		return this;
	}
	public String getSummary() {
		return summary;
	}
	public ChangePropertyEditInputBean setSummary(String summary) {
		this.summary = summary;
		return this;
	}
	public List<ItemLabelsBean> getCategoryViews() {
		return categoryViews;
	}
	public ChangePropertyEditInputBean setCategoryViews(List<ItemLabelsBean> categoryViews) {
		this.categoryViews = categoryViews;
		return this;
	}
	public List<ItemLabelsBean> getMstoneViews() {
		return mstoneViews;
	}
	public ChangePropertyEditInputBean setMstoneViews(List<ItemLabelsBean> mstoneViews) {
		this.mstoneViews = mstoneViews;
		return this;
	}
	public String getAssigneeId() {
		return assigneeId;
	}
	public ChangePropertyEditInputBean setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
		return this;
	}
	public String getCtgId() {
		return ctgId;
	}
	public ChangePropertyEditInputBean setCtgId(String ctgId) {
		this.ctgId = ctgId;
		return this;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public ChangePropertyEditInputBean setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		return this;
	}
	public List<ItemLabelsBean> getAssigneeViews() {
		return assigneeViews;
	}
	public ChangePropertyEditInputBean setAssigneeViews(List<ItemLabelsBean> assigneeViews) {
		this.assigneeViews = assigneeViews;
		return this;
	}
	
	public String getCreatedTime() {
		return createdTime;
	}
	public ChangePropertyEditInputBean setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
		return this;
	}
	public String getUpdTime() {
		return updTime;
	}
	public ChangePropertyEditInputBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}
}
