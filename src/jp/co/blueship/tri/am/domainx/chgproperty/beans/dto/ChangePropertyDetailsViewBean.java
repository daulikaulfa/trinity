package jp.co.blueship.tri.am.domainx.chgproperty.beans.dto;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ChangePropertyDetailsViewBean {
	private boolean closed = false;
	private String pjtId;
	private String stsId;
	private String status;
	private String referenceId;
	private String referenceCategoryId;
	private String referenceCategoryNm;
	private String summary;
	private String contents;
	private String submitterNm;
	private String submitterIconPath;
	private String assigneeNm;
	private String assigneeIconPath;
	private String categoryNm;
	private String mstoneNm;
	private String createdTime;
	private String updTime;
	private boolean testCompletionEnabled = false;

	public boolean isClosed() {
		return closed;
	}
	public ChangePropertyDetailsViewBean setClosed(boolean closed) {
		this.closed = closed;
		return this;
	}
	public String getPjtId() {
		return pjtId;
	}
	public ChangePropertyDetailsViewBean setPjtId(String pjtId) {
		this.pjtId = pjtId;
		return this;
	}
	public String getStsId() {
		return stsId;
	}
	public ChangePropertyDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}
	public String getStatus() {
		return status;
	}
	public ChangePropertyDetailsViewBean setStatus(String status) {
		this.status = status;
		return this;
	}
	public String getReferenceId() {
		return referenceId;
	}
	public ChangePropertyDetailsViewBean setReferenceId(String referenceId) {
		this.referenceId = referenceId;
		return this;
	}

	public String getReferenceCategoryId() {
		return referenceCategoryId;
	}

	public ChangePropertyDetailsViewBean setReferenceCategoryId(String referenceCategoryId) {
		this.referenceCategoryId = referenceCategoryId;
		return this;
	}

	public String getReferenceCategoryNm() {
		return referenceCategoryNm;
	}
	public ChangePropertyDetailsViewBean setReferenceCategoryNm(String referenceCategoryNm) {
		this.referenceCategoryNm = referenceCategoryNm;
		return this;
	}
	public String getSummary() {
		return summary;
	}
	public ChangePropertyDetailsViewBean setSummary(String summary) {
		this.summary = summary;
		return this;
	}
	public String getContents() {
		return contents;
	}
	public ChangePropertyDetailsViewBean setContents(String contents) {
		this.contents = contents;
		return this;
	}
	public String getSubmitterNm() {
		return submitterNm;
	}
	public ChangePropertyDetailsViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}
	public String getSubmitterIconPath() {
		return submitterIconPath;
	}
	public ChangePropertyDetailsViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public ChangePropertyDetailsViewBean setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
		return this;
	}
	public String getAssigneeIconPath() {
		return assigneeIconPath;
	}
	public ChangePropertyDetailsViewBean setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
		return this;
	}
	public String getCategoryNm() {
		return categoryNm;
	}
	public ChangePropertyDetailsViewBean setCategoryNm(String category) {
		this.categoryNm = category;
		return this;
	}
	public String getMstoneNm() {
		return mstoneNm;
	}
	public ChangePropertyDetailsViewBean setMstoneNm(String mstone) {
		this.mstoneNm = mstone;
		return this;
	}
	public String getCreatedTime() {
		return createdTime;
	}
	public ChangePropertyDetailsViewBean setCreatedTime(String createdTime) {
		this.createdTime = createdTime;
		return this;
	}
	public String getUpdTime() {
		return updTime;
	}
	public ChangePropertyDetailsViewBean setUpdTime(String updTime) {
		this.updTime = updTime;
		return this;
	}
	public boolean isTestCompletionEnabled() {
		return testCompletionEnabled;
	}
	public ChangePropertyDetailsViewBean setTestCompletionEnabled(boolean testCompletionEnabled) {
		this.testCompletionEnabled = testCompletionEnabled;
		return this;
	}
}
