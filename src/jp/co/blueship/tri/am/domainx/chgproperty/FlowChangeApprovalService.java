package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */

public class FlowChangeApprovalService implements IDomain<FlowChangeApprovalServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;

	public void setConfirmationService(
			IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	@Override
	public IServiceDto<FlowChangeApprovalServiceBean> execute(
			IServiceDto<FlowChangeApprovalServiceBean> serviceDto) {
		FlowChangeApprovalServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			String[] areqIds = paramBean.getParam().getInputInfo().getSelectedAreqIds();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqIds), "SelectedAreqIds is not specified");

			if (RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void validate(FlowChangeApprovalServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtApproveServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String[] areqIds = paramBean.getParam().getInputInfo().getSelectedAreqIds();
			AreqDtoList areqDtoList = new AreqDtoList();
			for (String areqNo : areqIds ) {
				areqDtoList.add( this.support.findAreqDto( areqNo, AmTables.AM_AREQ ) );
			}
			IPjtEntity[] pjtEntities	= this.support.getPjtEntity( areqDtoList );
			String lotId				= pjtEntities[0].getLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setPjtIds( this.support.getPjtNo( pjtEntities ) )
			.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		// Call Pjt Approval Confirmation to validate input information
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_DETAIL_VIEW);
			serviceBean.setForward(ChaLibScreenID.PJT_APPROVE_CONFIRM);
			confirmationService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void submitChanges(FlowChangeApprovalServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtApproveServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String[] areqIds = paramBean.getParam().getInputInfo().getSelectedAreqIds();
			AreqDtoList areqDtoList = new AreqDtoList();
			for (String areqNo : areqIds ) {
				areqDtoList.add( this.support.findAreqDto( areqNo, AmTables.AM_AREQ ) );
			}
			IPjtEntity[] pjtEntities	= this.support.getPjtEntity( areqDtoList );
			String lotId				= pjtEntities[0].getLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setPjtIds( this.support.getPjtNo( pjtEntities ) )
			.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		// Call Pjt Approval Confirmation to validate input information
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_DETAIL_VIEW);
			serviceBean.setForward(ChaLibScreenID.PJT_APPROVE_CONFIRM);
			confirmationService.execute(dto);
		}

		// Call Pjt Approval Confirmation Service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE);
			confirmationService.execute(dto);
		}

		// Call Pjt Approval Complete Service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE);
			completeService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowChangeApprovalServiceBean src, FlowChaLibPjtApproveServiceBean dest) {
		String[] selectedAreqIds = src.getParam().getInputInfo().getSelectedAreqIds();
		String[] selectedPjtIds = this.support.getPjtIdsFromAreqIds(selectedAreqIds);
		dest.setSelectedPjtNo(selectedPjtIds);

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {

			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
			String serverId = srvEntity.getBldSrvId();

			PjtEditInputBean pjtInputBean = dest.getPjtEditInputBean();
			pjtInputBean.setApproveComment(src.getParam().getInputInfo().getComment());

			IPjtEntity pjtEntity = this.support.findPjtEntity(selectedPjtIds[0]);

			dest.setProcId(src.getProcId());

			dest.setLockServerId(serverId);
			dest.setSelectedLotNo(pjtEntity.getLotId());
			dest.setTargetApplyNo(selectedAreqIds);
		}
	}

	private void afterExecution(FlowChaLibPjtApproveServiceBean src, FlowChangeApprovalServiceBean dest) {

		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003007I);
		}
	}
}
