package jp.co.blueship.tri.am.domainx.chgproperty.beans.dto;

import jp.co.blueship.tri.fw.constants.FileStatus;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class ChangeApprovedRequestDetailsViewBean implements IChangeApprovedRequestDetails {
	private boolean directory;
	private String path;
	private String name;
	private String size;
	private int count = 0;
	private String stsId;

	@Override
	public boolean isFile() {
		return !directory;
	}
	@Override
	public boolean isDirectory() {
		return directory;
	}
	public ChangeApprovedRequestDetailsViewBean setDirectory(boolean directory) {
		this.directory = directory;
		return this;
	}
	@Override
	public String getPath() {
		return this.path;
	}
	public ChangeApprovedRequestDetailsViewBean setPath(String path) {
		this.path = path;
		return this;
	}
	@Override
	public String getName() {
		return this.name;
	}
	public ChangeApprovedRequestDetailsViewBean setName(String name) {
		this.name = name;
		return this;
	}
	@Override
	public String getSize() {
		return this.size;
	}
	public ChangeApprovedRequestDetailsViewBean setSize(String size) {
		this.size = size;
		return this;
	}
	@Override
	public int getCount() {
		return count;
	}
	
	public ChangeApprovedRequestDetailsViewBean setCount(int count) {
		this.count = count;
		return this;
	}
	
	@Override
	public String getStsId() {
		return stsId;
	}
	
	public ChangeApprovedRequestDetailsViewBean setStsId(String stsId) {
		this.stsId = stsId;
		return this;
	}
	
	@Override
	public boolean isDiff() {
		if( FileStatus.Edit.equals( this.stsId ) ){
			return true;
		}
		return false;
	}
}
