package jp.co.blueship.tri.am.domainx.chgproperty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.SearchedViewsUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 * Provide the following backend services.
 * <br> - List
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowChangePropertyListService implements IDomain<FlowChangePropertyListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeActionByTestComplete = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setStatusIfNotMergeMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeAction = action;
	}

	public void setStatusIfNotMergeMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeActionByTestComplete = action;
	}

	@Override
	public IServiceDto<FlowChangePropertyListServiceBean> execute(
			IServiceDto<FlowChangePropertyListServiceBean> serviceDto) {

		FlowChangePropertyListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String lotId = paramBean.getParam().getSelectedLotId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005106S, e);
		}
	}

	/**
	 * Called when initializing by service.
	 *
	 * @param serviceBean
	 */
	private void init( FlowChangePropertyListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();

		this.generateDropBox( lotId, searchCondition );
		this.onChange( serviceBean );
	}

	/**
	 * @param lotId Target lot-ID
	 * @param searchCondition Set the generated search conditions
	 */
	private void generateDropBox( String lotId, SearchCondition searchCondition ) {
		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCategoryViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		List<IStatusId> statuses = new ArrayList<IStatusId>();
		statuses.add( AmPjtStatusId.ChangePropertyInProgress );
		statuses.add( AmPjtStatusId.ChangePropertyTestCompleted );
		statuses.add( AmPjtStatusId.ChangePropertyClosed );
		statuses.add( AmPjtStatusIdForExecData.Merging );
		statuses.add( AmPjtStatusId.Merged );
		statuses.add( AmPjtStatusIdForExecData.MergeError );
		searchCondition.setStatusViews(FluentList.from( statuses ).map( AmFluentFunctionUtils.toItemLabelsFromStatusId).asList());

	}

	/**
	 * Called when request by service.
	 *
	 * @param serviceBean
	 */
	private void onChange( FlowChangePropertyListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		SearchCondition searchParam = serviceBean.getParam().getSearchCondition();
		boolean isClosable = AmPjtStatusId.ChangePropertyClosable.getStatusId().equals( searchParam.getStsId() );

		PjtCondition condition = new PjtCondition();
		condition.setLotId(lotId);

		Set<String> cacheIdSet = serviceBean.getParam().getListSelection().getSelectedIdSet();

		if(RequestType.onChange.equals(serviceBean.getParam().getRequestType())){
			cacheIdSet = SearchedViewsUtils.getCaheIdSet(cacheIdSet , serviceBean.getParam().getListSelection().getSelectedIds() , serviceBean.getChangePropertyViews());
			serviceBean.getParam().getListSelection().setSelectedIdSet(cacheIdSet);
		}

		String statusId = searchParam.getStsId();

		if ( TriStringUtils.isNotEmpty(statusId) ) {
			if ( AmPjtStatusId.ChangePropertyClosable.getStatusId().equals( statusId ) ) {

			} else if ( AmPjtStatusId.ChangePropertyNotClosed.getStatusId().equals( statusId ) ) {
				condition.setProcStsIdsByNotEquals(AmPjtStatusId.ChangePropertyClosed.getStatusId());

			} else {
				condition.setProcStsIds( searchParam.getStsId() );
			}
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId(searchParam.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId(searchParam.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(PjtItems.updTimestamp, TriSortOrder.Desc, 1);

		if (isClosable) {
			List<IPjtEntity> entities = support.getPjtDao().find(condition.getCondition(), sort);
			List<ChangePropertyView> views = getChangePropertyViews(serviceBean, entities, cacheIdSet, isClosable);

			ILimit limit = new PageLimit();

			limit.setMaxRows( views.size() );
			limit.setViewRows( serviceBean.getParam().getLinesPerPage() );
			limit.getPageBar().setValue( searchParam.getSelectedPageNo() );
			views = limit.getLimit(views);
			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit);
			serviceBean.setPage(page).setChangePropertyViews( views );

		} else {
			IEntityLimit<IPjtEntity> limit = support.getPjtDao().find(
					condition.getCondition(),
					sort,
					searchParam.getSelectedPageNo(),
					serviceBean.getParam().getLinesPerPage());
			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
			serviceBean.setPage(page).setChangePropertyViews( getChangePropertyViews(serviceBean, limit.getEntities(),cacheIdSet , isClosable ) );
		}
	}

	/**
	 * @param serviceBean
	 * @param entities
	 * @param cacheIdSet
	 * @param isClosable
	 * @return
	 */
	private List<ChangePropertyView> getChangePropertyViews(
										FlowChangePropertyListServiceBean serviceBean,
										List<IPjtEntity> entities,
										Set<String> cacheIdSet,
										boolean isClosable) {
		List<ChangePropertyView> views = new ArrayList<ChangePropertyView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		ILotEntity lotEntity = this.support.findLotEntity( serviceBean.getParam().getSelectedLotId() );

		for (IPjtEntity entity : entities) {
			ChangePropertyView viewBean = serviceBean.new ChangePropertyView()
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setReferenceCategoryNm( entity.getChgFactorId() )
					.setSummary( entity.getSummary() )
					.setSubmitterNm( entity.getRegUserNm() )
					.setSubmitterIconPath(this.support.getUmFinderSupport().getIconPath(entity.getRegUserId()))
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath(this.support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
					.setCreatedDate( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMD) )
					.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
					.setCloseEnabled( this.isClosable(serviceBean, lotEntity, entity) )
					.setSelected(cacheIdSet.contains(entity.getPjtId()))
			;

			if ( isClosable && ! viewBean.isCloseEnabled() ) {
				continue;
			}
			views.add(viewBean);
		}

		return views;
	}

	/**
	 * Validates whether the change property closable by this status.
	 *
	 * @param serviceBean
	 * @param lotEntity
	 * @param pjtEntity
	 * @return true if and only if the change property closable by this status; false otherwise
	 */
	private boolean isClosable(
			FlowChangePropertyListServiceBean serviceBean,
			ILotEntity lotEntity,
			IPjtEntity pjtEntity ) {

		if ( AmPjtStatusId.ChangePropertyClosed.equals(pjtEntity.getStsId()) ) {
			return false;
		}

		IAreqEntity[] requests = this.support.getAssetApplyEntityLimit(pjtEntity.getPjtId(), 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
				.toArray(new IAreqEntity[0]);

		if ( 0 == requests.length )
			return false;

		StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( serviceBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( pjtEntity.getLotId() )
			.setPjtIds( pjtEntity.getPjtId() )
			.setAreqIds( support.getAssetApplyNo(requests) );

		if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
			if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
				// テスト完了運用を行う
				statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
			} else {
				// テスト完了運用を行わない
				statusDto.setActionList( statusMatrixIfNotMergeAction );
			}
		}

		return StatusMatrixCheckUtils.checkStatusMatrix( statusDto, true );
	}

}
