package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChangePropertyEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ChangePropertyDetailsView detailsView = new ChangePropertyDetailsView();

	{
		this.setInnerService( new FlowChaLibPjtModifyServiceBean() );
	}


	@Override
	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangePropertyEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ChangePropertyDetailsView getDetailsView() {
		return detailsView;
	}

	public FlowChangePropertyEditServiceBean setDetailsView(ChangePropertyDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String pjtId = null;
		private ChangePropertyEditInputBean inputInfo = new ChangePropertyEditInputBean();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedPjtId() {
			return pjtId;
		}

		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public ChangePropertyEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(ChangePropertyEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}

		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	public enum RequestOption {
		none( "" ),
		refreshCategory( "refreshCategory" ),
		refreshMilestone( "refreshMilestone" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * Change Property Information
	 */
	public class ChangePropertyDetailsView {
		private String submitterNm;
		private String submitterIconPath;
		private String stsId;
		private String status;
		private String createdTime;
		private String updTime;

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyDetailsView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		
		public String getStsId() {
			return stsId;
		}
		public ChangePropertyDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangePropertyDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedTime() {
			return createdTime;
		}
		public ChangePropertyDetailsView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}

		public String getUpdTime() {
			return updTime;
		}
		public ChangePropertyDetailsView setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
