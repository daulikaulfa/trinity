package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyOverviewServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyOverviewServiceBean.ChangePropertyOverview;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChangePropertyOverviewService implements IDomain<FlowChangePropertyOverviewServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangePropertyOverviewServiceBean> execute(
			IServiceDto<FlowChangePropertyOverviewServiceBean> serviceDto) {

		FlowChangePropertyOverviewServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String pjtId = serviceBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType()) ) {

				IPjtEntity pjtEntity = this.support.findPjtEntity(pjtId);
				ILotDto lotDto = this.support.findLotDto(pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

				ChangePropertyOverview changePropertyDetailsViewBean =
						getChangePropertyOverView(serviceBean, pjtEntity);

				serviceBean.setDetailsView(changePropertyDetailsViewBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, serviceBean.getFlowAction());
		}
	}


	private ChangePropertyOverview getChangePropertyOverView (
			FlowChangePropertyOverviewServiceBean serviceBean,
			IPjtEntity pjtEntity) {

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();


		ChangePropertyOverview view = serviceBean.getDetailsView();
		view.setReferenceId			( pjtEntity.getChgFactorNo() );
		view.setStsId				( pjtEntity.getProcStsId() );
		view.setStatus				( sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()) );
		view.setReferenceCategoryNm	( pjtEntity.getChgFactorId() );
		view.setSummary				( pjtEntity.getSummary() );
		view.setSubmitterNm			( pjtEntity.getRegUserNm() );
		view.setAssigneeNm			( pjtEntity.getAssigneeNm() );

		view.setCreatedTime(
				TriDateUtils.convertViewDateFormat(
						pjtEntity.getRegTimestamp(),
						TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
						).toString() );

		return view;
	}
}
