package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowChangeApprovedDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ChangePropertyDetailsView detailsView = new ChangePropertyDetailsView();
	private List<ChangeApprovedRequestView> approvedRequestViews = new ArrayList<ChangeApprovedRequestView>();
	private List<ChangeApprovedHistoryViewBean> approvedHistoryViews = new ArrayList<ChangeApprovedHistoryViewBean>();

	public RequestParam getParam() {
		return param;
	}

	public ChangePropertyDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowChangeApprovedDetailsServiceBean setDetailsView(ChangePropertyDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public List<ChangeApprovedRequestView> getApprovedRequestViews() {
		return approvedRequestViews;
	}
	public FlowChangeApprovedDetailsServiceBean setApprovedRequestViews(List<ChangeApprovedRequestView> approvedRequestViews) {
		this.approvedRequestViews = approvedRequestViews;
		return this;
	}

	public List<ChangeApprovedHistoryViewBean> getApprovedHistoryViews() {
		return approvedHistoryViews;
	}
	public FlowChangeApprovedDetailsServiceBean setApprovedHistoryViews(List<ChangeApprovedHistoryViewBean> approvedHistoryViews) {
		this.approvedHistoryViews = approvedHistoryViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private String pjtId = null;

		public String getSelectedPjtId() {
			return pjtId;
		}
		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

	}

	public class ChangePropertyDetailsView {

		private String pjtId;
		private String stsId;
		private String status;
		private String referenceId;
		private String referenceCategoryNm;
		private String changeFactorId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String createdTime;
		private String updTime;
		private String summary;
		private String contents;
		private String categoryNm;
		private String mstoneNm;

		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyDetailsView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public ChangePropertyDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public ChangePropertyDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyDetailsView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}
		public String getChgFactorId() {
			return changeFactorId;
		}
		public ChangePropertyDetailsView setChgFactorId(String changeFactorId) {
			this.changeFactorId = changeFactorId;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyDetailsView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		public String getAssigneeNm() {
			return assigneeNm;
		}
		public ChangePropertyDetailsView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}
		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public ChangePropertyDetailsView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}
		public String getCreatedTime() {
			return createdTime;
		}
		public ChangePropertyDetailsView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}
		public String getUpdTime() {
			return updTime;
		}
		public ChangePropertyDetailsView setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
		public String getSummary() {
			return summary;
		}
		public ChangePropertyDetailsView setSummary(String summary) {
			this.summary = summary;
			return this;
		}
		public String getContents() {
			return contents;
		}
		public ChangePropertyDetailsView setContents(String contents) {
			this.contents = contents;
			return this;
		}
		public String getCategoryNm() {
			return categoryNm;
		}
		public ChangePropertyDetailsView setCategoryNm(String category) {
			this.categoryNm = category;
			return this;
		}
		public String getMstoneNm() {
			return mstoneNm;
		}
		public ChangePropertyDetailsView setMstoneNm(String mstone) {
			this.mstoneNm = mstone;
			return this;
		}
		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public ChangePropertyDetailsView setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}
		
	}

	public class ChangeApprovedRequestView {
		private AreqCtgCd areqType;
		private String areqId;
		private String subject;
		private String requestTime;
		private String submitterNm;
		private String submitterIconPath;
		private String groupNm;
		private String stsId;
		private String status;

		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public ChangeApprovedRequestView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public String getAreqId() {
			return areqId;
		}
		public ChangeApprovedRequestView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public ChangeApprovedRequestView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getRequestTime() {
			return requestTime;
		}
		public ChangeApprovedRequestView setRequestTime(String requestTime) {
			this.requestTime = requestTime;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangeApprovedRequestView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangeApprovedRequestView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		
		public String getGroupNm() {
			return groupNm;
		}
		public ChangeApprovedRequestView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ChangeApprovedRequestView setStsId(String statusId) {
			this.stsId = statusId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangeApprovedRequestView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	public class ChangeApprovedHistoryViewBean {

		private String updTime;
		private String stsId;
		private String status;
		private String submitterNm;
		private String submitterIconPath;
		private String comment;

		public String getUpdTime() {
			return updTime;
		}
		public ChangeApprovedHistoryViewBean setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ChangeApprovedHistoryViewBean setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangeApprovedHistoryViewBean setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangeApprovedHistoryViewBean setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangeApprovedHistoryViewBean setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		
		public String getComment() {
			return comment;
		}
		public ChangeApprovedHistoryViewBean setComment(String comment) {
			this.comment = comment;
			return this;
		}

	}

}
