package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyDetailsViewBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowChangePropertyDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ChangePropertyDetailsViewBean detailsView = new ChangePropertyDetailsViewBean();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public ChangePropertyDetailsViewBean getChangePropertyDetailsView() {
		return detailsView;
	}
	public FlowChangePropertyDetailsServiceBean setChangePropertyDetailsView(ChangePropertyDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String pjtId = null;

		public String getSelectedPjtId() {
			return pjtId;
		}

		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
	}
}
