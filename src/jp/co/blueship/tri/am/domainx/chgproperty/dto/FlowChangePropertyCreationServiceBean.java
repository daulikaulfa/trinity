package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ChangePropertyView detailsView = new ChangePropertyView();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibPjtEntryServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public ChangePropertyView getDetailsView() {
		return detailsView;
	}

	public FlowChangePropertyCreationServiceBean setDetailsView(ChangePropertyView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangePropertyCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ChangePropertyEditInputBean inputInfo = new ChangePropertyEditInputBean();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ChangePropertyEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(ChangePropertyEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	public enum RequestOption {
		none( "" ),
		refreshCategory( "refreshCategory" ),
		refreshMilestone( "refreshMilestone" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	/**
	 * Change Property Information
	 */
	public class ChangePropertyView {
		private String submitterNm;
		private String submitterIconPath;

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
		
		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String pjtId;
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

		public String getPjtId() {
			return pjtId;
		}

		public RequestsCompletion setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
	}

}
