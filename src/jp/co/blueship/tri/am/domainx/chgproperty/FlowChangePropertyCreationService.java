package jp.co.blueship.tri.am.domainx.chgproperty;

import java.util.List;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCreationServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCreationServiceBean.RequestOption;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * Provide the following backend services.
 * <br> - Create
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyCreationService implements IDomain<FlowChangePropertyCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support;
	private IUmFinderSupport umSupport;

	private IDomain<IGeneralServiceBean> inputService = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	public void setInputService(IDomain<IGeneralServiceBean> inputService) {
		this.inputService = inputService;
	}

	public void setConfirmationService(
			IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowChangePropertyCreationServiceBean> execute(
			IServiceDto<FlowChangePropertyCreationServiceBean> serviceDto) {

		FlowChangePropertyCreationServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibPjtEntryServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals( paramBean.getParam().getRequestType() )) {
				this.onChange( paramBean, lotId );
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
		return serviceDto;
	}

	private void init(FlowChangePropertyCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);
		serviceBean.setReferer(ChaLibScreenID.LIB_TOP);

		// Call entry input service
		{
			serviceBean.setForward(ChaLibScreenID.PJT_ENTRY_INPUT);
			inputService.execute(dto);
		}
		this.afterExecution(serviceBean, paramBean);
	}

	private void onChange( FlowChangePropertyCreationServiceBean paramBean, String lotId) {

		ChangePropertyEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		if ( RequestOption.refreshCategory.equals( paramBean.getParam().getRequestOption() )) {

			List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
			inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());


		} else if ( RequestOption.refreshMilestone.equals( paramBean.getParam().getRequestOption() )) {
			List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
			inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
		}
	}

	private void submitChanges(FlowChangePropertyCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, serviceBean);

		// Call entry input service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_ENTRY_INPUT);
			serviceBean.setForward(ChaLibScreenID.PJT_ENTRY_CONFIRM);
			inputService.execute(dto);
		}

		// Call pjt modify confirmation service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_ENTRY);
			confirmationService.execute(dto);
		}

		// Call pjt modify complete service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_ENTRY);
			completeService.execute(dto);
		}
		
		// Call pjt mail service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_ENTRY_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_ENTRY);
			mailService.execute(dto);
		}		

		this.afterExecution(serviceBean, paramBean);
	}

	private void beforeExecution(FlowChangePropertyCreationServiceBean src, FlowChaLibPjtEntryServiceBean dest) {
		ChangePropertyEditInputBean srcInfo = src.getParam().getInputInfo();
		PjtDetailInputBean pjtDetailInputBean = dest.getPjtDetailInputBean();
		PjtDetailBean destInfo = null;

		src.getResult().setCompleted(false);

		if (RequestType.init.equals(src.getParam().getRequestType())) {

			if (null == pjtDetailInputBean) {
				pjtDetailInputBean = new PjtDetailInputBean();
			}
			destInfo = new PjtDetailBean();
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
			destInfo.setLotNo(src.getParam().getSelectedLotId());

			pjtDetailInputBean.setPjtDetailBean(destInfo);

		}

		if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			destInfo = pjtDetailInputBean.getPjtDetailBean();
			dest.setSelectedLotNo(src.getParam().getSelectedLotId());
			destInfo.setLotNo(src.getParam().getSelectedLotId());
			destInfo.setChangeCauseClassify(
					DesignSheetFactory.getDesignSheet().getValue( AmDesignBeanId.changeCauseClassifyId, srcInfo.getReferenceCategoryId() ));
			destInfo.setChangeCauseNo(srcInfo.getReferenceId());
			destInfo.setPjtContent(srcInfo.getContents());
			destInfo.setPjtSummary(srcInfo.getSummary());

			if ( TriStringUtils.isEmpty(srcInfo.getAssigneeId()) ) {
				destInfo.setAssigneeId("");
				destInfo.setAssigneeNm("");
			} else {
				destInfo.setAssigneeId(srcInfo.getAssigneeId());
				destInfo.setAssigneeNm(umSupport.findUserByUserId(srcInfo.getAssigneeId()).getUserNm());
			}

			destInfo.setCtgId(srcInfo.getCtgId());
			destInfo.setMstoneId(srcInfo.getMstoneId());
		}
	}

	private void afterExecution(FlowChaLibPjtEntryServiceBean src, FlowChangePropertyCreationServiceBean dest) {
		ChangePropertyEditInputBean destInfo = dest.getParam().getInputInfo();
		PjtDetailBean srcInfo = src.getPjtDetailInputBean().getPjtDetailBean();

		if ( RequestType.init.equals(dest.getParam().getRequestType()) ) {
			FlowChangePropertyCreationServiceBean.ChangePropertyView destView = dest.getDetailsView();
			destView.setSubmitterNm(dest.getHeader().getLogInUserName());
			destView.setSubmitterIconPath(umSupport.getIconPath(dest.getHeader().getLogInUserId()));
			this.generateDropBox(destInfo, dest.getParam().getSelectedLotId());
		}

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.getResult().setPjtId(srcInfo.getPjtNo());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003002I);
		}

	}

	private void generateDropBox(ChangePropertyEditInputBean inputInfo, String lotId) {
		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

		inputInfo.setReferenceCategoryViews(FluentList.from(sheet.getKeyList(AmDesignBeanId.changeCauseClassifyId)).map( AmFluentFunctionUtils.toItemLabelsFromReferenceCategoryId ).asList());

		List<ICtgEntity> ctgEntityList = this.umSupport.findCtgByLotId(lotId);
		inputInfo.setCategoryViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntityList = this.umSupport.findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());

		String[] groups = FluentList
			.from(support.findLotDto(lotId, AmTables.AM_LOT_GRP_LNK).getLotGrpLnkEntities())
			.map( AmFluentFunctionUtils.toGroupIdFromGrpUserLnkEntity ).toArray(new String[0]);

		List<IUserEntity> userEntityList = this.umSupport.findUserByGroups( groups );
		inputInfo.setAssigneeViews(FluentList.from( userEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());
	}

}
