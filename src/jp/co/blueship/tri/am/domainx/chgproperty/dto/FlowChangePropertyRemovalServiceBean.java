package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCancelServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChangePropertyRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowChaLibPjtCancelServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowChangePropertyRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String pjtId = null;
		private ChangePropertyRemovalInputInfo inputInfo = new ChangePropertyRemovalInputInfo();

		public String getSelectedPjtId() {
			return pjtId;
		}
		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public ChangePropertyRemovalInputInfo getInputInfo(){
			return inputInfo;
		}
		public RequestParam setInputInfo(ChangePropertyRemovalInputInfo inputInfo){
			this.inputInfo = inputInfo;
			return this;

		}
	}

	/**
	 * Change Property Information
	 */
	public class ChangePropertyRemovalInputInfo {
		private String comment;

		public String getComment(){
			return comment;
		}
		public ChangePropertyRemovalInputInfo setComment(String comment){

			this.comment = comment;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
