package jp.co.blueship.tri.am.domainx.chgproperty.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author Sam
 */
public class FlowChangeApprovalPendingDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ChangePropertyDetailsView detailsView = new ChangePropertyDetailsView();
	private List<ChangeApprovalPendingRequestView> approvalPendingRequestViews = new ArrayList<ChangeApprovalPendingRequestView>();
	private List<ChangeApprovedRequestView> approvedRequestViews = new ArrayList<ChangeApprovedRequestView>();

	public RequestParam getParam() {
		return param;
	}

	public ChangePropertyDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowChangeApprovalPendingDetailsServiceBean setDetailsView(ChangePropertyDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public List<ChangeApprovalPendingRequestView> getApprovalPendingRequestViews() {
		return approvalPendingRequestViews;
	}
	public FlowChangeApprovalPendingDetailsServiceBean setApprovalPendingRequestViews(List<ChangeApprovalPendingRequestView> approvalPendingRequestViews) {
		this.approvalPendingRequestViews = approvalPendingRequestViews;
		return this;
	}

	public List<ChangeApprovedRequestView> getApprovedRequestViews() {
		return approvedRequestViews;
	}
	public FlowChangeApprovalPendingDetailsServiceBean setApprovedRequestViews(List<ChangeApprovedRequestView> pastApplyAreqViewList) {
		this.approvedRequestViews = pastApplyAreqViewList;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private String pjtId = null;

		public String getSelectedPjtId() {
			return pjtId;
		}
		public RequestParam setSelectedPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

	}

	public class ChangePropertyDetailsView {

		private String pjtId;
		private String stsId;
		private String status;
		private String referenceId;
		private String referenceCategoryNm;
		private String changeFactorId;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String createdTime;
		private String updTime;
		private String summary;
		private String contents;
		private String categoryNm;
		private String mstoneNm;

		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public ChangePropertyDetailsView setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}
		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyDetailsView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public ChangePropertyDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public ChangePropertyDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyDetailsView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}
		public String getChgFactorId() {
			return changeFactorId;
		}
		public ChangePropertyDetailsView setChgFactorId(String changeFactorId) {
			this.changeFactorId = changeFactorId;
			return this;
		}
		public String getSummary() {
			return summary;
		}
		public ChangePropertyDetailsView setSummary(String summary) {
			this.summary = summary;
			return this;
		}
		public String getContents() {
			return contents;
		}
		public ChangePropertyDetailsView setContents(String contents) {
			this.contents = contents;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}
		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyDetailsView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		public String getAssigneeNm() {
			return assigneeNm;
		}
		public ChangePropertyDetailsView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}
		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public ChangePropertyDetailsView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}
		public String getCreatedTime() {
			return createdTime;
		}
		public ChangePropertyDetailsView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}
		public String getUpdTime() {
			return updTime;
		}
		public ChangePropertyDetailsView setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}
		public String getCategoryNm() {
			return categoryNm;
		}
		public ChangePropertyDetailsView setCategoryNm(String category) {
			this.categoryNm = category;
			return this;
		}
		public String getMstoneNm() {
			return mstoneNm;
		}
		public ChangePropertyDetailsView setMstoneNm(String mstone) {
			this.mstoneNm = mstone;
			return this;
		}
	}

	public class AreqView {
		private AreqCtgCd areqType;
		private String areqId;
		private String subject;
		private String requestTime;
		private String submitterNm;
		private String submitterIconPath;
		private String groupNm;
		private String stsId;
		private String status;

		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public AreqView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public String getAreqId() {
			return areqId;
		}
		public AreqView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public AreqView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getRequestTime() {
			return requestTime;
		}
		public AreqView setRequestTime(String requestTime) {
			this.requestTime = requestTime;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public AreqView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public AreqView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}
		
		public String getGroupNm() {
			return groupNm;
		}
		public AreqView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public AreqView setStsId(String statusId) {
			this.stsId = statusId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public AreqView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	public class ChangeApprovalPendingRequestView extends AreqView {
	}

	public class ChangeApprovedRequestView extends AreqView {
	}
}
