package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveRejectServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingReturnToSubmitterServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChangeApprovalPendingReturnToSubmitterService implements IDomain<FlowChangeApprovalPendingReturnToSubmitterServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;
	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setConfirmationService(
			IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowChangeApprovalPendingReturnToSubmitterServiceBean> execute(
			IServiceDto<FlowChangeApprovalPendingReturnToSubmitterServiceBean> serviceDto) {
		FlowChangeApprovalPendingReturnToSubmitterServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String areqId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(areqId), "SelectedAreqId is not specified");

			if (RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}

	private void validate(FlowChangeApprovalPendingReturnToSubmitterServiceBean paramBean) {

		String areqId = paramBean.getParam().getSelectedAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( areqEntity.getLotId() )
					.setAreqIds( areqId )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.validateInputInfo(paramBean);
	}

	private void submitChanges(FlowChangeApprovalPendingReturnToSubmitterServiceBean paramBean) {

		String areqId = paramBean.getParam().getSelectedAreqId();
		IAreqEntity areqEntity = this.support.findAreqEntity(areqId);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( areqEntity.getLotId() )
					.setAreqIds( areqId )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.validateInputInfo(paramBean);

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtApproveRejectServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean, areqEntity);

		// Call Pjt Approval Rejection Confirmation Service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE_REJECT);
			confirmationService.execute(dto);
		}

		// Call Pjt Approval Rejection Complete Service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE_REJECT);
			completeService.execute(dto);
		}

		// Call Pjt Approval Rejection Mail Service
		{
			serviceBean.setReferer(ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM);
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_APPROVE_REJECT);
			mailService.execute(dto);
		}


		this.afterExecution(serviceBean, paramBean);
	}


	private void validateInputInfo(FlowChangeApprovalPendingReturnToSubmitterServiceBean paramBean) {
		String areqId = paramBean.getParam().getSelectedAreqId();

		AreqCondition condition = this.getApprovalRejectAreqCondition();
		condition.setAreqId(areqId);

		IAreqEntity areqEntity = this.support.getAreqDao().findByPrimaryKey(condition.getCondition());

		// Check AM_AREQ is possible to reject
		if (null == areqEntity) {
			throw new TriSystemException(AmMessageId.AM001086E);
		}

		String pjtId = areqEntity.getPjtId();

		IPjtEntity pjtEntity = this.support.findPjtEntity(pjtId);

		// pjt_status should be equal to PJT_PROGRESS
		if ( !AmPjtStatusId.ChangePropertyInProgress.getStatusId().equals(pjtEntity.getStsId())) {
			throw new TriSystemException(AmMessageId.AM001088E);
		}

		// Check Accessible Group
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
	}

	private AreqCondition getApprovalRejectAreqCondition() {

		AreqCondition condition = new AreqCondition();

		String[] baseStatusId = { 	AmAreqStatusId.CheckinRequested.getStatusId() ,
				AmAreqStatusId.RemovalRequested.getStatusId() };

		condition.setStsIds		( baseStatusId );

		String[] statusIdByNotEquals = {
				AmAreqStatusIdForExecData.ApprovingCheckinRequest.getStatusId(),
				AmAreqStatusIdForExecData.ApprovingRemovalRequest.getStatusId() };
		condition.setProcStsIdsByNotEquals		( statusIdByNotEquals );

		return condition;
	}

	private void beforeExecution(FlowChangeApprovalPendingReturnToSubmitterServiceBean src, FlowChaLibPjtApproveRejectServiceBean dest, IAreqEntity areqEntity) {

		dest.setProcId(src.getProcId());
		dest.setSelectedApplyNo(areqEntity.getAreqId());
		dest.getPjtEditInputBean().setApproveRejectComment(src.getParam().getInputInfo().getComment());
		dest.setSelectedPjtNo(areqEntity.getPjtId());
		dest.setSelectedLotNo(areqEntity.getLotId());
	}

	private void afterExecution(FlowChaLibPjtApproveRejectServiceBean src, FlowChangeApprovalPendingReturnToSubmitterServiceBean dest) {
		dest.getResult().setCompleted(true);
		dest.getMessageInfo().addFlashTranslatable(AmMessageId.AM003009I);
	}
}
