package jp.co.blueship.tri.am.domainx.chgproperty.beans.dto;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IResourceViewBean;

/**
 * @version V4.00.00
 * @author Sam
 */
public interface IChangeApprovedRequestDetails extends IResourceViewBean{
	public String getStsId();
	public boolean isDiff();
}
