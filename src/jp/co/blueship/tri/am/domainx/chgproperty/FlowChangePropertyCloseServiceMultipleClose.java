package jp.co.blueship.tri.am.domainx.chgproperty;

import java.util.List;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceMultipleCloseBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceMultipleCloseBean.ChangePropertyCloseInput;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * Provide the following backend services.
 * <br> - Multiple Close
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyCloseServiceMultipleClose implements IDomain<FlowChangePropertyCloseServiceMultipleCloseBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IDomain<IGeneralServiceBean> completeService = null;
	private IAmFinderSupport support =null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeActionByTestComplete = null;

	public void setSupport( IAmFinderSupport support ){
		this.support = support;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}
	public void setStatusIfNotMergeMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeAction = action;
	}
	public void setStatusIfNotMergeMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeActionByTestComplete = action;
	}

	@Override
	public IServiceDto<FlowChangePropertyCloseServiceMultipleCloseBean> execute(
			IServiceDto<FlowChangePropertyCloseServiceMultipleCloseBean> serviceDto) {

		FlowChangePropertyCloseServiceMultipleCloseBean paramBean = serviceDto.getServiceBean();
		FlowChaLibPjtCloseServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String[] pjtIds = paramBean.getParam().getSelectedPjtIds();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtIds), "SelectedPjtId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				paramBean.getResult().setCompleted(true);
			}

			if( RequestType.validate.equals(paramBean.getParam().getRequestType()) ){
				this.validate(paramBean);
			}

            if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
            	this.validate(paramBean);
                this.submitChanges(paramBean);
            }

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}  finally {
            ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

		return serviceDto;
	}

	private void validate( FlowChangePropertyCloseServiceMultipleCloseBean paramBean ){

		String lotId = paramBean.getParam().getSelectedLotId();
		String[] pjtIds = paramBean.getParam().getSelectedPjtIds();

		ILotEntity lotEntity = this.support.findLotEntity( lotId );

		AreqCondition areqCondition = new AreqCondition();
		areqCondition.setPjtIds( pjtIds );

		List<IAreqEntity> areqEntityList = this.support.getAreqDao().find( areqCondition.getCondition() );
		String[] areqIds = FluentList.from( areqEntityList ).map( AmFluentFunctionUtils.toAreqIds ).toArray( new String[0] );

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean	( paramBean )
				.setFinder		( (AmFinderSupport)support )
				.setActionList	( statusMatrixAction )
				.setLotIds		( lotId )
				.setPjtIds		( pjtIds )
				.setAreqIds		( areqIds )
				;

		if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
			if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
				statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );

			} else {
				statusDto.setActionList( statusMatrixIfNotMergeAction );
			}
		}
		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
	}

	private void submitChanges(FlowChangePropertyCloseServiceMultipleCloseBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer(ChaLibScreenID.PJT_CLOSE_CONFIRM);

		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CLOSE);
			completeService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003005I);
	}

	private void beforeExecution(FlowChangePropertyCloseServiceMultipleCloseBean src, FlowChaLibPjtCloseServiceBean dest) {
		ChangePropertyCloseInput srcInfo = src.getParam().getInputInfo();
		PjtEditInputBean destInfo = dest.getPjtEditInputBean();
		src.getResult().setCompleted(false);

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.setSelectedPjtIdArray(src.getParam().getSelectedPjtIds());
			dest.setBatchMode(true);
			destInfo.setCloseComment(srcInfo.getComment());
		}
	}
}
