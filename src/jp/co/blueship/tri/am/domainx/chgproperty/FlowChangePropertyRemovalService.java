package jp.co.blueship.tri.am.domainx.chgproperty;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCancelServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyRemovalServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * Provide the following backend services.
 * <br> - Remove
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChangePropertyRemovalService implements IDomain<FlowChangePropertyRemovalServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setConfirmationService(IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowChangePropertyRemovalServiceBean> execute(
			IServiceDto<FlowChangePropertyRemovalServiceBean> serviceDto) {

		FlowChangePropertyRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibPjtCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedPjtId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
				this.validate(paramBean);
			}
			
			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
		return serviceDto;
	}

	private void validate(FlowChangePropertyRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);
		
		// Status Matrix Check
		{
			String pjtId = paramBean.getParam().getSelectedPjtId();
			IPjtEntity pjtEntity			= this.support.findPjtEntity( pjtId );
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit( pjtId, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCancelConfirm() ).getEntities().toArray(new IAreqEntity[0]);
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( pjtEntity.getLotId() )
			.setPjtIds( pjtId )
			.setAreqIds( support.getAssetApplyNo( applyList ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			serviceBean.setSelectedLotNo(pjtEntity.getLotId());
		}
		
		this.beforeExecution(paramBean, serviceBean);
		
		serviceBean.setReferer(ChaLibScreenID.PJT_DETAIL_VIEW);
		{
			serviceBean.setForward(ChaLibScreenID.PJT_CANCEL_CONFIRM);
			confirmationService.execute(dto);
		}
		
	}
	
	private void submitChanges(FlowChangePropertyRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibPjtCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String pjtId = paramBean.getParam().getSelectedPjtId();
			IPjtEntity pjtEntity			= this.support.findPjtEntity( pjtId );
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit( pjtId, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCancelConfirm() ).getEntities().toArray(new IAreqEntity[0]);
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( pjtEntity.getLotId() )
			.setPjtIds( pjtId )
			.setAreqIds( support.getAssetApplyNo( applyList ) );
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			serviceBean.setSelectedLotNo(pjtEntity.getLotId());
		}
		
	

		this.beforeExecution(paramBean, serviceBean);

		serviceBean.setReferer(ChaLibScreenID.PJT_CANCEL_CONFIRM);
		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CANCEL);
			confirmationService.execute(dto);
		}

		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CANCEL);
			completeService.execute(dto);
		}
		
		{
			serviceBean.setForward(ChaLibScreenID.COMP_PJT_CANCEL);
			mailService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(AmMessageId.AM003014I);
	}

	private void beforeExecution(FlowChangePropertyRemovalServiceBean src, FlowChaLibPjtCancelServiceBean dest) {
		src.getResult().setCompleted(false);
		dest.setSelectedPjtNo(src.getParam().getSelectedPjtId());

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			dest.getPjtEditInputBean().setDelComment(src.getParam().getInputInfo().getComment());
		}
	}
}
