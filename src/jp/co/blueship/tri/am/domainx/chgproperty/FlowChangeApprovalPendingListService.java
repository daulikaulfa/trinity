package jp.co.blueship.tri.am.domainx.chgproperty;


import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.*;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.AreqView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.ListSelection;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.RequestParam;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.SearchedViewsUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowChangeApprovalPendingListService implements IDomain<FlowChangeApprovalPendingListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChangeApprovalPendingListServiceBean> execute(
			IServiceDto<FlowChangeApprovalPendingListServiceBean> serviceDto) {
		FlowChangeApprovalPendingListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified!");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
		return serviceDto;
	}

	/**
	 * Call when initializing by service
	 *
	 * @param serviceBean
	 */
	private void init ( FlowChangeApprovalPendingListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

		ILotDto lotDto = this.support.findLotDto( lotId );
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,
			this.support.getUmFinderSupport().getGrpDao(),
			this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

		this.generateDropBox(lotId, serviceBean);
		this.onChange( serviceBean );
	}

	/**
	 * @param lotId Target lot-ID
	 */
	private void generateDropBox( String lotId, FlowChangeApprovalPendingListServiceBean serviceBean ) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity ).asList());

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity ).asList());
	}

	/**
	 * Call when request by service
	 *
	 * @param serviceBean
	 */
	private void onChange( FlowChangeApprovalPendingListServiceBean serviceBean ) {
		this.getPjtSelection(serviceBean);

		//Get a new change property list
		IEntityLimit<IPjtEntity> limit = getPjtByIncludeApprovableApplyInfo(serviceBean);

		if ( (null != limit) && (null != limit.getEntities()) ) {
			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
			serviceBean.setPage(page);
			serviceBean.setChangePropertyViews( this.getPjtViewBean(serviceBean, limit.getEntities()) );

		} else {
			serviceBean.setPage(new PageNoInfo());
			serviceBean.setChangePropertyViews(new ArrayList<ChangePropertyView>());
		}
	}

	/**
	 * Call when click submit button
	 *
	 * @param serviceBean
	 */
	private void submitChanges( FlowChangeApprovalPendingListServiceBean serviceBean ) {
		this.getPjtSelection(serviceBean);
	}

	/**
	 *
	 * @param serviceBean
	 */
	private void getPjtSelection( FlowChangeApprovalPendingListServiceBean serviceBean ) {
		ListSelection listSelection = serviceBean.getParam().getListSelection();
		String[] selectedIds = listSelection.getSelectedIds();
		Set<String> selectedIdSet = listSelection.getSelectedIdSet();

		//Add selected Id
		for ( String selectedId : selectedIds ) {
			if ( !selectedIdSet.contains(selectedId) ) {
				selectedIdSet.add(selectedId);
			}
		}

		//Remove unselected ID
		for (ChangePropertyView pjtView : serviceBean.getChangePropertyViews()) {
			String id = pjtView.getPjtId();

			if ( selectedIdSet.contains(id) ) {
				boolean isSelected = false;
				for ( String selectedId : selectedIds) {
					if ( TriStringUtils.equals(id, selectedId) ) {
						isSelected = true;
						break;
					}
				}

				if ( !isSelected ) {
					selectedIdSet.remove(id);
				}
			}
		}

		//Add selected Id Set to result
		List<String> selectedIdList = new ArrayList<String>();
		selectedIdList.addAll(selectedIdSet);
		serviceBean.getResult().setSelectedIds(selectedIdList);
	}

	/**
	 *
	 * @param serviceBean
	 */
	private IEntityLimit<IPjtEntity> getPjtByIncludeApprovableApplyInfo (
										FlowChangeApprovalPendingListServiceBean serviceBean) {
		String lotId = serviceBean.getParam().getSelectedLotId();

		IJdbcCondition assetApplyCondition = AmDBSearchConditionAddonUtils.getApprovableAreqCondition(lotId);

		List<IAreqEntity> areqEntities = this.support.getAreqDao().find(assetApplyCondition.getCondition());
		if (areqEntities.size() == 0) {
			serviceBean.setInfoMessage(AmMessageId.AM001086E);
			return new EntityLimit<IPjtEntity>();
		}

		List<IPjtAvlAreqLnkEntity> pjtNoArray = this.support.getPjtNo( this.support.findAreqDtoList(areqEntities) );
		Set<String> pjtIdSet = new TreeSet<String>();
		for (IPjtAvlAreqLnkEntity pjtNo : pjtNoArray) {
			pjtIdSet.add(pjtNo.getPjtId());
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(PjtItems.pjtId, TriSortOrder.Desc, 1);
		int linesPerPage = serviceBean.getParam().getLinesPerPage();
		int pageNo = serviceBean.getParam().getSearchCondition().getSelectedPageNo();

		if ( pjtNoArray.size() > 0 ) {
			IEntityLimit<IPjtEntity> pjtEntityLimit = this.support.getPjtDao().find(
					this.getCondition(serviceBean, pjtIdSet).getCondition(),
					sort,
					pageNo,
					linesPerPage);

			if (pjtEntityLimit.getEntities().size() == 0) {
				serviceBean.setInfoMessage(AmMessageId.AM001088E);
				return new EntityLimit<IPjtEntity>();
			}

			return pjtEntityLimit;

		} else {
			return new EntityLimit<IPjtEntity>();

		}
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private PjtCondition getCondition(
			FlowChangeApprovalPendingListServiceBean serviceBean,
			Set<String> pjtIdSet) {

		RequestParam param = serviceBean.getParam();
		SearchCondition searchParam = param.getSearchCondition();
		PjtCondition condition = new PjtCondition();

		condition.setStsIds( ExtractStatusAddonUtils.getPjtBaseStatusByPjtProgress() );
		condition.setPjtIds(pjtIdSet.toArray(new String[0]));

		if (  TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId(searchParam.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId(searchParam.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(param.getSearchCondition().getKeyword()) ) {
			condition.setContainsByKeyword(param.getSearchCondition().getKeyword());
		}

		return condition;
	}

	/**
	 * @param serviceBean Service Bean
	 * @param pjtList Pending Change Property Approval List
	 */
	private List<ChangePropertyView> getPjtViewBean (
							FlowChangeApprovalPendingListServiceBean serviceBean,
							List<IPjtEntity> pjtList) {
		List<ChangePropertyView> viewBeanList = new ArrayList<ChangePropertyView>();

		Set<String> cacheIdSet = serviceBean.getParam().getListSelection().getSelectedIdSet();

		if(RequestType.onChange.equals(serviceBean.getParam().getRequestType())){
			cacheIdSet = SearchedViewsUtils.getCaheIdSet(cacheIdSet , serviceBean.getParam().getListSelection().getSelectedIds() , serviceBean.getChangePropertyViews());
			serviceBean.getParam().getListSelection().setSelectedIdSet(cacheIdSet);
		}

		for (IPjtEntity entity : pjtList) {
			List<AreqView> areqViewList = this.getAreqViewBean(serviceBean, entity.getPjtId());

			ChangePropertyView view = serviceBean.new ChangePropertyView()
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setSelected(cacheIdSet.contains(entity.getPjtId()))
					.setAreqViews(areqViewList)
			;

			if (serviceBean.getParam().getListSelection().getSelectedIdSet().contains(entity.getPjtId())) {
				view.setSelected(true);
			}

			viewBeanList.add(view);
		}
		return viewBeanList;
	}

	/**
	 * @param serviceBean Service Bean
	 * @param pjtId Change Property Id
	 */
	private List<AreqView> getAreqViewBean(
							FlowChangeApprovalPendingListServiceBean serviceBean,
							String pjtId) {
		AreqCondition condition = (AreqCondition) AmDBSearchConditionAddonUtils.getAreqCondition(pjtId);
		condition.setStsIds( AmAreqStatusId.CheckinRequested.getStatusId(), AmAreqStatusId.RemovalRequested.getStatusId() );

		ISqlSort sort = new SortBuilder();
		IEntityLimit<IAreqEntity> entityLimit = this.support.getAreqDao().find(
				condition.getCondition(),
				sort,
				1,
				0);
		AreqDtoList entities = this.support.findAreqDtoList( entityLimit.getEntities() );
		Collections.sort(entities, new PendingApprovalRequestComparator());

		List<AreqView> viewBeanList = new ArrayList<AreqView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IAreqDto entityDto : entities) {
			IAreqEntity entity = entityDto.getAreqEntity();

			AreqView view = serviceBean.new AreqView()
					.setAreqType(AreqCtgCd.value(entity.getAreqCtgCd()))
					.setAreqId( entity.getAreqId() )
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath( support.getUmFinderSupport().getIconPath(entity.getAssigneeId()))
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) )
					.setSubmitterNm( entity.getRegUserNm() )
					.setSubmitterIconPath( support.getUmFinderSupport().getIconPath( entity.getRegUserId() ))
					.setRequestDate( TriDateUtils.convertViewDateFormat(entity.getReqTimestamp(), formatYMD) )
			;

			viewBeanList.add(view);
		}

		return viewBeanList;
	}

	/**
	 * Order By
	 */
	private class PendingApprovalRequestComparator implements Comparator<IAreqDto> {

		public int compare(IAreqDto entity1, IAreqDto entity2) {

			String status1 = entity1.getAreqEntity().getStsId();
			String status2 = entity2.getAreqEntity().getStsId();

			if (AmAreqStatusId.CheckinRequested.equals(status1) && AmAreqStatusId.CheckinRequested.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			}

			if (AmAreqStatusId.RemovalRequested.equals(status1) && AmAreqStatusId.RemovalRequested.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			}

			if (AmAreqStatusId.RemovalRequested.equals(status1))
				return -1;

			if (AmAreqStatusId.RemovalRequested.equals(status2))
				return 1;

			if (AmAreqStatusId.CheckinRequested.equals(status1))
				return -1;

			if (AmAreqStatusId.CheckinRequested.equals(status2))
				return 1;

			if (status1.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			} else {
				return status1.compareTo(status2);
			}
		}

		/**
		 * 申請情報を申請日でソートする
		 *
		 * @param entity1
		 * @param entity2
		 * @return
		 */
		private int compareApplyDate(IAreqDto entity1, IAreqDto entity2) {

			Timestamp applyDate1 = getApplyDate(entity1);
			Timestamp applyDate2 = getApplyDate(entity2);

			if (applyDate1.equals(applyDate2)) {
				return compareApplyNo(entity1, entity2);
			} else {
				return applyDate1.compareTo(applyDate2);
			}
		}

		/**
		 * 申請日を取得する。
		 *
		 * @param entity IAreqEntityエンティティ
		 * @return 申請日
		 */
		private Timestamp getApplyDate(IAreqDto entity) {

			Timestamp applyDate = null;

			if (AmBusinessJudgUtils.isLendApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getLendReqTimestamp();
			} else if (AmBusinessJudgUtils.isReturnApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getRtnReqTimestamp();
			} else if (AmBusinessJudgUtils.isDeleteApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getDelReqTimestamp();
			}

			return applyDate;
		}

		/**
		 * 申請情報を申請番号でソートする
		 *
		 * @param entity1
		 * @param entity2
		 * @return
		 */
		private int compareApplyNo(IAreqDto entity1, IAreqDto entity2) {

			String applyNo1 = entity1.getAreqEntity().getAreqId();
			String applyNo2 = entity2.getAreqEntity().getAreqId();

			return applyNo1.compareTo(applyNo2);
		}
	}

}
