package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckBaselineListServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public class FlowConflictCheckServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<BaselineView> baselineViews = new ArrayList<BaselineView>();

	private ConflictCheckConfirmView confirmView = new ConflictCheckConfirmView();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerViewService(new FlowChaLibConflictCheckBaselineListServiceBean());
		this.setInnerService(new FlowChaLibConflictCheckServiceBean());
	}
	public RequestParam getParam() {
		return param;
	}

	public List<BaselineView> getBaselineViews() {
		return baselineViews;
	}
	public FlowConflictCheckServiceBean setBaselineViews(List<BaselineView> baselineViews) {
		this.baselineViews = baselineViews;
		return this;
	}

	public ConflictCheckConfirmView getConfirmView() {
		return confirmView;
	}
	public FlowConflictCheckServiceBean setConfirmView(ConflictCheckConfirmView confirmView) {
		this.confirmView = confirmView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowConflictCheckServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ConflictCheckInputInfo inputInfo = new ConflictCheckInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ConflictCheckInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ConflictCheckInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	public class ConflictCheckInputInfo {
		private String lotVerTag;
		private String comment;
		private boolean submitAgreement;

		public String getSelectedLotVerTag() {
			return lotVerTag;
		}

		public ConflictCheckInputInfo setSelectedLotVerTag( String lotVerTag ) {
			this.lotVerTag = lotVerTag;
			return this;
		}
		public String getComment() {
			return comment;
		}
		public ConflictCheckInputInfo setComment(String comment) {
			this.comment = comment;
			return this;
		}

		public boolean isSubmitAgreement() {
			return submitAgreement;
		}

		public ConflictCheckInputInfo setSubmitAgreement(boolean submitAgreement) {
			this.submitAgreement = submitAgreement;
			return this;
		}

 	}

	public class BaselineView {
		private String lotBlTag;
		private String lotVerTag;
		private List<String> bpIds = new ArrayList<String>();
		private List<String> rpIds = new ArrayList<String>();
		private String lotCheckinSubmitterNm;
		private String lotCheckinSubmitterIconPath;
		private String lotCheckinTime;

		public String getLotBlTag() {
			return lotBlTag;
		}
		public BaselineView setLotBlTag(String lotBlTag) {
			this.lotBlTag = lotBlTag;
			return this;
		}
		public List<String> getBpIds() {
			return bpIds;
		}
		public BaselineView setBpIds(List<String> bpIds) {
			this.bpIds = bpIds;
			return this;
		}

		public List<String> getRpIds() {
			return rpIds;
		}
		public BaselineView setRpIds(List<String> rpIds) {
			this.rpIds = rpIds;
			return this;
		}

		public String getLotCheckinSubmitterNm() {
			return lotCheckinSubmitterNm;
		}
		public BaselineView setLotCheckinSubmitterNm(String lotCheckinSubmitterNm) {
			this.lotCheckinSubmitterNm = lotCheckinSubmitterNm;
			return this;
		}

		public String getLotCheckinSubmitterIconPath() {
			return lotCheckinSubmitterIconPath;
		}
		public BaselineView setLotCheckinSubmitterIconPath(String lotCheckinSubmitterIconPath) {
			this.lotCheckinSubmitterIconPath = lotCheckinSubmitterIconPath;
			return this;
		}

		public String getLotCheckinTime() {
			return lotCheckinTime;
		}
		public BaselineView setLotCheckinTime(String lotCheckinTime) {
			this.lotCheckinTime = lotCheckinTime;
			return this;
		}
		/**
		 * @return the lotVerTag
		 */
		public String getLotVerTag() {
			return lotVerTag;
		}
		/**
		 * @param lotVerTag the lotVerTag to set
		 */
		public BaselineView setLotVerTag(String lotVerTag) {
			this.lotVerTag = lotVerTag;
			return this;
		}


	}

	public class ConflictCheckConfirmView {
		private String agreementMessage;
		private List<String> warningMessages = new ArrayList<String>();


		public boolean isWarning() {
			if( TriStringUtils.isNotEmpty( agreementMessage ) ) {
				return true;
			}

			if( 0 != warningMessages.size() ) {
				return true;
			}
			return false;
		}

		public String getAgreementMessage() {
			return agreementMessage;
		}
		public ConflictCheckConfirmView setAgreementMessage( String agreementMessage ) {
			this.agreementMessage = agreementMessage;
			return this;
		}

		public List<String> getWarningMessages() {
			return warningMessages;
		}
		public ConflictCheckConfirmView setWarningMessages( List<String> warningMessages ) {
			this.warningMessages = warningMessages;
			return this;
		}

		public ConflictCheckConfirmView clear() {
			this.agreementMessage = null;
			this.warningMessages.clear();

			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
