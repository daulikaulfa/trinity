package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMergeLotListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private CurrentMergeLotView currentLotView = new CurrentMergeLotView();
	private List<OtherMergeLotView> otherLotViews = new ArrayList<OtherMergeLotView>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public CurrentMergeLotView getCurrentLotView() {
		return currentLotView;
	}
	public FlowMergeLotListServiceBean setCurrentLotView(CurrentMergeLotView currentLotView) {
		this.currentLotView = currentLotView;
		return this;
	}

	public List<OtherMergeLotView> getOtherLotViews() {
		return otherLotViews;
	}
	public FlowMergeLotListServiceBean setOtherLotViews(List<OtherMergeLotView> otherLotViews) {
		this.otherLotViews = otherLotViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowMergeLotListServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 20;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		private Integer selectedPageNo = 1;

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
	}

	abstract public class MergeLotView {
		private String lotId;
		private String lotNm;
		private int noOfMerges = 0;
		private int noOfChangePropertys = 0;
		private String startTime;
		private String endTime;
		private String stsId;
		private String status;
		private Boolean warningOfConflict;
		private Boolean warningOfMerge;

		public String getLotId() {
			return lotId;
		}
		public MergeLotView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotNm() {
			return lotNm;
		}
		public MergeLotView setLotNm(String lotNm) {
			this.lotNm = lotNm;
			return this;
		}

		public int getNoOfMerges() {
			return noOfMerges;
		}
		public MergeLotView setNoOfMerges(int noOfMerges) {
			this.noOfMerges = noOfMerges;
			return this;
		}

		public int getNoOfChangePropertys() {
			return noOfChangePropertys;
		}
		public MergeLotView setNoOfChangePropertys(int noOfChangePropertys) {
			this.noOfChangePropertys = noOfChangePropertys;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public MergeLotView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public MergeLotView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public MergeLotView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public MergeLotView setStatus(String status) {
			this.status = status;
			return this;
		}

		public Boolean getWarningOfConflict() {
			return warningOfConflict;
		}
		public MergeLotView setWarningOfConflict(Boolean warningOfConflict) {
			this.warningOfConflict = warningOfConflict;
			return this;
		}

		public Boolean getWarningOfMerge() {
			return warningOfMerge;
		}
		public MergeLotView setWarningOfMerge(Boolean warningOfMerge) {
			this.warningOfMerge = warningOfMerge;
			return this;
		}
	}

	public class CurrentMergeLotView extends MergeLotView {
		private boolean conflictCheckEnabled;
		private boolean conflictCheckResultEnabled;
		private boolean mergeEnabled;
		private boolean mergeHistoryEnabled;
		private boolean useMerge;
		
		public boolean isConflictCheckEnabled() {
			return conflictCheckEnabled;
		}
		
		public CurrentMergeLotView setConflictCheckEnabled(boolean conflictCheckEnabled) {
			this.conflictCheckEnabled = conflictCheckEnabled;
			return this;
		}
		
		public boolean isConflictCheckResultEnabled() {
			return conflictCheckResultEnabled;
		}
		
		public CurrentMergeLotView setConflictCheckResultEnabled(boolean conflictCheckResultEnabled) {
			this.conflictCheckResultEnabled = conflictCheckResultEnabled;
			return this;
		}
		
		public boolean isMergeEnabled() {
			return mergeEnabled;
		}
		
		public CurrentMergeLotView setMergeEnabled(boolean mergeEnabled) {
			this.mergeEnabled = mergeEnabled;
			return this;
		}
		
		public boolean isMergeHistoryEnabled() {
			return mergeHistoryEnabled;
		}
		
		public CurrentMergeLotView setMergeHistoryEnabled(boolean mergeHistoryEnabled) {
			this.mergeHistoryEnabled = mergeHistoryEnabled;
			return this;
		}

		public boolean isUseMerge() {
			return useMerge;
		}

		public CurrentMergeLotView setUseMerge(boolean useMerge) {
			this.useMerge = useMerge;
			return this;
		}
		
		
	}

	public class OtherMergeLotView extends MergeLotView {

	}
}
