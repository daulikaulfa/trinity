package jp.co.blueship.tri.am.domainx.merge;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean.Module;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean.ChangePropertyDetailsView;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowMergeService implements IDomain<FlowMergeServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	private IDomain<IGeneralServiceBean> mergeConfirm = null;
	private IDomain<IGeneralServiceBean> mergeComplete = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public IDomain<IGeneralServiceBean> getMergeConfirm() {
		return mergeConfirm;
	}

	public void setMergeConfirm(IDomain<IGeneralServiceBean> mergeConfirm) {
		this.mergeConfirm = mergeConfirm;
	}

	public IDomain<IGeneralServiceBean> getMergeComplete() {
		return mergeComplete;
	}

	public void setMergeComplete(IDomain<IGeneralServiceBean> mergeComplete) {
		this.mergeComplete = mergeComplete;
	}

	@Override
	public IServiceDto<FlowMergeServiceBean> execute(IServiceDto<FlowMergeServiceBean> serviceDto) {
		FlowMergeServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibMergeBaselineDetailViewServiceBean detailViewBean = paramBean.getInnerViewService();
		FlowChaLibMergeCommitServiceBean serviceBean = paramBean.getInnerService();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init( paramBean , lotId );
			}

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
				this.validate( paramBean, lotId );
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges( paramBean, lotId );
			}

			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());

		} finally {

			ExceptionUtils.copyToParent(paramBean, serviceBean);
			ExceptionUtils.copyToParent(paramBean, detailViewBean);
		}
	}

	private void init( FlowMergeServiceBean paramBean, String lotId ) {

		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter() ;
		ILotBlEntity lotBlEntity = this.support.getLotBlEntityByMergeEnableAtFirst( lotId );
		if( null == lotBlEntity ) {
			throw new ContinuableBusinessException( AmMessageId.AM001106E  , lotId ) ;
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMergeCommitServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Call confirmation to check status matrix
		{
			serviceBean.setScreenType( ScreenType.next );
			serviceBean.setSelectedLotId( lotId );
			serviceBean.setSelectedLotVersionTag( lotBlEntity.getLotVerTag() );
			serviceBean.setProcId( paramBean.getProcId() );
			serviceBean.setProcMgtStatusId( paramBean.getProcMgtStatusId() );
			serviceBean.setReferer( ChaLibScreenID.COMP_MERGECOMMIT );
			serviceBean.setForward( ChaLibScreenID.MERGECOMMIT_CONFIRM );
			mergeConfirm.execute(dto);
		}
		if( serviceBean.getWarningCheckMergeCommitError().isExistWarning() )
		{
			List<String> mergeWarningMessages =  ca.getMessage(serviceBean);
			paramBean.setMessages( mergeWarningMessages );
		}

		ILotDto lotDto = this.support.findLotDto( lotId, AmTables.AM_LOT_MDL_LNK );

		// Check group accessible
		AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(),
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		// Set Lot detail view Bean
		this.getLotDetailViewBean( paramBean , lotDto, formatYMDHM ) ;

		ILotBlDto lotBlDto = this.support.findLotBlDto( lotBlEntity.getLotBlId(), AmTables.AM_LOT_BL );
		List<ILotBlDto> lotBlEntityArray = this.support.getLotBlDtoByMergeEnable( lotId , lotBlDto.getLotBlEntity().getLotChkinTimestamp() , TriSortOrder.Desc ) ;

		// Conflict Check Overview
		String closedVerTag = lotBlDto.getLotBlEntity().getLotVerTag();
		this.getBaselineView( paramBean, lotDto, closedVerTag, lotId, formatYMDHM );

		// Change property details views
		List<IPjtEntity> pjtEntities = this.support.getPjtEntities(
				lotBlEntityArray,AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView() ) ;
		this.getChangePropertyDetailsView( paramBean, pjtEntities, formatYMDHM );

	}

	private void validate( FlowMergeServiceBean paramBean, String lotId ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMergeCommitServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties( serviceBean, paramBean );
		dto.setServiceBean( serviceBean );

		this.beforeExecution( paramBean, serviceBean, lotId );

		{
			String selectedLotVerTag = serviceBean.getSelectedLotVersionTag();
			ILotBlEntity lotBlEntity = this.support.getLotBlEntityByVersionTag( lotId, selectedLotVerTag, TriSortOrder.Desc ).get(0);
			List<ILotBlDto> lotBlDtoList = this.support.getLotBlDtoByMergeEnable( lotId, lotBlEntity.getLotChkinTimestamp(), TriSortOrder.Desc );

			List<IPjtEntity> pjtEntities = this.support.getPjtEntities(lotBlDtoList, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView());
			String[] pjtIds = FluentList.from( pjtEntities ).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);

			IAreqEntity[] areqEntities = this.support.getAreqEntitiesFromPjtIds( null, pjtIds );
			String[] AreqIds = FluentList.from( areqEntities ).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( lotId )
					.setHeadBls( this.getHeadBlIds(lotId) )
					.setAreqIds( AreqIds )
					.setBpIds( this.getBpIds(lotBlDtoList) )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		// Call Merge Confirmation Service
		{
			serviceBean.setReferer( ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW );
			serviceBean.setForward( ChaLibScreenID.MERGECOMMIT_CONFIRM );
			mergeConfirm.execute( dto );
		}

		this.afterExecution( serviceBean, paramBean, lotId );
	}

	private void submitChanges( FlowMergeServiceBean paramBean, String lotId ) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMergeCommitServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties( serviceBean, paramBean );
		dto.setServiceBean( serviceBean );

		this.beforeExecution( paramBean, serviceBean, lotId );

		// Status Matrix Check
		{
			String selectedLotVerTag = serviceBean.getSelectedLotVersionTag();
			ILotBlEntity lotBlEntity = this.support.getLotBlEntityByVersionTag( lotId, selectedLotVerTag, TriSortOrder.Desc ).get(0);
			List<ILotBlDto> lotBlDtoList = this.support.getLotBlDtoByMergeEnable( lotId, lotBlEntity.getLotChkinTimestamp(), TriSortOrder.Desc );

			List<IPjtEntity> pjtEntities = this.support.getPjtEntities(lotBlDtoList, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView());
			String[] pjtIds = FluentList.from( pjtEntities ).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);

			IAreqEntity[] areqEntities = this.support.getAreqEntitiesFromPjtIds( null, pjtIds );
			String[] AreqIds = FluentList.from( areqEntities ).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( lotId )
					.setHeadBls( this.getHeadBlIds(lotId) )
					.setAreqIds( AreqIds )
					.setBpIds( this.getBpIds(lotBlDtoList) )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		// Call Merge Confirmation Service for validation again
		{
			serviceBean.setReferer( ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW );
			serviceBean.setForward( ChaLibScreenID.MERGECOMMIT_CONFIRM );
			mergeConfirm.execute( dto );
		}

		// Call Merge Confirmation Service
		{
			serviceBean.setReferer( ChaLibScreenID.MERGECOMMIT_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_MERGECOMMIT );
			mergeConfirm.execute( dto );
		}

		// Call Merge Complete Service
		{
			serviceBean.setReferer( ChaLibScreenID.MERGECOMMIT_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_MERGECOMMIT );
			mergeComplete.execute( dto );
		}

		this.afterExecution( serviceBean, paramBean, lotId );
	}

	private void getLotDetailViewBean( FlowMergeServiceBean paramBean, ILotDto lotDto, SimpleDateFormat formatYMDHM ) {
		ILotEntity lotEntity = lotDto.getLotEntity();

		paramBean.getLotDetailsView()
		.setLotId( lotEntity.getLotId() )
		.setStsId( lotEntity.getProcStsId()  )
		.setStatus( sheet.getValue(AmDesignBeanId.statusId, lotEntity.getProcStsId()) )
		.setCreatedTime( TriDateUtils.convertViewDateFormat( lotEntity.getRegTimestamp(), formatYMDHM) )
		.setSubject( lotEntity.getLotNm() )
		.setSummary( lotEntity.getSummary() )
		.setContents( lotEntity.getContent() )
		.setLatestBaselineTag( lotEntity.getLotLatestBlTag() )
		.setLatestVerTag( lotEntity.getLotLatestVerTag() )
		.setBranchBaselineTag( lotEntity.getLotBranchBlTag() )
		.setModules( this.getModuleList( lotDto.getIncludeMdlEntities(true) ) );
	}

	private void getBaselineView( FlowMergeServiceBean paramBean, ILotDto lotDto, String closedVerTag, String lotId, SimpleDateFormat formatYMDHM) {

		try {

			ILotBlEntity lotBlEntity = this.support.getLotBlEntityByVersionTag( lotId, closedVerTag, TriSortOrder.Desc ).get(0);
			IHeadBlEntity headBlEntity = this.support.getHeadBaselineList( lotBlEntity ).get(0);

			boolean mergeEnable = true;

			String workspace = lotDto.getLotEntity().getWsPath() ;
			String mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;
			String mergeReportFilePath = TriStringUtils.linkPathBySlash( mergeTemp , sheet.getValue( AmDesignEntryKeyByMerge.mergeResultFileName ) ) ;
			Object obj = TriFileUtils.readObjectFromFile( new File( mergeReportFilePath ) ) ;
			@SuppressWarnings("unchecked")
			Map<String,MergeResult> mergeResultMap = (Map<String,MergeResult>)obj ;
			Iterator<MergeResult> iter = mergeResultMap.values().iterator() ;
			while( iter.hasNext() ) {
				MergeResult mergeResult = iter.next() ;
				if( MergeStatus.Conflicted.equals( mergeResult.getMergeStatus() ) ) {
					mergeEnable = false;
				}
			}

			paramBean.getBaselineView()
				.setHeadBlId( headBlEntity.getHeadBlId() )
				.setStsId( headBlEntity.getProcStsId() )
				.setStatus( sheet.getValue( AmDesignBeanId.statusId, headBlEntity.getProcStsId() ))
				.setConflictCheckComment( headBlEntity.getMergechkCmt() )
				.setStartTime( TriDateUtils.convertViewDateFormat(headBlEntity.getMergechkStTimestamp(), formatYMDHM) )
				.setEndTime( TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkEndTimestamp(), formatYMDHM ) )
				.setMergeEnabled( mergeEnable );

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005090S, e );
		}
	}

	private void getChangePropertyDetailsView( FlowMergeServiceBean paramBean, List<IPjtEntity> pjtEntities, SimpleDateFormat formatYMDHM ) {
		List<ChangePropertyDetailsView> chgDetailViews = new ArrayList<ChangePropertyDetailsView>();

		for (IPjtEntity entity: pjtEntities ) {
			ChangePropertyDetailsView chgView = paramBean.new ChangePropertyDetailsView()
					.setPjtId( entity.getPjtId() )
					.setReferenceId( entity.getChgFactorNo() )
					.setReferenceCategoryNm( entity.getChgFactorId() )
					.setSummary( entity.getSummary() )
					.setSubmitterNm( entity.getRegUserNm() )
					.setSubmitterIconPath( entity.getRegUserIconPath() )
					.setAssigneeNm( entity.getAssigneeNm() )
					.setAssigneeIconPath( entity.getAssigneeIconPath() )
					.setCreatedTime( TriDateUtils.convertViewDateFormat( entity.getRegTimestamp(), formatYMDHM) )
					.setUpdTime( TriDateUtils.convertViewDateFormat( entity.getUpdTimestamp(), formatYMDHM) )
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(AmDesignBeanId.statusId, entity.getProcStsId()) );

			chgDetailViews.add( chgView);
		}

		paramBean.setChangePropertyViews( chgDetailViews );
	}

	private List<Module> getModuleList( List<ILotMdlLnkEntity> mdlEntities ) {
		List<Module> moduleList = new ArrayList<Module>();
		List<IVcsReposDto> vcsReposDtoList = this.support.findVcsReposDtoFromLotMdlLnkEntities(mdlEntities);

		for ( IVcsReposDto vcsReposDto : vcsReposDtoList ) {
			Module mdl = new LotDetailsViewBean().new Module();
			mdl.setModuleNm( vcsReposDto.getVcsMdlEntity().getMdlNm() );
			mdl.setRepository( vcsReposDto.getVcsReposEntity().getVcsReposUrl() );

			moduleList.add( mdl );
		}

		return moduleList;
	}

	private void beforeExecution( FlowMergeServiceBean src, FlowChaLibMergeCommitServiceBean mergeDest, String lotId) {

		if ( RequestType.validate.equals(src.getParam().getRequestType())
				|| RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {
			ILotBlEntity lotBlEntity = this.support.getLotBlEntityByMergeEnableAtFirst( lotId );

			mergeDest.setScreenType( ScreenType.next );
			mergeDest.setSelectedLotId( lotId );
			mergeDest.setSelectedLotVersionTag( lotBlEntity.getLotVerTag() );
			mergeDest.setProcId( src.getProcId() );
			mergeDest.setProcMgtStatusId( src.getProcMgtStatusId() );

			if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
				MergeEditInputBean inputBean = new MergeEditInputBean();
				inputBean.setMergeComment( src.getParam().getInputInfo().getComment() );
				mergeDest.getWarningCheckMergeCommitError().setConfirmCheck(true);
				mergeDest.setMergeEditInputBean( inputBean );
			}
		}
	}

	private void afterExecution( FlowChaLibMergeCommitServiceBean src, FlowMergeServiceBean dest, String lotId) {

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.setLockByThread( src.isLockByThread() );
			dest.getResult().setCompleted( true );
			dest.getMessageInfo().addFlashTranslatable(UmMessageId.UM003032I);
		}
	}

	private String[] getHeadBlIds(String lotId){
		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( HistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IHeadBlEntity> entityList =  this.support.getHeadBlDao().find(condition.getCondition(), sort);
		List<String> headBlIdList = new ArrayList<String>();

		if( entityList.size() != 0){
			headBlIdList.add( entityList.get(0).getHeadBlId() );
		}

		return headBlIdList.toArray(new String[]{});
	}

	private String[] getBpIds( List<ILotBlDto> lotBlDtoList ){

		List<String> bpIdList = new ArrayList<String>();

		for( ILotBlDto dto : lotBlDtoList ){
			if( dto.getLotBlEntity().getDataCtgCd().equals( BmTables.BM_BP.name() ) ){
				bpIdList.add( dto.getLotBlEntity().getDataId() );
			}
		}

		return bpIdList.toArray(new String[]{});
	}

}
