package jp.co.blueship.tri.am.domainx.merge;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.OtherMergeLotView;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowMergeLotListService implements IDomain<FlowMergeLotListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowMergeLotListServiceBean> execute(IServiceDto<FlowMergeLotListServiceBean> serviceDto) {
		FlowMergeLotListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {

				this.init( paramBean, lotId );

			} else if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {

				this.onChange(paramBean, lotId, false);
			}
			return serviceDto;

		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());

		}
	}

	private void init( FlowMergeLotListServiceBean paramBean, String lotId ) {

		this.onChange(paramBean, lotId, true);
	}

	private void onChange( FlowMergeLotListServiceBean paramBean, String lotId, boolean isInit ) {

		LotCondition condition	= AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		condition.setUseMerge( StatusFlg.on );
		condition.setLotIdsByNotEquals(lotId);
		ISqlSort sort			= AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibMargeLotList();

		List<ILotEntity> lotEntities = this.support.getLotDao().find( condition.getCondition(), sort);

		List<ILotDto> lotDtos = this.support.findLotDto(lotEntities, AmTables.AM_LOT_GRP_LNK);

		List<IGrpEntity> grpEntities = this.support.getUmFinderSupport().findGroupByUserId( paramBean.getUserId() );
		List<String> grpIds = FluentList.from( grpEntities ).map( UmFluentFunctionUtils.toGroupIdFromGrpEntity).asList();

		if (isInit) {

			// Current Lot Views
			ILotEntity currentLotEntity = this.support.findLotEntity( lotId );
			ILotDto currentLotDto = this.support.findLotDto(currentLotEntity, AmTables.AM_LOT_GRP_LNK);
			currentLotDto.setLotEntity( currentLotEntity );

			lotDtos.add( currentLotDto );
		}

		this.getLotView( paramBean, lotDtos, lotId, grpIds );

	}


	private void getLotView( FlowMergeLotListServiceBean paramBean, List<ILotDto> lotDtos, String selectedLotId, List<String> grpIdList) {

		ILimit limit = new PageLimit();
		int size = 0;
		int selectedPageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		selectedPageNo = ( 0 == selectedPageNo )? 1 : selectedPageNo;
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		List<OtherMergeLotView> otherMergeLotViews = new ArrayList<OtherMergeLotView>();

		for (ILotDto lotDto : lotDtos) {

			ILotEntity lotEntity = lotDto.getLotEntity();

			if (TriStringUtils.isNotEmpty( lotDto.getLotGrpLnkEntities() )) {

				boolean enabledLot = false;

				for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {

					if (grpIdList.contains(group.getGrpId())) {
						enabledLot = true;
						break;
					}
				}

				if (!enabledLot && StatusFlg.off == lotEntity.getAllowListView()) {
					continue;
				}
			}

			int numberOfMerges = this.support.countNoOfMerges( lotEntity.getLotId() );
			int numberOfChangeProperties = this.support.countAliveByPjt( lotEntity.getLotId() );
			String mergeStatus = null;
			String mergeStatusId = AmHeadBlStatusId.Unspecified.getStatusId();
			Boolean warningOfConflict = false;
			Boolean warningOfMerge = false;
			String startTime = null;
			String endTime = null;

			boolean conflictCheckEnabled = false;
			boolean conflictCheckResultEnabled = false;
			boolean mergeEnabled = false;
			boolean mergeHistoryEnabled = false;

			List<IHeadBlEntity> headBlEntityList = null;
			{
				HeadBlCondition condition = new HeadBlCondition() ;

				condition.setLotId( lotEntity.getLotId() ) ;

				ISqlSort sort = new SortBuilder();
				sort.setElement(HeadBlItems.regTimestamp, TriSortOrder.Desc, 0);

				headBlEntityList = this.support.getHeadBlDao().find( condition.getCondition(), sort );
			}
			String procStsId = "";

			if ( TriCollectionUtils.isNotEmpty(headBlEntityList) ) {
				IHeadBlEntity headBlEntity = headBlEntityList.get(0);
				procStsId = headBlEntity.getProcStsId();

				mergeStatus = sheet.getValue( AmDesignBeanId.statusId , procStsId );
				mergeStatusId = procStsId;

				if( procStsId.equals( AmHeadBlStatusId.ConflictChecked.getStatusId() )) {

					if ( AmHeadBlMergeStatusCode.LOT_MERGE_CHECK_RESOURCE_WARNING.equals( headBlEntity.getMergeStsCd() ) ){
						warningOfConflict = true;
					}
				}

				if (procStsId.equals( AmHeadBlStatusId.Merged.getStatusId() )) {
					mergeHistoryEnabled = true;

					if ( AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.equals( headBlEntity.getMergeStsCd() ) ) {
						warningOfMerge = true;
					}
				}

				if ( procStsId.equals( AmHeadBlStatusId.Merged.getStatusId())
						|| procStsId.equals( AmHeadBlStatusIdForExecData.Merging.getStatusId() )
						|| procStsId.equals( AmHeadBlStatusIdForExecData.MergeError.getStatusId() )) {

					startTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergeStTimestamp(), formatYMD);
					endTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergeEndTimestamp(), formatYMD);

				} else if ( procStsId.equals( AmHeadBlStatusId.ConflictChecked.getStatusId())
						|| procStsId.equals( AmHeadBlStatusIdForExecData.ConflictChecking.getStatusId() )
						|| procStsId.equals( AmHeadBlStatusIdForExecData.ConflictCheckError.getStatusId() )) {

					startTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkStTimestamp(), formatYMD);
					endTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkEndTimestamp(), formatYMD);
				}

			}

			List<ILotBlEntity> lotBlEntityArray = this.support.getLotBlEntitiesByMergeCheckEnable( lotEntity.getLotId() , null , TriSortOrder.Desc );
			if (TriStringUtils.isNotEmpty(lotBlEntityArray)) {
				conflictCheckEnabled = true;

				if (AmHeadBlStatusId.ConflictChecked.getStatusId().equals(procStsId)) {
					conflictCheckResultEnabled = true;
					mergeEnabled = true;
				}
			}



			if ( selectedLotId.equals( lotEntity.getLotId() ) ) {

				paramBean.getCurrentLotView()
					.setLotId( lotEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setNoOfMerges( numberOfMerges )
					.setNoOfChangePropertys( numberOfChangeProperties )
					.setStartTime( startTime )
					.setEndTime( endTime )
					.setStsId( mergeStatusId )
					.setStatus( mergeStatus )
					.setWarningOfConflict( warningOfConflict )
					.setWarningOfMerge(warningOfMerge );

				paramBean.getCurrentLotView()
					.setConflictCheckEnabled( conflictCheckEnabled )
					.setConflictCheckResultEnabled( conflictCheckResultEnabled )
					.setMergeEnabled( mergeEnabled )
					.setMergeHistoryEnabled( mergeHistoryEnabled )
					.setUseMerge( lotEntity.isUseMerge().parseBoolean());
			} else {

				OtherMergeLotView otherMergeLotView = paramBean.new OtherMergeLotView();
				otherMergeLotView.setLotId( lotEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setNoOfMerges( numberOfMerges )
					.setNoOfChangePropertys( numberOfChangeProperties )
					.setStartTime( startTime )
					.setEndTime( endTime )
					.setStsId( mergeStatusId )
					.setStatus( mergeStatus )
					.setWarningOfConflict( warningOfConflict )
					.setWarningOfMerge(warningOfMerge );

				otherMergeLotViews.add( otherMergeLotView );
				size++;
			}
		}

		if (size <= linesPerPage) {

			selectedPageNo = 1;
			paramBean.setOtherLotViews( otherMergeLotViews );

		} else {

			int maxRowRange = (selectedPageNo * linesPerPage);

			if (maxRowRange > size || linesPerPage == 0) {

				maxRowRange = size;
			}

			paramBean.setOtherLotViews(new ArrayList<OtherMergeLotView>());

			for (int i = ((selectedPageNo - 1) * linesPerPage); i < maxRowRange; i++ ) {
				paramBean.getOtherLotViews().add( otherMergeLotViews.get(i) );
			}
		}

		{
			limit.setMaxRows( size );
			limit.setViewRows( linesPerPage );
			limit.getPageBar().setValue( selectedPageNo );

			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit);
			paramBean.setPage(page);
		}

	}
}
