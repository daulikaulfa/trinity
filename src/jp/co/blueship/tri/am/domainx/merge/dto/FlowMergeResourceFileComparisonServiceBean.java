package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean;
import jp.co.blueship.tri.am.constants.MergeDiffOption;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMergeResourceFileComparisonServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ResourceComparisonViewBean resourceComparisonView = new ResourceComparisonViewBean();

	public RequestParam getParam() {
		return param;
	}

	public ResourceComparisonViewBean getResourceComparisonView() {
		return resourceComparisonView;
	}
	public FlowMergeResourceFileComparisonServiceBean setResourceComparisonView(
			ResourceComparisonViewBean resourceComparisonView) {
		this.resourceComparisonView = resourceComparisonView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String areqId = null;
		private String resourcePath = null;
		private MergeResourceFileComparisonInputInfo inputInfo = new MergeResourceFileComparisonInputInfo();

		public String getSelectedAreqId() {
			return areqId;
		}

		public RequestParam setSelectedAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getSelectedResourcePath() {
			return resourcePath;
		}
		public RequestParam setSelectedResourcePath(String selectedResourcePath) {
			this.resourcePath = selectedResourcePath;
			return this;
		}

		public MergeResourceFileComparisonInputInfo getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(MergeResourceFileComparisonInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	public class MergeResourceFileComparisonInputInfo {
		private MergeDiffOption dest = MergeDiffOption.none;
		private MergeDiffOption src = MergeDiffOption.none;
		private List<MergeDiffOption> destViews = new ArrayList<MergeDiffOption>();
		private List<MergeDiffOption> srcViews = new ArrayList<MergeDiffOption>();

		public MergeDiffOption getDest() {
			return dest;
		}
		public MergeResourceFileComparisonInputInfo setDest(MergeDiffOption destVersion) {
			this.dest = destVersion;
			return this;
		}

		public MergeDiffOption getSrc() {
			return src;
		}
		public MergeResourceFileComparisonInputInfo setSrc(MergeDiffOption srcVersion) {
			this.src = srcVersion;
			return this;
		}

		public List<MergeDiffOption> getDestViews() {
			return destViews;
		}
		public MergeResourceFileComparisonInputInfo setDestViews(List<MergeDiffOption> destViews) {
			this.destViews = destViews;
			return this;
		}

		public List<MergeDiffOption> getSrcViews() {
			return srcViews;
		}
		public MergeResourceFileComparisonInputInfo setSrcViews(List<MergeDiffOption> srcViews) {
			this.srcViews = srcViews;
			return this;
		}
	}

}
