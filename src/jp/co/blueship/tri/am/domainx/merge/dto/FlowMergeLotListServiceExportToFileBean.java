package jp.co.blueship.tri.am.domainx.merge.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.CurrentMergeLotView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.OtherMergeLotView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowMergeLotListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private CurrentMergeLotView currentLotView = new FlowMergeLotListServiceBean().new CurrentMergeLotView();
	private MergeLotListFileResponse fileResponse = new MergeLotListFileResponse();
	private List<OtherMergeLotView> otherLotViews = new ArrayList<OtherMergeLotView>();

	{
		this.setInnerService( new FlowMergeLotListServiceBean() );
	}

	public List<OtherMergeLotView> getOtherLotViews() {
		return otherLotViews;
	}
	public FlowMergeLotListServiceExportToFileBean setOtherLotViews(List<OtherMergeLotView> otherLotViews) {
		this.otherLotViews = otherLotViews;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowMergeLotListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public MergeLotListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowMergeLotListServiceExportToFileBean setFileResponse(MergeLotListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public CurrentMergeLotView getCurrentLotView() {
		return currentLotView;
	}
	public FlowMergeLotListServiceExportToFileBean setCurrentLotView(CurrentMergeLotView currentLotView) {
		this.currentLotView = currentLotView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowMergeLotListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class MergeLotListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	public class MergeLotView {
		private String lotId;
		private String lotNm;
		private int noOfMerges = 0;
		private int noOfChangePropertys = 0;
		private String startTime;
		private String endTime;
		private String stsId;
		private String status;

		public String getLotId() {
			return lotId;
		}
		public MergeLotView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotNm() {
			return lotNm;
		}
		public MergeLotView setLotNm(String lotNm) {
			this.lotNm = lotNm;
			return this;
		}

		public int getNoOfMerges() {
			return noOfMerges;
		}
		public MergeLotView setNoOfMerges(int noOfMerges) {
			this.noOfMerges = noOfMerges;
			return this;
		}

		public int getNoOfChangePropertys() {
			return noOfChangePropertys;
		}
		public MergeLotView setNoOfChangePropertys(int noOfChangePropertys) {
			this.noOfChangePropertys = noOfChangePropertys;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public MergeLotView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public MergeLotView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public MergeLotView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public MergeLotView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/*public class CurrentMergeLotView {
		private String lotId;
		private String lotNm;
		private int noOfMerges = 0;
		private int noOfChangePropertys = 0;
		private String startTime;
		private String endTime;
		private String stsId;
		private String status;


		public String getLotId() {
			return lotId;
		}
		public CurrentMergeLotView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotNm() {
			return lotNm;
		}
		public CurrentMergeLotView setLotNm(String lotNm) {
			this.lotNm = lotNm;
			return this;
		}

		public int getNoOfMerges() {
			return noOfMerges;
		}
		public CurrentMergeLotView setNoOfMerges(int noOfMerges) {
			this.noOfMerges = noOfMerges;
			return this;
		}

		public int getNoOfChangePropertys() {
			return noOfChangePropertys;
		}
		public CurrentMergeLotView setNoOfChangePropertys(int noOfChangePropertys) {
			this.noOfChangePropertys = noOfChangePropertys;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public CurrentMergeLotView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public CurrentMergeLotView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public CurrentMergeLotView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public CurrentMergeLotView setStatus(String status) {
			this.status = status;
			return this;
		}


	}*/
}
