package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMergeHistoryDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<MergeHistoryView> mergeHistoryViews = new ArrayList<MergeHistoryView>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<MergeHistoryView> getMergeHistoryViews() {
		return mergeHistoryViews;
	}
	public FlowMergeHistoryDetailsServiceBean setMergeHistoryViews(List<MergeHistoryView> mergeHistoryViews) {
		this.mergeHistoryViews = mergeHistoryViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowMergeHistoryDetailsServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 20;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		private Integer selectedPageNo = 1;

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
	}

	public class MergeHistoryView {
		private String lotId;
		private String lotNm;
		private String headBlId;
		private int noOfChangePropertys = 0;
		private String startTime;
		private String endTime;
		private String stsId;
		private String status;

		public String getLotId() {
			return lotId;
		}
		public MergeHistoryView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getLotNm() {
			return lotNm;
		}
		public MergeHistoryView setLotNm(String lotNm) {
			this.lotNm = lotNm;
			return this;
		}

		public String getHeadBlId() {
			return headBlId;
		}
		public MergeHistoryView setHeadBlId(String headBlId) {
			this.headBlId = headBlId;
			return this;
		}

		public int getNoOfChangePropertys() {
			return noOfChangePropertys;
		}
		public MergeHistoryView setNoOfChangePropertys(int noOfChangePropertys) {
			this.noOfChangePropertys = noOfChangePropertys;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public MergeHistoryView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public MergeHistoryView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public MergeHistoryView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public MergeHistoryView setStatus(String status) {
			this.status = status;
			return this;
		}
	}
}
