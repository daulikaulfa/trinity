package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowConflictCheckResourceFileResultsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ConflictCheckResourceFileResultView> conflictCheckResultViews = new ArrayList<ConflictCheckResourceFileResultView>();

	{
		this.setInnerViewService( new FlowChaLibMergeBaselineDetailViewServiceBean() );
		this.setInnerService(new FlowChaLibMergeConflictCheckResultServiceBean());
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public List<ConflictCheckResourceFileResultView> getConflictCheckResultViews() {
		return conflictCheckResultViews;
	}
	public FlowConflictCheckResourceFileResultsServiceBean setConflictCheckResultViews(List<ConflictCheckResourceFileResultView> conflictCheckResultViews) {
		this.conflictCheckResultViews = conflictCheckResultViews;
		return this;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
	}

	public class ConflictCheckResourceFileResultView {
		private String resourcePath;
		private List<ChangePropertyView> changeProperties;
		private String moduleNm;
		private MergeStatus mergeStatus = MergeStatus.none;
		private boolean diff;
		private boolean isBinary;

		public boolean isSuccess() {
			return (!MergeStatus.none.equals(mergeStatus)) &&  (!isWarningOfConflict());
		}
		public boolean isWarningOfConflict() {
			return MergeStatus.Conflicted.equals(mergeStatus);
		}

		public String getResourcePath() {
			return resourcePath;
		}
		public ConflictCheckResourceFileResultView setResourcePath(String resourcePath) {
			this.resourcePath = resourcePath;
			return this;
		}

		public List<ChangePropertyView> getChangeProperties() {
			return changeProperties;
		}
		public ConflictCheckResourceFileResultView setChangeProperties(List<ChangePropertyView> changeProperties) {
			this.changeProperties = changeProperties;
			return this;
		}

		public String getModuleNm() {
			return moduleNm;
		}
		public ConflictCheckResourceFileResultView setModuleNm(String moduleNm) {
			this.moduleNm = moduleNm;
			return this;
		}

		public MergeStatus getMergeStatus() {
			return mergeStatus;
		}
		public ConflictCheckResourceFileResultView setMergeStatus(MergeStatus mergeStatus) {
			this.mergeStatus = mergeStatus;
			return this;
		}

		public boolean isDiff() {
			return diff;
		}
		public ConflictCheckResourceFileResultView setDiff(boolean diff) {
			this.diff = diff;
			return this;
		}

		public boolean isBinary(){
			return this.isBinary;
		}
		public ConflictCheckResourceFileResultView setBinary(boolean isBinary) {
			this.isBinary = isBinary;
			return this;
		}
	}

	public class ChangePropertyView {
		private String pjtId;
		private String referenceId;

		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}
	}

}
