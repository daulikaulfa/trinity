package jp.co.blueship.tri.am.domainx.merge;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean.ResourceLineView;
import jp.co.blueship.tri.am.beans.dto.ResourceComparisonViewBean.ResourcePropertyView;
import jp.co.blueship.tri.am.constants.MergeDiffOption;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeResourceFileComparisonServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowMergeResourceFileComparisonService implements IDomain<FlowMergeResourceFileComparisonServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}


	@Override
	public IServiceDto<FlowMergeResourceFileComparisonServiceBean> execute(
			IServiceDto<FlowMergeResourceFileComparisonServiceBean> serviceDto) {

		FlowMergeResourceFileComparisonServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String pjtId = paramBean.getParam().getSelectedAreqId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(pjtId), "SelectedPjtId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}else if(RequestType.onChange.equals(paramBean.getParam().getRequestType())){
				this.onChange(paramBean);

			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}
	}


	private void init(FlowMergeResourceFileComparisonServiceBean serviceBean) throws Exception{

		String pjtId 		= serviceBean.getParam().getSelectedAreqId();
		String resourcePath = serviceBean.getParam().getSelectedResourcePath();

		MergeDiffOption srcVer = serviceBean.getParam().getInputInfo().getSrc();
		MergeDiffOption dstVer = serviceBean.getParam().getInputInfo().getDest();

		IPjtEntity pjtEntity		 = this.support.findPjtEntity( pjtId ) ;

		String lotId = pjtEntity.getLotId();

		File srcFile = getFile(lotId , resourcePath , srcVer);
		File dstFile = getFile(lotId , resourcePath , dstVer);


		this.setResourcePropertyView( serviceBean , resourcePath , srcFile , dstFile);

		//diff
		EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer( srcFile , dstFile ) ;
		differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

		this.setLine(serviceBean, differ.diff());
	}


	private void onChange(FlowMergeResourceFileComparisonServiceBean serviceBean) throws Exception{
		this.init(serviceBean);
	}


	private void setResourcePropertyView( FlowMergeResourceFileComparisonServiceBean serviceBean ,
										  String resourceFile,File srcFile , File dstFile){

		String srcSubmitterNm	= "";
		String dstSubmitterNm	= "";
		Timestamp srcUpdTime	= new Timestamp( srcFile.lastModified() );
		Timestamp dstUpdTime	= new Timestamp( dstFile.lastModified() );


		ResourcePropertyView src =  serviceBean.getResourceComparisonView().getSrcResource()
				.setResourcePath	( resourceFile )
				.setSubmitterNm		( srcSubmitterNm )
				.setResourceUpdTime	( TriDateUtils.convertViewDateFormat(
										srcUpdTime,
										TriDateUtils.getYMDHMSDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
										).toString() )
				.setVisible			( srcFile.exists() )
				;

		ResourcePropertyView dst =  serviceBean.getResourceComparisonView().getDestResource()
				.setResourcePath	( resourceFile )
				.setSubmitterNm		( dstSubmitterNm )
				.setResourceUpdTime	( TriDateUtils.convertViewDateFormat(
										dstUpdTime,
										TriDateUtils.getYMDHMSDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone())
										).toString() )
				.setVisible			( dstFile.exists() )
				;

		serviceBean.getResourceComparisonView().setSrcResource (src);
		serviceBean.getResourceComparisonView().setDestResource(dst);
	}


	private void setLine( FlowMergeResourceFileComparisonServiceBean serviceBean ,
						  DiffResult diff){

		List<ResourceLineView> lines = serviceBean.getResourceComparisonView().getLine();
		lines.clear();
		for(IDiffElement resultElement : diff.getDiffElementList()){

			FileStatus fileStatus = null;
			Status status  = resultElement.getStatus();

			if		(Status.ADD.equals(status))		fileStatus = FileStatus.Add;
			else if (Status.DELETE.equals(status))	fileStatus = FileStatus.Delete;
			else if (Status.CHANGE.equals(status))	fileStatus = FileStatus.Edit;
			else if (Status.EQUAL.equals(status))	fileStatus = FileStatus.Same;


			ResourceLineView lineView = serviceBean.getResourceComparisonView().new ResourceLineView()
					.setStatus		(fileStatus)
					.setDestLineNo	(resultElement.getDstLineNumber()+"")
					.setSrcLineNo	(resultElement.getSrcLineNumber()+"")
					.setDest		(resultElement.getDstContents())
					.setSrc			(resultElement.getSrcContents())
					;

			lines.add(lineView);
		}
		serviceBean.getResourceComparisonView().setLine(lines);
	}

	private File getFile(String lotId , String resourcePath , MergeDiffOption requestFile){

		ILotEntity  lotEntity  = this.support.findLotEntity( lotId ) ;
		IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;
		String workspace = lotEntity.getWsPath() ;

		String filePath = null;


		if( requestFile.equals(MergeDiffOption.Branche) ){
			filePath = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTempBranchCheckOut ) ) ;

		}else if( requestFile.equals(MergeDiffOption.Master) ){
			filePath = AmDesignBusinessRuleUtils.getMasterWorkPath( headEntity ).getPath();

		}else if( requestFile.equals(MergeDiffOption.MergeResult) ){
			filePath = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) );

		}else if( requestFile.equals(MergeDiffOption.none) ){
			throw new TriSystemException( AmMessageId.AM004043F , "" ) ;
		}

		PreConditions.assertOf(filePath != null, "filePath is not specified");

		//file check
		if( true != new File( filePath ).exists() ) {
			throw new TriSystemException( AmMessageId.AM004043F , filePath ) ;
		}

		return new File( TriStringUtils.linkPathBySlash( filePath , resourcePath ) );
	}
}
