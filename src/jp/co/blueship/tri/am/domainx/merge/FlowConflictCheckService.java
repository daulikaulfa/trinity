package jp.co.blueship.tri.am.domainx.merge;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean.BaselineView;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowConflictCheckService implements IDomain<FlowConflictCheckServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	private IDomain<IGeneralServiceBean> confirmationService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixActionByTestComplete = null;

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setStatusMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixActionByTestComplete = action;
	}

	public IDomain<IGeneralServiceBean> getConfirmationService() {
		return confirmationService;
	}

	public void setConfirmationService(IDomain<IGeneralServiceBean> confirmationService) {
		this.confirmationService = confirmationService;
	}

	public IDomain<IGeneralServiceBean> getCompleteService() {
		return completeService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	@Override
	public IServiceDto<FlowConflictCheckServiceBean> execute
		(IServiceDto<FlowConflictCheckServiceBean> serviceDto) {
		FlowConflictCheckServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibConflictCheckServiceBean serviceBean = paramBean.getInnerService();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {

				this.init(paramBean, lotId);
			} else {

				if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
					this.validate( paramBean, lotId );
				}

				if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
					this.submitChanges(paramBean, lotId);
				}
			}

			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());

		} finally {

			ExceptionUtils.copyToParent(paramBean, serviceBean);
		}
	}


	private void init(FlowConflictCheckServiceBean paramBean, String lotId) {
		ILotEntity lotEntity = this.support.findLotEntity( lotId );
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter() ;
		ILotDto lotDto = new LotDto();
		lotDto.setLotEntity( lotEntity );

		// グループの存在チェック
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,
				this.support.getUmFinderSupport().getGrpDao(),
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		List<ILotBlEntity> lotBlEntityArray = this.support.getLotBlEntitiesByMergeCheckEnable( lotId , null , TriSortOrder.Desc );
		if (TriStringUtils.isNotEmpty( lotBlEntityArray )) {
			IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
			FlowChaLibConflictCheckServiceBean serviceBean = paramBean.getInnerService();
			TriPropertyUtils.copyProperties(serviceBean, paramBean);
			dto.setServiceBean(serviceBean);

			ILotBlEntity lotBlEntity = this.support.findLotBlEntity( lotBlEntityArray.get(0).getLotBlId() );
			// Call confirmation to check status matrix
			{
				serviceBean.setSelectedLotVersionTag( lotBlEntity.getLotVerTag() );
				serviceBean.setScreenType( ScreenType.next );
				serviceBean.setSelectedLotId(lotId);
				serviceBean.setReferer( ChaLibScreenID.COMP_CONFLICTCHECK );
				serviceBean.setForward( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
				confirmationService.execute(dto);
			}
			if( serviceBean.getWarningCheckMergeCommitError().isExistWarning() )
			{
				List<String> mergeWarningMessages =  ca.getMessage(serviceBean);
				paramBean.setMessages( mergeWarningMessages );
			}

			paramBean.setBaselineViews( this.getBaselineView(paramBean, lotBlEntityArray.toArray(new ILotBlEntity[0]) ));
		} else {
			throw new ContinuableBusinessException(AmMessageId.AM001107E);
		}
	}

	private List<BaselineView> getBaselineView( FlowConflictCheckServiceBean paramBean, ILotBlEntity[] lotBlEntities) {
		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		Map<String, List<String>> lotVerTagBpIdMap = new HashMap<String, List<String>>();
		List<BaselineView> baselineViews = new ArrayList<BaselineView>();

		// Sort by lot_chkin_timestamp DESC
		Arrays.sort( lotBlEntities,
				new Comparator<ILotBlEntity>() {
					public int compare( ILotBlEntity o1, ILotBlEntity o2 ) {
						ILotBlEntity src1 = (ILotBlEntity)o1;
						ILotBlEntity src2 = (ILotBlEntity)o2;

						return src2.getLotChkinTimestamp().compareTo( src1.getLotChkinTimestamp() );
					}
				} );


		for ( ILotBlEntity lotBlEntity : lotBlEntities ) {

			if ( lotVerTagBpIdMap.containsKey( lotBlEntity.getLotVerTag() ) ) {
				lotVerTagBpIdMap.get(lotBlEntity.getLotVerTag()).add(lotBlEntity.getDataId());
				continue;
			} else {
				List<String> bpIds = new ArrayList<String>();
				bpIds.add(lotBlEntity.getDataId());
				lotVerTagBpIdMap.put(lotBlEntity.getLotVerTag(), bpIds);
			}

			BaselineView baselineView = paramBean.new BaselineView()
					.setLotBlTag( lotBlEntity.getLotBlTag() )
					.setLotVerTag( lotBlEntity.getLotVerTag() )
					.setLotCheckinSubmitterNm( lotBlEntity.getLotChkinUserNm() )
					.setLotCheckinSubmitterIconPath( lotBlEntity.getLotChkinUserIconPath() )
					.setLotCheckinTime( TriDateUtils.convertViewDateFormat( lotBlEntity.getLotChkinTimestamp(), formatYMD ) );

			baselineViews.add( baselineView);
		}

		for ( BaselineView baselineView : baselineViews ) {
			List<String> bpIds = lotVerTagBpIdMap.get(baselineView.getLotVerTag());
			List<String> rpIds = new ArrayList<String>();

			if ( null != bpIds) {
				rpIds = this.support.getBmFinderSupport().findRpIdsByBpIds( bpIds.toArray( new String[0] ));
			}
			baselineView.setBpIds( bpIds )
						.setRpIds( rpIds );
		}

		return baselineViews;
	}

	private void validate( FlowConflictCheckServiceBean paramBean, String lotId ) {

		{

			String closeVersionTag = paramBean.getParam().getInputInfo().getSelectedLotVerTag();

			List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( lotId, closeVersionTag, TriSortOrder.Desc );
			List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
			ILotBlDto lotBlDto = lotBlDtoList.get(0);

			List<ILotBlDto> pastLotBlDtoList = this.support.getLotBlDtoByMergeCheckEnable(lotId,
					lotBlDto.getLotBlEntity().getLotChkinTimestamp(), TriSortOrder.Desc);
			List<IPjtEntity> pjtEntities = this.support.getPjtEntities(pastLotBlDtoList,
					AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView());

			String[] pjtIds = FluentList.from(pjtEntities).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);
			IAreqEntity[] areqEntities = this.support.getAreqEntitiesFromPjtIds((ISqlSort)null, pjtIds);

			String[] blIds = FluentList.from(pastLotBlDtoList).map(AmFluentFunctionUtils.toLotBlIdsFromLotBlDto).toArray(new String[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setLotIds( lotBlDto.getLotBlEntity().getLotId() )
					.setPjtIds( pjtIds )
					.setAreqIds( FluentList.from(areqEntities).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]) )
					.setLotBls( blIds )
					.setHeadBls( this.getHeadBlIds( lotBlDto.getLotBlEntity().getLotId() ) )
					.setBpIds( this.getBpIds( lotBlEntityList ) );

			if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
				// テスト完了を運用前提とする
				statusDto.setActionList( statusMatrixActionByTestComplete );
			} else {
				// テスト完了を運用前提としない
				statusDto.setActionList( statusMatrixAction );
			}
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibConflictCheckServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean, lotId);

		// Call confirmation to check status matrix
		{
			serviceBean.setReferer( ChaLibScreenID.CONFLICTCHECK_BASELINE_DETAIL_VIEW );
			serviceBean.setForward( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
			confirmationService.execute(dto);
		}

		paramBean.getConfirmView().clear();

		if( serviceBean.getWarningCheckMergeCommitError().isExistWarning() )
		{
			List<String> mergeCommitErrorMessages = serviceBean.getWarningCheckMergeCommitError().getCommentList();

			if( 0 != mergeCommitErrorMessages.size() ){
				String agreementMessage = mergeCommitErrorMessages.get(0);
				mergeCommitErrorMessages.remove(0);

				paramBean.getConfirmView()
						.setAgreementMessage( agreementMessage )
						.setWarningMessages	( mergeCommitErrorMessages )
						;
			}
		}

		this.afterExecution(paramBean, serviceBean, lotId);
	}

	private void submitChanges( FlowConflictCheckServiceBean paramBean, String lotId ) {

		// Status Matrix Check
		{
			String closeVersionTag = paramBean.getParam().getInputInfo().getSelectedLotVerTag();

			List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( lotId, closeVersionTag, TriSortOrder.Desc );
			List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
			ILotBlDto lotBlDto = lotBlDtoList.get(0);

			List<ILotBlDto> pastLotBlDtoList = this.support.getLotBlDtoByMergeCheckEnable(lotId,
					lotBlDto.getLotBlEntity().getLotChkinTimestamp(), TriSortOrder.Desc);
			List<IPjtEntity> pjtEntities = this.support.getPjtEntities(pastLotBlDtoList,
					AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView());

			String[] pjtIds = FluentList.from(pjtEntities).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);
			IAreqEntity[] areqEntities = this.support.getAreqEntitiesFromPjtIds((ISqlSort)null, pjtIds);

			String[] blIds = FluentList.from(pastLotBlDtoList).map(AmFluentFunctionUtils.toLotBlIdsFromLotBlDto).toArray(new String[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setLotIds( lotBlDto.getLotBlEntity().getLotId() )
					.setPjtIds( pjtIds )
					.setAreqIds( FluentList.from(areqEntities).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]) )
					.setLotBls( blIds )
					.setHeadBls( this.getHeadBlIds( lotBlDto.getLotBlEntity().getLotId() ) )
					.setBpIds( this.getBpIds( lotBlEntityList ) );

			if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
				// テスト完了を運用前提とする
				statusDto.setActionList( statusMatrixActionByTestComplete );
			} else {
				// テスト完了を運用前提としない
				statusDto.setActionList( statusMatrixAction );
			}
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibConflictCheckServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean, lotId);

		// Call confirmation to check status matrix
		{
			serviceBean.setReferer( ChaLibScreenID.CONFLICTCHECK_BASELINE_DETAIL_VIEW );
			serviceBean.setForward( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
			confirmationService.execute(dto);
		}

		// Call confirmation to check input information
		{
			serviceBean.setReferer( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_CONFLICTCHECK );
			confirmationService.execute(dto);
		}

		// Call complete service
		{
			serviceBean.setReferer( ChaLibScreenID.CONFLICTCHECK_CONFIRM );
			serviceBean.setForward( ChaLibScreenID.COMP_CONFLICTCHECK );
			completeService.execute(dto);
		}

		this.afterExecution(paramBean, serviceBean, lotId);
	}

	private void beforeExecution(FlowConflictCheckServiceBean src, FlowChaLibConflictCheckServiceBean dest, String lotId) {

		if ( RequestType.validate.equals(src.getParam().getRequestType())
			|| RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {

			String lotVerTag = src.getParam().getInputInfo().getSelectedLotVerTag();
			if( TriStringUtils.isEmpty( lotVerTag ) ){
				if( TriCollectionUtils.isEmpty(src.getBaselineViews()) ) {
					throw new ContinuableBusinessException( AmMessageId.AM001142E );
				} else {
					throw new ContinuableBusinessException( AmMessageId.AM004012F , lotVerTag );
				}
			}

			dest.setSelectedLotVersionTag( lotVerTag );
			dest.setScreenType( ScreenType.next );
			dest.setSelectedLotId(lotId);

			if ( RequestType.submitChanges.equals(src.getParam().getRequestType()) ) {

				dest.setProcId( src.getProcId() );
				dest.setProcMgtStatusId( src.getProcMgtStatusId());
				dest.setLockLotNo( src.getParam().getSelectedLotId() );

				MergeEditInputBean mergeInputBen = new MergeEditInputBean();
				mergeInputBen.setConflictCheckComment( src.getParam().getInputInfo().getComment() );
				dest.setMergeEditInputBean( mergeInputBen );

				dest.getWarningCheckMergeCommitError().setConfirmCheck(src.getParam().getInputInfo().isSubmitAgreement());
			}
		}
	}

	private void afterExecution(FlowConflictCheckServiceBean dest, FlowChaLibConflictCheckServiceBean src, String lotId) {
		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType()) ) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted( true );
			dest.getMessageInfo().addFlashTranslatable(UmMessageId.UM003028I);
		}
	}

	private String[] getHeadBlIds(String lotId){
		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( HistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IHeadBlEntity> entityList =  this.support.getHeadBlDao().find(condition.getCondition(), sort);
		List<String> headBlIdList = new ArrayList<String>();

		if( entityList.size() != 0){
			headBlIdList.add( entityList.get(0).getHeadBlId() );
		}

		return headBlIdList.toArray(new String[]{});
	}

	private String[] getBpIds( List<ILotBlEntity> lotBlEntityList ){

		List<String> bpIdList = new ArrayList<String>();

		for( ILotBlEntity entity : lotBlEntityList ){
			if( entity.getDataCtgCd().equals( BmTables.BM_BP.name() ) ){
				bpIdList.add( entity.getDataId() );
			}
		}

		return bpIdList.toArray(new String[]{});
	}
}
