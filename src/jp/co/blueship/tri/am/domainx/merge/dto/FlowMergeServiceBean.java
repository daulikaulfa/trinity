package jp.co.blueship.tri.am.domainx.merge.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean.Module;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMergeServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private LotDetailsViewBean lotDetailsView = new LotDetailsViewBean();
	private ConflictCheckResultOverview baselineView = new ConflictCheckResultOverview();
	private List<ChangePropertyDetailsView> changePropertyDetailsViews = new ArrayList<ChangePropertyDetailsView>();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerViewService( new FlowChaLibMergeBaselineDetailViewServiceBean());
		this.setInnerService(new FlowChaLibMergeCommitServiceBean());
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public LotDetailsViewBean getLotDetailsView() {
		return lotDetailsView;
	}
	public FlowMergeServiceBean setLotDetailsView(LotDetailsViewBean lotDetailsView) {
		this.lotDetailsView = lotDetailsView;
		return this;
	}

	public ConflictCheckResultOverview getBaselineView() {
		return baselineView;
	}
	public FlowMergeServiceBean setBaselineView(ConflictCheckResultOverview baselineView) {
		this.baselineView = baselineView;
		return this;
	}

	public List<ChangePropertyDetailsView> getChangePropertyViews() {
		return changePropertyDetailsViews;
	}
	public FlowMergeServiceBean setChangePropertyViews(List<ChangePropertyDetailsView> changePropertyViews) {
		this.changePropertyDetailsViews = changePropertyViews;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowMergeServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private MergeInputInfo inputInfo = new MergeInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public MergeInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(MergeInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	public class MergeInputInfo {
		private String comment;

		public String getComment() {
			return comment;
		}
		public MergeInputInfo setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}

	public class LotDetailsViewBean {
		private String lotId;
		private String stsId;
		private String status;
		private String createdTime;
		private String subject;
		private String summary;
		private String contents;
		private String latestBaselineTag;
		private String latestVerTag;
		private String branchBaselineTag;
		private List<Module> modules = new ArrayList<Module>();

		public String getLotId() {
			return lotId;
		}
		public LotDetailsViewBean setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public LotDetailsViewBean setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public LotDetailsViewBean setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedTime() {
			return createdTime;
		}
		public LotDetailsViewBean setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public LotDetailsViewBean setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public LotDetailsViewBean setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getContents() {
			return contents;
		}
		public LotDetailsViewBean setContents(String contents) {
			this.contents = contents;
			return this;
		}

		public String getLatestBaselineTag() {
			return latestBaselineTag;
		}
		public LotDetailsViewBean setLatestBaselineTag(String latestBaselineTag) {
			this.latestBaselineTag = latestBaselineTag;
			return this;
		}

		public String getLatestVerTag() {
			return latestVerTag;
		}
		public LotDetailsViewBean setLatestVerTag(String latestVerTag) {
			this.latestVerTag = latestVerTag;
			return this;
		}

		public String getBranchBaselineTag() {
			return branchBaselineTag;
		}
		public LotDetailsViewBean setBranchBaselineTag(String branchBaselineTag) {
			this.branchBaselineTag = branchBaselineTag;
			return this;
		}

		public List<Module> getModules() {
			return modules;
		}
		public LotDetailsViewBean setModules(List<Module> modules) {
			this.modules = modules;
			return this;
		}
	}

	public class ConflictCheckResultOverview {
		private String headBlId;
		private String stsId;
		private String status;
		private String conflictCheckComment;
		private String startTime;
		private String endTime;
		private boolean mergeEnabled = false;

		public String getHeadBlId() {
			return headBlId;
		}
		public ConflictCheckResultOverview setHeadBlId(String headBlId) {
			this.headBlId = headBlId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ConflictCheckResultOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ConflictCheckResultOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getConflictCheckComment() {
			return conflictCheckComment;
		}
		public ConflictCheckResultOverview setConflictCheckComment(String conflictCheckComment) {
			this.conflictCheckComment = conflictCheckComment;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public ConflictCheckResultOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public ConflictCheckResultOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public boolean isMergeEnabled() {
			return mergeEnabled;
		}
		public ConflictCheckResultOverview setMergeEnabled(boolean mergeEnabled) {
			this.mergeEnabled = mergeEnabled;
			return this;
		}

	}

	public class ChangePropertyDetailsView {
		private String pjtId;
		private String referenceId;
		private String referenceCategoryNm;
		private String summary;
		private String submitterNm;
		private String submitterIconPath;
		private String assigneeNm;
		private String assigneeIconPath;
		private String createdTime;
		private String updTime;
		private String stsId;
		private String status;

		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyDetailsView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyDetailsView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getReferenceCategoryNm() {
			return referenceCategoryNm;
		}
		public ChangePropertyDetailsView setReferenceCategoryNm(String referenceCategoryNm) {
			this.referenceCategoryNm = referenceCategoryNm;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public ChangePropertyDetailsView setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyDetailsView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyDetailsView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public ChangePropertyDetailsView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public ChangePropertyDetailsView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getCreatedTime() {
			return createdTime;
		}
		public ChangePropertyDetailsView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}

		public String getUpdTime() {
			return updTime;
		}
		public ChangePropertyDetailsView setUpdTime(String updTime) {
			this.updTime = updTime;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ChangePropertyDetailsView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ChangePropertyDetailsView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

	}
}
