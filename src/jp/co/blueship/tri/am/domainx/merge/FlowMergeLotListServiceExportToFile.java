package jp.co.blueship.tri.am.domainx.merge;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceExportToFileBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.CurrentMergeLotView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.OtherMergeLotView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.SearchCondition;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;

public class FlowMergeLotListServiceExportToFile implements IDomain<FlowMergeLotListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private IDomain<IGeneralServiceBean> mergeLotList = null;

	public void setMergeLotList(IDomain<IGeneralServiceBean> mergeLotList) {
		this.mergeLotList = mergeLotList;
	}

	@Override
	public IServiceDto<FlowMergeLotListServiceExportToFileBean> execute(
			IServiceDto<FlowMergeLotListServiceExportToFileBean> serviceDto) {
		FlowMergeLotListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowMergeLotListServiceBean innerServiceBean = paramBean.getInnerService();


		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}finally{
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowMergeLotListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowMergeLotListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowMergeLotListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			mergeLotList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		mergeLotList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowMergeLotListServiceExportToFileBean src, FlowMergeLotListServiceBean dest) {

		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
		}


	}

	private void afterExecution(FlowMergeLotListServiceBean src, FlowMergeLotListServiceExportToFileBean dest) {
		List<OtherMergeLotView> srcMergeLotViews = src.getOtherLotViews();
		List<OtherMergeLotView> destMergeLotViews = new ArrayList<OtherMergeLotView>();

		CurrentMergeLotView srcCurrentLotView = src.getCurrentLotView();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcMergeLotViews.size(); i++) {
				OtherMergeLotView view = srcMergeLotViews.get(i);
				destMergeLotViews.add(view);
			}
			dest.setOtherLotViews(destMergeLotViews);
			dest.setCurrentLotView(srcCurrentLotView);
		}


	}
}
