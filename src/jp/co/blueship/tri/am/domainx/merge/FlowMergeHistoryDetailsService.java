package jp.co.blueship.tri.am.domainx.merge;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean.MergeHistoryView;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowMergeHistoryDetailsService implements IDomain<FlowMergeHistoryDetailsServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowMergeHistoryDetailsServiceBean> execute(
			IServiceDto<FlowMergeHistoryDetailsServiceBean> serviceDto) {
		FlowMergeHistoryDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())
					|| RequestType.onChange.equals(paramBean.getParam().getRequestType())) {

				this.getMergeHistoryView( paramBean );
			}
			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());

		}
	}

	private void getMergeHistoryView( FlowMergeHistoryDetailsServiceBean paramBean ) {
		LotCondition lotCondition	= AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
		lotCondition.setUseMerge( StatusFlg.on );

		ISqlSort lotSort			= AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibMargeLotList();
		List<ILotEntity> lotEntities = this.support.getLotDao().find( lotCondition.getCondition(), lotSort);

		List<ILotDto> lotDtos = this.support.findLotDto(lotEntities, AmTables.AM_LOT_GRP_LNK);

		List<IGrpEntity> grpEntities = this.support.getUmFinderSupport().findGroupByUserId( paramBean.getUserId() );
		List<String> grpIds = FluentList.from( grpEntities ).map( UmFluentFunctionUtils.toGroupIdFromGrpEntity).asList();

		ILimit limit = new PageLimit();
		int size = 0;
		int selectedPageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		selectedPageNo = ( 0 == selectedPageNo )? 1 : selectedPageNo;
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		List<MergeHistoryView> mergeHistoryViews = new ArrayList<MergeHistoryView>();

		for (ILotDto lotDto : lotDtos) {
			ILotEntity lotEntity = lotDto.getLotEntity();

			if (TriStringUtils.isNotEmpty( lotDto.getLotGrpLnkEntities() )) {

				boolean enabledLot = false;

				for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {

					if (grpIds.contains(group.getGrpId())) {
						enabledLot = true;
						break;
					}
				}

				if (!enabledLot && StatusFlg.off == lotEntity.getAllowListView()) {
					continue;
				}
			}

			int numberOfChangeProperties = this.support.countAliveByPjt( lotEntity.getLotId() );
			String mergeStatus = null;
			String mergeStatusId = null;
			String headBlId = null;
			String startTime = null;
			String endTime = null;

			List<IHeadBlEntity> headBlEntityList = null;
			{
				HeadBlCondition headCondition = new HeadBlCondition() ;

				headCondition.setLotId( lotEntity.getLotId() ) ;
				headCondition.setStsIds( new String[]{
						AmHeadBlStatusId.Merged.getStatusId(),
						AmHeadBlStatusIdForExecData.ConflictCheckError.getStatusId() } );

				ISqlSort headSort = new SortBuilder();
				headSort.setElement(HeadBlItems.regTimestamp, TriSortOrder.Desc, 0);

				headBlEntityList = this.support.getHeadBlDao().find( headCondition.getCondition(), headSort );
			}
			String procStsId = "";

			if ( TriCollectionUtils.isNotEmpty(headBlEntityList) ) {
				IHeadBlEntity headBlEntity = headBlEntityList.get(0);
				headBlId = headBlEntity.getHeadBlId();
				procStsId = headBlEntity.getProcStsId();
				mergeStatus = sheet.getValue( AmDesignBeanId.statusId , procStsId );
				mergeStatusId = procStsId;
				startTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkStTimestamp(), formatYMDHM);
				endTime = TriDateUtils.convertViewDateFormat( headBlEntity.getMergechkEndTimestamp(), formatYMDHM);
			} else {

				continue;
			}

			MergeHistoryView viewBean = paramBean.new MergeHistoryView()
					.setLotId( lotEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setHeadBlId( headBlId )
					.setNoOfChangePropertys( numberOfChangeProperties )
					.setStartTime( startTime )
					.setEndTime( endTime )
					.setStsId( mergeStatusId )
					.setStatus( mergeStatus );

			mergeHistoryViews.add( viewBean );
			size++;
		}

		if (size <= linesPerPage) {

			selectedPageNo = 1;
			paramBean.setMergeHistoryViews( mergeHistoryViews );

		} else {

			int maxRowRange = (selectedPageNo * linesPerPage);

			if (maxRowRange > size || linesPerPage == 0) {

				maxRowRange = size;
			}

			paramBean.setMergeHistoryViews(new ArrayList<MergeHistoryView>());

			for (int i = ((selectedPageNo - 1) * linesPerPage); i < maxRowRange; i++ ) {

				paramBean.getMergeHistoryViews().add( mergeHistoryViews.get(i) );
			}
		}

		{
			limit.setMaxRows( size );
			limit.setViewRows( linesPerPage );
			limit.getPageBar().setValue( selectedPageNo );

			IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit);
			paramBean.setPage(page);
		}

	}

}
