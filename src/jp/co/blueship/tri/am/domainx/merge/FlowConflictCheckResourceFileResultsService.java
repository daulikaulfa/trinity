package jp.co.blueship.tri.am.domainx.merge;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeViewBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckResourceFileResultsServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckResourceFileResultsServiceBean.ChangePropertyView;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckResourceFileResultsServiceBean.ConflictCheckResourceFileResultView;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowConflictCheckResourceFileResultsService implements IDomain<FlowConflictCheckResourceFileResultsServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	private IDomain<IGeneralServiceBean> conflictCheckResult  = null;
	private IDomain<IGeneralServiceBean> baselineDetailView = null;

	public IDomain<IGeneralServiceBean> getConflictCheckResult() {
		return conflictCheckResult;
	}

	public void setConflictCheckResult(IDomain<IGeneralServiceBean> conflictCheckResult) {
		this.conflictCheckResult = conflictCheckResult;
	}

	public IDomain<IGeneralServiceBean> getBaselineDetailView() {
		return baselineDetailView;
	}

	public void setBaselineDetailView(IDomain<IGeneralServiceBean> baselineDetailView) {
		this.baselineDetailView = baselineDetailView;
	}

	@Override
	public IServiceDto<FlowConflictCheckResourceFileResultsServiceBean> execute(
			IServiceDto<FlowConflictCheckResourceFileResultsServiceBean> serviceDto) {
		FlowConflictCheckResourceFileResultsServiceBean paramBean = serviceDto.getServiceBean();
		FlowChaLibMergeBaselineDetailViewServiceBean detailServiceBean = paramBean.getInnerViewService();
		FlowChaLibMergeConflictCheckResultServiceBean serviceBean = paramBean.getInnerService();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {

				this.init( paramBean, lotId );
			}
			return serviceDto;

		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());

		} finally {

			ExceptionUtils.copyToParent(paramBean, serviceBean);
			ExceptionUtils.copyToParent(paramBean, detailServiceBean);
		}
	}

	private void init( FlowConflictCheckResourceFileResultsServiceBean paramBean, String lotId ) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibMergeBaselineDetailViewServiceBean detailServiceBean = paramBean.getInnerViewService();
		FlowChaLibMergeConflictCheckResultServiceBean serviceBean = paramBean.getInnerService();

		TriPropertyUtils.copyProperties(detailServiceBean, paramBean);
		TriPropertyUtils.copyProperties(serviceBean, paramBean);

		// Call merge baseline detail service
		{
			dto.setServiceBean( detailServiceBean );
			detailServiceBean.setSelectedLotNo( lotId );

			baselineDetailView.execute(dto);
		}

		// Call conflict check service
		{
			dto.setServiceBean( serviceBean );
			serviceBean.setSelectedLotNo( lotId );
			serviceBean.setSelectedCloseVersionTag( detailServiceBean.getBaselineViewBean().getVersionTag() );
			conflictCheckResult.execute(dto);
		}

		this.setConflictChecktResource(serviceBean, paramBean);

	}

	private void setConflictChecktResource( FlowChaLibMergeConflictCheckResultServiceBean src, FlowConflictCheckResourceFileResultsServiceBean dest ) {
		List<ConflictCheckResourceFileResultView> conflictCheckResourceList = new ArrayList<ConflictCheckResourceFileResultView>();

		Map<String, IExtEntity> ucfExtensionMap = support.getExtMap();

		for ( MergeViewBean viewBean : src.getMergeViewBeanList() ) {
			ConflictCheckResourceFileResultView conflictCheckResource = dest.new ConflictCheckResourceFileResultView();

			List<ChangePropertyView> changeProperties = new ArrayList<ChangePropertyView>();

			for (PjtViewBean pjtViewBean : viewBean.getPjtViewList() ) {
				changeProperties.add( dest.new ChangePropertyView()
					.setPjtId( pjtViewBean.getPjtNo() )
					.setReferenceId( pjtViewBean.getChangeCauseNo() )
				);
			}

			boolean diff = !EqualsContentsSetSameLineDiffer.Status.EQUAL.equals(EqualsContentsSetSameLineDiffer.Status.valueOf( viewBean.getDiffStatus() ))
					&& MergeStatus.Modified.equals(MergeStatus.value( viewBean.getMergeStatus() ));

			conflictCheckResource.setResourcePath( viewBean.getFileName() )
				.setChangeProperties( changeProperties )
				.setModuleNm( viewBean.getModuleName() )
				.setMergeStatus( MergeStatus.value( viewBean.getMergeStatus()) )
				.setDiff( diff )
				.setBinary( AmBusinessFileUtils.isBinary(viewBean.getFileName(), ucfExtensionMap) );

			conflictCheckResourceList.add( conflictCheckResource );

		}

		dest.setConflictCheckResultViews( conflictCheckResourceList );
	}
}
