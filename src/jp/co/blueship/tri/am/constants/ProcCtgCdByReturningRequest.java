package jp.co.blueship.tri.am.constants;

import jp.co.blueship.tri.fw.sm.contants.SmProcCtgCd;

/**
 * プレコンパイルの処理状況を表す、アクションＩＤごとにユニークな識別ＩＤです。
 * <br>このＩＤは、処理の順番を定義したものではない点に注意して下さい。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ProcCtgCdByReturningRequest {

	/**
	 * 二重返却資産チェック処理中
	 */
	public static final String DUPLICATION_FILE_CHECK = SmProcCtgCd.DUPLICATION_FILE_CHECK.getCode();
	/**
	 * 返却フォルダ内部コピー処理中
	 */
	public static final String RETURN_FILE_COPY = SmProcCtgCd.RETURN_FILE_COPY.getCode();
	/**
	 * 拡張子チェック処理中
	 */
	public static final String EXTENSION_CHECK = SmProcCtgCd.EXTENSION_CHECK.getCode();
	/**
	 * コンパイル構文チェック
	 */
	public static final String COMPILE_SYNTAX_CHECK = SmProcCtgCd.COMPILE_SYNTAX_CHECK.getCode();
	/**
	 * 文字コードチェック
	 */
	public static final String CHARSET_CHECK = SmProcCtgCd.CHARSET_CHECK.getCode();
	/**
	 * 改行コードチェック
	 */
	public static final String LINEFEED_CHECK = SmProcCtgCd.LINEFEED_CHECK.getCode();
	/**
	 * 外部スクリプト呼び出し
	 */
	public static final String CALL_SCRIPT = SmProcCtgCd.CALL_SCRIPT.getCode();
}
