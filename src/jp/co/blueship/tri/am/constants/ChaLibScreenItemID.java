package jp.co.blueship.tri.am.constants;

/**
 *
 * 変更管理画面項目ID
 *
 */
public class ChaLibScreenItemID {

	/**
	 * ロット作成
	 */
	public enum FlowChaLibLotEntryCheck {
		// ロット概要
		LOT_SUMMARY("lotSummary"),
		// ロット内容
		LOT_CONTENT("lotContent"),
		// ベースラインタグ
		BASELINE_TAG("baseLineTag");

		private String itemName;

		FlowChaLibLotEntryCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * ロットクローズ
	 */
	public enum FlowChaLibLotCloseCheck {
		// クローズコメント
		CLOSE_COMMENT("closeComment");

		private String itemName;

		FlowChaLibLotCloseCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * ロット取消
	 */
	public enum FlowChaLibLotCancelCheck {
		// 取消コメント
		CANCEL_COMMENT("delComment");

		private String itemName;

		FlowChaLibLotCancelCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理登録／編集
	 */
	public enum FlowChaLibPjtEntryCheck {
		// 変更要因番号
		CHANGE_CAUSE_NO("changeCauseNo"),
		// 変更要因分類
		CHANGE_CAUSE_CLASSIFY("changeCauseClassify"),
		// 変更管理概要
		SUMMARY("summary"),
		// 変更管理内容
		CONTENT("content");

		private String itemName;

		FlowChaLibPjtEntryCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理取消
	 */
	public enum FlowChaLibPjtCancelCheck {
		// 取消コメント
		CANCEL_COMMENT("delComment");

		private String itemName;

		FlowChaLibPjtCancelCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理クローズ
	 */
	public enum FlowChaLibPjtCloseCheck {
		// クローズコメント
		CLOSE_COMMENT("closeComment");

		private String itemName;

		FlowChaLibPjtCloseCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理テスト完了
	 */
	public enum FlowChaLibPjtTestCompleteCheck {
		// クローズコメント
		TEST_COMPLETE_COMMENT("testCompleteComment");

		private String itemName;

		FlowChaLibPjtTestCompleteCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理承認
	 */
	public enum FlowChaLibPjtApproveCheck {
		// 承認コメント
		APPROVE_COMMENT("approveComment");

		private String itemName;

		FlowChaLibPjtApproveCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理承認取消
	 */
	public enum FlowChaLibPjtApproveCancelCheck {
		// 承認取消コメント
		APPROVE_CANCEL_COMMENT("approveCancelComment");

		private String itemName;

		FlowChaLibPjtApproveCancelCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 変更管理承認却下
	 */
	public enum FlowChaLibPjtApproveRejectCheck {
		// 承認却下コメント
		APPROVE_REJECT_COMMENT("approveRejectComment");

		private String itemName;

		FlowChaLibPjtApproveRejectCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 貸出申請 登録／編集
	 */
	public enum FlowChaLibLendEntryCheck {
		//概要
		SUMMARY("summary"),
		//内容
		CONTENT("content");

		private String itemName;

		FlowChaLibLendEntryCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 原本削除申請 登録
	 */
	public enum FlowChaLibMasterDelEntryCheck {
		//概要
		SUMMARY("summary"),
		//内容
		CONTENT("content");

		private String itemName;

		FlowChaLibMasterDelEntryCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * コンフリクトチェック
	 */
	public enum FlowChaLibConflictCheckCheck {
		// コンフリクトコメント
		CONFLICT_COMMENT("conflictComment");

		private String itemName;

		FlowChaLibConflictCheckCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * マージコミット
	 */
	public enum FlowChaLibMergeCommitCheck {
		// コンフリクトコメント
		MERGE_COMMENT("mergeComment");

		private String itemName;

		FlowChaLibMergeCommitCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

}

