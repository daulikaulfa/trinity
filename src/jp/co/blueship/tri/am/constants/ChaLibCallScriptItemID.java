package jp.co.blueship.tri.am.constants;


/**
 *
 * 外部ツール呼び出し機能項目ID
 *
 */
public class ChaLibCallScriptItemID {

	/**
	 * 返却申請時概要設定
	 */
	public enum SummaryAtReturnApply {
		// 機能のON/OFF
		DO_EXEC("doExec"),
		// スクリプトファイルの格納パス
		SCRIPT("script"),
		// 実行ディレクトリ
		DIR("dir");

		private String itemName;

		SummaryAtReturnApply( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * 返却申請時終了コード設定
	 */
	public enum ErrCdAtReturnApply {
		// その他F
		OTHER("other");

		private String itemName;

		ErrCdAtReturnApply( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

}

