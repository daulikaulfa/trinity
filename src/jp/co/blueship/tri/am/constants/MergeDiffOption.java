package jp.co.blueship.tri.am.constants;

/**
 * Enumeration type of Diff option.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public enum MergeDiffOption {
	none(""),
	/**
	 * Master
	 */
	Master("headResources"),
	/**
	 * Branche
	 */
	Branche("lotResources"),
	/**
	 * Branche
	 */
	MergeResult("mergeResult"),
	;

	private String value = null;

	private MergeDiffOption( String value ) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static MergeDiffOption value( String value ) {
		for ( MergeDiffOption define : values() ) {
			if ( define.value().equals( value ) )
				return define;
		}

		return none;
	}
}
