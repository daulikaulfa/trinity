package jp.co.blueship.tri.am.constants;

import java.io.Serializable;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * Enumeration type of merge status.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum MergeStatus implements Serializable {
	none(""),
	/**
	 * In a status operation denotes that the item in the Working Copy being currently processed has local modifications.
	 * <br>
	 * <br>衝突なし、マージ済み（ロットのみの更新を含む）
	 */
	Modified("MODIFIED"),
	/**
	 * - STATUS_CONFLICTED: In a status operation denotes that the item in the Working Copy being currently processed is in a conflict state (local changes overlap those that came from the repository). The conflicting overlaps need to be manually resolved.
	 * <br>- STATUS_MISSING: In a status operation denotes that the item in the Working Copy being currently processed is under version control but is missing - for example, removed from the filesystem with a non-SVN, non-SVNKit or any other SVN non-compatible delete command).
	 * <br>- CONFLICTED_UNRESOLVED: Denotes that the conflict state on the item is still unresolved. For example, it can be set when trying to merge into a file that is in conflict with the repository.
	 * <br>
	 * <br>コンフリクト発生
	 */
	Conflicted("CONFLICTED"),
	/**
	 * In a status operation denotes that the item in the Working Copy being currently processed is scheduled for deletion from the repository.
	 * <br>
	 * <br>削除ファイル
	 */
	Deleted("DELETED"),
	/**
	 * In a status operation denotes that the item in the Working Copy being currently processed is scheduled for addition to the repository.
	 * <br>
	 * <br>新規追加（Subversionのみ）
	 */
	Added("ADDED");

	private String statusId = null;

	private MergeStatus( String statusId) {
		this.statusId = statusId;
	}

	public boolean equals( String value ) {
		MergeStatus type = value( value );

		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String getStatusId() {
		return this.statusId;
	}

	public static MergeStatus value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return none;
		}

		for ( MergeStatus value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return none;
	}

}
