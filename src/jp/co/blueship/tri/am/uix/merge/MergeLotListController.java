package jp.co.blueship.tri.am.uix.merge;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/merge")
public class MergeLotListController extends TriControllerSupport<FlowMergeLotListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmMergeLotListService;
	}

	@Override
	protected FlowMergeLotListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeLotListServiceBean bean = new FlowMergeLotListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowMergeLotListServiceBean bean, TriModel model ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"","/","/list"})
	public String index(FlowMergeLotListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		List<String> flashMessages = new ArrayList<String>();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if( model.isRedirect() && !bean.getMessageInfo().isEmptyFlashMessages() ) {
				flashMessages = bean.getMessageInfo().getFlashMessages();
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination", bean.getPage())
			.addAttribute("view", TriView.MergeLotList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "mergeSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);

		if( flashMessages.size() > 0 && bean.getMessageInfo().isEmptyFlashMessages() ){
			bean.getMessageInfo().addFlashMessages( flashMessages );
		}

		return view;
	}

	private void mapping(FlowMergeLotListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if(RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		} else if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			String selectedPageNo = requestInfo.getParameter("selectedPageNo");
			if(TriStringUtils.isNotEmpty(selectedPageNo)) {
				searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
			} else {
				searchCondition.setSelectedPageNo(1);
			}
		}
	}

}
