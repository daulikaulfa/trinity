package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/merge/commit/history/print")
public class PrintMergeHistoryDetailsController extends TriControllerSupport<FlowMergeHistoryDetailsServiceBean>  {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintMergeHistoryDetailsService;
	}

	@Override
	protected FlowMergeHistoryDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeHistoryDetailsServiceBean bean = new FlowMergeHistoryDetailsServiceBean();
		return bean;
	}



	@RequestMapping
	public String print(FlowMergeHistoryDetailsServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintMergeHistory.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping(FlowMergeHistoryDetailsServiceBean bean, TriModel model){
	}
}
