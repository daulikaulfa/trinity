package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeHistoryDetailsServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/merge/commit")
public class MergeHistoryDetailsController extends TriControllerSupport<FlowMergeHistoryDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmMergeHistoryDetailsService;
	}

	@Override
	protected FlowMergeHistoryDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeHistoryDetailsServiceBean bean = new FlowMergeHistoryDetailsServiceBean();
		return bean;
	}

	@RequestMapping("/history")
	public String merge(FlowMergeHistoryDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination", bean.getPage())
			.addAttribute("view", TriView.MergeHistory.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "mergeSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowMergeHistoryDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		String selectedPageNo = requestInfo.getParameter("selectedPageNo");
		if(TriStringUtils.isNotEmpty(selectedPageNo)) {
			searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
		} else {
			searchCondition.setSelectedPageNo(1);
		}

	}

}
