package jp.co.blueship.tri.am.uix.merge;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.constants.MergeDiffOption;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeResourceFileComparisonServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeResourceFileComparisonServiceBean.RequestParam;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 */
@Controller
@RequestMapping("/merge/conflictcheck/result")
public class MergeResourceFileComparisonController extends TriControllerSupport<FlowMergeResourceFileComparisonServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmMergeResourceFileComparisonService;
	}

	@Override
	protected FlowMergeResourceFileComparisonServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeResourceFileComparisonServiceBean bean = new FlowMergeResourceFileComparisonServiceBean();
		return bean;
	}

	@RequestMapping("/diff")
	public String diff(FlowMergeResourceFileComparisonServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			if( !this.mapping(bean,model) ) return view;
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.ConflictCheckDifferences.value() );
		model.getModel().addAttribute("selectedMenu", "changeMenu");
		model.getModel().addAttribute("selectedSubMenu", "mergeSubmenu");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}


	private boolean mapping(FlowMergeResourceFileComparisonServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();

		MergeDiffOption src = MergeDiffOption.none;
		MergeDiffOption dst = MergeDiffOption.none;


		if(RequestType.init.equals(param.getRequestType())){
			if ( requestInfo.getParameter("pjtId") != null ){
				param.setSelectedAreqId		  ( requestInfo.getParameter("pjtId") );
			}
			String moduleNm = requestInfo.getParameter("moduleNm");
			String fileNm	= requestInfo.getParameter("resourcePath");
			param.setSelectedResourcePath ( TriStringUtils.linkPathBySlash(moduleNm,fileNm) );

			String conflictFlg = requestInfo.getParameter("conflict");

			if(conflictFlg.equals("false")){
				List<MergeDiffOption> srcViews =  param.getInputInfo().getSrcViews();
					srcViews.add(MergeDiffOption.Master);
					srcViews.add(MergeDiffOption.Branche);
					srcViews.add(MergeDiffOption.MergeResult);
				param.getInputInfo().setSrcViews(srcViews);

				List<MergeDiffOption> dstViews =  param.getInputInfo().getDestViews();
					dstViews.add(MergeDiffOption.Master);
					dstViews.add(MergeDiffOption.Branche);
					dstViews.add(MergeDiffOption.MergeResult);
				param.getInputInfo().setDestViews(dstViews);
			}

			src = MergeDiffOption.Master;
			dst = MergeDiffOption.Branche;
		}


		if(RequestType.onChange.equals(param.getRequestType())){
			src = MergeDiffOption.value( requestInfo.getParameter("srcVer") );
			dst = MergeDiffOption.value( requestInfo.getParameter("dstVer") );
		}

		//err check
		if(src.equals(MergeDiffOption.none) || dst.equals(MergeDiffOption.none) || src.equals(dst)){
			return false;
		}

		param.getInputInfo()
			.setSrc ( src )
			.setDest( dst );

		bean.setParam(param);

		return true;
	}
}