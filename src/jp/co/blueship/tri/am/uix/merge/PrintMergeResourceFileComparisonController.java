package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.constants.MergeDiffOption;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeResourceFileComparisonServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeResourceFileComparisonServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
@Controller
@RequestMapping("/merge/conflictcheck/result")
public class PrintMergeResourceFileComparisonController extends TriControllerSupport<FlowMergeResourceFileComparisonServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintMergeResourceFileComparisonService;
	}

	@Override
	protected FlowMergeResourceFileComparisonServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeResourceFileComparisonServiceBean bean = new FlowMergeResourceFileComparisonServiceBean();
		return bean;
	}

	@RequestMapping("/diff/print")
	public String deff(FlowMergeResourceFileComparisonServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			if( !this.mapping(bean,model) ) return view;
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.PrintConflictCheckDifferences.value() );
		model.getModel().addAttribute("result", bean);
		
		setPrev(model);
		return view;
	}


	private boolean mapping(FlowMergeResourceFileComparisonServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();

		if(RequestType.init.equals(param.getRequestType())){
			if ( requestInfo.getParameter("pjtId") != null ){
				param.setSelectedAreqId( requestInfo.getParameter("pjtId") );
			}
			param.setSelectedResourcePath ( requestInfo.getParameter("selectedResourcePath") );
		}

		MergeDiffOption	src = MergeDiffOption.value( requestInfo.getParameter("printSrcVer") );
		MergeDiffOption	dst = MergeDiffOption.value( requestInfo.getParameter("printDstVer") );

		//err check
		if(src.equals(MergeDiffOption.none) || dst.equals(MergeDiffOption.none) || src.equals(dst)){
			return false;
		}

		param.getInputInfo()
			.setSrc ( src )
			.setDest( dst );

		bean.setParam(param);

		return true;
	}
}