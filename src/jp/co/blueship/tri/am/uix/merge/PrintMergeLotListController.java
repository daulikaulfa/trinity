package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeLotListServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/merge/list/print")
public class PrintMergeLotListController extends TriControllerSupport<FlowMergeLotListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintMergeLotListService;
	}

	@Override
	protected FlowMergeLotListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeLotListServiceBean bean = new FlowMergeLotListServiceBean();
		return bean;
	}


	@RequestMapping
	public String print(FlowMergeLotListServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintMergeLotList.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping(FlowMergeLotListServiceBean bean, TriModel model){
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		bean.getParam().setLinesPerPage( 0 );
	}
}
