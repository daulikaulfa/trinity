package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckResourceFileResultsServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckResourceFileResultsServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/merge/conflictcheck/result/print")
public class PrintConflictCheckResourceFileResultController
		extends TriControllerSupport<FlowConflictCheckResourceFileResultsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintConflictCheckResourceFileResultsService;
	}

	@Override
	protected FlowConflictCheckResourceFileResultsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowConflictCheckResourceFileResultsServiceBean bean = new FlowConflictCheckResourceFileResultsServiceBean();
		return bean;
	}

	@RequestMapping
	public String check(FlowConflictCheckResourceFileResultsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintConflictCheckResults.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "mergeSubmenu")
			.addAttribute("result", bean)
			.addAttribute("selectedLotId", this.getSessionSelectedLot(model.getSessionInfo(), bean))
		;

		setPrev(model);
		return TriTemplateView.PrintTemplate.value();

	}

	private void mapping(FlowConflictCheckResourceFileResultsServiceBean bean, TriModel model) {
		RequestParam param = bean.getParam();
		param.setSelectedLotId(this.getSessionSelectedLot(model.getSessionInfo(), bean));
	}

}
