package jp.co.blueship.tri.am.uix.merge;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean;
import jp.co.blueship.tri.am.domainx.merge.dto.FlowConflictCheckServiceBean.RequestParam;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/merge/conflictcheck")
public class ConflictCheckController extends TriControllerSupport<FlowConflictCheckServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmConflictCheckService;
	}

	@Override
	protected FlowConflictCheckServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowConflictCheckServiceBean bean = new FlowConflictCheckServiceBean();
		return bean;
	}

	@RequestMapping
	public String check(FlowConflictCheckServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if(bean.getResult().isCompleted()) {
				view = "redirect:/merge/redirect";
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ConflictCheck.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "mergeSubmenu")
			.addAttribute("result", bean)
		;
        setPrev(model);
		return view;
	}


	@RequestMapping("/validate")
	public String checkComment(FlowConflictCheckServiceBean bean, TriModel model) {
		String view  = "common/Comment::conflictCheckComment";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowConflictCheckServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if ( RequestType.submitChanges.equals(param.getRequestType())
			|| RequestType.validate.equals(param.getRequestType()) ) {
			String selectedLotVerTag = "";
			List<String> selectedLotVerTagValues = FluentList
					.from( requestInfo.getParameterValues("selectedLotVerTag") ).asList();
			for (String selectedLotVerTagValue : selectedLotVerTagValues) {
				if( !TriStringUtils.isEmpty(selectedLotVerTagValue)) {
					selectedLotVerTag = selectedLotVerTagValue;
					break;
				}
			}
			
			param.getInputInfo()
				.setSelectedLotVerTag(selectedLotVerTag)
				.setComment(requestInfo.getParameter("reason"))
			;
			if ( requestInfo.getParameter("submitAgreement") != null ){
				param.getInputInfo().setSubmitAgreement(Boolean.valueOf(requestInfo.getParameter("submitAgreement")));	
			}
			
		}
	}

}
