package jp.co.blueship.tri.am.uix.merge;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.merge.dto.FlowMergeServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/merge/commit")
public class MergeController extends TriControllerSupport<FlowMergeServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmMergeService;
	}

	@Override
	protected FlowMergeServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowMergeServiceBean bean = new FlowMergeServiceBean();
		return bean;
	}

	@RequestMapping
	public String merge(FlowMergeServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if(bean.getResult().isCompleted()) {
				view = "redirect:/merge/redirect";
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.Merge.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "mergeSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/validate")
	public String mergeComment(FlowMergeServiceBean bean, TriModel model) {
		String view = "common/Comment::mergeCommit";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowMergeServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
	}
}
