package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */

@Controller
@RequestMapping("/lot/resource")
public class LotResourceListController extends TriControllerSupport<FlowLotResourceListServiceBean> {
	@RequestMapping("/list")
	public String index(FlowLotResourceListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotResourceListServiceBean.RequestParam param = bean.getParam();

		String view = TriTemplateView.MainTemplate.value();

		try {
			param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute( getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute( "result", bean )
			.addAttribute( "selectedMenu" , "lotMenu" )
			.addAttribute( "selectedSubMenu" , "lotResourceListSubmenu" )
			.addAttribute( "view", TriView.LotResourceViewList.value() )
		;
		String json = bean.getResourceFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		setPrev(model);
		return view;
	}
	
	private void mapping(FlowLotResourceListServiceBean bean, TriModel model ) {
		FlowLotResourceListServiceBean.RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		if (RequestType.init.equals(param.getRequestType())) {
		}

		if (RequestType.onChange.equals(param.getRequestType())) {
			bean.getParam().setRequestOption(RequestOption.value( requestInfo.getParameter("requestOption") ));
			ResourceSelection resourceSelection = bean.getParam().getResourceSelection();
			resourceSelection.setPath( requestInfo.getParameter("path") );
			
			String resourceRequestType = requestInfo.getParameter("resourceRequestType");
			if( ResourceSelection.ResourceRequestType.selectFolder.equals( resourceRequestType ) ){	
				resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
			} 
			else if( ResourceSelection.ResourceRequestType.openFolder.equals( resourceRequestType ) ) {
				resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);
			} 
			else if( ResourceSelection.ResourceRequestType.closeFolder.equals( resourceRequestType ) ) {
				resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
			} 
			else if( ResourceSelection.ResourceRequestType.openAllFolder.equals( resourceRequestType ) ) {
				resourceSelection.setType(ResourceSelection.ResourceRequestType.openAllFolder);
			} 
			else if( ResourceSelection.ResourceRequestType.closeAllFolder.equals( resourceRequestType ) ) {
				resourceSelection.setType(ResourceSelection.ResourceRequestType.closeAllFolder);
			}
		}
	}

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmLotResourceListService;
	}

	@Override
	protected FlowLotResourceListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotResourceListServiceBean bean = new FlowLotResourceListServiceBean();
		return bean;
	}
}
