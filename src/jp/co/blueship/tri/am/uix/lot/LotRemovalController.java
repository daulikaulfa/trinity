package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Sam
 *
 */
@Controller
@RequestMapping("/lot/remove")
public class LotRemovalController extends TriControllerSupport<FlowLotRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmLotRemovalService;
	}

	@Override
	protected FlowLotRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotRemovalServiceBean bean = new FlowLotRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String removal(FlowLotRemovalServiceBean bean,
            TriModel model) {
		String view = "redirect:/dashboard/redirect";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		model.getModel().addAttribute("result", bean);

		return view;
	}

	@RequestMapping(value = {"/validate"} )
	public String removalComment(FlowLotRemovalServiceBean bean, TriModel model) {
		String view =  "common/Comment::lotRemoval";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowLotRemovalServiceBean bean, TriModel model) {

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo RequestInfo = model.getRequestInfo();
		FlowLotRemovalServiceBean.RequestParam param = bean.getParam();
		param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if (RequestType.validate.equals(param.getRequestType())) {
		}

		if(RequestType.submitChanges.equals(param.getRequestType())){
			param.getInputInfo().setComment(RequestInfo.getParameter("reason"));
		}
	}
}
