package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection.ResourceRequestType;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceListServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/lot/resource/list/print")
public class PrintLotResourceListController extends TriControllerSupport<FlowLotResourceListServiceBean> {
	@RequestMapping
	public String index(FlowLotResourceListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotResourceListServiceBean.RequestParam param = bean.getParam();

		String view = TriTemplateView.PrintTemplate.value();

		try {
			param.setSelectedLotId	( this.getSessionSelectedLot(sesInfo, bean) )
				 .setRequestType	( RequestType.init )
				 ;
			this.execute( getServiceId(), bean , model );

			param.setRequestOption	( RequestOption.selectResource )
				 .setRequestType	( RequestType.onChange )
				 ;

			param.getResourceSelection().setType( ResourceRequestType.openAllFolder );
			this.execute( getServiceId(), bean , model );

			param.getResourceSelection().setPath( model.getRequestInfo().getParameter("path") )
										.setType( ResourceRequestType.selectFolder )
										;
			this.execute( getServiceId(), bean , model );


		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute( "result", bean )
			.addAttribute( "selectedMenu" , "lotMenu" )
			.addAttribute( "selectedSubMenu" , "lotResourceListSubmenu" )
			.addAttribute( "view", TriView.PrintLotResourceViewList.value() )
		;
		setPrev(model);
		return view;
	}

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintLotResourceListService;
	}

	@Override
	protected FlowLotResourceListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotResourceListServiceBean bean = new FlowLotResourceListServiceBean();
		return bean;
	}
}
