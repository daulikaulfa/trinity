package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCloseServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;

@Controller
@RequestMapping("/lot/close")
public class LotCloseController extends TriControllerSupport<FlowLotCloseServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmLotCloseService;
	}
	@Override
	protected FlowLotCloseServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotCloseServiceBean bean = new FlowLotCloseServiceBean();
		return bean;
	}
	@RequestMapping
	public String close( FlowLotCloseServiceBean bean,
			TriModel model) {
		String view = "redirect:/dashboard/redirect";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		bean.getParam().setRequestType(RequestType.init);
		model.getRedirectAttributes().addFlashAttribute("result", bean);
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping(value = {"/validate"} )
	public String closeComment(FlowLotCloseServiceBean bean, TriModel model) {
		String view = "common/Comment::lotClose";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}


	private void mapping(FlowLotCloseServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotCloseServiceBean.RequestParam param = bean.getParam();

		param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if (RequestType.validate.equals(param.getRequestType())) {
		}

		if (RequestType.submitChanges.equals(param.getRequestType())) {
			param.getInputInfo().setComment(requestInfo.getParameter("reason"));
		}
	}
}
