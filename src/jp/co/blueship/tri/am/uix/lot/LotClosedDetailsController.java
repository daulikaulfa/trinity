package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotDetailsViewBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ThemeColor;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping(value = { "/lot/closed" })
public class LotClosedDetailsController extends TriControllerSupport<FlowLotDetailsServiceBean> {
	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmLotDetailsService;
	}

	@Override
	protected FlowLotDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotDetailsServiceBean bean = new FlowLotDetailsServiceBean();
		return bean;
	}

	@RequestMapping("/details")
	public String index(FlowLotDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotDetailsServiceBean.RequestParam param = bean.getParam();

		String view = TriTemplateView.MainTemplate.value();

		try {

			Integer windowId = bean.getHeader().getWindowsId();
			if (model.isRedirect() == true) {
				param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			} else {
				param.setSelectedLotId(requestInfo.getParameter("lotId"));
			}

			this.execute(getServiceId(), bean, model);

			LotDetailsViewBean detailView = bean.getDetailsView();
			saveLotInfoToSession(sesInfo, windowId, param.getSelectedLotId(), detailView.getLotSubject(), detailView.getLotIconPath(), ThemeColor.id(detailView.getThemeColor()).value(), !detailView.isClosed());
			this.setHeader(bean, sesInfo, windowId);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.LotClosedDetails.value())
			.addAttribute("selectedMenu", "lotMenu")
			.addAttribute("selectedSubMenu", "lotSubmenu")
		;

		setPrev(model);
		return view;
	}
}
