package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author thang.vu
 */
@Controller
@RequestMapping(value = { "/lot/details/print" })
public class PrintLotDetailsController extends TriControllerSupport<FlowLotDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintLotDetailsService;
	}

	@Override
	protected FlowLotDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotDetailsServiceBean bean = new FlowLotDetailsServiceBean();
		return bean;
	}

	private void mapping(FlowLotDetailsServiceBean bean, TriModel model) {
		FlowLotDetailsServiceBean.RequestParam param = bean.getParam();

		if (RequestType.init.equals(param.getRequestType())) {

		}

		if (RequestType.submitChanges.equals(param.getRequestType())) {

		}
	}

	@RequestMapping
	public String printLotDetails(FlowLotDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotDetailsServiceBean.RequestParam param = bean.getParam();

		try {
			if (model.isRedirect() == true) {
				param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			} else {
				param.setSelectedLotId(requestInfo.getParameter("lotId"));
			}
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			Integer windowId = bean.getHeader().getWindowsId();

			this.setHeader(bean, sesInfo, windowId);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("selectedMenu", "lotMenu")
			.addAttribute("selectedSubMenu", "lotSubmenu")
			.addAttribute("view", TriView.PrintLotDetails.value())
		;

		return view;
	}
}
