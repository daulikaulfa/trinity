package jp.co.blueship.tri.am.uix.lot;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.AuthorizedGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.BuildEnvironment;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.MailRecipientGroup;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.Module;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.LotEditInputBean.ReleaseEnvironment;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotCreationServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
@Controller
@RequestMapping("/lot")
public class LotCreationController extends TriControllerSupport<FlowLotCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmLotCreationService;
	}

	@Override
	protected FlowLotCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotCreationServiceBean bean = new FlowLotCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String index(FlowLotCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.ProjectTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				view = "redirect:/dashboard/redirect";
				model.getRedirectAttributes().addFlashAttribute("result", bean);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.LotCreation.value())
			;

		setPrev(model);
		return view;
	}

	private void mapping(FlowLotCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		LotEditInputBean info = bean.getParam().getInputInfo();

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			info
				.setLotSubject		( requestInfo.getParameter("subject") )
				.setLotSummary		( requestInfo.getParameter("summary") )
				.setLotContents		( requestInfo.getParameter("contents") )
				.setBaselineTag		( requestInfo.getParameter("baselineTagName") )
				.setUseMerge		( (null != requestInfo.getParameter("useMerge")) )
				.setPublicPath		( requestInfo.getParameter("publicPath") )
				.setPrivatePath		( requestInfo.getParameter("privatePath") )
				.setUseDefaultPublicPath
									( null != requestInfo.getParameter("isUseDefaultPublicPath") )
				.setUseDefaultPrivatePath
									( null != requestInfo.getParameter("isUseDefaultPrivatePath") )
				.setEnableUnauthorizedGroup
									( (null != requestInfo.getParameter("enableUnauthorizedGroup")) );

			{
				List<String> values = FluentList.from(requestInfo.getParameterValues("selectedAuthorizedGroups"))
						.asList();
				for (AuthorizedGroup grp : info.getAuthorizedGroups()) {
					grp.setSelected(values.contains(grp.getGroupId()));
				}
			}
			{
				List<String> values = FluentList.from(requestInfo.getParameterValues("selectedRecipientGroups"))
						.asList();
				for (MailRecipientGroup grp : info.getMailRecipientGroups()) {
					grp.setSelected(values.contains(grp.getGroupId()));
				}
			}
			{
				List<String> values = FluentList.from(requestInfo.getParameterValues("selectedModules")).asList();
				for (Module module : info.getModules()) {
					module.setSelected(values.contains(module.getModuleNm()));
				}
			}
			{
				List<String> buildValues = FluentList.from(requestInfo.getParameterValues("selectedBuildEnvs"))
						.asList();
				List<String> FullBuildValues = FluentList.from(requestInfo.getParameterValues("selectedFullBuildEnvs"))
						.asList();
				for (BuildEnvironment env : info.getBuildEnvs()) {
					env.setSelectedBuild(buildValues.contains(env.getEnvId()));
					env.setSelectedFullBuild(FullBuildValues.contains(env.getEnvId()));
				}
			}
			{
				List<String> values = FluentList.from(requestInfo.getParameterValues("selectedReleaseEnvs")).asList();
				for (ReleaseEnvironment env : info.getReleaseEnvs()) {
					env.setSelected(values.contains(env.getEnvId()));
				}
			}
		}
	}
}
