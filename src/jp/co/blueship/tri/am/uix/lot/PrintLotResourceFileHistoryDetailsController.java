package jp.co.blueship.tri.am.uix.lot;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.lot.dto.FlowLotResourceFileHistoryDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * 
 * @version V4.01.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/lot/resource/detail/print")
public class PrintLotResourceFileHistoryDetailsController extends TriControllerSupport<FlowLotResourceFileHistoryDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintLotResourceFileHistoryDetailsService;
	}

	@Override
	protected FlowLotResourceFileHistoryDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowLotResourceFileHistoryDetailsServiceBean bean = new FlowLotResourceFileHistoryDetailsServiceBean();
		return bean;
	}
	
	@RequestMapping
	public String index(FlowLotResourceFileHistoryDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowLotResourceFileHistoryDetailsServiceBean.RequestParam param = bean.getParam();

		try {
			param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute( getServiceId(), bean , model );
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute( "result", bean )
			.addAttribute( "selectedMenu" , "lotMenu" )
			.addAttribute( "selectedSubMenu" , "lotResourceListSubmenu" )
			.addAttribute( "view", TriView.PrintLotResourceFileHistoryDetail.value() )
		;
		setPrev(model);
		return view;
	}
	
	private void mapping( FlowLotResourceFileHistoryDetailsServiceBean bean, TriModel model ) {
		FlowLotResourceFileHistoryDetailsServiceBean.RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		param.setSelectedResourcePath(requestInfo.getParameter("resourcePath") );
	}

}
