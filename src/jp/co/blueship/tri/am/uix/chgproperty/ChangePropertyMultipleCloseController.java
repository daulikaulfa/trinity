package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceMultipleCloseBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceMultipleCloseBean.ChangePropertyCloseInput;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */

@Controller
@RequestMapping("/chgproperty/multipleclose")
public class ChangePropertyMultipleCloseController extends TriControllerSupport<FlowChangePropertyCloseServiceMultipleCloseBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyCloseServiceMultipleClose;
	}

	@Override
	protected FlowChangePropertyCloseServiceMultipleCloseBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyCloseServiceMultipleCloseBean bean = new FlowChangePropertyCloseServiceMultipleCloseBean();
		return bean;
	}

	@RequestMapping
	public String MultipleClose(FlowChangePropertyCloseServiceMultipleCloseBean bean, TriModel model){
		String view = "redirect:/chgproperty/refresh";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	@RequestMapping(value = "/validate")
	public String multiplecloseComment(FlowChangePropertyCloseServiceMultipleCloseBean bean, TriModel model) {
		String view = "common/Comment::multiplecloseComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);
		return view;
	}

	private void mapping(FlowChangePropertyCloseServiceMultipleCloseBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		FlowChangePropertyCloseServiceMultipleCloseBean.RequestParam param = bean.getParam();
		ChangePropertyCloseInput info = param.getInputInfo();

		param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if ( requestInfo.getParameterValues("selectedPjtIds") != null){
			param.setSelectedPjtIds(requestInfo.getParameterValues("selectedPjtIds"));
		}

		if (RequestType.validate.equals(param.getRequestType())) {
		}

		if (RequestType.submitChanges.equals(param.getRequestType())) {
			info.setComment(requestInfo.getParameter("reason"));
		}
	}
}
