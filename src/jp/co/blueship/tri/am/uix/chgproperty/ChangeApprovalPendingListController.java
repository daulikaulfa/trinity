package jp.co.blueship.tri.am.uix.chgproperty;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.ListSelection;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
@Controller
@RequestMapping("/chgproperty/pending")
public class ChangeApprovalPendingListController extends TriControllerSupport<FlowChangeApprovalPendingListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalPendingListService;
	}

	@Override
	protected FlowChangeApprovalPendingListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingListServiceBean bean = new FlowChangeApprovalPendingListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowChangeApprovalPendingListServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.init);
		return listView( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refresh")
	public String refresh (FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("tabName", "Pending")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/","/list"})
	public String listView (FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("tabName", "Pending")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/error"})
	public String error(FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		return listView(bean, model.setRedirect(true));
	}

	@RequestMapping(value = "/search")
	public String search(FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubMenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("tabName", "Pending")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowChangeApprovalPendingListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter(bean.getParam().getSearchCondition());

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch ( Exception e ) {
			ExceptionUtils.printStackTrace(e);
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowChangeApprovalPendingListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowChangeApprovalPendingListServiceBean.SearchCondition condition = gson.fromJson(json, FlowChangeApprovalPendingListServiceBean.SearchCondition.class);

			bean.getParam()
				.setLinesPerPage(5)
				.setSearchCondition(condition)
				.getSearchCondition().setSelectedPageNo(1)
			;

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination",bean.getPage())
			.addAttribute("selectedTabId"  , "0")
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "approvalSubmenu")
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}

	private void mapping (FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if(requestInfo.getParameter("pending_selectedPageNo") != null){
				searchCondition.setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("pending_selectedPageNo")));
			}else{
				searchCondition.setSelectedPageNo(1);
			}

			searchCondition
				.setCtgId(requestInfo.getParameter("pending_condCategory"))
				.setMstoneId(requestInfo.getParameter("pending_condMilestone"))
				.setKeyword(requestInfo.getParameter("pending_searchKeyword"))
			;

			List<String> values = FluentList.from(requestInfo.getParameterValues("selectedPjtIds")).asList();
			ListSelection listSelection = bean.getParam().getListSelection();
			listSelection.setSelectedIds(values.toArray(new String[0]));
		}
	}
}
