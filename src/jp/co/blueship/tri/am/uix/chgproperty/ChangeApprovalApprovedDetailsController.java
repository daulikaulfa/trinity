package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedDetailsServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/chgproperty/approved")
public class ChangeApprovalApprovedDetailsController
		extends TriControllerSupport<FlowChangeApprovedDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovedDetailsService;
	}

	@Override
	protected FlowChangeApprovedDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovedDetailsServiceBean bean = new FlowChangeApprovedDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = "/details")
	public String changeApprovedDetails (FlowChangeApprovedDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.ChangeRequestApprovalApprovedDetails.value());
		model.getModel().addAttribute("selectedMenu", "changeMenu");
		model.getModel().addAttribute("selectedSubMenu", "approvalSubmenu");
		model.getModel().addAttribute("result", bean);

		setPrev(model);

		return view;
	}

	private void mapping(FlowChangeApprovedDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("pjtId") != null ){
			param.setSelectedPjtId(requestInfo.getParameter("pjtId"));
		}
	}
}
