package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * Provide the following backend services.
 * <br> - Details
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/chgproperty/pending/details/print")
public class PrintChangeApprovalPendingDetailsController
	extends TriControllerSupport<FlowChangeApprovalPendingDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintChangeApprovalPendingDetailsService;
	}

	@Override
	protected FlowChangeApprovalPendingDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingDetailsServiceBean bean = new FlowChangeApprovalPendingDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String printDetails (FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintChangeApprovalPendingDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping (FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedPjtId(requestInfo.getParameter("pjtId"));
	}
}
