package jp.co.blueship.tri.am.uix.chgproperty;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceExportToFileBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceExportToFileBean.RequestParam;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
@Controller
@RequestMapping("/chgproperty/pending/export")
public class ChangeApprovalPendingListServiceExportToFileController
		extends TriControllerSupport<FlowChangeApprovalPendingListServiceExportToFileBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalPendingListServiceExportToFile;
	}


	@Override
	protected FlowChangeApprovalPendingListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingListServiceExportToFileBean bean = new FlowChangeApprovalPendingListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowChangeApprovalPendingListServiceExportToFileBean bean, TriModel model) {

		String view = TriView.ChangeRequestApprovalList.value()+"::resultTable";
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/validate")
	public String validate(FlowChangeApprovalPendingListServiceExportToFileBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowChangeApprovalPendingListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		return;
	}

	private void insertMapping(FlowChangeApprovalPendingListServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));

		if ((RequestType.onChange).equals(param.getRequestType())) {
			searchCondition
				.setCtgId		( requestInfo.getParameter("pending_condCategory") )
				.setMstoneId	( requestInfo.getParameter("pending_condMilestone") )
				.setKeyword		( requestInfo.getParameter("pending_searchKeyword") )
			;

		}else if((RequestType.submitChanges).equals(param.getRequestType())) {
			String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
			ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
			param.setExportBean(fileBean);
			ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
			param.setSubmitOption(option);
		}
	}

}
