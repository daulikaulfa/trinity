package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyEditServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
@Controller
@RequestMapping("/chgproperty")
public class ChangePropertyEditController extends TriControllerSupport<FlowChangePropertyEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyEditService;
	}

	@Override
	protected FlowChangePropertyEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyEditServiceBean bean = new FlowChangePropertyEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowChangePropertyEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		return update( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/edit/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowChangePropertyEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		return update( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/edit")
	public String update(FlowChangePropertyEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model );
			if( bean.getResult().isCompleted() ){
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("pjtId", bean.getParam().getSelectedPjtId());
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.ChangePropertyEdit.value());
		model.getModel().addAttribute("selectedMenu"  , "changeMenu");
		model.getModel().addAttribute("selectedSubMenu"  , "changeSubmenu");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}

	private void mapping(FlowChangePropertyEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowChangePropertyEditServiceBean.RequestParam param = bean.getParam();
		ChangePropertyEditInputBean info = param.getInputInfo();

		LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + bean.getParam().getRequestOption());
		LogHandler.debug( TriLogFactory.getInstance(), "requestType:=" + bean.getParam().getRequestType());

		if (RequestType.init.equals(param.getRequestType())) {
			if ( requestInfo.getParameter("pjtId") != null )
			param.setSelectedPjtId(requestInfo.getParameter("pjtId"));
		}
		if (RequestType.submitChanges.equals(param.getRequestType())) {
			info.setReferenceId(requestInfo.getParameter("referenceId"))
				.setReferenceCategoryId(requestInfo.getParameter("referenceCategoryId"))
				.setSummary(requestInfo.getParameter("summary"))
				.setContents(requestInfo.getParameter("contents"))
				.setAssigneeId(requestInfo.getParameter("assigneeId"))
				.setCtgId(requestInfo.getParameter("ctgId"))
				.setMstoneId(requestInfo.getParameter("mstoneId"));
		}
	}

}
