package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingListServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/chgproperty/pending/list/print")
public class PrintChangeApprovalPendingListServiceController
	extends TriControllerSupport<FlowChangeApprovalPendingListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintChangeApprovalPendingListService;
	}

	@Override
	protected FlowChangeApprovalPendingListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingListServiceBean bean = new FlowChangeApprovalPendingListServiceBean();
		return bean;
	}

	@RequestMapping
	public String print(FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.printMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintChangeApprovalPendingList.value())
			.addAttribute("result", bean)
		;

		return view;
	}

	private void printMapping (FlowChangeApprovalPendingListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		bean.getParam().setLinesPerPage(0);

		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().getSearchCondition()
			.setSelectedPageNo	(1)
			.setCtgId			(requestInfo.getParameter("pending_condCategory"))
			.setMstoneId		(requestInfo.getParameter("pending_condMilestone"))
			.setKeyword			(requestInfo.getParameter("pending_searchKeyword"))
		;
	}
}
