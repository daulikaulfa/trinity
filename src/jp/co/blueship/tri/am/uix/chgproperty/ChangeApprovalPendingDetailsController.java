package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingDetailsServiceBean.RequestParam;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/chgproperty/pending")
public class ChangeApprovalPendingDetailsController
		extends TriControllerSupport<FlowChangeApprovalPendingDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalPendingDetailsService;
	}

	@Override
	protected FlowChangeApprovalPendingDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingDetailsServiceBean bean = new FlowChangeApprovalPendingDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = "/details/redirect")
	public String redirect(FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		bean.getParam().setRequestType(RequestType.init);
		return changeApprovalDetails(bean, model.setRedirect(true));
	}

	@RequestMapping(value = "/details/refresh")
	public String refresh(FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.init);
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalPendingApprovalDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/details")
	public String changeApprovalDetails(FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalPendingApprovalDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}

	private void mapping(FlowChangeApprovalPendingDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		RequestParam param = bean.getParam();
		String pjtId = requestInfo.getParameter("pjtId");

		if ( TriStringUtils.isNotEmpty( model.getModel().asMap().get("serviceId")) ) {
			String serviceId = model.getModel().asMap().get("serviceId").toString();

			if (TriStringUtils.isEquals(serviceId, "FlowChangeApprovalPendingReturnToSubmitterService")) {
				pjtId = model.getModel().asMap().get("pjtId").toString();
			}
		}
		if ( TriStringUtils.isNotEmpty(pjtId) ){
			param.setSelectedPjtId(pjtId);
		}


	}
}
