package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalServiceMultipleApprovalBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;


@Controller
@RequestMapping("/chgproperty/pending/multipleapproval")
public class ChangeApprovalServiceMultipleApprovalController extends TriControllerSupport<FlowChangeApprovalServiceMultipleApprovalBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalServiceMultipleApproval;
	}

	@Override
	protected FlowChangeApprovalServiceMultipleApprovalBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalServiceMultipleApprovalBean bean = new FlowChangeApprovalServiceMultipleApprovalBean();
		return bean;
	}

	@RequestMapping
	public String multipleApproval(FlowChangeApprovalServiceMultipleApprovalBean bean, TriModel model){

		String view = "redirect:/chgproperty/pending/refresh";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );

		return view;
	}

	@RequestMapping("/validate")
	public String multipleApprovalComment(FlowChangeApprovalServiceMultipleApprovalBean bean, TriModel model) {
		String view = "common/Comment::changeApprovalServiceMultipleApprovalComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		setPrev(model);

		return view;
	}

	private void mapping(FlowChangeApprovalServiceMultipleApprovalBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if ( requestInfo.getParameterValues("selectedAreqIds") != null )
		bean.getParam().getInputInfo().setSelectedAreqIds(requestInfo.getParameterValues("selectedAreqIds"));

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().getInputInfo().setComment(requestInfo.getParameter("reason"));
		}
	}
}
