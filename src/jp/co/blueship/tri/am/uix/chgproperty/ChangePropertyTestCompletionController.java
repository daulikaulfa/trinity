package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyTestCompletionServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyTestCompletionServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/chgproperty/testcomplete")
public class ChangePropertyTestCompletionController extends TriControllerSupport<FlowChangePropertyTestCompletionServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyTestCompletionService;
	}

	@Override
	protected FlowChangePropertyTestCompletionServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyTestCompletionServiceBean bean = new FlowChangePropertyTestCompletionServiceBean();
		return bean;
	}

	@RequestMapping
	public String testComplete(FlowChangePropertyTestCompletionServiceBean bean,
			TriModel model) {
		String view = "redirect:/chgproperty/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String testCompletionComment(FlowChangePropertyTestCompletionServiceBean bean,
			TriModel model) {
		String view = "common/Comment::testCompletionComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);
		return view;
	}

	private void mapping(FlowChangePropertyTestCompletionServiceBean bean,
			TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("pjtId") != null ){
			param.setSelectedPjtId(requestInfo.getParameter("pjtId"));
		}
		if ( requestInfo.getParameter("reason") != null ) {
			param.getInputInfo().setComment(requestInfo.getParameter("reason"));
		}

	}
}
