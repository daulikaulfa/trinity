package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/chgproperty")
public class ChangePropertyListController extends TriControllerSupport<FlowChangePropertyListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyListService;
	}

	@Override
	protected FlowChangePropertyListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyListServiceBean bean = new FlowChangePropertyListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowChangePropertyListServiceBean bean, TriModel model, ModelMap modelmap){
		return index(bean, model.setRedirect(true));
	}

	@RequestMapping(value = "/refresh")
	public String refresh( FlowChangePropertyListServiceBean bean, TriModel model ){

		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.execute(getServiceId(), bean , model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"", "/list"})
	public String index(FlowChangePropertyListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/search")
	public String search(FlowChangePropertyListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try{
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowChangePropertyListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch ( Exception e ) {
			ExceptionUtils.printStackTrace(e);
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowChangePropertyListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;

			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowChangePropertyListServiceBean.SearchCondition condition = gson.fromJson(json, FlowChangePropertyListServiceBean.SearchCondition.class);

			bean.getParam()
				.setLinesPerPage(5)
				.setSearchCondition( condition )
				.getSearchCondition().setSelectedPageNo(1)
			;

			bean.getParam().setRequestType(RequestType.init);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(
			FlowChangePropertyListServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		FlowChangePropertyListServiceBean.RequestParam param = bean.getParam();
		param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if (RequestType.init.equals(param.getRequestType())) {
		}

		if (RequestType.onChange.equals(param.getRequestType()) ){
			bean.getParam().getSearchCondition().setStsId(requestInfo.getParameter("condStatus"))
				.setCtgId(requestInfo.getParameter("condCategory"))
				.setMstoneId(requestInfo.getParameter("condMilestone"))
				.setKeyword(requestInfo.getParameter("searchKeyword"))
				;
			if(TriStringUtils.isNotEmpty(requestInfo.getParameter("selectedPageNo"))){
				bean.getParam().getSearchCondition().setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedPageNo")));
			}else{
				bean.getParam().getSearchCondition().setSelectedPageNo(0);
			}


			bean.getParam().getListSelection().setSelectedIds(requestInfo.getParameterValues("selectedPjtIds"));
		}
	}
}
