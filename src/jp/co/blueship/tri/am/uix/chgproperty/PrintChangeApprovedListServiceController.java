package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
*
* @version V4.00.00
* @author Hai Thach
*
*/
@Controller
@RequestMapping("/chgproperty/approved/list/print")
public class PrintChangeApprovedListServiceController
	extends TriControllerSupport<FlowChangeApprovedListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintChangeApprovedListService;
	}

	@Override
	protected FlowChangeApprovedListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovedListServiceBean bean = new FlowChangeApprovedListServiceBean();
		return bean;
	}

	@RequestMapping
	public String print(FlowChangeApprovedListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.printMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintChangeApprovedList.value())
			.addAttribute("result", bean)
		;

		return view;
	}

	private void printMapping (FlowChangeApprovedListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		bean.getParam().setLinesPerPage(0);

		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().getSearchCondition()
			.setSelectedPageNo	(1)
			.setCtgId			(requestInfo.getParameter("approved_condCategory"))
			.setMstoneId		(requestInfo.getParameter("approved_condMilestone"))
			.setKeyword			(requestInfo.getParameter("approved_searchKeyword"))
		;
	}
}