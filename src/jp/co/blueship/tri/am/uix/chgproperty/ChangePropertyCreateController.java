package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.beans.dto.ChangePropertyEditInputBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCreationServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCreationServiceBean.RequestOption;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Yusna Marlina
 */

@Controller
@RequestMapping("/chgproperty")
public class ChangePropertyCreateController extends TriControllerSupport<FlowChangePropertyCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyCreationService;
	}

	@Override
	protected FlowChangePropertyCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyCreationServiceBean bean = new FlowChangePropertyCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowChangePropertyCreationServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );

		return insert( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/create/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowChangePropertyCreationServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );

		return insert( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/create")
	public String insert(FlowChangePropertyCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.insertmapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/chgproperty/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyCreate.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "changeSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void insertmapping(FlowChangePropertyCreationServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		ChangePropertyEditInputBean info = bean.getParam().getInputInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			info.setReferenceId(requestInfo.getParameter("referenceId"))
					.setReferenceCategoryId(requestInfo.getParameter("referenceCategoryId"))
					.setSummary(requestInfo.getParameter("summary"))
					.setContents(requestInfo.getParameter("contents"))
					.setAssigneeId(requestInfo.getParameter("assigneeId"))
					.setCtgId(requestInfo.getParameter("ctgId"))
					.setMstoneId(requestInfo.getParameter("mstoneId"));
		}
	}
}
