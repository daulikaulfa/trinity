package jp.co.blueship.tri.am.uix.chgproperty;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalPendingReturnToSubmitterServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/chgproperty/pending/returnToSubmitter")
public class ChangeApprovalPendingReturnToSubmitterContoller extends TriControllerSupport<FlowChangeApprovalPendingReturnToSubmitterServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalReturnToSubmitterService;
	}

	@Override
	protected FlowChangeApprovalPendingReturnToSubmitterServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalPendingReturnToSubmitterServiceBean bean = new FlowChangeApprovalPendingReturnToSubmitterServiceBean();
		return bean;
	}

	@RequestMapping
	public String retrunToSubmitter(
			FlowChangeApprovalPendingReturnToSubmitterServiceBean bean,
			TriModel model) {

		String view = "redirect:/chgproperty/pending/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		IRequestInfo requestInfo = model.getRequestInfo();
		String pjtId = requestInfo.getParameter("pjtId");

		model.getRedirectAttributes()
			.addFlashAttribute( "result", bean )
			.addFlashAttribute( "pjtId", pjtId )
			.addFlashAttribute( "serviceId", "FlowChangeApprovalPendingReturnToSubmitterService" )
		;

		return view;
	}

	@RequestMapping("/validate")
	public String returnToSubmitterComment(FlowChangeApprovalPendingReturnToSubmitterServiceBean bean,
											TriModel model) {
		String view = "common/Comment::changeApprovalPendingReturnToSubmitterComment";
		List<String> message = bean.getMessages();

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		if (message != null && !message.isEmpty()){
			bean.setMessages(message);
		}

		model.getModel().addAttribute("result", bean );
		setPrev(model);

		return view;
	}


	private void mapping(FlowChangeApprovalPendingReturnToSubmitterServiceBean bean,
						 TriModel model){

		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedAreqId( requestInfo.getParameter("areqId") );

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().getInputInfo().setComment( requestInfo.getParameter("reason") );
		}
	}
}
