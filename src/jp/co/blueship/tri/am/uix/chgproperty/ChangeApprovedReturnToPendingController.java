package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedReturnToPendingServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedReturnToPendingServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@RequestMapping(value = "/chgproperty/approved/returnToPending")
public class ChangeApprovedReturnToPendingController
				extends TriControllerSupport<FlowChangeApprovedReturnToPendingServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovedReturnToPendingService;
	}

	@Override
	protected FlowChangeApprovedReturnToPendingServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovedReturnToPendingServiceBean bean =
						new FlowChangeApprovedReturnToPendingServiceBean();
		return bean;
	}

	@RequestMapping
	public String returnToPending (FlowChangeApprovedReturnToPendingServiceBean bean, TriModel model) {
		String view = "redirect:/chgproperty/pending/redirect";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String returnToPendingComment(FlowChangeApprovedReturnToPendingServiceBean bean, TriModel model) {
		String view = "common/Comment::returnToPending";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowChangeApprovedReturnToPendingServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedPjtId( requestInfo.getParameter("pjtId") );

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().setComment(requestInfo.getParameter("reason"));
		}
	}
}
