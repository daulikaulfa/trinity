package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyDetailsServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/chgproperty")
public class ChangePropertyDetailsController extends TriControllerSupport<FlowChangePropertyDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyDetailsService;
	}

	@Override
	protected FlowChangePropertyDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyDetailsServiceBean bean = new FlowChangePropertyDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = "/details")
	public String changePropertyDetails(
			FlowChangePropertyDetailsServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangePropertyDetails.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute( "result" , bean ); setPrev(model)
		;

		return view;
	}

	@RequestMapping(value = "/details/redirect")
	public String redirect( @ModelAttribute("pjtId") String pjtId,
							@ModelAttribute("result") DomainServiceBean prevBean,
							FlowChangePropertyDetailsServiceBean bean,
							TriModel model ){

		bean.getParam().setSelectedPjtId(pjtId);
		bean.getMessageInfo().addFlashMessages( prevBean.getMessageInfo().getFlashMessages() );

		return changePropertyDetails(bean, model.setRedirect(true));
	}

	private void mapping(FlowChangePropertyDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		FlowChangePropertyDetailsServiceBean.RequestParam param = bean.getParam();
		if ( TriStringUtils.isEmpty(bean.getParam().getSelectedPjtId()) ) {
			param.setSelectedPjtId( requestInfo.getParameter("pjtId") );
		}
	}
}
