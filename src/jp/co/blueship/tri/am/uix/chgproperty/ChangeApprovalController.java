package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/chgproperty/pending/approve")
public class ChangeApprovalController extends TriControllerSupport<FlowChangeApprovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovalService;
	}

	@Override
	protected FlowChangeApprovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovalServiceBean bean = new FlowChangeApprovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String approval(FlowChangeApprovalServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if( bean.getResult().isCompleted() ){
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				view = "redirect:/chgproperty/pending/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.ChangeRequestApprovalPendingApprovalDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
		;
		
		//setPrev(model);
		return view;
	}

	@RequestMapping("/validate")
	public String approvalComment(FlowChangeApprovalServiceBean bean, TriModel model){

		String view = "common/Comment::changeRequestApprovalPendingAprovalComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);

		return view;
	}

	private void mapping(FlowChangeApprovalServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if ( requestInfo.getParameterValues("areqIds") != null)
		bean.getParam().getInputInfo().setSelectedAreqIds(requestInfo.getParameterValues("areqIds"));

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().getInputInfo().setComment(requestInfo.getParameter("reason"));
		}
	}
}
