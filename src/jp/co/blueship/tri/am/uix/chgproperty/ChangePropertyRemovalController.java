package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/chgproperty/remove")
public class ChangePropertyRemovalController extends TriControllerSupport<FlowChangePropertyRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyRemovalService;
	}

	@Override
	protected FlowChangePropertyRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyRemovalServiceBean bean = new FlowChangePropertyRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String removal(FlowChangePropertyRemovalServiceBean bean, TriModel model) {

		String view = "redirect:/chgproperty/redirect";
		String ref = model.getRequestInfo().getParameter("referer");
		String fwd = model.getRequestInfo().getParameter("forward");

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				bean.getParam().setRequestType(RequestType.init);
				if (ref.equals(fwd)) {
					if (fwd.equals(TriView.ChangePropertyList.value())) {
						view = "redirect:/chgproperty/refresh";
					}
				} else {
					if (fwd.equals(TriView.ChangePropertyList.value())) {
						view = "redirect:/chgproperty/redirect";
					} else if (fwd.equals(TriView.ChangePropertyDetails.value())) {
						view = "redirect:/chgproperty/redirect";
					}
				}
			} else {
				if (ref.equals(TriView.ChangePropertyList.value())) {
					view = "redirect:/chgproperty/refresh";
				} else if (ref.equals(TriView.ChangePropertyDetails.value())) {
					view = "redirect:/chgproperty/details";
				}
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String removalComment(FlowChangePropertyRemovalServiceBean bean,
			TriModel model) {
		String view = "common/Comment::removalComment";
		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result" , bean ); 
		setPrev(model);
		return view;
	}

	private void mapping(FlowChangePropertyRemovalServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowChangePropertyRemovalServiceBean.RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("pjtId") != null ){
			param.setSelectedPjtId(requestInfo.getParameter("pjtId"));
			
		}
		
		if (RequestType.validate.equals(bean.getParam().getRequestType())) {
			if ( requestInfo.getParameter("reason") != null ){
				param.getInputInfo().setComment(requestInfo.getParameter("reason"));
			}
		}
	}
}