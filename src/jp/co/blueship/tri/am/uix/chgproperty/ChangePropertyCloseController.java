package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyCloseServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */

@Controller
@RequestMapping("/chgproperty/close")
public class ChangePropertyCloseController extends TriControllerSupport<FlowChangePropertyCloseServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangePropertyCloseService;
	}

	@Override
	protected FlowChangePropertyCloseServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyCloseServiceBean bean = new FlowChangePropertyCloseServiceBean();
		return bean;
	}

	@RequestMapping
	public String close(FlowChangePropertyCloseServiceBean bean, TriModel model) {
		String view = "redirect:/chgproperty/redirect";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		model.getModel().addAttribute("result", bean);
		bean.getParam().setRequestType(RequestType.init);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String closeComment(FlowChangePropertyCloseServiceBean bean,
			TriModel model) {
		String view = "common/Comment::closeComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);
		return view;
	}

	private void mapping(FlowChangePropertyCloseServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedPjtId(requestInfo.getParameter("pjtId"));

		if ( requestInfo.getParameter("reason") != null ) {
			param.getInputInfo().setComment(requestInfo.getParameter("reason"));
		}
	}
}
