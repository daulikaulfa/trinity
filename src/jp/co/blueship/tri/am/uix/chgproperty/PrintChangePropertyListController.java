package jp.co.blueship.tri.am.uix.chgproperty;

import java.io.UnsupportedEncodingException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.RequestParam;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * This is a service for the front-end.
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
@Controller
@RequestMapping("/chgproperty/list/print")
public class PrintChangePropertyListController
		extends TriControllerSupport<FlowChangePropertyListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintChangePropertyListService;
	}


	@Override
	protected FlowChangePropertyListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyListServiceBean bean = new FlowChangePropertyListServiceBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowChangePropertyListServiceBean bean, TriModel model) {

		String view = TriTemplateView.PrintTemplate.value();
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel()
			.addAttribute("view", TriView.PrintChangePropertyList.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void insertMapping(FlowChangePropertyListServiceBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if(RequestType.onChange.equals(param.getRequestType())){
			searchCondition
				.setCtgId	( requestInfo.getParameter("condCategory") )
				.setMstoneId( requestInfo.getParameter("condMilestone") )
				.setKeyword	( requestInfo.getParameter("searchKeyword") )
				.setStsId	( requestInfo.getParameter("condStatus") )
				.setSelectedPageNo(1)
			;
			param.setLinesPerPage(0);
		}
	}
}
