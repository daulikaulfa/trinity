package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/chgproperty/details/print")
public class PrintChangePropertyDetailsController extends TriControllerSupport<FlowChangePropertyDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintChangePropertyDetailsService;
	}

	@Override
	protected FlowChangePropertyDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangePropertyDetailsServiceBean bean = new FlowChangePropertyDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String changePropertyDetails(FlowChangePropertyDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintChangePropertyDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "changeSubmenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowChangePropertyDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		FlowChangePropertyDetailsServiceBean.RequestParam param = bean.getParam();
		param.setSelectedPjtId(requestInfo.getParameter("pjtId"));

	}
}
