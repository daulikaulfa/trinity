package jp.co.blueship.tri.am.uix.chgproperty;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangeApprovedListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Norheda
 *
 * @version V4.00.00
 * @author Yusna Marlina
 */
@Controller
@RequestMapping("/chgproperty/approved")
public class ChangeApprovedListController extends TriControllerSupport<FlowChangeApprovedListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmChangeApprovedListService;
	}

	@Override
	protected FlowChangeApprovedListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowChangeApprovedListServiceBean bean = new FlowChangeApprovedListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowChangeApprovedListServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.init);
		return listView( bean , model.setRedirect(true) );
	}

	@RequestMapping(value ="/refresh")
	public String refresh (FlowChangeApprovedListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	@RequestMapping(value = {"","/"})
	public String listView (FlowChangeApprovedListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/error"})
	public String error(FlowChangeApprovedListServiceBean bean, TriModel model) {
		return listView(bean, model.setRedirect(true));
	}

	@RequestMapping(value = "/search")
	public String search(FlowChangeApprovedListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu", "approvalSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
		;
		setPrev(model);
		return view;
	}

	private void mapping (FlowChangeApprovedListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if(TriStringUtils.isNotEmpty(requestInfo.getParameter("approved_selectedPageNo"))){
				searchCondition.setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("approved_selectedPageNo")));
			}else{
				searchCondition.setSelectedPageNo(0);
			}
			searchCondition
				.setCtgId(requestInfo.getParameter("approved_condCategory"))
				.setMstoneId(requestInfo.getParameter("approved_condMilestone"))
				.setKeyword(requestInfo.getParameter("approved_searchKeyword"))
			;
		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowChangeApprovedListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try {
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition());

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch ( Exception e ) {
			ExceptionUtils.printStackTrace(e);
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowChangeApprovedListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowChangeApprovedListServiceBean.SearchCondition condition = gson.fromJson(json, FlowChangeApprovedListServiceBean.SearchCondition.class);

			bean.getParam()
				.setLinesPerPage(5)
				.setSearchCondition(condition)
				.getSearchCondition().setSelectedPageNo(1);

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("pagination",bean.getPage())
			.addAttribute("selectedTabId"  , "1")
			.addAttribute("view", TriView.ChangeRequestApprovalList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "approvalSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}
}
