package jp.co.blueship.tri.am.uix.checkinout;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */

@Controller
@RequestMapping("/checkout/details/print")
public class PrintCheckOutRequestDetailsController extends TriControllerSupport<FlowCheckoutRequestDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintCheckoutRequestDetailsService;
	}

	@Override
	protected FlowCheckoutRequestDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckoutRequestDetailsServiceBean bean = new FlowCheckoutRequestDetailsServiceBean();
		return bean;

	}
	@RequestMapping
	public String index(FlowCheckoutRequestDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.PrintCheckOutRequestDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "checkinoutSubmenu")
		;

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);

		return view;
	}


	private void mapping(FlowCheckoutRequestDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
			bean.getParam().getResourceSelection().setPath("");
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
		}

	}








}
