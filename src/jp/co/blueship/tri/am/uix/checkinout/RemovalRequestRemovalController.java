package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestRemovalServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestRemovalServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestRemovalController extends TriControllerSupport<FlowRemovalRequestRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestRemovalService;
	}

	@Override
	protected FlowRemovalRequestRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestRemovalServiceBean bean = new FlowRemovalRequestRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String delete(FlowRemovalRequestRemovalServiceBean bean, TriModel model){

		String view = "redirect:/removal/redirect";
		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/removal/refresh";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean);
		return view;
	}

	private void mapping(FlowRemovalRequestRemovalServiceBean bean, TriModel model) {
		bean.getParam().setRequestType(RequestType.submitChanges);
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedAreqId(requestInfo.getParameter("areqId"));
	}
}
