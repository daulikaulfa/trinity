package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestDetailsController extends TriControllerSupport<FlowRemovalRequestDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestDetailsService;
	}

	@Override
	protected FlowRemovalRequestDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestDetailsServiceBean bean = new FlowRemovalRequestDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = "/details")
	public String removalRequestDetails(FlowRemovalRequestDetailsServiceBean bean,
										TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RemovalRequestDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
			.addAttribute("result", bean)
		;

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		setPrev(model);
		return view;
	}

	private void mapping(FlowRemovalRequestDetailsServiceBean bean, TriModel model ) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		ResourceSelection resourceSelection = param.getResourceSelection();
		String resourceRequestType = requestInfo.getParameter("resourceRequestType");
		
		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if (requestInfo.getParameter("areqId") != null){
				param.setSelectedAreqId(requestInfo.getParameter("areqId"));
			}
			bean.getParam().getResourceSelection().setPath("");
		}

		if (RequestType.onChange.equals(param.getRequestType())) {
			bean.getParam().getResourceSelection().setPath(requestInfo.getParameter("path"));
			if( ResourceSelection.ResourceRequestType.selectFolder.equals(resourceRequestType) ){
				resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
				resourceSelection.setSelectedFiles(requestInfo.getParameterValues("selectedFiles"));
			} else if( ResourceSelection.ResourceRequestType.openFolder.equals(resourceRequestType) ){
				resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);
			} else if( ResourceSelection.ResourceRequestType.closeFolder.equals(resourceRequestType) ){
				resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
			}
			
			if( "list".equalsIgnoreCase(requestInfo.getParameter("viewMode")) ){
				param.getInputInfo().setFolderTreeFormat(false);
			}else {
				bean.getParam().getInputInfo().setFolderTreeFormat(true);
			}

		}
	}
}
