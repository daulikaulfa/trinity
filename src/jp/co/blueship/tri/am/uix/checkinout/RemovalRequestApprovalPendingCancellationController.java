package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestApprovalPendingCancellationServiceBean.RemovalRequestCancellationInputBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/removal/cancel")
public class RemovalRequestApprovalPendingCancellationController extends TriControllerSupport<FlowRemovalRequestApprovalPendingCancellationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestApprovalPendingCancellationService;
	}

	@Override
	protected FlowRemovalRequestApprovalPendingCancellationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestApprovalPendingCancellationServiceBean bean =
				new FlowRemovalRequestApprovalPendingCancellationServiceBean();
		return bean;
	}

	@RequestMapping
	public String cancel(FlowRemovalRequestApprovalPendingCancellationServiceBean bean, TriModel model) {
		String view = "redirect:/removal/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.cancelMapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		bean.getParam().setRequestType(RequestType.init);
		model.getRedirectAttributes().addFlashAttribute("result", bean);
		model.getModel().addAttribute("result", bean);

		return view;
	}

	@RequestMapping(value = "/validate")
	public String cancelComment(FlowRemovalRequestApprovalPendingCancellationServiceBean bean,
								TriModel model) {
		String view = "common/Comment::removalRequestCancellationComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.cancelMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result" , bean);
		return view;
	}

	private void cancelMapping (FlowRemovalRequestApprovalPendingCancellationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		RemovalRequestCancellationInputBean input = bean.getParam().getInputInfo();
		bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			input.setComment(requestInfo.getParameter("reason"));
		}
	}

}
