package jp.co.blueship.tri.am.uix.checkinout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.CheckoutRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckoutResourceComparator;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Faiz Azhar
 */

@Controller
@RequestMapping("/checkout")
public class CheckOutRequestController extends TriControllerSupport<FlowCheckoutRequestServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckoutRequestService;
	}

	@Override
	protected FlowCheckoutRequestServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckoutRequestServiceBean bean = new FlowCheckoutRequestServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowCheckoutRequestServiceBean bean, TriModel model ){
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		LogHandler.debug( TriLogFactory.getInstance(), "ctgId:=" + ctgId );
		bean.getParam().getInputInfo().setCtgId( ctgId );
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		Object temp = sesInfo.getAttribute("flowReleaseRequestBeanTemp");
		String sessionKey = this.getSessionKey(bean.getHeader().getWindowsId(), bean.getParam().getReferer());
		sesInfo.setAttribute(sessionKey, temp);
		sesInfo.removeAttribute("flowReleaseRequestBeanTemp");
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/create/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowCheckoutRequestServiceBean bean, TriModel model ){
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		LogHandler.debug( TriLogFactory.getInstance(), "mstoneId:=" + mstoneId );
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		Object temp = sesInfo.getAttribute("flowReleaseRequestBeanTemp");
		String sessionKey = this.getSessionKey(bean.getHeader().getWindowsId(), bean.getParam().getReferer());
		sesInfo.setAttribute(sessionKey, temp);
		sesInfo.removeAttribute("flowReleaseRequestBeanTemp");
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value="/create/upload")
	public String upload(@RequestParam("userFile") MultipartFile userFile, FlowCheckoutRequestServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.uploadFile(userFile, bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.CheckOutRequestCreation.value());
		return view;
	}
	
	@RequestMapping(value="/create/listfile")
	public String listFile(FlowCheckoutRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowCheckoutRequestServiceBean returnBean =	new FlowCheckoutRequestServiceBean();
		String startIndexStr = requestInfo.getParameter("startIndex");
		String endIndexStr = requestInfo.getParameter("endIndex");
		if (TriStringUtils.isDigits(startIndexStr) && TriStringUtils.isDigits(endIndexStr)){
			int startIndex = Integer.parseInt(startIndexStr);
			int endIndex = Integer.parseInt(endIndexStr);
			List<ICheckoutResourceViewBean> listFiles = bean.getResourceSelectionFolderView().getRequestViews();
			if (endIndex > listFiles.size()) endIndex = listFiles.size();
			if ( startIndex <= endIndex ){
				List<ICheckoutResourceViewBean> subListFiles = listFiles.subList(startIndex, endIndex);
				returnBean.getResourceSelectionFolderView().setRequestViews(subListFiles);
			}
		}
		
		
		model.getModel()
		.addAttribute("result", returnBean)
		.addAttribute("view", TriView.CheckOutRequestCreation.value());
		model.getModel().addAttribute("requestViewList", returnBean.getResourceSelectionFolderView().getRequestViews());
		return "CheckOutRequestCreation :: table[@id='fileViewTable']";
	}
	
	
	@RequestMapping(value="/create/sortfile")
	public String sortfile(FlowCheckoutRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String columnName = requestInfo.getParameter("columnName");
		String sortOrder = requestInfo.getParameter("sortOrder");
		CheckoutResourceComparator resoureComparator = new CheckoutResourceComparator(columnName, sortOrder);
		Collections.sort(bean.getResourceSelectionFolderView().getRequestViews(),resoureComparator);
		if(sortOrder.equalsIgnoreCase("descending")){
			Collections.reverse(bean.getResourceSelectionFolderView().getRequestViews());
		}
		List<ICheckoutResourceViewBean> tempListView =  new ArrayList<ICheckoutResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		
		model.getModel()
		.addAttribute("result", bean)
		.addAttribute("view", TriView.CheckOutRequestCreation.value());
		model.getModel().addAttribute("requestViewList", tempListView);
		return "CheckOutRequestCreation :: table[@id='fileViewTable']";
	}
	

	@RequestMapping(value = { "/create" })
	public String index(FlowCheckoutRequestServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/checkinout/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.CheckOutRequestCreation.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "checkinoutSubmenu")
		;
		List<ICheckoutResourceViewBean> tempListView =  new ArrayList<ICheckoutResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		model.getModel().addAttribute("requestViewList", tempListView);
		
		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		setPrev(model);
		return view;
	}
	
	@RequestMapping(value = { "/create/singleResource" })
	@ResponseBody
	public String singleResource(FlowCheckoutRequestServiceBean bean, TriModel model) {

		try {

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
		}
		
		return "";
		
	}

	private void uploadFile(MultipartFile userFile, FlowCheckoutRequestServiceBean bean, TriModel model) throws IOException {
		CheckoutRequestEditInputBean checkoutInfo = bean.getParam().getInputInfo();
		jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean.RequestParam param = bean.getParam();

		param.setRequestOption(RequestOption.fileUpload);
		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			checkoutInfo.setCsvInputStreamBytes(fileBytes);
			checkoutInfo.setCsvFileNm(userFile.getOriginalFilename());
			param.getResourceSelection().setPath("");
		}
	}

	private void mapping(FlowCheckoutRequestServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		ResourceSelection resourceSelection = bean.getParam().getResourceSelection();
		CheckoutRequestEditInputBean inputBean = bean.getParam().getInputInfo();
		ResourceSelectionFolderView<ICheckoutResourceViewBean> selectionFolder = bean.getResourceSelectionFolderView();

		LogHandler.debug( TriLogFactory.getInstance(), "requestType:=" + bean.getParam().getRequestType());
		LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + bean.getParam().getRequestOption());
		LogHandler.debug( TriLogFactory.getInstance(), "resourceRequestType:=" + requestInfo.getParameter("resourceRequestType"));
		LogHandler.debug( TriLogFactory.getInstance(), "path:=" + requestInfo.getParameter("path"));
		LogHandler.debug( TriLogFactory.getInstance(), "selectedFiles:=" + requestInfo.getParameter("selectedFiles"));

		// init
		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			String sessionSelectedLotId = this.getSessionSelectedLot(sesInfo, bean);
			if (sessionSelectedLotId != null) {
				bean.getParam().setSelectedLotId(sessionSelectedLotId);
			}			
			bean.getParam().setRequestOption(RequestOption.selectResource);
			bean.getParam().getResourceSelection().setPath("");
		}

		// onChange
		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if (!model.isRedirect()) {
				bean.getParam().setRequestOption(RequestOption.value( requestInfo.getParameter("requestOption") ));
				resourceSelection.setPath(requestInfo.getParameter("path"));
			}


			if(RequestOption.selectGroup.equals( bean.getParam().getRequestOption())){
				inputBean.setGroupId(requestInfo.getParameter("groupId"));
			}

			if(RequestOption.selectAssignee.equals( bean.getParam().getRequestOption())){
				inputBean.setAssigneeId(requestInfo.getParameter("assigneeId"));
			}
			if(RequestOption.selectResource.equals( bean.getParam().getRequestOption())){

				if( ResourceSelection.ResourceRequestType.selectFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
					
				} else if( ResourceSelection.ResourceRequestType.openFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);

				} else if( ResourceSelection.ResourceRequestType.closeFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
					
				} else if( ResourceSelection.ResourceRequestType.selectSingleResource.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectSingleResource);
					Set<String> selectedResources = resourceSelection.getSelectedFileSet();
					String resource = requestInfo.getParameter("selectedResource");
					if (null != requestInfo.getParameter("isSelected") && !Boolean.parseBoolean(requestInfo.getParameter("isSelected"))) {
						if ( TriStringUtils.isNotEmpty( resource ) ) {
							selectedResources = AmResourceSelectionUtils.unselectResource(resource, selectedResources);
						}
					}else {
						selectedResources.add(resource);
					}
					
					resourceSelection.setSelectedFiles(selectedResources.toArray(new String[selectedResources.size()]));
				} else if( ResourceSelection.ResourceRequestType.selectAllResource.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectAllResource);
					String selectedPath = requestInfo.getParameter("selectedPath");
					if ( TriStringUtils.isNotEmpty(selectedPath) && selectedPath.equalsIgnoreCase(selectionFolder.getSelectedPath()) ){
						if (null != requestInfo.getParameter("isSelected") ) {
							resourceSelection.setSelectAllFilesInFolder(Boolean.parseBoolean(requestInfo.getParameter("isSelected")));
						}
					}
				}
			}
			if( RequestOption.fileUpload.equals( bean.getParam().getRequestOption()) ){
				inputBean.setCsvInputStreamBytes(requestInfo.getParameter("checkOutRequestFile").getBytes());
			}
		}

		// submitChanges
		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			inputBean.setPjtId( requestInfo.getParameter("pjtId"))
				.setGroupId( requestInfo.getParameter("groupId"))
				.setAssigneeId( requestInfo.getParameter("assigneeId"))
				.setContents( requestInfo.getParameter("contents"))
				.setSubject( requestInfo.getParameter("subject"))
				.setCheckinDueDate( requestInfo.getParameter("checkinDueDate"))
				.setCtgId( requestInfo.getParameter("ctgId"))
				.setMstoneId( requestInfo.getParameter("mstoneId"))
				.setResourceSelection( AmResourceSelectionUtils.treeMode.equals(requestInfo.getParameter("resourceMode"))? true : false)
				.setSubmitMode( SubmitMode.value( requestInfo.getParameter("submitMode")));

			resourceSelection.setPath(TriStringUtils.isEmpty(requestInfo.getParameter("path"))? "" : requestInfo.getParameter("path"));
			resourceSelection.setSelectedFiles(new String[0]);
		}

	}




}
