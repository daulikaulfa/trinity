package jp.co.blueship.tri.am.uix.checkinout;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestOverviewServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestOverviewContoller extends TriControllerSupport<FlowRemovalRequestOverviewServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestOverviewService;
	}

	@Override
	protected FlowRemovalRequestOverviewServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestOverviewServiceBean bean = new FlowRemovalRequestOverviewServiceBean();
		return bean;
	}

	@RequestMapping("/overview")
	public String overView(FlowRemovalRequestOverviewServiceBean bean , TriModel model){

		String view = "common/OverviewPopup::removalRequestOverview";
		List<String> message = bean.getMessages();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		if(message != null && !message.isEmpty()){
			bean.setMessages(message);
		}
		model.getModel().addAttribute( "result" , bean ); setPrev(model);

		return view;
	}

	private void mapping(FlowRemovalRequestOverviewServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedAreqId( requestInfo.getParameter("areqId") );
	}
}
