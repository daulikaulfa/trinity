package jp.co.blueship.tri.am.uix.checkinout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.RemovalResourceComparator;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;


/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 * @version V4.00.00
 * @author Syaza Hanapi
 *
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestEditController extends TriControllerSupport<FlowRemovalRequestEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestEditService;
	}

	@Override
	protected FlowRemovalRequestEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestEditServiceBean bean = new FlowRemovalRequestEditServiceBean();
		return bean;
	}

	@RequestMapping(value = "/edit/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowRemovalRequestEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		return removalRequestEdit ( bean , model.setRedirect(true) );
	}


	@RequestMapping(value = "/edit/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowRemovalRequestEditServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		return removalRequestEdit( bean , model.setRedirect(true) );
	}
	
	
	@RequestMapping(value="/edit/listfile")
	public String listFile(FlowRemovalRequestEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowRemovalRequestEditServiceBean returnBean =	new FlowRemovalRequestEditServiceBean();
		String startIndexStr = requestInfo.getParameter("startIndex");
		String endIndexStr = requestInfo.getParameter("endIndex");
		if (TriStringUtils.isDigits(startIndexStr) && TriStringUtils.isDigits(endIndexStr)){
			int startIndex = Integer.parseInt(startIndexStr);
			int endIndex = Integer.parseInt(endIndexStr);
			List<IRemovalResourceViewBean> listFiles = bean.getResourceSelectionFolderView().getRequestViews();
			if (endIndex > listFiles.size()) endIndex = listFiles.size();
			if ( startIndex <= endIndex ){
				List<IRemovalResourceViewBean> subListFiles = listFiles.subList(startIndex, endIndex);
				returnBean.getResourceSelectionFolderView().setRequestViews(subListFiles);
			}
		}
		
		
		model.getModel()
		.addAttribute("result", returnBean)
		.addAttribute("view", TriView.RemovalRequestEdit.value());
		model.getModel().addAttribute("requestViewList", returnBean.getResourceSelectionFolderView().getRequestViews());
		return "RemovalRequestEdit :: table[@id='fileViewTable']";
	}
	
	
	@RequestMapping(value="/edit/sortfile")
	public String sortfile(FlowRemovalRequestEditServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String columnName = requestInfo.getParameter("columnName");
		String sortOrder = requestInfo.getParameter("sortOrder");
		RemovalResourceComparator resoureComparator = new RemovalResourceComparator(columnName, sortOrder);
		Collections.sort(bean.getResourceSelectionFolderView().getRequestViews(),resoureComparator);
		if(sortOrder.equalsIgnoreCase("descending")){
			Collections.reverse(bean.getResourceSelectionFolderView().getRequestViews());
		}
		List<IRemovalResourceViewBean> tempListView =  new ArrayList<IRemovalResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		
		model.getModel()
		.addAttribute("result", bean)
		.addAttribute("view", TriView.RemovalRequestEdit.value());
		model.getModel().addAttribute("requestViewList", tempListView);
		return "RemovalRequestEdit :: table[@id='fileViewTable']";
	}

	@RequestMapping(value = "/edit")
	public String removalRequestEdit(FlowRemovalRequestEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (SubmitMode.request.equals(bean.getParam().getInputInfo().getSubmitMode())
					&& RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				view = "redirect:/removal/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RemovalRequestEdit.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
			.addAttribute("result", bean)
		;
		
		List<IRemovalResourceViewBean> tempListView =  new ArrayList<IRemovalResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		model.getModel().addAttribute("requestViewList", tempListView);

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		setPrev(model);
		return view;
	}
	

	@RequestMapping(value = { "/edit/singleResource" })
	@ResponseBody
	public String singleResource(FlowRemovalRequestEditServiceBean bean, TriModel model) {

		try {

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
		}
		
		return "";
		
	}

	@RequestMapping(value="/edit/upload")
	public String upload(@RequestParam("userFile") MultipartFile userFile, FlowRemovalRequestEditServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.uploadFile(userFile, bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.RemovalRequestEdit.value());
		return view;
	}

	private void mapping(FlowRemovalRequestEditServiceBean bean, TriModel model ) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ResourceSelection resourceSelection = bean.getParam().getResourceSelection();
		RemovalRequestEditInputBean inputBean = bean.getParam().getInputInfo();
		ResourceSelectionFolderView<IRemovalResourceViewBean> selectionFolder = bean.getResourceSelectionFolderView();

		LogHandler.debug( TriLogFactory.getInstance(), "requestType:=" + bean.getParam().getRequestType());
		LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + requestInfo.getParameter("requestOption"));
		LogHandler.debug( TriLogFactory.getInstance(), "resourceRequestType:=" + requestInfo.getParameter("resourceRequestType"));
		LogHandler.debug( TriLogFactory.getInstance(), "path:=" + requestInfo.getParameter("path"));
		LogHandler.debug( TriLogFactory.getInstance(), "selectedFiles:=" + requestInfo.getParameter("selectedFiles"));

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if (requestInfo.getParameter("areqId") != null){
				bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
			}
			bean.getParam().setRequestOption(RequestOption.selectResource);
			bean.getParam().getResourceSelection().setPath("");
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {

			if (!model.isRedirect()) {
				bean.getParam().setRequestOption(RequestOption.value( requestInfo.getParameter("requestOption") ));
				resourceSelection.setPath(requestInfo.getParameter("path"));
			}

			LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + bean.getParam().getRequestOption());


			if(RequestOption.selectGroup.equals( bean.getParam().getRequestOption())){
				inputBean.setGroupId(requestInfo.getParameter("groupId"));
			}

			if(RequestOption.selectAssignee.equals( bean.getParam().getRequestOption())){
				inputBean.setAssigneeId(requestInfo.getParameter("assigneeId"));
			}

			if(RequestOption.selectResource.equals( bean.getParam().getRequestOption())){
				if( ResourceSelection.ResourceRequestType.selectFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
					resourceSelection.setSelectedFiles(requestInfo.getParameterValues("selectedFiles"));
				}
				else if( ResourceSelection.ResourceRequestType.openFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);
				}
				else if( ResourceSelection.ResourceRequestType.closeFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
				}else if( ResourceSelection.ResourceRequestType.selectSingleResource.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectSingleResource);
					Set<String> selectedResources = resourceSelection.getSelectedFileSet();
					String resource = requestInfo.getParameter("selectedResource");
					if (null != requestInfo.getParameter("isSelected") && !Boolean.parseBoolean(requestInfo.getParameter("isSelected"))) {
						if ( TriStringUtils.isNotEmpty( resource ) ) {
							selectedResources = AmResourceSelectionUtils.unselectResource(resource, selectedResources);
						}
					}else {
						selectedResources.add(resource);
					}
					
					resourceSelection.setSelectedFiles(selectedResources.toArray(new String[selectedResources.size()]));
				} else if( ResourceSelection.ResourceRequestType.selectAllResource.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectAllResource);
					String selectedPath = requestInfo.getParameter("selectedPath");
					if ( TriStringUtils.isNotEmpty(selectedPath) && selectedPath.equalsIgnoreCase(selectionFolder.getSelectedPath()) ){
						if (null != requestInfo.getParameter("isSelected") ) {
							resourceSelection.setSelectAllFilesInFolder(Boolean.parseBoolean(requestInfo.getParameter("isSelected")));
						}
					}
				}
			}
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			inputBean.setPjtId( requestInfo.getParameter("pjtId"))
				.setGroupId( requestInfo.getParameter("groupId"))
				.setAssigneeId( requestInfo.getParameter("assigneeId"))
				.setContents( requestInfo.getParameter("contents"))
				.setSubject( requestInfo.getParameter("subject"))
				.setCtgId( requestInfo.getParameter("ctgId"))
				.setMstoneId( requestInfo.getParameter("mstoneId"))
				.setResourceSelection( AmResourceSelectionUtils.treeMode.equals(requestInfo.getParameter("resourceMode"))? true : false )
				.setSubmitMode( SubmitMode.value( requestInfo.getParameter("submitMode")));

			resourceSelection.setSelectedFiles(new String[0]);
			resourceSelection.setPath(TriStringUtils.isEmpty(requestInfo.getParameter("path"))? "" : requestInfo.getParameter("path"));
		}
	}

	private void uploadFile(MultipartFile userFile, FlowRemovalRequestEditServiceBean bean,
			TriModel model) throws IOException {
		RemovalRequestEditInputBean removalInfo = bean.getParam().getInputInfo();
		 jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean.RequestParam param =  bean.getParam();

		param.setRequestOption(RequestOption.fileUpload);
		byte[] fileBytes = new byte[] {};
		if (!userFile.isEmpty()) {
			fileBytes = userFile.getBytes();
			removalInfo.setCsvInputStreamBytes(fileBytes);
			removalInfo.setCsvFileNm(userFile.getOriginalFilename());
		}
	}

}
