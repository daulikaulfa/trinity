package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
@Controller
@RequestMapping("/removal/list/print")
public class PrintRemovalRequestListController extends TriControllerSupport<FlowRemovalRequestListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintRemovalRequestListService;
	}

	@Override
	protected FlowRemovalRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestListServiceBean bean = new FlowRemovalRequestListServiceBean();
		return bean;
	}

	@RequestMapping
	public String index(FlowRemovalRequestListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try{
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		if( isDraft ){
			model.getModel().addAttribute("pagination", bean.getDraftPage());
		}else{
			model.getModel().addAttribute("pagination", bean.getPage());
		}

		model.getModel()
			.addAttribute("view", TriView.PrintRemovalRequestList.value())
			.addAttribute("selectedMenu", "lotMenu")
			.addAttribute("selectedSubMenu", "removalSubMenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return TriTemplateView.PrintTemplate.value();
	}

	private void mapping (FlowRemovalRequestListServiceBean bean, TriModel model) {

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		if(isDraft) {
			bean.getParam().setSelectedDraft(true);

			searchDraftCondition
				.setCtgId(requestInfo.getParameter("draft_ctdId"))
				.setMstoneId(requestInfo.getParameter("draft_mstoneId"))
				.setStsId(requestInfo.getParameter("draft_stsId"))
				.setKeyword(requestInfo.getParameter("draft_keyword"))
			;
		}else{
			bean.getParam().setSelectedDraft(false);

			searchCondition
				.setCtgId(requestInfo.getParameter("ctgId"))
				.setMstoneId(requestInfo.getParameter("mstoneId"))
				.setStsId(requestInfo.getParameter("stsId"))
				.setKeyword(requestInfo.getParameter("keyword"))
			;
		}
	}
}
