package jp.co.blueship.tri.am.uix.checkinout;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceExportToFileBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceExportToFileBean.RequestParam;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

@Controller
@RequestMapping("/checkinout/export")
public class CheckinOutRequestListServiceExportToFileController extends TriControllerSupport<FlowCheckInOutRequestListServiceExportToFileBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinOutRequestListServiceExportToFile;
	}

	@Override
	protected FlowCheckInOutRequestListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckInOutRequestListServiceExportToFileBean bean = new FlowCheckInOutRequestListServiceExportToFileBean();
		return bean;
	}


	@RequestMapping
	public String export(FlowCheckInOutRequestListServiceExportToFileBean bean, TriModel model) {
		String view= null;

		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = false;

		try {
			isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
			if(isDraft){
				view = TriView.CheckinOutRequestList.value()+"::resultDraftTable";
			}else{
				view = TriView.CheckinOutRequestList.value()+"::resultTable";
			}

			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);


		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/validate")
	public String validateExportFile(FlowCheckInOutRequestListServiceExportToFileBean bean, TriModel model,
									 HttpServletResponse response) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowCheckInOutRequestListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());


		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		return;
	}
	private void insertMapping(FlowCheckInOutRequestListServiceExportToFileBean bean, TriModel model)
		throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		if((RequestType.onChange).equals(param.getRequestType())){

			if(isDraft){
				SearchCondition searchCondition = bean.getParam().getSearchDraftCondition();

				bean.getParam().setSelectedDraft(true);
				searchCondition
					.setAreqCtgCd	( requestInfo.getParameter("draft_condTarget") )
					.setCtgId		( requestInfo.getParameter("draft_condCategory") )
					.setMstoneId	( requestInfo.getParameter("draft_condMilestone") )
					.setKeyword		( requestInfo.getParameter("draft_searchKeyword") )
				;
			}else{
				SearchCondition searchCondition = bean.getParam().getSearchCondition();

				bean.getParam().setSelectedDraft(false);
				searchCondition
					.setAreqCtgCd	( requestInfo.getParameter("condTarget") )
					.setCtgId		( requestInfo.getParameter("condCategory") )
					.setMstoneId	( requestInfo.getParameter("condMilestone") )
					.setStsId		( requestInfo.getParameter("condStatus") )
					.setKeyword		( requestInfo.getParameter("searchKeyword") )
				;
			}
		}

		if ((RequestType.submitChanges).equals(param.getRequestType())) {
				String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
				ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
				param.setExportBean(fileBean);
				ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
				param.setSubmitOption(option);
		}

	}

}


