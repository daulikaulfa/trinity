package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestRemovalServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestRemovalServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;

/**
 *
 * @version V4.00.00
 * @author Norheda
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/checkout/remove")
public class CheckOutRequestRemovalController extends TriControllerSupport<FlowCheckoutRequestRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckoutRequestRemovalService;
	}

	@Override
	protected FlowCheckoutRequestRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckoutRequestRemovalServiceBean bean = new FlowCheckoutRequestRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String delete(FlowCheckoutRequestRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/checkinout/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute("result", bean).addFlashAttribute("draft",bean.getResult().isRedirectToDraft());
		return view;
	}

	private void mapping(FlowCheckoutRequestRemovalServiceBean bean, TriModel model) {
		bean.getParam().setRequestType(RequestType.submitChanges);
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedAreqId(requestInfo.getParameter("areqId"));
	}

}
