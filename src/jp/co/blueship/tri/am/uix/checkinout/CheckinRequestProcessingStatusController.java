package jp.co.blueship.tri.am.uix.checkinout;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestProcessingStatusServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
/**
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
@Controller
@RequestMapping("/checkin")
public class CheckinRequestProcessingStatusController extends TriControllerSupport<FlowCheckinRequestProcessingStatusServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestProcessingStatusService;
	}

	@Override
	protected FlowCheckinRequestProcessingStatusServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestProcessingStatusServiceBean bean = new FlowCheckinRequestProcessingStatusServiceBean();
		return bean;
	}

	@RequestMapping(value = {"/progress"})
	public String checkinRequestProcessingStatus(FlowCheckinRequestProcessingStatusServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean,model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			bean.getParam().setRequestType(RequestType.onChange);
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		if ( model.getModel().asMap().get("flashMessage") != null ) {
				List<String> messageInfo = (List<String>)  model.getModel().asMap().get("flashMessage");
				bean.getMessageInfo().addFlashMessages(messageInfo);
		}

		model.getModel()
			.addAttribute("view", TriView.CheckinRequestProcessingStatus.value() )
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "checkinoutSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowCheckinRequestProcessingStatusServiceBean bean, TriModel model) {

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if ( null != model.getModel().asMap().get("areqId") ) {
				String areqId = model.getModel().asMap().get("areqId").toString();
				bean.getParam().setAreqId(areqId);
			}else if(model.getRequestInfo().getParameter("areqId") != null) {
				bean.getParam().setAreqId(model.getRequestInfo().getParameter("areqId"));
			}
		}
	}

}
