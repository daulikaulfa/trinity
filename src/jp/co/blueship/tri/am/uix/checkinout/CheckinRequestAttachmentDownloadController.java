package jp.co.blueship.tri.am.uix.checkinout;

import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestAttachmentDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;


@Controller
@RequestMapping("/checkin/download/attachment")
public class CheckinRequestAttachmentDownloadController extends TriControllerSupport<FlowCheckinRequestAttachmentDownloadServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestAttachmentDownloadService;
	}

	@Override
	protected FlowCheckinRequestAttachmentDownloadServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestAttachmentDownloadServiceBean bean = new FlowCheckinRequestAttachmentDownloadServiceBean();
		return bean;
	}

	@RequestMapping("/validate")
	public String validate(FlowCheckinRequestAttachmentDownloadServiceBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			bean.getParam().setRequestType(RequestType.validate);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping
	public void attachmentDL(HttpServletResponse  response,
							   FlowCheckinRequestAttachmentDownloadServiceBean bean,
							   TriModel model){

		try{
			this.mapping(bean,model);
			this.execute(this.getServiceId(), bean,model);

			if(bean.getResult().isCompleted()){
				StreamUtils.download(response, bean.getFileResponse().getFile());
			}

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);
	}

	private void mapping(FlowCheckinRequestAttachmentDownloadServiceBean bean,
						 TriModel model){

		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam()
			.setSelectedAreqId( requestInfo.getParameter("areqId") )
			.setSelectedFileNm( requestInfo.getParameter("fileNm") )
			.setSeqNo( requestInfo.getParameter("seqNo") );
	}
}
