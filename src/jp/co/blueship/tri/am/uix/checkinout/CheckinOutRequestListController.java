package jp.co.blueship.tri.am.uix.checkinout;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;;
/**
 *
 * @version V4.00.00
 * @author Sam
 *
 * @version V4.00.00
 * @author Yusna Marlina
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
@Controller
@RequestMapping("/checkinout")
public class CheckinOutRequestListController extends TriControllerSupport<FlowCheckInOutRequestListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinOutRequestListService;
	}

	@Override
	protected FlowCheckInOutRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckInOutRequestListServiceBean bean = new FlowCheckInOutRequestListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/error")
	public String error(FlowCheckInOutRequestListServiceBean bean, TriModel model ) {
		return index( bean , model.setRedirect(true));
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowCheckInOutRequestListServiceBean bean, TriModel model, HttpServletResponse response ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"","/","/list"})
	public String index(FlowCheckInOutRequestListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		Boolean isDraft = false;

		try {
			IRequestInfo requestInfo = model.getRequestInfo();
			if(model.getModel().asMap().get("draft") != null) {
				isDraft = (boolean)model.getModel().asMap().get("draft");
			}
			bean.getParam().setSelectedDraft(isDraft);
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( isDraft ){
			model.getModel().addAttribute("pagination",bean.getDraftPage());
		}else{
			model.getModel().addAttribute("pagination",bean.getPage());
		}
		
		

		model.getModel()
			.addAttribute("view", TriView.CheckinOutRequestList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "checkinoutSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/search")
	public String search(FlowCheckInOutRequestListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		Boolean isDraft = false;

		try{
			IRequestInfo requestInfo = model.getRequestInfo();
			isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( isDraft ){
			model.getModel().addAttribute("pagination",bean.getDraftPage());
		}else{
			model.getModel().addAttribute("pagination",bean.getPage());
		}

		model.getModel()
			.addAttribute("view", TriView.CheckinOutRequestList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "checkinoutSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}
	private void mapping(FlowCheckInOutRequestListServiceBean bean, TriModel model){

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		bean.getParam().setLinesPerPage(20);

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition(false) );
				bean.getParam().setSearchDraftCondition(bean.new SearchCondition(true) );
			}
			
			String sessionSelectedLotId = this.getSessionSelectedLot(sesInfo, bean);
			if (sessionSelectedLotId != null) {
				bean.getParam().setSelectedLotId(sessionSelectedLotId);
			}

		} else if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if (isDraft){
				bean.getParam().setSelectedDraft(true);

				String draftSelectedPageNo = requestInfo.getParameter("draft_pageNo");
				if (TriStringUtils.isNotEmpty(draftSelectedPageNo)) {
					searchDraftCondition.setSelectedPageNo(Integer.parseInt(draftSelectedPageNo));
				} else {
					searchDraftCondition.setSelectedPageNo(1);
				}

				searchDraftCondition
					.setAreqCtgCd(requestInfo.getParameter("draft_condTarget"))
					.setCtgId(requestInfo.getParameter("draft_condCategory"))
					.setMstoneId(requestInfo.getParameter("draft_condMilestone"))
					.setKeyword(requestInfo.getParameter("draft_searchKeyword"));

			} else {
				bean.getParam().setSelectedDraft(false);
				String selectedPageNo = requestInfo.getParameter("selectedPageNo");
				if (TriStringUtils.isNotEmpty(selectedPageNo)) {
					searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
				} else {
					searchCondition.setSelectedPageNo(1);
				}

				searchCondition
					.setAreqCtgCd(requestInfo.getParameter("condTarget"))
					.setCtgId(requestInfo.getParameter("condCategory"))
					.setMstoneId(requestInfo.getParameter("condMilestone"))
					.setStsId(requestInfo.getParameter("condStatus"))
					.setKeyword(requestInfo.getParameter("searchKeyword"));
			}
		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowCheckInOutRequestListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		Boolean isDraft = bean.getParam().isSelectedDraft();

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() );

			if(isDraft){
				searchFilterBean.getParam().getInputInfo()
					.setSearchFilter( bean.getParam().getSearchDraftCondition() );
			}else{
				searchFilterBean.getParam().getInputInfo()
				.setSearchFilter( bean.getParam().getSearchCondition() );
			}

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowCheckInOutRequestListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();
		Boolean isDraft = false;

		try {
			model.setRedirect(true);
			String json = null;

			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowCheckInOutRequestListServiceBean.SearchCondition condition = gson.fromJson(json, FlowCheckInOutRequestListServiceBean.SearchCondition.class);

			isDraft = condition.isDraft();

			if( isDraft ){
				bean.getParam()
					.setSelectedDraft(true)
					.setSearchDraftCondition(condition)
					.getSearchDraftCondition().setSelectedPageNo(1);
			}else{
				bean.getParam()
					.setSelectedDraft(false)
					.setSearchCondition(condition)
					.getSearchCondition().setSelectedPageNo(1);
			}

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( isDraft ){
			model.getModel()
				.addAttribute("pagination",bean.getDraftPage())
				.addAttribute("selectedTabId"  , "1");
		}else{
			model.getModel()
				.addAttribute("pagination",bean.getPage())
				.addAttribute("selectedTabId"  , "0");
		}

		model.getModel()
			.addAttribute("view", TriView.CheckinOutRequestList.value())
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "checkinoutSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}
}
