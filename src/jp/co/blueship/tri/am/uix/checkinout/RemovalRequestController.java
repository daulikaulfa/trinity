package jp.co.blueship.tri.am.uix.checkinout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IRemovalResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.RemovalRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckoutDetailResourceComparator;
import jp.co.blueship.tri.fw.cmn.utils.comparator.RemovalResourceComparator;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestController extends TriControllerSupport<FlowRemovalRequestServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestService;
	}

	@Override
	protected FlowRemovalRequestServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestServiceBean bean = new FlowRemovalRequestServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create/refreshCategory")
	public String refreshCategory( @ModelAttribute("ctgId") String ctgId, FlowRemovalRequestServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshCategory);
		bean.getParam().getInputInfo().setCtgId( ctgId );
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/create/refreshMilestone")
	public String refreshMilestone( @ModelAttribute("mstoneId") String mstoneId, FlowRemovalRequestServiceBean bean, TriModel model ){
		bean.getParam().setRequestType(RequestType.onChange);
		bean.getParam().setRequestOption(RequestOption.refreshMilestone);
		bean.getParam().getInputInfo().setMstoneId( mstoneId );
		return index( bean , model.setRedirect(true) );
	}
	
	@RequestMapping(value="/create/listfile")
	public String listFile(FlowRemovalRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowRemovalRequestServiceBean returnBean =	new FlowRemovalRequestServiceBean();
		String startIndexStr = requestInfo.getParameter("startIndex");
		String endIndexStr = requestInfo.getParameter("endIndex");
		if (TriStringUtils.isDigits(startIndexStr) && TriStringUtils.isDigits(endIndexStr)){
			int startIndex = Integer.parseInt(startIndexStr);
			int endIndex = Integer.parseInt(endIndexStr);
			List<IRemovalResourceViewBean> listFiles = bean.getResourceSelectionFolderView().getRequestViews();
			if (endIndex > listFiles.size()) endIndex = listFiles.size();
			if ( startIndex <= endIndex ){
				List<IRemovalResourceViewBean> subListFiles = listFiles.subList(startIndex, endIndex);
				returnBean.getResourceSelectionFolderView().setRequestViews(subListFiles);
			}
		}
		
		
		model.getModel()
		.addAttribute("result", returnBean)
		.addAttribute("view", TriView.RemovalRequestCreation.value());
		model.getModel().addAttribute("requestViewList", returnBean.getResourceSelectionFolderView().getRequestViews());
		return "RemovalRequestCreation :: table[@id='fileViewTable']";
	}
	
	@RequestMapping(value="/create/sortfile")
	public String sortfile(FlowRemovalRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String columnName = requestInfo.getParameter("columnName");
		String sortOrder = requestInfo.getParameter("sortOrder");
		RemovalResourceComparator resoureComparator = new RemovalResourceComparator(columnName, sortOrder);
		Collections.sort(bean.getResourceSelectionFolderView().getRequestViews(),resoureComparator);
		if(sortOrder.equalsIgnoreCase("descending")){
			Collections.reverse(bean.getResourceSelectionFolderView().getRequestViews());
		}
		List<IRemovalResourceViewBean> tempListView =  new ArrayList<IRemovalResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		
		model.getModel()
		.addAttribute("result", bean)
		.addAttribute("view", TriView.RemovalRequestCreation.value());
		model.getModel().addAttribute("requestViewList", tempListView);
		return "RemovalRequestCreation :: table[@id='fileViewTable']";
	}

	@RequestMapping(value = { "/create" })
	public String index(FlowRemovalRequestServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute("result", bean);
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/removal/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.RemovalRequestCreation.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
		;
		

		List<IRemovalResourceViewBean> tempListView =  new ArrayList<IRemovalResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		model.getModel().addAttribute("requestViewList", tempListView);

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		setPrev(model);
		return view;
	}
	
	@RequestMapping(value = { "/create/singleResource" })
	@ResponseBody
	public String singleResource(FlowRemovalRequestServiceBean bean, TriModel model) {

		try {

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
		}
		
		return "";
		
	}

	@RequestMapping(value="/create/upload")
	public String upload(@RequestParam("userFile") MultipartFile userFile, FlowRemovalRequestServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.uploadFile(userFile, bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("view", TriView.RemovalRequestCreation.value());
		return view;
	}

	private void mapping(FlowRemovalRequestServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		ResourceSelection resourceSelection = bean.getParam().getResourceSelection();
		RemovalRequestEditInputBean inputBean = bean.getParam().getInputInfo();
		ResourceSelectionFolderView<IRemovalResourceViewBean> selectionFolder = bean.getResourceSelectionFolderView();

		LogHandler.debug( TriLogFactory.getInstance(), "requestType:=" + bean.getParam().getRequestType());
		LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + requestInfo.getParameter("requestOption"));
		LogHandler.debug( TriLogFactory.getInstance(), "resourceRequestType:=" + requestInfo.getParameter("resourceRequestType"));
		LogHandler.debug( TriLogFactory.getInstance(), "path:=" + requestInfo.getParameter("path"));
		LogHandler.debug( TriLogFactory.getInstance(), "selectedFiles:=" + requestInfo.getParameter("selectedFiles"));

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().setRequestOption(RequestOption.selectResource);
			bean.getParam().getResourceSelection().setPath("");
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if (!model.isRedirect()) {
				bean.getParam().setRequestOption(RequestOption.value( requestInfo.getParameter("requestOption") ));
				resourceSelection.setPath(requestInfo.getParameter("path"));
			}
			LogHandler.debug( TriLogFactory.getInstance(), "requestOption:=" + bean.getParam().getRequestOption());


			if(RequestOption.selectGroup.equals( bean.getParam().getRequestOption())){
				inputBean.setGroupId(requestInfo.getParameter("groupId"));
			}

			if(RequestOption.selectAssignee.equals( bean.getParam().getRequestOption())){
				inputBean.setAssigneeId(requestInfo.getParameter("assigneeId"));
			}
			if(RequestOption.selectResource.equals( bean.getParam().getRequestOption())){

				if( ResourceSelection.ResourceRequestType.selectFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
					resourceSelection.setSelectedFiles(requestInfo.getParameterValues("selectedFiles"));

				} else if( ResourceSelection.ResourceRequestType.openFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);

				} else if( ResourceSelection.ResourceRequestType.closeFolder.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
				} else if( ResourceSelection.ResourceRequestType.selectSingleResource.equals(requestInfo.getParameter("resourceRequestType")) ){

					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectSingleResource);
					Set<String> selectedResources = resourceSelection.getSelectedFileSet();
					String resource = requestInfo.getParameter("selectedResource");
					if (null != requestInfo.getParameter("isSelected") && !Boolean.parseBoolean(requestInfo.getParameter("isSelected"))) {
						if ( TriStringUtils.isNotEmpty( resource ) ) {
							selectedResources = AmResourceSelectionUtils.unselectResource(resource, selectedResources);
						}
					}else {
						selectedResources.add(resource);
					}
					
					resourceSelection.setSelectedFiles(selectedResources.toArray(new String[selectedResources.size()]));
				} else if( ResourceSelection.ResourceRequestType.selectAllResource.equals(requestInfo.getParameter("resourceRequestType")) ){
					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectAllResource);
					String selectedPath = requestInfo.getParameter("selectedPath");
					if ( TriStringUtils.isNotEmpty(selectedPath) && selectedPath.equalsIgnoreCase(selectionFolder.getSelectedPath()) ){
						if (null != requestInfo.getParameter("isSelected") ) {
							resourceSelection.setSelectAllFilesInFolder(Boolean.parseBoolean(requestInfo.getParameter("isSelected")));
						}
					}
				}
			}
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			inputBean.setPjtId( requestInfo.getParameter("pjtId"))
				.setGroupId( requestInfo.getParameter("groupId"))
				.setAssigneeId( requestInfo.getParameter("assigneeId"))
				.setContents( requestInfo.getParameter("contents"))
				.setSubject( requestInfo.getParameter("subject"))
				.setCtgId( requestInfo.getParameter("category"))
				.setMstoneId( requestInfo.getParameter("milestone"))
				.setResourceSelection( AmResourceSelectionUtils.treeMode.equals(requestInfo.getParameter("resourceMode"))? true : false)
				.setSubmitMode( SubmitMode.value( requestInfo.getParameter("submitMode")));

			resourceSelection.setSelectedFiles(new String[0]);
			resourceSelection.setPath(TriStringUtils.isEmpty(requestInfo.getParameter("path"))? "" : requestInfo.getParameter("path"));
		}

	}

	private void uploadFile(MultipartFile userFile, FlowRemovalRequestServiceBean bean,
			TriModel model) throws IOException {
		RemovalRequestEditInputBean removalInfo = bean.getParam().getInputInfo();
		 jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestServiceBean.RequestParam param =  bean.getParam();

		param.setRequestOption(RequestOption.fileUpload);
		byte[] fileBytes = new byte[] {};
		if (!userFile.isEmpty()) {
			fileBytes = userFile.getBytes();
			removalInfo.setCsvInputStreamBytes(fileBytes);
			removalInfo.setCsvFileNm(userFile.getOriginalFilename());
		}
	}

}
