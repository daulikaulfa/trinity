package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestDetailsServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/removal/details/print")
public class PrintRemovalRequestDetailsController extends TriControllerSupport<FlowRemovalRequestDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintRemovalRequestDetailsService;
	}

	@Override
	protected FlowRemovalRequestDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestDetailsServiceBean bean = new FlowRemovalRequestDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String removalRequestDetails(FlowRemovalRequestDetailsServiceBean bean,
										TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintRemovalRequestDetails.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowRemovalRequestDetailsServiceBean bean, TriModel model ) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		param.setSelectedAreqId(requestInfo.getParameter("areqId"));
	}
}
