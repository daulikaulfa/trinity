package jp.co.blueship.tri.am.uix.checkinout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceDetailsViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestDetailsServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestDetailsServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckinDetailResourceComparator;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckoutDetailResourceComparator;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
@Controller
@RequestMapping("/checkin")
public class CheckinRequestDetailsController extends TriControllerSupport<FlowCheckinRequestDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestDetailsService;
	}

	@Override
	protected FlowCheckinRequestDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestDetailsServiceBean bean = new FlowCheckinRequestDetailsServiceBean();
		return bean;
	}
	
	@RequestMapping(value="/details/listfile")
	public String listFile(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowCheckinRequestDetailsServiceBean returnBean =	new FlowCheckinRequestDetailsServiceBean();

		String startIndexStr = requestInfo.getParameter("startIndex");
		String endIndexStr = requestInfo.getParameter("endIndex");
		if (TriStringUtils.isDigits(startIndexStr) && TriStringUtils.isDigits(endIndexStr)){
			int startIndex = Integer.parseInt(startIndexStr);
			int endIndex = Integer.parseInt(endIndexStr);
			List<ICheckinResourceDetailsViewBean> listFiles = bean.getResourceSelectionFolderView().getRequestViews();
			if (endIndex > listFiles.size()) endIndex = listFiles.size();
			if ( startIndex <= endIndex ){
				List<ICheckinResourceDetailsViewBean> subListFiles = listFiles.subList(startIndex, endIndex);
				returnBean.getResourceSelectionFolderView().setRequestViews(subListFiles);
			}
		}
		
		
		model.getModel()
		.addAttribute("result", returnBean)
		.addAttribute("view", TriView.CheckinRequestDetails.value());
		model.getModel().addAttribute("requestViewList", returnBean.getResourceSelectionFolderView().getRequestViews());
		return "CheckInRequestDetails :: table[@id='fileViewTable']";
	}
	
	@RequestMapping(value="/details/sortfile")
	public String sortfile(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String columnName = requestInfo.getParameter("columnName");
		String sortOrder = requestInfo.getParameter("sortOrder");
		CheckinDetailResourceComparator resoureComparator = new CheckinDetailResourceComparator(columnName, sortOrder);
		Collections.sort(bean.getResourceSelectionFolderView().getRequestViews(),resoureComparator);
		if(sortOrder.equalsIgnoreCase("descending")){
			Collections.reverse(bean.getResourceSelectionFolderView().getRequestViews());
		}
		List<ICheckinResourceDetailsViewBean> tempListView =  new ArrayList<ICheckinResourceDetailsViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		
		model.getModel()
		.addAttribute("result", bean)
		.addAttribute("view", TriView.CheckinRequestDetails.value());
		model.getModel().addAttribute("requestViewList", tempListView);
		return "CheckInRequestDetails :: table[@id='fileViewTable']";
	}
	

	@RequestMapping(value = {"/details"})
	public String checkinRequestCreation(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean,model);
			this.execute(getServiceId(), bean, model);
//			model.getSessionInfo().setAttribute(TriSessionAttributes.SelectedLotId.value(), bean.getDetailsView().getLotId());
			model.getSessionInfo().setAttribute(
					this.getSessionKey(bean.getHeader().getWindowsId(), TriSessionAttributes.SelectedLotId.value()), bean.getDetailsView().getLotId());

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel().addAttribute("json", json);
		model.getModel().addAttribute("view", TriView.CheckinRequestDetails.value() );
		model.getModel().addAttribute("selectedMenu"  , "changeMenu");
		model.getModel().addAttribute("selectedSubMenu"  , "checkinoutSubmenu");
		model.getModel().addAttribute("result", bean);
		List<ICheckinResourceDetailsViewBean> tempListView =  new ArrayList<ICheckinResourceDetailsViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		model.getModel().addAttribute("requestViewList", tempListView);
		
		setPrev(model);
		return view;
	}

	private void mapping(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if (requestInfo.getParameter("areqId") != null) {
				bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
			}

			bean.getParam().getResourceSelection().setPath("");
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ) {
			bean.getParam().getResourceSelection().setPath(requestInfo.getParameter("path"));
			if( ResourceSelection.ResourceRequestType.selectFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
				bean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.selectFolder);
				bean.getParam().getResourceSelection().setSelectedFiles(requestInfo.getParameterValues("selectedFiles"));
			}else if( ResourceSelection.ResourceRequestType.openFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
				bean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.openFolder);
			}else if( ResourceSelection.ResourceRequestType.closeFolder.equals(requestInfo.getParameter("resourceRequestType")) ){
				bean.getParam().getResourceSelection().setType(ResourceSelection.ResourceRequestType.closeFolder);
			}
			
			if( "list".equalsIgnoreCase(requestInfo.getParameter("viewMode")) ){
				bean.getParam().getInputInfo().setFolderTreeFormat(false);
			}else {
				bean.getParam().getInputInfo().setFolderTreeFormat(true);
			}
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

		}

	}


}
