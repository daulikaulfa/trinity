package jp.co.blueship.tri.am.uix.checkinout;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.google.gson.JsonObject;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckoutRequestServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.CheckinRequestInputInfo;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.RequestOption;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckinResourceComparator;
import jp.co.blueship.tri.fw.cmn.utils.comparator.CheckoutResourceComparator;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
@Controller
@RequestMapping("/checkin")
public class CheckinRequestController extends TriControllerSupport<FlowCheckinRequestServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestService;
	}

	@Override
	protected FlowCheckinRequestServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestServiceBean bean = new FlowCheckinRequestServiceBean();
		return bean;
	}

	@RequestMapping(value = { "/", "/create", "" })
	public String checkinRequestCreation(FlowCheckinRequestServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				view = "redirect:/checkin/progress";
				model.getRedirectAttributes().addAttribute("areqId", bean.getDetailsView().getAreqId());
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();

		model.getModel().addAttribute("json", json);
		model.getModel().addAttribute("view", TriView.CheckinRequestCreation.value());
		model.getModel().addAttribute("selectedMenu", "changeMenu");
		model.getModel().addAttribute("selectedSubMenu", "checkinoutSubmenu");
		model.getModel().addAttribute("result", bean);
		
		List<ICheckinResourceViewBean> tempListView =  new ArrayList<ICheckinResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		model.getModel().addAttribute("requestViewList", tempListView);
		

		if (bean.getResult().isCompleted()) {
			bean.getParam().setRequestType(RequestType.init);
			model.getRedirectAttributes().addFlashAttribute("result",bean);
			model.getRedirectAttributes().addFlashAttribute("flashMessage",bean.getMessageInfo().getFlashMessages());
		}
		setPrev(model);
		return view;
	}
	
	@RequestMapping(value="/create/listfile")
	public String listFile(FlowCheckinRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowCheckinRequestServiceBean returnBean =	new FlowCheckinRequestServiceBean();
		String startIndexStr = requestInfo.getParameter("startIndex");
		String endIndexStr = requestInfo.getParameter("endIndex");
		if (TriStringUtils.isDigits(startIndexStr) && TriStringUtils.isDigits(endIndexStr)){
			int startIndex = Integer.parseInt(startIndexStr);
			int endIndex = Integer.parseInt(endIndexStr);
			List<ICheckinResourceViewBean> listFiles = bean.getResourceSelectionFolderView().getRequestViews();
			if (endIndex > listFiles.size()) endIndex = listFiles.size();
			if ( startIndex <= endIndex ){
				List<ICheckinResourceViewBean> subListFiles = listFiles.subList(startIndex, endIndex);
				returnBean.getResourceSelectionFolderView().setRequestViews(subListFiles);
			}
		}
		
		
		model.getModel()
		.addAttribute("result", returnBean)
		.addAttribute("view", TriView.CheckinRequestCreation.value());
		model.getModel().addAttribute("requestViewList", returnBean.getResourceSelectionFolderView().getRequestViews());
		return "CheckInRequestCreation :: table[@id='fileViewTable']";
	}
	
	@RequestMapping(value="/create/sortfile")
	public String sortfile(FlowCheckinRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String columnName = requestInfo.getParameter("columnName");
		String sortOrder = requestInfo.getParameter("sortOrder");
		CheckinResourceComparator resoureComparator = new CheckinResourceComparator(columnName, sortOrder);
		Collections.sort(bean.getResourceSelectionFolderView().getRequestViews(),resoureComparator);
		if(sortOrder.equalsIgnoreCase("descending")){
			Collections.reverse(bean.getResourceSelectionFolderView().getRequestViews());
		}
		List<ICheckinResourceViewBean> tempListView =  new ArrayList<ICheckinResourceViewBean>();
		if ( TriCollectionUtils.isNotEmpty(bean.getResourceSelectionFolderView().getRequestViews())){
			if (bean.getResourceSelectionFolderView().getRequestViews().size() > 30 ){
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0, 30);
			}else {
				tempListView = bean.getResourceSelectionFolderView().getRequestViews().subList(0,  
						bean.getResourceSelectionFolderView().getRequestViews().size());
			}
		}
		
		model.getModel()
		.addAttribute("result", bean)
		.addAttribute("view", TriView.CheckinRequestCreation.value());
		model.getModel().addAttribute("requestViewList", tempListView);
		return "CheckInRequestCreation :: table[@id='fileViewTable']";
	}
	

	@RequestMapping(value="/create/upload")
	public @ResponseBody String upload(@RequestParam("userFile") MultipartFile userFile, FlowCheckinRequestServiceBean bean, TriModel model) {
		try {
			this.uploadFile(userFile, bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		JsonObject json = new JsonObject();
		json.addProperty("seqNo", bean.getParam().getInputInfo().getAttachmentFileSeqNo());
		return json.toString();
	}

	@RequestMapping(value="/create/upload/delete")
	public @ResponseBody String delete(FlowCheckinRequestServiceBean bean, TriModel model) {
		String message = "success";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	private void uploadFile(MultipartFile userFile, FlowCheckinRequestServiceBean bean, TriModel model) throws IOException {
		CheckinRequestInputInfo checkinInfo = bean.getParam().getInputInfo();
		jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestServiceBean.RequestParam param = bean.getParam();

		param.setRequestOption(RequestOption.fileUpload);
		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			checkinInfo.setAttachmentInputStreamBytes(fileBytes);
			checkinInfo.setAttachmentFileNm(userFile.getOriginalFilename());
		}
	}


	private void mapping(FlowCheckinRequestServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ResourceSelection resourceSelection = bean.getParam().getResourceSelection();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			if (requestInfo.getParameter("areqId") != null) {
				bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
			}

			bean.getParam().setRequestOption(RequestOption.selectResource);
			bean.getParam().getResourceSelection().setPath("");
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			bean.getParam().setRequestOption(RequestOption.value(requestInfo.getParameter("requestOption")));
			bean.getParam().getResourceSelection().setPath(requestInfo.getParameter("path"));

			if (RequestOption.selectResource.equals(bean.getParam().getRequestOption())) {
				if (ResourceSelection.ResourceRequestType.selectFolder
						.equals(requestInfo.getParameter("resourceRequestType"))) {
					resourceSelection.setType(ResourceSelection.ResourceRequestType.selectFolder);
					resourceSelection.setSelectedFiles(requestInfo.getParameterValues("selectedFiles"));
				} else if (ResourceSelection.ResourceRequestType.openFolder
						.equals(requestInfo.getParameter("resourceRequestType"))) {
					resourceSelection.setType(ResourceSelection.ResourceRequestType.openFolder);
				} else if (ResourceSelection.ResourceRequestType.closeFolder
						.equals(requestInfo.getParameter("resourceRequestType"))) {
					resourceSelection.setType(ResourceSelection.ResourceRequestType.closeFolder);
				} 
			}

			if(RequestOption.deleteUplodedFile.equals(bean.getParam().getRequestOption())) {
				bean.getParam().getInputInfo().setAttachmentFileNm(requestInfo.getParameter("deletedFileNm"));
				bean.getParam().getInputInfo().setAttachmentFileSeqNo(requestInfo.getParameter("seqNo"));
			}
			
			if ("list".equals(requestInfo.getParameter("viewMode"))) {
				bean.getParam().getInputInfo().setFolderTreeFormat(false);
			}else {
				bean.getParam().getInputInfo().setFolderTreeFormat(true);
			}
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			bean.getParam().setRequestType(RequestType.submitChanges);
		}

	}

}
