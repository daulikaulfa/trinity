package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRequestResourceFileComparisonServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/checkin/diff/print")
public class PrintRequestResourceFileComparisonController extends TriControllerSupport<FlowRequestResourceFileComparisonServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintRequestResourceFileComparisonService;
	}

	@Override
	protected FlowRequestResourceFileComparisonServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRequestResourceFileComparisonServiceBean bean = new FlowRequestResourceFileComparisonServiceBean();
		return bean;
	}

	@RequestMapping
	public String print(FlowRequestResourceFileComparisonServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintCheckInFileDifferences.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping(FlowRequestResourceFileComparisonServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedAreqId( requestInfo.getParameter("areqId") );
		bean.getParam().setSelectedResourcePath( requestInfo.getParameter("resourcePath") );
	}
}
