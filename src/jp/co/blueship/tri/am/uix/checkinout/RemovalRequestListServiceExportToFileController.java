package jp.co.blueship.tri.am.uix.checkinout;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceExportToFileBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceExportToFileBean.RequestParam;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
@Controller
@RequestMapping("/removal/export")
public class RemovalRequestListServiceExportToFileController
		extends TriControllerSupport<FlowRemovalRequestListServiceExportToFileBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestListServiceExportToFile;
	}

	@Override
	protected FlowRemovalRequestListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestListServiceExportToFileBean bean = new FlowRemovalRequestListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export(FlowRemovalRequestListServiceExportToFileBean bean, TriModel model) {
		String view= null;
		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		if(isDraft){
			 view = TriView.RemovalRequestList.value()+"::resultDraftTable";
		}else{
			 view = TriView.RemovalRequestList.value()+"::resultTable";
		}
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}


		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/validate")
	public String validate(FlowRemovalRequestListServiceExportToFileBean bean, TriModel model,
									 HttpServletResponse response) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowRemovalRequestListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {
		try {
			this.insertMapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		return;
	}

	private void insertMapping(FlowRemovalRequestListServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));

		if((RequestType.onChange).equals(param.getRequestType())){

			if(isDraft){
				SearchCondition searchCondition = bean.getParam().getSearchDraftCondition();
				bean.getParam().setSelectedDraft(true);
				searchCondition
					.setCtgId		( requestInfo.getParameter("draft_ctdId") )
					.setMstoneId	( requestInfo.getParameter("draft_mstoneId") )
					.setStsId		( requestInfo.getParameter("draft_stsId") )
					.setKeyword		( requestInfo.getParameter("draft_keyword") )
				;
			}else{
				SearchCondition searchCondition = bean.getParam().getSearchCondition();
				bean.getParam().setSelectedDraft(false);
				searchCondition
					.setCtgId		( requestInfo.getParameter("ctgId") )
					.setMstoneId	( requestInfo.getParameter("mstoneId") )
					.setStsId		( requestInfo.getParameter("stsId") )
					.setKeyword		( requestInfo.getParameter("keyword") )
				;
			}

		}
		if ((RequestType.submitChanges).equals(param.getRequestType())) {
			String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
			ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
			param.setExportBean(fileBean);
			ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
			param.setSubmitOption(option);
		}
	}
}
