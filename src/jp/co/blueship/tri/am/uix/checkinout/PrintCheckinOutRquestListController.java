package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/checkinout/list/print")
public class PrintCheckinOutRquestListController extends TriControllerSupport<FlowCheckInOutRequestListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintCheckinOutRequestListService;
	}

	@Override
	protected FlowCheckInOutRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckInOutRequestListServiceBean bean = new FlowCheckInOutRequestListServiceBean();
		return bean;
	}

	@RequestMapping
	public String print( FlowCheckInOutRequestListServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintCheckInOutRequestList.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping( FlowCheckInOutRequestListServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();

		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();

		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		bean.getParam().setShowAll(true);

		if (isDraft) {
			bean.getParam().setSelectedDraft(true);

			searchDraftCondition.setAreqCtgCd(requestInfo.getParameter("draft_condTarget"))
								.setCtgId(requestInfo.getParameter("draft_condCategory"))
								.setMstoneId(requestInfo.getParameter("draft_condMilestone"))
								.setKeyword(requestInfo.getParameter("draft_searchKeyword"))
								;

		} else {
			bean.getParam().setSelectedDraft(false);

			searchCondition.setAreqCtgCd( requestInfo.getParameter("condTarget"))
						.setCtgId		( requestInfo.getParameter("condCategory"))
						.setMstoneId	( requestInfo.getParameter("condMilestone"))
						.setStsId		( requestInfo.getParameter("condStatus"))
						.setKeyword		( requestInfo.getParameter("searchKeyword"))
						;
		}
	}
}
