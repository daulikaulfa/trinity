package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean.RequestParam;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Maksm
 *
 */
@Controller
@RequestMapping("/reports")
public class ReportAssetRegisterCreationController extends TriControllerSupport<FlowReportAssetRegisterCreationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportAssetRegisterCreationService;
	}

	@Override
	protected FlowReportAssetRegisterCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportAssetRegisterCreationServiceBean bean = new FlowReportAssetRegisterCreationServiceBean();
		return bean;
	}

	@RequestMapping("/create")
	public String create(FlowReportAssetRegisterCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/reports/redirect";
			}
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("result", bean)
			.addAttribute("selectedMenu", "reportsMenu")
			.addAttribute("view", TriView.ReportCreation.value())
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReportAssetRegisterCreationServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();
		SearchCondition searchCondition = param.getSearchCondition();

		String lotId = this.getSessionSelectedLot(sesInfo, bean);

		bean.getParam().setSelectedLotId(lotId);
		if(RequestType.submitChanges.equals(param.getRequestType())) {
			searchCondition
				.setModuleNm	( requestInfo.getParameterValues("moduleNm[]") )
				.setPjtId		( requestInfo.getParameterValues("pjtId[]") )
				.setReferenceId	( requestInfo.getParameterValues("referenceId[]") )
				.setSubmitterId	( requestInfo.getParameterValues("submitterId[]") )
				.setGroupId		( requestInfo.getParameterValues("groupId[]") )
				.setStsId		( requestInfo.getParameterValues("stsId[]") )
				.setProcStsId	( requestInfo.getParameterValues("procStsId[]") )
				.setFilePath	( requestInfo.getParameter("filePath") )
			;

			boolean targetMW = "false".equals(requestInfo.getParameter("targetMW"));

			if(targetMW) {
				searchCondition.setTargetMW(false);
			}
		}
	}

}
