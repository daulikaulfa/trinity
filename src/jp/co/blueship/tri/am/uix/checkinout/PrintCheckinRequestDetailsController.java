package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author thang.vu
 */
@Controller
@RequestMapping("/checkin/details/print")
public class PrintCheckinRequestDetailsController extends TriControllerSupport<FlowCheckinRequestDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmPrintCheckinRequestDetailsService;
	}

	@Override
	protected FlowCheckinRequestDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestDetailsServiceBean bean = new FlowCheckinRequestDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String checkinRequestCreation(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean,model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		String json = bean.getResourceSelectionFolderView().toJsonFromFolderView();
		model.getModel()
			.addAttribute("json", json)
			.addAttribute("view", TriView.PrintCheckInRequestDetails.value() )
			.addAttribute("selectedMenu"  , "changeMenu")
			.addAttribute("selectedSubMenu"  , "checkinoutSubmenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowCheckinRequestDetailsServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
			bean.getParam().getResourceSelection().setPath("");
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ) {

		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {

		}

	}


}
