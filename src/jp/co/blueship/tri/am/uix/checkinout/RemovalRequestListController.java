package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Norheda
 *
 */
@Controller
@RequestMapping("/removal")
public class RemovalRequestListController extends TriControllerSupport<FlowRemovalRequestListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmRemovalRequestListService;
	}

	@Override
	protected FlowRemovalRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowRemovalRequestListServiceBean bean = new FlowRemovalRequestListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/error")
	public String error(FlowRemovalRequestListServiceBean bean,
			TriModel model) {
		return index(bean, model.setRedirect(true));
	}

	@RequestMapping(value = "/refresh")
	public String refresh(FlowRemovalRequestListServiceBean bean, TriModel model){
		String view = TriTemplateView.MainTemplate.value();
		try{
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().setRequestType(RequestType.onChange);
			this.execute(getServiceId(), bean , model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RemovalRequestList.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
		;

		if (bean.getParam().isSelectedDraft()) {
			model.getModel().addAttribute("pagination",bean.getDraftPage());
		} else {
			model.getModel().addAttribute("pagination",bean.getPage());
		}
		model.getModel().addAttribute("result" , bean);
		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/redirect")
	public String redirect(FlowRemovalRequestListServiceBean bean,
			TriModel model) {
		return index(bean, model.setRedirect(true));
	}

	@RequestMapping(value = {"/","","/list"})
	public String index(FlowRemovalRequestListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition(false) );
				bean.getParam().setSearchDraftCondition(bean.new SearchCondition(true) );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().setRequestType(RequestType.init);
			
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.RemovalRequestList.value())
			.addAttribute("selectedMenu", "changeMenu")
			.addAttribute("selectedSubMenu", "resourcesSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result" , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/search")
	public String search(FlowRemovalRequestListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try{
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		if( isDraft ){
			model.getModel().addAttribute("pagination",bean.getDraftPage());
		}else{
			model.getModel().addAttribute("pagination",bean.getPage());
		}

		model.getModel().addAttribute("view", TriView.RemovalRequestList.value());
		model.getModel().addAttribute("selectedMenu", "changeMenu");
		model.getModel().addAttribute("selectedSubMenu", "removalSubMenu");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}

	private void mapping (FlowRemovalRequestListServiceBean bean, TriModel model) {

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();

		if(RequestType.init.equals(bean.getParam().getRequestType())){
			
			bean.getParam().setSelectedDraft(false);
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		Boolean isDraft = null;
		if (null == requestInfo.getParameter("draft")) {
			isDraft = bean.getParam().isSelectedDraft();
		} else {
			isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if(isDraft) {
				bean.getParam().setSelectedDraft(true);
				String draftPageNo = requestInfo.getParameter("selectedDraftPageNo");
				if(TriStringUtils.isNotEmpty(draftPageNo)){
					searchDraftCondition.setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedDraftPageNo")));
				} else {
					searchDraftCondition.setSelectedPageNo(1);
				}
				searchDraftCondition.setCtgId(requestInfo.getParameter("draft_condCategory"));
				searchDraftCondition.setMstoneId(requestInfo.getParameter("draft_condMilestone"));
				searchDraftCondition.setStsId(requestInfo.getParameter("draft_condStatus"));
				searchDraftCondition.setKeyword(requestInfo.getParameter("draft_searchKeyword"));
			}else{
				bean.getParam().setSelectedDraft(false);
				String selectedPageNo = requestInfo.getParameter("selectedPageNo");
				if(TriStringUtils.isNotEmpty(selectedPageNo)){
					searchCondition.setSelectedPageNo(Integer.parseInt(requestInfo.getParameter("selectedPageNo")));
				} else {
					searchCondition.setSelectedPageNo(1);
				}
				searchCondition.setCtgId(requestInfo.getParameter("condCategory"));
				searchCondition.setMstoneId(requestInfo.getParameter("condMilestone"));
				searchCondition.setStsId(requestInfo.getParameter("condStatus"));
				searchCondition.setKeyword(requestInfo.getParameter("searchKeyword"));
			}
		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowRemovalRequestListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		Boolean isDraft = bean.getParam().isSelectedDraft();

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() );

			if(isDraft){
				searchFilterBean.getParam().getInputInfo()
					.setSearchFilter( bean.getParam().getSearchDraftCondition());
			}else{
				searchFilterBean.getParam().getInputInfo()
				.setSearchFilter( bean.getParam().getSearchCondition());
			}

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowRemovalRequestListServiceBean bean, TriModel model ){
		model.setRedirect(true);
		String json = null;
		if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
			json = model.valueOf(TriModelAttributes.RedirectFilter);
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		FlowRemovalRequestListServiceBean.SearchCondition condition = gson.fromJson(json, FlowRemovalRequestListServiceBean.SearchCondition.class);

		Boolean isDraft = condition.isDraft();
		if( isDraft ){
			bean.getParam().setSelectedDraft(true);
			bean.getParam().setSearchDraftCondition(condition);
			bean.getParam().getSearchDraftCondition().setSelectedPageNo(1);
		} else {
			bean.getParam().setSelectedDraft(false);
			bean.getParam().setSearchCondition(condition);
			bean.getParam().getSearchCondition().setSelectedPageNo(1);
		}

		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if( isDraft ){
			model.getModel().addAttribute("pagination",bean.getDraftPage());
			model.getModel().addAttribute("selectedTabId"  , "1");
		}else{
			model.getModel().addAttribute("pagination",bean.getPage());
			model.getModel().addAttribute("selectedTabId"  , "0");
		}

		model.getModel().addAttribute("view", TriView.RemovalRequestList.value());
		model.getModel().addAttribute("selectedMenu"  , "changeMenu");
		model.getModel().addAttribute("selectedSubMenu"  , "resourcesSubmenu");
		model.getModel().addAttribute("result", bean);

		setPrev(model);
		return view;
	}
}
