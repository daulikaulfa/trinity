package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;

@RequestMapping("/checkin/remove")
public class CheckinRequestRemovalController extends TriControllerSupport<FlowCheckinRequestRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestRemovalService;
	}

	@Override
	protected FlowCheckinRequestRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestRemovalServiceBean bean = new FlowCheckinRequestRemovalServiceBean();
		return bean;
	}


	@RequestMapping
	public String removal(FlowCheckinRequestRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/checkinout/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean,model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	@RequestMapping(value = "/validate")
	public String removalValidate(FlowCheckinRequestRemovalServiceBean bean, TriModel model) {
		String view = "common/Comment::checkinRequestRemoveComment";

		try {
			this.mapping(bean,model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute( "result", bean );
		return view;
	}


	private void mapping(FlowCheckinRequestRemovalServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowCheckinRequestRemovalServiceBean.RequestParam param = bean.getParam();
		param.setSelectedAreqId(requestInfo.getParameter("areqId"));
		param.getInputInfo().setComment(requestInfo.getParameter("comment"));
	}

}
