package jp.co.blueship.tri.am.uix.checkinout;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckinRequestApprovalPendingCancellationServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/checkin/cancel")
public class CheckinRequestApprovalPendingCancellationController extends TriControllerSupport<FlowCheckinRequestApprovalPendingCancellationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.AmCheckinRequestApprovalPendingCancellationService;
	}

	@Override
	protected FlowCheckinRequestApprovalPendingCancellationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowCheckinRequestApprovalPendingCancellationServiceBean bean =
				new FlowCheckinRequestApprovalPendingCancellationServiceBean();
		return bean;
	}

	@RequestMapping
	public String cancel (FlowCheckinRequestApprovalPendingCancellationServiceBean bean, TriModel model) {
		String view = "redirect:/checkinout/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.cancelMapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				bean.getParam().setRequestType(RequestType.init);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String cancelComment(FlowCheckinRequestApprovalPendingCancellationServiceBean bean,
								TriModel model) {
		String view = "common/Comment::checkinRequestCancellationComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.cancelMapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			return TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void cancelMapping (FlowCheckinRequestApprovalPendingCancellationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setSelectedAreqId(requestInfo.getParameter("areqId"));
	}

}
