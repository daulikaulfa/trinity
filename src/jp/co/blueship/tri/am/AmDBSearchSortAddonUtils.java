package jp.co.blueship.tri.am;

import java.util.Map;

import jp.co.blueship.tri.bm.dao.bldsrv.constants.BldSrvItems;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;

/**
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class AmDBSearchSortAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 変更管理・トップ画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByChaLibTop() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.topListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.topListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理・トップ画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByChaLibLotHistoryList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotHistoryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotHistoryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * コンフリクトチェック対象ロット一覧画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByChaLibConflictCheckLotList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.conflictCheckLotListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.conflictCheckLotListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * マージ可能ロット一覧画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByChaLibMargeLotList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.margeLotListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.margeLotListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * マージ履歴ロット一覧画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtLotSortFromDesignDefineByChaLibMargeHistoryLotList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.margeHistoryLotListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.margeHistoryLotListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット詳細画面・変更管理のソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibLotDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotDetailViewPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotDetailViewPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット履歴詳細画面・変更管理のソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibLotHistoryDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotHistoryDetailViewPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotHistoryDetailViewPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロットクローズ確認画面・変更管理のソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibLotCloseConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotCloseConfirmPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotCloseConfirmPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット取消確認画面・変更管理のソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibLotCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotCancelConfirmPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotCancelConfirmPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * マージ対象ベースライン詳細画面・ビルドパッケージのソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.conflictCheckBaselineDetailViewPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.conflictCheckBaselineDetailViewPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * マージ可能ベースライン詳細画面・ビルドパッケージのソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.margeBaselineDetailViewPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.margeBaselineDetailViewPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * マージ履歴ベースライン詳細画面・ビルドパッケージのソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByChaLibMargeHistoryBaselineDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.margeHistoryBaselineDetailViewPjtListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.margeHistoryBaselineDetailViewPjtListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理承認一覧のソート条件を取得する
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSortFromDesignDefineByPjtApproveList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 貸出申請一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibLendList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lendListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lendListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 原本削除申請一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibMasterDelList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.masterDelListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.masterDelListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 原本削除申請履歴一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibMasterDelHistoryList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.masterDelHistoryListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.masterDelHistoryListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 返却申請一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibRtnList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.rtnListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.rtnListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理承認確認画面・申請情報のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtApproveConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveConfirmAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveConfirmAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理承認履歴確認画面・申請情報のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtApproveDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveDetailViewAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveDetailViewAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理 承認履歴確認画面・変更承認情報のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getPjtApproveSortFromDesignDefineByChaLibPjtApproveDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveDetailViewPjtApproveListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveDetailViewPjtApproveListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理承認取消確認画面・申請情報のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtApproveCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveCancelConfirmAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveCancelConfirmAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理承認取消確認画面・関連申請情報のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static final ISqlSort getAssetApplyRelatedSortFromDesignDefineByChaLibPjtApproveCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtApproveCancelConfirmAssetApplyRelatedListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtApproveCancelConfirmAssetApplyRelatedListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理詳細画面・申請情報のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtDetailViewAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtDetailViewAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理履歴詳細画面・申請情報のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtHistoryDetailView() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtHistoryDetailViewAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtHistoryDetailViewAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理テスト完了確認画面・申請情報のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtTestCompleteConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtTestCompleteConfirmAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtTestCompleteConfirmAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理クローズ確認画面・申請情報のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtCloseConfirmAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtCloseConfirmAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理取消確認画面・申請情報のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getAssetApplySortFromDesignDefineByChaLibPjtCancelConfirm() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.pjtCancelConfirmAssetApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.pjtCancelConfirmAssetApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理選択で使用する「選択可能な変更管理」のソート順序を取得します。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getPjtSelectSortFromDesignDefine() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.entryPjtSelectOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.entryPjtSelectOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * 変更管理・申請台帳一覧画面のソート条件を取得する。
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getReportSortFromDesignDefineByChaLibReportApplyList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.reportApplyListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.reportApplyListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット作成 ビルド環境選択画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getBuildEnvSortFromDesignDefineByChaLibLotEntryBuildEnvList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotEntryBuildEnvListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotEntryBuildEnvListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット作成 リリース環境選択画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getRelEnvSortFromDesignDefineByChaLibLotEntryRelEnvList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotEntryRelEnvListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotEntryRelEnvListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}

	/**
	 * ロット作成 モジュール選択画面のソート条件を取得する。
	 *
	 * @return ソート条件を戻します。
	 */
	public static ISqlSort getUcfScmSortFromDesignDefineByChaLibLotEntryModuleList() {

		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( AmDesignBeanId.lotEntryUcfScmListOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( AmDesignBeanId.lotEntryUcfScmListOrderBy );

		return DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );
	}


	/**
	 * UcfRemoteServerのソート順（固定）を取得する。
	 * @return
	 */
	public static ISqlSort getUcfRemoteServerSort() {

		ISqlSort sort = new SortBuilder();
		sort.setElement( BldSrvItems.sortOdr, TriSortOrder.Asc, 1 );

		return sort;
	}
}
