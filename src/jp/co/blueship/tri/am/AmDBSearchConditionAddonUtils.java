package jp.co.blueship.tri.am;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.ext.eb.ExtCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.BmBldEnvStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.rm.ui.rp.beans.FlowRelCtlEntryProsecutor;


/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class AmDBSearchConditionAddonUtils {

	public static final String[] convertStatusToStatusId( String[] statusArray ) {
		List<String> statusIdList = new ArrayList<String>() ;

		Map<String,String> reverseMap = DesignDefineSearchUtils.reverseMap( DesignSheetFactory.getDesignSheet().getKeyMap( AmDesignBeanId.statusId ) );

		if ( TriStringUtils.isEmpty( statusArray ) ) {
			statusIdList.addAll( reverseMap.values() );

		} else {

			for( String status : statusArray ) {
				statusIdList.add( reverseMap.get( status ) ) ;
			}
		}

		return statusIdList.toArray( new String[ 0 ] );
	}

	/**
	 * 重複できない削除申請情報の検索条件を取得します。
	 * @param lotNo
	 * @return
	 */
	public static final AreqCondition
		getAssetDelApplyConditionByDuplicate( String lotId ) {

		AreqCondition condition = new AreqCondition();
		condition.setLotId( lotId );

		condition.setStsIds( ExtractStatusAddonUtils.getAssetDelApplyBaseStatusByUnderPjtApprove() );

		return condition;

	}

	/**
	 * 重複できない返却申請情報の検索条件を取得します。
	 * @param lotNo
	 * @return
	 */
	public static final AreqCondition
		getAssetRtnApplyConditionByDuplicate( String lotId ) {

		AreqCondition condition = new AreqCondition();
		condition.setLotId( lotId );

		condition.setStsIds( ExtractStatusAddonUtils.getAssetRtnApplyBaseStatusByUnderPjtApprove() );

		return condition;

	}

	/**
	 * 資産貸出排他中の申請情報を取得するための検索条件を取得します。
	 * @param lotNo
	 *
	 * @return
	 */
	public static final AreqCondition getAreqConditionByAssetLock( String lotId ) {
		AreqCondition condition = new AreqCondition();

		condition.setLotId( lotId );
		String[] ignoredStsIds = {AmAreqStatusId.DraftCheckoutRequest.getStatusId(), AmAreqStatusId.BuildPackageClosed.getStatusId() ,
									AmAreqStatusId.Merged.getStatusId(), AmAreqStatusId.DraftRemovalRequest.getStatusId()};
		condition.setStsIdsNotEquals( ignoredStsIds );

		return condition;

	}

	/**
	 * 関連資産になる得る貸出、返却、削除情報を取得するための検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 申請済みの貸出、返却、削除情報の検索条件
	 */
	public static final IJdbcCondition getRelatableAreqCondition( String lotId ) {

		AreqCondition condition = new AreqCondition();

		condition.setLotId			( lotId );

		// 資産が重複している場合は承認できないので、関連資産になり得る
		// ステータスは承認より前になる
		condition.setStsIds			( ExtractStatusAddonUtils.getAssetApplyBaseStatusByLessPjtApprove() );

		return condition;

	}

	/**
	 * 全ての貸出、返却、削除情報を取得するための検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 全ての貸出、返却、削除情報の検索条件
	 */
	public static final IJdbcCondition getAreqCondition( String pjtNo ) {

		AreqCondition condition = new AreqCondition();
		condition.setPjtId( pjtNo );

		return condition;

	}

	/**
	 * 承認済み以上の申請情報を取得するための検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 検索条件
	 */
	public static final IJdbcCondition getAreqConditionByOverPjtApprove( String pjtNo ) {

		AreqCondition condition = new AreqCondition();
		condition.setPjtId( pjtNo );
		condition.setStsIds( ExtractStatusAddonUtils.getAssetApplyBaseStatusByOverPjtApprove() );

		return condition;

	}

	/**
	 * 全ての拡張子情報情報を取得するための検索条件を取得します。
	 * @return 全ての拡張子情報情報を取得するための検索条件
	 */
	public static final ExtCondition getAllExtensionInfo() {

		ExtCondition condition = new ExtCondition();

		return condition;

	}

	/**
	 * 案件選択で使用する「選択可能な案件」の検索条件を取得します。
	 *
	 * @return 取得した検索条件を戻します。
	 */
	public static final PjtCondition getPjtSelectCondition() {
		PjtCondition condition = new PjtCondition();

		condition.setStsIds( ExtractStatusAddonUtils.getPjtBaseStatusByPjtProgress() );

		return condition;
	}

	/**
	 * 進行中でないロット情報(Headを除く)の検索条件を取得します。
	 * @return 進行中でないロット情報の検索条件
	 */
	public static final LotCondition getNonActivePjtLotConditionWithoutHead() {

		LotCondition condition = new LotCondition();

		String[] statusId	= {	AmLotStatusId.Unspecified.getStatusId(),
								AmLotStatusIdForExecData.EditingLot.getStatusId(),
								AmLotStatusIdForExecData.LotEditError.getStatusId(),
								AmLotStatusId.LotRemoved.getStatusId(),
								AmLotStatusId.LotClosed.getStatusId() };
		condition.setProcStsIds	( statusId );

		return condition;

	}

	/**
	 * 進行中のロット情報(Headを除く)の検索条件を取得します。
	 * @return 進行中のロット情報の検索条件
	 */
	public static final LotCondition getActivePjtLotConditionWithoutHead() {

		LotCondition condition = new LotCondition();

		condition.setStsIds		( ExtractStatusAddonUtils.getLotBaseStatusByLotProgress() );

		return condition;

	}

	/**
	 * マージ済み（件数カウント用）の変更管理情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return マージ済みの変更管理情報の検索条件
	 */

	public static final IJdbcCondition getMergedCountPjtCondition( String lotId ) {

		PjtCondition condition = new PjtCondition();

		condition.setLotId( lotId );

		condition.setStsIds( ExtractStatusAddonUtils.getPjtBaseStatusByOverPjtMerge() );

		return condition;
	}




	/**
	 * 完了した（件数カウント用）変更管理情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 完了した変更管理情報の検索条件
	 */
	public static final IJdbcCondition getCompleteCountPjtCondition( String lotId ) {

		PjtCondition condition = new PjtCondition();

		condition.setLotId( lotId );

		String[] statusId = ExtractStatusAddonUtils.getPjtBaseStatusByPjtClose();
		condition.setStsIds( statusId );

		return condition;
	}


	/**
	 * 進行中（件数カウント用）の変更管理情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 進行中の変更管理情報の検索条件
	 */
	public static final IJdbcCondition getProgressCountPjtCondition( String lotId ) {

		PjtCondition condition = new PjtCondition();
		condition.setLotId( lotId );

		condition.setStsIds	( ExtractStatusAddonUtils.getPjtBaseStatusByUnderPjtMerge() );

		return condition;
	}

	/**
	 * 進行中の変更管理情報の検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 進行中の変更管理情報の検索条件
	 */
	public static final IJdbcCondition getProgressPjtCondition( String[] pjtNo ) {

		PjtCondition condition = new PjtCondition();

		condition.setStsIds	( ExtractStatusAddonUtils.getPjtBaseStatusByPjtProgress() );
		condition.setPjtIds			( pjtNo );

		return condition;
	}

	/**
	 * クローズされていない（件数カウント用）変更管理情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return クローズされていない変更管理情報の検索条件
	 */
	public static final IJdbcCondition getAliveCountPjtCondition( String lotId ) {

		PjtCondition condition = new PjtCondition();
		condition.setLotId( lotId );

		condition.setStsIds	( ExtractStatusAddonUtils.getPjtBaseStatusByAll() );

		return condition;
	}


	/**
	 * 変更管理情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 変更管理情報の検索条件
	 */
	public static final IJdbcCondition getPjtCondition( String lotId ) {

		PjtCondition condition = new PjtCondition();
		condition.setLotId( lotId );

		return condition;
	}

	/**
	 * 変更管理情報の検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 変更管理情報の検索条件
	 */
	public static final IJdbcCondition getPjtCondition( String[] pjtNo ) {

		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			//ダミーの変更管理を設定して、全件検索を防止する。
			pjtNo = new String[]{ "" };
		}

		PjtCondition condition = new PjtCondition();
		condition.setPjtIds( pjtNo );

		return condition;
	}

	/**
	 * 成功したビルドパッケージ作成の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 成功したビルドパッケージ作成の検索条件
	 */
	public static final IJdbcCondition getSucceededRelUnitCondition( String lotId ) {

		BpCondition condition = new BpCondition();

		condition.setLotId( lotId );

		condition.setStsIds( ExtractStatusAddonUtils.getRelUnitBaseStatusByAll() );

		return condition;
	}

	/**
	 * 失敗したビルドパッケージ作成の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 失敗したビルドパッケージ作成の検索条件
	 */
	public static final IJdbcCondition getFailedRelUnitCondition( String lotId ) {

		BpCondition condition = new BpCondition();

		condition.setLotId( lotId );

		String[] statusId = { BmBpStatusIdForExecData.BuildPackageError.getStatusId() };
		condition.setProcStsIds( statusId );

		return condition;
	}

	/**
	 * 承認可能な申請情報の検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 承認可能な申請情報の検索条件
	 */
	public static final IJdbcCondition getApprovableAreqCondition( String[] pjtNo ) {

		AreqCondition condition = getApprovableAreqCondition();

		condition.setPjtIds( pjtNo );

		return condition;

	}

	/**
	 * 承認可能な申請情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 承認可能な申請情報の検索条件
	 */
	public static final IJdbcCondition getApprovableAreqCondition( String lotId ) {

		AreqCondition condition = getApprovableAreqCondition();

		if ( null != lotId ) {
			condition.setLotId( lotId );
		}

		return condition;
	}

	/**
	 * 承認可能な申請情報の検索条件を取得します。
	 * @return 承認可能な申請情報の検索条件
	 */
	private static AreqCondition getApprovableAreqCondition() {

		AreqCondition condition = new AreqCondition();

		String[] baseStatusId = { 	AmAreqStatusId.CheckinRequested.getStatusId() ,
									AmAreqStatusId.RemovalRequested.getStatusId() };
		condition.setStsIds		( baseStatusId );

		String[] statusIdByNotEquals = {
				AmAreqStatusIdForExecData.ApprovingCheckinRequest.getStatusId(),
				AmAreqStatusIdForExecData.ApprovingRemovalRequest.getStatusId() };
		condition.setProcStsIdsByNotEquals		( statusIdByNotEquals );

		return condition;

	}

	/**
	 * 承認済みの申請情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 承認済みの申請情報の検索条件
	 */
	public static final IJdbcCondition getApprovedAreqCondition( String lotId ) {

		AreqCondition condition = new AreqCondition();

		condition.setLotId			( lotId );
		condition.setStsIds			( ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove() );

		return condition;

	}

	/**
	 * 承認済みの申請情報の検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 承認済みの申請情報の検索条件
	 */
	public static final IJdbcCondition getApprovedAreqCondition( String lotId, String[] pjtNo ) {

		AreqCondition condition = new AreqCondition();

		condition.setLotId		( lotId );
		condition.setStsIds		( ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove() );
		condition.setPjtIds		( pjtNo );

		return condition;

	}

	/**
	 * 申請情報を取得するための検索条件を取得します。
	 * @param applyNo 申請番号
	 * @return 申請情報を取得するための検索条件
	 */
	public static final IJdbcCondition getAreqConditionByApplyNo( String[] applyNo ) {

		AreqCondition condition = new AreqCondition();
		condition.setAreqIds( applyNo );

		return condition;

	}

	/**
	 * 申請情報を取得するための検索条件を取得します。
	 * @param pjtNo 変更管理番号
	 * @return 申請情報を取得するための検索条件
	 */
	public static final IJdbcCondition getAreqConditionByPjtNo( String[] pjtNo ) {

		AreqCondition condition = new AreqCondition();
		condition.setPjtIds( pjtNo );

		return condition;

	}

	/**
	 * 変更管理承認履歴情報を取得するための検索条件を取得します。
	 * @param lotId ロット番号
	 * @return 変更管理承認履歴情報を取得するための検索条件
	 */
	public static final IJdbcCondition getPjtApproveCondition( String lotId ) {

		PjtAvlCondition condition = new PjtAvlCondition();

		if ( null != lotId ) {
			condition.setLotId	( lotId );
		}

		condition.setStsIds	( ExtractStatusAddonUtils.getPjtApproveBaseStatusByAll() );

		return condition;

	}


	/**
	 * ロット作成 進行中のビルド環境情報の検索条件を取得します。
	 * @return 進行中のビルド環境情報の検索条件
	 */
	public static final IJdbcCondition getActiveBuildEnvCondition() {

		BldEnvCondition condition = new BldEnvCondition();

		String[] statusId	= { BmBldEnvStatusId.Running.getStatusId() };
		condition.setSvcIds( new String[]{
				ServiceId.BmBuildPackageCreationService.value(),
				ServiceId.BmBuildPackageCreationService.valueOfV3() } );
		condition.setStsIds	( statusId );

		return condition;

	}

	/**
	 * 進行中のリリース環境の検索条件を取得する。
	 * @param 対象となるリリース環境番号の配列
	 * @return 進行中のリリース環境の検索条件
	 */
	public static final IJdbcCondition getActiveRelEnvCondition( String[] envNo ) {

		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvIds( envNo );
		condition.setSvcId( FlowRelCtlEntryProsecutor.FLOW_ACTION_ID );
		condition.setStsIds( new String[]{ BmBldEnvStatusId.Running.getStatusId() } );

		return condition;
	}

	/**
	 * 資産申請進行中の申請情報を取得するための検索条件を取得します。
	 * @param lotNo
	 *
	 * @return
	 */
	public static final AreqCondition getAreqConditionByAssetApplyProgress( String lotId ) {

		AreqCondition condition = new AreqCondition();

		String[] cancel = ExtractStatusAddonUtils.getAssetApplyBaseStatusByApplyCancel();
		String[] overUnitClose = ExtractStatusAddonUtils.getAssetApplyBaseStatusByOverRelUnitClose();

		List<String> status = new ArrayList<String>();
		status.addAll( FluentList.from(cancel).asList());
		status.addAll( FluentList.from(overUnitClose).asList());

		condition.setLotId( lotId );
		condition.setStsIdsNotEquals( (String[])status.toArray( new String[0] ));

		return condition;

	}

	/**
	 * 指定されたロット番号の検索条件を取得する。
	 * @return ロットの検索条件
	 */
	public static final IJdbcCondition getPjtLotConditionByLotNo( String[] lotId ) {

		LotCondition condition = new LotCondition();

// Hedはここにこないので以下コメント
//		String[] lotType = { ILotEntity.LotType.LOT.name() };
//		condition.setLotType		( lotType );
		condition.setLotIds			( lotId );
		condition.setStsIds			( ExtractStatusAddonUtils.getLotBaseStatusByAll() );

		return condition;
	}

	/**
	 * SCMが設置されたサーバ情報の検索条件を取得する。
	 * @return
	 */
	public static final IJdbcCondition getUcfRemoteServer() {

		BldSrvCondition condition = new BldSrvCondition();

		return condition;

	}
}
