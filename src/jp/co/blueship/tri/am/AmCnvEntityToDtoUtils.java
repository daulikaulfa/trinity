package jp.co.blueship.tri.am;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryViewServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AppendFileConstID;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * エンティティ、ビーンの変換共通処理Class
 * <br>
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class AmCnvEntityToDtoUtils {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBean (
													ChaLibEntryBaseInfoBean bean,
													IPjtEntity pjtEntity,
													IAreqEntity entity ) {

		if ( null ==  bean)
			bean = new ChaLibEntryBaseInfoBean();
		bean.setLotNo			( pjtEntity.getLotId() ) ;
		bean.setPrjNo			( entity.getPjtId() );
		bean.setChangeCauseNo	( pjtEntity.getChgFactorNo() ) ;
		bean.setGroupName		( entity.getGrpNm() );
		bean.setSubject			( entity.getSummary() );
		bean.setContent			( entity.getContent() );
		bean.setAssigneeId      ( entity.getAssigneeId() );
		bean.setAssigneeNm      ( entity.getAssetReqNm() );
		bean.setCheckinDueDate         ( entity.getRtnReqDueDate() );
		bean.setCtgId           ( entity.getCtgId() );
		bean.setMstoneId        ( entity.getMstoneId() );
		bean.setCtgNm           ( entity.getCtgNm() );
		bean.setMstoneNm        ( entity.getMstoneNm() );

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByLendApply ( IPjtEntity pjtEntity , IAreqDto dto ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean = convertChaLibEntityBaseInfoBean( bean, pjtEntity, dto.getAreqEntity() );

		bean.setApplyUser		( dto.getAreqEntity().getLendReqUserNm() );
		bean.setApplyUserId		( dto.getAreqEntity().getLendReqUserId() );
		bean.setApplyDate		( TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getLendReqTimestamp()) );

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByDelApply ( IPjtEntity pjtEntity , IAreqEntity entity ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean = convertChaLibEntityBaseInfoBean( bean, pjtEntity, entity );

		bean.setApplyUser		( entity.getDelReqUserNm() );
		bean.setApplyUserId		( entity.getDelReqUserId() );
		bean.setApplyDate		( TriDateUtils.convertViewDateFormat(entity.getDelReqTimestamp()) );

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByLendApply ( IAreqDto dto, ChaLibEntryInputBaseInfoBean inBaseInfoBean ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean.setLotNo			( inBaseInfoBean.getInLotNo() );
		bean.setPrjNo			( inBaseInfoBean.getInPrjNo() );
		bean.setChangeCauseNo	( inBaseInfoBean.getInChangeCauseNo() );
		bean.setGroupName		( inBaseInfoBean.getInGroupName() );
		bean.setSubject			( inBaseInfoBean.getInSubject() );
		bean.setContent			( inBaseInfoBean.getInContent() );
		bean.setApplyUser		( inBaseInfoBean.getInUserName() );
		bean.setApplyUserId		( inBaseInfoBean.getInUserId() );

		if ( null != dto ) {
			bean.setApplyDate	( TriDateUtils.convertViewDateFormat(dto.getAreqEntity().getLendReqTimestamp()) );
		}

		return bean;
	}

	public static ChaLibEntryAssetResourceInfoBean  convertChaLibAssetResourceInfoBeanByLendApply (
														ILotDto lotDto,
														IAreqDto dto) {

		List<Object> list = new ArrayList<Object>();
		list.add( lotDto );
		list.add( dto );

		File masterPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( list );

		List<File> files = BusinessFileUtils.getFiles(masterPath);

		ChaLibEntryAssetResourceInfoBean bean = convertFiles2AssetResourceInfoBean(files, true);

		bean.setMasterPath( TriStringUtils.convertPath( masterPath ) );

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByReturnInfo ( IPjtEntity pjtEntity , IAreqDto dto ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean = convertChaLibEntityBaseInfoBean( bean, pjtEntity, dto.getAreqEntity() );

		bean.setApplyUser		( dto.getAreqEntity().getLendReqUserNm() );
		bean.setApplyUserId		( dto.getAreqEntity().getLendReqUserId() );
		bean.setApplyDate		( TriDateUtils.convertViewDateFormat( dto.getAreqEntity().getLendReqTimestamp() ) );

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByReturnAsset (
			IPjtEntity pjtEntity,
			IAreqEntity areqEntity,
			List<IAreqAttachedFileEntity> areqAttachedFileEntities ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean = convertChaLibEntityBaseInfoBean( bean, pjtEntity, areqEntity );

		String applyUser = null;
		String applyUserId = null;
		Timestamp applyDate = null;

		if ( ! AmBusinessJudgUtils.isDeleteApply(areqEntity) ) {
			applyUser = areqEntity.getRegUserNm();
			applyUserId = areqEntity.getRtnReqUserId();
			applyDate = areqEntity.getRtnReqTimestamp();
		} else {
			applyUser = areqEntity.getDelReqUserNm();
			applyUserId = areqEntity.getDelReqUserId();
			applyDate = areqEntity.getDelReqTimestamp();
		}

		bean.setApplyUser		( applyUser );
		bean.setApplyUserId		( applyUserId );
		bean.setApplyDate		( TriDateUtils.convertViewDateFormat(applyDate ) );

		// 添付ファイルの表示
		if (null != areqAttachedFileEntities) {
			for (IAreqAttachedFileEntity areqAttachedFileEntity : areqAttachedFileEntities) {

				String filePath = areqAttachedFileEntity.getFilePath();
				bean.setReturnAssetAppendFile( filePath );
			}
		}

		// 添付ファイルのダウンロード
		String saveDirURL = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFileURL ) ;

		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		// 申請番号フォルダをつなげる
		String applyNo = areqEntity.getAreqId();
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		// 最新のユニークのフォルダをつなげる
		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;
		// 添付ファイルごとのフォルダをつなげる
		String append1 = AppendFileConstID.File.append1.value() ;
		String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

		if (null != areqAttachedFileEntities) {
			for (IAreqAttachedFileEntity areqAttachedFileEntity : areqAttachedFileEntities) {

				String filePath = areqAttachedFileEntity.getFilePath();
				if(TriStringUtils.isEmpty(filePath)) {
					// ファイルが登録されていない場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}
				File file = new File(appendPath1, filePath);
				String linkUrl = BusinessFileUtils.getAppendFileURL(saveDirURL, applyNo, uniqueKey, append1, filePath );

				if(false == file.exists()) {
					// ファイルがない場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}
				if(TriStringUtils.isEmpty(linkUrl)) {
					// URLがつくれなかった場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}

				bean.setReturnAssetAppendFileLink( linkUrl );
			}
		}

		

		return bean;
	}

	public static ChaLibEntryBaseInfoBean convertChaLibEntityBaseInfoBeanByPjtApprove (
			IPjtAvlDto pjtDto,
			IAreqEntity areqEntity,
			List<IAreqAttachedFileEntity> attachedFileEntities ) {

		ChaLibEntryBaseInfoBean bean = new ChaLibEntryBaseInfoBean();

		bean = convertChaLibEntityBaseInfoBean( bean, pjtDto.getPjtEntity(), areqEntity );

		String applyUser = null;
		String applyUserId = null;
		Timestamp applyDate = null;

		if ( ! AmBusinessJudgUtils.isDeleteApply(areqEntity) ) {
			applyUser = areqEntity.getRtnReqUserNm();
			applyUserId = areqEntity.getRtnReqUserId();
			applyDate = areqEntity.getRtnReqTimestamp();
		} else {
			applyUser = areqEntity.getDelReqUserNm();
			applyUserId = areqEntity.getDelReqUserId();
			applyDate = areqEntity.getDelReqTimestamp();
		}

		bean.setApplyUser		( applyUser );
		bean.setApplyUserId		(applyUserId );
		bean.setApplyDate		( TriDateUtils.convertViewDateFormat(applyDate) );

		bean.setPjtApproveUser	( pjtDto.getPjtAvlEntity().getPjtAvlUserNm() );
		bean.setPjtApproveUserId( pjtDto.getPjtAvlEntity().getPjtAvlUserId() );
		bean.setPjtApproveDate	( TriDateUtils.convertViewDateFormat(pjtDto.getPjtAvlEntity().getPjtAvlTimestamp()) );
		bean.setPjtApproveCancelUser	( pjtDto.getPjtAvlEntity().getPjtAvlCancelUserNm() );
		bean.setPjtApproveCancelUserId	( pjtDto.getPjtAvlEntity().getPjtAvlCancelUserId() );
		bean.setPjtApproveCancelDate	( TriDateUtils.convertViewDateFormat( pjtDto.getPjtAvlEntity().getPjtAvlCancelTimestamp() ) );

		if (null != attachedFileEntities) {
			for (IAreqAttachedFileEntity areqAttachedFileEntity : attachedFileEntities) {
				bean.setReturnAssetAppendFile( areqAttachedFileEntity.getFilePath() );
			}
		}

		// 添付ファイルのダウンロード
		String saveDirURL = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFileURL ) ;

		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		// 申請番号フォルダをつなげる
		String applyNo = areqEntity.getAreqId();
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		// 最新のユニークのフォルダをつなげる
		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;
		// 添付ファイルごとのフォルダをつなげる
		String append1 = AppendFileConstID.File.append1.value() ;
		String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

		if (null != attachedFileEntities) {
			for (IAreqAttachedFileEntity areqAttachedFileEntity : attachedFileEntities) {

				String filePath = areqAttachedFileEntity.getFilePath();
				if(TriStringUtils.isEmpty(filePath)) {
					// ファイルが登録されていない場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}
				File file = new File(appendPath1, filePath);
				String linkUrl = BusinessFileUtils.getAppendFileURL(saveDirURL, applyNo, uniqueKey, append1, filePath );

				if(false == file.exists()) {
					// ファイルがない場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}
				if(TriStringUtils.isEmpty(linkUrl)) {
					// URLがつくれなかった場合
					bean.setReturnAssetAppendFileLink( null );
					continue;
				}

				bean.setReturnAssetAppendFileLink( linkUrl );
			}
		}

		return bean;
	}

	public static ChaLibEntryAssetResourceInfoBean  convertChaLibAssetResourceInfoBeanByReturnInfo (
			ILotDto lotDto,
			IAreqDto dto) {

		List<Object> list = new ArrayList<Object>();
		list.add( lotDto );
		list.add( dto );

		File masterPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( list );

		List<File> files = BusinessFileUtils.getFiles( masterPath );


		ChaLibEntryAssetResourceInfoBean bean = convertFiles2AssetResourceInfoBean(files, true);

		bean.setMasterPath( TriStringUtils.convertPath( masterPath ) );

		return bean;
	}

	/**
	 * 資産情報の編集を行います。
	 * <br>返却済の申請に対しては、実体となる資産がすでに存在しない場合もあります。
	 * 実体のファイルからリソースの編集を行うことは、厳密には不可能です。
	 * <br>よって、Entityに記録された情報から、資産情報の編集を行います。
	 *
	 * @param lotEntity
	 * @param areqDto
	 * @return
	 */
	public static ChaLibEntryAssetResourceInfoBean  convertChaLibAssetResourceInfoBeanByReturnAsset (
														ILotDto lotDto,
														IAreqDto areqDto) {

		List<Object> list = new ArrayList<Object>();
		list.add( lotDto );
		list.add( areqDto.getAreqEntity() );

		File masterPath = null ;

		List<IAreqFileEntity> assetFileEntityList = null;
		List<IAreqBinaryFileEntity> assetBinaryFileEntityList = null;

		if( ! AmBusinessJudgUtils.isDeleteApply(areqDto.getAreqEntity()) ) {
			masterPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( list, areqDto.getAreqEntity() );

			assetFileEntityList = areqDto.getAreqFileEntities( AreqCtgCd.ReturningRequest );
		} else {
			//削除資産は返却側に削除ファイルがないので、マスターのパスをつなぐ
			masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( list );

			assetFileEntityList = areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest );
			assetBinaryFileEntityList = areqDto.getAreqBinaryFileEntities( AreqCtgCd.RemovalRequest );
		}

		ChaLibEntryAssetResourceInfoBean  resourceBean				= new ChaLibEntryAssetResourceInfoBean ();
		List<ChaLibAssetResourceViewBean> resourceViewlist			= new ArrayList<ChaLibAssetResourceViewBean>();
		List<ChaLibAssetResourceViewBean> binaryResourceViewlist	= new ArrayList<ChaLibAssetResourceViewBean>();

		if ( null != assetFileEntityList ) {
			for ( IAreqFileEntity assetFileEntity : assetFileEntityList ) {

				ChaLibAssetResourceViewBean viewBean = resourceBean.newAssetResourceViewBean();

				File assetFile = new File( masterPath, assetFileEntity.getFilePath() );

				viewBean.setResourceName( ( assetFile.isFile() )? assetFile.getName() : null );
				viewBean.setResourcePath( ( assetFile.isFile() )? TriStringUtils.convertPath( assetFile.getPath() ) : null );
				viewBean.setRelativeResourcePath( assetFileEntity.getFilePath() );
				viewBean.setChecked		( true );
				viewBean.setFile		( true );
				viewBean.setHidden		( false );

				resourceViewlist.add( viewBean );
			}
		}


		if ( null != assetBinaryFileEntityList ) {
			for ( IAreqBinaryFileEntity assetBinaryFileEntity : assetBinaryFileEntityList ) {

				ChaLibAssetResourceViewBean viewBean = resourceBean.newAssetResourceViewBean();

				File assetBinaryFile = new File( assetBinaryFileEntity.getFilePath() );

				viewBean.setResourceName( assetBinaryFile.getName() );
				viewBean.setResourcePath( TriStringUtils.convertPath( assetBinaryFile.getPath() ) );
				viewBean.setRelativeResourcePath( assetBinaryFileEntity.getFilePath() );
				viewBean.setChecked		( true );
				viewBean.setFile		( true );
				viewBean.setHidden		( false );

				binaryResourceViewlist.add( viewBean );
			}
		}


		resourceBean.setAssetResourceViewBeanList		( resourceViewlist );
		resourceBean.setAssetBinaryResourceViewBeanList	( binaryResourceViewlist );

		resourceBean.setMasterPath( TriStringUtils.convertPath( masterPath ) );

		return resourceBean;
	}

	public static ChaLibMasterDelEntryViewServiceBean convertChaLibMasterDelEntryViewBean  ( IPjtEntity pjtEntity , IAreqDto areqDto) {

		ChaLibMasterDelEntryViewServiceBean viewBean = new ChaLibMasterDelEntryViewServiceBean();

		IAreqEntity areqEntity = areqDto.getAreqEntity();

		viewBean.setApplyDate		( TriDateUtils.convertViewDateFormat( areqEntity.getDelReqTimestamp() ) );
		viewBean.setDelApplyUser	( areqEntity.getDelReqUserNm() );
		viewBean.setDelApplyUserId	( areqEntity.getDelReqUserId() );
		viewBean.setPrjNo			( areqEntity.getPjtId() );
		viewBean.setChgFactorNo		( pjtEntity.getChgFactorNo() ) ;
		viewBean.setDelApplySubject	( areqEntity.getSummary() );
		viewBean.setDelReason		( areqEntity.getContent() );
		viewBean.setUserGroup		( areqEntity.getGrpNm() );
		viewBean.setLotNo			( pjtEntity.getLotId() );

		// ソース
		Set<String> filePathSet = new TreeSet<String>();
		List<IAreqFileEntity> removalFileEntities = areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest );
		if ( !TriStringUtils.isEmpty( removalFileEntities )) {
			for ( IAreqFileEntity assetFileEntity : removalFileEntities ) {
				filePathSet.add( assetFileEntity.getFilePath() );
			}
		}
		viewBean.setApplyFilePath( (String[])filePathSet.toArray( new String[]{} ));


		// モジュール
		Set<String> binaryFilePathSet = new TreeSet<String>();
		List<IAreqBinaryFileEntity> removalBinaryFileEntities = areqDto.getAreqBinaryFileEntities( AreqCtgCd.RemovalRequest );
		if ( !TriStringUtils.isEmpty( removalBinaryFileEntities )) {
			for ( IAreqBinaryFileEntity assetFileEntity : removalBinaryFileEntities ) {
				binaryFilePathSet.add( assetFileEntity.getFilePath() );
			}
		}
		viewBean.setApplyBinaryFilePath( (String[])binaryFilePathSet.toArray( new String[]{} ));

		return viewBean;
	}

	/**
	 * FileリストをChaLibEntryAssetResourceInfoBeanに変換します。
	 * @param files 変換対象のファイルリスト
	 * @return
	 */
	public static ChaLibEntryAssetResourceInfoBean convertFiles2AssetResourceInfoBean( List<File> files ) {

		return convertFiles2AssetResourceInfoBean( files, false, false );
	}

	/**
	 * FileリストをChaLibEntryAssetResourceInfoBeanに変換します。
	 * @param files 変換対象のファイルリスト
	 * @param isCheck 貸出済の場合True
	 * @return
	 */
	public static ChaLibEntryAssetResourceInfoBean convertFiles2AssetResourceInfoBean( List<File> files, boolean isCheck ) {

		return convertFiles2AssetResourceInfoBean( files, isCheck, false );
	}

	/**
	 * FileリストをChaLibEntryAssetResourceInfoBeanに変換します。
	 * @param files 変換対象のファイルリスト
	 * @param isCheck 貸出済の場合True
	 * @param isHidden 貸出ロック中の場合True
	 * @return
	 */
	public static ChaLibEntryAssetResourceInfoBean convertFiles2AssetResourceInfoBean(
														List<File> files,
														boolean isCheck,
														boolean isHidden ) {

		ChaLibEntryAssetResourceInfoBean  bean = new ChaLibEntryAssetResourceInfoBean ();
		List<ChaLibAssetResourceViewBean> list = new ArrayList<ChaLibAssetResourceViewBean>();

		File[] fileArray = (File[])files.toArray( new File[0] );
		BusinessFileUtils.sortDefaultFileList( fileArray );

		for ( File file : fileArray ) {

			ChaLibAssetResourceViewBean viewBean = bean.newAssetResourceViewBean();

			viewBean.setResourceName( file.getName() );
			viewBean.setResourcePath( TriStringUtils.convertPath( file.getPath() )) ;
			viewBean.setChecked		( isCheck );
			viewBean.setFile		( file.isFile() );
			viewBean.setHidden		( isHidden );

			list.add( viewBean );
		}

		bean.setAssetResourceViewBeanList( list );

		return bean;

	}
}
