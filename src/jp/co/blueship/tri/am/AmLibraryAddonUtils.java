package jp.co.blueship.tri.am;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * 変更管理を扱うユーティリティークラスです。 <br>
 * 返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class AmLibraryAddonUtils {

	private static TriFunction<IGrpEntity, String> TO_GROUP_ID = new TriFunction<IGrpEntity, String>() {

		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	/**
	 * 資産貸出の申請時に使用する作業フォルダの絶対パスを取得します。 <br>
	 * 決められた業務ルールに従い絶対パスを算出します。
	 *
	 * @param info アプリケーション層で共通に使用するパラメタインタフェース
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException 取得元のマスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getLendApplyDefTempFilePath(List<Object> list) throws TriSystemException {

		IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo(list);

		if (null == param) {
			throw new TriSystemException(AmMessageId.AM005051S);
		}
		File file = new File(AmDesignBusinessRuleUtils.getWorkspacePath(list), param.getUserId() + File.separator);

		return file;
	}

	/**
	 * 資産削除の申請時に使用する作業フォルダの絶対パスを取得します。 <br>
	 * 決められた業務ルールに従い絶対パスを算出します。
	 *
	 * @param info アプリケーション層で共通に使用するパラメタインタフェース
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException 取得元のマスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getDelApplyDefTempFilePath(List<Object> list) throws TriSystemException {

		IDelApplyParamInfo param = AmExtractMessageAddonUtils.getDelApplyParamInfo(list);

		if (null == param) {
			throw new TriSystemException(AmMessageId.AM005052S);
		}
		File file = new File(AmDesignBusinessRuleUtils.getWorkspacePath(list), param.getUserId() + File.separator);

		return file;
	}

	/**
	 * ロットにアクセス可能なグループ名の一覧を取得する
	 *
	 * @param lotDto
	 * @return
	 */
	public static final List<String> getAccessableGroupNameList(ILotDto lotDto) {

		List<String> groupList = new ArrayList<String>();

		if (null != lotDto.getLotGrpLnkEntities()) {
			for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {
				groupList.add(group.getGrpNm());
			}
		}

		return groupList;
	}

	/**
	 * アクセス可能として選択したグループが存在するかをチェックする
	 *
	 * @param lotDto
	 * @param grpDao
	 * @throws TriSystemException
	 */
	public static final void existGroup(ILotDto lotDto, IGrpDao grpDao) throws TriSystemException {

		List<ILotGrpLnkEntity> grpEntityArray = lotDto.getLotGrpLnkEntities();
		if (TriStringUtils.isEmpty(grpEntityArray)) {
			return;
		}

		// グループの存在チェック
		for (ILotGrpLnkEntity grpEntity : grpEntityArray) {

			GrpCondition condition = new GrpCondition();
			condition.setGrpId(grpEntity.getGrpId());
			IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
			if (null == gEntity) {
				throw new TriSystemException(AmMessageId.AM004039F, grpEntity.getGrpId(), grpEntity.getGrpNm());
			}
		}
	}

	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>
	 * アクセス許可グループが、グループテーブルに存在するかのチェック
	 * </p>
	 * <p>
	 * 自分が所属するグループが、アクセス許可グループであるかのチェック
	 * </p>
	 *
	 * @param lotDto チェック対象のロット
	 * @param grpDao IGrpDao
	 * @param groupUsers 自分が所属するグループリスト
	 * @param messageList （OUT）業務エラーコードリスト
	 * @param messageArgsList （OUT）業務エラーパラメタリスト
	 * @throws TriSystemException
	 */
	public static final void checkAccessableGroupForV3(ILotDto lotDto, IGrpDao grpDao, List<IGrpEntity> groups, List<IMessageId> messageList,
			List<String[]> messageArgsList) throws TriSystemException {

		List<ILotGrpLnkEntity> grpEntityArray = lotDto.getLotGrpLnkEntities();
		if (TriStringUtils.isEmpty(grpEntityArray)) {
			return;
		}

		Set<String> myGroupIdSet = new HashSet<String>();
		TriCollectionUtils.collect(groups, TO_GROUP_ID, myGroupIdSet);

		boolean access = false;

		// グループの存在チェック
		for (ILotGrpLnkEntity grpEntity : grpEntityArray) {

			GrpCondition condition = new GrpCondition();
			condition.setGrpId(grpEntity.getGrpId());
			// 自分の所属グループのみ存在チェック
			if (myGroupIdSet.contains(grpEntity.getGrpId())) {

				IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					throw new TriSystemException(AmMessageId.AM004039F, grpEntity.getGrpId(), grpEntity.getGrpNm());
				}

				access = true;
			} else {
				IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					String groupId = (TriStringUtils.isEmpty(grpEntity.getGrpId())) ? "" : grpEntity.getGrpId();
					String groupName = (TriStringUtils.isEmpty(grpEntity.getGrpNm())) ? "" : grpEntity.getGrpNm();

					messageList.add(AmMessageId.AM001030E);
					messageArgsList.add(new String[] { groupId, groupName });
				}
			}
		}

		if (!access) {
			throw new TriSystemException(AmMessageId.AM004040F);
		}

	}
	
	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>
	 * アクセス許可グループが、グループテーブルに存在するかのチェック
	 * </p>
	 * <p>
	 * 自分が所属するグループが、アクセス許可グループであるかのチェック
	 * </p>
	 *
	 * @param lotDto チェック対象のロット
	 * @param grpDao IGrpDao
	 * @param groupUsers 自分が所属するグループリスト
	 * @param messageList （OUT）業務エラーコードリスト
	 * @param messageArgsList （OUT）業務エラーパラメタリスト
	 * @throws TriSystemException
	 */
	public static final void checkAccessableGroup(ILotDto lotDto, IGrpDao grpDao, List<IGrpEntity> groups, List<IMessageId> messageList,
			List<String[]> messageArgsList) throws TriSystemException {

		List<ILotGrpLnkEntity> grpEntityArray = lotDto.getLotGrpLnkEntities();
		if (TriStringUtils.isEmpty(grpEntityArray)) {
			return;
		}

		Set<String> myGroupIdSet = new HashSet<String>();
		TriCollectionUtils.collect(groups, TO_GROUP_ID, myGroupIdSet);

		boolean access = false;

		// グループの存在チェック
		for (ILotGrpLnkEntity grpEntity : grpEntityArray) {

			GrpCondition condition = new GrpCondition();
			condition.setGrpId(grpEntity.getGrpId());
			// 自分の所属グループのみ存在チェック
			if (myGroupIdSet.contains(grpEntity.getGrpId())) {

				IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					throw new TriSystemException(AmMessageId.AM004039F, grpEntity.getGrpId(), grpEntity.getGrpNm());
				}

				access = true;
			} else {
				IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					String groupId = (TriStringUtils.isEmpty(grpEntity.getGrpId())) ? "" : grpEntity.getGrpId();
					String groupName = (TriStringUtils.isEmpty(grpEntity.getGrpNm())) ? "" : grpEntity.getGrpNm();

					messageList.add(AmMessageId.AM001030E);
					messageArgsList.add(new String[] { groupId, groupName });
				}
			}
		}

		if (!access) {
			throw new BusinessException(AmMessageId.AM001135E);
		}

	}

	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>
	 * アクセス許可グループが、グループテーブルに存在するかのチェック
	 * </p>
	 * <p>
	 * 自分が所属するグループが、アクセス許可グループであるかのチェック
	 * </p>
	 *
	 * @param lotDto チェック対象のロット
	 * @param grpDao IGroupDao
	 * @param groups 自分が所属するグループリスト
	 * @throws TriSystemException
	 */
	public static final void checkAccessableGroup(ILotDto lotDto, IGrpDao grpDao, List<IGrpEntity> groups) throws TriSystemException {

		List<ILotGrpLnkEntity> groupEntityArray = lotDto.getLotGrpLnkEntities();
		if (TriStringUtils.isEmpty(groupEntityArray)) {
			return;
		}

		Set<String> myGroupIdSet = new HashSet<String>();
		TriCollectionUtils.collect(groups, TO_GROUP_ID, myGroupIdSet);

		boolean access = false;

		// グループの存在チェック
		for (ILotGrpLnkEntity grpEntity : groupEntityArray) {

			// 自分の所属グループのみ存在チェック
			if (myGroupIdSet.contains(grpEntity.getGrpId())) {

				GrpCondition condition = new GrpCondition();
				condition.setGrpId(grpEntity.getGrpId());
				IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					throw new TriSystemException(AmMessageId.AM004039F, grpEntity.getGrpId(), grpEntity.getGrpNm());
				}

				access = true;
			}
		}

		if (!access) {
			throw new TriSystemException(AmMessageId.AM004040F);
		}

	}

	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>
	 * 自分が所属するグループが、アクセス許可グループであるかのチェック
	 * </p>
	 *
	 * @param allowGroupIdSet ロットに登録されたアクセス許可グループ
	 * @param groupUsers 自分が所属するグループリスト
	 * @throws TriSystemException
	 */
	public static void checkAccessableGroup(Set<String> allowGroupIdSet, List<IGrpEntity> groupUsers) throws TriSystemException {

		if (TriCollectionUtils.isEmpty(allowGroupIdSet)) {
			return;
		}

		// アクセス許可グループであるかのチェック
		for (IGrpEntity groupUser : groupUsers) {
			if (allowGroupIdSet.contains(groupUser.getGrpId())) {
				return;
			}
		}

		throw new TriSystemException(AmMessageId.AM004040F);
	}

	/**
	 * ロット指定した特定グループ名の一覧を取得する
	 *
	 * @param pjtLotDto
	 * @return
	 */
	public static final List<String> getSpecifiedGroupNameList(ILotDto pjtLotDto) {

		List<String> groupList = new ArrayList<String>();

		if (null != pjtLotDto.getLotMailGrpLnkEntities()) {
			for (ILotMailGrpLnkEntity group : pjtLotDto.getLotMailGrpLnkEntities()) {
				groupList.add(group.getGrpNm());
			}
		}

		return groupList;
	}

	/**
	 * メール送信先として選択したグループが存在するかをチェックする
	 *
	 * @param pjtLotDto
	 * @param grpDao
	 * @param messageList （OUT）業務エラーコードリスト
	 * @param messageArgsList （OUT）業務エラーパラメタリスト
	 * @throws TriSystemException
	 */
	public static final void checkSpecifiedGroup(ILotDto pjtLotDto, IGrpDao grpDao, List<IMessageId> messageList, List<String[]> messageArgsList)
			throws TriSystemException {

		List<ILotMailGrpLnkEntity> groupSpecifiedEntityArray = pjtLotDto.getLotMailGrpLnkEntities();
		if (TriStringUtils.isEmpty(groupSpecifiedEntityArray)) {
			return;
		}

		// グループの存在チェック
		for (ILotMailGrpLnkEntity groupSpecifiedEntity : groupSpecifiedEntityArray) {

			GrpCondition condition = new GrpCondition();
			condition.setGrpId(groupSpecifiedEntity.getGrpId());
			IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
			if (null == gEntity) {
				String groupId = (TriStringUtils.isEmpty(groupSpecifiedEntity.getGrpId())) ? "" : groupSpecifiedEntity.getGrpId();
				String groupName = (TriStringUtils.isEmpty(groupSpecifiedEntity.getGrpNm())) ? "" : groupSpecifiedEntity.getGrpNm();

				messageList.add(AmMessageId.AM001031E);
				messageArgsList.add(new String[] { groupId, groupName });
			}
		}
	}

	/**
	 * メール送信先として選択したグループが存在するかをチェックする
	 *
	 * @param pjtLotEntity
	 * @param groupDao
	 * @throws TriSystemException
	 */
	public static final void existSpecifiedGroup(ILotDto pjtLotDto, IGrpDao grpDao) throws TriSystemException {

		List<ILotMailGrpLnkEntity> groupSpecifiedEntityArray = pjtLotDto.getLotMailGrpLnkEntities();
		if (TriStringUtils.isEmpty(groupSpecifiedEntityArray)) {
			return;
		}

		// グループの存在チェック
		for (ILotMailGrpLnkEntity groupSpecifiedEntity : groupSpecifiedEntityArray) {

			GrpCondition condition = new GrpCondition();
			condition.setGrpId(groupSpecifiedEntity.getGrpId());
			IGrpEntity gEntity = grpDao.findByPrimaryKey(condition.getCondition());
			if (null == gEntity) {
				String groupName = groupSpecifiedEntity.getGrpNm();
				throw new TriSystemException(AmMessageId.AM004041F, groupSpecifiedEntity.getGrpId(), groupName);
			}
		}
	}

	/**
	 * 公開フォルダを取得する
	 *
	 * @param entity
	 * @return
	 */
	public static String getPublicDirectoryPath(ILotEntity entity) {

		File inOutBox = new File(entity.getInOutBox());

		return TriStringUtils.convertPath(inOutBox.getParentFile().getParentFile());

	}

	/**
	 * 非公開フォルダを取得する
	 *
	 * @param entity
	 * @return
	 */
	public static String getPrivateDirectoryPath(ILotEntity entity) {

		File systemInBox = new File(entity.getSysInBox());

		return TriStringUtils.convertPath(systemInBox.getParentFile().getParentFile());

	}

	/**
	 * 指定されたファイルのパスと実資産を比較し、大文字／小文字が正しいかをチェックする。 <br>
	 * SCMでは、大文字／小文字を識別するが、windows環境では、同一視されるため、windows環境にSCMを設定
	 * している場合は、大文字／小文字を正しくチェックする必要がある。 <br>
	 * <br>
	 * このメソッドは、既存資産へのチェックを想定しており、新規追加資産に対しては、 業務エラーとせずに常にfalseを戻す。
	 *
	 * @param root （IN）相対パスまでのroot
	 * @param filePath （IN）検査するファイルのフルパス
	 * @param messageList （OUT）業務エラーコードリスト
	 * @param messageArgsList （OUT）業務エラーパラメタリスト
	 * @return
	 */
	public static boolean checkCanonicalPath(File root, String filePath, List<IMessageId> messageList, List<String[]> messageArgsList) {

		boolean isSuccess = false;
		String checkFilePath = (TriStringUtils.isEmpty(filePath)) ? "" : filePath;

		try {
			File checkFile = new File(checkFilePath);
			if (!checkFile.exists()) {
				return isSuccess;
			}

			String canonicalPath = checkFile.getCanonicalPath();

			checkFilePath = TriStringUtils.convertPath(checkFilePath);
			canonicalPath = TriStringUtils.convertPath(canonicalPath);

			if (checkFilePath.equals(canonicalPath)) {
				isSuccess = true;
			} else {
				String tempPath1 = TriStringUtils.convertRelativePath(root, checkFilePath);
				String tempPath2 = TriStringUtils.convertRelativePath(root, canonicalPath);
				messageList.add(AmMessageId.AM001120E);
				messageArgsList.add(new String[] { tempPath1, tempPath2 });
			}

		} catch (IOException e) {
			throw new TriSystemException(AmMessageId.AM004042F, e, checkFilePath);
		}

		return isSuccess;
	}

	/**
	 * ロットから利用可能なリリース環境名の一覧を取得する
	 *
	 * @param pjtLotDto
	 * @return
	 */
	public static final List<String> getRelEnvNameList(ILotDto pjtLotDto) {

		List<String> groupList = new ArrayList<String>();

		if (null != pjtLotDto.getLotRelEnvLnkEntities()) {
			for (ILotRelEnvLnkEntity env : pjtLotDto.getLotRelEnvLnkEntities()) {
				groupList.add(env.getBldEnvNm());
			}
		}

		return groupList;
	}
	
	public static final List<String> getAllGroupName (IGrpDao groupDao) {
		List<String> groupNameList = new ArrayList<String>();
		List<IGrpEntity> groups = groupDao.find((new GrpCondition()).getCondition());
		if (null != groups) {
			for (IGrpEntity group : groups) {
				groupNameList.add(group.getGrpNm());
			}
		}
		return groupNameList;
	}

}
