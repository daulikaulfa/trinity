package jp.co.blueship.tri.am;

import java.io.File;
import java.io.FileFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class AmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 以下のファイルを除外するフィルターを取得します。
	 * <li>ＳＣＭ固有のファイル
	 * <li>資産対象外のファイル
	 *
	 * @param lotDto ロットDTO
	 * @return 生成したフィルターを戻します。
	 */
	public static final FileFilter getNeglectFileFilter( ILotDto lotDto ) {
		final ILotEntity lot = lotDto.getLotEntity();
		final List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true );

		return new FileFilter() {
			public boolean accept(File pathname) {
				if ( TriFileUtils.isSCM( pathname ) )
					return false;


				List<String> neglectPaths =  sheet.getValueList(AmDesignBeanId.neglectPath);

				{
					File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lot );

					List<File> files = new ArrayList<File>();
					for ( ILotMdlLnkEntity module : mdlEntities ) {
						File modulePath = new File( masterPath, module.getMdlNm() );
						files.add( modulePath );
					}

					for ( File neglectFile : files ) {
						for ( String neglectPath : neglectPaths ) {
							if ( pathname.equals(new File( neglectFile, neglectPath )) )
								return false;
						}
					}
				}

				return true;
			}
		};
	}

	/**
	 * ページ制御情報からページ番号情報に設定します。
	 * @param dest ページ番号情報
	 * @param src ページ制御
	 * @return ページ番号情報を戻します。
	 */
	public static final IPageNoInfo convertPageNoInfo( IPageNoInfo dest, ILimit src ) {

		if ( null == dest ){
			throw new TriSystemException( AmMessageId.AM005038S );
		}
		if ( null == src ) {
			dest.setMaxPageNo( new Integer(0) );
			dest.setSelectPageNo( new Integer(0) );
			return dest;
		}

		dest.setMaxPageNo( new Integer(src.getPageBar().getMaximum()) );
		dest.setSelectPageNo( new Integer(src.getPageBar().getValue()) );

		dest.setViewRows(src.getViewRows());
		dest.setMaxRows(src.getMaxRows());

		return dest;
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param headEntity HEADエンティティ
	 * @return 取得した情報を戻します
	 */
	public static final File getMasterWorkPath(IHeadEntity headEntity) {

		return new File( headEntity.getMwPath() );
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @return 取得した情報を戻します
	 */
	public static final File getMasterWorkPath(ILotEntity lotEntity) {

		return new File( lotEntity.getLotMwPath() );
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static File getMasterWorkPath( List<Object> list )
			throws TriSystemException {

		ILotDto dto = AmExtractEntityAddonUtils.extractPjtLot( list );

		if ( null == dto || null == dto.getLotEntity() ) {
			throw new TriSystemException( AmMessageId.AM005039S );

		} else {

			File file = new File( dto.getLotEntity().getLotMwPath() );

			return file;
		}
	}

	/**
	 * 原本のリポジトリを取得します。
	 *
	 * @param mdlEntity モジュールエンティティ
	 * @param reposDtoList VCS・リポジトリDTO
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final String getMasterRepository(
			ILotMdlLnkEntity mdlEntity, List<IVcsReposDto> reposDtoList ) throws TriSystemException {

		if ( null == mdlEntity ){
			throw new TriSystemException( AmMessageId.AM005040S );
		}
		if ( null == reposDtoList ){
			throw new TriSystemException( AmMessageId.AM005041S );
		}

		String repository = null;

		for ( IVcsReposDto reposDto : reposDtoList ) {

			if ( mdlEntity.getMdlNm().equals( reposDto.getVcsMdlEntity().getMdlNm() )) {
				repository =  reposDto.getVcsReposEntity().getVcsReposUrl();
				break;
			}
		}

		if ( null == repository ){
			throw new TriSystemException( AmMessageId.AM004035F );
		}
		return repository;
	}

	/**
	 * ワークスペースフォルダを取得します。
	 * <br>パラメタのlistには、以下の情報が含まれていれば、パスを取得します。
	 * <li>LendsReturnInfo
	 * <br>
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException 取得元のマスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspacePath( List<Object> list )
			throws TriSystemException {

		ILotDto dto = AmExtractEntityAddonUtils.extractPjtLot( list );

		return getWorkspacePath( (null == dto)? null: dto.getLotEntity() );
	}

	/**
	 * ワークスペースフォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspacePath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( AmMessageId.AM005011S );
		}
		if ( null == entity.getWsPath() ){
			throw new TriSystemException( AmMessageId.AM004036F );
		}
		file = new File( entity.getWsPath() );

		return file;
	}

	/**
	 * 履歴フォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryPath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( AmMessageId.AM005011S );
		}
		if ( null == entity.getHistPath() ){
			throw new TriSystemException( AmMessageId.AM004037F , entity.getHistPath() );
		}
		file = new File( entity.getHistPath() );

		if ( ! file.isDirectory() ) {
			throw new TriSystemException( AmMessageId.AM004038F , file.getPath() );
		}

		return file;
	}

	/**
	 * 公開(InOutBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getInOutBoxPath( ILotEntity lotEntity ) throws TriSystemException  {
		if ( null == lotEntity ) {
			throw new TriSystemException( AmMessageId.AM005042S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getInOutBox() ) ) {
			throw new TriSystemException( AmMessageId.AM005043S );
		}

		File file = new File( lotEntity.getInOutBox() );

		return file;
	}

	/**
	 * 公開(InOutBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static String getInOutBoxPathString( ILotEntity lotEntity ) throws TriSystemException  {

		if ( null == lotEntity ) {
			throw new TriSystemException( AmMessageId.AM005042S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getInOutBox() ) ) {
			throw new TriSystemException( AmMessageId.AM005043S );
		}

		return lotEntity.getInOutBox();
	}

	/**
	 * 公開(InOutBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @param assetApplyEntity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getInOutBoxAssetBasePath(
			ILotEntity lotEntity, IAreqEntity assetApplyEntity, AmDesignEntryKeyByDirectory id ) throws TriSystemException  {

		File file = new File( 	TriStringUtils.linkPath( getInOutBoxPathString( lotEntity), sheet.getValue( id ) ),
								TriStringUtils.linkPath( assetApplyEntity.getGrpNm(),  assetApplyEntity.getPjtId() ));

		return file;
	}

	/**
	 * 公開(InOutBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param list 検索するリスト
	 * @param assetApplyEntity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getInOutBoxAssetBasePath(
			List<Object> list, IAreqEntity assetApplyEntity, AmDesignEntryKeyByDirectory id ) throws TriSystemException  {

		ILotDto dto = AmExtractEntityAddonUtils.extractPjtLot(list);
		ILotEntity lotEntity = (null == dto)? null: dto.getLotEntity();

		return getInOutBoxAssetBasePath( lotEntity, assetApplyEntity, id );
	}

	/**
	 * （共有）公開(InOutBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param assetApplyEntity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static String getInOutBoxAssetBaseSharePath(
			IAreqEntity assetApplyEntity,
			AmDesignEntryKeyByDirectory id ) throws TriSystemException  {

		String root = TriStringUtils.linkPath( sheet.getValue(UmDesignEntryKeyByCommon.sharePublicPath), sheet.getValue(AmDesignEntryKeyByChangec.inOutBox) );
		root = TriStringUtils.linkPath( root, assetApplyEntity.getLotId() );

		return TriStringUtils.linkPathByBackSlash(
				TriStringUtils.linkPath( root, sheet.getValue( id )),
				TriStringUtils.linkPath( assetApplyEntity.getGrpNm(),  assetApplyEntity.getPjtId() ));
	}

	/**
	 * 内部(SystemInBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getSystemInBoxPath( ILotEntity lotEntity ) throws TriSystemException  {

		if ( null == lotEntity ) {
			throw new TriSystemException( AmMessageId.AM005042S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getSysInBox() ) ) {
			throw new TriSystemException( AmMessageId.AM005043S );
		}

		File file = new File( 	lotEntity.getSysInBox()  );

		return file;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getSystemInBoxAssetBasePath( ILotEntity lotEntity ) throws TriSystemException  {
		File systemInBox = getSystemInBoxPath( lotEntity );

		return systemInBox;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param dto ロットDTO
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getSystemInBoxAssetBasePath( ILotDto dto ) throws TriSystemException  {

		ILotEntity lotEntity = (null == dto)? null: dto.getLotEntity();

		return getSystemInBoxAssetBasePath( lotEntity );
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getSystemInBoxAssetBasePath( List<Object> list ) throws TriSystemException  {

		ILotDto dto = AmExtractEntityAddonUtils.extractPjtLot(list);
		ILotEntity lotEntity = (null == dto)? null: dto.getLotEntity();

		return getSystemInBoxAssetBasePath( lotEntity );
	}

	/**
	 * 非公開(private)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。取得出来ない場合、nullを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getPrivatePath( ILotEntity lotEntity ) throws TriSystemException  {
		File systemInBox = AmDesignBusinessRuleUtils.getSystemInBoxPath( lotEntity );

		if ( null != systemInBox.getParent() ) {
			File systemInBoxParent = systemInBox.getParentFile();

			if ( systemInBoxParent.isDirectory() && lotEntity.getLotId().equals( systemInBoxParent.getName() ) ) {
				return systemInBoxParent;
			}
		}

		return null;
	}

	/**
	 * サービス単位のログの管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param flowAction 実行サービス名ID
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspaceLogPath( ILotEntity lotEntity, String flowActionId ) throws TriSystemException {
		File file = null;

		File workspaceFile = getWorkspacePath( lotEntity );

		if ( null == flowActionId ){
			throw new TriSystemException( AmMessageId.AM005044S );
		}
		file = new File( workspaceFile, TriStringUtils.linkPath(
												sheet.getValue( UmDesignEntryKeyByCommon.logRelationPath ),
												flowActionId )
										 );

		return file;
	}

	/**
	 * 貸出資産申請の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getLendAssetApplyNoPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005045S );
		}

		File file = new File( 	getInOutBoxAssetBasePath( list, entity, AmDesignEntryKeyByDirectory.lendAssetRelationPath ),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 貸出資産申請の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getLendAssetApplyNoPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( list );

		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractReturnApply( list );

		return getLendAssetApplyNoPath( list, dto.getAreqEntity() );
	}

	/**
	 * Gets the storage share folder path of the checkout request ID.
	 *
	 * @param entity checkout request entity
	 * @return It returns the storage folder path
	 * @throws TriSystemException
	 */
	public static String getSharePublicPathOfCheckoutRequest( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005045S );
		}

		return TriStringUtils.linkPathByBackSlash(
				getInOutBoxAssetBaseSharePath( entity, AmDesignEntryKeyByDirectory.lendAssetRelationPath ),
				getAssetApplyPathName( entity ));
	}

	/**
	 * Gets the storage share folder path of the checkout request ID.
	 *
	 * @param list parameter list
	 * @return It returns the storage folder path
	 * @throws TriSystemException
	 */
	public static String getSharePublicPathOfCheckoutRequest( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( list );

		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractReturnApply( list );

		return getSharePublicPathOfCheckoutRequest( dto.getAreqEntity() );
	}

	/**
	 * 返却情報申請の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getReturnInfoApplyNoPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005046S );
		}

		File file = new File( 	getInOutBoxAssetBasePath( list, entity, AmDesignEntryKeyByDirectory.returnInfoRelationPath ),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 返却情報申請の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getReturnInfoApplyNoPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( list );
		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractLendApply(list);

		return getReturnInfoApplyNoPath( list, dto.getAreqEntity() );
	}

	/**
	 * Gets the storage share folder path of the check-in request ID.
	 *
	 * @param entity checkout request entity
	 * @return It returns the storage folder path
	 * @throws TriSystemException
	 */
	public static String getSharePublicPathOfCheckinRequest( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005046S );
		}

		return TriStringUtils.linkPathByBackSlash(
				getInOutBoxAssetBaseSharePath( entity, AmDesignEntryKeyByDirectory.returnInfoRelationPath ),
				getAssetApplyPathName( entity ));
	}

	/**
	 * Gets the storage share folder path of the check-in request ID.
	 *
	 * @param list parameter list
	 * @return It returns the storage folder path
	 * @throws TriSystemException
	 */
	public static String getSharePublicPathOfCheckinRequest( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( list );

		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractLendApply( list );

		return getSharePublicPathOfCheckinRequest( dto.getAreqEntity() );
	}

	/**
	 * 返却資産申請の申請番号単位の格納フォルダ名（バックアップ）を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoBackupPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005046S );
		}


		File file = new File( 	getInOutBoxAssetBasePath( list, entity, AmDesignEntryKeyByDirectory.returnAssetBackupRelationPath ),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 返却資産申請の申請番号単位の格納フォルダ名（バックアップ）を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoBackupPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( list );
		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractLendApply(list);

		return getReturnAssetApplyNoBackupPath( list, dto.getAreqEntity() );
	}

	/**
	 * 返却資産承認の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetPath( List<Object> list )
			throws TriSystemException {

		File file = new File( 	getSystemInBoxAssetBasePath( list ).getPath());

		return file;
	}

	/**
	 * 返却資産承認の格納フォルダ名を取得します。
	 *
	 * @param lotDto ロットDTO
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetPath( ILotDto lotDto )
			throws TriSystemException {

		File file = new File( 	getSystemInBoxAssetBasePath( lotDto ).getPath());

		return file;
	}

	/**
	 * 返却資産承認の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param lotDto ロットDTO
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoPath( ILotDto lotDto, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005046S );
		}

		File file = new File( 	getReturnAssetPath( lotDto ).getPath(),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 返却資産承認の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005046S );
		}

		File file = new File( 	getReturnAssetPath( list ).getPath(),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 返却資産承認の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( list );
		if ( null == dto )
			dto = AmExtractEntityAddonUtils.extractLendApply( list );

		return getReturnAssetApplyNoPath( list, dto.getAreqEntity() );
	}

	/**
	 * 削除資産定義の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param applyInfoNo 検索する申請番号
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getDelApplyDefApplyNoPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractDeleteApply( list );

		if ( null == dto ){
			throw new TriSystemException( AmMessageId.AM005047S );
		}
		File file = new File( TriStringUtils.linkPathBySlash(
									sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
									sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefUploadRelativePath )), getDelApplyDefPathName( dto.getAreqEntity() ) );

		return file;
	}

	/**
	 * 貸出資産定義の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param applyInfoNo 検索する申請番号
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getLendApplyDefApplyNoPath( List<Object> list )
			throws TriSystemException {

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( list );

		if ( null == dto ){
			throw new TriSystemException( AmMessageId.AM005048S );
		}
		File file = new File(
				TriStringUtils.linkPathBySlash(
				sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
				sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefUploadRelativePath )), getLendApplyDefPathName( dto.getAreqEntity() ) );

		return file;
	}

	/**
	 * 申請の格納フォルダ名を取得します。
	 *
	 * @param entity 貸出返却情報申請
	 * @return 取得した格納フォルダ名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final String getAssetApplyPathName( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
//			throw new SystemException( "メソッドの使用方法に誤り :entity == null \n" );
			throw new TriSystemException( AmMessageId.AM005049S );
		}

		return entity.getAreqId();
	}

	/**
	 * 削除資産定義の格納フォルダ名を取得します。
	 *
	 * @param entity 削除情報申請
	 * @return 取得した格納フォルダ名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final String getDelApplyDefPathName( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005049S );
		}

		return entity.getAreqId();
	}

	/**
	 * 貸出資産定義の格納フォルダ名を取得します。
	 *
	 * @param entity 貸出返却情報申請
	 * @return 取得した格納フォルダ名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final String getLendApplyDefPathName( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( AmMessageId.AM005049S );
		}

		return entity.getAreqId();
	}

	/**
	 * 添付ファイルのパスの作成を行う
	 * @param areqId AreqID
	 * @param fileName 添付ファイル名
	 * @param seqNo attachment file sequence number
	 * @return 添付ファイルのパス
	 */
	public static File getAttachmentFilePath(String areqId , String fileName, String seqNo){
		String basePath = DesignSheetFactory.getDesignSheet().getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		String applyNo = areqId ;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		String append1 = seqNo ;
		String appendPath1 = TriStringUtils.linkPathBySlash( applyNoPath , append1 ) ;

		return new File(appendPath1 , fileName);
	}

	/**
	 * テンプレートフォルダを取得します。
	 *
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getTemplatePath() throws TriSystemException {

		return new File(
				sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
				sheet.getValue( UmDesignEntryKeyByCommon.templatesRelativePath ));
	}

	/**
	 * ロット作成時に、ブランチタグ発行直前でＨＥＡＤに付与するバージョンタグを取得する。
	 *
	 * @return ロット作成時に、ブランチタグ発行直前でＨＥＡＤに付与するバージョンタグ。
	 */
	public static final String getLotBaseVersionTag() {
		String prefix = sheet.getValue( AmDesignEntryKeyByChangec.lotBaseVersionTagPrefix );
		String value = sheet.getValue( AmDesignEntryKeyByChangec.tagDateFormat );

		SimpleDateFormat format = null;

		try {
			format = new SimpleDateFormat( value );
		} catch ( IllegalArgumentException iae ) {
			throw new TriSystemException( AmMessageId.AM004063F , iae , value );
		}

		String dateFormat = TriDateUtils.convertViewDateFormat( TriDateUtils.getSystemDate(), format );

		return prefix + dateFormat;
	}
	/**
	 * ロット作成時に作成するディレクトリのパスを取得する。
	 * <br>ロット作成時に、各種ディレクトリを作成するが、その際、公開用／非公開用のフォルダに
	 * 振り分けられる。
	 * 公開／非公開となるフォルダは業務ルールにより決められるため、このメソッド内で、
	 * どちらのディレクトリを取得するかを判定し、パスを戻す。
	 *
	 * @param lotNo ロット番号
	 * @param directoryPathPublic 公開ディレクトリのパス
	 * @param directoryPathPrivate 非公開ディレクトリのパス
	 * @param id ディレクトリの相対パス示すカスタマイズ項目
	 * @return ロット作成時に同時に作成するディレクトリのパス。
	 */
	public static final String getLotDirectory( 	String lotId ,
													String directoryPathPublic ,
													String directoryPathPrivate ,
													AmDesignEntryKeyByChangec id ) {
		String value = sheet.getValue( id ) ;
		String path = null;

		if( AmDesignEntryKeyByChangec.inOutBox.equals( id ) ||
			AmDesignEntryKeyByChangec.workPath.equals( id ) ||
			AmDesignEntryKeyByChangec.historyPath.equals( id ) ) {

			path = TriStringUtils.linkPathBySlash( directoryPathPublic , value ) ;
			path = TriStringUtils.linkPathBySlash( path , lotId ) ;


		} else if( AmDesignEntryKeyByChangec.workspace.equals( id ) ||
				AmDesignEntryKeyByChangec.systemInBox.equals( id ) ) {

			path = TriStringUtils.linkPathBySlash( directoryPathPrivate , lotId ) ;
			path = TriStringUtils.linkPathBySlash( path , value ) ;

		} else {
			throw new TriSystemException( AmMessageId.AM004064F , id.toString() );
		}

		return path ;
	}

	/**
	 * ロット作成時のブランチタグを取得する。
	 *
	 * @return ロット作成時のブランチタグ。
	 */
	public static final String getLotBranchTag() {
		String prefix = sheet.getValue( AmDesignEntryKeyByChangec.lotBranchTagPrefix );
		String value = sheet.getValue( AmDesignEntryKeyByChangec.tagDateFormat );

		SimpleDateFormat format = null;

		try {
			format = new SimpleDateFormat( value );
		} catch ( IllegalArgumentException iae ) {
			throw new TriSystemException( AmMessageId.AM004063F, iae , value );
		}

		String dateFormat = TriDateUtils.convertViewDateFormat( TriDateUtils.getSystemDate(), format );

		return prefix + dateFormat;
	}
	/**
	 * ロット作成時のブランチ開始直後に付加するバージョンタグを取得する。
	 *
	 * @return ロット作成時のブランチタグ。
	 */
	public static final String getLotVersionTag() {
		String prefix = sheet.getValue( AmDesignEntryKeyByChangec.lotVersionTagPrefix );
		String value = sheet.getValue( AmDesignEntryKeyByChangec.tagDateFormat );

		SimpleDateFormat format = null;

		try {
			format = new SimpleDateFormat( value );
		} catch ( IllegalArgumentException iae ) {
			throw new TriSystemException( AmMessageId.AM004063F , iae , value );
		}

		String dateFormat = TriDateUtils.convertViewDateFormat( TriDateUtils.getSystemDate(), format );

		return prefix + dateFormat;
	}
}
