package jp.co.blueship.tri.am;

import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates.*;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.IResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelection;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ResourceSelectionFolderView;
import jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceFolderView;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。 ExtractEntityAddonUtilの派生クラスです。依存性を解消するために作成しました。
 *
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class AmExtractEntityAddonUtils {

	/**
	 * 指定されたリストから貸出情報申請を取得します。 <br>
	 * 貸出情報申請が複数存在する場合、リストから最初に適合した情報を戻します。 <br>
	 * このメソッドは、ワークフロー単位に貸出情報申請が１つである事を前提としています。
	 *
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IAreqDto extractLendApply(List<Object> list) {

		return FluentList.//
				from(list).//
				select(isInstanceOf(IAreqDto.class)).//
				map(AmFluentFunctionUtils.toAreqDto).//
				atFirstOf(AmFluentFunctionUtils.isReturningRequest);
	}

	/**
	 * 指定されたリストから返却申請を取得します。 <br>
	 * 返却申請が複数存在する場合、リストから最初に適合した情報を戻します。 <br>
	 * このメソッドは、ワークフロー単位に返却申請が１つである事を前提としています。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IAreqDto extractReturnApply(List<Object> list) {

		return FluentList.//
				from(list).//
				select(isInstanceOf(IAreqDto.class)).//
				map(AmFluentFunctionUtils.toAreqDto).//
				atFirstOf(AmFluentFunctionUtils.isLendingRequest);

	}

	/**
	 * 指定されたリストから変更管理を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IPjtEntity extractPjt(List<Object> list) {

		return (IPjtEntity) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IPjtEntity.class));
	}

	/**
	 * 指定されたリストから変更管理を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IPjtAvlEntity extractPjtApprove(List<Object> list) {

		return (IPjtAvlEntity) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IPjtAvlEntity.class));

	}
	/**
	 * 指定されたリストから変更管理を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IPjtAvlDto extractPjtDtoApprove(List<Object> list) {

		return (IPjtAvlDto) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IPjtAvlDto.class));

	}

	/**
	 * 指定されたリストからパスワード情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IPassMgtEntity extractPasswordEntity(List<Object> list) {

		return (IPassMgtEntity) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IPassMgtEntity.class));
	}

	/**
	 * 指定されたリストから削除情報申請を取得します。 <br>
	 * 削除情報申請が複数存在する場合、リストから最初に適合した情報を戻します。 <br>
	 * このメソッドは、ワークフロー単位に削除情報申請が１つである事を前提としています。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IAreqDto extractDeleteApply(List<Object> list) {

		return FluentList.//
				from(list).//
				select(isInstanceOf(IAreqDto.class)).//
				map(AmFluentFunctionUtils.toAreqDto).//
				atFirstOf(AmFluentFunctionUtils.isRemovalRequest);
	}

	/**
	 * パラメータリストより申請情報エンティティの配列を取得します。
	 *
	 * @param list パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IAreqDto[] extractAssetApplyArray(List<Object> list) throws TriSystemException {

		return (IAreqDto[]) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IAreqDto[].class));
	}

	/**
	 * パラメータリストより申請情報エンティティを取得します。
	 *
	 * @param list パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IAreqDto extractAssetApply(List<Object> list) throws TriSystemException {

		return (IAreqDto) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IAreqDto.class));
	}

	/**
	 * 指定されたリストからロット情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotDto extractPjtLot(List<Object> list) {

		return (ILotDto) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(ILotDto.class));
	}

	/**
	 * 指定されたリストからリポジトリ情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static List<IVcsReposDto> extractVcsReposDto(List<Object> list) {

		IVcsReposDto[] dtoArray = //
		(IVcsReposDto[]) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(IVcsReposDto[].class));

		return FluentList.from(dtoArray).asList();
	}

	/**
	 * 指定されたリストから資産agentのステータス情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static AgentStatus[] extractAgentStatusArray(List<Object> list) {

		return (AgentStatus[]) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(AgentStatus[].class));

	}

	public static ResourceSelection extractResourceSelection(List<Object> list) {
		return (ResourceSelection) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(ResourceSelection.class));
	}
	
	public static jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection extractLotResourceSelection(List<Object> list) {
		return (jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection) FluentList.//
				from(list).
				atFirstOf(isInstanceOf(jp.co.blueship.tri.am.domainx.lot.beans.dto.ResourceSelection.class));
	}

	@SuppressWarnings("unchecked")
	public static ResourceSelectionFolderView<IResourceViewBean> extractResourceFolderView(List<Object> list) {
		return (ResourceSelectionFolderView<IResourceViewBean>) FluentList.//
				from(list).//
				atFirstOf(isInstanceOf(ResourceSelectionFolderView.class));
	}
	
	@SuppressWarnings("unchecked")
	public static ResourceFolderView<jp.co.blueship.tri.am.domainx.lot.beans.dto.IResourceViewBean> extractLotResourceFolderView(List<Object> list) {
		return (ResourceFolderView<jp.co.blueship.tri.am.domainx.lot.beans.dto.IResourceViewBean>) FluentList.//
				from(list).
				atFirstOf(isInstanceOf(ResourceFolderView.class));
	}

	@SuppressWarnings("unchecked")
	public static Map<String, IExtEntity> extractExtEntity(List<Object> list) {
		return (Map<String, IExtEntity>) FluentList.
				from(list).
				atFirstOf(isInstanceOf(Map.class));
	}

}
