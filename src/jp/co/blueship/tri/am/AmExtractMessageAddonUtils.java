package jp.co.blueship.tri.am;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 変更管理を扱うユーティリティークラスです。
 * <br>キャッシュエンティティを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class AmExtractMessageAddonUtils {

	/**
	 * 指定されたリストから指定された返却資産状況情報を取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IReturnProcessParam extractReturnProcessParam( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( obj instanceof IReturnProcessParam ) {
				IReturnProcessParam entity = (IReturnProcessParam)obj;

				return entity;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストから削除申請資産ファイルパス情報を取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final IDelApplyFileResult[] extractDelApplyFileResults( List<Object> list )
		throws TriSystemException {

		IDelApplyFileResult[] files = null;
		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();
			if ( !(obj instanceof IDelApplyFileResult[]) )
				continue;

			if ( obj instanceof IDelApplyBinaryFileResult[] )
				continue;

			files = (IDelApplyFileResult[])obj;
			break;
		}

		return files;
	}

	/**
	 * 指定されたリストから削除申請資産（モジュール）ファイルパス情報を取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final IDelApplyBinaryFileResult[] extractDelApplyBinaryFileResults( List<Object> list )
		throws TriSystemException {

		IDelApplyBinaryFileResult[] files = null;
		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();
			if ( !(obj instanceof IDelApplyBinaryFileResult[]) )
				continue;

			files = (IDelApplyBinaryFileResult[])obj;
			break;
		}

		return files;
	}

	/**
	 * 指定されたリストから貸出申請資産ファイルパス情報を取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final IEhLendApplyFileResult[] extractLendApplyFileResults( List<Object> list )
		throws TriSystemException {

		IEhLendApplyFileResult[] files = null;
		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();
			if ( !( obj instanceof IEhLendApplyFileResult[] ))
				continue;

			files = (IEhLendApplyFileResult[])obj;
			break;
		}

		if ( null == files ){
			throw new TriSystemException( AmMessageId.AM005050S );
		}
		return files;
	}

	/**
	 * パラメータリストより貸出申請情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IEhLendParamInfo getLendParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IEhLendParamInfo ) {
				return ((IEhLendParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストより削除申請情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IDelApplyParamInfo getDelApplyParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IDelApplyParamInfo ) {
				return ((IDelApplyParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストより承認情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IApproveParamInfo getApproveParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IApproveParamInfo ) {
				return ((IApproveParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストより承認情報パラメタの配列を取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IApproveParamInfo[] getApproveParamInfoArray( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IApproveParamInfo[] ) {
				return ((IApproveParamInfo[])obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりコンフリクトチェック情報パラメタの配列を取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IMergeParamInfo[] getMergeParamInfoArray( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IMergeParamInfo[] ) {
				return ((IMergeParamInfo[])obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりロット情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final LotParamInfo getLotParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof LotParamInfo ) {
				return ((LotParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりロット情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final MergeEditInputBean getMergeEditInputBean( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof MergeEditInputBean ) {
				return ((MergeEditInputBean)obj);
			}
		}

		return null;
	}

	/**
	 * 比較元と比較先の差分ファイル情報を取得します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 */
	public static final IFileDiffResult[] getFileDiffResult(  List<Object> list ) {

		List<IFileDiffResult> outList = new ArrayList<IFileDiffResult>();

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( !(obj instanceof IFileDiffResult[]) )
				continue;

			return (IFileDiffResult[])obj;
		}

		return outList.toArray( new IFileDiffResult[0] );
	}

}
