package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.fw.ws.WSSearchConditionUtils.*;

import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.ws.vo.LotPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.LotSearchConditions;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;

public class LotSqlConditionBuilder {

	private LotPrimaryKey lotPrimaryKey;
	private LotSearchConditions lotSearchConditions;

	public LotSqlConditionBuilder(LotPrimaryKey lotPrimaryKey) {
		this.lotPrimaryKey = lotPrimaryKey;
	}

	public LotSqlConditionBuilder(LotSearchConditions lotSearchConditions) {
		this.lotSearchConditions = lotSearchConditions;
	}

	public IJdbcCondition buildByPrimaryKey() {
		LotCondition condition = new LotCondition();
		condition.setLotId(lotPrimaryKey.getLotId());

		return condition;
	}

	public IJdbcCondition build() {
		LotCondition condition = new LotCondition();
		condition.setAllowListView(toStatusFlg(lotSearchConditions.getAllowListView()));
		condition.setBldEnvId(lotSearchConditions.getBldEnvId());
		condition.setCloseCmt(lotSearchConditions.getCloseCmt());
		condition.setCloseTimestamp(toTimeStamp(lotSearchConditions.getCloseTimestamp()));
		condition.setCloseUserId(lotSearchConditions.getCloseUserId());
		condition.setCloseUserNm(lotSearchConditions.getCloseUserNm());
		condition.setContent(lotSearchConditions.getContent());
		condition.setDelCmt(lotSearchConditions.getDelCmt());
		condition.setDelStsId(toStatusFlg(lotSearchConditions.getDelStsId()));
		condition.setDelTimestamp(toTimeStamp(lotSearchConditions.getDelTimestamp()));
		condition.setDelUserId(lotSearchConditions.getDelUserId());
		condition.setDelUserNm(lotSearchConditions.getDelUserNm());
		condition.setFullBldEnvId(lotSearchConditions.getFullBldEnvId());
		condition.setHistPath(lotSearchConditions.getHistPath());
		condition.setInOutBox(lotSearchConditions.getInOutBox());
		condition.setIsAssetDup(toStatusFlg(lotSearchConditions.getIsAssetDup()));
		condition.setLotBranchTag(lotSearchConditions.getLotBranchTag());
		condition.setLotId(lotSearchConditions.getLotId());
		condition.setLotLatestBlTag(lotSearchConditions.getLotLatestBlTag());
		condition.setLotLatestVerTag(lotSearchConditions.getLotLatestVerTag());
		condition.setLotMwPath(lotSearchConditions.getLotMwPath());
		condition.setLotNm(lotSearchConditions.getLotNm());
		condition.setStsId(lotSearchConditions.getStsId());
		condition.setSummary(lotSearchConditions.getSummary());
		condition.setSysInBox(lotSearchConditions.getSysInBox());
		condition.setVcsCtgCd(lotSearchConditions.getVcsCtgCd());
		condition.setWsPath(lotSearchConditions.getWsPath());

		return condition;
	}

}
