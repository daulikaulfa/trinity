package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.fw.cmn.utils.TriDateUtils.*;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterViewBean;
import jp.co.blueship.tri.am.ws.vo.AssetRegisterVo;
import jp.co.blueship.tri.am.ws.vo.LotVo;
import jp.co.blueship.tri.am.ws.vo.PjtVo;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;

/**
 *
 * @version V3L10.01
 * @author Takayuki Kubo
 *
 * @version V3L11.01
 * @author Takashi ono
 *
 */
public final class AmWSFunctions {

	private AmWSFunctions() {
		// NOP
	}

	public static final TriFunction<ILotEntity, LotVo> TO_LOT_VO = new TriFunction<ILotEntity, LotVo>() {

		@Override
		public LotVo apply(ILotEntity entity) {

			LotVo vo = new LotVo();
			vo.setAllowListView		( entity.getAllowListView().parseInteger() );
			vo.setBldEnvId			( entity.getBldEnvId() );
			vo.setCloseCmt			( entity.getCloseCmt() );
			vo.setCloseTimestamp	( convertViewDateFormat(entity.getCloseTimestamp()) );
			vo.setCloseUserId		( entity.getCloseUserId() );
			vo.setCloseUserNm		( entity.getCloseUserNm() );
			vo.setContent			( entity.getContent() );
			vo.setDelCmt			( entity.getDelCmt() );
			vo.setDelTimestamp		( convertViewDateFormat(entity.getDelTimestamp()) );
			vo.setDelUserId			( entity.getDelUserId() );
			vo.setDelUserNm			( entity.getDelUserNm() );
			vo.setFullBldEnvId		( entity.getFullBldEnvId() );
			vo.setHistPath			( entity.getHistPath() );
			vo.setInOutBox			( entity.getInOutBox() );
			vo.setIsAssetDup		( entity.getIsAssetDup().parseInteger() );
			vo.setLotBranchTag		( entity.getLotBranchTag() );
			vo.setLotId				( entity.getLotId() );
			vo.setLotLatestBlTag	( entity.getLotLatestBlTag() );
			vo.setLotLatestVerTag	( entity.getLotLatestVerTag() );
			vo.setLotMwPath			( entity.getLotMwPath() );
			vo.setLotNm				( entity.getLotNm() );
			vo.setStsId				( entity.getStsId() );
			vo.setSummary			( entity.getSummary() );
			vo.setSysInBox			( entity.getSysInBox() );
			vo.setVcsCtgCd			( entity.getVcsCtgCd() );
			vo.setWsPath			( entity.getWsPath() );

			return vo;
		}
	};

	public static final TriFunction<IPjtEntity, PjtVo> TO_PJT_VO = new TriFunction<IPjtEntity, PjtVo>() {

		@Override
		public PjtVo apply(IPjtEntity entity) {

			PjtVo vo = new PjtVo();
			vo.setChgFactorId		( entity.getChgFactorId() );
			vo.setChgFactorNo		( entity.getChgFactorNo() );
			vo.setCloseCmt			( entity.getCloseCmt() );
			vo.setCloseTimestamp	( convertViewDateFormat(entity.getCloseTimestamp()) );
			vo.setCloseUserId		( entity.getCloseUserId() );
			vo.setCloseUserNm		( entity.getCloseUserNm() );
			vo.setContent			( entity.getContent() );
			vo.setDelCmt			( entity.getDelCmt() );
			vo.setDelTimestamp		( convertViewDateFormat(entity.getDelTimestamp()) );
			vo.setDelUserId			( entity.getDelUserId() );
			vo.setDelUserNm			( entity.getDelUserNm() );
			vo.setLotId				( entity.getLotId() );
			vo.setPjtId				( entity.getPjtId() );
			vo.setPjtNm				( entity.getPjtNm() );
			vo.setStsId				( entity.getStsId() );
			vo.setSummary			( entity.getSummary() );
			vo.setTestCompCmt		( entity.getTestCompCmt() );
			vo.setTestCompTimestamp	( convertViewDateFormat(entity.getTestCompTimestamp()) );
			vo.setTestCompUserId	( entity.getTestCompUserId() );
			vo.setTestCompUserNm	( entity.getTestCompUserNm() );

			return vo;
		}
	};

	public static final TriFunction<AssetRegisterViewBean, AssetRegisterVo> TO_ASSET_REGISTER_VO = new TriFunction<AssetRegisterViewBean, AssetRegisterVo>() {

		@Override
		public AssetRegisterVo apply(AssetRegisterViewBean bean) {

			AssetRegisterVo vo = new AssetRegisterVo();
			vo.setMdlNm				( convertString(bean.getMdlNm()) );
			vo.setFilePath			( convertString(bean.getFilePath()) );
			vo.setLotId				( convertString(bean.getLotId()) );
			vo.setLotNm				( convertString(bean.getLotNm()) );
			vo.setPjtId				( convertString(bean.getPjtId()) );
			vo.setChgFactorNo		( convertString(bean.getChgFactorNo()) );
			vo.setAreqId			( convertString(bean.getAreqId()) );
			vo.setAreqCtgCode		( convertString(bean.getAreqCtgCd()) );
			vo.setGrpId				( convertString(bean.getGrpId()) );
			vo.setGrpNm				( convertString(bean.getGrpNm()) );
			vo.setAssetReqNm		( convertString(bean.getAssetReqNm()) );
			vo.setContent			( convertString(bean.getContent()) );
			vo.setLendReqUserId		( convertString(bean.getLendReqUserId()) );
			vo.setLendReqUserNm		( convertString(bean.getLendReqUserNm()) );
			vo.setLendReqDateTime	( convertString(bean.getLendReqDateTime()) );
			vo.setRtnReqUserId		( convertString(bean.getRtnReqUserId()) );
			vo.setRtnReqUserNm		( convertString(bean.getRtnReqUserNm()) );
			vo.setRtnReqDateTime	( convertString(bean.getRtnReqDateTime()) );
			vo.setDelReqUserId		( convertString(bean.getDelReqUserId()) );
			vo.setDelReqUserNm		( convertString(bean.getDelReqUserNm()) );
			vo.setDelReqDateTime	( convertString(bean.getDelReqDateTime()) );
			vo.setLendFileRevision	( convertString(bean.getLendFileRev()) );
			vo.setLendFileByteSize	( convertString((null == bean.getLendFileByteSize())? null: String.valueOf(bean.getLendFileByteSize())) );
			vo.setLendFileUpdateDateTime
									( convertString(bean.getLendFileUpdDateTime()) );
			vo.setRtnFileRevision	( convertString(bean.getRtnFileRev()) );
			vo.setRtnFileByteSize	( convertString((null == bean.getRtnFileByteSize())? null: String.valueOf(bean.getRtnFileByteSize())) );
			vo.setRtnFileUpdateDateTime
									( convertString(bean.getRtnFileUpdDateTime()) );
			vo.setDelFileRevision	( convertString(bean.getDelFileRev()) );
			vo.setDelFileByteSize	( convertString((null == bean.getDelFileByteSize())? null: String.valueOf(bean.getDelFileByteSize())) );
			vo.setDelFileUpdateDateTime
									( convertString(bean.getDelFileUpdDateTime()) );
			vo.setChgAppDateTime	( convertString(bean.getPjtAvlDateTime()) );
			vo.setStsId				( convertString(bean.getStsId()) );
			vo.setProcStsId			( convertString(bean.getProcStsId()) );
			vo.setResourceLock		( convertString((bean.isResourceLock())? "1": "0") );

			return vo;
		}

		private String convertString( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return "";
			}

			return value;
		}
	};

}
