package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.fw.ws.WSSearchConditionUtils.*;

import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.ws.vo.PjtPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.PjtSearchConditions;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.ws.WSSearchConditionUtils;

public class PjtSqlConditionBuilder {

	private PjtPrimaryKey pjtPrimaryKey;
	private PjtSearchConditions pjtSearchConditions;

	public PjtSqlConditionBuilder(PjtPrimaryKey pjtPrimaryKey) {
		this.pjtPrimaryKey = pjtPrimaryKey;
	}

	public PjtSqlConditionBuilder(PjtSearchConditions pjtSearchConditions) {
		this.pjtSearchConditions = pjtSearchConditions;
	}

	public IJdbcCondition buildByPrimaryKey() {

		PjtCondition condition = new PjtCondition();
		condition.setPjtId(pjtPrimaryKey.getPjtId());

		return condition;
	}

	public IJdbcCondition build() {

		PjtCondition condition = new PjtCondition();
		condition.setPjtId( pjtSearchConditions.getPjtId() );
		condition.setLotId( pjtSearchConditions.getDelUserNm() );
		condition.setPjtNm( pjtSearchConditions.getPjtNm() );
		condition.setSummary( pjtSearchConditions.getSummary() );
		condition.setContent( pjtSearchConditions.getContent() );
		condition.setChgFactorNo( pjtSearchConditions.getChgFactorNo() );
		condition.setChgFactorId( pjtSearchConditions.getChgFactorId() );
		condition.setStsId( pjtSearchConditions.getStsId() );
		condition.setCloseUserId( pjtSearchConditions.getCloseUserId() );
		condition.setCloseUserNm( pjtSearchConditions.getCloseUserNm() );
		condition.setCloseTimestamp( toTimeStamp(pjtSearchConditions.getCloseTimestamp()) );
		condition.setCloseCmt( pjtSearchConditions.getCloseCmt() );
		condition.setTestCompUserId( pjtSearchConditions.getTestCompUserId() );
		condition.setTestCompUserNm( pjtSearchConditions.getTestCompUserNm() );
		condition.setTestCompTimestamp( toTimeStamp(pjtSearchConditions.getTestCompTimestamp()) );
		condition.setTestCompCmt( pjtSearchConditions.getTestCompCmt() );
		condition.setDelUserId( pjtSearchConditions.getDelUserId() );
		condition.setDelUserNm( pjtSearchConditions.getDelUserNm() );
		condition.setDelTimestamp( toTimeStamp(pjtSearchConditions.getDelTimestamp()) );
		condition.setDelCmt( pjtSearchConditions.getDelCmt() );
		condition.setDelStsId( WSSearchConditionUtils.toStatusFlg(pjtSearchConditions.getDelStsId()) );

		return condition;
	}

}
