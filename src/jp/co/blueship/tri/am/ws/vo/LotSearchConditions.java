package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.ISearchConditions;

/**
 * ロット検索クエリパラメタを格納するためのクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class LotSearchConditions implements ISearchConditions {

	/**
	 * lot-ID
	 */
	@QueryParam("lotId")
	public String lotId;
	/**
	 * lot name
	 */
	@QueryParam("lotNm")
	public String lotNm;
	/**
	 * summary
	 */
	@QueryParam("summary")
	public String summary;
	/**
	 * content
	 */
	@QueryParam("content")
	public String content;
	/**
	 * vcs catalog code
	 */
	@QueryParam("vcsCtgCd")
	public String vcsCtgCd;
	/**
	 * in out box
	 */
	@QueryParam("inOutBox")
	public String inOutBox;
	/**
	 * system in box
	 */
	@QueryParam("sysInBox")
	public String sysInBox;
	/**
	 * lot mw path
	 */
	@QueryParam("lotMwPath")
	public String lotMwPath;
	/**
	 * history path
	 */
	@QueryParam("histPath")
	public String histPath;
	/**
	 * work space path
	 */
	@QueryParam("wsPath")
	public String wsPath;
	/**
	 * build env-ID
	 */
	@QueryParam("bldEnvId")
	public String bldEnvId;
	/**
	 * full build env-ID
	 */
	@QueryParam("fullBldEnvId")
	public String fullBldEnvId;
	/**
	 * is asset suplication
	 */
	@QueryParam("isAssetDup")
	public int isAssetDup;
	/**
	 * lot branch tag
	 */
	@QueryParam("lotBranchTag")
	public String lotBranchTag;
	/**
	 * lot latest version tag
	 */
	@QueryParam("lotLatestVerTag")
	public String lotLatestVerTag;
	/**
	 * lot latest baseline tag
	 */
	@QueryParam("lotLatestBlTag")
	public String lotLatestBlTag;
	/**
	 * allow list view
	 */
	@QueryParam("allowListView")
	public int allowListView;
	/**
	 * AM product activate
	 */
	@QueryParam("amProductActivate")
	public int amProductActivate;
	/**
	 * RM product activate
	 */
	@QueryParam("rmProductActivate")
	public int rmProductActivate;
	/**
	 * sts-ID
	 */
	@QueryParam("stsId")
	public String stsId;
	/**
	 * close user-ID
	 */
	@QueryParam("closeUserId")
	public String closeUserId;
	/**
	 * close user name
	 */
	@QueryParam("closeUserNm")
	public String closeUserNm;
	/**
	 * close time stamp
	 */
	@QueryParam("closeTimestamp")
	public String closeTimestamp;
	/**
	 * close comment
	 */
	@QueryParam("closeCmt")
	public String closeCmt;
	/**
	 * delete user-ID
	 */
	@QueryParam("delUserId")
	public String delUserId;
	/**
	 * delete user name
	 */
	@QueryParam("delUserNm")
	public String delUserNm;
	/**
	 * delete time stamp
	 */
	@QueryParam("delTimestamp")
	public String delTimestamp;
	/**
	 * delete comment
	 */
	@QueryParam("delCmt")
	public String delCmt;
	/**
	 * delete sts-ID
	 */
	@QueryParam("delStsId")
	public int delStsId;

	public String getLotId() {
		return lotId;
	}

	public String getLotNm() {
		return lotNm;
	}

	public String getSummary() {
		return summary;
	}

	public String getContent() {
		return content;
	}

	public String getVcsCtgCd() {
		return vcsCtgCd;
	}

	public String getInOutBox() {
		return inOutBox;
	}

	public String getSysInBox() {
		return sysInBox;
	}

	public String getLotMwPath() {
		return lotMwPath;
	}

	public String getHistPath() {
		return histPath;
	}

	public String getWsPath() {
		return wsPath;
	}

	public String getBldEnvId() {
		return bldEnvId;
	}

	public String getFullBldEnvId() {
		return fullBldEnvId;
	}

	public int getIsAssetDup() {
		return isAssetDup;
	}

	public String getLotBranchTag() {
		return lotBranchTag;
	}

	public String getLotLatestVerTag() {
		return lotLatestVerTag;
	}

	public String getLotLatestBlTag() {
		return lotLatestBlTag;
	}

	public int getAllowListView() {
		return allowListView;
	}

	public String getStsId() {
		return stsId;
	}

	public String getCloseUserId() {
		return closeUserId;
	}

	public String getCloseUserNm() {
		return closeUserNm;
	}

	public String getCloseTimestamp() {
		return closeTimestamp;
	}

	public String getCloseCmt() {
		return closeCmt;
	}

	public String getDelUserId() {
		return delUserId;
	}

	public String getDelUserNm() {
		return delUserNm;
	}

	public String getDelTimestamp() {
		return delTimestamp;
	}

	public String getDelCmt() {
		return delCmt;
	}

	public int getDelStsId() {
		return delStsId;
	}

	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	public void setLotNm(String lotNm) {
		this.lotNm = lotNm;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setVcsCtgCd(String vcsCtgCd) {
		this.vcsCtgCd = vcsCtgCd;
	}

	public void setInOutBox(String inOutBox) {
		this.inOutBox = inOutBox;
	}

	public void setSysInBox(String sysInBox) {
		this.sysInBox = sysInBox;
	}

	public void setLotMwPath(String lotMwPath) {
		this.lotMwPath = lotMwPath;
	}

	public void setHistPath(String histPath) {
		this.histPath = histPath;
	}

	public void setWsPath(String wsPath) {
		this.wsPath = wsPath;
	}

	public void setBldEnvId(String bldEnvId) {
		this.bldEnvId = bldEnvId;
	}

	public void setFullBldEnvId(String fullBldEnvId) {
		this.fullBldEnvId = fullBldEnvId;
	}

	public void setIsAssetDup(int isAssetDup) {
		this.isAssetDup = isAssetDup;
	}

	public void setLotBranchTag(String lotBranchTag) {
		this.lotBranchTag = lotBranchTag;
	}

	public void setLotLatestVerTag(String lotLatestVerTag) {
		this.lotLatestVerTag = lotLatestVerTag;
	}

	public void setLotLatestBlTag(String lotLatestBlTag) {
		this.lotLatestBlTag = lotLatestBlTag;
	}

	public void setAllowListView(int allowListView) {
		this.allowListView = allowListView;
	}

	public void setAmProductActivate(int amProductActivate) {
		this.amProductActivate = amProductActivate;
	}

	public void setRmProductActivate(int rmProductActivate) {
		this.rmProductActivate = rmProductActivate;
	}

	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	public void setCloseUserId(String closeUserId) {
		this.closeUserId = closeUserId;
	}

	public void setCloseUserNm(String closeUserNm) {
		this.closeUserNm = closeUserNm;
	}

	public void setCloseTimestamp(String closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	public void setCloseCmt(String closeCmt) {
		this.closeCmt = closeCmt;
	}

	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}

	public void setDelTimestamp(String delTimestamp) {
		this.delTimestamp = delTimestamp;
	}

	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}

	public void setDelStsId(int delStsId) {
		this.delStsId = delStsId;
	}
}
