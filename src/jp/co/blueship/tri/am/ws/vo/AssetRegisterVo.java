package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IValueObject;

/**
 * 資産台帳のValueObjectクラスです。
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */
public class AssetRegisterVo implements IValueObject {

	/**
	 * Module Name
	 */
	@QueryParam("mdlNm")
	public String mdlNm;
	/**
	 * Resource Path
	 */
	@QueryParam("filePath")
	public String filePath;
	/**
	 * Lot Id
	 */
	@QueryParam("lotId")
	public String lotId;
	/**
	 * Lot Name
	 */
	@QueryParam("lotNm")
	public String lotNm;
	/**
	 * Change Property
	 */
	@QueryParam("pjtId")
	public String pjtId;
	/**
	 * Change Factor No.
	 */
	@QueryParam("chgFactorNo")
	public String chgFactorNo;
	/**
	 * Resource Inquiry Id
	 */
	@QueryParam("areqId")
	public String areqId;
	/**
	 * Resource Category Code
	 */
	@QueryParam("areqCtgCode")
	public String areqCtgCode;
	/**
	 * Group Id
	 */
	@QueryParam("grpId")
	public String grpId;
	/**
	 * Group Name
	 */
	@QueryParam("grpNm")
	public String grpNm;
	/**
	 * Resource Inquiry Subject
	 */
	@QueryParam("assetReqNm")
	public String assetReqNm;
	/**
	 * Content
	 */
	@QueryParam("content")
	public String content;
	/**
	 * Checkout Inquiry User Id
	 */
	@QueryParam("lendReqUserId")
	public String lendReqUserId;
	/**
	 *
	 * Checkout Inquiry User Name
	 */
	@QueryParam("lendReqUserNm")
	public String lendReqUserNm;
	/**
	 * Checkout Inquiry Date Time
	 */
	@QueryParam("lendReqDateTime")
	public String lendReqDateTime;
	/**
	 * Check-in Inquiry User Id
	 */
	@QueryParam("rtnReqUserId")
	public String rtnReqUserId;
	/**
	 *
	 * Check-in Inquiry User Name
	 */
	@QueryParam("rtnReqUserNm")
	public String rtnReqUserNm;
	/**
	 *
	 * Check-in Inquiry Date Time
	 */
	@QueryParam("rtnReqDateTime")
	public String rtnReqDateTime;
	/**
	 * Removal Inquiry User Id
	 */
	@QueryParam("delReqUserId")
	public String delReqUserId;
	/**
	 *
	 * Removal Inquiry User Name
	 */
	@QueryParam("delReqUserNm")
	public String delReqUserNm;
	/**
	 *
	 * Removal Inquiry Date Time
	 */
	@QueryParam("delReqDateTime")
	public String delReqDateTime;
	/**
	 *
	 * Checkout Resource Revision
	 */
	@QueryParam("lendFileRevision")
	public String lendFileRevision;
	/**
	 * Checkout Resource Byte Size
	 */
	@QueryParam("lendFileByteSize")
	public String lendFileByteSize;
	/**
	 * Checkout Resource Date Time
	 */
	@QueryParam("lendFileUpdateDateTime")
	public String lendFileUpdateDateTime;
	/**
	 * Check-in Resource Revision
	 */
	@QueryParam("rtnFileRevision")
	public String rtnFileRevision;
	/**
	 * Check-in Resource Byte Size
	 */
	@QueryParam("rtnFileByteSize")
	public String rtnFileByteSize;
	/**
	 * Check-in Resource Date Time
	 */
	@QueryParam("rtnFileUpdateDateTime")
	public String rtnFileUpdateDateTime;
	/**
	 * Removal Resource Revision
	 */
	@QueryParam("delFileRevision")
	public String delFileRevision;
	/**
	 * Removal Resource Byte Size
	 */
	@QueryParam("delFileByteSize")
	public String delFileByteSize;
	/**
	 * Removal Resource Date Time
	 */
	@QueryParam("delFileUpdateDateTime")
	public String delFileUpdateDateTime;
	/**
	 * Change Property Approval Date Time
	 */
	@QueryParam("chgAppDateTime")
	public String chgAppDateTime;
	/**
	 * Status-ID
	 */
	@QueryParam("stsId")
	public String stsId;
	/**
	 * Process Status-ID
	 */
	@QueryParam("procStsId")
	public String procStsId;
	/**
	 * Resource Lock
	 */
	@QueryParam("resourceLock")
	private String resourceLock;
	/**
	 * Module Nameを取得します。
	 * @return Module Name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * Module Nameを設定します。
	 * @param mdlNm Module Name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	/**
	 * Resource Pathを取得します。
	 * @return Resource Path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * Resource Pathを設定します。
	 * @param filePath Resource Path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}
	/**
	 * Lot Idを取得します。
	 * @return Lot Id
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * Lot Idを設定します。
	 * @param lotId Lot Id
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * Lot Nameを取得します。
	 * @return Lot Name
	 */
	public String getLotNm() {
	    return lotNm;
	}
	/**
	 * Lot Nameを設定します。
	 * @param lotNm Lot Name
	 */
	public void setLotNm(String lotNm) {
	    this.lotNm = lotNm;
	}
	/**
	 * Change Propertyを取得します。
	 * @return Change Property
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * Change Propertyを設定します。
	 * @param pjtId Change Property
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * Change Factor No.を取得します。
	 * @return Change Factor No.
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}
	/**
	 * Change Factor No.を設定します。
	 * @param chgFactorNo Change Factor No.
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	}
	/**
	 * Resource Inquiry Idを取得します。
	 * @return Resource Inquiry Id
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * Resource Inquiry Idを設定します。
	 * @param areqId Resource Inquiry Id
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * Resource Category Codeを取得します。
	 * @return Resource Category Code
	 */
	public String getAreqCtgCode() {
	    return areqCtgCode;
	}
	/**
	 * Resource Category Codeを設定します。
	 * @param areqCtgCode Resource Category Code
	 */
	public void setAreqCtgCode(String areqCtgCode) {
	    this.areqCtgCode = areqCtgCode;
	}
	/**
	 * Group Idを取得します。
	 * @return Group Id
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * Group Idを設定します。
	 * @param grpId Group Id
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * Group Nameを取得します。
	 * @return Group Name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * Group Nameを設定します。
	 * @param grpNm Group Name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}
	/**
	 * Resource Inquiry Subjectを取得します。
	 * @return Resource Inquiry Subject
	 */
	public String getAssetReqNm() {
	    return assetReqNm;
	}
	/**
	 * Resource Inquiry Subjectを設定します。
	 * @param assetReqNm Resource Inquiry Subject
	 */
	public void setAssetReqNm(String assetReqNm) {
	    this.assetReqNm = assetReqNm;
	}
	/**
	 * Contentを取得します。
	 * @return Content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * Contentを設定します。
	 * @param content Content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * Checkout Inquiry User Idを取得します。
	 * @return Checkout Inquiry User Id
	 */
	public String getLendReqUserId() {
	    return lendReqUserId;
	}
	/**
	 * Checkout Inquiry User Idを設定します。
	 * @param lendReqUserId Checkout Inquiry User Id
	 */
	public void setLendReqUserId(String lendReqUserId) {
	    this.lendReqUserId = lendReqUserId;
	}
	/**
	 * * Checkout Inquiry User Nameを取得します。
	 * @return * Checkout Inquiry User Name
	 */
	public String getLendReqUserNm() {
	    return lendReqUserNm;
	}
	/**
	 * * Checkout Inquiry User Nameを設定します。
	 * @param lendReqUserNm * Checkout Inquiry User Name
	 */
	public void setLendReqUserNm(String lendReqUserNm) {
	    this.lendReqUserNm = lendReqUserNm;
	}
	/**
	 * Checkout Inquiry Date Timeを取得します。
	 * @return Checkout Inquiry Date Time
	 */
	public String getLendReqDateTime() {
	    return lendReqDateTime;
	}
	/**
	 * Checkout Inquiry Date Timeを設定します。
	 * @param lendReqDateTime Checkout Inquiry Date Time
	 */
	public void setLendReqDateTime(String lendReqDateTime) {
	    this.lendReqDateTime = lendReqDateTime;
	}
	/**
	 * Check-in Inquiry User Idを取得します。
	 * @return Check-in Inquiry User Id
	 */
	public String getRtnReqUserId() {
	    return rtnReqUserId;
	}
	/**
	 * Check-in Inquiry User Idを設定します。
	 * @param rtnReqUserId Check-in Inquiry User Id
	 */
	public void setRtnReqUserId(String rtnReqUserId) {
	    this.rtnReqUserId = rtnReqUserId;
	}
	/**
	 * * Check-in Inquiry User Nameを取得します。
	 * @return * Check-in Inquiry User Name
	 */
	public String getRtnReqUserNm() {
	    return rtnReqUserNm;
	}
	/**
	 * * Check-in Inquiry User Nameを設定します。
	 * @param rtnReqUserNm * Check-in Inquiry User Name
	 */
	public void setRtnReqUserNm(String rtnReqUserNm) {
	    this.rtnReqUserNm = rtnReqUserNm;
	}
	/**
	 * * Check-in Inquiry Date Timeを取得します。
	 * @return * Check-in Inquiry Date Time
	 */
	public String getRtnReqDateTime() {
	    return rtnReqDateTime;
	}
	/**
	 * * Check-in Inquiry Date Timeを設定します。
	 * @param rtnReqDateTime * Check-in Inquiry Date Time
	 */
	public void setRtnReqDateTime(String rtnReqDateTime) {
	    this.rtnReqDateTime = rtnReqDateTime;
	}
	/**
	 * Removal Inquiry User Idを取得します。
	 * @return Removal Inquiry User Id
	 */
	public String getDelReqUserId() {
	    return delReqUserId;
	}
	/**
	 * Removal Inquiry User Idを設定します。
	 * @param delReqUserId Removal Inquiry User Id
	 */
	public void setDelReqUserId(String delReqUserId) {
	    this.delReqUserId = delReqUserId;
	}
	/**
	 * * Removal Inquiry User Nameを取得します。
	 * @return * Removal Inquiry User Name
	 */
	public String getDelReqUserNm() {
	    return delReqUserNm;
	}
	/**
	 * * Removal Inquiry User Nameを設定します。
	 * @param delReqUserNm * Removal Inquiry User Name
	 */
	public void setDelReqUserNm(String delReqUserNm) {
	    this.delReqUserNm = delReqUserNm;
	}
	/**
	 * * Removal Inquiry Date Timeを取得します。
	 * @return * Removal Inquiry Date Time
	 */
	public String getDelReqDateTime() {
	    return delReqDateTime;
	}
	/**
	 * * Removal Inquiry Date Timeを設定します。
	 * @param delReqDateTime * Removal Inquiry Date Time
	 */
	public void setDelReqDateTime(String delReqDateTime) {
	    this.delReqDateTime = delReqDateTime;
	}
	/**
	 * * Checkout Resource Revisionを取得します。
	 * @return * Checkout Resource Revision
	 */
	public String getLendFileRevision() {
	    return lendFileRevision;
	}
	/**
	 * * Checkout Resource Revisionを設定します。
	 * @param lendFileRevision * Checkout Resource Revision
	 */
	public void setLendFileRevision(String lendFileRevision) {
	    this.lendFileRevision = lendFileRevision;
	}
	/**
	 * Checkout Resource Byte Sizeを取得します。
	 * @return Checkout Resource Byte Size
	 */
	public String getLendFileByteSize() {
	    return lendFileByteSize;
	}
	/**
	 * Checkout Resource Byte Sizeを設定します。
	 * @param lendFileByteSize Checkout Resource Byte Size
	 */
	public void setLendFileByteSize(String lendFileByteSize) {
	    this.lendFileByteSize = lendFileByteSize;
	}
	/**
	 * Checkout Resource Date Timeを取得します。
	 * @return Checkout Resource Date Time
	 */
	public String getLendFileUpdateDateTime() {
	    return lendFileUpdateDateTime;
	}
	/**
	 * Checkout Resource Date Timeを設定します。
	 * @param lendFileUpdateDateTime Checkout Resource Date Time
	 */
	public void setLendFileUpdateDateTime(String lendFileUpdateDateTime) {
	    this.lendFileUpdateDateTime = lendFileUpdateDateTime;
	}
	/**
	 * Check-in Resource Revisionを取得します。
	 * @return Check-in Resource Revision
	 */
	public String getRtnFileRevision() {
	    return rtnFileRevision;
	}
	/**
	 * Check-in Resource Revisionを設定します。
	 * @param rtnFileRevision Check-in Resource Revision
	 */
	public void setRtnFileRevision(String rtnFileRevision) {
	    this.rtnFileRevision = rtnFileRevision;
	}
	/**
	 * Check-in Resource Byte Sizeを取得します。
	 * @return Check-in Resource Byte Size
	 */
	public String getRtnFileByteSize() {
	    return rtnFileByteSize;
	}
	/**
	 * Check-in Resource Byte Sizeを設定します。
	 * @param rtnFileByteSize Check-in Resource Byte Size
	 */
	public void setRtnFileByteSize(String rtnFileByteSize) {
	    this.rtnFileByteSize = rtnFileByteSize;
	}
	/**
	 * Check-in Resource Date Timeを取得します。
	 * @return Check-in Resource Date Time
	 */
	public String getRtnFileUpdateDateTime() {
	    return rtnFileUpdateDateTime;
	}
	/**
	 * Check-in Resource Date Timeを設定します。
	 * @param rtnFileUpdateDateTime Check-in Resource Date Time
	 */
	public void setRtnFileUpdateDateTime(String rtnFileUpdateDateTime) {
	    this.rtnFileUpdateDateTime = rtnFileUpdateDateTime;
	}
	/**
	 * Removal Resource Revisionを取得します。
	 * @return Removal Resource Revision
	 */
	public String getDelFileRevision() {
	    return delFileRevision;
	}
	/**
	 * Removal Resource Revisionを設定します。
	 * @param delFileRevision Removal Resource Revision
	 */
	public void setDelFileRevision(String delFileRevision) {
	    this.delFileRevision = delFileRevision;
	}
	/**
	 * Removal Resource Byte Sizeを取得します。
	 * @return Removal Resource Byte Size
	 */
	public String getDelFileByteSize() {
	    return delFileByteSize;
	}
	/**
	 * Removal Resource Byte Sizeを設定します。
	 * @param delFileByteSize Removal Resource Byte Size
	 */
	public void setDelFileByteSize(String delFileByteSize) {
	    this.delFileByteSize = delFileByteSize;
	}
	/**
	 * Removal Resource Date Timeを取得します。
	 * @return Removal Resource Date Time
	 */
	public String getDelFileUpdateDateTime() {
	    return delFileUpdateDateTime;
	}
	/**
	 * Removal Resource Date Timeを設定します。
	 * @param delFileUpdateDateTime Removal Resource Date Time
	 */
	public void setDelFileUpdateDateTime(String delFileUpdateDateTime) {
	    this.delFileUpdateDateTime = delFileUpdateDateTime;
	}
	/**
	 * Change Property Approval Date Timeを取得します。
	 * @return Change Property Approval Date Time
	 */
	public String getChgAppDateTime() {
	    return chgAppDateTime;
	}
	/**
	 * Change Property Approval Date Timeを設定します。
	 * @param chgAppDateTime Change Property Approval Date Time
	 */
	public void setChgAppDateTime(String chgAppDateTime) {
	    this.chgAppDateTime = chgAppDateTime;
	}
	/**
	 * Status-IDを取得します。
	 * @return Status-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * Status-IDを設定します。
	 * @param stsId Status-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * Process Status-IDを取得します。
	 * @return Process Status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * Process Status-IDを設定します。
	 * @param procStsId Process Status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * Resource Lockを取得します。
	 * @return Resource Lock
	 */
	public String getResourceLock() {
	    return resourceLock;
	}
	/**
	 * Resource Lockを設定します。
	 * @param resourceLock Resource Lock
	 */
	public void setResourceLock(String resourceLock) {
	    this.resourceLock = resourceLock;
	}


}
