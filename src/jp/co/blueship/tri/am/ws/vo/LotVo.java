package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IValueObject;

/**
 * ロット情報のValueObjectクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class LotVo implements IValueObject {

	/**
	 * lot-ID
	 */
	@QueryParam("lotId")
	public String lotId;
	/**
	 * lot name
	 */
	@QueryParam("lotNm")
	public String lotNm;
	/**
	 * summary
	 */
	@QueryParam("summary")
	public String summary;
	/**
	 * content
	 */
	@QueryParam("content")
	public String content;
	/**
	 * vcs catalog code
	 */
	@QueryParam("vcsCtgCd")
	public String vcsCtgCd;
	/**
	 * in out box
	 */
	@QueryParam("inOutBox")
	public String inOutBox;
	/**
	 * system in box
	 */
	@QueryParam("sysInBox")
	public String sysInBox;
	/**
	 * lot mw path
	 */
	@QueryParam("lotMwPath")
	public String lotMwPath;
	/**
	 * history path
	 */
	@QueryParam("histPath")
	public String histPath;
	/**
	 * work space path
	 */
	@QueryParam("wsPath")
	public String wsPath;
	/**
	 * build env-ID
	 */
	@QueryParam("bldEnvId")
	public String bldEnvId;
	/**
	 * full build env-ID
	 */
	@QueryParam("fullBldEnvId")
	public String fullBldEnvId;
	/**
	 * is asset suplication
	 */
	@QueryParam("isAssetDup")
	public int isAssetDup;
	/**
	 * lot branch tag
	 */
	@QueryParam("lotBranchTag")
	public String lotBranchTag;
	/**
	 * lot latest version tag
	 */
	@QueryParam("lotLatestVerTag")
	public String lotLatestVerTag;
	/**
	 * lot latest baseline tag
	 */
	@QueryParam("lotLatestBlTag")
	public String lotLatestBlTag;
	/**
	 * lot resistration head revision
	 */
	@QueryParam("lotRegHeadRev")
	public String lotRegHeadRev;
	/**
	 * lot resistration revision
	 */
	@QueryParam("lotRegRev")
	public String lotRegRev;
	/**
	 * lot close revision
	 */
	@QueryParam("lotCloseRev")
	public String lotCloseRev;
	/**
	 * lot latest merge revision
	 */
	@QueryParam("lotLatestMergeRev")
	public String lotLatestMergeRev;
	/**
	 * allow list view
	 */
	@QueryParam("allowListView")
	public int allowListView;
	/**
	 * AM product activate
	 */
	@QueryParam("amProductActivate")
	public int amProductActivate;
	/**
	 * RM product activate
	 */
	@QueryParam("rmProductActivate")
	public int rmProductActivate;
	/**
	 * sts-ID
	 */
	@QueryParam("stsId")
	public String stsId;
	/**
	 * close user-ID
	 */
	@QueryParam("closeUserId")
	public String closeUserId;
	/**
	 * close user name
	 */
	@QueryParam("closeUserNm")
	public String closeUserNm;
	/**
	 * close time stamp
	 */
	@QueryParam("closeTimestamp")
	public String closeTimestamp;
	/**
	 * close comment
	 */
	@QueryParam("closeCmt")
	public String closeCmt;
	/**
	 * delete user-ID
	 */
	@QueryParam("delUserId")
	public String delUserId;
	/**
	 * delete user name
	 */
	@QueryParam("delUserNm")
	public String delUserNm;
	/**
	 * delete time stamp
	 */
	@QueryParam("delTimestamp")
	public String delTimestamp;
	/**
	 * delete comment
	 */
	@QueryParam("delCmt")
	public String delCmt;

	public String getLotId() {
		return lotId;
	}

	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	public String getLotNm() {
		return lotNm;
	}

	public void setLotNm(String lotNm) {
		this.lotNm = lotNm;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getVcsCtgCd() {
		return vcsCtgCd;
	}

	public void setVcsCtgCd(String vcsCtgCd) {
		this.vcsCtgCd = vcsCtgCd;
	}

	public String getInOutBox() {
		return inOutBox;
	}

	public void setInOutBox(String inOutBox) {
		this.inOutBox = inOutBox;
	}

	public String getSysInBox() {
		return sysInBox;
	}

	public void setSysInBox(String sysInBox) {
		this.sysInBox = sysInBox;
	}

	public String getLotMwPath() {
		return lotMwPath;
	}

	public void setLotMwPath(String lotMwPath) {
		this.lotMwPath = lotMwPath;
	}

	public String getHistPath() {
		return histPath;
	}

	public void setHistPath(String histPath) {
		this.histPath = histPath;
	}

	public String getWsPath() {
		return wsPath;
	}

	public void setWsPath(String wsPath) {
		this.wsPath = wsPath;
	}

	public String getBldEnvId() {
		return bldEnvId;
	}

	public void setBldEnvId(String bldEnvId) {
		this.bldEnvId = bldEnvId;
	}

	public String getFullBldEnvId() {
		return fullBldEnvId;
	}

	public void setFullBldEnvId(String fullBldEnvId) {
		this.fullBldEnvId = fullBldEnvId;
	}

	public int getIsAssetDup() {
		return isAssetDup;
	}

	public void setIsAssetDup(int isAssetDup) {
		this.isAssetDup = isAssetDup;
	}

	public String getLotBranchTag() {
		return lotBranchTag;
	}

	public void setLotBranchTag(String lotBranchTag) {
		this.lotBranchTag = lotBranchTag;
	}

	public String getLotLatestVerTag() {
		return lotLatestVerTag;
	}

	public void setLotLatestVerTag(String lotLatestVerTag) {
		this.lotLatestVerTag = lotLatestVerTag;
	}

	public String getLotLatestBlTag() {
		return lotLatestBlTag;
	}

	public void setLotLatestBlTag(String lotLatestBlTag) {
		this.lotLatestBlTag = lotLatestBlTag;
	}

	public int getAllowListView() {
		return allowListView;
	}

	public void setAllowListView(int allowListView) {
		this.allowListView = allowListView;
	}

	public int getAmProductActivate() {
		return amProductActivate;
	}

	public void setRmProductActivate(int rmProductActivate) {
		this.rmProductActivate = rmProductActivate;
	}

	public String getStsId() {
		return stsId;
	}

	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	public String getCloseUserId() {
		return closeUserId;
	}

	public void setCloseUserId(String closeUserId) {
		this.closeUserId = closeUserId;
	}

	public String getCloseUserNm() {
		return closeUserNm;
	}

	public void setCloseUserNm(String closeUserNm) {
		this.closeUserNm = closeUserNm;
	}

	public String getCloseTimestamp() {
		return closeTimestamp;
	}

	public void setCloseTimestamp(String closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	public String getCloseCmt() {
		return closeCmt;
	}

	public void setCloseCmt(String closeCmt) {
		this.closeCmt = closeCmt;
	}

	public String getDelUserId() {
		return delUserId;
	}

	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	public String getDelUserNm() {
		return delUserNm;
	}

	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}

	public String getDelTimestamp() {
		return delTimestamp;
	}

	public void setDelTimestamp(String delTimestamp) {
		this.delTimestamp = delTimestamp;
	}

	public String getDelCmt() {
		return delCmt;
	}

	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}

}
