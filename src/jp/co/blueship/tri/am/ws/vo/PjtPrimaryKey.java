package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IPrimaryKey;

/**
 * 変更情報の主キーを表現するクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class PjtPrimaryKey implements IPrimaryKey {

	@QueryParam("pjtId")
	public String pjtId;

	/**
	 * 変更情報IDを返します。
	 *
	 * @return 変更情報ID
	 */
	public String getPjtId() {
		return pjtId;
	}

	/**
	 * 変更情報IDを設定します。
	 *
	 * @param pjtId 変更情報ID
	 */
	public void setPjtId(String pjtId) {
		this.pjtId = pjtId;
	}

}
