package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IPrimaryKey;
import jp.co.blueship.tri.fw.ws.ISearchConditions;

/**
 * 資産台帳の検索クエリパラメタを格納するためのクラスです。
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */
public class AssetRegisterWebCondition implements ISearchConditions, IPrimaryKey {

	/**
	 * lot-ID
	 */
	@QueryParam("lotId")
	public String lotId;
	/**
	 * lot name
	 */
	@QueryParam("lotNm")
	public String lotNm;
	/**
	 * module name
	 */
	@QueryParam("mdlNm")
	public String mdlNm;
	/**
	/**
	 * file path
	 */
	@QueryParam("filePath")
	public String filePath;
	/**
	 * Information Change-ID
	 */
	@QueryParam("pjtId")
	public String pjtId;
	/**
	 * Change Factor Number
	 */
	@QueryParam("chgFactorNo")
	public String chgFactorNo;
	/**
	 * Request User-ID
	 */
	@QueryParam("reqUserId")
	public String reqUserId;
	/**
	 * Request Group Name
	 */
	@QueryParam("reqGrpNm")
	public String reqGrpNm;
	/**
	 * status-ID
	 */
	@QueryParam("stsId")
	public String stsId;
	/**
	 * Process Status-ID
	 */
	@QueryParam("procStsId")
	public String procStsId;
	/**
	 * Target MW
	 */
	@QueryParam("targetMW")
	public String targetMW;

	public String getLotId() {
		return lotId;
	}

	public String getLotNm() {
		return lotNm;
	}

	public String getMdlNm() {
		return mdlNm;
	}

	public String getFilePath() {
		return filePath;
	}

	public String getPjtId() {
		return pjtId;
	}

	public String getChgFactorNo() {
		return chgFactorNo;
	}

	public String getReqUserId() {
		return reqUserId;
	}

	public String getReqGrpNm() {
		return reqGrpNm;
	}

	public String getStsId() {
		return stsId;
	}

	public String getProcStsId() {
		return procStsId;
	}

	public String getTargetMW() {
		return targetMW;
	}

	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	public void setLotNm(String lotNm) {
		this.lotNm = lotNm;
	}

	public void setMdlNm(String mdlNm) {
		this.mdlNm = mdlNm;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public void setPjtId(String pjtId) {
		this.pjtId = pjtId;
	}

	public void setChgFactorNo(String chgFactorNo) {
		this.chgFactorNo = chgFactorNo;
	}

	public void setReqUserId(String reqUserId) {
		this.reqUserId = reqUserId;
	}

	public void setReqGrpNm(String grpNm) {
		this.reqGrpNm = grpNm;
	}

	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	public void setProcStsId(String procStsId) {
		this.procStsId = procStsId;
	}

	public void setTargetMW(String targetMW) {
		this.targetMW = targetMW;
	}

}

