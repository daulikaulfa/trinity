package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IValueObject;

public class PjtVo implements IValueObject {

	/**
	 * pjt-ID
	 */
	@QueryParam("pjtId")
	public String pjtId;
	/**
	 * lot-IDe
	 */
	@QueryParam("lotId")
	public String lotId;
	/**
	 * pjt name
	 */
	@QueryParam("pjtNm")
	public String pjtNm;
	/**
	 * summary
	 */
	@QueryParam("summary")
	public String summary;
	/**
	 * content
	 */
	@QueryParam("content")
	public String content;
	/**
	 * change factor no
	 */
	@QueryParam("chgFactorNo")
	public String chgFactorNo;
	/**
	 * change factor id
	 */
	@QueryParam("chgFactorId")
	public String chgFactorId;
	/**
	 * sts-ID
	 */
	@QueryParam("stsId")
	public String stsId;
	/**
	 * close user-ID
	 */
	@QueryParam("closeUserId")
	public String closeUserId;
	/**
	 * close user name
	 */
	@QueryParam("closeUserNm")
	public String closeUserNm;
	/**
	 * close time stamp
	 */
	@QueryParam("closeTimestamp")
	public String closeTimestamp;
	/**
	 * close comment
	 */
	@QueryParam("closeCmt")
	public String closeCmt;
	/**
	 * test completed user-ID
	 */
	@QueryParam("testCompUserId")
	public String testCompUserId;
	/**
	 * test completed user name
	 */
	@QueryParam("testCompUserNm")
	public String testCompUserNm;
	/**
	 * test completed time stamp
	 */
	@QueryParam("testCompTimestamp")
	public String testCompTimestamp;
	/**
	 * test completed comment
	 */
	@QueryParam("testCompCmt")
	public String testCompCmt;
	/**
	 * delete user-ID
	 */
	@QueryParam("delUserId")
	public String delUserId;
	/**
	 * delete user name
	 */
	@QueryParam("delUserNm")
	public String delUserNm;
	/**
	 * delete time stamp
	 */
	@QueryParam("delTimestamp")
	public String delTimestamp;
	/**
	 * delete comment
	 */
	@QueryParam("delCmt")
	public String delCmt;
	/**
	 * delete comment
	 */
	@QueryParam("delStsId")
	public int delStsId;

	public String getPjtId() {
		return pjtId;
	}

	public String getLotId() {
		return lotId;
	}

	public String getPjtNm() {
		return pjtNm;
	}

	public String getSummary() {
		return summary;
	}

	public String getContent() {
		return content;
	}

	public String getChgFactorNo() {
		return chgFactorNo;
	}

	public String getChgFactorId() {
		return chgFactorId;
	}

	public String getStsId() {
		return stsId;
	}

	public String getCloseUserId() {
		return closeUserId;
	}

	public String getCloseUserNm() {
		return closeUserNm;
	}

	public String getCloseTimestamp() {
		return closeTimestamp;
	}

	public String getCloseCmt() {
		return closeCmt;
	}

	public String getTestCompUserId() {
		return testCompUserId;
	}

	public String getTestCompUserNm() {
		return testCompUserNm;
	}

	public String getTestCompTimestamp() {
		return testCompTimestamp;
	}

	public String getTestCompCmt() {
		return testCompCmt;
	}

	public String getDelUserId() {
		return delUserId;
	}

	public String getDelUserNm() {
		return delUserNm;
	}

	public String getDelTimestamp() {
		return delTimestamp;
	}

	public String getDelCmt() {
		return delCmt;
	}

	public int getDelStsId() {
		return delStsId;
	}

	public void setPjtId(String pjtId) {
		this.pjtId = pjtId;
	}

	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	public void setPjtNm(String pjtNm) {
		this.pjtNm = pjtNm;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public void setChgFactorNo(String chgFactorNo) {
		this.chgFactorNo = chgFactorNo;
	}

	public void setChgFactorId(String chgFactorId) {
		this.chgFactorId = chgFactorId;
	}

	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	public void setCloseUserId(String closeUserId) {
		this.closeUserId = closeUserId;
	}

	public void setCloseUserNm(String closeUserNm) {
		this.closeUserNm = closeUserNm;
	}

	public void setCloseTimestamp(String closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	public void setCloseCmt(String closeCmt) {
		this.closeCmt = closeCmt;
	}

	public void setTestCompUserId(String testCompUserId) {
		this.testCompUserId = testCompUserId;
	}

	public void setTestCompUserNm(String testCompUserNm) {
		this.testCompUserNm = testCompUserNm;
	}

	public void setTestCompTimestamp(String testCompTimestamp) {
		this.testCompTimestamp = testCompTimestamp;
	}

	public void setTestCompCmt(String testCompCmt) {
		this.testCompCmt = testCompCmt;
	}

	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}

	public void setDelTimestamp(String delTimestamp) {
		this.delTimestamp = delTimestamp;
	}

	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}

	public void setDelStsId(int delStsId) {
		this.delStsId = delStsId;
	}

}
