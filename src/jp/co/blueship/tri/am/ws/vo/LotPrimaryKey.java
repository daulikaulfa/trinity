package jp.co.blueship.tri.am.ws.vo;

import javax.ws.rs.QueryParam;

import jp.co.blueship.tri.fw.ws.IPrimaryKey;

/**
 * ロット情報の主キーを表現するクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class LotPrimaryKey implements IPrimaryKey {

	@QueryParam("lotId")
	public String lotId;

	/**
	 * ロットIDを返します。
	 *
	 * @return ロットID
	 */
	public String getLotId() {
		return lotId;
	}

	/**
	 * ロットIDを設定します。
	 *
	 * @param lotId ロットID
	 */
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

}
