package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.am.ws.AmWSFunctions.*;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.sun.jersey.api.core.InjectParam;

import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterCondition;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibAssetRegisterServiceBean;
import jp.co.blueship.tri.am.ws.vo.AssetRegisterVo;
import jp.co.blueship.tri.am.ws.vo.AssetRegisterWebCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ws.TriWebService;

/**
 * 資産台帳のためのWebサービスインタフェースです。
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */
@Path("AssetRegister")
public class AssetRegisterWebService extends TriWebService<AssetRegisterVo, AssetRegisterWebCondition, AssetRegisterWebCondition> {
	private static final ILog log = TriLogFactory.getInstance();

	public static final String FLOW_ACTION_ID = "FlowChaLibAssetRegisterService";

	@GET
	@Path("/get")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response get(@InjectParam AssetRegisterWebCondition primaryKey) {

		// not support function
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@GET
	@Path("/list")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response list() {

		//not support function
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@GET
	@Path("/find")
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response find(@InjectParam AssetRegisterWebCondition condition) {

		try {
			IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
			FlowChaLibAssetRegisterServiceBean serviceBean = new FlowChaLibAssetRegisterServiceBean();

			serviceBean.setAssetRegisterCondition( convertToAssetRegisterCondition( condition ) );
			serviceDto.setServiceBean( serviceBean );

			IService service = ((IService)this.context().getBean("generalWebService"));
			service.init();

			((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto);

			FlowChaLibAssetRegisterServiceBean responseBean = (FlowChaLibAssetRegisterServiceBean)serviceDto.getServiceBean();

			List<AssetRegisterVo> results =
					FluentList.from(responseBean.getAssetRegisterViewList()).map(TO_ASSET_REGISTER_VO).asList();

			if ( 0 == results.size() ) {
				String message = this.message( AmMessageId.AM003000I, new String[0] );
				LogHandler.info( log, message );

				return Response.status(Response.Status.NO_CONTENT).//
						//entity(message).//
						build();
			}

			return responseAsList( results );

		} catch (Exception e) {
			LogHandler.fatal( log , e ) ;

			try {
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( AmMessageId.AM005158S , e , FLOW_ACTION_ID );
			} catch (Exception innerEx) {
				return handleException(innerEx);
			}

		}
	}

	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response insert(@InjectParam AssetRegisterVo vo) {
		// not support function
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@PUT
	@Path("/update")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response update(@InjectParam AssetRegisterVo vo) {
		// not support function
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	@DELETE
	@Path("/delete")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Override
	public Response delete(@InjectParam AssetRegisterWebCondition primaryKey) {
		// not support function
		return Response.status(Status.INTERNAL_SERVER_ERROR).build();
	}

	/**
	 * Convert condition
	 * @param entity Conversion source of condition
	 * @return Conversion destination of condition
	 */
	private AssetRegisterCondition convertToAssetRegisterCondition( AssetRegisterWebCondition webCondition ) {
		final String separatorChars = ",";

		AssetRegisterCondition bean = new AssetRegisterCondition();

		bean.setLotId		( TriStringUtils.split( webCondition.getLotId(), separatorChars, false, true) );
		bean.setLotNm		( TriStringUtils.split( webCondition.getLotNm(), separatorChars, false, true) );
		bean.setMdlNm		( TriStringUtils.split( webCondition.getMdlNm(), separatorChars, false, true) );
		bean.setFilePath	( TriStringUtils.split( webCondition.getFilePath(), separatorChars, false, true) );
		bean.setPjtId		( TriStringUtils.split( webCondition.getPjtId(), separatorChars, false, true) );
		bean.setChgFactorNo	( TriStringUtils.split( webCondition.getChgFactorNo(), separatorChars, false, true) );
		bean.setReqUserId	( TriStringUtils.split( webCondition.getReqUserId(), separatorChars, false, true) );
		bean.setGrpNm		( TriStringUtils.split( webCondition.getReqGrpNm(), separatorChars, false, true) );
		bean.setStsId		( TriStringUtils.split( webCondition.getStsId(), separatorChars, false, true) );
		bean.setProcStsId	( TriStringUtils.split( webCondition.getProcStsId(), separatorChars, false, true) );
		bean.setTargetMW	( "1".equals(webCondition.getTargetMW())? true: false );

		return bean;
	}

}
