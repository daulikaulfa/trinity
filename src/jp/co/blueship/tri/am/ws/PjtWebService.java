package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.am.ws.AmWSFunctions.*;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.ws.vo.PjtPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.PjtSearchConditions;
import jp.co.blueship.tri.am.ws.vo.PjtVo;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ws.TriWebService;

//import com.sun.jersey.api.core.InjectParam;

/**
 * 変更情報アクセスのためのWebサービスインタフェースです。
 *
 * @author Takayuki Kubo
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
//@Path("pjt")
public class PjtWebService extends TriWebService<PjtVo, PjtSearchConditions, PjtPrimaryKey> {

	private IPjtDao pjtDao;

	public void setPjtDao(IPjtDao pjtDao) {
		this.pjtDao = pjtDao;
	}

	//@GET
	//@Path("/get")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response get(/*@InjectParam*/ PjtPrimaryKey primaryKey) {

		PjtSqlConditionBuilder builder = new PjtSqlConditionBuilder(primaryKey);
		IPjtEntity result = pjtDao.findByPrimaryKey(builder.buildByPrimaryKey().getCondition());

		return responseAsSingleVo(TO_PJT_VO.apply(result));
	}

	//@GET
	//@Path("/list")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response list() {

		List<IPjtEntity> result = pjtDao.find(new PjtCondition().getCondition());

		return responseAsList(FluentList.from(result).map(TO_PJT_VO).asList());
	}

	//@GET
	//@Path("/find")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response find(/*@InjectParam*/ PjtSearchConditions conditions) {

		PjtSqlConditionBuilder builder = new PjtSqlConditionBuilder(conditions);
		List<IPjtEntity> result = pjtDao.find(builder.build().getCondition());

		return responseAsList(FluentList.from(result).map(TO_PJT_VO).asList());
	}

	//@POST
	//@Path("/insert")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response insert(/*@InjectParam*/ PjtVo vo) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

	//@PUT
	//@Path("/update")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response update(/*@InjectParam*/ PjtVo vo) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

	//@DELETE
	//@Path("/delete")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response delete(/*@InjectParam*/ PjtPrimaryKey primaryKey) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

}
