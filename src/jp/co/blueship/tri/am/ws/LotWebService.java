package jp.co.blueship.tri.am.ws;

import static jp.co.blueship.tri.am.ws.AmWSFunctions.*;

import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import jp.co.blueship.tri.am.dao.lot.ILotDao;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.ws.vo.LotPrimaryKey;
import jp.co.blueship.tri.am.ws.vo.LotSearchConditions;
import jp.co.blueship.tri.am.ws.vo.LotVo;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ws.TriWebService;

//import com.sun.jersey.api.core.InjectParam;

/**
 * ロット情報アクセスのためのWebサービスインタフェースです。
 *
 * @author Takayuki Kubo
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
//@Path("lot")
public class LotWebService extends TriWebService<LotVo, LotSearchConditions, LotPrimaryKey> {

	private ILotDao lotDao;

	public void setLotDao(ILotDao lotDao) {
		this.lotDao = lotDao;
	}

	//@GET
	//@Path("/get")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response get(/*@InjectParam*/ LotPrimaryKey primaryKey) {

		LotSqlConditionBuilder builder = new LotSqlConditionBuilder(primaryKey);
		try {
			ILotEntity result = lotDao.findByPrimaryKey(builder.buildByPrimaryKey().getCondition());

			if (result == null) {
				LotVo vo = new LotVo();
				return responseAsSingleVo( vo );
			}

			return responseAsSingleVo(TO_LOT_VO.apply(result));
		} catch (Exception e) {
			return handleException(e);
		}
	}

	//@GET
	//@Path("/list")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response list() {

		List<ILotEntity> result = lotDao.find(new LotCondition().getCondition());

		return responseAsList(FluentList.from(result).map(TO_LOT_VO).asList());
	}

	//@GET
	//@Path("/find")
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response find(/*@InjectParam*/ LotSearchConditions conditions) {

		LotSqlConditionBuilder builder = new LotSqlConditionBuilder(conditions);
		List<ILotEntity> result = lotDao.find(builder.build().getCondition());

		return responseAsList(FluentList.from(result).map(TO_LOT_VO).asList());
	}

	//@POST
	//@Path("/insert")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response insert(/*@InjectParam*/ LotVo vo) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

	//@PUT
	//@Path("/update")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response update(/*@InjectParam*/ LotVo vo) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

	//@DELETE
	//@Path("/delete")
	//@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	//@Override
	public Response delete(/*@InjectParam*/ LotPrimaryKey primaryKey) {
		// 現時点では未サポート
		return Response.status(Status.BAD_REQUEST).build();
	}

}
