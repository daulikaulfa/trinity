package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理・ロット作成画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotEntryService implements IDomain<FlowChaLibLotEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotEntryServiceBean> execute( IServiceDto<FlowChaLibLotEntryServiceBean> serviceDto ) {

		FlowChaLibLotEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward() ;
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LOT_ENTRY ) &&
					!forward.equals( ChaLibScreenID.LOT_ENTRY )) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean() ;

			paramBean.setSelectableAllowAssetDuplication( AmViewInfoAddonUtils.isSelectableAllowAssetDuplication() );
			paramBean.setSelectableAllowAssetDuplicationName( sheet.getValue( AmDesignBeanId.allowAssetDuplication , StatusFlg.on.value() ) );

			try {
				List<IMessageId> commentList		= new ArrayList<IMessageId>();
				List<String[]> commentArgsList	= new ArrayList<String[]>();

				commentList.add		( AmMessageId.AM001016E );
				commentArgsList.add	( new String[]{} );

				paramBean.setInfoCommentIdList		( commentList );
				paramBean.setInfoCommentArgsList	( commentArgsList );

				if( ChaLibScreenID.LOT_ENTRY.equals( referer ) ) {//ロット作成
					lotEditInputBean.setAllowAssetDuplicationName(
							sheet.getValue(
									AmDesignBeanId.allowAssetDuplication , (lotEditInputBean.isAllowAssetDuplication())? StatusFlg.on.value() : StatusFlg.off.value() ) );

					if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
						AmItemChkUtils.checkLotBasicInfo( lotEditInputBean , true ) ;
					}
				}

				if( ChaLibScreenID.LOT_ENTRY.equals( forward ) ) {//ロット作成
					if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
						lotEditInputBean.setAllowAssetDuplicationName(
								sheet.getValue(
										AmDesignBeanId.allowAssetDuplication , (lotEditInputBean.isAllowAssetDuplication())? StatusFlg.on.value() : StatusFlg.off.value() ) );

						lotEditInputBean.setGroupViewBeanList			( support.getGroupViewBean() );
						lotEditInputBean.setSelectedGroupViewBeanList	( support.getDefaultSelectedGroupViewBean( paramBean.getUserId() ));

						lotEditInputBean.setSpecifiedGroupList			( support.getGroupViewBean() );

						List<String> selectedGroupIdList = new ArrayList<String>() ;
						for ( GroupViewBean groupViewBean : lotEditInputBean.getSelectedGroupViewBeanList() ) {
							selectedGroupIdList.add( groupViewBean.getGroupId() );
						}
						lotEditInputBean.setSelectedGroupIdString		( selectedGroupIdList.toArray( new String[ 0 ] )) ;
					}
				}

			} finally {
				paramBean.setLotEditInputBean( lotEditInputBean ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005102S,e);
		}
	}


}
