package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * 変更管理・トップ画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibTopService implements IDomain<FlowChaLibTopServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	@SuppressWarnings("unused")
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibTopServiceBean> execute(IServiceDto<FlowChaLibTopServiceBean> serviceDto) {

		FlowChaLibTopServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			/*
			 * CHANGEC疎通チェックは不要 //アクション List<Object> paramList = new
			 * ArrayList<Object>(); paramList.add(
			 * RemoteServiceType.AGENT_CHANGEC ) ;
			 *
			 * try { for ( IActionPojo action : this.actions ) { action.execute(
			 * paramList ); } } finally { //agent疎通チェック結果によるメッセージ出力
			 * this.checkActiveAgents( paramList , paramBean ) ; }
			 */
			// いった全部取得してアクセス許可ロットのみ抽出
			LotCondition condition = AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();

			List<String> disableLinkLotNumbers = new ArrayList<String>();
			boolean isSearch = this.support.setAccessableLotNumbers(condition, paramBean, true, disableLinkLotNumbers);

			IEntityLimit<ILotEntity> entityLimit = null;
			int selectPageNo = 1;

			// アクセス可能なロットが１件もない場合は検索に行かない
			// （ロット番号が指定できないので、全部取ってきてしまうので）
			if (isSearch) {
				// 改めて検索
				ISqlSort sort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibTop();

				selectPageNo = (0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo();

				entityLimit = this.support.getLotDao().find(condition.getCondition(), sort, selectPageNo,
						sheet.intValue(AmDesignEntryKeyByChangec.maxPageNumberByTopMenu));

			} else {

				entityLimit = new EntityLimit<ILotEntity>();
				entityLimit.setEntities(FluentList.from(new ILotEntity[0]).asList());
			}

			if (TriStringUtils.isEmpty(entityLimit.getEntities()) && TriStringUtils.isEmpty(paramBean.getInfoMessageId())) {
				paramBean.setInfoMessage(AmMessageId.AM001122E);
			}

			List<ILotDto> lotDto = this.support.findLotDto(entityLimit.getEntities());

			List<LotViewBean> lotViewBeanList = this.support.getLotViewBean(lotDto, disableLinkLotNumbers);

			paramBean.setLotViewBeanList(lotViewBeanList);
			paramBean.setPageInfoView(AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), entityLimit.getLimit()));
			paramBean.setSelectPageNo(selectPageNo);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005106S, e);
		}
	}

	/**
	 * agentの疎通チェック結果を取得し、メッセージ作成を行う。
	 *
	 * @param paramList
	 * @param paramBean
	 */

	@SuppressWarnings("unused")
	private void checkActiveAgents(List<Object> paramList, FlowChaLibTopServiceBean paramBean) {

		AgentStatus[] agentStatusArray = AmExtractEntityAddonUtils.extractAgentStatusArray(paramList);
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		List<IMessageId> messageInfoList = new ArrayList<IMessageId>();
		List<String[]> messageInfoArgsList = new ArrayList<String[]>();

		if (null != agentStatusArray) {
			for (AgentStatus status : agentStatusArray) {
				IRmiSvcDto remoteService = status.getRemoteServiceEntity();
				// 画面にはエラーのもののみ出力
				if (true != status.isStatus()) {
					messageInfoList.add( AmMessageId.AM001124E );
					String[] messageArgs = new String[] { remoteService.getRmiSvcEntity().getBldSrvId(),
							remoteService.getRmiSvcEntity().getRmiSvcId(), remoteService.getRmiSvcEntity().getRmiHostNm(),
							String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()), String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()), };
					messageInfoArgsList.add(messageArgs);
				}
				// ログにはすべてのステータスを出力
				String statusStr = status.isStatus() ? ac.getMessage(TriLogMessage.LSM0014) : ac.getMessage(TriLogMessage.LSM0015);
				String[] messageArgs = new String[] { statusStr, remoteService.getRmiSvcEntity().getBldSrvId(),
						remoteService.getRmiSvcEntity().getRmiSvcId(), remoteService.getRmiSvcEntity().getRmiHostNm(),
						String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()), String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()), };
				LogHandler.info(log, ac.getMessage(AmMessageId.AM001125E, messageArgs));
			}

			if (0 != messageInfoList.size()) {
				paramBean.setInfoMessageIdList(messageInfoList);
				paramBean.setInfoMessageArgsList(messageInfoArgsList);
			}
		}
	}
}
