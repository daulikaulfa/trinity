package jp.co.blueship.tri.am.domain.lot.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * ロット情報を「ロット作成中」で新規登録します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionCreateLotInfoActive extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		this.initialize( serviceDto ) ;
		return serviceDto;
	}

	/**
	 * ロット情報を登録します
	 * @param paramList
	 * @throws BaseBusinessException
	 */
	private void initialize( IServiceDto<IGeneralServiceBean> serviceDto ) throws BaseBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		LotParamInfo paramInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfo ) ) {
			throw new TriSystemException( AmMessageId.AM005026S ) ;
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		/** ロット情報エンティティを作成する **/
		ILotDto lotDto = paramInfo.getLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		support.getLotDao().insert( lotEntity );
		support.getLotMdlLnkDao().insert( lotDto.getLotMdlLnkEntities() );
		support.getLotGrpLnkDao().insert( lotDto.getLotGrpLnkEntities() );
		support.getLotMailGrpLnkDao().insert( lotDto.getLotMailGrpLnkEntities() );
		support.getLotRelEnvLnkDao().insert( lotDto.getLotRelEnvLnkEntities() );

		this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_LOT, lotEntity.getLotId());
		this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_LOT, AmLotStatusIdForExecData.EditingLot, lotEntity.getLotId());

	}

}
