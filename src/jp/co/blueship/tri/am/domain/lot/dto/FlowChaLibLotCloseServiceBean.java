package jp.co.blueship.tri.am.domain.lot.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理・ロットクローズ用
 * 
 * @version V3L10.01
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibLotCloseServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 2L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット詳細表示情報 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** 一括変更クローズ用変更管理番号 **/
	private String[] selectedPjtIdArray;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** ロット編集入力情報 */
	private LotEditInputV3Bean lotEditInputBean = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public LotDetailViewBean getLotDetailViewBean() {
		if ( null == lotDetailViewBean ) {
			lotDetailViewBean = new LotDetailViewBean();
		}
		return lotDetailViewBean;
	}
	public String[] getSelectedPjtIdArray() {
		return selectedPjtIdArray;
	}
	public void setSelectedPjtIdArray(String[] selectedPjtIdArray) {
		this.selectedPjtIdArray = selectedPjtIdArray;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}
	
	public List<PjtViewBean> getPjtViewBeanList() {
		if ( null == pjtViewBeanList ) {
			pjtViewBeanList = new ArrayList<PjtViewBean>();
		}
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}
	
	public LotEditInputV3Bean getLotEditInputBean() {
		if ( null == lotEditInputBean ) {
			lotEditInputBean = new LotEditInputV3Bean();
		}
		return lotEditInputBean;
	}
	public void setLotEditInputBean( LotEditInputV3Bean lotEditInputBean ) {
		this.lotEditInputBean = lotEditInputBean;
	}
	
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
}

