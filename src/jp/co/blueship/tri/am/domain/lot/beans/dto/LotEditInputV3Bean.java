package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 変更管理・ロット操作用
 *
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class LotEditInputV3Bean implements	Serializable {

	private static final long serialVersionUID = 1L;

	/** 入力者名 */
	private String inputUserName = null ;
	/** 入力者ＩＤ */
	private String inputUserId = null ;

	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null ;
	/** ロット概要 */
	private String lotSummary = null ;
	/** ロット内容 */
	private String lotContent = null ;
	private String branchBaseLineTag = null ;
	/** ベースタインタグ */
	private String baseLineTag = null ;
	/** ロットステータスID */
	private String statusId = null;
	/** ロットステータス */
	private String status = null;
	/** ロット作成日時 */
	private Timestamp createdTime = null;
	/** 公開ディレクトリパス */
	private String directoryPathPublic = null ;
	/** 非公開ディレクトリパス */
	private String directoryPathPrivate = null ;
	/** 資産２重貸出 */
	private boolean allowAssetDuplication = false;
	/** 資産２重貸出の表示名 */
	private String allowAssetDuplicationName = null;

	/** マージを利用する/しない設定を行う(編集用) */
	private boolean editUseMerge = true;
	/** マージを利用する/しない設定を行う */
	private boolean useMerge = true;


	/** 選択可能なグループ */
	private List<GroupViewBean> groupViewBeanList = null ;
	/** 選択されたグループ */
	private List<GroupViewBean> selectedGroupViewBeanList = null ;
	/** 選択されたグループIDの配列を文字列に変換したもの 【チェックボックス表示用（checked）】*/
	private String selectedGroupIdString = null ;
	private String[] selectedGroupIds = new String[0];
	/** ロット変更前に選択されていた既存グループ */
	private List<GroupViewBean> preservedGroupViewBeanList = null ;

	/** 一覧表示許可 */
	private boolean allowListView = false;
	/** 一覧表示文言 */
	private String allowListContent;
	/** 編集時一覧表示許可変更可能フラグ */
	private boolean allowModifyAllowListView = false;

	/** 選択可能なサーバ */
	private List<ServerViewBean> serverViewBeanList = null ;
	/** 選択されたサーバ */
	private ServerViewBean selectedServerViewBean = null ;

	/** Ｓｃｍタイプ */
	private String scmType = null;


	/** 選択可能なメール送信先 特定グループ **/
	private List<GroupViewBean> specifiedGroupList = null ;
	/** 選択されたメール送信先 特定グループ */
	private List<GroupViewBean> selectedSpecifiedGroupList = null ;
	/** 選択されたメール送信先 特定グループIDの配列を文字列に変換したもの 【チェックボックス表示用（checked）】*/
	private String selectedSpecifiedGroupIdString = null ;
	private String[] selectedSpecifiedGroupIds = new String[0];
	/** ロット変更前に選択されていた既存メール送信先 特定グループ */
	private List<GroupViewBean> preservedSpecifiedGroupList = null ;


	/** 選択可能なモジュール */
	private List<ModuleViewBean> moduleViewBeanList = null ;
	/** 選択されたモジュール */
	private List<ModuleViewBean> selectedModuleViewBeanList = null ;
	/** 選択されたモジュール名の配列を文字列に変換したもの 【チェックボックス表示用（checked）】*/
	private String selectedModuleNameString = null ;
	private String[] selectedModuleNames = new String[0];

	/** ビルド環境ビュー 【すべてのステータスのビルド環境を表示】*/
	private List<BuildEnvViewBean> buildEnvViewBeanList = null ;
	/** ビルド環境選択可能リスト 【選択可能なステータスのビルド環境を表示】*/
	private List<BuildEnvViewBean> buildEnvViewBeanEnableList = null ;

	/** 選択された「ビルド環境」 */
	private String selectedBuildEnvNo = null ;
	private BuildEnvViewBean buildEnvBean = null ;
	/** 選択された「フルビルド環境」 */
	private String selectedFullBuildEnvNo = null ;
	private BuildEnvViewBean fullBuildEnvBean = null ;

	/** 選択可能なモジュール */
	private List<RelEnvViewBean> relEnvViewBeanList = null ;
	/** 選択されたモジュール */
	private List<RelEnvViewBean> selectedRelEnvViewBeanList = null ;
	/** 選択されたリリース環境番号の配列を文字列に変換したもの 【チェックボックス表示用（checked）】*/
	private String selectedRelEnvNoString = null ;
	private String[] selectedRelEnvNos = new String[0];

	/** 取消コメント */
	private String delComment = null;
	/** クローズコメント */
	private String closeComment = null;


	public String getInputUserName() {
		return inputUserName;
	}
	public void setInputUserName(String inputUserName) {
		this.inputUserName = inputUserName;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getDelComment() {
		return delComment;
	}
	public void setDelComment( String delComment ) {
		this.delComment = delComment;
	}

	public String getCloseComment() {
		return closeComment;
	}
	public void setCloseComment( String closeComment ) {
		this.closeComment = closeComment;
	}
	public String getDirectoryPathPrivate() {
		return directoryPathPrivate;
	}
	public void setDirectoryPathPrivate(String directoryPathPrivate) {
		this.directoryPathPrivate = directoryPathPrivate;
	}
	public String getDirectoryPathPublic() {
		return directoryPathPublic;
	}
	public void setDirectoryPathPublic(String directoryPathPublic) {
		this.directoryPathPublic = directoryPathPublic;
	}
	public String getLotContent() {
		return lotContent;
	}
	public void setLotContent(String lotContent) {
		this.lotContent = lotContent;
	}
	public String getBranchBaseLineTag() {
		return branchBaseLineTag;
	}
	public void setBranchBaseLineTag(String branchBaseLineTag) {
		this.branchBaseLineTag = branchBaseLineTag;
	}
	public String getBaseLineTag() {
		return baseLineTag;
	}
	public void setBaseLineTag(String baseLineTag) {
		this.baseLineTag = baseLineTag;
	}
	public void setStatusId(String statusId){
		this.statusId = statusId;
	}
	public String getStatusId(){
		return statusId;
	}
	public void setStatus(String status){
		this.status = status;
	}
	public String getStatus(){
		return status;
	}
	public void setCreatedTime(Timestamp createdTime){
		this.createdTime = createdTime;
	}
	public Timestamp getCreatedTime(){
		return createdTime;
	}
	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	public String getLotSummary() {
		return lotSummary;
	}
	public void setLotSummary(String lotSummary) {
		this.lotSummary = lotSummary;
	}
	public boolean isAllowAssetDuplication() {
		return allowAssetDuplication;
	}
	public void setAllowAssetDuplication( boolean value ) {
		this.allowAssetDuplication = value;
	}
	public String getAllowAssetDuplicationName() {
		return allowAssetDuplicationName;
	}
	public void setAllowAssetDuplicationName(String allowAssetDuplicationName) {
		this.allowAssetDuplicationName = allowAssetDuplicationName;
	}
	public boolean isEditUseMerge() {
		return editUseMerge;
	}
	public void setEditUseMerge( boolean value ) {
		this.editUseMerge = value;
	}
	public boolean isUseMerge() {
		return useMerge;
	}
	public void setUseMerge( boolean value ) {
		this.useMerge = value;
	}

	public List<GroupViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	public List<GroupViewBean> getSelectedGroupViewBeanList() {
		return selectedGroupViewBeanList;
	}
	public void setSelectedGroupViewBeanList(
			List<GroupViewBean> selectedGroupViewBeanList) {
		this.selectedGroupViewBeanList = selectedGroupViewBeanList;
	}
	public String getSelectedGroupIdString() {
		return selectedGroupIdString;
	}
	public String[] getSelectedGroupIds() {
		return selectedGroupIds;
	}
	public void setSelectedGroupIdString( String... selectedGroupIdString ) {
		selectedGroupIds = selectedGroupIdString;

		StringBuilder stb = new StringBuilder() ;
		stb.append( "[" ) ;
		stb.append( TriStringUtils.convertArrayToString( selectedGroupIdString ) ) ;
		stb.append( "]" ) ;
		this.selectedGroupIdString = stb.toString() ;
	}
	public List<GroupViewBean> getPreservedGroupViewBeanList() {
		return preservedGroupViewBeanList;
	}
	public void setPreservedGroupViewBeanList(
			List<GroupViewBean> preservedGroupViewBeanList) {
		this.preservedGroupViewBeanList = preservedGroupViewBeanList;
	}

	public boolean isAllowListView() {
		return allowListView;
	}
	public void setAllowListView( boolean value ) {
		this.allowListView = value;
	}
	public String getAllowListContent() {
		return allowListContent;
	}
	public void setAllowListContent( String content ) {
		this.allowListContent = content;
	}

	public boolean isAllowModifyAllowListView() {
		return allowModifyAllowListView;
	}
	public void setAllowModifyAllowListView( boolean value ) {
		this.allowModifyAllowListView = value;
	}


	public List<ModuleViewBean> getSelectedModuleViewBeanList() {
		return selectedModuleViewBeanList;
	}
	public void setSelectedModuleViewBeanList(
			List<ModuleViewBean> selectedModuleViewBeanList) {
		this.selectedModuleViewBeanList = selectedModuleViewBeanList;
	}

	public BuildEnvViewBean getBuildEnvBean() {
		return buildEnvBean;
	}
	public void setBuildEnvBean(BuildEnvViewBean buildEnvBean) {
		this.buildEnvBean = buildEnvBean;
	}
	public BuildEnvViewBean getFullBuildEnvBean() {
		return fullBuildEnvBean;
	}
	public void setFullBuildEnvBean(BuildEnvViewBean fullBuildEnvBean) {
		this.fullBuildEnvBean = fullBuildEnvBean;
	}

	public String getSelectedBuildEnvNo() {
		return selectedBuildEnvNo;
	}
	public void setSelectedBuildEnvNo(String selectedBuildEnvNo) {
		this.selectedBuildEnvNo = selectedBuildEnvNo;
	}
	public String getSelectedFullBuildEnvNo() {
		return selectedFullBuildEnvNo;
	}
	public void setSelectedFullBuildEnvNo(String selectedDullBuildEnvNo) {
		this.selectedFullBuildEnvNo = selectedDullBuildEnvNo;
	}

	public List<ServerViewBean> getServerViewBeanList() {
		return serverViewBeanList;
	}
	public void setServerViewBeanList( List<ServerViewBean> serverViewBeanList ) {
		this.serverViewBeanList = serverViewBeanList;
	}
	public ServerViewBean getSelectedServerViewBean() {
		return selectedServerViewBean;
	}
	public void setSelectedServerViewBean( ServerViewBean selectedServerViewBean ) {
		this.selectedServerViewBean = selectedServerViewBean;
	}
	public String getScmType() {
		return scmType;
	}
	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}

	public List<ModuleViewBean> getModuleViewBeanList() {
		return moduleViewBeanList;
	}
	public void setModuleViewBeanList(List<ModuleViewBean> moduleViewBeanList) {
		this.moduleViewBeanList = moduleViewBeanList;
	}
	public List<BuildEnvViewBean> getBuildEnvViewBeanList() {
		return buildEnvViewBeanList;
	}
	public void setBuildEnvViewBeanList(List<BuildEnvViewBean> buildEnvViewBeanList) {
		this.buildEnvViewBeanList = buildEnvViewBeanList;
	}
	public List<BuildEnvViewBean> getBuildEnvViewBeanEnableList() {
		return buildEnvViewBeanEnableList;
	}
	public void setBuildEnvViewBeanEnableList(
			List<BuildEnvViewBean> buildEnvViewBeanEnableList) {
		this.buildEnvViewBeanEnableList = buildEnvViewBeanEnableList;
	}
	public String getSelectedModuleNameString() {
		return selectedModuleNameString;
	}
	public String[] getSelectedModuleNames() {
		return this.selectedModuleNames;
	}
	public void setSelectedModuleNameString( String... selectedModuleNameString ) {
		this.selectedModuleNames = selectedModuleNameString;

		StringBuilder stb = new StringBuilder() ;
		stb.append( "[" ) ;
		stb.append( TriStringUtils.convertArrayToString( selectedModuleNameString ) ) ;
		stb.append( "]" ) ;
		this.selectedModuleNameString = stb.toString() ;
	}

	public String getSelectedRelEnvNoString() {
		return selectedRelEnvNoString;
	}
	public void setSelectedRelEnvNoString(String selectedRelEnvNoString) {
		this.selectedRelEnvNoString = selectedRelEnvNoString;
	}
	public String[] getSelectedRelEnvNos() {
		return this.selectedRelEnvNos;
	}
	public void setSelectedRelEnvNoString( String... selectedRelEnvNoString ) {
		this.selectedRelEnvNos = selectedRelEnvNoString;

		StringBuilder stb = new StringBuilder() ;
		stb.append( "[" ) ;
		stb.append( TriStringUtils.convertArrayToString( selectedRelEnvNoString ) ) ;
		stb.append( "]" ) ;
		this.selectedRelEnvNoString = stb.toString() ;
	}

	public String getInputUserId() {
		return inputUserId;
	}
	public void setInputUserId(String inputUserId) {
		this.inputUserId = inputUserId;
	}

	public List<GroupViewBean> getSpecifiedGroupList() {
		return specifiedGroupList;
	}
	public void setSpecifiedGroupList(List<GroupViewBean> specifiedGroupList) {
		this.specifiedGroupList = specifiedGroupList;
	}
	public List<GroupViewBean> getPreservedSpecifiedGroupList() {
		return preservedSpecifiedGroupList;
	}
	public void setPreservedSpecifiedGroupList(
			List<GroupViewBean> preservedSpecifiedGroupList) {
		this.preservedSpecifiedGroupList = preservedSpecifiedGroupList;
	}
	public String getSelectedSpecifiedGroupIdString() {
		return selectedSpecifiedGroupIdString;
	}
	public String[] getSelectedSpecifiedGroupIds() {
		return selectedSpecifiedGroupIds;
	}
	public void setSelectedSpecifiedGroupIdString(
			String... selectedSpecifiedGroupIdString) {
		selectedSpecifiedGroupIds = selectedSpecifiedGroupIdString;

		StringBuilder stb = new StringBuilder() ;
		stb.append( "[" ) ;
		stb.append( TriStringUtils.convertArrayToString( selectedSpecifiedGroupIdString ) ) ;
		stb.append( "]" ) ;
		this.selectedSpecifiedGroupIdString = stb.toString() ;
	}
	public List<GroupViewBean> getSelectedSpecifiedGroupList() {
		return selectedSpecifiedGroupList;
	}
	public void setSelectedSpecifiedGroupList(
			List<GroupViewBean> selectedSpecifiedGroupList) {
		this.selectedSpecifiedGroupList = selectedSpecifiedGroupList;
	}
	public void setSelectedGroupIdString(String selectedGroupIdString) {
		this.selectedGroupIdString = selectedGroupIdString;
	}
	public void setSelectedModuleNameString(String selectedModuleNameString) {
		this.selectedModuleNameString = selectedModuleNameString;
	}
	public List<RelEnvViewBean> getRelEnvViewBeanList() {
		return relEnvViewBeanList;
	}
	public void setRelEnvViewBeanList(List<RelEnvViewBean> relEnvViewBeanList) {
		this.relEnvViewBeanList = relEnvViewBeanList;
	}
	public List<RelEnvViewBean> getSelectedRelEnvViewBeanList() {
		return selectedRelEnvViewBeanList;
	}
	public void setSelectedRelEnvViewBeanList(
			List<RelEnvViewBean> selectedRelEnvViewBeanList) {
		this.selectedRelEnvViewBeanList = selectedRelEnvViewBeanList;
	}

}

