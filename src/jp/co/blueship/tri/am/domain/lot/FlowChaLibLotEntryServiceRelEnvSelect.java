package jp.co.blueship.tri.am.domain.lot;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理・ロット作成画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotEntryServiceRelEnvSelect implements IDomain<FlowChaLibLotEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}
	@Override
	public IServiceDto<FlowChaLibLotEntryServiceBean> execute( IServiceDto<FlowChaLibLotEntryServiceBean> serviceDto ) {

		FlowChaLibLotEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward() ;
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LOT_REL_ENV_SELECT ) &&
					!forward.equals( ChaLibScreenID.LOT_REL_ENV_SELECT )) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean() ;

			try {
				if( ChaLibScreenID.LOT_REL_ENV_SELECT.equals( referer ) ) {
					if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {

					}
				}
				if( ChaLibScreenID.LOT_REL_ENV_SELECT.equals( forward ) ) {//リリース環境選択画面
					if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

						List<RelEnvViewBean> relEnvViewBeanList = this.support.getRelEnvViewBeanList( paramBean );
						lotEditInputBean.setRelEnvViewBeanList( relEnvViewBeanList ) ;
					}
				}

			} finally {

				paramBean.setLotEditInputBean( lotEditInputBean ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005102S , e );
		}
	}
}
