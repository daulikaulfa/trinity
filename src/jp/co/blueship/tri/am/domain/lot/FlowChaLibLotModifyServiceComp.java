package jp.co.blueship.tri.am.domain.lot;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 変更管理・ロット変更画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibLotModifyServiceComp implements IDomain<FlowChaLibLotModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = null;

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	private ActionStatusMatrixList statusMatrixAction = null;

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibLotModifyServiceBean> execute(IServiceDto<FlowChaLibLotModifyServiceBean> serviceDto) {

		FlowChaLibLotModifyServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (!referer.equals(ChaLibScreenID.COMP_LOT_MODIFY) && !forward.equals(ChaLibScreenID.COMP_LOT_MODIFY)) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

			try {

				if (ChaLibScreenID.COMP_LOT_MODIFY.equals(forward)) {// ロット変更完了画面
					if (ScreenType.next.equals(paramBean.getScreenType())) {

						if( paramBean.isStatusMatrixV3() ) {
							StatusCheckDto statusDto = new StatusCheckDto()
							.setServiceBean( paramBean )
							.setFinder( support )
							.setActionList( statusMatrixAction )
							.setLotIds( lotEditInputBean.getLotNo() );
	
							StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
						}
						
						List<Object> paramList = new CopyOnWriteArrayList<Object>();
						paramList.add(paramBean); // インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）

						ILotDto lotDto = this.support.findLotDto(lotEditInputBean.getLotNo());

						LotParamInfo lotParamInfo = this.support.populateLotParamInfo(lotEditInputBean, lotDto);
						paramList.add(lotParamInfo);

						IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(paramBean)
								.setParamList(paramList);

						for (IDomain<IGeneralServiceBean> action : this.actions) {
							action.execute(innerServiceDto);
						}
					}
				}

			} finally {

				paramBean.setLotEditInputBean(lotEditInputBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005105S, e);
		}
	}
}
