package jp.co.blueship.tri.am.domain.lot;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 変更管理・ロット作成画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibLotEntryServiceComp implements IDomain<FlowChaLibLotEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = null;

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibLotEntryServiceBean> execute(IServiceDto<FlowChaLibLotEntryServiceBean> serviceDto) {

		FlowChaLibLotEntryServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (!referer.equals(ChaLibScreenID.COMP_LOT_ENTRY) && !forward.equals(ChaLibScreenID.COMP_LOT_ENTRY)) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

			try {

				if (ChaLibScreenID.COMP_LOT_ENTRY.equals(forward)) {// ロット作成完了画面
					if (ScreenType.next.equals(paramBean.getScreenType())) {

						List<Object> paramList = new CopyOnWriteArrayList<Object>();
						paramList.add(paramBean); // インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）

						LotParamInfo lotParamInfo = this.support.createLotParamInfo(lotEditInputBean);
						paramList.add(lotParamInfo);

						IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(paramBean)
								.setParamList(paramList);

						paramBean.setLotNo(lotParamInfo.getLotDto().getLotEntity().getLotId());
						paramBean.setLockLotNo(lotParamInfo.getLotDto().getLotEntity().getLotId());
						
						for (IDomain<IGeneralServiceBean> action : this.actions) {
							action.execute(innerServiceDto);
						}

					}
				}

			} finally {

				paramBean.setLotEditInputBean(lotEditInputBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005102S, e);
		}
	}

}
