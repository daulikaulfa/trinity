package jp.co.blueship.tri.am.domain.lot.beans;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;

/**
 * ロット作成時の事前チェック処理を行います。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionValidateCreateLotForSvn extends ActionPojoAbstract<IGeneralServiceBean> {
	@SuppressWarnings("unused")
	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;
	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {

			List<Object> paramList = serviceDto.getParamList();
			LotParamInfo lotParamInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;
			ILotDto lotDto = lotParamInfo.getLotDto();
			ILotEntity pjtLotEntity = lotDto.getLotEntity() ;

			if( ! VcsCategory.SVN.equals( VcsCategory.value( pjtLotEntity.getVcsCtgCd() ))) {
				return serviceDto;
			}

			String serverNo = this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId();


			List<IVcsReposDto> reposDtoList =
					this.support.findVcsReposDtoFromLotMdlLnkEntities( lotDto.getIncludeMdlEntities( true ) );
			if ( 0 == reposDtoList.size() ) {
				throw new TriSystemException( AmMessageId.AM004062F );
			}

			String userName = reposDtoList.get(0).getVcsReposEntity().getVcsUserNm();
			String password = this.support.getSmFinderSupport().findPasswordEntity( PasswordCategory.SVN , serverNo , userName ).getPassword();

//			SVNリポジトリに当該モジュールが実在することをチェック
			this.chkExistsModuleInTrunk(userName , password , reposDtoList , lotDto ) ;

			//SVNでは、初回ロット作成時にHEAD原本資産のワークパスが存在しなかった場合、自動でチェックアウトを行う。
			//そのため、以下のチェックは行わないこととする。V2Lxx
			//ＨＥＡＤ情報取得
			//新規作成するロット作成時点に存在したすべてのモジュールごとにチェック
			//ＨＥＡＤに当該モジュールが実在することをチェック
			//新規作成するロットで選択された、モジュールごとにチェック
			//ＨＥＡＤに当該モジュールが実在することをチェック

			// グループの存在チェック
			List<ILotGrpLnkEntity> groupEntityArray = lotDto.getLotGrpLnkEntities() ;
			if ( null != groupEntityArray ) {
				for( ILotGrpLnkEntity groupEntity : groupEntityArray ) {
					GrpCondition condition = new GrpCondition();
					condition.setGrpId(groupEntity.getGrpId());
					IGrpEntity gEntity = this.support.getUmFinderSupport().getGrpDao().findByPrimaryKey( condition.getCondition() ) ;
					if( null == gEntity ) {
						throw new TriSystemException(AmMessageId.AM004032F , groupEntity.getGrpId() );
					}
				}
			}

			// グループの存在チェック
			List<ILotMailGrpLnkEntity> specifiedGroupEntityArray = lotDto.getLotMailGrpLnkEntities() ;
			for( ILotMailGrpLnkEntity specifiedGroupEntity : specifiedGroupEntityArray ) {
				GrpCondition condition = new GrpCondition();
				condition.setGrpId( specifiedGroupEntity.getGrpId() );
				IGrpEntity gEntity = this.support.getUmFinderSupport().getGrpDao().findByPrimaryKey( condition.getCondition() ) ;
				if( null == gEntity ) {
					throw new TriSystemException(AmMessageId.AM004033F , specifiedGroupEntity.getGrpId() );
				}
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005077S , e ) ;
		}

		return serviceDto;
	}

	/**
	 * リポジトリのtrunk以下をチェックし、チェックアウトしようとしているモジュールがすべて存在するか判定して返す。<br>
	 * @param userName ユーザ名
	 * @param password パスワード
	 * @param reposDtoList リポジトリ情報
	 * @param lotDto ロット情報
	 * @throws ScmException
	 */
	private void chkExistsModuleInTrunk(String userName , String password , List<IVcsReposDto> reposDtoList , ILotDto lotDto ) throws ScmException {
		ILotEntity lotEntity = lotDto.getLotEntity();

		Map<String, Set<String>> moduleNameSetMap = new HashMap<String, Set<String>>();

		IVcsService vcsService = factory.createService(VcsCategory.value( lotEntity.getVcsCtgCd() ), userName, password);
		IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;

		if( ! new File( headEntity.getMwPath() ).exists() ) {
			throw new BusinessException(AmMessageId.AM001001E , TriObjectUtils.defaultIfNull(headEntity.getMwPath(), "null") );
		}

		for ( IVcsReposDto reposDto: reposDtoList ) {
			IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
			String moduleName = reposDto.getVcsMdlEntity().getMdlNm();

			if ( ! moduleNameSetMap.containsKey( reposEntity.getVcsReposId() ) ) {
				ListSubDirVO vo = new ListSubDirVO();
				vo.setRepository( reposEntity.getVcsReposUrl() );
				Set<String> moduleNameSet = vcsService.chkExistsModuleInTrunk(vo);

				moduleNameSetMap.put( reposEntity.getVcsReposId(), moduleNameSet );
			}

			Set<String> moduleNameSet = moduleNameSetMap.get( reposEntity.getVcsReposId() );

			if( true != moduleNameSet.contains( moduleName ) ) {
				throw new TriSystemException(AmMessageId.AM004034F , moduleName );
			}

			//チェックアウト済み、かつワーキングコピーとして正常かチェック
			try {
				vcsService.statusSingle(setSVO(headEntity.getMwPath(), moduleName));
			} catch( Exception e ) {
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new BusinessException(AmMessageId.AM001000E , e, moduleName );
			}
		}
	}

	private StatusVO setSVO(String pathDest,String moduleName){

		StatusVO vo = new StatusVO();
		vo.setPathDest(pathDest);
		vo.setObjectPath(moduleName);
		return vo;
	}

}
