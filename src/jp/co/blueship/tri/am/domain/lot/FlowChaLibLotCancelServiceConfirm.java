package jp.co.blueship.tri.am.domain.lot;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * 変更管理・ロット取消確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibLotCancelServiceConfirm implements IDomain<FlowChaLibLotCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibLotCancelServiceBean> execute( IServiceDto<FlowChaLibLotCancelServiceBean> serviceDto ) {

		FlowChaLibLotCancelServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID 	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.LOT_CANCEL_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.LOT_CANCEL_CONFIRM ) ){
				return serviceDto;
			}

			String selectedLotNo	= paramBean.getSelectedLotNo();
			int selectPageNo		=
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			if ( refererID.equals( ChaLibScreenID.LOT_CANCEL_CONFIRM )) {

				LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

				if ( ScreenType.next.equals( screenType )) {

					AmItemChkUtils.checkLotCancel( lotEditInputBean );
					
					if( paramBean.isStatusMatrixV3() ) {
	
						IPjtEntity[] entities = this.support.getPjtEntityLimit( selectedLotNo, 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);
	
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( selectedLotNo )
						.setPjtIds( this.support.getPjtNo( entities ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				paramBean.setLotEditInputBean( lotEditInputBean );
			}


			if ( forwordID.equals( ChaLibScreenID.LOT_CANCEL_CONFIRM )) {

				AmItemChkUtils.checkLotNo( selectedLotNo );

				ILotDto lotDto = this.support.findLotDto( selectedLotNo );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				// Scmモード
				LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

				VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
				lotEditInputBean.setScmType( scmType.value() );

				paramBean.setLotDetailViewBean( this.support.getLotDetailViewBean( paramBean, lotDto ));

				if( paramBean.isStatusMatrixV3() ) {
					IPjtEntity[] entities = this.support.getPjtEntityLimit( selectedLotNo, 1, 0, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() ).getEntities().toArray(new IPjtEntity[0]);
	
					StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
						.setLotIds( selectedLotNo )
						.setPjtIds( this.support.getPjtNo( entities ) );
	
					StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
				}
				
				IEntityLimit<IPjtEntity> limit = getPjtEntityLimit( selectedLotNo, selectPageNo );

				setServiceBeanPjtEntity( limit, selectPageNo, paramBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005098S , e );
		}
	}

	/**
	 * 変更管理エンティティリミットを取得する
	 * @param lotId ロット番号
	 * @param pageNo ページ番号
	 * @return 変更管理エンティティリミット
	 */
	private IEntityLimit<IPjtEntity> getPjtEntityLimit( String lotId, int pageNo ) {

		IEntityLimit<IPjtEntity> limit =
			this.support.getPjtEntityLimit(
					lotId,
					pageNo,
					sheet.intValue( AmDesignEntryKeyByChangec.maxPageNumberByLotCancelConfirm ),
					AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotCancelConfirm() );

		return limit;
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 * @param limit 変更管理エンティティリミット
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibLotCancelServiceBeanオブジェクト
	 */
	private void setServiceBeanPjtEntity(
			IEntityLimit<IPjtEntity> limit, int selectPageNo, FlowChaLibLotCancelServiceBean paramBean ) {

		paramBean.setPjtViewBeanList	( AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( limit.getEntities().toArray(new IPjtEntity[0]) ));
		paramBean.setPageInfoView		(
				AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
		paramBean.setSelectPageNo		( selectPageNo );

	}
}
