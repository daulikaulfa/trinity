package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・モジュール一覧用
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class RelEnvViewBean implements Serializable {

    private static final long serialVersionUID = 1L;

    /** 環境番号 */
    private String envNo = null ;

    /** 環境名 */
    private String envName = null ;

    /** 環境概要 */
    private String envSummary = null ;

    /** 最終更新日時 */
    private String latestUpdateDate = null ;

    private String statusId = null ;
    
    public String getEnvName() {
        return envName;
    }

    public void setEnvName(String envName) {
        this.envName = envName;
    }

    public String getEnvNo() {
        return envNo;
    }

    public void setEnvNo(String envNo) {
        this.envNo = envNo;
    }

    public String getEnvSummary() {
        return envSummary;
    }

    public void setEnvSummary(String envSummary) {
        this.envSummary = envSummary;
    }
    
    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getLatestUpdateDate() {
        return latestUpdateDate;
    }

    public void setLatestUpdateDate(String latestUpdateDate) {
        this.latestUpdateDate = latestUpdateDate;
    }
}
