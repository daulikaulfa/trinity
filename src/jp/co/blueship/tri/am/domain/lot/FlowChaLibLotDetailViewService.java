package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchBeanAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtSearchBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * 変更管理・ロット詳細画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotDetailViewService implements IDomain<FlowChaLibLotDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotDetailViewServiceBean> execute( IServiceDto<FlowChaLibLotDetailViewServiceBean> serviceDto ) {

		FlowChaLibLotDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo( selectedLotNo );

			// LotDto設定
			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			List<IMessageId> messageList = new ArrayList<IMessageId>();
			List<String[]> messageArgsList = new ArrayList<String[]>();

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroupForV3(
					lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()),
					messageList,
					messageArgsList
					);

			AmLibraryAddonUtils.checkSpecifiedGroup(
					lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					messageList,
					messageArgsList
					);

			if ( 0 < messageList.size() ) {
				// 業務エラーメッセージの設定
				paramBean.setInfoMessageIdList( messageList );
				paramBean.setInfoMessageArgsList( messageArgsList );
			}

			// Scmモード
			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
			paramBean.setScmType( scmType.value() );

			paramBean.setLotDetailViewBean( this.support.getLotDetailViewBean( paramBean, lotDto ));

			//変更管理詳細検索
			paramBean.setPjtSearchBean( AmDBSearchBeanAddonUtils.setPjtSearchBean( paramBean.getPjtSearchBean() ) ) ;

			this.setServiceBeanRelUnitResult	( selectedLotNo, paramBean );

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			this.setServiceBeanPjtEntity		( selectedLotNo, selectPageNo, paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005101S , e );
		}
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 * @param selectedLotNo ロット番号
	 * @param paramBean FlowChaLibLotDetailViewServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(
			String selectedLotNo, FlowChaLibLotDetailViewServiceBean paramBean ) {

		IJdbcCondition successCountCondition =
			AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition( selectedLotNo );

		paramBean.setSucceededRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( successCountCondition.getCondition() ) );

		IJdbcCondition failureCountCondition =
			AmDBSearchConditionAddonUtils.getFailedRelUnitCondition( selectedLotNo );

		paramBean.setFailedRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( failureCountCondition.getCondition() ) );
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 * @param lotId ロット番号
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibLotDetailViewServiceBeanオブジェクト
	 */
	private void setServiceBeanPjtEntity(
			String lotId, int selectPageNo, FlowChaLibLotDetailViewServiceBean paramBean ) {

		PjtSearchBean pjtSearchBean = paramBean.getPjtSearchBean() ;

		int viewRows = sheet.intValue(AmDesignEntryKeyByChangec.maxPageNumberByLotDetailView ) ;
		ISqlSort sort = AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotDetailView() ;

		PjtCondition condition = new PjtCondition();
		condition.setLotId( lotId );

		//変更管理番号
		condition.setPjtId( pjtSearchBean.getSearchPjtNo(), true );

		//変更要因番号
		condition.setChgFactorNo( pjtSearchBean.getSearchChangeCauseNo(), true ) ;

		String[] selectedStatusArray = pjtSearchBean.getSelectedStatusStringArray() ;
		condition.setProcStsIds( AmDBSearchConditionAddonUtils.convertStatusToStatusId( selectedStatusArray ) ) ;

		@SuppressWarnings("unused")
		int maxHitLimit = 0;

		if ( ! TriStringUtils.isEmpty( pjtSearchBean.getSelectedPjtCount() ) ) {
			maxHitLimit = Integer.parseInt( pjtSearchBean.getSelectedPjtCount() );
		}

		IEntityLimit<IPjtEntity> limit =
//			this.support.getPjtDao().find(	condition.getCondition(), sort, selectPageNo, viewRows, maxHitLimit );
				this.support.getPjtDao().find(	condition.getCondition(), sort, selectPageNo, viewRows );

		paramBean.setPjtViewBeanList	( AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( limit.getEntities().toArray(new IPjtEntity[0]) ));
		paramBean.setPageInfoView		(
				AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
		paramBean.setSelectPageNo		( selectPageNo );

	}
}
