package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・モジュール一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ModuleViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** モジュール名 */
	private String moduleName = null ;
	/** リポジトリ */
	private String repository = null;
	
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getRepository() {
		return repository;
	}
	public void setRepository(String repository) {
		this.repository = repository;
	}

}
