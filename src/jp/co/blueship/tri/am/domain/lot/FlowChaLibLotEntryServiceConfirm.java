package jp.co.blueship.tri.am.domain.lot;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理・ロット作成画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotEntryServiceConfirm implements IDomain<FlowChaLibLotEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowChaLibLotEntryServiceBean> execute( IServiceDto<FlowChaLibLotEntryServiceBean> serviceDto ) {

		FlowChaLibLotEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward() ;
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LOT_ENTRY_CONFIRM ) &&
					!forward.equals( ChaLibScreenID.LOT_ENTRY_CONFIRM )) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean() ;
			setAllowListContent(lotEditInputBean);
			try {

				if( ChaLibScreenID.LOT_ENTRY_CONFIRM.equals( forward ) ) {//ロット作成確認画面
					if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
						//AmItemChkUtils.checkLotRelEnvInfo( lotEditInputBean ) ;
					}
				}

			} finally {

				paramBean.setLotEditInputBean( lotEditInputBean ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005102S,e);
		}
	}

	/**
	 * 一覧表示許可の文言を設定する(ロケール切り替え対応)
	 * @param bean
	 */
	private void setAllowListContent(LotEditInputV3Bean bean ) {

		if ( bean.isAllowListView() ) {
			bean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.ALLOW_LIST_VIEW_CONTENT.getKey() ) );
		} else {
			bean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.DENY_LIST_VIEW_CONTENT.getKey() ) );
		}
	}

}
