package jp.co.blueship.tri.am.domain.lot.beans;

import java.io.File;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistEntity;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsCommitInfo;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;

/**
 * ロット作成処理を行います。
 * Subversion用
 *
 * @version V3L10R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Tang
 */
public class ActionCreateLotForSvn extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;

	private FlowChaLibLotEditSupport support = null;
	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}

	private IVcsService vcsService = null;

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		LogBusinessFlow logBusinessFlow = new LogBusinessFlow() ;
		String flowActionTitle = null ;

		boolean isSuccess = false;

		try {

			LotParamInfo lotParamInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;
			ILotDto lotDto = lotParamInfo.getLotDto();
			ILotEntity lotEntity = lotParamInfo.getLotDto().getLotEntity();

			if( ! VcsCategory.SVN.equals( VcsCategory.value( lotEntity.getVcsCtgCd() ))) {
				return serviceDto;
			}

			//IGeneralServiceBean info = AmExtractEntityAddonUtils.getinfo( paramList );
			IGeneralServiceBean info = serviceDto.getServiceBean();

			//ログの初期処理
			logBusinessFlow.makeLogPath( info.getFlowAction() , lotEntity ) ;

			//ログの開始ラベル出力
			flowActionTitle = LogBusinessFlow.makeFlowActionTitle( info.getFlowAction() ) ;
			logBusinessFlow.writeLogWithDateBegin( flowActionTitle ) ;

			//ログのヘッダ部出力
			this.outLogHeader( logBusinessFlow , info , lotDto ) ;

			String serverNo = this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId();

			Map<String, IVcsReposDto> reposMapByMdlNmKey = this.getVcsReposDtoMap( lotDto );

			if ( 0 == reposMapByMdlNmKey.size() ) {
				throw new TriSystemException( AmMessageId.AM004062F );
			}

			String userName = reposMapByMdlNmKey.values().toArray( new IVcsReposDto[0])[0].getVcsReposEntity().getVcsUserNm();
			String password = this.support.getSmFinderSupport().findPasswordEntity( PasswordCategory.SVN , serverNo , userName ).getPassword();

			vcsService = factory.createService(VcsCategory.value( lotEntity.getVcsCtgCd() ), userName, password);

			IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;

			//HEAD原本がチェックアウトされているかをチェック、チェックアウト未ならばここで行う
			this.chkExistsTrunkWorkPath(reposMapByMdlNmKey, headEntity, lotDto, logBusinessFlow ) ;

			//ロットに関連するディレクトリの作成
			this.makeDirectories( lotParamInfo , lotDto) ;

			Map<String, IVcsReposEntity> reposMapByReposIdKey = this.getReposMap( reposMapByMdlNmKey );
			lotParamInfo.setBaseRevisionNo(  new HashMap<String, String>() );
			lotParamInfo.setBranchRevisionNo(  new HashMap<String, String>() ) ;

			for ( IVcsReposEntity reposEntity: reposMapByReposIdKey.values() ) {
				//新規作成するロット作成時点に存在したすべてのモジュールごとに処理（HEADタグ付与・ロット作成）
				this.makeBranch(lotParamInfo , reposEntity , reposMapByMdlNmKey, lotDto , logBusinessFlow ) ;
				//新規作成するロットで選択された、モジュールごとに処理（ロットのチェックアウト）
				this.checkoutBranchModules(reposEntity , reposMapByMdlNmKey, lotDto , logBusinessFlow ) ;
			}

			this.updateLotInfo( lotParamInfo );
			this.createWiki(lotEntity);

			isSuccess = true;

		} catch( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;

			throw be ;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005075S , e );
		} finally {
			if ( null != flowActionTitle ) {
				logBusinessFlow.writeLogWithDateEnd( flowActionTitle ) ;

				if ( isSuccess ) {
					logBusinessFlow.deleteLog( AmDesignEntryKeyByLogs.deleteLogAtSuccessLotEntry );
				}
			}

		}

		return serviceDto;
	}

	/**
	 * Create a Wiki of Home.
	 *
	 * @param lotEntity
	 */
	private void createWiki(ILotEntity lotEntity) {
		String wikiId = this.insertWiki(lotEntity.getLotId(), "", "");
		this.insertWikiHist(wikiId);
	}

	/**
	 * Create a Wiki Information of Home.
	 *
	 * @param lotId
	 * @param pageNm
	 * @param content
	 * @return
	 */
	private String insertWiki(String lotId, String pageNm, String content) {
		String wikiId = this.support.getUmFinderSupport().getWikiNumberingDao().nextval();

		IWikiEntity entity = new WikiEntity();
		entity.setWikiId(wikiId);
		entity.setLotId(lotId);
		entity.setPageNm("Home");
		entity.setContent(content);
		entity.setIsWikiHome(StatusFlg.on);
		this.support.getUmFinderSupport().getWikiDao().insert(entity);

		return wikiId;
	}

	/**
	 * Create a Wiki change history of Home
	 *
	 * @param wikiId
	 */
	private void insertWikiHist(String wikiId) {
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.support.getUmFinderSupport().getWikiDao()
				.findByPrimaryKey(condition.getCondition());

		IWikiHistEntity histEntity = new WikiHistEntity();
		histEntity.setWikiId(wikiId);
		histEntity.setWikiVerNo(wikiEntity.getWikiVerNo());
		histEntity.setPageNm(wikiEntity.getPageNm());
		histEntity.setContent(wikiEntity.getContent());

		this.support.getUmFinderSupport().getWikiHistDao().insert(histEntity);
	}

	/**
	 * ロット情報を更新する。
	 * <br>
	 * @param paramInfo
	 */
	private void updateLotInfo( LotParamInfo paramInfo ) {
		ILotEntity entity = paramInfo.getLotDto().getLotEntity();
		List<ILotMdlLnkEntity> mdlEntities = paramInfo.getLotDto().getLotMdlLnkEntities();

		Map<String, String> headRevMap =  paramInfo.getBaseRevisionNo();
		Map<String, String> lotRevMap =  paramInfo.getBranchRevisionNo();
		for ( ILotMdlLnkEntity mdlEntity: mdlEntities ) {
			if ( headRevMap.containsKey( mdlEntity.getMdlNm() ) ) {
				mdlEntity.setLotRegHeadRev( headRevMap.get( mdlEntity.getMdlNm() ) );
			}

			if ( lotRevMap.containsKey( mdlEntity.getMdlNm()) ) {
				mdlEntity.setLotRegRev( lotRevMap.get( mdlEntity.getMdlNm() ) );

			}
		}

		entity.setStsId( AmLotStatusId.LotInProgress.getStatusId() );

		this.support.getLotDao().update( entity );
		this.support.getLotMdlLnkDao().update( mdlEntities );
	}

	private Map<String, IVcsReposEntity> getReposMap( Map<String, IVcsReposDto> reposDtoMap ) throws ScmException {

		Map<String, IVcsReposEntity> reposMap = new HashMap<String, IVcsReposEntity>();

		for ( IVcsReposDto reposDto: reposDtoMap.values() ) {
			IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();

			if ( ! reposMap.containsKey( reposEntity.getVcsReposId() ) ) {
				reposMap.put( reposEntity.getVcsReposId(), reposEntity );
			}
		}

		return reposMap;
	}

	/**
	 * ロットに関連する各種ディレクトリを作成する<br>
	 * @param pjtLotEntity
	 * @param logBusinessFlow
	 * @throws Exception
	 */
	private void makeDirectories( LotParamInfo lotParamInfo , ILotDto lotDto) throws Exception {

		ILotEntity pjtLotEntity = lotDto.getLotEntity();
		String workPath = AmDesignBusinessRuleUtils.getMasterWorkPath( pjtLotEntity ).getCanonicalPath() ;
		String inOutBox = pjtLotEntity.getInOutBox() ;
		String historyPath = pjtLotEntity.getHistPath() ;
		String workspace = pjtLotEntity.getWsPath() ;
		String systemInBox = pjtLotEntity.getSysInBox() ;

		/** ロットのディレクトリ作成 **/
		this.makeDirectory( inOutBox ) ;
		this.makeDirectory( workPath ) ;
		this.makeDirectory( historyPath ) ;
		this.makeDirectory( workspace ) ;
		this.makeDirectory( systemInBox ) ;

		/** グループ名フォルダまで作成する */
		AmBusinessFileUtils.createInOutBoxDirectory( lotParamInfo, lotDto );
	}
	private void makeBranch(LotParamInfo lotParamInfo ,
			IVcsReposEntity ucfRepositoryEntity ,
			Map<String, IVcsReposDto> reposDtoMap,
			ILotDto lotDto ,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		try {

			String repository = ucfRepositoryEntity.getVcsReposUrl() ;

			logBusinessFlow.writeLog( TriLogMessage.LAM0049 , SEP ,repository + SEP + SEP ) ;

			/** ロット作成（ブランチタグを切る） **/
			VcsCommitInfo vcsCommitInfo = vcsService.branch(setCommitVO(repository , lotDto.getLotEntity() , logBusinessFlow ));

			logBusinessFlow.writeLog( this.editCommitInfo( vcsCommitInfo ) ) ;

			long branchRevision = vcsCommitInfo.getNewRevision() ;

			Map<String, String> headRevMap = lotParamInfo.getBaseRevisionNo();
			Map<String, String> lotRevMap = lotParamInfo.getBranchRevisionNo();

			for ( IVcsReposDto vcsReposDto: reposDtoMap.values() ) {
				if ( ! ucfRepositoryEntity.getVcsReposId().equals(vcsReposDto.getVcsReposEntity().getVcsReposId()) )
					continue;

				//ブランチ作成前のリビジョン番号は必ず前(-1)の番号
				headRevMap.put(vcsReposDto.getVcsMdlEntity().getMdlNm(), String.valueOf(branchRevision - 1));

				//ブランチ作成時のリビジョン番号を記録するためにセット
				lotRevMap.put(vcsReposDto.getVcsMdlEntity().getMdlNm(), String.valueOf(branchRevision));
			}

		} catch( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
		}

	}

	/**
	 * HEAD原本資産のMasterWorkPathがチェックアウトされているかチェック、チェックアウト未ならばチェックアウトを行う。<br>
	 * HEAD原本資産のチェックアウト動作は、まず最初の１回目にしか実行されない。
	 * @param reposDtoMap リポジトリ情報
	 * @param headEntity HEAD情報
	 * @param lotDto ロット情報
	 * @param logBusinessFlow ログハンドラ
	 */
	private void chkExistsTrunkWorkPath(
			Map<String, IVcsReposDto> reposDtoMap,
			IHeadEntity headEntity,
			ILotDto lotDto,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		logBusinessFlow.writeLog( TriLogMessage.LAM0050 ) ;

		String masterworkPath = headEntity.getMwPath() ;
		File masterworkFile = new File( masterworkPath ) ;

		if( masterworkFile.exists() ) {//MasterworkPathのフォルダが存在
			logBusinessFlow.writeLog( TriLogMessage.LAM0051 , masterworkPath ) ;
			//チェックアウト済み、かつワーキングコピーとして正常かチェック

			Set<String> mdlNmSet = new HashSet<String>();
			for ( ILotMdlLnkEntity mdlLnkEntity: lotDto.getIncludeMdlEntities( true ) ) {
				mdlNmSet.add( mdlLnkEntity.getMdlNm() );
			}

			for( IVcsReposDto reposDto : reposDtoMap.values() ) {
					IVcsMdlEntity mdlEntity = reposDto.getVcsMdlEntity();

					if ( ! mdlNmSet.contains( mdlEntity.getMdlNm() ) ) {
						continue;
					}

					logBusinessFlow.writeLog( TriLogMessage.LAM0052 , mdlEntity.getMdlNm() ) ;
					try {
						SVNStatus svnStatus = vcsService.statusSingle(setSVO(masterworkPath, mdlEntity.getMdlNm()));
						logBusinessFlow.writeLog( vcsService.makeSvnStatusStr( svnStatus ) ) ;
					} catch( Exception e ) {
						logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
						ExceptionUtils.reThrowIfTrinityException(e);
						throw new TriSystemException(AmMessageId.AM004055F , mdlEntity.getMdlNm() );
					}
				}
		} else {
			throw new TriSystemException(AmMessageId.AM004056F , TriObjectUtils.defaultIfNull(headEntity.getMwPath(), "null") );
		}

//		if( true != errFlg ) {
//			logBusinessFlow.writeLog( TriLogMessage.LAM0053 ) ;
//			return ;//正常なので抜ける
//		} else {//チェックアウトがされていない、もしくはワーキングコピーとして異常
//			logBusinessFlow.writeLog( TriLogMessage.LAM0054 ) ;
//			this.support.initializeDirectory( masterworkPath ) ;
//			//新たにチェックアウトを行う
//			for( IVcsReposDto reposDto : reposDtoMap.values() ) {
//				String repository = reposDto.getVcsReposEntity().getVcsReposUrl();
//				vcsService.checkoutHead(setChkHeadVO(repository , masterworkPath , reposDto.getVcsMdlEntity().getMdlNm()));
//			}
//		}
	}

	private StatusVO setSVO(String pathDest,String moduleName){

		StatusVO vo = new StatusVO();
		vo.setPathDest(pathDest);
		vo.setObjectPath(moduleName);
		return vo;
	}
	/**
	 * ＨＥＡＤの資産をチェックアウトするための情報をVOにセットする<br>
	 * @param repository リポジトリ
	 * @param workDir 作業ディレクトリパス
	 * @param moduleName モジュール名
	 * @param logBusinessFlow ログハンドラ
	 * @return vo
	 */
//	private CheckOutVO setChkHeadVO(String repository , String workDir , String moduleName ){
//
//		CheckOutVO vo = new CheckOutVO();
//		vo.setRepository(repository);
//		vo.setPathDest(workDir);
//		vo.setModuleName(moduleName);
//		return vo;
//	}

	/**
	 * ロット資産を公開パスにチェックアウトする。
	 * @param vcsReposEntity リポジトリ情報
	 * @param reposMapByMdlNmKey リポジトリ情報
	 * @param lotDto ロット情報
	 * @param logBusinessFlow ログハンドラ
	 * @throws Exception
	 */
	private void checkoutBranchModules(
			IVcsReposEntity vcsReposEntity ,
			Map<String, IVcsReposDto> reposMapByMdlNmKey,
			ILotDto lotDto ,
			LogBusinessFlow logBusinessFlow ) throws Exception {
		logBusinessFlow.writeLog( TriLogMessage.LAM0002 ) ;

		ILotEntity lotEntity = lotDto.getLotEntity();
		String workPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity ).getCanonicalPath() ;
		String lotBranchTag = lotEntity.getLotBranchTag() ;

		String url = vcsReposEntity.getVcsReposUrl() ;

		Set<String> mdlNmSet = new HashSet<String>();
		for ( ILotMdlLnkEntity mdlLnkEntity: lotDto.getIncludeMdlEntities( true ) ) {
			mdlNmSet.add( mdlLnkEntity.getMdlNm() );
		}

		for ( IVcsReposDto moduleEntity : reposMapByMdlNmKey.values() ) {
			if ( ! vcsReposEntity.getVcsReposId().equals( moduleEntity.getVcsReposEntity().getVcsReposId() ) )
				continue;

			String moduleName = moduleEntity.getVcsMdlEntity().getMdlNm();

			if ( ! mdlNmSet.contains( moduleName ) ) {
				continue;
			}

			logBusinessFlow.writeLog( TriLogMessage.LAM0018, LinefeedCode.CRLF.getValue() , url + LinefeedCode.CRLF.getValue() , moduleName + LinefeedCode.CRLF.getValue() ) ;
			/** 作成したロットのチェックアウト **/
			vcsService.checkoutBranch(setChkBranchVO(url , workPath , moduleName , lotBranchTag));
		}
	}

	/**
	 * ブランチ作成処理を行うための情報をVOにセットする<br>
	 * 【対象はロット（ブランチ）】<br>
	 * ブランチ作成時のブランチタグを打つ。<br>
	 * @param repository リポジトリ
	 * @param pjtLotEntity ロット情報
	 * @param logBusinessFlow ログハンドラ
	 * @return vo
	 */
	private CommitVO setCommitVO(String repository , ILotEntity pjtLotEntity , LogBusinessFlow logBusinessFlow) {

		logBusinessFlow.writeLog( TriLogMessage.LAM0019, repository , pjtLotEntity.getLotBranchTag() ) ;

		CommitVO vo = new CommitVO();
		vo.setRepository( repository );
		vo.setLabelBranch( pjtLotEntity.getLotBranchTag() );
		vo.setCommitComment( pjtLotEntity.getLotNm() + "_" + pjtLotEntity.getLotLatestBlTag() );
		return vo;
	}

	/**
	 * VcsCommitInfoからメッセージを作成します。
	 * @param info
	 * @return
	 */
	private String editCommitInfo( VcsCommitInfo info ) {

		if( null != info.getErrorMessage() ) {
			return getContext().getMessage( TriLogMessage.LAM0030, info.getAuthor() , info.getDate().toString()
					, info.getErrorMessage() , String.valueOf(info.getNewRevision()) );
		}
		return getContext().getMessage( TriLogMessage.LAM0031, info.getAuthor() , info.getDate().toString()
				, String.valueOf(info.getNewRevision()));

	}

	/**
	 * チェックアウト処理を行うための情報をVOにセットする<br>
	 * 【対象はロット（ブランチ）】<br>
	 * @param repository リポジトリ
	 * @param workDir 作業ディレクトリ
	 * @param moduleName モジュール名
	 * @param branchTag ブランチタグ
	 * @return vo;
	 */
	private CheckOutVO setChkBranchVO(String repository , String workDir , String moduleName , String branchTag){

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository(repository);
		vo.setModuleName(moduleName);
		vo.setPathDest(workDir);
		vo.setLabelBranch(branchTag);
		return vo;
	}

	/**
	 * ディレクトリを作成する<br>
	 * @param path ディレクトリを作成するパス
	 */
	private void makeDirectory( String path ) {

		File file = new File( path ) ;
		if( file.exists() && file.isFile() ) {//同名のファイルがすでに存在する
			throw new TriSystemException( AmMessageId.AM004061F , path ) ;
		}
		file.mkdirs() ;

	}

	/**
	 * ログのヘッダ部分を出力する
	 * @param logBusinessFlow ログハンドラ
	 * @param info
	 * @param lotDto ロット情報
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logBusinessFlow ,
			IGeneralServiceBean info ,
			ILotDto lotDto ) throws Exception {

		ILotEntity lotEntity = lotDto.getLotEntity();
		StringBuilder stb = new StringBuilder() ;
		stb = logBusinessFlow.makeUserAndLotLabel(stb, info, lotEntity.getLotNm() , lotEntity.getLotId() );
		//ロットに含まれるモジュール
		for( ILotMdlLnkEntity moduleEntity : lotDto.getIncludeMdlEntities( true ) ) {
			stb.append( getContext().getMessage( TriLogMessage.LAM0013 , moduleEntity.getMdlNm() ) + SEP ) ;
		}

		logBusinessFlow.writeLog( stb.toString() ) ;
	}

	/**
	 * モジュール名とリポジトリリストのDTOを取得する。
	 * @param lotDto ロットエンティティ
	 * @return モジュール名とリポジトリのDTOマップ
	 */
	private Map<String, IVcsReposDto> getVcsReposDtoMap( ILotDto lotDto ) {
		Map<String, IVcsReposDto> map = new LinkedHashMap<String, IVcsReposDto>();

		for ( IVcsReposDto reposDto: this.support.findVcsReposDtoFromLotMdlLnkEntities( lotDto.getLotMdlLnkEntities() ) ) {
			map.put(reposDto.getVcsMdlEntity().getMdlNm(), reposDto);
		}

		return map;
	}

}
