package jp.co.blueship.tri.am.domain.lot.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * ロット情報のステータスをエラーに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeLotInfoError extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		LotParamInfo lotParamInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;
		if ( TriStringUtils.isEmpty( lotParamInfo ) ) {
			throw new TriSystemException( AmMessageId.AM005026S ) ;
		}

		lotParamInfo.setProcessResult( false ) ;//エラーフラグを立てる

		return serviceDto;
	}
}
