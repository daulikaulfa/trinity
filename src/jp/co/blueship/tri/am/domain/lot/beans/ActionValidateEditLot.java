package jp.co.blueship.tri.am.domain.lot.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * ロット変更時の事前チェック処理を行います。
 *
 */
public class ActionValidateEditLot extends ActionPojoAbstract<IGeneralServiceBean> {

	private FlowChaLibLotEditSupport support = null;
	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {

			List<Object> paramList = serviceDto.getParamList();
			LotParamInfo lotParamInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;

			ILotDto lotDto = this.support.findLotDto(
					lotParamInfo.getLotDto().getLotEntity().getLotId(),
					AmTables.AM_LOT_GRP_LNK,
					AmTables.AM_LOT_MAIL_GRP_LNK);

			AmLibraryAddonUtils.existGroup( lotDto, this.support.getUmFinderSupport().getGrpDao() );
			AmLibraryAddonUtils.existSpecifiedGroup( lotDto, this.support.getUmFinderSupport().getGrpDao() );

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005077S , e ) ;
		}

		return serviceDto;
	}
}
