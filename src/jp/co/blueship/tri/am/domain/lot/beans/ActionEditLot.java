package jp.co.blueship.tri.am.domain.lot.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * ロット変更処理を行います。
 *
 */
public class ActionEditLot extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;

	private FlowChaLibLotEditSupport support = null;
	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		LogBusinessFlow logBusinessFlow = new LogBusinessFlow() ;
		String flowActionTitle = null ;

		boolean isSuccess = false;

		try {
			IGeneralServiceBean paramBean = serviceDto.getServiceBean();
			LotParamInfo lotParamInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList ) ;
			ILotEntity pjtLotEntity = lotParamInfo.getLotDto().getLotEntity() ;

			//ログの初期処理
			logBusinessFlow.makeLogPath( paramBean.getFlowAction() , pjtLotEntity ) ;
			//ログの開始ラベル出力
			flowActionTitle = LogBusinessFlow.makeFlowActionTitle( paramBean.getFlowAction() ) ;
			logBusinessFlow.writeLogWithDateBegin( flowActionTitle ) ;
			//ログのヘッダ部出力
			this.outLogHeader( lotParamInfo , logBusinessFlow , paramBean , pjtLotEntity ) ;

			//ロットに関連するディレクトリの作成
			this.editLotProcMakeDirectory( lotParamInfo, lotParamInfo.getLotDto() , logBusinessFlow ) ;

			this.updateLotInfo( lotParamInfo );

			isSuccess = true;

		} catch( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;

			throw be ;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005076S , e );
		} finally {
			if ( null != flowActionTitle ) {
				logBusinessFlow.writeLogWithDateEnd( flowActionTitle ) ;

				if ( isSuccess ) {
					logBusinessFlow.deleteLog( AmDesignEntryKeyByLogs.deleteLogAtSuccessLotModify );
				}
			}

		}

		return serviceDto;
	}

	/**
	 * ロット情報を更新する。
	 * <br>
	 * @param paramInfo
	 */
	private void updateLotInfo( LotParamInfo paramInfo ) {
		ILotDto dto = paramInfo.getLotDto();
		String lotId = dto.getLotEntity().getLotId();

		support.getLotGrpLnkDao().delete( new LotGrpLnkCondition().setLotId( lotId ).getCondition() );
		support.getLotMailGrpLnkDao().delete( new LotMailGrpLnkCondition().setLotId( lotId ).getCondition() );
		support.getLotRelEnvLnkDao().delete( new LotRelEnvLnkCondition().setLotId( lotId ).getCondition() );

		support.getLotDao().update( dto.getLotEntity() );

		support.getLotGrpLnkDao().insert( dto.getLotGrpLnkEntities() );
		support.getLotMailGrpLnkDao().insert( dto.getLotMailGrpLnkEntities() );
		support.getLotRelEnvLnkDao().insert( dto.getLotRelEnvLnkEntities() );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Edit.getStatusId());

			this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , dto);
		}
	}

	/**
	 * ロットに関連する各種ディレクトリを作成する<br>
	 * @param lotParamInfo
	 * @param pjtLotEntity
	 * @param logBusinessFlow
	 * @throws Exception
	 */
	private void editLotProcMakeDirectory(
			LotParamInfo lotParamInfo, ILotDto lotDto , LogBusinessFlow logBusinessFlow ) throws Exception {

		logBusinessFlow.writeLogWithDateBegin( " editLotProcMakeDirectory "  ) ;

		try {
			/** グループ名フォルダまで作成する */
			AmBusinessFileUtils.createInOutBoxDirectory( lotParamInfo, lotDto );

		} finally {
			logBusinessFlow.writeLogWithDateEnd( " editLotProcMakeDirectory "  ) ;
		}

	}

	/**
	 * ログのヘッダ部分を出力する
	 * @param logBusinessFlow
	 * @param businessCommonInfo
	 * @param pjtLotEntity ロット情報
	 * @throws Exception
	 */
	private void outLogHeader(
			LotParamInfo lotParamInfo ,
			LogBusinessFlow logBusinessFlow ,
			IGeneralServiceBean businessCommonInfo ,
			ILotEntity pjtLotEntity ) throws Exception {

		StringBuilder stb = new StringBuilder() ;
		stb = logBusinessFlow.makeUserAndLotLabel(stb, businessCommonInfo , pjtLotEntity.getLotNm(), pjtLotEntity.getLotId() );
		//ロットに追加されるモジュール
		List<ModuleViewBean> moduleViewBeanList = lotParamInfo.getTargetModuleList() ;
		for ( ModuleViewBean moduleViewBean : moduleViewBeanList ) {
			stb.append( getContext().getMessage( TriLogMessage.LAM0025 , moduleViewBean.getModuleName() ) + SEP ) ;
		}

		logBusinessFlow.writeLog( stb.toString() ) ;
	}
}
