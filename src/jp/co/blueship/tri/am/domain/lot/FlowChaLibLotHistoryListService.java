package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotHistoryListServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * 変更管理・ロット履歴一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibLotHistoryListService implements IDomain<FlowChaLibLotHistoryListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotHistoryListServiceBean> execute( IServiceDto<FlowChaLibLotHistoryListServiceBean> serviceDto ) {

		FlowChaLibLotHistoryListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			LotCondition condition = AmDBSearchConditionAddonUtils.getNonActivePjtLotConditionWithoutHead();

			List<String> disableLinkLotNumbers	= new ArrayList<String>();
			boolean isSearch =
				this.support.setAccessableLotNumbers( condition, paramBean, false, disableLinkLotNumbers );


			IEntityLimit<ILotEntity> entityLimit = null;
			int selectPageNo = 1;

			// アクセス可能なロットが１件もない場合は検索に行かない
			// （ロット番号が指定できないので、全部取ってきてしまうので）
			if ( isSearch ) {
				ISqlSort sort = AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibLotHistoryList();

				selectPageNo =
					( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

				entityLimit = this.support.getLotDao().find( condition.getCondition(), sort, selectPageNo,
									sheet.intValue(
										AmDesignEntryKeyByChangec.maxPageNumberByLotHistoryList ) );
			} else {

				entityLimit = new EntityLimit<ILotEntity>();
				entityLimit.setEntities( FluentList.from(new ILotEntity[0]).asList() );
			}
			List<ILotDto> lotDto = this.support.findLotDto( entityLimit.getEntities() );

			// 履歴だからいらんでしょ
//			if ( StringAddonUtil.isNothing( entityLimit.getEntity() )) {
//				paramBean.setInfoMessage(MessageId.MESCHA2002);
//			}

			List<LotViewBean> lotViewBeanList =
				this.support.getLotViewBean( lotDto, disableLinkLotNumbers );

			paramBean.setLotViewBeanList	( lotViewBeanList );
			paramBean.setPageInfoView		(
					AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), entityLimit.getLimit() ));
			paramBean.setSelectPageNo		( selectPageNo );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005104S,e);
		}
	}
}
