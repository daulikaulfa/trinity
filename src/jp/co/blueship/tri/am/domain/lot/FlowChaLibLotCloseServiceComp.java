package jp.co.blueship.tri.am.domain.lot;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCloseServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * 変更管理起票・ロットクローズ完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibLotCloseServiceComp implements IDomain<FlowChaLibLotCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotCloseServiceBean> execute( IServiceDto<FlowChaLibLotCloseServiceBean> serviceDto ) {

		FlowChaLibLotCloseServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_LOT_CLOSE ) &&
					!forwordID.equals( ChaLibScreenID.COMP_LOT_CLOSE ) ){
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_LOT_CLOSE )) {
				return serviceDto;
			}
			if ( ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo( selectedLotNo );

			ILotEntity entity		= this.support.findLotEntity( selectedLotNo );

			entity.setCloseCmt			( lotEditInputBean.getCloseComment() );
			entity.setCloseTimestamp	( TriDateUtils.getSystemTimestamp() );
			entity.setCloseUserNm		( paramBean.getUserName() );
			entity.setCloseUserId		( paramBean.getUserId() );
			entity.setStsId				( AmLotStatusId.LotClosed.getStatusId() );
			entity.setUpdUserNm			( paramBean.getUserName() );
			entity.setUpdUserId			( paramBean.getUserId() );

			this.support.getLotDao().update( entity );

			ILotDto lotDto = this.support.findLotDto(selectedLotNo);

			if ( DesignSheetUtils.isRecord() ) {
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts(UmActStatusId.Close.getStatusId());

				this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , lotDto);
			}

			LotDetailViewBean viewBean = paramBean.getLotDetailViewBean();
			viewBean.setLotNo( selectedLotNo );
			viewBean.setLotName(entity.getLotNm());
			paramBean.setLotDetailViewBean( viewBean );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005099S,e);
		}
	}
}
