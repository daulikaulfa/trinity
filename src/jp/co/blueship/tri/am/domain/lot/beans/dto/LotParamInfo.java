package jp.co.blueship.tri.am.domain.lot.beans.dto;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class LotParamInfo extends CommonParamInfo implements ILotParamInfo {

	private static final long serialVersionUID = 2L;

	private String user = null;

	private boolean processResult = true;

	private ILotDto lotDto = null ;

	/** ＨＥＡＤ側バージョンタグ */
	private String headVersionTag = null ;

	/** ブランチ作成前のリビジョン番号 */
	private Map<String, String> baseRevisionNo = new HashMap<String, String>() ;
	/** ブランチ作成時のリビジョン番号 */
	private Map<String, String> branchRevisionNo = new HashMap<String, String>() ;

	/** チェックアウト対象とするモジュールのリスト */
	private List<ModuleViewBean> targetModuleList = null ;

	/** 選択可能なグループ */
	private List<GroupViewBean> groupViewBeanList = null ;

	/** 選択可能なメール送信先 特定グループ **/
	private List<GroupViewBean> specifiedGroupList = null ;

	public List<ModuleViewBean> getTargetModuleList() {
		return targetModuleList;
	}
	public void setTargetModuleList(List<ModuleViewBean> targetModuleList) {
		this.targetModuleList = targetModuleList;
	}
	public String getUser() {
		return user;
	}
	public void setUser( String user ) {
		this.user = user;
	}

	public boolean getProcessResult() {
		return processResult;
	}
	public void setProcessResult( boolean processResult ) {
		this.processResult = processResult;
	}
	public ILotDto getLotDto() {
		return lotDto;
	}
	public void setLotDto(ILotDto lotDto) {
		this.lotDto = lotDto;
	}
	public String getHeadVersionTag() {
		return headVersionTag;
	}
	public void setHeadVersionTag(String headVersionTag) {
		this.headVersionTag = headVersionTag;
	}

	public Map<String, String> getBranchRevisionNo() {
		return branchRevisionNo;
	}
	public void setBranchRevisionNo(Map<String, String> branchRevisionNo) {
		this.branchRevisionNo = branchRevisionNo;
	}
	public Map<String, String> getBaseRevisionNo() {
		return baseRevisionNo;
	}
	public void setBaseRevisionNo(Map<String, String> baseRevisionNo) {
		this.baseRevisionNo = baseRevisionNo;
	}

	public List<GroupViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}

	public List<GroupViewBean> getSpecifiedGroupList() {
		return specifiedGroupList;
	}
	public void setSpecifiedGroupList(List<GroupViewBean> specifiedGroupList) {
		this.specifiedGroupList = specifiedGroupList;
	}
}
