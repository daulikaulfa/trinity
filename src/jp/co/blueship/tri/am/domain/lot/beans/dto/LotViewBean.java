package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.domainx.lot.beans.dto.IDashboardLotViewBean;

/**
 * 変更管理・ロット一覧用
 *
 * <br>
 *
 * @version V3L10.01
 * @author trinity V3
 *
 * @version V3L11.01
 * @author Yusna Marlina
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Tang vu
 */
public class LotViewBean implements IDashboardLotViewBean, Serializable {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String lotId = null;
	/** サーバ番号 */
	private String serverNo = null;
	/** ロット名 */
	private String lotName = null;
	/** 完了変更件数 */
	private int compPjtCount = 0;
	/** 進行中変更件数 */
	private int progressPjtCount = 0;
	/** ステータス */
	private String lotStatus = null;
	/** ロット管理・作成／変更エラーの判定 エラーならtrue */
	private boolean isError;
	/** ロット管理・作成／変更中の判定 変更中ならtrue */
	private boolean isProcessing;
	/** 前回コンフリクトチェック実行ステータス */
	private String conflictCheckStatus = null ;
	/** 前回コンフリクトチェック実行ステータスID */
	private String conflictCheckStatusId = null ;
	/** 前回コンフリクトチェック開始日時 */
	private String conflictCheckStartDate = null ;
	/** 前回コンフリクトチェック終了日時 */
	private String conflictCheckEndDate = null ;
	/** 前回マージ実行ステータス */
	private String mergeStatus = null ;
	/** 前回マージ実行ステータスID */
	private String mergeStatusId = null ;
	/** 前回マージ開始日時 */
	private String mergeStartDate = null ;
	/** 前回マージ終了日時 */
	private String mergeEndDate = null ;
	/**
	 * このロットの閲覧・編集リンクを有効にするかどうか
	 */
	private boolean isEditLinkEnabled = false;
	/**
	 * このロットの閲覧リンクを有効にするかどうか
	 */
	private boolean isViewLinkEnabled = false;

	/** マージ結果に警告を含むかどうか */
	private boolean isMergeWarning = false ;

	private String lotIconPath = null;

	public String getLotId() {
		return lotId;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotSubject() {
		return lotName;
	}

	public int getCompPjtCount() {
		return compPjtCount;
	}
	public void setCompPjtCount(int compPjtCount) {
		this.compPjtCount = compPjtCount;
	}

	public int getProgressPjtCount() {
		return progressPjtCount;
	}
	public void setProgressPjtCount(int progressPjtCount) {
		this.progressPjtCount = progressPjtCount;
	}

	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}

	public boolean getError() {
		return isError;
	}
	public void isError(boolean isError) {
		this.isError = isError;
	}
	public boolean getProcessing() {
		return isProcessing;
	}
	public void isProcessing(boolean isProcessing) {
		this.isProcessing = isProcessing;
	}

	public String getConflictCheckEndDate() {
		return conflictCheckEndDate;
	}
	public void setConflictCheckEndDate(String conflictCheckEndDate) {
		this.conflictCheckEndDate = conflictCheckEndDate;
	}
	public String getConflictCheckStatusId() {
	    return conflictCheckStatusId;
	}
	public void setConflictCheckStatusId(String conflictCheckStatusId) {
	    this.conflictCheckStatusId = conflictCheckStatusId;
	}
	public String getConflictCheckStartDate() {
		return conflictCheckStartDate;
	}
	public void setConflictCheckStartDate(String conflictCheckStartDate) {
		this.conflictCheckStartDate = conflictCheckStartDate;
	}
	public String getConflictCheckStatus() {
		return conflictCheckStatus;
	}
	public void setConflictCheckStatus(String conflictCheckStatus) {
		this.conflictCheckStatus = conflictCheckStatus;
	}
	public String getMergeEndDate() {
		return mergeEndDate;
	}
	public void setMergeEndDate(String mergeEndDate) {
		this.mergeEndDate = mergeEndDate;
	}
	public String getMergeStatusId() {
	    return mergeStatusId;
	}
	public void setMergeStatusId(String mergeStatusId) {
	    this.mergeStatusId = mergeStatusId;
	}
	public String getMergeStartDate() {
		return mergeStartDate;
	}
	public void setMergeStartDate(String mergeStartDate) {
		this.mergeStartDate = mergeStartDate;
	}
	public String getMergeStatus() {
		return mergeStatus;
	}
	public void setMergeStatus(String mergeStatus) {
		this.mergeStatus = mergeStatus;
	}
	public boolean isEditLinkEnabled() {
		return isEditLinkEnabled;
	}
	public void setEditLinkEnabled( boolean isEditLinkEnabled ) {
		this.isEditLinkEnabled = isEditLinkEnabled;
	}
	public boolean isViewLinkEnabled() {
		return isViewLinkEnabled;
	}
	public void setViewLinkEnabled( boolean isViewLinkEnabled ) {
		this.isViewLinkEnabled = isViewLinkEnabled;
	}
	public boolean isMergeWarning(){
		return isMergeWarning;
	}
	public void setMergeWarning(boolean isMergeWarning){
		this.isMergeWarning = isMergeWarning;
	}
	public String getLotIconPath() {
		return lotIconPath;
	}
	public void setLotIconPath(String lotIconPath) {
		this.lotIconPath = lotIconPath;
	}

}
