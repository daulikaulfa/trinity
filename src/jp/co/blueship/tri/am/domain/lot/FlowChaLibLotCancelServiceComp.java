package jp.co.blueship.tri.am.domain.lot;

import java.io.File;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkCondition;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * 変更管理起票・ロット取消完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibLotCancelServiceComp implements IDomain<FlowChaLibLotCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotCancelServiceBean> execute( IServiceDto<FlowChaLibLotCancelServiceBean> serviceDto ) {

		FlowChaLibLotCancelServiceBean paramBean = null;

		boolean isSuccess = false;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_LOT_CANCEL ) &&
					!forwordID.equals( ChaLibScreenID.COMP_LOT_CANCEL ) ){
				return serviceDto;
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_LOT_CANCEL )) {
				//遷移元フローアクション名を画面に戻す
				paramBean.setPreviousFlowAction( paramBean.getPreviousFlowAction() ) ;

				if (  ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					return serviceDto;
				}

				String selectedLotNo = paramBean.getSelectedLotNo();

				ILotDto lotDto = this.support.findLotDto( selectedLotNo );

				LotDetailViewBean viewBean = paramBean.getLotDetailViewBean();
				viewBean.setLotNo( selectedLotNo );
				paramBean.setLotDetailViewBean( viewBean );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				//レコード更新
				this.updatePjtLotEntity( paramBean ) ;

				isSuccess = true;
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005097S,e);
		} finally {
			if ( isSuccess ) {
				this.deleteDirectory( paramBean );
			}
		}
	}

	/**
	 * ロット情報レコードを「ロット取消」状態に更新する
	 * @param paramBean
	 */
	private void updatePjtLotEntity( FlowChaLibLotCancelServiceBean paramBean ) {

		LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

		String selectedLotNo = paramBean.getSelectedLotNo();
		AmItemChkUtils.checkLotNo( selectedLotNo );

		// am_lot 論理削除
		ILotEntity entity		= this.support.findLotEntity( selectedLotNo );

		entity.setDelCmt			( lotEditInputBean.getDelComment() );
		entity.setDelTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setDelUserNm			( paramBean.getUserName() );
		entity.setDelUserId			( paramBean.getUserId() );
		entity.setDelStsId			( StatusFlg.on );
		entity.setStsId				( AmLotStatusId.LotRemoved.getStatusId() );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );

		this.support.getLotDao().update( entity );

		// am_lot_mdl_lnk 論理削除
		LotMdlLnkCondition mdlCondition = new LotMdlLnkCondition();
		mdlCondition.setLotId( entity.getLotId() );
		ILotMdlLnkEntity mdlEntity = new LotMdlLnkEntity();
		mdlEntity.setDelStsId( StatusFlg.on );
		support.getLotMdlLnkDao().update( mdlCondition.getCondition(), mdlEntity );

		// am_lot_rel_env_lnk 論理削除
		LotRelEnvLnkCondition relCondition = new LotRelEnvLnkCondition();
		relCondition.setLotId( entity.getLotId() );
		ILotRelEnvLnkEntity relEntity = new LotRelEnvLnkEntity();
		relEntity.setDelStsId( StatusFlg.on );
		support.getLotRelEnvLnkDao().update( relCondition.getCondition(), relEntity );

		// am_lot_grp_lnk 論理削除
		LotGrpLnkCondition grpCondition = new LotGrpLnkCondition();
		grpCondition.setLotId( entity.getLotId() );
		ILotGrpLnkEntity grpEntity = new LotGrpLnkEntity();
		grpEntity.setDelStsId( StatusFlg.on );
		support.getLotGrpLnkDao().update( grpCondition.getCondition(), grpEntity );

		// am_lot_mail_grp_lnk 論理削除
		LotMailGrpLnkCondition mailGrpCondition = new LotMailGrpLnkCondition();
		mailGrpCondition.setLotId( entity.getLotId() );
		ILotMailGrpLnkEntity mailGrpEntity = new LotMailGrpLnkEntity();
		mailGrpEntity.setDelStsId( StatusFlg.on );
		support.getLotMailGrpLnkDao().update( mailGrpCondition.getCondition(), mailGrpEntity );

		if ( DesignSheetUtils.isRecord() ) {
			ILotDto lotDto = this.support.findLotDto(entity);

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Remove.getStatusId());

			this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , lotDto);
		}
	}

	/**
	 * ロット情報に関連する「公開フォルダ」「非公開フォルダ」を削除する。
	 * <br>トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
	 *
	 * @param paramBean
	 */
	private void deleteDirectory( FlowChaLibLotCancelServiceBean paramBean ) {
		try {
			String selectedLotNo = paramBean.getSelectedLotNo();

			ILotEntity lotEntity = this.support.findLotEntity(selectedLotNo, null);
			File inOutBoxPath	= AmDesignBusinessRuleUtils.getInOutBoxPath( lotEntity );
			File historyPath	= AmDesignBusinessRuleUtils.getHistoryPath( lotEntity );
			File masterWorkPath	= AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );
			File privatePath	= AmDesignBusinessRuleUtils.getPrivatePath( lotEntity );

			if ( null != inOutBoxPath ) {
				TriFileUtils.delete( inOutBoxPath );
			}

			if ( null != historyPath ) {
				TriFileUtils.delete( historyPath );
			}

			if ( null != masterWorkPath ) {
				TriFileUtils.delete( masterWorkPath );
			}

			if ( null != privatePath ) {
				TriFileUtils.delete( privatePath );
			}
		} catch( Exception e ) {
			//トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
			LogHandler.fatal( log , e ) ;
		}
	}
}
