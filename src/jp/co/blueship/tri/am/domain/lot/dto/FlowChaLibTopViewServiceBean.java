package jp.co.blueship.tri.am.domain.lot.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * トップ系画面へのフロー
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibTopViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * トップメニューリスト
	 */
	private List<TopMenuViewBean> topMenuViewBeanList = new ArrayList<TopMenuViewBean>();
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public TopMenuViewBean newTopMenuViewBean() {
		TopMenuViewBean bean = new TopMenuViewBean();
		
		return bean;
	}
	
	/**
	 * @return topMenuViewBeanList
	 */
	public List<TopMenuViewBean> getTopMenuViewBeanList() {
		return topMenuViewBeanList;
	}

	/**
	 * @param topMenuViewBeanList 設定する topMenuViewBeanList
	 */
	public void setTopMenuViewBeanList(List<TopMenuViewBean> topMenuViewBeanList) {
		this.topMenuViewBeanList = topMenuViewBeanList;
	}

	
	public class TopMenuViewBean implements Serializable {
		
		private static final long serialVersionUID = 1L;
		
		/**
		 * 変更管理番号
		 */
		private String pjtNo;
		/**
		 * 変更要因番号
		 */
		private String changeCauseNo;
		/**
		 * 申請情報番号
		 */
		private String applyNo;
		/**
		 * 申請件名
		 */
		private String applySubject;
		/**
		 * 申請ユーザ
		 */
		private String applyUser;
		/**
		 * 申請ユーザＩＤ
		 */
		private String applyUserId;
		/**
		 * 申請理由
		 */
		private String applyReason;
		/**
		 * 申請日時
		 */
		private String applyDate;
		/**
		 * ステータス
		 */
		private String status;
		/**
		 * @return applyDate
		 */
		public String getApplyDate() {
			return applyDate;
		}
		/**
		 * @param applyDate 設定する applyDate
		 */
		public void setApplyDate(String applyDate) {
			this.applyDate = applyDate;
		}
		/**
		 * @return applyNo
		 */
		public String getApplyNo() {
			return applyNo;
		}
		/**
		 * @param applyNo 設定する applyNo
		 */
		public void setApplyNo(String applyNo) {
			this.applyNo = applyNo;
		}
		/**
		 * @return applySubject
		 */
		public String getApplySubject() {
			return applySubject;
		}
		/**
		 * @param applySubject 設定する applySubject
		 */
		public void setApplySubject(String applySubject) {
			this.applySubject = applySubject;
		}
		/**
		 * @return pjtNo
		 */
		public String getPjtNo() {
			return pjtNo;
		}
		/**
		 * @param pjtNo 設定する pjtNo
		 */
		public void setPjtNo(String pjtNo) {
			this.pjtNo = pjtNo;
		}
		/**
		 * @return status
		 */
		public String getStatus() {
			return status;
		}
		/**
		 * @param status 設定する status
		 */
		public void setStatus(String status) {
			this.status = status;
		}
		/**
		 * @return lendApplyUser
		 */
		public String getApplyUser() {
			return applyUser;
		}
		/**
		 * @param lendApplyUser 設定する lendApplyUser
		 */
		public void setApplyUser(String applyUser) {
			this.applyUser = applyUser;
		}
		/**
		 * @return applyReason
		 */
		public String getApplyReason() {
			return applyReason;
		}
		/**
		 * @param applyReason 設定する applyReason
		 */
		public void setApplyReason(String applyReason) {
			this.applyReason = applyReason;
		}
		public String getChangeCauseNo() {
			return changeCauseNo;
		}
		public void setChangeCauseNo(String changeCauseNo) {
			this.changeCauseNo = changeCauseNo;
		}
		public String getApplyUserId() {
			return applyUserId;
		}
		public void setApplyUserId(String applyUserId) {
			this.applyUserId = applyUserId;
		}
		
	}

}
