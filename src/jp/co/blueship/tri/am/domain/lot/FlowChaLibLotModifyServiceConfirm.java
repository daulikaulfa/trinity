package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;


/**
 * 変更管理・ロット変更画面の表示情報設定Class<br>
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotModifyServiceConfirm implements IDomain<FlowChaLibLotModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotModifyServiceBean> execute( IServiceDto<FlowChaLibLotModifyServiceBean> serviceDto ) {

		FlowChaLibLotModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LOT_MODIFY_CONFIRM ) &&
					!forward.equals( ChaLibScreenID.LOT_MODIFY_CONFIRM )) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

			try {
				if ( ChaLibScreenID.LOT_MODIFY_CONFIRM.equals( referer )) {

					if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
						AmItemChkUtils.checkLotModifyCheck( paramBean );

						ILotDto lotDto = this.support.findLotDto( paramBean.getLotNo() );
						if (lotDto.getLotEntity().isUseMerge().parseBoolean() == false && lotEditInputBean.isEditUseMerge() == true) {
							throw new BusinessException(AmMessageId.AM001128E);
						}
					}
				}

				if( ChaLibScreenID.LOT_MODIFY_CONFIRM.equals( forward ) ) {//ロット変更確認画面
					if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

						//警告 有無のチェック
						this.checkWarningError( paramBean ) ;
					}
				}

			} finally {

				paramBean.setLotEditInputBean( lotEditInputBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e );
			throw new TriSystemException( AmMessageId.AM005105S , e );
		}
	}

	/**
	 *
	 * @param paramBean
	 */

	private void checkWarningError( FlowChaLibLotModifyServiceBean paramBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		List<RelEnvViewBean> selectedViewBeanList = paramBean.getLotEditInputBean().getSelectedRelEnvViewBeanList();

		if ( TriStringUtils.isEmpty( selectedViewBeanList ) ) {
			//全リリース環境選択時は、チェックする必要なし
			return;
		}

		ILotDto lotDto = this.support.findLotDto( paramBean.getLotNo() );

		//既存ロットのリリース環境を抽出
		IBldEnvEntity[] envEntityArray = new IBldEnvEntity[0];
		{
			List<ILotRelEnvLnkEntity> envArray = lotDto.getLotRelEnvLnkEntities();

			if ( TriStringUtils.isEmpty(envArray) ) {
				envEntityArray = this.support.getRelEnvEntityArray();
			} else {
				Map<String, ILotRelEnvLnkEntity> envMap = this.support.convertToLotEnvMap( envArray );
				envEntityArray = this.support.getRelEnvEntityArray( envMap.keySet().toArray(new String[0]) );
			}
		}

		Set<String> selectedEnvNo = this.support.convertToEnvNoSet( selectedViewBeanList );

		for ( IBldEnvEntity envEntity: envEntityArray ) {
			if ( selectedEnvNo.contains( envEntity.getBldEnvId() ) ) {
				continue;
			}

			//選択が外されたリリース申請があれば警告を表示する
			if ( this.existsRelApplyEntity(envEntity.getBldEnvId(), lotDto.getLotEntity().getLotId()) ) {
				messageList.add( AmMessageId.AM001032E );
				messageArgsList.add( new String[]{ envEntity.getBldEnvNm(), envEntity.getBldEnvId() } );
			}

			//選択が外されたリリースがあれば警告を表示する
			if ( this.existsRelEntity(envEntity.getBldEnvId(), lotDto.getLotEntity().getLotId()) ) {
				messageList.add( AmMessageId.AM001033E );
				messageArgsList.add( new String[]{ envEntity.getBldEnvNm(), envEntity.getBldEnvId() } );
			}
		}

		if( null == paramBean.getInfoMessageIdList() ) {
			paramBean.setInfoMessageIdList( messageList );
		} else {
			paramBean.getInfoMessageIdList().addAll( messageList );
		}
		if( null == paramBean.getInfoMessageArgsList() ) {
			paramBean.setInfoMessageArgsList( messageArgsList );
		} else {
			paramBean.getInfoMessageArgsList().addAll( messageArgsList );
		}

		WarningCheckBean warningCheck = new WarningCheckBean();
		warningCheck.setExistWarning	( ( 0 != messageList.size() ) ? true : false );

		//メッセージ生成
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		List<String> warningCommentList = warningCheck.getCommentList() ;
		//[0]
		warningCommentList.add( ac.getMessage( paramBean.getLanguage(), AmMessageId.AM001034E ) ) ;
		//[1]
		warningCommentList.add( ac.getMessage( paramBean.getLanguage(), AmMessageId.AM001035E ) ) ;

		paramBean.setWarningCheckError( warningCheck );
	}

	/**
	 * リリース環境がリリース申請で利用されているかどうか判定する
	 * <br>削除されていないものを対象とする
	 *
	 * @param envNo 対象の環境番号
	 * @param lotId 対象のロット番号
	 * @return 利用されている場合true。それ以外はfalseを戻す
	 */
	private final boolean existsRelApplyEntity( String envNo, String lotId ) {
		RaCondition condition = new RaCondition();
		condition.setBldEnvId( envNo );
		condition.setLotId( lotId );

		int count = this.support.getRmFinderSupport().getRaDao().count(condition.getCondition());

		return (0 < count)? true: false;
	}

	/**
	 * リリース環境がリリースで利用されているかどうか判定する
	 * <br>削除されていないものを対象とする
	 *
	 * @param envNo 対象の環境番号
	 * @param lotId 対象のロット番号
	 * @return 利用されている場合true。それ以外はfalseを戻す
	 */
	private final boolean existsRelEntity( String envNo, String lotId ) {
		RpCondition condition = new RpCondition();
		condition.setBldEnvId( envNo );
		condition.setLotId( lotId );

		int count = this.support.getRmFinderSupport().getRpDao().count(condition.getCondition());

		return (0 < count)? true: false;
	}

}
