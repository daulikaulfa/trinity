package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

/**
 * アプリケーション層で、共通に使用する承認情報を格納するインタフェースです。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public interface ILotParamInfo extends ICommonParamInfo, Serializable {

	/**
	 * 操作ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUser();
	/**
	 * 操作ユーザを設定します。
	 * @param value 操作ユーザ
	 */
	public void setUser( String user );
	/**
	 * 処理の成功、失敗のフラグを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean getProcessResult();
	/**
	 * 処理の成功、失敗のフラグを設定します。
	 * @param value 処理の成功、失敗のフラグ
	 */
	public void setProcessResult( boolean processResult );

	/**
	 * ロットDTOを取得します。
	 * @return 取得した値を戻します。
	 */
	public ILotDto getLotDto();
	/**
	 * ロットDTOを設定します。
	 * @param lotDto ロットDTO
	 */
	public void setLotDto( ILotDto lotDto );
}