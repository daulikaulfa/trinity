package jp.co.blueship.tri.am.domain.lot.dto;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理・ロット作成用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLotEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	private String lotId = null ;
	private LotEditInputV3Bean lotEditInputBean = null ;
	private boolean selectableAllowAssetDuplication = false;
	private String selectableAllowAssetDuplicationName = null;

	public LotEditInputV3Bean getLotEditInputBean() {
		return lotEditInputBean;
	}

	public void setLotEditInputBean(LotEditInputV3Bean lotEditInputBean) {
		this.lotEditInputBean = lotEditInputBean;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public boolean isSelectableAllowAssetDuplication() {
		return selectableAllowAssetDuplication;
	}
	public void setSelectableAllowAssetDuplication( boolean value ) {
		this.selectableAllowAssetDuplication = value;
	}

	public String getSelectableAllowAssetDuplicationName() {
		return selectableAllowAssetDuplicationName;
	}

	public void setSelectableAllowAssetDuplicationName(
			String selectableAllowAssetDuplicationName) {
		this.selectableAllowAssetDuplicationName = selectableAllowAssetDuplicationName;
	}
}