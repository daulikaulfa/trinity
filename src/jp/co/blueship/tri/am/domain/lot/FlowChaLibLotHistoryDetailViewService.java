package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * 変更管理・ロット履歴詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibLotHistoryDetailViewService implements IDomain<FlowChaLibLotHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotHistoryDetailViewServiceBean> execute( IServiceDto<FlowChaLibLotHistoryDetailViewServiceBean> serviceDto ) {

		FlowChaLibLotHistoryDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo( selectedLotNo );

			// LotDto設定
			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			List<IMessageId> messageList = new ArrayList<IMessageId>();
			List<String[]> messageArgsList = new ArrayList<String[]>();

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroupForV3(
					lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()),
					messageList,
					messageArgsList
					);

			AmLibraryAddonUtils.checkSpecifiedGroup(
					lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					messageList,
					messageArgsList
					);

			if ( 0 < messageList.size() ) {
				// 業務エラーメッセージの設定
				paramBean.setInfoMessageIdList( messageList );
				paramBean.setInfoMessageArgsList( messageArgsList );
			}

			// Scmモード
			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
			paramBean.setScmType( scmType.value() );


			paramBean.setLotDetailViewBean( this.support.getLotDetailViewBean( paramBean, lotDto ));

			setServiceBeanRelUnitResult	( selectedLotNo, paramBean );

//			ロット取消操作が有効かチェック
			if( AmLotStatusIdForExecData.LotEditError.equals( lotDto.getLotEntity().getProcStsId() ) ) {//「ロット作成エラー」のみ、「ロット取消」可とする
				paramBean.setEnableLotCancel( true ) ;
			}

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			setServiceBeanPjtEntity		( selectedLotNo, selectPageNo, paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005103S , e);
		}
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 * @param selectedLotNo ロット番号
	 * @param paramBean FlowChaLibLotHistoryDetailViewServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(
			String selectedLotNo, FlowChaLibLotHistoryDetailViewServiceBean paramBean ) {

		IJdbcCondition successCountCondition =
			AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition( selectedLotNo );

		paramBean.setSucceededRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( successCountCondition.getCondition() ) );


		IJdbcCondition failureCountCondition =
			AmDBSearchConditionAddonUtils.getFailedRelUnitCondition( selectedLotNo );

		paramBean.setFailedRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( failureCountCondition.getCondition() ) );
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 * @param lotId ロット番号
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibLotHistoryDetailViewServiceBeanオブジェクト
	 */
	private void setServiceBeanPjtEntity(
			String lotId, int selectPageNo, FlowChaLibLotHistoryDetailViewServiceBean paramBean ) {

		IEntityLimit<IPjtEntity> limit =
			this.support.getPjtEntityLimit( lotId, selectPageNo,
					sheet.intValue(
							AmDesignEntryKeyByChangec.maxPageNumberByLotHistoryDetailView ),
							AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibLotHistoryDetailView() );

		paramBean.setPjtViewBeanList	( AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( limit.getEntities().toArray(new IPjtEntity[0]) ));
		paramBean.setPageInfoView		(
				AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
		paramBean.setSelectPageNo		( selectPageNo );

	}
}
