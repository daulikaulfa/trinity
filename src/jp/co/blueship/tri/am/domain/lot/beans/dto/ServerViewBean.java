package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・サーバ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011
 */
public class ServerViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String serverNo = null;
	private String serverName = null;
	
	public final String getServerNo() {
		return serverNo;
	}

	public final void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public final String getServerName() {
		return serverName;
	}

	public final void setServerName( String serverName ) {
		this.serverName = serverName;
	}
}
