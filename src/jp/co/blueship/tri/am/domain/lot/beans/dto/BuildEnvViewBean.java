package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・ビルド環境一覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class BuildEnvViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ビルドＮｏ */
	private String envNo = null ;
	/** ビルド環境名 */
	private String envName = null ;
	/** ビルド環境概要 */
	private String summary = null;
	/** ビルド環境説明 */
	private String content = null;
	/** ステータス */
	private String statusId = null ;
	/** 最終更新日時 */
	private String insertUpdateDate = null ;

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public String getInsertUpdateDate() {
		return insertUpdateDate;
	}
	public void setInsertUpdateDate(String insertUpdateDate) {
		this.insertUpdateDate = insertUpdateDate;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getEnvNo() {
		return envNo;
	}
	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}



}
