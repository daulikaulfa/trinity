package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ServerViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 変更管理・ロット変更画面の表示情報設定Class<br>
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotModifyService implements IDomain<FlowChaLibLotModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport(FlowChaLibLotEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotModifyServiceBean> execute(IServiceDto<FlowChaLibLotModifyServiceBean> serviceDto) {

		FlowChaLibLotModifyServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (!referer.equals(ChaLibScreenID.LOT_MODIFY) && !forward.equals(ChaLibScreenID.LOT_MODIFY)) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();

			paramBean.setSelectableAllowAssetDuplicationName(sheet.getValue(AmDesignBeanId.allowAssetDuplication, StatusFlg.on.value()));

			try {
				List<IMessageId> commentList = new ArrayList<IMessageId>();
				List<String[]> commentArgsList = new ArrayList<String[]>();

				commentList.add(AmMessageId.AM001016E);
				commentArgsList.add(new String[] {});

				paramBean.setInfoCommentIdList(commentList);
				paramBean.setInfoCommentArgsList(commentArgsList);

				if (ChaLibScreenID.LOT_MODIFY.equals(referer)) {// ロット変更
					lotEditInputBean.setAllowAssetDuplicationName(sheet.getValue(AmDesignBeanId.allowAssetDuplication,
							(lotEditInputBean.isAllowAssetDuplication()) ? StatusFlg.on.value() : StatusFlg.off.value()));

					if (ScreenType.next.equals(paramBean.getScreenType())) {

						checkGroupAccessPermission(lotEditInputBean);

						ILotDto lotDto = this.support.findLotDto( paramBean.getLotNo() );
						if (lotDto.getLotEntity().isUseMerge().parseBoolean() == false && lotEditInputBean.isEditUseMerge() == true) {
							throw new BusinessException(AmMessageId.AM001128E);
						}

						//画面で許可⇒禁止に変更できない制御をしているが、念のため、業務コードで抑える
						if ( lotDto.getLotEntity().getIsAssetDup().parseBoolean() &&
								! lotEditInputBean.isAllowAssetDuplication() ) {
							throw new BusinessException( AmMessageId.AM001016E );
						}

						AmItemChkUtils.checkLotBasicInfo( lotEditInputBean , false ) ;
					}
				}

				if (ChaLibScreenID.LOT_MODIFY.equals(forward)) {// ロット変更
					if (!ScreenType.bussinessException.equals(paramBean.getScreenType())) {

						String lotId = paramBean.getLotNo();
						ILotDto lotDto = this.support.findLotDto( lotId );

						paramBean.setSelectableAllowAssetDuplication(AmViewInfoAddonUtils.isSelectableAllowAssetDuplication());

						if (lotDto.getLotEntity().getIsAssetDup().parseBoolean()) {
							// 一度許可された資産重複貸出は、変更不可
							paramBean.setSelectableAllowAssetDuplication(false);
						}

						if (null == lotEditInputBean.getLotNo()) {
							lotEditInputBean = this.makeLotEditInputBean(lotDto, paramBean);
							// paramBean.setLotEditInputBean( lotEditInputBean )
							// ;getGroupViewBeanList
						}

						lotEditInputBean.setGroupViewBeanList(support.getGroupViewBean());
						lotEditInputBean.setSpecifiedGroupList(support.getGroupViewBean());

					}

				}

			} finally {
				paramBean.setLotEditInputBean(lotEditInputBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005105S, e);
		}
	}

	/**
	 * グループのアクセス許可をチェックする
	 *
	 */

	private void checkGroupAccessPermission(LotEditInputV3Bean lotEditInputBean) {

		List<GroupViewBean> selectedGroupList = lotEditInputBean.getSelectedGroupViewBeanList();
		List<GroupViewBean> preservedGroupList = lotEditInputBean.getPreservedGroupViewBeanList();
		List<GroupViewBean> groupList = lotEditInputBean.getGroupViewBeanList();

		if (null != selectedGroupList && 0 < selectedGroupList.size()) {

			List<IMessageId> messageList = new ArrayList<IMessageId>();
			List<String[]> messageArgsList = new ArrayList<String[]>();

			// 今までアクセス許可グループなし＝全グループにアクセス許可があった場合
			if (null == preservedGroupList || 0 == preservedGroupList.size()) {
				checkGroupAccessPermission(lotEditInputBean, messageList, messageArgsList, selectedGroupList, groupList, AmMessageId.AM001029E);
			}

			// アクセス許可グループに変化があったかも知れない場合
			else {
				checkGroupAccessPermission(lotEditInputBean, messageList, messageArgsList, selectedGroupList, preservedGroupList,
						AmMessageId.AM001029E);
			}
			if (0 != messageList.size()) {
				throw new ContinuableBusinessException(messageList, messageArgsList);
			}
		}
	}

	/**
	 * グループのアクセス許可をチェックする
	 *
	 */
	private void checkGroupAccessPermission(LotEditInputV3Bean lotEditInputBean, List<IMessageId> messageList, List<String[]> messageArgsList,
			List<GroupViewBean> selectedGroupList, List<GroupViewBean> groupList, IMessageId messageId) {

		// チェック対象のグループ名
		Set<String> targetGroupNameSet = new TreeSet<String>();

		for (GroupViewBean preservedGroup : groupList) {

			boolean exist = false;

			for (GroupViewBean selectedGroup : selectedGroupList) {

				if (preservedGroup.getGroupId().equals(selectedGroup.getGroupId())) {
					exist = true;
					break;
				}
			}

			if (!exist) {
				targetGroupNameSet.add(preservedGroup.getGroupName());
			}
		}

		// 対象グループがあった場合
		if (0 < targetGroupNameSet.size()) {

			List<IAreqEntity> assetApplyList = this.support.getAssetApplyByAssetApplyProgress(lotEditInputBean.getLotNo(),
					(String[]) targetGroupNameSet.toArray(new String[0]));

			setMessage(messageList, messageArgsList, assetApplyList, messageId);
		}
	}

	/**
	 * エラーメッセージを設定する。
	 *
	 * @param messageList
	 * @param messageArgsList
	 * @param assetApplyList
	 * @param messageId
	 */
	private void setMessage(List<IMessageId> messageList, List<String[]> messageArgsList, List<IAreqEntity> assetApplyList, IMessageId messageId) {

		for (IAreqEntity assetApply : assetApplyList) {

			String user = null;

			if (AmBusinessJudgUtils.isLendApply(assetApply)) {
				user = assetApply.getLendReqUserNm();
			} else if (AmBusinessJudgUtils.isReturnApply(assetApply)) {
				user = assetApply.getRtnReqUserNm();
			} else if (AmBusinessJudgUtils.isDeleteApply(assetApply)) {
				user = assetApply.getDelReqUserNm();
			}

			messageList.add(messageId);
			messageArgsList.add(new String[] { assetApply.getGrpNm(), user, assetApply.getAreqId() });
		}
	}

	/**
	 *
	 * @param pjtLotEntity
	 * @return
	 */

	private LotEditInputV3Bean makeLotEditInputBean(ILotDto lotDto, FlowChaLibLotModifyServiceBean paramBean) {

		ILotEntity lotEntity = lotDto.getLotEntity();
		LotEditInputV3Bean lotEditInputBean = new LotEditInputV3Bean();

		// 基本情報
		lotEditInputBean.setLotNo(lotEntity.getLotId());
		lotEditInputBean.setStatusId(lotEntity.getStsId());
		lotEditInputBean.setLotName(lotEntity.getLotNm());
		lotEditInputBean.setLotSummary(lotEntity.getSummary());
		lotEditInputBean.setLotContent(lotEntity.getContent());
		lotEditInputBean.setBaseLineTag(lotEntity.getLotLatestBlTag());
		lotEditInputBean.setCreatedTime(lotEntity.getRegTimestamp());
		lotEditInputBean.setBranchBaseLineTag(lotEntity.getLotLatestVerTag());
		lotEditInputBean.setUseMerge( lotEntity.isUseMerge().parseBoolean() );
		lotEditInputBean.setEditUseMerge( lotEntity.isUseMerge().parseBoolean() );
		lotEditInputBean.setAllowAssetDuplication(lotEntity.getIsAssetDup().parseBoolean());
		lotEditInputBean.setAllowAssetDuplicationName(sheet.getValue(AmDesignBeanId.allowAssetDuplication,
				(lotEntity.getIsAssetDup().parseBoolean()) ? StatusFlg.on.value() : StatusFlg.off.value()));
		lotEditInputBean.setAllowListView(lotEntity.getAllowListView().parseBoolean());
		// 既存データ対応（未設定のデータは更新可能）
		if (null == lotEntity.getAllowListView()) {
			lotEditInputBean.setAllowModifyAllowListView(true);
		}
		if ( lotEntity.getAllowListView().parseBoolean() ) {
			lotEditInputBean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.ALLOW_LIST_VIEW_CONTENT.getKey()));
		} else {
			lotEditInputBean.setAllowListContent(DesignUtils.getMessageParameter(MessageParameter.DENY_LIST_VIEW_CONTENT.getKey()));
		}

		// ディレクトリパス
		lotEditInputBean.setDirectoryPathPublic(AmLibraryAddonUtils.getPublicDirectoryPath(lotEntity));
		lotEditInputBean.setDirectoryPathPrivate(AmLibraryAddonUtils.getPrivateDirectoryPath(lotEntity));

		// サーバ情報
		VcsCategory scmType = VcsCategory.value(lotEntity.getVcsCtgCd());
		lotEditInputBean.setScmType(scmType.value());

		ServerViewBean serverBean = new ServerViewBean();
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		String serverNo = srvEntity.getBldSrvId();
		String serverName = srvEntity.getBldSrvNm();

		serverBean.setServerNo(serverNo);
		serverBean.setServerName(serverName);

		lotEditInputBean.setSelectedServerViewBean(serverBean);

		// モジュール情報の取得
		Map<String, IVcsReposDto> vcsReposMap = this.getVcsReposMap();

		// 選択済みモジュール情報
		List<String> selectedModuleNameList = new ArrayList<String>();
		List<ModuleViewBean> selectedModuleViewBeanList = new ArrayList<ModuleViewBean>();
		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities(true);

		for (ILotMdlLnkEntity moduleEntity : mdlEntities) {
			IVcsReposDto reposDto = vcsReposMap.get(moduleEntity.getMdlNm());
			if (null == reposDto) {
				throw new TriSystemException(AmMessageId.AM004019F, moduleEntity.getMdlNm());
			}

			ModuleViewBean moduleViewBean = new ModuleViewBean();
			moduleViewBean.setModuleName(reposDto.getVcsMdlEntity().getMdlNm());
			moduleViewBean.setRepository(reposDto.getVcsReposEntity().getVcsReposUrl());

			selectedModuleViewBeanList.add(moduleViewBean);
			selectedModuleNameList.add(reposDto.getVcsMdlEntity().getMdlNm());
		}

		lotEditInputBean.setSelectedModuleViewBeanList(selectedModuleViewBeanList);
		lotEditInputBean.setSelectedModuleNameString(selectedModuleNameList.toArray(new String[0]));

		// //ロット作成時に選択されなかったモジュール情報
		List<String> moduleNameList = new ArrayList<String>();
		List<ModuleViewBean> moduleViewBeanList = new ArrayList<ModuleViewBean>();
		List<ILotMdlLnkEntity> lotMdlLnkEntities = lotDto.getLotMdlLnkEntities();

		for (ILotMdlLnkEntity mdlEntity : lotMdlLnkEntities) {
			IVcsReposDto reposDto = vcsReposMap.get(mdlEntity.getMdlNm());
			if (null == reposDto) {
				throw new TriSystemException(AmMessageId.AM004021F, mdlEntity.getMdlNm());
			}
			ModuleViewBean moduleViewBean = new ModuleViewBean();
			moduleViewBean.setModuleName(mdlEntity.getMdlNm());
			moduleViewBean.setRepository(reposDto.getVcsReposEntity().getVcsReposUrl());

			moduleViewBeanList.add(moduleViewBean);
			moduleNameList.add(mdlEntity.getMdlNm());
		}

		lotEditInputBean.setModuleViewBeanList(moduleViewBeanList);

		List<IMessageId> infoList = new ArrayList<IMessageId>();
		List<String[]> infoArgsList = new ArrayList<String[]>();
		// 選択済みグループ情報
		Set<String> selectedGroupIdSet = new HashSet<String>();
		List<GroupViewBean> selectedGroupViewBeanList = new ArrayList<GroupViewBean>();

		List<ILotGrpLnkEntity> groupEntityArray = lotDto.getLotGrpLnkEntities();
		if (null != groupEntityArray) {
			for (ILotGrpLnkEntity groupEntity : groupEntityArray) {

				//存在チェック(V2互換)
				this.support.getUmFinderSupport().findGroupById(groupEntity.getGrpId());

				GroupViewBean groupViewBean = new GroupViewBean();
				groupViewBean.setGroupId(groupEntity.getGrpId());
				groupViewBean.setGroupName(groupEntity.getGrpNm());

				selectedGroupViewBeanList.add(groupViewBean);
				selectedGroupIdSet.add(groupEntity.getGrpId());
			}
		}
		lotEditInputBean.setSelectedGroupViewBeanList(selectedGroupViewBeanList);
		lotEditInputBean.setPreservedGroupViewBeanList(selectedGroupViewBeanList);
		lotEditInputBean.setSelectedGroupIdString(selectedGroupIdSet.toArray(new String[0]));

		// 選択済みメール送信先の設定 特定グループ
		Set<String> selectedSpecifiedGroupIdSet = new HashSet<String>();
		List<GroupViewBean> selectedSpecifiedGroupList = new ArrayList<GroupViewBean>();

		List<ILotMailGrpLnkEntity> specifiedGroupEntityArray = lotDto.getLotMailGrpLnkEntities();
		if (null != specifiedGroupEntityArray) {
			for (ILotMailGrpLnkEntity specifiedGroupEntity : specifiedGroupEntityArray) {

				GrpCondition condition = new GrpCondition();
				condition.setGrpId(specifiedGroupEntity.getGrpId());
				IGrpEntity gEntity = this.support.getUmFinderSupport().getGrpDao().findByPrimaryKey(condition.getCondition());
				if (null == gEntity) {
					infoList.add(AmMessageId.AM001031E);
					infoArgsList.add(new String[] { specifiedGroupEntity.getGrpId(), "" });
				} else {
					GroupViewBean groupViewBean = new GroupViewBean();
					groupViewBean.setGroupId(specifiedGroupEntity.getGrpId());
					groupViewBean.setGroupName(gEntity.getGrpNm());

					selectedSpecifiedGroupList.add(groupViewBean);
					selectedSpecifiedGroupIdSet.add(specifiedGroupEntity.getGrpId());
				}
			}
		}
		lotEditInputBean.setSelectedSpecifiedGroupList(selectedSpecifiedGroupList);
		lotEditInputBean.setPreservedSpecifiedGroupList(selectedSpecifiedGroupList);
		lotEditInputBean.setSelectedSpecifiedGroupIdString(selectedSpecifiedGroupIdSet.toArray(new String[0]));

		// 業務エラーメッセージの設定
		paramBean.setInfoMessageIdList(infoList);
		paramBean.setInfoMessageArgsList(infoArgsList);

		// アクセス許可グループの情報が変更されていないかチェック
		AmLibraryAddonUtils.checkAccessableGroup(selectedGroupIdSet, support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		// ビルド環境情報
		lotEditInputBean.setSelectedBuildEnvNo(lotEntity.getBldEnvId());
		lotEditInputBean.setSelectedFullBuildEnvNo(lotEntity.getFullBldEnvId());

		// 選択済みリリース環境情報
		List<String> selectedRelEnvNoList = new ArrayList<String>();
		List<RelEnvViewBean> selectedRelEnvViewBeanList = new ArrayList<RelEnvViewBean>();
		List<ILotRelEnvLnkEntity> relEnvEntityArray = lotDto.getLotRelEnvLnkEntities();

		for (ILotRelEnvLnkEntity envEntity : relEnvEntityArray) {
			RelEnvViewBean viewBean = new RelEnvViewBean();
			viewBean.setEnvNo(envEntity.getBldEnvId());
			viewBean.setEnvName(envEntity.getBldEnvNm());

			selectedRelEnvViewBeanList.add(viewBean);
			selectedRelEnvNoList.add(envEntity.getBldEnvId());
		}

		lotEditInputBean.setSelectedRelEnvViewBeanList(selectedRelEnvViewBeanList);
		lotEditInputBean.setSelectedRelEnvNoString(selectedRelEnvNoList.toArray(new String[0]));

		return lotEditInputBean;
	}

	/**
	 *
	 * @return
	 */
	private Map<String, IVcsReposDto> getVcsReposMap() {

		List<IVcsReposDto> reposDtoList = this.support.findVcsReposDto();

		if (TriStringUtils.isEmpty(reposDtoList)) {
			throw new TriSystemException(AmMessageId.AM004022F);
		}

		Map<String, IVcsReposDto> ucfScmEntityMap = new HashMap<String, IVcsReposDto>();

		for (IVcsReposDto reposDto : reposDtoList) {
			ucfScmEntityMap.put(reposDto.getVcsMdlEntity().getMdlNm(), reposDto);
		}
		return ucfScmEntityMap;
	}

}
