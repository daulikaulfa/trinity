package jp.co.blueship.tri.am.domain.lot.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * ロット情報のステータスをロット運用中orエラーに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionUnlockLotInfo extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		this.unlock( serviceDto );
		return serviceDto;
	}

	private void unlock( IServiceDto<IGeneralServiceBean> serviceDto ) throws BaseBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		LotParamInfo paramInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList );
		if ( TriStringUtils.isEmpty( paramInfo ) ) {
			throw new TriSystemException( AmMessageId.AM005026S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		ILotEntity entity = paramInfo.getLotDto().getLotEntity();

		if ( paramInfo.getProcessResult() ) {
			support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

		} else {
			support.getSmFinderSupport().updateExecDataSts(
					paramBean.getProcId(), AmTables.AM_LOT, AmLotStatusIdForExecData.LotEditError, entity.getLotId());

		}

		//履歴に記録
		if ( DesignSheetUtils.isRecord() ) {
			ILotDto lotDto =support.findLotDto( entity.getLotId() );

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , lotDto);
		}
	}
}
