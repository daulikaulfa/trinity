package jp.co.blueship.tri.am.domain.lot.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・ロット詳細表示用
 *
 * <br>
 *
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class LotDetailViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** ロット概要 */
	private String lotSummary = null;
	/** ロット内容 */
	private String lotContent = null;
	/** 資産重複貸出の表示 */
	private boolean allowAssetDuplicationVisible = false;
	/** 資産重複貸出の表示名 */
	private String allowAssetDuplicationName = null;
	/** ステータス */
	private String lotStatus = null;
	/** 作成日時 */
	private String inputDate = null;
	/** 最新タグ */
	private String recentTag = null;
	private String branchBaselineTag = null;
	/** 最新バージョン */
	private String recentVersion = null;
	/** モジュール名 */
	private String[] moduleName = null;
	/** 一覧表示許可 */
	private boolean allowListView = false;
	/** 一覧表示内容 */
	private String allowListContent;
	/** マージを利用する/しない設定を行う */
	private boolean useMerge = true;
	/** アクセス許可グループ名 */
	private String[] groupName = null;
	/** メール送信先 特定グループ */
	private String[] specifiedGroupName = null;
	/** サーバ番号 */
	private String serverNo = null;
	/** サーバ名 */
	private String serverName = null;
	/** 公開フォルダ */
	private String publicFolder = null;
	/** 非公開フォルダ */
	private String privateFolder = null;
	/** リリース環境名 */
	private String[] relEnvName = null;

	private String themeColor = null;

	private String icon = null;

	/**
	 * @return the icon
	 */
	public String getIcon() {
		return icon;
	}

	/**
	 * @param icon
	 *            the icon to set
	 */
	public void setIcon(String icon) {
		this.icon = icon;
	}

	/**
	 * @return the themeColor
	 */
	public String getThemeColor() {
		return themeColor;
	}

	/**
	 * @param themeColor
	 *            the themeColor to set
	 */
	public void setThemeColor(String themeColor) {
		this.themeColor = themeColor;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotStatus() {
		return lotStatus;
	}

	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getRecentTag() {
		return recentTag;
	}

	public void setRecentTag(String recentTag) {
		this.recentTag = recentTag;
	}

	public String getBranchBaselineTag() {
		return branchBaselineTag;
	}

	public void setBranchBaselineTag(String branchBaselineTag) {
		this.branchBaselineTag = branchBaselineTag;
	}

	public String getRecentVersion() {
		return recentVersion;
	}

	public void setRecentVersion(String recentVersion) {
		this.recentVersion = recentVersion;
	}

	public String[] getModuleName() {
		return moduleName;
	}

	public void setModuleName(String[] moduleName) {
		this.moduleName = moduleName;
	}

	public String getAllowAssetDuplicationName() {
		return allowAssetDuplicationName;
	}

	public void setAllowAssetDuplicationName(String allowAssetDuplicationName) {
		this.allowAssetDuplicationName = allowAssetDuplicationName;
	}

	public boolean isAllowAssetDuplicationVisible() {
		return allowAssetDuplicationVisible;
	}

	public void setAllowAssetDuplicationVisible(boolean allowAssetDuplicationVisible) {
		this.allowAssetDuplicationVisible = allowAssetDuplicationVisible;
	}

	public String getLotContent() {
		return lotContent;
	}

	public void setLotContent(String lotContent) {
		this.lotContent = lotContent;
	}

	public String getLotSummary() {
		return lotSummary;
	}

	public void setLotSummary(String lotSummary) {
		this.lotSummary = lotSummary;
	}

	public boolean isAllowListView() {
		return allowListView;
	}

	public void setAllowListView(boolean value) {
		this.allowListView = value;
	}

	public String getAllowListContent() {
		return this.allowListContent;
	}

	public void setAllowListContent(String content) {
		this.allowListContent = content;
	}

	public boolean isUseMerge() {
		return useMerge;
	}

	public void setUseMerge(boolean useMerge) {
		this.useMerge = useMerge;
	}

	public String[] getGroupName() {
		return groupName;
	}

	public void setGroupName(String[] groupName) {
		this.groupName = groupName;
	}

	public String[] getSpecifiedGroupName() {
		return specifiedGroupName;
	}

	public void setSpecifiedGroupName(String[] specifiedGroupName) {
		this.specifiedGroupName = specifiedGroupName;
	}

	public String getServerNo() {
		return serverNo;
	}

	public void setServerNo(String serverNo) {
		this.serverNo = serverNo;
	}

	public String getServerName() {
		return serverName;
	}

	public void setServerName(String serverName) {
		this.serverName = serverName;
	}

	public String getPublicFolder() {
		return publicFolder;
	}

	public void setPublicFolder(String publicFolder) {
		this.publicFolder = publicFolder;
	}

	public String getPrivateFolder() {
		return privateFolder;
	}

	public void setPrivateFolder(String privateFolder) {
		this.privateFolder = privateFolder;
	}

	public String[] getRelEnvName() {
		return relEnvName;
	}

	public void setRelEnvName(String[] relEnvName) {
		this.relEnvName = relEnvName;
	}

}
