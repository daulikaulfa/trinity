package jp.co.blueship.tri.am.domain.lot.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtSearchBean;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理・ロット詳細閲覧用
 * 
 * @version V3L10.01
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibLotDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 2L;
	
	/** 
	 * 選択ロット番号 
	 */
	private String selectedLotNo = null;
	/** 
	 * ロット詳細情報 
	 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** 
	 * ビルドパッケージ作成成功回数 
	 */
	private int succeededRelUnitCount = 0;
	/** 
	 * ビルドパッケージ作成失敗回数 
	 */
	private int failedRelUnitCount = 0;
	/** 
	 * 変更管理情報 
	 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 
	 * 選択ページ 
	 */
	private int selectPageNo = 0;
	/** 
	 * ページ制御 
	 */
	private IPageNoInfo pageInfoView = null;
	/** 
	 * 変更管理 詳細検索用Bean 
	 */
	private PjtSearchBean pjtSearchBean = null ;
	/**
 	* 変更一括クローズ選択変更情報ID
 	*/
 	private String[] selectedPjtIdArray;
	
	
	public PjtSearchBean getPjtSearchBean() {
		return pjtSearchBean;
	}
	public void setPjtSearchBean(PjtSearchBean pjtSearchBean) {
		this.pjtSearchBean = pjtSearchBean;
	}
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public LotDetailViewBean getLotDetailViewBean() {
		if ( null == lotDetailViewBean ) {
			lotDetailViewBean = new LotDetailViewBean();
		}
		return lotDetailViewBean;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}
	
	public int getSucceededRelUnitCount() {
		return succeededRelUnitCount;
	}
	public void setSucceededRelUnitCount(int succeededRelUnitCount) {
		this.succeededRelUnitCount = succeededRelUnitCount;
	}
	
	public int getFailedRelUnitCount() {
		return failedRelUnitCount;
	}
	public void setFailedRelUnitCount(int failedRelUnitCount) {
		this.failedRelUnitCount = failedRelUnitCount;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		if ( null == pjtViewBeanList ) {
			pjtViewBeanList = new ArrayList<PjtViewBean>();
		}
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public String[] getSelectedPjtIdArray() {
		return selectedPjtIdArray;
	}
	public void setSelectedPjtIdArray(String[] selectedPjtIdArray) {
		this.selectedPjtIdArray = selectedPjtIdArray;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	String scmType = null;
	public String getScmType() {
		return scmType;
	}
	
	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
}

