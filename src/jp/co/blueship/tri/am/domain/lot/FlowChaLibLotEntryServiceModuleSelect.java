package jp.co.blueship.tri.am.domain.lot;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ModuleViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.ServerViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLotEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * 変更管理・ロット作成画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotEntryServiceModuleSelect implements IDomain<FlowChaLibLotEntryServiceBean> {


	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibLotEditSupport support = null;

	public void setSupport( FlowChaLibLotEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLotEntryServiceBean> execute( IServiceDto<FlowChaLibLotEntryServiceBean> serviceDto ) {

		FlowChaLibLotEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward() ;
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LOT_MODULE_SELECT ) &&
					!forward.equals( ChaLibScreenID.LOT_MODULE_SELECT )) {
				return serviceDto;
			}

			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean() ;

			try {
				if( ChaLibScreenID.LOT_MODULE_SELECT.equals( referer ) ) {//モジュール選択画面
					if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
						AmItemChkUtils.checkLotModuleInfo( lotEditInputBean ) ;
					}
				}
				if( ChaLibScreenID.LOT_MODULE_SELECT.equals( forward ) ) {//モジュール選択画面
					if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

						List<ModuleViewBean> moduleViewBeanList = null ;

						VcsCategory scmType = VcsCategory.value( sheet.getValue(AmDesignEntryKeyByScm.useApplication ) ) ;
						lotEditInputBean.setScmType( scmType.value() );

						if( VcsCategory.SVN.equals(  scmType ) ) {

							// サーバ情報設定
							setServerInfo( lotEditInputBean );

							String serverId = null;
							if ( null != lotEditInputBean.getSelectedServerViewBean() ) {
								serverId = lotEditInputBean.getSelectedServerViewBean().getServerNo();
							}

							if ( null != serverId ) {
								moduleViewBeanList = this.support.getModuleViewBeanListForSvn( serverId ) ;
							} else {
								moduleViewBeanList = new ArrayList<ModuleViewBean>();
							}

						} else {
							throw new TriSystemException(AmMessageId.AM004016F , scmType.value() );
						}

						lotEditInputBean.setModuleViewBeanList( moduleViewBeanList ) ;

						if ( null == lotEditInputBean.getSelectedServerViewBean() ) {
							lotEditInputBean.setSelectedServerViewBean( new ServerViewBean() );
						}
					}
				}

			} finally {

				paramBean.setLotEditInputBean( lotEditInputBean ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005102S,e);
		}
	}

	/**
	 * サーバ情報を設定する
	 * @param lotEditInputBean
	 */
	private void setServerInfo( LotEditInputV3Bean lotEditInputBean ) {
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

		List<ServerViewBean> beanList = new ArrayList<ServerViewBean>();

		ServerViewBean bean = new ServerViewBean();
		bean.setServerNo	( srvEntity.getBldSrvId() );
		bean.setServerName	( srvEntity.getBldSrvNm() );

		beanList.add( bean );
		lotEditInputBean.setServerViewBeanList( beanList );
	}
}
