package jp.co.blueship.tri.am.domain.lot;

import jp.co.blueship.tri.am.beans.mail.dto.PjtMailServiceBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * ロット取消処理のメール送信設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLotCancelServiceMail implements IDomain<FlowChaLibLotCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	private MailGenericService successMail = null;


	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}
	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}


	@Override
	public IServiceDto<FlowChaLibLotCancelServiceBean> execute( IServiceDto<FlowChaLibLotCancelServiceBean> serviceDto) {

		FlowChaLibLotCancelServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.COMP_LOT_CANCEL ) &&
					!forwordID.equals( ChaLibScreenID.COMP_LOT_CANCEL ) ){
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_LOT_CANCEL )) {
				return serviceDto;
			}
			if ( ScreenType.bussinessException.equals( screenType ) ) {
				return serviceDto;
			}
			PjtMailServiceBean successMailBean = new PjtMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, paramBean );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

			String selectedLotNo = paramBean.getLotDetailViewBean().getLotNo();
			ILotEntity pjtLotEntity = this.support.findLotEntity(selectedLotNo, null);
			//画面入力値取得
			LotEditInputV3Bean lotEditInputBean = paramBean.getLotEditInputBean();
			pjtLotEntity.setContent(lotEditInputBean.getDelComment());

			successMailBean.setLotEntity( pjtLotEntity );

			successMail.execute( mailServiceDto );

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S , e , paramBean.getFlowAction() ) ) ;
		}
		return serviceDto;
	}
}
