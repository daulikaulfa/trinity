package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmEntityAddonUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 * 変更管理起票・変更管理情報更新完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowChaLibPjtModifyServiceComp implements IDomain<FlowChaLibPjtModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	private IUmFinderSupport umSupport;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowChaLibPjtModifyServiceBean> execute( IServiceDto<FlowChaLibPjtModifyServiceBean> serviceDto ) {

		FlowChaLibPjtModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_MODIFY ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_MODIFY ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.COMP_PJT_MODIFY )) {
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_PJT_MODIFY )) {
				return serviceDto;
			}
			if ( ScreenType.bussinessException.equals( screenType ) ) {
				return serviceDto;
			}

			String selectedPjtNo = paramBean.getSelectedPjtNo();
			AmItemChkUtils.checkPjtNo( selectedPjtNo );

			PjtDetailInputBean pjtDetailInputBean = paramBean.getPjtDetailInputBean();
			PjtDetailBean pjtDetailBean = pjtDetailInputBean.getPjtDetailBean();

			IPjtEntity entity = this.support.findPjtEntity( selectedPjtNo );

			AmEntityAddonUtils.setPjtEntityPjtDetailBean(
							entity, pjtDetailInputBean.getPjtDetailBean(), paramBean.getUserName() , paramBean.getUserId() );


			this.support.getPjtDao().update( entity );

			this.umSupport.updateCtgLnk(pjtDetailBean.getCtgId(), AmTables.AM_PJT, pjtDetailBean.getPjtNo());
			this.umSupport.updateMstoneLnk(pjtDetailBean.getMstoneId(), AmTables.AM_PJT, pjtDetailBean.getPjtNo());

			if ( DesignSheetUtils.isRecord() ){
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.Edit.getStatusId() );

				support.getPjtHistDao().insert( histEntity , entity);
			}

			// グループの存在チェック
			ILotDto lotDto = this.support.findLotDto( entity.getLotId() );
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005116S,e);
		}
	}
}
