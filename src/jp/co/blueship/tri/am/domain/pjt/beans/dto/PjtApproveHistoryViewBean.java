package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

/**
 * 変更管理承認・変更管理承認履歴一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtApproveHistoryViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 承認日時 */
	private String approveTime = null;
	/** 承認日時(検索用) */
	private String approveDate4Search = null;
	/** 承認コメント */
	private String approveComment = null;
	/** 承認申請番号 */
	private String approveApplyNo = null;
	/** 承認者 */
	private String approveUser = null;
	/** 承認者ＩＤ */
	private String approveUserId = null;
	/** 承認取消日時 */
	private String approveCancelDate = null;
	/** 承認取消者 */
	private String approveCancelUser = null;
	/** 承認取消者ＩＤ */
	private String approveCancelUserId = null;
	/** ステータス */
	private String status = null;
	
	/** 取消ボタンの表示の有無 */
	private boolean approveCancelView = false;
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	
	public String getApproveTime() {
		return approveTime;
	}
	public void setApproveTime( String approveTime ) {
		this.approveTime = approveTime;
	}
	
	public String getApproveDate4Search() {
		return approveDate4Search;
	}
	public void setApproveDate4Search( String approveDate4Search ) {
		this.approveDate4Search = approveDate4Search;
	}
	
	public String getApproveComment() {
		return approveComment;
	}
	public void setApproveComment( String approveComment ) {
		this.approveComment = approveComment;
	}
	
	public String getApproveApplyNo() {
		return approveApplyNo;
	}
	public void setApproveApplyNo( String approveApplyNo ) {
		this.approveApplyNo = approveApplyNo;
	}
	
	public String getApproveUser() {
		return approveUser;
	}
	public void setApproveUser( String approveUser ) {
		this.approveUser = approveUser;
	}
	
	public String getApproveCancelDate() {
		return approveCancelDate;
	}
	public void setApproveCancelDate( String approveCancelDate ) {
		this.approveCancelDate = approveCancelDate;
	}
	
	public String getApproveCancelUser() {
		return approveCancelUser;
	}
	public void setApproveCancelUser( String approveCancelUser ) {
		this.approveCancelUser = approveCancelUser;
	}
	
	public boolean getApproveCancelView() {
		return approveCancelView;
	}
	public void setApproveCancelView( boolean approveCancelView ) {
		this.approveCancelView = approveCancelView;
	}
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	public String getApproveCancelUserId() {
		return approveCancelUserId;
	}
	public void setApproveCancelUserId(String approveCancelUserId) {
		this.approveCancelUserId = approveCancelUserId;
	}
	public String getApproveUserId() {
		return approveUserId;
	}
	public void setApproveUserId(String approveUserId) {
		this.approveUserId = approveUserId;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	
}
