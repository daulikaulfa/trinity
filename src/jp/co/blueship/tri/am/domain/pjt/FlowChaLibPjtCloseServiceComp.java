package jp.co.blueship.tri.am.domain.pjt;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * 変更管理起票・変更管理クローズ完了画面の表示情報設定Class<br>
 *
 * @version V3L10.01
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibPjtCloseServiceComp implements IDomain<FlowChaLibPjtCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtCloseServiceBean> execute( IServiceDto<FlowChaLibPjtCloseServiceBean> serviceDto ) {

		FlowChaLibPjtCloseServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_CLOSE ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_CLOSE ) ){
				return serviceDto;
			}

			if ( paramBean.isBatchMode() ){
		   		return serviceDto;
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_CLOSE )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String selectedPjtNo = paramBean.getSelectedPjtNo();
					AmItemChkUtils.checkPjtNo( selectedPjtNo );


					PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

					Timestamp systemDate		= TriDateUtils.getSystemTimestamp();

					IPjtEntity entity		= this.support.findPjtEntity( selectedPjtNo );

					entity.setCloseCmt			( pjtEditInputBean.getCloseComment() );
					entity.setCloseTimestamp	( systemDate );
					entity.setCloseUserNm		( paramBean.getUserName() );
					entity.setCloseUserId		( paramBean.getUserId() );
					entity.setStsId				( AmPjtStatusId.ChangePropertyClosed.getStatusId() );
					entity.setUpdTimestamp		( systemDate );
					entity.setUpdUserNm			( paramBean.getUserName() );
					entity.setUpdUserId			( paramBean.getUserId() );
					this.support.getPjtDao().update( entity );

					if ( DesignSheetUtils.isRecord() ){
						IHistEntity histEntity = new HistEntity();
						histEntity.setDataCtgCd( AmTables.AM_PJT.name() );
						histEntity.setActSts( UmActStatusId.Close.getStatusId() );

						support.getPjtHistDao().insert( histEntity , entity);
					}

					PjtDetailViewBean pjtDetailViewBean = paramBean.getPjtDetailViewBean();
					pjtDetailViewBean.getPjtDetailBean().setPjtNo( selectedPjtNo );
					paramBean.setPjtDetailViewBean( pjtDetailViewBean );

					// グループの存在チェック
					ILotDto lotDto = this.support.findLotDto( entity.getLotId() );
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005129S , e );
		}
	}
}
