package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * 変更管理・履歴詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtHistoryDetailViewService implements IDomain<FlowChaLibPjtHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtHistoryDetailViewServiceBean> execute( IServiceDto<FlowChaLibPjtHistoryDetailViewServiceBean> serviceDto ) {

		FlowChaLibPjtHistoryDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedPjtNo = paramBean.getSelectedPjtNo();
			AmItemChkUtils.checkPjtNo( selectedPjtNo );

			IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo );
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			PjtDetailViewBean pjtDetailViewBean =
				AmViewInfoAddonUtils.getPjtDetailViewBean( lotDto.getLotEntity(), pjtEntity );

			paramBean.setPjtDetailViewBean( pjtDetailViewBean );

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			IEntityLimit<IAreqEntity> entityLimit =
					this.support.getAssetApplyEntityLimit(
							selectedPjtNo,
							selectPageNo,
							sheet.intValue(
									AmDesignEntryKeyByChangec.maxPageNumberByPjtHistoryDetailView ),
									AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtHistoryDetailView() );

			setServiceBeanSearchResult( entityLimit, selectPageNo, paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005115S , e );
		}
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param limit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean  FlowChaLibPjtHistoryDetailViewServiceBeanオブジェクト
	 *
	 */
	private void setServiceBeanSearchResult(
			IEntityLimit<IAreqEntity> limit, int selectPageNo, FlowChaLibPjtHistoryDetailViewServiceBean paramBean ) {

		AreqDtoList areqDtoList = this.support.findAreqDtoList( limit.getEntities() );
		paramBean.setApplyInfoViewBeanList(
				this.support.getApplyInfoViewPjtDetailBean( paramBean.getUserId(), areqDtoList ) );
		paramBean.setPageInfoView			(
				AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
		paramBean.setSelectPageNo			( selectPageNo );

	}
}
