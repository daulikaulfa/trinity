package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理起票・変更管理更新画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtModifyServiceInput implements IDomain<FlowChaLibPjtModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtModifyServiceBean> execute( IServiceDto<FlowChaLibPjtModifyServiceBean> serviceDto ) {

		FlowChaLibPjtModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_MODIFY_INPUT ) &&
					!forwordID.equals( ChaLibScreenID.PJT_MODIFY_INPUT ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.PJT_MODIFY_INPUT )) {

				PjtDetailInputBean pjtDetailInputBean =
										paramBean.getPjtDetailInputBean();

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( ChaLibScreenID.PJT_MODIFY_INPUT )) {
					AmItemChkUtils.checkPjtInput( pjtDetailInputBean, support);
				}

				paramBean.setPjtDetailInputBean( pjtDetailInputBean );
			}

			if ( forwordID.equals( ChaLibScreenID.PJT_MODIFY_INPUT )) {

				String selectedPjtNo	= paramBean.getSelectedPjtNo();
				AmItemChkUtils.checkPjtNo( selectedPjtNo );

				IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo );
				ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				PjtDetailInputBean pjtDetailInputBean =
					AmViewInfoAddonUtils.getPjtDetailInputBean( lotDto.getLotEntity(), pjtEntity );

				paramBean.setPjtDetailInputBean( pjtDetailInputBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005118S , e );
		}
	}
}
