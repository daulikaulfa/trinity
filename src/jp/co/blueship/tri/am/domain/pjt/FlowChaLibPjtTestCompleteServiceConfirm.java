package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtTestCompleteServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理起票・変更管理テスト完了確認画面の表示情報設定Class
 *
 * @version V3L10.02
 *
 * @version SP-20150622_V3L13R01
 * @author Norheda Zulkipeli
 *
 */

public class FlowChaLibPjtTestCompleteServiceConfirm implements IDomain<FlowChaLibPjtTestCompleteServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtTestCompleteServiceBean> execute(IServiceDto<FlowChaLibPjtTestCompleteServiceBean> serviceDto ) {

		FlowChaLibPjtTestCompleteServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_TEST_COMPLETE_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_TEST_COMPLETE_CONFIRM ) ){
				return serviceDto;
			}

			String selectedPjtNo	= paramBean.getSelectedPjtNo();
			int selectPageNo		=
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			if ( refererID.equals( ChaLibScreenID.PJT_TEST_COMPLETE_CONFIRM )) {

				PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

				if ( ScreenType.next.equals( screenType )) {

					IPjtEntity pjtEntity			= this.support.findPjtEntity( selectedPjtNo );
					IAreqEntity[] applyList	= this.support.getAssetApplyEntityLimit( selectedPjtNo, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtTestCompleteConfirm() ).getEntities().toArray(new IAreqEntity[0]);

					AmItemChkUtils.checkPjtTestComplete( pjtEditInputBean, applyList );

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( support.getAssetApplyNo( applyList ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				paramBean.setPjtEditInputBean( pjtEditInputBean );
			}


			if ( forwordID.equals( ChaLibScreenID.PJT_TEST_COMPLETE_CONFIRM )) {

				AmItemChkUtils.checkPjtNo( selectedPjtNo );

				IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo );
				ILotEntity lotEntity	= this.support.findLotEntity( pjtEntity.getLotId() );

				PjtDetailViewBean pjtDetailViewBean =
					AmViewInfoAddonUtils.getPjtDetailViewBean( lotEntity, pjtEntity );

				paramBean.setPjtDetailViewBean( pjtDetailViewBean );

				IEntityLimit<IAreqEntity> limit = this.getAssetApplyEntityLimit( selectedPjtNo, selectPageNo );

				IAreqEntity[] applyList	= this.support.getAssetApplyEntityLimit( selectedPjtNo, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtTestCompleteConfirm() ).getEntities().toArray(new IAreqEntity[0]);

				if (0 == applyList.length) {
					throw new ContinuableBusinessException(AmMessageId.AM001129E);
				}

				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( support.getAssetApplyNo( applyList ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				setServiceBeanSearchResult( limit, selectPageNo, paramBean );

			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005120S , e );
		}
	}

	/**
	 * 申請情報エンティティリミットを取得する
	 * @param pjtNo 変更管理番号
	 * @param pageNo ページ番号
	 * @return 申請情報エンティティリミット
	 */
	private IEntityLimit<IAreqEntity> getAssetApplyEntityLimit( String pjtNo, int pageNo ) {

		IEntityLimit<IAreqEntity> limit =
			this.support.getAssetApplyEntityLimit(
					pjtNo,
					pageNo,
					sheet.intValue(
							AmDesignEntryKeyByChangec.maxPageNumberByPjtTestCompleteConfirm ),
							AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtTestCompleteConfirm() );

		return limit;
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param limit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean  FlowChaLibPjtCancelServiceBeanオブジェクト
	 */
	private void setServiceBeanSearchResult(
			IEntityLimit<IAreqEntity> limit, int selectPageNo, FlowChaLibPjtTestCompleteServiceBean paramBean ) {

		AreqDtoList areqDtoList = this.support.findAreqDtoList( limit.getEntities() );
		paramBean.setApplyInfoViewBeanList(
				this.support.getApplyInfoViewPjtDetailBean( paramBean.getUserId() , areqDtoList ) );
		paramBean.setPageInfoView			(
				AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
		paramBean.setSelectPageNo			( selectPageNo );
	}

}
