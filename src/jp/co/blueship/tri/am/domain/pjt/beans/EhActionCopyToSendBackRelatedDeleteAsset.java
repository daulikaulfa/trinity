package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 返却資産申請の取消時、貸出資産、返却資産を差戻しフォルダにコピーします。（関連）
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionCopyToSendBackRelatedDeleteAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	private IDomain<IGeneralServiceBean> action = null;

	/**
	 * ビジネス処理がインスタンス生成時に自動的に設定されます。
	 * @param start ビジネス処理
	 */
	public final void setAction( IDomain<IGeneralServiceBean> action ) {
		this.action = action;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity lotEntity = lotDto.getLotEntity();
		IEhAssetRelatedEntity relatedEntity = this.getAssetRelatedEntity( paramList );

		for ( IAreqDto entity : relatedEntity.getAssetRelatedReturnEntity() ) {

			List<Object> list = new ArrayList<Object>();

			list.add( lotEntity );
			list.add( entity );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( serviceDto.getServiceBean() )
					.setParamList( paramList );

			action.execute( innerServiceDto );
		}

		return serviceDto;
	}

	/**
	 * 返却資産に関連する貸出／返却申請一覧の格納情報を取得します。
	 * @param paramList コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 */
	private final IEhAssetRelatedEntity getAssetRelatedEntity( List<Object> paramList ) {

		IAreqDto dto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == dto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		return paramInfo.getApplyNoAssetRelatedEntityMap().get( dto.getAreqEntity().getAreqId() );

	}
}
