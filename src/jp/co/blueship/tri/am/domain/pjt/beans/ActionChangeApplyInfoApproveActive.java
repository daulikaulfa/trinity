package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 申請情報のステータスを承認処理中に更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeApplyInfoApproveActive extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		IAreqEntity areqEntity = areqDto.getAreqEntity();

		this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqEntity.getAreqId());
		this.registerProcessRecord( paramBean.getProcId(), areqEntity );

		return serviceDto;

	}

	private void registerProcessRecord(
			String procId, IAreqEntity areqEntity ) {

		String statusId = null;

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			statusId = AmAreqStatusIdForExecData.ApprovingCheckinRequest.getStatusId();

		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			statusId = AmAreqStatusIdForExecData.ApprovingRemovalRequest.getStatusId();
		}

		support.getSmFinderSupport().registerExecDataSts(procId, AmTables.AM_AREQ, statusId, areqEntity.getAreqId());
	}

}
