package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class ApproveParamInfo extends CommonParamInfo implements IApproveParamInfo {

	private static final long serialVersionUID = 2L;

	private String comment = null;
	private String user = null;
	private IPjtAvlDto pjtAvlDto = null;
	private AreqDtoList assetApplyEntities = null;
	private Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap = null;
	private boolean processResult = true;

	private String lotBlId;
	private String lotVerTag;

	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用される
	 */
	private ILotBlDto registerLotBlDto = null;

	public IPjtAvlDto getPjtAvlDto() {
		return pjtAvlDto;
	}
	public void setPjtAvlDto( IPjtAvlDto pjtAvlDto ) {
		this.pjtAvlDto = pjtAvlDto;
	}

	public AreqDtoList getAssetApplyEntities() {
		return assetApplyEntities;
	}
	public void setAssetApplyEntities( AreqDtoList assetApplyEntities ) {
		this.assetApplyEntities = assetApplyEntities;
	}
	@Override
	public String getLotBlId() {
	    return lotBlId;
	}
	@Override
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	}
	@Override
	public String getLotVerTag() {
	    return lotVerTag;
	}
	@Override
	public void setLotVerTag(String lotVerTag) {
	    this.lotVerTag = lotVerTag;
	}
	@Override
	public ILotBlDto getRegisterLotBlDto() {
	    return registerLotBlDto;
	}
	@Override
	public void setRegisterLotBlDto(ILotBlDto registerLotBlDto) {
	    this.registerLotBlDto = registerLotBlDto;
	}

	public String getComment() {
		return comment;
	}
	public void setComment( String comment ) {
		this.comment = comment;
	}

	public String getUser() {
		return user;
	}
	public void setUser( String user ) {
		this.user = user;
	}

	public Map<String,IEhAssetRelatedEntity> getApplyNoAssetRelatedEntityMap() {
		if ( null == applyNoAssetRelatedEntityMap ) {
			applyNoAssetRelatedEntityMap = new TreeMap<String,IEhAssetRelatedEntity>();
		}
		return applyNoAssetRelatedEntityMap;
	}
	public void setApplyNoAssetRelatedEntityMap(
			Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap ) {
		this.applyNoAssetRelatedEntityMap = applyNoAssetRelatedEntityMap;
	}

	public boolean getProcessResult() {
		return processResult;
	}
	public void setProcessResult( boolean processResult ) {
		this.processResult = processResult;
	}

}
