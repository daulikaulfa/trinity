package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

/**
 * アプリケーション層で、共通に使用する承認情報を格納するインタフェースです。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public interface IApproveParamInfo extends ICommonParamInfo, Serializable {

	/**
	 * 変更管理エンティティを取得します。
	 * @return 取得した値を戻します。
	 */
	public IPjtAvlDto getPjtAvlDto();
	/**
	 * 変更管理番号を設定します。
	 * @param value 変更管理エンティティ
	 */
	public void setPjtAvlDto( IPjtAvlDto pjtAvlDto );
	/**
	 * 変更管理に属する申請情報エンティティの配列を取得します。
	 * @return 取得した値を戻します。
	 */
	public AreqDtoList getAssetApplyEntities();
	/**
	 * 変更管理に属する申請情報エンティティを設定します。
	 * @param value 申請情報エンティティの配列
	 */
	public void setAssetApplyEntities( AreqDtoList assetApplyEntities );

	/**
	 * ロット・ベースラインIDを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLotBlId();
	/**
	 * ロット・ベースラインIDを設定します。
	 * @param lotBlId ロット・ベースラインID
	 */
	public void setLotBlId( String lotBlId );

	/**
	 * ロット・VCSタグを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLotVerTag();
	/**
	 * ロット・VCSタグを設定します。
	 * @param lotVerTag ロット・VCSタグ
	 */
	public void setLotVerTag( String lotVerTag );

	/**
	 * 登録したロット・ベースラインDTO。
	 * Domain層でのみ利用されます。
	 *
	 * @return 登録したロット・ベースラインDTO。
	 */
	public ILotBlDto getRegisterLotBlDto();
	/**
	 * 登録したロット・ベースラインDTO。
	 * Domain層でのみ利用されます。
	 *
	 * @param registerLotBlDto 登録したロット・ベースラインDTO。
	 */
	public void setRegisterLotBlDto(ILotBlDto registerLotBlDto);
	/**
	 * 操作コメントを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getComment();
	/**
	 * 操作コメントを設定します。
	 * @param value 承認コメント
	 */
	public void setComment( String comment );
	/**
	 * ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * ユーザＩＤを設定します。
	 * @param id ユーザＩＤ
	 */
	public void setUserId( String id );
	/**
	 * 操作ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUser();
	/**
	 * 操作ユーザを設定します。
	 * @param value 操作ユーザ
	 */
	public void setUser( String user );
	/**
	 * 申請番号とそれの関連申請情報エンティティのマップを取得します。
	 * @return 取得した値を戻します。
	 */
	public Map<String,IEhAssetRelatedEntity> getApplyNoAssetRelatedEntityMap();
	/**
	 * 申請番号とそれの関連申請情報エンティティのマップを設定します。
	 * @param value 申請番号とそれの関連申請情報エンティティのマップ
	 */
	public void setApplyNoAssetRelatedEntityMap(
			Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap );
	/**
	 * 処理の成功、失敗のフラグを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean getProcessResult();
	/**
	 * 処理の成功、失敗のフラグを設定します。
	 * @param value 処理の成功、失敗のフラグ
	 */
	public void setProcessResult( boolean processResult );

}