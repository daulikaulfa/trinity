package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 申請情報のステータスを承認済みに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeApplyInfoApproveSuccess extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}
		IAreqEntity areqEntity = areqDto.getAreqEntity();
		IPjtAvlDto pjtApproveEntity = paramInfo.getPjtAvlDto();
		if ( null == pjtApproveEntity.getPjtAvlEntity() ) {
			throw new TriSystemException(AmMessageId.AM005034S);
		}

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			areqEntity.setStsId( AmAreqStatusId.CheckinRequestApproved.getStatusId() );
		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			areqEntity.setStsId( AmAreqStatusId.RemovalRequestApproved.getStatusId() );
		}
		support.getAreqDao().update( areqEntity );

		support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqEntity.getAreqId());

		if ( DesignSheetUtils.isRecord() ) {
			// 履歴用Dto設定
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Approve.getStatusId() );

			support.getAreqHistDao().insert( histEntity , areqDto);
		}

		return serviceDto;

	}
}
