package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * １申請ずつ返却、削除を判定し、それぞれの処理を別処理に委譲します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionDelegateByApplyInfo extends ActionPojoAbstract<IGeneralServiceBean>  {

	/**
	 * 返却用の処理
	 */
	private List<IDomain<IGeneralServiceBean>> returnActions = null;
	public void setReturnActions( List<IDomain<IGeneralServiceBean>> returnActions ) {
		this.returnActions = returnActions;
	}

	/**
	 * 削除用の処理
	 */
	private List<IDomain<IGeneralServiceBean>> deleteActions = null;
	public void setDeleteActions( List<IDomain<IGeneralServiceBean>> deleteActions ) {
		this.deleteActions = deleteActions;
	}

	/**
	 * 共通用の処理
	 */
	private List<IDomain<IGeneralServiceBean>> commonActions = null;
	public void setCommonActions( List<IDomain<IGeneralServiceBean>> commonActions ) {
		this.commonActions = commonActions;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();
		IApproveParamInfo param = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == param ) {

			throw new TriSystemException(AmMessageId.AM005031S);
		}

		for ( IAreqDto entity : param.getAssetApplyEntities() ) {

			try {
				paramList.add( entity );

				if ( AmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() )) {

					if ( null != returnActions ) {
						for ( IDomain<IGeneralServiceBean> returnAction : returnActions ) {
							returnAction.execute( serviceDto );
						}
					}

				} else if ( AmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) {

					if ( null != deleteActions ) {
						for ( IDomain<IGeneralServiceBean> deleteAction : deleteActions ) {
							deleteAction.execute( serviceDto );
						}
					}
				}

				if ( null != commonActions ) {
					for ( IDomain<IGeneralServiceBean> commonAction : commonActions ) {
						commonAction.execute( serviceDto );
					}
				}
			} finally {
				//forループの中でaddしたAreqEntityを削除している
				IAreqDto dtoOld = AmExtractEntityAddonUtils.extractAssetApply( paramList ) ;
				paramList.remove( dtoOld );
			}
		}
		return serviceDto;
	}
}
