package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;
import java.util.List;

/**
 * 変更管理・変更管理登録、更新用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtDetailInputBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理詳細情報 */
	private PjtDetailBean pjtDetailBean = null;
	/** 変更要因分類のコンボボックス */
	private List<String> changeCauseClassifyList = null;
	
	
	public PjtDetailBean getPjtDetailBean() {
		if ( null == pjtDetailBean ) {
			pjtDetailBean = new PjtDetailBean();
		}
		return pjtDetailBean;
	}
	public void setPjtDetailBean( PjtDetailBean pjtDetailBean ) {
		this.pjtDetailBean = pjtDetailBean;
	}	
	
	public List<String> getChangeCauseClassifyList() {
		return changeCauseClassifyList;
	}
	public void setChangeCauseClassifyList( List<String> changeCauseClassifyList ) {
		this.changeCauseClassifyList = changeCauseClassifyList;
	}
}
