package jp.co.blueship.tri.am.domain.pjt;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * 変更管理起票・変更管理取消完了画面の表示情報設定Class<br>
 * <br>
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChaLibPjtCancelServiceComp implements IDomain<FlowChaLibPjtCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtCancelServiceBean> execute( IServiceDto<FlowChaLibPjtCancelServiceBean> serviceDto ) {

		FlowChaLibPjtCancelServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_CANCEL ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_CANCEL ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.COMP_PJT_CANCEL )) {

			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_CANCEL )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String selectedPjtNo = paramBean.getSelectedPjtNo();
					AmItemChkUtils.checkPjtNo( selectedPjtNo );

					PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

					Timestamp systemDate		= TriDateUtils.getSystemTimestamp();

					IPjtEntity entity		= this.support.findPjtEntity( selectedPjtNo );
					entity.setDelCmt			( pjtEditInputBean.getDelComment() );
					entity.setDelTimestamp		( systemDate );
					entity.setDelUserNm			( paramBean.getUserName() );
					entity.setDelUserId			( paramBean.getUserId() );
					entity.setDelStsId			( StatusFlg.on );
					entity.setStsId				( AmPjtStatusId.ChangePropertyRemoved.getStatusId() );

					this.support.getPjtDao().update( entity );

					this.support.getUmFinderSupport().cleaningCtgLnk(AmTables.AM_PJT, selectedPjtNo);
					this.support.getUmFinderSupport().cleaningMstoneLnk(AmTables.AM_PJT, selectedPjtNo);

					if ( DesignSheetUtils.isRecord() ){
						IHistEntity histEntity = new HistEntity();
						histEntity.setActSts( UmActStatusId.Remove.getStatusId() );

						support.getPjtHistDao().insert( histEntity , entity);
					}

					PjtDetailViewBean pjtDetailViewBean = paramBean.getPjtDetailViewBean();
					pjtDetailViewBean.getPjtDetailBean().setPjtNo( selectedPjtNo );
					paramBean.setPjtDetailViewBean( pjtDetailViewBean );

					// グループの存在チェック
					ILotDto lotDto = this.support.findLotDto( entity.getLotId() );
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005127S , e );
		}
	}
}
