package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 返却資産申請の取消時、貸出資産、返却資産を差戻しフォルダにコピーします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
@Deprecated
public class EhActionCopyToSendBackAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		@SuppressWarnings("unused")
		List<Object> paramList = serviceDto.getParamList();

		return serviceDto;
	}
}
