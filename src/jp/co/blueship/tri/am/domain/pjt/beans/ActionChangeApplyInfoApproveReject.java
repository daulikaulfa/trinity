package jp.co.blueship.tri.am.domain.pjt.beans;

import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 申請情報のステータスを承認却下成功に更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ActionChangeApplyInfoApproveReject extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		IAreqEntity areqEntity = areqDto.getAreqEntity();

		Timestamp systemDate = TriDateUtils.getSystemTimestamp();

		String stsId = null;

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			areqEntity.setAreqId				( areqEntity.getAreqId() );
			areqEntity.setAreqCtgCd				( AreqCtgCd.LendingRequest.value() );
			areqEntity.setStsId					( AmAreqStatusId.CheckoutRequested.getStatusId() );
			stsId = AmAreqStatusIdForExecData.ReturnToCheckoutRequest.getStatusId();

		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			areqEntity.setAreqId				( areqEntity.getAreqId() );
			areqEntity.setAreqCtgCd				( AreqCtgCd.RemovalRequest.value() );
			areqEntity.setStsId					( AmAreqStatusId.PendingRemovalRequest.getStatusId() );
			areqEntity.setDelTimestamp			( systemDate );
			areqEntity.setDelUserNm				( paramInfo.getUser() );
			areqEntity.setDelUserId				( paramInfo.getUserId() );
			areqEntity.setDelStsId				( StatusFlg.off );

			// am_areq_file 論理削除
			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );
			IAreqFileEntity fileEntity = new AreqFileEntity();
			fileEntity.setDelStsId( StatusFlg.off );
			support.getAreqFileDao().update( fileCondition.getCondition(), fileEntity );

			// am_areq_binary_file 論理削除
			AreqBinaryFileCondition binaryFileCondition = new AreqBinaryFileCondition();
			binaryFileCondition.setAreqId( areqEntity.getAreqId() );
			IAreqBinaryFileEntity binaryFileEntity = new AreqBinaryFileEntity();
			binaryFileEntity.setDelStsId( StatusFlg.off );
			support.getAreqBinaryFileDao().update( binaryFileCondition.getCondition(), binaryFileEntity );

			stsId = AmAreqStatusIdForExecData.ReturnToPendingRemovalRequest.getStatusId();
		}

		support.getAreqDao().update( areqEntity );

		support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqEntity.getAreqId());

		support.getSmFinderSupport().registerExecDataSts( paramBean.getProcId(), AmTables.AM_AREQ, stsId, areqEntity.getAreqId());

		paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

		if ( DesignSheetUtils.isRecord() ) {
			// 履歴用Dto設定
			areqDto.setAreqEntity(areqEntity);
			areqDto.setAreqFileEntities(support.findAreqFileEntities(areqEntity.getAreqId()));
			areqDto.setAreqBinaryFileEntities(support.findAreqBinaryFileEntities(areqEntity.getAreqId()));
			areqDto.setAreqAttachedFileEntities(support.findAreqAttachedFileEntities(areqEntity.getAreqId()));

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Cancel.getStatusId() );

			support.getAreqHistDao().insert( histEntity , areqDto);
		}
		return serviceDto;
	}

}
