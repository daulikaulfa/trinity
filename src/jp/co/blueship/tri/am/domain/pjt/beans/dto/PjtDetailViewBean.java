package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・変更管理詳細用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtDetailViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理詳細情報 */
	private PjtDetailBean pjtDetailBean = null;
	
	
	public PjtDetailBean getPjtDetailBean() {
		if ( null == pjtDetailBean ) {
			pjtDetailBean = new PjtDetailBean();
		}
		return pjtDetailBean;
	}
	public void setPjtDetailBean( PjtDetailBean pjtDetailBean ) {
		this.pjtDetailBean = pjtDetailBean;
	}	
}
