package jp.co.blueship.tri.am.domain.pjt.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 変更管理単位でステータスを承認済みに更新します。<br>
 *
 * @version V3L10.02
 *
 * @version SP20150701_V3L13R01
 * @author Takashi Ono
 */
public class ActionChangePjtApprove extends ActionPojoAbstract<FlowChaLibPjtApproveServiceBean>  {
	private AmFinderSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveServiceBean> execute( IServiceDto<FlowChaLibPjtApproveServiceBean> serviceDto ) {
		FlowChaLibPjtApproveServiceBean paramBean = serviceDto.getServiceBean();
		LogBusinessFlow logService = paramBean.getLogService();

		final String  logString = " Change Approval ";
		logService.writeLogWithDateBegin(logString);

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		try {
			IPjtAvlDto pjtAvlDto = paramInfo.getPjtAvlDto();
			IPjtEntity pjtEntity = pjtAvlDto.getPjtEntity();
			String pjtAvlSeqNo = this.support.nextvalByPjtAvlSeqNo();

			this.updatePjtEntity( paramInfo, pjtAvlSeqNo );
			this.insertPjtAvlEntity( paramInfo, pjtAvlSeqNo );

			pjtAvlDto = this.support.findPjtAvlDtoByLatest( pjtEntity.getPjtId() );
			paramInfo.setPjtAvlDto( pjtAvlDto );
			paramList.add( pjtAvlDto );

			if ( DesignSheetUtils.isRecord() ){
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.none.getStatusId() );

				support.getPjtHistDao().insert( histEntity , pjtEntity);
			}

			return serviceDto;

		} finally {
			logService.writeLogWithDateEnd(logString);
		}
	}

	private void updatePjtEntity( IApproveParamInfo paramInfo, String pjtAvlSeqNo ) {
		IPjtAvlDto pjtAvlDto = paramInfo.getPjtAvlDto();
		IPjtEntity pjtEntity = pjtAvlDto.getPjtEntity();

		IPjtEntity updPjtEntity = new PjtEntity();
		updPjtEntity.setPjtId( pjtEntity.getPjtId() );
		updPjtEntity.setPjtLatestAvlSeqNo( pjtAvlSeqNo );
		support.getPjtDao().update( updPjtEntity );

	}

	private void insertPjtAvlEntity( IApproveParamInfo paramInfo, String pjtAvlSeqNo ) {
		Timestamp systemDate	= TriDateUtils.getSystemTimestamp();

		IPjtAvlDto pjtAvlDto = paramInfo.getPjtAvlDto();
		IPjtEntity pjtEntity = pjtAvlDto.getPjtEntity();

		IPjtAvlEntity pjtAvlEntity = new PjtAvlEntity();

		pjtAvlEntity.setPjtId					( pjtEntity.getPjtId() );
		pjtAvlEntity.setPjtAvlSeqNo				( pjtAvlSeqNo );
		pjtAvlEntity.setPjtAvlCmt				( paramInfo.getComment() );
		pjtAvlEntity.setPjtAvlTimestamp			( systemDate );
		pjtAvlEntity.setPjtAvlCancelUserNm		( paramInfo.getUser() );
		pjtAvlEntity.setPjtAvlUserId			( paramInfo.getUserId() );
		pjtAvlEntity.setPjtAvlUserNm			( paramInfo.getUser() );
		pjtAvlEntity.setPjtAvlCancelCmt			( null );
		pjtAvlEntity.setPjtAvlCancelTimestamp	( null );
		pjtAvlEntity.setPjtAvlCancelUserNm		( null );
		pjtAvlEntity.setPjtAvlCancelUserId		( null );
		pjtAvlEntity.setStsId					( AmPjtStatusId.ChangePropertyApproved.getStatusId() );

		List<IPjtAvlAreqLnkEntity> pjtApplyEntityList = new ArrayList<IPjtAvlAreqLnkEntity>();
		List<IAreqEntity> areqEntityList = new ArrayList<IAreqEntity>();

		for ( IAreqDto assetApplyEntity: paramInfo.getAssetApplyEntities() ) {
			IPjtAvlAreqLnkEntity areqLnkEntity = new PjtAvlAreqLnkEntity();

			areqLnkEntity.setAreqId		( assetApplyEntity.getAreqEntity().getAreqId() );
			areqLnkEntity.setPjtId		( pjtEntity.getPjtId() );
			areqLnkEntity.setPjtAvlSeqNo( pjtAvlSeqNo );

			pjtApplyEntityList.add( areqLnkEntity );

			IAreqEntity areqEntity = new AreqEntity();
			areqEntity.setAreqId( assetApplyEntity.getAreqEntity().getAreqId() );
			areqEntity.setPjtAvlSeqNo( pjtAvlSeqNo );
			areqEntityList.add( areqEntity );
		}

		this.support.getPjtAvlDao().insert( pjtAvlEntity );
		this.support.getPjtAvlAreqLnkDao().insert( pjtApplyEntityList );
		this.support.getAreqDao().update( areqEntityList );
	}
}
