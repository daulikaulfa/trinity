package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.beans.mail.dto.PjtMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;

/**
 * 申請情報のステータスを承認エラーに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeApplyInfoApproveError extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService errorMail = null;
	public void setErrorMail( MailGenericService errorMail ) {
		this.errorMail = errorMail;
	}

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		IAreqEntity areqEntity = areqDto.getAreqEntity();

		// エラーがなければ脱出
		if ( paramInfo.getProcessResult() ) {
			this.support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId(), AmTables.AM_AREQ, areqEntity.getAreqId() );

			return serviceDto;
		}

		String stsId = null;

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			stsId = AmAreqStatusIdForExecData.CheckinRequestApprovalError.getStatusId();
		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			stsId = AmAreqStatusIdForExecData.RemovalRequestError.getStatusId();
		}

		this.support.getSmFinderSupport().updateExecDataSts(
				paramBean.getProcId(), AmTables.AM_AREQ, stsId, areqEntity.getAreqId() );


		//エラー時、メールを送信
		try {
			PjtMailServiceBean errorMailBean = null;
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>();

			if ( null != errorMail ) {
				errorMailBean = new PjtMailServiceBean();
				TriPropertyUtils.copyProperties(errorMailBean, paramBean);
				mailServiceDto.setServiceBean(errorMailBean);

				errorMailBean.setPjtAvlDto( paramInfo.getPjtAvlDto() );
				errorMail.execute( mailServiceDto );
			}
		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			String flowAction = (TriStringUtils.isEmpty(paramBean))? "": paramBean.getFlowAction();
			flowAction = (TriStringUtils.isEmpty( flowAction ))? "": flowAction;
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , flowAction ));
		}
		return serviceDto;
	}

}
