package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理起票・変更管理更新確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtModifyServiceConfirm implements IDomain<FlowChaLibPjtModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtModifyServiceBean> execute( IServiceDto<FlowChaLibPjtModifyServiceBean> serviceDto ) {

		FlowChaLibPjtModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_MODIFY_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_MODIFY_CONFIRM ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.PJT_MODIFY_CONFIRM )) {

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( ChaLibScreenID.PJT_MODIFY_CONFIRM )) {

					String selectedPjtNo = paramBean.getSelectedPjtNo();
					AmItemChkUtils.checkPjtNo( selectedPjtNo );

					if( paramBean.isStatusMatrixV3() ) {
						IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo );
						IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit( selectedPjtNo, 1, 0, null ).getEntities().toArray(new IAreqEntity[0]);
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( support.getAssetApplyNo( applyList ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( ChaLibScreenID.PJT_MODIFY_CONFIRM )) {
				if( paramBean.isStatusMatrixV3() ) {
					String selectedPjtNo = paramBean.getSelectedPjtNo();
					IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo );
					IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit( selectedPjtNo, 1, 0, null ).getEntities().toArray(new IAreqEntity[0]);
	
					StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( support.getAssetApplyNo( applyList ) );
	
					StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
				}
				
				PjtDetailInputBean inputBean	= paramBean.getPjtDetailInputBean();
				PjtDetailViewBean viewBean		= new PjtDetailViewBean();

				viewBean.setPjtDetailBean( inputBean.getPjtDetailBean() );

				paramBean.setPjtDetailViewBean( viewBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005117S , e );
		}
	}
}
