package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;

/**
 * 指定された返却資産に関連する貸出／返却申請一覧のエンティティーを格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IEhAssetRelatedEntity extends Serializable {

	/**
	 * 返却申請に関連する資産申請エンティティを取得します。
	 * @return 取得した値を戻します。
	 */
	public AreqDtoList getAssetRelatedReturnEntity();
	/**
	 * 返却申請に関連する資産申請エンティティを設定します。
	 * @param entitys 返却申請情報エンティティ
	 */
	public void setAssetRelatedReturnEntity( AreqDtoList entitys );

}
