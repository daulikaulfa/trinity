package jp.co.blueship.tri.am.domain.pjt.dto;

import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibPjtApproveAssetApplyDetailViewServiceBean  extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * 「詳細表示画面」への項目を保持します。
	 */
	ChaLibPjtApproveAssetApplyDetailViewServiceBean chaLibPjtApproveAssetApplyDetailViewServiceBean = null ;

	/**
	 * 「Ｄｉｆｆ比較画面」への項目を保持します。
	 */
	ChaLibPjtApproveAssetApplyResourceDiffServiceBean chaLibPjtApproveAssetApplyResourceDiffServiceBean = null ;


	public ChaLibPjtApproveAssetApplyDetailViewServiceBean getChaLibPjtApproveAssetApplyDetailViewServiceBean() {
		return chaLibPjtApproveAssetApplyDetailViewServiceBean;
	}
	public void setChaLibPjtApproveAssetApplyDetailViewServiceBean(
			ChaLibPjtApproveAssetApplyDetailViewServiceBean chaLibPjtApproveAssetApplyDetailViewServiceBean) {
		this.chaLibPjtApproveAssetApplyDetailViewServiceBean = chaLibPjtApproveAssetApplyDetailViewServiceBean;
	}
	public ChaLibPjtApproveAssetApplyResourceDiffServiceBean getChaLibPjtApproveAssetApplyResourceDiffServiceBean() {
		return chaLibPjtApproveAssetApplyResourceDiffServiceBean;
	}
	public void setChaLibPjtApproveAssetApplyResourceDiffServiceBean(
			ChaLibPjtApproveAssetApplyResourceDiffServiceBean chaLibPjtApproveAssetApplyResourceDiffServiceBean) {
		this.chaLibPjtApproveAssetApplyResourceDiffServiceBean = chaLibPjtApproveAssetApplyResourceDiffServiceBean;
	}

}
