package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 変更管理・承認対象詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibPjtApproveWaitAssetApplyDetailViewService implements IDomain<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> execute(IServiceDto<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> serviceDto) {

		FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean paramBeanChild = paramBean.getChaLibPjtApproveWaitAssetApplyDetailViewServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (!referer.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW)
					&& !forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW)) {
				return serviceDto;
			}

			if (!forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW)) {
				return serviceDto;
			}
			String applyNo = paramBeanChild.getApplyNo();
			AmItemChkUtils.checkApplyNo(applyNo);

			// 変更管理情報
			IAreqEntity assetApplyEntity = this.support.findAreqEntity(applyNo);
			List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(applyNo);
			List<IAreqBinaryFileEntity> areqBinaryFileEntities = this.support.findAreqBinaryFileEntities(applyNo);
			List<IAreqAttachedFileEntity> areqAttachedFileEntities = this.support.findAreqAttachedFileEntities(applyNo);
			IPjtEntity pjtEntity = this.support.findPjtEntity(assetApplyEntity.getPjtId());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity(assetApplyEntity);
			areqDto.setAreqFileEntities(areqFileEntities);
			areqDto.setAreqBinaryFileEntities(areqBinaryFileEntities);

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			List<Object> paramList = new ArrayList<Object>();
			paramList.add(lotDto);

			paramBeanChild.setApplyNo(applyNo);
			paramBeanChild.setPjtApproveEnabled(AmBusinessJudgUtils.isPjtApproveEnabled(assetApplyEntity));

			if (!AmBusinessJudgUtils.isDeleteApply(assetApplyEntity)) {
				paramBeanChild.setChaLibEntryBaseInfoBean(AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByReturnAsset(pjtEntity,
						assetApplyEntity, areqAttachedFileEntities));
			} else {
				paramBeanChild.setChaLibEntryBaseInfoBean(AmCnvEntityToDtoUtils
						.convertChaLibEntityBaseInfoBeanByDelApply(pjtEntity, assetApplyEntity));
			}
			paramBeanChild.setChaLibEntryAssetResourceInfoBean(AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByReturnAsset(lotDto,
					areqDto));

			this.support.setFileStatusByAssetResourceInfoBean(paramBeanChild.getChaLibEntryAssetResourceInfoBean(), paramList, areqDto, true);

			this.support.setDiffEnabled(paramBeanChild.getChaLibEntryAssetResourceInfoBean());

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005112S, e);
		}
	}
}
