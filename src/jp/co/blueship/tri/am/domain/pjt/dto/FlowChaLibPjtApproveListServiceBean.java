package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理承認・変更管理承認一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtApproveListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;
	/** 承認履歴情報 */
	private List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public List<PjtApproveHistoryViewBean> getPjtApproveHistoryViewBeanList() {
		if ( null == pjtApproveHistoryViewBeanList ) {
			pjtApproveHistoryViewBeanList = new ArrayList<PjtApproveHistoryViewBean>();
		}
		return pjtApproveHistoryViewBeanList;
	}
	public void setPjtApproveHistoryViewBeanList(
					List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList ) {
		this.pjtApproveHistoryViewBeanList = pjtApproveHistoryViewBeanList;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
}

