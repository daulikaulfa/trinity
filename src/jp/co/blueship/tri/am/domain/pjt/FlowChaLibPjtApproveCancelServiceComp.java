package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.RelatedApplyInfoViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.EhAssetRelatedEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理承認取消完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveCancelServiceComp implements IDomain<FlowChaLibPjtApproveCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> actions = null;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveCancelServiceBean> execute( IServiceDto<FlowChaLibPjtApproveCancelServiceBean> serviceDto ) {

		FlowChaLibPjtApproveCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL )) {

			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL )) {

				String selectedPjtNo = paramBean.getSelectedPjtNo();
				AmItemChkUtils.checkPjtNo( selectedPjtNo );

				String pjtAvlSeqNo = paramBean.getPjtAvlSeqNo();
				//AmItemChkUtils.checkApproveDate( pjtAvlSeqNo );

				// 承認履歴
				IPjtAvlDto pjtAvlDto = this.support.findPjtAvlDto( selectedPjtNo, pjtAvlSeqNo );

				// 取消対象申請情報
				AreqDtoList assetApplyEntities =
					this.support.getAssetApplyEntities( paramBean.getApplyInfoViewBeanList() );

				// 関連申請情報
				Map<String, List<RelatedApplyInfoViewBean>> map =
					getApplyNoApplyInfoViewPjtDetailBeanList( paramBean.getRelatedApplyInfoViewBeanList() );


				IApproveParamInfo paramInfo = new ApproveParamInfo();
				paramInfo.setPjtAvlDto			( pjtAvlDto );
				paramInfo.setAssetApplyEntities	( assetApplyEntities );
				paramInfo.setUser				( paramBean.getUserName() );
				paramInfo.setUserId				( paramBean.getUserId() );
				paramInfo.setComment			(
						paramBean.getPjtEditInputBean().getApproveCancelComment() );

				for ( String applyNo : map.keySet() ) {

					paramInfo.getApplyNoAssetRelatedEntityMap().put(
							applyNo, getAssetRelatedEntity( map.get( applyNo )));

				}

				ILotDto lotDto = this.support.findLotDto( pjtAvlDto.getPjtEntity().getLotId() );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				List<Object> paramList = new ArrayList<Object>();
				paramList.add( pjtAvlDto );
				paramList.add( lotDto );
				paramList.add(
						this.support.findVcsReposDtoFromLotMdlLnkEntities(
								lotDto.getIncludeMdlEntities( true )).toArray( new IVcsReposDto[0] ));
				paramList.add( paramInfo );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}

			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005123S , e );
		}
	}

	/**
	 * 関連申請情報のリストを、関連元申請番号でグループ化する。
	 * @param relatedApplyInfoViewBeanList 関連申請情報のリスト
	 * @return 関連元申請番号-関連申請情報リストのマップ
	 */
	private Map<String, List<RelatedApplyInfoViewBean>>
		getApplyNoApplyInfoViewPjtDetailBeanList(
				List<RelatedApplyInfoViewBean> relatedApplyInfoViewBeanList ) {

		Map<String, List<RelatedApplyInfoViewBean>> map =
			new TreeMap<String, List<RelatedApplyInfoViewBean>>();

		for ( RelatedApplyInfoViewBean viewBean : relatedApplyInfoViewBeanList ) {

			// 関連元申請番号
			String origApplyNo = viewBean.getOrigApplyNo();

			if ( !map.containsKey( origApplyNo )) {
				map.put( origApplyNo, new ArrayList<RelatedApplyInfoViewBean>() );
			}
			map.get( origApplyNo ).add( viewBean );
		}

		return map;
	}

	/**
	 * 関連申請情報から関連資産エンティティを作成する。
	 * @param relatedApplyInfoViewBeanList 関連申請情報のリスト
	 * @return 関連資産エンティティ
	 */
	private IEhAssetRelatedEntity getAssetRelatedEntity(
			List<RelatedApplyInfoViewBean> relatedApplyInfoViewBeanList ) {

		List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList =
								new ArrayList<ApplyInfoViewPjtDetailBean>();

		for ( RelatedApplyInfoViewBean viewBean : relatedApplyInfoViewBeanList ) {
			applyInfoViewBeanList.add( (ApplyInfoViewPjtDetailBean)viewBean );
		}

		AreqDtoList assetApplyEntities =
			this.support.getAssetApplyEntities( applyInfoViewBeanList );

		IEhAssetRelatedEntity relatedEntity = new EhAssetRelatedEntity();
		relatedEntity.setAssetRelatedReturnEntity( assetApplyEntities );

		return relatedEntity;
	}
}
