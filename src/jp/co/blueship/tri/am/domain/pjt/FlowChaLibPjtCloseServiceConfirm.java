package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 変更管理起票・変更管理クローズ確認画面の表示情報設定Class<br>
 * <br>
 *
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 */
public class FlowChaLibPjtCloseServiceConfirm implements IDomain<FlowChaLibPjtCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeActionByTestComplete = null;

	public void setSupport(FlowChaLibPjtEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setStatusIfNotMergeMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeAction = action;
	}

	public void setStatusIfNotMergeMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeActionByTestComplete = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtCloseServiceBean> execute(IServiceDto<FlowChaLibPjtCloseServiceBean> serviceDto) {

		FlowChaLibPjtCloseServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType = paramBean.getScreenType();

			if (!refererID.equals(ChaLibScreenID.PJT_CLOSE_CONFIRM) && !forwordID.equals(ChaLibScreenID.PJT_CLOSE_CONFIRM)) {
				return serviceDto;
			}

			String selectedPjtNo = paramBean.getSelectedPjtNo();

			if (refererID.equals(ChaLibScreenID.PJT_CLOSE_CONFIRM)) {

				PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

				if (ScreenType.next.equals(screenType)) {

					IPjtEntity pjtEntity = this.support.findPjtEntity( selectedPjtNo );
					ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
					IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit(selectedPjtNo, 1, 0, AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
							.toArray(new IAreqEntity[0]);

					AmItemChkUtils.checkPjtClose(pjtEditInputBean, applyList);

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
							.setServiceBean( paramBean )
							.setFinder( support )
							.setActionList( statusMatrixAction )
							.setLotIds( pjtEntity.getLotId() )
							.setPjtIds( selectedPjtNo )
							.setAreqIds( support.getAssetApplyNo(applyList) );
	
						if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
							if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
								// テスト完了運用を行う
								statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
							} else {
								// テスト完了運用を行わない
								statusDto.setActionList( statusMatrixIfNotMergeAction );
							}
						}
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				paramBean.setPjtEditInputBean(pjtEditInputBean);
			}

			if (forwordID.equals(ChaLibScreenID.PJT_CLOSE_CONFIRM)) {

				IPjtEntity pjtEntity = this.support.findPjtEntity(selectedPjtNo);
				ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
				ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );
				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				AmItemChkUtils.checkPjtNo(selectedPjtNo);

				PjtDetailViewBean pjtDetailViewBean = AmViewInfoAddonUtils.getPjtDetailViewBean(lotDto.getLotEntity(), pjtEntity);

				paramBean.setPjtDetailViewBean(pjtDetailViewBean);

				int selectPageNo = (0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo();

				IEntityLimit<IAreqEntity> entityLimit = this.support.getAssetApplyEntityLimit(selectedPjtNo, selectPageNo,
						sheet.intValue(AmDesignEntryKeyByChangec.maxPageNumberByPjtCloseConfirm),
						AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm());

				IAreqEntity[] applyList = this.support
						.getAssetApplyEntityLimit(selectedPjtNo, 1, 0,
								AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
						.toArray(new IAreqEntity[0]);

				if (0 == applyList.length) {
					throw new ContinuableBusinessException(AmMessageId.AM001008E);
				}

				if( paramBean.isStatusMatrixV3() ) {
					StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( support.getAssetApplyNo(applyList) );
	
					if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
						if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
							// テスト完了運用を行う
							statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
						} else {
							// テスト完了運用を行わない
							statusDto.setActionList( statusMatrixIfNotMergeAction );
						}
					}
	
					StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
				}

				setServiceBeanSearchResult(entityLimit, selectPageNo, paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005130S, e);
		}
	}

	/**
	 * Beanに検索結果を設定する。
	 *
	 * @param limit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibPjtCloseServiceBeanオブジェクト
	 */
	private void setServiceBeanSearchResult(IEntityLimit<IAreqEntity> limit, int selectPageNo, FlowChaLibPjtCloseServiceBean paramBean) {

		AreqDtoList areqDtoList = this.support.findAreqDtoList( limit.getEntities() );
		paramBean.setApplyInfoViewBeanList( this.support.getApplyInfoViewPjtDetailBean(paramBean.getUserId(), areqDtoList) );
		paramBean.setPageInfoView(AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit()));
		paramBean.setSelectPageNo(selectPageNo);

	}
}
