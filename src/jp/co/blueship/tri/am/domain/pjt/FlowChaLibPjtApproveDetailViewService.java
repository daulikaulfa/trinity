package jp.co.blueship.tri.am.domain.pjt;

import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlCondition;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 * 変更管理・承認詳細画面の表示情報設定Class<br>
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Norheda
 */
public class FlowChaLibPjtApproveDetailViewService implements IDomain<FlowChaLibPjtApproveDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;
	private IUmFinderSupport umFinderSupport;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveDetailViewServiceBean> execute( IServiceDto<FlowChaLibPjtApproveDetailViewServiceBean> serviceDto ) {

		FlowChaLibPjtApproveDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedPjtNo	= paramBean.getSelectedPjtNo();
			AmItemChkUtils.checkPjtNo( selectedPjtNo );

			// 変更管理情報
			IPjtEntity pjtEntity	= this.support.findPjtEntity( selectedPjtNo, (StatusFlg)null );
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					umFinderSupport.findGroupByUserId(paramBean.getUserId()));

			setServiceBeanPjtInfo( paramBean, lotDto.getLotEntity(), pjtEntity );

			// 申請情報
			AreqDtoList areqDtoList = getAssetApplyEntity( selectedPjtNo );

			paramBean.setSelectedLotNo( pjtEntity.getLotId() ) ;
			paramBean.setSelectedPjtNo( selectedPjtNo ) ;

			paramBean.setApplyInfoViewBeanList(
					this.support.getApplyInfoViewPjtDetailBean( paramBean.getUserId() , areqDtoList ));

			// 承認履歴情報
			List<IPjtAvlDto> pjtDtoList = support.findPjtAvlDto( getPjtApproveEntity(selectedPjtNo) );

			paramBean.setPjtApproveHistoryViewBeanList(
					this.support.getPjtApproveHistoryViewBean( pjtDtoList ));

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005108S , e );
		}
	}

	/**
	 * 申請情報を取得する
	 * @param selectedPjtNo 変更管理番号
	 * @param selectPageNo 選択ページ
	 * @return 申請情報
	 */
	private AreqDtoList getAssetApplyEntity( String selectedPjtNo ) {

		IJdbcCondition condition =
			AmDBSearchConditionAddonUtils.getAreqConditionByOverPjtApprove( selectedPjtNo );

		ISqlSort sort =
			AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtApproveDetailView();

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find( condition.getCondition(), sort );

		return this.support.findAreqDtoList( areqEntities );

	}

	/**
	 * Beanに変更管理情報を設定する。
	 * @param paramBean  FlowChaLibPjtApproveDetailViewServiceBeanオブジェクト
	 * @param lotEntity ロットエンティティ
	 * @param pjtEntity 変更管理エンティティ
	 */
	private void setServiceBeanPjtInfo(
			FlowChaLibPjtApproveDetailViewServiceBean paramBean,
			ILotEntity lotEntity, IPjtEntity pjtEntity ) {

		PjtDetailViewBean pjtDetailViewBean =
			AmViewInfoAddonUtils.getPjtDetailViewBean( lotEntity, pjtEntity );

		paramBean.setPjtDetailViewBean( pjtDetailViewBean );

	}

	/**
	 * 変更承認情報を取得する
	 *
	 * @param selectedPjtNo 変更管理番号
	 * @return 申請情報
	 */
	public List<IPjtAvlEntity> getPjtApproveEntity(String selectedPjtNo) {

		PjtAvlCondition condition = new PjtAvlCondition();
		condition.setPjtId(selectedPjtNo);
		AmDBSearchConditionAddonUtils.getAreqCondition(selectedPjtNo);

		ISqlSort sort = AmDBSearchSortAddonUtils.getPjtApproveSortFromDesignDefineByChaLibPjtApproveDetailView();

		List<IPjtAvlEntity> entities = this.support.getPjtAvlDao().find(condition.getCondition(), sort);

		return entities;

	}

}
