package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 申請情報のステータスを承認取消済みに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeApplyInfoApproveCancel extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}
		IAreqEntity areqEntity = areqDto.getAreqEntity();

		IPjtAvlDto PjtApprove = AmExtractEntityAddonUtils.extractPjtDtoApprove( paramList );
		if ( null == PjtApprove.getPjtAvlEntity() ) {
			throw new TriSystemException(AmMessageId.AM005034S);
		}

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			areqEntity.setStsId				( AmAreqStatusId.CheckinRequested.getStatusId() );

		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			areqEntity.setStsId				( AmAreqStatusId.RemovalRequested.getStatusId() );
		}
		support.getAreqDao().update( areqEntity );

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqEntity.getAreqId());

		if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {
			support.getSmFinderSupport().registerExecDataSts(
					paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.CheckinApprovalCancelled, areqEntity.getAreqId());

		} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity )) {
			support.getSmFinderSupport().registerExecDataSts(
					paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.RemovalApprovalCancelled, areqEntity.getAreqId());
		}

		//承認履歴は、必ず記録する
		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Reject.getStatusId() );

			support.getAreqHistDao().insert( histEntity , areqDto);
		}
		return serviceDto;
	}
}
