package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;


/**
 * 指定された返却資産に関連する貸出／返却申請一覧のエンティティーを格納したクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhAssetRelatedEntity implements IEhAssetRelatedEntity {

	private static final long serialVersionUID = 2L;

	private AreqDtoList assetRelatedReturnEntitys = null;

	public final AreqDtoList getAssetRelatedReturnEntity() {
		return assetRelatedReturnEntitys;
	}
	public final void setAssetRelatedReturnEntity( AreqDtoList entitys) {
		assetRelatedReturnEntitys = entitys;
	}

}
