package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理・承認済み申請詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveAssetApplyDetailViewService implements IDomain<FlowChaLibPjtApproveAssetApplyDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveAssetApplyDetailViewServiceBean> execute( IServiceDto<FlowChaLibPjtApproveAssetApplyDetailViewServiceBean> serviceDto ) {

		FlowChaLibPjtApproveAssetApplyDetailViewServiceBean paramBean	= null;

		try {

			paramBean	= serviceDto.getServiceBean();

			ChaLibPjtApproveAssetApplyDetailViewServiceBean paramBeanChild = paramBean.getChaLibPjtApproveAssetApplyDetailViewServiceBean() ;

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.PJT_APPROVE_ASSET_APPLY_DETAIL_VIEW ) &&
					!forward.equals( ChaLibScreenID.PJT_APPROVE_ASSET_APPLY_DETAIL_VIEW )) {
				return serviceDto;
			}

			if( forward.equals( ChaLibScreenID.PJT_APPROVE_ASSET_APPLY_DETAIL_VIEW ) ) {

				String applyNo	= paramBeanChild.getApplyNo();
				AmItemChkUtils.checkApplyNo( applyNo );

				// 変更管理情報
				IAreqEntity assetApplyEntity	= this.support.findAreqEntity( applyNo );
				List<IAreqFileEntity> areqFileEntities =  this.support.findAreqFileEntities( applyNo );
				List<IAreqBinaryFileEntity> areqBinaryFileEntities =  this.support.findAreqBinaryFileEntities( applyNo );
				List<IAreqAttachedFileEntity> areqAttachedFileEntities	= this.support.findAreqAttachedFileEntities( applyNo );
				IPjtAvlDto pjtDto = new PjtAvlDto();
				IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() ) ;
				pjtDto.setPjtEntity( pjtEntity );
				pjtDto.setPjtAvlEntity( this.support.findPjtAvlEntity( pjtEntity.getPjtId(), pjtEntity.getPjtLatestAvlSeqNo() ) );

				ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

				IAreqDto areqDto = new AreqDto();
				areqDto.setAreqEntity( assetApplyEntity );
				areqDto.setAreqFileEntities(areqFileEntities);
				areqDto.setAreqBinaryFileEntities(areqBinaryFileEntities);


				paramBeanChild.setLotNo( lotDto.getLotEntity().getLotId() ) ;

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				List<Object> paramList = new ArrayList<Object>();
				paramList.add( lotDto.getLotEntity() );

				paramBeanChild.setBaseInfoViewBean(
						AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByPjtApprove(
								pjtDto ,
								assetApplyEntity,
								areqAttachedFileEntities ) );
				paramBeanChild.setAssetSelectViewBean(
						AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByReturnAsset( lotDto, areqDto ));

				paramBeanChild.setStatusView(
								sheet.getValue( AmDesignBeanId.statusId, assetApplyEntity.getProcStsId() ));

			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf(TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005107S , e );
		}
	}
}
