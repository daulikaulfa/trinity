package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・変更管理詳細用
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class PjtDetailBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 変更要因分類 */
	private String changeCauseClassify = null;
	/** 変更管理概要 */
	private String pjtSummary;
	/** 変更管理内容 */
	private String pjtContent;
	/** 変更管理ステータス */
	private String pjtStatus;
	/** テスト完了有効フラグ */
	private boolean isEnableTestComplete;

	/** SubmitterName */
	private String submitterNm;

	/** Assignee Id */
	private String assigneeId;

	/** Assignee Name */
	private String assigneeNm;

	/** Category Id */
	private String ctgId;

	/** Category Name */
	private String ctgNm;

	/** Milestone Id */
	private String mstoneId;

	/** Milestone Name */
	private String mstoneNm;



	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getChangeCauseClassify() {
		return changeCauseClassify;
	}
	public void setChangeCauseClassify( String changeCauseClassify ) {
		this.changeCauseClassify = changeCauseClassify;
	}

	public String getPjtSummary() {
		return pjtSummary;
	}
	public void setPjtSummary(String pjtSummary) {
		this.pjtSummary = pjtSummary;
	}

	public String getPjtContent() {
		return pjtContent;
	}
	public void setPjtContent(String pjtContent) {
		this.pjtContent = pjtContent;
	}

	public String getPjtStatus() {
		return pjtStatus;
	}
	public void setPjtStatus( String pjtStatus ) {
		this.pjtStatus = pjtStatus;
	}

	public boolean isEnableTestComplete(){
		return isEnableTestComplete;
	}
	public void setEnableTestComplete(String value) {
		if ( value.equals("0") ){
			this.isEnableTestComplete = false;
		}else{
			this.isEnableTestComplete =true;
		}
	}
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}
	public String getSubmitterNm() {
		return submitterNm;
	}
	public void setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
	}
	public String getAssigneeId() {
		return assigneeId;
	}
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public void setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
	}
}
