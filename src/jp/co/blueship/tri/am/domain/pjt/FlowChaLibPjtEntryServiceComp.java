package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmEntityAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 * 変更管理起票・変更管理情報登録完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowChaLibPjtEntryServiceComp implements IDomain<FlowChaLibPjtEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	private IUmFinderSupport umSupport;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
		this.umSupport = this.support.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowChaLibPjtEntryServiceBean> execute( IServiceDto<FlowChaLibPjtEntryServiceBean> serviceDto ) {

		FlowChaLibPjtEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_ENTRY ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_ENTRY ) ){
				return serviceDto;
			}


			if ( refererID.equals( ChaLibScreenID.COMP_PJT_ENTRY )) {
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_ENTRY )) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					String pjtNo = this.support.getPjtNumberingDao().nextval();
					IPjtEntity entity = new PjtEntity();
					entity.setPjtId( pjtNo );

					PjtDetailInputBean pjtDetailInputBean = paramBean.getPjtDetailInputBean();

					PjtDetailBean pjtDetailBean	= pjtDetailInputBean.getPjtDetailBean();
					pjtDetailBean.setPjtNo( pjtNo );

					AmEntityAddonUtils.setPjtEntityPjtDetailBean(
							entity, pjtDetailBean, paramBean.getUserName() , paramBean.getUserId() );

					this.support.getPjtDao().insert( entity );
					this.umSupport.registerCtgLnk(pjtDetailBean.getCtgId(), AmTables.AM_PJT, pjtDetailBean.getPjtNo());
					this.umSupport.registerMstoneLnk(pjtDetailBean.getMstoneId(), AmTables.AM_PJT, pjtDetailBean.getPjtNo());

					if ( DesignSheetUtils.isRecord() ){
						IHistEntity histEntity = new HistEntity();
						histEntity.setActSts( UmActStatusId.Add.getStatusId() );

						support.getPjtHistDao().insert( histEntity , entity);
					}

					paramBean.setPjtNo( pjtNo );

					// グループの存在チェック
					ILotDto lotDto = this.support.findLotDto( entity.getLotId() );
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));


				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005131S , e );
		}
	}

}
