package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmStatusCheckAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.RelatedApplyInfoViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.bm.BmFluentFunctionUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理取消画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveCancelServiceConfirm implements IDomain<FlowChaLibPjtApproveCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> actions = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibPjtApproveEditSupport support = null;

	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveCancelServiceBean>  execute( IServiceDto<FlowChaLibPjtApproveCancelServiceBean>  serviceDto ) {

		FlowChaLibPjtApproveCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID 	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {
				return serviceDto;
			}

			String selectedPjtNo		= paramBean.getSelectedPjtNo();

			if ( refererID.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {

				PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

				if ( ScreenType.next.equals( screenType )) {

					AmItemChkUtils.checkPjtApproveCancel( pjtEditInputBean );

					IAreqEntity[] relatedAssetApplyEntities =
						getRelatedAssetApplyEntities( paramBean.getRelatedApplyInfoViewBeanList() );

					// 関連資産に返却申請があるかどうかをチェック
					AmStatusCheckAddonUtils.checkPjtApproveCancelByRelatedAssetApply( relatedAssetApplyEntities );

					if( paramBean.isStatusMatrixV3() ) {
						// ステータスのチェックに使用するので、履歴情報ではなく最新を取得
						IPjtEntity pjtEntity = this.support.findPjtEntity( selectedPjtNo );
	
						AreqDtoList areqDtoList = this.support.getAssetApplyEntities( paramBean.getApplyInfoViewBeanList() );
	
						String[] bpIds =
								this.getBpIds( areqDtoList );
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) )
						.setBpIds( bpIds );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				paramBean.setPjtEditInputBean( pjtEditInputBean );
			}


			if ( forwordID.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {

				AmItemChkUtils.checkPjtNo		( selectedPjtNo );

				//String selectedApproveDate = paramBean.getSelectedApproveDate();
				//AmItemChkUtils.checkApproveDate	( selectedApproveDate );
				String selectedPjtAvlSeqNo = paramBean.getPjtAvlSeqNo();

				// 変更情報設定
				IPjtAvlDto pjtAvlDto = this.support.findPjtAvlDto(selectedPjtNo, selectedPjtAvlSeqNo);

				// ロット設定
				String lotId			= pjtAvlDto.getPjtEntity().getLotId();
				ILotDto lotDto = this.support.findLotDto( lotId );

				if ( ! AmBusinessJudgUtils.voidableApprovalByPjt( lotDto.getLotEntity() ) ) {
					throw new BusinessException( AmMessageId.AM005124S );
				}

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				// 変更管理情報
				setServiceBeanPjtInfo( paramBean, lotDto.getLotEntity(), pjtAvlDto.getPjtEntity() );

				// 承認された(取消対象の)申請情報
				AreqDtoList areqDtoList = getApprovedAssetApplyEntity( pjtAvlDto );

				// 承認履歴情報
				List<IPjtAvlDto> pjtDtos = new ArrayList<IPjtAvlDto>();
				pjtDtos.add( pjtAvlDto );
				paramBean.setPjtApproveHistoryViewBeanList(
						this.support.getPjtApproveHistoryViewBean( pjtDtos ) );

				// 承認された(取消対象の)申請情報
				paramBean.setApplyInfoViewBeanList(
						this.support.getApplyInfoViewPjtDetailBean( paramBean.getUserId() , areqDtoList ));


				// 関連申請情報取得
				List<Object> paramList = new ArrayList<Object>();
				paramList.add( lotDto );
				paramList.add( getRelatableAssetApplyEntities( lotId ) );
				IApproveParamInfo paramInfo = new ApproveParamInfo();
				paramInfo.setPjtAvlDto			( pjtAvlDto );
				paramInfo.setAssetApplyEntities	( areqDtoList );
				paramList.add( paramInfo );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}


				// 結果の取り出し
				paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
				Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap =
										paramInfo.getApplyNoAssetRelatedEntityMap();
				setServiceBeanRelatedApplyInfo( paramBean, applyNoAssetRelatedEntityMap );

				// 関連資産に返却申請があるかどうかをチェック
				AmStatusCheckAddonUtils.checkPjtApproveCancelByBeforeConfirm( applyNoAssetRelatedEntityMap );

			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005124S , e );
		}
	}

	/**
	 * 承認された申請情報を取得する
	 * @param pjtAvlDto 変更管理承認履歴エンティティ
	 * @return 承認された申請情報
	 */
	private AreqDtoList getApprovedAssetApplyEntity( IPjtAvlDto pjtAvlDto ) {

		List<String> applyNoList = new ArrayList<String>();
		for ( IPjtAvlAreqLnkEntity applyEntity : pjtAvlDto.getPjtAvlAreqLnkEntityList() ) {
			applyNoList.add( applyEntity.getAreqId() );
		}

		IJdbcCondition condition =
			AmDBSearchConditionAddonUtils.getAreqConditionByApplyNo(
						(String[])applyNoList.toArray( new String[0] ));

		ISqlSort sort =
			AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtApproveCancelConfirm();

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find( condition.getCondition(), sort );

		return this.support.findAreqDtoList( areqEntities );

	}

	/**
	 * Beanに変更管理情報を設定する。
	 * @param paramBean  FlowChaLibPjtApproveCancelServiceBeanオブジェクト
	 * @param lotEntity ロットエンティティ
	 * @param pjtEntity 変更管理エンティティ
	 */
	private void setServiceBeanPjtInfo(
			FlowChaLibPjtApproveCancelServiceBean paramBean,
			ILotEntity lotEntity, IPjtEntity pjtEntity ) {

		PjtDetailViewBean pjtDetailViewBean =
			AmViewInfoAddonUtils.getPjtDetailViewBean( lotEntity, pjtEntity );

		paramBean.setPjtDetailViewBean( pjtDetailViewBean );

	}

	/**
	 * Beanに関連申請情報を設定する。
	 * @param paramBean  FlowChaLibPjtApproveCancelServiceBeanオブジェクト
	 * @param applyNoAssetRelatedEntityMap 申請番号と関連申請情報のマップ
	 */
	private void setServiceBeanRelatedApplyInfo(
			FlowChaLibPjtApproveCancelServiceBean paramBean,
			Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap ) {

		List<RelatedApplyInfoViewBean> relatedApplyInfoViewlBean =
											new ArrayList<RelatedApplyInfoViewBean>();

		AmViewInfoAddonUtils.setRelatedApplyInfoViewBeanAssetApplyEntity(
				paramBean.getUserId() ,
				relatedApplyInfoViewlBean, applyNoAssetRelatedEntityMap );

		paramBean.setRelatedApplyInfoViewBeanList( relatedApplyInfoViewlBean );

	}

	/**
	 * 申請情報を取得する
	 * @param relatedApplyInfoViewBeanList 関連申請情報のリスト
	 * @return 申請情報
	 */
	private IAreqEntity[] getRelatedAssetApplyEntities(
			List<RelatedApplyInfoViewBean> relatedApplyInfoViewBeanList ) {

		if ( 0 == relatedApplyInfoViewBeanList.size() ) {
			return new IAreqEntity[0];
		}


		List<String> applyNoList = new ArrayList<String>();
		for ( RelatedApplyInfoViewBean vewBean : relatedApplyInfoViewBeanList ) {
			applyNoList.add( vewBean.getApplyNo() );
		}


		IJdbcCondition condition =
			AmDBSearchConditionAddonUtils.getAreqConditionByApplyNo(
					(String[])applyNoList.toArray( new String[0] ));

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find( condition.getCondition() );

		return areqEntities.toArray(new IAreqEntity[0]);

	}

	/**
	 * 関連資産となり得る申請情報を取得する
	 * @param lotId ロット番号
	 * @return 関連資産となり得る申請情報
	 */
	private IAreqDto[] getRelatableAssetApplyEntities( String lotId ) {

		IJdbcCondition condition =
			AmDBSearchConditionAddonUtils.getRelatableAreqCondition( lotId );

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find(
					condition.getCondition(),
					AmDBSearchSortAddonUtils.getAssetApplyRelatedSortFromDesignDefineByChaLibPjtApproveCancelConfirm()
					 );
		List<IAreqDto> areqDtos = new ArrayList<IAreqDto>();
		for ( IAreqEntity entity : areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( entity );
			areqDtos.add( areqDto );
		}

		return areqDtos.toArray(new IAreqDto[0]);

	}


	/**
	 * 申請情報に関連する、ビルドパッケージ情報を取得する
	 *
	 * @param areqDtoList 申請情報のリスト
	 * @return ビルドパッケージ情報
	 */
	private String[] getBpIds(AreqDtoList areqDtoList) {

		String[] areqIds = FluentList.from(areqDtoList).map(AmFluentFunctionUtils.toAreqIdsFromAreqDto).toArray(new String[0]);

		BpAreqLnkCondition bpAreqCondition = new BpAreqLnkCondition();
		bpAreqCondition.setAreqIds( areqIds );
		List<IBpAreqLnkEntity> bpAreqEntities = this.support.getBmFinderSupport().getBpAreqLnkDao().find( bpAreqCondition.getCondition() );

		String[] bpIds = FluentList.from(bpAreqEntities).map(BmFluentFunctionUtils.toBpIdFromBpAreqLnk).toArray(new String[0]);

		return bpIds;

	}

}
