package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.beans.mail.dto.PjtMailServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;

/**
 * １変更管理番号ごとに、処理を別処理に委譲します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionApproveDelegateByPjt extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	private List<IDomain<IGeneralServiceBean>> actions = null;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	private List<IDomain<IGeneralServiceBean>> throwAdvices = null;
	public void setThrowAdvices( List<IDomain<IGeneralServiceBean>> throwAdvices ) {
		this.throwAdvices = throwAdvices;
	}

	/**
	 * 例外発生時に実行されるビジネス処理のリストを取得します。
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<IGeneralServiceBean>> getThrowAdvices() {
		if ( null == throwAdvices )
			throwAdvices = new ArrayList<IDomain<IGeneralServiceBean>>();

		return throwAdvices;
	}

	private List<IDomain<IGeneralServiceBean>> afterAdvices = null;
	public void setAfterAdvices( List<IDomain<IGeneralServiceBean>> afterAdvices ) {
		this.afterAdvices = afterAdvices;
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理のリストを取得します。
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<IGeneralServiceBean>> getAfterAdvices() {
		if ( null == afterAdvices )
			afterAdvices = new ArrayList<IDomain<IGeneralServiceBean>>();

		return afterAdvices;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo[] paramArray =
						AmExtractMessageAddonUtils.getApproveParamInfoArray( paramList );
		if ( null == paramArray ) {
			throw new TriSystemException(AmMessageId.AM005032S);
		}

		IGeneralServiceBean paramBean = null;
		PjtMailServiceBean successMailBean = null;
		IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>();

		try {
			if ( null != successMail ) {
				paramBean = serviceDto.getServiceBean();
				successMailBean = new PjtMailServiceBean();
				TriPropertyUtils.copyProperties(successMailBean, paramBean);
				mailServiceDto.setServiceBean(successMailBean);
			}
		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			paramBean = serviceDto.getServiceBean();
			String flowAction = (TriStringUtils.isEmpty(paramBean))? "": paramBean.getFlowAction();
			flowAction = (TriStringUtils.isEmpty( flowAction ))? "": flowAction;
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , flowAction ));
		}

		for ( IApproveParamInfo param : paramArray ) {

			try {
				paramList.add( param );

				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( serviceDto );
				}

				try {
					if ( null != successMail ) {
						//正常系メール送信
						successMailBean.setPjtAvlDto( param.getPjtAvlDto() );
						successMail.execute( mailServiceDto );
					}
				} catch ( Exception e ) {
					//メール送信が失敗しても処理を続行する
					paramBean = serviceDto.getServiceBean();
					String flowAction = (TriStringUtils.isEmpty(paramBean))? "": paramBean.getFlowAction();
					flowAction = (TriStringUtils.isEmpty( flowAction ))? "": flowAction;
					LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , flowAction ));
				}

			} catch ( BaseBusinessException e ) {

				for ( IDomain<IGeneralServiceBean> throwAdvice : this.getThrowAdvices() ) {
					throwAdvice.execute( serviceDto );
				}

				throw e;

			} catch ( Exception e ) {

				for ( IDomain<IGeneralServiceBean> throwAdvice : this.getThrowAdvices() ) {
					throwAdvice.execute( serviceDto );
				}
				throw new TriSystemException( AmMessageId.AM005079S , e );

			} finally {

				for ( IDomain<IGeneralServiceBean> afterAdvice : this.getAfterAdvices() ) {
					afterAdvice.execute( serviceDto );
				}

				IApproveParamInfo paramOld = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
				paramList.remove( paramOld );

			}
		}

		return serviceDto;
	}

}
