package jp.co.blueship.tri.am.domain.pjt.dto;

import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理・変更情報登録用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String pjtNo = null;
	/** 変更管理詳細入力情報 */
	private PjtDetailInputBean pjtDetailInputBean = null;
	/** 変更管理詳細表示情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;

	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo( String pjtNo ) {
		this.pjtNo = pjtNo;
	}

	public PjtDetailInputBean getPjtDetailInputBean() {
		if ( null == pjtDetailInputBean ) {
			pjtDetailInputBean = new PjtDetailInputBean();
		}
		return pjtDetailInputBean;
	}
	public void setPjtDetailInputBean( PjtDetailInputBean pjtDetailInputBean ) {
		this.pjtDetailInputBean = pjtDetailInputBean;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		if ( null == pjtDetailViewBean ) {
			pjtDetailViewBean = new PjtDetailViewBean();
		}
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}
}

