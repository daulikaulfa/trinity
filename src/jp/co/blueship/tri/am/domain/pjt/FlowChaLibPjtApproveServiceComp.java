package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理承認完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 * 
 */
public class FlowChaLibPjtApproveServiceComp implements IDomain<FlowChaLibPjtApproveServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> actions = null;
	private FlowChaLibPjtApproveEditSupport support = null;

	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveServiceBean> execute( IServiceDto<FlowChaLibPjtApproveServiceBean> serviceDto ) {

		FlowChaLibPjtApproveServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE ) ){
				return serviceDto;
			}

			if ( refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

				}
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String[] targetApplyNo	= paramBean.getTargetApplyNo();
					AmItemChkUtils.checkApplyNo( targetApplyNo );
					String[] selectedPjtId = paramBean.getSelectedPjtNo();
					AmItemChkUtils.checkPjtNo( selectedPjtId );
					AreqDtoList areqDtoList = new AreqDtoList();
					for ( String applyNo : targetApplyNo ) {
						areqDtoList.add( this.support.findAreqDto( applyNo ) );
					}
					// 変更管理情報を取得
					List<IPjtEntity> pjtEntities = this.support.findPjtEntities( selectedPjtId );
					List<IPjtAvlDto> pjtDtoList = new ArrayList<IPjtAvlDto>();
					for ( IPjtEntity pjtEntity: pjtEntities ) {
						IPjtAvlDto pjtDto = new PjtAvlDto();
						pjtDto.setPjtEntity( pjtEntity );
						pjtDtoList.add( pjtDto );
					}
					// ロット番号は同じなので先頭のデータから取得
					ILotDto lotDto = this.support.findLotDto( pjtDtoList.get(0).getPjtEntity().getLotId() );
					paramBean.setCacheLotDto( lotDto );
					Date timestamp = TriDateUtils.convertTimestampToDate(paramBean.getSystemDate());
					paramBean.setLogService( new LogBusinessFlow(timestamp) );


					// グループの存在チェック
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

					List<Object> paramList = new CopyOnWriteArrayList<Object>();
					paramList.add( paramBean );	//インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）
					paramList.add( lotDto );
					paramList.add(
							this.support.findVcsReposDtoFromLotMdlLnkEntities(
									lotDto.getIncludeMdlEntities( true )).toArray( new IVcsReposDto[0] ) );
					paramList.add( getApproveParamInfo(
							paramBean, pjtDtoList, areqDtoList ));

					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
							.setServiceBean( paramBean )
							.setParamList( paramList );

					for ( IDomain<IGeneralServiceBean> action : actions ) {
						action.execute( innerServiceDto );
					}
				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005125S,e);
		}
	}


	/**
	 * 承認情報パラメータを取得します。
	 * @param paramBean FlowChaLibPjtApproveServiceBeanオブジェクト
	 * @return 承認情報パラメータ
	 */
	private IApproveParamInfo[] getApproveParamInfo(
			FlowChaLibPjtApproveServiceBean paramBean,
			List<IPjtAvlDto> pjtDtoList, AreqDtoList assetApplyEntities ) {

		Map<String, AreqDtoList> pjtNoApplyEntityListMap =
				getPjtNoApplyEntityListMap( assetApplyEntities );


		List<IApproveParamInfo> paramList = new ArrayList<IApproveParamInfo>();

		for ( IPjtAvlDto pjtDto : pjtDtoList ) {
			
			IApproveParamInfo paramInfo = new ApproveParamInfo();

			paramInfo.setPjtAvlDto			( pjtDto );
			paramInfo.setAssetApplyEntities	( pjtNoApplyEntityListMap.get( pjtDto.getPjtEntity().getPjtId() ) );
			paramInfo.setComment			(
					paramBean.getPjtEditInputBean().getApproveComment() );
			paramInfo.setUser				( paramBean.getUserName() );
			paramInfo.setUserId				( paramBean.getUserId() );

			paramList.add( paramInfo );
		}

		return (IApproveParamInfo[])paramList.toArray( new IApproveParamInfo[0] );
	}

	/**
	 * 申請情報エンティティを変更管理番号でグループ化する。
	 * @param entities 申請情報	エンティティ
	 * @return 変更管理番号でグループ化された申請情報エンティティ
	 */
	private Map<String,AreqDtoList> getPjtNoApplyEntityListMap(
			AreqDtoList entities ) {

		Map<String,AreqDtoList> pjtApplyNoListMap =
				new TreeMap<String,AreqDtoList>();


		for ( IAreqDto areqDto : entities ) {
			String pjtNo = areqDto.getAreqEntity().getPjtId();
			if ( !pjtApplyNoListMap.containsKey( pjtNo )) {
				pjtApplyNoListMap.put( pjtNo, new AreqDtoList() );
			}

			pjtApplyNoListMap.get( pjtNo ).add( areqDto );
		}
		return pjtApplyNoListMap;
	}
}
