package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotParamInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 処理を別処理に委譲します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionDelegateForLot extends ActionPojoAbstract<IGeneralServiceBean> {

	private List<IDomain<IGeneralServiceBean>> actions = null;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		LotParamInfo paramInfo = AmExtractMessageAddonUtils.getLotParamInfo( paramList );
		if ( TriStringUtils.isEmpty( paramInfo ) ) {
			throw new TriSystemException(AmMessageId.AM005035S);
		}
		for ( IDomain<IGeneralServiceBean> action : actions ) {
			action.execute( serviceDto );
		}

		return serviceDto;
	}
}
