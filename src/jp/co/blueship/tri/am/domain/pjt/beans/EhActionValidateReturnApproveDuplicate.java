package jp.co.blueship.tri.am.domain.pjt.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却資産承認時の妥当性チェックを行います。
 * <li>原本比較処理
 *
 */
public class EhActionValidateReturnApproveDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private IFilesDiffer differ = null;

	/**
	 * 差分チェックがインスタンス生成時に自動的に設定されます。
	 * @param diff 差分チェック
	 */
	public final void setDiffer( IFilesDiffer diff ) {
		this.differ = diff;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		paramList.add( check( serviceDto ) );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param serviceDto コンバート間に流通させるビーン
	 * @return ファイル差分情報を戻します。
	 * @throws ContinuableBusinessException
	 */
	private final IFileDiffResult[] check( IServiceDto<IGeneralServiceBean> serviceDto )
		throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		IFileDiffResult[] results = null;

		results = extractDiff( paramList );

		return results;
	}

	/**
	 * 原本比較を行います。
	 * <li>今回申請するファイルと原本作業ファイルを順次比較します。
	 * <li>原本と同一（差分なし）のファイルは、申請対象から外します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return ファイル差分情報を戻します。
	 */
	private final IFileDiffResult[] extractDiff( List<Object> paramList ) {

		List<IFileDiffResult> resultList = new ArrayList<IFileDiffResult>();

		File infoPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		IFileDiffResult[] results = differ.execute( infoPath, masterPath );

		for ( int i = 0; i < results.length; i++ ) {
			if ( ! results[i].isDiff() )
				continue;

			resultList.add( results[i] );
		}

		return (IFileDiffResult[])resultList.toArray(new IFileDiffResult[0]);
	}
}
