package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理起票・変更管理クローズ用
 * 
 * @version V3L10.01
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibPjtCloseServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 2L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String selectedPjtNo = null;
	/** 一括変更クローズ用変更管理番号 **/
	private String[] selectedPjtIdArray;
	/** 変更管理詳細表示情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = null;
	/** 変更管理編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** 一括で変更クローズを行う/行わないの設定を行う */
	private boolean isBatchMode = false;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		if ( null == pjtDetailViewBean ) {
			pjtDetailViewBean = new PjtDetailViewBean();
		}
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}
	
	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
					List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	public String[] getSelectedPjtIdArray() {
		return selectedPjtIdArray;
	}
	public void setSelectedPjtIdArray(String[] selectedPjtIdArray) {
		this.selectedPjtIdArray = selectedPjtIdArray;
	}
	public boolean isBatchMode() {
		return isBatchMode;
	}
	public void setBatchMode(boolean isBatchMode) {
		this.isBatchMode = isBatchMode;
	}
}

