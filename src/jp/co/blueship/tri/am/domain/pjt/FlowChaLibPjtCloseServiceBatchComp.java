package jp.co.blueship.tri.am.domain.pjt;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 変更情報・変更情報一括クローズ用のクローズ完了画面の表示情報設定Class
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibPjtCloseServiceBatchComp implements IDomain<FlowChaLibPjtCloseServiceBean>  {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private static final ILog log = TriLogFactory.getInstance();
	private ActionStatusMatrixList statusMatrixAction = null;
	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeAction = null;
	private ActionStatusMatrixList statusMatrixIfNotMergeActionByTestComplete = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}
	 public void setStatusIfNotMergeMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixIfNotMergeAction = action;
	}

	public void setStatusIfNotMergeMatrixActionByTestComplete(ActionStatusMatrixList  action) {
		this.statusMatrixIfNotMergeActionByTestComplete = action;
	}


	@Override
	public IServiceDto<FlowChaLibPjtCloseServiceBean> execute( IServiceDto<FlowChaLibPjtCloseServiceBean> serviceDto ) {

		FlowChaLibPjtCloseServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forwordID = paramBean.getForward();

			if ( paramBean.isBatchMode() == false ){
				return serviceDto;
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_CLOSE )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String[] selectedPjtIdArray = new String[1];

					selectedPjtIdArray = paramBean.getSelectedPjtIdArray();
					AmItemChkUtils.checkPjtNo( selectedPjtIdArray );

					this.checkStatusMatrix(paramBean,selectedPjtIdArray);

					PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

					Timestamp systemDate		= TriDateUtils.getSystemTimestamp();
					for ( String selectedPjtNo : selectedPjtIdArray ) {
						IPjtEntity entity		= this.support.findPjtEntity( selectedPjtNo );

						entity.setCloseCmt			( pjtEditInputBean.getCloseComment() );
						entity.setCloseTimestamp	( systemDate );
						entity.setCloseUserNm		( paramBean.getUserName() );
						entity.setCloseUserId		( paramBean.getUserId() );
						entity.setStsId				( AmPjtStatusId.ChangePropertyClosed.getStatusId() );
						entity.setUpdTimestamp		( systemDate );
						entity.setUpdUserNm			( paramBean.getUserName() );
						entity.setUpdUserId			( paramBean.getUserId() );
						this.support.getPjtDao().update( entity );

						if ( DesignSheetUtils.isRecord() ){
							IHistEntity histEntity = new HistEntity();
							histEntity.setDataCtgCd( AmTables.AM_PJT.name() );
							histEntity.setActSts( UmActStatusId.MultipleClose.getStatusId() );

							support.getPjtHistDao().insert( histEntity , entity);
						}

						// グループの存在チェック
						ILotDto lotDto = this.support.findLotDto( entity.getLotId() );
						AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
								this.support.getUmFinderSupport().getGrpDao(),//
								this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
					}
					PjtDetailViewBean pjtDetailViewBean = paramBean.getPjtDetailViewBean();
					pjtDetailViewBean.getPjtDetailBean().setPjtNo( TriStringUtils.convertArrayToString(selectedPjtIdArray) );
					paramBean.setPjtDetailViewBean( pjtDetailViewBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005127S , e );
		}
	}

	/**
	 * ステータスマトリクスのチェックを行います。
	 *
	 * @param paramBean
	 * @param selectedPjtIdArray
	 */
	private void checkStatusMatrix(FlowChaLibPjtCloseServiceBean paramBean,String[] selectedPjtIdArray){

		List<IAreqEntity> areqList = new ArrayList<IAreqEntity>();
		for ( String selectedPjtId : selectedPjtIdArray ) {
			IAreqEntity[] applyList = this.support.getAssetApplyEntityLimit(selectedPjtId, 1, 0, //
					AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtCloseConfirm()).getEntities()
					.toArray(new IAreqEntity[0]);
			for (IAreqEntity areqEntity : applyList) {
				areqList.add( areqEntity );
			}
		}
		IPjtEntity pjtEntity = this.support.findPjtEntity(selectedPjtIdArray[0]);
		StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( pjtEntity.getLotId() )
			.setPjtIds( selectedPjtIdArray )
			.setAreqIds( support.getAssetApplyNo(areqList.toArray(new IAreqEntity[0])) );

		ILotEntity lotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
		if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
			if (StatusFlg.on.value().equals( sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
				// テスト完了運用を行う
				statusDto.setActionList( statusMatrixIfNotMergeActionByTestComplete );
			} else {
				// テスト完了運用を行わない
				statusDto.setActionList( statusMatrixIfNotMergeAction );
			}
		}
		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

	}
}
