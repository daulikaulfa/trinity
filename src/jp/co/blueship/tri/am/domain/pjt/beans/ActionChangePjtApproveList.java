package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionListAbstract;

/**
 * 変更承認を変更ID単位にコミットするため、トランザクション内の最上位ドメイン層として利用する<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 */
public class ActionChangePjtApproveList extends ActionListAbstract<FlowChaLibPjtApproveServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator();

	@Override
	public IServiceDto<FlowChaLibPjtApproveServiceBean> execute( IServiceDto<FlowChaLibPjtApproveServiceBean> serviceDto ) {
		FlowChaLibPjtApproveServiceBean paramBean = serviceDto.getServiceBean();

		String flowActionTitle = null;
		boolean isSuccess = false;

		LogBusinessFlow logService = paramBean.getLogService();

		try {
			List<Object> paramList = serviceDto.getParamList();

			IApproveParamInfo[] paramArray =
							AmExtractMessageAddonUtils.getApproveParamInfoArray( paramList );
			if ( null == paramArray ) {
				throw new TriSystemException(AmMessageId.AM005032S);
			}

			ILotDto lotDto = paramBean.getCacheLotDto();

			// ログの初期処理
			logService.makeLogPath(paramBean.getFlowAction(), lotDto.getLotEntity());
			// ログの開始ラベル出力
			flowActionTitle = LogBusinessFlow.makeFlowActionTitle(paramBean.getFlowAction());
			logService.writeLogWithDateBegin(flowActionTitle);
			// ログのヘッダ部出力
			this.outLogHeader(logService, paramBean, paramArray);

			if ( null != this.getGathering()) {
				this.getGathering().execute( serviceDto );
			}

			this.beforeAdvices( serviceDto );
			this.actions( serviceDto );

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;
			if (null != logService) {
				logService.writeLog(e.getStackTraceString());
			}

			this.throwAdvices( serviceDto );
			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			if (null != logService) {
				logService.writeLog(ExceptionUtils.getStackTraceString(e));
			}

			this.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		} finally {
			try {
				this.afterAdvices( serviceDto );

				if (null != flowActionTitle) {
					int count = serviceDto.getServiceBean().getCountBySkipResource();
					if( 0 < count ) {
						logService.writeLog( AmMessageId.AM002010W , String.valueOf(count));
					}

					logService.writeLogWithDateEnd(flowActionTitle);

					if (isSuccess) {
						logService.deleteLog(AmDesignEntryKeyByLogs.deleteLogAtSuccessPjtApprove);
					}
				}
			} catch ( Exception e ) {
				this.throwAdvices( serviceDto );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( SmMessageId.SM005052S , e );
			}
		}

		return serviceDto;

	}

	/**
	 * ログのヘッダ部分を出力する
	 *
	 * @param logService
	 * @param paramBean
	 * @param paramArray
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logService,
			FlowChaLibPjtApproveServiceBean paramBean,
			IApproveParamInfo[] paramArray) throws Exception {
		ILotEntity lotEntity = paramBean.getCacheLotDto().getLotEntity();

		StringBuilder stb = new StringBuilder();
		stb = logService.makeUserAndLotLabel(stb, paramBean, lotEntity.getLotNm(), lotEntity.getLotId());
		// ロットに含まれるモジュール
		for (IApproveParamInfo info : paramArray) {
			stb.append(this.getContext().getMessage(
					TriLogMessage.LAM0060,
					info.getPjtAvlDto().getPjtEntity().getPjtId()) + SEP);
		}

		logService.writeLog(stb.toString());
	}

}
