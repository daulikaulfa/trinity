package jp.co.blueship.tri.am.domain.pjt.dto;

import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean  extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * 「詳細表示画面」への項目を保持します。
	 */
	ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean chaLibPjtApproveWaitAssetApplyDetailViewServiceBean = null ;

	/**
	 * 「Ｄｉｆｆ比較画面」への項目を保持します。
	 */
	ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean chaLibPjtApproveWaitAssetApplyResourceDiffServiceBean = null ;


	public ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean getChaLibPjtApproveWaitAssetApplyDetailViewServiceBean() {
		return chaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
	}
	public void setChaLibPjtApproveWaitAssetApplyDetailViewServiceBean(
			ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean chaLibPjtApproveWaitAssetApplyDetailViewServiceBean) {
		this.chaLibPjtApproveWaitAssetApplyDetailViewServiceBean = chaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
	}
	public ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean getChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean() {
		return chaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
	}
	public void setChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean(
			ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean chaLibPjtApproveWaitAssetApplyResourceDiffServiceBean) {
		this.chaLibPjtApproveWaitAssetApplyResourceDiffServiceBean = chaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
	}
}
