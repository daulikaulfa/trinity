package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.beans.mail.dto.PjtApproveMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveRejectServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理承認却下完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibPjtApproveRejectServiceMail implements IDomain<FlowChaLibPjtApproveRejectServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	private FlowChaLibPjtApproveEditSupport support = null;
	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveRejectServiceBean> execute( IServiceDto<FlowChaLibPjtApproveRejectServiceBean> serviceDto ) {

		FlowChaLibPjtApproveRejectServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT ) ){
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT )) {
				return serviceDto;
			}

			if ( ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				return serviceDto;
			}

			PjtApproveMailServiceBean successMailBean = new PjtApproveMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, paramBean );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

			String selectedApplyNo = paramBean.getSelectedApplyNo();
			String approveComment = paramBean.getPjtEditInputBean().getApproveRejectComment();
			IAreqDto areqDto	= this.support.findAreqDto( selectedApplyNo, (StatusFlg)null );
			IPjtEntity pjtEntity = this.support.findPjtEntity(areqDto.getAreqEntity().getPjtId());

			AreqDtoList assetApplyEntityList = new AreqDtoList();
			assetApplyEntityList.add(areqDto);

			successMailBean.setPjtEntity( pjtEntity );
			successMailBean.setAssetApplyEntityList(assetApplyEntityList);
			successMailBean.setRejectApproveApplyNo(selectedApplyNo);
			successMailBean.setApproveComment(approveComment);

			successMail.execute( mailServiceDto );

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , paramBean.getFlowAction() ) ) ;
		}

		return serviceDto;
	}
}
