package jp.co.blueship.tri.am.domain.pjt.beans;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.EhAssetRelatedEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 承認取消する返却、取消申請に関連する貸出／返却／削除申請を抽出します。<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EhActionExtractApplyInfoApproveCancel extends ActionPojoAbstract<IGeneralServiceBean> {

	@SuppressWarnings("unused")
	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		try {

			List<Object> paramList = serviceDto.getParamList();

			IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
			if ( null == paramInfo ) {
				throw new TriSystemException(AmMessageId.AM005031S);
			}

			IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
			if ( null == areqDto ) {
				throw new TriSystemException(AmMessageId.AM005033S);
			}

			IAreqEntity areqEntity = areqDto.getAreqEntity();

			// 関連申請情報になり得る全てのエンティティ
			// xxxServiceで設定されている前提
			IAreqDto[] areqDtoEntities = AmExtractEntityAddonUtils.extractAssetApplyArray( paramList );
			if ( null == areqDtoEntities ) {
				throw new TriSystemException(AmMessageId.AM005037S);
			}

			// List.containsが使いたいのでList化
			List<IAreqDto> areqDtoList = FluentList.from(areqDtoEntities).asList();
			areqDtoList = new ArrayList<IAreqDto>( areqDtoList );


			AreqDtoList cancelApplyInfoList = new AreqDtoList();
			setRelatedApplyInfoByCancel(
					paramList,
					areqDto,
					cancelApplyInfoList,
					areqDtoList );

			// 取り消された返却資産そのものは関連資産から外す
			// ↓ループしながらコレクションの要素を削除しようとするとConcurrentModificationExceptionが
			//   発生するため、Iterator.remove()を使用する
			// for ( IAreqEntity cancelApplyInfo : cancelApplyInfoList ) {
			for ( Iterator<IAreqDto> it = cancelApplyInfoList.iterator();
					it.hasNext();) {
				IAreqEntity removalAreqEntity = it.next().getAreqEntity();

				if ( removalAreqEntity.getAreqId().equals( areqEntity.getAreqId() )) {
					it.remove();
				}
			}

			IEhAssetRelatedEntity relatedEntity = new EhAssetRelatedEntity();
//			relatedEntity.setAssetRelatedReturnEntity(
//			(IAreqEntity[])cancelApplyInfoList.toArray( new IAreqEntity[]{} ) );
			relatedEntity.setAssetRelatedReturnEntity( cancelApplyInfoList  );


			// 取得エンティティの設定
			paramInfo.getApplyNoAssetRelatedEntityMap().put(
														areqEntity.getAreqId(), relatedEntity );

		} catch ( IOException ioe ) {
			throw new TriSystemException( AmMessageId.AM005081S , ioe );
		}

		return serviceDto;
	}

	/**
	 * 関連して取消される貸出、削除、返却申請エンティティをリストに追加します。
	 * @param paramList パラメータリスト
	 * @param areqDto 取消対象の申請情報
	 * @param removalApplyInfoList 削除対象となる貸出返却申請エンティティのリスト
	 * @param allApplyInfoList 全ユーザの貸出／返却／削除申請情報
	 * @throws IOException
	 */
	private void setRelatedApplyInfoByCancel(
			List<Object> paramList,
			IAreqDto areqDto,
			List<IAreqDto> removalApplyInfoList,
			List<IAreqDto> allApplyInfoList )
		throws IOException {


		// 取消対象となる申請番号以降の申請情報
		List<IAreqDto> checkTargetEntityList =
			getRelatedApplyInfoEntityList( areqDto, allApplyInfoList );

		if ( !removalApplyInfoList.contains( areqDto ) ) {
			removalApplyInfoList.add( areqDto );
		}


		// 資産の一覧
		List<String> cancelAssetList = getAssetPathList( paramList, areqDto );

		for ( String cancelAssetPath : cancelAssetList ) {

			for ( IAreqDto targetDto : checkTargetEntityList ) {

				if ( targetDto.getAreqEntity().getAreqId().equals( areqDto.getAreqEntity().getAreqId() )) {
					continue;
				}

				List<String> assetList = getAssetPathList( paramList, targetDto );

				if ( assetList.contains( cancelAssetPath )) {

					if ( !removalApplyInfoList.contains( targetDto ) ) {

						removalApplyInfoList.add( targetDto );

						setRelatedApplyInfoByCancel(
								paramList,
								targetDto,
								removalApplyInfoList,
								allApplyInfoList );
					}
				}
			}
		}
	}

	/**
	 * 申請情報が持つ資産パスのリストを取得します。
	 * @param paramList パラメータリスト
	 * @param areqDto 申請情報エンティティ
	 * @return 申請情報が持つ資産パスのリスト
	 */
	private List<String> getAssetPathList( List<Object> paramList, IAreqDto areqDto ) {

		List<String> tempPathList = null;

		try {
			IAreqEntity areqEntity = areqDto.getAreqEntity();

			if ( AmBusinessJudgUtils.isLendApply( areqEntity )) {

				tempPathList = TriFileUtils.getFilePaths(
						AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList, areqEntity ),
						TriFileUtils.TYPE_FILE );

			} else if ( AmBusinessJudgUtils.isReturnApply( areqEntity )) {

				tempPathList = TriFileUtils.getFilePaths(
						AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList, areqEntity ),
						TriFileUtils.TYPE_FILE );

			} else {
				// 削除申請
				if (null == tempPathList) {
					tempPathList = new ArrayList<String>();
				}
				for ( IAreqFileEntity assetFileEntity : areqDto.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {
					tempPathList.add( assetFileEntity.getFilePath() );
				}
			}
		} catch ( IOException ioe ) {
			throw new TriSystemException( AmMessageId.AM005082S , ioe );
		}

		// 比較できるようにするため、パス区切り文字を合わせておく
		List<String> pathList = new ArrayList<String>();
		for ( String tempPath : tempPathList ) {
			pathList.add( TriStringUtils.convertPath( tempPath ) );
		}

		return pathList;
	}

	/**
	 * 取消対象となり得るIAreqEntityを取得します。
	 * @param areqDto 取消対象申請情報
	 * @param allApplyInfoList 全ユーザの貸出／返却／削除申請情報
	 * @return 取消対象となり得る申請番号-IAreqEntityのリスト。申請日時の昇順に格納。
	 */
	private List<IAreqDto> getRelatedApplyInfoEntityList(
			IAreqDto areqDto, List<IAreqDto> allApplyInfoList ) {

		// 申請日時の昇順に並べる
		AssetApplyEntityComparator comp = new AssetApplyEntityComparator();
		Collections.sort( allApplyInfoList, comp );


		List<IAreqDto> relatedList = new ArrayList<IAreqDto>();

		for ( IAreqDto dto : allApplyInfoList ) {

			if ( comp.getApplyDate( dto ).compareTo( comp.getApplyDate( areqDto ) ) >= 0 ) {
				relatedList.add( areqDto );
			}
		}

		return relatedList;
	}

	/**
	 * 申請エンティティを昇順でソートするためのComparatorクラス
	 */
	private class AssetApplyEntityComparator implements Comparator<IAreqDto> {

		public int compare( IAreqDto entity1, IAreqDto entity2 ) {

			Timestamp startDate1 = getApplyDate( entity1 );
			Timestamp startDate2 = getApplyDate( entity2 );

			return startDate1.compareTo( startDate2 );
		}

		/**
		 * 申請日を取得する。
		 * @param dto IAreqEntityエンティティ
		 * @return 申請日
		 */
		private Timestamp getApplyDate( IAreqDto dto ) {

			Timestamp applyDate = null;
			IAreqEntity entity = dto.getAreqEntity();

			if ( AmBusinessJudgUtils.isLendApply( entity )) {
				applyDate = entity.getLendReqTimestamp();
			} else if ( AmBusinessJudgUtils.isReturnApply( entity )) {
				applyDate = entity.getRtnReqTimestamp();
			} else if ( AmBusinessJudgUtils.isDeleteApply( entity )) {
				applyDate = entity.getDelReqTimestamp();
			}

			return applyDate;

		}
	}
}
