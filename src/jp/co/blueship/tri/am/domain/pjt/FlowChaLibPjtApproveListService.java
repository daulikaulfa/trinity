package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveListServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * 変更管理承認・変更管理承認一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveListService implements IDomain<FlowChaLibPjtApproveListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveListServiceBean> execute( IServiceDto<FlowChaLibPjtApproveListServiceBean> serviceDto ) {

		FlowChaLibPjtApproveListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			// 選択コンボ用ロットリスト
			List<LotViewBean> lotViewBeanList = this.support.getLotViewBeanForPullDown( paramBean );

			paramBean.setLotViewBeanList	( lotViewBeanList );
			String selectedLotNo = paramBean.getSelectedLotNo();

			//変更管理情報

			// 変更管理承認履歴情報
			IJdbcCondition condition	= AmDBSearchConditionAddonUtils.getPjtApproveCondition( selectedLotNo );
			ISqlSort sort			= AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByPjtApproveList();

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			List<IPjtAvlDto> pjtDtoList = new ArrayList<IPjtAvlDto>();
			ILimit limit = null;

			if ( TriStringUtils.isNotEmpty(selectedLotNo) ) {

				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( selectedLotNo );

				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				IEntityLimit<IPjtAvlEntity> entityLimit = null;

				entityLimit = this.support.getPjtAvlDao().find( condition.getCondition(), sort, selectPageNo,
									sheet.intValue(
											AmDesignEntryKeyByChangec.maxPageNumberByPjtApproveList ));

				if ( entityLimit.getEntities().size() == 0 ) {
					paramBean.setInfoMessage( AmMessageId.AM001098E );
				}

				limit = entityLimit.getLimit();
				pjtDtoList = this.support.findPjtAvlDto( entityLimit.getEntities() );
			}


			// 画面表示内容設定
			paramBean.setPjtApproveHistoryViewBeanList(
					this.support.getPjtApproveHistoryViewBean( pjtDtoList ));

			paramBean.setPageInfoView		(
					AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit ));
			paramBean.setSelectPageNo( selectPageNo );


			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005109S , e );
		}
	}
}
