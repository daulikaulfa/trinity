package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.RelatedApplyInfoViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理承認・変更管理取消用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtApproveCancelServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String selectedPjtNo = null;
	/** 変更承認通番 **/
	private String pjtAvlSeqNo = null;
	/** 変更管理編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = null;
	/** 承認履歴情報 */
	private List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList = null;
	/** 関連申請情報 */
	private List<RelatedApplyInfoViewBean> relatedApplyInfoViewBeanList = null;


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}

	public String getPjtAvlSeqNo() {
		return pjtAvlSeqNo;
	}
	public void setPjtAvlSeqNo( String pjtAvlSeqNo ) {
		this.pjtAvlSeqNo = pjtAvlSeqNo;
	}

	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}

	public PjtDetailViewBean getPjtDetailViewBean() {
		if ( null == pjtDetailViewBean ) {
			pjtDetailViewBean = new PjtDetailViewBean();
		}
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}

	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
					List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}

	public List<PjtApproveHistoryViewBean> getPjtApproveHistoryViewBeanList() {
		if ( null == pjtApproveHistoryViewBeanList ) {
			pjtApproveHistoryViewBeanList = new ArrayList<PjtApproveHistoryViewBean>();
		}
		return pjtApproveHistoryViewBeanList;
	}
	public void setPjtApproveHistoryViewBeanList(
					List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList ) {
		this.pjtApproveHistoryViewBeanList = pjtApproveHistoryViewBeanList;
	}

	public List<RelatedApplyInfoViewBean> getRelatedApplyInfoViewBeanList() {
		if ( null == relatedApplyInfoViewBeanList ) {
			relatedApplyInfoViewBeanList = new ArrayList<RelatedApplyInfoViewBean>();
		}
		return relatedApplyInfoViewBeanList;
	}
	public void setRelatedApplyInfoViewBeanList(
					List<RelatedApplyInfoViewBean> relatedApplyInfoViewBeanList ) {
		this.relatedApplyInfoViewBeanList = relatedApplyInfoViewBeanList;
	}

}