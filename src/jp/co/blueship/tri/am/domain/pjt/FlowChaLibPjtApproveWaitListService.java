package jp.co.blueship.tri.am.domain.pjt;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitListServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * 変更管理承認・承認待ち一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibPjtApproveWaitListService implements IDomain<FlowChaLibPjtApproveWaitListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	@Override
	public IServiceDto<FlowChaLibPjtApproveWaitListServiceBean> execute(IServiceDto<FlowChaLibPjtApproveWaitListServiceBean> serviceDto) {

		FlowChaLibPjtApproveWaitListServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			int selectedPageNo = 1;
			int maxPageNumber = 0;

			// 選択コンボ用ロットリスト
			List<LotViewBean> lotViewBeanList = this.support.getLotViewBeanForPullDown(paramBean);

			paramBean.setLotViewBeanList(lotViewBeanList);
			String selectedLotNo = paramBean.getSelectedLotNo();

			if ( TriStringUtils.isNotEmpty(selectedLotNo)) {
				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( selectedLotNo );
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
			}

			// 承認可能な返却、削除承認を含む変更管理
			String[] pjtNo = null;
			pjtNo = this.getPjtNoByIncludeApprovableApplyInfo(paramBean, selectedLotNo);

			if (!TriStringUtils.isEmpty(pjtNo)) {
				// 対象の変更管理配下の全申請情報を取得
				AreqCondition condition = (AreqCondition) AmDBSearchConditionAddonUtils.getAreqConditionByPjtNo(pjtNo);
				condition.setStsIdsNotEquals(new String[] { AmAreqStatusId.CheckoutRequested.getStatusId() });

				// ソート順
				// 変更管理番号 -> ステータス(返却、削除申請を特別に上位にする) -> 申請日時 -> 申請番号
				// 先ずは変更管理番号でソートしておく
				ISqlSort sort = new SortBuilder();
				sort.setElement(AreqItems.pjtId, TriSortOrder.Asc, 1);

				IEntityLimit<IAreqEntity> entityLimit = this.support.getAreqDao().find(condition.getCondition(), sort, selectedPageNo, maxPageNumber);

				AreqDtoList entities = this.support.findAreqDtoList( entityLimit.getEntities() );
				Collections.sort(entities, new AssetApplyComparator() );
				paramBean.setApplyInfoViewBeanList(this.support.getApplyInfoViewBeanList(entities));

			} else {
				paramBean.setApplyInfoViewBeanList(null);

				if ( TriStringUtils.isNotEmpty(selectedLotNo)) {
					paramBean.setInfoMessage(AmMessageId.AM001088E);
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005113S, e);
		}
	}

	/**
	 * 承認可能な申請情報を持つ変更管理番号を取得する
	 *
	 * @param paramBean パラメタ情報
	 * @param lotId ロット番号
	 * @return 承認可能な申請情報を持つ変更管理番号
	 */

	private String[] getPjtNoByIncludeApprovableApplyInfo(FlowChaLibPjtApproveWaitListServiceBean paramBean, String lotId) {
		if (TriStringUtils.isEmpty(lotId)) {
			return new String[0];
		}

		// 承認系の返却、削除承認
		IJdbcCondition assetApplyCondition = AmDBSearchConditionAddonUtils.getApprovableAreqCondition(lotId);

		List<IAreqEntity> areqEntities = this.support.getAreqDao().find(assetApplyCondition.getCondition());
		if (areqEntities.size() == 0 && lotId == null) {
			paramBean.setInfoMessage(AmMessageId.AM001086E);
			return new String[0];
		}

		// 変更管理番号を抽出
		List<IPjtAvlAreqLnkEntity> pjtNoArray = this.support.getPjtNo( this.support.findAreqDtoList( areqEntities ) );
		Set<String> pjtIdSet = new TreeSet<String>();
		for (IPjtAvlAreqLnkEntity pjtNo : pjtNoArray) {
			pjtIdSet.add(pjtNo.getPjtId());
		}

		if (null != pjtNoArray && pjtNoArray.size() > 0) {
			// 進行中の変更管理
			IJdbcCondition pjtCondition = AmDBSearchConditionAddonUtils.getProgressPjtCondition(pjtIdSet.toArray(new String[0]));

			IEntityLimit<IPjtEntity> pjtEntityLimit = this.support.getPjtDao().find(pjtCondition.getCondition(), null, 1, 0);
			if (pjtEntityLimit.getEntities().size() == 0) {
				paramBean.setInfoMessage(AmMessageId.AM001088E);
				return new String[0];
			}

			// 変更管理番号を抽出
			return this.support.getPjtNo(pjtEntityLimit.getEntities().toArray(new IPjtEntity[0]));
		} else {
			return new String[0];
		}

	}

	/**
	 * 申請情報を下記の昇順でソートするクラス。<br>
	 * 変更管理番号 -> ステータス(返却、削除申請を特別に上位にする) -> 申請日時 -> 申請番号
	 */
	private class AssetApplyComparator implements Comparator<IAreqDto> {

		public int compare(IAreqDto entity1, IAreqDto entity2) {

			String pjtNo1 = entity1.getAreqEntity().getPjtId();
			String pjtNo2 = entity2.getAreqEntity().getPjtId();

			if (pjtNo1.equals(pjtNo2)) {
				return compareStatus(entity1, entity2);
			} else {
				return pjtNo1.compareTo(pjtNo2);
			}
		}

		/**
		 * 申請情報を基本ステータスでソートする <li>承認前のステータスを上位に表示 <li>承認前のステータスは削除申請を上位に表示 <li>
		 * 同一のステータスであれば、申請日時の順で表示 <li>それ以外はステータスの順で表示
		 *
		 * @param entity1
		 * @param entity2
		 * @return
		 */
		private int compareStatus(IAreqDto entity1, IAreqDto entity2) {

			String status1 = entity1.getAreqEntity().getStsId();
			String status2 = entity2.getAreqEntity().getStsId();

			if (AmAreqStatusId.CheckinRequested.equals(status1) && AmAreqStatusId.CheckinRequested.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			}

			if (AmAreqStatusId.RemovalRequested.equals(status1) && AmAreqStatusId.RemovalRequested.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			}

			if (AmAreqStatusId.RemovalRequested.equals(status1))
				return -1;

			if (AmAreqStatusId.RemovalRequested.equals(status2))
				return 1;

			if (AmAreqStatusId.CheckinRequested.equals(status1))
				return -1;

			if (AmAreqStatusId.CheckinRequested.equals(status2))
				return 1;

			if (status1.equals(status2)) {
				return compareApplyDate(entity1, entity2);
			} else {
				return status1.compareTo(status2);
			}
		}

		/**
		 * 申請情報を申請日でソートする
		 *
		 * @param entity1
		 * @param entity2
		 * @return
		 */
		private int compareApplyDate(IAreqDto entity1, IAreqDto entity2) {

			Timestamp applyDate1 = getApplyDate(entity1);
			Timestamp applyDate2 = getApplyDate(entity2);

			if (applyDate1.equals(applyDate2)) {
				return compareApplyNo(entity1, entity2);
			} else {
				return applyDate1.compareTo(applyDate2);
			}
		}

		/**
		 * 申請日を取得する。
		 *
		 * @param entity IAreqEntityエンティティ
		 * @return 申請日
		 */
		private Timestamp getApplyDate(IAreqDto entity) {

			Timestamp applyDate = null;

			if (AmBusinessJudgUtils.isLendApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getLendReqTimestamp();
			} else if (AmBusinessJudgUtils.isReturnApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getRtnReqTimestamp();
			} else if (AmBusinessJudgUtils.isDeleteApply(entity.getAreqEntity())) {
				applyDate = entity.getAreqEntity().getDelReqTimestamp();
			}

			return applyDate;
		}

		/**
		 * 申請情報を申請番号でソートする
		 *
		 * @param entity1
		 * @param entity2
		 * @return
		 */
		private int compareApplyNo(IAreqDto entity1, IAreqDto entity2) {

			String applyNo1 = entity1.getAreqEntity().getAreqId();
			String applyNo2 = entity2.getAreqEntity().getAreqId();

			return applyNo1.compareTo(applyNo2);
		}
	}

}
