package jp.co.blueship.tri.am.domain.pjt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoDetailViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoDetailViewPjtApproveBean.AssetInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveRejectServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AppendFileConstID;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 * 変更管理承認・変更管理承認却下確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveRejectServiceConfirm implements IDomain<FlowChaLibPjtApproveRejectServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;
	private IUmFinderSupport umFinderSupport;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveRejectServiceBean> execute( IServiceDto<FlowChaLibPjtApproveRejectServiceBean> serviceDto ) {

		FlowChaLibPjtApproveRejectServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM )) {
				return serviceDto;
			}

			String selectedApplyNo = paramBean.getSelectedApplyNo();
			AmItemChkUtils.checkApplyNo( selectedApplyNo );

			IAreqEntity assetEntity = this.support.findAreqEntity( selectedApplyNo );
			List<IAreqFileEntity> removalFileEntities = this.support.findAreqFileEntities(selectedApplyNo, AreqCtgCd.RemovalRequest);
			List<IAreqBinaryFileEntity> removalBinaryFileEntities = this.support.findAreqBinaryFileEntities(selectedApplyNo, AreqCtgCd.RemovalRequest);
			IPjtEntity pjtEntity = this.support.findPjtEntity( assetEntity.getPjtId() ) ;
			String selectedLotNo = pjtEntity.getLotId();
			String selectedPjtNo = assetEntity.getPjtId();

			List<IAreqAttachedFileEntity> areqAttachedFileEntities = this.support.findAreqAttachedFileEntities( selectedApplyNo );


			if ( refererID.equals( ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM )) {

				PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

				if ( ScreenType.next.equals( screenType )) {

					AmItemChkUtils.checkPjtApproveReject( pjtEditInputBean );

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setPjtIds( selectedPjtNo )
						.setAreqIds( assetEntity.getAreqId() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				paramBean.setPjtEditInputBean( pjtEditInputBean );
			}

			if ( !forwordID.equals( ChaLibScreenID.PJT_APPROVE_REJECT_CONFIRM )) {
				return serviceDto;
			}
			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					umFinderSupport.findGroupByUserId(paramBean.getUserId()));

			ApplyInfoDetailViewPjtApproveBean applyInfoDetailViewBean =
					new ApplyInfoDetailViewPjtApproveBean();

			applyInfoDetailViewBean.setPjtNo		( assetEntity.getPjtId() );
			applyInfoDetailViewBean.setApplyNo		( selectedApplyNo );
			applyInfoDetailViewBean.setApplyContent	( assetEntity.getContent() );
			applyInfoDetailViewBean.setApplySubject	( assetEntity.getSummary() );
			applyInfoDetailViewBean.setApplyGroup	( assetEntity.getGrpNm() );

			List<AssetInfo> assetInfoList		= new ArrayList<AssetInfo>();
			List<AssetInfo> assetBinaryInfoList = new ArrayList<AssetInfo>();

			PjtDetailViewBean pjtDetailViewBean = AmViewInfoAddonUtils.getPjtDetailViewBean( lotDto.getLotEntity(), pjtEntity );

			if ( AmBusinessJudgUtils.isReturnApply( assetEntity )) {

				applyInfoDetailViewBean.setApplyUser	( assetEntity.getRtnReqUserNm() );
				applyInfoDetailViewBean.setApplyUserId	( assetEntity.getRtnReqUserId() );
				applyInfoDetailViewBean.setApplyDate	( TriDateUtils.convertViewDateFormat(assetEntity.getRtnReqTimestamp()) );
				List<String> assetList =
						this.support.getReturnAssetList( lotDto, assetEntity );
				for ( String path : assetList ) {

					AssetInfo assetInfo = applyInfoDetailViewBean.newAssetInfo();

					assetInfo.setAssetPath	( path );
					assetInfo.setAssetStatus( "" );

					assetInfoList.add( assetInfo );
				}

				// 添付ファイルの表示
				for (IAreqAttachedFileEntity areqAttachedFileEntity : areqAttachedFileEntities) {

					String filePath = areqAttachedFileEntity.getFilePath();
					applyInfoDetailViewBean.setReturnAssetAppendFile( filePath );
				}

				// 添付ファイルのダウンロード
				String saveDirURL = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFileURL ) ;

				String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

				// 申請番号フォルダをつなげる
				String applyNoPath = TriStringUtils.linkPathBySlash( basePath , selectedApplyNo ) ;

				// 最新のユニークのフォルダをつなげる
				String uniqueKey = "";
				String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

				// 添付ファイルごとのフォルダをつなげる
				String append1 = AppendFileConstID.File.append1.value() ;
				String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

				for (IAreqAttachedFileEntity areqAttachedFileEntity : areqAttachedFileEntities) {

					String filePath = areqAttachedFileEntity.getFilePath();
					if(TriStringUtils.isEmpty(filePath)) {
						// ファイルが登録されていない場合
						applyInfoDetailViewBean.setReturnAssetAppendFileLink( null );
						continue;
					}
					File file = new File(appendPath1, filePath);
					String linkUrl = BusinessFileUtils.getAppendFileURL(saveDirURL, selectedApplyNo, uniqueKey, append1, filePath );

					if(false == file.exists()) {
						// ファイルがない場合
						applyInfoDetailViewBean.setReturnAssetAppendFileLink( null );
						continue;
					}
					if(TriStringUtils.isEmpty(linkUrl)) {
						// URLがつくれなかった場合
						applyInfoDetailViewBean.setReturnAssetAppendFileLink( null );
						continue;
					}

					applyInfoDetailViewBean.setReturnAssetAppendFileLink( linkUrl );
				}

			} else if ( AmBusinessJudgUtils.isDeleteApply( assetEntity )) {

				applyInfoDetailViewBean.setApplyUser	( assetEntity.getDelReqUserNm() );
				applyInfoDetailViewBean.setApplyUserId	( assetEntity.getDelReqUserId() );
				applyInfoDetailViewBean.setApplyDate	( TriDateUtils.convertViewDateFormat(assetEntity.getDelReqTimestamp()) );

				for ( IAreqFileEntity entity : removalFileEntities ) {

					AssetInfo assetInfo = applyInfoDetailViewBean.newAssetInfo();

					assetInfo.setAssetPath	( entity.getFilePath() );
					assetInfo.setAssetStatus( "" );

					assetInfoList.add( assetInfo );
				}

				for ( IAreqBinaryFileEntity entity : removalBinaryFileEntities ) {

					AssetInfo assetInfo = applyInfoDetailViewBean.newAssetInfo();

					assetInfo.setAssetPath	( entity.getFilePath() );
					assetInfo.setAssetStatus( "" );

					assetBinaryInfoList.add( assetInfo );
				}

			}
			applyInfoDetailViewBean.setAssetInfoList		( assetInfoList );
			applyInfoDetailViewBean.setAssetBinaryInfoList	( assetBinaryInfoList );

			paramBean.setPjtDetailViewBean( pjtDetailViewBean );
				paramBean.setApplyInfoDetailViewBean( applyInfoDetailViewBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class, e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005111S , e );
		}
	}
}
