package jp.co.blueship.tri.am.domain.pjt;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理起票・変更管理登録確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtEntryServiceConfirm implements IDomain<FlowChaLibPjtEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtEntryServiceBean> execute( IServiceDto<FlowChaLibPjtEntryServiceBean> serviceDto ) {

		FlowChaLibPjtEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_ENTRY_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_ENTRY_CONFIRM ) ){
				return serviceDto;
			}


			if ( refererID.equals( ChaLibScreenID.PJT_ENTRY_CONFIRM )) {

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( ChaLibScreenID.PJT_ENTRY_CONFIRM )) {

					String selectedLotNo = paramBean.getSelectedLotNo();
					AmItemChkUtils.checkLotNo( selectedLotNo );

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( paramBean.getSelectedLotNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( ChaLibScreenID.PJT_ENTRY_CONFIRM )) {
				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
					if( paramBean.isStatusMatrixV3() ) {
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( paramBean.getSelectedLotNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				PjtDetailInputBean inputBean	= paramBean.getPjtDetailInputBean();
				PjtDetailViewBean viewBean		= new PjtDetailViewBean();

				viewBean.setPjtDetailBean( inputBean.getPjtDetailBean() );

				paramBean.setPjtDetailViewBean( viewBean );
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005132S , e );
		}
	}
}
