package jp.co.blueship.tri.am.domain.pjt.dto;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoDetailViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理承認・承認却下用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtApproveRejectServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 */
	private String selectedPjtNo = null;
	/** 申請番号 */
	private String selectedApplyNo = null;
	/** 変更管理編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private ApplyInfoDetailViewPjtApproveBean applyInfoDetailViewBean = null;


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}

	public String getSelectedApplyNo() {
		return selectedApplyNo;
	}
	public void setSelectedApplyNo( String selectedApplyNo ) {
		this.selectedApplyNo = selectedApplyNo;
	}

	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}

	public PjtDetailViewBean getPjtDetailViewBean() {
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean(PjtDetailViewBean pjtDetailViewBean) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}

	public ApplyInfoDetailViewPjtApproveBean getApplyInfoDetailViewBean() {
		if ( null == applyInfoDetailViewBean ) {
			applyInfoDetailViewBean = new ApplyInfoDetailViewPjtApproveBean();
		}
		return applyInfoDetailViewBean;
	}
	public void setApplyInfoDetailViewBean( ApplyInfoDetailViewPjtApproveBean applyInfoDetailViewBean ) {
		this.applyInfoDetailViewBean = applyInfoDetailViewBean;
	}

}

