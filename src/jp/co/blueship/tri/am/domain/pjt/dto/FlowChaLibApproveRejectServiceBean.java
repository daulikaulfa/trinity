package jp.co.blueship.tri.am.domain.pjt.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibApproveRejectServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * 申請情報番号（表示用）
	 */
	private String[] applyNo;

	/**
	 * 申請情報番号（入力用）
	 */
	private String[] inApplyNo;



	/**
	 * @return inApplyNo
	 */
	public String[] getInApplyNo() {
		return inApplyNo;
	}

	/**
	 * @param inApplyNo 設定する inApplyNo
	 */
	public void setInApplyNo(String[] inApplyNo) {
		this.inApplyNo = inApplyNo;
	}

	/**
	 * @return applyNo
	 */
	public String[] getApplyNo() {
		return applyNo;
	}

	/**
	 * @param applyNo 設定する applyNo
	 */
	public void setApplyNo(String[] applyNo) {
		this.applyNo = applyNo;
	}
}
