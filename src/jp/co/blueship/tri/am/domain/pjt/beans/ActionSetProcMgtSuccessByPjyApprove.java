package jp.co.blueship.tri.am.domain.pjt.beans;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 承認処理は、複数の変更情報を１つ１つ承認＋コミットしていくため最初にエラーステータスを初期化します。<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class ActionSetProcMgtSuccessByPjyApprove extends ActionPojoAbstract<FlowChaLibPjtApproveServiceBean> {


	@Override
	public IServiceDto<FlowChaLibPjtApproveServiceBean> execute( IServiceDto<FlowChaLibPjtApproveServiceBean> serviceDto ) {

		FlowChaLibPjtApproveServiceBean paramBean = serviceDto.getServiceBean();

		paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

		return serviceDto;
	}
}
