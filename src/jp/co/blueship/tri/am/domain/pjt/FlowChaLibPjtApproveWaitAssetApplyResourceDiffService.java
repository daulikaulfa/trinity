package jp.co.blueship.tri.am.domain.pjt;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;



/**
 * FlowCompareReturnResourceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 返却申請Ｄｉｆｆ画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtApproveWaitAssetApplyResourceDiffService implements IDomain<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibPjtApproveEditSupport support = null;

	public void setSupport(FlowChaLibPjtApproveEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> execute( IServiceDto<FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean> serviceDto ) {

		FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean paramBeanChild = paramBean.getChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean() ;

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF ) &&
					!forward.equals( ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF )) {
				return serviceDto;
			}

			if ( !forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF) ) {
				return serviceDto;
			}
			// 業務ロジックを記述
			String applyNo = paramBeanChild.getApplyNo();

			IAreqEntity assetApplyEntity = this.support.findAreqEntity( applyNo );
			IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() ) ;
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			paramBeanChild.setApplyNo( applyNo );

			List<Object> paramList = new ArrayList<Object>() ;
			paramList.add( lotDto );//Util用

			//原本と内部返却フォルダ資産との比較チェックを行う。
			File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ) ;
			File returnAssetPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList , assetApplyEntity ) ;

			String selectedResource = paramBeanChild.getSelectedResource() ;

			/** diff処理**/
			File srcFile = new File( TriStringUtils.linkPathBySlash( masterWorkPath.getAbsolutePath() , selectedResource ) ) ;
			File dstFile = new File( TriStringUtils.linkPathBySlash( returnAssetPath.getAbsolutePath() , selectedResource ) ) ;

			EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer( srcFile , dstFile ) ;
			differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

			DiffResult diffResult = differ.diff() ;
			paramBeanChild.setEncoding( ( null != diffResult.getCharsetSrc() ) ? diffResult.getCharsetSrc() : diffResult.getCharsetDst() ) ;
			paramBeanChild.setDiffResultList( diffResult.getDiffElementList() ) ;

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

}
