package jp.co.blueship.tri.am.domain.pjt;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveRejectServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 * 変更管理承認・変更管理承認却下完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtApproveRejectServiceComp implements IDomain<FlowChaLibPjtApproveRejectServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> actions = null;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	private FlowChaLibPjtApproveEditSupport support = null;
	private IUmFinderSupport umFinderSupport;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveRejectServiceBean> execute( IServiceDto<FlowChaLibPjtApproveRejectServiceBean> serviceDto ) {

		FlowChaLibPjtApproveRejectServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT ) ){
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_REJECT )) {
				return serviceDto;
			}

			if ( ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				return serviceDto;
			}

			String selectedApplyNo		= paramBean.getSelectedApplyNo();
			AmItemChkUtils.checkApplyNo( selectedApplyNo );

			IAreqDto areqDto = this.support.findAreqDto( selectedApplyNo );
			AreqDtoList areqDtoList = new AreqDtoList();
			areqDtoList.add(areqDto);
			ILotDto lotDto = this.support.findLotDto( areqDto.getAreqEntity().getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					umFinderSupport.findGroupByUserId(paramBean.getUserId()));

			IApproveParamInfo paramInfo = new ApproveParamInfo();
			paramInfo.setAssetApplyEntities	( areqDtoList );
			paramInfo.setUser				( paramBean.getUserName() );
			paramInfo.setUserId				( paramBean.getUserId() );
			paramInfo.setComment			(
					paramBean.getPjtEditInputBean().getApproveRejectComment() );

			List<Object> paramList = new ArrayList<Object>();
			paramList.add( lotDto );
			paramList.add( paramInfo );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			for ( IDomain<IGeneralServiceBean> action : actions ) {
				action.execute( innerServiceDto );
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005110S , e );
		}
	}
}
