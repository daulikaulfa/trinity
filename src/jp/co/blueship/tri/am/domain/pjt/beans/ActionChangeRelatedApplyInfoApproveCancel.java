package jp.co.blueship.tri.am.domain.pjt.beans;

import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 関連申請情報のステータスを承認取消済みに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangeRelatedApplyInfoApproveCancel extends ActionPojoAbstract<IGeneralServiceBean>  {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractAssetApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException(AmMessageId.AM005033S);
		}

		IAreqEntity areqEntity = areqDto.getAreqEntity();

		IEhAssetRelatedEntity assetRelatedEntity =
			paramInfo.getApplyNoAssetRelatedEntityMap().get( areqEntity.getAreqId() );

		// 関連資産なし
		if ( null == assetRelatedEntity ) return serviceDto;


		Timestamp systemDate = TriDateUtils.getSystemTimestamp();
		for ( IAreqDto relatedAreqDto : assetRelatedEntity.getAssetRelatedReturnEntity() ) {

			// 返却申請、返却承認、削除承認は関連資産にないことにした

			// 貸出申請
			if ( AmAreqStatusId.CheckoutRequested.equals( relatedAreqDto.getAreqEntity().getStsId() )) {
				relatedAreqDto.getAreqEntity().setStsId( AmAreqStatusId.CheckoutRequestRemoved.getStatusId() );
			}

			// 削除申請
			if ( AmAreqStatusId.RemovalRequested.equals( relatedAreqDto.getAreqEntity().getStsId() )) {
				relatedAreqDto.getAreqEntity().setStsId( AmAreqStatusId.RemovalRequestRemoved.getStatusId() );
			}

			relatedAreqDto.getAreqEntity().setDelTimestamp		( systemDate );
			relatedAreqDto.getAreqEntity().setDelUserNm			( paramInfo.getUser() );
			relatedAreqDto.getAreqEntity().setDelUserId			( paramInfo.getUserId() );
			relatedAreqDto.getAreqEntity().setDelStsId			( StatusFlg.on );
			support.getAreqDao().update( relatedAreqDto.getAreqEntity() );

			//承認履歴は必ず記録する
			if ( DesignSheetUtils.isRecord() ) {
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.none.getStatusId() );
				support.getAreqHistDao().insert( histEntity , areqDto);
			}
		}
		return serviceDto;
	}
}
