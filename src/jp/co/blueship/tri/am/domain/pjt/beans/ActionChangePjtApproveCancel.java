package jp.co.blueship.tri.am.domain.pjt.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.pjt.IPjtDao;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.IPjtAvlDao;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 変更管理単位でステータスを承認取消済みに更新します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class ActionChangePjtApproveCancel extends ActionPojoAbstract<IGeneralServiceBean>  {

	private AmFinderSupport support = null;

	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	private IPjtDao pjtDao = null;
	public void setPjtDao( IPjtDao pjtDao ) {
		this.pjtDao = pjtDao;
	}

	private IPjtAvlDao pjtAvlDao = null;
	public void setPjtAvlDao( IPjtAvlDao pjtAvlDao ) {
		this.pjtAvlDao = pjtAvlDao;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException(AmMessageId.AM005031S);
		}


		IPjtAvlEntity recEntity = paramInfo.getPjtAvlDto().getPjtAvlEntity();

		// 元の変更管理情報を更新
		PjtCondition condition = new PjtCondition();
		condition.setPjtId(recEntity.getPjtId());
		IPjtEntity entity		= this.pjtDao.find( condition.getCondition() ).get(0);

		entity.setPjtLatestAvlSeqNo		( recEntity.getPjtAvlSeqNo() );
		entity.setUpdUserNm				( paramInfo.getUser() );
		entity.setUpdUserId				( paramInfo.getUserId() );

		this.pjtDao.update( entity );

		// 新たに、承認取消として変更管理履歴を積み上げ
		recEntity.setStsId					( AmPjtStatusId.ChangePropertyApprovalCancelled.getStatusId() );
		recEntity.setPjtAvlCancelCmt		( paramInfo.getComment() );
		recEntity.setPjtAvlCancelTimestamp	( TriDateUtils.getSystemTimestamp() );
		recEntity.setPjtAvlCancelUserNm		( paramInfo.getUser() );
		recEntity.setPjtAvlCancelUserId		( paramInfo.getUserId() );
		recEntity.setUpdUserNm				( paramInfo.getUser() );
		recEntity.setUpdUserId				( paramInfo.getUserId() );

		this.pjtAvlDao.update( recEntity );

		paramList.add( recEntity );

		if ( DesignSheetUtils.isRecord() ){
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getPjtHistDao().insert( histEntity , entity);
		}

		return serviceDto;
	}
}
