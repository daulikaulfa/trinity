package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;

public class ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 *  申請番号 
	 */
	private String applyNo;
	/**
	 *  申請者 
	 */
	private String applyUserName = null;
	/**
	 *  申請者グループ 
	 */
	private String applyUserGroup = null;
	/**
	 * 申請基本情報Bean（登録画面・編集確認・詳細画面表示用）
	 */	
	private ChaLibEntryBaseInfoBean chaLibEntryBaseInfoBean;
	
	/**
	 * 申請資産情報Bean（登録画面・編集確認・詳細画面表示用）
	 */
	private ChaLibEntryAssetResourceInfoBean chaLibEntryAssetResourceInfoBean;
	
	/**
	 * 変更承認待ちの申請情報かどうかを判定
	 */
	private boolean isPjtApproveEnabled = false;
	
	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyUserGroup() {
		return applyUserGroup;
	}

	public void setApplyUserGroup(String applyUserGroup) {
		this.applyUserGroup = applyUserGroup;
	}

	public String getApplyUserName() {
		return applyUserName;
	}

	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}


	/**
	 * @return chaLibEntryAssetResourceInfoBean
	 */
	public ChaLibEntryAssetResourceInfoBean getChaLibEntryAssetResourceInfoBean() {
		return chaLibEntryAssetResourceInfoBean;
	}

	/**
	 * @param chaLibEntryAssetResourceInfoBean 設定する chaLibEntryAssetResourceInfoBean
	 */
	public void setChaLibEntryAssetResourceInfoBean(
			ChaLibEntryAssetResourceInfoBean chaLibEntryAssetResourceInfoBean) {
		this.chaLibEntryAssetResourceInfoBean = chaLibEntryAssetResourceInfoBean;
	}

	/**
	 * @return chaLibEntryBaseInfoBean
	 */
	public ChaLibEntryBaseInfoBean getChaLibEntryBaseInfoBean() {
		return chaLibEntryBaseInfoBean;
	}

	/**
	 * @param chaLibEntryBaseInfoBean 設定する chaLibEntryBaseInfoBean
	 */
	public void setChaLibEntryBaseInfoBean(
			ChaLibEntryBaseInfoBean chaLibEntryBaseInfoBean) {
		this.chaLibEntryBaseInfoBean = chaLibEntryBaseInfoBean;
	}

	public boolean isPjtApproveEnabled() {
		return isPjtApproveEnabled;
	}

	public void setPjtApproveEnabled(boolean isPjtApproveEnabled) {
		this.isPjtApproveEnabled = isPjtApproveEnabled;
	}

}
