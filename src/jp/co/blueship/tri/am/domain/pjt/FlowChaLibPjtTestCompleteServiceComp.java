package jp.co.blueship.tri.am.domain.pjt;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtTestCompleteServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理起票・変更管理テスト完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibPjtTestCompleteServiceComp implements IDomain<FlowChaLibPjtTestCompleteServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtEditSupport support = null;
	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtTestCompleteServiceBean> execute( IServiceDto<FlowChaLibPjtTestCompleteServiceBean> serviceDto ) {

		FlowChaLibPjtTestCompleteServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_TEST_COMPLETE ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_TEST_COMPLETE ) ){
				return serviceDto;
			}

			if ( ! forwordID.equals( ChaLibScreenID.COMP_PJT_TEST_COMPLETE )) {
				return serviceDto;
			}
			if ( ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				return serviceDto;
			}

			String selectedPjtNo = paramBean.getSelectedPjtNo();
			AmItemChkUtils.checkPjtNo( selectedPjtNo );

			PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

			Timestamp systemDate		= TriDateUtils.getSystemTimestamp();

			IPjtEntity entity		= this.support.findPjtEntity( selectedPjtNo );

			entity.setTestCompCmt		( pjtEditInputBean.getTestCompleteComment() );
			entity.setTestCompTimestamp	( systemDate );
			entity.setTestCompUserNm	( paramBean.getUserName() );
			entity.setTestCompUserId	( paramBean.getUserId() );
			entity.setDelStsId			( StatusFlg.off );
			entity.setStsId				( AmPjtStatusId.ChangePropertyTestCompleted.getStatusId() );
			entity.setUpdTimestamp		( systemDate );
			entity.setUpdUserNm			( paramBean.getUserName() );
			entity.setUpdUserId			( paramBean.getUserId() );

			this.support.getPjtDao().update( entity );

			PjtDetailViewBean pjtDetailViewBean = paramBean.getPjtDetailViewBean();
			pjtDetailViewBean.getPjtDetailBean().setPjtNo( selectedPjtNo );
			paramBean.setPjtDetailViewBean( pjtDetailViewBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIf( TriRuntimeException.class , e );
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005109S , e );
		}
	}
}
