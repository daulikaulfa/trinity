package jp.co.blueship.tri.am.domain.pjt;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理承認確認画面の表示情報設定Class<br>
 * 
 * @version V3L10.02
 * 
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 */
public class FlowChaLibPjtApproveServiceConfirm implements IDomain<FlowChaLibPjtApproveServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibPjtApproveEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveServiceBean> execute( IServiceDto<FlowChaLibPjtApproveServiceBean> serviceDto ) {

		FlowChaLibPjtApproveServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.PJT_APPROVE_CONFIRM ) &&
					!forwordID.equals( ChaLibScreenID.PJT_APPROVE_CONFIRM )) {
				return serviceDto;
			}


			if ( refererID.equals( ChaLibScreenID.PJT_APPROVE_CONFIRM )) {

				PjtEditInputBean pjtEditInputBean = paramBean.getPjtEditInputBean();

				if ( ScreenType.next.equals( screenType )) {

					AmItemChkUtils.checkPjtApprove( pjtEditInputBean );

					String[] targetApplyNo	= paramBean.getTargetApplyNo();
					AmItemChkUtils.checkApplyNo( targetApplyNo );

					if( paramBean.isStatusMatrixV3() ) {
						AreqDtoList areqDtoList = new AreqDtoList();
						for (String applyNo : targetApplyNo ) {
							areqDtoList.add( this.support.findAreqDto( applyNo ) );
						}
						IPjtEntity[] pjtEntities	= this.support.getPjtEntity( areqDtoList );
						String lotId				= pjtEntities[0].getLotId();
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( lotId )
						.setPjtIds( this.support.getPjtNo( pjtEntities ) )
						.setAreqIds( this.support.getAssetApplyNo( areqDtoList ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

					}
				}

				paramBean.setPjtEditInputBean		( pjtEditInputBean );
			}

			if ( forwordID.equals( ChaLibScreenID.PJT_APPROVE_CONFIRM )) {
				String[] selectedPjtNo	= paramBean.getSelectedPjtNo();
				AreqDtoList assetApplyEntities = new AreqDtoList();

				if ( ! TriStringUtils.isEmpty( selectedPjtNo ) ) {
					assetApplyEntities = getApprovableAssetApplyEntity( selectedPjtNo );

					paramBean.setApplyInfoViewBeanList(
							this.support.getApplyInfoViewBeanList( assetApplyEntities ) );
				}

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					AmItemChkUtils.checkPjtNo( selectedPjtNo );

					// ロットをまたがる選択は不可
					String lotId = this.checkSelectedMultiLotByPjtApprove( assetApplyEntities );
					ILotDto lotDto = this.support.findLotDto( lotId );

					// グループの存在チェック
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

					// ２重貸出チェック
					boolean duplicationCheck = ! lotDto.getLotEntity().getIsAssetDup().parseBoolean();

					if ( !duplicationCheck ) {

						// ２重貸出チェックしない場合でも、最低限のチェックを行う
						// 同じ変更管理の中に重複する資産がないかどうかをチェック
						checkAssetDuplicateInPjt( lotDto, assetApplyEntities, getApprovedAssetApplyEntity( lotId, selectedPjtNo ) );
					}


					// 今回の申請に含まれた資産の重複チェック
					checkAssetDuplicate( lotDto, assetApplyEntities, duplicationCheck );

					List<IAreqDto> approvedEntities = getApprovedAssetApplyEntity( lotId );

					// 承認済み資産との重複チェック
					checkAssetDuplicateApprovedAsset(
							lotDto, assetApplyEntities, approvedEntities, duplicationCheck );

				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005126S , e );
		}
	}

	/**
	 * 変更管理承認時の変更管理選択で、ロットをまたがって選択されていないかをチェックする
	 * @param areqDtoList 申請情報エンティティ
	 */

	public String checkSelectedMultiLotByPjtApprove( AreqDtoList areqDtoList )
		throws BaseBusinessException {

		IPjtEntity[] pjtEntities = support.getPjtEntity( areqDtoList );

		Set<String> lotSet = new TreeSet<String>();
		for ( IPjtEntity entity : pjtEntities ) {
			lotSet.add( entity.getLotId() );
		}

		if ( 1 < lotSet.size() ) {
			throw new ContinuableBusinessException( AmMessageId.AM001105E, new String[] {} );
		}

		if ( 0 != pjtEntities.length )
			return pjtEntities[0].getLotId();

		return null;
	}

	/**
	 * 同じ承認対象の変更管理の中で資産が重複していないかをチェックする
	 * @param lotDto ロットエンティティ
	 * @param targetEntities 承認対象の申請情報エンティティ
	 * @param approvedEntities 承認済みの申請情報エンティティ
	 */

	private void checkAssetDuplicateInPjt(
			ILotDto lotDto,
			List<IAreqDto> targetEntities,
			List<IAreqDto> approvedEntities ) throws BaseBusinessException {

		List<IAreqDto> applyList = new ArrayList<IAreqDto>();
		applyList.addAll( targetEntities );
		applyList.addAll( approvedEntities );

		Map<String,Set<IAreqDto>> pjtNoAssetApplyEntitySet =
			getPjtNoAssetApplyEntityListMap( applyList );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( String pjtNo : pjtNoAssetApplyEntitySet.keySet() ) {

				Map<String,String> retAssetPathApplyNoMap = new HashMap<String,String>();

				for ( IAreqDto targetEntity : pjtNoAssetApplyEntitySet.get( pjtNo )) {
					// 返却資産同士のチェック（共通メソッドのため、擬似的に２重貸出チェックの実施:trueにする）
					checkReturnAssetDuplicate(
								lotDto, targetEntity, retAssetPathApplyNoMap,
								messageList, messageArgsList, true );
				}
			}
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 申請エンティティを返却申請のみ抽出し、変更管理番号ごとにグループ化する
	 * @param assetApplyEntities 申請エンティティ
	 * @return 変更管理番号ごとにグループ化された申請エンティティ
	 */
	private Map<String,Set<IAreqDto>>
		getPjtNoAssetApplyEntityListMap( List<IAreqDto> assetApplyEntities ) {

		Map<String,Set<IAreqDto>> map = new HashMap<String, Set<IAreqDto>>();

		for ( IAreqDto assetApplyEntity : assetApplyEntities ) {

			if ( !AmBusinessJudgUtils.isReturnApply( assetApplyEntity.getAreqEntity() )) continue;

			String pjtNo = assetApplyEntity.getAreqEntity().getPjtId();

			if ( !map.containsKey( pjtNo )) {
				map.put( pjtNo, new HashSet<IAreqDto>() );
			}
			map.get( pjtNo ).add( assetApplyEntity );
		}

		return map;

	}

	/**
	 * 承認済みの申請情報を取得する
	 * @param lotId ロット番号
	 * @return 承認済みの申請情報
	 */
	private List<IAreqDto> getApprovedAssetApplyEntity( String lotId ) {

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();

		// 承認可能な返却承認
		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getApprovedAreqCondition( lotId );

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find( condition.getCondition(), null, 1, 0 ).getEntities();

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}

		return areqDtoEntities;
	}

	/**
	 * 承認済みの申請情報を取得する
	 * @param lotId ロット番号
	 * @param pjtNo 変更管理番号
	 * @return 承認済みの申請情報
	 */
	private List<IAreqDto> getApprovedAssetApplyEntity( String lotId, String[] pjtNo ) {

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();

		// 承認可能な返却承認
		IJdbcCondition condition = AmDBSearchConditionAddonUtils.getApprovedAreqCondition( lotId, pjtNo );

		List<IAreqEntity> areqEntities =
				this.support.getAreqDao().find( condition.getCondition(), null, 1, 0 ).getEntities();

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}

		return areqDtoEntities;
	}

	/**
	 * 承認可能な申請情報を取得する
	 * @param pjtNo 変更管理番号
	 * @return 承認可能な申請情報
	 */

	private AreqDtoList getApprovableAssetApplyEntity( String[] pjtNo ) {

		AreqDtoList areqDtoEntities = new AreqDtoList();

		// 承認可能な返却承認
		IJdbcCondition condition	=
			AmDBSearchConditionAddonUtils.getApprovableAreqCondition( pjtNo );
		ISqlSort sort			= AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibPjtApproveConfirm();

		List<IAreqEntity> areqEntities =
			this.support.getAreqDao().find( condition.getCondition(), sort, 1, 0 ).getEntities();
		if ( areqEntities.size() == 0 ) {
			throw new BusinessException( AmMessageId.AM001086E );
		} else {
			for ( IAreqEntity areqEntity: areqEntities ) {
				IAreqDto areqDto = new AreqDto();
				areqDto.setAreqEntity( areqEntity );

				AreqFileCondition fileCondition = new AreqFileCondition();
				fileCondition.setAreqId( areqEntity.getAreqId() );

				areqDto.setAreqFileEntities( this.support.getAreqFileDao().find( fileCondition.getCondition() ) );
				areqDtoEntities.add( areqDto );
			}
		}

		return areqDtoEntities;
	}

	/**
	 * 承認対象の資産同士が重複していないかをチェックする
	 * @param lotEntity ロットエンティティ
	 * @param targetEntities 承認対象の申請情報エンティティ
	 * @param duplicationCheck ２重貸出チェックの実施
	 */

	private void checkAssetDuplicate(
			ILotDto lotDto,
			List<IAreqDto> targetEntities,
			boolean duplicationCheck ) throws BaseBusinessException {

		Map<String,String> retAssetPathApplyNoMap = new HashMap<String,String>();
		Map<String,String> delAssetPathApplyNoMap = new HashMap<String,String>();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( IAreqDto targetEntity : targetEntities ) {

				if ( AmBusinessJudgUtils.isReturnApply( targetEntity.getAreqEntity() )) {

					// 返却資産同士のチェック
					checkReturnAssetDuplicate(
							lotDto, targetEntity, retAssetPathApplyNoMap,
							messageList, messageArgsList, duplicationCheck );

				} else if ( AmBusinessJudgUtils.isDeleteApply( targetEntity.getAreqEntity() )) {

					// 削除資産同士のチェック
					checkDeleteAssetDuplicate(
							lotDto.getLotEntity(), targetEntity, delAssetPathApplyNoMap,
							messageList, messageArgsList );
				}
			}

			// 返却資産、削除資産間のチェック
			checkAssetDuplicate(
					retAssetPathApplyNoMap, delAssetPathApplyNoMap,
					messageList, messageArgsList );


		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 承認対象の返却申請の資産同士が重複していないかをチェックする
	 * @param lotDto ロットエンティティ
	 * @param retEntity 返却申請エンティティ
	 * @param retAssetPathApplyNoMap 返却資産-返却申請番号のマップ
	 * @param messageList エラーメッセージのリスト
	 * @param messageArgsList エラーメッセージの引数のリスト
	 * @param duplicationCheck ２重貸出チェックの実施
	 */

	private void checkReturnAssetDuplicate(
 			ILotDto lotDto, IAreqDto retEntity,
			Map<String,String> retAssetPathApplyNoMap,
			List<IMessageId> messageList, List<String[]> messageArgsList,
			boolean duplicationCheck ) {

		Map<String,String> assetPathApplyNoMap =
			getReturnAssetPathApplyNoMap( lotDto, retEntity );

		for ( String assetPath : assetPathApplyNoMap.keySet() ) {

			String applyNo = assetPathApplyNoMap.get( assetPath );

			if ( retAssetPathApplyNoMap.containsKey( assetPath ) && duplicationCheck ) {

				messageList.add		( AmMessageId.AM001090E );
				messageArgsList.add	( new String[] {
						applyNo,
						assetPath,
						retAssetPathApplyNoMap.get( assetPath ) } );

			} else {
				retAssetPathApplyNoMap.put( assetPath, applyNo );
			}
		}
	}

	/**
	 * 承認対象の削除申請の資産同士が重複していないかをチェックする
	 * @param lotEntity ロットエンティティ
	 * @param delEntity 削除申請エンティティ
	 * @param delAssetPathApplyNoMap 削除資産-削除申請番号のマップ
	 * @param messageList エラーメッセージのリスト
	 * @param messageArgsList エラーメッセージの引数のリスト
	 */

	private void checkDeleteAssetDuplicate(
 			ILotEntity lotEntity, IAreqDto delEntity,
			Map<String,String> delAssetPathApplyNoMap,
			List<IMessageId> messageList, List<String[]> messageArgsList ) {

		Map<String,String> assetPathApplyNoMap =
			getDeleteAssetPathApplyNoMap( lotEntity, delEntity );

		for ( String assetPath : assetPathApplyNoMap.keySet() ) {

			String applyNo = assetPathApplyNoMap.get( assetPath );

			if ( delAssetPathApplyNoMap.containsKey( assetPath )) {

				messageList.add		( AmMessageId.AM001091E );
				messageArgsList.add	( new String[] {
						applyNo,
						assetPath,
						delAssetPathApplyNoMap.get( assetPath ) } );

			} else {
				delAssetPathApplyNoMap.put( assetPath, applyNo );
			}
		}
	}

	/**
	 * 承認対象の返却申請と削除申請の資産同士が重複していないかをチェックする
	 * @param retAssetPathApplyNoMap 返却資産-返却申請番号のマップ
	 * @param delAssetPathApplyNoMap 削除資産-削除申請番号のマップ
	 * @param messageList エラーメッセージのリスト
	 * @param messageArgsList エラーメッセージの引数のリスト
	 */

	private void checkAssetDuplicate(
			Map<String,String> retAssetPathApplyNoMap,
			Map<String,String> delAssetPathApplyNoMap,
			List<IMessageId> messageList, List<String[]> messageArgsList ) {

		for ( String retAssetPath : retAssetPathApplyNoMap.keySet() ) {

			String retApplyNo = retAssetPathApplyNoMap.get( retAssetPath );

			if ( delAssetPathApplyNoMap.containsKey( retAssetPath )) {

				messageList.add		( AmMessageId.AM001092E );
				messageArgsList.add	( new String[] {
						retApplyNo,
						retAssetPath,
						delAssetPathApplyNoMap.get( retApplyNo ) } );
			}
		}
	}

	/**
	 * 承認対象の返却、削除申請の資産が、承認済みの返却、削除申請の資産と重複していないかをチェックする
	 * @param lotEntity ロットエンティティ
	 * @param targetEntities 承認対象の申請情報エンティティ
	 * @param approvedEntities 承認済みの申請情報エンティティ
	 * @param duplicationCheck ２重貸出チェックの実施
	 */

	private void checkAssetDuplicateApprovedAsset(
			ILotDto lotDto,
			List<IAreqDto> targetEntities,
			List<IAreqDto> approvedEntities,
			boolean duplicationCheck ) throws BaseBusinessException {

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotDto );

		Map<String,String> retAssetPathApplyNoMap = new HashMap<String,String>();
		Map<String,String> delAssetPathApplyNoMap = new HashMap<String,String>();
		setAssetPathApplyNoMap(
				lotDto, targetEntities,
				retAssetPathApplyNoMap, delAssetPathApplyNoMap );


		Map<String,String> approvedRetAssetPathApplyNoMap = new HashMap<String,String>();
		Map<String,String> approvedDelAssetPathApplyNoMap = new HashMap<String,String>();

		setAssetPathApplyNoMap(
				lotDto, approvedEntities,
				approvedRetAssetPathApplyNoMap, approvedDelAssetPathApplyNoMap );


		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			// 返却承認 vs 返却承認済み
			for ( String assetPath : retAssetPathApplyNoMap.keySet() ) {

				if ( approvedRetAssetPathApplyNoMap.containsKey( assetPath ) && duplicationCheck ) {

					messageList.add		( AmMessageId.AM001093E );
					messageArgsList.add	( new String[] {
							retAssetPathApplyNoMap.get( assetPath ),
							assetPath,
							approvedRetAssetPathApplyNoMap.get( assetPath ) } );
				}
			}

			// 削除承認 vs 削除承認済み
			for ( String assetPath : delAssetPathApplyNoMap.keySet() ) {

				if ( approvedDelAssetPathApplyNoMap.containsKey( assetPath )) {

					messageList.add		( AmMessageId.AM001094E );
					messageArgsList.add	( new String[] {
							delAssetPathApplyNoMap.get( assetPath ),
							assetPath,
							approvedDelAssetPathApplyNoMap.get( assetPath ) } );
				}
			}

			// 返却承認 vs 削除承認済み
			for ( String assetPath : retAssetPathApplyNoMap.keySet() ) {

				if ( approvedDelAssetPathApplyNoMap.containsKey( assetPath )) {

					messageList.add		( AmMessageId.AM001095E );
					messageArgsList.add	( new String[] {
							retAssetPathApplyNoMap.get( assetPath ),
							assetPath,
							approvedDelAssetPathApplyNoMap.get( assetPath ) } );
				}
			}

			// 削除承認 vs 返却承認済み
			for ( String assetPath : delAssetPathApplyNoMap.keySet() ) {

				if ( approvedRetAssetPathApplyNoMap.containsKey( assetPath )) {

					messageList.add		( AmMessageId.AM001096E );
					messageArgsList.add	( new String[] {
							delAssetPathApplyNoMap.get( assetPath ),
							assetPath,
							approvedRetAssetPathApplyNoMap.get( assetPath ) } );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 申請情報エンティティから、返却資産パス-返却申請番号、削除申請パス-削除申請番号を
	 * マップにセットする
	 * @param lotDto ロットエンティティ
	 * @param entities 申請情報エンティティ
	 * @param returnAssetPathApplyNoMap 返却資産パス-返却申請番号のマップ
	 * @param deleteAssetPathApplyNoMap 削除申請パス-削除申請番号のマップ
	 */
	private void setAssetPathApplyNoMap(
			ILotDto lotDto, List<IAreqDto> entities,
			Map<String,String> returnAssetPathApplyNoMap,
			Map<String,String> deleteAssetPathApplyNoMap ) {

		for ( IAreqDto entity : entities ) {

			if ( AmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() )) {
				returnAssetPathApplyNoMap.putAll(
						getReturnAssetPathApplyNoMap( lotDto, entity ));

			} else if ( AmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) {

				deleteAssetPathApplyNoMap.putAll(
						getDeleteAssetPathApplyNoMap( lotDto.getLotEntity(), entity ));
			}
		}

	}

	/**
	 * 指定された返却申請エンティティの資産の相対パスを取得し、申請番号とセットにして返す。
	 * @param lotDto ロットエンティティ
	 * @param retEntity 返却申請エンティティ
	 * @return 資産パスと申請番号のマップ
	 */
	private Map<String,String> getReturnAssetPathApplyNoMap(
			ILotDto lotDto, IAreqDto retEntity ) {
		List<String> assetList = this.support.getReturnAssetList( lotDto, retEntity.getAreqEntity() );

		// キー：資産パス、バリュー：申請番号
		Map<String,String> assetPathApplyNoMap = new HashMap<String,String>();

		for ( String assetPath : assetList ) {
			assetPathApplyNoMap.put( assetPath, retEntity.getAreqEntity().getAreqId() );
		}

		return assetPathApplyNoMap;
	}

	/**
	 * 指定された削除申請エンティティの資産の申請パスを取得し、申請番号とセットにして返す。
	 * @param lotEntity ロットエンティティ
	 * @param delEntity 削除申請エンティティ
	 * @return 申請パスと申請番号のマップ
	 */
	private Map<String,String> getDeleteAssetPathApplyNoMap(
			ILotEntity lotEntity, IAreqDto delEntity ) {

		File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );

		// キー：申請パス、バリュー：申請番号
		Map<String,String> assetPathApplyNoMap = new HashMap<String,String>();

		for ( IAreqFileEntity assetEntity : delEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {

			// 削除申請の場合は資産を確認する
			if ( AmAreqStatusId.RemovalRequested.equals( delEntity.getAreqEntity().getStsId() )) {
				File assetFile = new File( masterWorkPath, assetEntity.getFilePath() );
				if ( !assetFile.exists() ) {
					throw new TriSystemException(AmMessageId.AM004044F , delEntity.getAreqEntity().getAreqId() , assetEntity.getFilePath() );
				}
			}

			assetPathApplyNoMap.put( assetEntity.getFilePath(), delEntity.getAreqEntity().getAreqId() );

		}

		return assetPathApplyNoMap;
	}
}
