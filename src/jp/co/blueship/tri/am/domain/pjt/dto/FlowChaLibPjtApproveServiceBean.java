package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;

/**
 * 変更情報承認・承認用
 *
 * @version V3L10.01
 * @author trinity V3
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibPjtApproveServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更情報番号 */
	private String[] selectedPjtNo = null;
	/** 申請番号 */
	private String[] targetApplyNo = null;
	/** 変更情報編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList = null;
	/** ログサービス */
	private LogBusinessFlow logService = new LogBusinessFlow();

	/**
	 * コミット処理時に処理されずにスキップされた資産数
	 */
	private int countBySkipResource;

	/**
	 * CacheしたロットDTO。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private ILotDto cacheLotDto = null;

	/**
	 * 変更情報単位のVCSタグ
	 */
	private Map<String, String> numberingVerTagMap = new HashMap<String, String>();


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String[] getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String[] selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}

	public String[] getTargetApplyNo() {
		return targetApplyNo;
	}
	public void setTargetApplyNo( String[] targetApplyNo ) {
		this.targetApplyNo = targetApplyNo;
	}

	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}

	public List<ApplyInfoViewPjtApproveBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtApproveBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
			List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	/**
	 * ログサービスを取得します。
	 * @return ログサービス
	 */
	public LogBusinessFlow getLogService() {
	    return logService;
	}
	/**
	 * ログサービスを設定します。
	 * @param logService ログサービス
	 */
	public void setLogService(LogBusinessFlow logService) {
	    this.logService = logService;
	}
	/**
	 * コミット処理時に処理されずにスキップされた資産数を取得します。
	 * @return コミット処理時に処理されずにスキップされた資産数
	 */
	public int getCountBySkipResource() {
	    return countBySkipResource;
	}
	/**
	 * コミット処理時に処理されずにスキップされた資産数を設定します。
	 * @param countBySkipResource コミット処理時に処理されずにスキップされた資産数
	 */
	public void setCountBySkipResource(int countBySkipResource) {
	    this.countBySkipResource = countBySkipResource;
	}
	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されるを取得します。
	 *
	 * @return CacheしたロットDTO。
	 */
	public ILotDto getCacheLotDto() {
	    return cacheLotDto;
	}
	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されるを設定します。
	 *
	 * @param cacheLotDto CacheしたロットDTO。
	 */
	public void setCacheLotDto(ILotDto cacheLotDto) {
	    this.cacheLotDto = cacheLotDto;
	}
	/**
	 * 変更情報単位のVCSタグを取得します。
	 * @return 変更情報単位のVCSタグ
	 */
	public Map<String,String> getNumberingVerTagMap() {
	    return numberingVerTagMap;
	}
	/**
	 * 変更情報単位のVCSタグを設定します。
	 * @param numberingVerTagMap 変更情報単位のVCSタグ
	 */
	public void setNumberingVerTagMap(Map<String,String> numberingVerTagMap) {
	    this.numberingVerTagMap = numberingVerTagMap;
	}

}

