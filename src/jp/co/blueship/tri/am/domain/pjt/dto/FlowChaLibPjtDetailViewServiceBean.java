package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理起票・変更管理詳細閲覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String selectedPjtNo = null;
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		if ( null == pjtDetailViewBean ) {
			pjtDetailViewBean = new PjtDetailViewBean();
		}
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}

	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
					List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
}

