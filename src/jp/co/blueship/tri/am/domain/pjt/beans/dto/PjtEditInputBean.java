package jp.co.blueship.tri.am.domain.pjt.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・変更管理操作用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtEditInputBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 承認コメント */
	private String approveComment = null;
	/** 承認取消コメント */
	private String approveCancelComment = null;
	/** 承認却下コメント */
	private String approveRejectComment = null;
	/** 取消コメント */
	private String delComment = null;
	/** クローズコメント */
	private String closeComment = null;
	/** クローズコメント */
	private String testCompleteComment = null;
	
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo( String pjtNo ) {
		this.pjtNo = pjtNo;
	}

	public String getApproveComment() {
		return approveComment;
	}
	public void setApproveComment( String approveComment ) {
		this.approveComment = approveComment;
	}
	
	public String getApproveCancelComment() {
		return approveCancelComment;
	}
	public void setApproveCancelComment( String approveCancelComment ) {
		this.approveCancelComment = approveCancelComment;
	}

	public String getApproveRejectComment() {
		return approveRejectComment;
	}
	public void setApproveRejectComment( String approveRejectComment ) {
		this.approveRejectComment = approveRejectComment;
	}
	
	public String getDelComment() {
		return delComment;
	}
	public void setDelComment( String delComment ) {
		this.delComment = delComment;
	}
	
	public String getCloseComment() {
		return closeComment;
	}
	public void setCloseComment( String closeComment ) {
		this.closeComment = closeComment;
	}

	public String getTestCompleteComment() {
		return testCompleteComment;
	}
	public void setTestCompleteComment( String testCompleteComment ) {
		this.testCompleteComment = testCompleteComment;
	}
}

