package jp.co.blueship.tri.am.domain.pjt.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理承認・承認待ち一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibPjtApproveWaitListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public List<ApplyInfoViewPjtApproveBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtApproveBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
			List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
}

