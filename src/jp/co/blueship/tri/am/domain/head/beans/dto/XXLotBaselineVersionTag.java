package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceBean;


/**
 *
 * ロット・ベースライン情報
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class XXLotBaselineVersionTag implements IServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット・ベースラインタグ */
	private String baselineTag = null ;
	/** ロット・VCSタグ */
	private String versionTag = null ;
	/** チェックイン日時 */
	private String checkInDate = null ;
	/** ロット・ベースライン情報 */
	private List<XXLotBaselineBean> lotBlViewBean = new ArrayList<XXLotBaselineBean>() ;

	public String getBaselineTag() {
		return baselineTag;
	}
	public void setBaselineTag(String baselineTag) {
		this.baselineTag = baselineTag;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}
	public List<XXLotBaselineBean> getBuildInfoList() {
		return lotBlViewBean;
	}
	public void setBuildInfoList(List<XXLotBaselineBean> buildInfoList) {
		this.lotBlViewBean = buildInfoList;
	}

}
