package jp.co.blueship.tri.am.domain.head.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibMergeConflictCheckResultDiffServiceBean  extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 *  ロット番号
	 */
	private String lotId;
	/**
	 * 文字コード
	 */
	private String encoding = null ;
	/**
	 *  チェック状況
	 */
	private List<IDiffElement> diffResultList = new ArrayList<IDiffElement>();
	/**
	 *  エラーメッセージ
	 */
	private String errMessage;
	/**
	 *  完了ステータス
	 */
	private String completeStatus;
	/**
	 * 選択された資産ファイル
	 */
	private String selectedResource ;

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getSelectedResource() {
		return selectedResource;
	}

	public void setSelectedResource(String selectedResource) {
		this.selectedResource = selectedResource;
	}

	public String getCompleteStatus() {
		return completeStatus;
	}

	public void setCompleteStatus(String completeStatus) {
		this.completeStatus = completeStatus;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public List<IDiffElement> getDiffResultList() {
		return diffResultList;
	}

	public void setDiffResultList(List<IDiffElement> diffResultList) {
		this.diffResultList = diffResultList;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

}
