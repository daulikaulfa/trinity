package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryLotListServiceBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * 変更管理・マージ マージ履歴ロット一覧画面の表示情報設定Class<br>
 * <br>
 *
 * @version V3L10.01
 * @author trinity V3
 *
 * @version V3L11.01
 * @author Yusna Marlina
 *
 * @version V3L12.01
 * @author Siti Hajar
 */
public class FlowChaLibMergeHistoryLotListService implements IDomain<FlowChaLibMergeHistoryLotListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;
	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeHistoryLotListServiceBean> execute( IServiceDto<FlowChaLibMergeHistoryLotListServiceBean> serviceDto ) {

		FlowChaLibMergeHistoryLotListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			LotCondition condition	= AmDBSearchConditionAddonUtils.getActivePjtLotConditionWithoutHead();
			condition.setUseMerge( StatusFlg.on );
			ISqlSort sort			= AmDBSearchSortAddonUtils.getPjtLotSortFromDesignDefineByChaLibMargeHistoryLotList();

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			IEntityLimit<ILotEntity> entityLimit =
				//ページ制御を実装するまでは、全件取得
//				this.support.getPjtLotDao().find( condition, sort, selectPageNo,
//								DesignProjectUtil.getIntValue(
//										DesignProjectChangecDefineId.maxPageNumberByTopMenu ) );
				this.support.getLotDao().find( condition.getCondition(), sort, 1, 0 );

			if ( TriStringUtils.isEmpty( entityLimit.getEntities() ) ) {
				paramBean.setInfoMessage( AmMessageId.AM001007E);
			}
			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
			List<ILotDto> lotDto = this.support.findLotDto( entityLimit.getEntities() );

			List<LotViewBean> lotViewBeanList =
				this.getLotViewBeanForMergeHistory( lotDto, paramBean.getUserId(), srvEntity.getBldSrvId() );
			if ( !TriStringUtils.isEmpty( entityLimit.getEntities() ) && TriStringUtils.isEmpty( lotViewBeanList )) {
				paramBean.setInfoMessage( AmMessageId.AM001122E );
			}

			paramBean.setLotViewBeanList	( lotViewBeanList );
			paramBean.setPageInfoView		(
					AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), entityLimit.getLimit() ));
			paramBean.setSelectPageNo		( selectPageNo );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005095S, e );
		}
	}

	/**
	 * ロットエンティティからロット画面用ビーンを作成します。
	 * @param entities ロットエンティティ
	 * @return ロット画面用ビーンのリスト
	 */
	public List<LotViewBean> getLotViewBeanForMergeHistory( List<ILotDto> lotDto, String userId, String srvId ) {

		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();
		AmViewInfoAddonUtils.populateLotViewBeanPjtLotEntity( lotViewBeanList, lotDto, support.getUmFinderSupport().findGroupByUserId(userId), srvId );

		for ( LotViewBean viewBean : lotViewBeanList ) {

			viewBean.setCompPjtCount	( support.countMergedByPjt		( viewBean.getLotNo() ));
			viewBean.setProgressPjtCount( support.countAliveByPjt	( viewBean.getLotNo() ));

			//ロット中のマージ済みのベースライン（ビルドパッケージ）を抽出する
			List<IHeadBlEntity> headBlEntityList = null;
			{
				HeadBlCondition condition = new HeadBlCondition() ;

				condition.setLotId( viewBean.getLotNo() ) ;
				condition.setStsIds( new String[]{
						AmHeadBlStatusId.Merged.getStatusId()} );

				ISqlSort sort = new SortBuilder();
				sort.setElement(HeadBlItems.mergeEndTimestamp, TriSortOrder.Desc, 0);

				headBlEntityList = this.support.getHeadBlDao().find( condition.getCondition(), sort );
			}

			if ( TriCollectionUtils.isNotEmpty( headBlEntityList ) ) {
				IHeadBlEntity headBlEntity = headBlEntityList.get(0);
				if( headBlEntity.getStsId().equals(AmHeadBlStatusId.Merged.getStatusId()) ) {
					viewBean.setMergeStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getStsId() ) ) ;
					viewBean.setMergeStatusId( AmHeadBlStatusId.value( headBlEntity.getStsId() ).name() );

					if( AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.equals( headBlEntity.getMergeStsCd() ) ){
						viewBean.setMergeWarning(true);
					}
				} else {
					viewBean.setMergeStatus( sheet.getValue( AmDesignBeanId.statusId , headBlEntity.getProcStsId() ) ) ;
					viewBean.setMergeStatusId( AmHeadBlStatusIdForExecData.value( headBlEntity.getProcStsId() ).name() );
				}
				viewBean.setMergeStartDate(  TriDateUtils.convertViewDateFormat( headBlEntity.getMergeStTimestamp() ) ) ;
				viewBean.setMergeEndDate(  TriDateUtils.convertViewDateFormat( headBlEntity.getMergeEndTimestamp() ) ) ;
			}
		}


		return lotViewBeanList;
	}

}
