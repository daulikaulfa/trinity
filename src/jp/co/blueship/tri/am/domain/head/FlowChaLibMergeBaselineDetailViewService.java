package jp.co.blueship.tri.am.domain.head;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 変更管理・マージ ベースライン詳細画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMergeBaselineDetailViewService implements IDomain<FlowChaLibMergeBaselineDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeBaselineDetailViewServiceBean> execute( IServiceDto<FlowChaLibMergeBaselineDetailViewServiceBean> serviceDto ) {

		FlowChaLibMergeBaselineDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			//String selectedBuildNo = paramBean.getSelectedBuildNo() ;

			//コンフリクトチェック状態の取得（ベースライン情報） マージ可能なベースラインが存在しなければ画面遷移しない
			ILotBlEntity lotBlEntity = this.support.getLotBlEntityByMergeEnableAtFirst( selectedLotNo );
			if( null == lotBlEntity ) {
				throw new ContinuableBusinessException( AmMessageId.AM001106E  , selectedLotNo ) ;
			}
			ILotBlDto lotBlDto = this.support.findLotBlDto( lotBlEntity.getLotBlId() );

			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			//コンフリクトチェック情報が古くなっていないか（後に他のマージが動作していないか）をチェックする。
			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;

			//ベースライン以前のＲＵ情報取得
			List<ILotBlDto> lotBlEntityArray = this.support.getLotBlDtoByMergeEnable( selectedLotNo , lotBlDto.getLotBlEntity().getLotChkinTimestamp() , TriSortOrder.Desc ) ;
			paramBean.setBuildViewBeanList( this.support.getLotBaselineGroupViewBean( lotBlEntityArray ) ) ;

			//ベースライン以前のＲＵに紐付く変更管理番号情報取得
			List<IPjtEntity> pjtEntityArray = this.support.getPjtEntities(
					lotBlEntityArray,AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView() ) ;
			paramBean.setPjtViewBeanList( AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( pjtEntityArray.toArray(new IPjtEntity[0]) ) ) ;


			String selectedCloseVersionTag = lotBlDto.getLotBlEntity().getLotVerTag();

			AmItemChkUtils.checkLotNo( selectedLotNo );


			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			// Scmモード
			paramBean.setScmType( scmType.value() );

			paramBean.setLotDetailViewBean( this.support.getLotDetailViewBean( paramBean, lotDto ));

			setServiceBeanRelUnitResult	( selectedLotNo, paramBean );

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			this.setServiceBeanBuildEntity		( selectedLotNo, selectedCloseVersionTag, selectPageNo, paramBean );

			// マージチェックコメント取得の為、HeadBlを取得する
			List<IHeadBlEntity> headBlEntity = this.support.getHeadBaselineList( lotBlDto.getLotBlEntity() );
			paramBean.setConflictComment( headBlEntity.get(0).getMergechkCmt() ) ;

			//マージ用の作業フォルダパス取得
			String workspace = lotDto.getLotEntity().getWsPath() ;
			String mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;

			//コンフリクトチェックログを取得し、マージ状態をチェックする

			//コンフリクトチェック結果ログの取得・解析
			String mergeReportFilePath = TriStringUtils.linkPathBySlash( mergeTemp , sheet.getValue( AmDesignEntryKeyByMerge.mergeResultFileName ) ) ;
			Object obj = TriFileUtils.readObjectFromFile( new File( mergeReportFilePath ) ) ;
			@SuppressWarnings("unchecked")
			Map<String,MergeResult> mergeResultMap = (Map<String,MergeResult>)obj ;

			paramBean.setMergeStatus( this.makeMergeStatus( mergeResultMap ) ) ;

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005090S, e );
		}
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return this.support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 * @param selectedLotNo ロット番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(
			String selectedLotNo, FlowChaLibMergeBaselineDetailViewServiceBean paramBean ) {

		IJdbcCondition successCountCondition =
			AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition( selectedLotNo );

		paramBean.setSucceededRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( successCountCondition.getCondition() ) );


		IJdbcCondition failureCountCondition =
			AmDBSearchConditionAddonUtils.getFailedRelUnitCondition( selectedLotNo );

		paramBean.setFailedRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( failureCountCondition.getCondition() ) );
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 * @param lotId ロット番号
	 * @param closeVersionTag ビルドパッケージタグ
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanBuildEntity(
									String lotId,
									String closeVersionTag,
									int selectPageNo,
									FlowChaLibMergeBaselineDetailViewServiceBean paramBean ) {

		List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( lotId, closeVersionTag, TriSortOrder.Desc );
		List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
		List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = this.support.getLotBaselineViewGroupByVersionTag( lotBlDtoList) ;
		paramBean.setBaselineViewBean( baselineViewBeanList.get( 0 ) ) ;
		//RUに紐付く変更管理のみを抽出
		List<String> pjtNoList = new ArrayList<String>() ;

		for( ILotBlDto lotBlDto : lotBlDtoList ) {
			List<ILotBlReqLnkEntity> lotBlReqLnkEntities = lotBlDto.getLotBlReqLnkEntities();
			for( ILotBlReqLnkEntity lotBlReqLnkEntity : lotBlReqLnkEntities ) {
				pjtNoList.add( lotBlReqLnkEntity.getPjtId() ) ;
			}
		}

		PageNoInfo page = new PageNoInfo();
		page.setMaxPageNo(1);
		page.setSelectPageNo(1);
		paramBean.setPageInfoView( page );
		paramBean.setSelectPageNo( 1 );
	}


	/**
	 * マージ結果を格納したMapをもとに、コンフリクト状況を判定し、MergeStatusを返す<br>
	 * @param mergeResultMap マージ結果を格納したMap
	 * @return MergeStatus
	 * @throws Exception
	 */
	private String makeMergeStatus( Map<String,MergeResult> mergeResultMap ) throws Exception {

		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		Iterator<MergeResult> iter = mergeResultMap.values().iterator() ;
		while( iter.hasNext() ) {
			MergeResult mergeResult = iter.next() ;
			if( MergeStatus.Conflicted.equals( mergeResult.getMergeStatus() ) ) {
				return ac.getMessage( TriLogMessage.LAM0037 );
			}
		}
		return ac.getMessage( TriLogMessage.LAM0036 );
	}
}
