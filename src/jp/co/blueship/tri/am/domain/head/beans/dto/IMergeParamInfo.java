package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

/**
 * アプリケーション層で、共通に使用するマージ情報を格納するインタフェースです。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public interface IMergeParamInfo extends ICommonParamInfo, Serializable {

	/**
	 * ロット・ベースラインDTOを取得します。
	 * @return 取得した値を戻します。
	 */
	public ILotBlDto getLotBlDto();

	/**
	 * ロット・ベースラインDTOを設定します。
	 * @param lotBlDto 対象となるロット・ベースラインDTO
	 */
	public void setLotBlDto ( ILotBlDto lotBlDto );

	/**
	 * 操作コメントを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getComment();
	/**
	 * 操作コメントを設定します。
	 * @param value 承認コメント
	 */
	public void setComment( String comment );
}