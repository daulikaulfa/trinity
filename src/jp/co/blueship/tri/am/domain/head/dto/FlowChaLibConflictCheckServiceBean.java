package jp.co.blueship.tri.am.domain.head.dto;

import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;

/**
 * コンフリクトチェックサービスDTO
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibConflictCheckServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ロットID */
	private String selectedLotId = null;

	/** 選択ロットのチェックイン・バージョンタグ */
	private String selectedLotVerTag = null;

	/**ロットベースライン情報 */
	private LotBaselineViewGroupByVersionTag headBlViewBean = null ;

	/** 変更管理編集入力情報 */
	private MergeEditInputBean MergeEditInputBean = null;

	/**
	 * ログサービス。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private LogBusinessFlow logService = new LogBusinessFlow();

	/**
	 * CacheしたロットDTO。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private ILotDto cacheLotDto;

	/**
	 * 選択されたベースラインDTO。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private ILotBlDto selectedLotBlDto;

	/**
	 * コンフリクトチェック時 マージエラー警告
	 */
	private WarningCheckBean warningCheckMergeCommitError = new WarningCheckBean() ;

	/**
	 * 登録したHEAD・ベースラインDTO。Domain層でのみ利用される
	 */
	private IHeadBlDto registerHeadBlDto;

	public String getSelectedLotId() {
		return selectedLotId;
	}
	public void setSelectedLotId( String selectedLotId ) {
		this.selectedLotId = selectedLotId;
	}

	public MergeEditInputBean getMergeEditInputBean() {
		return MergeEditInputBean;
	}
	public void setMergeEditInputBean(MergeEditInputBean mergeEditInputBean) {
		MergeEditInputBean = mergeEditInputBean;
	}
	public LotBaselineViewGroupByVersionTag getHeadBlViewBean() {
		return headBlViewBean;
	}
	public void setHeadBlViewBean(LotBaselineViewGroupByVersionTag headBlViewBean) {
		this.headBlViewBean = headBlViewBean;
	}
	public String getSelectedLotVersionTag() {
		return selectedLotVerTag;
	}
	public void setSelectedLotVersionTag(String selectedLotVerTag) {
		this.selectedLotVerTag = selectedLotVerTag;
	}
	/**
	 * 選択されたベースラインDTO。Domain層の上位で設定されDomain層下でのみ利用されるを取得します。
	 * @return 選択されたベースラインDTO。
	 */
	public ILotBlDto getSelectedLotBlDto() {
	    return selectedLotBlDto;
	}
	/**
	 * 選択されたベースラインDTO。Domain層の上位で設定されDomain層下でのみ利用されるを設定します。
	 * @param selectedLotBlDto 選択されたベースラインDTO。
	 */
	public void setSelectedLotBlDto(ILotBlDto selectedLotBlDto) {
	    this.selectedLotBlDto = selectedLotBlDto;
	}
	public WarningCheckBean getWarningCheckMergeCommitError() {
		return warningCheckMergeCommitError;
	}
	public void setWarningCheckMergeCommitError(
			WarningCheckBean warningCheckMergeCommitError) {
		this.warningCheckMergeCommitError = warningCheckMergeCommitError;
	}
	/**
	 * ログサービスを取得します。
	 * @return ログサービス
	 */
	public LogBusinessFlow getLogService() {
	    return logService;
	}
	/**
	 * ログサービスを設定します。
	 * @param logService ログサービス
	 */
	public void setLogService(LogBusinessFlow logService) {
	    this.logService = logService;
	}

	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されます。
	 *
	 * @return CacheしたロットDTO。
	 */
	public ILotDto getCacheLotDto() {
	    return cacheLotDto;
	}
	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されます。
	 *
	 * @param cacheLotDto CacheしたロットDTO。
	 */
	public void setCacheLotDto(ILotDto cacheLotDto) {
	    this.cacheLotDto = cacheLotDto;
	}
	/**
	 * 登録したHEAD・ベースラインDTO。Domain層でのみ利用されます。
	 * @return 登録したHEAD・ベースラインDTO。
	 */
	public IHeadBlDto getRegisterHeadBlDto() {
	    return registerHeadBlDto;
	}
	/**
	 * 登録したHEAD・ベースラインDTO。Domain層でのみ利用されます。
	 * @param registerHeadBlDto 登録したHEAD・ベースラインDTO。
	 */
	public void setRegisterHeadBlDto(IHeadBlDto registerHeadBlDto) {
	    this.registerHeadBlDto = registerHeadBlDto;
	}

}

