package jp.co.blueship.tri.am.domain.head.beans;

import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ServiceMatrixSupport;

/**
 * 機能サービスの制限をチェックします。
 * マージ運用を行わない場合に、利用できる機能に制限を加えます。
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 */
public class ServiceMatrixIfNotMergeRestriction extends ServiceMatrixSupport<Set<String>> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	protected void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key) {
		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		String serviceId = paramBean.getFlowAction();

		Set<String> serviceSet = this.getActions().get( key );

		if ( 0 == serviceSet.size() ) {
			//何も指定されていない場合は、全機能利用可能
			return;
		}

		if ( serviceSet.contains( serviceId ) ) {
			if ( TriStringUtils.isNotEmpty( paramBean.getLockLotNo() ) ) {
				String screenType = paramBean.getScreenType();

				if (ScreenType.bussinessException.equals(screenType)) {
					return;
				}

				ILotEntity lotEntity = support.findLotEntity( paramBean.getLockLotNo() );

				if ( StatusFlg.off.equals( lotEntity.isUseMerge() ) ) {
					throw new BusinessException( AmMessageId.AM001127E );
				}
			}
		}
	}

}
