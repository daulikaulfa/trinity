package jp.co.blueship.tri.am.domain.head;

import java.io.File;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultDiffServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.io.diff.TrinomialDiffer;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;



/**
 * FlowChaLibMergeConflictCheckResultDiffイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 マージＤｉｆｆ画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMergeConflictCheckResultDiffService implements IDomain<FlowChaLibMergeConflictCheckResultDiffServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();


	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeConflictCheckResultDiffServiceBean> execute( IServiceDto<FlowChaLibMergeConflictCheckResultDiffServiceBean> serviceDto) {

		FlowChaLibMergeConflictCheckResultDiffServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_DIFF ) &&
					!forward.equals( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_DIFF ) &&
					!referer.equals( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_TRINOMIAL_DIFF ) &&
					!forward.equals( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_TRINOMIAL_DIFF )
					) {
				return serviceDto;
			}

			if ( forward.equals(ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_DIFF)
					|| forward.equals(ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_TRINOMIAL_DIFF) ) {

				String lotId = paramBean.getLotNo() ;
				String selectedResource = paramBean.getSelectedResource() ;

				boolean diffMode = true ;

				if( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_DIFF.equals( forward ) ) {//Diff
					diffMode = true ;
				} else if( ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT_TRINOMIAL_DIFF.equals( forward ) ) {//３点Diff
					diffMode = false ;
				}

				/** Diff処理 **/
				DiffResult diffResult = this.diffProc( lotId , selectedResource , diffMode ) ;
				paramBean.setDiffResultList( diffResult.getDiffElementList() ) ;
				paramBean.setEncoding( ( null != diffResult.getCharsetSrc() ) ? diffResult.getCharsetSrc() : diffResult.getCharsetDst() ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面が表示されるclassはBaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException(AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}
	}

	/**
	 * diff処理を行う<br>
	 * @param lotNo ロット番号
	 * @param selectedResource Diff比較を行うファイルの相対パス
	 * @param diffMode Diff処理種別
	 * <pre>
	 * 		true	:	２点diff（ＨＥＡＤ・ロット）
	 * 		false	:	３点diff（ＨＥＡＤ・マージ結果・ロット）
	 * </pre>
	 * @return Diff処理結果
	 * @throws Exception
	 */
	private DiffResult diffProc( String lotId , String selectedResource , boolean diffMode ) throws Exception {

		//LOT情報取得
		ILotEntity lotEntity = this.support.findLotEntity( lotId ) ;
		String workspace = lotEntity.getWsPath() ;

		//HEAD情報取得
		String headWorkPath = null ;
		String mergeTemp = null ;
		String mergeTempBranchCheckOut = null ;

		IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;
		headWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( headEntity ).getPath() ;

		/** Diffで比較するワーキングコピーの存在チェック **/
		mergeTempBranchCheckOut = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTempBranchCheckOut ) ) ;
		if( true != new File( mergeTempBranchCheckOut ).exists() ) {
			throw new TriSystemException( AmMessageId.AM004043F , mergeTempBranchCheckOut ) ;
		}
		mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;
		if( true != new File( mergeTemp ).exists() ) {
			throw new TriSystemException( AmMessageId.AM004043F , mergeTemp ) ;
		}
		if( true != new File( headWorkPath ).exists() ) {
			throw new TriSystemException( AmMessageId.AM004043F , headWorkPath ) ;
		}

		DiffResult diffResult = null ;

		File srcFile = new File( TriStringUtils.linkPathBySlash( headWorkPath , selectedResource ) ) ;
		File dstFile = new File( TriStringUtils.linkPathBySlash( mergeTempBranchCheckOut , selectedResource ) ) ;

		if( true == diffMode ) {//Diff

			EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer( srcFile , dstFile ) ;
			differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;
			diffResult = differ.diff() ;
		} else {//３点Diff

			File mergedFile = new File( TriStringUtils.linkPathBySlash( mergeTemp , selectedResource ) ) ;

			TrinomialDiffer differ = new TrinomialDiffer() ;
			differ.setCharsetMode( TrinomialDiffer.CharsetMode.SAME ) ;
			differ.setSrcFile( srcFile ) ;
			differ.setDestFile( dstFile ) ;
			differ.setMergedFile( mergedFile ) ;
			differ.setSkipwordList( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

			diffResult = differ.execute() ;
		}

		return diffResult ;
	}
}
