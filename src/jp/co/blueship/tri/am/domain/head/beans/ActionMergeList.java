package jp.co.blueship.tri.am.domain.head.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionListAbstract;

/**
 * マージ処理のトランザクション内の最上位ドメイン層として利用する<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Siti Hajar
 */
public class ActionMergeList extends ActionListAbstract<FlowChaLibMergeCommitServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute( IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto ) {
		FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();

		String flowActionTitle = null;
		boolean isSuccess = false;

		LogBusinessFlow logService = paramBean.getLogService();

		try {
			List<Object> paramList = serviceDto.getParamList();

			IMergeParamInfo[] paramArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
			if ( TriStringUtils.isEmpty( paramArray ) ) {
				throw new TriSystemException( AmMessageId.AM005018S ) ;
			}

			ILotDto lotDto = paramBean.getCacheLotDto();

			// ログの初期処理
			logService.makeLogPath(paramBean.getFlowAction(), lotDto.getLotEntity());
			// ログの開始ラベル出力
			flowActionTitle = LogBusinessFlow.makeFlowActionTitle(paramBean.getFlowAction());
			logService.writeLogWithDateBegin(flowActionTitle);

			// ログのヘッダ部出力
			this.outLogHeader(logService, paramBean, paramArray);

			if ( null != this.getGathering()) {
				this.getGathering().execute( serviceDto );
			}

			this.beforeAdvices( serviceDto );
			this.actions( serviceDto );
			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;
			if (null != logService) {
				logService.writeLog(e.getStackTraceString());
			}

			this.throwAdvices( serviceDto );
			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			if (null != logService) {
				logService.writeLog(ExceptionUtils.getStackTraceString(e));
			}

			this.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		} finally {
			try {
				this.afterAdvices( serviceDto );

				if (null != flowActionTitle) {
					int count = serviceDto.getServiceBean().getCountBySkipResource();
					if( 0 < count ) {
						logService.writeLog( AmMessageId.AM002000W , String.valueOf(count));
					}

					logService.writeLogWithDateEnd(flowActionTitle);

					if (isSuccess) {
						logService.deleteLog(AmDesignEntryKeyByLogs.deleteLogAtSuccessMergeCommit);
					}
				}
			} catch ( Exception e ) {
				this.throwAdvices( serviceDto );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( SmMessageId.SM005052S , e );
			}
		}

		return serviceDto;

	}

	/**
	 * ログのヘッダ部分を出力する
	 *
	 * @param logService
	 * @param paramBean
	 * @param paramArray
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logService,
			FlowChaLibMergeCommitServiceBean paramBean,
			IMergeParamInfo[] paramArray) throws Exception {

		ILotEntity lotEntity = paramBean.getCacheLotDto().getLotEntity();
		ILotBlEntity selectedLotBlEntity = paramBean.getSelectedLotBlDto().getLotBlEntity();

		StringBuilder stb = new StringBuilder();
		stb = logService.makeUserAndLotLabel(stb, paramBean, lotEntity.getLotNm(), lotEntity.getLotId());
		stb = logService.makeBaseLineLabel(stb, selectedLotBlEntity.getLotBlTag(), selectedLotBlEntity.getLotVerTag());

		// ベースラインに含まれる情報
		for (IMergeParamInfo paramInfo : paramArray) {
			String dataId = paramInfo.getLotBlDto().getLotBlEntity().getDataId();
			stb = logService.makeCheckInIdLabel(stb, dataId);
		}
		logService.writeLog(stb.toString());
	}

}
