package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceBean;


/**
 *
 * ロット・ベースライン情報
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class LotBaselineViewGroupByVersionTag implements IServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット・ベースラインタグ */
	private String baselineTag = null ;
	/** ロット・VCSタグ */
	private String versionTag = null ;
	/** チェックイン日時 */
	private String checkInDate = null ;
	/** 前回マージチェック結果ステータス **/
	private String mergeCheckStatus = null;
	/** 前回マージチェック結果ステータスID **/
	private String mergeCheckStatusId = null;
	/** 前回マージチェック結果開始日時 **/
	private String mergeCheckStartDate = null;
	/** 前回マージチェック結果終了日時 **/
	private String mergeCheckEndDate = null;
	/** 前回マージステータス **/
	private String mergeStatus = null;
	/** 前回マージステータスID **/
	private String mergeStatusId = null;
	/** 前回マージ開始日時 **/
	private String mergeStartDate = null;
	/** 前回マージ終了日時 **/
	private String mergeEndDate = null;
	/** ロット・ベースライン情報 */
	private List<LotBaselineViewBean> lotBlViewBean = new ArrayList<LotBaselineViewBean>() ;

	public String getBaselineTag() {
		return baselineTag;
	}
	public void setBaselineTag(String baselineTag) {
		this.baselineTag = baselineTag;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	public List<LotBaselineViewBean> getBuildInfoList() {
		return lotBlViewBean;
	}
	public void setBuildInfoList(List<LotBaselineViewBean> buildInfoList) {
		this.lotBlViewBean = buildInfoList;
	}
	public String getMergeCheckStatus() {
	    return mergeCheckStatus;
	}
	public void setMergeCheckStatus(String mergeCheckStatus) {
	    this.mergeCheckStatus = mergeCheckStatus;
	}
	public String getMergeCheckStatusId() {
	    return mergeCheckStatusId;
	}
	public void setMergeCheckStatusId(String mergeCheckStatusId) {
	    this.mergeCheckStatusId = mergeCheckStatusId;
	}
	public String getMergeCheckStartDate() {
	    return mergeCheckStartDate;
	}
	public void setMergeCheckStartDate(String mergeCheckStartDate) {
	    this.mergeCheckStartDate = mergeCheckStartDate;
	}
	public String getMergeCheckEndDate() {
	    return mergeCheckEndDate;
	}
	public void setMergeCheckEndDate(String mergeCheckEndDate) {
	    this.mergeCheckEndDate = mergeCheckEndDate;
	}
	public String getMergeStatus() {
	    return mergeStatus;
	}
	public void setMergeStatus(String mergeStatus) {
	    this.mergeStatus = mergeStatus;
	}
	public String getMergeStatusId() {
	    return mergeStatusId;
	}
	public void setMergeStatusId(String mergeStatusId) {
	    this.mergeStatusId = mergeStatusId;
	}
	public String getMergeStartDate() {
	    return mergeStartDate;
	}
	public void setMergeStartDate(String mergeStartDate) {
	    this.mergeStartDate = mergeStartDate;
	}
	public String getMergeEndDate() {
	    return mergeEndDate;
	}
	public void setMergeEndDate(String mergeEndDate) {
	    this.mergeEndDate = mergeEndDate;
	}

}