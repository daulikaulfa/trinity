package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;

/**
 * ロット原本チェックインサービスのDTO
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Siti Hajar
 */
public class ActionLotCheckInServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * ログサービス
	 */
	private LogBusinessFlow logService;
	/**
	 * ロット・VCSタグ
	 */
	private String lotVerTag;
	/**
	 * ロット・ベースラインタグ
	 */
	private String lotBlTag;
	/**
	 * チェックイン日時
	 */
	private Timestamp lotChkinTimestamp;
	/**
	 * チェックインコメント
	 */
	private String lotChkinCmt;


	/**
	 * 対象ロットDTO。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private ILotDto targetLotDto;

	/**
	 * 対象資産申請DTOのList。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	List<IAreqDto> targetAreqDtoList;

	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用される
	 */
	private List<ILotBlDto> registerLotBlDto = new ArrayList<ILotBlDto>();

	/**
	 * コミット処理時に処理されずにスキップされた資産数
	 */
	private int countBySkipResource;

	/**
	 * ログサービスを取得します。
	 * @return ログサービス
	 */
	public LogBusinessFlow getLogService() {
	    return logService;
	}

	/**
	 * ログサービスを設定します。
	 * @param logService ログサービス
	 */
	public void setLogService(LogBusinessFlow logService) {
	    this.logService = logService;
	}

	/**
	 * ロット・VCSタグを取得します。
	 * @return ロット・VCSタグ
	 */
	public String getLotVerTag() {
	    return lotVerTag;
	}

	/**
	 * ロット・VCSタグを設定します。
	 * @param lotVerTag ロット・VCSタグ
	 */
	public void setLotVerTag(String lotVerTag) {
	    this.lotVerTag = lotVerTag;
	}

	/**
	 * ロット・ベースラインタグを取得します。
	 * @return ロット・ベースラインタグ
	 */
	public String getLotBlTag() {
	    return lotBlTag;
	}

	/**
	 * ロット・ベースラインタグを設定します。
	 * @param lotBlTag ロット・ベースラインタグ
	 */
	public void setLotBlTag(String lotBlTag) {
	    this.lotBlTag = lotBlTag;
	}

	/**
	 * チェックイン日時を取得します。
	 * @return チェックイン日時
	 */
	public Timestamp getLotChkinTimestamp() {
	    return lotChkinTimestamp;
	}

	/**
	 * チェックイン日時を設定します。
	 * @param chkinTimestamp チェックイン日時
	 */
	public void setLotChkinTimestamp(Timestamp chkinTimestamp) {
	    this.lotChkinTimestamp = chkinTimestamp;
	}

	/**
	 * チェックインコメントを取得します。
	 * @return チェックインコメント
	 */
	public String getLotChkinCmt() {
	    return lotChkinCmt;
	}

	/**
	 * チェックインコメントを設定します。
	 * @param lotChkinCmt チェックインコメント
	 */
	public void setLotChkinCmt(String lotChkinCmt) {
	    this.lotChkinCmt = lotChkinCmt;
	}

	/**
	 * 対象ロットDTO。Domain層の上位で設定されDomain層下でのみ利用されます。
	 * @return 対象ロットDTO。
	 */
	public ILotDto getTargetLotDto() {
	    return targetLotDto;
	}

	/**
	 * 対象ロットDTO。Domain層の上位で設定されDomain層下でのみ利用されます。
	 * @param targetLotDto 対象ロットDTO。
	 */
	public void setTargetLotDto(ILotDto targetLotDto) {
	    this.targetLotDto = targetLotDto;
	}
	/**
	 * 対象資産申請DTOのList。Domain層の上位で設定されDomain層下でのみ利用されます。
	 * @return 対象資産申請DTOのList。
	 */
	public List<IAreqDto> getTargetAreqDtoList() {
	    return targetAreqDtoList;
	}

	/**
	 * 対象資産申請DTOのList。Domain層の上位で設定されDomain層下でのみ利用されます。
	 * @param targetAreqDtoList 対象資産申請DTOのList。
	 */
	public void setTargetAreqDtoList(List<IAreqDto> targetAreqDtoList) {
	    this.targetAreqDtoList = targetAreqDtoList;
	}

	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用されます。
	 * @return 登録したロット・ベースラインDTO。
	 */
	public List<ILotBlDto> getRegisterLotBlDto() {
	    return registerLotBlDto;
	}
	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用されます。
	 * @param registerLotBlDto 登録したロット・ベースラインDTO。
	 */
	public void setRegisterLotBlDto(List<ILotBlDto> registerLotBlDto) {
	    this.registerLotBlDto = registerLotBlDto;
	}

	/**
	 * コミット処理時に処理されずにスキップされた資産数を取得します。
	 * @return コミット処理時に処理されずにスキップされた資産数
	 */
	public int getCountBySkipResource() {
	    return countBySkipResource;
	}

	/**
	 * コミット処理時に処理されずにスキップされた資産数を設定します。
	 * @param countBySkipResource コミット処理時に処理されずにスキップされた資産数
	 */
	public void setCountBySkipResource(int countBySkipResource) {
	    this.countBySkipResource = countBySkipResource;
	}

}
