package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.io.Serializable;
import java.util.Set;

import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;

public class MergeResult implements Serializable {

	private static final long serialVersionUID = -2491322999912744378L;
	private String lotId = null ;
//	private String pjtNo = null ;
//	private String applyNo = null ;
	private String moduleName = null ;
	private String path = null ;
	private Status binaryDiffStatus = null ;
	private MergeStatus mergeStatus = null ;

	private Set<String> pjtIdSet = null ;
	private Set<String> applyNoSet = null ;


	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
/*	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}*/

	public MergeStatus getMergeStatus() {
		return this.mergeStatus;
	}
	public void setMergeStatus(MergeStatus mergeStatus) {
		this.mergeStatus = mergeStatus;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public Status getBinaryDiffStatus() {
		return binaryDiffStatus;
	}
	public void setBinaryDiffStatus(Status binaryDiffStatus) {
		this.binaryDiffStatus = binaryDiffStatus;
	}
	public Set<String> getApplyNoSet() {
		return applyNoSet;
	}
	public void setApplyNoSet(Set<String> applyNoSet) {
		this.applyNoSet = applyNoSet;
	}
	public Set<String> getPjtNoSet() {
		return pjtIdSet;
	}
	public void setPjtNoSet(Set<String> pjtIdSet) {
		this.pjtIdSet = pjtIdSet;
	}


}
