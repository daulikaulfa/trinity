package jp.co.blueship.tri.am.domain.head.beans;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlMdlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadLotBlLnkCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * マージ情報のステータスをコンフリクトチェック処理中に更新します。<br>
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public class ActionRegisterBaselineByConflictCheck extends ActionPojoAbstract<FlowChaLibConflictCheckServiceBean> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibConflictCheckServiceBean> execute( IServiceDto<FlowChaLibConflictCheckServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
			throw new TriSystemException( AmMessageId.AM005018S ) ;
		}

		FlowChaLibConflictCheckServiceBean paramBean = serviceDto.getServiceBean();
		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		this.removeHeadBl( lotEntity.getLotId() );

		IHeadBlEntity headBlEntity = this.insertHeadBl( paramBean );
		this.insertHeadLotBlLnk( paramInfoArray, headBlEntity );
		this.insertHeadBlMdlLnk( paramInfoArray, headBlEntity, lotDto.getIncludeMdlEntities(true) );

		paramBean.setRegisterHeadBlDto( this.support.findHeadBlDto( headBlEntity.getHeadBlId() ));

		//HEAD・ベースラインの実行中ステータス(Logical Key = ロットID)
		{
			this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_HEAD_BL, lotEntity.getLotId());
			this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_HEAD_BL, AmHeadBlStatusIdForExecData.ConflictChecking, lotEntity.getLotId());
		}

		//ロット・ベースラインの実行中ステータス(Logical Key = データID)
		{
			LotBlCondition condition = new LotBlCondition();
			condition.setLotId( lotEntity.getLotId() );
			condition.setStsIds(
					AmLotBlStatusId.CommittedToLot.getStatusId() );
			List<ILotBlEntity> lotBlEntities = this.support.getLotBlDao().find( condition.getCondition() );

			String[] dataIds = FluentList.from(lotBlEntities).map( AmFluentFunctionUtils.toDataIdsFromLotBl ).toArray( new String[0] );

			this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_LOT_BL, dataIds);
			this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.ConflictChecking, dataIds);
		}

		return serviceDto;
	}

	/**
	 * HEAD・ベースライン削除
	 *
	 * @param lotId
	 */
	private void removeHeadBl( String lotId ) {
		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );
		condition.setStsIds( new String[]{
				AmHeadBlStatusId.Unspecified.getStatusId(),
				AmHeadBlStatusId.ConflictChecked.getStatusId(),
				});

		List<IHeadBlEntity> headEntities = this.support.getHeadBlDao().find(condition.getCondition());
		String[] headBlIds = FluentList.from(headEntities).map(AmFluentFunctionUtils.toHeadBlIds).toArray(new String[0]);

		if ( TriStringUtils.isEmpty( headBlIds ) ) {
			return;
		}

		{
			HeadBlCondition headBlCondition = new HeadBlCondition();
			headBlCondition.setHeadBlIds( headBlIds );

			IHeadBlEntity headBlEntity = new HeadBlEntity();
			headBlEntity.setDelStsId( StatusFlg.on );

			this.support.getHeadBlDao().update(headBlCondition.getCondition(), headBlEntity);
		}

		{
			HeadLotBlLnkCondition headLotBlLnkCondition = new HeadLotBlLnkCondition();
			headLotBlLnkCondition.setHeadBlIds( headBlIds );

			IHeadLotBlLnkEntity headLotBlLnkEntity = new HeadLotBlLnkEntity();
			headLotBlLnkEntity.setDelStsId( StatusFlg.on );

			this.support.getHeadLotBlLnkDao().update(headLotBlLnkCondition.getCondition(), headLotBlLnkEntity);
		}

		{
			HeadBlMdlLnkCondition mdlLnkCondition = new HeadBlMdlLnkCondition();
			mdlLnkCondition.setHeadBlIds( headBlIds );

			IHeadBlMdlLnkEntity mdlLnkEntity = new HeadBlMdlLnkEntity();
			mdlLnkEntity.setDelStsId( StatusFlg.on );

			this.support.getHeadBlMdlLnkDao().update(mdlLnkCondition.getCondition(), mdlLnkEntity);
		}

	}

	/**
	 * HEAD・ベースライン登録
	 *
	 * @param param
	 * @return 登録したHEAD・ベースラインEntity
	 */
	private IHeadBlEntity insertHeadBl( FlowChaLibConflictCheckServiceBean param ) {
		MergeEditInputBean inputBean = param.getMergeEditInputBean();

		IHeadBlEntity headBlEntity = new HeadBlEntity();

		headBlEntity.setHeadBlId( this.support.nextvalByHeadBlId() );
		headBlEntity.setLotId( param.getCacheLotDto().getLotEntity().getLotId() );
		headBlEntity.setHeadVerTag( null );
		headBlEntity.setHeadBlTag( null );
		headBlEntity.setStsId( AmHeadBlStatusId.Unspecified.getStatusId() );
		headBlEntity.setMergechkUserId( param.getUserId() );
		headBlEntity.setMergechkUserNm( param.getUserName() );
		headBlEntity.setMergechkStTimestamp( TriDateUtils.getSystemTimestamp() );
		headBlEntity.setMergechkEndTimestamp( null );
		headBlEntity.setMergechkCmt( inputBean.getConflictCheckComment() );
		headBlEntity.setDelStsId( StatusFlg.off );
		headBlEntity.setMergeStsCd(AmHeadBlMergeStatusCode.LOT_UN_MERGE_CHECK.getStatusCode() );

		this.support.getHeadBlDao().insert( headBlEntity );

		return headBlEntity;
	}

	/**
	 * HEAD・ロット・ベースライン
	 *
	 * @param paramInfoArray
	 * @param headBlEntity
	 */
	private void insertHeadLotBlLnk( IMergeParamInfo[] paramInfoArray, IHeadBlEntity headBlEntity ) {
		List<IHeadLotBlLnkEntity> headLotBlLnkEntities = new ArrayList<IHeadLotBlLnkEntity>();

		for ( IMergeParamInfo paramInfo: paramInfoArray ) {
			ILotBlEntity lotBlEntity = paramInfo.getLotBlDto().getLotBlEntity();

			IHeadLotBlLnkEntity headLotBlLnkEntity = new HeadLotBlLnkEntity();
			headLotBlLnkEntity.setHeadBlId( headBlEntity.getHeadBlId() );
			headLotBlLnkEntity.setLotBlId( lotBlEntity.getLotBlId() );
			headLotBlLnkEntity.setDelStsId( StatusFlg.off );

			headLotBlLnkEntities.add( headLotBlLnkEntity );
		}

		this.support.getHeadLotBlLnkDao().insert( headLotBlLnkEntities );

		return;
	}

	/**
	 * HEAD・ベースライン・モジュール登録
	 *
	 * @param paramInfoArray
	 * @param headBlEntity
	 * @param mdlEntities
	 */
	private void insertHeadBlMdlLnk( IMergeParamInfo[] paramInfoArray, IHeadBlEntity headBlEntity, List<ILotMdlLnkEntity> mdlEntities ) {
		Map<String, IHeadBlMdlLnkEntity> headBlMdlLnkMap = new LinkedHashMap<String, IHeadBlMdlLnkEntity>();

		for ( IMergeParamInfo paramInfo: paramInfoArray ) {
			Set<String> modifiedModuleSet = new TreeSet<String>();
			this.setModuleName(paramInfo.getLotBlDto().getCheckInMdlEntities(true), modifiedModuleSet);

			for ( ILotMdlLnkEntity lotMdlEntity : mdlEntities ) {
				if ( headBlMdlLnkMap.containsKey( lotMdlEntity.getMdlNm() ) ) {
					continue;
				}

				IHeadBlMdlLnkEntity mdlEntity = new HeadBlMdlLnkEntity();

				mdlEntity.setHeadBlId( headBlEntity.getHeadBlId() );
				mdlEntity.setMdlNm( lotMdlEntity.getMdlNm() );
				mdlEntity.setMdlVerTag( lotMdlEntity.getMdlVerTag() );

				if ( modifiedModuleSet.contains(lotMdlEntity.getMdlNm()) ) {
					mdlEntity.setMergeTargetMdl( StatusFlg.on );
				} else {
					mdlEntity.setMergeTargetMdl( StatusFlg.off );
				}

				mdlEntity.setMergechkHeadRev( null );
				mdlEntity.setMergeHeadRev( null );
				mdlEntity.setDelStsId( StatusFlg.off );

				headBlMdlLnkMap.put( mdlEntity.getMdlNm(), mdlEntity );
			}
		}

		this.support.getHeadBlMdlLnkDao().insert( new ArrayList<IHeadBlMdlLnkEntity>(headBlMdlLnkMap.values()) );
	}

	/**
	 * ロット・ベースライン・モジュールからモジュール名を抽出する
	 * @param mdlLnkEntities ロット・ベースライン・モジュールエンティティのList
	 * @param modifiedModuleSet 抽出したモジュール名を格納するセット
	 */
	private void setModuleName(
			List<ILotBlMdlLnkEntity> mdlLnkEntities, Set<String> modifiedModuleSet ) {

		for ( ILotBlMdlLnkEntity mdlEntity : mdlLnkEntities ) {
			modifiedModuleSet.add( mdlEntity.getMdlNm() );
		}
	}

}
