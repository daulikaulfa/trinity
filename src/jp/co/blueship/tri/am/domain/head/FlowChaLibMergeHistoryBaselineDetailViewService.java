package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 変更管理・マージ履歴 ベースライン詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibMergeHistoryBaselineDetailViewService implements IDomain<FlowChaLibMergeHistoryBaselineDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeHistoryBaselineDetailViewServiceBean> execute(IServiceDto<FlowChaLibMergeHistoryBaselineDetailViewServiceBean> serviceDto) {

		FlowChaLibMergeHistoryBaselineDetailViewServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String lotId = paramBean.getSelectedLotNo();
			String closeVersionTag = paramBean.getSelectedCloseVersionTag();

			AmItemChkUtils.checkLotNo(lotId);

			ILotDto lotDto = this.support.findLotDto( lotId );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			// Scmモード
			VcsCategory scmType = VcsCategory.value(lotDto.getLotEntity().getVcsCtgCd());
			paramBean.setScmType(scmType.value());

			paramBean.setLotDetailViewBean(this.support.getLotDetailViewBean(paramBean, lotDto));

			List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag(lotId, closeVersionTag, TriSortOrder.Desc);
			List<IHeadBlEntity> headBlEntityList = this.support.getHeadBaselineList( lotBlEntityList.get(0) );

			setServiceBeanRelUnitResult(lotId, paramBean);

			int selectPageNo = (0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo();

			this.setServiceBeanBuildEntity(lotId, closeVersionTag, selectPageNo, paramBean);

			// ベースライン以前のＲＵ情報取得
			List<ILotBlDto> lotBlEntityArray = this.support.getLotBlDtoByMergedBaseline(lotId, headBlEntityList.get(0), false);

			paramBean.setBuildViewBeanList(this.support.getLotBaselineGroupViewBean(lotBlEntityArray));

			// ベースライン以前のＲＵに紐付く変更管理番号情報取得
			List<IPjtEntity> pjtEntityArray = this.support.getPjtEntities(lotBlEntityArray,
					AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView());
			paramBean.setPjtViewBeanList(AmViewInfoAddonUtils.setPjtViewBeanPjtEntity( pjtEntityArray.toArray(new IPjtEntity[0]) ));

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005086S, e);
		}
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return this.support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 *
	 * @param selectedLotNo ロット番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(String selectedLotNo, FlowChaLibMergeHistoryBaselineDetailViewServiceBean paramBean) {

		IJdbcCondition successCountCondition = AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition(selectedLotNo);

		paramBean.setSucceededRelUnitCount(this.support.getBmFinderSupport().getBpDao().count(successCountCondition.getCondition()));

		IJdbcCondition failureCountCondition = AmDBSearchConditionAddonUtils.getFailedRelUnitCondition(selectedLotNo);

		paramBean.setFailedRelUnitCount(this.support.getBmFinderSupport().getBpDao().count(failureCountCondition.getCondition()));
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 *
	 * @param lotId ロット番号
	 * @param closeVersionTag ビルドパッケージタグ
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanBuildEntity(String lotId, String closeVersionTag, int selectPageNo,
			FlowChaLibMergeHistoryBaselineDetailViewServiceBean paramBean) {

		List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag(lotId, closeVersionTag, TriSortOrder.Desc);
		List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
		List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = this.support.getLotBaselineViewGroupByVersionTag(lotBlDtoList);

		paramBean.setBaselineViewBean(baselineViewBeanList.get(0));
		// RUに紐付く変更管理のみを抽出
		List<String> pjtNoList = new ArrayList<String>();
		for (ILotBlDto lotBlDto : lotBlDtoList) {
			List<ILotBlReqLnkEntity> lotBlReqLnkEntities = lotBlDto.getLotBlReqLnkEntities();
			for (ILotBlReqLnkEntity lotBlReqEntity : lotBlReqLnkEntities) {
				pjtNoList.add(lotBlReqEntity.getPjtId());
			}
		}

		IJdbcCondition condition	= AmDBSearchConditionAddonUtils.getPjtCondition( pjtNoList.toArray(new String[0]) );

		IEntityLimit<IPjtEntity> pjtEntityLimit = support.getPjtDao().find(condition.getCondition(),
				AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeHistoryBaselineDetailView(), 1, 0);

		paramBean.setPageInfoView(AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), pjtEntityLimit.getLimit()));
		paramBean.setSelectPageNo(selectPageNo);
	}

}
