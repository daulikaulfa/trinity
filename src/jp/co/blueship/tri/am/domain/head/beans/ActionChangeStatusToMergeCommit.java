package jp.co.blueship.tri.am.domain.head.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * マージ情報のステータスを正常更新します。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class ActionChangeStatusToMergeCommit extends ActionPojoAbstract<FlowChaLibMergeCommitServiceBean> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute( IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto ) {
		FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();

		List<Object> paramList = serviceDto.getParamList();

		// 正常終了した場合のみ処理を行う
		if ( ! SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) ) {
			return serviceDto;
		}

		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
			throw new TriSystemException( AmMessageId.AM005018S ) ;
		}

		//HEAD・ベースラインの実行中ステータス(Logical Key = ロットID)
		{
			this.support.getSmFinderSupport().cleaningExecDataSts(
					paramBean.getProcId(), AmTables.AM_HEAD_BL, lotEntity.getLotId() );
		}

		//ロット・ベースラインの実行中ステータス(Logical Key = データID)
		{
			Set<String> dataIdSet = new HashSet<String>();
			for ( IMergeParamInfo param: paramInfoArray ) {
				dataIdSet.add(param.getLotBlDto().getLotBlEntity().getDataId());
			}

			String[] dataIds = dataIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().cleaningExecDataSts(paramBean.getProcId(), AmTables.AM_LOT_BL, dataIds);
		}

		//変更情報の実行中ステータス
		{
			Set<String> pjtIdSet = new HashSet<String>();

			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();
				pjtIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toPjtIdsFromLotBlReqLnk ).asList() );
			}

			String[] pjtIds = pjtIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().cleaningExecDataSts(paramBean.getProcId(), AmTables.AM_PJT, pjtIds);
		}

		//資産申請情報の実行中ステータス
		{
			Set<String> areqIdSet = new HashSet<String>();

			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();
				areqIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk ).asList() );
			}

			String[] areqIds = areqIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().cleaningExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, areqIds);
		}

		return serviceDto;
	}

}
