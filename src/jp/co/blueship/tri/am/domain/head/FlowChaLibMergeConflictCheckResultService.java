package jp.co.blueship.tri.am.domain.head;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeViewBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * 変更管理・マージ コンフリクトチェック結果画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMergeConflictCheckResultService implements IDomain<FlowChaLibMergeConflictCheckResultServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeConflictCheckResultServiceBean> execute(IServiceDto<FlowChaLibMergeConflictCheckResultServiceBean> serviceDto) {

		FlowChaLibMergeConflictCheckResultServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String lotId = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo(lotId);

			// グループの存在チェック
			ILotDto lotDto = this.support.findLotDto( lotId );

			AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			String closeVersionTag = paramBean.getSelectedCloseVersionTag();

			paramBean.setSelectedLotNo(lotId);
			paramBean.setSelectedCloseVersionTag(closeVersionTag);

			List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag(lotId, closeVersionTag, TriSortOrder.Desc);
			List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto(lotBlEntityList);
			List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = this.support.getLotBaselineViewGroupByVersionTag(lotBlDtoList);
			paramBean.setBaselineViewBean(baselineViewBeanList.get(0));

			// マージ用の作業フォルダパス取得
			String workspace = lotDto.getLotEntity().getWsPath();
			String mergeTemp = TriStringUtils.linkPathBySlash(workspace, sheet.getValue(AmDesignEntryKeyByMerge.mergeTemp));

			// 画面表示用にマージ結果Viewを作成
			String mergeReportFilePath = TriStringUtils.linkPathBySlash(mergeTemp, sheet.getValue(AmDesignEntryKeyByMerge.mergeResultFileName));
			Object obj = TriFileUtils.readObjectFromFile(new File(mergeReportFilePath));
			@SuppressWarnings("unchecked")
			Map<String, MergeResult> mergeResultMap = (Map<String, MergeResult>) obj;
			// Map<String,MergeResult> mergeResultMap =
			// paramBean.getMergeResultMap() ;
			List<MergeViewBean> mergeViewBeanList = this.makeMergeViewBean(lotId, mergeResultMap);
			paramBean.setMergeViewBeanList(mergeViewBeanList);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005093S, e);
		}
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return this.support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * MergeResultをもとに、画面表示向けのMergeViewBeanを生成する
	 *
	 * @param lotNo
	 * @param mergeResultMap
	 * @return MergeViewBeanのList
	 */
	private List<MergeViewBean> makeMergeViewBean(String lotId, Map<String, MergeResult> mergeResultMap) throws Exception {
		List<MergeViewBean> mergeViewBeanList = new ArrayList<MergeViewBean>();
		Map<String, IExtEntity> ucfExtensionMap = support.getExtMap();

		// MergeResultのソート（モジュール名＋ファイルパス名）
		List<MergeResult> mergeResultList = new ArrayList<MergeResult>();
		Set<String> pjtIdSet = new HashSet<String>();

		for (MergeResult mergeResult : mergeResultMap.values()) {
			mergeResultList.add(mergeResult);
			pjtIdSet.addAll(mergeResult.getPjtNoSet());
		}
		MergeResult[] mergeResultArray = mergeResultList.toArray(new MergeResult[0]);
		this.sortDefaultMergeResultList(mergeResultArray);

		// 変更管理情報の取得
		PjtCondition pjtCondition = new PjtCondition();
		{
			pjtCondition.setLotId(lotId);

			String[] pjtNoArray = pjtIdSet.toArray(new String[0]);
			if (TriStringUtils.isEmpty(pjtNoArray)) {
				// ダミーを設定して、全件検索を防止する。
				pjtCondition.setPjtIds(new String[] { "" });
			} else {
				pjtCondition.setPjtIds(pjtNoArray);
			}
		}

		IEntityLimit<IPjtEntity> pjtLimit = support.getPjtDao().find(pjtCondition.getCondition(), new SortBuilder(), 1, 0);
		Map<String, IPjtEntity> pjtMap = new HashMap<String, IPjtEntity>();
		for (IPjtEntity entity : pjtLimit.getEntities()) {
			pjtMap.put(entity.getPjtId(), entity);
		}

		// MergeViewBeanの生成
		for (MergeResult mergeResult : mergeResultArray) {

			MergeViewBean mergeViewBean = new MergeViewBean();
			mergeViewBean.setLotNo(mergeResult.getLotNo());
			mergeViewBean.setModuleName(mergeResult.getModuleName());
			mergeViewBean.setFileName(mergeResult.getPath());
			mergeViewBean.setMergeStatus(mergeResult.getMergeStatus().getStatusId());
			mergeViewBean.setDiffStatus(mergeResult.getBinaryDiffStatus().toString());

			// バイナリ資産or拡張子未登録の資産はDiffボタンを非表示にする
			mergeViewBean.setEnabled( !AmBusinessFileUtils.isBinary(mergeResult.getPath(), ucfExtensionMap) );

			// 申請情報番号
			List<String> applyNoList = new ArrayList<String>();
			for (String applyNo : mergeResult.getApplyNoSet()) {
				applyNoList.add(applyNo);
			}
			mergeViewBean.setApplyNoList(applyNoList);

			// 変更管理情報
			List<PjtViewBean> pjtViewList = new ArrayList<PjtViewBean>();
			Iterator<String> iter = mergeResult.getPjtNoSet().iterator();
			while (iter.hasNext()) {
				String pjtNo = iter.next();
				if (pjtMap.containsKey(pjtNo)) {
					IPjtEntity pjtEntity = pjtMap.get(pjtNo);

					PjtViewBean view = new PjtViewBean();
					view.setPjtNo(pjtEntity.getPjtId());
					view.setChangeCauseNo(pjtEntity.getChgFactorNo());
					view.setPjtSummary(pjtEntity.getSummary());
					view.setPjtStatus(sheet.getValue(AmDesignBeanId.statusId, pjtEntity.getProcStsId()));

					pjtViewList.add(view);
				}
			}

			mergeViewBean.setPjtViewList(pjtViewList);

			String mergeMessage = this.makeMergeMessage(mergeResult);
			mergeViewBean.setMergeMessage(mergeMessage);

			mergeViewBeanList.add(mergeViewBean);
		}
		return mergeViewBeanList;
	}

	/**
	 * マージのステータスとバイナリDiffのステータスをもとに、マージ可否メッセージを生成する<br>
	 *
	 * @param mergeResult
	 * @return マージ可否メッセージ
	 */
	private String makeMergeMessage(MergeResult mergeResult) {

		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		if (MergeStatus.Conflicted.equals(mergeResult.getMergeStatus())) {// コンフリクト
			return ac.getMessage(TriLogMessage.LAM0055);
		} else if (MergeStatus.Added.equals(mergeResult.getMergeStatus())) {// 新規追加
			return ac.getMessage(TriLogMessage.LAM0058);
		} else if (MergeStatus.Deleted.equals(mergeResult.getMergeStatus())) {// 削除
			return ac.getMessage(TriLogMessage.LAM0059);
		} else if (MergeStatus.Modified.equals(mergeResult.getMergeStatus())) {// マージ可能

			if (EqualsContentsSetSameLineDiffer.Status.EQUAL.equals(mergeResult.getBinaryDiffStatus())) {// 完全一致
				return ac.getMessage(TriLogMessage.LAM0056);
			} else {// マージ可能
				return ac.getMessage(TriLogMessage.LAM0057);
			}
		}
		return ""; // 元はnull nullよりemptyを返したほうが良いと判断
	}

	/**
	 * マージ結果一覧をソートする。 「モジュール名＋ファイルパス名」を、アルファベット順で昇順とする。
	 *
	 * @param mergeResultArray マージ結果一覧の配列
	 */
	private void sortDefaultMergeResultList(MergeResult[] mergeResultArray) {

		List<MergeResult> resultList = FluentList.from(mergeResultArray).asList();

		Comparator<MergeResult> comp = new Comparator<MergeResult>() {
			public int compare(MergeResult o1, MergeResult o2) {
				String fullPath1 = TriStringUtils.linkPathBySlash(o1.getModuleName(), o1.getPath());
				String fullPath2 = TriStringUtils.linkPathBySlash(o2.getModuleName(), o2.getPath());
				return fullPath1.compareTo(fullPath2);
			}
		};

		Collections.sort(resultList, comp);
	}
}
