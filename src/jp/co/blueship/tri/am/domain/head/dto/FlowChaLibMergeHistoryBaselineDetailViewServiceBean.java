package jp.co.blueship.tri.am.domain.head.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理・マージ履歴 ベースライン詳細閲覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMergeHistoryBaselineDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** 選択パッケージクローズバージョンタグ */
	private String selectedCloseVersionTag = null;

	/** ロット詳細情報 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** ビルドパッケージ作成成功回数 */
	private int succeededRelUnitCount = 0;
	/** ビルドパッケージ作成失敗回数 */
	private int failedRelUnitCount = 0;
	/** ベースライン管理情報 */
	private LotBaselineViewGroupByVersionTag baselineViewBean = null;
	/** ベースライン管理情報 */
	private List<LotBaselineViewGroupByVersionTag> buildViewBeanList = null;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedCloseVersionTag() {
		return selectedCloseVersionTag;
	}
	public void setSelectedCloseVersionTag(String selectedCloseVersionTag) {
		this.selectedCloseVersionTag = selectedCloseVersionTag;
	}
	public LotDetailViewBean getLotDetailViewBean() {
		if ( null == lotDetailViewBean ) {
			lotDetailViewBean = new LotDetailViewBean();
		}
		return lotDetailViewBean;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}

	public int getSucceededRelUnitCount() {
		return succeededRelUnitCount;
	}
	public void setSucceededRelUnitCount(int succeededRelUnitCount) {
		this.succeededRelUnitCount = succeededRelUnitCount;
	}

	public int getFailedRelUnitCount() {
		return failedRelUnitCount;
	}
	public void setFailedRelUnitCount(int failedRelUnitCount) {
		this.failedRelUnitCount = failedRelUnitCount;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		if ( null == pjtViewBeanList ) {
			pjtViewBeanList = new ArrayList<PjtViewBean>();
		}
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
	public LotBaselineViewGroupByVersionTag getBaselineViewBean() {
		return baselineViewBean;
	}
	public void setBaselineViewBean(LotBaselineViewGroupByVersionTag baselineViewBean) {
		this.baselineViewBean = baselineViewBean;
	}
	public List<LotBaselineViewGroupByVersionTag> getBuildViewBeanList() {
		return buildViewBeanList;
	}
	public void setBuildViewBeanList(List<LotBaselineViewGroupByVersionTag> buildViewBeanList) {
		this.buildViewBeanList = buildViewBeanList;
	}

	String scmType = null;
	public String getScmType() {
		return scmType;
	}

	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
}

