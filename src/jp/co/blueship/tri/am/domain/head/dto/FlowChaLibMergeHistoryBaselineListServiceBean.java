package jp.co.blueship.tri.am.domain.head.dto;

import java.util.List;

import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理・マージ履歴 ベースライン一覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMergeHistoryBaselineListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** ロット詳細情報 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** ビルドパッケージ作成成功回数 */
	private int succeededRelUnitCount = 0;
	/** ビルドパッケージ作成失敗回数 */
	private int failedRelUnitCount = 0;
	/** ベースライン管理情報 */
	private List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public LotDetailViewBean getLotDetailViewBean() {
		if ( null == lotDetailViewBean ) {
			lotDetailViewBean = new LotDetailViewBean();
		}
		return lotDetailViewBean;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}

	public int getSucceededRelUnitCount() {
		return succeededRelUnitCount;
	}
	public void setSucceededRelUnitCount(int succeededRelUnitCount) {
		this.succeededRelUnitCount = succeededRelUnitCount;
	}

	public int getFailedRelUnitCount() {
		return failedRelUnitCount;
	}
	public void setFailedRelUnitCount(int failedRelUnitCount) {
		this.failedRelUnitCount = failedRelUnitCount;
	}
	public List<LotBaselineViewGroupByVersionTag> getBaselineViewBeanList() {
		return baselineViewBeanList;
	}
	public void setBaselineViewBeanList(List<LotBaselineViewGroupByVersionTag> baselineViewBeanList) {
		this.baselineViewBeanList = baselineViewBeanList;
	}
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	String scmType = null;
	public String getScmType() {
		return scmType;
	}

	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
}

