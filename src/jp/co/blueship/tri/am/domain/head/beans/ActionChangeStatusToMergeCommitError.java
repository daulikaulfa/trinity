package jp.co.blueship.tri.am.domain.head.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * マージ情報のステータスをエラーに更新します。<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public class ActionChangeStatusToMergeCommitError extends ActionPojoAbstract<FlowChaLibMergeCommitServiceBean> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute( IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto ) {
		FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();

		List<Object> paramList = serviceDto.getParamList();

		// エラーがなければ脱出
		if ( SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) ) {
			return serviceDto;
		}

		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
			throw new TriSystemException( AmMessageId.AM005018S ) ;
		}

		this.updateHeadBl( paramBean );

		//HEAD・ベースラインの実行中ステータス(Logical Key = ロットID)
		{
			this.support.getSmFinderSupport().updateExecDataSts(
					paramBean.getProcId(), AmTables.AM_HEAD_BL, AmHeadBlStatusIdForExecData.MergeError, lotEntity.getLotId() );
		}

		//ロット・ベースラインの実行中ステータス(Logical Key = データID)
		{
			Set<String> dataIdSet = new HashSet<String>();
			for ( IMergeParamInfo param: paramInfoArray ) {
				dataIdSet.add(param.getLotBlDto().getLotBlEntity().getDataId());
			}

			String[] dataIds = dataIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.MergeError, dataIds);
		}

		//変更情報の実行中ステータス
		{
			Set<String> pjtIdSet = new HashSet<String>();

			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();
				pjtIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toPjtIdsFromLotBlReqLnk ).asList() );
			}

			String[] pjtIds = pjtIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_PJT, AmPjtStatusIdForExecData.MergeError, pjtIds);
		}

		//資産申請情報の実行中ステータス
		{
			Set<String> areqIdSet = new HashSet<String>();

			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();
				areqIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk ).asList() );
			}

			String[] areqIds = areqIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.MergeError, areqIds);
		}

		return serviceDto;
	}

	/**
	 * HEAD・ベースライン更新
	 *
	 * @param paramBean
	 */
	private void updateHeadBl( FlowChaLibMergeCommitServiceBean paramBean ) {
		if ( null == paramBean.getRegisterHeadBlDto() )
			return;

		IHeadBlEntity headBlEntity = paramBean.getRegisterHeadBlDto().getHeadBlEntity();

		IHeadBlEntity updHeadBlEntity = new HeadBlEntity();

		updHeadBlEntity.setHeadBlId( headBlEntity.getHeadBlId() );
		updHeadBlEntity.setMergeEndTimestamp( TriDateUtils.getSystemTimestamp() );
		updHeadBlEntity.setMergeStsCd(AmHeadBlMergeStatusCode.LOT_MERGE_ERROR.getStatusCode() );
		this.support.getHeadBlDao().update( updHeadBlEntity );

	}

}
