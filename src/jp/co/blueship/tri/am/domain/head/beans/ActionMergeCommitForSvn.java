package jp.co.blueship.tri.am.domain.head.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.fw.cmn.io.Copy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverride;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.vcs.ScmUtils;
import jp.co.blueship.tri.fw.vcs.VcsCommitInfo;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;
import jp.co.blueship.tri.fw.vcs.vo.UpdateVO;

/**
 * マージコミット処理を行います。
 * ＣＶＳでは変更対象資産をHEAD公開資産領域にコピーしてコミットを行っていたが、処理を変更。
 * ＳＶＮでは一時領域にHEAD資産を別途チェックアウトし、そこにマージ済みの資産をコピーしてコミットする。
 * そうすることで、マージコミット時にエラーが発生した場合にHEAD公開資産領域が中途半端な状態で残る状況
 * が解消される。
 *
 *
 * @version V3L10.01
 * @author trinity V3
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionMergeCommitForSvn extends MergeCommitServiceTemplate {
	/**
	 *
	 * 内部クラス
	 * Add/Delete宣言が必要なファイル群とコミットが必要かどうかの情報を保持する。
	 *
	 */
	class ScmFileInfo {

		Map<String,Boolean> needCommitMap = null ;	//key:モジュール名 false: コミット不要、true: コミット必要
		List<String> fileList = null ;
		/**
		 * コミット処理時に処理されずにスキップされた資産数
		 */
		private Map<String, Integer> countBySkipResourceMap = null;

		/**
		 * コンストラクタ
		 *
		 */
		public ScmFileInfo() {
			fileList = new ArrayList<String>() ;
			needCommitMap = new LinkedHashMap<String,Boolean>() ;
			countBySkipResourceMap = new HashMap<String, Integer>();
		}

		/**
		 * コミット処理時に処理されずにスキップされた資産数を取得します。
		 * @param moduleName module name
		 * @return コミット処理時に処理されずにスキップされた資産数
		 */
		public int getCountBySkipResource( String moduleName ) {
			if ( countBySkipResourceMap.containsKey(moduleName) ) {
				return countBySkipResourceMap.get( moduleName );
			}

		    return 0;
		}
		/**
		 * コミット処理時に処理されずにスキップされた資産数を設定します。
		 * @param moduleName module name
		 * @param countBySkipResource コミット処理時に処理されずにスキップされた資産数
		 */
		public void addCountBySkipResource( String moduleName, int countBySkipResource ) {
			int count = 0;

			if ( countBySkipResourceMap.containsKey(moduleName) ) {
				count = countBySkipResourceMap.get( moduleName );
			}

			count += countBySkipResource;
			countBySkipResourceMap.put(moduleName, count);
		}
	}

	@Override
	protected boolean isTargetVcsCategory( String vcsCtgCd ) {
		if ( VcsCategory.SVN.equals(VcsCategory.value(vcsCtgCd) )) {
			return true;
		}

		return false;
	}

	/**
	 * マージコミットを行う
	 */
	@Override
	public void innerExecute( IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto, IMergeParamInfo[] paramInfoArray ) throws Exception {

		LogBusinessFlow logBusinessFlow = null ;

		List<Object> paramList = serviceDto.getParamList();
		FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();

		logBusinessFlow = paramBean.getLogService();

		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();
		
		IHeadBlDto headBlDto = paramBean.getRegisterHeadBlDto();
	
		ILotBlEntity lotBlEntity = this.support.getLotBlEntityByMergeEnableAtFirst( lotEntity.getLotId(), headBlDto );
		List<ILotBlDto> lotBlDtoList = this.support.getLotBlDtoByMergeEnable(
				lotEntity.getLotId(),
				lotBlEntity.getLotChkinTimestamp(),
				TriSortOrder.Desc );

		Set<String> mergeModuleSet = new HashSet<String>();
		for ( ILotBlDto lotBlDto: lotBlDtoList ) {
			for ( ILotBlMdlLnkEntity mdlEntity: lotBlDto.getCheckInMdlEntities( true ) ) {
				if ( ! mergeModuleSet.contains( mdlEntity.getMdlNm() ) ) {
					mergeModuleSet.add(mdlEntity.getMdlNm());
				}
			}
		}

		//コミットコメント
		MergeEditInputBean mergeEditInputBean = AmExtractMessageAddonUtils.getMergeEditInputBean( paramList ) ;
		String commitComment = mergeEditInputBean.getMergeComment() ;

		//リポジトリ情報の取得
		String serverNo = this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId();

		List<IVcsReposDto> reposDtoList =
				this.support.findVcsReposDtoFromLotMdlLnkEntities( lotDto.getIncludeMdlEntities( true ) );
		if ( 0 == reposDtoList.size() ) {
			throw new TriSystemException( AmMessageId.AM004062F );
		}

		String userName = reposDtoList.get(0).getVcsReposEntity().getVcsUserNm();
		IPassMgtEntity passwordEntity = this.support.getSmFinderSupport().findPasswordEntity( PasswordCategory.SVN , serverNo , userName );

		vcsService = factory.createService(VcsCategory.value( lotEntity.getVcsCtgCd() ), userName, passwordEntity.getPassword());

		//関連する資産パス
		String workspace = lotEntity.getWsPath() ;
		String mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;
		String commitTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.commitTemp ) ) ;

		this.chkExistsMergeTempPath(mergeTemp , lotDto , mergeModuleSet, logBusinessFlow ) ;

		//バージョンタグは一連の処理で共通のものを用いる
		String versionTag = paramBean.getNumberingVal();

		ScmFileInfo scmFileInfoAdd = new ScmFileInfo() ;//新規追加資産のリスト
		ScmFileInfo scmFileInfoRemove = new ScmFileInfo() ;//削除資産のリスト

		//マージコミット用の作業フォルダパス以下を全部初期化（消す）
		AmBusinessFileUtils.cleaningDirectory( commitTemp ) ;

		//HEAD資産を一時領域にチェックアウトする（モジュールごとに処理）
		this.checkoutTrunkAsset(reposDtoList, mergeModuleSet, commitTemp, logBusinessFlow );

		//マージ対象資産を取得して、HEADワークスペースにコピー ＆ SCMに渡す資産リストを生成する
		this.mergeAssetCopyProc(
				paramList,commitTemp,
				mergeTemp,
				lotEntity,
				lotBlDtoList,
				scmFileInfoAdd,
				scmFileInfoRemove,
				logBusinessFlow);
		Map<String, IVcsReposEntity> reposMap = this.getReposMap( reposDtoList );

		//コミット
		for ( IVcsReposDto reposDto : reposDtoList ) {
			if ( ! mergeModuleSet.contains( reposDto.getVcsMdlEntity().getMdlNm() ) ) {
				continue;
			}

			this.commitProc(
					paramBean,
					commitTemp,
					lotEntity,
					reposDto,
					scmFileInfoAdd,
					scmFileInfoRemove,
					commitComment,
					logBusinessFlow );
		}

		IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;

		for ( IVcsReposEntity reposEntity: reposMap.values() ) {
			this.attachVersionTag(reposEntity , lotEntity, versionTag , commitComment , logBusinessFlow );
		}

		//HEAD公開資産パスのupdate
		this.updateHeadWorkPath(headEntity , lotDto.getIncludeMdlEntities( true ) , mergeModuleSet, logBusinessFlow ) ;

		//Map<String, String> revisionMap = this.getRevisionMapByMdlNmKey( reposDtoList );
		Map<String, String> revisionMap = this.getModuleNameMap( lotBlDtoList );

		this.updateLotInfo(
				paramBean.getRegisterHeadBlDto(),
				lotDto,
				revisionMap,
				logBusinessFlow );

		logBusinessFlow.writeLogWithDateBegin( " Update Merge-Commit Status "  ) ;
		try {
			boolean isWarning = ( 0 < paramBean.getCountBySkipResource() )? true: false;
			paramBean.setRegisterHeadBlDto( this.updateHeadBaselineInfo( paramBean.getRegisterHeadBlDto(), revisionMap, isWarning ));
			ILotBlEntity lotBlEntityAtFirst = this.support.getLotBlEntityAtFirst( lotEntity.getLotId() );
			
			this.updateLotBaselineInfo( paramInfoArray );
			if (TriStringUtils.equals(lotBlEntityAtFirst.getLotBlId(), lotBlEntity.getLotBlId())){
				this.updatePjtInfo( paramInfoArray );	
			}
			this.updateAreqInfo( paramInfoArray );
		} finally {
			logBusinessFlow.writeLogWithDateEnd( " Update Merge-Commit Status "  ) ;
		}

	}

	/**
	 * 変更のあったモジュール名を取得する
	 *
	 * @param lotBlDtoList ロットベースラインDTOのList
	 * @return モジュール名のマップ
	 */
	protected Map<String, String> getModuleNameMap(List<ILotBlDto> lotBlDtoList) {

		Map<String, String> moduleNameMap = new LinkedHashMap<String, String>();

		for (ILotBlDto dto : lotBlDtoList) {
			for ( ILotBlMdlLnkEntity entity : dto.getCheckInMdlEntities(true) ) {
				if ( moduleNameMap.containsKey( entity.getMdlNm() ) ) {
					continue;
				}
				moduleNameMap.put( entity.getMdlNm(), entity.getLotChkinRev() );
			}
		}

		return moduleNameMap;
	}

//	private ListSubDirVO setLisVO(String repository){
//		ListSubDirVO vo = new ListSubDirVO();
//		vo.setRepository(repository);
//		return vo;
//	}

	private Map<String, IVcsReposEntity> getReposMap( List<IVcsReposDto> reposDtoList ) {

		Map<String, IVcsReposEntity> reposMap = new HashMap<String, IVcsReposEntity>();

		for ( IVcsReposDto reposDto: reposDtoList ) {
			IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();

			if ( ! reposMap.containsKey( reposEntity.getVcsReposId() ) ) {
				reposMap.put( reposEntity.getVcsReposId(), reposEntity );
			}
		}

		return reposMap;
	}

	/**
	 * trunk資産をチェックアウトする。
	 * @param reposDtoList リポジトリ情報
	 * @param mergeModuleSet マージ対象モジュールSet
	 * @param lotDto ロット情報
	 * @param commitTemp マージコミット一時領域のパス
	 * @param logBusinessFlow ログハンドラ
	 */
	private void checkoutTrunkAsset(
			List<IVcsReposDto> reposDtoList,
			Set<String> mergeModuleSet,
			String commitTemp,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		for ( IVcsReposDto reposDto : reposDtoList ) {
			if ( !mergeModuleSet.contains( reposDto.getVcsMdlEntity().getMdlNm() ) ) {
				continue;
			}

			IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
			String url = reposEntity.getVcsReposUrl();

			String moduleName = reposDto.getVcsMdlEntity().getMdlNm();

			logBusinessFlow.writeLog( TriLogMessage.LAM0035 , SEP , url + SEP , moduleName + SEP ) ;

			/** trunk資産のチェックアウト **/
			vcsService.checkoutHead(setChkVO(url , commitTemp , moduleName));
		}
	}
	/**
	 * マージ対象資産(mergeTemp)を取得して、HEAD資産のワーキングコピー(commitTemp)にコピーする<br>
	 * また、ＳＣＭに渡すための、新規追加資産の情報はaddFileList、削除資産の情報はremoveFileListに<br>
	 * それぞれ格納して返す。<br>
	 * @param paramList
	 * @param headWorkPath HEADのワークパス
	 * @param mergeTemp mergeTempのパス
	 * @param pjtLotEntity ロット情報
	 * @param lotBlDtoList ロット・ベースライン情報のリスト
	 * @param addFileList SCMに渡す新規追加資産のリスト
	 * @param removeFileList SCMに渡す削除資産のリスト
	 * @throws Exception
	 */
	private void mergeAssetCopyProc(
			List<Object> paramList,
			String commitTemp,
			String mergeTemp,
			ILotEntity pjtLotEntity,
			List<ILotBlDto> lotBlDtoList,
			ScmFileInfo scmFileInfoAdd,
			ScmFileInfo scmFileInfoRemove,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		//コンフリクトチェック結果ログの取得・解析
		String mergeReportFilePath = TriStringUtils.linkPathBySlash( mergeTemp , sheet.getValue( AmDesignEntryKeyByMerge.mergeResultFileName ) ) ;
		Object obj = TriFileUtils.readObjectFromFile( new File( mergeReportFilePath ) ) ;
		@SuppressWarnings("unchecked")
		Map<String,MergeResult> mergeResultMap = (Map<String,MergeResult>)obj ;

		Map<String, String> assetMap = new HashMap<String, String>();

		/*
		 * ロット・ベースラインの降順で処理する。
		 * 資産が重複する申請の場合、その資産を省く。
		 * これにより、ベースライン対象に複数のベースラインの最新資産のみがマージの対象となる。
		 */
		for( ILotBlDto lotBlDto : lotBlDtoList ) {
			List<ILotBlReqLnkEntity> reqLnkEntities = lotBlDto.getLotBlReqLnkEntities();

			//ベースラインに含まれる申請は、返却申請と削除申請とを、承認日付順にソート、順に処理していく
			String[] areqIds = FluentList.from(reqLnkEntities).map(AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk).toArray(new String[0]);
			List<IAreqDto> areqDtoEntities = this.support.getAreqEntitiesSortByPjtAvlTimestamp( areqIds );

			//申請の集約（追加・削除を繰り返している資産のエントリを、最後の状態だけ残してあとは消す
			this.intensiveAssetApply( assetMap, areqDtoEntities );

			//申請番号単位に処理する
			for( IAreqDto areqDto : areqDtoEntities ) {//申請番号（返却申請or削除申請）を順次処理
				IAreqEntity areqEntity = areqDto.getAreqEntity();

				//返却資産申請
				if( AmBusinessJudgUtils.isReturnApply( areqEntity.getAreqCtgCd() ) ) {

					String returnAssetApplyNoPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, areqEntity ).getAbsolutePath() ;
					String returnAssetPath = TriStringUtils.convertPath( returnAssetApplyNoPath ) ;
					//対象のファイルリストを見つつ、MergeTempにあるマージ済みのファイルをHEADのworkpathにコピーする
					List<IAreqFileEntity> assetFileEntityArray = areqDto.getAreqFileEntities(AreqCtgCd.ReturningRequest);
					for( IAreqFileEntity assetFileEntity : assetFileEntityArray ) {
						String filePath = TriStringUtils.convertPath( assetFileEntity.getFilePath() );

						//マージでコンフリクトが発生していたファイルは、返却資産フォルダから直接コピーしてしまう⇒「ブランチがマージ済みとみなす」
						String srcBasePath = null;
						MergeResult mergeResult = mergeResultMap.get( filePath ) ;
						if( null != mergeResult && MergeStatus.Conflicted.equals( mergeResult.getMergeStatus() ) ) {//該当ファイルがコンフリクト
							srcBasePath = returnAssetPath;//ファイルのコピー元： 返却フォルダ
						} else {
							srcBasePath = mergeTemp;//ファイルのコピー元： mergeTemp
						}

						File mergeSrcPathFile = new File( TriStringUtils.linkPathBySlash( srcBasePath , filePath ) ) ;
						File headDestPathFile = new File( TriStringUtils.linkPathBySlash( commitTemp , filePath ) );

						//mergeTemp ⇒ commitTempコピー
						Copy copy = new Copy();
						copy.setFileCopy( new FileCopyOverride() );
						copy.copy( mergeSrcPathFile , headDestPathFile , false );

						//ステータス取得
						String moduleName = TriStringUtils.substringBefore( filePath );
						SVNStatus svnStatus = null ;
						try {
							svnStatus = vcsService.statusSingle(setStatusVO(commitTemp,filePath));
						} catch( ScmException e ) {//新規フォルダ下の新規ファイルはSubversion管理下にないため例外が発生する：想定内
							scmFileInfoAdd.fileList.add( filePath );
							scmFileInfoAdd.needCommitMap.put( moduleName , true ) ;
							continue ;
						}

						if ( null == svnStatus ) {

							//ステータスが取得できない場合は新規資産とみなす
							scmFileInfoAdd.fileList.add( filePath );
							scmFileInfoAdd.needCommitMap.put( moduleName , true ) ;
						} else {
							SVNStatusType contentsStatus = svnStatus.getContentsStatus() ;
							if( SVNStatusType.STATUS_UNVERSIONED.equals( contentsStatus ) ) {//Add宣言未
								scmFileInfoAdd.fileList.add( filePath );
								scmFileInfoAdd.needCommitMap.put( moduleName , true ) ;

							} else if( SVNStatusType.STATUS_ADDED.equals( contentsStatus ) ) {//新規・Add宣言済み
								scmFileInfoAdd.needCommitMap.put( moduleName , true ) ;

							} else if( SVNStatusType.STATUS_NORMAL.equals( contentsStatus ) ) {//コミット済み
								logBusinessFlow.writeLog( AmMessageId.AM002005W , commitTemp, filePath, ((null == contentsStatus)? "null": contentsStatus.toString()));
								scmFileInfoAdd.addCountBySkipResource( moduleName, 1 );

							} else if( SVNStatusType.STATUS_MODIFIED.equals( contentsStatus ) ) {//変更資産・変更あり
								scmFileInfoAdd.needCommitMap.put( moduleName , true ) ;

							} else {
								logBusinessFlow.writeLog( AmMessageId.AM002006W , commitTemp, filePath, ((null == contentsStatus)? "null": contentsStatus.toString()));
								scmFileInfoAdd.addCountBySkipResource( moduleName, 1 );

							}
						}
					}
				}

				//削除資産申請
				if( AmBusinessJudgUtils.isDeleteApply( areqEntity.getAreqCtgCd() ) ) {
					List<IAreqFileEntity> areqFileEntities = areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest );

					if( !TriStringUtils.isEmpty( areqFileEntities ) ) {
						for( IAreqFileEntity areqFileEntity : areqFileEntities ) {
							String delPath = areqFileEntity.getFilePath();
							//ステータス取得
							String moduleName = TriStringUtils.substringBefore( delPath ) ;
							SVNStatus svnStatus = null ;
							try {
								svnStatus = vcsService.statusSingle(setStatusVO(commitTemp,delPath));
							} catch( ScmException e ) {//新規フォルダ下の削除ファイルはSubversion管理下にないため例外が発生する：想定内
								continue ;
							}
							if( null == svnStatus ) {//コミット済み
								logBusinessFlow.writeLog( AmMessageId.AM002007W , commitTemp, delPath);
								scmFileInfoRemove.addCountBySkipResource( moduleName, 1 );
							} else {
								SVNStatusType contentsStatus = svnStatus.getContentsStatus() ;

								if( SVNStatusType.STATUS_NORMAL.equals( contentsStatus ) ) {//削除前
									scmFileInfoRemove.fileList.add( areqFileEntity.getFilePath() ) ;
									scmFileInfoRemove.needCommitMap.put( moduleName , true ) ;

								} else if( SVNStatusType.STATUS_DELETED.equals( contentsStatus ) ) {//Delete宣言済み
									scmFileInfoRemove.needCommitMap.put( moduleName , true ) ;

								} else if( SVNStatusType.STATUS_MISSING.equals( contentsStatus ) ) {//削除済み
									logBusinessFlow.writeLog( AmMessageId.AM002008W ,  commitTemp, delPath, ((null == contentsStatus)? "null": contentsStatus.toString()));
									scmFileInfoRemove.addCountBySkipResource( moduleName, 1 );

								} else {
									logBusinessFlow.writeLog( AmMessageId.AM002009W ,  commitTemp, delPath, ((null == contentsStatus)? "null": contentsStatus.toString()));
									scmFileInfoRemove.addCountBySkipResource( moduleName, 1 );

								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * 申請情報から、重複する資産をもつ古い申請情報から対象資産を外して、
	 * コミット対象の資産情報のみを集約し直す
	 * <br>
	 * <br>※論理集約しているため、このEntityを更新対象にしてはいけない。
	 * 利用方法が限られるため、Entityを複写してsrcのEntityを保証するようなクリティカルな処置は行わない。
	 * （以後の改修によるデグレを防止するため）
	 *
	 * @param areqDtoEntities 旧⇒新の順にソートされた申請情報
	 */
	private void intensiveAssetApply( Map<String, String> assetMap, List<IAreqDto> areqDtoEntities ) {
		IAreqDto[] areqDtoArrays = areqDtoEntities.toArray( new IAreqDto[0] );

		//新⇒旧の順に資産を評価する
		for ( int i = areqDtoArrays.length - 1; i >= 0; i-- ) {
			IAreqDto areqDto = areqDtoArrays[ i ];

			List<IAreqFileEntity> inAssetFileArray =
					areqDto.getAreqFileEntities( AreqCtgCd.ReturningRequest, AreqCtgCd.RemovalRequest );
			List<IAreqFileEntity> outAssetFileList = new ArrayList<IAreqFileEntity>();

			if ( null != inAssetFileArray ) {
				for ( IAreqFileEntity assetFile : inAssetFileArray ) {
					if ( assetMap.containsKey( assetFile.getFilePath() ) )
						continue;

					assetMap.put( assetFile.getFilePath(), assetFile.getFilePath() );
					outAssetFileList.add( assetFile );
				}
			}

			areqDto.setAreqFileEntities( outAssetFileList );
		}
	}

	/**
	 * mergeTempに、コンフリクトチェックによって生成されたマージ済みの資産があることをチェックする
	 * @param mergeTemp mergeTempのパス
	 * @param lotDto ロット情報
	 * @param logBusinessflow ログハンドラ
	 */
	private void chkExistsMergeTempPath(String mergeTemp , ILotDto lotDto , Set<String> mergeModuleSet, LogBusinessFlow logBusinessFlow ) throws ScmException , ScmSysException {

		ILotEntity lotEntity = lotDto.getLotEntity();
		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true );

		if( TriStringUtils.isEmpty( mdlEntities ) ) {
			throw new TriSystemException( AmMessageId.AM004003F , lotEntity.getLotId() , lotEntity.getLotLatestVerTag() );
		}

		File fileMaster = new File( mergeTemp ) ;
		if( true != fileMaster.exists() ) {
			throw new TriSystemException( AmMessageId.AM004027F , mergeTemp );
		}
		for( ILotMdlLnkEntity moduleEntity : mdlEntities ) {
			if ( ! mergeModuleSet.contains( moduleEntity.getMdlNm() ) ) {
				continue;
			}

			String pathModule = TriStringUtils.linkPathBySlash( mergeTemp , moduleEntity.getMdlNm() ) ;
			File fileModule = new File( pathModule ) ;
			if( true != fileModule.exists() ) {
				throw new TriSystemException( AmMessageId.AM004028F , pathModule );
			}
			//各モジュールがワーキングコピーとして正常であることを確認
			logBusinessFlow.writeLog( TriLogMessage.LAM0038 , moduleEntity.getMdlNm() ) ;
			SVNStatus svnStatus = vcsService.statusSingle(setStatusVO(mergeTemp, moduleEntity.getMdlNm()));
			logBusinessFlow.writeLog( vcsService.makeSvnStatusStr( svnStatus ) ) ;
		}
	}
	/**
	 * バージョンタグを付与する
	 * @param reposMap リポジトリ情報
	 * @param lotEntity ロット情報
	 * @param versionTag バージョンタグ
	 * @param commitComment コミットコメント
	 * @param logBusinessFlow ログハンドラ
	 * @return コミットコメント
	 * @throws Exception
	 */
	private VcsCommitInfo attachVersionTag(
			IVcsReposEntity ucfRepositoryEntity,
			ILotEntity lotEntity,
			String versionTag ,
			String commitComment ,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		try {

			String repository = ucfRepositoryEntity.getVcsReposUrl() ;
			//既にタグが存在しないかチェック、存在する場合はスルー（前回クローズ時のタグ付与後にエラーが発生した場合など	）
			if( true != vcsService.chkExistsTag(setListVO(repository,versionTag)) ) {
				logBusinessFlow.writeLog( TriLogMessage.LAM0039 , repository , versionTag ) ;
				VcsCommitInfo vcsCommitInfo = vcsService.tagToTrunk(setCommitTrunkVO(repository,versionTag,commitComment));
				return vcsCommitInfo ;
			} else {
				logBusinessFlow.writeLog( TriLogMessage.LAM0041 ) ;
				return null ;
			}

		} catch ( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			throw be;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			throw e ;
		}
	}
	/**
	 * /**
	 * バージョンタグを付与する
	 * @param headEntity HEAD情報
	 * @param mdlEntities モジュール情報
	 * @param mergeModuleSet マージ対象モジュール
	 * @param logBusinessFlow ログハンドラ
	 * @throws Exception
	 */
	private void updateHeadWorkPath(
			IHeadEntity headEntity,
			List<ILotMdlLnkEntity> mdlEntities,
			Set<String> mergeModuleSet,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		String workPath = headEntity.getMwPath() ;

		try {

			for( ILotMdlLnkEntity moduleEntity : mdlEntities ) {
				if ( ! mergeModuleSet.contains( moduleEntity.getMdlNm() ) ) {
					continue;
				}

				String moduleName = moduleEntity.getMdlNm();
				logBusinessFlow.writeLog( TriLogMessage.LAM0042 , SEP , workPath + SEP , moduleName ) ;
				vcsService.update(setUpdateVO(workPath,moduleName));
			}
		} catch ( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			throw be;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			throw e ;
		}
	}
	/**
	 * コミットを行う。
	 * @param paramBean Service bean of merge DTO
	 * @param commitTemp マージコミット一時領域のパス
	 * @param lotEntity ロット情報
	 * @param reposDto リポジトリ情報
	 * @param moduleEntity モジュール情報
	 * @param scmFileInfoAdd 追加資産の情報
	 * @param scmFileInfoRemove 削除資産の情報
	 * @param commitComment コミットコメント
	 * @param logBusinessFlow ログハンドラ
	 * @throws ScmException
	 */
	private void commitProc(
			FlowChaLibMergeCommitServiceBean paramBean,
			String commitTemp ,
			ILotEntity lotEntity,
			IVcsReposDto reposDto,
			ScmFileInfo scmFileInfoAdd,
			ScmFileInfo scmFileInfoRemove,
			String commitComment ,
			LogBusinessFlow logBusinessFlow ) throws ScmException {

		IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
		IVcsMdlEntity moduleEntity = reposDto.getVcsMdlEntity();

		//SCMに渡すのは当該モジュールに絞り込む
		List<String> addFileModuleList = ScmUtils.getFileListInModule( scmFileInfoAdd.fileList , moduleEntity );
		List<String> removeFileModuleList = ScmUtils.getFileListInModule( scmFileInfoRemove.fileList , moduleEntity );

		String moduleName = moduleEntity.getMdlNm();

		logBusinessFlow.writeLog( TriLogMessage.LAM0043 , SEP , reposEntity.getVcsReposUrl() + SEP , commitTemp + SEP , moduleName + SEP , lotEntity.getLotBranchTag() , SEP ) ;

		//新規資産のAdd宣言
		if( 0 != addFileModuleList.size() ) {//モジュールに該当する返却・追加資産なし
			for( String addFileInfo : addFileModuleList ) {
				logBusinessFlow.writeLog( TriLogMessage.LAM0046 , addFileInfo ) ;
			}
			vcsService.add(setAddVO(commitTemp, moduleName, addFileModuleList.toArray(new String[0])));
		}
		logBusinessFlow.writeLog( "" ) ;
		//削除資産のDelete宣言
		if( 0 != removeFileModuleList.size() ) {//モジュールに該当する削除資産なし
			for( String removeFileInfo : removeFileModuleList ) {
				logBusinessFlow.writeLog( TriLogMessage.LAM0047 , removeFileInfo ) ;
			}
			vcsService.remove(setDeleteVO(commitTemp, moduleName, removeFileModuleList.toArray( new String[ 0 ] )));
		}

		//コミット（モジュールごとコミット、本当はモジュールをまとめて１度にコミットしたいが）
		Boolean needCommitAdd = scmFileInfoAdd.needCommitMap.get( moduleName ) ;
		Boolean needCommitRemove = scmFileInfoRemove.needCommitMap.get( moduleName ) ;
		if( ( null != needCommitAdd && true == needCommitAdd.booleanValue() ) ||
				null != needCommitRemove && true == needCommitRemove.booleanValue() ) {
			logBusinessFlow.writeLog( TriLogMessage.LAM0048 , commitTemp , moduleName ) ;
			vcsService.commit(setCommitVO(commitTemp,new String[]{moduleName},commitComment));

			int count = paramBean.getCountBySkipResource();
			count += scmFileInfoAdd.getCountBySkipResource( moduleName );
			count += scmFileInfoRemove.getCountBySkipResource( moduleName );
			paramBean.setCountBySkipResource( count );
		}
	}

	/**
	 * 以下ValueObjectにパラメータをセットするprivateメソッド
	 */
	private CheckOutVO setChkVO(String repository,String commitTemp,String moduleName){

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository(repository);
		vo.setPathDest(commitTemp);
		vo.setModuleName(moduleName);
		return vo;
	}

	private StatusVO setStatusVO(String path,String objectpath){
		StatusVO vo = new StatusVO();
		vo.setPathDest(path);
		vo.setObjectPath(objectpath);
		return vo;
	}

	private ListSubDirVO setListVO(String repository,String versionTag){
		ListSubDirVO vo = new ListSubDirVO();
		vo.setRepository(repository);
		vo.setVersionTag(versionTag);
		return vo;
	}

	private CommitVO setCommitTrunkVO(String repository,String versionTag,String commitComment){
		CommitVO vo = new CommitVO();
		vo.setRepository(repository);
		vo.setLabelTag(versionTag);
		vo.setCommitComment(commitComment);
		return vo;
	}

	private CommitVO setCommitVO(String path, String[] module,String comment){

		CommitVO vo = new CommitVO();
		vo.setPathDest(path);
		vo.setModuleNameArray(module);
		vo.setCommitComment(comment);
		return vo;
	}

	private UpdateVO setUpdateVO(String path,String module){

		UpdateVO vo = new UpdateVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		return vo;
	}

	private AddVO setAddVO(String path, String module, String[] pathArray){

		AddVO vo = new AddVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		vo.setFilePathArray(pathArray);
		return vo;
	}

	/**
	 * パラメータをDeleteVOにセットします。
	 * @param path
	 * @param module
	 * @param pathArray
	 * @return vo
	 */
	private DeleteVO setDeleteVO(String path, String module, String[] pathArray){
		DeleteVO vo = new DeleteVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		vo.setFilePathArray(pathArray);
		return vo;
	}

}
