package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * マージコミット 承認確認画面の表示情報設定Class<br>
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMergeCommitServiceConfirm implements IDomain<FlowChaLibMergeCommitServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute(IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto) {

		FlowChaLibMergeCommitServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType = paramBean.getScreenType();

			String closeVersionTag = paramBean.getSelectedLotVersionTag();

			paramBean.setSelectedLotVersionTag(closeVersionTag);
			paramBean.setSelectedLotId(paramBean.getSelectedLotId());

			if (!refererID.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM) && !forwordID.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM)) {

				setBaselineInfo( paramBean );

				return serviceDto;
			}

			if (refererID.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM)) {

				if (ScreenType.next.equals(screenType)) {
					AmItemChkUtils.checkMergeCommitCheck(paramBean);
					setBaselineInfo( paramBean );
				}
			}

			if (forwordID.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM)) {
				ILotDto lotDto = this.support.findLotDto( paramBean.getSelectedLotId() );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

				setBaselineInfo( paramBean );

				// マージエラー警告 有無のチェック
				this.checkMergeCommitError(lotDto.getLotEntity(), paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005092S, e);
		}
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 *
	 * @param pjtLotEntity
	 * @param paramBean
	 */

	private void checkMergeCommitError(ILotEntity pjtLotEntity, FlowChaLibMergeCommitServiceBean paramBean) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		// 全ビルド（全ロットで）の中でマージエラーが１件でもあれば警告を表示する
		ILotBlEntity[] lotBlEntityArray = this.support.getLotBlEntitiesByMergeError();
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		for (ILotBlEntity lotBlEntity : lotBlEntityArray) {
			messageList.add(AmMessageId.AM001113E);
			messageArgsList.add(new String[] { lotBlEntity.getLotId(), lotBlEntity.getLotBlId() });
		}

		if (null == paramBean.getInfoMessageIdList()) {
			paramBean.setInfoMessageIdList(messageList);
		} else {
			paramBean.getInfoMessageIdList().addAll(messageList);
		}
		if (null == paramBean.getInfoMessageArgsList()) {
			paramBean.setInfoMessageArgsList(messageArgsList);
		} else {
			paramBean.getInfoMessageArgsList().addAll(messageArgsList);
		}

		WarningCheckBean warningCheck = new WarningCheckBean();
		warningCheck.setExistWarning((0 != messageList.size()) ? true : false);

		// メッセージ生成
		List<String> commentListUnitCloseError = warningCheck.getCommentList();
		warningCheck.setConfirmCheck(paramBean.getWarningCheckMergeCommitError().isConfirmCheck());
		// [0]
		commentListUnitCloseError.add(ac.getMessage(paramBean.getLanguage(), AmMessageId.AM001112E));
		// [1]
		String logPath = TriStringUtils.convertPath(AmDesignBusinessRuleUtils.getWorkspaceLogPath(pjtLotEntity, paramBean.getFlowAction()));
		commentListUnitCloseError.add(ac.getMessage(paramBean.getLanguage(), AmMessageId.AM001114E, logPath));

		paramBean.setWarningCheckMergeCommitError(warningCheck);
	}
	/**
	 * ベースライン情報を取得し格納する
	 *
	 * @param paramBean
	 */

	private void setBaselineInfo(FlowChaLibMergeCommitServiceBean paramBean) {
		String versionTag = paramBean.getSelectedLotVersionTag();

		List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( paramBean.getSelectedLotId(), versionTag, TriSortOrder.Desc );
		List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
		List<LotBaselineViewGroupByVersionTag> baselineViewList = this.support.getLotBaselineViewGroupByVersionTag( lotBlDtoList );
		paramBean.setHeadBlViewBean( baselineViewList.get( 0 ) );
	}
}
