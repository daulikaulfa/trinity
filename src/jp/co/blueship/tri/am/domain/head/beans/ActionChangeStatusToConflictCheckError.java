package jp.co.blueship.tri.am.domain.head.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * マージ情報のステータスをエラーに更新します。<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author anh nguyen duc
 */
public class ActionChangeStatusToConflictCheckError extends ActionPojoAbstract<FlowChaLibConflictCheckServiceBean> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibConflictCheckServiceBean> execute( IServiceDto<FlowChaLibConflictCheckServiceBean> serviceDto ) {
		FlowChaLibConflictCheckServiceBean paramBean = serviceDto.getServiceBean();

		List<Object> paramList = serviceDto.getParamList();

		// エラーがなければ脱出
		if ( SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) ) {
			return serviceDto;
		}

		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
			throw new TriSystemException( AmMessageId.AM005018S ) ;
		}

		this.updateHeadBl( paramBean );

		//HEAD・ベースラインの実行中ステータス(Logical Key = ロットID)
		{
			this.support.getSmFinderSupport().updateExecDataSts(
					paramBean.getProcId(), AmTables.AM_HEAD_BL, AmHeadBlStatusIdForExecData.ConflictCheckError, lotEntity.getLotId() );
		}

		//ロット・ベースラインの実行中ステータス(Logical Key = データID)
		{
			Set<String> dataIdSet = new HashSet<String>();
			for ( IMergeParamInfo param: paramInfoArray ) {
				dataIdSet.add(param.getLotBlDto().getLotBlEntity().getDataId());
			}

			String[] dataIds = dataIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.ConflictCheckError, dataIds);
		}

		return serviceDto;
	}

	/**
	 * HEAD・ベースライン更新
	 *
	 * @param paramBean
	 */
	private void updateHeadBl( FlowChaLibConflictCheckServiceBean paramBean ) {
		if ( null == paramBean.getRegisterHeadBlDto() )
			return;

		IHeadBlEntity headBlEntity = paramBean.getRegisterHeadBlDto().getHeadBlEntity();

		IHeadBlEntity updHeadBlEntity = new HeadBlEntity();

		updHeadBlEntity.setHeadBlId( headBlEntity.getHeadBlId() );
		updHeadBlEntity.setMergechkEndTimestamp( TriDateUtils.getSystemTimestamp() );
		this.support.getHeadBlDao().update( updHeadBlEntity );

	}

}
