package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryBaselineListServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 変更管理・マージ履歴 ベースライン一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibMergeHistoryBaselineListService implements IDomain<FlowChaLibMergeHistoryBaselineListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeHistoryBaselineListServiceBean> execute(IServiceDto<FlowChaLibMergeHistoryBaselineListServiceBean> serviceDto) {

		FlowChaLibMergeHistoryBaselineListServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo(selectedLotNo);

			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			// Scmモード
			VcsCategory scmType = VcsCategory.value(lotDto.getLotEntity().getVcsCtgCd());
			paramBean.setScmType(scmType.value());

			paramBean.setLotDetailViewBean(this.support.getLotDetailViewBean(paramBean, lotDto));

			setServiceBeanRelUnitResult(selectedLotNo, paramBean);

			int selectPageNo = (0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo();

			setServiceBeanBuildEntity(selectedLotNo, selectPageNo, paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005094S, e);
		}
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return this.support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 *
	 * @param selectedLotNo ロット番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(String selectedLotNo, FlowChaLibMergeHistoryBaselineListServiceBean paramBean) {

		IJdbcCondition successCountCondition = AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition(selectedLotNo);

		paramBean.setSucceededRelUnitCount(this.support.getBmFinderSupport().getBpDao().count(successCountCondition.getCondition()));

		IJdbcCondition failureCountCondition = AmDBSearchConditionAddonUtils.getFailedRelUnitCondition(selectedLotNo);

		paramBean.setFailedRelUnitCount(this.support.getBmFinderSupport().getBpDao().count(failureCountCondition.getCondition()));
	}

	/**
	 * Beanにベースライン情報の一覧検索結果を設定する。
	 *
	 * @param lotId ロット番号
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */

	private void setServiceBeanBuildEntity(String lotId, int selectPageNo, FlowChaLibMergeHistoryBaselineListServiceBean paramBean) {

		// Lotに紐付くマージ済みのあるヘッドベースラインを取得
		List<IHeadBlEntity> headBlEntityList = null;
		{
			HeadBlCondition condition = new HeadBlCondition() ;

			condition.setLotId( lotId ) ;
			condition.setStsIds( new String[]{
					AmHeadBlStatusId.Merged.getStatusId()} );

			ISqlSort sort = new SortBuilder();
			sort.setElement(HeadBlItems.mergeEndTimestamp, TriSortOrder.Desc, 0);

			headBlEntityList = this.support.getHeadBlDao().find( condition.getCondition(), sort );
		}
		// Lotに紐付くマージ済みのRU全件を取得
		List<ILotBlDto> lotBlDtoList = new ArrayList<ILotBlDto>();
		if( 0<headBlEntityList.size() ) {
			for ( IHeadBlEntity headBlEntity : headBlEntityList ) {
				lotBlDtoList.addAll( this.support.getLotBlDtoByMergedBaseline(lotId, headBlEntity, false) );
			}
		}

		// マージ済み/マージエラーのＲＵが１件もなければ元画面に戻す
		if (TriStringUtils.isEmpty(lotBlDtoList)) {
			throw new ContinuableBusinessException(AmMessageId.AM001111E, lotId);
		}

		List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = this.support.getLotBaselineViewGroupByVersionTag(lotBlDtoList);
		// 同じタイミングでマージ

		paramBean.setBaselineViewBeanList(baselineViewBeanList);

		PageNoInfo page = new PageNoInfo();
		page.setMaxPageNo(1);
		page.setSelectPageNo(1);
		paramBean.setPageInfoView(page);
		paramBean.setSelectPageNo(selectPageNo);
	}
}
