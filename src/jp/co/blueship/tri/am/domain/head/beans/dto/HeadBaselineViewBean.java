package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceBean;

/**
 *
 * HEAD・ベースライン情報
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class HeadBaselineViewBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	/** HEAD・ベースラインタグ */
	private String baselineTag = null ;
	/** HEAD・VCSタグ */
	private String versionTag = null ;
	/** ロット・ベースライン情報 */
	private List<LotBaselineViewGroupByVersionTag> lotBaselineList = new ArrayList<LotBaselineViewGroupByVersionTag>() ;
	/** ステータス */
	private String statusId = null ;
	/** コンフリクトチェック開始日時 */
	private String conflictCheckStartDate = null ;
	/** コンフリクトチェック終了日時 */
	private String conflictCheckEndDate = null ;
	/** マージ開始日時 */
	private String mergeStartDate = null ;
	/** マージ終了日時 */
	private String mergeEndDate = null ;

	public String getBaselineTag() {
		return baselineTag;
	}
	public void setBaselineTag(String baselineTag) {
		this.baselineTag = baselineTag;
	}
	public List<LotBaselineViewGroupByVersionTag> getLotBaselineList() {
		return lotBaselineList;
	}
	public void setLotBaselineList(List<LotBaselineViewGroupByVersionTag> lotBaselineList) {
		this.lotBaselineList = lotBaselineList;
	}
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	public String getConflictCheckStartDate() {
		return conflictCheckStartDate;
	}
	public void setConflictCheckStartDate(String conflictCheckStartDate) {
		this.conflictCheckStartDate = conflictCheckStartDate;
	}
	public String getConflictCheckEndDate() {
		return conflictCheckEndDate;
	}
	public void setConflictCheckEndDate(String conflictCheckEndDate) {
		this.conflictCheckEndDate = conflictCheckEndDate;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}

	public String getMergeEndDate() {
		return mergeEndDate;
	}
	public void setMergeEndDate(String mergeEndDate) {
		this.mergeEndDate = mergeEndDate;
	}
	public String getMergeStartDate() {
		return mergeStartDate;
	}
	public void setMergeStartDate(String mergeStartDate) {
		this.mergeStartDate = mergeStartDate;
	}

}