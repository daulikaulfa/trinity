package jp.co.blueship.tri.am.domain.head.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.IServiceBean;

/**
 * ロット・ベースライン情報
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class XXLotBaselineBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット・ベースラインタグ */
	private String baselineTag = null ;
	/** ロット・VCSタグ */
	private String versionTag = null ;
	/** データID */
	private String dataId = null;
	/** ロット・ベースライン名 */
	private String baselineName = null;

	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getBaselineName() {
		return baselineName;
	}
	public void setBaselineName(String baselineName) {
		this.baselineName = baselineName;
	}
	public String getBaselineTag() {
		return baselineTag;
	}
	public void setBaselineTag(String baselineTag) {
		this.baselineTag = baselineTag;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}

}
