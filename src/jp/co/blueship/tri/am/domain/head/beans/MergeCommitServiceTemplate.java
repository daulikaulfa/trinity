package jp.co.blueship.tri.am.domain.head.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlEntity;
import jp.co.blueship.tri.am.dao.head.eb.HeadEntity;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;

/**
 * マージ処理で利用するサービステンプレートです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public abstract class MergeCommitServiceTemplate extends ActionPojoAbstract<FlowChaLibMergeCommitServiceBean> {

	protected static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	protected static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	protected FlowChaLibMergeEditSupport support = null;
	protected VcsServiceFactory factory;
	protected IVcsService vcsService = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowChaLibMergeEditSupport support ) {
		this.support = support;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute(
			IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto) {

		LogBusinessFlow logBusinessFlow = null ;

		try {

			List<Object> paramList = serviceDto.getParamList();
			FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();

			logBusinessFlow = paramBean.getLogService();

			paramBean.getCacheLotDto();
			ILotDto lotDto = paramBean.getCacheLotDto();
			ILotEntity lotEntity = lotDto.getLotEntity();

			if ( TriStringUtils.isEmpty( lotEntity ) ) {
				throw new TriSystemException( AmMessageId.AM005007S ) ;
			}

			if ( ! isTargetVcsCategory( lotEntity.getVcsCtgCd() ) ) {
				return serviceDto;
			}

			IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
			if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
				throw new TriSystemException( AmMessageId.AM005018S ) ;
			}

			this.innerExecute( serviceDto, paramInfoArray );

			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

		} catch( TriRuntimeException be ) {
			if ( null != logBusinessFlow ) {
				logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			}

			throw be ;
		} catch ( Exception e ) {
			if ( null != logBusinessFlow ) {
				logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			}
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005073S , e );
		}

		return serviceDto;
	}

	/**
	 * Templeteを継承したクラスで実装する必要のあるメソッドです。
	 *
	 * @param serviceDto Service呼び出し時のパラメータ
	 * @param paramInfoArray
	 */
	protected abstract void innerExecute(
			IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto,
			IMergeParamInfo[] paramInfoArray ) throws Exception;

	/**
	 * 処理対象のVCSかどうかの判定を行います。
	 *
	 * @param vcsCtgCd VCS分類コード
	 * @return 処理対象であればtrue。それ以外はfalse
	 */
	protected abstract boolean isTargetVcsCategory( String vcsCtgCd );

	/**
	 * ロット情報を更新する。
	 * <br>
	 * @param headBlDto
	 * @param lotDto
	 * @param lotDto ロット情報
	 * @param revisionMap
	 * @param logBusinessFlow
	 */
	protected void updateLotInfo(
			IHeadBlDto headBlDto,
			ILotDto lotDto,
			Map<String, String> revisionMap,
			LogBusinessFlow logBusinessFlow ) throws Exception {

		logBusinessFlow.writeLogWithDateBegin( " updateLotRecord "  ) ;

		try {
			IHeadBlEntity headBlEntity = headBlDto.getHeadBlEntity();
			ILotEntity lotEntity = lotDto.getLotEntity();

			//HEAD情報を更新する
			{
				IHeadEntity updHeadEntity = new HeadEntity();
				updHeadEntity.setVcsCtgCd			( lotEntity.getVcsCtgCd() );
				updHeadEntity.setHeadLatestBlTag	( headBlEntity.getHeadBlTag() );
				updHeadEntity.setHeadLatestVerTag	( headBlEntity.getHeadVerTag() );

				this.support.getHeadDao().update( updHeadEntity );
			}

			Set<String> targetMdlSet = new HashSet<String>();
			for ( IHeadBlMdlLnkEntity mdlLnkEntity: headBlDto.getMergeMdlEntities(true) ) {
				targetMdlSet.add( mdlLnkEntity.getMdlNm() );
			}

			//対象ロット情報を更新する
			{
				List<ILotMdlLnkEntity> mdlEntities = new ArrayList<ILotMdlLnkEntity>();

				for ( ILotMdlLnkEntity mdlEntity: lotDto.getIncludeMdlEntities( true ) ) {
					if ( ! targetMdlSet.contains( mdlEntity.getMdlNm() ) ) {
						continue;
					}

					ILotMdlLnkEntity updMdlEntity = new LotMdlLnkEntity();
					updMdlEntity.setLotId( lotEntity.getLotId() );
					updMdlEntity.setMdlNm( mdlEntity.getMdlNm() );
					updMdlEntity.setMergeHeadVerTag( headBlEntity.getHeadVerTag() );

					if ( revisionMap.containsKey( mdlEntity.getMdlNm() ) ) {
						updMdlEntity.setLotLatestMergeRev( revisionMap.get( mdlEntity.getMdlNm() ) );
					}

					mdlEntities.add( updMdlEntity );
				}

				this.support.getLotMdlLnkDao().update( mdlEntities );

				//ロット履歴情報を記録
				if ( DesignSheetUtils.isRecord() ) {

					ILotDto dto = this.support.findLotDto(lotEntity);

					IHistEntity histEntity = new HistEntity();
					histEntity.setActSts(UmActStatusId.none.getStatusId());

					this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , dto);
				}
			}

		} finally {
			logBusinessFlow.writeLogWithDateEnd( " updateLotRecord "  );
		}
	}

	/**
	 * HEAD・ベースラインのステータスを更新します。
	 * @param headBlDto
	 * @param trunkRevisionMap
	 * @param isWarning
	 * @return
	 */
	protected IHeadBlDto updateHeadBaselineInfo( IHeadBlDto headBlDto, Map<String, String> trunkRevisionMap, boolean isWarning ) {
		String headBlId = headBlDto.getHeadBlEntity().getHeadBlId();
		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		IHeadBlEntity updHeadBlEntity = new HeadBlEntity();
		updHeadBlEntity.setHeadBlId( headBlId );
		updHeadBlEntity.setMergeEndTimestamp( systemTimestamp );
		updHeadBlEntity.setStsId( AmHeadBlStatusId.Merged.getStatusId() );

		if ( isWarning ) {
			updHeadBlEntity.setMergeStsCd( AmHeadBlMergeStatusCode.LOT_MERGE_RESOURCE_WARNING.getStatusCode() );
		} else {
			updHeadBlEntity.setMergeStsCd( AmHeadBlMergeStatusCode.LOT_MERGE_SUCCESS.getStatusCode() );
		}

		this.support.getHeadBlDao().update( updHeadBlEntity );

		List<IHeadBlMdlLnkEntity> updMdlLnkEntities = new ArrayList<IHeadBlMdlLnkEntity>();
		for ( IHeadBlMdlLnkEntity mdlEntity: headBlDto.getMergeMdlEntities(true) ) {
			String rev = "";
			if ( trunkRevisionMap.containsKey( mdlEntity.getMdlNm() ) ) {
				rev = trunkRevisionMap.get( mdlEntity.getMdlNm() );
			}

			IHeadBlMdlLnkEntity updMdlLnkEntity = new HeadBlMdlLnkEntity();
			updMdlLnkEntity.setHeadBlId( mdlEntity.getHeadBlId() );
			updMdlLnkEntity.setMdlNm( mdlEntity.getMdlNm() );
			updMdlLnkEntity.setMdlVerTag( headBlDto.getHeadBlEntity().getHeadVerTag() );
			updMdlLnkEntity.setMergeHeadRev( rev );

			updMdlLnkEntities.add( updMdlLnkEntity );
		}

		this.support.getHeadBlMdlLnkDao().update( updMdlLnkEntities );

		return this.support.findHeadBlDto( headBlId );
	}

	/**
	 * ロット・ベースラインのステータスをマージ済にします。
	 * @param paramInfoArray
	 */
	protected void updateLotBaselineInfo( IMergeParamInfo[] paramInfoArray ) {
		Set<String> dataIdSet = new HashSet<String>();
		for ( IMergeParamInfo param: paramInfoArray ) {
			dataIdSet.add(param.getLotBlDto().getLotBlEntity().getDataId());
		}

		String[] dataIds = dataIdSet.toArray(new String[0]);

		LotBlCondition condition = new LotBlCondition();
		condition.setDataIds( dataIds );

		ILotBlEntity updLotBlEntity = new LotBlEntity();
		updLotBlEntity.setStsId( AmLotBlStatusId.Merged.getStatusId() );

		this.support.getLotBlDao().update(condition.getCondition(), updLotBlEntity);
	}

	/**
	 * 変更情報のステータスをマージ済にします。
	 * @param paramInfoArray
	 */
	protected void updatePjtInfo( IMergeParamInfo[] paramInfoArray ) {
		Set<String> pjtIdSet = new HashSet<String>();
		for ( IMergeParamInfo param: paramInfoArray ) {
			List<ILotBlReqLnkEntity> areqLnkEntities = param.getLotBlDto().getLotBlReqLnkEntities();
			pjtIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toPjtIdsFromLotBlReqLnk ).asList() );
		}

		if( 0<pjtIdSet.size() ) {	// 全資産時全レコード更新対策
			String[] pjtIds = pjtIdSet.toArray(new String[0]);

			PjtCondition condition = new PjtCondition();
			condition.setPjtIds( pjtIds );

			IPjtEntity updPjtEntity = new PjtEntity();
			updPjtEntity.setStsId( AmPjtStatusId.Merged.getStatusId() );

			this.support.getPjtDao().update(condition.getCondition(), updPjtEntity);
		}
	}

	/**
	 * 資産申請のステータスをマージ済にします。
	 * @param paramInfoArray
	 */
	protected void updateAreqInfo( IMergeParamInfo[] paramInfoArray ) {
		Set<String> areqIdSet = new HashSet<String>();

		for ( IMergeParamInfo param: paramInfoArray ) {
			List<ILotBlReqLnkEntity> areqLnkEntities = param.getLotBlDto().getLotBlReqLnkEntities();
			areqIdSet.addAll( FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk ).asList() );
		}

		if( 0<areqIdSet.size() ) {	// 全資産時全レコード更新対策

			String[] areqIds = areqIdSet.toArray(new String[0]);

			AreqCondition condition = new AreqCondition();
			condition.setAreqIds( areqIds );

			IAreqEntity updAreqEntity = new AreqEntity();
			updAreqEntity.setStsId( AmAreqStatusId.Merged.getStatusId() );

			this.support.getAreqDao().update(condition.getCondition(), updAreqEntity);

			//履歴に記録
			if ( DesignSheetUtils.isRecord() ) {
				for ( String areqId: areqIds ) {
					IAreqDto areqDto = this.support.findAreqDto(areqId);

					IHistEntity histEntity = new HistEntity();
					histEntity.setActSts(UmActStatusId.none.getStatusId());

					support.getAreqHistDao().insert( histEntity , areqDto );
				}
			}
		}
	}

}
