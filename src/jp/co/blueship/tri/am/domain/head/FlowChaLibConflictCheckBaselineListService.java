package jp.co.blueship.tri.am.domain.head;

import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckBaselineListServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 変更管理・コンフリクトチェック ベースライン一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowChaLibConflictCheckBaselineListService implements IDomain<FlowChaLibConflictCheckBaselineListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeViewSupport support = null;

	public void setSupport( FlowChaLibMergeViewSupport support ) {
		this.support = support;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IServiceDto<FlowChaLibConflictCheckBaselineListServiceBean> execute(IServiceDto<FlowChaLibConflictCheckBaselineListServiceBean> serviceDto ) {

		FlowChaLibConflictCheckBaselineListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			AmItemChkUtils.checkLotNo( selectedLotNo );

			ILotDto lotDto = this.support.findLotDto( selectedLotNo );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			// Scmモード
			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
			paramBean.setScmType( scmType.value() );

			paramBean.setLotDetailViewBean( this.support.getLotDetailViewBean( paramBean, lotDto ));

			setServiceBeanRelUnitResult	( selectedLotNo, paramBean );

			int selectPageNo =
				( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

			setServiceBeanBuildEntity		( selectedLotNo, selectPageNo, paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException(AmMessageId.AM005086S, e );
		}
	}

	/**
	 * Beanにビルドパッケージ作成結果の件数を設定する。
	 * @param selectedLotNo ロット番号
	 * @param retBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */
	private void setServiceBeanRelUnitResult(
			String selectedLotNo, FlowChaLibConflictCheckBaselineListServiceBean retBean ) {

		IJdbcCondition successCountCondition =
			AmDBSearchConditionAddonUtils.getSucceededRelUnitCondition( selectedLotNo );

		retBean.setSucceededRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( successCountCondition.getCondition() ) );


		IJdbcCondition failureCountCondition =
			AmDBSearchConditionAddonUtils.getFailedRelUnitCondition( selectedLotNo );

		retBean.setFailedRelUnitCount( this.support.getBmFinderSupport().getBpDao().count( failureCountCondition.getCondition() ) );
	}

	/**
	 * Beanに変更管理情報の一覧検索結果を設定する。
	 * @param lotId ロット番号
	 * @param selectPageNo 選択ページ番号
	 * @param retBean FlowChaLibConflictCheckBaselineListServiceBeanオブジェクト
	 */

	private void setServiceBeanBuildEntity(
			String lotId, int selectPageNo, FlowChaLibConflictCheckBaselineListServiceBean retBean ) {

		//Lotに紐付くRU全件を取得
		List<ILotBlEntity> lotBlEntityArray = this.support.getLotBlEntitiesByMergeCheckEnable( lotId , null , TriSortOrder.Desc );
		IEntityLimit<ILotBlEntity> lotBlEntityLimit = new EntityLimit<ILotBlEntity>();
		lotBlEntityLimit.setEntities( lotBlEntityArray );

		//クローズ済みのＲＵが１件もなければ元画面に戻す
		if( TriStringUtils.isEmpty( lotBlEntityArray ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001107E , lotId) ;
		}
		retBean.setBaselineViewBeanList	( this.support.getLotBaselineViewGroupByVersionTag( lotBlEntityArray.toArray(new ILotBlEntity[0]) ));
		retBean.setPageInfoView		( AmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), lotBlEntityLimit.getLimit() ) ) ;
		retBean.setSelectPageNo		( selectPageNo );
	}
}
