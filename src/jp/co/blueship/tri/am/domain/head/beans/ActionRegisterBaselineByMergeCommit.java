package jp.co.blueship.tri.am.domain.head.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * マージ情報のステータスをマージ処理中に更新します。<br>
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class ActionRegisterBaselineByMergeCommit extends ActionPojoAbstract<FlowChaLibMergeCommitServiceBean> {

	private FlowChaLibMergeEditSupport support = null;
	public void setSupport( FlowChaLibMergeEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute( IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
		if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
			throw new TriSystemException( AmMessageId.AM005018S ) ;
		}

		FlowChaLibMergeCommitServiceBean paramBean = serviceDto.getServiceBean();
		ILotDto lotDto = paramBean.getCacheLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		IHeadBlDto headBlDto = this.support.getTargetHeadBlDtoForMerge( lotEntity.getLotId() );
		headBlDto = this.updateHeadBl( paramBean, headBlDto );
		paramBean.setRegisterHeadBlDto( headBlDto );

		//HEAD・ベースラインの実行中ステータス(Logical Key = ロットID)
		{
			this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_HEAD_BL, lotEntity.getLotId());
			this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_HEAD_BL, AmHeadBlStatusIdForExecData.Merging, lotEntity.getLotId());
		}

		//ロット・ベースラインの実行中ステータス(Logical Key = データID)
		{
			Set<String> dataIdSet = new HashSet<String>();
			for ( IMergeParamInfo param: paramInfoArray ) {
				dataIdSet.add(param.getLotBlDto().getLotBlEntity().getDataId());
			}

			String[] dataIds = dataIdSet.toArray(new String[0]);
			this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_LOT_BL, dataIds);
			this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.Merging, dataIds);
		}

		//変更情報の実行中ステータス
		{
			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();

				Set<String> pjtIdSet = new HashSet<String>();

				for( ILotBlReqLnkEntity areqLnkEntity : areqLnkEntities ) {
					pjtIdSet.add( areqLnkEntity.getPjtId() );
				}

				String[] pjtIds = pjtIdSet.toArray(new String[0]);
				this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_PJT, pjtIds);
				this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_PJT, AmPjtStatusIdForExecData.Merging, pjtIds);
			}
		}

		//資産申請情報の実行中ステータス
		{
			for( IMergeParamInfo paramInfo : paramInfoArray ) {
				List<ILotBlReqLnkEntity> areqLnkEntities = paramInfo.getLotBlDto().getLotBlReqLnkEntities();
				String[] areqIds = FluentList.from( areqLnkEntities ).map( AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk ).toArray(new String[0]);

				this.support.getSmFinderSupport().cleaningExecDataSts(AmTables.AM_AREQ, areqIds);
				this.support.getSmFinderSupport().registerExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.Merging, areqIds);
			}
		}

		return serviceDto;
	}

	/**
	 * HEAD・ベースライン更新
	 *
	 * @param param
	 * @param headBlDto
	 * @return 更新したHEAD・ベースラインEntity
	 */
	private IHeadBlDto updateHeadBl( FlowChaLibMergeCommitServiceBean param, IHeadBlDto headBlDto ) {
		IHeadBlEntity headBlEntity = headBlDto.getHeadBlEntity();

		MergeEditInputBean inputBean = param.getMergeEditInputBean();

		IHeadBlEntity updHeadBlEntity = new HeadBlEntity();

		updHeadBlEntity.setHeadBlId( headBlEntity.getHeadBlId() );
		updHeadBlEntity.setHeadVerTag( param.getNumberingVal() );
		updHeadBlEntity.setHeadBlTag( null );
		updHeadBlEntity.setMergeUserId( param.getUserId() );
		updHeadBlEntity.setMergeUserNm( param.getUserName() );
		updHeadBlEntity.setMergeStTimestamp( TriDateUtils.getSystemTimestamp() );
		updHeadBlEntity.setMergeEndTimestamp( null );
		updHeadBlEntity.setMergeCmt( TriObjectUtils.defaultIfNull(inputBean.getMergeComment(), "") );
		updHeadBlEntity.setDelStsId( StatusFlg.off );

		this.support.getHeadBlDao().update( updHeadBlEntity );

		return this.support.findHeadBlDto( updHeadBlEntity.getHeadBlId() );
	}

}
