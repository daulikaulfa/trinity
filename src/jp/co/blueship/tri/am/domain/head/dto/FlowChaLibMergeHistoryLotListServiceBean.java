package jp.co.blueship.tri.am.domain.head.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * 変更管理・マージ マージ履歴ロット一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMergeHistoryLotListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	public List<LotViewBean> getLotViewBeanList() {
		if ( null == lotViewBeanList ) {
			lotViewBeanList = new ArrayList<LotViewBean>();
		}
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
}
