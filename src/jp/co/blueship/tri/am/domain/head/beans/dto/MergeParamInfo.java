package jp.co.blueship.tri.am.domain.head.beans.dto;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class MergeParamInfo extends CommonParamInfo implements IMergeParamInfo {

	private static final long serialVersionUID = 2L;

	private ILotBlDto lotBlDto;
	private String comment = null;

	@Override
	public ILotBlDto getLotBlDto() {
	    return lotBlDto;
	}
	@Override
	public void setLotBlDto(ILotBlDto lotBlDto) {
	    this.lotBlDto = lotBlDto;
	}
	@Override
	public String getComment() {
		return comment;
	}
	@Override
	public void setComment( String comment ) {
		this.comment = comment;
	}

}
