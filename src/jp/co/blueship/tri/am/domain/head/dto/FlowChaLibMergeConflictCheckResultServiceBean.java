package jp.co.blueship.tri.am.domain.head.dto;


import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 変更管理・ロット詳細閲覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMergeConflictCheckResultServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ロット番号 */
	private String selectedLotNo = null;

	/** 選択ビルドパッケージクローズバージョンタグ */
	private String selectedCloseVersionTag = null ;
	/** */
	private LotBaselineViewGroupByVersionTag baselineViewBean = null ;
	/** マージ判定結果Map **/
	private Map<String,MergeResult> mergeResultMap = null ;
	/** マージ判定結果View **/
	private List<MergeViewBean> mergeViewBeanList = null ;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedCloseVersionTag() {
		return selectedCloseVersionTag;
	}
	public void setSelectedCloseVersionTag(String selectedCloseVersionTag) {
		this.selectedCloseVersionTag = selectedCloseVersionTag;
	}
	public Map<String, MergeResult> getMergeResultMap() {
		return mergeResultMap;
	}
	public void setMergeResultMap(Map<String, MergeResult> mergeResultMap) {
		this.mergeResultMap = mergeResultMap;
	}
	public List<MergeViewBean> getMergeViewBeanList() {
		return mergeViewBeanList;
	}
	public void setMergeViewBeanList(List<MergeViewBean> mergeViewBeanList) {
		this.mergeViewBeanList = mergeViewBeanList;
	}
	public LotBaselineViewGroupByVersionTag getBaselineViewBean() {
		return baselineViewBean;
	}
	public void setBaselineViewBean(LotBaselineViewGroupByVersionTag baselineViewBean) {
		this.baselineViewBean = baselineViewBean;
	}
}