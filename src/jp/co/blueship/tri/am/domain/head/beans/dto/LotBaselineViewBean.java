package jp.co.blueship.tri.am.domain.head.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.IServiceBean;

/**
 * ロット・ベースライン情報
 *
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class LotBaselineViewBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	/** データID */
	private String dataId = null;
	/** ロット・ベースライン名 */
	private String baselineName = null;
	/** チェックイン日時 */
	private String checkInDate = null ;

	public String getDataId() {
		return dataId;
	}
	public void setDataId(String dataId) {
		this.dataId = dataId;
	}
	public String getBaselineName() {
		return baselineName;
	}
	public void setBaselineName(String baselineName) {
		this.baselineName = baselineName;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}

}