package jp.co.blueship.tri.am.domain.head.beans;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;

import jp.co.blueship.tri.am.AmBusinessFileUtils;
import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.constants.MergeStatus;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeResult;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlMergeStatusCode;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.MergeVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;

/**
 * コンフリクトチェック処理を行います。
 *
 * @version V3L10.01
 * @author trinity V3
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public class ActionConflictCheckForSvn extends ActionPojoAbstract<FlowChaLibConflictCheckServiceBean> {

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMergeEditSupport support = null;
	public void setSupport( FlowChaLibMergeEditSupport support ) {
		this.support = support;
	}
	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}
	private IVcsService vcsService = null;

	@Override
	public IServiceDto<FlowChaLibConflictCheckServiceBean> execute( IServiceDto<FlowChaLibConflictCheckServiceBean> serviceDto ) {

		FlowChaLibConflictCheckServiceBean paramBean = serviceDto.getServiceBean();
		LogBusinessFlow logService = paramBean.getLogService();

		try {

			List<Object> paramList = serviceDto.getParamList();

			ILotDto lotDto = paramBean.getCacheLotDto();
			ILotEntity lotEntity = lotDto.getLotEntity();

			if ( TriStringUtils.isEmpty( lotEntity ) ) {
				throw new TriSystemException( AmMessageId.AM005007S );
			}

			if( ! VcsCategory.SVN.equals( VcsCategory.value( lotEntity.getVcsCtgCd() ))) {
				return serviceDto;
			}

			ILotBlDto lotBlDto = paramBean.getSelectedLotBlDto();
			ILotBlEntity lotBlEntity = lotBlDto.getLotBlEntity();

			if (TriStringUtils.isEmpty(lotBlEntity.getLotVerTag())) {
				throw new TriSystemException(AmMessageId.AM004026F, lotBlEntity.getLotBlId());
			}

			IMergeParamInfo[] paramInfoArray = AmExtractMessageAddonUtils.getMergeParamInfoArray( paramList ) ;
			if ( TriStringUtils.isEmpty( paramInfoArray ) ) {
				throw new TriSystemException( AmMessageId.AM005018S ) ;
			}

			Map<String, String> checkInRevMap = new HashMap<String, String>();
			for ( IMergeParamInfo paramInfo: paramInfoArray ) {
				for ( ILotBlMdlLnkEntity mdlEntity: paramInfo.getLotBlDto().getCheckInMdlEntities(true) ) {
					if ( ! checkInRevMap.containsKey( mdlEntity.getMdlNm() ) ) {
						checkInRevMap.put(mdlEntity.getMdlNm(), mdlEntity.getLotChkinRev());
					}
				}
			}

			IHeadEntity headEntity = this.support.findHeadEntity( VcsCategory.value(lotEntity.getVcsCtgCd()) ) ;

			//リポジトリ情報の取得
			String srvId = this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId();

			List<IVcsReposDto> reposDtoList =
					this.support.findVcsReposDtoFromLotMdlLnkEntities( lotDto.getIncludeMdlEntities( true ) );
			if ( 0 == reposDtoList.size() ) {
				throw new TriSystemException( AmMessageId.AM004062F );
			}

			String userName = reposDtoList.get(0).getVcsReposEntity().getVcsUserNm();
			String password = this.support.getSmFinderSupport().findPasswordEntity( PasswordCategory.SVN , srvId , userName ).getPassword();

			vcsService = factory.createService(VcsCategory.value( lotEntity.getVcsCtgCd() ), userName, password);

			//マージ用の作業フォルダパス取得
			String workspace = lotEntity.getWsPath() ;
			String mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;
			String mergeTempBranchCheckOut = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTempBranchCheckOut ) ) ;
			String commitTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.commitTemp ) ) ;

			//マージ用の作業フォルダパス以下を全部初期化（消す）
			AmBusinessFileUtils.cleaningDirectory( mergeTemp ) ;
			AmBusinessFileUtils.cleaningDirectory( mergeTempBranchCheckOut ) ;
			AmBusinessFileUtils.cleaningDirectory( commitTemp ) ;

			/**モジュール単位に処理**/
			List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true ) ;

			if( TriStringUtils.isEmpty( mdlEntities ) ) {
				throw new TriSystemException( AmMessageId.AM004003F , lotEntity.getLotId() , lotEntity.getLotLatestVerTag() );
			}
			//前回マージ実行の有無により、今回のマージ範囲の基底リビジョンを設定する
			Map<String, Long> pegRevisionMap = this.getPegRevisionNo( lotDto , checkInRevMap , logService ) ;
			String branchTag = lotEntity.getLotBranchTag() ;

			Map<String,SVNStatus> svnStatusMap = new HashMap<String,SVNStatus>() ;
			for( IVcsReposDto reposDto : reposDtoList ) {
				String moduleName = reposDto.getVcsMdlEntity().getMdlNm();

				if ( ! checkInRevMap.containsKey( moduleName ) ) {
					continue;
				}

				IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();

				long pegRevisionNo = pegRevisionMap.get( moduleName );
				String checkInRev = checkInRevMap.get( moduleName );

				/** MergeTempへHEADをチェックアウト **/
				vcsService.checkoutHead(setChkHeadVO(reposEntity.getVcsReposUrl(), mergeTemp, moduleName, logService));
				/** MergeTempBranchCheckOutへブランチ資産をチェックアウト **/
				vcsService.checkoutBranchRevision(setChkBranchVO(reposEntity.getVcsReposUrl() , mergeTempBranchCheckOut , moduleName , branchTag , checkInRev , logService));
				/**マージ処理を行う**/
				vcsService.merge(setMergeVO(reposEntity.getVcsReposUrl() , mergeTemp , branchTag , moduleName , pegRevisionNo , Long.parseLong( checkInRev ) , logService ));
				//マージ後のワーキングコピーのステータスを取得する
				List<SVNStatus> svnStatusList = this.getMergeStatus(mergeTemp , moduleName , logService) ;

				//Mapに詰めなおす
				for( SVNStatus svnStatus : svnStatusList ) {
					String fullPath = TriStringUtils.convertPath( svnStatus.getFile().getPath() ) ;
					String relativePath = StringUtils.replace( fullPath , mergeTemp + "/" , "" ) ;//絶対パス部+最後の"/"を取り除き、モジュール名以降のみに編集
					svnStatusMap.put( relativePath , svnStatus ) ;
				}
			}
			Map<String,MergeResult> mergeResultMap = this.makeMergeResultForSvn( headEntity, lotDto , lotBlEntity , svnStatusMap ) ;

			/** マージ結果をまとめ、ファイルに出力する。（Javaのオブジェクトをそのままダンプ）**/
			this.saveFileMergeResult( lotEntity , mergeResultMap , logService ) ;

			Map<String, String> trunkRevisionMap = this.getRevisionMapByMdlNmKey( reposDtoList );

			paramBean.setRegisterHeadBlDto( this.updateHeadBaselineInfo( paramBean.getRegisterHeadBlDto(), trunkRevisionMap, isConflicted(mergeResultMap) ) );

			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );


		} catch( TriRuntimeException be ) {
			if ( null != logService ) {
				logService.writeLog( be.getStackTraceString() ) ;
			}

			throw be ;
		} catch ( Exception e ) {
			if ( null != logService ) {
				logService.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			}
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005072S , e );
		}

		return serviceDto;
	}

	private IHeadBlDto updateHeadBaselineInfo( IHeadBlDto headBlDto, Map<String, String> trunkRevisionMap,boolean isWarning ) {
		String headBlId = headBlDto.getHeadBlEntity().getHeadBlId();
		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		IHeadBlEntity updHeadBlEntity = new HeadBlEntity();
		updHeadBlEntity.setHeadBlId( headBlId );
		updHeadBlEntity.setMergechkEndTimestamp( systemTimestamp );
		updHeadBlEntity.setStsId( AmHeadBlStatusId.ConflictChecked.getStatusId() );
		if ( isWarning ) {
			updHeadBlEntity.setMergeStsCd( AmHeadBlMergeStatusCode.LOT_MERGE_CHECK_RESOURCE_WARNING.getStatusCode() );
		} else {
			updHeadBlEntity.setMergeStsCd(AmHeadBlMergeStatusCode.LOT_MERGE_CHECK_SUCCESS.getStatusCode() );
		}

		this.support.getHeadBlDao().update( updHeadBlEntity );

		List<IHeadBlMdlLnkEntity> updMdlLnkEntities = new ArrayList<IHeadBlMdlLnkEntity>();
		for ( IHeadBlMdlLnkEntity mdlEntity: headBlDto.getMergeMdlEntities(true) ) {
			mdlEntity.setMergechkHeadRev( trunkRevisionMap.get( mdlEntity.getMdlNm() ) );
			updMdlLnkEntities.add( mdlEntity );
		}

		this.support.getHeadBlMdlLnkDao().update( updMdlLnkEntities );

		return this.support.findHeadBlDto( headBlId );
	}

	/**
	 * ＨＥＡＤの資産をチェックアウトするための情報をVOにセットする<br>
	 * @param repository リポジトリ
	 * @param workDir 作業ディレクトリパス
	 * @param moduleName モジュール名
	 * @param logService ログ情報
	 * @return vo;
	 */
	private CheckOutVO setChkHeadVO( String repository , String workDir , String moduleName , LogBusinessFlow logService ) {

		logService.writeLog( TriLogMessage.LAM0017 , SEP , repository + SEP , workDir + SEP , moduleName ) ;

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository(repository);
		vo.setPathDest(workDir);
		vo.setModuleName(moduleName);
		return vo;
	}

	/**
	 * ロットの資産をチェックアウトするための情報をVOにセットする<br>
	 * @param repository リポジトリ
	 * @param workDir 作業ディレクトリパス
	 * @param moduleName モジュール名
	 * @param logService ログ情報
	 * @return vo;
	 */
	private CheckOutVO setChkBranchVO(String repository , String workDir , String moduleName , String branchTag , String revision , LogBusinessFlow logService ){

		logService.writeLog( TriLogMessage.LAM0021 , SEP , repository + SEP , workDir + SEP , moduleName + SEP , branchTag + SEP , revision ) ;

		CheckOutVO vo = new CheckOutVO();
		vo.setRepository(repository);
		vo.setPathDest(workDir);
		vo.setModuleName(moduleName);
		vo.setLabelBranch(branchTag);
		vo.setRevisionNo(Long.parseLong(revision));
		return vo;
	}

	/**
	 * マージ処理を行うための情報をVOにセットする<br>
	 * @param repository リポジトリ
	 * @param workDir 作業ディレクトリパス
	 * @param branchTag ブランチタグ
	 * @param moduleName モジュール名
	 * @param pegRevisionNo マージ差分基点のRevisionNo
	 * @param versionTag バージョンタグ
	 * @param logService ログ情報
	 * @return vo;
	 */
	private MergeVO setMergeVO(String repository , String workDir , String branchTag , String moduleName ,
								long pegRevisionNo , long baselineRevisionNo ,
									LogBusinessFlow logService ){

		logService.writeLog(TriLogMessage.LAM0024 , SEP , repository + SEP , workDir + SEP , moduleName + SEP ,
				branchTag + SEP , String.valueOf(pegRevisionNo) + SEP , String.valueOf(baselineRevisionNo) ) ;

		MergeVO vo = new MergeVO();
		vo.setRepository(repository);
		vo.setPathDest(workDir);
		vo.setLabelBranch(branchTag);
		vo.setModuleName(moduleName);
		vo.setMergeFrom(SVNRevision.create( pegRevisionNo ));
		vo.setMergeTo(SVNRevision.create( baselineRevisionNo ));
		return vo;
	}
	/**
	 * マージ後のステータスを取得、マージレポートを作成してファイルに出力する<br>
	 * @param pjtLotEntity ロット情報
	 * @param logService
	 * @throws Exception
	 */
	private void saveFileMergeResult( ILotEntity pjtLotEntity ,
										Map<String,MergeResult> mergeResultMap ,
										LogBusinessFlow logService ) throws Exception {

		//マージ用の作業フォルダパス取得
		String workspace = pjtLotEntity.getWsPath() ;
		String mergeTemp = TriStringUtils.linkPathBySlash( workspace , sheet.getValue( AmDesignEntryKeyByMerge.mergeTemp ) ) ;

		String outFilePath = TriStringUtils.linkPathBySlash( mergeTemp , sheet.getValue( AmDesignEntryKeyByMerge.mergeResultFileName ) ) ;
		logService.writeLog( TriLogMessage.LAM0016 , outFilePath ) ;

		File outFile = new File( outFilePath ) ;
		TriFileUtils.writeObjectToFile( outFile , (Object)mergeResultMap ) ;

	}
	/**
	 * マージ後のワーキングコピーのステータスを取得する。
	 *
	 */
	private List<SVNStatus> getMergeStatus(String workPath , String moduleName , LogBusinessFlow logService ) throws ScmException {

		logService.writeLog( TriLogMessage.LAM0027 , moduleName ) ;

		List<SVNStatus> svnStatusList = vcsService.mergeStatusAll(setStatusVO(workPath,moduleName));
		if( TriStringUtils.isEmpty( svnStatusList ) ) {
			return null ;
		} else {
			//ログにSVNStatusのダンプを出力する
			logService.writeLog( vcsService.makeSvnStatusStr(svnStatusList) ) ;
			return svnStatusList ;
		}
	}

	private StatusVO setStatusVO(String workPath,String moduleName){
		StatusVO vo = new StatusVO();
		vo.setPathDest(workPath);
		vo.setModuleName(moduleName);
		return vo;
	}

	private Map<String, String> getRevisionMapByMdlNmKey( List<IVcsReposDto> reposDtoList ) throws ScmException {

		Map<String, String> revisionMap = new HashMap<String, String>();
		Map<String, Long> reposMap = new HashMap<String, Long>();

		for ( IVcsReposDto reposDto: reposDtoList ) {
			IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
			String moduleName = reposDto.getVcsMdlEntity().getMdlNm();

			if ( ! reposMap.containsKey( reposEntity.getVcsReposId() ) ) {
				long trunkRevisionNo = vcsService.retriveTrunkRevision(setLisVO(reposEntity.getVcsReposUrl()));
				reposMap.put( reposEntity.getVcsReposId(), trunkRevisionNo );
			}

			revisionMap.put( moduleName, String.valueOf( reposMap.get( reposEntity.getVcsReposId() ) ) );

		}

		return revisionMap;
	}

	private ListSubDirVO setLisVO(String repository){
		ListSubDirVO vo = new ListSubDirVO();
		vo.setRepository(repository);
		return vo;
	}

	/**
	 * 前回マージ実行の有無により、今回のマージ範囲の基底リビジョンを設定する。<br>
	 * <pre>
	 * 		『過去にマージ履歴あり』： マージの基底リビジョンを、前回マージリビジョンに設定する
	 * 		『過去にマージ履歴なし』： マージの基底リビジョンを、ブランチ作成時のリビジョンに設定する
	 * </pre>
	 * @param lotDto ロット情報
	 * @param checkInRevMap ベースラインのビルド情報
	 * @param logService ログハンドラ
	 * @return
	 */
	private Map<String, Long> getPegRevisionNo( ILotDto lotDto, Map<String, String> checkInRevMap, LogBusinessFlow logService ) {
		logService.writeLog( TriLogMessage.LAM0028 ) ;

		ILotEntity lotEntity = lotDto.getLotEntity();
		List<ILotMdlLnkEntity> lotMdlEntities = lotDto.getIncludeMdlEntities(true);
		Map<String, Long> revisionMap = new HashMap<String, Long>();

		for ( ILotMdlLnkEntity mdlEntity: lotMdlEntities ) {
			String latestMergedRevision = mdlEntity.getLotLatestMergeRev() ;//前回マージ時に取り込んだロットベースラインのリビジョン
			String branchTag = lotEntity.getLotBranchTag() ;
			String branchRevision = mdlEntity.getLotRegRev() ;//ロット（ブランチ）作成時のリビジョン
			String checkInRevision = checkInRevMap.get( mdlEntity.getMdlNm() );

			logService.writeLog( TriLogMessage.LAM0029 , latestMergedRevision + SEP , branchTag + SEP , branchRevision + SEP , checkInRevision ) ;

			//ブランチ後１度でもマージしたことがあれば、前回マージリビジョン。未マージならばブランチ作成時のリビジョンとする。
			String pegRevision = null ;
			if( ! TriStringUtils.isEmpty( latestMergedRevision ) ) {
				pegRevision = latestMergedRevision ;
				logService.writeLog( TriLogMessage.LAM0032 , latestMergedRevision ) ;
			} else {
				pegRevision = branchRevision ;
				logService.writeLog( TriLogMessage.LAM0033 , branchRevision ) ;
			}

			long pegRevisionNo = Long.parseLong( pegRevision ) ;

			revisionMap.put(mdlEntity.getMdlNm(), pegRevisionNo);
		}

		return revisionMap;
	}
	/**
	 *
	 * @param svnStatusList
	 * @return
	 */
	public Map<String,MergeResult> makeMergeResultForSvn(
			IHeadEntity headEntity,
			ILotDto lotDto,
			ILotBlEntity lotBlEntity,
			Map<String,SVNStatus> svnStatusMap ) throws Exception {

		List<Object> paramList = new ArrayList<Object>() ;
		paramList.add( lotDto );//Util用

		/**クローズ日時が古い順にRUビルドに紐づく申請を処理、対象ファイルの照合を行う**/
		Map<String,MergeResult> mergeResultMap = new LinkedHashMap<String,MergeResult>() ;

		if( null == lotBlEntity ) {
			throw new TriSystemException( AmMessageId.AM004006F , lotDto.getLotEntity().getLotId() );
		}

		ILotEntity lotEntity = lotDto.getLotEntity();
//		List<ILotBlDto> lotBlDtoList = this.support.getLotBlDtoByVersionTag( lotEntity.getLotId() , lotBlEntity.getLotVerTag() , TriSortOrder.Asc ) ;
		// 指定のベースライン以前のデータが対象
		List<ILotBlDto> lotBlDtoList = this.support.getLotBlDtoByMergeCheckEnable( lotEntity.getLotId() , lotBlEntity.getLotChkinTimestamp() , TriSortOrder.Asc ) ;
		for( ILotBlDto lotBlDto : lotBlDtoList ) {

			String[] areqIds = FluentList.from(
					lotBlDto.getLotBlReqLnkEntities()).map( AmFluentFunctionUtils.toAreqIdsFromLotBlReqLnk ).toArray( new String[0] );

			//パッケージ作成（全資産）の場合等、パッケージに紐付く資産情報は存在しない場合がある。
			List<IAreqDto> areqDtoEntities = this.support.getAreqEntitiesSortByPjtAvlTimestamp( areqIds ) ;

			//申請番号単位に処理する
			for( IAreqDto areqDto : areqDtoEntities ) {
				IAreqEntity areqEntity = areqDto.getAreqEntity();

				/**ソース一覧とdiff/mergeの結果とを照合する **/
				String returnAssetApplyNoPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, areqEntity ).getAbsolutePath() ;
				String returnAssetPath = TriStringUtils.convertPath( returnAssetApplyNoPath ) ;

				List<IAreqFileEntity> assetFileEntityArray = null ;
				if ( AmBusinessJudgUtils.isReturnApply( areqEntity.getAreqCtgCd() ) ) {//返却資産
					assetFileEntityArray = areqDto.getAreqFileEntities( AreqCtgCd.ReturningRequest );
				} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity.getAreqCtgCd() ) ) {//削除資産
					assetFileEntityArray = areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest );
				} else {
					throw new TriSystemException( AmMessageId.AM004007F , areqEntity.getAreqId() , areqEntity.getAreqCtgCd() );
				}

				if ( null != assetFileEntityArray ) {

					for( IAreqFileEntity assetFileEntity : assetFileEntityArray ) {
						//SVNStatusからmerge結果照合
						String filePath = assetFileEntity.getFilePath() ;
						SVNStatus svnStatus = null ;
						File file = new File(filePath);
						while (file != null && !svnStatusMap.containsKey( TriStringUtils.convertPath( file.getPath() ) )) {
							file = file.getParentFile();
						}
						if (file != null) {
							svnStatus = svnStatusMap.get( TriStringUtils.convertPath( file.getPath() ) ) ;
						}
						if (svnStatus == null) {
							continue;
						}

						MergeStatus mergeStatus = MergeStatus.Modified ;
						SVNStatusType svnStatusType = svnStatus.getContentsStatus() ;
						if( SVNStatusType.STATUS_CONFLICTED.equals( svnStatusType ) ||
								SVNStatusType.STATUS_MISSING.equals( svnStatusType ) ||
								SVNStatusType.CONFLICTED_UNRESOLVED.equals( svnStatusType ) ||
								SVNStatusType.STATUS_NAME_CONFLICT.equals( svnStatusType ) ||
								svnStatus.isConflicted()) {//コンフリクト
							mergeStatus = MergeStatus.Conflicted ;
						} else if( SVNStatusType.STATUS_DELETED.equals( svnStatusType ) ) {//削除
							mergeStatus = MergeStatus.Deleted ;
						} else if( SVNStatusType.STATUS_ADDED.equals( svnStatusType ) ) {//新規追加
							mergeStatus = MergeStatus.Added ;
						} else if( SVNStatusType.STATUS_MODIFIED.equals( svnStatusType ) ) {//変更
							mergeStatus = MergeStatus.Modified ;
						}else if( SVNStatusType.STATUS_NORMAL.equals( svnStatusType ) ) {
							if ( SVNStatusType.STATUS_ADDED.equals(svnStatus.getNodeStatus()) ) {
								mergeStatus = MergeStatus.Added ;
							} else if ( SVNStatusType.STATUS_DELETED.equals(svnStatus.getNodeStatus()) ) {
								mergeStatus = MergeStatus.Deleted ;
							}
						}

						//バイナリdiff結果照合
						Status diffStatus = null ;
						String headPath = TriStringUtils.linkPathBySlash( headEntity.getMwPath() , filePath ) ;
						String lotPath = TriStringUtils.linkPathBySlash( returnAssetPath , filePath ) ;

						if ( AmBusinessJudgUtils.isReturnApply( areqEntity.getAreqCtgCd() ) ) {//返却資産
							diffStatus = AmBusinessFileUtils.checkBinaryDiff( lotPath , headPath ) ;
						} else if ( AmBusinessJudgUtils.isDeleteApply( areqEntity.getAreqCtgCd() ) ) {//削除資産
							diffStatus = AmBusinessFileUtils.checkDeleteDiff(headPath);
						} else {
							throw new TriSystemException( AmMessageId.AM004007F , areqEntity.getAreqId() , areqEntity.getAreqCtgCd() );
						}
						//マージ結果セット
						String keyLabel = filePath ;//モジュール名以下でユニーク性を保証
						String moduleName = StringUtils.substringBefore( filePath , "/" ) ;
						String relativePath = StringUtils.substringAfter( filePath , "/" ) ;

						MergeResult mergeResult = ( mergeResultMap.containsKey( keyLabel ) ) ? mergeResultMap.get( keyLabel ) : new MergeResult() ;
						mergeResult.setLotNo( lotDto.getLotEntity().getLotId() ) ;
						mergeResult.setModuleName( moduleName ) ;
						mergeResult.setPath( relativePath ) ;
						mergeResult.setMergeStatus( mergeStatus ) ;
						mergeResult.setBinaryDiffStatus( diffStatus ) ;

						Set<String> applyNoSet = ( null != mergeResult.getApplyNoSet() ) ? mergeResult.getApplyNoSet() : new TreeSet<String>() ;
						applyNoSet.add( areqEntity.getAreqId() ) ;
						mergeResult.setApplyNoSet( applyNoSet ) ;

						Set<String> pjtIdSet = ( null != mergeResult.getPjtNoSet() ) ? mergeResult.getPjtNoSet() : new TreeSet<String>() ;
						pjtIdSet.add( areqEntity.getPjtId() ) ;
						mergeResult.setPjtNoSet( pjtIdSet ) ;

						mergeResultMap.put( keyLabel , mergeResult ) ;
					}
				}
			}
		}
		return mergeResultMap ;
	}

	private boolean isConflicted( Map<String,MergeResult> mergeResultMap ) throws Exception {

		for (MergeResult mergeResult : mergeResultMap.values()) {
			if( MergeStatus.Conflicted.equals( mergeResult.getMergeStatus() ) ) {
				return true;
			}
		}
		return false;
	}

}
