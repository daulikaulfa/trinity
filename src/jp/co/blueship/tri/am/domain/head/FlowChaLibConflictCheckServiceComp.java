package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * コンフリクトチェック 承認・承認完了画面の表示情報設定Class<br>
 * <br>
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class FlowChaLibConflictCheckServiceComp implements IDomain<FlowChaLibConflictCheckServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private List<IDomain<IGeneralServiceBean>> actions = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixActionByTestComplete = null;
	private FlowChaLibMergeEditSupport support = null;

	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setStatusMatrixActionByTestComplete( ActionStatusMatrixList action ) {
		this.statusMatrixActionByTestComplete = action;
	}

	public void setSupport( FlowChaLibMergeEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibConflictCheckServiceBean> execute(IServiceDto<FlowChaLibConflictCheckServiceBean> serviceDto ) {

		FlowChaLibConflictCheckServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_CONFLICTCHECK ) &&
				 !forwordID.equals( ChaLibScreenID.COMP_CONFLICTCHECK ) ){
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_CONFLICTCHECK )) {
				return serviceDto;
			}
			if ( !ScreenType.next.equals( paramBean.getScreenType() )) {
				return serviceDto;
			}

			String selectedLotVerTag = paramBean.getSelectedLotVersionTag();

			//処理に渡すための編集項目
			ILotDto lotDto = support.findLotDto( paramBean.getSelectedLotId() );
			paramBean.setCacheLotDto( lotDto );
			Date timestamp = TriDateUtils.convertTimestampToDate(paramBean.getSystemDate());
			paramBean.setLogService( new LogBusinessFlow(timestamp) );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto, support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			IMergeParamInfo[] mergeParamArrays = this.getMergeParamInfo( paramBean,selectedLotVerTag );

			paramBean.setSelectedLotBlDto( mergeParamArrays[0].getLotBlDto() );

			List<Object> paramList = new CopyOnWriteArrayList<Object>();
			paramList.add( paramBean );	//インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）
			paramList.add( lotDto );
			paramList.add( mergeParamArrays );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			//下層のアクションを実行する
			{
				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			LogHandler.fatal( log,e );
			throw new TriSystemException(AmMessageId.AM005088S,e );
		}
	}

	/**
	 * 指定したユーザが所属するグループを取得します。
	 *
	 * @param userId ユーザID
	 * @return 対象グループ情報EntityのList
	 */
	private List<IGrpEntity> groupsFrom(String userId) {
		return support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * 選択されたベースラインのタグ情報を起点として、それ以前のベースラインをマージ対象として抽出します。
	 *
	 * @param paramBean ドメイン層で利用するBean
	 * @param selectedLotVerTag 選択されたロットベースラインのタグ
	 * @return マージ対象のパラメタ情報
	 */
	private IMergeParamInfo[] getMergeParamInfo(
			FlowChaLibConflictCheckServiceBean paramBean,
			String selectedLotVerTag ) {

		String lotId = paramBean.getSelectedLotId();

		ILotBlEntity lotBlEntity = support.getLotBlEntityByVersionTagAtFirst(
				lotId,
				selectedLotVerTag );

		List<ILotBlDto> lotBlDtoList = support.getLotBlDtoByMergeCheckEnable(
				lotId,
				lotBlEntity.getLotChkinTimestamp(),
				TriSortOrder.Desc );

		if( paramBean.isStatusMatrixV3() ) {
			List<IPjtEntity> pjtEntities = support.getPjtEntities( lotBlDtoList, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView() );
			String[] pjtIds = FluentList.from( pjtEntities ).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);
	
			List<IAreqEntity> areqEntities = support.getAreqEntitiesFromPjtIds( pjtIds, null );
			String[] AreqIds = FluentList.from( areqEntities ).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]);
	
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setLotIds( lotId )
			.setPjtIds( pjtIds )
			.setAreqIds( AreqIds );
	
			if( StatusFlg.on.value().equals( sheet.getValue( AmDesignEntryKeyByMerge.checkStatusTestComplete ) ) ) {
				//テスト完了を運用前提とする
				statusDto.setActionList( statusMatrixActionByTestComplete );
	
				StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			} else {
				//テスト完了を運用前提としない
				statusDto.setActionList( statusMatrixAction );
	
				StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			}
		}

		List<IMergeParamInfo> paramList = new ArrayList<IMergeParamInfo>();
		for ( ILotBlDto dto : lotBlDtoList ) {
			IMergeParamInfo paramInfo = new MergeParamInfo();
			paramInfo.setLotBlDto(dto);

			paramInfo.setComment( paramBean.getMergeEditInputBean().getConflictCheckComment() );
			paramInfo.setUserId( paramBean.getUserId() );

			paramList.add( paramInfo );
		}

		return (IMergeParamInfo[])paramList.toArray( new IMergeParamInfo[ 0 ] );
	}


}
