package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;

/**
 * 変更管理・マージ結果一覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class MergeViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ロットNo */
	private String lotId = null ;
	/** モジュール名 */
	private String moduleName = null;
	/** ファイル名 */
	private String fileName = null;
	/** マージステータス */
	private String mergeStatus = null;
	/** Diffステータス */
	private String diffStatus = null;
	/** マージ可否メッセージ */
	private String mergeMessage = null;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewList = null ;
	/** 申請番号 */
	private List<String> applyNoList = null ;
	/** ボタン表示可否*/
	private boolean enabled = false ;

	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public List<String> getApplyNoList() {
		return applyNoList;
	}
	public void setApplyNoList(List<String> applyNoList) {
		this.applyNoList = applyNoList;
	}
	public List<PjtViewBean> getPjtViewList() {
		return pjtViewList;
	}
	public void setPjtViewList(List<PjtViewBean> pjtViewList) {
		this.pjtViewList = pjtViewList;
	}
	public String getDiffStatus() {
		return diffStatus;
	}
	public void setDiffStatus(String diffStatus) {
		this.diffStatus = diffStatus;
	}
	public String getMergeMessage() {
		return mergeMessage;
	}
	public void setMergeMessage(String mergeMessage) {
		this.mergeMessage = mergeMessage;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getMergeStatus() {
		return mergeStatus;
	}
	public void setMergeStatus(String mergeStatus) {
		this.mergeStatus = mergeStatus;
	}
	public String getModuleName() {
		return moduleName;
	}
	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}





}
