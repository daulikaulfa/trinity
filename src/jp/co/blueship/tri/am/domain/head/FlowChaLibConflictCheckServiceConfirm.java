package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmItemChkUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeViewSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * コンフリクトチェック 承認確認画面の表示情報設定Class<br>
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibConflictCheckServiceConfirm implements IDomain<FlowChaLibConflictCheckServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private ActionStatusMatrixList statusMatrixAction = null;
	private ActionStatusMatrixList statusMatrixActionByTestComplete = null;
	private FlowChaLibMergeViewSupport support = null;

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setStatusMatrixActionByTestComplete(ActionStatusMatrixList action) {
		this.statusMatrixActionByTestComplete = action;
	}

	public void setSupport(FlowChaLibMergeViewSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibConflictCheckServiceBean> execute(IServiceDto<FlowChaLibConflictCheckServiceBean> serviceDto) {

		FlowChaLibConflictCheckServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType = paramBean.getScreenType();

			if (!refererID.equals(ChaLibScreenID.CONFLICTCHECK_CONFIRM) && !forwordID.equals(ChaLibScreenID.CONFLICTCHECK_CONFIRM)) {

				setBaselineInfo( paramBean );

				return serviceDto;
			}

			if (refererID.equals(ChaLibScreenID.CONFLICTCHECK_BASELINE_DETAIL_VIEW)) {
				if (ScreenType.next.equals(screenType)) {
					if( paramBean.isStatusMatrixV3() ) {
						String closeVersionTag = paramBean.getSelectedLotVersionTag();
	
						List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( paramBean.getSelectedLotId(), closeVersionTag, TriSortOrder.Desc );
						List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
						ILotBlDto lotBlDto = lotBlDtoList.get(0);
						String lotId = lotBlDto.getLotBlEntity().getLotId();
	
						List<ILotBlDto> pastLotBlDtoList = this.support.getLotBlDtoByMergeCheckEnable(lotId,
								lotBlDto.getLotBlEntity().getLotChkinTimestamp(), TriSortOrder.Desc);
						List<IPjtEntity> pjtEntities = this.support.getPjtEntities(pastLotBlDtoList,
								AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibConflictCheckBaselineDetailView());
	
						String[] pjtIds = FluentList.from(pjtEntities).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);
						IAreqEntity[] areqEntities = this.support.getAreqEntitiesFromPjtIds((ISqlSort)null, pjtIds);
	
						String[] blIds = FluentList.from(pastLotBlDtoList).map(AmFluentFunctionUtils.toLotBlIdsFromLotBlDto).toArray(new String[0]);
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setLotIds( lotBlDto.getLotBlEntity().getLotId() )
						.setPjtIds( pjtIds )
						.setAreqIds( FluentList.from(areqEntities).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]) )
						.setLotBls( blIds );
	
						if (StatusFlg.on.value().equals(sheet.getValue(AmDesignEntryKeyByMerge.checkStatusTestComplete))) {
							// テスト完了を運用前提とする
							statusDto.setActionList( statusMatrixActionByTestComplete );
	
							StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
						} else {
							// テスト完了を運用前提としない
							statusDto.setActionList( statusMatrixAction );
	
							StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
						}
					}
				}
			}

			if (refererID.equals(ChaLibScreenID.CONFLICTCHECK_CONFIRM)) {

				if (ScreenType.next.equals(screenType)) {
					AmItemChkUtils.checkConflictCheck(paramBean);
					setBaselineInfo( paramBean );
				}
			}

			if (forwordID.equals(ChaLibScreenID.CONFLICTCHECK_CONFIRM)) {

				ILotDto lotDto = this.support.findLotDto( paramBean.getSelectedLotId() );

				// グループの存在チェック
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				setBaselineInfo( paramBean );

				// マージエラー警告 有無のチェック
				this.checkMergeCommitError(lotDto.getLotEntity(), paramBean);

			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005089S, e);
		}
	}

	/**
	 *
	 * @param pjtLotEntity
	 * @param paramBean
	 */

	private void checkMergeCommitError(ILotEntity pjtLotEntity, FlowChaLibConflictCheckServiceBean paramBean) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		// 全ビルド（全ロットで）の中でマージエラーが１件でもあれば警告を表示する
		ILotBlEntity[] lotBlEntityArray = this.support.getLotBlEntitiesByMergeError();
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		for (ILotBlEntity lotBlEntity : lotBlEntityArray) {
			messageList.add( AmMessageId.AM001113E );
			messageArgsList.add(new String[] { lotBlEntity.getLotId(), lotBlEntity.getLotBlId() });
		}

		if (null == paramBean.getInfoMessageIdList()) {
			paramBean.setInfoMessageIdList(messageList);
		} else {
			paramBean.getInfoMessageIdList().addAll(messageList);
		}
		if (null == paramBean.getInfoMessageArgsList()) {
			paramBean.setInfoMessageArgsList(messageArgsList);
		} else {
			paramBean.getInfoMessageArgsList().addAll(messageArgsList);
		}

		WarningCheckBean warningCheck = new WarningCheckBean();
		warningCheck.setExistWarning((0 != messageList.size()) ? true : false);
		warningCheck.setConfirmCheck(paramBean.getWarningCheckMergeCommitError().isConfirmCheck());
		// メッセージ生成
		List<String> commentListUnitCloseError = warningCheck.getCommentList();
		// [0]
		commentListUnitCloseError.add(ac.getMessage(paramBean.getLanguage(), AmMessageId.AM001116E));
		// [1]
		String logPath = TriStringUtils.convertPath(AmDesignBusinessRuleUtils.getWorkspaceLogPath(pjtLotEntity, paramBean.getFlowAction()));
		commentListUnitCloseError.add(ac.getMessage(paramBean.getLanguage(), AmMessageId.AM001114E, logPath));

		paramBean.setWarningCheckMergeCommitError(warningCheck);
	}

	/**
	 * ベースライン情報を取得し格納する
	 *
	 * @param paramBean
	 */

	private void setBaselineInfo(FlowChaLibConflictCheckServiceBean paramBean) {
		String selectedLotVerTag = paramBean.getSelectedLotVersionTag();

		List<ILotBlEntity> lotBlEntityList = this.support.getLotBlEntityByVersionTag( paramBean.getSelectedLotId(), selectedLotVerTag, TriSortOrder.Desc );
		List<ILotBlDto> lotBlDtoList = this.support.findLotBlDto( lotBlEntityList );
		List<LotBaselineViewGroupByVersionTag> baselineViewList = this.support.getLotBaselineViewGroupByVersionTag( lotBlDtoList );
		paramBean.setHeadBlViewBean( baselineViewList.get( 0 ) );
	}
}
