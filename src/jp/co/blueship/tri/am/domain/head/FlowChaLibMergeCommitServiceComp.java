package jp.co.blueship.tri.am.domain.head;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.IMergeParamInfo;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeParamInfo;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.NumberingServiceSupport;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * マージコミット 承認・承認完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowChaLibMergeCommitServiceComp implements IDomain<FlowChaLibMergeCommitServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private NumberingServiceSupport<IGeneralServiceBean> numbering;
	private FlowChaLibMergeEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions;
	private ActionStatusMatrixList statusMatrixAction;

	public void setNumbering(NumberingServiceSupport<IGeneralServiceBean> numbering) {
		this.numbering = numbering;
	}

	public void setSupport(FlowChaLibMergeEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowChaLibMergeCommitServiceBean> execute(IServiceDto<FlowChaLibMergeCommitServiceBean> serviceDto) {

		FlowChaLibMergeCommitServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if (!refererID.equals(ChaLibScreenID.COMP_MERGECOMMIT) && !forwordID.equals(ChaLibScreenID.COMP_MERGECOMMIT)) {
				return serviceDto;
			}

			if ( !forwordID.equals( ChaLibScreenID.COMP_MERGECOMMIT )) {
				return serviceDto;
			}
			if ( !ScreenType.next.equals( paramBean.getScreenType() )) {
				return serviceDto;
			}

			String selectedLotVerTag = paramBean.getSelectedLotVersionTag();

			{
				// マージ時のＨＥＡＤタグ番号を取得する
				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
				innerServiceDto.setServiceBean( paramBean );

				numbering.execute(innerServiceDto);
			}

			// 処理に渡すための編集項目
			ILotDto lotDto = support.findLotDto( paramBean.getSelectedLotId() );
			paramBean.setCacheLotDto( lotDto );
			Date timestamp = TriDateUtils.convertTimestampToDate(paramBean.getSystemDate());
			paramBean.setLogService( new LogBusinessFlow(timestamp) );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto, support.getUmFinderSupport().getGrpDao(), groupsFrom(paramBean.getUserId()));

			IMergeParamInfo[] mergeParamArrays = this.getMergeParamInfo(paramBean, selectedLotVerTag);

			paramBean.setSelectedLotBlDto( mergeParamArrays[0].getLotBlDto() );

			List<Object> paramList = new CopyOnWriteArrayList<Object>();
			paramList.add(paramBean); // インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）
			paramList.add(lotDto);
			paramList.add(paramBean.getMergeEditInputBean());
			paramList.add( mergeParamArrays );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			//下層のアクションを実行する
			{
				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005091S, e);
		}
	}

	/**
	 * 指定したユーザが所属するグループを取得します。
	 *
	 * @param userId ユーザID
	 * @return 対象グループ情報EntityのList
	 */
	private List<IGrpEntity> groupsFrom(String userId) {
		return support.getUmFinderSupport().findGroupByUserId(userId);
	}

	/**
	 * 選択されたベースラインのタグ情報を起点として、それ以前のベースラインをマージ対象として抽出します。
	 *
	 * @param paramBean ドメイン層で利用するBean
	 * @param selectedLotVerTag 選択されたロットベースラインのタグ
	 * @return マージ対象のパラメタ情報
	 */
	private IMergeParamInfo[] getMergeParamInfo(
			FlowChaLibMergeCommitServiceBean paramBean,
			String selectedLotVerTag) {

		String lotId = paramBean.getSelectedLotId();

		ILotBlEntity lotBlEntity = support.getLotBlEntityByVersionTagAtFirst(
				lotId,
				selectedLotVerTag );

		List<ILotBlDto> lotBlDtoList = support.getLotBlDtoByMergeEnable(
				lotId,
				lotBlEntity.getLotChkinTimestamp(),
				TriSortOrder.Desc );

		if( paramBean.isStatusMatrixV3() ) {
			List<IPjtEntity> pjtEntities = support.getPjtEntities(lotBlDtoList, AmDBSearchSortAddonUtils.getPjtSortFromDesignDefineByChaLibMargeBaselineDetailView());
			String[] pjtIds = FluentList.from( pjtEntities ).map(AmFluentFunctionUtils.toPjtIds).toArray(new String[0]);
	
			List<IAreqEntity> areqEntities = support.getAreqEntitiesFromPjtIds( pjtIds, null );
			String[] AreqIds = FluentList.from( areqEntities ).map(AmFluentFunctionUtils.toAreqIds).toArray(new String[0]);
	
	
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setPjtIds( pjtIds )
			.setAreqIds( AreqIds );
	
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		List<IMergeParamInfo> paramList = new ArrayList<IMergeParamInfo>();
		for ( ILotBlDto dto : lotBlDtoList ) {
			IMergeParamInfo paramInfo = new MergeParamInfo();
			paramInfo.setLotBlDto(dto);

			paramInfo.setComment(paramBean.getMergeEditInputBean().getMergeComment());
			paramInfo.setUserId(paramBean.getUserId());

			paramList.add(paramInfo);
		}

		return (IMergeParamInfo[]) paramList.toArray(new IMergeParamInfo[0]);
	}
}
