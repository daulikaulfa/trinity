package jp.co.blueship.tri.am.domain.head.beans.dto;

import java.io.Serializable;

/**
 * マージ操作用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class MergeEditInputBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** コンフリクトチェックコメント */
	private String conflictCheckComment = null;
	/** マージコメント */
	private String mergeComment = null;

	public String getConflictCheckComment() {
		return conflictCheckComment;
	}
	public void setConflictCheckComment(String conflictCheckComment) {
		this.conflictCheckComment = conflictCheckComment;
	}
	public String getMergeComment() {
		return mergeComment;
	}
	public void setMergeComment(String mergeComment) {
		this.mergeComment = mergeComment;
	}

}

