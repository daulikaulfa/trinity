package jp.co.blueship.tri.am.domain.head.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.ActionLotCheckInServiceBean;
import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.ScmUtils;
import jp.co.blueship.tri.fw.vcs.VcsCommitInfo;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;

/**
 * ロット原本チェックイン処理（SVN）で利用するサービスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Siti Hajar
 * 
 * @version V4.03
 * @author anh.nguyenduc
 */
public class ActionLotCheckInServiceForSvn implements IAmDomain<ActionLotCheckInServiceBean> {

	protected static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator();

	protected IContextAdapter ac;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	protected AmFinderSupport support;

	protected IFilesDiffer differ;
	protected ICopy copy;

	private IVcsService vcsService = null;
	private VcsServiceFactory factory;

	/**
	 *
	 * 内部クラス
	 * Add/Delete宣言が必要なファイル群とコミットが必要かどうかの情報を保持する。
	 *
	 */
	class ScmFileInfo {

		private Map<String,Boolean> needCommitMap = null ;	//key:モジュール名 false: コミット不要、true: コミット必要
		private List<String> fileList = null ;
		/**
		 * コミット処理時に処理されずにスキップされた資産数
		 */
		private Map<String, Integer> countBySkipResourceMap = null;

		/**
		 * コンストラクタ
		 *
		 */
		public ScmFileInfo() {
			fileList = new ArrayList<String>() ;
			needCommitMap = new LinkedHashMap<String,Boolean>() ;
			countBySkipResourceMap = new HashMap<String, Integer>();
		}

		/**
		 * コミット処理時に処理されずにスキップされた資産数を取得します。
		 * @param moduleName module name
		 * @return コミット処理時に処理されずにスキップされた資産数
		 */
		public int getCountBySkipResource( String moduleName ) {
			if ( countBySkipResourceMap.containsKey(moduleName) ) {
				return countBySkipResourceMap.get( moduleName );
			}

		    return 0;
		}
		/**
		 * コミット処理時に処理されずにスキップされた資産数を設定します。
		 * @param moduleName module name
		 * @param countBySkipResource コミット処理時に処理されずにスキップされた資産数
		 */
		public void addCountBySkipResource( String moduleName, int countBySkipResource ) {
			int count = 0;

			if ( countBySkipResourceMap.containsKey(moduleName) ) {
				count = countBySkipResourceMap.get( moduleName );
			}

			count += countBySkipResource;
			countBySkipResourceMap.put(moduleName, count);
		}
	}

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport(AmFinderSupport support) {
		this.support = support;
	}

	/**
	 * 差分チェックインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param diff 差分チェックインタフェース
	 */
	public final void setDiffer(IFilesDiffer diff) {
		this.differ = diff;
	}

	/**
	 * Copyインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param diff Copyインタフェース
	 */
	public final void setCopy(ICopy copy) {
		this.copy = copy;
	}

	/**
	 * VCSアクセスサービスクラスのファクトリがインスタンス生成時に自動的に設定されます。
	 * @param factory VCSアクセスサービスクラスのファクトリ
	 */
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}

	private IContextAdapter contextAdapter() {

		if (ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return ac;
	}

	@Override
	public IServiceDto<ActionLotCheckInServiceBean> execute(
			IServiceDto<ActionLotCheckInServiceBean> serviceDto) {

		ActionLotCheckInServiceBean paramBean = serviceDto.getServiceBean();

		if ( ! VcsCategory.SVN.equals(VcsCategory.value(paramBean.getTargetLotDto().getLotEntity().getVcsCtgCd()) )) {
			return serviceDto;
		}

		Map<String, String> moduleNameMap = this.getModuleNameMap(paramBean.getRegisterLotBlDto());

		moduleNameMap = this.checkIn(paramBean, moduleNameMap);
		this.updateAreqFileForRevision(paramBean);
		this.updateLotBlForRevision(paramBean, moduleNameMap);
		this.updateLotEntityForRevision( paramBean, moduleNameMap );

		return serviceDto;
	}

	/**
	 * Scmに資産の追加、更新、削除を行い、タグを打つ
	 *
	 * @param paramBean サービスDTO
	 * @param moduleNameMap 対象のモジュール名のMap
	 * @return 最新のモジュール名のマップ
	 */
	private Map<String, String> checkIn(ActionLotCheckInServiceBean paramBean, Map<String, String> moduleNameMap) {
		LogBusinessFlow lotService = paramBean.getLogService();

		final String  logString = " Check-in ";
		lotService.writeLogWithDateBegin(logString);

		try {
			ILotDto lotDto = paramBean.getTargetLotDto();
			ILotEntity lotEntity = lotDto.getLotEntity();
			List<IAreqDto> areqDtoList = paramBean.getTargetAreqDtoList();

			IPassMgtEntity passMgtEntity = this.support.findPassMgtEntityBySVN();
			String usrName = passMgtEntity.getUserId();
			String password = passMgtEntity.getPassword();

			vcsService = factory.createService(VcsCategory.value( lotEntity.getVcsCtgCd() ),usrName, password);

			//ロット資産のワーキングコピーが存在することをチェック
			this.chkExistsLotWorkpath( paramBean ) ;

			List<IVcsReposDto> reposDtoList =
					this.support.findVcsReposDtoFromLotMdlLnkEntities(lotDto.getIncludeMdlEntities( true ));
			if ( 0 == reposDtoList.size() ) {
				throw new TriSystemException( AmMessageId.AM004045F, AmTables.AM_VCS_MDL.name() );
			}

			// 全資産コンパイル対応
			if (0 < areqDtoList.size()) {
				Map<String, String> assetMap = new HashMap<String, String>();
				this.intensiveAssetApply( assetMap , areqDtoList ) ;

				// 追加対象ファイルの宣言
				ScmFileInfo scmFileInfoAdd = this.getFileListReturnAsset( paramBean );
				//削除対象ファイルの宣言
				ScmFileInfo scmFileInfoRemove = this.getFileListRemoveAsset( paramBean );
				//コミット
				this.commitAsset( paramBean, scmFileInfoAdd , scmFileInfoRemove , reposDtoList ) ;
			}

			//タグ
			Map<String, VcsCommitInfo> reposMap = new HashMap<String, VcsCommitInfo>();

			for ( IVcsReposDto reposDto: reposDtoList ) {
				IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
				String moduleName = reposDto.getVcsMdlEntity().getMdlNm();

				if ( ! moduleNameMap.containsKey(moduleName) ) {
					continue;
				}

				if ( ! reposMap.containsKey( reposEntity.getVcsReposId() ) ) {
					VcsCommitInfo vcsCommitInfo = this.attachVersionTag( paramBean, reposEntity ) ;
					reposMap.put( reposEntity.getVcsReposId(), vcsCommitInfo );
				}

				VcsCommitInfo vcsCommitInfo = reposMap.get( reposEntity.getVcsReposId() );

				if( null != vcsCommitInfo ) {
					moduleNameMap.put( moduleName, String.valueOf( vcsCommitInfo.getNewRevision() ) );
				} else {//前回タグ打ち後にエラーとなった場合、タグを打たずに抜ける。その場合、レコードにリビジョンはセット済みであるはず。
					;//何もしない
				}

			}

			return moduleNameMap;

		} catch (TriRuntimeException be) {
			lotService.writeLog(be.getStackTraceString());
			throw be;
		} catch (Exception e) {
			lotService.writeLog(ExceptionUtils.getStackTraceString(e));
			LogHandler.fatal(log, contextAdapter().getMessage(AmMessageId.AM005149S), e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005149S, e);
		} finally {
			lotService.writeLogWithDateEnd(logString);
		}

	}
	/**
	 * バージョンタグを付与する
	 * @param paramBean サービスDTO
	 * @param reposEntity タグ付け対象のモジュール名
	 */
	private VcsCommitInfo attachVersionTag(
			ActionLotCheckInServiceBean paramBean,
			IVcsReposEntity reposEntity ) throws Exception {
		LogBusinessFlow lotService = paramBean.getLogService();

		try {
			ILotDto lotDto = paramBean.getTargetLotDto();
			ILotEntity lotEntity = lotDto.getLotEntity();

			String reposUrl = reposEntity.getVcsReposUrl();

			String branchTag = lotEntity.getLotBranchTag();
			String lotVerTag = paramBean.getLotVerTag();
			String chkinCmt = paramBean.getLotChkinCmt();

			lotService.writeLog( TriLogMessage.LBM0017 ,SEP ,
					reposUrl + SEP , branchTag + SEP , lotVerTag + SEP , chkinCmt ) ;

			//既にタグが存在しないかチェック、存在する場合はスルー（前回クローズ時のタグ付与後にエラーが発生した場合など	）
			if( true != vcsService.chkExistsTag(setListVO(reposUrl,lotVerTag)) ) {
				lotService.writeLog( TriLogMessage.LBM0018 , reposUrl , lotVerTag ) ;
				VcsCommitInfo vcsCommitInfo = vcsService.tagToBranch(setCommitBranchVO(reposUrl, branchTag, lotVerTag, chkinCmt));
				return vcsCommitInfo ;
			} else {
				lotService.writeLog( TriLogMessage.LBM0019 ) ;
				return null ;
			}

		} catch ( TriRuntimeException be ) {
			lotService.writeLog( be.getStackTraceString() ) ;
			throw be;
		} catch ( Exception e ) {
			lotService.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			throw e ;
		}
	}

	/**
	 * ロット（ブランチ）資産のワーキングコピーが存在していることをチェックする。
	 * @param paramBean サービスDTO
	 * @throws ScmException
	 * @throws ScmSysException
	 */
	private void chkExistsLotWorkpath( ActionLotCheckInServiceBean paramBean ) throws ScmException , ScmSysException {
		LogBusinessFlow lotService = paramBean.getLogService();

		ILotDto lotDto = paramBean.getTargetLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true );
		if( TriStringUtils.isEmpty( mdlEntities ) ) {
			throw new TriSystemException( AmMessageId.AM004047F , lotEntity.getLotId() , lotEntity.getLotLatestVerTag() ) ;
		}
		String workpath = lotEntity.getLotMwPath() ;
		File fileMaster = new File( workpath ) ;
		if( true != fileMaster.exists() ) {
			throw new TriSystemException( AmMessageId.AM004048F , workpath ) ;
		}
		for( ILotMdlLnkEntity moduleEntity : mdlEntities ) {
			String pathModule = TriStringUtils.linkPathBySlash( workpath , moduleEntity.getMdlNm() ) ;
			File fileModule = new File( pathModule ) ;
			if( true != fileModule.exists() ) {
				throw new TriSystemException( AmMessageId.AM004049F , pathModule ) ;
			}
			//各モジュールがワーキングコピーとして正常であることを確認
			lotService.writeLog( TriLogMessage.LBM0011 , moduleEntity.getMdlNm() ) ;
			SVNStatus svnStatus = vcsService.statusSingle(setStatusSingleVO(workpath,moduleEntity.getMdlNm()));
			lotService.writeLog( vcsService.makeSvnStatusStr(svnStatus) ) ;
		}
	}

	/**
	 * 申請情報から、重複する資産をもつ古い申請情報から対象資産を外して、
	 * コミット対象の資産情報のみを集約し直す
	 * <br>
	 * <br>※論理集約しているため、このEntityを更新対象にしてはいけない。
	 * 利用方法が限られるため、Entityを複写してsrcのEntityを保証するようなクリティカルな処置は行わない。
	 * （以後の改修によるデグレを防止するため）
	 *
	 * @param areqDtoEntities 旧⇒新の順にソートされた申請情報
	 */
	private void intensiveAssetApply( Map<String, String> assetMap, List<IAreqDto> areqDtoEntities ) {
		IAreqDto[] areqDtoArrays = areqDtoEntities.toArray( new IAreqDto[0] );

		//新⇒旧の順に資産を評価する
		for ( int i = areqDtoArrays.length - 1; i >= 0; i-- ) {
			IAreqDto areqDto = areqDtoArrays[ i ];

			List<IAreqFileEntity> inAssetFileList = new ArrayList<IAreqFileEntity>();
			inAssetFileList.addAll( areqDto.getAreqFileEntities( AreqCtgCd.ReturningRequest ) );
			inAssetFileList.addAll( areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest ) );

			List<IAreqFileEntity> outAssetFileList = new ArrayList<IAreqFileEntity>();

			if ( null != inAssetFileList ) {

				for ( IAreqFileEntity fileEntity : inAssetFileList ) {
					if ( assetMap.containsKey( fileEntity.getFilePath() ) )
						continue;

					assetMap.put( fileEntity.getFilePath(), fileEntity.getFilePath() );
					outAssetFileList.add( fileEntity );
				}
			}

			areqDto.setAreqFileEntities( outAssetFileList );
		}
	}

	/**
	 * 追加・更新対象のScmファイル情報を取得します
	 * [1]追加・更新対象のファイルをロット原本資産にコピーする。
	 * [2]追加対象のファイルのステータスをチェックし、Add宣言の必要な
	 *
	 * @param paramBean サービスDTO
	 * @return 追加・更新対象のScmファイル情報のリスト
	 */
	private ScmFileInfo getFileListReturnAsset( ActionLotCheckInServiceBean paramBean ) throws Exception {
		LogBusinessFlow lotService = paramBean.getLogService();

		ILotDto lotDto = paramBean.getTargetLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();
		List<IAreqDto> areqDtoList = paramBean.getTargetAreqDtoList();

		ScmFileInfo scmFileInfo = new ScmFileInfo() ;

		File masterWorkDir = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );

		for ( IAreqDto dto : areqDtoList ) {
			IAreqEntity entity = dto.getAreqEntity();

			if ( !AmBusinessJudgUtils.isReturnApply( entity ) ) {
				continue;
			}

			File assetApplyNoPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath(lotDto, entity);

			IFileDiffResult[] results = this.differ.execute( assetApplyNoPath, masterWorkDir );

			for( IFileDiffResult result : results ) {
				if ( result.isDiff() ) {
					File srcFile = new File( assetApplyNoPath, result.getFilePath() );
					File dstFile = new File( masterWorkDir, result.getFilePath() );
					this.copy.copy( srcFile, dstFile );//追加・更新ファイルをロット原本に複写する
				}
			}
			
			String warterMark = sheet.getValue( AmDesignEntryKeyByScm.waterMarkStatusMget ) ;
			Map<String,SVNStatus> svnStatusMap = null ;
			if( ! TriStringUtils.isEmpty( warterMark ) &&
					Integer.parseInt( warterMark ) < results.length ) {

				Set<String> moduleNameSet = new LinkedHashSet<String>() ;
				for( IFileDiffResult result : results ) {
					moduleNameSet.add( StringUtils.substringBefore( TriStringUtils.convertPath( result.getFilePath() ) , "/" ) ) ;
				}
				Iterator<String> iter = moduleNameSet.iterator() ;
				while( iter.hasNext() ) {
					String moduleName = iter.next() ;
					if( null == svnStatusMap ) {
						svnStatusMap = new LinkedHashMap<String,SVNStatus>() ;//初期化
					}
					svnStatusMap.putAll( vcsService.statusAll(setStatusAllVO(masterWorkDir.getPath(), moduleName))) ;
				}
			}

			for ( IFileDiffResult result : results ) {
				//資産コピー：差分のあるものを対象とする（追加・更新） 以前のRPで失敗していた場合も、差分によりコピー実施を判断する
				String checkInFilePath = result.getFilePath();				//ステータス取得
				String moduleName = TriStringUtils.substringBefore( checkInFilePath ) ;
				SVNStatus svnStatus = null ;
				if( null == svnStatusMap ) {//１件ずつステータスを読み込む
					try {
						svnStatus = vcsService.statusSingle(setStatusSingleVO(masterWorkDir.getPath(),checkInFilePath));
					} catch( ScmException e ) {//新規フォルダ下の新規ファイルはSubversion管理下にないため例外が発生する：想定内
						svnStatus = null ;
					}
				} else {//まとめて読み込んだステータスMapから取得
					String absolutePath = TriStringUtils.linkPathBySlash( masterWorkDir.getCanonicalPath() , checkInFilePath ) ;
					svnStatus = svnStatusMap.get( absolutePath ) ;
				}

				if( null != svnStatus ) {
					SVNStatusType contentsStatus = svnStatus.getContentsStatus() ;
					if( SVNStatusType.STATUS_UNVERSIONED.equals( contentsStatus ) ||
						SVNStatusType.STATUS_NONE.equals( contentsStatus ) ) {//Add宣言未 
						scmFileInfo.fileList.add( checkInFilePath );
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_ADDED.equals( contentsStatus ) ) {//新規・Add宣言済み
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_NORMAL.equals( contentsStatus ) ) {//コミット済み
						lotService.writeLog( AmMessageId.AM002001W , TriStringUtils.convertPath( checkInFilePath ), ((null == contentsStatus)? "null": contentsStatus.toString()));
						scmFileInfo.addCountBySkipResource(moduleName, 1);

					} else if( SVNStatusType.STATUS_MODIFIED.equals( contentsStatus ) ) {//変更資産・変更あり
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_CONFLICTED.equals( contentsStatus ) ) {
						throw new TriSystemException( AmMessageId.AM004059F , svnStatus.getFile().getPath() ) ;
					} else {
						lotService.writeLog( AmMessageId.AM002002W , TriStringUtils.convertPath( checkInFilePath ), ((null == contentsStatus)? "null": contentsStatus.toString()));
						scmFileInfo.addCountBySkipResource(moduleName, 1);
					}
				} else {//新規ディレクトリ下の新規資産などはステータスが取得できない→新規資産とみなす
					scmFileInfo.fileList.add( checkInFilePath ) ;
					scmFileInfo.needCommitMap.put( moduleName , true ) ;
					continue ;
				}
			}
		}

		return scmFileInfo;
	}

	/**
	 * 削除対象のScmファイル情報を取得します
	 * @param paramBean サービスDTO
	 * @return 削除対象のScmファイル情報のリスト
	 */
	private ScmFileInfo getFileListRemoveAsset( ActionLotCheckInServiceBean paramBean ) throws IOException , ScmException {
		LogBusinessFlow lotService = paramBean.getLogService();

		ILotDto lotDto = paramBean.getTargetLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();
		List<IAreqDto> areqDtoList = paramBean.getTargetAreqDtoList();

		ScmFileInfo scmFileInfo = new ScmFileInfo() ;
		File masterWorkDir = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );

		// 内部返却フォルダ(バックアップ先)
		File returnAssetDir = AmDesignBusinessRuleUtils.getReturnAssetPath( lotDto );

		for ( IAreqDto dto : areqDtoList ) {
			IAreqEntity entity = dto.getAreqEntity();

			if ( !AmBusinessJudgUtils.isDeleteApply( entity ) ) {
				continue;
			}
			if ( null == dto.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) continue;

			for ( IAreqFileEntity assetFileEntity :dto.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {
				String filePath = assetFileEntity.getFilePath() ;
				// 先頭に/が入ると絶対パスとみなしてしまうため削除する。
				if( filePath.startsWith( "/" ) ) {
					filePath = filePath.substring( 1 ) ;
				}

				File srcFile		= new File( masterWorkDir,	filePath );
				File dstParentDir	= new File( returnAssetDir,	entity.getAreqId() );
				File dstFile		= new File(	dstParentDir,	filePath );

				//ロット原本から資産を削除・内部返却フォルダにコピーする。
				if ( srcFile.exists() ) {// 既に削除済みの場合は、スルー
					if ( !dstParentDir.exists() ) {
						dstParentDir.mkdirs();
					} else if ( dstParentDir.isFile() ) {
						TriFileUtils.delete( dstParentDir );
					}

					this.copy.copy( srcFile, dstFile );
					//FileUtil.delete( srcFile );//ここで削除はしない。Delete宣言と同時に行う 2011/11/28 by Tani.
				}

				//ステータス取得
				String moduleName = TriStringUtils.substringBefore( filePath ) ;
				SVNStatus svnStatus = vcsService.statusSingle(setStatusSingleVO(masterWorkDir.getPath(),filePath));
				if( null == svnStatus ) {//コミット済み
					lotService.writeLog( AmMessageId.AM002003W , filePath );
					scmFileInfo.addCountBySkipResource(moduleName, 1);
				} else {
					SVNStatusType contentsStatus = svnStatus.getContentsStatus() ;

					if( SVNStatusType.STATUS_NORMAL.equals( contentsStatus ) ||
						SVNStatusType.STATUS_REPLACED.equals( contentsStatus ) ||
						SVNStatusType.STATUS_ADDED.equals( contentsStatus ) ) {//削除前
						scmFileInfo.fileList.add( assetFileEntity.getFilePath() ) ;
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_DELETED.equals( contentsStatus ) ) {//Delete宣言済み
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_MISSING.equals( contentsStatus ) ) {//ファイルの削除だけが行われ、宣言はされていない
						scmFileInfo.needCommitMap.put( moduleName , true ) ;
					} else if( SVNStatusType.STATUS_CONFLICTED.equals( contentsStatus ) ) {
						throw new TriSystemException( AmMessageId.AM004059F , svnStatus.getFile().getPath() ) ;
					} else {
						lotService.writeLog( AmMessageId.AM002004W, filePath, ((null == contentsStatus)? "null": contentsStatus.toString())
								);
						scmFileInfo.addCountBySkipResource(moduleName, 1);
					}
				}
			}
		}

		return scmFileInfo ;
	}

	/**
	 * 返却・削除資産を原本に反映する
	 * @param paramBean サービスDTO
	 * @param scmFileInfoAdd 内部クラス Add宣言が必要なファイル群とコミットが必要かどうかの情報
	 * @param scmFileInfoRemove 内部クラス Delete宣言が必要なファイル群とコミットが必要かどうかの情報
	 * @param reposDtoList リポジトリ情報
	 */
	private void commitAsset( ActionLotCheckInServiceBean paramBean,
								ScmFileInfo scmFileInfoAdd,
								ScmFileInfo scmFileInfoRemove,
								List<IVcsReposDto> reposDtoList )
		throws ScmException, ScmSysException ,Exception {

		LogBusinessFlow lotService = paramBean.getLogService();

		ILotDto lotDto = paramBean.getTargetLotDto();
		ILotEntity lotEntity = lotDto.getLotEntity();

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotDto );

		try {
			for ( IVcsReposDto reposDto : reposDtoList ) {
				IVcsReposEntity reposEntity = reposDto.getVcsReposEntity();
				IVcsMdlEntity mdlEntity = reposDto.getVcsMdlEntity();

				//commitFileListには各種モジュール混在のファイルリストが入っているので、
				//SCMに渡すのは当該モジュールのもののみに絞り込む

				List<String> addFileModuleList = ScmUtils.getFileListInModule( scmFileInfoAdd.fileList , mdlEntity );
				List<String> removeFileModuleList = ScmUtils.getFileListInModule( scmFileInfoRemove.fileList , mdlEntity );

				// リポジトリ単位でコマンド実行
				String moduleName = mdlEntity.getMdlNm();

				lotService.writeLog( TriLogMessage.LBM0012 , SEP , reposEntity.getVcsReposUrl() + SEP
						, moduleName + SEP , lotEntity.getLotBranchTag() + SEP ) ;

				String workPath = lotEntity.getLotMwPath() ;

				//新規資産のAdd宣言
				if( 0 != addFileModuleList.size() ) {//モジュールに該当する返却・追加資産なし
					for( String addFileModule : addFileModuleList ) {
						lotService.writeLog( TriLogMessage.LBM0013 , addFileModule ) ;
					}
					vcsService.add(setAddVO(workPath,moduleName,addFileModuleList.toArray( new String[ 0 ] )));
				}
				lotService.writeLog( "" ) ;
				//削除資産のDelete宣言
				if( 0 != removeFileModuleList.size() ) {//モジュールに該当する削除資産なし
					for( String removeFileModule : removeFileModuleList ) {
						lotService.writeLog( TriLogMessage.LBM0014 , removeFileModule ) ;
					}
					vcsService.remove(setDeleteVO(workPath,moduleName,removeFileModuleList.toArray( new String[ 0 ] )));
				}

				//コミット（モジュールごとコミット、本当はモジュールをまとめて１度にコミットしたいが）

				Boolean needCommitAdd = scmFileInfoAdd.needCommitMap.get( moduleName ) ;
				Boolean needCommitRemove = scmFileInfoRemove.needCommitMap.get( moduleName ) ;
				if( ( null != needCommitAdd && true == needCommitAdd.booleanValue() ) ||
						null != needCommitRemove && true == needCommitRemove.booleanValue() ) {
					lotService.writeLog( TriLogMessage.LBM0015 , workPath , moduleName ) ;
					vcsService.commit(setCommitVO(workPath, new String[]{moduleName}, paramBean.getLotChkinCmt()));
				} else {
					lotService.writeLog( TriLogMessage.LBM0016 , moduleName ) ;
				}

				int count = paramBean.getCountBySkipResource();
				count += scmFileInfoAdd.getCountBySkipResource( moduleName );
				count += scmFileInfoRemove.getCountBySkipResource( moduleName );
				paramBean.setCountBySkipResource( count );
			}
		} catch ( TriRuntimeException be ) {
			lotService.writeLog( be.getStackTraceString() ) ;
			throw be;
		} catch ( Exception e ) {
			lotService.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			throw e ;
		}
	}

	/**
	 * 変更のあったモジュール名を取得する
	 *
	 * @param lotBlDtoList ロットベースラインDTOのList
	 * @return モジュール名のマップ
	 */
	protected Map<String, String> getModuleNameMap(List<ILotBlDto> lotBlDtoList) {

		Map<String, String> moduleNameMap = new LinkedHashMap<String, String>();

		for (ILotBlDto dto : lotBlDtoList) {
			for ( ILotBlMdlLnkEntity entity : dto.getCheckInMdlEntities(true) ) {
				moduleNameMap.put( entity.getMdlNm(), entity.getLotChkinRev() );
			}
		}

		return moduleNameMap;
	}

	/**
	 * 申請情報に最新のリビジョンを設定する
	 *
	 * @param paramBean サービスDTO
	 */
	private void updateAreqFileForRevision(ActionLotCheckInServiceBean paramBean) {
		LogBusinessFlow lotService = paramBean.getLogService();

		final String  logString = " Update " + AmTables.AM_AREQ_FILE.name() + " for File revision ";
		lotService.writeLogWithDateBegin(logString);

		try {
			ILotDto lotDto = paramBean.getTargetLotDto();
			List<IAreqDto> areqDtoList = paramBean.getTargetAreqDtoList();

			List<IAreqFileEntity> areqFileEntities = new ArrayList<IAreqFileEntity>();

			for (IAreqDto areqDto : areqDtoList) {
				this.setAssetFileEntity( vcsService , lotDto, areqDto );

				List<IAreqFileEntity> innerAreqFileEntities = new ArrayList<IAreqFileEntity>();
				innerAreqFileEntities.addAll(areqDto.getAreqFileEntities(AreqCtgCd.RemovalRequest));
				innerAreqFileEntities.addAll(areqDto.getAreqFileEntities(AreqCtgCd.ReturningRequest));

				innerAreqFileEntities = FluentList.from(innerAreqFileEntities).map(
						new TriFunction<IAreqFileEntity, IAreqFileEntity>() {
							public IAreqFileEntity apply(IAreqFileEntity input) {
								IAreqFileEntity fileEntity = new AreqFileEntity();
								fileEntity.setAreqId(input.getAreqId());
								fileEntity.setAreqCtgCd(input.getAreqCtgCd());
								fileEntity.setAreqFileSeqNo(input.getAreqFileSeqNo());
								fileEntity.setFileRev(input.getFileRev());

								return fileEntity;
							}
						}).asList();

				areqFileEntities.addAll(innerAreqFileEntities);
			}

			this.support.getAreqFileDao().update(areqFileEntities);

		} catch (TriRuntimeException be) {
			lotService.writeLog(be.getStackTraceString());
			throw be;
		} catch (Exception e) {
			lotService.writeLog(ExceptionUtils.getStackTraceString(e));
			LogHandler.fatal(log, contextAdapter().getMessage(AmMessageId.AM005149S), e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005149S, e);
		} finally {
			lotService.writeLogWithDateEnd(logString);
		}
	}

	/**
	 * 申請レコードに、申請ファイル情報を設定します。
	 *
	 * @param paramList
	 * @param logMasterBeans
	 * @throws ScmSysException
	 * @throws ScmException
	 */
	public final void setAssetFileEntity( IVcsService vcsService ,
			ILotDto lotDto,
			IAreqDto entity ) throws IOException , ScmException, ScmSysException {

		Map<String, SVNStatus> statusMap = new HashMap<String, SVNStatus>();

		List<IAreqFileEntity> assetFileEntity = null ;
		if ( BmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() ) ) {
			assetFileEntity = entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ;
		} else 	if ( BmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() ) ) {
			assetFileEntity = entity.getAreqFileEntities(AreqCtgCd.ReturningRequest) ;
		}

		if ( TriCollectionUtils.isEmpty(assetFileEntity) ) return;


		this.getStatus( vcsService , lotDto, assetFileEntity , statusMap);
		for ( IAreqFileEntity fileEntity : assetFileEntity ) {
			setAssetFileEntityRevision( lotDto , fileEntity, statusMap );
		}
	}
	/**
	 * ステータスを取得する。
	 * @param svnUtil
	 * @param paramList
	 * @param assetFileEntityArray
	 * @param statusMap
	 * @throws ScmException
	 * @throws ScmSysException
	 */
	private final void getStatus( IVcsService vcsService , ILotDto lotDto,
			List<IAreqFileEntity> assetFileEntityArray ,
			Map<String, SVNStatus> statusMap ) throws IOException , ScmException, ScmSysException {

		ILotEntity lotEntity			= lotDto.getLotEntity();

		String workPath = lotEntity.getLotMwPath() ;

		String warterMark = sheet.getValue( AmDesignEntryKeyByScm.waterMarkStatusMget ) ;

		if( ! TriStringUtils.isEmpty( warterMark ) &&
				Integer.parseInt( warterMark ) < assetFileEntityArray.size() ) {//モジュール一括で取得：少量資産の場合は遅くなる

			Set<String> moduleNameSet = new LinkedHashSet<String>() ;
			for( IAreqFileEntity assetFileEntity : assetFileEntityArray ) {
				moduleNameSet.add( StringUtils.substringBefore( TriStringUtils.convertPath( assetFileEntity.getFilePath() ) , "/" ) ) ;
			}
			StatusVO svo = new StatusVO();
			svo.setPathDest(workPath);
			Iterator<String> iter = moduleNameSet.iterator() ;
			while( iter.hasNext() ) {
				svo.setModuleName(iter.next());
				statusMap.putAll( vcsService.statusAll(svo)) ;
			}

		} else {//資産１件ごとに取得：大量資産貸出時は一括より遅くなる
			for ( IAreqFileEntity assetFileEntity : assetFileEntityArray ) {
				String filePath = assetFileEntity.getFilePath() ;
				try {
					SVNStatus status = vcsService.statusSingle(setStatusVO(workPath, filePath));

					if ( null != status ) {
						statusMap.put( TriStringUtils.linkPathBySlash( workPath , filePath ), status);
					}
				} catch ( ScmException e ) {
					throw new ScmSysException ( AmMessageId.AM005157S , e );
				}
			}
		}
	}

	/**
	 * 申請レコードに、申請ファイル情報を設定します。
	 *
	 * @param fileEntity
	 * @param logMasterBeans
	 */
	private final void setAssetFileEntityRevision( ILotDto lotDto,
			IAreqFileEntity fileEntity,
			Map<String, SVNStatus> statusMap ) {

		ILotEntity lotEntity			= lotDto.getLotEntity();
		String workPath = lotEntity.getLotMwPath() ;

		fileEntity.setFilePath( TriStringUtils.convertPath(fileEntity.getFilePath()) );

		boolean errFlg = false ;

		try {
			String absolutePath = TriStringUtils.linkPathBySlash( workPath , fileEntity.getFilePath() ) ;
			if ( statusMap.containsKey( absolutePath ) ) {
				SVNStatus status = statusMap.get( absolutePath ) ;
				SVNStatusType statusType = status.getContentsStatus() ;
				if( SVNStatusType.MISSING.equals( statusType ) ) {
					fileEntity.setFileRev( "" ) ;//削除資産
					return ;
				}
				long revisionNo = status.getCommittedRevision().getNumber() ;
				if( -1 == revisionNo ) {//リビジョン未管理資産が混入
					errFlg = true ;
				} else {
					fileEntity.setFileRev( String.valueOf( revisionNo ) ) ;
				}
			} else {
				fileEntity.setFileRev( "" ) ;//削除資産
				return ;
			}
		} finally {
			if( errFlg ) {
				throw new TriSystemException(AmMessageId.AM004057F , fileEntity.getFilePath() );
			}
		}
	}

	/**
	 * ロットベースラインのタグ情報を更新します。
	 *
	 * @param paramBean サービスDTO
	 * @param moduleNameMap 対象のモジュール名のMap
	 */
	private void updateLotBlForRevision( ActionLotCheckInServiceBean paramBean, Map<String, String> moduleNameMap ) {
		LogBusinessFlow lotService = paramBean.getLogService();

		final String  logString = " Update " + AmTables.AM_LOT_BL.name() + " & " + AmTables.AM_LOT_BL_MDL_LNK.name() + " for Version Tag ";
		lotService.writeLogWithDateBegin( logString );

		try {

			List<ILotBlEntity> lotBlEntities = new ArrayList<ILotBlEntity>();
			List<ILotBlMdlLnkEntity> mdlEntities = new ArrayList<ILotBlMdlLnkEntity>();

			for (ILotBlDto lotBlDto : paramBean.getRegisterLotBlDto()) {
				ILotBlEntity lotBlEntity = lotBlDto.getLotBlEntity();
				ILotBlEntity updLotBlEntity = new LotBlEntity();

				for ( ILotBlMdlLnkEntity mdlEntity: lotBlDto.getCheckInMdlEntities(true) ) {
					if ( ! moduleNameMap.containsKey( mdlEntity.getMdlNm() ) ) {
						continue;
					}

					ILotBlMdlLnkEntity updMdlEntity = new LotBlMdlLnkEntity();
					updMdlEntity.setLotBlId( mdlEntity.getLotBlId() );
					updMdlEntity.setMdlNm( mdlEntity.getMdlNm() );
					updMdlEntity.setMdlVerTag( paramBean.getLotVerTag() );
					updMdlEntity.setLotChkinRev( moduleNameMap.get( mdlEntity.getMdlNm() ) );

					mdlEntities.add( updMdlEntity );
				}

				updLotBlEntity.setLotBlId( lotBlEntity.getLotBlId() );
				updLotBlEntity.setLotVerTag( paramBean.getLotVerTag() );
				updLotBlEntity.setLotBlTag( paramBean.getLotBlTag() );

				updLotBlEntity.setLotChkinUserId( paramBean.getUserId() );
				updLotBlEntity.setLotChkinUserNm( paramBean.getUserName() );
				updLotBlEntity.setLotChkinTimestamp( paramBean.getLotChkinTimestamp() );
				updLotBlEntity.setLotChkinCmt( paramBean.getLotChkinCmt() );

				updLotBlEntity.setStsId( AmLotBlStatusId.CommittedToLot.getStatusId() );

				lotBlEntities.add( updLotBlEntity );
			}

			this.support.getLotBlDao().update( lotBlEntities );
			this.support.getLotBlMdlLnkDao().update( mdlEntities );

		} finally {
			lotService.writeLogWithDateEnd( logString );
		}
	}

	/**
	 * ロット情報を更新する。
	 *
	 * @param paramBean サービスDTO
	 * @param moduleNameMap 対象のモジュール名のMap
	 */
	protected void updateLotEntityForRevision( ActionLotCheckInServiceBean paramBean, Map<String, String> moduleNameMap ) {
		LogBusinessFlow lotService = paramBean.getLogService();

		final String  logString = " Update " + AmTables.AM_LOT.name() + " & " + AmTables.AM_LOT_MDL_LNK.name() + " for Version Tag ";
		lotService.writeLogWithDateBegin( logString );

		try {

			List<ILotMdlLnkEntity> mdlEntities = new ArrayList<ILotMdlLnkEntity>();

			ILotEntity lotEntity = paramBean.getTargetLotDto().getLotEntity();
			ILotEntity updLotEntity = new LotEntity();

			updLotEntity.setLotId( lotEntity.getLotId() );
			updLotEntity.setLotLatestBlTag(paramBean.getLotBlTag());
			updLotEntity.setLotLatestVerTag(paramBean.getLotVerTag());

			for (ILotMdlLnkEntity mdlEntity : paramBean.getTargetLotDto().getIncludeMdlEntities(true)) {
				if ( ! moduleNameMap.containsKey( mdlEntity.getMdlNm() ) ) {
					continue;
				}

				ILotMdlLnkEntity updMdlEntity = new LotMdlLnkEntity();
				updMdlEntity.setLotId( mdlEntity.getLotId() );
				updMdlEntity.setMdlNm( mdlEntity.getMdlNm() );
				updMdlEntity.setLotChkinRev( moduleNameMap.get( mdlEntity.getMdlNm() ) );

				updMdlEntity.setMdlVerTag( paramBean.getLotVerTag() );

				mdlEntities.add( updMdlEntity );
			}

			this.support.getLotDao().update(lotEntity);
			this.support.getLotMdlLnkDao().update(mdlEntities);

		} finally {
			lotService.writeLogWithDateEnd( logString );
		}
	}
	/**
	 * パラメータをStatusVOにセットします。(statusSingle用)
	 * @param path
	 * @param object
	 * @return
	 */
	private StatusVO setStatusSingleVO(String path,String object){
		StatusVO vo = new StatusVO();
		vo.setPathDest(path);
		vo.setObjectPath(object);
		return vo;
	}
	/**
	 * パラメータをStatusVOにセットします。(statusAll用)
	 * @param path
	 * @param module
	 * @return vo
	 */
	private StatusVO setStatusAllVO(String path,String module){
		StatusVO vo = new StatusVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		return vo;
	}

	/**
	 * パラメータをAddVOにセットします。
	 * @param path
	 * @param module
	 * @param pathArray
	 * @return vo
	 */
	private AddVO setAddVO(String path,String module, String[] pathArray){
		AddVO vo = new AddVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		vo.setFilePathArray(pathArray);
		return vo;
	}

	/**
	 * パラメータをDeleteVOにセットします。
	 * @param path
	 * @param module
	 * @param pathArray
	 * @return vo
	 */
	private DeleteVO setDeleteVO(String path,String module, String[] pathArray){
		DeleteVO vo = new DeleteVO();
		vo.setPathDest(path);
		vo.setModuleName(module);
		vo.setFilePathArray(pathArray);
		return vo;
	}

	/**
	 * パラメータをCommitVOにセットします。
	 * @param path
	 * @param module
	 * @param comment
	 * @return vo
	 */
	private CommitVO setCommitVO(String path,String[] module, String comment){
		CommitVO vo = new CommitVO();
		vo.setPathDest(path);
		vo.setModuleNameArray(module);
		vo.setCommitComment(comment);
		return vo;
	}

	/**
	 * パラメータをCommitVOにセットします。(Branch用)
	 * @param repository
	 * @param branchTag
	 * @param versionTag
	 * @param comment
	 * @return vo
	 */
	private CommitVO setCommitBranchVO(String repository,String branchTag,String versionTag,String comment){
		CommitVO vo = new CommitVO();
		vo.setRepository(repository);
		vo.setLabelBranch(branchTag);
		vo.setLabelTag(versionTag);
		vo.setCommitComment(comment);
		return vo;
	}
	/**
	 * パラメータをListSubDirVOにセットします。
	 * @param repository
	 * @param versionTag
	 * @return vo
	 */
	private ListSubDirVO setListVO(String repository,String versionTag){
		ListSubDirVO vo = new ListSubDirVO();
		vo.setRepository(repository);
		vo.setVersionTag(versionTag);
		return vo;
	}

	private static StatusVO setStatusVO(String path, String objectPath){

		StatusVO vo = new StatusVO();
		vo.setPathDest(path);
		vo.setObjectPath(objectPath);
		return vo;
	}

}
