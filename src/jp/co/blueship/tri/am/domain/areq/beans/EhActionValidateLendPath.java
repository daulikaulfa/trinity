package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 貸出フォルダの妥当性チェックを行います。
 * <br>貸出申請済で、格納パスが存在しない場合に、エラーとなります。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateLendPath extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File file = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );

		if ( file.isDirectory() ){
			return serviceDto;
		}
		throw new TriSystemException( AmMessageId.AM004060F , file.getName() );
	}
}
