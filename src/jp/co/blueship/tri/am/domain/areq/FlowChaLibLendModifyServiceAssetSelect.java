package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;



/**
 * FlowChaLibLendModifyイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出編集資産選択画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendModifyServiceAssetSelect implements IDomain<FlowChaLibLendModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute(IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto) {

		FlowChaLibLendModifyServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LEND_ENTRY_ASSET_SELECT ) &&
					!forward.equals( ChaLibScreenID.LEND_ENTRY_ASSET_SELECT )) {
				return serviceDto;
			}

			if ( forward.equals(ChaLibScreenID.LEND_ENTRY_ASSET_SELECT) ) {
				executeForward(paramBean);
			}

			if ( referer.equals(ChaLibScreenID.LEND_ENTRY_ASSET_SELECT) ) {
				if( ! TriStringUtils.isEmpty( paramBean.getInAssetSelectBean().getInAssetResourcePath() ) ||
					! TriStringUtils.isEmpty( paramBean.getInAssetSelectBean().getInDirectory() ) ) {
					setAssetSelectList(paramBean);
				}

				if ( forward.equals(ChaLibScreenID.LEND_ENTRY_ASSET_SELECT) ) {
					executeEquals(paramBean);
				}

				executeReferer(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 貸出資産選択画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void executeForward(FlowChaLibLendModifyServiceBean paramBean) {
		ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

		IPjtEntity pjtEntity = support.findPjtEntity( inBean.getInPrjNo() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );

		List<File> files = new ArrayList<File>();
		for ( ILotMdlLnkEntity module : lotDto.getIncludeMdlEntities(true) ) {
			File modulePath = new File( masterPath, module.getMdlNm() );
			if ( modulePath.exists() )
				files.add( modulePath );
		}

		ChaLibEntryAssetResourceInfoBean assetResourceBean = AmCnvEntityToDtoUtils.convertFiles2AssetResourceInfoBean( files );
		//カレントパスのセット
		assetResourceBean.setPathInfoViewBeanList( support.makePathInfoViewBeanList( masterPath.getPath() , masterPath.getPath() ) ) ;
		support.checkAssetLock( paramBean.getApplyNo(), lotDto, assetResourceBean );

		paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;
		paramBean.setAssetSelectViewBean( assetResourceBean );

		//「次へ」で次画面に進む場合
		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
			if ( ! TriStringUtils.isEmpty( paramBean.getInAssetSelectBean() ) ) {
				ChaLibInputLendEntryAssetSelectBean inputBean = paramBean.getInAssetSelectBean();

				inputBean.setInDirectory( TriStringUtils.convertPath( masterPath ) );
			}

			if ( TriStringUtils.isEmpty( paramBean.getInAssetSelectBean() ) ||
					TriStringUtils.isEmpty( paramBean.getInAssetSelectBean().getInAssetResourcePath() ) ) {
				IAreqEntity assetApplyEntity = support.findAreqEntity( paramBean.getApplyNo() );

				ChaLibInputLendEntryAssetSelectBean inputBean = new ChaLibInputLendEntryAssetSelectBean();

				inputBean.setInAssetResourcePath( this.support.getSelectedAssetResourcePath( lotDto, assetApplyEntity ) );
				inputBean.setInDirectory( TriStringUtils.convertPath( masterPath ) );
				paramBean.setInAssetSelectBean( inputBean );
			}
		}

		// ２重貸出チェック
		paramBean.setDuplicationCheck( ! lotDto.getLotEntity().getIsAssetDup().parseBoolean() );

		//変更管理番号からリンクする変更要因番号をセットする
		paramBean.getInBaseInfoBean().setInChangeCauseNo( pjtEntity.getChgFactorNo() ) ;
	}

	/**
	 * 貸出資産選択申請画面からの遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void executeReferer(FlowChaLibLendModifyServiceBean paramBean) {
		ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

		//「次へ」で次画面に進む場合
		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
			IPjtEntity pjtEntity = support.findPjtEntity( inBean.getInPrjNo() );
			ILotDto lotDto = support.findLotDto( pjtEntity.getLotId() );

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );

			ChaLibEntryAssetResourceInfoBean assetResourceBean = new ChaLibEntryAssetResourceInfoBean();
			//カレントパスのセット
			assetResourceBean.setPathInfoViewBeanList( support.makePathInfoViewBeanList( masterPath.getPath() , masterPath.getPath() ) ) ;

			paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;
			paramBean.setAssetSelectViewBean( assetResourceBean );

			assetResourceBean.setMasterPath( TriStringUtils.convertPath( masterPath ) );

			//下層のアクションを実行する
			{
				List<Object> paramList = new ArrayList<Object>();

				IEhLendParamInfo param = new EhLendParamInfo();
				support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);
				support.setLendParamInfo(paramBean.getInAssetSelectBean(), masterPath, param);
				paramList.add( param );
				paramList.add( lotDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}
			}
		}

	}

	/**
	 * 同画面からの遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void executeEquals(FlowChaLibLendModifyServiceBean paramBean) {

		if ( TriStringUtils.isEmpty( paramBean.getInAssetSelectBean().getInAssetResourcePath() ) ) {
			return;
		}
		// チェックフラグの反映
		for ( ChaLibAssetResourceViewBean viewBean : paramBean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			if ( viewBean.isFile() ) {
				boolean checked = false;
				for ( String selectAsset : paramBean.getInAssetSelectBean().getInAssetResourcePath() ) {
					if ( viewBean.getResourcePath().equals(selectAsset) ) {
						checked = true;
						break;
					}
				}
				viewBean.setChecked(checked);
			}
		}
	}

	/**
	 * ディレクトリを選択して、上階層／又は下階層に移動、又は資産の選択を行います。
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void setAssetSelectList(FlowChaLibLendModifyServiceBean paramBean) {
		ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

		IPjtEntity pjtEntity = support.findPjtEntity( inBean.getInPrjNo() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		String masterPath = TriStringUtils.convertPath( AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() ) );
		String selectedDir = paramBean.getInAssetSelectBean().getInDirectory();
		String ladderSelectedDir = selectedDir.substring( 0 , selectedDir.lastIndexOf( "/" ));

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "masterPath:=" + masterPath);
			LogHandler.debug( log , "selectedDir:=" + selectedDir);
			LogHandler.debug( log , "ladderSelectedDir:=" + ladderSelectedDir);
		}

		boolean isTopPath = false;

		if( selectedDir.equals( masterPath ) ) {
			isTopPath = true;
		}

		List<File> files = new ArrayList<File>();
		File moduleFile = new File( selectedDir );
		List<File> moduleFiles = BusinessFileUtils.getListFiles( moduleFile, AmDesignBusinessRuleUtils.getNeglectFileFilter(lotDto) );
		if( ! isTopPath ){
			files.add( new File( ladderSelectedDir) );
		}
		files.addAll( moduleFiles );

		ChaLibEntryAssetResourceInfoBean assetResourceBean = AmCnvEntityToDtoUtils.convertFiles2AssetResourceInfoBean( files );

		//カレントパスのセット
		assetResourceBean.setPathInfoViewBeanList( support.makePathInfoViewBeanList( masterPath , selectedDir ) ) ;

		if( ! isTopPath ){
			assetResourceBean.getAssetResourceViewBeanList().get(0).setResourceName("../");
		}
		support.checkAssetLock( paramBean.getApplyNo(), lotDto, assetResourceBean );

		paramBean.setAssetSelectViewBean( assetResourceBean );
	}
}
