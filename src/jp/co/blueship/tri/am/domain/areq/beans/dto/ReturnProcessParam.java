package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;


public class ReturnProcessParam extends CommonParamInfo implements IReturnProcessParam {

	private static final long serialVersionUID = 1L;

	private String applyNo = null;
	private String user = null;
	private String procCtgCd = null;
	private String messageId = null;
	private String message = null;
	private Timestamp processDate = null;
	private boolean duplicationCheck;
	private String returnAssetAppendFile ;

	public final String getApplyNo() {
		return applyNo;
	}
	public final void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getUser() {
		return user;
	}
	public void setUser( String user ) {
		this.user = user;
	}
	public final String getProcCtgCd() {
		return procCtgCd;
	}
	public final void setProcCtgCd(String procCtgCd) {
		this.procCtgCd = procCtgCd;
	}
	public final String getMessageId() {
		return messageId;
	}
	public final void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	public final String getMessage() {
		return message;
	}
	public final void setMessage(String message) {
		this.message = message;
	}
	public Timestamp getProcessDate() {
		return processDate;
	}
	public void setProcessDate(Timestamp processDate) {
		this.processDate = processDate;
	}

	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean value ) {
		this.duplicationCheck = value;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}
	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}
}
