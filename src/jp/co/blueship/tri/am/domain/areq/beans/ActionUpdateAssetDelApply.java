package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;



/**
 *
 * 削除資産情報の更新処理を行います。
 * 	V2変更管理移植に伴い下記処理を統合しました
 * 		ActionChangeDelApplyApprove
 * 		ActionDeleteDelApplyApprove
 * 		ActionDeleteDelApplyCommission
 *
 * @version V3L10.02
 * @author Eguchi Yukihiro
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class ActionUpdateAssetDelApply extends ActionPojoAbstract<IGeneralServiceBean> {
	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractDeleteApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		support.getAreqDao().update( areqDto.getAreqEntity() );
		insertAreqFileDao( areqDto );
		insertAreqBinaryFileDao(areqDto);

		IDelApplyParamInfo param = AmExtractMessageAddonUtils.getDelApplyParamInfo(paramList);
		if ( null != param ) {
			support.getUmFinderSupport().updateCtgLnk(param.getCtgId(), AmTables.AM_AREQ, areqDto.getAreqEntity().getAreqId());
			support.getUmFinderSupport().updateMstoneLnk(param.getMstoneId(), AmTables.AM_AREQ, areqDto.getAreqEntity().getAreqId());
		}

		if ( DesignSheetUtils.isRecord() ) {

			IAreqDto dto = support.findAreqDto(areqDto.getAreqEntity().getAreqId(), (StatusFlg)null);

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( dto.getAreqEntity().getDelStsId().parseBoolean()? UmActStatusId.Remove.getStatusId() :
																						UmActStatusId.EditRequest.getStatusId());

			support.getAreqHistDao().insert( histEntity , dto );
		}
		return serviceDto;
	}

	/**
	 * 削除資産申請ファイルの更新を行います。
	 * 更新を行う際には、一度deleteしてからinsertを行います。
	 * @param dto
	 */
	private void insertAreqFileDao( IAreqDto dto ) {

		AreqFileCondition condition = new AreqFileCondition();
		condition.setAreqId( dto.getAreqEntity().getAreqId() );
		condition.setAreqCtgCd( AreqCtgCd.RemovalRequest.value() );
		support.getAreqFileDao().delete(condition.getCondition() );
		support.getAreqFileDao().insert( dto.getAreqFileEntities() );

	}
	/**
	 * 削除資産申請バイナリファイルの更新を行います。
	 * 更新を行う際には、一度deleteしてからinsertを行います。
	 * @param dto
	 */
	private void insertAreqBinaryFileDao( IAreqDto dto ) {

		AreqBinaryFileCondition condition = new AreqBinaryFileCondition();
		condition.setAreqId( dto.getAreqEntity().getAreqId() );
		condition.setAreqCtgCd( AreqCtgCd.RemovalRequest.value() );
		support.getAreqBinaryFileDao().delete(condition.getCondition() );
		support.getAreqBinaryFileDao().insert( dto.getAreqBinaryFileEntities() );

	}
}
