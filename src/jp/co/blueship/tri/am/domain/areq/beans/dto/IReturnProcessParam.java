package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;
import java.sql.Timestamp;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;


/**
 * アプリケーション層で使用する、返却資産申請の処理状況を格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IReturnProcessParam extends ICommonParamInfo, Serializable {

	/**
	 * 申請情報番号を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getApplyNo();
	/**
	 * 申請情報番号を設定します。
	 * @param no 申請情報番号
	 */
	public void setApplyNo( String no );
	/**
	 * 操作ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUser();
	/**
	 * 操作ユーザを設定します。
	 * @param value 操作ユーザ
	 */
	public void setUser( String user );
	/**
	 * ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * ユーザＩＤを設定します。
	 * @param id ユーザＩＤ
	 */
	public void setUserId( String id );
	/**
	 * プロセスIDを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getProcCtgCd();
	/**
	 * @param procCtgCd プロセス分類コード
	 */
	public void setProcCtgCd( String procCtgCd );

	/**
	 * メッセージＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getMessageId();
	/**
	 * メッセージＩＤを設定します。
	 * @param id メッセージＩＤ
	 */
	public void setMessageId( String id );

	/**
	 * メッセージを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getMessage();
	/**
	 * メッセージを設定します。
	 * @param message メッセージ
	 */
	public void setMessage( String message );

	/**
	 * 処理日時を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public Timestamp getProcessDate();

	/**
	 * 処理日時を設定します。
	 *
	 * @param processDate 処理日時
	 */
	public void setProcessDate( Timestamp processDate );

	/**
	 * 二重貸出チェックフラグを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean isDuplicationCheck();

	/**
	 * 二重貸出チェックフラグを設定します。
	 * @param value 二重貸出チェックフラグ
	 */
	public void setDuplicationCheck( boolean value );

	/**
	 * 添付ファイルの名前を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getReturnAssetAppendFile();

	/**
	 * 添付ファイルの名前を設定します。
	 * @param returnAssetAppendFile 添付ファイルの名前
	 */
	public void setReturnAssetAppendFile(String returnAssetAppendFile);

}
