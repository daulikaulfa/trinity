package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.svc.beans.ActionMapAbstract;

/**
 * Run collectively the business processing.
 * Be sure to run in singleton = false.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class ActionDispatcherByCheckoutStatus extends ActionMapAbstract<List<IDomain<IGeneralServiceBean>>> {
	private IAmFinderSupport support = null;

	/**
	 * @param support Database access finder
	 */
	public void setSupport( IAmFinderSupport support ) {
		this.support = support;
	}

	@Override
	protected void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key) {
		List<Object> paramList = serviceDto.getParamList();
		IAreqDto areqDto = AmExtractEntityAddonUtils.extractLendApply( paramList );

		PreConditions.assertOf(areqDto != null, "IAreqDto is not specified");

		IAreqEntity areqEntity = support.findAreqEntity( areqDto.getAreqEntity().getAreqId(), (StatusFlg)null );

		if ( !key.equals( areqEntity.getStsId() ) || StatusFlg.on.equals(areqEntity.getDelStsId()) ) {
			return;
		}

		for (IDomain<IGeneralServiceBean> action : this.getActions().get( key )) {
			action.execute(serviceDto);
		}
	}
}
