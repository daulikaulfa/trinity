package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 指定された資産削除の申請フォーマットファイルを読み出し、ファイルパス名に変換します。
 * <li>資産ファイルの場合、ディレクトリは、原本作業パスからの相対パスファイル名で配下を展開します。
 * <li>ファイルの親パス名がSCM管理ファイルの場合、変換対象から、完全に除外されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class GatherDelApplyFormatFile extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static final String SEPARATOR = "\\|" ;//削除資産ファイルとバイナリファイルの区切り文字にパイプ|を用いる。

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<IDelApplyFileResult>		parseFiles			= new ArrayList<IDelApplyFileResult>();
		List<IDelApplyBinaryFileResult>	parseBinaryFiles	= new ArrayList<IDelApplyBinaryFileResult>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			List<String> files = new ArrayList<String>();
			List<String> binaryFiles = new ArrayList<String>();

			File file = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
									sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefUploadFile ) );

			FileInputStream fis = null ;
			InputStreamReader isr = null ;
			try {
				fis = new FileInputStream( file ) ;
				isr = new InputStreamReader(fis) ;
				this.splitFormatFile(files, binaryFiles, TriFileUtils.readFileLine( isr , false ));
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isr);
				TriFileUtils.close(fis);
			}

			//削除資産ファイル
			files = this.parseFiles( AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ), files);
			for ( Iterator<String> it = files.iterator(); it.hasNext(); ) {
				String path = it.next();

				IDelApplyFileResult result = new DelApplyFileResult();
				result.setFilePath( path );

				parseFiles.add( result );
			}

			paramList.add( this.sort( parseFiles.toArray( new IDelApplyFileResult[] {} )) );


			for ( String path : binaryFiles ) {

				path = FlowChaLibMasterDelEditSupport.convertPath( path );

				IDelApplyBinaryFileResult result = new DelApplyBinaryFileResult();
				result.setFilePath( path );

				parseBinaryFiles.add( result );
			}

			paramList.add( this.sort( parseBinaryFiles.toArray( new IDelApplyBinaryFileResult[] {} )) );
		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E, e);
		} catch ( NullPointerException e ) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E, e);
		}

		return serviceDto;
	}

	/**
	 * 申請フォーマットファイルを書式に従い、分割します。
	 *
	 * @param destFiles 資産ファイルリスト
	 * @param destBinaryFiles バイナリ資産ファイルリスト
	 * @param src 申請フォーマットファイル
	 */
	private final void splitFormatFile(
			List<String> destFiles, List<String> destBinaryFiles, String[] src ) {

		for ( int i = 0; i < src.length; i++ ) {

			src[i] = StringUtils.replace( src[i] , "\"" , "" ) ;

			if ( TriStringUtils.isEmpty( src[i] ) )
				continue;

			String[] splits = src[i].split( SEPARATOR );

			if ( 0 < splits.length && ! TriStringUtils.isEmpty( splits[0].trim() ) )
				destFiles.add( splits[0].trim() );

			if ( 1 < splits.length && ! TriStringUtils.isEmpty( splits[1].trim() ) )
				destBinaryFiles.add( splits[1].trim() );
		}
	}

	/**
	 * ディレクトリを、ルートパスからの相対パスファイル名で配下を展開します。
	 *
	 * @param root 対象となるディレクトリパス
	 * @param files 相対パス（ファイル／ディレクトリ混在）
	 * @return 展開したファイルリストを戻します。
	 * @throws IOException
	 */
	private final List<String> parseFiles(  File root, List<String> files ) throws IOException {

		List<String> parseFiles = new ArrayList<String>();
		Map<String,String> map = new HashMap<String,String>();

		for ( String path : files ) {
			String result = FlowChaLibMasterDelEditSupport.convertPath(path);
			File file = new File( root, result );

			if ( ! file.exists() ) {
				if ( map.containsKey( result ) )
					continue;

				if ( TriFileUtils.isSCM( file ) )
					continue;

				parseFiles.add( path );
				continue;
			}

			if ( file.isFile() ) {
				if ( map.containsKey( result ) )
					continue;

				if ( TriFileUtils.isSCM( file ) )
					continue;

				map.put( result, result );
				parseFiles.add( result );
			}

			if ( file.isDirectory() ) {
				result = TriStringUtils.trimTailSeparator( result );
				List<String> fileList = TriFileUtils.getFilePaths( file, TriFileUtils.TYPE_FILE  ,  BusinessFileUtils.getFileFilter() ) ;
				Iterator<String> iter = fileList.iterator() ;
				while( iter.hasNext() ) {
					String parseFile = TriStringUtils.linkPathBySlash( result , iter.next() ) ;

					if ( map.containsKey( parseFile ) )
						continue;

					if ( TriFileUtils.isSCM( new File(root, parseFile) ) )
						continue;

					map.put(parseFile, parseFile);
					parseFiles.add( FlowChaLibMasterDelEditSupport.convertPath( parseFile ) );
				}
			}
		}

		return parseFiles;
	}

	/**
	 * ファイルパスのソートを行います。
	 * @param src ソートを行うファイル
	 * @return ソート結果を戻します。
	 */
	private final Object[] sort( IFileResult[] src ) {
		Arrays.sort( src,
					new Comparator<IFileResult>() {
						public int compare( IFileResult o1, IFileResult o2 ) {
							IFileResult src1 = (IFileResult)o1;
							IFileResult src2 = (IFileResult)o2;

							return src1.getFilePath().compareTo( src2.getFilePath() );
						}
					} );

		return src;
	}

}
