package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.constants.ChaLibCallScriptItemID;
import jp.co.blueship.tri.am.constants.ProcCtgCdByReturningRequest;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ProcessStatusID;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean.ChaLibRtnEntryCheckProcessViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcDetailsItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsCondition;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibRtnEntryServiceCheck implements IDomain<FlowChaLibRtnEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support = null;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnEntryServiceBean> execute(IServiceDto<FlowChaLibRtnEntryServiceBean> serviceDto) {
		FlowChaLibRtnEntryServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (forward.equals(ChaLibScreenID.RTN_ENTRY_CHECK)) {
				if (!ScreenType.bussinessException.equals(paramBean.getScreenType())) {
					executeForward(paramBean, paramBean);
				}
			}

			if (referer.equals(ChaLibScreenID.RTN_ENTRY_CHECK)) {
				if (ScreenType.next.equals(paramBean.getScreenType())) {
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * 返却申請状況画面への遷移
	 *
	 * @param paramBean
	 * @param retBean
	 */
	private final void executeForward(FlowChaLibRtnEntryServiceBean paramBean, FlowChaLibRtnEntryServiceBean retBean) throws Exception {

		retBean.setLotNo(paramBean.getLotNo());
		retBean.setApplyNo(paramBean.getApplyNo());
		retBean.setApplyUserName(paramBean.getApplyUserName());
		retBean.setApplyUserId(paramBean.getApplyUserId());
		retBean.setApplyUserGroup(paramBean.getApplyUserGroup());

		// ２重貸出チェック
		retBean.setDuplicationCheck(true);

		List<IProcDetailsEntity> entitys = new ArrayList<IProcDetailsEntity>();

		ProcDetailsCondition condition = new ProcDetailsCondition();
		condition.setProcId(paramBean.getProcId());
		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcDetailsItems.procStTimestamp, TriSortOrder.Asc, 0);
		entitys = this.support.getSmFinderSupport().getProcDetailsDao().find(condition.getCondition(), sort);

		// 完了ステータス（はじめに0にする）
		retBean.setCompleteStatus( StatusFlg.off.value() );

		List<ChaLibRtnEntryCheckProcessViewBean> processList = new ArrayList<ChaLibRtnEntryCheckProcessViewBean>();

		String processCategoryCode = "";
		for (IProcDetailsEntity entity : entitys) {

			processCategoryCode = entity.getProcCtgCd();

			if (!TriStringUtils.isEmpty(entity.getMsgId()) && retBean.getErrMessage() == null) {
				retBean.setErrMessage(entity.getMsg());
			}

			if ((processCategoryCode.equals(ProcCtgCdByReturningRequest.DUPLICATION_FILE_CHECK) && retBean.isDuplicationCheck())
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.RETURN_FILE_COPY)
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.EXTENSION_CHECK)
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.COMPILE_SYNTAX_CHECK)
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.CHARSET_CHECK)
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.LINEFEED_CHECK)
					|| processCategoryCode.equals(ProcCtgCdByReturningRequest.CALL_SCRIPT)) {

				ChaLibRtnEntryCheckProcessViewBean viewBean = retBean.newChaLibRtnEntryCheckProcessViewBean();

				viewBean.setProcessName(processCategoryCode);

				String processStatusID = ProcessStatusID.PROCESS_ACTIVE;
				if (null != entity.getMsgId() && !entity.getMsgId().equals("")) {
					processStatusID = ProcessStatusID.PROCESS_ERROR;
				} else if (entity.getStsId().equals(StatusFlg.off.value())) {
					processStatusID = ProcessStatusID.PROCESS_STOP;
				}
				viewBean.setProcessStatus(processStatusID);

				if (log.isInfoEnabled()) {
					LogHandler.info(log, TriLogMessage.LAM0001, processCategoryCode, processStatusID);
				}

				processList.add(viewBean);

				// 完了扱いとみなす処理のセット(デフォルトは拡張子チェック)
				String asCompProcessId = ProcCtgCdByReturningRequest.EXTENSION_CHECK;
				if (!StatusFlg.off.value().equals(
						sheet.getValue(AmDesignBeanId.callScriptAtReturnApply, ChaLibCallScriptItemID.SummaryAtReturnApply.DO_EXEC.toString()))) {
					asCompProcessId = ProcCtgCdByReturningRequest.CALL_SCRIPT;
				}

				// 完了扱いとみなす処理が完了の場合
				if (processCategoryCode.equals(asCompProcessId) && entity.getCompStsId().equals(StatusFlg.on)) {
					retBean.setCompleteStatus(StatusFlg.on.value());
				}
			}
		}

		retBean.setCheckProcessViewBeanList(processList);
	}

}
