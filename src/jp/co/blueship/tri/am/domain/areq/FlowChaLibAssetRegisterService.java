package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterCondition;
import jp.co.blueship.tri.am.dao.lot.constants.LotItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterViewBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibAssetRegisterServiceBean;
import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.am.support.FlowChaLibReportSupport;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.FileResult;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * FlowChaLibAssetRegisterServiceイベントのサービスClass
 * <br>
 * @version V3L11.01
 * @author Yukihiro Eguchi
 * <br>
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibAssetRegisterService implements IAmDomain<FlowChaLibAssetRegisterServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibReportSupport support = null;

	public void setSupport(FlowChaLibReportSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibAssetRegisterServiceBean> execute(IServiceDto<FlowChaLibAssetRegisterServiceBean> serviceDto) {

		FlowChaLibAssetRegisterServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			AssetRegisterCondition paramCondition = paramBean.getAssetRegisterCondition();

			if ( TriStringUtils.isEmpty( paramCondition.getLotId() )
				&& TriStringUtils.isEmpty( paramCondition.getLotNm() ) ) {
				throw new BusinessException( AmMessageId.AM001126E );
			}

			List<AssetRegisterViewBean> results = new ArrayList<AssetRegisterViewBean>();

			List<ILotEntity> lotEntities = this.findLot(paramBean);

			for ( ILotEntity lotEntity: lotEntities ) {
				List<IViewAssetRegisterEntity> entities = this.findAssetRegister( paramBean, lotEntity.getLotId() );

				Set<String> findPathSet = new LinkedHashSet<String>();

				for ( IViewAssetRegisterEntity entity: entities ) {
					findPathSet.add( entity.getFilePath() );
					results.add( this.convertAssetToRegisterViewBean( paramBean, lotEntity, entity ) );
				}

				results.addAll( this.getAssetRegisterFromMW(paramBean, findPathSet, lotEntity) );
			}

			if ( 0 == results.size() ) {
				paramBean.setInfoMessage( AmMessageId.AM003000I );
			}

			paramBean.setAssetRegisterViewBean( this.sort( results ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}
	}

	/**
	 * 指定された条件でロット情報を取得する
	 *
	 * @param paramBean Domain Service Bean
	 * @return 取得したロットリスト
	 */
	private List<ILotEntity> findLot( FlowChaLibAssetRegisterServiceBean paramBean ) {
		AssetRegisterCondition paramCondition = paramBean.getAssetRegisterCondition();

		LotCondition condition = new LotCondition();
		condition.setLotIds( paramCondition.getLotId() );
		condition.setLotNms( paramCondition.getLotNm() );
		condition.setStsIds( ExtractStatusAddonUtils.getLotBaseStatusByAll() );

		ISqlSort sort = new SortBuilder();
		sort.setElement(LotItems.lotId, TriSortOrder.Asc, 1);

		return support.getLotDao().find(condition.getCondition(), sort);
	}

	/**
	 * 申請情報を検索して最新の資産、および申請情報を取得する
	 *
	 * @param paramBean Domain Service Bean
	 * @param lotId 抽出対象のロットID
	 * @return 取得した資産リスト
	 */
	private List<IViewAssetRegisterEntity> findAssetRegister(
			FlowChaLibAssetRegisterServiceBean paramBean,
			String lotId ) {
		AssetRegisterCondition paramCondition = paramBean.getAssetRegisterCondition();

		//ステータスで絞りこまなければ、削除済の資産も抽出対象
		ViewAssetRegisterCondition condition = new ViewAssetRegisterCondition();
		condition.setLotId( lotId );
		condition.setMdlNm( paramCondition.getMdlNm() );
		condition.setFilePath( paramCondition.getFilePath() );
		condition.setPjtId( paramCondition.getPjtId() );
		condition.setChgFactorNo( paramCondition.getChgFactorNo() );
		condition.setReqUserId( paramCondition.getReqUserId() );
		condition.setGrpNm( paramCondition.getGrpNm() );
		condition.setStsId( paramCondition.getStsId() );
		condition.setProcStsId( paramCondition.getProcStsId() );

		List<IViewAssetRegisterEntity> entities = support.getViewAssetRegisterDao().find( condition );

		return entities;
	}

	/**
	 * ロット原本の公開フォルダ（Working Copy)から資産リストを取得する
	 *
	 * @param paramBean Domain Service Bean
	 * @param findPathSet 抽出済みのファイルパス
	 * @param lotEntity 対象となるロットのEntity
	 * @return Resource List
	 */
	private List<AssetRegisterViewBean> getAssetRegisterFromMW(
			FlowChaLibAssetRegisterServiceBean paramBean,
			Set<String> findPathSet,
			ILotEntity lotEntity ) {

		List<AssetRegisterViewBean> results = new ArrayList<AssetRegisterViewBean>();

		AssetRegisterCondition paramCondition = paramBean.getAssetRegisterCondition();

		boolean doFileMatching = TriStringUtils.isNotEmpty(paramCondition.getFilePath());

		if ( paramCondition.isTargetMW() ) {
			Map<String, IFileResult> filesMap = this.getMasterWorkFiles( paramBean, lotEntity );

			for ( IFileResult result: filesMap.values() ) {
				String filePath = result.getFilePath();

				if ( findPathSet.contains( filePath ) ) {
					continue;
				}

				if ( doFileMatching ) {
					boolean isMatch = false;

					for ( String param: paramCondition.getFilePath() ) {
						if ( -1 != filePath.indexOf( param ) ) {
							isMatch = true;
							break;
						}
					}

					if ( !isMatch ) {
						continue;
					}
				}

				AssetRegisterViewBean view = new AssetRegisterViewBean();

				view.setLotId( lotEntity.getLotId() );
				view.setLotNm( lotEntity.getLotNm() );
				view.setMdlNm( TriStringUtils.substringBefore( result.getFilePath() ) );
				view.setFilePath( result.getFilePath() );

				results.add( view );
			}
		}

		return results;
	}

	/**
	 * ロット原本の公開フォルダ（Working Copy)から資産リストを取得する
	 *
	 * @param paramBean Domain Service Bean
	 * @param lotEntity 対象となるロットのEntity
	 * @return Map of Resource Path
	 */
	private Map<String, IFileResult> getMasterWorkFiles(
			FlowChaLibAssetRegisterServiceBean paramBean,
			ILotEntity lotEntity ) {

		List<IFileResult> results = new ArrayList<IFileResult>();

		Set<String> moduleConditionSet = new HashSet<String>();
		String[] moduleNmArray = paramBean.getAssetRegisterCondition().getMdlNm();
		if ( ! TriStringUtils.isEmpty(moduleNmArray) ) {
			for ( String module: moduleNmArray ) {
				moduleConditionSet.add( module.trim() );
			}
		}

		Set<String> moduleSet = new HashSet<String>();
		for ( IVcsReposDto dto: support.findVcsReposDto() ) {
			String moduleNm = dto.getVcsMdlEntity().getMdlNm();

			if ( 0 < moduleConditionSet.size() ) {
				if ( moduleConditionSet.contains( moduleNm ) ) {
					moduleSet.add( moduleNm );
				}
			} else {
				moduleSet.add( moduleNm );
			}
		}

		Map<String, IFileResult> filesMap = new LinkedHashMap<String, IFileResult>();

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );

		List<File> files = BusinessFileUtils.getListFiles( masterPath );
		for ( File file: files ) {
			if ( ! file.isDirectory() )
				continue;

			String path = TriStringUtils.convertPath( file.getPath() );
			String relativePath = TriStringUtils.convertRelativePath( masterPath, path );

			String module = TriStringUtils.substringBefore( relativePath );

			if ( ! moduleSet.contains(module) )
				continue;

			results = this.doRecursive(masterPath, file, results);
		}

		for ( IFileResult result: results ) {
			filesMap.put( result.getFilePath(), result );
		}

		return filesMap;
	}

	/**
	 * 指定されたファイルパスを起点として再帰的にフォルダ階層を検索し、すべての資産を取得する
	 *
	 * @param root 起点となるファイルパス
	 * @param src 現在のファイルパス
	 * @param results ファイルパス格納リスト
	 * @return ファイルパス格納リスト
	 */
	private List<IFileResult> doRecursive( File root, File src, List<IFileResult> results ) {
		if (src.isFile()) {
			IFileResult file = new FileResult();

			String path = TriStringUtils.convertPath( src.getPath() );
			file.setFilePath( TriStringUtils.convertRelativePath( root, path ) );
			results.add( file );

		} else if (src.isDirectory()) {
			List<File> files = BusinessFileUtils.getListFiles( src );

			for ( File file: files ) {
				this.doRecursive(root, file, results);
			}
		}

		return results;
	}

	/**
	 * Convert entity
	 *
	 * @param paramBean Domain Service Bean
	 * @param paramLotEntity Parameter of lot
	 * @param entity Conversion source of Entity
	 * @return Conversion destination of Entity
	 */
	private AssetRegisterViewBean convertAssetToRegisterViewBean(
			FlowChaLibAssetRegisterServiceBean paramBean,
			ILotEntity paramLotEntity,
			IViewAssetRegisterEntity entity ) {

		AssetRegisterViewBean view = new AssetRegisterViewBean();

		SimpleDateFormat format = TriDateUtils.getYMDHMSDateFormat( SystemProps.TriSystemLanguage.getProperty() );

		view.setMdlNm( entity.getMdlNm() );
		view.setFilePath( entity.getFilePath() );
		view.setLotId( entity.getLotId() );
		view.setLotNm( entity.getLotNm() );
		view.setPjtId( entity.getPjtId() );
		view.setChgFactorNo( entity.getChgFactorNo() );
		view.setAreqId( entity.getAreqId() );
		view.setAreqCtgCd( entity.getAreqCtgCd() );
		view.setGrpId( entity.getGrpId() );
		view.setGrpNm( entity.getGrpNm() );
		view.setAssetReqNm( entity.getAssetReqNm() );
		view.setContent( entity.getContent() );
		view.setLendReqUserId( entity.getLendReqUserId() );
		view.setLendReqUserNm( entity.getLendReqUserNm() );
		view.setLendReqDateTime( TriDateUtils.convertViewDateFormat( entity.getLendReqTimestamp(), format ) );
		view.setRtnReqUserId( entity.getRtnReqUserId() );
		view.setRtnReqUserNm( entity.getRtnReqUserNm() );
		view.setRtnReqDateTime( TriDateUtils.convertViewDateFormat( entity.getRtnReqTimestamp(), format ) );
		view.setDelReqUserId( entity.getDelReqUserId() );
		view.setDelReqUserNm( entity.getDelReqUserNm() );
		view.setDelReqDateTime( TriDateUtils.convertViewDateFormat( entity.getDelReqTimestamp(), format ) );
		view.setLendFileRev( entity.getLendFileRev() );
		view.setLendFileByteSize( entity.getLendFileByteSize() );
		view.setLendFileUpdDateTime( TriDateUtils.convertViewDateFormat( entity.getLendFileUpdTimestamp(), format ) );
		view.setRtnFileRev( entity.getRtnFileRev() );
		view.setRtnFileByteSize( entity.getRtnFileByteSize() );
		view.setRtnFileUpdDateTime( TriDateUtils.convertViewDateFormat( entity.getRtnFileUpdTimestamp(), format ) );
		view.setDelFileRev( entity.getDelFileRev() );
		view.setDelFileByteSize( entity.getDelFileByteSize() );
		view.setDelFileUpdDateTime( TriDateUtils.convertViewDateFormat( entity.getDelFileUpdTimestamp(), format ) );
		view.setPjtAvlDateTime( TriDateUtils.convertViewDateFormat( entity.getPjtAvlTimestamp(), format ) );
		view.setStsId( entity.getStsId() );
		view.setProcStsId( entity.getProcStsId() );

		view.setResourceLock( AmBusinessJudgUtils.isResourceLock(entity.getStsId()) );

		return view;
	}


	/**
	 * ファイルパスごとにソートする
	 * @param list ソート対象のBean
	 * @return ソート結果
	 */
	public List<AssetRegisterViewBean> sort( List<AssetRegisterViewBean> list ) {

		Collections.sort( list,
				new Comparator<AssetRegisterViewBean>() {
					public int compare( AssetRegisterViewBean src1, AssetRegisterViewBean src2 ) {
						int compareTo = 0;

						String lotId1 = src1.getLotId();
						String lotId2 = src2.getLotId();

						compareTo = lotId1.compareTo(lotId2);

						if ( 0 != compareTo )
							return compareTo;

						String[] paths1 = TriStringUtils.split( src1.getFilePath(), "/" );
						String[] paths2 = TriStringUtils.split( src2.getFilePath(), "/" );

						int max = (paths1.length > paths2.length)? paths1.length: paths2.length;

						for ( int i = 0; i < max; i++ ) {
							if ( paths1.length -1 < i )
								return -1;

							if ( paths2.length -1 < i )
								return 1;

							if ( paths1.length -1 == i && paths2.length -1 != i )
								return -1;

							if ( paths2.length -1 == i && paths1.length -1 != i )
								return 1;

							compareTo = paths1[i].compareTo(paths2[i]);

							if ( 0 == compareTo )
								continue;

							return compareTo;
						}

						return compareTo;
					}
				} );

		return list;
	}

}
