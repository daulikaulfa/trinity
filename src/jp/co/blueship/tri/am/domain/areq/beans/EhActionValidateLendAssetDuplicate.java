package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.FileDiffResult;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;


/**
 * 貸出申請の登録時の妥当性チェックを行います。
 * <li>原本比較処理
 * <li>貸出二重チェック
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateLendAssetDuplicate extends ActionPojoAbstract<IGeneralServiceBean>  {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		paramList.add( check( serviceDto ) );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param info アプリケーション層に使用する共通インタフェース
	 * @param list コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private IFileDiffResult[] check( IServiceDto<IGeneralServiceBean> serviceDto )
										throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		String[] files = AmExtractMessageAddonUtils.getLendParamInfo( paramList ).getLendFiles();

		if ( TriStringUtils.isEmpty( files )) {
			return new IFileDiffResult[]{};
		}

		return duplicationCheck( paramList, extractDiff( paramList, files ) );
	}

	/**
	 * 原本比較を行います。
	 * <li>今回申請するファイルと原本作業ファイルを順次比較します。
	 * <li>原本に存在しない場合、業務エラーとします。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return ファイル差分情報を戻します。
	 * @throws ContinuableBusinessException
	 */

	private final IFileDiffResult[] extractDiff( List<Object> paramList, String[] files )
											throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		IFileDiffResult[] results = new IFileDiffResult[0];

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		List<IFileDiffResult> resultList = new ArrayList<IFileDiffResult>();

		for ( int i = 0; i < files.length; i++ ) {

			IFileDiffResult result = new FileDiffResult();
			result.setFilePath(files[i]);
			result.setDiff( false );
			result.setMasterExists( false );

			File infoPath = new File( masterPath, files[i] );
			if ( infoPath.isFile() )
				result.setMasterExists( true );

			resultList.add( result );
		}

		results = (IFileDiffResult[])resultList.toArray( new IFileDiffResult[0] );


		try {
			for ( int i = 0; i < results.length; i++ ) {
				if ( ! results[i].isMasterExists() ) {
					messageList.add( AmMessageId.AM001052E );
					messageArgsList.add(
							new String[] { TriStringUtils.convertRelativePath(masterPath, results[i].getFilePath()) });
				}
			}
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

		return results;
	}

	/**
	 * 他の貸出申請情報と貸出二重チェックを行います。
	 * <li>今回申請するファイルと他の貸出申請ファイルを順次比較します。
	 * <li>資産が重複する場合、エラーとします。
	 *
	 * @param entity 貸出返却情報
	 * @param fileDiffs 原本との差分ファイル情報
	 * @throws ContinuableBusinessException
	 */

	private final IFileDiffResult[] duplicationCheck(
											List<Object> paramList,
											IFileDiffResult[] fileDiffs )
			throws ContinuableBusinessException {

		IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );

		// ２重貸出チェック実施せず
		if ( !param.isDuplicationCheck() ) {
			return fileDiffs;
		}

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		String applyNo = param.getApplyNo();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity pjtLotEntity = lotDto.getLotEntity();
		AreqCondition condition =
			AmDBSearchConditionAddonUtils.getAreqConditionByAssetLock( pjtLotEntity.getLotId() );
		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> areqEntities = support.getAreqDao().find( condition.getCondition(), sort );

		try {
			for ( IAreqEntity entity : areqEntities ) {

				if ( entity.getAreqId().equals( applyNo ))
					continue;

				File lendAssetNoPath	= AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList, entity );
				File returnAssetNoPath	= AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList, entity );


				for ( int j = 0; j < fileDiffs.length; j++ ) {

					File otherLendFile		= new File( lendAssetNoPath, fileDiffs[j].getFilePath() );
					File otherReturnFile	= new File( returnAssetNoPath, fileDiffs[j].getFilePath() );
					if ( ! otherLendFile.isFile() && ! otherReturnFile.isFile() )
						continue;

					messageList.add( AmMessageId.AM001043E );
					messageArgsList.add(
							new String[] { TriStringUtils.convertRelativePath(masterPath, fileDiffs[j].getFilePath()) , entity.getPjtId() , entity.getAreqId() });
				}
			}

			return fileDiffs;
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}



}
