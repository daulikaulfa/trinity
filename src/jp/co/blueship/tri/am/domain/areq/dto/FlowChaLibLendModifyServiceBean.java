package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibSelectLendEntryAssetSelectBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * 貸出申請編集を行うフローのテンプレート
 *
 */
public class FlowChaLibLendModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	


	/**
	 * ロット番号
	 */
	private String lotId;
	/**
	 * 申請情報番号
	 */
	private String applyNo;

	/**
	 * 入力貸出申請情報
	 */
	private ChaLibEntryInputBaseInfoBean inBaseInfoBean;

	/**
	 * 入力資産選択情報
	 */
	private ChaLibInputLendEntryAssetSelectBean inAssetSelectBean;

	/**
	 * 貸出申請基本情報
	 */
	private ChaLibEntryBaseInfoBean baseInfoViewBean;

	/**
	 * 申請資産情報
	 */
	private ChaLibEntryAssetResourceInfoBean assetSelectViewBean;

	/**
	 * 貸出資産の選択リスト
	 */
	private ChaLibSelectLendEntryAssetSelectBean selectAssetSelectViewBean;

	/**
	 * 二重貸出チェック
	 */
	private boolean duplicationCheck;

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public ChaLibEntryAssetResourceInfoBean getAssetSelectViewBean() {
		return assetSelectViewBean;
	}

	public void setAssetSelectViewBean(
			ChaLibEntryAssetResourceInfoBean assetSelectViewBean) {
		this.assetSelectViewBean = assetSelectViewBean;
	}

	public ChaLibEntryBaseInfoBean getBaseInfoViewBean() {
		return baseInfoViewBean;
	}

	public void setBaseInfoViewBean(ChaLibEntryBaseInfoBean baseInfoViewBean) {
		this.baseInfoViewBean = baseInfoViewBean;
	}

	public ChaLibInputLendEntryAssetSelectBean getInAssetSelectBean() {
		return inAssetSelectBean;
	}

	public void setInAssetSelectBean(
			ChaLibInputLendEntryAssetSelectBean inAssetSelectBean) {
		this.inAssetSelectBean = inAssetSelectBean;
	}

	public ChaLibEntryInputBaseInfoBean getInBaseInfoBean() {
		return inBaseInfoBean;
	}

	public void setInBaseInfoBean(ChaLibEntryInputBaseInfoBean inBaseInfoBean) {
		this.inBaseInfoBean = inBaseInfoBean;
	}

	public ChaLibSelectLendEntryAssetSelectBean getSelectAssetSelectViewBean() {
		return selectAssetSelectViewBean;
	}

	public void setSelectAssetSelectViewBean(
			ChaLibSelectLendEntryAssetSelectBean selectAssetSelectViewBean) {
		this.selectAssetSelectViewBean = selectAssetSelectViewBean;
	}

	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean value ) {
		this.duplicationCheck = value;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
}
