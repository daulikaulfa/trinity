package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.am.support.FlowChaLibScmEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;

/**
 * 貸出情報申請の更新を行います。
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class EhActionUpdateLend extends ActionPojoAbstract<IGeneralServiceBean> {

	private FlowChaLibLendEditSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowChaLibLendEditSupport support ) {
		this.support = support;
	}

	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}
	private IVcsService vcsService = null;

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		List<Object> paramList = serviceDto.getParamList();

		try {
			ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
			ILotEntity lotEntity = lotDto.getLotEntity();
			if ( TriStringUtils.isEmpty( lotEntity ) ) {
				throw new TriSystemException( AmMessageId.AM005007S ) ;
			}


			IAreqDto areqDto = AmExtractEntityAddonUtils.extractLendApply( paramList );
			List<IAreqFileEntity> areqFileEntities = new ArrayList<IAreqFileEntity>();

			VcsCategory scmType = VcsCategory.value( lotEntity.getVcsCtgCd() ) ;
			if( VcsCategory.SVN.equals( scmType ) ) {

				IPassMgtEntity passwordEntity = AmExtractEntityAddonUtils.extractPasswordEntity( paramList ) ;

				vcsService = factory.createService(scmType ,passwordEntity.getUserId(), passwordEntity.getPassword());

				Map<String,SVNStatus> statusMap = new HashMap<String,SVNStatus>();

				//ロット原本資産のワークパスを参照する。
				String workPath = lotEntity.getLotMwPath() ;

				IFileDiffResult[] results = AmExtractMessageAddonUtils.getFileDiffResult( paramList );

				FlowChaLibScmEditSupport.getStatus( vcsService , workPath , results , statusMap );
				areqFileEntities = support.setAssetFileEntity( paramList, statusMap );

			} else {
				throw new TriSystemException( AmMessageId.AM004016F , scmType.value() ) ;
			}

			support.getAreqDao().update( areqDto.getAreqEntity() );
			{
				AreqFileCondition condition = new AreqFileCondition();
				condition.setAreqId( areqDto.getAreqEntity().getAreqId());
				support.getAreqFileDao().delete( condition.getCondition() );
				support.getAreqFileDao().insert( areqFileEntities );
			}

			String areqId = areqDto.getAreqEntity().getAreqId();

			IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );
			if( null != param ){
				support.getUmFinderSupport().updateCtgLnk(param.getCtgId(), AmTables.AM_AREQ, areqId);
				support.getUmFinderSupport().updateMstoneLnk(param.getMstoneId(), AmTables.AM_AREQ, areqId);
			}

			areqDto = this.support.findAreqDto( areqId, areqDto.getAreqEntity().getDelStsId() );
			paramList.remove(areqDto);
			paramList.add(areqDto);

			// 履歴更新
			if ( DesignSheetUtils.isRecord() ) {
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.EditRequest.getStatusId() );

				support.getAreqHistDao().insert( histEntity , areqDto);
			}
		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005071S , e , "IOError" );
		} catch (ScmException e) {
			throw new TriSystemException( AmMessageId.AM005071S , e , "ScmError" );
		} catch (ScmSysException e) {
			throw new TriSystemException( AmMessageId.AM005071S , e , "ScmSysError" );
		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}

}
