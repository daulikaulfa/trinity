package jp.co.blueship.tri.am.domain.areq.beans.dto;

import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * アプリケーション層で、貸出資産ファイルパスを格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IEhLendApplyFileResult extends IFileResult {
	
	

}
