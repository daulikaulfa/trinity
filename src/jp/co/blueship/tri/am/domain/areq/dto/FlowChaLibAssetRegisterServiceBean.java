package jp.co.blueship.tri.am.domain.areq.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */
public class FlowChaLibAssetRegisterServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	private AssetRegisterCondition assetRegisterCondition;
	private List<AssetRegisterViewBean> assetRegisterViewList = new ArrayList<AssetRegisterViewBean>();

	public AssetRegisterCondition getAssetRegisterCondition() {
		return assetRegisterCondition;
	}

	public void setAssetRegisterCondition(AssetRegisterCondition assetRegisterCondition) {
		this.assetRegisterCondition = assetRegisterCondition;
	}

	public List<AssetRegisterViewBean> getAssetRegisterViewList() {
		return assetRegisterViewList;
	}

	public void setAssetRegisterViewBean(List<AssetRegisterViewBean> assetRegisterViewList) {
		this.assetRegisterViewList = assetRegisterViewList;
	}

}
