package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;


/**
 * 指定された資産削除申請ファイルを一時作業パスに出力します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionWriteFileToDelApplyCommission extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList );

			IFileResult[] fileResults = AmExtractMessageAddonUtils.extractDelApplyFileResults( paramList );
			if ( null != fileResults ) {

				List<String> writeFiles = new ArrayList<String>();
				for ( IFileResult fileResult : fileResults ) {
					writeFiles.add( fileResult.getFilePath() );
				}

				TriFileUtils.writeStringFile(
						new File( file, sheet.getValue(
								AmDesignEntryKeyByChangec.delApplyDefTempFile )),
						(String[])writeFiles.toArray( new String[] {} ) );
			}

			IFileResult[] binaryFileResults = AmExtractMessageAddonUtils.extractDelApplyBinaryFileResults( paramList );
			if ( null != binaryFileResults ) {

				List<String> writeFiles = new ArrayList<String>();
				for ( IFileResult binaryFileResult : binaryFileResults ) {
					writeFiles.add( binaryFileResult.getFilePath() );
				}

				TriFileUtils.writeStringFile(
						new File( file, sheet.getValue(
								AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile )),
						(String[])writeFiles.toArray( new String[] {} ) );
			}

		} catch( IOException e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005136S , e );
		}

		return serviceDto;
	}
}
