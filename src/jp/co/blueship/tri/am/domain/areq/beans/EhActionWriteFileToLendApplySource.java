package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 指定された資産貸出申請ファイルをそのまま一時作業パスに出力します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionWriteFileToLendApplySource extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();


	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );

		if ( null == param ){
			throw new TriSystemException( AmMessageId.AM005015S );
		}
		File inApplyFilePath = new File( param.getInApplyFilePath() );

		try {

			//拡張子のチェック
			this.checkExtension( inApplyFilePath.getPath() ) ;

			String[] lines = TriFileUtils.readFileLine( inApplyFilePath, true );
			List<String> writeFiles = new ArrayList<String>();
			for ( int i = 0; i < lines.length; i++ ) {
				writeFiles.add( lines[i] );
			}

			File file = new File( 	AmLibraryAddonUtils.getLendApplyDefTempFilePath( paramList ),
									sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefFile ) );

			TriFileUtils.writeStringFile( file, (String[])writeFiles.toArray( new String[] {} ) );

		} catch( IOException e ) {
			LogHandler.fatal( log , e ) ;
			throw new TriSystemException( AmMessageId.AM005142S , e );
		} finally {

			if ( inApplyFilePath.exists() ) {
				inApplyFilePath.deleteOnExit();
			}
		}

		return serviceDto;
	}

	/**
	 * アップロードされたファイルの拡張子をチェックする
	 * @param path
	 */

	private void checkExtension( String path ) {
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		final String EXT = "txt" ;

		try {

			String extension = TriFileUtils.getExtension( path ) ;
			if( TriStringUtils.isEmpty( extension ) || //拡張子なしのファイル
					! EXT.toLowerCase().equals( extension.toLowerCase() ) ) {//拡張子が一致しない
				throw new ContinuableBusinessException( messageList , messageArgsList ) ;
			}
		}  catch ( Exception e ) {
			messageList.add		( AmMessageId.AM001059E ) ;
			messageArgsList.add	( new String[] { EXT } ) ;
			throw new ContinuableBusinessException( messageList , messageArgsList ) ;
		}
	}
}
