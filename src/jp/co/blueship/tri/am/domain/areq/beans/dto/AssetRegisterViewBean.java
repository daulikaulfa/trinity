package jp.co.blueship.tri.am.domain.areq.beans.dto;

/**
 *
 * @version V3L10.01
 * @author trinityV3
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
import java.io.Serializable;

public class AssetRegisterViewBean implements Serializable  {

	private static final long serialVersionUID = 1L;

	/**
	 * Module Name
	 */
	private String mdlNm = null;

	/**
	 * Resource Path
	 */
	private String filePath = null;
	/**
	 * Lot Id
	 */
	private String lotId = null;
	/**
	 * Lot Name
	 */
	private String lotNm = null;
	/**
	 * Change Property
	 */
	private String pjtId = null;
	/**
	 * Change Factor No.
	 */
	private String chgFactorNo = null;
	/**
	 * Resource Inquiry Id
	 */
	private String areqId = null;
	/**
	 * Resource Category Code
	 */
	private String areqCtgCd = null;
	/**
	 * Group Id
	 */
	private String grpId = null;
	/**
	 * Group Name
	 */
	private String grpNm = null;
	/**
	 * Resource Inquiry Subject
	 */
	private String assetReqNm = null;
	/**
	 * Content
	 */
	private String content = null;
	/**
	 * Checkout Inquiry User Id
	 */
	private String lendReqUserId = null;
	/**
	 * Checkout Inquiry User Name
	 */
	private String lendReqUserNm = null;
	/**
	 * Checkout Inquiry Date Time
	 */
	private String lendReqDateTime = null;
	/**
	 * Check-in Inquiry User Id
	 */
	private String rtnReqUserId = null;
	/**
	 * Check-in Inquiry User Name
	 */
	private String rtnReqUserNm = null;
	/**
	 * Check-in Inquiry Date Time
	 */
	private String rtnReqDateTime = null;
	/**
	 * Removal Inquiry User Id
	 */
	private String delReqUserId = null;
	/**
	 * Removal Inquiry User Name
	 */
	private String delReqUserNm = null;
	/**
	 * Removal Inquiry Date Time
	 */
	private String delReqDateTime = null;
	/**
	 * Checkout Resource Revision
	 */
	private String lendFileRev = null;
	/**
	 * Checkout Resource Revision
	 */
	private Integer lendFileByteSize = null;
	/**
	 * Checkout Resource Date Time
	 */
	private String lendFileUpdDateTime = null;
	/**
	 * Check-in Resource Revision
	 */
	private String rtnFileRev = null;
	/**
	 * Check-in Resource Revision
	 */
	private Integer rtnFileByteSize = null;
	/**
	 * Check-in Resource Date Time
	 */
	private String rtnFileUpdDateTime = null;
	/**
	 * Removal Resource Revision
	 */
	private String delFileRev = null;
	/**
	 * Removal Resource Revision
	 */
	private Integer delFileByteSize = null;
	/**
	 * Removal Resource Date Time
	 */
	private String delFileUpdDateTime = null;
	/**
	 * Change Property Approval Date Time
	 */
	private String pjtAvlDateTime = null;
	/**
	 * Status Id
	 */
	private String stsId = null;
	/**
	 * Process Status Id
	 */
	private String procStsId = null;
	/**
	 * Resource Lock
	 */
	private boolean resourceLock = false;
	/**
	 * Module Nameを取得します。
	 * @return Module Name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * Module Nameを設定します。
	 * @param mdlNm Module Name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	/**
	 * Resource Pathを取得します。
	 * @return Resource Path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * Resource Pathを設定します。
	 * @param filePath Resource Path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}
	/**
	 * Lot Idを取得します。
	 * @return Lot Id
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * Lot Idを設定します。
	 * @param lotId Lot Id
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * Lot Nameを取得します。
	 * @return Lot Name
	 */
	public String getLotNm() {
	    return lotNm;
	}
	/**
	 * Lot Nameを設定します。
	 * @param lotNm Lot Name
	 */
	public void setLotNm(String lotNm) {
	    this.lotNm = lotNm;
	}
	/**
	 * Change Propertyを取得します。
	 * @return Change Property
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * Change Propertyを設定します。
	 * @param pjtId Change Property
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * Change Factor No.を取得します。
	 * @return Change Factor No.
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}
	/**
	 * Change Factor No.を設定します。
	 * @param chgFactorNo Change Factor No.
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	}
	/**
	 * Resource Inquiry Idを取得します。
	 * @return Resource Inquiry Id
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * Resource Inquiry Idを設定します。
	 * @param areqId Resource Inquiry Id
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * Resource Category Codeを取得します。
	 * @return Resource Category Code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * Resource Category Codeを設定します。
	 * @param areqCtgCd Resource Category Code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	}
	/**
	 * Group Idを取得します。
	 * @return Group Id
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * Group Idを設定します。
	 * @param grpId Group Id
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * Group Nameを取得します。
	 * @return Group Name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * Group Nameを設定します。
	 * @param grpNm Group Name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}
	/**
	 * Resource Inquiry Subjectを取得します。
	 * @return Resource Inquiry Subject
	 */
	public String getAssetReqNm() {
	    return assetReqNm;
	}
	/**
	 * Resource Inquiry Subjectを設定します。
	 * @param assetReqNm Resource Inquiry Subject
	 */
	public void setAssetReqNm(String assetReqNm) {
	    this.assetReqNm = assetReqNm;
	}
	/**
	 * Contentを取得します。
	 * @return Content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * Contentを設定します。
	 * @param content Content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * Checkout Inquiry User Idを取得します。
	 * @return Checkout Inquiry User Id
	 */
	public String getLendReqUserId() {
	    return lendReqUserId;
	}
	/**
	 * Checkout Inquiry User Idを設定します。
	 * @param lendReqUserId Checkout Inquiry User Id
	 */
	public void setLendReqUserId(String lendReqUserId) {
	    this.lendReqUserId = lendReqUserId;
	}
	/**
	 * Checkout Inquiry User Nameを取得します。
	 * @return Checkout Inquiry User Name
	 */
	public String getLendReqUserNm() {
	    return lendReqUserNm;
	}
	/**
	 * Checkout Inquiry User Nameを設定します。
	 * @param lendReqUserNm Checkout Inquiry User Name
	 */
	public void setLendReqUserNm(String lendReqUserNm) {
	    this.lendReqUserNm = lendReqUserNm;
	}
	/**
	 * Checkout Inquiry Date Timeを取得します。
	 * @return Checkout Inquiry Date Time
	 */
	public String getLendReqDateTime() {
	    return lendReqDateTime;
	}
	/**
	 * Checkout Inquiry Date Timeを設定します。
	 * @param lendReqDateTime Checkout Inquiry Date Time
	 */
	public void setLendReqDateTime(String lendReqDateTime) {
	    this.lendReqDateTime = lendReqDateTime;
	}
	/**
	 * Check-in Inquiry User Idを取得します。
	 * @return Check-in Inquiry User Id
	 */
	public String getRtnReqUserId() {
	    return rtnReqUserId;
	}
	/**
	 * Check-in Inquiry User Idを設定します。
	 * @param rtnReqUserId Check-in Inquiry User Id
	 */
	public void setRtnReqUserId(String rtnReqUserId) {
	    this.rtnReqUserId = rtnReqUserId;
	}
	/**
	 * Check-in Inquiry User Nameを取得します。
	 * @return Check-in Inquiry User Name
	 */
	public String getRtnReqUserNm() {
	    return rtnReqUserNm;
	}
	/**
	 * Check-in Inquiry User Nameを設定します。
	 * @param rtnReqUserNm Check-in Inquiry User Name
	 */
	public void setRtnReqUserNm(String rtnReqUserNm) {
	    this.rtnReqUserNm = rtnReqUserNm;
	}
	/**
	 * Check-in Inquiry Date Timeを取得します。
	 * @return Check-in Inquiry Date Time
	 */
	public String getRtnReqDateTime() {
	    return rtnReqDateTime;
	}
	/**
	 * Check-in Inquiry Date Timeを設定します。
	 * @param rtnReqDateTime Check-in Inquiry Date Time
	 */
	public void setRtnReqDateTime(String rtnReqDateTime) {
	    this.rtnReqDateTime = rtnReqDateTime;
	}
	/**
	 * Removal Inquiry User Idを取得します。
	 * @return Removal Inquiry User Id
	 */
	public String getDelReqUserId() {
	    return delReqUserId;
	}
	/**
	 * Removal Inquiry User Idを設定します。
	 * @param delReqUserId Removal Inquiry User Id
	 */
	public void setDelReqUserId(String delReqUserId) {
	    this.delReqUserId = delReqUserId;
	}
	/**
	 * Removal Inquiry User Nameを取得します。
	 * @return Removal Inquiry User Name
	 */
	public String getDelReqUserNm() {
	    return delReqUserNm;
	}
	/**
	 * Removal Inquiry User Nameを設定します。
	 * @param delReqUserNm Removal Inquiry User Name
	 */
	public void setDelReqUserNm(String delReqUserNm) {
	    this.delReqUserNm = delReqUserNm;
	}
	/**
	 * Removal Inquiry Date Timeを取得します。
	 * @return Removal Inquiry Date Time
	 */
	public String getDelReqDateTime() {
	    return delReqDateTime;
	}
	/**
	 * Removal Inquiry Date Timeを設定します。
	 * @param delReqDateTime Removal Inquiry Date Time
	 */
	public void setDelReqDateTime(String delReqDateTime) {
	    this.delReqDateTime = delReqDateTime;
	}
	/**
	 * Checkout Resource Revisionを取得します。
	 * @return Checkout Resource Revision
	 */
	public String getLendFileRev() {
	    return lendFileRev;
	}
	/**
	 * Checkout Resource Revisionを設定します。
	 * @param lendFileRev Checkout Resource Revision
	 */
	public void setLendFileRev(String lendFileRev) {
	    this.lendFileRev = lendFileRev;
	}
	/**
	 * Checkout Resource Revisionを取得します。
	 * @return Checkout Resource Revision
	 */
	public Integer getLendFileByteSize() {
	    return lendFileByteSize;
	}
	/**
	 * Checkout Resource Revisionを設定します。
	 * @param lendFileByteSize Checkout Resource Revision
	 */
	public void setLendFileByteSize(Integer lendFileByteSize) {
	    this.lendFileByteSize = lendFileByteSize;
	}
	/**
	 * Checkout Resource Date Timeを取得します。
	 * @return Checkout Resource Date Time
	 */
	public String getLendFileUpdDateTime() {
	    return lendFileUpdDateTime;
	}
	/**
	 * Checkout Resource Date Timeを設定します。
	 * @param lendFileUpdDateTime Checkout Resource Date Time
	 */
	public void setLendFileUpdDateTime(String lendFileUpdDateTime) {
	    this.lendFileUpdDateTime = lendFileUpdDateTime;
	}
	/**
	 * Check-in Resource Revisionを取得します。
	 * @return Check-in Resource Revision
	 */
	public String getRtnFileRev() {
	    return rtnFileRev;
	}
	/**
	 * Check-in Resource Revisionを設定します。
	 * @param rtnFileRev Check-in Resource Revision
	 */
	public void setRtnFileRev(String rtnFileRev) {
	    this.rtnFileRev = rtnFileRev;
	}
	/**
	 * Check-in Resource Revisionを取得します。
	 * @return Check-in Resource Revision
	 */
	public Integer getRtnFileByteSize() {
	    return rtnFileByteSize;
	}
	/**
	 * Check-in Resource Revisionを設定します。
	 * @param rtnFileByteSize Check-in Resource Revision
	 */
	public void setRtnFileByteSize(Integer rtnFileByteSize) {
	    this.rtnFileByteSize = rtnFileByteSize;
	}
	/**
	 * Check-in Resource Date Timeを取得します。
	 * @return Check-in Resource Date Time
	 */
	public String getRtnFileUpdDateTime() {
	    return rtnFileUpdDateTime;
	}
	/**
	 * Check-in Resource Date Timeを設定します。
	 * @param rtnFileUpdDateTime Check-in Resource Date Time
	 */
	public void setRtnFileUpdDateTime(String rtnFileUpdDateTime) {
	    this.rtnFileUpdDateTime = rtnFileUpdDateTime;
	}
	/**
	 * Removal Resource Revisionを取得します。
	 * @return Removal Resource Revision
	 */
	public String getDelFileRev() {
	    return delFileRev;
	}
	/**
	 * Removal Resource Revisionを設定します。
	 * @param delFileRev Removal Resource Revision
	 */
	public void setDelFileRev(String delFileRev) {
	    this.delFileRev = delFileRev;
	}
	/**
	 * Removal Resource Revisionを取得します。
	 * @return Removal Resource Revision
	 */
	public Integer getDelFileByteSize() {
	    return delFileByteSize;
	}
	/**
	 * Removal Resource Revisionを設定します。
	 * @param delFileByteSize Removal Resource Revision
	 */
	public void setDelFileByteSize(Integer delFileByteSize) {
	    this.delFileByteSize = delFileByteSize;
	}
	/**
	 * Removal Resource Date Timeを取得します。
	 * @return Removal Resource Date Time
	 */
	public String getDelFileUpdDateTime() {
	    return delFileUpdDateTime;
	}
	/**
	 * Removal Resource Date Timeを設定します。
	 * @param delFileUpdDateTime Removal Resource Date Time
	 */
	public void setDelFileUpdDateTime(String delFileUpdDateTime) {
	    this.delFileUpdDateTime = delFileUpdDateTime;
	}
	/**
	 * Change Property Approval Date Timeを取得します。
	 * @return Change Property Approval Date Time
	 */
	public String getPjtAvlDateTime() {
	    return pjtAvlDateTime;
	}
	/**
	 * Change Property Approval Date Timeを設定します。
	 * @param pjtAvlDateTime Change Property Approval Date Time
	 */
	public void setPjtAvlDateTime(String pjtAvlDateTime) {
	    this.pjtAvlDateTime = pjtAvlDateTime;
	}
	/**
	 * Status Idを取得します。
	 * @return Status Id
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * Status Idを設定します。
	 * @param stsId Status Id
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * Process Status Idを取得します。
	 * @return Process Status Id
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * Process Status Idを設定します。
	 * @param procStsId Process Status Id
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * Resource Lockを取得します。
	 * @return Resource Lock
	 */
	public boolean isResourceLock() {
	    return resourceLock;
	}
	/**
	 * Resource Lockを設定します。
	 * @param resourceLock Resource Lock
	 */
	public void setResourceLock(boolean resourceLock) {
	    this.resourceLock = resourceLock;
	}

}
