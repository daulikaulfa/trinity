package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.DraftReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.LendingReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 申請情報番号の自動採番を行います。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ActionNumberingApplyNo extends ActionPojoAbstract<FlowChaLibLendEntryServiceBean> {

	LendingReqNumberingDao numberingDao = null;
	DraftReqNumberingDao draftNumberingDao = null;

	public void setNumberingDao(LendingReqNumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}

	public void setDraftNumberingDao(DraftReqNumberingDao draftNumberingDao) {
		this.draftNumberingDao = draftNumberingDao;
	}

	@Override
	public IServiceDto<FlowChaLibLendEntryServiceBean> execute( IServiceDto<FlowChaLibLendEntryServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();
		List<Object> selected = TriCollectionUtils.select(paramList, TriPredicates.isInstanceOf(IAreqDto.class));

		PreConditions.assertOf(selected.size() == 1, "IAreqEntity should be contained only one instance.");

		IAreqEntity areqEntity = ((IAreqDto) selected.get(0)).getAreqEntity();
		String areqId = null;

		if (AmAreqStatusId.CheckoutRequested.equals(areqEntity.getStsId())) {
			areqId = this.numberingDao.nextval();
		} else {
			areqId = this.draftNumberingDao.nextval();
		}
		areqEntity.setAreqId( areqId );
		serviceDto.getServiceBean().setApplyNo( areqId );

		return serviceDto;
	}
}
