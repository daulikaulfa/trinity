package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmEntityAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompLendCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * FlowChaLibCompLendCancelServiceイベントのサービスClass <br>
 * <p>
 * 変更管理 貸出申請の取消処理を行う。
 * </p>
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 *
 */
public class FlowChaLibCompLendCancelService implements IDomain<FlowChaLibCompLendCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction(ActionStatusMatrixList action) {
		this.statusMatrixAction = action;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibCompLendCancelServiceBean> execute(IServiceDto<FlowChaLibCompLendCancelServiceBean> serviceDto) {

		FlowChaLibCompLendCancelServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String areqId = paramBean.getApplyNo();

			if( paramBean.isStatusMatrixV3() ) {
				StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setAreqIds( areqId );

				StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			}

			IAreqDto areqDto = this.support.findAreqDto(areqId);
			AmEntityAddonUtils.setAssetApplyEntityEntryCancelServiceBean(areqDto,paramBean);

			IPjtEntity pjtEntity = this.support.findPjtEntity(areqDto.getAreqEntity().getPjtId());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// Subversionのパスワード情報を取得
			IPassMgtEntity passMgtEntity = null;
			VcsCategory scmType = VcsCategory.value(lotDto.getLotEntity().getVcsCtgCd());
			if (VcsCategory.SVN.equals(scmType)) {
				passMgtEntity = this.support.findPassMgtEntityBySVN();
			}

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			// 下層のアクションを実行する
			{
				List<Object> paramList = new ArrayList<Object>();

				paramList.add( areqDto );
				paramList.add(lotDto);
				paramList.add(paramBean);

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				if (VcsCategory.SVN.equals(scmType)) {
					paramList.add(passMgtEntity);
				}

				for (IDomain<IGeneralServiceBean> action : actions) {
					action.execute(innerServiceDto);
				}
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}

	}
}
