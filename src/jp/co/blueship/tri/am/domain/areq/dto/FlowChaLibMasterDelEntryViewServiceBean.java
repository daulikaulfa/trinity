package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryViewServiceBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibMasterDelEntryViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * ロット番号
	 */
	private String lotId ;

	/**
	 * 表示用申請番号
	 */
	private String delApplyNo;

	/**
	 * 表示情報
	 */
	private ChaLibMasterDelEntryViewServiceBean viewServiceBean;


	/**
	 * @return delApplyNo
	 */
	public String getDelApplyNo() {
		return delApplyNo;
	}


	/**
	 * @param delApplyNo 設定する delApplyNo
	 */
	public void setDelApplyNo(String delApplyNo) {
		this.delApplyNo = delApplyNo;
	}


	/**
	 * @return viewServiceBean
	 */
	public ChaLibMasterDelEntryViewServiceBean getViewServiceBean() {
		return viewServiceBean;
	}


	/**
	 * @param viewServiceBean 設定する viewServiceBean
	 */
	public void setViewServiceBean(
			ChaLibMasterDelEntryViewServiceBean viewServiceBean) {
		this.viewServiceBean = viewServiceBean;
	}


	public String getLotNo() {
		return lotId;
	}


	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

}
