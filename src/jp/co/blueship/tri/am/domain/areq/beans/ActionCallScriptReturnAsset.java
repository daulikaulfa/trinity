package jp.co.blueship.tri.am.domain.areq.beans;

import static jp.co.blueship.tri.fw.cmn.utils.TriDateUtils.*;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.constants.ChaLibCallScriptItemID;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.agent.ShellExecute;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AppendFileConstID;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請に外部スクリプトを呼び出します。
 *
 * @author kanda
 *
 */
public class ActionCallScriptReturnAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	/**
	 * 設定できる引数の上限（cmd.exeの仕様）
	 */
	private final int MAX_PARAM = 9;

	private final String SEP = SystemProps.LineSeparator.getProperty();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			List<IMessageId> messageList = new ArrayList<IMessageId>();
			List<String[]> messageArgsList = new ArrayList<String[]>();
			/*
			 * 設定の読み込み
			 */
			// OFFの場合return
			if ( StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.callScriptAtReturnApply,
							ChaLibCallScriptItemID.SummaryAtReturnApply.DO_EXEC.toString() )) ) {
				return serviceDto;

			}
			// スクリプトパス（絶対パス）
			String script = sheet.getValue(
					AmDesignBeanId.callScriptAtReturnApply,
					ChaLibCallScriptItemID.SummaryAtReturnApply.SCRIPT.toString() );
			File scriptFile = new File(script);
			// 存在チェック
			if( false == scriptFile.exists() ) {
				messageList.add( AmMessageId.AM001064E );
				messageArgsList.add( new String[] {scriptFile.getAbsolutePath()} );
			} else {
				// ファイルチェック
				if( false == scriptFile.isFile() ) {
					messageList.add( AmMessageId.AM001065E );
					messageArgsList.add( new String[] {scriptFile.getAbsolutePath()} );
				}
			}

			// 実行ディレクトリ（絶対パス）
			String dir = sheet.getValue(
					AmDesignBeanId.callScriptAtReturnApply,
					ChaLibCallScriptItemID.SummaryAtReturnApply.DIR.toString() );
			File execDir = new File(dir);
			// 存在チェック
			if( false == execDir.exists() ) {
				messageList.add( AmMessageId.AM001066E );
				messageArgsList.add( new String[] {execDir.getAbsolutePath()} );
			} else {
				// ディレクトリチェック
				if( false == execDir.isDirectory() ) {
					messageList.add( AmMessageId.AM001067E );
					messageArgsList.add( new String[] {execDir.getAbsolutePath()} );
				}
			}

			// 呼び出し時に設定可能な引数
			Map<String, String> callScriptAtReturnApplyParamDefineMap =
					sheet.getKeyMap(AmDesignBeanId.callScriptAtReturnApplyParam);
			// 数のチェック（９個まで）
			if( MAX_PARAM < callScriptAtReturnApplyParamDefineMap.size() ) {
				messageList.add( AmMessageId.AM001068E );
				messageArgsList.add(new String[] {
						Integer.toString(callScriptAtReturnApplyParamDefineMap
								.size()), Integer.toString(MAX_PARAM) });
			}

			// スクリプト終了コード
			Map<String, String> callScriptAtReturnApplyErrCdDefineMap =
					sheet.getKeyMap(AmDesignBeanId.callScriptAtReturnApplyErrCd);
			Map<Integer, String> callScriptAtReturnApplyErrCdMap4Use = new HashMap<Integer, String>();

			for (Entry<String, String> errCdEntry : callScriptAtReturnApplyErrCdDefineMap.entrySet()) {

				String errCdKey = errCdEntry.getKey();
				String errCdVal = errCdEntry.getValue();

				if( ChaLibCallScriptItemID.ErrCdAtReturnApply.OTHER.toString().equals(errCdKey) ) {
					continue;
				}
				// 文字種チェック（other以外は数値）
				if( false == TriStringUtils.isDigits(errCdKey) ) {
					messageList.add( AmMessageId.AM001069E );
					messageArgsList.add( new String[] {errCdKey} );
					continue;
				}
				int errCd = Integer.valueOf(errCdKey);
				// 使用できないコード指定（0、999）
				if( 0 == errCd || ShellExecute.DEFAULT_ERROR_LEVEL == errCd ) {
					messageList.add( AmMessageId.AM001069E );
					messageArgsList.add( new String[] {errCdKey} );
					continue;
				}
				callScriptAtReturnApplyErrCdMap4Use.put(errCd, errCdVal);
			}


			/*
			 * 引数辞書の読み替え
			 */
			// 申請番号から上位エンティティを検索してストック
			List<Object> entityList = new ArrayList<Object>();
			IAreqDto areqDto =  AmExtractEntityAddonUtils.extractAssetApply(paramList);
			entityList.add( areqDto );
			IPjtEntity pjtEntity = support.findPjtEntity(areqDto.getAreqEntity().getPjtId());
			entityList.add( pjtEntity );
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );
			entityList.add( lotDto );
			IPjtAvlEntity pjtAvlEntity = support.findPjtAvlEntity(pjtEntity.getPjtId(), pjtEntity.getPjtLatestAvlSeqNo());
			entityList.add( pjtAvlEntity );

			// 引数設定を順番どおりに取得
			LinkedHashMap<String, String> sortedParamMap = new LinkedHashMap<String, String>( callScriptAtReturnApplyParamDefineMap );
			List<String> sortedParams = new ArrayList<String>(sortedParamMap.values());

			// 引数辞書を取得
			HashMap<String, String> itemMap = DesignUtils.getItemMap();

			// 引数を連結したコマンド
			StringBuilder command = new StringBuilder(script);

			for (String param : sortedParams) {

				String itemKey = this.getItemKeyByName(itemMap, param);
				// 辞書検索に失敗
				if( null == itemKey ) {
					messageList.add( AmMessageId.AM001070E );
					messageArgsList.add( new String[] {param} );
					continue;
				}
				String param4Use = this.getEntityData(entityList, itemKey);
				if( null == param4Use ) {
					param4Use = this.getOptionData(paramList, itemKey);
				}
				// 値検索に失敗
				if( null == param4Use ) {
					messageList.add( AmMessageId.AM001071E );
					messageArgsList.add( new String[] {param} );
					continue;
				}

				// 改行コードをつぶす
				param4Use = param4Use.replace(SEP, " ").replace("\n", " ").replace("\r", " ");

				command.append(" \"").append(param4Use).append("\"");
			}

			// この時点でエラーがあればthrow
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );


			/*
			 * スクリプト実行
			 */
			ShellExecute shellExecute = new ShellExecute() ;
			shellExecute.setLog( TriLogFactory.getInstance() ) ;
			shellExecute.setExecShellPath( command.toString() ) ;
			shellExecute.setWorkDir( dir ) ;
			try {
				shellExecute.execute() ;
			} catch (Exception e) {
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException(AmMessageId.AM005054S , e);
			}

			// 終了コードの割り当てとメッセージング
			int exitValue = shellExecute.getExitValue();
			if( 0 == exitValue ) {

				// 正常終了（何もしない）
			} else if (ShellExecute.DEFAULT_ERROR_LEVEL == exitValue) {

				// 999のときは標準エラーを出しておく
				// 標準出力はINFOレベルでログに出力される
				LogHandler.warn(log, shellExecute.getErrLog());

			} else {

				// 設定されたメッセージ
				if( callScriptAtReturnApplyErrCdMap4Use.containsKey(exitValue) ) {

					String message = sheet.getValue(
							AmDesignBeanId.callScriptAtReturnApplyErrCd,
							Integer.toString(exitValue));
					messageList.add( AmMessageId.AM001072E );
					messageArgsList.add( new String[] {Integer.toString(exitValue), message} );
				} else {

					// その他のメッセージ
					if( callScriptAtReturnApplyErrCdDefineMap.containsKey(ChaLibCallScriptItemID.ErrCdAtReturnApply.OTHER.toString()) ) {

						String message = sheet.getValue(
								AmDesignBeanId.callScriptAtReturnApplyErrCd,
								ChaLibCallScriptItemID.ErrCdAtReturnApply.OTHER.toString());
						messageList.add( AmMessageId.AM001072E );
						messageArgsList.add( new String[] {Integer.toString(exitValue), message} );
					}
					// 設定されていない終了コード
					else {

						messageList.add( AmMessageId.AM001073E );
						messageArgsList.add( new String[] {Integer.toString(exitValue)} );
					}
				}

			}

			if ( 0 != messageList.size() ){
				throw new ContinuableBusinessException( messageList, messageArgsList );
			}
		} finally {
		}

		return serviceDto;
	}

	/**
	 *
	 * @param itemMap
	 * @param itemName
	 * @return 見つからなかった場合はnull
	 */
	private String getItemKeyByName(HashMap<String, String> itemMap, String itemName) {

		if( null == itemMap
				|| TriStringUtils.isEmpty(itemName) ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		if( false == itemMap.containsValue(itemName) ) {
			// 見つからなかったら探さない
			return null;
		}

		for (Entry<String, String> entry : itemMap.entrySet()) {

			if( itemName.equals(entry.getValue()) ) {

				// 最初に見つかったキーを返す
				return entry.getKey();
			}
		}
		return null;
	}

	/**
	 *
	 * @param itemKey
	 * @param entityList
	 * @return 見つからなかった場合はnull
	 */
	private String getEntityData(List<Object> entityList, String itemKey) {

		if( null == entityList
				|| TriStringUtils.isEmpty(itemKey) ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		String[] itemKeyFragment = itemKey.split("\\.");
		if( 2 > itemKeyFragment.length ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		String entity = itemKeyFragment[0];
		String item = itemKeyFragment[1];
		if( TriStringUtils.isEmpty(entity)
				|| TriStringUtils.isEmpty(item) ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		Object entityObj = null;
		if (AmTables.AM_AREQ.name().equals(entity)) {

			entityObj = AmExtractEntityAddonUtils.extractAssetApply(entityList).getAreqEntity();
		} else if(AmTables.AM_PJT.name().equals(entity)) {
			entityObj = AmExtractEntityAddonUtils.extractPjt(entityList);
		} else if(AmTables.AM_PJT_AVL.name().equals(entity)) {

			entityObj = AmExtractEntityAddonUtils.extractPjtApprove(entityList);
		} else if(AmTables.AM_LOT.name().equals(entity)) {

			entityObj = AmExtractEntityAddonUtils.extractPjtLot(entityList).getLotEntity();
		} else if(AmTables.AM_LOT_MDL_LNK.name().equals(entity)) {

			entityObj = AmExtractEntityAddonUtils.extractPjtLot(entityList).getLotMdlLnkEntities().get(0);
		}

		if( null == entityObj ) {
			// 見つからなかったら探さない
			return null;
		}

		Object data = null;
		if( TriPropertyUtils.isReadable(entityObj, item) ) {
			data = TriPropertyUtils.getProperty(entityObj, item);
		}

		if( data instanceof String ) {
			return (String)data;
		} else if( data instanceof StatusFlg ) {
				return ((StatusFlg)data).toString();
		} else if( data instanceof Timestamp ) {
			return convertViewDateFormat((Timestamp)data);
		} else {
			// 見つからなかった場合
			return null;
		}
	}

	/**
	 *
	 * @param itemKey
	 * @param entityList
	 * @return 見つからなかった場合はnull
	 */
	private String getOptionData(List<Object> entityList, String itemKey) {

		if( null == entityList
				|| TriStringUtils.isEmpty(itemKey) ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		if( false == itemKey.startsWith("option.") ) {
			// 見つからなかった場合と同じにする
			return null;
		}

		/*
		 * 返却資産申請画面添付ファイル
		 */
		if( "option.returnAssetAppendFile".equals(itemKey) ) {

			for ( Iterator<Object> it = entityList.iterator(); it.hasNext(); ) {
				Object obj = it.next();

				if ( obj instanceof FlowChaLibRtnEntryServiceBean ) {
					FlowChaLibRtnEntryServiceBean bean = (FlowChaLibRtnEntryServiceBean)obj;

					String returnAssetAppendFile = bean.getReturnAssetAppendFile();

					if( null == returnAssetAppendFile ) {
						// データがない場合
						return "";
					}

					String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

					// 申請番号フォルダをつなげる
					String applyNo = bean.getApplyNo() ;
					String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

					// 最新のユニークのフォルダをつなげる
					String uniqueKey = "";
					String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

					// 添付ファイルごとのフォルダをつなげる
					String append1 = AppendFileConstID.File.append1.value() ;
					String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

					File file = new File(appendPath1, returnAssetAppendFile);

					if(TriStringUtils.isEmpty(file)) {
						// ファイルが登録されていない場合
						return "";
					}
					if(false == file.exists()) {
						// ファイルがない場合
						return "";
					}
					return file.getAbsolutePath();

				}
			}
		}

		// 見つからなかった場合
		return null;
	}

}
