package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.dao.ext.IExtDao;
import jp.co.blueship.tri.am.dao.ext.eb.ExtCondition;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;


/**
 * 資産ファイルの拡張子の妥当性を行います。
 * <br>拡張子テーブルに定義された拡張子のいずれかに該当するかどうかをチェックします。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ExtensionSimpleValidator implements IExtensionValidator {

	private IExtDao ucfExtensionDao = null;
	public void setExtDao( IExtDao ucfExtensionDao ) {
		this.ucfExtensionDao = ucfExtensionDao;
	}

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * <br>返却資産申請の妥当性チェックで使用されます。
	 * @param projectId プロジェクトＩＤ
	 * @param file 対象となるディレクトリパス
	 * @throws ContinuableBusinessException
	 */

	public final void validate( File file ) throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {
			List<String> files = TriFileUtils.getFilePaths(file, TriFileUtils.TYPE_FILE, BusinessFileUtils.getFileFilter());

			if ( 0 == files.size() )
				throw new BusinessException( AmMessageId.AM001047E );

			Map<String,String> extensionMap = this.getExtensionMap();

			this.validateExtension(
						messageList,
						messageArgsList,
						file,
						files,
						extensionMap );

			if ( 0 != messageList.size() ){
				throw new ContinuableBusinessException( messageList, messageArgsList );
			}
		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005017S, e );
		}
	}

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * <br>削除申請された資産の妥当性チェックで使用されます。
	 * @param projectId プロジェクトＩＤ
	 * @param file 親フォルダパス
	 * @param files 対象となる相対ファイルパス
	 * @throws ContinuableBusinessException
	 */

	public void validate( File file, String[] files ) throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {
			Map<String,String> extensionMap = this.getExtensionMap();

			this.validateExtension(
						messageList,
						messageArgsList,
						file,
						FluentList.from(files).asList(),
						extensionMap );

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

		} finally {
		}
	}

	/**
	 * 言語種別ごとの拡張子テーブルを取得し、言語種別ごとにマップに格納します。
	 * @return 取得したマップを戻します。
	 */
	private final Map<String,String> getExtensionMap() {

		ExtCondition condition = AmDBSearchConditionAddonUtils.getAllExtensionInfo();
		ISqlSort sort = new SortBuilder();
		List<IExtEntity> extEntities = this.ucfExtensionDao.find( condition.getCondition(), sort );

		Map<String,String> map = new HashMap<String,String>();

		for ( IExtEntity entity : extEntities ) {
			//map.put( extension.toUpperCase(), extension );
			map.put( entity.getExt(), entity.getExt() );
		}

		return map;
	}

	/**
	 * 資産ファイルの拡張子の妥当性チェックを行います。
	 * @param file 親フォルダパス
	 * @param files 相対パスリスト
	 * @param extensionMap 拡張子リストのマップ
	 */

	private final void validateExtension(
							List<IMessageId> messageList,
							List<String[]> messageArgsList,
							File file,
							List<String> files,
							Map<String,String> extensionMap ) {

		Set<String> errorExtension = new HashSet<String>();

		for ( Iterator<String> it = files.iterator(); it.hasNext(); ) {
			String path = it.next();

			String fileName = (new File( file, path )).getName();

			String extension = TriStringUtils.getExtension( fileName );

			if ( null == extension )
				continue;

			if ( extensionMap.containsKey( extension ) )
				continue;

			if ( errorExtension.contains( extension ) )
				continue;


			messageList.add( AmMessageId.AM001048E );
			messageArgsList.add( new String[] { extension } );

			errorExtension.add( extension );
		}
	}

}
