package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * FlowChaLibMasterDelEntryServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除申請完了画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMasterDelEntryServiceComp implements IDomain<FlowChaLibMasterDelEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelEntryServiceBean> execute( IServiceDto<FlowChaLibMasterDelEntryServiceBean> serviceDto) {

		FlowChaLibMasterDelEntryServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			// 業務ロジックを記述
			String forward = paramBean.getForward();

			if ( forward.equals( ChaLibScreenID.COMP_MASTER_DEL_ENTRY ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					this.executeForward( paramBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 原本削除申請完了画面への遷移
	 *
	 * @param paramBean
	 */
	private final void executeForward(FlowChaLibMasterDelEntryServiceBean paramBean) throws Exception {
		//	削除申請登録処理
		IAreqDto areqDto = new AreqDto();
		IAreqEntity areqEntity = new AreqEntity();
		IPjtEntity pjtEntity = this.support.findPjtEntity( paramBean.getViewServiceBean().getPrjNo() );
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		List<IAreqFileEntity> fileEntities = new ArrayList<IAreqFileEntity>();
		List<IAreqBinaryFileEntity> binaryFileEntities = new ArrayList<IAreqBinaryFileEntity>();
		setEntity( paramBean, areqEntity, fileEntities, binaryFileEntities, lotDto.getLotEntity(), areqDto );


		// グループの存在チェック
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		//下層のアクションを実行する
		IDelApplyParamInfo actionParam = new DelApplyParamInfo();
		{
			List<Object> paramList = new ArrayList<Object>();

			actionParam.setUserId( paramBean.getUserId() );
			actionParam.setDelApplyUser( paramBean.getUserName() );
			actionParam.setDelApplyUserId( paramBean.getUserId() );

			paramList.add( areqEntity );
			paramList.add( areqDto );
			paramList.add( fileEntities );
			paramList.add( binaryFileEntities );
			paramList.add( pjtEntity );
//			paramList.add( lotEntity );
			paramList.add( lotDto );
			paramList.add( actionParam );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
			if( VcsCategory.SVN.equals( scmType ) ) {

				List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
				paramList.add( reposEntities.toArray( new IVcsReposEntity[0] ) ) ;

				IPassMgtEntity passwordEntity = this.support.findPassMgtEntityBySVN();
				paramList.add( passwordEntity ) ;

			} else {
				throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
			}
			for ( IDomain<IGeneralServiceBean> action : actions ) {
				action.execute( innerServiceDto );
			}
		}
		paramBean.setLotNo( pjtEntity.getLotId() );
		paramBean.setApplyNo( areqEntity.getAreqId() );
		paramBean.setApplyDate( TriDateUtils.convertViewDateFormat( areqEntity.getDelReqTimestamp() ) );
	}

	private final void setEntity(
			FlowChaLibMasterDelEntryServiceBean paramBean,
			IAreqEntity entity,
			List<IAreqFileEntity> fileEntities,
			List<IAreqBinaryFileEntity> binaryFileEntities,
			ILotEntity lotEntity,
			IAreqDto areqDto) {

		paramBean.setSystemDate( TriDateUtils.getSystemTimestamp() );

		entity.setAreqCtgCd			( AreqCtgCd.RemovalRequest.value() );
		entity.setStsId				( AmAreqStatusId.RemovalRequested.getStatusId() );
		entity.setDelStsId			( StatusFlg.off);
		entity.setDelReqUserNm		( paramBean.getUserName() );
		entity.setDelReqUserId		( paramBean.getUserId() );
		entity.setDelReqTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setPjtId				( paramBean.getViewServiceBean().getPrjNo() );
		entity.setSummary			( paramBean.getViewServiceBean().getDelApplySubject() );
		entity.setContent			( paramBean.getViewServiceBean().getDelReason() );
		entity.setGrpId				( this.support.getUmFinderSupport().findGroupByName( paramBean.getViewServiceBean().getUserGroup() ).getGrpId());
		entity.setGrpNm				( paramBean.getViewServiceBean().getUserGroup() );

		for ( String path : paramBean.getViewServiceBean().getApplyFilePath() ) {
			path = FlowChaLibMasterDelEditSupport.convertPath( path );

			IAreqFileEntity assetFileEntity = new AreqFileEntity();
			assetFileEntity.setAreqCtgCd( entity.getAreqCtgCd() );
			assetFileEntity.setFilePath( path );

			fileEntities.add( assetFileEntity );
		}

		for ( String path : paramBean.getViewServiceBean().getApplyBinaryFilePath() ) {
			path = FlowChaLibMasterDelEditSupport.convertPath( path );

			IAreqBinaryFileEntity assetFileEntity = new AreqBinaryFileEntity();
			assetFileEntity.setAreqCtgCd( entity.getAreqCtgCd() );
			assetFileEntity.setFilePath( path );

			binaryFileEntities.add( assetFileEntity );
		}
		areqDto.setAreqEntity(entity);
		areqDto.setAreqFileEntities(fileEntities);
		areqDto.setAreqBinaryFileEntities(binaryFileEntities);
	}

}
