package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 返却情報申請の申請単位に原本作業フォルダの構成と同一の空フォルダを自動的に作成します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionCopyMasterWorkDirToReturnInfoDir extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			//貸出時、返却フォルダに自動的に空フォルダを作成するかの設定
			String makeEmptyFolderForReturnInfo =
					sheet.getValue( AmDesignEntryKeyByChangec.makeEmptyFolderForReturnInfo ) ;
			if( TriStringUtils.isEmpty( makeEmptyFolderForReturnInfo ) ) {
				throw new TriSystemException( AmMessageId.AM004025F ) ;
			}
			if( StatusFlg.on.value().equals( makeEmptyFolderForReturnInfo ) ) {

				File root = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

				List<?> files = TriFileUtils.getFilePaths(
								AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ),
								TriFileUtils.TYPE_DIR,
								AmDesignBusinessRuleUtils.getNeglectFileFilter( AmExtractEntityAddonUtils.extractPjtLot( paramList ) ) );

				for ( Iterator<?> it = files.iterator(); it.hasNext(); ) {
					String filePath = (String)it.next();

					new File( root, filePath ).mkdirs();
				}
			}

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005122S, e );
		}

		return serviceDto;
	}
}
