package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;



/**
 * FlowChaLibLendModifyイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出変更画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendModifyServiceLendModify implements IDomain<FlowChaLibLendModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> actions4InputFile = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setActions4InputFile( List<IDomain<IGeneralServiceBean>> actions4InputFile ) {
		this.actions4InputFile = actions4InputFile;
	}

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute( IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto ) {

		FlowChaLibLendModifyServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LEND_MODIFY ) &&
					!forward.equals( ChaLibScreenID.LEND_MODIFY )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.LEND_MODIFY)) {
				executeForward(paramBean);
			}

			if (referer.equals(ChaLibScreenID.LEND_MODIFY)) {
				executeReferer(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}
	}

	/**
	 * 貸出申請画面への遷移
	 *
	 * @param paramBean
	 */
	private final void executeForward(FlowChaLibLendModifyServiceBean paramBean) {

		ChaLibEntryBaseInfoBean baseInfoBean = null;

		IAreqDto areqDto = this.support.findAreqDto(  paramBean.getApplyNo()  );
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() ) ;


		// グループの存在チェック
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
			baseInfoBean = AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply( pjtEntity , areqDto );
		} else {
			baseInfoBean = AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply( areqDto, paramBean.getInBaseInfoBean() );
		}
        //for case v4 create draft checkout, then user try to edit in v3
		this.chkLendApplyDir( lotDto, areqDto ) ;
		paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;
		paramBean.setBaseInfoViewBean( baseInfoBean );
	}

	/**
	 * 貸出申請画面からの遷移
	 *
	 * @param paramBean
	 */

	private final void executeReferer(FlowChaLibLendModifyServiceBean paramBean) {

		ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

		ChaLibEntryBaseInfoBean baseInfoBean = new ChaLibEntryBaseInfoBean();

		IPjtEntity pjtEntity = this.support.findPjtEntity( inBean.getInPrjNo() );
		//	入力項目をセット

		baseInfoBean.setPrjNo			( inBean.getInPrjNo() );

		baseInfoBean.setGroupName		( inBean.getInGroupName() );
		baseInfoBean.setSubject			( inBean.getInSubject() );
		baseInfoBean.setContent			( inBean.getInContent() );
		baseInfoBean.setApplyUser		( inBean.getInUserName() );
		baseInfoBean.setApplyUserId		( inBean.getInUserId() );

		baseInfoBean.setChangeCauseNo	( pjtEntity.getChgFactorNo() ) ;

		baseInfoBean.setLotNo			( pjtEntity.getLotId() );

		paramBean.setLotNo( pjtEntity.getLotId() ) ;
		paramBean.setBaseInfoViewBean( baseInfoBean );

		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );
		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() )) {

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_MODIFY_CONFIRM.equals( paramBean.getForward() )) {
				FlowChaLibLendEditSupport.saveLendApplyDefUploadFile(
						paramBean.getInBaseInfoBean(), paramBean.getUserId() );
			}

			List<Object> paramList = new ArrayList<Object>();

			IEhLendParamInfo param = new EhLendParamInfo();
			this.support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);
			paramList.add( param );
			paramList.add( lotDto );
			paramList.add( this.support.findAreqDto( paramBean.getApplyNo() ) );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_MODIFY_CONFIRM.equals( paramBean.getForward() )) {
				param.setInApplyFilePath( inBean.getLendApplyFilePath() );
				FlowChaLibLendEditSupport.check4InputFile( inBean );
			}

			for ( IDomain<IGeneralServiceBean> action : this.actions ) {
				action.execute( innerServiceDto );
			}

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_MODIFY_CONFIRM.equals( paramBean.getForward() )) {

				// ２重貸出チェック
				param.setDuplicationCheck	( ! lotDto.getLotEntity().getIsAssetDup().parseBoolean() );
				param.setUserId				( paramBean.getUserId() );

				for ( IDomain<IGeneralServiceBean> action : this.actions4InputFile ) {
					action.execute( innerServiceDto );
				}

				// 確認画面への遷移対応
				ChaLibEntryAssetResourceInfoBean assetResourceBean = new ChaLibEntryAssetResourceInfoBean();
				File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );
				assetResourceBean.setMasterPath	( TriStringUtils.convertPath( masterPath ));
				paramBean.setAssetSelectViewBean	( assetResourceBean );

				ChaLibInputLendEntryAssetSelectBean inputBean = new ChaLibInputLendEntryAssetSelectBean();
				inputBean.setInAssetResourcePath(
						FlowChaLibLendEditSupport.getLendAssetPath( masterPath, paramList ));
				paramBean.setInAssetSelectBean	( inputBean );

				// 変更管理番号からリンクする変更要因番号をセットする
				if ( null != pjtEntity ) {
					paramBean.getInBaseInfoBean().setInChangeCauseNo( pjtEntity.getChgFactorNo() );
				}
			}
		}

	}

	/**
	 * 貸出フォルダの存在有無をチェック、存在しなければ空フォルダを作成する。
	 * @param lotDto
	 * @param areqDto
	 */
	private void chkLendApplyDir( ILotDto lotDto , IAreqDto areqDto ) {

		List<Object> list = new ArrayList<Object>();
		list.add( lotDto );
		list.add( areqDto );
		File lendApplyPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( list );
		if( ! lendApplyPath.exists() ) {
			lendApplyPath.mkdirs() ;
		}
	}

}
