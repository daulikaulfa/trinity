package jp.co.blueship.tri.am.domain.areq;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.beans.mail.dto.AssetApplyMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibLendEntryイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出編集完了時のメール送信処理を行う。
 * </p>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibLendModifyServiceMail implements IDomain<FlowChaLibLendModifyServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowChaLibLendEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute( IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto ) {

		FlowChaLibLendModifyServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.COMP_LEND_MODIFY ) &&
					!forward.equals( ChaLibScreenID.COMP_LEND_MODIFY )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.COMP_LEND_MODIFY)) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					List<Object> paramList = new CopyOnWriteArrayList<Object>();
					paramList.add( paramBean );	//インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）

					//基礎情報のコピー
					AssetApplyMailServiceBean successMailBean = new AssetApplyMailServiceBean();
					TriPropertyUtils.copyProperties(successMailBean, paramBean);
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					IAreqEntity assetApplyEntity = this.support.findAreqEntity( paramBean.getApplyNo() );
					IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() );
					ILotEntity pjtLotEntity = this.support.findLotEntity( pjtEntity.getLotId() );
					List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities( paramBean.getApplyNo(), AreqCtgCd.LendingRequest );

					IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

					successMailBean.setAssetApplyEntity( assetApplyEntity );
					successMailBean.setPjtEntity( pjtEntity );
					successMailBean.setLotEntity( pjtLotEntity );
					successMailBean.setLendApplyFileList( areqFileEntities );
					successMailBean.setBldSrvId(srvEntity.getBldSrvId());
					successMailBean.setBldSrvNm(srvEntity.getBldSrvNm());

					successMail.execute( mailServiceDto );

				}
			}

			if (referer.equals(ChaLibScreenID.COMP_LEND_MODIFY)) {
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , paramBean.getFlowAction() ));
		}

		return serviceDto;

	}

}
