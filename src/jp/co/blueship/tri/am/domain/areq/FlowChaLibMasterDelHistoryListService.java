package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelHistoryListServiceBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopViewServiceBean.TopMenuViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;



/**
 * FlowChaLibMasterDelHistoryListServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除履歴一覧の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMasterDelHistoryListService implements IDomain<FlowChaLibMasterDelHistoryListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelHistoryListServiceBean> execute( IServiceDto<FlowChaLibMasterDelHistoryListServiceBean> serviceDto ) {

		FlowChaLibMasterDelHistoryListServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();


			if ( !TriStringUtils.isEmpty( selectedLotNo )) {
				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( selectedLotNo );

				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
			}


			List<LotViewBean> lotViewBeanList = this.support.getLotViewBeanForPullDown( paramBean );

			paramBean.setLotViewBeanList(lotViewBeanList);
			paramBean.setSelectedLotNo(selectedLotNo);
			IJdbcCondition condition	= getCondition( paramBean );
			ISqlSort sort			= AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibMasterDelHistoryList();
			int selectPageNo		= ( (0 == paramBean.getSelectPageNo())? 1: paramBean.getSelectPageNo() );
			int maxPageNumber		= sheet.intValue( AmDesignEntryKeyByChangec.maxPageNumberByMasterDelHistoryList );

			IEntityLimit<IAreqEntity> entityLimit = null;

			if ( ! TriStringUtils.isEmpty(selectedLotNo) ) {
				entityLimit =  this.support.getAreqDao().find( condition.getCondition(), sort, selectPageNo ,maxPageNumber );
			}

			setServiceBeanSearchResult	( paramBean, entityLimit );
			paramBean.setSelectPageNo(selectPageNo);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}
	}

	private final IJdbcCondition getCondition(FlowChaLibMasterDelHistoryListServiceBean paramBean) {

		AreqCondition condition = new AreqCondition();

		String[] baseStatus = {
					AmAreqStatusId.RemovalRequestApproved.getStatusId(),
					AmAreqStatusId.Merged.getStatusId(),
					AmAreqStatusId.RemovalRequestRemoved.getStatusId(),
					AmAreqStatusId.PendingRemovalRequestRemoved.getStatusId(),
					AmAreqStatusId.DraftRemovalRequestRemoved.getStatusId(),

					};

		condition.setStsIds( baseStatus );
		condition.setDelReqUserNm(paramBean.getUserName());
		condition.setDelReqUserIds( new String[]{ paramBean.getUserId() } );
		condition.setDelStsId( null );
		condition.setLotId(paramBean.getSelectedLotNo());

		return condition;
	}

	private final void setServiceBeanSearchResult(
			FlowChaLibMasterDelHistoryListServiceBean paramBean, IEntityLimit<IAreqEntity> entityLimit ) {

		List<TopMenuViewBean> list = new ArrayList<TopMenuViewBean>();

		ILimit limit = null;

		if ( null != entityLimit ) {
			IEntityLimit<IAreqEntity> assetEntityLimit =entityLimit;

			IAreqEntity[] entitys = assetEntityLimit.getEntities().toArray(new IAreqEntity[0]);

//			処理高速化のために、Pjtをまとめて取得し、Mapで受け取る
			String[] pjtNoArray = this.support.getPjtNoArrayFromAssetApplyArray( entitys ) ;
			Map<String, IPjtEntity> pjtEntityMap = this.support.getPjtEntityPjtNoMap( pjtNoArray ) ;

			for ( IAreqEntity assetApplyEntity : entitys ) {
				TopMenuViewBean viewBean	= paramBean.newTopMenuViewBean();

				IPjtEntity pjtEntity = pjtEntityMap.get( assetApplyEntity.getPjtId() ) ;
				if ( null == pjtEntity ) {
					throw new TriSystemException(AmMessageId.AM004017F , assetApplyEntity.getPjtId() );
				}
				AmViewInfoAddonUtils.setChaLibListViewBeanAssetDelApplyEntity( viewBean, pjtEntity , assetApplyEntity );

				list.add( viewBean );
			}

			limit = assetEntityLimit.getLimit();
		}

		paramBean.setTopMenuViewBeanList(list);
		paramBean.setPageInfoView( AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit) ) ;

	}
}
