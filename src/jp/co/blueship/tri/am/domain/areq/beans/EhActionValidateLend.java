package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.constants.ChaLibScreenItemID;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 貸出情報申請の妥当性チェックを行います。
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class EhActionValidateLend extends ActionPojoAbstract<IGeneralServiceBean> {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IAmFinderSupport support = null;
	private IUmFinderSupport umSupport = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( IAmFinderSupport amSupport ) {
		support = amSupport;
		umSupport = support.getUmFinderSupport();
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );

		if ( null == param ){
			throw new TriSystemException( AmMessageId.AM005015S );
		}
		validate( param );

		return serviceDto;
	}

	/**
	 * 申請内容のチェックを行う。
	 * @param param 貸出申請パラメタ
	 * @throws ContinuableBusinessException
	 */

	public void validate( IEhLendParamInfo param )
									throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {

			// 変更管理番号の必須
			if( TriStringUtils.isEmpty( param.getPjtNo() ) ) {
				messageList.add		( AmMessageId.AM001038E );
				messageArgsList.add	( new String[] {} );
			}

			// 申請グループの必須
			if( TriStringUtils.isEmpty( param.getGroupName()) ) {
				messageList.add		( AmMessageId.AM001077E );
				messageArgsList.add	( new String[] {} );
			}

			// 申請ユーザ名の必須
			if( TriStringUtils.isEmpty( param.getLendApplyUser()) ) {
				messageList.add		( AmMessageId.AM001039E );
				messageArgsList.add	( new String[] {} );
			}

			// 申請件名の必須
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibLendEntryCheck,
							ChaLibScreenItemID.FlowChaLibLendEntryCheck.SUMMARY.toString() )) ) {
				if (!param.isDraff() && TriStringUtils.isEmpty( param.getSummary()) ) {
					messageList.add		( AmMessageId.AM001040E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//申請内容の必須
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.flowChaLibLendEntryCheck,
							ChaLibScreenItemID.FlowChaLibLendEntryCheck.CONTENT.toString() )) ) {
				if (!param.isDraff() && TriStringUtils.isEmpty( param.getContent()) ) {
					messageList.add		( AmMessageId.AM001041E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if (TriStringUtils.isNotEmpty(param.getCtgId())) {
				ICtgEntity ctgEntity = umSupport.findCtgByPrimaryKey(param.getCtgId());
				if (null == ctgEntity) {
					messageList.add		( AmMessageId.AM001130E );
					messageArgsList.add	( new String[] {param.getCtgId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(param.getMstoneId())) {
				IMstoneEntity mstoneEntity = umSupport.findMstoneByPrimaryKey(param.getMstoneId());
				if (null == mstoneEntity) {
					messageList.add		( AmMessageId.AM001131E );
					messageArgsList.add	( new String[] {param.getMstoneId()} );
				}
			}

			if (TriStringUtils.isNotEmpty(param.getAssigneeId())) {
				String groupId = param.getGroupId();
				if (TriStringUtils.isNotEmpty(groupId)) {
					IGrpUserLnkEntity grpUserLnkEntity = umSupport.findGrpUserLnkByPrimaryKey(groupId, param.getAssigneeId());

					if (null == grpUserLnkEntity) {
						messageList.add		( AmMessageId.AM001133E );
						messageArgsList.add	( new String[] {groupId} );
					}
				}

			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}

}
