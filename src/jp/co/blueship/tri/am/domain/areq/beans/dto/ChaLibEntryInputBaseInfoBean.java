package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ChaLibEntryInputBaseInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;



	/**
	 * 入力ロット番号
	 */
	private String inLotNo;
	/**
	 * 入力変更管理番号
	 */
	private String inPrjNo;
	/**
	 * 入力変更要因番号
	 */
	private String inChangeCauseNo;
	/**
	 * 入力申請件名
	 */
	private String inSubject;
	/**
	 * 入力申請内容
	 */
	private String inContent;
	/**
	 * 入力申請ユーザー
	 */
	private String inUserName;
	/**
	 * 入力申請ユーザーＩＤ
	 */
	private String inUserId;
	/**
	 * 入力申請ユーザーグループID
	 */
	private String inGroupId;
	/**
	 * 入力申請ユーザーグループ
	 */
	private String inGroupName;

	/** 貸出申請モード */
	private String lendApplyMode;

	/** 貸出申請ファイル名 */
	private String lendApplyFileName;

	/** 貸出申請ファイルパス */
	private String lendApplyFilePath;

	/** 貸出申請ファイルストリーム */
	private byte[] lendApplyInputStreamBytes;

	private String assigneeId;

	private String assigneeNm;

	private String checkinDueDate;

	private String ctgId;

	private String mstoneId;
	
	private boolean isDraff = false;

	public boolean isDraff() {
		return isDraff;
	}
	public void setDraff(boolean isDraff) {
		this.isDraff = isDraff;
	}
	public String getInLotNo() {
		return inLotNo;
	}
	public void setInLotNo(String inLotNo) {
		this.inLotNo = inLotNo;
	}
	public String getInContent() {
		return inContent;
	}
	public void setInContent(String inContent) {
		this.inContent = inContent;
	}
	/**
	 * 入力申請ユーザーグループIDを取得します。
	 * @return 入力申請ユーザーグループID
	 */
	public String getInGroupId() {
	    return inGroupId;
	}
	/**
	 * 入力申請ユーザーグループIDを設定します。
	 * @param inGroupId 入力申請ユーザーグループID
	 */
	public void setInGroupId(String inGroupId) {
	    this.inGroupId = inGroupId;
	}
	public String getInGroupName() {
		return inGroupName;
	}
	public void setInGroupName(String inGroupName) {
		this.inGroupName = inGroupName;
	}
	public String getInPrjNo() {
		return inPrjNo;
	}
	public void setInPrjNo(String inPrjNo) {
		this.inPrjNo = inPrjNo;
	}
	public String getInSubject() {
		return inSubject;
	}
	public void setInSubject(String inSubject) {
		this.inSubject = inSubject;
	}
	public String getInUserName() {
		return inUserName;
	}
	public void setInUserName(String inUserName) {
		this.inUserName = inUserName;
	}
	public String getInChangeCauseNo() {
		return inChangeCauseNo;
	}
	public void setInChangeCauseNo(String inChangeCauseNo) {
		this.inChangeCauseNo = inChangeCauseNo;
	}

	public String getLendApplyMode() {
		return lendApplyMode;
	}
	public void setLendApplyMode( String lendApplyMode ) {
		this.lendApplyMode = lendApplyMode;
	}

	public String getLendApplyFileName() {
		return lendApplyFileName;
	}
	public void setLendApplyFileName( String lendApplyFileName ) {
		this.lendApplyFileName = lendApplyFileName;
	}

	public String getLendApplyFilePath() {
		return lendApplyFilePath;
	}
	public void setLendApplyFilePath( String lendApplyFilePath ) {
		this.lendApplyFilePath = lendApplyFilePath;
	}

	public byte[] getLendApplyInputStreamBytes() {
		return lendApplyInputStreamBytes;
	}
	public void setLendApplyInputStreamBytes( byte[] lendApplyInputStreamBytes ) {
		this.lendApplyInputStreamBytes = lendApplyInputStreamBytes;
	}
	public String getInUserId() {
		return inUserId;
	}
	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}
	public String getAssigneeId() {
		return assigneeId;
	}
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public void setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
	}
	public String getCheckinDueDate() {
		return checkinDueDate;
	}
	public void setCheckinDueDate(String checkinDueDate) {
		this.checkinDueDate = checkinDueDate;
	}
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

}
