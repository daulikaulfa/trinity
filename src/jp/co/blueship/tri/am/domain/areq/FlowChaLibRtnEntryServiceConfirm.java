package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ReturnProcessParam;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.AppendFileConstID;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibRtnEntryServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 返却申請確認画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibRtnEntryServiceConfirm implements IDomain<FlowChaLibRtnEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;
	private List<IDomain<IGeneralServiceBean>> pojo = null;
	private List<IDomain<IGeneralServiceBean>> actions;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setPojo( List<IDomain<IGeneralServiceBean>> pojo ) {
		this.pojo = pojo;
	}

	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibRtnEntryServiceBean> execute(IServiceDto<FlowChaLibRtnEntryServiceBean> serviceDto) {

		FlowChaLibRtnEntryServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.RTN_ENTRY_CONFIRM ) &&
					!forward.equals( ChaLibScreenID.RTN_ENTRY_CONFIRM )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM)) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					paramBean = executeForward(paramBean);
				}
			}

			if (referer.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM)) {
				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
					if( paramBean.isStatusMatrixV3() ) {
						IAreqEntity assetApplyEntity = support.findAreqEntity( paramBean.getApplyNo() );
	
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setPjtIds( assetApplyEntity.getPjtId() )
						.setAreqIds( paramBean.getApplyNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				executeReferer(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 返却申請確認画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final FlowChaLibRtnEntryServiceBean executeForward( FlowChaLibRtnEntryServiceBean paramBean ) {

		IAreqEntity entity	= this.support.findAreqEntity( paramBean.getApplyNo() );
		IPjtEntity pjtEntity = this.support.findPjtEntity( entity.getPjtId() ) ;
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		// グループの存在チェック
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
				this.support.getUmFinderSupport().getGrpDao(),//
				this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
		innerServiceDto.setServiceBean( paramBean );

		for ( IDomain<IGeneralServiceBean> pojoObj : this.pojo ) {
			innerServiceDto = pojoObj.execute( innerServiceDto );
		}

		paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;

		//添付ファイルのアップロード
		boolean isUpload = this.saveDelApplyDefUploadFile( paramBean ) ;
		//添付必須チェック
		if( ! isUpload &&
			StatusFlg.on.value().equals( sheet.getValue( AmDesignEntryKeyByChangec.needRtnAttachFile ) ) ) {
			throw new ContinuableBusinessException( AmMessageId.AM001074E  );
		}
		if( isUpload ) {
			paramBean.setReturnAssetAppendFile( paramBean.getReturnAssetAppendFileInputStreamName() ) ;
		} else {
			paramBean.setReturnAssetAppendFile( null ) ;
		}
		return paramBean;
	}

	/**
	 * 返却申請確認画面からの遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void executeReferer(FlowChaLibRtnEntryServiceBean paramBean) {
		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {

			paramBean.setApplyUserName( paramBean.getUserName() );
			paramBean.setApplyUserId	( paramBean.getUserId() );
			paramBean.setApplyNo		( paramBean.getApplyNo() );

			// ２重貸出チェック
			paramBean.setDuplicationCheck( ! this.support.isAllowAssetDuplicationCheck( paramBean.getApplyNo() ));

			IAreqDto areqDto = this.support.findAreqDto( paramBean.getApplyNo() );
			IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() ) ;
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			paramBean.setLotNo		( lotDto.getLotEntity().getLotId() ) ;

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			{
				List<Object> paramList = new CopyOnWriteArrayList<Object>();

				IReturnProcessParam processParam = new ReturnProcessParam();
				processParam.setApplyNo			( paramBean.getApplyNo() );
				processParam.setUserId			( paramBean.getUserId() );
				processParam.setUser			( paramBean.getUserName() );
				processParam.setDuplicationCheck( paramBean.isDuplicationCheck() );
				processParam.setReturnAssetAppendFile	( paramBean.getReturnAssetAppendFile() );

				paramList.add( paramBean );	//インタフェース参照以外は、許可されない（業務固有Serviceへのcast不可）
				paramList.add( processParam );
				paramList.add( lotDto );
				paramList.add( areqDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}
			}
		}

	}

	/**
	 * 指定された申請アップロードファイルをサーバに保存します。
	 *
	 * @param inBean
	 */
	private final boolean saveDelApplyDefUploadFile( FlowChaLibRtnEntryServiceBean bean ) {

		if ( null == bean.getReturnAssetAppendFileInputStreamBytes() ||
			0 == bean.getReturnAssetAppendFileInputStreamBytes().length ) {
			return false ;
		}

		String basePath = sheet.getValue( AmDesignEntryKeyByChangec.returnAssetAppendFilePath ) ;

		// 申請番号フォルダをつなげる
		String applyNo = bean.getApplyNo() ;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		// ユニークになるよう日時フォルダをつなげる
		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

		// 添付ファイルごとのフォルダをつなげる
		String append1 = AppendFileConstID.File.append1.value() ;
		String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;

		BusinessFileUtils.saveAppendFile	(
				basePath , applyNo , uniqueKey , append1 ,
				StreamUtils.convertBytesToInputStreamToBytes( bean.getReturnAssetAppendFileInputStreamBytes() ),
				bean.getReturnAssetAppendFileInputStreamName() );

		String filePath = new File(
				appendPath1 ,
				bean.getReturnAssetAppendFileInputStreamName() ).getPath();

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "★★★:filePath:=" + filePath );
		}

		if ( new File(filePath).exists() ) {
			bean.setReturnAssetAppendFileInputStreamBytes( null );
			return true ;
		} else {
			return false ;
		}
	}
}
