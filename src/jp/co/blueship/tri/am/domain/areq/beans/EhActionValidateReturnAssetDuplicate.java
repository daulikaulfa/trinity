package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却資産申請の登録時の妥当性チェックを行います。
 * <li>原本比較処理
 * <li>コンフリクトチェック
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateReturnAssetDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFilesDiffer differ = null;

	/**
	 * 差分チェックがインスタンス生成時に自動的に設定されます。
	 * @param diff 差分チェック
	 */
	public final void setDiffer( IFilesDiffer diff ) {
		this.differ = diff;
	}

	private AmFinderSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		paramList.add( check( serviceDto ) );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @return ファイル差分情報を戻します。
	 * @throws ContinuableBusinessException
	 */

	private final IFileDiffResult[] check( IServiceDto<IGeneralServiceBean> serviceDto )
		throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		IFileDiffResult[] results = null;

		results = duplicationCheck(	paramList,
									extractDiff( paramList ) );

		return results;
	}

	/**
	 * 原本比較を行います。
	 * <li>今回申請するファイルと原本作業ファイルを順次比較します。
	 * <li>原本と同一（差分なし）のファイルは、申請対象から外します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return ファイル差分情報を戻します。
	 */
	private final IFileDiffResult[] extractDiff( List<Object> list ) {

		List<IFileDiffResult> resultList = new ArrayList<IFileDiffResult>();

		File infoPath	= AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( list) ;
		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( list );

		IFileDiffResult[] results = differ.execute(infoPath, masterPath );

		for ( int i = 0; i < results.length; i++ ) {
			if ( ! results[i].isDiff() )
				continue;

			resultList.add( results[i] );
		}

		return (IFileDiffResult[])resultList.toArray(new IFileDiffResult[0]);
	}

	/**
	 * 他の資産申請情報とコンフリクトチェックを行います。
	 * <li>今回申請するファイルと他の資産申請ファイルを順次比較します。
	 * <li>資産が重複する場合、差分比較して同一ファイルであれば、申請対象から外します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @param fileDiffs 原本との差分ファイル情報
	 * @return ファイル差分情報を戻します。
	 * @throws ContinuableBusinessException
	 */

	private final IFileDiffResult[] duplicationCheck(
											List<Object> list,
											IFileDiffResult[] fileDiffs )
			throws ContinuableBusinessException {

		List<IFileDiffResult> resultList = new ArrayList<IFileDiffResult>();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList	= new ArrayList<String[]>();

		File masterPath		= AmDesignBusinessRuleUtils.getMasterWorkPath( list );
		File assetPath		= AmDesignBusinessRuleUtils.getReturnAssetPath( list );
		File srcApplyNoPath	= AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( list );
//		File applyNoPath	= DesignBusinessRuleUtils.getLendAssetApplyNoPath( list );


		IAreqDto areqDto = AmExtractEntityAddonUtils.extractReturnApply( list );
		if ( null == areqDto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		AreqCondition condition	=
			AmDBSearchConditionAddonUtils.getAssetRtnApplyConditionByDuplicate( areqDto.getAreqEntity().getLotId() );
		ISqlSort sort = new SortBuilder();
		List<IAreqEntity> areqEntities	= support.getAreqDao().find( condition.getCondition(), sort );

		// ２重貸出チェック
		boolean duplicationCheck =
			AmExtractMessageAddonUtils.extractReturnProcessParam( list ).isDuplicationCheck();

		try {
			//大文字・小文字の判別（返却資産が原本作業パスと一致するか比較する）
			{
				boolean checkStrictlyFilePath = StatusFlg.on.value().equals(
						sheet.getValue( AmDesignEntryKeyByChangec.checkStrictlyFilePath ) );

				if ( checkStrictlyFilePath ) {
					for ( IFileDiffResult result: fileDiffs ) {
						if ( ! result.isDiff() )
							continue;

						File srcFile = new File( masterPath, result.getFilePath() );

						AmLibraryAddonUtils.checkCanonicalPath( masterPath , srcFile.getPath() , messageList , messageArgsList ) ;
					}
				}
			}

			//他の資産申請情報とコンフリクトチェック
			for ( IAreqEntity entity : areqEntities ) {

				File otherApplyNoPath = new File( assetPath, AmDesignBusinessRuleUtils.getAssetApplyPathName( entity ) );

				for ( int j = 0; j < fileDiffs.length; j++ ) {

					if ( ! fileDiffs[j].isDiff() )
						continue;

					//File srcFile	= new File( srcApplyNoPath, fileDiffs[j].getFilePath() );

					File otherFile	= new File( otherApplyNoPath, fileDiffs[j].getFilePath() );
					if ( ! otherFile.isFile() )
						continue;

					/*
					// 2011/10/24 コメントアウト
					//この処理が入っていると、新規資産の返却が重複していた場合に、申請から黙って除外され、
					//重複していることがメッセージで表示されないため不便。
					//また、他貸出資産との資産内容同一チェックは不要とみなし、当該処理を除去。
					if ( FileUtil.isSame( srcFile, otherFile ) ) {
						//資産は、他資産と同一のため、申請からはずす
						fileDiffs[j].setDiff( false );
						continue;
					}*/

					//既に新規資産として返却された資産を返却しようとした場合のチェック

					if ( otherFile.isFile() && duplicationCheck ) {
						String otherPath = TriStringUtils.convertRelativePath( otherApplyNoPath, TriStringUtils.convertPath( otherFile ) );

						//返却済の資産が存在する場合、申請不可
						messageList.add		( AmMessageId.AM001050E );
						messageArgsList.add	(
								new String[] {srcApplyNoPath.getName(), entity.getPjtId() , entity.getAreqId() , otherPath} );
					}

				}
			}
		}/*catch ( IOException e ) {
			throw new SystemException ( e );
		}*/ finally {
			if ( 0 != messageList.size() ) {
//				業務エラーをログファイルに出力させるため、ここでメッセージ生成を行う
//				MessageManager messageManager = ContextAdapterFactory.getContextAdapter().getMessageManager() ;

				for( int count = 0 ; count < messageList.size() ; count++ ) {
					LogHandler.warn( log , getContext().getMessage( messageList.get( count ) , messageArgsList.get( count ) ) ) ;
				}

				throw new ContinuableBusinessException( messageList, messageArgsList );
			}
		}

		for ( int i = 0; i < fileDiffs.length; i++ ) {
			if ( ! fileDiffs[i].isDiff() )
				continue;

			resultList.add( fileDiffs[i] );
		}

		return (IFileDiffResult[])resultList.toArray(new IFileDiffResult[0]);
	}
}
