package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.beans.mail.dto.AssetApplyMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * FlowChaLibMasterDelEntryCancelServiceイベントのサービスClass <br>
 * <p>
 * 変更管理 原本削除申請取消時のメール送信処理を行う。
 * </p>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibCompMasterDelCancelServiceMail implements IDomain<FlowChaLibCompMasterDelCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support = null;
	private MailGenericService successMail = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setSuccessMail(MailGenericService successMail) {
		this.successMail = successMail;
	}

	@Override
	public IServiceDto<FlowChaLibCompMasterDelCancelServiceBean> execute(IServiceDto<FlowChaLibCompMasterDelCancelServiceBean> serviceDto) {

		FlowChaLibCompMasterDelCancelServiceBean paramBean = serviceDto.getServiceBean();

		try {
			// 業務ロジックを記述
			String forword = paramBean.getForward();

			String delApplyNo = paramBean.getDelApplyNo();

			paramBean.setDelApplyNo(delApplyNo);

			if (forword.equals(ChaLibScreenID.COMP_MASTER_DEL_CANCEL)) {
				if (!ScreenType.bussinessException.equals(paramBean.getScreenType())) {

					AssetApplyMailServiceBean successMailBean = new AssetApplyMailServiceBean();
					TriPropertyUtils.copyProperties(successMailBean, paramBean);
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					IAreqEntity assetApplyEntity = this.support.findAreqEntity(delApplyNo, null);
					String pjtNo = assetApplyEntity.getPjtId();
					IPjtEntity pjtEntity = this.support.findPjtEntity(pjtNo);
					ILotEntity pjtLotEntity = this.support.findLotEntity(pjtEntity.getLotId());

					IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

					successMailBean.setAssetApplyEntity(assetApplyEntity);
					successMailBean.setPjtEntity(pjtEntity);
					successMailBean.setLotEntity(pjtLotEntity);
					successMailBean.setBldSrvId(srvEntity.getBldSrvId());
					successMailBean.setBldSrvNm(srvEntity.getBldSrvNm());

					successMail.execute(mailServiceDto);

				}
			}

			return serviceDto;

		} catch (Exception e) {
			// メール送信が失敗しても処理を続行する
			LogHandler.fatal(log, new TriSystemException( AmMessageId.AM005085S , e , paramBean.getFlowAction() ));
		}

		return serviceDto;

	}

}
