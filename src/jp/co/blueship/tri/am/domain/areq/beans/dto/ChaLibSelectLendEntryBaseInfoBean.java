package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;

public class ChaLibSelectLendEntryBaseInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * ロットの選択リスト
	 */
	private List<LotViewBean> selectLotNo;
	/**
	 * 変更管理番号の選択リスト
	 */
	private List<PjtViewBean> selectPjtViewBeanList = new ArrayList<PjtViewBean>();
	/**
	 * グループの選択リスト
	 */
	private List<String> selectGroupName;
	
	public List<String> getSelectGroupName() {
		return selectGroupName;
	}
	public void setSelectGroupName(List<String> selectGroupName) {
		this.selectGroupName = selectGroupName;
	}
	public List<PjtViewBean> getSelectPjtViewBeanList() {
		return selectPjtViewBeanList;
	}
	public void setSelectPjtViewBeanList(List<PjtViewBean> selectPjtViewBeanList) {
		this.selectPjtViewBeanList = selectPjtViewBeanList;
	}
	public List<LotViewBean> getSelectLotNo() {
		return selectLotNo;
	}
	public void setSelectLotNo(List<LotViewBean> selectLotNo) {
		this.selectLotNo = selectLotNo;
	}

}
