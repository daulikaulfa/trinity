package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 変更管理承認・申請情報詳細用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ApplyInfoDetailViewPjtApproveBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 申請番号 */
	private String applyNo = null;
	/** 申請件名 */
	private String applySubject = null;
	/** 申請内容 */
	private String applyContent = null;
	/** 申請日時 */
	private String applyDate = null;
	/** 申請者 */
	private String applyUser = null;
	/** 申請者 */
	private String applyUserId = null;
	/** 申請グループ */
	private String applyGroup = null;
	/** 資産情報 */
	private List<AssetInfo> assetInfoList = null;
	/** バイナリ資産情報 */
	private List<AssetInfo> assetBinaryInfoList = null;
	/** 添付ファイルの名前 */
	private String returnAssetAppendFile ;
	/** 添付ファイルへのリンク */
	private String returnAssetAppendFileLink ;
	
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo( String pjtNo ) {
		this.pjtNo = pjtNo;
	}
	
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplySubject() {
		return applySubject;
	}
	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getApplyContent() {
		return applyContent;
	}
	public void setApplyContent( String applyContent ) {
		this.applyContent = applyContent;
	}
	
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate( String applyDate ) {
		this.applyDate = applyDate;
	}
	
	public String getApplyUser() {
		return applyUser;
	}
	public void setApplyUser( String applyUser ) {
		this.applyUser = applyUser;
	}
	
	public String getApplyGroup() {
		return applyGroup;
	}
	public void setApplyGroup( String applyGroup ) {
		this.applyGroup = applyGroup;
	}
	
	public List<AssetInfo> getAssetInfoList() {
		if ( null == assetInfoList ) {
			assetInfoList = new ArrayList<AssetInfo>();
		}
		return assetInfoList;
	}
	public void setAssetInfoList( List<AssetInfo> assetInfoList ) {
		this.assetInfoList = assetInfoList;
	}
	
	public List<AssetInfo> getAssetBinaryInfoList() {
		if ( null == assetBinaryInfoList ) {
			assetBinaryInfoList = new ArrayList<AssetInfo>();
		}
		return assetBinaryInfoList;
	}
	public void setAssetBinaryInfoList( List<AssetInfo> assetBinaryInfoList ) {
		this.assetBinaryInfoList = assetBinaryInfoList;
	}
	
	public AssetInfo newAssetInfo() {
		return new AssetInfo();
	}
	
	
	/**
	 * 資産情報クラス
	 */
	public class AssetInfo implements Serializable {

		private static final long serialVersionUID = 1L;
	
		/** 資産パス */
		private String assetPath;
		/** 資産ステータス */
		private String assetStatus;
		
		
		public String getAssetPath() {
			return assetPath;
		}
		public void setAssetPath( String assetPath ) {
			this.assetPath = assetPath;
		}
		
		public String getAssetStatus() {
			return assetStatus;
		}
		public void setAssetStatus( String assetStatus ) {
			this.assetStatus = assetStatus;
		}
	}


	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}
	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}
	public String getReturnAssetAppendFileLink() {
		return returnAssetAppendFileLink;
	}
	public void setReturnAssetAppendFileLink(String returnAssetAppendFileLink) {
		this.returnAssetAppendFileLink = returnAssetAppendFileLink;
	}
	
}
