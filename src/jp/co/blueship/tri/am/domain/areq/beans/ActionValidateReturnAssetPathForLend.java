package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却フォルダの妥当性チェックを行います。
 * <br>返却資産申請時、格納パスが既に存在する場合に、エラーとなります。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionValidateReturnAssetPathForLend extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( paramList );

		File file = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, dto.getAreqEntity() );

		if ( file.isDirectory() ){
			throw new TriSystemException( AmMessageId.AM004023F , file.getName() );
		}

		return serviceDto;
	}

}
