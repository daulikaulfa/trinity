package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;


/**
 * アプリケーション層で使用する、削除申請の情報を格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IDelApplyParamInfo extends ICommonParamInfo, Serializable {
	
	

	/**
	 * 申請情報番号を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getApplyNo();
	/**
	 * 申請情報番号を設定します。
	 * @param value 申請情報番号
	 */
	public void setApplyNo( String value );
	/**
	 * 申請ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getDelApplyUser();
	/**
	 * 申請ユーザを設定します。
	 * @param value 申請ユーザ
	 */
	public void setDelApplyUser( String value );

	/**
	 * 申請ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getDelApplyUserId();
	/**
	 * 申請ユーザＩＤを設定します。
	 * @param value 申請ユーザＩＤ
	 */
	public void setDelApplyUserId( String value );
	
	/**
	 * 申請ファイルパスを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getInApplyFilePath();
	/**
	 * 申請ファイルパスをを設定します。
	 * @param value 申請ファイルパス
	 */
	public void setInApplyFilePath( String value );
	
	public String getCtgId();
	
	public void setCtgId(String ctgId);
	
	public String getMstoneId();
	
	public void setMstoneId(String mstoneId);

}
