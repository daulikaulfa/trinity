package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.constants.ChaLibScreenItemID;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryInputServiceBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * FlowChaLibMasterDelEntryServiceイベントのサービスClass <br>
 * <p>
 * 変更管理 原本削除申請画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibMasterDelEntryServiceEntry implements IDomain<FlowChaLibMasterDelEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelEntryServiceBean> execute(IServiceDto<FlowChaLibMasterDelEntryServiceBean> serviceDto) {

		FlowChaLibMasterDelEntryServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if (!referer.equals(ChaLibScreenID.MASTER_DEL_ENTRY) && !forward.equals(ChaLibScreenID.MASTER_DEL_ENTRY)) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.MASTER_DEL_ENTRY)) {

				List<PjtViewBean> pjtViewList = null;
				if (null != paramBean.getSelectedLotNo() && paramBean.getSelectedLotNo().length() > 0) {
					pjtViewList = support.getSelectPjtViewList(paramBean.getSelectedLotNo());
				} else {
					pjtViewList = support.getSelectPjtViewList();
				}

				// ロット選択有無のチェック
				String lotId = paramBean.getSelectedLotNo();
				if (TriStringUtils.isEmpty(lotId)) {
					throw new ContinuableBusinessException(AmMessageId.AM001003E);
				} else if (0 == pjtViewList.size()) {
					// 変更管理存在チェック
					throw new ContinuableBusinessException(AmMessageId.AM001005E);
				}
				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( lotId );
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

				paramBean.setLotNo(lotId);
				paramBean.setSelectPjtViewList(pjtViewList);

				List<String> applyGroupList = this.support.getSelectGroupNameList(lotId, paramBean.getUserId());
				if (TriStringUtils.isEmpty(applyGroupList)) {
					paramBean.setInfoMessage(AmMessageId.AM001121E);
				}

				paramBean.setApprovUserGroupList(applyGroupList);
			}

			if (referer.equals(ChaLibScreenID.MASTER_DEL_ENTRY)) {
				this.saveDelApplyDefUploadFile(paramBean);
				this.executeReferer(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * 原本削除入力画面からの遷移
	 *
	 * @param paramBean
	 * @throws IOException
	 * @throws FileNotFoundException
	 */

	private final void executeReferer(FlowChaLibMasterDelEntryServiceBean paramBean) throws FileNotFoundException, IOException {

		// 「次へ」で次画面に進む場合、下層のアクションを実行する
		if (ScreenType.next.equals(paramBean.getScreenType())) {
			// 画面入力項目をセット
			ChaLibMasterDelEntryInputServiceBean inBean = paramBean.getInputServiceBean();

			checkInputServiceBean(inBean);

			IPjtEntity pjtEntity = this.support.findPjtEntity(inBean.getInPrjNo());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			List<Object> paramList = new ArrayList<Object>();

			// 下層のアクションを実行する
			IDelApplyParamInfo actionParam = new DelApplyParamInfo();

			{
				actionParam.setUserId(paramBean.getUserId());
				actionParam.setDelApplyUser(paramBean.getUserName());
				actionParam.setDelApplyUserId(paramBean.getUserId());
				actionParam.setInApplyFilePath(inBean.getInApplyFilePath());

				paramList.add(lotDto);
				paramList.add(actionParam);

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for (IDomain<IGeneralServiceBean> action : actions) {
					action.execute(innerServiceDto);
				}
			}
		}
	}

	/**
	 * 指定された申請アップロードファイルをサーバに保存します。
	 *
	 * @param inBean
	 */
	private final void saveDelApplyDefUploadFile(FlowChaLibMasterDelEntryServiceBean inBean) {
		if (null == inBean.getInputServiceBean().getInApplyFileInputStreamBytes()) {
			return;
		}

		ChaLibMasterDelEntryInputServiceBean inputBean = inBean.getInputServiceBean();

		BusinessFileUtils.saveAppendFile(
				TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
						sheet.getValue(AmDesignEntryKeyByChangec.delApplyDefUploadRelativePath)), inBean.getUserId(),
				StreamUtils.convertBytesToInputStreamToBytes(inputBean.getInApplyFileInputStreamBytes()), inputBean.getInApplyFileInputStreamName());

		String filePath = new File(TriStringUtils.linkPathBySlash(sheet.getValue(UmDesignEntryKeyByCommon.homeTopPath),
				sheet.getValue(AmDesignEntryKeyByChangec.delApplyDefUploadRelativePath)), TriStringUtils.linkPath(inBean.getUserId(),
				inputBean.getInApplyFileInputStreamName())).getPath();

		if (log.isDebugEnabled()) {
			LogHandler.debug(log, "★★★:filePath:=" + filePath);
		}

		if (new File(filePath).exists()) {
			inputBean.setInApplyFileInputStreamBytes(null);
			inputBean.setInApplyFilePath(filePath);
		}
	}

	private final void checkInputServiceBean(ChaLibMasterDelEntryInputServiceBean inBean) throws ContinuableBusinessException {
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		if (TriStringUtils.isEmpty(inBean.getInPrjNo())) {
			messageList.add(AmMessageId.AM001038E);
			messageArgsList.add(new String[] {});
		}

		if (TriStringUtils.isEmpty(inBean.getInUserGroup())) {
			messageList.add(AmMessageId.AM001077E);
			messageArgsList.add(new String[] {});
		}

		if (!StatusFlg.off.value().equals(
				sheet.getValue(AmDesignBeanId.flowChaLibMasterDelEntryCheck, ChaLibScreenItemID.FlowChaLibMasterDelEntryCheck.SUMMARY.toString()))) {
			if (TriStringUtils.isEmpty(inBean.getInDelApplySubject())) {
				messageList.add(AmMessageId.AM001040E);
				messageArgsList.add(new String[] {});
			}
		}

		if (!StatusFlg.off.value().equals(
				sheet.getValue(AmDesignBeanId.flowChaLibMasterDelEntryCheck, ChaLibScreenItemID.FlowChaLibMasterDelEntryCheck.CONTENT.toString()))) {
			if (TriStringUtils.isEmpty(inBean.getInDelReason())) {
				messageList.add(AmMessageId.AM001076E);
				messageArgsList.add(new String[] {});
			}
		}

		if (TriStringUtils.isEmpty(inBean.getInApplyFilePath()) || !new File(inBean.getInApplyFilePath()).isFile()) {
			messageList.add(AmMessageId.AM001075E);
			messageArgsList.add(new String[] {});
		}

		if (messageList.size() != 0) {
			throw new ContinuableBusinessException(messageList, messageArgsList);
		}

	}

}
