package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 貸出資産申請の登録時の妥当性チェックを行います。
 * <li>コンフリクトチェック（削除資産申請との重複）
 * <br>
 * <br>この処理は、貸出資産申請の妥当性チェック後に呼び出されます。
 * <br>原本比較、他資産申請との比較済で呼び出されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateLendAssetLinkDelApplyDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		check( serviceDto );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private void check( IServiceDto<IGeneralServiceBean> serviceDto )
							throws ContinuableBusinessException {

		duplicationCheck( serviceDto );

	}

	/**
	 * 他の削除資産申請情報とコンフリクトチェックを行います。
	 * <li>今回申請するファイルと他の削除資産申請ファイルを順次比較します。
	 * <li>資産が重複する場合、エラーとします。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void duplicationCheck( IServiceDto<IGeneralServiceBean> serviceDto )
								throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity pjtLotEntity = lotDto.getLotEntity();
		AreqCondition condition =
			AmDBSearchConditionAddonUtils.getAssetDelApplyConditionByDuplicate( pjtLotEntity.getLotId() );
		ISqlSort sort = new SortBuilder();
		List<IAreqEntity> areqEntities = support.getAreqDao().find( condition.getCondition(), sort );

		File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		String[] assetFiles = param.getLendFiles();

		try {

			for ( IAreqEntity delEntity : areqEntities ) {
				List<IAreqFileEntity> removalFileEntities = this.support.findAreqFileEntities(delEntity.getAreqId(), AreqCtgCd.RemovalRequest);

				for ( IAreqFileEntity delFilePath : removalFileEntities ) {

					File delApplyFile = new File( masterPath, delFilePath.getFilePath() );

					for ( int i = 0; i < assetFiles.length; i++ ) {
						File lendAssetFile = new File( masterPath, assetFiles[i] );

						if ( ! delApplyFile.equals( lendAssetFile ) )
							continue;

						messageList.add( AmMessageId.AM001044E );
						messageArgsList.add(
							new String[] { TriStringUtils.convertRelativePath(masterPath, assetFiles[i]) , delEntity.getPjtId() , delEntity.getAreqId() } ) ;
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}


}
