package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;


/**
 * 貸出資産格納パス > 返却情報格納パスに資産をコピーします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EhActionCopyToReturnInfo extends ActionPojoAbstract<IGeneralServiceBean> {

	private ICopy copy = null;

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File srcPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );
		File destPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

		IFileDiffResult[] results = this.getFileDiffResult( paramList );

		try {
			for ( int i = 0; i < results.length; i++ ) {
				File destFile = new File(destPath, results[i].getFilePath());
				if ( destFile.isFile() )
					continue;

				copy.copy(	new File(srcPath, results[i].getFilePath()),
							destFile );
			}

		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005000S , e );
		}

		return serviceDto;
	}

	/**
	 * 比較元と比較先の差分ファイル情報を取得します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 */
	private final IFileDiffResult[] getFileDiffResult( List<Object> list ) {

		List<IFileDiffResult> outList = new ArrayList<IFileDiffResult>();

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( !(obj instanceof IFileDiffResult[]) )
				continue;

			return (IFileDiffResult[])obj;
		}

		return (IFileDiffResult[])outList.toArray( new IFileDiffResult[0] );
	}

}
