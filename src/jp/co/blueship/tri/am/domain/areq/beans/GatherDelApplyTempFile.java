package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyBinaryFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyFileResult;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 一時作業パスに展開された申請フォーマットファイルを読み出します。
 * （資産ファイル）
 *
 * @author Yukihiro Eguchi
 *
 */
public class GatherDelApplyTempFile extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
									sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefTempFile ) );


			FileInputStream fis = null ;
			InputStreamReader isr = null ;
			String[] files = null ;
			try {
				fis = new FileInputStream( file ) ;
				isr = new InputStreamReader( fis ) ;
				files = TriFileUtils.readFileLine( isr, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isr);
				TriFileUtils.close(fis);
			}


			List<IDelApplyFileResult> parsePath = new ArrayList<IDelApplyFileResult>();
			for ( String path : files ) {
				IDelApplyFileResult result = new DelApplyFileResult();
				result.setFilePath( FlowChaLibMasterDelEditSupport.convertPath( path ));
				parsePath.add( result );
			}

			paramList.add( parsePath.toArray( new IDelApplyFileResult[] {} ) );



			File binaryFile = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
					sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile ) );

			FileInputStream fisBin = null ;
			InputStreamReader isrBin = null ;
			String[] binaryFiles = null ;
			try {
				fisBin = new FileInputStream( binaryFile ) ;
				isrBin = new InputStreamReader( fisBin ) ;
				binaryFiles = TriFileUtils.readFileLine( isrBin, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isrBin);
				TriFileUtils.close(fisBin);
			}


			List<IDelApplyBinaryFileResult> binaryParsePath = new ArrayList<IDelApplyBinaryFileResult>();
			for ( String binaryPath : binaryFiles ) {
				IDelApplyBinaryFileResult result = new DelApplyBinaryFileResult();
				result.setFilePath( FlowChaLibMasterDelEditSupport.convertPath( binaryPath ));
				binaryParsePath.add( result );
			}

			paramList.add( binaryParsePath.toArray( new IDelApplyBinaryFileResult[] {} ) );

		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		}

		return serviceDto;

	}

}
