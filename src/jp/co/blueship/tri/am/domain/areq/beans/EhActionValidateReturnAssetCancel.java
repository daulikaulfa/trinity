package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の取消前の妥当性チェックを行います。
 * <li>返却資産申請が申請済みでない場合、エラーとなります。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateReturnAssetCancel extends ActionPojoAbstract<IGeneralServiceBean> {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( paramList );
		if ( null == dto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		if ( ! dto.getAreqEntity().getProcStsId().equals( AmAreqStatusId.CheckinRequested.getStatusId() )) {
			String expectStatusLabel = sheet.getValue( AmDesignBeanId.statusId, AmAreqStatusId.CheckinRequested.getStatusId() ) ;
			String curStatusLabel = sheet.getValue( AmDesignBeanId.statusId, dto.getAreqEntity().getProcStsId() ) ;
			throw new BusinessException( AmMessageId.AM001055E , expectStatusLabel , curStatusLabel );
		}

		return serviceDto;
	}
}
