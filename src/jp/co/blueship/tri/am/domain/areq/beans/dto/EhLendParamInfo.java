package jp.co.blueship.tri.am.domain.areq.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

/**
 * @version V4.00.00
 * @author le.thixuan
 */
public class EhLendParamInfo extends CommonParamInfo implements IEhLendParamInfo {

	private static final long serialVersionUID = 1L;



	private String applyNo = null;
	private String lendApplyUser = null;
	private String lendApplyUserId = null;
	private String groupName = null;
	private String summary = null;
	private String lendApplyDate = null;
	private String content = null;
	private String pjtNo = null;
	private String[] lendFiles = null;
	private boolean duplicationCheck;
	private String inApplyFilePath = null;
	private String assigneeId = null;
	private String ctgId = null;
	private String mstoneId = null;
	private String groupId = null;
	private boolean isDraff = false;

	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getLendApplyDate() {
		return lendApplyDate;
	}
	public void setLendApplyDate(String lendApplyDate) {
		this.lendApplyDate = lendApplyDate;
	}
	public String[] getLendFiles() {
		return lendFiles;
	}
	public void setLendFiles(String[] lendFiles) {
		this.lendFiles = lendFiles;
	}
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	public String getSummary() {
		return summary;
	}
	public void setSummary(String summary) {
		this.summary = summary;
	}
	public String getLendApplyUser() {
		return lendApplyUser;
	}
	public void setLendApplyUser(String lendApplyUser) {
		this.lendApplyUser = lendApplyUser;
	}

	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean value ) {
		this.duplicationCheck = value;
	}

	public String getInApplyFilePath() {
		return inApplyFilePath;
	}
	public void setInApplyFilePath( String inApplyFilePath ) {
		this.inApplyFilePath = inApplyFilePath;
	}
	public String getLendApplyUserId() {
		return lendApplyUserId;
	}
	public void setLendApplyUserId(String lendApplyUserId) {
		this.lendApplyUserId = lendApplyUserId;
	}
	@Override
	public String getAssigneeId() {
		return assigneeId;
	}
	@Override
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;

	}
	@Override
	public String getCtgId() {
		return ctgId;
	}
	@Override
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}
	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	@Override
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	@Override
	public boolean isDraff() {
		return isDraff;
	}
	@Override
	public void setDraff(boolean isDraff) {
		this.isDraff = isDraff;
	}

}
