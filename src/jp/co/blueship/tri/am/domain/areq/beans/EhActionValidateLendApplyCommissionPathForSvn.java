package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.IVcsReposDao;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.IPassMgtDao;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 貸出申請された資産（依頼）ファイルの妥当性チェックを行います。
 * <br>指定されたフォルダ／ファイルの存在チェック及び展開を行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateLendApplyCommissionPathForSvn extends ActionPojoAbstract<IGeneralServiceBean> {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	IVcsReposDao vcsReposDao = null;
	public void setVcsReposDao(IVcsReposDao ucfRepositoryDao) {
		this.vcsReposDao = ucfRepositoryDao;
	}

	IPassMgtDao passwordDao = null;
	public void setPassMgtDao(IPassMgtDao passwordDao) {
		this.passwordDao = passwordDao;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity pjtLotEntity = lotDto.getLotEntity();
		if( ! VcsCategory.SVN.equals( VcsCategory.value( pjtLotEntity.getVcsCtgCd() ) ) ) {
			return serviceDto;
		}

		this.check( serviceDto );

		return serviceDto;
	}

	/**
	 * 妥当性チェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @throws BaseBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto ) throws BaseBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList	= new ArrayList<IMessageId>();
		List<String[]> messageArgsList	= new ArrayList<String[]>();

		try {
			IFileResult[] files = AmExtractMessageAddonUtils.extractLendApplyFileResults( paramList );

			//SvnUtil svnUtil = this.getSvnUtil() ;

			File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			boolean checkStrictlyFilePath = StatusFlg.on.value().equals(
					sheet.getValue( AmDesignEntryKeyByChangec.checkStrictlyFilePath ) );

			for ( IFileResult fileResult : files ) {
				String filePath = fileResult.getFilePath() ;

				File file = new File( masterWorkPath, filePath );
				//ファイルの実在チェック（大文字・小文字は判別できない）
				if ( ! file.exists() ) {
					messageList.add		( AmMessageId.AM001058E );
					messageArgsList.add	( new String[] { fileResult.getFilePath() } );
					continue ;
				}

				//大文字・小文字も判別
				if ( checkStrictlyFilePath ) {
					AmLibraryAddonUtils.checkCanonicalPath( masterWorkPath , file.getPath() , messageList , messageArgsList ) ;
				}
			}
		} catch( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005010S , e ) ;
		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}
}
