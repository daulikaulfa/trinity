package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.DraftReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.RemovalReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 原本削除申請の自動採番を行います。
 *
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 */
public class ActionNumberingDelApplyNo extends ActionPojoAbstract<FlowChaLibMasterDelEntryServiceBean> {

	RemovalReqNumberingDao numberingDao = null;
	DraftReqNumberingDao draftNumberingDao = null;
	
	public void setNumberingDao(RemovalReqNumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}
	
	public void setDraftNumberingDao(DraftReqNumberingDao draftNumberingDao) {
		this.draftNumberingDao = draftNumberingDao;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelEntryServiceBean> execute( IServiceDto<FlowChaLibMasterDelEntryServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IAreqDto dto = AmExtractEntityAddonUtils.extractDeleteApply( paramList );
		if ( null == dto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}
		
		IAreqEntity areqEntity = dto.getAreqEntity();
		String areqId = null;
		
		if (AmAreqStatusId.RemovalRequested.equals(areqEntity.getStsId())) {
			areqId = this.numberingDao.nextval();
		} else {
			areqId = this.draftNumberingDao.nextval();
		}
		
		areqEntity.setAreqId( areqId );
		serviceDto.getServiceBean().setApplyNo( areqId );

		paramList.add( dto );

		return serviceDto;
	}
}
