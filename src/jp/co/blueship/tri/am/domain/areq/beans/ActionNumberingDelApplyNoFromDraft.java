package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.RemovalReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowRemovalRequestEditServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * @version V4.00.00
 * @author le.thixuan
 */
public class ActionNumberingDelApplyNoFromDraft extends ActionPojoAbstract<FlowRemovalRequestEditServiceBean> {
	
	RemovalReqNumberingDao numberingDao = null;
	public void setNumberingDao(RemovalReqNumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}
	
	@Override
	public IServiceDto<FlowRemovalRequestEditServiceBean> execute(
			IServiceDto<FlowRemovalRequestEditServiceBean> serviceDto) {
		List<Object> paramList = serviceDto.getParamList();
		List<Object> selected = TriCollectionUtils.select(paramList, TriPredicates.isInstanceOf(IAreqDto.class));

		PreConditions.assertOf(selected.size() == 1, "IAreqEntity should be contained only one instance.");

		IAreqEntity areqEntity = ((IAreqDto) selected.get(0)).getAreqEntity();
		String areqId = null;

		PreConditions.assertOf(AmAreqStatusId.RemovalRequested.equals(areqEntity.getStsId()), "Status is not " + AmAreqStatusId.RemovalRequested.getStatusId() + ".");

		areqId = this.numberingDao.nextval();
		areqEntity.setAreqId( areqId );
		serviceDto.getServiceBean().getParam().setSelectedAreqId( areqId );

		return serviceDto;
	}

}
