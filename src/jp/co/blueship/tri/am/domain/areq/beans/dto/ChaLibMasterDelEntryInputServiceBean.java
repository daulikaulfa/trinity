package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

public class ChaLibMasterDelEntryInputServiceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 変更管理番号
	 */
	private String inPrjNo;
	/**
	 * 申請件名
	 */
	private String inDelApplySubject;
	/**
	 * 削除理由
	 */
	private String inDelReason;
	/**
	 * 申請グループ
	 */
	private String inUserGroup;
	/**
	 * 申請(削除申請ファイルパス格納)ファイルパス
	 */
	private String inApplyFilePath;
	/**
	 * 申請(削除申請ファイルパス格納)ファイルストリーム
	 */
	private byte[] inApplyFileInputStreamBytes;
	/**
	 * 申請(削除申請ファイルパス格納)ファイルストリームのファイル名
	 */
	private String inApplyFileInputStreamName;

	public String getInApplyFilePath() {
		return inApplyFilePath;
	}

	public void setInApplyFilePath(String inApplyFilePath) {
		this.inApplyFilePath = inApplyFilePath;
	}

	public String getInDelReason() {
		return inDelReason;
	}

	public void setInDelReason(String inDelReason) {
		this.inDelReason = inDelReason;
	}

	public String getInPrjNo() {
		return inPrjNo;
	}

	public void setInPrjNo(String inPrjNo) {
		this.inPrjNo = inPrjNo;
	}

	public String getInUserGroup() {
		return inUserGroup;
	}

	public void setInUserGroup(String inUserGroup) {
		this.inUserGroup = inUserGroup;
	}

	public byte[] getInApplyFileInputStreamBytes() {
		return inApplyFileInputStreamBytes;
	}

	public void setInApplyFileInputStreamBytes( byte[] inApplyFileInputStreamBytes ) {
		this.inApplyFileInputStreamBytes = inApplyFileInputStreamBytes;
	}

	public String getInApplyFileInputStreamName() {
		return inApplyFileInputStreamName;
	}

	public void setInApplyFileInputStreamName(String inApplyFileInputStreamName) {
		this.inApplyFileInputStreamName = inApplyFileInputStreamName;
	}

	public String getInDelApplySubject() {
		return inDelApplySubject;
	}

	public void setInDelApplySubject(String inDelApplySubject) {
		this.inDelApplySubject = inDelApplySubject;
	}


}
