package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却情報申請の申請単位の格納フォルダを削除します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionDelDirReturnInfo extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {

			File returnInfoApplyNoPathFile = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList ) ;
			if( returnInfoApplyNoPathFile.exists() ) {
				TriFileUtils.delete( returnInfoApplyNoPathFile ) ;
			}
		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005060S , e , "IOError" );
		} catch ( SecurityException e ) {
			throw new TriSystemException( AmMessageId.AM005060S , e , "SecurityError" );
		}

		return serviceDto;

	}

}
