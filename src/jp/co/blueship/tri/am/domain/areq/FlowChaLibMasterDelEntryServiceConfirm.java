package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryInputServiceBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.DelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.ContinuableActionList;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibMasterDelEntryServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除申請確認画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMasterDelEntryServiceConfirm implements IDomain<FlowChaLibMasterDelEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibMasterDelEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	ContinuableActionList validateDelApplyCommission = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setValidateDelApplyCommission(ContinuableActionList validateDelApplyCommission) {
		this.validateDelApplyCommission = validateDelApplyCommission;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelEntryServiceBean> execute( IServiceDto<FlowChaLibMasterDelEntryServiceBean> serviceDto ) {

		FlowChaLibMasterDelEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			// 業務ロジックを記述
			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM ) &&
					!forward.equals( ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM )) {
				return serviceDto;
			}

			if ( forward.equals( ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM ) ) {
				this.executeForward( paramBean );

			}

			if (referer.equals(ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM)) {
				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setPjtIds( paramBean.getInputServiceBean().getInPrjNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 原本削除申請確認画面へのの遷移
	 *
	 * @param paramBean
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	private final void executeForward( FlowChaLibMasterDelEntryServiceBean paramBean )
							throws FileNotFoundException, IOException {

		//	画面入力項目をセット
		// RMI対策( paramBean -> paramBean )
		ChaLibMasterDelEntryInputServiceBean inBean = paramBean.getInputServiceBean();

		ChaLibMasterDelEntryViewServiceBean viewServiceBean = new ChaLibMasterDelEntryViewServiceBean();

		String chgFactorNo = null ;

		if ( inBean.getInApplyFilePath() != null ) {

			IPjtEntity pjtEntity = this.support.findPjtEntity( inBean.getInPrjNo() );
			chgFactorNo = pjtEntity.getChgFactorNo() ;

			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );
			List<Object> paramList = new ArrayList<Object>();
			IDelApplyParamInfo actionParam = new DelApplyParamInfo();

			actionParam.setUserId( paramBean.getUserId() );
			actionParam.setDelApplyUser( paramBean.getUserName() );
			actionParam.setDelApplyUserId( paramBean.getUserId() );
			actionParam.setInApplyFilePath( inBean.getInApplyFilePath() );
			paramList.add( lotDto );
			paramList.add( actionParam );


			File file = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
					sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefTempFile ) );

			FileInputStream fis = null ;
			InputStreamReader isr = null ;
			String[] files = null ;
			try {
				fis = new FileInputStream( file ) ;
				isr = new InputStreamReader( fis ) ;
				files = TriFileUtils.readFileLine( isr, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isr);
				TriFileUtils.close(fis);
			}


			List<String> applyFilePathList = new ArrayList<String>();

			for ( String path : files ) {
				path = FlowChaLibMasterDelEditSupport.convertPath( path );
				applyFilePathList.add( path );
			}

			viewServiceBean.setApplyFilePath( (String[])applyFilePathList.toArray( new String[]{} ));


			File binaryFile = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
					sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile ) );

			FileInputStream fisBin = null ;
			InputStreamReader isrBin = null ;
			String[] binaryFiles = null ;
			try {
				fisBin = new FileInputStream( binaryFile ) ;
				isrBin = new InputStreamReader( fisBin ) ;
				binaryFiles = TriFileUtils.readFileLine( isrBin , false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isrBin);
				TriFileUtils.close(fisBin);
			}


			List<String> applyBinaryFilePathList = new ArrayList<String>();

			for ( String binaryPath : binaryFiles ) {
				binaryPath = FlowChaLibMasterDelEditSupport.convertPath( binaryPath );
				applyBinaryFilePathList.add( binaryPath );
			}

			viewServiceBean.setApplyBinaryFilePath( (String[])applyBinaryFilePathList.toArray( new String[]{} ));

			paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;
		}

		viewServiceBean.setPrjNo			( inBean.getInPrjNo() );
		viewServiceBean.setChgFactorNo		( chgFactorNo ) ;
		viewServiceBean.setDelApplyUser 	( paramBean.getUserName() );
		viewServiceBean.setDelApplyUserId 	( paramBean.getUserId() );
		viewServiceBean.setDelApplySubject	( inBean.getInDelApplySubject() );
		viewServiceBean.setDelReason		( inBean.getInDelReason() );
		viewServiceBean.setUserGroup		( inBean.getInUserGroup() );
		viewServiceBean.setLotNo			( paramBean.getSelectedLotNo() );

		paramBean.setViewServiceBean(viewServiceBean);
	}

}
