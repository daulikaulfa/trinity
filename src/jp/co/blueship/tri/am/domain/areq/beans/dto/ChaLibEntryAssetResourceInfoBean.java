package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class ChaLibEntryAssetResourceInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	
	
	private List<PathInfoViewBean> pathInfoViewBeanList = new ArrayList<PathInfoViewBean>() ;
	private List<ChaLibAssetResourceViewBean> assetResourceViewBeanList = new ArrayList<ChaLibAssetResourceViewBean>();
	private List<ChaLibAssetResourceViewBean> assetBinaryResourceViewBeanList = new ArrayList<ChaLibAssetResourceViewBean>();
	
	/**
	 * 原本作業フォルダパス
	 */
	private String masterPath;

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ChaLibAssetResourceViewBean newAssetResourceViewBean() {
		ChaLibAssetResourceViewBean bean = new ChaLibAssetResourceViewBean();
		return bean;
	}
	
	
	public List<ChaLibAssetResourceViewBean> getAssetResourceViewBeanList() {
		return assetResourceViewBeanList;
	}

	public void setAssetResourceViewBeanList(
			List<ChaLibAssetResourceViewBean> assetResourceViewBeanList) {
		this.assetResourceViewBeanList = assetResourceViewBeanList;
	}

	public List<ChaLibAssetResourceViewBean> getAssetBinaryResourceViewBeanList() {
		return assetBinaryResourceViewBeanList;
	}

	public void setAssetBinaryResourceViewBeanList(
			List<ChaLibAssetResourceViewBean> assetBinaryResourceViewBeanList) {
		this.assetBinaryResourceViewBeanList = assetBinaryResourceViewBeanList;
	}

	public String getMasterPath() {
		return masterPath;
	}


	public void setMasterPath(String masterPath) {
		this.masterPath = masterPath;
	}

	public List<PathInfoViewBean> getPathInfoViewBeanList() {
		return pathInfoViewBeanList;
	}


	public void setPathInfoViewBeanList(List<PathInfoViewBean> pathInfoViewBeanList) {
		this.pathInfoViewBeanList = pathInfoViewBeanList;
	}
	
	


}
