package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

public class ChaLibAssetResourceViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 資産名称
	 */
	private String resourceName;

	/**
	 * 資産ファイルパス
	 */
	private String resourcePath;

	/**
	 * （相対）資産ファイルパス
	 */
	private String relativeResourcePath;

	/**
	 * 非表示フラグ
	 */
	private boolean hidden;

	/**
	 * 選択フラグ
	 */
	private boolean checked;

	/**
	 * ファイルフラグ
	 */
	private boolean file;

	/**
	 * ファイルステータス
	 */
	private String fileStatus;

	/**
	 * ファイルステータスID
	 */
	private String fileStatusId;
	/**
	 * diffボタン表示/非表示フラグ
	 */
	private boolean diffEnabled ;

	public String getFileStatusId() {
	    return fileStatusId;
	}

	public void setFileStatusId(String fileStatusId) {
	    this.fileStatusId = fileStatusId;
	}

	public boolean isDiffEnabled() {
		return diffEnabled;
	}

	public void setDiffEnabled(boolean diffEnabled) {
		this.diffEnabled = diffEnabled;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isFile() {
		return file;
	}

	public void setFile(boolean file) {
		this.file = file;
	}

	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public String getResourceName() {
		return resourceName;
	}

	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	public String getRelativeResourcePath() {
		return relativeResourcePath;
	}

	public void setRelativeResourcePath(String relativeResourcePath) {
		this.relativeResourcePath = relativeResourcePath;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getFileStatus() {
		return fileStatus;
	}

	public void setFileStatus(String fileStatus) {
		this.fileStatus = fileStatus;
	}

}
