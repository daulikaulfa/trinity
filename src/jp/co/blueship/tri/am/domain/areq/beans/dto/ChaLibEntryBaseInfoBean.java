package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

/**
 * @version V4.00.00
 * @author le.thixuan
 */

public class ChaLibEntryBaseInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;


	/**
	 * ロット番号
	 */
	private String lotId;
	/**
	 * 変更管理番号
	 */
	private String prjNo;
	/**
	 * 変更要因番号
	 */
	private String changeCauseNo ;
	/**
	 * 申請ユーザ
	 */
	private String applyUser;
	/**
	 * 申請ユーザＩＤ
	 */
	private String applyUserId;
	/**
	 * 申請グループ
	 */
	private String groupName;
	/**
	 * 申請日時
	 */
	private String applyDate;
	/**
	 * 変更承認ユーザ
	 */
	private String pjtApproveUser;
	/**
	 * 変更承認ユーザＩＤ
	 */
	private String pjtApproveUserId;
	/**
	 * 変更承認日時
	 */
	private String pjtApproveDate;
	/**
	 * 変更承認ユーザ
	 */
	private String pjtApproveCancelUser;
	/**
	 * 変更承認ユーザＩＤ
	 */
	private String pjtApproveCancelUserId;
	/**
	 * 変更承認日時
	 */
	private String pjtApproveCancelDate;
	/**
	 * 申請件名
	 */
	private String subject;
	/**
	 * 申請内容
	 */
	private String content;
	/**
	 * 添付ファイルの名前
	 */
	private String returnAssetAppendFile ;
	/**
	 * 添付ファイルのリンクURL
	 */
	private String returnAssetAppendFileLink ;

	private String assigneeId;

	private String assigneeNm;

	private String checkinDueDate;

	private String ctgId;

	private String ctgNm;

	private String mstoneId;

	private String mstoneNm;

	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}
	public String getApplyUser() {
		return applyUser;
	}
	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}
	public String getPrjNo() {
		return prjNo;
	}
	public void setPrjNo(String prjNo) {
		this.prjNo = prjNo;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	public String getPjtApproveDate() {
		return pjtApproveDate;
	}
	public void setPjtApproveDate(String pjtApproveDate) {
		this.pjtApproveDate = pjtApproveDate;
	}
	public String getPjtApproveUser() {
		return pjtApproveUser;
	}
	public void setPjtApproveUser(String pjtApproveUser) {
		this.pjtApproveUser = pjtApproveUser;
	}
	public String getPjtApproveCancelDate() {
		return pjtApproveCancelDate;
	}
	public void setPjtApproveCancelDate(String pjtApproveCancelDate) {
		this.pjtApproveCancelDate = pjtApproveCancelDate;
	}
	public String getPjtApproveCancelUser() {
		return pjtApproveCancelUser;
	}
	public void setPjtApproveCancelUser(String pjtApproveCancelUser) {
		this.pjtApproveCancelUser = pjtApproveCancelUser;
	}
	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	public String getPjtApproveCancelUserId() {
		return pjtApproveCancelUserId;
	}
	public void setPjtApproveCancelUserId(String pjtApproveCancelUserId) {
		this.pjtApproveCancelUserId = pjtApproveCancelUserId;
	}
	public String getPjtApproveUserId() {
		return pjtApproveUserId;
	}
	public void setPjtApproveUserId(String pjtApproveUserId) {
		this.pjtApproveUserId = pjtApproveUserId;
	}
	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}
	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}
	public String getReturnAssetAppendFileLink() {
		return returnAssetAppendFileLink;
	}
	public void setReturnAssetAppendFileLink(String returnAssetAppendFileLink) {
		this.returnAssetAppendFileLink = returnAssetAppendFileLink;
	}
	public String getAssigneeId() {
		return assigneeId;
	}
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}
	public String getAssigneeNm() {
		return assigneeNm;
	}
	public void setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
	}
	public String getCheckinDueDate() {
		return checkinDueDate;
	}
	public void setCheckinDueDate(String checkinDueDate) {
		this.checkinDueDate = checkinDueDate;
	}
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

}
