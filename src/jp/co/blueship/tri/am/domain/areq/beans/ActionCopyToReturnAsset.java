package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.IFilterCopy;
import jp.co.blueship.tri.fw.cmn.io.ITouch;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却情報申請 > 返却資産申請に資産をコピーします。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ActionCopyToReturnAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	private IFilterCopy copy = null;
	private ITouch touch = null;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( IFilterCopy c ) {
		copy = c;
	}

	/**
	 * タッチがインスタンス生成時に自動的に設定されます。
	 * @param t タッチ
	 */
	public final void setTouch( ITouch t ) {
		touch = t;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File infoPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );
		File assetPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );

		IFileDiffResult[] results = this.getFileDiffResult( paramList );

		try {
			List<Pattern> pList = this.getPatternList( paramList );

			//  setterインジェクションでは渡せないのでcopy処理前にFilterを渡す
			copy.setFileFilter(BusinessFileUtils.getFileFilter());
			for ( int i = 0; i < results.length; i++ ) {

				copy.copy(	new File(infoPath, results[i].getFilePath()),
							new File(assetPath, results[i].getFilePath()) );
				// 資産をコピーした後、pListにマッチするファイルをtouchする
				if ( pList != null ) {
					touch.touch(	new File(assetPath, results[i].getFilePath()),
							System.currentTimeMillis(),
							pList);
				}
			}

			File file = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );
			List<String> files = TriFileUtils.getFilePaths(file, TriFileUtils.TYPE_FILE,
					AmDesignBusinessRuleUtils.getNeglectFileFilter(AmExtractEntityAddonUtils.extractPjtLot( paramList ) ));

			if ( 0 == files.size() )
				throw new BusinessException( AmMessageId.AM001047E );

		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005056S , e );
		}

		return serviceDto;
	}

	/**
	 * 比較元と比較先の差分ファイル情報を取得します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 */
	private final IFileDiffResult[] getFileDiffResult( List<Object> list ) {

		List<IFileDiffResult> outList = new ArrayList<IFileDiffResult>();

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( !(obj instanceof IFileDiffResult[]) )
				continue;

			return (IFileDiffResult[])obj;
		}

		return outList.toArray( new IFileDiffResult[0] );
	}

	/**
	 * 日付を変更する対象とするファイル名の正規表現リストを取得します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws IOException
	 */

	private final List<Pattern> getPatternList( List<Object> list ) throws IOException {

		File defsPath = new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
									sheet.getValue( AmDesignEntryKeyByChangec.defsRelativePath ));

		File defPath = new File(defsPath, "tstamp");
		File file = new File(defPath, "changeTStamp.def");
		List<Pattern> pList = null;

		if ( file.exists() ) {
			String[] contents = TriFileUtils.readFileLine( file, false );

			pList = new ArrayList<Pattern>();
			for(int i=0;i<contents.length;i++) {
				try{
					Pattern p = Pattern.compile(contents[i]);
					pList.add( p );
				}catch (PatternSyntaxException e) {
					throw new BusinessException( AmMessageId.AM001051E , e , e.getPattern() +" - "+e.getMessage() );
				}
			}
		}

		return pList;
	}
}
