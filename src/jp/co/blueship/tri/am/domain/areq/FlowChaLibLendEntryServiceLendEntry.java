package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibSelectLendEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * FlowChaLibLendEntryイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出申請画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendEntryServiceLendEntry implements IDomain<FlowChaLibLendEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private List<IDomain<IGeneralServiceBean>> actions4InputFile = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	public void setActions4InputFile( List<IDomain<IGeneralServiceBean>> actions4InputFile ) {
		this.actions4InputFile = actions4InputFile;
	}
	@Override
	public IServiceDto<FlowChaLibLendEntryServiceBean> execute(IServiceDto<FlowChaLibLendEntryServiceBean> serviceDto) {

		FlowChaLibLendEntryServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LEND_ENTRY ) &&
					!forward.equals( ChaLibScreenID.LEND_ENTRY )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.LEND_ENTRY)) {
				executeForward(paramBean);
			}

			if (referer.equals(ChaLibScreenID.LEND_ENTRY)) {
				executeReferer(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction());
		}
	}

	/**
	 * 貸出申請画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private final void executeForward(FlowChaLibLendEntryServiceBean paramBean) {


		ChaLibEntryBaseInfoBean baseInfoBean = null;
		ChaLibSelectLendEntryBaseInfoBean selectBaseInfoBean = new ChaLibSelectLendEntryBaseInfoBean();

//		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
//			baseInfoBean = new ChaLibEntryBaseInfoBean();
//			baseInfoBean.setApplyUser( paramBean.getUserName() );
//			baseInfoBean.setApplyUserId( paramBean.getUserNameId() );
//		} else {
			baseInfoBean = AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply( null, paramBean.getInBaseInfoBean() );
//		}

		paramBean.setBaseInfoViewBean( baseInfoBean );

		//	選択項目をセット
		{
			List<LotViewBean> lotViewBeanList = this.support.getLotViewBeanForPullDown( paramBean );
			selectBaseInfoBean.setSelectLotNo( lotViewBeanList );

			List<PjtViewBean> pjtViewList = null;
			if (null != baseInfoBean.getLotNo() && baseInfoBean.getLotNo().length() > 0) {
				pjtViewList = support.getSelectPjtViewList( baseInfoBean.getLotNo() );
			} else {
				pjtViewList = support.getSelectPjtViewList();
			}

			//ロット選択有無のチェック
			String lotId = baseInfoBean.getLotNo() ;
			if( TriStringUtils.isEmpty( lotId ) ) {
				throw new ContinuableBusinessException( AmMessageId.AM001003E ) ;
			} else if( 0 == pjtViewList.size() ) {
				//変更管理存在チェック
				throw new ContinuableBusinessException( AmMessageId.AM001005E ) ;
			}

			// グループの存在チェック
			ILotDto lotDto = this.support.findLotDto( lotId );
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			selectBaseInfoBean.setSelectPjtViewBeanList( pjtViewList );

			List<String> applyGroupList = this.support.getSelectGroupNameList( lotId, paramBean.getUserId() );
			if ( TriStringUtils.isEmpty( applyGroupList )) {
				paramBean.setInfoMessage( AmMessageId.AM001121E );
			}

			selectBaseInfoBean.setSelectGroupName( applyGroupList );

			paramBean.setLotNo( lotId ) ;
			paramBean.setSelectBaseInfoViewBean( selectBaseInfoBean );
		}

	}

	/**
	 * 貸出申請画面からの遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private final void executeReferer(
			FlowChaLibLendEntryServiceBean paramBean ) {

		ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

		ChaLibEntryBaseInfoBean baseInfoBean = new ChaLibEntryBaseInfoBean();
		//	入力項目をセット
		baseInfoBean.setLotNo			( inBean.getInLotNo() );
		baseInfoBean.setPrjNo			( inBean.getInPrjNo() );
		baseInfoBean.setChangeCauseNo	( inBean.getInChangeCauseNo() ) ;
		baseInfoBean.setGroupName		( inBean.getInGroupName() );
		baseInfoBean.setSubject			( inBean.getInSubject() );
		baseInfoBean.setContent			( inBean.getInContent() );
		baseInfoBean.setApplyUser		( inBean.getInUserName() );
		baseInfoBean.setApplyUserId		( inBean.getInUserId() );

		IPjtEntity pjtEntity = null;
		if ( ! TriStringUtils.isEmpty( inBean.getInPrjNo() )) {
			pjtEntity = this.support.findPjtEntity( inBean.getInPrjNo() ) ;
			baseInfoBean.setChangeCauseNo( pjtEntity.getChgFactorNo() ) ;
		}

		paramBean.setBaseInfoViewBean( baseInfoBean );

		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() )) {

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_ENTRY_CONFIRM.equals( paramBean.getForward() )) {
				FlowChaLibLendEditSupport.saveLendApplyDefUploadFile(
						paramBean.getInBaseInfoBean(), paramBean.getUserId() );
			}

			List<Object> paramList = new ArrayList<Object>();

			IEhLendParamInfo param = new EhLendParamInfo();
			this.support.setLendParamInfo( paramBean.getInBaseInfoBean(), null, param );

			paramList.add( param );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_ENTRY_CONFIRM.equals( paramBean.getForward() )) {
				param.setInApplyFilePath( inBean.getLendApplyFilePath() );
				FlowChaLibLendEditSupport.check4InputFile( inBean );
			}

			for ( IDomain<IGeneralServiceBean> action : this.actions ) {
				action.execute( innerServiceDto );
			}

			// 申請ファイルによる申請
			if ( ChaLibScreenID.LEND_ENTRY_CONFIRM.equals( paramBean.getForward() )) {

				ILotEntity lotEntity	= this.support.findLotEntity( inBean.getInLotNo() );
				ILotDto lotDto = this.support.findLotDto( inBean.getInLotNo() );
				paramList.add( lotDto );

				// ２重貸出チェック
				param.setDuplicationCheck	( ! lotEntity.getIsAssetDup().parseBoolean() );
				param.setUserId				( paramBean.getUserId() );

				for ( IDomain<IGeneralServiceBean> action : this.actions4InputFile ) {
					action.execute( innerServiceDto );
				}


				// 確認画面への遷移対応
				ChaLibEntryAssetResourceInfoBean assetResourceBean = new ChaLibEntryAssetResourceInfoBean();
				File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity );
				assetResourceBean.setMasterPath	( TriStringUtils.convertPath( masterPath ));
				paramBean.setAssetSelectViewBean	( assetResourceBean );

				ChaLibInputLendEntryAssetSelectBean inputBean = new ChaLibInputLendEntryAssetSelectBean();
				inputBean.setInAssetResourcePath(
						FlowChaLibLendEditSupport.getLendAssetPath( masterPath, paramList ));
				paramBean.setInAssetSelectBean	( inputBean );

				// 変更管理番号からリンクする変更要因番号をセットする
				if ( null != pjtEntity ) {
					paramBean.getInBaseInfoBean().setInChangeCauseNo( pjtEntity.getChgFactorNo() );
				}
			}
		}
	}
}
