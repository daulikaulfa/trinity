package jp.co.blueship.tri.am.domain.areq.beans.dto;



/**
 * アプリケーション層で、バイナリ削除資産ファイルパスを格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IDelApplyBinaryFileResult extends IDelApplyFileResult {
	
	

}
