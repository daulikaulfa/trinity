package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;



public class ChaLibSelectLendEntryAssetSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<ChaLibAssetResourceViewBean> assetResourceViewBeanList = new ArrayList<ChaLibAssetResourceViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ChaLibAssetResourceViewBean newLendEntryAssetResourceBean() {
		ChaLibAssetResourceViewBean bean = new ChaLibAssetResourceViewBean();
		return bean;
	}


	public List<ChaLibAssetResourceViewBean> getAssetResourceViewBeanList() {
		return assetResourceViewBeanList;
	}



	public void setAssetResourceViewBeanList(
			List<ChaLibAssetResourceViewBean> assetResourceViewBeanList) {
		this.assetResourceViewBeanList = assetResourceViewBeanList;
	}

}
