package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibMasterDelEntryCancelServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除申請の取消を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibCompMasterDelCancelService implements IDomain<FlowChaLibCompMasterDelCancelServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibCompMasterDelCancelServiceBean> execute(IServiceDto<FlowChaLibCompMasterDelCancelServiceBean> serviceDto) {

		FlowChaLibCompMasterDelCancelServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			// 業務ロジックを記述
			String forword		= paramBean.getForward();
			String delApplyNo	= paramBean.getDelApplyNo();

			paramBean.setDelApplyNo(delApplyNo);

			if ( forword.equals(ChaLibScreenID.COMP_MASTER_DEL_CANCEL) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					IAreqDto areqDto = support.findAreqDto( delApplyNo );
					IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() );

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( pjtEntity.getLotId() )
						.setPjtIds( areqDto.getAreqEntity().getPjtId() )
						.setAreqIds( delApplyNo );

						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}

					setEntity( areqDto, paramBean );

					ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

					// グループの存在チェック
					AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
							this.support.getUmFinderSupport().getGrpDao(),//
							this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

					//下層のアクションを実行する
					{
						List<Object> paramList = new ArrayList<Object>();

						paramList.add( areqDto );
						paramList.add( lotDto );

						IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
								.setServiceBean( paramBean )
								.setParamList( paramList );

						for ( IDomain<IGeneralServiceBean> action : actions ) {
							action.execute( innerServiceDto );
						}
					}
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}
	private void  setEntity(IAreqDto dto, FlowChaLibCompMasterDelCancelServiceBean paramBean) {

		paramBean.setSystemDate( TriDateUtils.getSystemTimestamp() );
		paramBean.setUserId(paramBean.getUserId());
		paramBean.setUserName(paramBean.getUserName());

		IAreqEntity entity = dto.getAreqEntity();

		String stsId = AmAreqStatusId.RemovalRequestRemoved.getStatusId();

		if( entity.getStsId().equals( AmAreqStatusId.DraftRemovalRequest.getStatusId() ) ){
			stsId = AmAreqStatusId.DraftRemovalRequestRemoved.getStatusId();

		}else if( entity.getStsId().equals( AmAreqStatusId.PendingRemovalRequest.getStatusId() ) ){
			stsId = AmAreqStatusId.PendingRemovalRequestRemoved.getStatusId();
		}

		entity.setStsId			( stsId );
		entity.setDelStsId		( StatusFlg.on );
		entity.setDelTimestamp	( paramBean.getSystemDate());
		entity.setDelUserId		( paramBean.getUserId());
		entity.setDelUserNm		( paramBean.getUserName());

		// am_areq_fileレコード論理削除
		for( IAreqFileEntity fileEntity : dto.getAreqFileEntities() ) {
			fileEntity.setDelStsId( StatusFlg.on );
		}
		// am_areq_binary_fileレコード論理削除
		for( IAreqBinaryFileEntity binaryFileEntity : dto.getAreqBinaryFileEntities() ) {
			binaryFileEntity.setDelStsId( StatusFlg.on );
		}
	}

}
