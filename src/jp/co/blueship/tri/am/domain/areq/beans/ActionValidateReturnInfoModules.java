package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却情報申請の返却資産ファイルと、原本モジュールとの構成における妥当性チェックを行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionValidateReturnInfoModules extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity pjtLotEntity = lotDto.getLotEntity();

		if( TriStringUtils.isEmpty( pjtLotEntity ) ) {
			throw new TriSystemException( AmMessageId.AM005011S );
		}
		//モジュール名
		List<ILotMdlLnkEntity> mdlEntities = lotDto.getIncludeMdlEntities( true );
		Set<String> moduleSet = new LinkedHashSet<String>() ;
		for( ILotMdlLnkEntity module : mdlEntities ) {
			moduleSet.add( module.getMdlNm() ) ;
		}

		File file = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

		//返却資産とモジュール名を照合する
		this.checkReturnInfoFilesModules( file , moduleSet ) ;

		return serviceDto;
	}

	/**
	 *
	 * @param returnInfoApplyNoPathFile
	 * @param moduleSet
	 */

	private void checkReturnInfoFilesModules( File returnInfoApplyNoPathFile , Set<String> moduleSet ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>() ;
		List<String[]> messageArgsList = new ArrayList<String[]>() ;

		File[] moduleFileArray = returnInfoApplyNoPathFile.listFiles() ;
		for( File moduleFile : moduleFileArray ) {
			if( moduleFile.isDirectory() ) {
				if( true != moduleSet.contains( moduleFile.getName() ) ) {//モジュール登録にないフォルダが返却された
					messageList.add( AmMessageId.AM001054E ) ;
					messageArgsList.add( new String[] { moduleFile.getName() }  ) ;
				}
			} else if( moduleFile.isFile() ) {//モジュールの階層にファイルが返却された
				messageList.add( AmMessageId.AM001056E ) ;
				messageArgsList.add( new String[] { moduleFile.getName() }  ) ;
			}
		}

		if ( 0 != messageList.size() ) {
			throw new ContinuableBusinessException( messageList , messageArgsList ) ;
		}
	}
}
