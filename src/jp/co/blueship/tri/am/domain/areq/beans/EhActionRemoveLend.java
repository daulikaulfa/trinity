package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * 貸出情報申請の取消を行います。
 *
 * @version V4.00.00
 * @author yukihiro eguchi
 */
public class EhActionRemoveLend extends ActionPojoAbstract<IGeneralServiceBean> {

	private FlowChaLibLendEditSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowChaLibLendEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		List<Object> paramList = serviceDto.getParamList();

		try {
			IAreqDto areqDto = AmExtractEntityAddonUtils.extractLendApply( paramList );
			String areqId = areqDto.getAreqEntity().getAreqId();

			support.getAreqDao().update( areqDto.getAreqEntity() );
			support.cleaningAreq(areqId);
			support.getUmFinderSupport().cleaningCtgLnk( AmTables.AM_AREQ, areqId);
			support.getUmFinderSupport().cleaningMstoneLnk( AmTables.AM_AREQ, areqId);

			areqDto = this.support.findAreqDto( areqId, areqDto.getAreqEntity().getDelStsId() );
			paramList.remove(areqDto);
			paramList.add(areqDto);

			// 履歴更新
			if ( DesignSheetUtils.isRecord() ) {

				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.Remove.getStatusId() );

				support.getAreqHistDao().insert( histEntity , areqDto);
			}
		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}

}
