package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 資産削除申請の申請単位の格納フォルダを作成します。
 * 指定された資産削除申請ファイルの格納フォルダを作成します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMkDirDelApplyCommission extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ).mkdirs();

		} catch (SecurityException e) {
			throw new TriSystemException( AmMessageId.AM005062S , e );
		}

		return serviceDto;
	}
}
