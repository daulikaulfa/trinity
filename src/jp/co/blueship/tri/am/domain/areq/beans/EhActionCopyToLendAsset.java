package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;


/**
 * 原本作業格納パス > 貸出申請に資産をコピーします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EhActionCopyToLendAsset  extends ActionPojoAbstract<IGeneralServiceBean> {

	private ICopy copy = null;

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File srcPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );
		File destPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );

		IFileDiffResult[] results = AmExtractMessageAddonUtils.getFileDiffResult( paramList );

		try {
			for ( int i = 0; i < results.length; i++ ) {

				copy.copy(	new File(srcPath, results[i].getFilePath()),
							new File(destPath, results[i].getFilePath()) );
			}

			TriFileUtils.getFilePaths(destPath, TriFileUtils.TYPE_FILE,
					AmDesignBusinessRuleUtils.getNeglectFileFilter( AmExtractEntityAddonUtils.extractPjtLot( paramList ) ));

		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005066S , e );
		}

		return serviceDto;
	}

}
