package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.AllowFilePathUtil;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 資産ファイルのファイルパスの妥当性を行います。
 * <br>プロパティファイルに定義された正規表現かどうかをチェックします。
 *
 * @author kawakami
 *
 */
public class FilePathValidator implements IFilePathValidator {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * <br>返却資産申請の妥当性チェックで使用されます。
	 * @param file 対象となるディレクトリパス
	 * @throws ContinuableBusinessException
	 */

	public final void validate( File file ) throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {
			List<String> files = TriFileUtils.getFilePaths(file, TriFileUtils.TYPE_FILE, BusinessFileUtils.getFileFilter());

			if ( 0 == files.size() )
				throw new BusinessException( AmMessageId.AM001046E );

			this.validateFilePath(
						messageList,
						messageArgsList,
						file,
						files);

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005017S, e );
		}
	}

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * <br>削除申請された資産の妥当性チェックで使用されます。
	 * @param file 親フォルダパス
	 * @param files 対象となる相対ファイルパス
	 * @throws ContinuableBusinessException
	 */

	public void validate( File file, String[] files ) throws ContinuableBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {

			this.validateFilePath(
						messageList,
						messageArgsList,
						file,
						FluentList.from(files).asList()
						);

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

		} finally {
		}
	}

	/**
	 * 資産ファイルパスの妥当性チェックを行います。
	 * @param file 親フォルダパス
	 * @param files 相対パスリスト
	 */

	private final void validateFilePath(
							List<IMessageId> messageList,
							List<String[]> messageArgsList,
							File file,
							List<String> files) {

		String checkMaxLengthByFilePath = sheet.getValue( AmDesignEntryKeyByChangec.checkMaxLengthByFilePath );

		if ( ! TriStringUtils.isDigits( checkMaxLengthByFilePath ) ) {
			messageList.add( AmMessageId.AM001084E );
			messageArgsList.add( new String[] {checkMaxLengthByFilePath} );
			return;
		}

		int maxLength = Integer.parseInt( checkMaxLengthByFilePath );

		for ( String path : files ) {

			if ( 0 != maxLength ) {
				String checkFilePath = TriStringUtils.convertPath( new File( file, path ) );

				if ( maxLength < checkFilePath.length() ) {
					messageList.add( AmMessageId.AM001085E );
					messageArgsList.add( new String[] {checkFilePath, checkMaxLengthByFilePath} );
				}
			}

			if ( AllowFilePathUtil.evalSequencial(path) ) {
				continue;
			} else {
				messageList.add( AmMessageId.AM001046E );
				messageArgsList.add( new String[] { path } );
			}
		}
	}

}
