package jp.co.blueship.tri.am.domain.areq.beans.dto;

/**
 * 変更管理・変更管理詳細の関連申請情報一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelatedApplyInfoViewBean extends ApplyInfoViewPjtDetailBean {

	private static final long serialVersionUID = 1L;
	
	/** 関連元申請番号 */
	private String origApplyNo = null;
	
	public String getOrigApplyNo() {
		return origApplyNo;
	}
	public void setOrigApplyNo( String origApplyNo ) {
		this.origApplyNo = origApplyNo;
	}

}
