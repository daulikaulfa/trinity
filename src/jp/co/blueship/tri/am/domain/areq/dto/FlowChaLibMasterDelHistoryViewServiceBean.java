package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

public class FlowChaLibMasterDelHistoryViewServiceBean extends FlowChaLibMasterDelEntryViewServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 選択ページ
	 */
	private int selectPageNo = 0;

	/**
	 * ページ制御
	 */
	private IPageNoInfo pageInfoView;
	
	

	/**
	 * @return pageInfoView
	 */
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}

	/**
	 * @param pageInfoView 設定する pageInfoView
	 */
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	/**
	 * @return selectPageNo
	 */
	public int getSelectPageNo() {
		return selectPageNo;
	}

	/**
	 * @param selectPageNo 設定する selectPageNo
	 */
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	

}
