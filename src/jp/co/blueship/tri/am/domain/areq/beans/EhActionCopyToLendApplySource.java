package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 貸出資産格納パスに定義ファイルをコピーします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class EhActionCopyToLendApplySource extends ActionPojoAbstract<IGeneralServiceBean> {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private ICopy copy = null;

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();
		File file = new File(	AmLibraryAddonUtils.getLendApplyDefTempFilePath( paramList ),
								sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefTempFile ) );

		if ( file.exists() ) {

			File outFilePath = AmDesignBusinessRuleUtils.getLendApplyDefApplyNoPath( paramList );

			try {
				copy.copy(	file,
							new File( outFilePath,
									sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefFile )) );

				TriFileUtils.delete( file );

			} catch ( IOException e ) {
				throw new TriSystemException( AmMessageId.AM005065S , e );
			}
		}

		return serviceDto;
	}
}
