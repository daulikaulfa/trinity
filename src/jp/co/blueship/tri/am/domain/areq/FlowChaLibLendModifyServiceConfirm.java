package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmEntityAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;


/**
 * FlowChaLibLendModifyイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出申請確認画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendModifyServiceConfirm implements IDomain<FlowChaLibLendModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute( IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto ) {

		FlowChaLibLendModifyServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.LEND_MODIFY_CONFIRM ) &&
					!forward.equals( ChaLibScreenID.LEND_MODIFY_CONFIRM )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.LEND_MODIFY_CONFIRM)) {
			}

			if (referer.equals(ChaLibScreenID.LEND_MODIFY_CONFIRM)) {
				IAreqEntity assetApplyEntity = support.findAreqEntity( paramBean.getApplyNo() );

				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setPjtIds( assetApplyEntity.getPjtId() )
						.setAreqIds( paramBean.getApplyNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}

				executeReferer(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 貸出申請確認画面からの遷移
	 *
	 * @param paramBean
	 */
	private final void executeReferer(FlowChaLibLendModifyServiceBean paramBean) throws Exception {
		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
			ChaLibEntryInputBaseInfoBean inBean = paramBean.getInBaseInfoBean();

			IPjtEntity pjtEntity = support.findPjtEntity( inBean.getInPrjNo() );

			ILotDto lotDto = support.findLotDto( pjtEntity.getLotId() );

			IAreqDto areqDto = support.findAreqDto( paramBean.getApplyNo() );
			AmEntityAddonUtils.setAssetApplyEntityByFlowChaLibLendEntryService( areqDto.getAreqEntity(), lotDto.getLotEntity(), inBean );

			File masterPath = AmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			//下層のアクションを実行する
			{
				List<Object> paramList = new ArrayList<Object>();

				IEhLendParamInfo param = new EhLendParamInfo();

				support.setLendParamInfo(paramBean.getInBaseInfoBean(), paramBean.getApplyNo(), param);
				support.setLendParamInfo(paramBean.getInAssetSelectBean(), masterPath, param);
				// ２重貸出チェック
				param.setDuplicationCheck( ! lotDto.getLotEntity().getIsAssetDup().parseBoolean() ) ;
				param.setUserId( paramBean.getUserId() );

				paramList.add( param );
				paramList.add( lotDto );
				paramList.add( areqDto );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				VcsCategory scmType = VcsCategory.value( lotDto.getLotEntity().getVcsCtgCd() ) ;
				if( VcsCategory.SVN.equals(  scmType ) ) {
					List<IVcsReposEntity> reposEntities = this.support.findVcsReposEntities();
					paramList.add( reposEntities.toArray( new IVcsReposEntity[0] ) ) ;
					paramList.add( this.support.findPassMgtEntityBySVN() );

				} else {
					throw new TriSystemException( AmMessageId.AM004016F , scmType.value() );
				}

				for ( IDomain<IGeneralServiceBean> action : actions ) {
					action.execute( innerServiceDto );
				}
			}
			paramBean.setLotNo( lotDto.getLotEntity().getLotId() ) ;
			paramBean.setApplyNo( areqDto.getAreqEntity().getAreqId() );
			paramBean.getBaseInfoViewBean().setLotNo( lotDto.getLotEntity().getLotId() ) ;
			paramBean.getBaseInfoViewBean().setApplyDate( TriDateUtils.convertViewDateFormat( areqDto.getAreqEntity().getLendReqTimestamp() ) );
		}
	}

}
