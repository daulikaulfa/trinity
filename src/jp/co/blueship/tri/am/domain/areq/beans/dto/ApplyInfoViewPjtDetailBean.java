package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;

/**
 * 変更管理・変更管理詳細の申請情報一覧用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ApplyInfoViewPjtDetailBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 申請番号 */
	private String applyNo = null;
	/** 申請件名 */
	private String applySubject = null;
	/** 貸出申請日時 */
	private String lendApplyDate = null;
	/** 削除申請日時 */
	private String delApplyDate = null;
	/** 返却申請日時 */
	private String returnApplyDate = null;
	/** 返却承認日時 */
	private String returnApproveDate = null;
	/** 申請者 */
	private String applyUser = null;
	/** 申請者ＩＤ */
	private String applyUserId = null;
	/** ステータス */
	private String applyStatus = null;
	/** 申請区分 */
	private String applyId = null;
	/** 閲覧許可 */
	private boolean enableView = false ;

	/**
	 * 貸出／削除申請日時（算出値）
	 * @return
	 */
	public String getLendDelApplyDate() {
		if ( AreqCtgCd.RemovalRequest.value().equals(this.applyId) ) {
			return this.delApplyDate;
		} else {
			return this.lendApplyDate;
		}
	}
	/**
	 * 返却／削除申請日時（算出値）
	 * @return
	 */
	public String getReturnDelApplyDate() {
		if ( AreqCtgCd.RemovalRequest.value().equals(this.applyId) ) {
			return this.delApplyDate;
		} else {
			return this.returnApplyDate;
		}
	}
	public String getApplyId() {
		return applyId;
	}
	public void setApplyId(String applyId) {
		this.applyId = applyId;
	}

	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplySubject() {
		return applySubject;
	}
	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getLendApplyDate() {
		return lendApplyDate;
	}
	public void setLendApplyDate(String lendApplyDate) {
		this.lendApplyDate = lendApplyDate;
	}

	public String getReturnApplyDate() {
		return returnApplyDate;
	}
	public void setReturnApplyDate(String returnApplyDate) {
		this.returnApplyDate = returnApplyDate;
	}

	public String getReturnApproveDate() {
		return returnApproveDate;
	}
	public void setReturnApproveDate(String returnApproveDate) {
		this.returnApproveDate = returnApproveDate;
	}

	public String getApplyUser() {
		return applyUser;
	}
	public void setApplyUser( String applyUser ) {
		this.applyUser = applyUser;
	}

	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}

	public String getApplyClassify() {
		return DesignSheetFactory.getDesignSheet().getValue( AmDesignBeanId.applyId, this.applyId );
	}
	public boolean getIsRtn() {
		return AmBusinessJudgUtils.isReturnApply( this.applyId );
	}
	public boolean getIsLend() {
		return AmBusinessJudgUtils.isLendApply( this.applyId );
	}
	public boolean getIsDel() {
		return AmBusinessJudgUtils.isDeleteApply( this.applyId );
	}
	public String getDelApplyDate() {
		return delApplyDate;
	}
	public void setDelApplyDate(String delApplyDate) {
		this.delApplyDate = delApplyDate;
	}
	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	public boolean getEnableView() {
		return enableView;
	}
	public void setEnableView(boolean enableView) {
		this.enableView = enableView;
	}

}
