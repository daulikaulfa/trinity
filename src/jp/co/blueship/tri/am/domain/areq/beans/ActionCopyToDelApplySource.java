package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 削除資産格納パスに定義ファイルをコピーします。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ActionCopyToDelApplySource extends ActionPojoAbstract<IGeneralServiceBean> {

	private ICopy copy = null;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File file = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
				sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefTempFile ) );

		File binaryFile = new File(	AmLibraryAddonUtils.getDelApplyDefTempFilePath( paramList ),
				sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefTempFile ) );

		File outFilePath = AmDesignBusinessRuleUtils.getDelApplyDefApplyNoPath( paramList );

		try {
			copy.copy(	file,
						new File( outFilePath,
								sheet.getValue( AmDesignEntryKeyByChangec.delApplyDefFile )) );

			copy.copy(	binaryFile,
					new File( outFilePath,
							sheet.getValue( AmDesignEntryKeyByChangec.delApplyBinaryDefFile )) );

			TriFileUtils.delete( file );
			TriFileUtils.delete( binaryFile );
		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005055S , e );
		}

		return serviceDto;
	}
}
