package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsEntity;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請処理の現在の申請状況を記録します。
 * <li>処理ステータス：("1")
 * <li>完了ステータス：("0")
 * として、指定された処理ＩＤが実行履歴として登録されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionInsertReturnProcess extends ActionPojoAbstract<IGeneralServiceBean> {

	private IProcDetailsDao procDao = null;
	private boolean isRecord = true;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param dao アクセスインタフェース
	 */
	public final void setProcDetailsDao( IProcDetailsDao dao ) {
		procDao = dao;
	}

	/**
	 * 申請状況を記録する場合、trueを設定します。falseを設定すると、記録を行いません。
	 * @param record 記録を行う場合true。
	 */
	public final void setRecord( boolean record ) {
		isRecord = record;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		if ( ! isRecord ){
			return serviceDto;
		}

		List<Object> paramList = serviceDto.getParamList();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		IProcDetailsEntity entity = new ProcDetailsEntity();

		entity.setProcId			( paramBean.getProcId() );
		entity.setProcCtgCd			( param.getProcCtgCd() );
		entity.setProcStTimestamp	( param.getProcessDate() );
		entity.setStsId				( StatusFlg.on.value() );
		entity.setCompStsId			( StatusFlg.off );
		entity.setMsgId				( "" );
		entity.setMsg				( "" );

		entity.setRegUserId			( param.getUserId() );
		entity.setRegUserNm			( param.getUser() );

		procDao.insert( entity );

		return serviceDto;
	}
}
