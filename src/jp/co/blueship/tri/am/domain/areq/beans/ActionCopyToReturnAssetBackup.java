package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の資産をバックアップします。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ActionCopyToReturnAssetBackup extends ActionPojoAbstract<IGeneralServiceBean> {

	private ICopy copy = null;

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}


	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File src	= AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );
		File dest	= AmDesignBusinessRuleUtils.getReturnAssetApplyNoBackupPath( paramList );

		try {

			if ( dest.exists() ) {
				TriFileUtils.delete( dest );
			}

			copy.copy( src, dest );

		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005057S , e );
		}

		return serviceDto;
	}

}
