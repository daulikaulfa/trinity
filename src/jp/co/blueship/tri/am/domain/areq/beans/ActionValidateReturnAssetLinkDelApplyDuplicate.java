package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却資産申請の登録時の妥当性チェックを行います。
 * <li>コンフリクトチェック（削除資産申請との重複）
 * <br>
 * <br>この処理は、返却資産申請の妥当性チェック後に呼び出されます。
 * <br>原本比較、他資産申請との比較済で呼び出されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionValidateReturnAssetLinkDelApplyDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		check( serviceDto );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto )
		throws ContinuableBusinessException {

		duplicationCheck( serviceDto );

	}

	/**
	 * 他の削除資産申請情報とコンフリクトチェックを行います。
	 * <li>今回申請するファイルと他の削除資産申請ファイルを順次比較します。
	 * <li>資産が重複する場合、エラーとします。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void duplicationCheck( IServiceDto<IGeneralServiceBean> serviceDto )
			throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		File infoPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );
		IFileDiffResult[] assetFiles = this.getFileDiffResult( paramList );

		IAreqDto assetDto = AmExtractEntityAddonUtils.extractReturnApply( paramList );
		if ( null == assetDto ) {
			throw new TriSystemException( AmMessageId.AM005003S);
		}

		AreqCondition condition =
			AmDBSearchConditionAddonUtils.getAssetDelApplyConditionByDuplicate( assetDto.getAreqEntity().getLotId() );
		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> areqEntities =
				support.getAreqDao().find( condition.getCondition(), sort );

		try {
			for ( IAreqEntity entity : areqEntities ) {
				List<IAreqFileEntity> removalFileEntities = this.support.findAreqFileEntities(entity.getAreqId(), AreqCtgCd.RemovalRequest);

				for ( IAreqFileEntity path : removalFileEntities) {

					File delApplyFile = new File( infoPath, path.getFilePath() );

					for ( int j = 0; j < assetFiles.length; j++ ) {
						File returnAssetFile = new File( infoPath, assetFiles[j].getFilePath() );

						if ( ! delApplyFile.equals( returnAssetFile ) )
							continue;

						messageList.add( AmMessageId.AM001044E );
						messageArgsList.add( new String[] { assetFiles[j].getFilePath() , entity.getPjtId() , entity.getAreqId() } );
					}
				}
			}
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 比較元と比較先の差分ファイル情報を取得します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 */
	private final IFileDiffResult[] getFileDiffResult( List<Object> list ) {

		List<IFileDiffResult> outList = new ArrayList<IFileDiffResult>();

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( !(obj instanceof IFileDiffResult[]) )
				continue;

			return (IFileDiffResult[])obj;
		}

		return (IFileDiffResult[])outList.toArray( new IFileDiffResult[0] );
	}
}
