package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

/**
 * 変更管理承認・申請情報一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ApplyInfoViewPjtApproveBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 申請番号 */
	private String applyNo = null;
	/** 申請件名 */
	private String applySubject = null;
	/** 申請日時 */
	private String applyDate = null;
	/** 申請者 */
	private String applyUser = null;
	/** 申請者ＩＤ */
	private String applyUserId = null;
	/** ステータス */
	private String applyStatus = null;
	/** 承認チェックボックスの表示の有無 */
	private boolean approveView = false;
	/** 却下ボタンの表示の有無 */
	private boolean isPjtApproveRejectEnabled = false;
	/** 承認対象の申請かどうか */
	private boolean isPjtApproveEnabled = false;
	/** 承認対象の背景色 */
	private String pjtApproveColor = null;
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplySubject() {
		return applySubject;
	}
	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate( String applyDate ) {
		this.applyDate = applyDate;
	}
	
	public String getApplyUser() {
		return applyUser;
	}
	public void setApplyUser( String applyUser ) {
		this.applyUser = applyUser;
	}
	
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
	
	public boolean getApproveView() {
		return approveView;
	}
	public void setApproveView( boolean approveView ) {
		this.approveView = approveView;
	}
	
	public boolean isPjtApproveRejectEnabled() {
		return isPjtApproveRejectEnabled;
	}
	public void setPjtApproveRejectEnabled( boolean isPjtApproveRejectEnabled ) {
		this.isPjtApproveRejectEnabled = isPjtApproveRejectEnabled;
	}
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	public boolean isPjtApproveEnabled() {
		return isPjtApproveEnabled;
	}
	public void setPjtApproveEnabled(boolean isPjtApproveEnabled) {
		this.isPjtApproveEnabled = isPjtApproveEnabled;
	}
	public String getPjtApproveColor() {
		return pjtApproveColor;
	}
	public void setPjtApproveColor(String pjtApproveColor) {
		this.pjtApproveColor = pjtApproveColor;
	}
	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	
}

