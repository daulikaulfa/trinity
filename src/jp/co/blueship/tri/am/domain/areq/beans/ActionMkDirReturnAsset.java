package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の申請単位の格納フォルダを作成します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMkDirReturnAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList ).mkdirs();

		} catch (SecurityException e) {
			throw new TriSystemException( AmMessageId.AM005064S , e );
		}

		return serviceDto;
	}
}
