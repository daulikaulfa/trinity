package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の登録前の妥当性チェックを行います。
 * <li>貸出申請済みでない場合、資産申請出来ない旨のエラーとなります。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateReturnApplyForLend extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( paramList );
		if ( null == dto ) {
			throw new BusinessException( AmMessageId.AM001057E );
		}

		return serviceDto;
	}

}
