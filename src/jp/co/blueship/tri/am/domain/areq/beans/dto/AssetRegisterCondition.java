package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

/**
 *
 * @version V3L11.01
 * @author Siti Hajar
 *
 */

public class AssetRegisterCondition implements Serializable {

	private static final long serialVersionUID = 1L;
	/**
	 * Lot Id's
	 */
	private String[] lotId = null;
	/**
	 * Lot Name's
	 */
	private String[] lotNm = null;
	/**
	 * Module Name's
	 */
	private String[] mdlNm = null;
	/**
	 * Resource Path's
	 */
	private String[] filePath = null;
	/**
	 * Change Property's
	 */
	private String[] pjtId = null;
	/**
	 * Change Factor No's
	 */
	private String[] chgFactorNo = null;
	/**
	 * (Checkout or Check-in or Removal)Inquiry User Id's
	 */
	private String[] reqUserId = null;
	/**
	 * (Checkout or Check-in or Removal)Inquiry Group Name
	 */
	private String[] grpNm = null;
	/**
	 * Status Id's
	 */
	private String[] stsId = null;
	/**
	 * Process Status Id's
	 */
	private String[] procStsId = null;
	/**
	 * Target MasterWork
	 */
	public boolean targetMW = true;


	/**
	 * Lot Id'sを取得します。
	 * @return Lot Id's
	 */
	public String[] getLotId() {
		return lotId;
	}

	/**
	 * Lot Id'sを設定します。
	 */
	public void setLotId(String... lotId) {
		this.lotId = lotId;
	}

	/**
	 * Lot Name'sを取得します。
	 * @return Lot Id's
	 */
	public String[] getLotNm() {
		return lotNm;
	}

	/**
	 * Lot Name'sを設定します。
	 */
	public void setLotNm(String... lotNm) {
		this.lotNm = lotNm;
	}

	/**
	 * Module Name'sを取得します。
	 * @return Module Name's
	 */
	public String[] getMdlNm() {
		return mdlNm;
	}

	/**
	 * Module Name'sを設定します。
	 */
	public void setMdlNm(String... mdlNm) {
		this.mdlNm = mdlNm;
	}

	/**
	 * Resource Path'sを取得します。
	 * @return Resource Path's
	 */
	public String[] getFilePath() {
		return filePath;
	}

	/**
	 * Resource Path'sを設定します。
	 */
	public void setFilePath(String... filePath) {
		this.filePath = filePath;
	}

	/**
	 * Change Property'sを取得します。
	 * @return Change Property's
	 */
	public String[] getPjtId() {
		return pjtId;
	}

	/**
	 * Change Property'sを設定します。
	 */
	public void setPjtId(String... pjtId) {
		this.pjtId = pjtId;
	}

	/**
	 * Change Factor No'sを取得します。
	 * @return Change Factor No's
	 */
	public String[] getChgFactorNo() {
		return chgFactorNo;
	}

	/**
	 * Change Factor No'sを設定します。
	 */
	public void setChgFactorNo(String... chgFactorNo) {
		this.chgFactorNo = chgFactorNo;
	}

	/**
	 * (Checkout or Check-in or Removal)Inquiry User Id'sを取得します。
	 * @return (Checkout or Check-in or Removal)Inquiry User Id's
	 */
	public String[] getReqUserId() {
		return reqUserId;
	}

	/**
	 * (Checkout or Check-in or Removal)Inquiry User Id'sを設定します。
	 */
	public void setReqUserId(String... reqUserId) {
		this.reqUserId = reqUserId;
	}

	/**
	 * (Checkout or Check-in or Removal)Inquiry Group Nameを取得します。
	 * @return (Checkout or Check-in or Removal)Inquiry Group Name
	 */
	public String[] getGrpNm() {
		return grpNm;
	}

	/**
	 * (Checkout or Check-in or Removal)Inquiry Group Nameを設定します。
	 */
	public void setGrpNm(String... grpNm) {
		this.grpNm = grpNm;
	}

	/**
	 * Status Id'sを取得します。
	 * @return Status Id's
	 */
	public String[] getStsId() {
		return stsId;
	}

	/**
	 * Status Id'sを設定します。
	 */
	public void setStsId(String... stsId) {
		this.stsId = stsId;
	}

	/**
	 * Process Status Id'sを取得します。
	 * @return Process Status Id's
	 */
	public String[] getProcStsId() {
		return procStsId;
	}

	/**
	 * Process Status Id'sを設定します。
	 */
	public void setProcStsId(String... procStsId) {
		this.procStsId = procStsId;
	}

	/**
	 * Target MasterWorkを取得します。
	 * @return Target MasterWork
	 */
	public boolean isTargetMW() {
	    return targetMW;
	}

	/**
	 * Target MasterWorkを設定します。
	 * @param targetMW Target MasterWork
	 */
	public void setTargetMW(boolean targetMW) {
	    this.targetMW = targetMW;
	}

}
