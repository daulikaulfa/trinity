package jp.co.blueship.tri.am.domain.areq;

import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibMasterDelEntryCancelServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除申請情報の閲覧を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibMasterDelDetailViewService implements IDomain<FlowChaLibMasterDelDetailViewServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelDetailViewServiceBean> execute( IServiceDto<FlowChaLibMasterDelDetailViewServiceBean> serviceDto) {

		FlowChaLibMasterDelDetailViewServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			// 業務ロジックを記述
			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.MASTER_DEL_DETAIL_VIEW ) &&
					!forward.equals( ChaLibScreenID.MASTER_DEL_DETAIL_VIEW )) {
				return serviceDto;
			}

			String delApplyNo = paramBean.getDelApplyNo();

			IAreqEntity assetApplyEntity = this.support.findAreqEntity( delApplyNo );
			List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(delApplyNo, AreqCtgCd.RemovalRequest);
			List<IAreqBinaryFileEntity> areqBinaryFileEntities = this.support.findAreqBinaryFileEntities(delApplyNo, AreqCtgCd.RemovalRequest);
			IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() ) ;

			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( assetApplyEntity );
			areqDto.setAreqFileEntities( areqFileEntities );
			areqDto.setAreqBinaryFileEntities( areqBinaryFileEntities );

			paramBean.setDelApplyNo(delApplyNo);
			paramBean.setLotNo( pjtEntity.getLotId() ) ;

			if (forward.equals(ChaLibScreenID.MASTER_DEL_DETAIL_VIEW)) {

				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));


				paramBean.setViewServiceBean( AmCnvEntityToDtoUtils.convertChaLibMasterDelEntryViewBean( pjtEntity , areqDto) );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

}
