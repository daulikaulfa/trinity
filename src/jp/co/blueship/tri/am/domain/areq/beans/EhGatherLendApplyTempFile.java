package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendApplyFileResult;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 一時作業パスに展開された貸出申請フォーマットファイルを読み出します。
 * （資産ファイル）
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhGatherLendApplyTempFile extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = new File(	AmLibraryAddonUtils.getLendApplyDefTempFilePath( paramList ),
									sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefTempFile ) );

			FileInputStream fis  = null ;
			InputStreamReader isr = null ;
			String[] files = null ;
			try {
				fis = new FileInputStream( file ) ;
				isr = new InputStreamReader( fis ) ;
				files = TriFileUtils.readFileLine( isr , false );
			} catch( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isr);
				TriFileUtils.close(fis);
			}


			List<IEhLendApplyFileResult> parsePath = new ArrayList<IEhLendApplyFileResult>();

			for ( int i = 0; i < files.length; i++ ) {
				IEhLendApplyFileResult result = new EhLendApplyFileResult();
				result.setFilePath( FlowChaLibLendEditSupport.convertPath( files[i] ));
				parsePath.add( result );
			}

			paramList.add( parsePath.toArray( new IEhLendApplyFileResult[] {} ));

		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		}

		return serviceDto;

	}

}
