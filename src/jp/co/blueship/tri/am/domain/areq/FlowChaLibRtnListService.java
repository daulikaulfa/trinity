package jp.co.blueship.tri.am.domain.areq;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDBSearchSortAddonUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.AmViewInfoAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnListServiceBean.ChaLibApplyViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * FlowChaLibRtnListServiceイベントのサービスClass <br>
 * <p>
 * 変更管理 返却申請一覧の処理を行う。
 * </p>
 *
 * @version V3L10.02
 * @author Eguchi Yukihiro
 *
 */
public class FlowChaLibRtnListService implements IDomain<FlowChaLibRtnListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support = null;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnListServiceBean> execute(IServiceDto<FlowChaLibRtnListServiceBean> serviceDto) {

		FlowChaLibRtnListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String selectedLotNo = paramBean.getSelectedLotNo();

			if ( TriStringUtils.isNotEmpty(selectedLotNo)) {
				// グループの存在チェック
				ILotDto lotDto = this.support.findLotDto( selectedLotNo );
				AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
						this.support.getUmFinderSupport().getGrpDao(),//
						this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));
			}

			List<LotViewBean> lotViewBeanList = this.support.getLotViewBeanForPullDown(paramBean);

			paramBean.setLotViewBeanList(lotViewBeanList);
			paramBean.setSelectedLotNo(selectedLotNo);

			int selectPageNo = ((0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo());

			IEntityLimit<IAreqEntity> entityLimit = getAssetEntityList(paramBean);

			this.setServiceBeanSearchResult(paramBean, entityLimit);
			paramBean.setSelectPageNo(selectPageNo);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}
	}

	/**
	 *
	 * @param paramBean
	 * @param mode
	 * @return
	 */
	private final IEntityLimit<IAreqEntity> getAssetEntityList(FlowChaLibRtnListServiceBean paramBean) {
		String selectedLotNo = paramBean.getSelectedLotNo();

		IJdbcCondition condition = getCondition(paramBean);
		ISqlSort sort = AmDBSearchSortAddonUtils.getAssetApplySortFromDesignDefineByChaLibRtnList();
		int selectPageNo = ((0 == paramBean.getSelectPageNo()) ? 1 : paramBean.getSelectPageNo());
		int maxPageNumber = sheet.intValue(AmDesignEntryKeyByChangec.maxPageNumberByRtnList);

		IEntityLimit<IAreqEntity> entityLimit = null;

		if ( TriStringUtils.isNotEmpty(selectedLotNo)) {
			entityLimit = this.support.getAreqDao().find(condition.getCondition(), sort, selectPageNo, maxPageNumber);
		}

		return entityLimit;
	}

	private final IJdbcCondition getCondition(FlowChaLibRtnListServiceBean paramBean) {

		AreqCondition condition = new AreqCondition();

		condition.setLotId(paramBean.getSelectedLotNo());
		condition.setStsIds(new String[] { AmAreqStatusId.CheckoutRequested.getStatusId() });
		condition.setLendReqUserIds(new String[] { paramBean.getUserId() });

		return condition;
	}

	private final void setServiceBeanSearchResult(FlowChaLibRtnListServiceBean paramBean, IEntityLimit<IAreqEntity> entityLimit) {

		List<ChaLibApplyViewBean> list = new ArrayList<ChaLibApplyViewBean>();

		ILimit limit = null;

		if (null != entityLimit) {
			IEntityLimit<IAreqEntity> assetEntityLimit = (IEntityLimit<IAreqEntity>) entityLimit;

			IAreqEntity[] entitys = assetEntityLimit.getEntities().toArray(new IAreqEntity[0]);

			// 処理高速化のために、Pjtをまとめて取得し、Mapで受け取る
			String[] pjtNoArray = this.support.getPjtNoArrayFromAssetApplyArray(entitys);
			Map<String, IPjtEntity> pjtEntityMap = this.support.getPjtEntityPjtNoMap(pjtNoArray);

			for (IAreqEntity entity : entitys) {
				ChaLibApplyViewBean viewBean = paramBean.newApplyViewBean();

				AmViewInfoAddonUtils.setChaLibApplyViewBeanFromAssetApplyEntity(viewBean, entity);

				IPjtEntity pjtEntity = pjtEntityMap.get(entity.getPjtId());
				if (null == pjtEntity) {
					throw new TriSystemException(AmMessageId.AM004017F, entity.getPjtId());
				}
				viewBean.setChangeCauseNo(pjtEntity.getChgFactorNo());
				list.add(viewBean);
			}

			limit = assetEntityLimit.getLimit();
		}

		paramBean.setApplyViewBeanList(list);
		paramBean.setPageInfoView(AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit));
	}

}
