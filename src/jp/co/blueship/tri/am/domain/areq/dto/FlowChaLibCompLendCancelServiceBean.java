package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibCompLendCancelServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * 申請情報番号
	 */
	private String applyNo;

	/**
	 * @return applyNo
	 */
	public String getApplyNo() {
		return applyNo;
	}

	/**
	 * @param applyNo 設定する applyNo
	 */
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

}
