package jp.co.blueship.tri.am.domain.areq.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibRtnEntryServiceBean  extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * ロット番号
	 */
	private String lotId;
	/**
	 *  申請番号
	 */
	private String applyNo;
	/**
	 *  申請者
	 */
	private String applyUserName = null;
	/**
	 *  申請者ＩＤ
	 */
	private String applyUserId = null;
	/**
	 *  申請者グループ
	 */
	private String applyUserGroup = null;
	/**
	 * 貸出申請基本情報Bean（登録画面・編集確認・詳細画面表示用）
	 */
	private ChaLibEntryBaseInfoBean chaLibEntryBaseInfoBean;

	/**
	 * 申請資産情報Bean（登録画面・編集確認・詳細画面表示用）
	 */
	private ChaLibEntryAssetResourceInfoBean chaLibEntryAssetResourceInfoBean;
	/**
	 *  チェック状況
	 */
	private List<ChaLibRtnEntryCheckProcessViewBean> checkProcessViewBeanList = new ArrayList<ChaLibRtnEntryCheckProcessViewBean>();
	/**
	 *  エラーメッセージ
	 */
	private String errMessage;
	/**
	 *  完了ステータス
	 */
	private String completeStatus;
	/**
	 *  ２重貸出チェック
	 */
	private boolean duplicationCheck;

	/**
	 * 添付ファイルの名前
	 */
	private String returnAssetAppendFile ;

	/**
	 * 添付ファイルのストリーム
	 */
	private byte[] returnAssetAppendFileInputStreamBytes ;
	/**
	 *
	 */
	private String returnAssetAppendFileInputStreamName ;

	public byte[] getReturnAssetAppendFileInputStreamBytes() {
		return returnAssetAppendFileInputStreamBytes;
	}

	public void setReturnAssetAppendFileInputStreamBytes( byte[] returnAssetAppendFileInputStreamBytes ) {
		this.returnAssetAppendFileInputStreamBytes = returnAssetAppendFileInputStreamBytes;
	}

	public String getReturnAssetAppendFileInputStreamName() {
		return returnAssetAppendFileInputStreamName;
	}

	public void setReturnAssetAppendFileInputStreamName(String returnAssetAppendFileInputStreamName) {
		this.returnAssetAppendFileInputStreamName = returnAssetAppendFileInputStreamName;
	}


	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean duplicationCheck ) {
		this.duplicationCheck = duplicationCheck;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyUserGroup() {
		return applyUserGroup;
	}

	public void setApplyUserGroup(String applyUserGroup) {
		this.applyUserGroup = applyUserGroup;
	}

	public String getApplyUserName() {
		return applyUserName;
	}

	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}

	public List<ChaLibRtnEntryCheckProcessViewBean> getCheckProcessViewBeanList() {
		return checkProcessViewBeanList;
	}

	public void setCheckProcessViewBeanList(
			List<ChaLibRtnEntryCheckProcessViewBean> checkProcessViewBeanList) {
		this.checkProcessViewBeanList = checkProcessViewBeanList;
	}


	/**
	 * @return chaLibEntryAssetResourceInfoBean
	 */
	public ChaLibEntryAssetResourceInfoBean getChaLibEntryAssetResourceInfoBean() {
		return chaLibEntryAssetResourceInfoBean;
	}

	/**
	 * @param chaLibEntryAssetResourceInfoBean 設定する chaLibEntryAssetResourceInfoBean
	 */
	public void setChaLibEntryAssetResourceInfoBean(
			ChaLibEntryAssetResourceInfoBean chaLibEntryAssetResourceInfoBean) {
		this.chaLibEntryAssetResourceInfoBean = chaLibEntryAssetResourceInfoBean;
	}

	/**
	 * @return chaLibEntryBaseInfoBean
	 */
	public ChaLibEntryBaseInfoBean getChaLibEntryBaseInfoBean() {
		return chaLibEntryBaseInfoBean;
	}

	/**
	 * @param chaLibEntryBaseInfoBean 設定する chaLibEntryBaseInfoBean
	 */
	public void setChaLibEntryBaseInfoBean(
			ChaLibEntryBaseInfoBean chaLibEntryBaseInfoBean) {
		this.chaLibEntryBaseInfoBean = chaLibEntryBaseInfoBean;
	}

	public String getCompleteStatus() {
		return completeStatus;
	}

	public void setCompleteStatus(String completeStatus) {
		this.completeStatus = completeStatus;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}

	public void setReturnAssetAppendFile(String attachFile) {
		this.returnAssetAppendFile = attachFile;
	}

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ChaLibRtnEntryCheckProcessViewBean newChaLibRtnEntryCheckProcessViewBean() {
		ChaLibRtnEntryCheckProcessViewBean bean = new ChaLibRtnEntryCheckProcessViewBean();
		return bean;
	}

	public class ChaLibRtnEntryCheckProcessViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/**
		 *  プロセス
		 */
		private String processName = null;
		/**
		 *  プロセスステータス
		 */
		private String processStatus = null;

		/**
		 * @return processName
		 */
		public String getProcessName() {
			return processName;
		}

		/**
		 * @param processName 設定する processName
		 */
		public void setProcessName(String processName) {
			this.processName = processName;
		}

		/**
		 * @return processStatus
		 */
		public String getProcessStatus() {
			return processStatus;
		}

		/**
		 * @param processStatus 設定する processStatus
		 */
		public void setProcessStatus(String processStatus) {
			this.processStatus = processStatus;
		}

	}

	public String getApplyUserId() {
		return applyUserId;
	}
	public void setApplyUserId(String applyUserId) {
		this.applyUserId = applyUserId;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
}
