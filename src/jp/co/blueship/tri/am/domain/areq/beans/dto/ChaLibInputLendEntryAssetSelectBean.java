package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

public class ChaLibInputLendEntryAssetSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 入力貸出資産のパス
	 */
	private String[] inAssetResourcePath;
	/**
	 * ディレクトリ選択
	 */
	private String inDirectory;

	public String[] getInAssetResourcePath() {
		return inAssetResourcePath;
	}

	public void setInAssetResourcePath(String[] inAssetResourcePath) {
		this.inAssetResourcePath = inAssetResourcePath;
	}

	public String getInDirectory() {
		return inDirectory;
	}

	public void setInDirectory(String inDirectory) {
		this.inDirectory = inDirectory;
	}

}
