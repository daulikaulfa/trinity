package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.LendingReqNumberingDao;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 申請情報番号の自動採番を行います。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ActionNumberingApplyNoFromDraft extends ActionPojoAbstract<FlowChaLibLendModifyServiceBean> {

	LendingReqNumberingDao numberingDao = null;

	public void setNumberingDao(LendingReqNumberingDao numberingDao) {
		this.numberingDao = numberingDao;
	}

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute( IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();
		List<Object> selected = TriCollectionUtils.select(paramList, TriPredicates.isInstanceOf(IAreqDto.class));

		PreConditions.assertOf(selected.size() == 1, "IAreqEntity should be contained only one instance.");

		IAreqEntity areqEntity = ((IAreqDto) selected.get(0)).getAreqEntity();
		String areqId = null;

		PreConditions.assertOf(AmAreqStatusId.CheckoutRequested.equals(areqEntity.getStsId()), "Status is not " + AmAreqStatusId.CheckoutRequested.getStatusId() + ".");

		areqId = this.numberingDao.nextval();
		areqEntity.setAreqId( areqId );
		serviceDto.getServiceBean().setApplyNo( areqId );

		return serviceDto;

	}
}
