package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibLendDetailViewイベントのサービスClass <br>
 * <p>
 * 変更管理 貸出申請履歴詳細画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibLendHistoryDetailViewService implements IDomain<FlowChaLibLendHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLendHistoryDetailViewServiceBean> execute(IServiceDto<FlowChaLibLendHistoryDetailViewServiceBean> serviceDto) {

		FlowChaLibLendHistoryDetailViewServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String applyNo = paramBean.getApplyNo();

			IAreqDto areqDto = this.support.findAreqDto(applyNo);
			IPjtEntity pjtEntity = this.support.findPjtEntity(areqDto.getAreqEntity().getPjtId());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			paramBean.setApplyNo(applyNo);

			paramBean.setBaseInfoViewBean(AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply(pjtEntity, areqDto ) );
			paramBean.setStatusView( areqDto.getAreqEntity().getProcStsId() );
			paramBean.setAssetSelectViewBean(AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByLendApply( lotDto, areqDto ) );

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}

	}

}
