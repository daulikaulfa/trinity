package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却情報申請の返却資産ファイルの妥当性チェックを行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionValidateReturnInfoFiles extends ActionPojoAbstract<IGeneralServiceBean> {

	private IExtensionValidator extensionValidate = null;
	public void setExtensionValidator( IExtensionValidator extensionValidate ) {
		this.extensionValidate = extensionValidate;
	}

	private IFilePathValidator filePathValidate = null;
	public void setFilePathValidator( IFilePathValidator filePathValidate ) {
		this.filePathValidate = filePathValidate;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File file = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

		this.extensionValidate.validate( file );
		this.filePathValidate.validate( file );

		return serviceDto;
	}

}
