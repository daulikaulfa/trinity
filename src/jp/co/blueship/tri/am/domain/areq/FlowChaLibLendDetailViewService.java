package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibLendDetailViewイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出申請詳細画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendDetailViewService implements IDomain<FlowChaLibLendDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibLendEditSupport support = null;

	public void setSupport(FlowChaLibLendEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibLendDetailViewServiceBean> execute(IServiceDto<FlowChaLibLendDetailViewServiceBean> serviceDto) {

		FlowChaLibLendDetailViewServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			// 業務ロジックを記述
			String applyNo = paramBean.getApplyNo();

			IAreqDto areqDto = this.support.findAreqDto( applyNo );
			IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() ) ;

			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			//貸出フォルダの存在有無をチェック、存在しなければ空フォルダを作成する。
			//貸出取消時に失敗した際、再び取消ができない問題に対処するため。
			this.chkLendApplyDir( lotDto, areqDto ) ;

			paramBean.setLotNo( pjtEntity.getLotId() ) ;
			paramBean.setApplyNo( applyNo );

			paramBean.setBaseInfoViewBean( AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply( pjtEntity , areqDto ) );
			paramBean.setStatusView( areqDto.getAreqEntity().getProcStsId() );
			paramBean.setAssetSelectViewBean(
					AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByLendApply( lotDto, areqDto ));

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S, e , paramBean.getFlowAction() );
		}

	}
	/**
	 * 貸出フォルダの存在有無をチェック、存在しなければ空フォルダを作成する。
	 * @param lotDto
	 * @param areqDto
	 */
	private void chkLendApplyDir( ILotDto lotDto , IAreqDto areqDto ) {

		List<Object> list = new ArrayList<Object>();
		list.add( lotDto );
		list.add( areqDto );
		File lendApplyPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( list );
		if( ! lendApplyPath.exists() ) {
			lendApplyPath.mkdirs() ;
		}
	}
}
