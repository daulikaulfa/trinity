package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibRtnDetailViewイベントのサービスClass <br>
 * <p>
 * 変更管理 返却申請詳細画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibRtnDetailViewService implements IDomain<FlowChaLibRtnDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support = null;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnDetailViewServiceBean> execute(IServiceDto<FlowChaLibRtnDetailViewServiceBean> serviceDto) {

		FlowChaLibRtnDetailViewServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String applyNo = paramBean.getApplyNo();

			IAreqDto areqDto = this.support.findAreqDto(applyNo);
			IPjtEntity pjtEntity = this.support.findPjtEntity(areqDto.getAreqEntity().getPjtId());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			paramBean.setApplyNo(applyNo);
			paramBean.setLotNo(pjtEntity.getLotId());
			paramBean.setBaseInfoViewBean(AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByReturnInfo( pjtEntity, areqDto ) );
			paramBean.setStatusView( areqDto.getAreqEntity().getProcStsId() );
			paramBean.setReturnAssetAppendFile(paramBean.getReturnAssetAppendFile());
			paramBean.setAssetSelectViewBean(AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByReturnInfo( lotDto, areqDto ) );

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}

	}

}
