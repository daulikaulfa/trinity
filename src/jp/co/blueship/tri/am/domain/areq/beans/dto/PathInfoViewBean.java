package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・貸出資産のカレントパス関連情報一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PathInfoViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ディレクトリ名 */
	private String dirName = null ;
	/** ディレクトリ名 */
	private String fullPath = null ;
	
	public String getDirName() {
		return dirName;
	}
	public void setDirName(String dirName) {
		this.dirName = dirName;
	}
	public String getFullPath() {
		return fullPath;
	}
	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}
	
	

}
