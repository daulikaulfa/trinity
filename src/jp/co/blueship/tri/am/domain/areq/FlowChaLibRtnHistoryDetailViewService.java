package jp.co.blueship.tri.am.domain.areq;

import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibRtnDetailViewイベントのサービスClass <br>
 * <p>
 * 変更管理 返却申請履歴詳細画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibRtnHistoryDetailViewService implements IDomain<FlowChaLibRtnHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support = null;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnHistoryDetailViewServiceBean> execute(IServiceDto<FlowChaLibRtnHistoryDetailViewServiceBean> serviceDto) {

		FlowChaLibRtnHistoryDetailViewServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String applyNo = paramBean.getApplyNo();

			IAreqEntity assetApplyEntity = this.support.findAreqEntity(applyNo);
			List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(applyNo);
			List<IAreqBinaryFileEntity> areqBinaryFileEntities = this.support.findAreqBinaryFileEntities(applyNo);
			List<IAreqAttachedFileEntity> areqAttachedFileEntities = this.support.findAreqAttachedFileEntities(applyNo);
			IPjtEntity pjtEntity = this.support.findPjtEntity(assetApplyEntity.getPjtId());
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity(assetApplyEntity);
			areqDto.setAreqFileEntities(areqFileEntities);
			areqDto.setAreqBinaryFileEntities(areqBinaryFileEntities);

			// グループの存在チェック
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			paramBean.setLotNo(pjtEntity.getLotId());
			paramBean.setApplyNo(applyNo);

			paramBean.setBaseInfoViewBean(AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByReturnAsset(pjtEntity, assetApplyEntity,
					areqAttachedFileEntities));
			paramBean.setStatusView(sheet.getValue(AmDesignBeanId.statusId, assetApplyEntity.getProcStsId()));
			paramBean.setAssetSelectViewBean(AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByReturnAsset(lotDto, areqDto));

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, paramBean.getFlowAction());
		}

	}

}
