package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 貸出申請の資産をバックアップします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EhActionCopyToLendAssetBackup extends ActionPojoAbstract<IGeneralServiceBean> {

	private ICopy copy = null;

	/**
	 * ファイルコピーがインスタンス生成時に自動的に設定されます。
	 * @param c ファイルコピー
	 */
	public final void setCopy( ICopy c ) {
		copy = c;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		File src = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );
		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( paramList );

		File dest = AmDesignBusinessRuleUtils.getReturnAssetApplyNoBackupPath( paramList, dto.getAreqEntity() );

		try {
			copy.copy(src, dest);

		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005067S , e );
		}

		return serviceDto;
	}

}
