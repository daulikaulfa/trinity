package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 削除資産定義の申請単位の格納フォルダを作成します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMkDirDelApplyDef extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = AmDesignBusinessRuleUtils.getDelApplyDefApplyNoPath( paramList );

			if ( file.isDirectory() ){
				throw new TriSystemException(AmMessageId.AM005008S , file.getName() );
			}
			file.mkdirs();

		} catch (SecurityException e) {
			throw new TriSystemException( AmMessageId.AM005063S , e );
		}

		return serviceDto;
	}

}
