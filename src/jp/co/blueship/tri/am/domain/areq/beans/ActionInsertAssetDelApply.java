package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IDelApplyParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.am.support.FlowChaLibScmEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.eb.IPassMgtEntity;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsServiceFactory;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.ex.ScmSysException;

/**
 * 削除申請された資産（依頼）の登録を行います。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ActionInsertAssetDelApply extends ActionPojoAbstract<IGeneralServiceBean> {
	private FlowChaLibMasterDelEditSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowChaLibMasterDelEditSupport support ) {
		this.support = support;
	}

	private VcsServiceFactory factory;
	public void setVcsServiceFactory(VcsServiceFactory factory) {
		this.factory = factory;
	}
	private IVcsService vcsService = null;

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {

			ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
			ILotEntity lotEntity = lotDto.getLotEntity();

			if ( TriStringUtils.isEmpty( lotEntity ) ) {
				throw new TriSystemException( AmMessageId.AM005007S ) ;
			}

			IAreqDto areqDto = AmExtractEntityAddonUtils.extractDeleteApply( paramList );
			if ( null == areqDto.getAreqEntity() ) {
				throw new TriSystemException( AmMessageId.AM005003S );
			}

			areqDto.setAreqFileEntities( new ArrayList<IAreqFileEntity>() );
			areqDto.setAreqBinaryFileEntities( new ArrayList<IAreqBinaryFileEntity>() );

			VcsCategory scmType = VcsCategory.value( lotEntity.getVcsCtgCd() ) ;
			if( VcsCategory.SVN.equals( scmType ) ) {

				IPassMgtEntity passwordEntity = AmExtractEntityAddonUtils.extractPasswordEntity( paramList ) ;

				vcsService = factory.createService(scmType ,passwordEntity.getUserId(), passwordEntity.getPassword());

				Map<String,SVNStatus> statusMap = new HashMap<String,SVNStatus>();

				String workPath = lotEntity.getLotMwPath() ;

				IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( paramList );

				FlowChaLibScmEditSupport.getStatus( vcsService , workPath , files , statusMap );
				areqDto = this.support.setAssetFileEntity(
								paramList,
								statusMap,
								areqDto );

			} else {
				throw new TriSystemException( AmMessageId.AM004016F , scmType.value() ) ;
			}
			support.getAreqDao().insert( areqDto.getAreqEntity() );
			support.getAreqFileDao().insert( areqDto.getAreqFileEntities() );
			support.getAreqBinaryFileDao().insert( areqDto.getAreqBinaryFileEntities() );

			IDelApplyParamInfo param = AmExtractMessageAddonUtils.getDelApplyParamInfo(paramList);

			support.getUmFinderSupport().registerCtgLnk(param.getCtgId(), AmTables.AM_AREQ, areqDto.getAreqEntity().getAreqId());
			support.getUmFinderSupport().registerMstoneLnk(param.getMstoneId(), AmTables.AM_AREQ, areqDto.getAreqEntity().getAreqId());

			if ( DesignSheetUtils.isRecord() ) {

				((AreqEntity)areqDto.getAreqEntity()).setLotId( lotEntity.getLotId() );

				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.Request.getStatusId() );

				support.getAreqHistDao().insert( histEntity , areqDto);
			}
		} catch (IOException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "IOError" );
		} catch (ScmException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "ScmError" );
		} catch (ScmSysException e) {
			throw new TriSystemException( AmMessageId.AM005061S , e , "ScmSysError" );
		} finally {

			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}
}
