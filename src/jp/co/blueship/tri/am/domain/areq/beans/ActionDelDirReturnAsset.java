package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の申請単位の格納フォルダを削除します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionDelDirReturnAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );

			if ( file.isDirectory() )
				TriFileUtils.delete( file );

		} catch ( IOException e ) {
			throw new TriSystemException( AmMessageId.AM005059S , e , "IOError" );
		} catch ( SecurityException e ) {
			throw new TriSystemException( AmMessageId.AM005059S , e , "SecurityError" );
		}

		return serviceDto;
	}
}
