package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 貸出申請された資産（依頼）ファイルの妥当性チェックを行います。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhActionValidateLendApplyCommissionFiles extends ActionPojoAbstract<IGeneralServiceBean> {

	private IExtensionValidator extensionValidate = null;
	public void setExtensionValidator( IExtensionValidator extensionValidate ) {
		this.extensionValidate = extensionValidate;
	}

	private IFilePathValidator filePathValidator = null;
	public void setFilePathValidator( IFilePathValidator filePathValidator ) {
		this.filePathValidator = filePathValidator;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		this.check( serviceDto );

		return serviceDto;
	}

	/**
	 * 妥当性チェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @throws BaseBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto ) throws BaseBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		IFileResult[] files = AmExtractMessageAddonUtils.extractLendApplyFileResults(paramList);

		List<String> checkFiles = new ArrayList<String>();
		for ( int i = 0; i < files.length; i++ ) {
			checkFiles.add( files[i].getFilePath() );
		}

		this.extensionValidate.validate( masterWorkPath, (String[])checkFiles.toArray( new String[] {} ));
		this.filePathValidator.validate( masterWorkPath, (String[])checkFiles.toArray( new String[] {} ));

	}
}
