package jp.co.blueship.tri.am.domain.areq.beans.dto;


/**
 *
 * プロセスステータスのID
 *
 * 値は適当です by Koyama
 *
 */

public class ProcessStatusID {

	/**
	 * ACTIVE
	 */
	public static final String PROCESS_ACTIVE = "ACTIVE";

	/**
	 * STOP
	 */
	public static final String PROCESS_STOP = "STOP";

	/**
	 * ERROR
	 */
	public static final String PROCESS_ERROR = "ERROR";

}
