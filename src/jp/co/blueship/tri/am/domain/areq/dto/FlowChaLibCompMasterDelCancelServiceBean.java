package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibCompMasterDelCancelServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 申請番号
	 */
	private String delApplyNo;

	/**
	 * @return applyNo
	 */
	public String getDelApplyNo() {
		return delApplyNo;
	}

	/**
	 * @param applyNo 設定する applyNo
	 */
	public void setDelApplyNo(String applyNo) {
		this.delApplyNo = applyNo;
	}

}
