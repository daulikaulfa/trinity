package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.EhLendApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendApplyFileResult;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IEhLendParamInfo;
import jp.co.blueship.tri.am.support.FlowChaLibLendEditSupport;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 指定された資産貸出の申請フォーマットファイルを読み出し、ファイルパス名に変換します。
 * <li>資産ファイルの場合、ディレクトリは、原本作業パスからの相対パスファイル名で配下を展開します。
 * <li>ファイルの親パス名がＳＣＭ固有のファイルの場合、変換対象から、完全に除外されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class EhGatherLendApplyFormatFile extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<IEhLendApplyFileResult> parseFiles = new ArrayList<IEhLendApplyFileResult>();

		List<Object> paramList = serviceDto.getParamList();

		try {
			File file = new File(	AmLibraryAddonUtils.getLendApplyDefTempFilePath( paramList ),
									sheet.getValue( AmDesignEntryKeyByChangec.lendApplyDefFile ));

			FileInputStream fis = null ;
			InputStreamReader isr = null ;
			String[] files = null ;
			try {
				fis = new FileInputStream( file ) ;
				isr = new InputStreamReader( fis) ;
				files = TriFileUtils.readFileLine( isr, false );
			} catch ( IOException e ) {
				throw e ;
			} finally {
				TriFileUtils.close(isr);
				TriFileUtils.close(fis);
			}


			// 貸出資産ファイル
			List<String> fileList = this.parseFiles( AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ), files );

			IEhLendParamInfo param = AmExtractMessageAddonUtils.getLendParamInfo( paramList );
			param.setLendFiles( fileList.toArray( new String[0] ));

			for ( Iterator<String> it = fileList.iterator(); it.hasNext(); ) {
				String path = it.next();

				IEhLendApplyFileResult result = new EhLendApplyFileResult();
				result.setFilePath( path );

				parseFiles.add( result );

			}

			paramList.add( this.sort( parseFiles.toArray( new IEhLendApplyFileResult[] {} )) );

		} catch (IOException e) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		} catch ( NullPointerException e ) {
			LogHandler.fatal( log , e ) ;
			throw new BusinessException( AmMessageId.AM001045E , e );
		}

		return serviceDto;
	}

	/**
	 * ディレクトリを、ルートパスからの相対パスファイル名で配下を展開します。
	 *
	 * @param root 対象となるディレクトリパス
	 * @param files 相対パス（ファイル／ディレクトリ混在）
	 * @return 展開したファイルリストを戻します。
	 * @throws IOException
	 */
	private final List<String> parseFiles(  File root, String[] files ) throws IOException {


		List<String> parseFiles	= new ArrayList<String>();
		Map<String,String> map	= new HashMap<String,String>();

		for ( String path : files ) {

			String result = FlowChaLibLendEditSupport.convertPath( StringUtils.replace( path, "\"", "" ));
			File file = new File( root, result );

			if ( ! file.exists() ) {
				parseFiles.add( path );
				continue;
			}

			if ( file.isFile() ) {

				if ( map.containsKey( result ))
					continue;

				if ( TriFileUtils.isSCM( file ) )
					continue;

				map.put( result, result );
				parseFiles.add( result );
			}

			if ( file.isDirectory() ) {
				result = TriStringUtils.trimTailSeparator( result );

				List<String> fileList = TriFileUtils.getFilePaths( file, TriFileUtils.TYPE_FILE ,  BusinessFileUtils.getFileFilter() ) ;

				Iterator<String> iter = fileList.iterator() ;
				while( iter.hasNext() ) {
					String parseFile = TriStringUtils.linkPathBySlash( result , iter.next() );

					if ( map.containsKey( parseFile ))
						continue;

					if ( TriFileUtils.isSCM( new File( root, parseFile )) )
						continue;

					map.put(parseFile, parseFile);

					parseFile = FlowChaLibLendEditSupport.convertPath( parseFile );
					parseFiles.add( parseFile );
				}
			}
		}

		return parseFiles;
	}

	/**
	 * ファイルパスのソートを行います。
	 * @param src ソートを行うファイル
	 * @return ソート結果を戻します。
	 */
	private final Object[] sort( IFileResult[] src ) {
		Arrays.sort( src,
					new Comparator<IFileResult>() {
						public int compare( IFileResult o1, IFileResult o2 ) {
							IFileResult src1 = (IFileResult)o1;
							IFileResult src2 = (IFileResult)o2;

							return src1.getFilePath().compareTo( src2.getFilePath() );
						}
					} );

		return src;
	}

}
