package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.IFilesDiffer;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * 返却資産申請の返却資産ファイルの妥当性チェックを行います。
 * <li>拡張子の妥当性チェック
 * <li>返却資産の貸出チェック
 *
 */
public class EhActionValidateReturnAssetFiles extends ActionPojoAbstract<IGeneralServiceBean> {

	private IFilesDiffer differ = null;

	/**
	 * 差分チェックがインスタンス生成時に自動的に設定されます。
	 * @param diff 差分チェック
	 */
	public final void setDiffer( IFilesDiffer diff ) {
		this.differ = diff;
	}

	private IExtensionValidator extensionValidator = null;
	public void setExtensionValidator( IExtensionValidator extensionValidator ) {
		this.extensionValidator = extensionValidator;
	}

	private IFilePathValidator filePathValidator = null;
	public void setFilePathValidator( IFilePathValidator filePathValidator ) {
		this.filePathValidator = filePathValidator;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		this.check( serviceDto );

		return serviceDto;
	}

	/**
	 * 拡張子チェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @throws BaseBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto ) throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		File returnFile = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );

		this.extensionValidator.validate( returnFile );
		this.filePathValidator.validate	( returnFile );


		File masterWorkFile = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

		IFileDiffResult[] results = this.differ.execute( returnFile, masterWorkFile );

		List<IFileDiffResult> returnAssetExistFrontWork = new ArrayList<IFileDiffResult>();
		if ( null != results ) {
			for ( int i = 0; i < results.length; i++ ) {
				if ( results[i].isMasterExists() ) {
					returnAssetExistFrontWork.add( results[i] );
				}
			}
		}


		File lendFile = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();
		for ( Iterator<IFileDiffResult> it = returnAssetExistFrontWork.iterator(); it.hasNext(); ) {

			String filePath = it.next().getFilePath();
			File checkFile = new File( lendFile, filePath );

			if ( !checkFile.exists() ) {
				messageList.add( AmMessageId.AM001053E );
				messageArgsList.add( new String[] {filePath}  );
			}
		}

		if ( 0 != messageList.size() )
			throw new ContinuableBusinessException( messageList, messageArgsList );
	}
}
