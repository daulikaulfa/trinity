package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;

import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;

/**
 * 資産ファイルの拡張子の妥当性を行うインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IExtensionValidator {

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * @param file 対象となるディレクトリパス
	 * @throws ContinuableBusinessException
	 */
	public void validate( File file ) throws ContinuableBusinessException;

	/**
	 * 指定されたパスを元に、拡張子の妥当性を検証します。
	 * @param file 親フォルダパス
	 * @param files 対象となる相対ファイルパス
	 * @throws ContinuableBusinessException
	 */
	public void validate( File file, String[] files ) throws ContinuableBusinessException;
}
