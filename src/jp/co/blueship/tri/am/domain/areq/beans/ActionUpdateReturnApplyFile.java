package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AppendFileConstID;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;



/**
 * 返却申請情報のファイル情報を更新します。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 *
 */
public class ActionUpdateReturnApplyFile extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		List<IAreqFileEntity> areqFileEntities = new ArrayList<IAreqFileEntity>();
		IAreqDto dto = this.setAssetFileEntity( paramList, areqFileEntities );
		dto.setAreqFileEntities(areqFileEntities);
		IAreqEntity areqEntity = dto.getAreqEntity();

		this.setAssetApplyEntityRtnEntryServiceBean( param, dto );
		support.getAreqDao().update( areqEntity );
		{
			AreqFileCondition condition = new AreqFileCondition();
			condition.setAreqId( areqEntity.getAreqId() );
			condition.setAreqCtgCd( areqEntity.getAreqCtgCd() );

			support.getAreqFileDao().delete( condition.getCondition() );
			support.getAreqFileDao().insert( areqFileEntities );
		}
		{
			AreqAttachedFileCondition condition = new AreqAttachedFileCondition();
			condition.setAreqId( areqEntity.getAreqId() );

			support.getAreqAttachedFileDao().delete( condition.getCondition() );
			support.getAreqAttachedFileDao().insert( dto.getAreqAttachedFileEntities() );
		}

		if ( DesignSheetUtils.isRecord() ) {

			IAreqDto areqDto = support.findAreqDto(dto.getAreqEntity().getAreqId());

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Request.getStatusId() );

			support.getAreqHistDao().insert( histEntity , areqDto);
		}
		return serviceDto;
	}

	private final void setAssetApplyEntityRtnEntryServiceBean(
			IReturnProcessParam param,
			IAreqDto dto ) {

		IAreqEntity areqEntity = dto.getAreqEntity();
		Timestamp systemDate = TriDateUtils.getSystemTimestamp();

		areqEntity.setAreqCtgCd			( AreqCtgCd.ReturningRequest.value() );
		areqEntity.setStsId				( AmAreqStatusId.CheckinRequested.getStatusId() );
		areqEntity.setRtnReqTimestamp	( systemDate );
		areqEntity.setRtnReqUserNm		( param.getUser() );
		areqEntity.setRtnReqUserId		( param.getUserId() );

		if ( ! TriStringUtils.isEmpty(param.getReturnAssetAppendFile()) ) {
			IAreqAttachedFileEntity attachedFile = new AreqAttachedFileEntity();
			attachedFile.setAreqId( areqEntity.getAreqId() );
			attachedFile.setAttachedFileSeqNo(
					support.nextvalByAttachedFileSeqNo(
							Integer.parseInt(AppendFileConstID.File.append1.value())) );
			attachedFile.setFilePath( param.getReturnAssetAppendFile() );
			dto.setAreqAttachedFileEntities(FluentList.from(new IAreqAttachedFileEntity[]{attachedFile}).asList());
		}
	}

	/**
	 * 返却申請レコードに、申請ファイル情報を設定します。
	 *
	 * @param paramList
	 */
	public final IAreqDto setAssetFileEntity( List<Object> paramList, List<IAreqFileEntity> areqFileEntities ) {

		IAreqDto dto = AmExtractEntityAddonUtils.extractReturnApply( paramList );
		if ( null == dto ) {
			throw new TriSystemException( AmMessageId.AM005003S) ;
		}

		IAreqEntity areqEntity = dto.getAreqEntity();
		File assetPath = AmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList );

		IFileDiffResult[] results = AmExtractMessageAddonUtils.getFileDiffResult( paramList );

		int seqNo = 0;
		for ( IFileDiffResult path : results ) {
			File file = new File( assetPath, path.getFilePath() );
			if ( !file.isFile() ){
				throw new TriSystemException( AmMessageId.AM005005S , path.getFilePath() ) ;
			}
			IAreqFileEntity fileEntity = new AreqFileEntity();

			fileEntity.setAreqId( areqEntity.getAreqId() );
			fileEntity.setAreqCtgCd( areqEntity.getAreqCtgCd() );
			fileEntity.setAreqFileSeqNo( support.getAreqFileNumberingDao().nextval( ++seqNo ) );
			fileEntity.setFilePath( TriStringUtils.convertPath( path.getFilePath() ) );
			fileEntity.setFileByteSize( new Long(file.length()).intValue() );
			fileEntity.setFileUpdTimestamp( new Timestamp( file.lastModified() ) );
			fileEntity.setFileStsId( path.isMasterExists() ? FileStatus.Edit.getStatusId() : FileStatus.Add.getStatusId() );

			areqFileEntities.add( fileEntity );
		}

		dto.getAreqFileEntities().addAll( areqFileEntities );

		return dto;
	}

}
