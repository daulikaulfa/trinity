package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

public class ChaLibMasterDelEntryViewServiceBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * 削除申請ユーザ
	 */
	private String delApplyUser;
	/**
	 * 削除申請ユーザＩＤ
	 */
	private String delApplyUserId;
	/**
	 * 申請日時
	 */
	private String applyDate;
	/**
	 * 申請件名
	 */
	private String delApplySubject;
	/**
	 * 削除理由
	 */
	private String delReason;
	/**
	 * 申請ファイルパス
	 */
	private String[] applyFilePath;
	/**
	 * バイナリ申請ファイルパス
	 */
	private String[] applyBinaryFilePath;
	/**
	 * 変更管理番号
	 */
	private String prjNo;
	/**
	 * 変更要因番号
	 */
	private String chgFactorNo;
	/**
	 * 申請ユーザーグループ
	 */
	private String userGroup;
	/**
	 * ロット番号
	 */
	private String lotId;


	public String[] getApplyFilePath() {
		return applyFilePath;
	}

	public void setApplyFilePath( String[] applyFilePath ) {
		this.applyFilePath = applyFilePath;
	}

	public String[] getApplyBinaryFilePath() {
		return applyBinaryFilePath;
	}

	public void setApplyBinaryFilePath( String[] applyBinaryFilePath ) {
		this.applyBinaryFilePath = applyBinaryFilePath;
	}

	public String getDelApplyUser() {
		return delApplyUser;
	}

	public void setDelApplyUser(String delApplyUser) {
		this.delApplyUser = delApplyUser;
	}

	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getDelReason() {
		return delReason;
	}

	public void setDelReason(String delReason) {
		this.delReason = delReason;
	}

	public String getPrjNo() {
		return prjNo;
	}

	public void setPrjNo(String prjNo) {
		this.prjNo = prjNo;
	}

	public String getUserGroup() {
		return userGroup;
	}

	public void setUserGroup(String userGroup) {
		this.userGroup = userGroup;
	}

	public String getDelApplySubject() {
		return delApplySubject;
	}

	public void setDelApplySubject(String delApplySubject) {
		this.delApplySubject = delApplySubject;
	}

	public String getChgFactorNo() {
		return chgFactorNo;
	}

	public void setChgFactorNo(String chgFactorNo) {
		this.chgFactorNo = chgFactorNo;
	}

	public String getDelApplyUserId() {
		return delApplyUserId;
	}

	public void setDelApplyUserId(String delApplyUserId) {
		this.delApplyUserId = delApplyUserId;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
}
