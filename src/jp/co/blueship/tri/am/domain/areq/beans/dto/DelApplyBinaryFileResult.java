package jp.co.blueship.tri.am.domain.areq.beans.dto;

/**
 * アプリケーション層で、バイナリ削除資産ファイルパスファイルパスを格納するクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class DelApplyBinaryFileResult extends DelApplyFileResult implements IDelApplyBinaryFileResult {
	
	private static final long serialVersionUID = 1L;
	
	

}
