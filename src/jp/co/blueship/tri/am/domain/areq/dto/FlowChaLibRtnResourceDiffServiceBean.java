package jp.co.blueship.tri.am.domain.areq.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibRtnResourceDiffServiceBean  extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 *  遷移元画面ID
	 */
	private String referer;
	/**
	 *  遷移先画面ID
	 */
	private String forward;
	/**
	 * 遷移タイプ
	 */
	private String screenType;
	/**
	 *  申請番号
	 */
	private String applyNo;

	/**
	 *  チェック状況
	 */
	private List<IDiffElement> diffResultList = new ArrayList<IDiffElement>();
	/**
	 * エンコード文字セットのコード
	 */
	private String encoding = null ;

	/**
	 *  エラーメッセージ
	 */
	private String errMessage;
	/**
	 *  完了ステータス
	 */
	private String completeStatus;
	/**
	 * 選択された資産ファイル
	 */
	private String selectedResource ;

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	public String getSelectedResource() {
		return selectedResource;
	}

	public void setSelectedResource(String selectedResource) {
		this.selectedResource = selectedResource;
	}

	public String getCompleteStatus() {
		return completeStatus;
	}

	public void setCompleteStatus(String completeStatus) {
		this.completeStatus = completeStatus;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public List<IDiffElement> getDiffResultList() {
		return diffResultList;
	}

	public void setDiffResultList(List<IDiffElement> diffResultList) {
		this.diffResultList = diffResultList;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

}
