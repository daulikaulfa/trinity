package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmBusinessJudgUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.ExtractStatusAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 削除資産申請の登録時の妥当性チェックを行います。
 * <li>コンフリクトチェック（貸出／返却申請）
 *
 * @version V3L10.02
 * @author Yukihiro Eguchi
 *
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 */
public class ActionValidateDelApplyCommissionLinkAssetApplyDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		check( serviceDto );

		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto )
		throws ContinuableBusinessException {

		duplicationCheck( serviceDto );
	}

	/**
	 * 他の資産申請情報とコンフリクトチェックを行います。
	 * <li>今回申請するファイルと他の資産申請ファイルを順次比較します。
	 * <li>資産が重複する場合、エラーとします。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void duplicationCheck( IServiceDto<IGeneralServiceBean> serviceDto )
			throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		IAreqDto delDto = AmExtractEntityAddonUtils.extractDeleteApply( paramList );
		if ( null == delDto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList );

		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( paramList );

		AreqCondition condition = getAreqConditionByDuplicate( lotDto.getLotEntity() );
		ISqlSort sort = new SortBuilder();
		List<IAreqEntity> areqEntities = support.getAreqDao().find( condition.getCondition(), sort );

		try {
			for ( IAreqEntity entity : areqEntities ) {
				if ( AmBusinessJudgUtils.isDeleteApply( entity ) )
					continue;

				File lendAssetNoPath	= AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList, entity );
				File returnAssetNoPath	= AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList, entity );

				for ( int i = 0; i < files.length; i++ ) {

					File lendAssetFile		= new File( lendAssetNoPath, files[i].getFilePath() );
					File returnAssetFile	= new File( returnAssetNoPath, files[i].getFilePath() );
					if ( ! lendAssetFile.isFile() && ! returnAssetFile.isFile() )
						continue;

					messageList.add( AmMessageId.AM001049E );
					messageArgsList.add(new String[] {files[i].getFilePath(), entity.getPjtId() , entity.getAreqId()});
				}
			}
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 重複できない（貸出・返却・削除）申請情報の検索条件を取得します。
	 * @param lotEntity
	 * @return
	 */
	private final AreqCondition getAreqConditionByDuplicate( ILotEntity lotEntity ) {
		List<String> baseStatusList = new ArrayList<String>();

		baseStatusList.addAll( FluentList.from(ExtractStatusAddonUtils.getAssetApplyBaseStatusByLessPjtApprove()).asList() );

		baseStatusList.addAll( FluentList.from(ExtractStatusAddonUtils.getAssetApplyBaseStatusByPjtApprove()).asList() );

		AreqCondition condition = new AreqCondition();
		condition.setLotId( lotEntity.getLotId() );
		condition.setStsIds( baseStatusList.toArray(new String[0]) );

		return condition;

	}

}
