package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibLendModifyイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出編集完了画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendModifyServiceComp implements IDomain<FlowChaLibLendModifyServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowChaLibLendModifyServiceBean> execute(IServiceDto<FlowChaLibLendModifyServiceBean> serviceDto) {

		FlowChaLibLendModifyServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.COMP_LEND_MODIFY ) &&
					!forward.equals( ChaLibScreenID.COMP_LEND_MODIFY )) {
				return serviceDto;
			}

			if ( forward.equals(ChaLibScreenID.COMP_LEND_MODIFY) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

				}
			}

			if (referer.equals(ChaLibScreenID.COMP_LEND_MODIFY)) {
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

}
