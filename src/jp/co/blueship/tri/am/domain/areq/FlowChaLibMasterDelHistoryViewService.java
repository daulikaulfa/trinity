package jp.co.blueship.tri.am.domain.areq;

import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelHistoryViewServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibMasterDelHistoryViewServiceイベントのサービスClass <br>
 * <p>
 * 変更管理 原本削除履歴の確認を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009
 */
public class FlowChaLibMasterDelHistoryViewService implements IDomain<FlowChaLibMasterDelHistoryViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMasterDelEditSupport support = null;

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelHistoryViewServiceBean> execute(IServiceDto<FlowChaLibMasterDelHistoryViewServiceBean> serviceDto) {

		FlowChaLibMasterDelHistoryViewServiceBean paramBean = null;

		try {

			paramBean = serviceDto.getServiceBean();

			// 業務ロジックを記述
			String applyNo = paramBean.getDelApplyNo();

			IAreqEntity assetApplyEntity = this.support.findAreqEntity(applyNo,null);
			List<IAreqFileEntity> areqFileEntities = this.support.findAreqFileEntities(applyNo, assetApplyEntity.getDelStsId(), AreqCtgCd.RemovalRequest);
			List<IAreqBinaryFileEntity> areqBinaryFileEntities = this.support.findAreqBinaryFileEntities(applyNo, assetApplyEntity.getDelStsId(), AreqCtgCd.RemovalRequest);
			IPjtEntity pjtEntity = this.support.findPjtEntity(assetApplyEntity.getPjtId());

			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity(assetApplyEntity);
			areqDto.setAreqFileEntities(areqFileEntities);
			areqDto.setAreqBinaryFileEntities(areqBinaryFileEntities);

			// グループの存在チェック
			ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

			AmLibraryAddonUtils.checkAccessableGroup(lotDto,//
					this.support.getUmFinderSupport().getGrpDao(),//
					this.support.getUmFinderSupport().findGroupByUserId(paramBean.getUserId()));

			paramBean.setLotNo(pjtEntity.getLotId());
			paramBean.setDelApplyNo(applyNo);

			setChaLibMasterDelEntryView(paramBean, pjtEntity, areqDto);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, "FlowChaLibConfirmService");
		}

	}

	private final void setChaLibMasterDelEntryView(GenericServiceBean paramBean, IPjtEntity pjtEntity, IAreqDto areqDto) {

		FlowChaLibMasterDelHistoryViewServiceBean bean = (FlowChaLibMasterDelHistoryViewServiceBean) paramBean;

		bean.setViewServiceBean(AmCnvEntityToDtoUtils.convertChaLibMasterDelEntryViewBean(pjtEntity, areqDto));
	}

}
