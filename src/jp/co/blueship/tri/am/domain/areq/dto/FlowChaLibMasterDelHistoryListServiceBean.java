package jp.co.blueship.tri.am.domain.areq.dto;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopViewServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

public class FlowChaLibMasterDelHistoryListServiceBean extends FlowChaLibTopViewServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 選択ページ
	 */
	private int selectPageNo = 0;

	/**
	 * ページ制御
	 */
	private IPageNoInfo pageInfoView;
	
	/** 
	 * ロット番号 
	 */
	private String selectedLotNo = null;

	/** 
	 * ロット情報 
	 */
	private List<LotViewBean> lotViewBeanList = null;


	/**
	 * @return pageInfoView
	 */
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}

	/**
	 * @param pageInfoView 設定する pageInfoView
	 */
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	/**
	 * @return selectPageNo
	 */
	public int getSelectPageNo() {
		return selectPageNo;
	}

	/**
	 * @param selectPageNo 設定する selectPageNo
	 */
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	/**
	 * @return selectedLotNo
	 */
	public String getSelectedLotNo() {
		return selectedLotNo;
	}

	/**
	 * @param selectedLotNo 設定する selectedLotNo
	 */
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	/**
	 * @return lotViewBeanList
	 */
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}

	/**
	 * @param lotViewBeanList 設定する lotViewBeanList
	 */
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

}
