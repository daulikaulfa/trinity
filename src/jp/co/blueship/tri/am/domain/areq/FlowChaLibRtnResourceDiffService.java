package jp.co.blueship.tri.am.domain.areq;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnResourceDiffServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;



/**
 * FlowCompareReturnResourceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 返却申請Ｄｉｆｆ画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibRtnResourceDiffService implements IDomain<FlowChaLibRtnResourceDiffServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowChaLibRtnEditSupport support = null;

	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnResourceDiffServiceBean> execute( IServiceDto<FlowChaLibRtnResourceDiffServiceBean> serviceDto ) {

		FlowChaLibRtnResourceDiffServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.RTN_RESOURCE_DIFF ) &&
					!forward.equals( ChaLibScreenID.RTN_RESOURCE_DIFF )) {
				return serviceDto;
			}

			if ( !forward.equals(ChaLibScreenID.RTN_RESOURCE_DIFF) ) {
				return serviceDto;
			}
			// 業務ロジックを記述
			String applyNo = paramBean.getApplyNo();

			IAreqEntity assetApplyEntity = this.support.findAreqEntity( applyNo );
			IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() ) ;
			ILotDto lotDto = this.support.findLotDto(pjtEntity.getLotId() );

			paramBean.setApplyNo( applyNo );

			List<Object> paramList = new ArrayList<Object>() ;
			paramList.add( lotDto);//Util用

			//原本と公開返却フォルダ資産との比較チェックを行う。
			File masterWorkPath = AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ) ;
			File returnInfoPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList , assetApplyEntity ) ;

			String selectedResource = paramBean.getSelectedResource() ;

			/** diff処理**/
			File srcFile = new File( TriStringUtils.linkPathBySlash( masterWorkPath.getAbsolutePath() , selectedResource ) ) ;
			File dstFile = new File( TriStringUtils.linkPathBySlash( returnInfoPath.getAbsolutePath() , selectedResource ) ) ;

			EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer( srcFile , dstFile ) ;
			differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

			DiffResult diffResult = differ.diff() ;
			paramBean.setEncoding( ( null != diffResult.getCharsetSrc() ) ? diffResult.getCharsetSrc() : diffResult.getCharsetDst() ) ;
			paramBean.setDiffResultList( diffResult.getDiffElementList() ) ;

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面が表示されるclassはBaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

}
