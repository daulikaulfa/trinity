package jp.co.blueship.tri.am.domain.areq;

import java.util.List;

import jp.co.blueship.tri.am.beans.mail.dto.AssetApplyMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibMasterDelEditSupport;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibMasterDelEntryServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 原本削除申請時のメール送信処理を行う。
 * </p>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMasterDelEntryServiceMail implements IDomain<FlowChaLibMasterDelEntryServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowChaLibMasterDelEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	public void setSupport(FlowChaLibMasterDelEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibMasterDelEntryServiceBean> execute( IServiceDto<FlowChaLibMasterDelEntryServiceBean> serviceDto ) {

		FlowChaLibMasterDelEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
			// 業務ロジックを記述
			String forward = paramBean.getForward();

			if ( forward.equals( ChaLibScreenID.COMP_MASTER_DEL_ENTRY ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					AssetApplyMailServiceBean successMailBean = new AssetApplyMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					IAreqDto areqDto = this.support.findAreqDto(paramBean.getApplyNo(), (StatusFlg)null);
					IAreqEntity assetApplyEntity = areqDto.getAreqEntity();
					List<IAreqFileEntity> removalFileEntities = areqDto.getAreqFileEntities( AreqCtgCd.RemovalRequest );
					List<IAreqBinaryFileEntity> removalBinaryFileEntities = areqDto.getAreqBinaryFileEntities( AreqCtgCd.RemovalRequest );

					IPjtEntity pjtEntity = this.support.findPjtEntity( assetApplyEntity.getPjtId() );
					ILotEntity pjtLotEntity = this.support.findLotEntity( pjtEntity.getLotId() );

					IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

					successMailBean.setAssetApplyEntity( assetApplyEntity );
					successMailBean.setPjtEntity( pjtEntity );
					successMailBean.setLotEntity( pjtLotEntity );
					successMailBean
							.setDelApplyFileList(removalFileEntities);
					successMailBean
							.setDelApplyBinaryFileList(removalBinaryFileEntities);
					successMailBean.setBldSrvId(srvEntity.getBldSrvId());
					successMailBean.setBldSrvNm(srvEntity.getBldSrvNm());

					successMail.execute( mailServiceDto );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , paramBean.getFlowAction() ) ) ;
		}

		return serviceDto;

	}


}
