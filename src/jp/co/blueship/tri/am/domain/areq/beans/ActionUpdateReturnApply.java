package jp.co.blueship.tri.am.domain.areq.beans;

import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * 返却申請情報を更新します。
 * <br>ここでは、更新を行わず、Entityの値だけ変更します。
 * 資産の複写後に、一回だけ更新します。
 * ここで、Entityの値だけを変更するのは、以降のPOJO処理で、キャッシュされたEntityを返却申請として
 * 扱わせるためです。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 *
 */
public class ActionUpdateReturnApply extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		IAreqDto dto = AmExtractEntityAddonUtils.extractLendApply( paramList );
		if ( null == dto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		this.setAssetApplyEntityRtnEntryServiceBean( param, dto );

		return serviceDto;
	}

	private final void setAssetApplyEntityRtnEntryServiceBean( IReturnProcessParam param, IAreqDto dto ) {
		IAreqEntity areqEntity = dto.getAreqEntity();

		//	ステータスを更新
		Timestamp systemDate = TriDateUtils.getSystemTimestamp();

		areqEntity.setAreqCtgCd			( AreqCtgCd.ReturningRequest.value() );
		areqEntity.setStsId				( AmAreqStatusId.CheckinRequested.getStatusId() );
		areqEntity.setRtnReqTimestamp	( systemDate );
		areqEntity.setRtnReqUserNm		( param.getUser() );
		areqEntity.setRtnReqUserId		( param.getUserId() );
	}

}
