package jp.co.blueship.tri.am.domain.areq.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

/**
 * アプリケーション層で、共通に使用する貸出申請情報を格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public interface IEhLendParamInfo extends ICommonParamInfo, Serializable {



	/**
	 * 申請情報番号を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getApplyNo();
	/**
	 * 申請情報番号を設定します。
	 * @param value 申請情報番号
	 */
	public void setApplyNo( String value );
	/**
	 * 申請ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLendApplyUser();
	/**
	 * 申請ユーザを設定します。
	 * @param value 申請ユーザ
	 */
	public void setLendApplyUser( String value );

	/**
	 * 申請ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLendApplyUserId();
	/**
	 * 申請ユーザＩＤを設定します。
	 * @param value 申請ユーザＩＤ
	 */
	public void setLendApplyUserId( String value );

	/**
	 * 申請グループを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getGroupName();
	/**
	 * @param value 申請グループ
	 */
	public void setGroupName( String value );

	/**
	 * 申請件名を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getSummary();
	/**
	 * 申請件名を設定します。
	 * @param value 申請件名
	 */
	public void setSummary( String value );

	/**
	 * 申請日時を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLendApplyDate();
	/**
	 * 申請日時を設定します。
	 * @param value 申請日時
	 */
	public void setLendApplyDate( String value );

	/**
	 * 申請内容を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getContent();
	/**
	 * 申請内容を設定します。
	 * @param value 申請内容
	 */
	public void setContent( String value );

	/**
	 * 案件番号を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getPjtNo();
	/**
	 * 案件番号を設定します。
	 * @param value 案件番号
	 */
	public void setPjtNo( String value );

	/**
	 * 貸出申請ファイルパスを取得します。
	 * @return 取得した値を戻します。
	 */
	public String[] getLendFiles();
	/**
	 * 貸出申請ファイルパスをを設定します。
	 * @param value 貸出申請ファイルパス
	 */
	public void setLendFiles( String[] value );

	/**
	 * 二重貸出チェックフラグを取得します。
	 * @return 取得した値を戻します。
	 */
	public boolean isDuplicationCheck();

	/**
	 * 二重貸出チェックフラグを設定します。
	 * @param value 二重貸出チェックフラグ
	 */
	public void setDuplicationCheck( boolean value );

	/**
	 * 申請ファイルパスを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getInApplyFilePath();
	/**
	 * 申請ファイルパスをを設定します。
	 * @param value 申請ファイルパス
	 */
	public void setInApplyFilePath( String value );

	public String getAssigneeId();

	public void setAssigneeId(String assigneeId);

	public String getCtgId();

	public void setCtgId(String ctgId);

	public String getMstoneId();

	public void setMstoneId(String mstoneId);

	public String getGroupId();

	public void setGroupId(String groupId);
	
	public boolean isDraff();
	
	public void setDraff(boolean isDraff);
}
