package jp.co.blueship.tri.am.domain.areq.beans.dto;

/**
 * アプリケーション層で、削除資産ファイルパスファイルパスを格納するクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class DelApplyFileResult implements IDelApplyFileResult {
	
	private static final long serialVersionUID = 1L;
	
	
	
	private String filePath = null;
	
	public final String getFilePath() {
		return filePath;
	}
	public final void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
