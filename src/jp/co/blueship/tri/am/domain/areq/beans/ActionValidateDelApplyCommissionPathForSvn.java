package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.IVcsReposDao;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.dao.passmgt.IPassMgtDao;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * 削除申請された資産（依頼）ファイルの妥当性チェックを行います。
 * <br>指定されたフォルダ／ファイルの存在チェック及び展開を行います。
 * javaのFileクラスでは、大文字/小文字までの厳密な判別ができないため、こちらで判断を行う。
 * SubversionのStatusを
 *
 */
public class ActionValidateDelApplyCommissionPathForSvn extends ActionPojoAbstract<IGeneralServiceBean> {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	IVcsReposDao ucfRepositoryDao = null;
	public void setVcsReposDao(IVcsReposDao ucfRepositoryDao) {
		this.ucfRepositoryDao = ucfRepositoryDao;
	}

	IPassMgtDao passwordDao = null;
	public void setPassMgtDao(IPassMgtDao passwordDao) {
		this.passwordDao = passwordDao;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		ILotDto lotDto = AmExtractEntityAddonUtils.extractPjtLot( paramList ) ;
		ILotEntity pjtLotEntity = lotDto.getLotEntity();
		if( ! VcsCategory.SVN.equals( VcsCategory.value( pjtLotEntity.getVcsCtgCd() ) ) ) {
			return serviceDto;
		}

		this.check( paramList );

		return serviceDto;
	}

	/**
	 * 妥当性チェックを行います。
	 * @param list コンバート間に流通させるビーン
	 * @throws BaseBusinessException
	 */

	private final void check( List<Object> list ) throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {
			IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( list );
			IFileResult[] binaryFiles = AmExtractMessageAddonUtils.extractDelApplyBinaryFileResults( list );

			if( TriStringUtils.isEmpty( files ) &&
				TriStringUtils.isEmpty( binaryFiles ) ) {

				messageList.add( AmMessageId.AM001079E );
				messageArgsList.add( new String[] { } );
				return ;
			}

			/** 原本資産のチェック **/
			this.checkAssets( list , messageList , messageArgsList ) ;

			/** バイナリ資産のチェック **/
			this.checkBinaryAssets( list , messageList , messageArgsList ) ;

		} catch( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005010S , e ) ;
		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}
	/**
	 * 原本資産のチェックを行う。
	 * @param list
	 * @param messageList
	 * @param messageArgsList
	 * @throws Exception
	 */

	private void checkAssets( List<Object> list , List<IMessageId> messageList , List<String[]> messageArgsList ) throws Exception {

		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( list );
		File masterworkPathFile = AmDesignBusinessRuleUtils.getMasterWorkPath( list );

		if( TriStringUtils.isEmpty( files ) ) {
			return ;
		}

		boolean checkStrictlyFilePath = StatusFlg.on.value().equals(
				sheet.getValue( AmDesignEntryKeyByChangec.checkStrictlyFilePath ) );

		for( IFileResult fileResult : files ) {
			File delRequestPath = new File( fileResult.getFilePath() ) ;
			String delAbsolutePath = TriStringUtils.linkPathBySlash( masterworkPathFile.getPath() , delRequestPath.getPath() ) ;

			File file = new File( delAbsolutePath );
			//ファイルの実在チェック（大文字・小文字は判別できない）
			if ( ! file.isFile() && ! file.isDirectory() ) {
				messageList.add( AmMessageId.AM001042E );
				messageArgsList.add( new String[] { delAbsolutePath , "" } );
				continue ;
			}

			//Svnへのstatus問い合わせ（大文字・小文字も判別）
			if ( checkStrictlyFilePath ) {
				AmLibraryAddonUtils.checkCanonicalPath( masterworkPathFile , file.getPath() , messageList , messageArgsList ) ;
			}
		}
	}
	/**
	 * バイナリ資産のチェックを行う。
	 * @param list
	 * @param messageList
	 * @param messageArgsList
	 * @throws Exception
	 */

	private void checkBinaryAssets( List<Object> list , List<IMessageId> messageList , List<String[]> messageArgsList ) throws Exception {

		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyBinaryFileResults( list );
		File masterworkPathFile = AmDesignBusinessRuleUtils.getMasterWorkPath( list );

		for( IFileResult fileResult : files ) {
			File delRequestPath = new File( fileResult.getFilePath() ) ;
			String delAbsolutePath = TriStringUtils.linkPathBySlash( masterworkPathFile.getPath() , delRequestPath.getPath() ) ;

			File file = new File( delAbsolutePath );
			//ファイルの原本への実在チェック（大文字・小文字は判別できない）
			if ( file.isFile() || file.isDirectory() ) {
				messageList.add( AmMessageId.AM001063E );
				messageArgsList.add( new String[] { delAbsolutePath , "" } );
				continue ;
			}
		}
	}

}
