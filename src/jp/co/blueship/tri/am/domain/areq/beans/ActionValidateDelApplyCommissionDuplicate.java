package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 削除資産申請の登録時の妥当性チェックを行います。
 * <li>コンフリクトチェック
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionValidateDelApplyCommissionDuplicate extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		check( serviceDto );
		return serviceDto;
	}

	/**
	 * コンフリクトチェックを行います。
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void check( IServiceDto<IGeneralServiceBean> serviceDto )
		throws ContinuableBusinessException {

		duplicationCheck( serviceDto );

	}

	/**
	 * 他の削除資産申請情報とコンフリクトチェックを行います。
	 * <li>今回申請するファイルと他の削除資産申請ファイルを順次比較します。
	 * <li>資産が重複する場合、エラーとします。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @throws ContinuableBusinessException
	 */

	private final void duplicationCheck( IServiceDto<IGeneralServiceBean> serviceDto )
			throws ContinuableBusinessException {

		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		IAreqDto areqDto = AmExtractEntityAddonUtils.extractDeleteApply( paramList );
		if ( null == areqDto ) {
			throw new TriSystemException( AmMessageId.AM005003S );
		}

		IAreqEntity areqEntity = areqDto.getAreqEntity();
		IPjtEntity pjtEntity = AmExtractEntityAddonUtils.extractPjt( paramList );

		Set<String> fileSet			= this.getFileSet( paramList );
		Set<String> binaryFileSet	= this.getBinaryFileSet( paramList );

		AreqCondition condition =
			AmDBSearchConditionAddonUtils.getAssetDelApplyConditionByDuplicate( pjtEntity.getLotId() );
		ISqlSort sort = new SortBuilder();

		List<IAreqEntity> areqEntities =
				support.getAreqDao().find( condition.getCondition(), sort );

		try {
			for ( IAreqEntity entity : areqEntities ) {

				if ( entity.getAreqId().equals( areqEntity.getAreqId() ))
					continue;

				List<IAreqFileEntity> removalFileEntities = this.support.findAreqFileEntities(entity.getAreqId(), AreqCtgCd.RemovalRequest);
				List<IAreqBinaryFileEntity> removalBinaryFileEntities = this.support.findAreqBinaryFileEntities(entity.getAreqId(), AreqCtgCd.RemovalRequest);

				for ( IAreqFileEntity filePath : removalFileEntities) {

					if ( fileSet.contains( filePath.getFilePath() ) ) {
						messageList.add( AmMessageId.AM001044E );
						messageArgsList.add( new String[] { filePath.getFilePath() , entity.getPjtId() , entity.getAreqId() });
					}
				}

				for ( IAreqBinaryFileEntity binaryFilePath : removalBinaryFileEntities ) {

					if ( binaryFileSet.contains( binaryFilePath.getFilePath() ) ) {
						messageList.add( AmMessageId.AM001060E );
						messageArgsList.add( new String[] { binaryFilePath.getFilePath() , entity.getPjtId() , entity.getAreqId() });
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 削除資産申請ファイルパステーブルを取得し、パス名をセットに格納します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得したセットを戻します。
	 */
	private final Set<String> getFileSet( List<Object> list ) {

		Set<String> fileSet = new HashSet<String>();

		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyFileResults( list );

		for ( IFileResult file : files ) {
			fileSet.add( file.getFilePath() );
		}

		return fileSet;
	}

	/**
	 * 削除資産申請ファイルパス（バイナリ）テーブルを取得し、パス名をセットに格納します。
	 * @param list コンバート間に流通させるビーン
	 * @return 取得したセットを戻します。
	 */
	private final Set<String> getBinaryFileSet( List<Object> list ) {

		Set<String> fileSet = new HashSet<String>();

		IFileResult[] files = AmExtractMessageAddonUtils.extractDelApplyBinaryFileResults( list );

		for ( IFileResult file : files ) {
			fileSet.add( file.getFilePath() );
		}

		return fileSet;
	}
}
