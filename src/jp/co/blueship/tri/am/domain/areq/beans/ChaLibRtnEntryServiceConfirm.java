package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmCnvEntityToDtoUtils;
import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibRtnEditSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;

/**
 * FlowChaLibRtnEntryServiceイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 返却申請確認画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ChaLibRtnEntryServiceConfirm implements IDomain<FlowChaLibRtnEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibRtnEditSupport support = null;
	public void setSupport(FlowChaLibRtnEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibRtnEntryServiceBean> execute(IServiceDto<FlowChaLibRtnEntryServiceBean> serviceDto) {

		FlowChaLibRtnEntryServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			executeForward( paramBean, paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005003S , e , paramBean.getFlowAction() );
		}

	}

	/**
	 * 返却申請確認画面への遷移
	 *
	 * @param paramBean
	 * @param retBean
	 */
	private final void executeForward( FlowChaLibRtnEntryServiceBean paramBean, FlowChaLibRtnEntryServiceBean retBean ) {

		// 業務ロジックを記述
		IAreqDto areqDto = this.support.findAreqDto( paramBean.getApplyNo() );
		IPjtEntity pjtEntity = this.support.findPjtEntity( areqDto.getAreqEntity().getPjtId() ) ;
		ILotDto lotDto = this.support.findLotDto( pjtEntity.getLotId() );

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotDto );

		retBean.setChaLibEntryBaseInfoBean(
				AmCnvEntityToDtoUtils.convertChaLibEntityBaseInfoBeanByLendApply( pjtEntity , areqDto ) );
		retBean.setChaLibEntryAssetResourceInfoBean(
				AmCnvEntityToDtoUtils.convertChaLibAssetResourceInfoBeanByReturnInfo( lotDto, areqDto ) );

		this.support.setFileStatusByAssetResourceInfoBean(
				retBean.getChaLibEntryAssetResourceInfoBean(),
				AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList, areqDto.getAreqEntity() ),
				AmDesignBusinessRuleUtils.getMasterWorkPath( paramList ) );
		this.support.setDiffEnabledByUcfExtension( retBean.getChaLibEntryAssetResourceInfoBean() ) ;
	}

}
