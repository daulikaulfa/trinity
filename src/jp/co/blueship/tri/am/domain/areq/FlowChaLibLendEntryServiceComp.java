package jp.co.blueship.tri.am.domain.areq;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * FlowChaLibLendEntryイベントのサービスClass
 * <br>
 * <p>
 * 変更管理 貸出申請完了画面の処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowChaLibLendEntryServiceComp implements IDomain<FlowChaLibLendEntryServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowChaLibLendEntryServiceBean> execute(IServiceDto<FlowChaLibLendEntryServiceBean> serviceDto) {

		FlowChaLibLendEntryServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( ChaLibScreenID.COMP_LEND_ENTRY ) &&
					!forward.equals( ChaLibScreenID.COMP_LEND_ENTRY )) {
				return serviceDto;
			}

			if (forward.equals(ChaLibScreenID.COMP_LEND_ENTRY)) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

				}
			}

			if (referer.equals(ChaLibScreenID.COMP_LEND_ENTRY)) {
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005084S , e , paramBean.getFlowAction() );
		}

	}

}
