package jp.co.blueship.tri.am.domain.areq.beans;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmExtractEntityAddonUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却情報申請の申請単位の格納フォルダを作成します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionMkDirReturnInfo extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		try {
			File srcPath = AmDesignBusinessRuleUtils.getLendAssetApplyNoPath( paramList );
			File destPath = AmDesignBusinessRuleUtils.getReturnInfoApplyNoPath( paramList );

			destPath.mkdirs();

			List<String> files = TriFileUtils.getFilePaths(
							srcPath,
							TriFileUtils.TYPE_DIR,
							AmDesignBusinessRuleUtils.getNeglectFileFilter( AmExtractEntityAddonUtils.extractPjtLot( paramList ) ) );

			for ( Iterator<String> it = files.iterator(); it.hasNext(); ) {
				String filePath = (String)it.next();

				File targetFile = new File( destPath, filePath );

				if ( ! targetFile.isDirectory() )
					targetFile.mkdirs();
			}

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005122S , e );
		}

		return serviceDto;

	}

}
