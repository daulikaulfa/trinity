package jp.co.blueship.tri.am.domain.areq.dto;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowChaLibRtnHistoryDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * ロット番号
	 */
	private String lotId;
	/**
	 * 申請情報番号
	 */
	private String applyNo;
	/**
	 * ステータス
	 */
	private String statusView;
	/**
	 * 返却申請基本情報
	 */
	private ChaLibEntryBaseInfoBean baseInfoViewBean;
	/**
	 * 申請資産情報
	 */
	private ChaLibEntryAssetResourceInfoBean assetSelectViewBean;

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public ChaLibEntryAssetResourceInfoBean getAssetSelectViewBean() {
		return assetSelectViewBean;
	}

	public void setAssetSelectViewBean(
			ChaLibEntryAssetResourceInfoBean assetSelectViewBean) {
		this.assetSelectViewBean = assetSelectViewBean;
	}

	public ChaLibEntryBaseInfoBean getBaseInfoViewBean() {
		return baseInfoViewBean;
	}

	public void setBaseInfoViewBean(ChaLibEntryBaseInfoBean baseInfoViewBean) {
		this.baseInfoViewBean = baseInfoViewBean;
	}

	public String getStatusView() {
		return statusView;
	}

	public void setStatusView(String statusView) {
		this.statusView = statusView;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
}
