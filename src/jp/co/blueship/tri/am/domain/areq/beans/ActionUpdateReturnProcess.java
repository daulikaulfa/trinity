package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.procmgt.IProcDetailsDao;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsCondition;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請処理の現在の申請状況を記録します。
 * <li>処理ステータス：("0")
 * <li>完了ステータス：("1")
 * として、指定された処理ＩＤが実行履歴として更新されます。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionUpdateReturnProcess extends ActionPojoAbstract<IGeneralServiceBean> {

	private IProcDetailsDao procDao = null;
	private boolean isRecord = true;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param dao アクセスインタフェース
	 */
	public final void setProcDetailsDao( IProcDetailsDao dao ) {
		procDao = dao;
	}

	/**
	 * 申請状況を記録する場合、trueを設定します。falseを設定すると、記録を行いません。
	 * @param record 記録を行う場合true。
	 */
	public final void setRecord( boolean record ) {
		isRecord = record;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		if ( ! isRecord )
			return serviceDto;

		List<Object> paramList = serviceDto.getParamList();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		ProcDetailsCondition condition = new ProcDetailsCondition();
		condition.setProcId(paramBean.getProcId());
		condition.setProcCtgCd(param.getProcCtgCd());
		IProcDetailsEntity entity		= procDao.findByPrimaryKey(condition.getCondition());

		entity.setStsId				( StatusFlg.off.value() );
		entity.setCompStsId			( StatusFlg.on );
		entity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setMsgId				( param.getMessageId() );
		entity.setMsg				( param.getMessage() );
		entity.setUpdUserId			( param.getUserId() );
		entity.setUpdUserNm			( param.getUser() );

		this.procDao.update( entity );


		return serviceDto;
	}
}
