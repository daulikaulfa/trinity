package jp.co.blueship.tri.am.domain.areq.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

/**
 * 
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class DelApplyParamInfo extends CommonParamInfo implements IDelApplyParamInfo {
	
	private static final long serialVersionUID = 1L;
	
	

	private String applyNo = null;
	private String delApplyUser = null;
	private String delApplyUserId = null;
	private String pjtNo = null;
	private String inApplyFilePath = null;
	private String ctgId = null;
	private String mstoneId = null;
	
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getDelApplyUser() {
		return delApplyUser;
	}
	public void setDelApplyUser(String delApplyUser) {
		this.delApplyUser = delApplyUser;
	}
	public String getInApplyFilePath() {
		return inApplyFilePath;
	}
	public void setInApplyFilePath(String inApplyFilePath) {
		this.inApplyFilePath = inApplyFilePath;
	}
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	public String getDelApplyUserId() {
		return delApplyUserId;
	}
	public void setDelApplyUserId(String delApplyUserId) {
		this.delApplyUserId = delApplyUserId;
	}
	
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

}
