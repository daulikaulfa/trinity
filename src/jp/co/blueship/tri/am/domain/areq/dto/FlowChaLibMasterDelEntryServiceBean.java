package jp.co.blueship.tri.am.domain.areq.dto;

import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryInputServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

public class FlowChaLibMasterDelEntryServiceBean extends FlowChaLibMasterDelEntryViewServiceBean {

	private static final long serialVersionUID = 1L;

	


	/**
	 * 選択ページ
	 */
	private int selectPageNo = 0;

	/**
	 * ページ制御
	 */
	private IPageNoInfo pageInfoView;

	/**
	 * 入力情報
	 */
	private ChaLibMasterDelEntryInputServiceBean inputServiceBean;

	/**
	 * 変更管理番号リスト
	 */
	private List<PjtViewBean> selectPjtViewList;
	/**
	 * 申請ユーザーグループリスト
	 */
	private List<String> approvUserGroupList;

	/**
	 * ロット番号
	 */
	private String selectedLotNo = null;

	/**
	 * 申請情報番号
	 */
	private String applyNo;
	/**
	 * 申請日時
	 */
	private String applyDate;

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyDate() {
		return applyDate;
	}
	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public ChaLibMasterDelEntryInputServiceBean getInputServiceBean() {
		return inputServiceBean;
	}

	public void setInputServiceBean(
			ChaLibMasterDelEntryInputServiceBean inputServiceBean) {
		this.inputServiceBean = inputServiceBean;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}

	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}

	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public List<PjtViewBean> getSelectPjtViewList() {
		return selectPjtViewList;
	}

	public void setSelectPjtViewList(List<PjtViewBean> selectPjtViewList) {
		this.selectPjtViewList = selectPjtViewList;
	}

	public List<String> getApprovUserGroupList() {
		return approvUserGroupList;
	}

	public void setApprovUserGroupList(List<String> approvUserGroupList) {
		this.approvUserGroupList = approvUserGroupList;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}

	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

}
