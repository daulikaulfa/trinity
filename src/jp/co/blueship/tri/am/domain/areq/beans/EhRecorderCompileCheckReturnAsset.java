package jp.co.blueship.tri.am.domain.areq.beans;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.constants.ProcCtgCdByReturningRequest;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請の申請状況（コンパイル構文チェック）の実行の記録を行います。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class EhRecorderCompileCheckReturnAsset extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	IDomain<IGeneralServiceBean> beforeAction = null;
	public void setBeforeAction(IDomain<IGeneralServiceBean> beforeAction) {
		this.beforeAction = beforeAction;
	}
	IDomain<IGeneralServiceBean> action = null;
	public void setAction(IDomain<IGeneralServiceBean> action) {
		this.action = action;
	}
	IDomain<IGeneralServiceBean> afterAction = null;
	public void setAfterAction(IDomain<IGeneralServiceBean> afterAction) {
		this.afterAction = afterAction;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();
		IGeneralServiceBean serviceBean = serviceDto.getServiceBean();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		param.setProcCtgCd(ProcCtgCdByReturningRequest.COMPILE_SYNTAX_CHECK);

		param.setProcessDate( TriDateUtils.getSystemTimestamp() );

		this.beforeAction.execute( serviceDto );

		try {
			long msec = System.currentTimeMillis();

			this.action.execute(serviceDto);

			try {
				long nowmsec = System.currentTimeMillis() - msec;
				if ( (long)1000 > nowmsec )
					Thread.sleep( (long)1000 - nowmsec );

			} catch(InterruptedException e) {
				LogHandler.fatal( log , e ) ;
				throw new TriSystemException( AmMessageId.AM005134S , e );
			}

		} catch ( BaseBusinessException e ) {
			param.setMessageId( e.getMessage() );
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			param.setMessage( ca.getMessageManager().getMessage(serviceBean.getLanguage(), e.getMessageID(),e.getMessageArgs()) );

			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			param.setMessageId( AmMessageId.AM005135S.getMessageId() );
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			param.setMessage( ca.getMessageManager().getMessage( serviceBean.getLanguage(), AmMessageId.AM005135S ) );
			throw new TriSystemException( AmMessageId.AM005135S , e );
		} finally {

			this.afterAction.execute(serviceDto);
		}

		return serviceDto;

	}

}
