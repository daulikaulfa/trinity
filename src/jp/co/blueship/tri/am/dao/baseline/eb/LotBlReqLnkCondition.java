package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlReqLnkItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the lot baseline request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotBlReqLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_BL_REQ_LNK;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotBlReqLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LotBlReqLnkCondition(){
		super(attr);
	}

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId() {
	    return lotBlId;
	}

	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	    super.append( LotBlReqLnkItems.lotBlId, lotBlId );
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(LotBlReqLnkItems.areqId, areqId );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotBlReqLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}