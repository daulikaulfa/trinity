package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the head baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlinai
 *
 */
public enum HeadBlItems implements ITableItem {
	headBlId("head_bl_id"),
	lotId("lot_id"),
	headVerTag("head_ver_tag"),
	headBlTag("head_bl_tag"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	mergechkUserId("mergechk_user_id"),
	mergechkUserNm("mergechk_user_nm"),
	mergechkStTimestamp("mergechk_st_timestamp"),
	mergechkEndTimestamp("mergechk_end_timestamp"),
	mergechkCmt("mergechk_cmt"),
	mergeUserId("merge_user_id"),
	mergeUserNm("merge_user_nm"),
	mergeStTimestamp("merge_st_timestamp"),
	mergeEndTimestamp("merge_end_timestamp"),
	mergeCmt("merge_cmt"),
	mergeStsCd("merge_sts_cd"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private HeadBlItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
