package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the head lot baseline link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadLotBlLnkEntity extends IEntityFooter {
	/**
	 * head baseline IDを取得します。
	 * @return head baseline ID
	 */
	public String getHeadBlId();
	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId);
	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId();
	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId);
}
