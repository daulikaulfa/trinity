package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the head lot baseline request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum HeadLotBlReqLnkItems implements ITableItem {
	lotBlId("lot_bl_id"),
	lotId("lot_id"),
	verTag("ver_tag"),
	areqId("areq_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private HeadLotBlReqLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
