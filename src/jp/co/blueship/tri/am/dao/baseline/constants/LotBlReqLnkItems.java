package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lot baseline request link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum LotBlReqLnkItems implements ITableItem {
	lotBlId("lot_bl_id"),
	pjtId("pjt_id"),
	areqId("areq_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private LotBlReqLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
