package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the head baseline module link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadBlMdlLnkEntity extends IEntityFooter {
	/**
	 * head baseline IDを取得します。
	 * @return head baseline ID
	 */
	public String getHeadBlId();
	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId);
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm();
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm);
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag();
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag);
	/**
	 * is merge target module?を取得します。
	 * @return is merge target module?
	 */
	public StatusFlg isMergeTargetMdl();
	/**
	 * is merge target module?を設定します。
	 * @param isMergeTargetMdl is merge target module?
	 */
	public void setMergeTargetMdl(StatusFlg isMergeTargetMdl);
	/**
	 * merge check head revitionを取得します。
	 * @return merge check head revition
	 */
	public String getMergechkHeadRev();
	/**
	 * merge check head revitionを設定します。
	 * @param mergechkHeadRev merge check head revition
	 */
	public void setMergechkHeadRev(String mergechkHeadRev);
	/**
	 * merge head revitionを取得します。
	 * @return merge head revision
	 */
	public String getMergeHeadRev();
	/**
	 * merge head revitionを設定します。
	 * @param mergeHeadRev merge head revision
	 */
	public void setMergeHeadRev(String mergeHeadRev);
}
