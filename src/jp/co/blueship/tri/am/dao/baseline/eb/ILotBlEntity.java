package jp.co.blueship.tri.am.dao.baseline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlEntity extends IEntityFooter {

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId();
	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId);
	/**
	 * lot baseline nameを取得します。
	 * @return lot baseline name
	 */
	public String getLotBlNm();
	/**
	 * lot baseline nameを設定します。
	 * @param lotBlNm lot baseline name
	 */
	public void setLotBlNm(String lotBlNm);
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId();
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId);
	/**
	 * lot version tagを取得します。
	 * @return lot version tag
	 */
	public String getLotVerTag();
	/**
	 * lot version tagを設定します。
	 * @param lotVerTag lot version tag
	 */
	public void setLotVerTag(String lotVerTag);
	/**
	 * lot baseline tagを取得します。
	 * @return lot baseline tag
	 */
	public String getLotBlTag();
	/**
	 * lot baseline tagを設定します。
	 * @param lotBlTag lot baseline tag
	 */
	public void setLotBlTag(String lotBlTag);
	/**
	 * data catalog codeを取得します。
	 * @return data catalog code
	 */
	public String getDataCtgCd();
	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd);
	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId();
	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId);
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId();
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId);
	/**
	 * process sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getProcStsId();
	/**
	 * lot check-in user-IDを取得します。
	 * @return lot check-in user-ID
	 */
	public String getLotChkinUserId();
	/**
	 * lot check-in user-IDを設定します。
	 * @param lotChkinUserId lot check-in user-ID
	 */
	public void setLotChkinUserId(String lotChkinUserId);
	/**
	 * lot check-in user nameを取得します。
	 * @return lot check-in user name
	 */
	public String getLotChkinUserNm();
	/**
	 * lot check-in user nameを設定します。
	 * @param lotChkinUserNm lot check-in user name
	 */
	public void setLotChkinUserNm(String lotChkinUserNm);
	
	public void setLotChkinUserIconPath(String lotChkinUserIconPath);
	
	public String getLotChkinUserIconPath();
	/**
	 * lot check-in time stampを取得します。
	 * @return lot check-in time stamp
	 */
	public Timestamp getLotChkinTimestamp();
	/**
	 * lot check-in time stampを設定します。
	 * @param lotChkinTimestamp lot check-in time stamp
	 */
	public void setLotChkinTimestamp(Timestamp lotChkinTimestamp);
	/**
	 * lot check-in commentを取得します。
	 * @return lot check-in comment
	 */
	public String getLotChkinCmt();
	/**
	 * lot check-in commentを設定します。
	 * @param lotChkinCmt lot check-in comment
	 */
	public void setLotChkinCmt(String lotChkinCmt);

}
