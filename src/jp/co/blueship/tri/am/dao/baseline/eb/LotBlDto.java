package jp.co.blueship.tri.am.dao.baseline.eb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * lot baseline DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class LotBlDto implements ILotBlDto {

	/**
	 * ロット・ベースライン
	 */
	public ILotBlEntity lotBlEntity = null;

	/**
	 * ロット・ベースライン・モジュール
	 */
	public List<ILotBlMdlLnkEntity> lotBlMdlLnkEntities = new ArrayList<ILotBlMdlLnkEntity>();

	/**
	 * ロット・ベースライン・資産申請
	 */
	public List<ILotBlReqLnkEntity> lotBlReqLnkEntities = new ArrayList<ILotBlReqLnkEntity>();

	@Override
	public ILotBlMdlLnkEntity getMdlEntity( String mdlNm ) {
		for ( ILotBlMdlLnkEntity mdlEntity: lotBlMdlLnkEntities ) {
			if ( TriStringUtils.equals( mdlEntity.getMdlNm(), mdlNm ) ) {
				return mdlEntity;
			}
		}

		return null;
	}

	@Override
	public List<ILotBlMdlLnkEntity> getCheckInMdlEntities( boolean isCheckInMdl ) {
		List<ILotBlMdlLnkEntity> mdlEntities = new ArrayList<ILotBlMdlLnkEntity>();

		for ( ILotBlMdlLnkEntity mdlEntity: lotBlMdlLnkEntities ) {
			if ( mdlEntity.isChkinTargetMdl().parseBoolean().equals( isCheckInMdl ) ) {
				mdlEntities.add( mdlEntity );
			}
		}

		return mdlEntities;
	}

	@Override
	public ILotBlEntity getLotBlEntity() {
	    return lotBlEntity;
	}

	@Override
	public void setLotBlEntity(ILotBlEntity lotBlEntity) {
	    this.lotBlEntity = lotBlEntity;
	}

	@Override
	public List<ILotBlMdlLnkEntity> getLotBlMdlLnkEntities() {
	    return lotBlMdlLnkEntities;
	}

	@Override
	public void setLotBlMdlLnkEntities(List<ILotBlMdlLnkEntity> lotBlMdlLnkEntities) {
	    this.lotBlMdlLnkEntities = lotBlMdlLnkEntities;
	}

	@Override
	public List<ILotBlReqLnkEntity> getLotBlReqLnkEntities() {
	    return lotBlReqLnkEntities;
	}

	@Override
	public void setLotBlReqLnkEntities(List<ILotBlReqLnkEntity> lotBlReqLnkEntities) {
	    this.lotBlReqLnkEntities = lotBlReqLnkEntities;
	}

}
