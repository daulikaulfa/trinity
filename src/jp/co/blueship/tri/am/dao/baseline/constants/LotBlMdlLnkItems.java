package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lot baseline module link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum LotBlMdlLnkItems implements ITableItem {
	lotBlId("lot_bl_id"),
	mdlNm("mdl_nm"),
	mdlVerTag("mdl_ver_tag"),
	isChkinTargetMdl("is_chkin_target_mdl"),
	lotChkinRev("lot_chkin_rev"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private LotBlMdlLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
