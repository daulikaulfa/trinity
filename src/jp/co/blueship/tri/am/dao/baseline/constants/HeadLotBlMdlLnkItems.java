package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the head lot baseline module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum HeadLotBlMdlLnkItems implements ITableItem {
	lotBlId("lot_bl_id"),
	lotId("lot_id"),
	verTag("ver_tag"),
	mdlNm("mdl_nm"),
	mdlVerTag("mdl_ver_tag"),
	mergechk_head_rev("mergechk_head_rev"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private HeadLotBlMdlLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
