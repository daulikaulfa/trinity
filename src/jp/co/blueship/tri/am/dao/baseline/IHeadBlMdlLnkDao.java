package jp.co.blueship.tri.am.dao.baseline;

import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the head baseline module link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadBlMdlLnkDao extends IJdbcDao<IHeadBlMdlLnkEntity> {

}
