package jp.co.blueship.tri.am.dao.baseline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the head baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public interface IHeadBlEntity extends IEntityFooter {

	/**
	 * head baseline IDを取得します。
	 * @return head baseline ID
	 */
	public String getHeadBlId();
	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId);
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId();
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId);
	/**
	 * head version tagを取得します。
	 * @return head version tag
	 */
	public String getHeadVerTag();
	/**
	 * head version tagを設定します。
	 * @param headVerTag head version tag
	 */
	public void setHeadVerTag(String headVerTag);
	/**
	 * head baseline tagを取得します。
	 * @return head baseline tag
	 */
	public String getHeadBlTag();
	/**
	 * head baseline tagを設定します。
	 * @param headBlTag head baseline tag
	 */
	public void setHeadBlTag(String headBlTag);
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId();
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId);
	/**
	 * process sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getProcStsId();
	/**
	 * merge check user-IDを取得します。
	 * @return merge check user-ID
	 */
	public String getMergechkUserId();
	/**
	 * merge check user-IDを設定します。
	 * @param mergechkUserId merge check user-ID
	 */
	public void setMergechkUserId(String mergechkUserId);
	/**
	 * merge check user nameを取得します。
	 * @return merge check user name
	 */
	public String getMergechkUserNm();
	/**
	 * merge check user nameを設定します。
	 * @param mergechkUserNm merge check user name
	 */
	public void setMergechkUserNm(String mergechkUserNm);
	/**
	 * merge check start time stampを取得します。
	 * @return merge check start time stamp
	 */
	public Timestamp getMergechkStTimestamp();
	/**
	 * merge check start time stampを設定します。
	 * @param mergechkStTimestamp merge check start time stamp
	 */
	public void setMergechkStTimestamp(Timestamp mergechkStTimestamp);
	/**
	 * merge check end time stampを取得します。
	 * @return merge check end time stamp
	 */
	public Timestamp getMergechkEndTimestamp();
	/**
	 * merge check end time stampを設定します。
	 * @param mergechkEndTimestamp merge check end time stamp
	 */
	public void setMergechkEndTimestamp(Timestamp mergechkEndTimestamp);
	/**
	 * merge check commentを取得します。
	 * @return merge check comment
	 */
	public String getMergechkCmt();
	/**
	 * merge check commentを設定します。
	 * @param mergechkCmt merge check comment
	 */
	public void setMergechkCmt(String mergechkCmt);
	/**
	 * merge user-IDを取得します。
	 * @return merge user-ID
	 */
	public String getMergeUserId();
	/**
	 * merge user-IDを設定します。
	 * @param mergeUserId merge user-ID
	 */
	public void setMergeUserId(String mergeUserId);
	/**
	 * merge user nameを取得します。
	 * @return merge user name
	 */
	public String getMergeUserNm();
	/**
	 * merge user nameを設定します。
	 * @param mergeUserNm merge user name
	 */
	public void setMergeUserNm(String mergeUserNm);
	/**
	 * merge start time stampを取得します。
	 * @return merge start time stamp
	 */
	public Timestamp getMergeStTimestamp();
	/**
	 * merge start time stampを設定します。
	 * @param mergeStTimestamp merge start time stamp
	 */
	public void setMergeStTimestamp(Timestamp mergeStTimestamp);
	/**
	 * merge end time stampを取得します。
	 * @return merge end time stamp
	 */
	public Timestamp getMergeEndTimestamp();
	/**
	 * merge end time stampを設定します。
	 * @param mergeEndTimestamp merge end time stamp
	 */
	public void setMergeEndTimestamp(Timestamp mergeEndTimestamp);
	/**
	 * merge commentを取得します。
	 * @return merge comment
	 */
	public String getMergeCmt();
	/**
	 * merge commentを設定します。
	 * @param mergeCmt merge comment
	 */
	public void setMergeCmt(String mergeCmt);
	/**
	 * merge status codeを取得します。
	 * @return merge status code
	 */
	public String getMergeStsCd();
	/**
	 * merge status codeを設定します。
	 * @param mergeStsCd merge status code
	 */
	public void setMergeStsCd(String mergeStsCd);





}
