package jp.co.blueship.tri.am.dao.baseline;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot baseline module link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlMdlLnkDao extends IJdbcDao<ILotBlMdlLnkEntity> {

}
