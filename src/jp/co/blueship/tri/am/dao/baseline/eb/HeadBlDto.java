package jp.co.blueship.tri.am.dao.baseline.eb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * head baseline DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class HeadBlDto implements IHeadBlDto {

	/**
	 * HEAD・ベースライン
	 */
	public IHeadBlEntity headBlEntity = null;

	/**
	 * HEAD・ベースライン・モジュール
	 */
	public List<IHeadBlMdlLnkEntity> headBlMdlLnkEntities = new ArrayList<IHeadBlMdlLnkEntity>();

	/**
	 * HEAD・ロット・ベースライン
	 */
	public List<IHeadLotBlLnkEntity> headLotBlLnkEntities = new ArrayList<IHeadLotBlLnkEntity>();

	@Override
	public IHeadBlMdlLnkEntity getMdlEntity( String mdlNm ) {
		for ( IHeadBlMdlLnkEntity mdlEntity: headBlMdlLnkEntities ) {
			if ( TriStringUtils.equals( mdlEntity.getMdlNm(), mdlNm ) ) {
				return mdlEntity;
			}
		}

		return null;
	}

	@Override
	public List<IHeadBlMdlLnkEntity> getMergeMdlEntities( boolean isMergeMdl ) {
		List<IHeadBlMdlLnkEntity> mdlEntities = new ArrayList<IHeadBlMdlLnkEntity>();

		for ( IHeadBlMdlLnkEntity mdlEntity: headBlMdlLnkEntities ) {
			if ( mdlEntity.isMergeTargetMdl().parseBoolean().equals( isMergeMdl ) ) {
				mdlEntities.add( mdlEntity );
			}
		}

		return mdlEntities;
	}

	@Override
	public IHeadBlEntity getHeadBlEntity() {
	    return headBlEntity;
	}

	@Override
	public void setHeadBlEntity(IHeadBlEntity headBlEntity) {
	    this.headBlEntity = headBlEntity;
	}

	@Override
	public List<IHeadBlMdlLnkEntity> getHeadBlMdlLnkEntities() {
	    return headBlMdlLnkEntities;
	}

	@Override
	public void setHeadBlMdlLnkEntities(List<IHeadBlMdlLnkEntity> lotBlMdlLnkEntities) {
	    this.headBlMdlLnkEntities = lotBlMdlLnkEntities;
	}

	@Override
	public List<IHeadLotBlLnkEntity> getHeadLotBlLnkEntities() {
	    return headLotBlLnkEntities;
	}

	@Override
	public void setHeadLotBlLnkEntities(List<IHeadLotBlLnkEntity> headLotBlLnkEntities) {
	    this.headLotBlLnkEntities = headLotBlLnkEntities;
	}

}
