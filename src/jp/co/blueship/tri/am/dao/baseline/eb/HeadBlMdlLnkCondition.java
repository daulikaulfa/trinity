package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlMdlLnkItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the head baseline module linik entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HeadBlMdlLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_HEAD_BL_MDL_LNK;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * head baseline ID's
	 */
	public String[] headBlIds = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * is merge target module?
	 */
	public StatusFlg isMergeTargetMdl = StatusFlg.off;
	/**
	 * head revision at the merge check
	 */
	public String mergechkHeadRev = null;
	/**
	 * head revision at the merge
	 */
	public String mergeHeadRev = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(HeadBlMdlLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public HeadBlMdlLnkCondition(){
		super(attr);
	}

	/**
	 * head baseline IDを取得します。
	 * @return head baseline ID
	 */
	public String getHeadBlId() {
	    return headBlId;
	}

	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	    super.append( HeadBlMdlLnkItems.headBlId, headBlId );
	}

	/**
	 * head baseline ID'sを取得します。
	 * @return head baseline ID's
	 */
	public String[] getHeadBlIds() {
	    return headBlIds;
	}

	/**
	 * head baseline ID'sを設定します。
	 * @param headBlIds head baseline ID's
	 */
	public void setHeadBlIds(String[] headBlIds) {
	    this.headBlIds = headBlIds;
	    super.append( HeadBlMdlLnkItems.headBlId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(headBlIds, HeadBlMdlLnkItems.headBlId.getItemName(), false, true, false) );
	}
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	    super.append(HeadBlMdlLnkItems.mdlNm, mdlNm );
	}
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	    super.append(HeadBlMdlLnkItems.mdlVerTag, mdlVerTag );
	}

	/**
	 * merge target moduleかどうかを取得します。
	 * @return merge target moduleの場合true。それ以外はfalse。
	 */
	public StatusFlg isMergeTargetMdl() {
	    return isMergeTargetMdl;
	}

	/**
	 * merge target moduleかどうかを設定します。
	 * @param isMergeTargetMdl target moduleの場合true。それ以外はfalse。
	 */
	public void setMergeTargetMdl(StatusFlg isMergeTargetMdl) {
	    this.isMergeTargetMdl = isMergeTargetMdl;
	    super.append( HeadBlMdlLnkItems.isMergeTargetMdl, (null == isMergeTargetMdl)? StatusFlg.off.parseBoolean(): isMergeTargetMdl.parseBoolean() );
	}
	/**
	 * head check revision を取得します。
	 * @return head check revision
	 */
	public String getMergechkHeadRev() {
	    return mergechkHeadRev;
	}
	/**
	 * head check revisionを設定します。
	 * @param mergechkHeadRev head check revision
	 */
	public void setMergechkHeadRev(String mergechkHeadRev) {
	    this.mergechkHeadRev = mergechkHeadRev;
	    super.append(HeadBlMdlLnkItems.mergechkHeadRev, mergechkHeadRev );
	}
	/**
	 * head revision を取得します。
	 * @return module head revision
	 */
	public String getMergeHeadRev() {
	    return mergeHeadRev;
	}
	/**
	 * head revisionを設定します。
	 * @param mergeHeadRev head revision
	 */
	public void setMergeHeadRev(String mergeHeadRev) {
	    this.mergeHeadRev = mergeHeadRev;
	    super.append(HeadBlMdlLnkItems.mergeHeadRev, mergeHeadRev );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( HeadBlMdlLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}