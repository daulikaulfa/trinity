package jp.co.blueship.tri.am.dao.baseline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the head lot baseline entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotBlCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_BL;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * lot baseline ID's
	 */
	public String[] lotBlIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * data-ID's
	 */
	public String[] dataIds = null;
	/**
	 * lot version tag
	 */
	public String lotVerTag = null;
	/**
	 * lot baseline tag
	 */
	public String lotBlTag = null;
	/**
	 * lot check-in timestamp
	 */
	public Timestamp lotChkinTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotBlItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public LotBlCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( LotBlItems.dataId);
		super.setJoinUser( true );
		super.setKeyByJoinUsers(
				LotBlItems.lotChkinUserId );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( LotBlItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, LotBlItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( LotBlItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, LotBlItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId() {
	    return lotBlId;
	}

	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	    super.append( LotBlItems.lotBlId, lotBlId );
	}

	/**
	 * lot baseline ID'sを取得します。
	 * @return lot baseline ID's
	 */
	public String[] getLotBlIds() {
	    return lotBlIds;
	}

	/**
	 * lot baseline ID'sを設定します。
	 * @param lotBlIds lot baseline ID's
	 */
	public void setLotBlIds(String... lotBlIds) {
	    this.lotBlIds = lotBlIds;
	    super.append( LotBlItems.lotBlId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotBlIds, LotBlItems.lotBlId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append( LotBlItems.lotId, lotId );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String... lotIds) {
	    this.lotIds = lotIds;
	    super.append( LotBlItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, LotBlItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * data catalog codeを取得します。
	 * @return data catalog cod
	 */
	public String getDataCtgCd() {
	    return dataCtgCd;
	}

	/**
	 * data catalog codeを設定します。
	 * @param dataCtgCd data catalog code
	 */
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	    super.append(LotBlItems.dataCtgCd, dataCtgCd );
	}

	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}

	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	    super.append(LotBlItems.dataId, dataId );
	}

	/**
	 * data-ID'sを取得します。
	 * @return data-ID's
	 */
	public String[] getDataIds() {
	    return dataIds;
	}

	/**
	 * data-ID'sを設定します。
	 * @param dataId data-ID's
	 */
	public void setDataIds(String... dataIds) {
	    this.dataIds = dataIds;
	    super.append( LotBlItems.dataId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(dataIds, LotBlItems.dataId.getItemName(), false, true, false) );
	}

	/**
	 * lot version tagを取得します。
	 * @return lot version tag
	 */
	public String getLotVerTag() {
	    return lotVerTag;
	}

	/**
	 * lot version tagを設定します。
	 * @param verTag lot version tag
	 */
	public void setLotVerTag(String verTag) {
	    this.lotVerTag = verTag;
	    super.append( LotBlItems.lotVerTag, verTag );
	}

	/**
	 * lot baseline tagを取得します。
	 * @return lot baseline tag
	 */
	public String getLotBlTag() {
	    return lotBlTag;
	}

	/**
	 * lot baseline tagを設定します。
	 * @param blTag lot baseline tag
	 */
	public void setLotBlTag(String blTag) {
	    this.lotBlTag = blTag;
	    super.append( LotBlItems.lotBlTag, blTag );
	}

	/**
	 * lot check-in time stamp(from-to)を設定します。（大小比較）
	 * @param lotChkinTimestamp lot check-in time stam
	 */
	public void setLotChkinTimestamp(Timestamp lotChkinTimestampFrom, Timestamp lotChkinTimestampTo) {
	    super.append(LotBlItems.lotChkinTimestamp.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( lotChkinTimestampFrom, lotChkinTimestampTo, LotBlItems.lotChkinTimestamp.getItemName() ) );
	}

	/**
	 * lot check-in time stampを設定します。
	 * @param lotChkinTimestamp lot check-in time stam
	 */
	public void setLotChkinTimestamp(Timestamp lotChkinTimestampTo) {
	    this.lotChkinTimestamp = lotChkinTimestampTo;
	    super.append(LotBlItems.lotChkinTimestamp, lotChkinTimestampTo );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append( LotBlItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String... stsIds) {
	    this.stsIds = stsIds;
	    super.append( LotBlItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, LotBlItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotBlItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}