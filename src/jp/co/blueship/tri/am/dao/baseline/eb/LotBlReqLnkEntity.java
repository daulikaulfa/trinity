package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot baseline request link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class LotBlReqLnkEntity extends EntityFooter implements ILotBlReqLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * areq-ID
	 */
	public String areqId = null;

	@Override
	public String getLotBlId() {
	    return lotBlId;
	}
	@Override
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	}
	public String getPjtId() {
	    return pjtId;
	}
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	@Override
	public String getAreqId() {
	    return areqId;
	}
	@Override
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}

}
