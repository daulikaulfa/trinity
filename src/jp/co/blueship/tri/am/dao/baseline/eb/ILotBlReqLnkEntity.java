package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot baseline request link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlReqLnkEntity extends IEntityFooter {

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId();
	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId);

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId();

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId();
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId);
}
