package jp.co.blueship.tri.am.dao.baseline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class LotBlEntity extends EntityFooter implements ILotBlEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;

	/**
	 * lot baseline name
	 */
	public String lotBlNm = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot version tag
	 */
	public String lotVerTag = null;
	/**
	 * lot baseline tag
	 */
	public String lotBlTag = null;
	/**
	 * data catalog code
	 */
	public String dataCtgCd = null;
	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * lot check-in user-ID
	 */
	public String lotChkinUserId = null;
	/**
	 * lot check-in user name
	 */
	public String lotChkinUserNm = null;
	/**
	 * lot check-in time stamp
	 */
	public Timestamp lotChkinTimestamp = null;
	/**
	 * lot check-in comment
	 */
	public String lotChkinCmt = null;
	
	private String lotChkinUserIconPath = null;

	@Override
	public String getLotBlId() {
	    return lotBlId;
	}
	@Override
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	}
	@Override
	public String getLotBlNm() {
	    return lotBlNm;
	}
	@Override
	public void setLotBlNm(String lotBlNm) {
	    this.lotBlNm = lotBlNm;
	}
	@Override
	public String getLotId() {
	    return lotId;
	}
	@Override
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	@Override
	public String getLotVerTag() {
	    return lotVerTag;
	}
	@Override
	public void setLotVerTag(String lotVerTag) {
	    this.lotVerTag = lotVerTag;
	}
	@Override
	public String getLotBlTag() {
	    return lotBlTag;
	}
	@Override
	public void setLotBlTag(String lotBlTag) {
	    this.lotBlTag = lotBlTag;
	}
	@Override
	public String getDataCtgCd() {
	    return dataCtgCd;
	}
	@Override
	public void setDataCtgCd(String dataCtgCd) {
	    this.dataCtgCd = dataCtgCd;
	}
	@Override
	public String getDataId() {
	    return dataId;
	}
	@Override
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	}
	@Override
	public String getStsId() {
	    return stsId;
	}
	@Override
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	@Override
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	@Override
	public String getLotChkinUserId() {
	    return lotChkinUserId;
	}
	@Override
	public void setLotChkinUserId(String lotChkinUserId) {
	    this.lotChkinUserId = lotChkinUserId;
	}
	@Override
	public String getLotChkinUserNm() {
	    return lotChkinUserNm;
	}
	@Override
	public void setLotChkinUserNm(String lotChkinUserNm) {
	    this.lotChkinUserNm = lotChkinUserNm;
	}
	@Override
	public Timestamp getLotChkinTimestamp() {
	    return lotChkinTimestamp;
	}
	@Override
	public void setLotChkinTimestamp(Timestamp lotChkinTimestamp) {
	    this.lotChkinTimestamp = lotChkinTimestamp;
	}
	@Override
	public String getLotChkinCmt() {
	    return lotChkinCmt;
	}
	@Override
	public void setLotChkinCmt(String lotChkinCmt) {
	    this.lotChkinCmt = lotChkinCmt;
	}

	@Override
	public void setLotChkinUserIconPath(String lotChkinUserIconPath) {
		this.lotChkinUserIconPath = lotChkinUserIconPath;
	}
	
	@Override
	public String getLotChkinUserIconPath() {
		return this.lotChkinUserIconPath;
	}
}
