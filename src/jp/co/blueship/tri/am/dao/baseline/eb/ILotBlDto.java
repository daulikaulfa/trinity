package jp.co.blueship.tri.am.dao.baseline.eb;

import java.util.List;

/**
 * The interface of the lot baseline DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlDto {

	/**
	 * ロット・ベースラインを取得します。
	 *
	 * @return ロット・ベースラインEntity
	 */
	public ILotBlEntity getLotBlEntity();
	/**
	 * ロット・ベースラインを設定します。
	 *
	 * @param lotBlEntity ロット・ベースラインEntity
	 */
	public void setLotBlEntity(ILotBlEntity lotBlEntity);

	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param mdlNm 対象モジュール名
	 * @return 取得したModule Entity
	 */
	public ILotBlMdlLnkEntity getMdlEntity( String mdlNm );
	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param isCheckInMdl チェックイン対象のモジュールか？
	 * @return 取得したModule Entity
	 */
	public List<ILotBlMdlLnkEntity> getCheckInMdlEntities( boolean isCheckInMdl );

	/**
	 * ロット・ベースライン・モジュールEntityのListを取得します。
	 *
	 * @return ロット・ベースライン・モジュールEntityのList
	 */
	public List<ILotBlMdlLnkEntity> getLotBlMdlLnkEntities();

	/**
	 * ロット・ベースライン・モジュールEntityのListを設定します。
	 *
	 * @param lotBlMdlLnkEntities ロット・ベースライン・モジュールEntityのList
	 */
	public void setLotBlMdlLnkEntities(List<ILotBlMdlLnkEntity> lotBlMdlLnkEntities);
	/**
	 * ロット・ベースライン・資産申請のListを取得します。
	 * @return ロット・ベースライン・資産申請EntityのList
	 */
	public List<ILotBlReqLnkEntity> getLotBlReqLnkEntities();
	/**
	 * ロット・ベースライン・資産申請を設定します。
	 * @param pjtAvlAreqLnkEntities ロット・ベースライン・資産申請のList
	 */
	public void setLotBlReqLnkEntities(List<ILotBlReqLnkEntity> lotBlReqLnkEntities );


}
