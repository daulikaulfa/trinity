package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlMdlLnkItems;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot baseline module link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class LotBlMdlLnkDao extends JdbcBaseDao<ILotBlMdlLnkEntity> implements ILotBlMdlLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_BL_MDL_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotBlMdlLnkEntity entity ) {
		builder
			.append(LotBlMdlLnkItems.lotBlId, entity.getLotBlId(), true)
			.append(LotBlMdlLnkItems.mdlNm, entity.getMdlNm(), true)
			.append(LotBlMdlLnkItems.mdlVerTag, entity.getMdlVerTag())
			.append(LotBlMdlLnkItems.isChkinTargetMdl, (null == entity.isChkinTargetMdl())? null: entity.isChkinTargetMdl().parseBoolean())
			.append(LotBlMdlLnkItems.lotChkinRev, entity.getLotChkinRev())
			.append(LotBlMdlLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotBlMdlLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotBlMdlLnkItems.regUserId, entity.getRegUserId())
			.append(LotBlMdlLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotBlMdlLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotBlMdlLnkItems.updUserId, entity.getUpdUserId())
			.append(LotBlMdlLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotBlMdlLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		ILotBlMdlLnkEntity entity = new LotBlMdlLnkEntity();

  	  entity.setLotBlId( rs.getString(LotBlMdlLnkItems.lotBlId.getItemName()) );
  	  entity.setMdlNm( rs.getString(LotBlMdlLnkItems.mdlNm.getItemName()) );
  	  entity.setMdlVerTag( rs.getString(LotBlMdlLnkItems.mdlVerTag.getItemName()) );
 	  entity.setChkinTargetMdl( StatusFlg.value(rs.getBoolean(LotBlMdlLnkItems.isChkinTargetMdl.getItemName())) );
  	  entity.setLotChkinRev( rs.getString(LotBlMdlLnkItems.lotChkinRev.getItemName()) );
 	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotBlMdlLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LotBlMdlLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LotBlMdlLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LotBlMdlLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LotBlMdlLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LotBlMdlLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LotBlMdlLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
