package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * head baseline module link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class HeadBlMdlLnkEntity extends EntityFooter implements IHeadBlMdlLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * is merge target module?
	 */
	public StatusFlg isMergeTargetMdl = null;
	/**
	 * merge check head revision
	 */
	public String mergechkHeadRev = null;
	/**
	 * merge head revision
	 */
	public String mergeHeadRev = null;

	@Override
	public String getHeadBlId() {
	    return headBlId;
	}
	@Override
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	}
	@Override
	public String getMdlNm() {
	    return mdlNm;
	}
	@Override
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	@Override
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	@Override
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	}
	@Override
	public StatusFlg isMergeTargetMdl() {
	    return isMergeTargetMdl;
	}
	@Override
	public void setMergeTargetMdl(StatusFlg isMergeTargetMdl) {
	    this.isMergeTargetMdl = isMergeTargetMdl;
	}
	@Override
	public String getMergechkHeadRev() {
	    return mergechkHeadRev;
	}
	@Override
	public void setMergechkHeadRev(String mergechkHeadRev) {
	    this.mergechkHeadRev = mergechkHeadRev;
	}
	@Override
	public String getMergeHeadRev() {
	    return mergeHeadRev;
	}
	@Override
	public void setMergeHeadRev(String mergeHeadRev) {
	    this.mergeHeadRev = mergeHeadRev;
	}

}
