package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlMdlLnkItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the head baseline module link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class HeadBlMdlLnkDao extends JdbcBaseDao<IHeadBlMdlLnkEntity> implements IHeadBlMdlLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_HEAD_BL_MDL_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IHeadBlMdlLnkEntity entity ) {
		builder
			.append(HeadBlMdlLnkItems.headBlId, entity.getHeadBlId(), true)
			.append(HeadBlMdlLnkItems.mdlNm, entity.getMdlNm(), true)
			.append(HeadBlMdlLnkItems.mdlVerTag, entity.getMdlVerTag())
			.append(HeadBlMdlLnkItems.isMergeTargetMdl, (null == entity.isMergeTargetMdl())? null :entity.isMergeTargetMdl().parseBoolean())
			.append(HeadBlMdlLnkItems.mergechkHeadRev, entity.getMergechkHeadRev())
			.append(HeadBlMdlLnkItems.mergeHeadRev, entity.getMergeHeadRev())
			.append(HeadBlMdlLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(HeadBlMdlLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(HeadBlMdlLnkItems.regUserId, entity.getRegUserId())
			.append(HeadBlMdlLnkItems.regUserNm, entity.getRegUserNm())
			.append(HeadBlMdlLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(HeadBlMdlLnkItems.updUserId, entity.getUpdUserId())
			.append(HeadBlMdlLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IHeadBlMdlLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IHeadBlMdlLnkEntity entity = new HeadBlMdlLnkEntity();

  	  entity.setHeadBlId( rs.getString(HeadBlMdlLnkItems.headBlId.getItemName()) );
  	  entity.setMdlNm( rs.getString(HeadBlMdlLnkItems.mdlNm.getItemName()) );
  	  entity.setMdlVerTag( rs.getString(HeadBlMdlLnkItems.mdlVerTag.getItemName()) );
 	  entity.setMergeTargetMdl( StatusFlg.value(rs.getBoolean(HeadBlMdlLnkItems.isMergeTargetMdl.getItemName())) );
  	  entity.setMergechkHeadRev( rs.getString(HeadBlMdlLnkItems.mergechkHeadRev.getItemName()) );
  	  entity.setMergeHeadRev( rs.getString(HeadBlMdlLnkItems.mergeHeadRev.getItemName()) );
 	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(HeadBlMdlLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(HeadBlMdlLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(HeadBlMdlLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(HeadBlMdlLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(HeadBlMdlLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(HeadBlMdlLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(HeadBlMdlLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
