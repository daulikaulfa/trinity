package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * head lot baseline module link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class LotBlMdlLnkEntity extends EntityFooter implements ILotBlMdlLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * is check-in target module?
	 */
	public StatusFlg isChkinTargetMdl = null;
	/**
	 * lot check-in revision
	 */
	public String lotChkinRev = null;

	@Override
	public String getLotBlId() {
	    return lotBlId;
	}

	@Override
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	}
	@Override
	public String getMdlNm() {
	    return mdlNm;
	}
	@Override
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	@Override
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	@Override
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	}
	@Override
	public StatusFlg isChkinTargetMdl() {
	    return isChkinTargetMdl;
	}
	@Override
	public void setChkinTargetMdl(StatusFlg isChkinTargetMdl) {
	    this.isChkinTargetMdl = isChkinTargetMdl;
	}

	@Override
	public String getLotChkinRev() {
	    return lotChkinRev;
	}
	@Override
	public void setLotChkinRev(String lotChkinRev) {
	    this.lotChkinRev = lotChkinRev;
	}
}
