package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the head lot baseline module link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlMdlLnkEntity extends IEntityFooter {
	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId() ;
	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId);
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm();
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm);
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag();
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag);
	/**
	 * is check-in target module?を取得します。
	 * @return is check-in target module?
	 */
	public StatusFlg isChkinTargetMdl();
	/**
	 * is check-in target module?を設定します。
	 * @param isChkinTargetMdl is check-in target module?
	 */
	public void setChkinTargetMdl(StatusFlg isChkinTargetMdl);
	/**
	 * lot check-in revitionを取得します。
	 * @return lot check-in revition
	 */
	public String getLotChkinRev();
	/**
	 * lot check-in revitionを設定します。
	 * @param lotChkinRev lot check-in revition
	 */
	public void setLotChkinRev(String lotChkinRev);
}
