package jp.co.blueship.tri.am.dao.baseline;

import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the head lot baseline link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadLotBlLnkDao extends IJdbcDao<IHeadLotBlLnkEntity> {

}
