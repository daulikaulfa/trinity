package jp.co.blueship.tri.am.dao.baseline;

import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the head baseline DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadBlDao extends IJdbcDao<IHeadBlEntity> {

}
