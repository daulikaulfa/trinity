package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadLotBlLnkItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the head lot baseline link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HeadLotBlLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_HEAD_LOT_BL_LNK;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * head baseline ID's
	 */
	public String[] headBlIds = null;
	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(HeadLotBlLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public HeadLotBlLnkCondition(){
		super(attr);
	}

	/**
	 * head baseline IDを取得します。
	 * @return head baseline ID
	 */
	public String getHeadBlId() {
	    return headBlId;
	}

	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	    super.append( HeadLotBlLnkItems.headBlId, headBlId );
	}

	/**
	 * head baseline ID'sを取得します。
	 * @return head baseline ID's
	 */
	public String[] getHeadBlIds() {
	    return headBlIds;
	}

	/**
	 * head baseline ID'sを設定します。
	 * @param headBlIds head baseline ID's
	 */
	public void setHeadBlIds(String[] headBlIds) {
	    this.headBlIds = headBlIds;
	    super.append( HeadLotBlLnkItems.headBlId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(headBlIds, HeadLotBlLnkItems.headBlId.getItemName(), false, true, false) );
	}

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId() {
	    return lotBlId;
	}

	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	    super.append( HeadLotBlLnkItems.lotBlId, lotBlId );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( HeadLotBlLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}