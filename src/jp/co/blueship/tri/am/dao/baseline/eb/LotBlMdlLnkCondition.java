package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlMdlLnkItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the lot baseline module linik entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotBlMdlLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_BL_MDL_LNK;

	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * is check-in target module?
	 */
	public StatusFlg isChkinTargetMdl = StatusFlg.off;
	/**
	 * lot revision at the check-in
	 */
	public String lotChkinRev = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotBlMdlLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LotBlMdlLnkCondition(){
		super(attr);
	}

	/**
	 * lot baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getLotBlId() {
	    return lotBlId;
	}

	/**
	 * lot baseline IDを設定します。
	 * @param lotBlId lot baseline ID
	 */
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	    super.append( LotBlMdlLnkItems.lotBlId, lotBlId );
	}
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	    super.append(LotBlMdlLnkItems.mdlNm, mdlNm );
	}
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	    super.append(LotBlMdlLnkItems.mdlVerTag, mdlVerTag );
	}

	/**
	 * check-in target moduleかどうかを取得します。
	 * @return check-in target moduleの場合true。それ以外はfalse。
	 */
	public StatusFlg isChkinTargetMdl() {
	    return isChkinTargetMdl;
	}

	/**
	 * check-in target moduleかどうかを設定します。
	 * @param isChkinTargetMdlisChkinTargetMdl target moduleの場合true。それ以外はfalse。
	 */
	public void setChkinTargetMdl(StatusFlg isChkinTargetMdl) {
	    this.isChkinTargetMdl = isChkinTargetMdl;
	    super.append( LotBlMdlLnkItems.isChkinTargetMdl, (null == isChkinTargetMdl)? StatusFlg.off.parseBoolean(): isChkinTargetMdl.parseBoolean() );
	}
	/**
	 * lot check-in revision を取得します。
	 * @return lot check-in revision
	 */
	public String getLotChkinRev() {
	    return lotChkinRev;
	}
	/**
	 * lot check-in revisionを設定します。
	 * @param lotChkinRev lot check-in revision
	 */
	public void setLotChkinRev(String lotChkinRev) {
	    this.lotChkinRev = lotChkinRev;
	    super.append(LotBlMdlLnkItems.lotChkinRev, lotChkinRev );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotBlMdlLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}