package jp.co.blueship.tri.am.dao.baseline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lot baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum LotBlItems implements ITableItem {
	lotBlId("lot_bl_id"),
	lotBlNm("lot_bl_nm"),
	lotId("lot_id"),
	lotVerTag("lot_ver_tag"),
	lotBlTag("lot_bl_tag"),
	dataCtgCd("data_ctg_cd"),
	dataId("data_id"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	lotChkinUserId("lot_chkin_user_id"),
	lotChkinUserNm("lot_chkin_user_nm"),
	lotChkinTimestamp("lot_chkin_timestamp"),
	lotChkinCmt("lot_chkin_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),

	lotChkinUserIconPath("lot_chkin_user_id_icon_path"),
	;

	private String element = null;

	private LotBlItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
