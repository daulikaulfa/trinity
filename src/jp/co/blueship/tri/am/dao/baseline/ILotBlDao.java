package jp.co.blueship.tri.am.dao.baseline;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot baseline DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotBlDao extends IJdbcDao<ILotBlEntity> {

}
