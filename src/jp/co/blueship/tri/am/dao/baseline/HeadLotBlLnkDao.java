package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadLotBlLnkItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadLotBlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the head lot baseline link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class HeadLotBlLnkDao extends JdbcBaseDao<IHeadLotBlLnkEntity> implements IHeadLotBlLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_HEAD_LOT_BL_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IHeadLotBlLnkEntity entity ) {
		builder
			.append(HeadLotBlLnkItems.headBlId, entity.getHeadBlId(), true)
			.append(HeadLotBlLnkItems.lotBlId, entity.getLotBlId(), true)
			.append(HeadLotBlLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(HeadLotBlLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(HeadLotBlLnkItems.regUserId, entity.getRegUserId())
			.append(HeadLotBlLnkItems.regUserNm, entity.getRegUserNm())
			.append(HeadLotBlLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(HeadLotBlLnkItems.updUserId, entity.getUpdUserId())
			.append(HeadLotBlLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IHeadLotBlLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IHeadLotBlLnkEntity entity = new HeadLotBlLnkEntity();

  	  entity.setHeadBlId( rs.getString(HeadLotBlLnkItems.headBlId.getItemName()) );
  	  entity.setLotBlId( rs.getString(HeadLotBlLnkItems.lotBlId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(HeadLotBlLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(HeadLotBlLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(HeadLotBlLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(HeadLotBlLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(HeadLotBlLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(HeadLotBlLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(HeadLotBlLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
