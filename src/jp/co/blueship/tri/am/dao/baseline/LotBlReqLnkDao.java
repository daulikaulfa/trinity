package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlReqLnkItems;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot baseline request link DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LotBlReqLnkDao extends JdbcBaseDao<ILotBlReqLnkEntity> implements ILotBlReqLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_BL_REQ_LNK;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotBlReqLnkEntity entity ) {
		builder
			.append(LotBlReqLnkItems.lotBlId, entity.getLotBlId(), true)
			.append(LotBlReqLnkItems.areqId, entity.getAreqId(), true)
			.append(LotBlReqLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotBlReqLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotBlReqLnkItems.regUserId, entity.getRegUserId())
			.append(LotBlReqLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotBlReqLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotBlReqLnkItems.updUserId, entity.getUpdUserId())
			.append(LotBlReqLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotBlReqLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		LotBlReqLnkEntity entity = new LotBlReqLnkEntity();

		entity.setLotBlId( rs.getString(LotBlReqLnkItems.lotBlId.getItemName()) );
		entity.setPjtId( rs.getString(LotBlReqLnkItems.pjtId.getItemName()) );
		entity.setAreqId( rs.getString(LotBlReqLnkItems.areqId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotBlReqLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(LotBlReqLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(LotBlReqLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(LotBlReqLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(LotBlReqLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(LotBlReqLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(LotBlReqLnkItems.updUserNm.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,Q.PJT_ID")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT PJT_ID, AREQ_ID AS Q_AREQ_ID FROM AM_AREQ) Q ON AREQ_ID = Q.Q_AREQ_ID")
			;

			return buf.toString();
		}
	}

}
