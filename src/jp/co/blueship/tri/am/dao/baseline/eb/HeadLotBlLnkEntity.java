package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * head lot baseline link entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class HeadLotBlLnkEntity extends EntityFooter implements IHeadLotBlLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * lot baseline ID
	 */
	public String lotBlId = null;

	@Override
	public String getHeadBlId() {
	    return headBlId;
	}
	@Override
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	}
	@Override
	public String getLotBlId() {
	    return lotBlId;
	}
	@Override
	public void setLotBlId(String lotBlId) {
	    this.lotBlId = lotBlId;
	}


}
