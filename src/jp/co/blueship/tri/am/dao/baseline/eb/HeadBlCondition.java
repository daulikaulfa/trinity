package jp.co.blueship.tri.am.dao.baseline.eb;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the head lot baseline entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HeadBlCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_HEAD_BL;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * head baseline ID's
	 */
	public String[] headBlIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;
	/**
	 * head version tag
	 */
	public String headVerTag = null;
	/**
	 * head baseline tag
	 */
	public String headBlTag = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * mergechkUserId
	 */
	public String mergechkUserId = null;
	/**
	 * mergeUserId
	 */
	public String mergeUserId = null;
	/**
	 * mergeSts-Code's
	 */
	public String[] mergeStsCds = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(HeadBlItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public HeadBlCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( HeadBlItems.lotId ); //論理的にLOT_IDとつながる
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( HeadBlItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, HeadBlItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( HeadBlItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, HeadBlItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * head baseline IDを取得します。
	 * @return lot baseline ID
	 */
	public String getHeadBlId() {
	    return headBlId;
	}

	/**
	 * head baseline IDを設定します。
	 * @param headBlId head baseline ID
	 */
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	    super.append( HeadBlItems.headBlId, headBlId );
	}

	/**
	 * head baseline ID'sを取得します。
	 * @return head baseline ID's
	 */
	public String[] getHeadBlIds() {
	    return headBlIds;
	}

	/**
	 * head baseline ID'sを設定します。
	 * @param headBlIds head baseline ID's
	 */
	public void setHeadBlIds(String[] headBlIds) {
	    this.headBlIds = headBlIds;
	    super.append( HeadBlItems.headBlId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(headBlIds, HeadBlItems.headBlId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append( HeadBlItems.lotId, lotId );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String[] lotIds) {
	    this.lotIds = lotIds;
	    super.append( HeadBlItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, HeadBlItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * head version tagを取得します。
	 * @return head version tag
	 */
	public String getHeadVerTag() {
	    return headVerTag;
	}

	/**
	 * version tagを設定します。
	 * @param headVerTag head version tag
	 */
	public void setHeadVerTag(String headVerTag) {
	    this.headVerTag = headVerTag;
	    super.append( HeadBlItems.headVerTag, headVerTag );
	}

	/**
	 * head baseline tagを取得します。
	 * @return head baseline tag
	 */
	public String getHeadBlTag() {
	    return headBlTag;
	}

	/**
	 * head baseline tagを設定します。
	 * @param blTag head baseline tag
	 */
	public void setHeadBlTag(String headBlTag) {
	    this.headBlTag = headBlTag;
	    super.append( HeadBlItems.headBlTag, headBlTag );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append( HeadBlItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( HeadBlItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, HeadBlItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * mergeSts-Code'sを取得します。
	 * @return mergeSts-Code's
	 */
	public String[] getMergeStsCds() {
	    return mergeStsCds;
	}

	/**
	 * mergeSts-Code'sを設定します。
	 * @param mergeStsCodes mergeSts-Code's
	 */
	public void setMergeStsCds(String[] mergeStsCds) {
	    this.mergeStsCds = mergeStsCds;
	    super.append( HeadBlItems.mergeStsCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mergeStsCds, HeadBlItems.mergeStsCd.getItemName(), false, true, false) );
	}

	/**
	 * mergechkUserIdを取得します。
	 * @return mergechkUserId
	 */
	public String getMergechkUserId() {
	    return mergechkUserId;
	}

	/**
	 * mergechkUserIdを設定します。
	 * @param mergechkUserId
	 */
	public void setMergechkUserId(String mergechkUserId) {
	    this.mergechkUserId = mergechkUserId;
	    super.append( HeadBlItems.mergechkUserId, mergechkUserId );
	}

	/**
	 * mergechkTimeをFromToで設定します。
	 * @param from
	 * @param to
	 */
	public void setMergechkStTimeFromTo(String from, String to) {
		super.append(HeadBlItems.mergechkStTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, HeadBlItems.mergechkStTimestamp.getItemName(), true));
	}

	/**
	 * mergeUserIdを取得します。
	 * @return mergeUserId
	 */
	public String getMergeUserId() {
	    return mergeUserId;
	}

	/**
	 * mergeUserIdを設定します。
	 * @param mergeUserId
	 */
	public void setMergeUserId(String mergeUserId) {
	    this.mergeUserId = mergeUserId;
	    super.append( HeadBlItems.mergeUserId, mergeUserId );
	}

	/**
	 * mergeStTimeをFromToで設定します。
	 * @param from
	 * @param to
	 */
	public void setMergeStTimeFromTo(String from, String to) {
		super.append(HeadBlItems.mergeStTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, HeadBlItems.mergeStTimestamp.getItemName(), true));
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( HeadBlItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}