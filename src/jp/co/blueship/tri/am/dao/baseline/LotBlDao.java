package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.LotBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot baseline DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class LotBlDao extends JdbcBaseDao<ILotBlEntity> implements ILotBlDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_BL;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotBlEntity entity ) {
		builder
			.append(LotBlItems.lotBlId, entity.getLotBlId(), true)
			.append(LotBlItems.lotBlNm, entity.getLotBlNm())
			.append(LotBlItems.lotId, entity.getLotId())
			.append(LotBlItems.lotVerTag, entity.getLotVerTag())
			.append(LotBlItems.lotBlTag, entity.getLotBlTag())
			.append(LotBlItems.stsId, entity.getStsId())
			.append(LotBlItems.dataCtgCd, entity.getDataCtgCd())
			.append(LotBlItems.dataId, entity.getDataId())
			.append(LotBlItems.lotChkinUserId, entity.getLotChkinUserId())
			.append(LotBlItems.lotChkinUserNm, entity.getLotChkinUserNm())
			.append(LotBlItems.lotChkinTimestamp, entity.getLotChkinTimestamp())
			.append(LotBlItems.lotChkinCmt, entity.getLotChkinCmt())
			.append(LotBlItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotBlItems.regTimestamp, entity.getRegTimestamp())
			.append(LotBlItems.regUserId, entity.getRegUserId())
			.append(LotBlItems.regUserNm, entity.getRegUserNm())
			.append(LotBlItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotBlItems.updUserId, entity.getUpdUserId())
			.append(LotBlItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotBlEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  LotBlEntity entity = new LotBlEntity();

  	  entity.setLotBlId( rs.getString(LotBlItems.lotBlId.getItemName()) );
  	  entity.setLotBlNm( rs.getString(LotBlItems.lotBlNm.getItemName()) );
  	  entity.setLotId( rs.getString(LotBlItems.lotId.getItemName()) );
 	  entity.setLotVerTag( rs.getString(LotBlItems.lotVerTag.getItemName()) );
  	  entity.setLotBlTag( rs.getString(LotBlItems.lotBlTag.getItemName()) );
 	  entity.setDataCtgCd( rs.getString(LotBlItems.dataCtgCd.getItemName()) );
 	  entity.setDataId( rs.getString(LotBlItems.dataId.getItemName()) );
  	  entity.setStsId( rs.getString(LotBlItems.stsId.getItemName()) );
  	  entity.setProcStsId( rs.getString(LotBlItems.procStsId.getItemName()) );
   	  entity.setLotChkinUserId( rs.getString(LotBlItems.lotChkinUserId.getItemName()) );
  	  entity.setLotChkinUserNm( rs.getString(LotBlItems.lotChkinUserNm.getItemName()) );
  	  entity.setLotChkinTimestamp( rs.getTimestamp(LotBlItems.lotChkinTimestamp.getItemName()) );
  	  entity.setLotChkinCmt( rs.getString(LotBlItems.lotChkinCmt.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotBlItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LotBlItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LotBlItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LotBlItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LotBlItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LotBlItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LotBlItems.updUserNm.getItemName()) );

  	  entity.setLotChkinUserIconPath( rs.getString(LotBlItems.lotChkinUserIconPath.getItemName()) );

  	  return entity;
	}
}
