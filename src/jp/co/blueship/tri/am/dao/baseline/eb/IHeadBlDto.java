package jp.co.blueship.tri.am.dao.baseline.eb;

import java.util.List;

/**
 * The interface of the head baseline DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IHeadBlDto {

	/**
	 * HEAD・ベースラインを取得します。
	 *
	 * @return HEAD・ベースラインEntity
	 */
	public IHeadBlEntity getHeadBlEntity();
	/**
	 * HEAD・ベースラインを設定します。
	 *
	 * @param headBlEntity HEAD・ベースラインEntity
	 */
	public void setHeadBlEntity(IHeadBlEntity headBlEntity);

	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param mdlNm 対象モジュール名
	 * @return 取得したModule Entity
	 */
	public IHeadBlMdlLnkEntity getMdlEntity( String mdlNm );
	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param isMergeMdl マージ対象のモジュールか？
	 * @return 取得したModule Entity
	 */
	public List<IHeadBlMdlLnkEntity> getMergeMdlEntities( boolean isMergeMdl );

	/**
	 * HEAD・ベースライン・モジュールEntityのListを取得します。
	 *
	 * @return HEAD・ベースライン・モジュールEntityのList
	 */
	public List<IHeadBlMdlLnkEntity> getHeadBlMdlLnkEntities();

	/**
	 * HEAD・ベースライン・モジュールEntityのListを設定します。
	 *
	 * @param headBlMdlLnkEntities HEAD・ベースライン・モジュールEntityのList
	 */
	public void setHeadBlMdlLnkEntities(List<IHeadBlMdlLnkEntity> headBlMdlLnkEntities);
	/**
	 * HEAD・ロット・ベースラインのListを取得します。
	 * @return ロット・ロット・ベースラインのList
	 */
	public List<IHeadLotBlLnkEntity> getHeadLotBlLnkEntities();
	/**
	 * HEAD・ロット・ベースラインを設定します。
	 * @param headLotBlLnkEntities ロット・ロット・ベースラインのList
	 */
	public void setHeadLotBlLnkEntities(List<IHeadLotBlLnkEntity> headLotBlLnkEntities );


}
