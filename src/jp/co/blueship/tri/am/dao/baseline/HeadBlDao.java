package jp.co.blueship.tri.am.dao.baseline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadBlItems;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the head baseline DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public class HeadBlDao extends JdbcBaseDao<IHeadBlEntity> implements IHeadBlDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_HEAD_BL;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IHeadBlEntity entity ) {
		builder
			.append(HeadBlItems.headBlId, entity.getHeadBlId(), true)
			.append(HeadBlItems.lotId, entity.getLotId())
			.append(HeadBlItems.headVerTag, entity.getHeadVerTag())
			.append(HeadBlItems.headBlTag, entity.getHeadBlTag())
			.append(HeadBlItems.stsId, entity.getStsId())
			.append(HeadBlItems.mergeStsCd, entity.getMergeStsCd())
			.append(HeadBlItems.mergechkUserId, entity.getMergechkUserId())
			.append(HeadBlItems.mergechkUserNm, entity.getMergechkUserNm())
			.append(HeadBlItems.mergechkStTimestamp, entity.getMergechkStTimestamp())
			.append(HeadBlItems.mergechkEndTimestamp, entity.getMergechkEndTimestamp())
			.append(HeadBlItems.mergechkCmt, entity.getMergechkCmt())
			.append(HeadBlItems.mergeUserId, entity.getMergeUserId())
			.append(HeadBlItems.mergeUserNm, entity.getMergeUserNm())
			.append(HeadBlItems.mergeStTimestamp, entity.getMergeStTimestamp())
			.append(HeadBlItems.mergeEndTimestamp, entity.getMergeEndTimestamp())
			.append(HeadBlItems.mergeCmt, entity.getMergeCmt())
			.append(HeadBlItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(HeadBlItems.regTimestamp, entity.getRegTimestamp())
			.append(HeadBlItems.regUserId, entity.getRegUserId())
			.append(HeadBlItems.regUserNm, entity.getRegUserNm())
			.append(HeadBlItems.updTimestamp, entity.getUpdTimestamp())
			.append(HeadBlItems.updUserId, entity.getUpdUserId())
			.append(HeadBlItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IHeadBlEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  HeadBlEntity entity = new HeadBlEntity();

  	  entity.setHeadBlId( rs.getString(HeadBlItems.headBlId.getItemName()) );
  	  entity.setLotId( rs.getString(HeadBlItems.lotId.getItemName()) );
  	  entity.setHeadVerTag( rs.getString(HeadBlItems.headVerTag.getItemName()) );
  	  entity.setHeadBlTag( rs.getString(HeadBlItems.headBlTag.getItemName()) );
  	  entity.setStsId( rs.getString(HeadBlItems.stsId.getItemName()) );
  	  entity.setProcStsId( rs.getString(HeadBlItems.procStsId.getItemName()) );
  	  entity.setMergeStsCd(rs.getString(HeadBlItems.mergeStsCd.getItemName() ));
  	  entity.setMergechkUserId( rs.getString(HeadBlItems.mergechkUserId.getItemName()) );
  	  entity.setMergechkUserNm( rs.getString(HeadBlItems.mergechkUserNm.getItemName()) );
  	  entity.setMergechkStTimestamp( rs.getTimestamp(HeadBlItems.mergechkStTimestamp.getItemName()) );
  	  entity.setMergechkEndTimestamp( rs.getTimestamp(HeadBlItems.mergechkEndTimestamp.getItemName()) );
  	  entity.setMergechkCmt( rs.getString(HeadBlItems.mergechkCmt.getItemName()) );
  	  entity.setMergeUserId( rs.getString(HeadBlItems.mergeUserId.getItemName()) );
  	  entity.setMergeUserNm( rs.getString(HeadBlItems.mergeUserNm.getItemName()) );
  	  entity.setMergeStTimestamp( rs.getTimestamp(HeadBlItems.mergeStTimestamp.getItemName()) );
  	  entity.setMergeEndTimestamp( rs.getTimestamp(HeadBlItems.mergeEndTimestamp.getItemName()) );
  	  entity.setMergeCmt( rs.getString(HeadBlItems.mergeCmt.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(HeadBlItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(HeadBlItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(HeadBlItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(HeadBlItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(HeadBlItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(HeadBlItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(HeadBlItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
