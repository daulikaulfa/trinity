package jp.co.blueship.tri.am.dao.baseline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * head baseline entity.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yusna Marlina
 */
public class HeadBlEntity extends EntityFooter implements IHeadBlEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	/**
	 * head baseline ID
	 */
	public String headBlId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * head version tag
	 */
	public String headVerTag = null;
	/**
	 * head baseline tag
	 */
	public String headBlTag = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * merge check user-ID
	 */
	public String mergechkUserId = null;
	/**
	 * merge check user name
	 */
	public String mergechkUserNm = null;
	/**
	 * merge check start time stamp
	 */
	public Timestamp mergechkStTimestamp = null;
	/**
	 * merge check end time stamp
	 */
	public Timestamp mergechkEndTimestamp = null;
	/**
	 * merge check comment
	 */
	public String mergechkCmt = null;
	/**
	 * merge user-ID
	 */
	public String mergeUserId = null;
	/**
	 * merge user name
	 */
	public String mergeUserNm = null;
	/**
	 * merge start time stamp
	 */
	public Timestamp mergeStTimestamp = null;
	/**
	 * merge end time stamp
	 */
	public Timestamp mergeEndTimestamp = null;
	/**
	 * merge comment
	 */
	public String mergeCmt = null;
	/**
	 * merge status code
	 */
	public String mergeStsCd = null;

	@Override
	public String getHeadBlId() {
	    return headBlId;
	}
	@Override
	public void setHeadBlId(String headBlId) {
	    this.headBlId = headBlId;
	}
	@Override
	public String getLotId() {
	    return lotId;
	}
	@Override
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	@Override
	public String getHeadVerTag() {
	    return headVerTag;
	}
	@Override
	public void setHeadVerTag(String headVerTag) {
	    this.headVerTag = headVerTag;
	}
	@Override
	public String getHeadBlTag() {
	    return headBlTag;
	}
	@Override
	public void setHeadBlTag(String headBlTag) {
	    this.headBlTag = headBlTag;
	}
	@Override
	public String getStsId() {
	    return stsId;
	}
	@Override
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	@Override
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	@Override
	public String getMergechkUserId() {
	    return mergechkUserId;
	}
	@Override
	public void setMergechkUserId(String mergechkUserId) {
	    this.mergechkUserId = mergechkUserId;
	}
	@Override
	public String getMergechkUserNm() {
	    return mergechkUserNm;
	}
	@Override
	public void setMergechkUserNm(String mergechkUserNm) {
	    this.mergechkUserNm = mergechkUserNm;
	}
	@Override
	public Timestamp getMergechkStTimestamp() {
	    return mergechkStTimestamp;
	}
	@Override
	public void setMergechkStTimestamp(Timestamp mergechkStTimestamp) {
	    this.mergechkStTimestamp = mergechkStTimestamp;
	}
	@Override
	public Timestamp getMergechkEndTimestamp() {
	    return mergechkEndTimestamp;
	}
	@Override
	public void setMergechkEndTimestamp(Timestamp mergechkEndTimestamp) {
	    this.mergechkEndTimestamp = mergechkEndTimestamp;
	}
	@Override
	public String getMergechkCmt() {
	    return mergechkCmt;
	}
	@Override
	public void setMergechkCmt(String mergechkCmt) {
	    this.mergechkCmt = mergechkCmt;
	}
	@Override
	public String getMergeUserId() {
	    return mergeUserId;
	}
	@Override
	public void setMergeUserId(String mergeUserId) {
	    this.mergeUserId = mergeUserId;
	}
	@Override
	public String getMergeUserNm() {
	    return mergeUserNm;
	}
	@Override
	public void setMergeUserNm(String mergeUserNm) {
	    this.mergeUserNm = mergeUserNm;
	}
	@Override
	public Timestamp getMergeStTimestamp() {
	    return mergeStTimestamp;
	}
	@Override
	public void setMergeStTimestamp(Timestamp mergeStTimestamp) {
	    this.mergeStTimestamp = mergeStTimestamp;
	}
	@Override
	public Timestamp getMergeEndTimestamp() {
	    return mergeEndTimestamp;
	}
	@Override
	public void setMergeEndTimestamp(Timestamp mergeEndTimestamp) {
	    this.mergeEndTimestamp = mergeEndTimestamp;
	}
	@Override
	public String getMergeCmt() {
	    return mergeCmt;
	}
	@Override
	public void setMergeCmt(String mergeCmt) {
	    this.mergeCmt = mergeCmt;
	}
	@Override
	public String getMergeStsCd(){
		return mergeStsCd;
	}
	@Override
	public void setMergeStsCd(String mergeStsCd){
		this.mergeStsCd = mergeStsCd;
	}

}
