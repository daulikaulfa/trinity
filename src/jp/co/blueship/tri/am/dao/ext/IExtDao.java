package jp.co.blueship.tri.am.dao.ext;

import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the extension DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IExtDao extends IJdbcDao<IExtEntity> {

}
