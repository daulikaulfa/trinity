package jp.co.blueship.tri.am.dao.ext.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.ext.constants.ExtItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the extension entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ExtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_EXT;

	/**
	 * file extension
	 */
	public String ext = null;
	/**
	 * extension name
	 */
	public String extNm = null;
	/**
	 * file extension type
	 */
	public String extTyp = null;
	/**
	 * file extension type's
	 */
	public String[] extTyps = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(ExtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public ExtCondition(){
		super(attr);
	}

	/**
	 * extensionを取得します。
	 * @return ext
	 */
	public String getExt() {
	    return ext;
	}
	/**
	 * extensionを設定します。
	 * @param ext extension
	 */
	public void setExt(String ext) {
	    this.ext = ext;
	    super.append(ExtItems.ext, ext );
	}

	/**
	 * extension nameを取得します。
	 * @return extension-name
	 */
	public String getExtNm() {
	    return extNm;
	}
	/**
	 * extension nameを設定します。
	 * @param extNm extension-name
	 */
	public void setExtNm(String extNm) {
	    this.extNm = extNm;
	    super.append(ExtItems.extNm, extNm );
	}
	/**
	 * file extension typeを取得します。
	 * @return file-extension-type
	 */
	public String getExtTyp() {
	    return extTyp;
	}
	/**
	 * file extension typeを設定します。
	 * @param extTyp file-extension-type
	 */
	public void setExtTyp(String extTyp) {
	    this.extTyp = extTyp;
	    super.append(ExtItems.extTyp, extTyp );
	}
	/**
	 * file extension type'sを設定します。
	 * @param extTyp file-extension-type's
	 */
	public void setExtTyps(String[] extTyps) {
	    this.extTyps = extTyps;
	    super.append( ExtItems.extTyp.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(extTyps, ExtItems.extTyp.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( ExtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}