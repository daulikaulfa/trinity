package jp.co.blueship.tri.am.dao.ext;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.ext.constants.ExtItems;
import jp.co.blueship.tri.am.dao.ext.eb.ExtEntity;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the asset extension DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class ExtDao extends JdbcBaseDao<IExtEntity> implements IExtDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_EXT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IExtEntity entity ) {
		builder
			.append(ExtItems.ext, entity.getExt(), true)
			.append(ExtItems.extNm, entity.getExtNm())
			.append(ExtItems.extTyp, entity.getExtTyp())
			.append(ExtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(ExtItems.regTimestamp, entity.getRegTimestamp())
			.append(ExtItems.regUserId, entity.getRegUserId())
			.append(ExtItems.regUserNm, entity.getRegUserNm())
			.append(ExtItems.updTimestamp, entity.getUpdTimestamp())
			.append(ExtItems.updUserId, entity.getUpdUserId())
			.append(ExtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IExtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IExtEntity entity = new ExtEntity();

  	  entity.setExt( rs.getString(ExtItems.ext.getItemName()) );
  	  entity.setExtNm( rs.getString(ExtItems.extNm.getItemName()) );
  	  entity.setExtTyp( rs.getString(ExtItems.extTyp.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(ExtItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(ExtItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(ExtItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(ExtItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(ExtItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(ExtItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(ExtItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
