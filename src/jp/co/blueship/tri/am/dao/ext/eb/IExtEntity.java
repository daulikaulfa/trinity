package jp.co.blueship.tri.am.dao.ext.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the extension  entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IExtEntity extends IEntityFooter {

	public String getExt();
	public void setExt(String ext);

	public String getExtNm();
	public void setExtNm(String extNm);

	public String getExtTyp();
	public void setExtTyp(String extTyp);

}
