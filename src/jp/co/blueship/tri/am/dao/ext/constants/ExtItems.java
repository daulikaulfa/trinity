package jp.co.blueship.tri.am.dao.ext.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the extension entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum ExtItems implements ITableItem {
	ext("ext"),
	extNm("ext_nm"),
	extTyp("ext_typ"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private ExtItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
