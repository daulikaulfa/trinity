package jp.co.blueship.tri.am.dao.ext.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * extension entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class ExtEntity extends EntityFooter implements IExtEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * file extension
	 */
	public String ext = null;
	/**
	 * extension name
	 */
	public String extNm = null;
	/**
	 * file extension type
	 */
	public String extTyp = null;

	/**
	 * file extensionを取得します。
	 * @return file extension
	 */
	public String getExt() {
	    return ext;
	}
	/**
	 * file extensionを設定します。
	 * @param ext file extension
	 */
	public void setExt(String ext) {
	    this.ext = ext;
	}
	/**
	 * extension nameを取得します。
	 * @return extension name
	 */
	public String getExtNm() {
	    return extNm;
	}
	/**
	 * extension nameを設定します。
	 * @param extNm extension name
	 */
	public void setExtNm(String extNm) {
	    this.extNm = extNm;
	}
	/**
	 * file extension typeを取得します。
	 * @return file extension type
	 */
	public String getExtTyp() {
	    return extTyp;
	}
	/**
	 * file extension typeを設定します。
	 * @param extType file extension type
	 */
	public void setExtTyp(String extTyp) {
	    this.extTyp = extTyp;
	}

}
