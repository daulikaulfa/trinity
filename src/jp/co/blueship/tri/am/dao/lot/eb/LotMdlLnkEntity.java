package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotMdlLnkEntity extends EntityFooter implements ILotMdlLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * is lot include
	 */
	public StatusFlg isLotInclude = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * merge HEAD version tag
	 */
	public String mergeHeadVerTag = null;
	/**
	 * lot resistration head revision
	 */
	public String lotRegHeadRev = null;
	/**
	 * lot resistration revision
	 */
	public String lotRegRev = null;
	/**
	 * lot check-in revision
	 */
	public String lotChkinRev = null;
	/**
	 * lot latest merge revision
	 */
	public String lotLatestMergeRev = null;

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	/**
	 * is lot includeを取得します。
	 * @return is lot include
	 */
	public StatusFlg getIsLotInclude() {
	    return isLotInclude;
	}
	/**
	 * is lot includeを設定します。
	 * @param isLotInclude is lot include
	 */
	public void setIsLotInclude(StatusFlg isLotInclude) {
	    this.isLotInclude = isLotInclude;
	}
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	}
	/**
	 * merge HEAD version tagを取得します。
	 * @return merge HEAD version tag
	 */
	public String getMergeHeadVerTag() {
	    return mergeHeadVerTag;
	}
	/**
	 * merge HEAD version tagを設定します。
	 * @param mergeHeadVerTag merge HEAD version tag
	 */
	public void setMergeHeadVerTag(String mergeHeadVerTag) {
	    this.mergeHeadVerTag = mergeHeadVerTag;
	}
	/**
	 * lot resistration head revisionを取得します。
	 * @return lot resistration head revision
	 */
	public String getLotRegHeadRev() {
	    return lotRegHeadRev;
	}
	/**
	 * lot resistration head revisionを設定します。
	 * @param lotRegHeadRev lot resistration head revision
	 */
	public void setLotRegHeadRev(String lotRegHeadRev) {
	    this.lotRegHeadRev = lotRegHeadRev;
	}
	/**
	 * lot resistration revisionを取得します。
	 * @return lot resistration revision
	 */
	public String getLotRegRev() {
	    return lotRegRev;
	}
	/**
	 * lot resistration revisionを設定します。
	 * @param lotRegRev lot resistration revision
	 */
	public void setLotRegRev(String lotRegRev) {
	    this.lotRegRev = lotRegRev;
	}
	/**
	 * lot check-in revisionを取得します。
	 * @return lot check-in revision
	 */
	public String getLotChkinRev() {
	    return lotChkinRev;
	}
	/**
	 * lot check-in revisionを設定します。
	 * @param lotChkinRev lot check-in revision
	 */
	public void setLotChkinRev(String lotChkinRev) {
	    this.lotChkinRev = lotChkinRev;
	}
	/**
	 * lot latest merge revisionを取得します。
	 * @return lot latest merge revision
	 */
	public String getLotLatestMergeRev() {
	    return lotLatestMergeRev;
	}
	/**
	 * lot latest merge revisionを設定します。
	 * @param lotLatestMergeRev lot latest merge revision
	 */
	public void setLotLatestMergeRev(String lotLatestMergeRev) {
	    this.lotLatestMergeRev = lotLatestMergeRev;
	}

}
