package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotMailGrpLnkItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the lot mail group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotMailGrpLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_MAIL_GRP_LNK;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-ID's
	 */
	public String[] grpIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotMailGrpLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LotMailGrpLnkCondition(){
		super(attr);
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public LotMailGrpLnkCondition setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LotMailGrpLnkItems.lotId, lotId );
	    return this;
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(LotMailGrpLnkItems.grpId, grpId );
	}
	/**
	 * grp-ID'sを取得します。
	 * @return grp-ID's
	 */
	public String[] getGrpIds() {
	    return grpIds;
	}
	/**
	 * grp-ID'sを設定します。
	 * @param grpId grp-ID's
	 */
	public void setGrpIds(String[] grpIds) {
	    this.grpIds = grpIds;
	    super.append( LotMailGrpLnkItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, LotMailGrpLnkItems.grpId.getItemName(), false, true, false) );
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotMailGrpLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}