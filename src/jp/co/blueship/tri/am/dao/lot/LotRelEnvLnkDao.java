package jp.co.blueship.tri.am.dao.lot;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotRelEnvLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotRelEnvLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot relation environment link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LotRelEnvLnkDao extends JdbcBaseDao<ILotRelEnvLnkEntity> implements ILotRelEnvLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_REL_ENV_LNK;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotRelEnvLnkEntity entity ) {
		builder
			.append(LotRelEnvLnkItems.lotId, entity.getLotId(), true)
			.append(LotRelEnvLnkItems.bldEnvId, entity.getBldEnvId(), true)
			.append(LotRelEnvLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotRelEnvLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotRelEnvLnkItems.regUserId, entity.getRegUserId())
			.append(LotRelEnvLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotRelEnvLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotRelEnvLnkItems.updUserId, entity.getUpdUserId())
			.append(LotRelEnvLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotRelEnvLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  LotRelEnvLnkEntity entity = new LotRelEnvLnkEntity();

  	  entity.setLotId( rs.getString(LotRelEnvLnkItems.lotId.getItemName()) );
  	  entity.setBldEnvId( rs.getString(LotRelEnvLnkItems.bldEnvId.getItemName()) );
  	  entity.setBldEnvNm( rs.getString(LotRelEnvLnkItems.bldEnvNm.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotRelEnvLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LotRelEnvLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LotRelEnvLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LotRelEnvLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LotRelEnvLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LotRelEnvLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LotRelEnvLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,B.BLD_ENV_NM")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT BLD_ENV_ID AS B_BLD_ENV_ID, BLD_ENV_NM FROM BM_BLD_ENV) B ON BLD_ENV_ID = B.B_BLD_ENV_ID")
			;

			return buf.toString();
		}
	}

}
