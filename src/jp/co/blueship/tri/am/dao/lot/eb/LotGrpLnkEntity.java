package jp.co.blueship.tri.am.dao.lot.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotGrpLnkEntity extends EntityFooter implements ILotGrpLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-name
	 */
	public String grpNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp lotUpdTimestamp = null;

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * grp-nameを取得します。
	 * @return grp-name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * grp-nameを設定します。
	 * @param grpNm grp-name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}
	/**
	 * lot update timestampを取得します。
	 * @return lot update timestamp
	 */
	public Timestamp getLotUpdTimestamp() {
	    return lotUpdTimestamp;
	}
	/**
	 * lot update timestampを設定します。
	 * @param lotUpdTimestamp lot update timestamp
	 */
	public void setLotUpdTimestamp(Timestamp lotUpdTimestamp) {
	    this.lotUpdTimestamp = lotUpdTimestamp;
	}

}
