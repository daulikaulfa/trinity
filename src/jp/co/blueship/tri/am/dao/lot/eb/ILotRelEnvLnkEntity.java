package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot relation enviroment link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotRelEnvLnkEntity extends IEntityFooter {

	public String getLotId();
	public void setLotId(String lotId);

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getBldEnvNm();

}
