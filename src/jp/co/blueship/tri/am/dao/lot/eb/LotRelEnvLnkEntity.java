package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot relation environment link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotRelEnvLnkEntity extends EntityFooter implements ILotRelEnvLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build env-name
	 */
	public String bldEnvNm = null;

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * build env-nameを取得します。
	 * @return build env-name
	 */
	public String getBldEnvNm() {
	    return bldEnvNm;
	}
	/**
	 * build env-nameを設定します。
	 * @param bldEnvNm build env-name
	 */
	public void setBldEnvNm(String bldEnvNm) {
	    this.bldEnvNm = bldEnvNm;
	}

}
