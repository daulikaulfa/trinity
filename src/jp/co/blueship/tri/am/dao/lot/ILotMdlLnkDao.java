package jp.co.blueship.tri.am.dao.lot;

import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot module link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotMdlLnkDao extends IJdbcDao<ILotMdlLnkEntity> {

}
