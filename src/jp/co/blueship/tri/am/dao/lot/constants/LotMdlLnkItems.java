package jp.co.blueship.tri.am.dao.lot.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lot module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum LotMdlLnkItems implements ITableItem {
	lotId("lot_id"),
	mdlNm("mdl_nm"),
	isLotInclude("is_lot_include"),
	mdlVerTag("mdl_ver_tag"),
	mergeHeadVerTag("merge_head_ver_tag"),
	lotRegHeadRev("lot_reg_head_rev"),
	lotRegRev("lot_reg_rev"),
	lotChkinRev("lot_chkin_rev"),
	lotLatestMergeRev("lot_latest_merge_rev"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private LotMdlLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
