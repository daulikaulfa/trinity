package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotGrpLnkItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the lot group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotGrpLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_GRP_LNK;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-ID's
	 */
	public String[] grpIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotGrpLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LotGrpLnkCondition(){
		super(attr);
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public LotGrpLnkCondition setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LotGrpLnkItems.lotId, lotId );
	    return this;
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(LotGrpLnkItems.grpId, grpId );
	}
	/**
	 * grp-ID'sを取得します。
	 * @return grp-ID's
	 */
	public String[] getGrpIds() {
	    return grpIds;
	}
	/**
	 * grp-ID'sを設定します。
	 * @param grpId grp-ID's
	 */
	public void setGrpIds(String[] grpIds) {
	    this.grpIds = grpIds;
	    super.append( LotGrpLnkItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, LotGrpLnkItems.grpId.getItemName(), false, true, false) );
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotGrpLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}


	public void setLotUpdTimestampFromTo(String from, String to){
		super.append(LotGrpLnkItems.lotUpdTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, LotGrpLnkItems.lotUpdTimestamp.getItemName(), true));
	}
}