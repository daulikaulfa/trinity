package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotMdlLnkItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the lot module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotMdlLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_MDL_LNK;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * module-name
	 */
	public String mdlNm = null;
	/**
	 * is-lot-include
	 */
	public StatusFlg isLotInclude = StatusFlg.off;
	/**
	 * module-version-tag
	 */
	public String mdlVerTag = null;
	/**
	 * merge-HEAD-version-tag
	 */
	public String mergeHeadVerTag = null;
	/**
	 * lot resistration head revision
	 */
	public String lotRegHeadRev = null;
	/**
	 * lot resistration revision
	 */
	public String lotRegRev = null;
	/**
	 * lot check-in revision
	 */
	public String lotChkinRev = null;
	/**
	 * lot latest merge revision
	 */
	public String lotLatestMergeRev = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;

	public LotMdlLnkCondition(){
		super(attr);
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LotMdlLnkItems.lotId, lotId );
	}

	/**
	 * module-nameを取得します。
	 * @return module-name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}

	/**
	 * module-nameを設定します。
	 * @param mdlNm module-name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	    super.append(LotMdlLnkItems.mdlNm, mdlNm );
	}

	/**
	 * is-lot-includeを取得します。
	 * @return is-lot-include
	 */
	public StatusFlg getIsLotInclude() {
	    return isLotInclude;
	}

	/**
	 * is-lot-includeを設定します。
	 * @param isLotInclude is-lot-include
	 */
	public void setIsLotInclude(StatusFlg isLotInclude) {
	    this.isLotInclude = isLotInclude;
	    super.append( LotMdlLnkItems.isLotInclude, (null == isLotInclude)? StatusFlg.off.parseBoolean(): delStsId.parseBoolean() );
	}

	/**
	 * module-version-tagを取得します。
	 * @return module-version-tag
	 */
	public String getMdlVerTag() {
	    return mdlVerTag;
	}

	/**
	 * module-version-tagを設定します。
	 * @param mdlVerTag module-version-tag
	 */
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	    super.append(LotMdlLnkItems.mdlVerTag, mdlVerTag );
	}

	/**
	 * merge-HEAD-version-tagを取得します。
	 * @return merge-HEAD-version-tag
	 */
	public String getMergeHeadVerTag() {
	    return mergeHeadVerTag;
	}

	/**
	 * merge-HEAD-version-tagを設定します。
	 * @param mergeHeadVerTag merge-HEAD-version-tag
	 */
	public void setMergeHeadVerTag(String mergeHeadVerTag) {
	    this.mergeHeadVerTag = mergeHeadVerTag;
	    super.append(LotMdlLnkItems.mergeHeadVerTag, mergeHeadVerTag );
	}

	/**
	 * lot resistration head revisionを取得します。
	 * @return lot resistration head revision
	 */
	public String getLotRegHeadRev() {
	    return lotRegHeadRev;
	}

	/**
	 * lot resistration head revisionを設定します。
	 * @param lotRegHeadRev lot resistration head revision
	 */
	public void setLotRegHeadRev(String lotRegHeadRev) {
	    this.lotRegHeadRev = lotRegHeadRev;
	    super.append(LotMdlLnkItems.lotRegHeadRev, lotRegHeadRev );
	}

	/**
	 * lot resistration revisionを取得します。
	 * @return lot resistration revision
	 */
	public String getLotRegRev() {
	    return lotRegRev;
	}

	/**
	 * lot resistration revisionを設定します。
	 * @param lotRegRev lot resistration revision
	 */
	public void setLotRegRev(String lotRegRev) {
	    this.lotRegRev = lotRegRev;
	    super.append(LotMdlLnkItems.lotRegRev, lotRegRev );
	}

	/**
	 * lot check-in revisionを取得します。
	 * @return lot check-in revision
	 */
	public String getLotCloseRev() {
	    return lotChkinRev;
	}

	/**
	 * lot check-in revisionを設定します。
	 * @param lotChkinRev lot check-in revision
	 */
	public void setLotCloseRev(String lotChkinRev) {
	    this.lotChkinRev = lotChkinRev;
	    super.append(LotMdlLnkItems.lotChkinRev, lotChkinRev );
	}

	/**
	 * lot latest merge revisionを取得します。
	 * @return lot latest merge revision
	 */
	public String getLotLatestMergeRev() {
	    return lotLatestMergeRev;
	}

	/**
	 * lot latest merge revisionを設定します。
	 * @param lotLatestMergeRev lot latest merge revision
	 */
	public void setLotLatestMergeRev(String lotLatestMergeRev) {
	    this.lotLatestMergeRev = lotLatestMergeRev;
	    super.append(LotMdlLnkItems.lotLatestMergeRev, lotLatestMergeRev );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotMdlLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}