package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotMdlLnkEntity extends IEntityFooter {

	public String getLotId();
	public void setLotId(String lotId);

	public String getMdlNm();
	public void setMdlNm(String mdlNm);

	public StatusFlg getIsLotInclude();
	public void setIsLotInclude(StatusFlg isLotInclude);

	public String getMdlVerTag();
	public void setMdlVerTag(String mdlVerTag);

	public String getMergeHeadVerTag();
	public void setMergeHeadVerTag(String mergeTargetVerTag);

	public String getLotRegHeadRev();
	public void setLotRegHeadRev(String lotRegHeadRev);

	public String getLotRegRev();
	public void setLotRegRev(String lotRegRev);

	public String getLotChkinRev();
	public void setLotChkinRev(String lotChkinRev);

	public String getLotLatestMergeRev();
	public void setLotLatestMergeRev(String lotLatestMergeRev);

}
