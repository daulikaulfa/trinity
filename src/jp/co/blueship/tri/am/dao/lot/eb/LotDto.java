package jp.co.blueship.tri.am.dao.lot.eb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * lot DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class LotDto implements ILotDto {

	/**
	 * ロット
	 */
	public ILotEntity lotEntity = null;

	/**
	 * ロット・モジュールファイル
	 */
	public List<ILotMdlLnkEntity> lotMdlLnkEntities = new ArrayList<ILotMdlLnkEntity>();

	/**
	 * ロット・リリース環境ファイル
	 */
	public List<ILotRelEnvLnkEntity> lotRelEnvLnkEntities = new ArrayList<ILotRelEnvLnkEntity>();

	/**
	 * ロット・グループファイル
	 */
	public List<ILotGrpLnkEntity> lotGrpLnkEntities = new ArrayList<ILotGrpLnkEntity>();

	/**
	 * ロット・メールグループファイル
	 */
	public List<ILotMailGrpLnkEntity> lotMailGrpLnkEntities = new ArrayList<ILotMailGrpLnkEntity>();

	/**
	 * グループファイル
	 */
	public List<IGrpEntity> grpEntities = new ArrayList<IGrpEntity>();

	@Override
	public ILotMdlLnkEntity getMdlEntity( String mdlNm ) {
		for ( ILotMdlLnkEntity mdlEntity: lotMdlLnkEntities ) {
			if ( TriStringUtils.equals( mdlEntity.getMdlNm(), mdlNm ) ) {
				return mdlEntity;
			}
		}

		return null;
	}

	@Override
	public List<ILotMdlLnkEntity> getIncludeMdlEntities( boolean isLotInclude ) {
		List<ILotMdlLnkEntity> mdlEntities = new ArrayList<ILotMdlLnkEntity>();

		for ( ILotMdlLnkEntity mdlEntity: lotMdlLnkEntities ) {
			if ( mdlEntity.getIsLotInclude().parseBoolean().equals( isLotInclude ) ) {
				mdlEntities.add( mdlEntity );
			}
		}

		return mdlEntities;
	}

	/**
	 * ロットを取得します。
	 * @return ロット
	 */
	public ILotEntity getLotEntity() {
	    return lotEntity;
	}

	/**
	 * ロットを設定します。
	 * @param lotEntity ロット
	 */
	public void setLotEntity(ILotEntity lotEntity) {
	    this.lotEntity = lotEntity;
	}

	/**
	 * ロット・モジュールファイルを取得します。
	 * @return ロット・モジュールファイル
	 */
	public List<ILotMdlLnkEntity> getLotMdlLnkEntities() {
	    return lotMdlLnkEntities;
	}

	/**
	 * ロット・モジュールファイルを設定します。
	 * @param lotMdlLnkEntities ロット・モジュールファイル
	 */
	public void setLotMdlLnkEntities(List<ILotMdlLnkEntity> lotMdlLnkEntities) {
	    this.lotMdlLnkEntities = lotMdlLnkEntities;
	}

	/**
	 * ロット・リリース環境ファイルを取得します。
	 * @return ロット・リリース環境ファイル
	 */
	public List<ILotRelEnvLnkEntity> getLotRelEnvLnkEntities() {
	    return lotRelEnvLnkEntities;
	}

	/**
	 * ロット・リリース環境ファイルを設定します。
	 * @param lotRelEnvLnkEntities ロット・リリース環境ファイル
	 */
	public void setLotRelEnvLnkEntities(List<ILotRelEnvLnkEntity> lotRelEnvLnkEntities) {
	    this.lotRelEnvLnkEntities = lotRelEnvLnkEntities;
	}

	/**
	 * ロット・グループファイルを取得します。
	 * @return ロット・グループファイル
	 */
	public List<ILotGrpLnkEntity> getLotGrpLnkEntities() {
	    return lotGrpLnkEntities;
	}

	/**
	 * ロット・グループファイルを設定します。
	 * @param lotGrpLnkEntities ロット・グループファイル
	 */
	public void setLotGrpLnkEntities(List<ILotGrpLnkEntity> lotGrpLnkEntities) {
	    this.lotGrpLnkEntities = lotGrpLnkEntities;
	}

	/**
	 * ロット・メールグループファイルを取得します。
	 * @return ロット・メールグループファイル
	 */
	public List<ILotMailGrpLnkEntity> getLotMailGrpLnkEntities() {
	    return lotMailGrpLnkEntities;
	}

	/**
	 * ロット・メールグループファイルを設定します。
	 * @param lotMailGrpLnkEntities ロット・メールグループファイル
	 */
	public void setLotMailGrpLnkEntities(List<ILotMailGrpLnkEntity> lotMailGrpLnkEntities) {
	    this.lotMailGrpLnkEntities = lotMailGrpLnkEntities;
	}

	/**
	 * グループファイルを設定します。
	 * @param grpEntities グループファイル
	 */
	public void setGrpEntities(List<IGrpEntity> grpEntities) {
	    this.grpEntities = grpEntities;
	}

	/**
	 * グループEntityを取得します。
	 *
	 * @return グループEntity
	 */
	public IGrpEntity getGrpEntity(String grpId) {
		for ( IGrpEntity entity: this.grpEntities ) {
			if ( entity.getGrpId().equals(grpId) )
				return entity;
		}
		return null;
	}

}
