package jp.co.blueship.tri.am.dao.lot.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the lot entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public enum LotItems implements ITableItem {
	lotId("lot_id"),
	lotNm("lot_nm"),
	summary("summary"),
	content("content"),
	vcsCtgCd("vcs_ctg_cd"),
	inOutBox("in_out_box"),
	sysInBox("sys_in_box"),
	lotMwPath("lot_mw_path"),
	histPath("hist_path"),
	wsPath("ws_path"),
	bldEnvId("bld_env_id"),
	fullBldEnvId("full_bld_env_id"),
	isAssetDup("is_asset_dup"),
	lotBranchTag("lot_branch_tag"),
	lotBranchBlTag("lot_branch_bl_tag"),
	lotLatestVerTag("lot_latest_ver_tag"),
	lotLatestBlTag("lot_latest_bl_tag"),
	allowListView("allow_list_view"),
	useAm("use_am"),
	useRm("use_rm"),
	useMerge("use_merge"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	closeUserId("close_user_id"),
	closeUserNm("close_user_nm"),
	closeTimestamp("close_timestamp"),
	closeCmt("close_cmt"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	iconTyp("icon_typ"),
	defaultIconPath("default_icon_path"),
	customIconPath("custom_icon_path"),
	themeColor("theme_color");

	private String element = null;

	private LotItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
