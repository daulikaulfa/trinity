package jp.co.blueship.tri.am.dao.lot;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotMailGrpLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot mail group link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LotMailGrpLnkDao extends JdbcBaseDao<ILotMailGrpLnkEntity> implements ILotMailGrpLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_MAIL_GRP_LNK;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotMailGrpLnkEntity entity ) {
		builder
			.append(LotMailGrpLnkItems.lotId, entity.getLotId(), true)
			.append(LotMailGrpLnkItems.grpId, entity.getGrpId(), true)
			.append(LotMailGrpLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotMailGrpLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotMailGrpLnkItems.regUserId, entity.getRegUserId())
			.append(LotMailGrpLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotMailGrpLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotMailGrpLnkItems.updUserId, entity.getUpdUserId())
			.append(LotMailGrpLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotMailGrpLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  LotMailGrpLnkEntity entity = new LotMailGrpLnkEntity();

  	  entity.setLotId( rs.getString(LotMailGrpLnkItems.lotId.getItemName()) );
  	  entity.setGrpId( rs.getString(LotMailGrpLnkItems.grpId.getItemName()) );
  	  entity.setGrpNm( rs.getString(LotMailGrpLnkItems.grpNm.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotMailGrpLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(LotMailGrpLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(LotMailGrpLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(LotMailGrpLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(LotMailGrpLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(LotMailGrpLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(LotMailGrpLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,G.GRP_NM")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT GRP_ID AS G_GRP_ID, GRP_NM FROM UM_GRP) G ON GRP_ID = G.G_GRP_ID")
			;

			return buf.toString();
		}
	}

}
