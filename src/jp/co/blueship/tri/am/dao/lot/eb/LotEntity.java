package jp.co.blueship.tri.am.dao.lot.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class LotEntity extends EntityFooter implements ILotEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot name
	 */
	public String lotNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * vcs catalog code
	 */
	public String vcsCtgCd = null;
	/**
	 * in out box
	 */
	public String inOutBox = null;
	/**
	 * system in box
	 */
	public String sysInBox = null;
	/**
	 * lot mw path
	 */
	public String lotMwPath = null;
	/**
	 * history path
	 */
	public String histPath = null;
	/**
	 * work space path
	 */
	public String wsPath = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * full build env-ID
	 */
	public String fullBldEnvId = null;
	/**
	 * is asset suplication
	 */
	public StatusFlg isAssetDup = StatusFlg.off;
	/**
	 * lot branch tag
	 */
	public String lotBranchTag = null;
	/**
	 * lot branch baseline tag
	 */
	public String lotBranchBlTag = null;
	/**
	 * lot latest version tag
	 */
	public String lotLatestVerTag = null;
	/**
	 * lot latest baseline tag
	 */
	public String lotLatestBlTag = null;
	/**
	 * allow list view
	 */
	public StatusFlg allowListView = StatusFlg.off;
	/**
	 * 資産管理機能を利用する
	 */
	public StatusFlg useAm = StatusFlg.off;
	/**
	 * リリース管理機能を利用する
	 */
	public StatusFlg useRm = StatusFlg.off;
	/**
	 * マージ機能を利用する
	 */
	public StatusFlg useMerge = StatusFlg.off;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;

	/**
	 * icon type
	 */
	public String iconTyp = null;

	/**
	 * default icon
	 */
	public String defaultIconPath = null;

	/**
	 * custom icon
	 */
	public String customIconPath = null;

	/**
	 * theme color
	 */
	public String themeColor = null;
	
	/**
	 * @return the iconTyp
	 */
	public String getIconTyp() {
		return iconTyp;
	}

	/**
	 * @param iconTyp the iconTyp to set
	 */
	public void setIconTyp(String iconTyp) {
		this.iconTyp = iconTyp;
	}

	/**
	 * @return the defaultIcon
	 */
	public String getDefaultIconPath() {
		return defaultIconPath;
	}

	/**
	 * @param defaultIcon the defaultIcon to set
	 */
	public void setDefaultIconPath(String defaultIconPath) {
		this.defaultIconPath = defaultIconPath;
	}

	/**
	 * @return the customIcon
	 */
	public String getCustomIconPath() {
		return customIconPath;
	}

	/**
	 * @param customIcon the customIcon to set
	 */
	public void setCustomIconPath(String customIconPath) {
		this.customIconPath = customIconPath;
	}

	/**
	 * @return the themeColor
	 */
	public String getThemeColor() {
		return themeColor;
	}

	/**
	 * @param themeColor the themeColor to set
	 */
	public void setThemeColor(String themeColor) {
		this.themeColor = themeColor;
	}

	@Override
	public String getLotId() {
		return lotId;
	}

	@Override
	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public String getLotNm() {
		return lotNm;
	}

	@Override
	public void setLotNm(String lotNm) {
		this.lotNm = lotNm;
	}

	@Override
	public String getSummary() {
		return summary;
	}

	@Override
	public void setSummary(String summary) {
		this.summary = summary;
	}

	@Override
	public String getContent() {
		return content;
	}

	@Override
	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public String getVcsCtgCd() {
		return vcsCtgCd;
	}

	@Override
	public void setVcsCtgCd(String vcsCtgCd) {
		this.vcsCtgCd = vcsCtgCd;
	}

	@Override
	public String getInOutBox() {
		return inOutBox;
	}

	@Override
	public void setInOutBox(String inOutBox) {
		this.inOutBox = inOutBox;
	}

	@Override
	public String getSysInBox() {
		return sysInBox;
	}

	@Override
	public void setSysInBox(String sysInBox) {
		this.sysInBox = sysInBox;
	}

	@Override
	public String getLotMwPath() {
		return lotMwPath;
	}

	@Override
	public void setLotMwPath(String lotMwPath) {
		this.lotMwPath = lotMwPath;
	}

	@Override
	public String getHistPath() {
		return histPath;
	}

	@Override
	public void setHistPath(String histPath) {
		this.histPath = histPath;
	}

	@Override
	public String getWsPath() {
		return wsPath;
	}

	@Override
	public void setWsPath(String wsPath) {
		this.wsPath = wsPath;
	}

	@Override
	public String getFullBldEnvId() {
		return fullBldEnvId;
	}

	@Override
	public void setFullBldEnvId(String fullBldEnvId) {
		this.fullBldEnvId = fullBldEnvId;
	}

	@Override
	public String getBldEnvId() {
		return bldEnvId;
	}

	@Override
	public void setBldEnvId(String bldEnvId) {
		this.bldEnvId = bldEnvId;
	}

	@Override
	public StatusFlg getIsAssetDup() {
		return isAssetDup;
	}

	@Override
	public void setIsAssetDup(StatusFlg isAssetDup) {
		this.isAssetDup = isAssetDup;
	}

	@Override
	public String getLotBranchTag() {
		return lotBranchTag;
	}

	@Override
	public void setLotBranchTag(String lotBranchTag) {
		this.lotBranchTag = lotBranchTag;
	}

	@Override
	public String getLotBranchBlTag() {
		return lotBranchBlTag;
	}

	@Override
	public void setLotBranchBlTag(String lotBranchBlTag) {
		this.lotBranchBlTag = lotBranchBlTag;
	}

	@Override
	public String getLotLatestVerTag() {
		return lotLatestVerTag;
	}

	@Override
	public void setLotLatestVerTag(String lotLatestVerTag) {
		this.lotLatestVerTag = lotLatestVerTag;
	}

	@Override
	public String getLotLatestBlTag() {
		return lotLatestBlTag;
	}

	@Override
	public void setLotLatestBlTag(String lotLatestBlTag) {
		this.lotLatestBlTag = lotLatestBlTag;
	}

	@Override
	public StatusFlg getAllowListView() {
		return allowListView;
	}

	@Override
	public void setAllowListView(StatusFlg allowListView) {
		this.allowListView = allowListView;
	}

	@Override
	public StatusFlg isUseAm() {
		return useAm;
	}

	@Override
	public void setUseAm(StatusFlg useAm) {
		this.useAm = useAm;
	}

	@Override
	public StatusFlg isUseRm() {
		return useRm;
	}

	@Override
	public void setUseRm(StatusFlg useRm) {
		this.useRm = useRm;
	}

	@Override
	public StatusFlg isUseMerge() {
		return useMerge;
	}

	@Override
	public void setUseMerge(StatusFlg useMerge) {
		this.useMerge = useMerge;
	}

	@Override
	public String getStsId() {
		return stsId;
	}

	@Override
	public void setStsId(String stsId) {
		this.stsId = stsId;
	}

	@Override
	public String getProcStsId() {
		return procStsId;
	}

	@Override
	public void setProcStsId(String procStsId) {
		this.procStsId = procStsId;
	}

	@Override
	public String getCloseUserId() {
		return closeUserId;
	}

	@Override
	public void setCloseUserId(String closeUserId) {
		this.closeUserId = closeUserId;
	}

	@Override
	public String getCloseUserNm() {
		return closeUserNm;
	}

	@Override
	public void setCloseUserNm(String closeUserNm) {
		this.closeUserNm = closeUserNm;
	}

	@Override
	public Timestamp getCloseTimestamp() {
		return closeTimestamp;
	}

	@Override
	public void setCloseTimestamp(Timestamp closeTimestamp) {
		this.closeTimestamp = closeTimestamp;
	}

	@Override
	public String getCloseCmt() {
		return closeCmt;
	}

	@Override
	public void setCloseCmt(String closeCmt) {
		this.closeCmt = closeCmt;
	}

	@Override
	public String getDelUserId() {
		return delUserId;
	}

	@Override
	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}

	@Override
	public String getDelUserNm() {
		return delUserNm;
	}

	@Override
	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}

	@Override
	public Timestamp getDelTimestamp() {
		return delTimestamp;
	}

	@Override
	public void setDelTimestamp(Timestamp delTimestamp) {
		this.delTimestamp = delTimestamp;
	}

	@Override
	public String getDelCmt() {
		return delCmt;
	}

	@Override
	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}

	@Override
	public StatusFlg getDelStsId() {
		return delStsId;
	}

	@Override
	public void setDelStsId(StatusFlg delStsId) {
		this.delStsId = delStsId;
	}

	@Override
	public String getRegUserId() {
		return regUserId;
	}

	@Override
	public void setRegUserId(String regUserId) {
		this.regUserId = regUserId;
	}

	@Override
	public String getRegUserNm() {
		return regUserNm;
	}

	@Override
	public void setRegUserNm(String regUserNm) {
		this.regUserNm = regUserNm;
	}

	@Override
	public Timestamp getRegTimestamp() {
		return regTimestamp;
	}

	@Override
	public void setRegTimestamp(Timestamp regTimestamp) {
		this.regTimestamp = regTimestamp;
	}

	@Override
	public String getUpdUserId() {
		return updUserId;
	}

	@Override
	public void setUpdUserId(String updUserId) {
		this.updUserId = updUserId;
	}

	@Override
	public String getUpdUserNm() {
		return updUserNm;
	}

	@Override
	public void setUpdUserNm(String updUserNm) {
		this.updUserNm = updUserNm;
	}

	@Override
	public Timestamp getUpdTimestamp() {
		return updTimestamp;
	}

	@Override
	public void setUpdTimestamp(Timestamp updTimestamp) {
		this.updTimestamp = updTimestamp;
	}
}
