package jp.co.blueship.tri.am.dao.lot;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotGrpLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot group link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LotGrpLnkDao extends JdbcBaseDao<ILotGrpLnkEntity> implements ILotGrpLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;


	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_GRP_LNK;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotGrpLnkEntity entity ) {
		builder
			.append(LotGrpLnkItems.lotId, entity.getLotId(), true)
			.append(LotGrpLnkItems.grpId, entity.getGrpId(), true)
			.append(LotGrpLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotGrpLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotGrpLnkItems.regUserId, entity.getRegUserId())
			.append(LotGrpLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotGrpLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotGrpLnkItems.updUserId, entity.getUpdUserId())
			.append(LotGrpLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotGrpLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		LotGrpLnkEntity entity = new LotGrpLnkEntity();

		entity.setLotId( rs.getString(LotGrpLnkItems.lotId.getItemName()) );
		entity.setGrpId( rs.getString(LotGrpLnkItems.grpId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotGrpLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(LotGrpLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(LotGrpLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(LotGrpLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(LotGrpLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(LotGrpLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(LotGrpLnkItems.updUserNm.getItemName()) );

		entity.setGrpNm( rs.getString(LotGrpLnkItems.grpNm.getItemName()) );
		entity.setLotUpdTimestamp( rs.getTimestamp(LotGrpLnkItems.lotUpdTimestamp.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,G.GRP_NM")
				.append("  ,L.LOT_UPD_TIMESTAMP")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT GRP_ID AS G_GRP_ID, GRP_NM FROM UM_GRP) G ON GRP_ID = G.G_GRP_ID")
				.append(" LEFT JOIN (SELECT LOT_ID AS L_LOT_ID, UPD_TIMESTAMP AS LOT_UPD_TIMESTAMP FROM AM_LOT) L ON LOT_ID = L.L_LOT_ID")
			;

			return buf.toString();
		}
	}

}
