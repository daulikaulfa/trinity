package jp.co.blueship.tri.am.dao.lot.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface ILotEntity extends IEntityFooter, IEntityExecData {

	/**
	 * lot-IDを取得します。
	 * 
	 * @return lot-ID
	 */
	public String getLotId();

	/**
	 * lot-IDを設定します。
	 * 
	 * @param lotId
	 *            lot-ID
	 */
	public void setLotId(String lotId);

	/**
	 * lot nameを取得します。
	 * 
	 * @return lot name
	 */
	public String getLotNm();

	/**
	 * lot nameを設定します。
	 * 
	 * @param lotNm
	 *            lot name
	 */
	public void setLotNm(String lotNm);

	/**
	 * summaryを取得します。
	 * 
	 * @return summary
	 */
	public String getSummary();

	/**
	 * summaryを設定します。
	 * 
	 * @param summary
	 *            summary
	 */
	public void setSummary(String summary);

	/**
	 * contentを取得します。
	 * 
	 * @return content
	 */
	public String getContent();

	/**
	 * contentを設定します。
	 * 
	 * @param content
	 *            content
	 */
	public void setContent(String content);

	/**
	 * vcs catalog codeを取得します。
	 * 
	 * @return vcs catalog code
	 */
	public String getVcsCtgCd();

	/**
	 * vcs catalog codeを設定します。
	 * 
	 * @param vcsCtgCd
	 *            vcs catalog code
	 */
	public void setVcsCtgCd(String vcsCtgCd);

	/**
	 * in out boxを取得します。
	 * 
	 * @return in out box
	 */
	public String getInOutBox();

	/**
	 * in out boxを設定します。
	 * 
	 * @param inOutBox
	 *            in out box
	 */
	public void setInOutBox(String inOutBox);

	/**
	 * system in boxを取得します。
	 * 
	 * @return system in box
	 */
	public String getSysInBox();

	/**
	 * system in boxを設定します。
	 * 
	 * @param sysInBox
	 *            system in box
	 */
	public void setSysInBox(String sysInBox);

	/**
	 * lot mw pathを取得します。
	 * 
	 * @return lot mw path
	 */
	public String getLotMwPath();

	/**
	 * lot mw pathを設定します。
	 * 
	 * @param lotMwPath
	 *            lot mw path
	 */
	public void setLotMwPath(String lotMwPath);

	/**
	 * history pathを取得します。
	 * 
	 * @return history path
	 */
	public String getHistPath();

	/**
	 * history pathを設定します。
	 * 
	 * @param histPath
	 *            history path
	 */
	public void setHistPath(String histPath);

	/**
	 * work space pathを取得します。
	 * 
	 * @return work space path
	 */
	public String getWsPath();

	/**
	 * work space pathを設定します。
	 * 
	 * @param wsPath
	 *            work space path
	 */
	public void setWsPath(String wsPath);

	/**
	 * full build env-IDを取得します。
	 * 
	 * @return full build env-ID
	 */
	public String getFullBldEnvId();

	/**
	 * full build env-IDを設定します。
	 * 
	 * @param fullBldEnvId
	 *            full-build-env-ID
	 */
	public void setFullBldEnvId(String fullBldEnvId);

	/**
	 * build env-IDを取得します。
	 * 
	 * @return build env-ID
	 */
	public String getBldEnvId();

	/**
	 * build env-IDを設定します。
	 * 
	 * @param bldEnvId
	 *            build env-ID
	 */
	public void setBldEnvId(String bldEnvId);

	/**
	 * is asset suplicationを取得します。
	 * 
	 * @return is asset suplication
	 */
	public StatusFlg getIsAssetDup();

	/**
	 * is asset suplicationを設定します。
	 * 
	 * @param isAssetDup
	 *            is asset suplication
	 */
	public void setIsAssetDup(StatusFlg isAssetDup);

	/**
	 * lot branch tagを取得します。
	 * 
	 * @return lot branch tag
	 */
	public String getLotBranchTag();

	/**
	 * lot branch tagを設定します。
	 * 
	 * @param lotBranchTag
	 *            lot branch tag
	 */
	public void setLotBranchTag(String lotBranchTag);

	/**
	 * lot branch tagを取得します。
	 * 
	 * @return lot branch baseline tag
	 */
	public String getLotBranchBlTag();

	/**
	 * lot branch tagを設定します。
	 * 
	 * @param lotBranchBlTag
	 *            lot branch baseline tag
	 */
	public void setLotBranchBlTag(String lotBranchBlTag);

	/**
	 * lot latest version tagを取得します。
	 * 
	 * @return lot latest version tag
	 */
	public String getLotLatestVerTag();

	/**
	 * lot latest version tagを設定します。
	 * 
	 * @param lotLatestVerTag
	 *            lot latest version tag
	 */
	public void setLotLatestVerTag(String lotLatestVerTag);

	/**
	 * lot latest baseline tagを取得します。
	 * 
	 * @return lot latest baseline tag
	 */
	public String getLotLatestBlTag();

	/**
	 * lot latest baseline tagを設定します。
	 * 
	 * @param lotLatestBlTag
	 *            lot latest baseline tag
	 */
	public void setLotLatestBlTag(String lotLatestBlTag);

	/**
	 * allow list viewを取得します。
	 * 
	 * @return allow list view
	 */
	public StatusFlg getAllowListView();

	/**
	 * allow list viewを設定します。
	 * 
	 * @param allowListView
	 *            allow list view
	 */
	public void setAllowListView(StatusFlg allowListView);

	/**
	 * 資産管理機能を利用するかどうかを取得します。
	 * 
	 * @return 資産管理機能を利用する
	 */
	public StatusFlg isUseAm();

	/**
	 * 資産管理機能を利用するかどうかを設定します。
	 * 
	 * @param useAm
	 *            資産管理機能を利用する
	 */
	public void setUseAm(StatusFlg useAm);

	/**
	 * リリース管理機能を利用するかどうかを取得します。
	 * 
	 * @return リリース管理機能を利用する
	 */
	public StatusFlg isUseRm();

	/**
	 * リリース管理機能を利用するかどうかを設定します。
	 * 
	 * @param useRm
	 *            リリース管理機能を利用する
	 */
	public void setUseRm(StatusFlg useRm);

	/**
	 * マージ機能を利用するかどうかを取得します。
	 * 
	 * @return マージ機能を利用する
	 */
	public StatusFlg isUseMerge();

	/**
	 * マージ機能を利用するかどうかを設定します。
	 * 
	 * @param useMerge
	 *            マージ機能を利用する
	 */
	public void setUseMerge(StatusFlg useMerge);

	/**
	 * sts-IDを取得します。
	 * 
	 * @return sts-ID
	 */
	public String getStsId();

	/**
	 * sts-IDを設定します。
	 * 
	 * @param stsId
	 *            sts-ID
	 */
	public void setStsId(String stsId);

	/**
	 * process status-IDを取得します。
	 * 
	 * @return process status-ID
	 */
	public String getProcStsId();

	/**
	 * process status-IDを設定します。
	 * 
	 * @param procStsId
	 *            process status-ID
	 */
	public void setProcStsId(String procStsId);

	/**
	 * close user-IDを取得します。
	 * 
	 * @return close user-ID
	 */
	public String getCloseUserId();

	/**
	 * close user-IDを設定します。
	 * 
	 * @param closeUserId
	 *            close user-ID
	 */
	public void setCloseUserId(String closeUserId);

	/**
	 * close user nameを取得します。
	 * 
	 * @return close user name
	 */
	public String getCloseUserNm();

	/**
	 * close user nameを設定します。
	 * 
	 * @param closeUserNm
	 *            close user name
	 */
	public void setCloseUserNm(String closeUserNm);

	/**
	 * close time stampを取得します。
	 * 
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp();

	/**
	 * close time stampを設定します。
	 * 
	 * @param closeTimestamp
	 *            close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp);

	/**
	 * close commentを取得します。
	 * 
	 * @return close comment
	 */
	public String getCloseCmt();

	/**
	 * close commentを設定します。
	 * 
	 * @param closeCmt
	 *            close comment
	 */
	public void setCloseCmt(String closeCmt);

	/**
	 * delete user-IDを取得します。
	 * 
	 * @return delete user-ID
	 */
	public String getDelUserId();

	/**
	 * delete user-IDを設定します。
	 * 
	 * @param delUserId
	 *            delete user-ID
	 */
	public void setDelUserId(String delUserId);

	/**
	 * delete user nameを取得します。
	 * 
	 * @return delete user name
	 */
	public String getDelUserNm();

	/**
	 * delete user nameを設定します。
	 * 
	 * @param delUserNm
	 *            delete user name
	 */
	public void setDelUserNm(String delUserNm);

	/**
	 * delete time stampを取得します。
	 * 
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp();

	/**
	 * delete time stampを設定します。
	 * 
	 * @param delTimestamp
	 *            delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp);

	/**
	 * delete commentを取得します。
	 * 
	 * @return delete comment
	 */
	public String getDelCmt();

	/**
	 * delete commentを設定します。
	 * 
	 * @param delCmt
	 *            delete comment
	 */
	public void setDelCmt(String delCmt);

	/**
	 * delete-status-IDを取得します。
	 * 
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId();

	/**
	 * delete-status-IDを設定します。
	 * 
	 * @param delStsId
	 *            delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId);

	/**
	 * registration user-IDを取得します。
	 * 
	 * @return registration user-ID
	 */
	public String getRegUserId();

	/**
	 * registration user-IDを設定します。
	 * 
	 * @param regUserId
	 *            registration user-ID
	 */
	public void setRegUserId(String regUserId);

	/**
	 * registration user nameを取得します。
	 * 
	 * @return registration user name
	 */
	public String getRegUserNm();

	/**
	 * registration user nameを設定します。
	 * 
	 * @param regUserNm
	 *            registration user name
	 */
	public void setRegUserNm(String regUserNm);

	/**
	 * registration time stampを取得します。
	 * 
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp();

	/**
	 * registration time stampを設定します。
	 * 
	 * @param regTimestamp
	 *            registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp);

	/**
	 * update user-IDを取得します。
	 * 
	 * @return update user-ID
	 */
	public String getUpdUserId();

	/**
	 * update user-IDを設定します。
	 * 
	 * @param updUserId
	 *            update user-ID
	 */
	public void setUpdUserId(String updUserId);

	/**
	 * update user nameを取得します。
	 * 
	 * @return update user name
	 */
	public String getUpdUserNm();

	/**
	 * update user nameを設定します。
	 * 
	 * @param updUserNm
	 *            update user name
	 */
	public void setUpdUserNm(String updUserNm);

	/**
	 * update time stampを取得します。
	 * 
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp();

	/**
	 * update time stampを設定します。
	 * 
	 * @param updTimestamp
	 *            update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp);

	/**
	 * @return the iconTyp
	 */
	public String getIconTyp();

	/**
	 * @param iconTyp
	 *            the iconTyp to set
	 */
	public void setIconTyp(String iconTyp);

	/**
	 * @return the defaultIcon
	 */
	public String getDefaultIconPath();

	/**
	 * @param defaultIcon
	 *            the defaultIcon to set
	 */
	public void setDefaultIconPath(String defaultIconPath);

	/**
	 * @return the customIcon
	 */
	public String getCustomIconPath();

	/**
	 * @param customIcon
	 *            the customIcon to set
	 */
	public void setCustomIconPath(String customIconPath);

	/**
	 * @return the themeColor
	 */
	public String getThemeColor();

	/**
	 * @param themeColor
	 *            the themeColor to set
	 */
	public void setThemeColor(String themeColor);
}
