package jp.co.blueship.tri.am.dao.lot.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotGrpLnkEntity extends IEntityFooter {

	public String getLotId();
	public void setLotId(String lotId);

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getGrpNm();

	public Timestamp getLotUpdTimestamp();

}
