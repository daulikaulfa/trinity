package jp.co.blueship.tri.am.dao.lot;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;

/**
 * The implements of the lot DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class LotDao extends JdbcBaseDao<ILotEntity> implements ILotDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotEntity entity ) {
		builder
			.append(LotItems.lotId, entity.getLotId(), true)
			.append(LotItems.lotNm, entity.getLotNm())
			.append(LotItems.summary, entity.getSummary())
			.append(LotItems.content, entity.getContent())
			.append(LotItems.vcsCtgCd, entity.getVcsCtgCd())
			.append(LotItems.inOutBox, entity.getInOutBox())
			.append(LotItems.sysInBox, entity.getSysInBox())
			.append(LotItems.lotMwPath, entity.getLotMwPath())
			.append(LotItems.histPath, entity.getHistPath())
			.append(LotItems.wsPath, entity.getWsPath())
			.append(LotItems.bldEnvId, entity.getBldEnvId())
			.append(LotItems.fullBldEnvId, entity.getFullBldEnvId())
			.append(LotItems.isAssetDup, (null == entity.getIsAssetDup())? StatusFlg.off.parseBoolean(): entity.getIsAssetDup().parseBoolean())
			.append(LotItems.lotBranchTag, entity.getLotBranchTag())
			.append(LotItems.lotBranchBlTag, entity.getLotBranchBlTag())
			.append(LotItems.lotLatestVerTag, entity.getLotLatestVerTag())
			.append(LotItems.lotLatestBlTag, entity.getLotLatestBlTag())
			.append(LotItems.useAm, (null == entity.isUseAm())? StatusFlg.off.parseBoolean(): entity.isUseAm().parseBoolean())
			.append(LotItems.useRm, (null == entity.isUseRm())? StatusFlg.off.parseBoolean(): entity.isUseRm().parseBoolean())
			.append(LotItems.useMerge, (null == entity.isUseMerge())? StatusFlg.off.parseBoolean(): entity.isUseMerge().parseBoolean())
			.append(LotItems.allowListView, (null == entity.getAllowListView())? StatusFlg.off.parseBoolean(): entity.getAllowListView().parseBoolean())
			.append(LotItems.stsId, entity.getStsId())
			.append(LotItems.closeUserId, entity.getCloseUserId())
			.append(LotItems.closeUserNm, entity.getCloseUserNm())
			.append(LotItems.closeTimestamp, entity.getCloseTimestamp())
			.append(LotItems.closeCmt, entity.getCloseCmt())
			.append(LotItems.delUserId, entity.getDelUserId())
			.append(LotItems.delUserNm, entity.getDelUserNm())
			.append(LotItems.delTimestamp, entity.getDelTimestamp())
			.append(LotItems.delCmt, entity.getDelCmt())
			.append(LotItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotItems.regTimestamp, entity.getRegTimestamp())
			.append(LotItems.regUserId, entity.getRegUserId())
			.append(LotItems.regUserNm, entity.getRegUserNm())
			.append(LotItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotItems.updUserId, entity.getUpdUserId())
			.append(LotItems.updUserNm, entity.getUpdUserNm())
			.append(LotItems.iconTyp, entity.getIconTyp())
			.append(LotItems.defaultIconPath, entity.getDefaultIconPath())
			.append(LotItems.customIconPath, entity.getCustomIconPath())
			.append(LotItems.themeColor, entity.getThemeColor())
			;

		return builder;
	}

	@Override
	protected final ILotEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		LotEntity entity = new LotEntity();

		entity.setLotId( rs.getString(LotItems.lotId.getItemName()) );
		entity.setLotNm( rs.getString(LotItems.lotNm.getItemName()) );
		entity.setSummary( rs.getString(LotItems.summary.getItemName()) );
		entity.setContent( rs.getString(LotItems.content.getItemName()) );
		entity.setVcsCtgCd( rs.getString(LotItems.vcsCtgCd.getItemName()) );
		entity.setInOutBox( rs.getString(LotItems.inOutBox.getItemName()) );
		entity.setSysInBox( rs.getString(LotItems.sysInBox.getItemName()) );
		entity.setLotMwPath( rs.getString(LotItems.lotMwPath.getItemName()) );
		entity.setHistPath( rs.getString(LotItems.histPath.getItemName()) );
		entity.setWsPath( rs.getString(LotItems.wsPath.getItemName()) );
		entity.setBldEnvId( rs.getString(LotItems.bldEnvId.getItemName()) );
		entity.setFullBldEnvId( rs.getString(LotItems.fullBldEnvId.getItemName()) );
		entity.setIsAssetDup( StatusFlg.value(rs.getBoolean(LotItems.isAssetDup.getItemName())) );
		entity.setLotBranchTag( rs.getString(LotItems.lotBranchTag.getItemName()) );
		entity.setLotBranchBlTag( rs.getString(LotItems.lotBranchBlTag.getItemName()) );
		entity.setLotLatestVerTag( rs.getString(LotItems.lotLatestVerTag.getItemName()) );
		entity.setLotLatestBlTag( rs.getString(LotItems.lotLatestBlTag.getItemName()) );
		entity.setAllowListView(  StatusFlg.value(rs.getBoolean(LotItems.allowListView.getItemName())) );
		entity.setUseAm(  StatusFlg.value(rs.getBoolean(LotItems.useAm.getItemName())) );
		entity.setUseRm(  StatusFlg.value(rs.getBoolean(LotItems.useRm.getItemName())) );
		entity.setUseMerge(  StatusFlg.value(rs.getBoolean(LotItems.useMerge.getItemName())) );
		entity.setStsId( rs.getString(LotItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setCloseUserId( rs.getString(LotItems.closeUserId.getItemName()) );
		entity.setCloseUserNm( rs.getString(LotItems.closeUserNm.getItemName()) );
		entity.setCloseTimestamp( rs.getTimestamp(LotItems.closeTimestamp.getItemName()) );
		entity.setCloseCmt( rs.getString(LotItems.closeCmt.getItemName()) );
		entity.setDelUserId( rs.getString(LotItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(LotItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(LotItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(LotItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(LotItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(LotItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(LotItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(LotItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(LotItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(LotItems.updUserNm.getItemName()) );
		entity.setIconTyp(rs.getString(LotItems.iconTyp.getItemName()));
		entity.setDefaultIconPath(rs.getString(LotItems.defaultIconPath.getItemName()));
		entity.setCustomIconPath(rs.getString(LotItems.customIconPath.getItemName()));
		entity.setThemeColor(rs.getString(LotItems.themeColor.getItemName()));
		return entity;
	}

}
