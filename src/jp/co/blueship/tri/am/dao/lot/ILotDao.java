package jp.co.blueship.tri.am.dao.lot;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface ILotDao extends IJdbcDao<ILotEntity> {

}
