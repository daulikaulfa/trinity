package jp.co.blueship.tri.am.dao.lot;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotMdlLnkItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the lot module link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class LotMdlLnkDao extends JdbcBaseDao<ILotMdlLnkEntity> implements ILotMdlLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_LOT_MDL_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ILotMdlLnkEntity entity ) {
		builder
			.append(LotMdlLnkItems.lotId, entity.getLotId(), true)
			.append(LotMdlLnkItems.mdlNm, entity.getMdlNm(), true)
			.append(LotMdlLnkItems.isLotInclude, (null == entity.getIsLotInclude())? null: entity.getIsLotInclude().parseBoolean())
			.append(LotMdlLnkItems.mdlVerTag, entity.getMdlVerTag())
			.append(LotMdlLnkItems.mergeHeadVerTag, entity.getMergeHeadVerTag())
			.append(LotMdlLnkItems.lotRegHeadRev, entity.getLotRegHeadRev())
			.append(LotMdlLnkItems.lotRegRev, entity.getLotRegRev())
			.append(LotMdlLnkItems.lotChkinRev, entity.getLotChkinRev())
			.append(LotMdlLnkItems.lotLatestMergeRev, entity.getLotLatestMergeRev())
			.append(LotMdlLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(LotMdlLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(LotMdlLnkItems.regUserId, entity.getRegUserId())
			.append(LotMdlLnkItems.regUserNm, entity.getRegUserNm())
			.append(LotMdlLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(LotMdlLnkItems.updUserId, entity.getUpdUserId())
			.append(LotMdlLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ILotMdlLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  ILotMdlLnkEntity entity = new LotMdlLnkEntity();

		entity.setLotId( rs.getString(LotMdlLnkItems.lotId.getItemName()) );
		entity.setMdlNm( rs.getString(LotMdlLnkItems.mdlNm.getItemName()) );
		entity.setIsLotInclude( StatusFlg.value(rs.getBoolean(LotMdlLnkItems.isLotInclude.getItemName())) );
		entity.setMdlVerTag( rs.getString(LotMdlLnkItems.mdlVerTag.getItemName()) );
		entity.setMergeHeadVerTag( rs.getString(LotMdlLnkItems.mergeHeadVerTag.getItemName()) );
		entity.setLotRegHeadRev( rs.getString(LotMdlLnkItems.lotRegHeadRev.getItemName()) );
		entity.setLotRegRev( rs.getString(LotMdlLnkItems.lotRegRev.getItemName()) );
		entity.setLotChkinRev( rs.getString(LotMdlLnkItems.lotChkinRev.getItemName()) );
		entity.setLotLatestMergeRev( rs.getString(LotMdlLnkItems.lotLatestMergeRev.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(LotMdlLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(LotMdlLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(LotMdlLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(LotMdlLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(LotMdlLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(LotMdlLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(LotMdlLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
