package jp.co.blueship.tri.am.dao.lot;

import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot mail group link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotMailGrpLnkDao extends IJdbcDao<ILotMailGrpLnkEntity> {

}
