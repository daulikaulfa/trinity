package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotRelEnvLnkItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the lot relation environment link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotRelEnvLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT_REL_ENV_LNK;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * build-env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build-env-ID's
	 */
	public String[] bldEnvIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotRelEnvLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public LotRelEnvLnkCondition(){
		super(attr);
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public LotRelEnvLnkCondition setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LotRelEnvLnkItems.lotId, lotId );
	    return this;
	}
	/**
	 * build-env-IDを取得します。
	 * @return build-env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build-env-IDを設定します。
	 * @param bldEnvId build-env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(LotRelEnvLnkItems.bldEnvId, bldEnvId );
	}
	/**
	 * build-env-ID'sを取得します。
	 * @return build-env-ID's
	 */
	public String[] getBldEnvIds() {
	    return bldEnvIds;
	}
	/**
	 * build-env-ID'sを設定します。
	 * @param bldEnvId build-env-ID's
	 */
	public void setBldEnvIds(String[] bldEnvIds) {
	    this.bldEnvIds = bldEnvIds;
	    super.append( LotRelEnvLnkItems.bldEnvId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bldEnvIds, LotRelEnvLnkItems.bldEnvId.getItemName(), false, true, false) );
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotRelEnvLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}