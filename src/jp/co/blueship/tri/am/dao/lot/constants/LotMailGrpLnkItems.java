package jp.co.blueship.tri.am.dao.lot.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the lot mail group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum LotMailGrpLnkItems implements ITableItem {
	lotId("lot_id"),
	grpId("grp_id"),
	grpNm("grp_nm"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private LotMailGrpLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
