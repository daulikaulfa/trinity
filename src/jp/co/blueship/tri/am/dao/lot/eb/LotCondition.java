package jp.co.blueship.tri.am.dao.lot.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.constants.LotItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the lot entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L12.01
 * @author Siti Hajar
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class LotCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_LOT;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;
	/**
	 * lot name
	 */
	public String lotNm = null;
	/**
	 * lot name's
	 */
	public String[] lotNms = null;
	/**
	 * keyword
	 */
	public String keyword = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * vcs catalog code
	 */
	public String vcsCtgCd = null;
	/**
	 * in out box
	 */
	public String inOutBox = null;
	/**
	 * system in box
	 */
	public String sysInBox = null;
	/**
	 * lot mw path
	 */
	public String lotMwPath = null;
	/**
	 * history path
	 */
	public String histPath = null;
	/**
	 * work space path
	 */
	public String wsPath = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * full build env-ID
	 */
	public String fullBldEnvId = null;
	/**
	 * is asset suplication
	 */
	public StatusFlg isAssetDup = StatusFlg.off;
	/**
	 * lot branch tag
	 */
	public String lotBranchTag = null;
	/**
	 * lot branch baseline tag
	 */
	public String lotBranchBlTag = null;
	/**
	 * lot latest version tag
	 */
	public String lotLatestVerTag = null;
	/**
	 * lot latest baseline tag
	 */
	public String lotLatestBlTag = null;
	/**
	 * use merge
	 */
	public StatusFlg useMerge = null;
	/**
	 * allow list view
	 */
	public StatusFlg allowListView = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user-ID's
	 */
	public String[] closeUserIds = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user-ID's
	 */
	public String[] delUserIds = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(LotItems.delStsId, StatusFlg.off.parseBoolean() );
		}
	/**
	 * process-status-ID
	 */
	public String procStsId = null;
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public LotCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( LotItems.lotId );

	}

	@Override
	public LotCondition setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	@Override
	public LotCondition setJoinAccsHist(boolean isJoinAccsHist, JoinType join) {
		super.setJoinAccsHist(isJoinAccsHist, join);
		return this;
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	    super.append(LotItems.procStsId, procStsId );
	}

	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( LotItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, LotItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( LotItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, LotItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(LotItems.lotId, lotId );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String[] lotIds) {
	    this.lotIds = lotIds;
	    super.append( LotItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, LotItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * lot-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setLotIdsByNotEquals(String... lotIds) {
	    this.lotIds = lotIds;
	    super.appendByJoinExecData( LotItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, LotItems.lotId.getItemName(), false, false, true) );
	}

	/**
	 * lot nameを取得します。
	 * @return lot name
	 */
	public String getLotNm() {
	    return lotNm;
	}

	/**
	 * lot nameを設定します。
	 * @param lotNm lot name
	 */
	public void setLotNm(String lotNm) {
	    this.lotNm = lotNm;
	    super.append(LotItems.lotNm, lotNm );
	}

	/**
	 * lotNm'sを取得します。
	 * @return lotNm's
	 */
	public String[] getLotNms() {
	    return lotNms;
	}

	/**
	 * lotNm'sを設定します。（中間一致）
	 * @param lotNm lotNm's
	 */
	public void setLotNms(String[] lotNms) {
	    this.lotNms = lotNms;
	    super.append( LotItems.lotNm.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotNms, LotItems.lotNm.getItemName(), true, true, false) );
	}

	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String keyword) {
		if ( TriStringUtils.isEmpty(keyword) ) {
			return;
		}

	    this.keyword = keyword;
	    super.append( LotItems.lotId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[]{
	    			SqlFormatUtils.getCondition( this.keyword, LotItems.lotId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, LotItems.lotNm.getItemName(), true, false, false),
	    			}));
	}

	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}

	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	    super.append(LotItems.summary, summary );
	}

	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}

	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	    super.append(LotItems.content, content );
	}

	/**
	 * vcs catalog codeを取得します。
	 * @return vcs catalog code
	 */
	public String getVcsCtgCd() {
	    return vcsCtgCd;
	}

	/**
	 * vcs catalog codeを設定します。
	 * @param vcsCtgCd vcs catalog code
	 */
	public void setVcsCtgCd(String vcsCtgCd) {
	    this.vcsCtgCd = vcsCtgCd;
	    super.append(LotItems.vcsCtgCd, vcsCtgCd );
	}

	/**
	 * in out boxを取得します。
	 * @return in out box
	 */
	public String getInOutBox() {
	    return inOutBox;
	}

	/**
	 * in out boxを設定します。
	 * @param inOutBox in out box
	 */
	public void setInOutBox(String inOutBox) {
	    this.inOutBox = inOutBox;
	    super.append(LotItems.inOutBox, inOutBox );
	}

	/**
	 * system in boxを取得します。
	 * @return system in box
	 */
	public String getSysInBox() {
	    return sysInBox;
	}

	/**
	 * system in boxを設定します。
	 * @param sysInBox system in box
	 */
	public void setSysInBox(String sysInBox) {
	    this.sysInBox = sysInBox;
	    super.append(LotItems.sysInBox, sysInBox );
	}

	/**
	 * lot mw pathを取得します。
	 * @return lot mw path
	 */
	public String getLotMwPath() {
	    return lotMwPath;
	}

	/**
	 * lot mw pathを設定します。
	 * @param lotMwPath lot mw path
	 */
	public void setLotMwPath(String lotMwPath) {
	    this.lotMwPath = lotMwPath;
	    super.append(LotItems.lotMwPath, lotMwPath );
	}

	/**
	 * history pathを取得します。
	 * @return history path
	 */
	public String getHistPath() {
	    return histPath;
	}

	/**
	 * history pathを設定します。
	 * @param histPath history path
	 */
	public void setHistPath(String histPath) {
	    this.histPath = histPath;
	    super.append(LotItems.histPath, histPath );
	}

	/**
	 * work space pathを取得します。
	 * @return work space path
	 */
	public String getWsPath() {
	    return wsPath;
	}

	/**
	 * work space pathを設定します。
	 * @param wsPath work space path
	 */
	public void setWsPath(String wsPath) {
	    this.wsPath = wsPath;
	    super.append(LotItems.wsPath, wsPath );
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(LotItems.bldEnvId, bldEnvId );
	}

	/**
	 * full build env-IDを取得します。
	 * @return full build env-ID
	 */
	public String getFullBldEnvId() {
	    return fullBldEnvId;
	}

	/**
	 * full build env-IDを設定します。
	 * @param fullBldEnvId full build env-ID
	 */
	public void setFullBldEnvId(String fullBldEnvId) {
	    this.fullBldEnvId = fullBldEnvId;
	    super.append(LotItems.fullBldEnvId, fullBldEnvId );
	}

	/**
	 * is asset suplicationを取得します。
	 * @return is asset suplication
	 */
	public StatusFlg getIsAssetDup() {
	    return isAssetDup;
	}

	/**
	 * is asset suplicationを設定します。
	 * @param isAssetDup is asset suplication
	 */
	public void setIsAssetDup(StatusFlg isAssetDup) {
	    this.isAssetDup = isAssetDup;
	    super.append(LotItems.isAssetDup, (null == isAssetDup)? StatusFlg.off.parseBoolean(): isAssetDup.parseBoolean() );
	}

	/**
	 * lot branch tagを取得します。
	 * @return lot branch tag
	 */
	public String getLotBranchTag() {
	    return lotBranchTag;
	}

	/**
	 * lot branch tagを設定します。
	 * @param lotBranchTag lot branch tag
	 */
	public void setLotBranchTag(String lotBranchTag) {
	    this.lotBranchTag = lotBranchTag;
	    super.append(LotItems.lotBranchTag, lotBranchTag );
	}

	/**
	 * lot branch baseline tagを取得します。
	 * @return lot branch baseline tag
	 */
	public String getLotBranchBlTag() {
	    return lotBranchBlTag;
	}

	/**
	 * lot branch tagを設定します。
	 * @param lotBranchBlTag lot branch baseline tag
	 */
	public void setLotBranchBlTag(String lotBranchBlTag) {
	    this.lotBranchBlTag = lotBranchBlTag;
	    super.append(LotItems.lotBranchBlTag, lotBranchBlTag );
	}

	/**
	 * lot latest version tagを取得します。
	 * @return lot latest version tag
	 */
	public String getLotLatestVerTag() {
	    return lotLatestVerTag;
	}

	/**
	 * lot latest version tagを設定します。
	 * @param lotLatestVerTag lot latest version tag
	 */
	public void setLotLatestVerTag(String lotLatestVerTag) {
	    this.lotLatestVerTag = lotLatestVerTag;
	    super.append(LotItems.lotLatestVerTag, lotLatestVerTag );
	}

	/**
	 * lot latest baseline tagを取得します。
	 * @return lot latest baseline tag
	 */
	public String getLotLatestBlTag() {
	    return lotLatestBlTag;
	}

	/**
	 * lot latest baseline tagを設定します。
	 * @param lotLatestBlTag lot latest baseline tag
	 */
	public void setLotLatestBlTag(String lotLatestBlTag) {
	    this.lotLatestBlTag = lotLatestBlTag;
	    super.append(LotItems.lotLatestBlTag, lotLatestBlTag );
	}

	/**
	 * use mergeを取得します。
	 * @return useMerge
	 */
	public StatusFlg getUseMerge() {
	    return useMerge;
	}

	/**
	 * use mergeを設定します。
	 * @param useMerge use merge
	 */
	public void setUseMerge(StatusFlg useMerge) {
	    this.useMerge = useMerge;
	    super.append( LotItems.useMerge, (null == useMerge)? null :useMerge.parseBoolean() );
	}

	/**
	 * allow list viewを取得します。
	 * @return allow list view
	 */
	public StatusFlg getAllowListView() {
	    return allowListView;
	}

	/**
	 * allow list viewを設定します。
	 * @param allowListView allow list view
	 */
	public void setAllowListView(StatusFlg allowListView) {
	    this.allowListView = allowListView;
	    super.append(LotItems.allowListView, (null == allowListView)? StatusFlg.off.parseBoolean(): allowListView.parseBoolean()  );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(LotItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String... stsIds) {
	    this.stsIds = stsIds;
	    super.append( LotItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, LotItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}

	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	    super.append(LotItems.closeUserId, closeUserId );
	}

	/**
	 * close user-ID'sを取得します。
	 * @return close user-ID's
	 */
	public String[] getCloseUserIds() {
	    return closeUserIds;
	}

	/**
	 * close user-ID'sを設定します。
	 * @param closeUserIds close user-ID's
	 */
	public void setCloseUserIds(String[] closeUserIds) {
	    this.closeUserIds = closeUserIds;
	    super.append( LotItems.closeUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(closeUserIds, LotItems.closeUserId.getItemName(), false, true, false) );
	}

	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}

	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	    super.append(LotItems.closeUserNm, closeUserNm );
	}

	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}

	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	    super.append(LotItems.closeTimestamp, closeTimestamp );
	}

	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}

	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	    super.append(LotItems.closeCmt, closeCmt );
	}

	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}

	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	    super.append(LotItems.delUserId, delUserId );
	}

	/**
	 * delete user-ID'sを取得します。
	 * @return delete user-ID's
	 */
	public String[] getDelUserIds() {
	    return delUserIds;
	}

	/**
	 * delete user-ID'sを設定します。
	 * @param delUserIds delete user-ID's
	 */
	public void setDelUserIds(String[] delUserIds) {
	    this.delUserIds = delUserIds;
	    super.append( LotItems.delUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(delUserIds, LotItems.delUserId.getItemName(), false, true, false) );
	}

	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}

	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	    super.append(LotItems.delUserNm, delUserNm );
	}

	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}

	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	    super.append(LotItems.delTimestamp, delTimestamp );
	}

	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}

	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	    super.append(LotItems.delCmt, delCmt );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( LotItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}