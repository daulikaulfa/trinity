package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * lot mail group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class LotMailGrpLnkEntity extends EntityFooter implements ILotMailGrpLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * grp-name
	 */
	public String grpNm = null;

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * grp-nameを取得します。
	 * @return grp-name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * grp-nameを設定します。
	 * @param grpNm grp-name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}

}
