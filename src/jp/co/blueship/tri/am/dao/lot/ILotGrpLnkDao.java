package jp.co.blueship.tri.am.dao.lot;

import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot group link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotGrpLnkDao extends IJdbcDao<ILotGrpLnkEntity> {

}
