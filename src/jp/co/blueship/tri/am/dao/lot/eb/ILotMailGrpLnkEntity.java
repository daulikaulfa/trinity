package jp.co.blueship.tri.am.dao.lot.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the lot mail group link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotMailGrpLnkEntity extends IEntityFooter {

	public String getLotId();
	public void setLotId(String lotId);

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getGrpNm();

}
