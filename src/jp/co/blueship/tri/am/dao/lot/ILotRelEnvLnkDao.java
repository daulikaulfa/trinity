package jp.co.blueship.tri.am.dao.lot;

import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the lot relation environment link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ILotRelEnvLnkDao extends IJdbcDao<ILotRelEnvLnkEntity> {

}
