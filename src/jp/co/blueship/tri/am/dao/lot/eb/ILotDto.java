package jp.co.blueship.tri.am.dao.lot.eb;

import java.util.List;

import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * The interface of the lot DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface ILotDto {
	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param mdlNm 対象モジュール名
	 * @return 取得したModule Entity
	 */
	public ILotMdlLnkEntity getMdlEntity( String mdlNm );
	/**
	 * 対象のモジュールを取得します。
	 *
	 * @param isLotInclude ロット利用中のモジュールか？
	 * @return 取得したModule Entity
	 */
	public List<ILotMdlLnkEntity> getIncludeMdlEntities( boolean isLotInclude );

	/**
	 * ロットを取得します。
	 *
	 * @return ロットEntity
	 */
	public ILotEntity getLotEntity();
	/**
	 * ロットを設定します。
	 *
	 * @param lotEntity ロットEntity
	 */
	public void setLotEntity(ILotEntity lotEntity);

	/**
	 * ロット・モジュールEntityのListを取得します。
	 *
	 * @return ロット・モジュールEntityのList
	 */
	public List<ILotMdlLnkEntity> getLotMdlLnkEntities();

	/**
	 * ロット・モジュールEntityのListを設定します。
	 *
	 * @param lotMdlLnkEntities ロット・モジュールEntityのList
	 */
	public void setLotMdlLnkEntities(List<ILotMdlLnkEntity> lotMdlLnkEntities);

	/**
	 * ロット・リリース環境EntityのListを取得します。
	 *
	 * @return ロット・リリース環境EntityのList
	 */
	public List<ILotRelEnvLnkEntity> getLotRelEnvLnkEntities();

	/**
	 * ロット・リリース環境EntityのListを設定します。
	 *
	 * @param lotRelEnvLnkEntities ロット・リリース環境EntityのList
	 */
	public void setLotRelEnvLnkEntities(List<ILotRelEnvLnkEntity> lotRelEnvLnkEntities);

	/**
	 * ロット・グループEntityのListを取得します。
	 *
	 * @return ロット・グループEntityのList
	 */
	public List<ILotGrpLnkEntity> getLotGrpLnkEntities();

	/**
	 * ロット・グループEntityのListを設定します。
	 *
	 * @param lotGrpLnkEntities ロット・グループEntityのList
	 */
	public void setLotGrpLnkEntities(List<ILotGrpLnkEntity> lotGrpLnkEntities);

	/**
	 * ロット・メールグループEntityのListを取得します。
	 *
	 * @return ロット・メールグループEntityのList
	 */
	public List<ILotMailGrpLnkEntity> getLotMailGrpLnkEntities();

	/**
	 * ロット・メールグループEntityのListを設定します。
	 *
	 * @param lotGrpLnkEntities ロット・メールグループEntityのList
	 */
	public void setLotMailGrpLnkEntities(List<ILotMailGrpLnkEntity> lotMailGrpLnkEntities);

	/**
	 * グループEntityのListを取得します。
	 *
	 * @return グループEntity
	 */
	public IGrpEntity getGrpEntity(String grpId);

	/**
	 * グループEntityのListを設定します。
	 *
	 * @param grpEntities グループEntityのList
	 */
	public void setGrpEntities(List<IGrpEntity> grpEntities);

}
