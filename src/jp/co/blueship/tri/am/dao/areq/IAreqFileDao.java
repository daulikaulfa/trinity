package jp.co.blueship.tri.am.dao.areq;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the asset request file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAreqFileDao extends IJdbcDao<IAreqFileEntity> {

}
