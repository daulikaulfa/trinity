package jp.co.blueship.tri.am.dao.areq;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the asset request attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAreqAttachedFileDao extends IJdbcDao<IAreqAttachedFileEntity> {

}
