package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * asset request entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class AreqEntity extends EntityFooter implements IAreqEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category name
	 */
	public String ctgNm = null;
	/**
	 * category ID
	 */
	public String mstoneId = null;
	/**
	 * category ID
	 */
	public String mstoneNm = null;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * pjt avl sequence number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * asset request catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * asset request name
	 */
	public String assetReqNm = null;
	/**
	 * change factor number
	 */
	public String chgFactorNo = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * group name
	 */
	public String grpNm = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * due date
	 */
	public String rtnReqDueDate = null;
	/**
	 * status-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * request time stamp( lending or returning or removal )
	 */
	public Timestamp reqTimestamp = null;
	/**
	 * lend request user-ID
	 */
	public String lendReqUserId = null;
	/**
	 * lend request user name
	 */
	public String lendReqUserNm = null;
	/**
	 * lend request time stamp
	 */
	public Timestamp lendReqTimestamp = null;
	/**
	 * return request user-ID
	 */
	public String rtnReqUserId = null;
	/**
	 * return request user name
	 */
	public String rtnReqUserNm = null;
	/**
	 * return request time stamp
	 */
	public Timestamp rtnReqTimestamp = null;
	/**
	 * delete request user-ID
	 */
	public String delReqUserId = null;
	/**
	 * delete request user name
	 */
	public String delReqUserNm = null;
	/**
	 * delete request time stamp
	 */
	public Timestamp delReqTimestamp = null;
	/**
	 * assignee-ID
	 */
	public String assigneeId = null;
	/**
	 * assignee name
	 */
	public String assigneeNm = null;
	/**
	 * assinee icon path
	 */
	private String assigneeIconPath = null;

	/**
	 * pjt-apploval time stamp
	 */
	public Timestamp pjtAvlTimestamp = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 *
	 */
	public String summary = null;
	/**
	 * pjt subject
	 */
	public String pjtSubject = null;

	@Override
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	@Override
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	@Override
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
		return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
		this.areqId = areqId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
		return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
		this.pjtId = pjtId;
	}
	/**
	 * pjt avl sequence numberを取得します。
	 * @return pjt avl sequence number
	 */
	public String getPjtAvlSeqNo() {
		return pjtAvlSeqNo;
	}
	/**
	 * pjt avl sequence numberを設定します。
	 * @param pjtAvlSeqNo pjt avl sequence number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
		this.pjtAvlSeqNo = pjtAvlSeqNo;
	}
	/**
	 * asset request catalog codeを取得します。
	 * @return asset request catalog code
	 */
	public String getAreqCtgCd() {
		return areqCtgCd;
	}
	/**
	 * asset request catalog codeを設定します。
	 * @param areqCtgCd asset request catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
		this.areqCtgCd = areqCtgCd;
	}
	/**
	 * asset request nameを取得します。
	 * @return asset request name
	 */
	public String getAssetReqNm() {
		return assetReqNm;
	}
	/**
	 * asset request nameを設定します。
	 * @param assetReqNm asset request name
	 */
	public void setAssetReqNm(String assetReqNm) {
		this.assetReqNm = assetReqNm;
	}
	/**
	 * change factor numberを取得します。
	 * @return change factor number
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}
	/**
	 * change factor numberを設定します。
	 * @param chgFactorNo change factor number
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
		return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
		this.grpId = grpId;
	}
	/**
	 * group nameを取得します。
	 * @return group name
	 */
	public String getGrpNm() {
		return grpNm;
	}
	/**
	 * group nameを設定します。
	 * @param grpNm group name
	 */
	public void setGrpNm(String grpNm) {
		this.grpNm = grpNm;
	}
	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
		return content;
	}
	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	/**
	 * due dateを取得します。
	 * @return due date
	 */
	public String getRtnReqDueDate() {
		return rtnReqDueDate;
	}
	/**
	 * due dateを設定します。
	 * @param dueDate due date
	 */
	public void setRtnReqDueDate(String dueDate) {
		this.rtnReqDueDate = dueDate;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
		return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
		this.stsId = stsId;
	}
	/**
	 * process status-IDを取得します。
	 * @return process status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * request time stamp( lending or returning or removal )を取得します。
	 * @return request time stamp( lending or returning or removal )
	 */
	public Timestamp getReqTimestamp() {
	    return reqTimestamp;
	}
	/**
	 * request time stamp( lending or returning or removal )を設定します。
	 * @param reqTimestamp request time stamp( lending or returning or removal )
	 */
	public void setReqTimestamp(Timestamp reqTimestamp) {
	    this.reqTimestamp = reqTimestamp;
	}
	/**
	 * lend request user-IDを取得します。
	 * @return lend request user-ID
	 */
	public String getLendReqUserId() {
		return lendReqUserId;
	}
	/**
	 * lend request user-IDを設定します。
	 * @param lendReqUserId lend request user-ID
	 */
	public void setLendReqUserId(String lendReqUserId) {
		this.lendReqUserId = lendReqUserId;
	}
	/**
	 * lend request user nameを取得します。
	 * @return lend request user name
	 */
	public String getLendReqUserNm() {
		return lendReqUserNm;
	}
	/**
	 * lend request user nameを設定します。
	 * @param lendReqUserNm lend request user name
	 */
	public void setLendReqUserNm(String lendReqUserNm) {
		this.lendReqUserNm = lendReqUserNm;
	}
	@Override
	public String getSummary() {
		return summary;
	}
	@Override
	public void setSummary(String summary) {
		this.summary = summary;
	}
	/**
	 * lend request time stampを取得します。
	 * @return lend request time stamp
	 */
	public Timestamp getLendReqTimestamp() {
		return lendReqTimestamp;
	}
	/**
	 * lend request time stampを設定します。
	 * @param lendReqTimestamp lend request time stamp
	 */
	public void setLendReqTimestamp(Timestamp lendReqTimestamp) {
		this.lendReqTimestamp = lendReqTimestamp;
	}
	/**
	 * return request user-IDを取得します。
	 * @return return request user-ID
	 */
	public String getRtnReqUserId() {
		return rtnReqUserId;
	}
	/**
	 * return request user-IDを設定します。
	 * @param rtnReqUserId return request user-ID
	 */
	public void setRtnReqUserId(String rtnReqUserId) {
		this.rtnReqUserId = rtnReqUserId;
	}
	/**
	 * return request user nameを取得します。
	 * @return return request user name
	 */
	public String getRtnReqUserNm() {
		return rtnReqUserNm;
	}
	/**
	 * return request user nameを設定します。
	 * @param rtnReqUserNm return request user name
	 */
	public void setRtnReqUserNm(String rtnReqUserNm) {
		this.rtnReqUserNm = rtnReqUserNm;
	}
	/**
	 * return request time stampを取得します。
	 * @return return request time stamp
	 */
	public Timestamp getRtnReqTimestamp() {
		return rtnReqTimestamp;
	}
	/**
	 * return request time stampを設定します。
	 * @param rtnReqTimestamp return request time stamp
	 */
	public void setRtnReqTimestamp(Timestamp rtnReqTimestamp) {
		this.rtnReqTimestamp = rtnReqTimestamp;
	}
	/**
	 * delete request user-IDを取得します。
	 * @return delete request user-ID
	 */
	public String getDelReqUserId() {
		return delReqUserId;
	}
	/**
	 * delete request user-IDを設定します。
	 * @param delReqUserId delete request user-ID
	 */
	public void setDelReqUserId(String delReqUserId) {
		this.delReqUserId = delReqUserId;
	}
	/**
	 * delete request user nameを取得します。
	 * @return delete request user name
	 */
	public String getDelReqUserNm() {
		return delReqUserNm;
	}
	/**
	 * delete request user nameを設定します。
	 * @param delReqUserNm delete request user name
	 */
	public void setDelReqUserNm(String delReqUserNm) {
		this.delReqUserNm = delReqUserNm;
	}
	/**
	 * delete request time stampを取得します。
	 * @return delete request time stamp
	 */
	public Timestamp getDelReqTimestamp() {
		return delReqTimestamp;
	}
	/**
	 * delete request time stampを設定します。
	 * @param delReqTimestamp delete request time stamp
	 */
	public void setDelReqTimestamp(Timestamp delReqTimestamp) {
		this.delReqTimestamp = delReqTimestamp;
	}

	/**
	 * assignee-IDを取得します。
	 * @return delete assignee-ID
	 */
	public String getAssigneeId() {
		return assigneeId;
	}
	/**
	 * assignee-IDを設定します。
	 * @param assigneeId assignee-ID
	 */
	public void setAssigneeId(String assigneeId) {
		this.assigneeId = assigneeId;
	}
	/**
	 * assignee nameを取得します。
	 * @return assignee name
	 */
	public String getAssigneeNm() {
		return assigneeNm;
	}
	/**
	 * assignee nameを設定します。
	 * @param assigneeNm assignee name
	 */
	public void setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
	}

	public String getAssigneeIconPath() {
		return this.assigneeIconPath;
	}
	public void setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
	}

	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
		return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
		this.delUserId = delUserId;
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
		return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
		this.delUserNm = delUserNm;
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
		return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
		this.delTimestamp = delTimestamp;
	}
	/**
	 * pjt-apploval time stampを取得します。
	 * @return pjt-apploval time stamp
	 */
	public Timestamp getPjtAvlTimestamp() {
	    return pjtAvlTimestamp;
	}
	/**
	 * pjt-apploval time stampを設定します。
	 * @param pjtAvlTimestamp pjt-apploval time stamp
	 */
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp) {
	    this.pjtAvlTimestamp = pjtAvlTimestamp;
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
		return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
		this.delCmt = delCmt;
	}
	/**
	 * pjt subjectを取得します。
	 * @return pjt subject
	 */
	public String getPjtSubject() {
	    return pjtSubject;
	}
	/**
	 * pjt subjectを設定します。
	 * @param pjt subject
	 */
	public void setPjtSubject(String pjtSubject) {
	    this.pjtSubject = pjtSubject;
	}

}
