package jp.co.blueship.tri.am.dao.areq;

import java.text.DecimalFormat;

import jp.co.blueship.tri.fw.dao.orm.ISeqNumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * The implements of the Asset-Request file numbering DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class AreqFileNumberingDao implements ISeqNumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	public String nextval( int seq ) {
		try {
			DecimalFormat format = new DecimalFormat( "00000000" );
			return format.format( seq );

		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, Integer.toString(seq) );
		}
	}

}
