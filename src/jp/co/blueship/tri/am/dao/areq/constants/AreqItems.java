package jp.co.blueship.tri.am.dao.areq.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the asset request entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum AreqItems implements ITableItem {
	areqId("areq_id"),
	lotId("lot_id"),
	pjtId("pjt_id"),
	summary("summary"),
	pjtAvlSeqNo("pjt_avl_seq_no"),
	areqCtgCd("areq_ctg_cd"),
	assetReqNm("asset_req_nm"),
	chgFactorNo("chg_factor_no"),
	grpId("grp_id"),
	grpNm("grp_nm"),
	content("content"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	reqTimestamp("req_timestamp"),
	lendReqUserId("lend_req_user_id"),
	lendReqUserNm("lend_req_user_nm"),
	lendReqTimestamp("lend_req_timestamp"),
	rtnReqUserId("rtn_req_user_id"),
	rtnReqUserNm("rtn_req_user_nm"),
	rtnReqTimestamp("rtn_req_timestamp"),
	rtnReqDueDate("rtn_req_due_date"),
	delReqUserId("del_req_user_id"),
	delReqUserNm("del_req_user_nm"),
	delReqTimestamp("del_req_timestamp"),
	pjtAvlTimestamp("pjt_avl_timestamp"),
	assigneeId("assignee_id"),
	assigneedNm("assignee_nm"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),

	pjtSubject("p_summary"),
	assigneeIconPath("assignee_id_icon_path"),;

	private String element = null;

	private AreqItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
