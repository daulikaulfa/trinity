package jp.co.blueship.tri.am.dao.areq;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.am.dao.areq.constants.ViewAssetRegisterItems;
import jp.co.blueship.tri.am.dao.areq.eb.IViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterCondition;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.constants.AmViewTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * The implements of the asset register DAO.
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 */
public class ViewAssetRegisterDao extends JdbcBaseDao<IViewAssetRegisterEntity> implements IViewAssetRegisterDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private final String pjtCondition = "{@pjtCondition}";
	private final String areqCondition = "{@areqCondition}";
	private final String areqStsCondition = "{@areqStsCondition}";
	private final String areqFileCondition = "{@areqFileCondition}";

	private final String SQL_QUERY =
			"SELECT "
			+ " G.FILE_PATH,"
			+ " G.LEND_FILE_REV,"
			+ " G.LEND_FILE_BYTE_SIZE,"
			+ " G.LEND_FILE_UPD_TIMESTAMP,"
			+ " MAX(G.FILE_REV) FILE_REV,"
			+ " MAX(G.FILE_BYTE_SIZE) FILE_BYTE_SIZE,"
			+ " MAX(G.FILE_UPD_TIMESTAMP) FILE_UPD_TIMESTAMP,"
			+ " MAX(G.LEND_REQ_USER_ID) LEND_REQ_USER_ID,"
			+ " MAX(G.LEND_REQ_USER_NM) LEND_REQ_USER_NM,"
			+ " MAX(G.LEND_REQ_TIMESTAMP) LEND_REQ_TIMESTAMP,"
			+ " MAX(G.RTN_REQ_USER_ID) RTN_REQ_USER_ID,"
			+ " MAX(G.RTN_REQ_USER_NM) RTN_REQ_USER_NM,"
			+ " MAX(G.RTN_REQ_TIMESTAMP) RTN_REQ_TIMESTAMP,"
			+ " G.LOT_ID,"
			+ " G.LOT_NM,"
			+ " G.PJT_ID,"
			+ " G.CHG_FACTOR_NO,"
			+ " G.AREQ_ID,"
			+ " G.AREQ_CTG_CD,"
			+ " G.GRP_ID,"
			+ " G.GRP_NM,"
			+ " G.ASSET_REQ_NM,"
			+ " G.CONTENT,"
			+ " G.REQ_TIMESTAMP,"
			+ " G.REQ_USER_ID,"
			+ " G.DEL_REQ_USER_ID,"
			+ " G.DEL_REQ_USER_NM,"
			+ " G.DEL_REQ_TIMESTAMP,"
			+ " G.PJT_AVL_TIMESTAMP,"
			+ " G.STS_ID,"
			+ " G.PROC_STS_ID,"
			+ " G.DEL_STS_ID"
			+ " FROM ("
			+ " SELECT G1.*"
			+ " FROM ("
			+ " SELECT"
			+ "   F.FILE_PATH,"
			+ "   FL.FILE_REV AS LEND_FILE_REV,"
			+ "   FL.FILE_BYTE_SIZE AS LEND_FILE_BYTE_SIZE,"
			+ "   FL.FILE_UPD_TIMESTAMP AS LEND_FILE_UPD_TIMESTAMP,"
			+ "   (CASE WHEN F.AREQ_CTG_CD = '" + AreqCtgCd.LendingRequest.value() + "' THEN NULL"
			+ "   ELSE F.FILE_REV END) AS FILE_REV,"
			+ "   (CASE WHEN F.AREQ_CTG_CD = '" + AreqCtgCd.LendingRequest.value() + "' THEN NULL"
			+ "   ELSE F.FILE_BYTE_SIZE END) AS FILE_BYTE_SIZE,"
			+ "   (CASE WHEN F.AREQ_CTG_CD = '" + AreqCtgCd.LendingRequest.value() + "' THEN NULL"
			+ "   ELSE F.FILE_UPD_TIMESTAMP END) AS FILE_UPD_TIMESTAMP,"
			+ "   A.*"
			+ " FROM AM_AREQ_FILE F"
			+ "  INNER JOIN ("
			+ "   SELECT"
			+ "    P.LOT_ID,"
			+ "    P.LOT_NM,"
			+ "    P.P_PJT_ID AS PJT_ID,"
			+ "    P.CHG_FACTOR_NO,"
			+ "    A.AREQ_ID,"
			+ "    A.AREQ_CTG_CD,"
			+ "    A.GRP_ID,"
			+ "    A.GRP_NM,"
			+ "    A.SUMMARY AS ASSET_REQ_NM,"
			+ "    A.CONTENT,"
			+ "    (CASE WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.ReturningRequest.value() + "' THEN A.RTN_REQ_TIMESTAMP"
			+ "          WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.RemovalRequest.value() + "' THEN A.DEL_REQ_TIMESTAMP"
			+ "          ELSE A.LEND_REQ_TIMESTAMP END) AS REQ_TIMESTAMP,"
			+ "    (CASE WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.ReturningRequest.value() + "' THEN A.RTN_REQ_USER_ID"
			+ "          WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.RemovalRequest.value() + "' THEN A.DEL_REQ_USER_ID"
			+ "          ELSE A.LEND_REQ_USER_ID END) AS REQ_USER_ID,"
			+ "    A.LEND_REQ_USER_ID,"
			+ "    A.LEND_REQ_USER_NM,"
			+ "    A.LEND_REQ_TIMESTAMP,"
			+ "    A.RTN_REQ_USER_ID,"
			+ "    A.RTN_REQ_USER_NM,"
			+ "    A.RTN_REQ_TIMESTAMP,"
			+ "    A.DEL_REQ_USER_ID,"
			+ "    A.DEL_REQ_USER_NM,"
			+ "    A.DEL_REQ_TIMESTAMP,"
			+ "    AV.PJT_AVL_TIMESTAMP,"
			+ "    A.STS_ID,"
			+ "    (CASE WHEN S.PROC_STS_ID IS NULL THEN A.STS_ID ELSE S.PROC_STS_ID END) AS PROC_STS_ID,"
			+ "    A.DEL_STS_ID"
			+ "   FROM AM_AREQ A"
			+ "    INNER JOIN ("
			+ "      SELECT"
			+ "        L.LOT_ID, L.LOT_NM, P.CHG_FACTOR_NO, P.PJT_ID AS P_PJT_ID"
			+ "      FROM AM_PJT P"
			+ "        LEFT JOIN AM_LOT L ON L.LOT_ID = P.LOT_ID"
			+ "      WHERE " + pjtCondition + " L.DEL_STS_ID = FALSE AND P.DEL_STS_ID = FALSE"
			+ "    ) P ON A.PJT_ID = P.P_PJT_ID"
			+ "    LEFT JOIN (SELECT PJT_ID AS AV_PJT_ID, PJT_AVL_SEQ_NO AS AV_PJT_AVL_SEQ_NO, PJT_AVL_TIMESTAMP, DEL_STS_ID AS AV_DEL_STS_ID FROM AM_PJT_AVL) AV ON A.PJT_ID = AV.AV_PJT_ID AND A.PJT_AVL_SEQ_NO = AV.AV_PJT_AVL_SEQ_NO AND AV.AV_DEL_STS_ID = FALSE"
			+ "    LEFT JOIN ("
			+ "      SELECT DATA_CTG_CD"
			+ "        ,DATA_ID"
			+ "        ,MAX(STS_ID) AS PROC_STS_ID"
			+ "      FROM SM_EXEC_DATA_STS"
			+ "      WHERE DEL_STS_ID = FALSE"
			+ "      GROUP BY DATA_CTG_CD, DATA_ID"
			+ "    ) S ON S.DATA_ID = A.AREQ_ID AND S.DATA_CTG_CD = 'AM_AREQ'"
			+ "  ) A ON A.AREQ_ID = F.AREQ_ID AND " + areqCondition + " A.DEL_STS_ID = FALSE"
			+ "  LEFT JOIN AM_AREQ_FILE FL ON FL.AREQ_ID = F.AREQ_ID AND FL.AREQ_CTG_CD = '" + AreqCtgCd.LendingRequest.value() + "' AND FL.FILE_PATH = F.FILE_PATH"
			+ "  WHERE " + areqFileCondition + " F.DEL_STS_ID = FALSE"
			+ " ) G1"
			+ "  INNER JOIN ("
			+ "   SELECT"
			+ "     A.LOT_ID,"
			+ "     F.FILE_PATH,"
			+ "     MAX(A.REQ_TIMESTAMP) AS REQ_TIMESTAMP"
			+ "   FROM AM_AREQ_FILE F"
			+ "    INNER JOIN ("
			+ "     SELECT"
			+ "      P.LOT_ID,"
			+ "      A.AREQ_ID,"
			+ "      A.AREQ_CTG_CD,"
			+ "      A.GRP_NM,"
			+ "      (CASE WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.ReturningRequest.value() + "' THEN A.RTN_REQ_TIMESTAMP"
			+ "            WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.RemovalRequest.value() + "' THEN A.DEL_REQ_TIMESTAMP"
			+ "            ELSE A.LEND_REQ_TIMESTAMP END) AS REQ_TIMESTAMP,"
			+ "      (CASE WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.ReturningRequest.value() + "' THEN A.RTN_REQ_USER_ID"
			+ "            WHEN A.AREQ_CTG_CD = '" + AreqCtgCd.RemovalRequest.value() + "' THEN A.DEL_REQ_USER_ID"
			+ "            ELSE A.LEND_REQ_USER_ID END) AS REQ_USER_ID,"
			+ "      A.DEL_STS_ID"
			+ "     FROM AM_AREQ A"
			+ "      INNER JOIN ("
			+ "        SELECT"
			+ "          L.LOT_ID, L.LOT_NM, P.CHG_FACTOR_NO, P.PJT_ID AS P_PJT_ID"
			+ "        FROM AM_PJT P"
			+ "          LEFT JOIN AM_LOT L ON L.LOT_ID = P.LOT_ID"
			+ "        WHERE " + pjtCondition + " L.DEL_STS_ID = FALSE AND P.DEL_STS_ID = FALSE"
			+ "      ) P ON A.PJT_ID = P.P_PJT_ID"
			+ "    ) A ON A.AREQ_ID = F.AREQ_ID AND " + areqCondition + " A.DEL_STS_ID = FALSE"
			+ "    WHERE " + areqFileCondition + " F.DEL_STS_ID = FALSE"
			+ "   GROUP BY A.LOT_ID, F.FILE_PATH"
			+ "  ) G2 ON G2.LOT_ID = G1.LOT_ID AND G2.FILE_PATH = G1.FILE_PATH AND G2.REQ_TIMESTAMP = G1.REQ_TIMESTAMP"
			+ " WHERE " + areqStsCondition + " G1.DEL_STS_ID = FALSE"
			+ " ) G"
			+ " GROUP BY"
			+ "   G.FILE_PATH,"
			+ "   G.LEND_FILE_REV,"
			+ "   G.LEND_FILE_BYTE_SIZE,"
			+ "   G.LEND_FILE_UPD_TIMESTAMP,"
			+ "   G.LOT_ID,"
			+ "   G.LOT_NM,"
			+ "   G.PJT_ID,"
			+ "   G.CHG_FACTOR_NO,"
			+ "   G.AREQ_ID,"
			+ "   G.AREQ_CTG_CD,"
			+ "   G.GRP_ID,"
			+ "   G.GRP_NM,"
			+ "   G.ASSET_REQ_NM,"
			+ "   G.CONTENT,"
			+ "   G.REQ_TIMESTAMP,"
			+ "   G.REQ_USER_ID,"
			+ "   G.DEL_REQ_USER_ID,"
			+ "   G.DEL_REQ_USER_NM,"
			+ "   G.DEL_REQ_TIMESTAMP,"
			+ "   G.PJT_AVL_TIMESTAMP,"
			+ "   G.STS_ID,"
			+ "   G.PROC_STS_ID,"
			+ "   G.DEL_STS_ID"
			+ " ORDER BY G.LOT_ID, G.FILE_PATH"
			;

	@Override
	protected ITableAttribute getTableAttribute() {
		return AmViewTables.AM_ASSET_REGISTER;
	}

	@Override
	@Deprecated
	protected ISqlBuilder append(ISqlBuilder builder,
			IViewAssetRegisterEntity entity) {
		return null;
	}

	@Override
	protected IViewAssetRegisterEntity entityMapping(ResultSet rs, int row) throws SQLException {
		ViewAssetRegisterEntity entity = new ViewAssetRegisterEntity();

		entity.setFilePath( rs.getString(ViewAssetRegisterItems.filePath.getItemName()) );
		entity.setMdlNm( TriStringUtils.substringBefore( entity.getFilePath() ) );
		entity.setLotId( rs.getString(ViewAssetRegisterItems.lotId.getItemName()) );
		entity.setLotNm( rs.getString(ViewAssetRegisterItems.lotNm.getItemName()) );
		entity.setPjtId( rs.getString(ViewAssetRegisterItems.pjtId.getItemName()) );
		entity.setChgFactorNo( rs.getString(ViewAssetRegisterItems.chgFactorNo.getItemName()) );
		entity.setAreqId( rs.getString(ViewAssetRegisterItems.areqId.getItemName()) );
		entity.setAreqCtgCd( rs.getString(ViewAssetRegisterItems.areqCtgCd.getItemName()) );
		entity.setGrpId( rs.getString(ViewAssetRegisterItems.grpId.getItemName()) );
		entity.setGrpNm( rs.getString(ViewAssetRegisterItems.grpNm.getItemName()) );
		entity.setAssetReqNm( rs.getString(ViewAssetRegisterItems.assetReqNm.getItemName()) );
		entity.setContent( rs.getString(ViewAssetRegisterItems.content.getItemName()) );
		entity.setLendReqUserId( rs.getString(ViewAssetRegisterItems.lendReqUserId.getItemName()) );
		entity.setLendReqUserNm( rs.getString(ViewAssetRegisterItems.lendReqUserNm.getItemName()) );
		entity.setLendReqTimestamp( rs.getTimestamp(ViewAssetRegisterItems.lendReqTimestamp.getItemName()) );
		entity.setRtnReqUserId( rs.getString(ViewAssetRegisterItems.rtnReqUserId.getItemName()) );
		entity.setRtnReqUserNm( rs.getString(ViewAssetRegisterItems.rtnReqUserNm.getItemName()) );
		entity.setRtnReqTimestamp( rs.getTimestamp(ViewAssetRegisterItems.rtnReqTimestamp.getItemName()) );
		entity.setDelReqUserId( rs.getString(ViewAssetRegisterItems.delReqUserId.getItemName()) );
		entity.setDelReqUserNm( rs.getString(ViewAssetRegisterItems.delReqUserNm.getItemName()) );
		entity.setDelReqTimestamp( rs.getTimestamp(ViewAssetRegisterItems.delReqTimestamp.getItemName()) );

		entity.setLendFileRev( rs.getString(ViewAssetRegisterItems.lendFileRev.getItemName()) );
		String lendFileByteSize = rs.getString(ViewAssetRegisterItems.lendFileByteSize.getItemName());
		entity.setLendFileByteSize( ( null == lendFileByteSize )? null: Integer.valueOf(lendFileByteSize) );
		entity.setLendFileUpdTimestamp( rs.getTimestamp(ViewAssetRegisterItems.lendFileUpdTimestamp.getItemName()) );

		if ( AreqCtgCd.ReturningRequest.value().equals( entity.getAreqCtgCd() ) ) {
			entity.setRtnFileRev( rs.getString(ViewAssetRegisterItems.fileRev.getItemName()) );
			String fileByteSize = rs.getString(ViewAssetRegisterItems.fileByteSize.getItemName());
			entity.setRtnFileByteSize( ( null == fileByteSize )? null: Integer.valueOf(fileByteSize) );
			entity.setRtnFileUpdTimestamp( rs.getTimestamp(ViewAssetRegisterItems.fileUpdTimestamp.getItemName()) );
		}

		if ( AreqCtgCd.RemovalRequest.value().equals( entity.getAreqCtgCd() ) ) {
			entity.setDelFileRev( rs.getString(ViewAssetRegisterItems.fileRev.getItemName()) );
			String fileByteSize = rs.getString(ViewAssetRegisterItems.fileByteSize.getItemName());
			entity.setDelFileByteSize( ( null == fileByteSize )? null: Integer.valueOf(fileByteSize) );
			entity.setDelFileUpdTimestamp( rs.getTimestamp(ViewAssetRegisterItems.fileUpdTimestamp.getItemName()) );
		}

		entity.setPjtAvlTimestamp( rs.getTimestamp(ViewAssetRegisterItems.pjtAvlTimestamp.getItemName()) );
		entity.setStsId( rs.getString(ViewAssetRegisterItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ViewAssetRegisterItems.procStsId.getItemName()) );

		return entity;
	}

	@Override
	public List<IViewAssetRegisterEntity> find(ViewAssetRegisterCondition condition) throws TriJdbcDaoException {
		try {
			StringBuffer buf = new StringBuffer();
			String sql = null;

			buf
			.append( SQL_QUERY );

			sql = buf.toString();
			sql = StringUtils.replaceEach(
					sql,
					new String[]{pjtCondition, areqCondition, areqFileCondition, areqStsCondition},
					new String[]{
							this.getPjtCondition(condition),
							this.getAreqCondition(condition),
							this.getAreqFileCondition(condition),
							this.getAreqStsCondition(condition)});

			TriLogFactory.getInstance().debug( sql );

			List<IViewAssetRegisterEntity> rows = super.query(sql, new ParameterizedRowMapper<IViewAssetRegisterEntity>() {
				@Override
				public IViewAssetRegisterEntity mapRow(ResultSet resultset, int i) throws SQLException {
					return entityMapping(resultset, i);
				}
			});

			if ( TriStringUtils.isEmpty(condition.getMdlNm()) ) {
				return rows;
			} else {
				Set<String> moduleSet = new HashSet<String>();
				for ( String moduleName: condition.getMdlNm() ) {
					moduleSet.add( moduleName );
				}

				List<IViewAssetRegisterEntity> extractRows = new ArrayList<IViewAssetRegisterEntity>();
				for ( IViewAssetRegisterEntity entity: rows ) {
					String moduleName = TriStringUtils.substringBefore( entity.getFilePath() );
					if ( ! moduleSet.contains(moduleName) ) {
						continue;
					}

					extractRows.add( entity );
				}

				return extractRows;
			}

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}

	private StringBuilder setSqlBuilder( StringBuilder builder, List<String> paramList ) {

		for ( String param : paramList ) {
			if ( 0 != builder.length() ) {
				builder.append(" AND ");
			}

			builder.append( param );
		}

		return builder;
	}

	private String getPjtCondition(ViewAssetRegisterCondition condition) {
		StringBuilder builder = new StringBuilder();
		List<String> param = new ArrayList<String>();

		String lotId = SqlFormatUtils.getCondition(condition.getLotId(), "L." + ViewAssetRegisterItems.lotId.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(lotId) ) param.add( lotId );

		String lotNm = SqlFormatUtils.getCondition(condition.getLotNm(), "L." + ViewAssetRegisterItems.lotNm.getItemName(), true, true, false);
		if ( TriStringUtils.isNotEmpty(lotNm) ) param.add( lotNm );

		String pjtId = SqlFormatUtils.getCondition(condition.getPjtId(), "P." + ViewAssetRegisterItems.pjtId.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(pjtId) ) param.add( pjtId );

		String chgFactorNo = SqlFormatUtils.getCondition(condition.getChgFactorNo(), "P." + ViewAssetRegisterItems.chgFactorNo.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(chgFactorNo) ) param.add( chgFactorNo );

		builder = this.setSqlBuilder(builder, param);

		if ( 0 != builder.length() ) {
			builder.append(" AND ");
		}

		return builder.toString();
	}

	private String getAreqCondition(ViewAssetRegisterCondition condition) {
		StringBuilder builder = new StringBuilder();
		List<String> param = new ArrayList<String>();

		String reqUserId = SqlFormatUtils.getCondition(condition.getReqUserId(), "A." + ViewAssetRegisterItems.reqUserId.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(reqUserId) ) param.add( reqUserId );

		String grpNm = SqlFormatUtils.getCondition(condition.getGrpNm(), "A." + ViewAssetRegisterItems.grpNm.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(grpNm) ) param.add( grpNm );

		builder = this.setSqlBuilder(builder, param);

		if ( 0 != builder.length() ) {
			builder.append(" AND ");
		}

		return builder.toString();

	}

	private String getAreqStsCondition(ViewAssetRegisterCondition condition) {
		StringBuilder builder = new StringBuilder();
		List<String> param = new ArrayList<String>();

		String stsId = SqlFormatUtils.getCondition(condition.getStsId(), "G1." + ViewAssetRegisterItems.stsId.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(stsId) ) param.add( stsId );

		String procStsId = SqlFormatUtils.getCondition(condition.getProcStsId(), "G1." + ViewAssetRegisterItems.procStsId.getItemName(), false, true, false);
		if ( TriStringUtils.isNotEmpty(procStsId) ) param.add( procStsId );

		builder = this.setSqlBuilder(builder, param);

		if ( 0 != builder.length() ) {
			builder.append(" AND ");
		}

		return builder.toString();

	}

	private String getAreqFileCondition(ViewAssetRegisterCondition condition) {
		StringBuilder builder = new StringBuilder();
		List<String> param = new ArrayList<String>();

		String mdlNm = SqlFormatUtils.getCondition(condition.getMdlNm(), "F." + ViewAssetRegisterItems.filePath.getItemName(), true, true, false, true, false);
		if ( TriStringUtils.isNotEmpty(mdlNm) ) param.add( mdlNm );

		String filePath = SqlFormatUtils.getCondition(condition.getFilePath(), "F." + ViewAssetRegisterItems.filePath.getItemName(), true, true, false);
		if ( TriStringUtils.isNotEmpty(filePath) ) param.add( filePath );

		builder = this.setSqlBuilder(builder, param);

		if ( 0 != builder.length() ) {
			builder.append(" AND ");
		}

		return builder.toString();

	}

}
