package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * aseet request file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class AreqFileEntity extends EntityFooter implements IAreqFileEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq file sequence number
	 */
	public String areqFileSeqNo = null;
	/**
	 * areq catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * file path
	 */
	public String filePath = null;
	/**
	 * file revesion
	 */
	public String fileRev = null;
	/**
	 * file byte size
	 */
	public Integer fileByteSize = null;
	/**
	 * file update time stamp
	 */
	public Timestamp fileUpdTimestamp = null;
	/**
	 * file status id
	 */
	public String fileStsId = null;

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * areq file sequence numberを取得します。
	 * @return areq file sequence number
	 */
	public String getAreqFileSeqNo() {
	    return areqFileSeqNo;
	}
	/**
	 * areq file sequence numberを設定します。
	 * @param areqFileSeqNo areq file sequence number
	 */
	public void setAreqFileSeqNo(String areqFileSeqNo) {
	    this.areqFileSeqNo = areqFileSeqNo;
	}
	/**
	 * areq catalog codeを取得します。
	 * @return areq catalog code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * areq catalog codeを設定します。
	 * @param areqCtgCd areq catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}
	/**
	 * file revesionを取得します。
	 * @return file revesion
	 */
	public String getFileRev() {
	    return fileRev;
	}
	/**
	 * file revesionを設定します。
	 * @param fileRev file revesion
	 */
	public void setFileRev(String fileRev) {
	    this.fileRev = fileRev;
	}
	/**
	 * file byte sizeを取得します。
	 * @return file byte size
	 */
	public Integer getFileByteSize() {
	    return fileByteSize;
	}
	/**
	 * file byte sizeを設定します。
	 * @param fileBiteSize file byte size
	 */
	public void setFileByteSize(Integer fileByteSize) {
	    this.fileByteSize = fileByteSize;
	}
	/**
	 * file update time stampを取得します。
	 * @return file update time stamp
	 */
	public Timestamp getFileUpdTimestamp() {
	    return fileUpdTimestamp;
	}
	/**
	 * file update time stampを設定します。
	 * @param fileUpdTimestamp file update time stamp
	 */
	public void setFileUpdTimestamp(Timestamp fileUpdTimestamp) {
	    this.fileUpdTimestamp = fileUpdTimestamp;
	}
	@Override
	public String getFileStsId() {
	    return fileStsId;
	}
	@Override
	public void setFileStsId(String fileStsId) {
	    this.fileStsId = fileStsId;
	}

}
