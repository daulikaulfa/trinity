package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the aseet request binary file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAreqBinaryFileEntity extends IEntityFooter {

	public String getAreqId();
	public void setAreqId(String areqId);

	public String getAreqFileSeqNo();
	public void setAreqFileSeqNo(String areqFileSeqNo);

	public String getAreqCtgCd();
	public void setAreqCtgCd(String areqCtgCd);

	public String getFilePath();
	public void setFilePath(String filePath);

}
