package jp.co.blueship.tri.am.dao.areq.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;

/**
 * The items of the asset register entity.
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 */
public enum ViewAssetRegisterItems implements ITableItem {
	filePath("file_path"),
	lotId("lot_id"),
	lotNm("lot_nm"),
	pjtId("pjt_id"),
	chgFactorNo("chg_factor_no"),
	areqId("areq_id"),
	areqCtgCd("areq_ctg_cd"),
	grpId("grp_id"),
	grpNm("grp_nm"),
	assetReqNm("asset_req_nm"),
	content("content"),
	reqUserId("req_user_id"),
	lendReqUserId("lend_req_user_id"),
	lendReqUserNm("lend_req_user_nm"),
	lendReqTimestamp("lend_req_timestamp"),
	rtnReqUserId("rtn_req_user_id"),
	rtnReqUserNm("rtn_req_user_nm"),
	rtnReqTimestamp("rtn_req_timestamp"),
	delReqUserId("del_req_user_id"),
	delReqUserNm("del_req_user_nm"),
	delReqTimestamp("del_req_timestamp"),
	lendFileRev("lend_file_rev"),
	lendFileByteSize("lend_file_byte_size"),
	lendFileUpdTimestamp("lend_file_upd_timestamp"),
	fileRev("file_rev"),
	fileByteSize("file_byte_size"),
	fileUpdTimestamp("file_upd_timestamp"),
	pjtAvlTimestamp("pjt_avl_timestamp"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	;

	private String element = null;

	private ViewAssetRegisterItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
