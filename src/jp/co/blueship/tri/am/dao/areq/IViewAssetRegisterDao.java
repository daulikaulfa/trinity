package jp.co.blueship.tri.am.dao.areq;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterCondition;
import jp.co.blueship.tri.fw.dao.orm.IDao;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;

/**
 * The interface of the asset register view.
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 */
public interface IViewAssetRegisterDao extends IDao {

	/**
	 * 指定された検索条件で検索を行います。
	 *
     * @param condition 検索条件
	 * @return 検索結果を戻します。
	 * @throws TriJdbcDaoException
	 */
	public List<IViewAssetRegisterEntity> find( ViewAssetRegisterCondition condition ) throws TriJdbcDaoException;

}
