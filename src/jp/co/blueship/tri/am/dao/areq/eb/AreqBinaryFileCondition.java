package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.am.dao.areq.constants.AreqBinaryFileItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the attr request binary file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AreqBinaryFileCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_AREQ_BINARY_FILE;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq file sequence number
	 */
	public String areqFileSeqNo = null;
	/**
	 * areq catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * areq catalog code's
	 */
	public String[] areqCtgCds = null;
	/**
	 * file path
	 */
	public String filePath = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
	   super.append(AreqBinaryFileItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public AreqBinaryFileCondition(){
		super(attr);
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(AreqBinaryFileItems.areqId, areqId );
	}

	/**
	 * areq file sequence numberを取得します。
	 * @return areq file sequence number
	 */
	public String getAreqFileSeqNo() {
	    return areqFileSeqNo;
	}
	/**
	 * areq file sequence numberを設定します。
	 * @param areqFileSeqNo areq file sequence number
	 */
	public void setAreqFileSeqNo(String areqFileSeqNo) {
	    this.areqFileSeqNo = areqFileSeqNo;
	    super.append(AreqBinaryFileItems.areqFileSeqNo, areqFileSeqNo );
	}
	/**
	 * areq catalog codeを取得します。
	 * @return areq catalog code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * areq catalog codeを設定します。
	 * @param areqCtgCd areq catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	    super.append(AreqBinaryFileItems.areqCtgCd, areqCtgCd );
	}

	/**
	 * areq catalog code'sを取得します。
	 * @return catalog code's
	 */
	public String[] getAreqCtgCds() {
	    return areqCtgCds;
	}

	/**
	 * areq catalog code'sを設定します。
	 * @param areqCtgCds areq catalog code's
	 */
	public void setAreqCtgCds(String[] areqCtgCds) {
	    this.areqCtgCds = areqCtgCds;
	    super.append( AreqBinaryFileItems.areqCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(areqCtgCds, AreqBinaryFileItems.areqCtgCd.getItemName(), false, true, false) );
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	    super.append(AreqBinaryFileItems.filePath, filePath );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( AreqBinaryFileItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}