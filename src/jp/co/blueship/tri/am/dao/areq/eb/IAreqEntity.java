package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the asset request entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IAreqEntity extends IEntityFooter, IEntityExecData {

	public String getAreqId();
	public void setAreqId(String areqId);

	public String getLotId();

	public String getPjtId();
	public void setPjtId(String pjtId);

	public String getPjtAvlSeqNo();
	public void setPjtAvlSeqNo(String pjtAvlSeqNo);

	public String getAreqCtgCd();
	public void setAreqCtgCd(String areqCtgCd);

	public String getAssetReqNm();
	public void setAssetReqNm(String assetReqNm);

	public String getChgFactorNo();

	public String getGrpId();
	public void setGrpId(String grpId);

	public String getGrpNm();
	public void setGrpNm(String grpNm);

	public String getSummary();
	public void setSummary(String summary);

	public String getContent();
	public void setContent(String content);

	public String getRtnReqDueDate();
	public void setRtnReqDueDate(String dueDate);

	public String getStsId();
	public void setStsId(String stsId);

	public Timestamp getReqTimestamp();

	public String getLendReqUserId();
	public void setLendReqUserId(String lendReqUserId);

	public String getLendReqUserNm();
	public void setLendReqUserNm(String lendReqUserNm);

	public Timestamp getLendReqTimestamp();
	public void setLendReqTimestamp(Timestamp lendReqTimestamp);

	public String getRtnReqUserId();
	public void setRtnReqUserId(String rtnReqUserId);

	public String getRtnReqUserNm();
	public void setRtnReqUserNm(String rtnReqUserNm);

	public Timestamp getRtnReqTimestamp();
	public void setRtnReqTimestamp(Timestamp rtnReqTimestamp);

	public String getDelReqUserId();
	public void setDelReqUserId(String delReqUserId);

	public String getDelReqUserNm();
	public void setDelReqUserNm(String delReqUserNm);

	public Timestamp getDelReqTimestamp();
	public void setDelReqTimestamp(Timestamp delReqTimestamp);

	public String getAssigneeId();
	public void setAssigneeId(String assigneeId);

	public String getAssigneeNm();
	public void setAssigneeNm(String assigneeNm);

	public String getAssigneeIconPath();
	public void setAssigneeIconPath(String assigneeIconPath);

	public Timestamp getPjtAvlTimestamp();

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public String getPjtSubject();
	public String getProcStsId();

	public String getCtgId();
	public String getCtgNm();
	public String getMstoneId();
	public String getMstoneNm();
}
