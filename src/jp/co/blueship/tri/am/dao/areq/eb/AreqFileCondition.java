package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.areq.constants.AreqFileItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the asset request file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class AreqFileCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_AREQ_FILE;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq file sequence number
	 */
	public String areqFileSeqNo = null;
	/**
	 * areq catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * areq catalog code's
	 */
	public String[] areqCtgCds = null;
	/**
	 * file path
	 */
	public String filePath = null;
	/**
	 * file revesion
	 */
	public String fileRev = null;
	/**
	 * file byte size
	 */
	public String fileByteSize = null;
	/**
	 * file update time stamp
	 */
	public Timestamp fileUpdTimestamp = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(AreqFileItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * file status
	 */
	public String fileStsId = null;

	public AreqFileCondition(){
		super(attr);
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(AreqFileItems.areqId, areqId );
	}

	/**
	 * areq file sequence numberを取得します。
	 * @return areq file sequence number
	 */
	public String getAreqFileSeqNo() {
	    return areqFileSeqNo;
	}
	/**
	 * areq file sequence numberを設定します。
	 * @param areqFileSeqNo areq file sequence number
	 */
	public void setAreqFileSeqNo(String areqFileSeqNo) {
	    this.areqFileSeqNo = areqFileSeqNo;
	    super.append(AreqFileItems.areqFileSeqNo, areqFileSeqNo );
	}
	/**
	 * areq catalog codeを取得します。
	 * @return catalog code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * areq catalog codeを設定します。
	 * @param areqCtgCd areq catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	    super.append(AreqFileItems.areqCtgCd, areqCtgCd );
	}

	/**
	 * areq catalog code'sを取得します。
	 * @return catalog code's
	 */
	public String[] getAreqCtgCds() {
	    return areqCtgCds;
	}

	/**
	 * areq catalog code'sを設定します。
	 * @param areqCtgCds areq catalog code's
	 */
	public void setAreqCtgCds(String[] areqCtgCds) {
	    this.areqCtgCds = areqCtgCds;
	    super.append( AreqFileItems.areqCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(areqCtgCds, AreqFileItems.areqCtgCd.getItemName(), false, true, false) );
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	    super.append(AreqFileItems.filePath, filePath );
	}
	/**
	 * file revesionを取得します。
	 * @return file revesion
	 */
	public String getFileRev() {
	    return fileRev;
	}
	/**
	 * file revesionを設定します。
	 * @param fileRev file revesion
	 */
	public void setFileRev(String fileRev) {
	    this.fileRev = fileRev;
	    super.append(AreqFileItems.fileRev, fileRev );
	}
	/**
	 * file byte sizeを取得します。
	 * @return file byte size
	 */
	public String getFileByteSize() {
	    return fileByteSize;
	}
	/**
	 * file byte sizeを設定します。
	 * @param fileBiteSize file byte size
	 */
	public void setFileByteSize(String fileByteSize) {
	    this.fileByteSize = fileByteSize;
	    super.append(AreqFileItems.fileByteSize, fileByteSize );
	}
	/**
	 * file update time stampを取得します。
	 * @return file update time stamp
	 */
	public Timestamp getFileUpdTimestamp() {
	    return fileUpdTimestamp;
	}
	/**
	 * file update time stampを設定します。
	 * @param fileUpdTimestamp file update time stamp
	 */
	public void setFileUpdTimestamp(Timestamp fileUpdTimestamp) {
	    this.fileUpdTimestamp = fileUpdTimestamp;
	    super.append(AreqFileItems.fileUpdTimestamp, fileUpdTimestamp );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( AreqFileItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * file status Id
	 * @return fileStsId
	 */
	public String getFileStsId() {
		return fileStsId;
	}

	/**
	 * file status Id
	 * @param fileStsId file status id
	 */
	public void setFileStsId(String fileStsId) {
		this.fileStsId = fileStsId;
		super.append(AreqFileItems.fileStsId, fileStsId);
	}
}