package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

/**
 * asset register view entity.
 *
 * @version V3L11.01
 * @author YUkihiro Eguchi
 *
 */
public class ViewAssetRegisterEntity implements IViewAssetRegisterEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Module Name
	 */
	public String mdlNm = null;
	/**
	 * Resource Path
	 */
	public String filePath = null;
	/**
	 * Lot Id
	 */
	public String lotId = null;
	/**
	 * Lot Name
	 */
	public String lotNm = null;
	/**
	 * Change Property
	 */
	public String pjtId = null;
	/**
	 * Change Factor No.
	 */
	public String chgFactorNo = null;
	/**
	 * Resource Inquiry Id
	 */
	public String areqId = null;
	/**
	 * Resource Category Code
	 */
	public String areqCtgCd = null;
	/**
	 * Group Id
	 */
	public String grpId = null;
	/**
	 * Group Name
	 */
	public String grpNm = null;
	/**
	 * Resource Inquiry Subject
	 */
	public String assetReqNm = null;
	/**
	 * Content
	 */
	public String content = null;
	/**
	 * Checkout Inquiry User Id
	 */
	public String lendReqUserId = null;
	/**
	 * Checkout Inquiry User Name
	 */
	public String lendReqUserNm = null;
	/**
	 * Checkout Inquiry Timestamp
	 */
	public Timestamp lendReqTimestamp = null;
	/**
	 * Check-in Inquiry User Id
	 */
	public String rtnReqUserId = null;
	/**
	 * Check-in Inquiry User Name
	 */
	public String rtnReqUserNm = null;
	/**
	 * Check-in Inquiry Timestamp
	 */
	public Timestamp rtnReqTimestamp = null;
	/**
	 * Removal Inquiry User Id
	 */
	public String delReqUserId = null;
	/**
	 * Removal Inquiry User Name
	 */
	public String delReqUserNm = null;
	/**
	 * Removal Inquiry Timestamp
	 */
	public Timestamp delReqTimestamp = null;
	/**
	 * Checkout Resource Revision
	 */
	public String lendFileRev = null;
	/**
	 * Checkout Resource Byte Size
	 */
	public Integer lendFileByteSize = null;
	/**
	 * Checkout Resource Timestamp
	 */
	public Timestamp lendFileUpdTimestamp = null;
	/**
	 * Check-in Resource Revision
	 */
	public String rtnFileRev = null;
	/**
	 * Check-in Resource Byte Size
	 */
	public Integer rtnFileByteSize = null;
	/**
	 * Check-in Resource Timestamp
	 */
	public Timestamp rtnFileUpdTimestamp = null;
	/**
	 * Removal Resource Revision
	 */
	public String delFileRev = null;
	/**
	 * Removal Resource Byte Size
	 */
	public Integer delFileByteSize = null;
	/**
	 * Removal Resource Timestamp
	 */
	public Timestamp delFileUpdTimestamp = null;
	/**
	 * Change Property Approval Timestamp
	 */
	public Timestamp pjtAvlTimestamp = null;
	/**
	 * Status Id
	 */
	public String stsId = null;
	/**
	 * Process Status Id
	 */
	public String procStsId = null;

	/**
	 * Module Nameを取得します。
	 * @return Module Name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * Module Nameを設定します。
	 * @param mdlNm Module Name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	/**
	 * Resource Pathを取得します。
	 * @return Resource Path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * Resource Pathを設定します。
	 * @param filePath Resource Path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}
	/**
	 * Lot Idを取得します。
	 * @return Lot Id
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * Lot Idを設定します。
	 * @param lotId Lot Id
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * Lot Nameを取得します。
	 * @return Lot Name
	 */
	public String getLotNm() {
	    return lotNm;
	}
	/**
	 * Lot Nameを設定します。
	 * @param lotNm Lot Name
	 */
	public void setLotNm(String lotNm) {
	    this.lotNm = lotNm;
	}
	/**
	 * Change Propertyを取得します。
	 * @return Change Property
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * Change Propertyを設定します。
	 * @param pjtId Change Property
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * Change Factor No.を取得します。
	 * @return Change Factor No.
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}
	/**
	 * Change Factor No.を設定します。
	 * @param chgFactorNo Change Factor No.
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	}
	/**
	 * Resource Inquiry Idを取得します。
	 * @return Resource Inquiry Id
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * Resource Inquiry Idを設定します。
	 * @param areqId Resource Inquiry Id
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * Resource Category Codeを取得します。
	 * @return Resource Category Code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * Resource Category Codeを設定します。
	 * @param areqCtgCd Resource Category Code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	}
	/**
	 * Group Idを取得します。
	 * @return Group Id
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * Group Idを設定します。
	 * @param grpId Group Id
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	}
	/**
	 * Group Nameを取得します。
	 * @return Group Name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * Group Nameを設定します。
	 * @param grpNm Group Name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	}
	/**
	 * Resource Inquiry Subjectを取得します。
	 * @return Resource Inquiry Subject
	 */
	public String getAssetReqNm() {
	    return assetReqNm;
	}
	/**
	 * Resource Inquiry Subjectを設定します。
	 * @param assetReqNm Resource Inquiry Subject
	 */
	public void setAssetReqNm(String assetReqNm) {
	    this.assetReqNm = assetReqNm;
	}
	/**
	 * Contentを取得します。
	 * @return Content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * Contentを設定します。
	 * @param content Content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * Checkout Inquiry User Idを取得します。
	 * @return Checkout Inquiry User Id
	 */
	public String getLendReqUserId() {
	    return lendReqUserId;
	}
	/**
	 * Checkout Inquiry User Idを設定します。
	 * @param lendReqUserId Checkout Inquiry User Id
	 */
	public void setLendReqUserId(String lendReqUserId) {
	    this.lendReqUserId = lendReqUserId;
	}
	/**
	 * Checkout Inquiry User Nameを取得します。
	 * @return Checkout Inquiry User Name
	 */
	public String getLendReqUserNm() {
	    return lendReqUserNm;
	}
	/**
	 * Checkout Inquiry User Nameを設定します。
	 * @param lendReqUserNm Checkout Inquiry User Name
	 */
	public void setLendReqUserNm(String lendReqUserNm) {
	    this.lendReqUserNm = lendReqUserNm;
	}
	/**
	 * Checkout Inquiry Timestampを取得します。
	 * @return Checkout Inquiry Timestamp
	 */
	public Timestamp getLendReqTimestamp() {
	    return lendReqTimestamp;
	}
	/**
	 * Checkout Inquiry Timestampを設定します。
	 * @param lendReqTimestamp Checkout Inquiry Timestamp
	 */
	public void setLendReqTimestamp(Timestamp lendReqTimestamp) {
	    this.lendReqTimestamp = lendReqTimestamp;
	}
	/**
	 * Check-in Inquiry User Idを取得します。
	 * @return Check-in Inquiry User Id
	 */
	public String getRtnReqUserId() {
	    return rtnReqUserId;
	}
	/**
	 * Check-in Inquiry User Idを設定します。
	 * @param rtnReqUserId Check-in Inquiry User Id
	 */
	public void setRtnReqUserId(String rtnReqUserId) {
	    this.rtnReqUserId = rtnReqUserId;
	}
	/**
	 * Check-in Inquiry User Nameを取得します。
	 * @return Check-in Inquiry User Name
	 */
	public String getRtnReqUserNm() {
	    return rtnReqUserNm;
	}
	/**
	 * Check-in Inquiry User Nameを設定します。
	 * @param rtnReqUserNm Check-in Inquiry User Name
	 */
	public void setRtnReqUserNm(String rtnReqUserNm) {
	    this.rtnReqUserNm = rtnReqUserNm;
	}
	/**
	 * Check-in Inquiry Timestampを取得します。
	 * @return Check-in Inquiry Timestamp
	 */
	public Timestamp getRtnReqTimestamp() {
	    return rtnReqTimestamp;
	}
	/**
	 * Check-in Inquiry Timestampを設定します。
	 * @param rtnReqTimestamp Check-in Inquiry Timestamp
	 */
	public void setRtnReqTimestamp(Timestamp rtnReqTimestamp) {
	    this.rtnReqTimestamp = rtnReqTimestamp;
	}
	/**
	 * Removal Inquiry User Idを取得します。
	 * @return Removal Inquiry User Id
	 */
	public String getDelReqUserId() {
	    return delReqUserId;
	}
	/**
	 * Removal Inquiry User Idを設定します。
	 * @param delReqUserId Removal Inquiry User Id
	 */
	public void setDelReqUserId(String delReqUserId) {
	    this.delReqUserId = delReqUserId;
	}
	/**
	 * Removal Inquiry User Nameを取得します。
	 * @return Removal Inquiry User Name
	 */
	public String getDelReqUserNm() {
	    return delReqUserNm;
	}
	/**
	 * Removal Inquiry User Nameを設定します。
	 * @param delReqUserNm Removal Inquiry User Name
	 */
	public void setDelReqUserNm(String delReqUserNm) {
	    this.delReqUserNm = delReqUserNm;
	}
	/**
	 * Removal Inquiry Timestampを取得します。
	 * @return Removal Inquiry Timestamp
	 */
	public Timestamp getDelReqTimestamp() {
	    return delReqTimestamp;
	}
	/**
	 * Removal Inquiry Timestampを設定します。
	 * @param delReqTimestamp Removal Inquiry Timestamp
	 */
	public void setDelReqTimestamp(Timestamp delReqTimestamp) {
	    this.delReqTimestamp = delReqTimestamp;
	}
	/**
	 * Checkout Resource Revisionを取得します。
	 * @return Checkout Resource Revision
	 */
	public String getLendFileRev() {
	    return lendFileRev;
	}
	/**
	 * Checkout Resource Revisionを設定します。
	 * @param lendFileRev Checkout Resource Revision
	 */
	public void setLendFileRev(String lendFileRev) {
	    this.lendFileRev = lendFileRev;
	}
	/**
	 * Checkout Resource Byte Sizeを取得します。
	 * @return Checkout Resource Byte Size
	 */
	public Integer getLendFileByteSize() {
	    return lendFileByteSize;
	}
	/**
	 * Checkout Resource Byte Sizeを設定します。
	 * @param lendFileByteSize Checkout Resource Byte Size
	 */
	public void setLendFileByteSize(Integer lendFileByteSize) {
	    this.lendFileByteSize = lendFileByteSize;
	}
	/**
	 * Checkout Resource Timestampを取得します。
	 * @return Checkout Resource Timestamp
	 */
	public Timestamp getLendFileUpdTimestamp() {
	    return lendFileUpdTimestamp;
	}
	/**
	 * Checkout Resource Timestampを設定します。
	 * @param lendFileUpdTimestamp Checkout Resource Timestamp
	 */
	public void setLendFileUpdTimestamp(Timestamp lendFileUpdTimestamp) {
	    this.lendFileUpdTimestamp = lendFileUpdTimestamp;
	}
	/**
	 * Check-in Resource Revisionを取得します。
	 * @return Check-in Resource Revision
	 */
	public String getRtnFileRev() {
	    return rtnFileRev;
	}
	/**
	 * Check-in Resource Revisionを設定します。
	 * @param rtnFileRev Check-in Resource Revision
	 */
	public void setRtnFileRev(String rtnFileRev) {
	    this.rtnFileRev = rtnFileRev;
	}
	/**
	 * Check-in Resource Byte Sizeを取得します。
	 * @return Check-in Resource Byte Size
	 */
	public Integer getRtnFileByteSize() {
	    return rtnFileByteSize;
	}
	/**
	 * Check-in Resource Byte Sizeを設定します。
	 * @param rtnFileByteSize Check-in Resource Byte Size
	 */
	public void setRtnFileByteSize(Integer rtnFileByteSize) {
	    this.rtnFileByteSize = rtnFileByteSize;
	}
	/**
	 * Check-in Resource Timestampを取得します。
	 * @return Check-in Resource Timestamp
	 */
	public Timestamp getRtnFileUpdTimestamp() {
	    return rtnFileUpdTimestamp;
	}
	/**
	 * Check-in Resource Timestampを設定します。
	 * @param rtnFileUpdTimestamp Check-in Resource Timestamp
	 */
	public void setRtnFileUpdTimestamp(Timestamp rtnFileUpdTimestamp) {
	    this.rtnFileUpdTimestamp = rtnFileUpdTimestamp;
	}
	/**
	 * Removal Resource Revisionを取得します。
	 * @return Removal Resource Revision
	 */
	public String getDelFileRev() {
	    return delFileRev;
	}
	/**
	 * Removal Resource Revisionを設定します。
	 * @param delFileRev Removal Resource Revision
	 */
	public void setDelFileRev(String delFileRev) {
	    this.delFileRev = delFileRev;
	}
	/**
	 * Removal Resource Byte Sizeを取得します。
	 * @return Removal Resource Byte Size
	 */
	public Integer getDelFileByteSize() {
	    return delFileByteSize;
	}
	/**
	 * Removal Resource Byte Sizeを設定します。
	 * @param delFileByteSize Removal Resource Byte Size
	 */
	public void setDelFileByteSize(Integer delFileByteSize) {
	    this.delFileByteSize = delFileByteSize;
	}
	/**
	 * Removal Resource Timestampを取得します。
	 * @return Removal Resource Timestamp
	 */
	public Timestamp getDelFileUpdTimestamp() {
	    return delFileUpdTimestamp;
	}
	/**
	 * Removal Resource Timestampを設定します。
	 * @param delFileUpdTimestamp Removal Resource Timestamp
	 */
	public void setDelFileUpdTimestamp(Timestamp delFileUpdTimestamp) {
	    this.delFileUpdTimestamp = delFileUpdTimestamp;
	}
	/**
	 * Change Property Approval Timestampを取得します。
	 * @return Change Property Approval Timestamp
	 */
	public Timestamp getPjtAvlTimestamp() {
	    return pjtAvlTimestamp;
	}
	/**
	 * Change Property Approval Timestampを設定します。
	 * @param pjtAvlTimestamp Change Property Approval Timestamp
	 */
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp) {
	    this.pjtAvlTimestamp = pjtAvlTimestamp;
	}
	/**
	 * Status Idを取得します。
	 * @return Status Id
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * Status Idを設定します。
	 * @param stsId Status Id
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * Process Status Idを取得します。
	 * @return Process Status Id
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * Process Status Idを設定します。
	 * @param procStsId Process Status Id
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}


}
