package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the aseet request attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAreqAttachedFileEntity extends IEntityFooter {

	public String getAreqId();
	public void setAreqId(String areqId);

	public String getAttachedFileSeqNo();
	public void setAttachedFileSeqNo(String attachedFileSeqNo);

	public String getFilePath();
	public void setFilePath(String filePath);

}
