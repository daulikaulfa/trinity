package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * aseet request attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AreqAttachedFileEntity extends EntityFooter implements IAreqAttachedFileEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * attached file sequence number
	 */
	public String attachedFileSeqNo = null;
	/**
	 * file path
	 */
	public String filePath = null;

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * attached file sequence numberを取得します。
	 * @return attached file sequence number
	 */
	public String getAttachedFileSeqNo() {
	    return attachedFileSeqNo;
	}
	/**
	 * attached file sequence numberを設定します。
	 * @param attachedFileSeqNo attached file sequence number
	 */
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
	    this.attachedFileSeqNo = attachedFileSeqNo;
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}

}
