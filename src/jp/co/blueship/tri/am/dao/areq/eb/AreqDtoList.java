package jp.co.blueship.tri.am.dao.areq.eb;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Extractで安全にAreqDtoのListを取り出すために作成されたクラスです。
 * @author Takashi ono
 *
 */
public class AreqDtoList extends ArrayList<IAreqDto> {

	/**
	 * 修正後はインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	public AreqDtoList(){

	}
	public AreqDtoList(Collection<IAreqDto> c ){
		super(c);
	}

}
