package jp.co.blueship.tri.am.dao.areq.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the asset request attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum AreqAttachedFileItems implements ITableItem {
	areqId("areq_id"),
	attachedFileSeqNo("attached_file_seq_no"),
	filePath("file_path"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private AreqAttachedFileItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
