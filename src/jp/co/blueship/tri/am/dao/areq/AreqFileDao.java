package jp.co.blueship.tri.am.dao.areq;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.areq.constants.AreqFileItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the asset request attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class AreqFileDao extends JdbcBaseDao<IAreqFileEntity> implements IAreqFileDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_AREQ_FILE;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IAreqFileEntity entity ) {
		builder
			.append(AreqFileItems.areqId, entity.getAreqId(), true)
			.append(AreqFileItems.areqCtgCd, entity.getAreqCtgCd(), true)
			.append(AreqFileItems.areqFileSeqNo, entity.getAreqFileSeqNo(), true)
			.append(AreqFileItems.filePath, entity.getFilePath())
			.append(AreqFileItems.fileRev, entity.getFileRev())
			.append(AreqFileItems.fileByteSize, entity.getFileByteSize())
			.append(AreqFileItems.fileUpdTimestamp, entity.getFileUpdTimestamp())
			.append(AreqFileItems.fileStsId, entity.getFileStsId())
			.append(AreqFileItems.delStsId, (null == entity.getDelStsId())? null :entity.getDelStsId().parseBoolean())
			.append(AreqFileItems.regTimestamp, entity.getRegTimestamp())
			.append(AreqFileItems.regUserId, entity.getRegUserId())
			.append(AreqFileItems.regUserNm, entity.getRegUserNm())
			.append(AreqFileItems.updTimestamp, entity.getUpdTimestamp())
			.append(AreqFileItems.updUserId, entity.getUpdUserId())
			.append(AreqFileItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IAreqFileEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IAreqFileEntity entity = new AreqFileEntity();

  	  entity.setAreqId( rs.getString(AreqFileItems.areqId.getItemName()) );
  	  entity.setAreqCtgCd( rs.getString(AreqFileItems.areqCtgCd.getItemName()) );
  	  entity.setAreqFileSeqNo( rs.getString(AreqFileItems.areqFileSeqNo.getItemName()) );
  	  entity.setFilePath( rs.getString(AreqFileItems.filePath.getItemName()) );
  	  entity.setFileRev( rs.getString(AreqFileItems.fileRev.getItemName()) );
  	  entity.setFileByteSize( rs.getInt(AreqFileItems.fileByteSize.getItemName()) );
  	  entity.setFileUpdTimestamp( rs.getTimestamp(AreqFileItems.fileUpdTimestamp.getItemName()) );
  	  entity.setFileStsId( rs.getString(AreqFileItems.fileStsId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(AreqFileItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(AreqFileItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(AreqFileItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(AreqFileItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(AreqFileItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(AreqFileItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(AreqFileItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
