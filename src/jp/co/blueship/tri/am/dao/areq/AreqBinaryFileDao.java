package jp.co.blueship.tri.am.dao.areq;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.areq.constants.AreqBinaryFileItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the asset request attached binary file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class AreqBinaryFileDao extends JdbcBaseDao<IAreqBinaryFileEntity> implements IAreqBinaryFileDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_AREQ_BINARY_FILE;
	}

	@Override
	protected final ISqlBuilder append(ISqlBuilder builder, IAreqBinaryFileEntity entity) {
		builder.append(AreqBinaryFileItems.areqId, entity.getAreqId(), true).append(AreqBinaryFileItems.areqCtgCd, entity.getAreqCtgCd(), true)
				.append(AreqBinaryFileItems.areqFileSeqNo, entity.getAreqFileSeqNo(), true)
				.append(AreqBinaryFileItems.filePath, entity.getFilePath())
				.append(AreqBinaryFileItems.delStsId, (null == entity.getDelStsId())? null :entity.getDelStsId().parseBoolean())
				.append(AreqBinaryFileItems.regTimestamp, entity.getRegTimestamp()).append(AreqBinaryFileItems.regUserId, entity.getRegUserId())
				.append(AreqBinaryFileItems.regUserNm, entity.getRegUserNm()).append(AreqBinaryFileItems.updTimestamp, entity.getUpdTimestamp())
				.append(AreqBinaryFileItems.updUserId, entity.getUpdUserId()).append(AreqBinaryFileItems.updUserNm, entity.getUpdUserNm());

		return builder;
	}

	@Override
	protected final IAreqBinaryFileEntity entityMapping(ResultSet rs, int row) throws SQLException {
		IAreqBinaryFileEntity entity = new AreqBinaryFileEntity();

		entity.setAreqId(rs.getString(AreqBinaryFileItems.areqId.getItemName()));
		entity.setAreqCtgCd(rs.getString(AreqBinaryFileItems.areqCtgCd.getItemName()));
		entity.setAreqFileSeqNo(rs.getString(AreqBinaryFileItems.areqFileSeqNo.getItemName()));
		entity.setFilePath(rs.getString(AreqBinaryFileItems.filePath.getItemName()));
		entity.setDelStsId(StatusFlg.value(rs.getBoolean(AreqBinaryFileItems.delStsId.getItemName())));
		entity.setRegTimestamp(rs.getTimestamp(AreqBinaryFileItems.regTimestamp.getItemName()));
		entity.setRegUserId(rs.getString(AreqBinaryFileItems.regUserId.getItemName()));
		entity.setRegUserNm(rs.getString(AreqBinaryFileItems.regUserNm.getItemName()));
		entity.setUpdTimestamp(rs.getTimestamp(AreqBinaryFileItems.updTimestamp.getItemName()));
		entity.setUpdUserId(rs.getString(AreqBinaryFileItems.updUserId.getItemName()));
		entity.setUpdUserNm(rs.getString(AreqBinaryFileItems.updUserNm.getItemName()));

		return entity;
	}

}
