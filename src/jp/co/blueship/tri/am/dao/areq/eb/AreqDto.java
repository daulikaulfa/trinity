package jp.co.blueship.tri.am.dao.areq.eb;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;



/**
 * asset request DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AreqDto implements IAreqDto {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 資産申請
	 */
	public IAreqEntity areqEntity = null;

	/**
	 * 資産申請ファイル
	 */
	public List<IAreqFileEntity> areqFileEntities = new ArrayList<IAreqFileEntity>();

	/**
	 * 資産申請バイナリファイル
	 */
	public List<IAreqBinaryFileEntity> areqBinaryFileEntities = new ArrayList<IAreqBinaryFileEntity>();

	/**
	 * 資産申請添付ファイル
	 */
	public List<IAreqAttachedFileEntity> areqAttachedFileEntities = new ArrayList<IAreqAttachedFileEntity>();

	@Override
	public List<IAreqFileEntity> getAreqFileEntities(AreqCtgCd... ctgCd) {

		List<IAreqFileEntity> result = new ArrayList<IAreqFileEntity>();

		Set<String> ctgSet = new HashSet<String>();
		ctgSet.addAll( FluentList.from(ctgCd).map( AmFluentFunctionUtils.toValueByAreqCtgCd ).asList() );

		for ( IAreqFileEntity entity: this.getAreqFileEntities() ) {
			if ( ! ctgSet.contains( entity.getAreqCtgCd() ) )
				continue;

			result.add( entity );
		}

		return result;
	}

	@Override
	public List<IAreqBinaryFileEntity> getAreqBinaryFileEntities(AreqCtgCd... ctgCd) {

		List<IAreqBinaryFileEntity> result = new ArrayList<IAreqBinaryFileEntity>();

		Set<String> ctgSet = new HashSet<String>();
		ctgSet.addAll( FluentList.from(ctgCd).map( AmFluentFunctionUtils.toValueByAreqCtgCd ).asList() );

		for ( IAreqBinaryFileEntity entity: this.getAreqBinaryFileEntities() ) {
			if ( ! ctgSet.contains( entity.getAreqCtgCd() ) )
				continue;

			result.add( entity );
		}

		return result;

	}

	/**
	 * 資産申請を取得します。
	 * @return 資産申請
	 */
	public IAreqEntity getAreqEntity() {
	    return areqEntity;
	}

	/**
	 * 資産申請を設定します。
	 * @param areqEntity 資産申請
	 */
	public void setAreqEntity(IAreqEntity areqEntity) {
	    this.areqEntity = areqEntity;
	}

	/**
	 * 資産申請ファイルを取得します。
	 * @return 資産申請ファイル
	 */
	public List<IAreqFileEntity> getAreqFileEntities() {
	    return areqFileEntities;
	}

	/**
	 * 資産申請ファイルを設定します。
	 * @param areqFileEntities 資産申請ファイル
	 */
	public void setAreqFileEntities(List<IAreqFileEntity> areqFileEntities) {
	    this.areqFileEntities = areqFileEntities;
	}

	/**
	 * 資産申請バイナリファイルを取得します。
	 * @return 資産申請バイナリファイル
	 */
	public List<IAreqBinaryFileEntity> getAreqBinaryFileEntities() {
	    return areqBinaryFileEntities;
	}

	/**
	 * 資産申請バイナリファイルを設定します。
	 * @param areqBinaryFileEntities 資産申請バイナリファイル
	 */
	public void setAreqBinaryFileEntities(List<IAreqBinaryFileEntity> areqBinaryFileEntities) {
	    this.areqBinaryFileEntities = areqBinaryFileEntities;
	}

	/**
	 * 資産申請添付ファイルを取得します。
	 * @return 資産申請添付ファイル
	 */
	public List<IAreqAttachedFileEntity> getAreqAttachedFileEntities() {
	    return areqAttachedFileEntities;
	}

	/**
	 * 資産申請添付ファイルを設定します。
	 * @param areqAttachedFileEntities 資産申請添付ファイル
	 */
	public void setAreqAttachedFileEntities(List<IAreqAttachedFileEntity> areqAttachedFileEntities) {
	    this.areqAttachedFileEntities = areqAttachedFileEntities;
	}

}
