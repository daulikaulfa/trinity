package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * aseet request binary file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AreqBinaryFileEntity extends EntityFooter implements IAreqBinaryFileEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq file sequence number
	 */
	public String areqFileSeqNo = null;
	/**
	 * areq catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * file path
	 */
	public String filePath = null;

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * areq file sequence numberを取得します。
	 * @return areq file sequence number
	 */
	public String getAreqFileSeqNo() {
	    return areqFileSeqNo;
	}
	/**
	 * areq file sequence numberを設定します。
	 * @param areqFileSeqNo areq file sequence number
	 */
	public void setAreqFileSeqNo(String areqFileSeqNo) {
	    this.areqFileSeqNo = areqFileSeqNo;
	}
	/**
	 * areq catalog codeを取得します。
	 * @return areq catalog code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * areq catalog codeを設定します。
	 * @param areqCtgCd areq catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}

}
