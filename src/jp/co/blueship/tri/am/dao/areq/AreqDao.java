package jp.co.blueship.tri.am.dao.areq;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The implements of the asset request DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class AreqDao extends JdbcBaseDao<IAreqEntity> implements IAreqDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_AREQ;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IAreqEntity entity ) {
		builder
			.append(AreqItems.areqId, entity.getAreqId(), true)
			.append(AreqItems.pjtId, entity.getPjtId())
			.append(AreqItems.pjtAvlSeqNo, entity.getPjtAvlSeqNo())
			.append(AreqItems.areqCtgCd, entity.getAreqCtgCd())
			.append(AreqItems.assetReqNm, entity.getAssetReqNm())
			.append(AreqItems.grpId, entity.getGrpId())
			.append(AreqItems.grpNm, entity.getGrpNm())
			.append(AreqItems.summary, entity.getSummary())
			.append(AreqItems.content, entity.getContent())
			.append(AreqItems.stsId, entity.getStsId())
			.append(AreqItems.lendReqUserId, entity.getLendReqUserId())
			.append(AreqItems.lendReqUserNm, entity.getLendReqUserNm())
			.append(AreqItems.lendReqTimestamp, entity.getLendReqTimestamp())
			.append(AreqItems.rtnReqUserId, entity.getRtnReqUserId())
			.append(AreqItems.rtnReqUserNm, entity.getRtnReqUserNm())
			.append(AreqItems.rtnReqTimestamp, entity.getRtnReqTimestamp())
			.append(AreqItems.rtnReqDueDate, entity.getRtnReqDueDate())
			.append(AreqItems.delReqUserId, entity.getDelReqUserId())
			.append(AreqItems.delReqUserNm, entity.getDelReqUserNm())
			.append(AreqItems.delReqTimestamp, entity.getDelReqTimestamp())
			.append(AreqItems.delUserId, entity.getDelUserId())
			.append(AreqItems.delUserNm, entity.getDelUserNm())
			.append(AreqItems.delTimestamp, entity.getDelTimestamp())
			.append(AreqItems.delCmt, entity.getDelCmt())
			.append(AreqItems.delStsId, (null == entity.getDelStsId())? null :entity.getDelStsId().parseBoolean())
			.append(AreqItems.regTimestamp, entity.getRegTimestamp())
			.append(AreqItems.regUserId, entity.getRegUserId())
			.append(AreqItems.regUserNm, entity.getRegUserNm())
			.append(AreqItems.updTimestamp, entity.getUpdTimestamp())
			.append(AreqItems.updUserId, entity.getUpdUserId())
			.append(AreqItems.updUserNm, entity.getUpdUserNm())
			.append(AreqItems.rtnReqDueDate, entity.getRtnReqDueDate())
			.append(AreqItems.assigneeId, entity.getAssigneeId())
			.append(AreqItems.assigneedNm, entity.getAssigneeNm())
			;


		return builder;
	}

	@Override
	protected final IAreqEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		AreqEntity entity = new AreqEntity();

		entity.setAreqId( rs.getString(AreqItems.areqId.getItemName()) );
		entity.setLotId( rs.getString(AreqItems.lotId.getItemName()) );
		entity.setPjtId( rs.getString(AreqItems.pjtId.getItemName()) );
		entity.setPjtAvlSeqNo( rs.getString(AreqItems.pjtAvlSeqNo.getItemName()) );
		entity.setAreqCtgCd( rs.getString(AreqItems.areqCtgCd.getItemName()) );
		entity.setAssetReqNm( rs.getString(AreqItems.assetReqNm.getItemName()) );
		entity.setChgFactorNo( rs.getString(AreqItems.chgFactorNo.getItemName()) );
		entity.setGrpId( rs.getString(AreqItems.grpId.getItemName()) );
		entity.setGrpNm( rs.getString(AreqItems.grpNm.getItemName()) );
		entity.setSummary( rs.getString(AreqItems.summary.getItemName()) );
		entity.setContent( rs.getString(AreqItems.content.getItemName()) );
		entity.setStsId( rs.getString(AreqItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setReqTimestamp( rs.getTimestamp(AreqItems.reqTimestamp.getItemName()) );
		entity.setLendReqUserId( rs.getString(AreqItems.lendReqUserId.getItemName()) );
		entity.setLendReqUserNm( rs.getString(AreqItems.lendReqUserNm.getItemName()) );
		entity.setLendReqTimestamp( rs.getTimestamp(AreqItems.lendReqTimestamp.getItemName()) );
		entity.setRtnReqUserId( rs.getString(AreqItems.rtnReqUserId.getItemName()) );
		entity.setRtnReqUserNm( rs.getString(AreqItems.rtnReqUserNm.getItemName()) );
		entity.setRtnReqTimestamp( rs.getTimestamp(AreqItems.rtnReqTimestamp.getItemName()) );
		entity.setRtnReqDueDate( rs.getString(AreqItems.rtnReqDueDate.getItemName()) );
		entity.setDelReqUserId( rs.getString(AreqItems.delReqUserId.getItemName()) );
		entity.setDelReqUserNm( rs.getString(AreqItems.delReqUserNm.getItemName()) );
		entity.setDelReqTimestamp( rs.getTimestamp(AreqItems.delReqTimestamp.getItemName()) );
		entity.setPjtAvlTimestamp( rs.getTimestamp(AreqItems.pjtAvlTimestamp.getItemName()) );
		entity.setDelUserId( rs.getString(AreqItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(AreqItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(AreqItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(AreqItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(AreqItems.delStsId.getItemName())) );

		entity.setPjtSubject( rs.getString(AreqItems.pjtSubject.getItemName()) );

		entity.setRegTimestamp( rs.getTimestamp(AreqItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(AreqItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(AreqItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(AreqItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(AreqItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(AreqItems.updUserNm.getItemName()) );

		entity.setRtnReqDueDate(rs.getString(AreqItems.rtnReqDueDate.getItemName()));
		entity.setAssigneeId(rs.getString(AreqItems.assigneeId.getItemName()));
		entity.setAssigneeNm(rs.getString(AreqItems.assigneedNm.getItemName()));
		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );
		
		entity.setAssigneeIconPath( rs.getString( AreqItems.assigneeIconPath.getItemName() ) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,P.LOT_ID")
				.append("  ,P.CHG_FACTOR_NO")
				.append("  ,P.P_SUMMARY")
				.append("  ,AV.PJT_AVL_TIMESTAMP")
				.append("  ,(CASE WHEN AREQ_CTG_CD = '" + AreqCtgCd.ReturningRequest.value() + "' THEN RTN_REQ_TIMESTAMP")
				.append("         WHEN AREQ_CTG_CD = '" + AreqCtgCd.RemovalRequest.value() + "' THEN DEL_REQ_TIMESTAMP")
				.append("      ELSE LEND_REQ_TIMESTAMP END) AS REQ_TIMESTAMP")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT LOT_ID, CHG_FACTOR_NO, PJT_ID AS P_PJT_ID, SUMMARY AS P_SUMMARY FROM AM_PJT) P ON PJT_ID = P.P_PJT_ID")
				.append(" LEFT JOIN (SELECT PJT_ID AS AV_PJT_ID, PJT_AVL_SEQ_NO AS AV_PJT_AVL_SEQ_NO, PJT_AVL_TIMESTAMP, DEL_STS_ID AS AV_DEL_STS_ID FROM AM_PJT_AVL) AV ON PJT_ID = AV.AV_PJT_ID AND PJT_AVL_SEQ_NO = AV.AV_PJT_AVL_SEQ_NO AND AV.AV_DEL_STS_ID = FALSE")
			;

			return buf.toString();
		}
	}

}
