package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;

/**
 * The interface of the asset register view entity.
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 */
public interface IViewAssetRegisterEntity extends IEntity, IEntityExecData {
	/**
	 * Module Nameを取得します。
	 * @return Module Name
	 */
	public String getMdlNm();
	/**
	 * Resource Pathを取得します。
	 * @return Resource Path
	 */
	public String getFilePath();
	/**
	 * Lot Idを取得します。
	 * @return Lot Id
	 */
	public String getLotId();
	/**
	 * Lot Nameを取得します。
	 * @return Lot Name
	 */
	public String getLotNm();
	/**
	 * Change Propertyを取得します。
	 * @return Change Property
	 */
	public String getPjtId();
	/**
	 * Change Factor No.を取得します。
	 * @return Change Factor No.
	 */
	public String getChgFactorNo();
	/**
	 * Resource Inquiry Idを取得します。
	 * @return Resource Inquiry
	 */
	public String getAreqId();
	/**
	 * Resource Category Codeを取得します。
	 * @return Resource Category Code
	 */
	public String getAreqCtgCd();
	/**
	 * Group Idを取得します。
	 * @return Group Id
	 */
	public String getGrpId();
	/**
	 * Group Nameを取得します。
	 * @return Group Name
	 */
	public String getGrpNm();
	/**
	 * Resource Inquiry Subjectを取得します。
	 * @return Resource Inquiry Subject
	 */
	public String getAssetReqNm();
	/**
	 * Contentを取得します。
	 * @return Content
	 */
	public String getContent();
	/**
	 * Checkout Inquiry User Idを取得します。
	 * @return Checkout Inquiry User Id
	 */
	public String getLendReqUserId();
	/**
	 * Checkout Inquiry User Nameを取得します。
	 * @return Checkout Inquiry User Name
	 */
	public String getLendReqUserNm();
	/**
	 * Checkout Inquiry Timestampを取得します。
	 * @param Checkout Inquiry Timestamp lendReqTimestamp
	 */
	public Timestamp getLendReqTimestamp();
	/**
	 * Check-in Inquiry User Idを取得します。
	 * @return Check-in Inquiry User Id rtnReqUserId
	 */
	public String getRtnReqUserId();
	/**
	 * Check-in Inquiry User Nameを取得します。
	 * @return Check-in Inquiry User Name
	 */
	public String getRtnReqUserNm();
	/**
	 * Check-in Inquiry Timestampを取得します。
	 * @return Check-in Inquiry Timestamp
	 */
	public Timestamp getRtnReqTimestamp();
	/**
	 * Removal Inquiry User Idを取得します。
	 * @return Removal Inquiry User Id delReqUserId
	 */
	public String getDelReqUserId();
	/**
	 * Removal Inquiry User Nameを取得します。
	 * @return Removal Inquiry User Name
	 */
	public String getDelReqUserNm();
	/**
	 * Removal Inquiry Timestampを取得します。
	 * @return Check-in Inquiry Timestamp
	 */
	public Timestamp getDelReqTimestamp();
	/**
	 * Checkout Resource Revisionを取得します。
	 * @return Checkout Resource Revision
	 */
	public String getLendFileRev();
	/**
	 * Checkout Resource Byte Sizeを取得します。
	 * @return Checkout Resource Byte Size
	 */
	public Integer getLendFileByteSize();
	/**
	 * Checkout Resource Timestampを取得します。
	 * @return Checkout Resource Timestamp
	 */
	public Timestamp getLendFileUpdTimestamp();
	/**
	 * Check-in Resource Revisionを取得します。
	 * @return Check-in Resource Revision
	 */
	public String getRtnFileRev();
	/**
	 * Check-in Resource Byte Sizeを取得します。
	 * @return Check-in Resource Byte Size
	 */
	public Integer getRtnFileByteSize();
	/**
	 * Check-in Resource Timestampを取得します。
	 * @return Check-in Resource Timestamp
	 */
	public Timestamp getRtnFileUpdTimestamp();
	/**
	 * Removal Resource Revisionを取得します。
	 * @return Removal Resource Revision
	 */
	public String getDelFileRev();
	/**
	 * Removal Resource Byte Sizeを取得します。
	 * @return Removal Resource Byte Size
	 */
	public Integer getDelFileByteSize();
	/**
	 * Removal Resource Timestampを取得します。
	 * @return Removal Resource Timestamp
	 */
	public Timestamp getDelFileUpdTimestamp();
	/**
	 * Change Property Approval Timestampを取得します。
	 * @return Change Property Approval Timestamp
	 */
	public Timestamp getPjtAvlTimestamp();
	/**
	 * Status Idを取得します。
	 * @return Status Id
	 */
	public String getStsId();

}
