package jp.co.blueship.tri.am.dao.areq.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the asset request file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum AreqFileItems implements ITableItem {
	areqId("areq_id"),
	areqCtgCd("areq_ctg_cd"),
	areqFileSeqNo("areq_file_seq_no"),
	filePath("file_path"),
	fileRev("file_rev"),
	fileByteSize("file_byte_size"),
	fileUpdTimestamp("file_upd_timestamp"),
	fileStsId("file_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private AreqFileItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
