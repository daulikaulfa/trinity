package jp.co.blueship.tri.am.dao.areq;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the asset request binary file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IAreqBinaryFileDao extends IJdbcDao<IAreqBinaryFileEntity> {

}
