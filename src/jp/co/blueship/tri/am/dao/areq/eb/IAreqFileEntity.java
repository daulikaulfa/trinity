package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the aseet request file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IAreqFileEntity extends IEntityFooter {

	public String getAreqId();
	public void setAreqId(String areqId);

	public String getAreqFileSeqNo();
	public void setAreqFileSeqNo(String areqFileSeqNo);

	public String getAreqCtgCd();
	public void setAreqCtgCd(String areqCtgCd);

	public String getFilePath();
	public void setFilePath(String filePath);

	public String getFileRev();
	public void setFileRev(String fileRev);

	public Integer getFileByteSize();
	public void setFileByteSize(Integer fileByteSize);

	public Timestamp getFileUpdTimestamp();
	public void setFileUpdTimestamp(Timestamp fileUpdTimestamp);

	public String getFileStsId();
	public void setFileStsId(String fileStsId);

}
