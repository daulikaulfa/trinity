package jp.co.blueship.tri.am.dao.areq;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.areq.constants.AreqAttachedFileItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the asset request attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class AreqAttachedFileDao extends JdbcBaseDao<IAreqAttachedFileEntity> implements IAreqAttachedFileDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_AREQ_ATTACHED_FILE;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IAreqAttachedFileEntity entity ) {
		builder
			.append(AreqAttachedFileItems.areqId, entity.getAreqId(), true)
			.append(AreqAttachedFileItems.attachedFileSeqNo, entity.getAttachedFileSeqNo(), true)
			.append(AreqAttachedFileItems.filePath, entity.getFilePath())
			.append(AreqAttachedFileItems.delStsId, ( null == entity.getDelStsId())? null :entity.getDelStsId().parseBoolean())
			.append(AreqAttachedFileItems.regTimestamp, entity.getRegTimestamp())
			.append(AreqAttachedFileItems.regUserId, entity.getRegUserId())
			.append(AreqAttachedFileItems.regUserNm, entity.getRegUserNm())
			.append(AreqAttachedFileItems.updTimestamp, entity.getUpdTimestamp())
			.append(AreqAttachedFileItems.updUserId, entity.getUpdUserId())
			.append(AreqAttachedFileItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IAreqAttachedFileEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IAreqAttachedFileEntity entity = new AreqAttachedFileEntity();

  	  entity.setAreqId( rs.getString(AreqAttachedFileItems.areqId.getItemName()) );
  	  entity.setAttachedFileSeqNo( rs.getString(AreqAttachedFileItems.attachedFileSeqNo.getItemName()) );
  	  entity.setFilePath( rs.getString(AreqAttachedFileItems.filePath.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(AreqAttachedFileItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(AreqAttachedFileItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(AreqAttachedFileItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(AreqAttachedFileItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(AreqAttachedFileItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(AreqAttachedFileItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(AreqAttachedFileItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
