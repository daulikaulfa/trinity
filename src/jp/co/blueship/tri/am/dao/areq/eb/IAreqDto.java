package jp.co.blueship.tri.am.dao.areq.eb;

import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * The interface of the asset request DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IAreqDto extends IEntity {

	/**
	 * 資産申請を取得します。
	 *
	 * @return 資産申請Entity
	 */
	public IAreqEntity getAreqEntity();
	/**
	 * 資産申請を設定します。
	 *
	 * @param areqEntity 資産申請Entity
	 */
	public void setAreqEntity(IAreqEntity areqEntity);

	/**
	 * 資産申請ファイルEntityのListを取得します。
	 *
	 * @return 資産申請ファイルEntityのList
	 */
	public List<IAreqFileEntity> getAreqFileEntities();

	/**
	 * 資産申請ファイルEntityのListを設定します。
	 *
	 * @param areqFileEntities 資産申請ファイルEntityのList
	 */
	public void setAreqFileEntities(List<IAreqFileEntity> areqFileEntities);

	/**
	 * 資産申請ファイルを取得します。
	 *
	 * @param ctgCd 対象となる資産申請分類コード
	 * @return 資産申請ファイルEntityのList
	 */
	public List<IAreqFileEntity> getAreqFileEntities( AreqCtgCd... ctgCd );

	/**
	 * 資産申請BinaryファイルのListを取得します。
	 *
	 * @return 資産申請BinaryファイルのList
	 */
	public List<IAreqBinaryFileEntity> getAreqBinaryFileEntities();

	/**
	 * 資産申請BinaryファイルのListを設定します。
	 *
	 * @param areqBinaryFileEntities 資産申請Binaryファイル
	 */
	public void setAreqBinaryFileEntities(List<IAreqBinaryFileEntity> areqBinaryFileEntities);

	/**
	 * 資産申請Binaryファイルを取得します。
	 *
	 * @param ctgCd 対象となる資産申請分類コード
	 * @return 資産申請BinaryファイルEntityのList
	 */
	public List<IAreqBinaryFileEntity> getAreqBinaryFileEntities( AreqCtgCd... ctgCd );

	/**
	 * 申請添付ファイルのListを取得します。
	 *
	 * @return 申請添付ファイルのList
	 */
	public List<IAreqAttachedFileEntity> getAreqAttachedFileEntities();

	/**
	 * 申請添付ファイルのListを設定します。
	 *
	 * @param areqAttachedFileEntities 申請添付ファイルのList
	 */
	public void setAreqAttachedFileEntities(List<IAreqAttachedFileEntity> areqAttachedFileEntities);
}
