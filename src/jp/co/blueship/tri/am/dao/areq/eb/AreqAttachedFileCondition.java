package jp.co.blueship.tri.am.dao.areq.eb;

import jp.co.blueship.tri.am.dao.areq.constants.AreqAttachedFileItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the attr request attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class AreqAttachedFileCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_AREQ_ATTACHED_FILE;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * attached file sequence number
	 */
	public String attachedFileSeqNo = null;
	/**
	 * file path
	 */
	public String filePath = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
	   super.append(AreqAttachedFileItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public AreqAttachedFileCondition(){
		super(attr);
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(AreqAttachedFileItems.areqId, areqId );
	}
	/**
	 * attached file sequence numberを取得します。
	 * @return attached-file-sequence-number
	 */
	public String getAttachedFileSeqNo() {
	    return attachedFileSeqNo;
	}
	/**
	 * attached file sequence numberを設定します。
	 * @param attachedFileSeqNo attached-file-sequence-number
	 */
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
	    this.attachedFileSeqNo = attachedFileSeqNo;
	    super.append(AreqAttachedFileItems.attachedFileSeqNo, attachedFileSeqNo );
	}
	/**
	 * file pathを取得します。
	 * @return file-path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file-path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	    super.append(AreqAttachedFileItems.filePath, filePath );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( AreqAttachedFileItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}