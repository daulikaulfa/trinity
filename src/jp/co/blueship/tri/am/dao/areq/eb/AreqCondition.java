package jp.co.blueship.tri.am.dao.areq.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The SQL condition of the asset request entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki

 * @version V3L10.02
 * @author Eguchi Yukihiro
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class AreqCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_AREQ;

	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq-ID's
	 */
	public String[] areqIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;

	/**
	 * lot-ID's
	 */
	private String[] lotIds = null;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * pjt-ID's
	 */
	public String[] pjtIds = null;
	/**
	 * keyword
	 */
	public String keyword = null;
	/**
	 * My Request UsesrID
	 */
	private String myRequestUserId = null;
	/**
	 * pjt avl sequence number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * asset request catalog code
	 */
	public String areqCtgCd = null;
	/**
	 * asset request catalog code's
	 */
	public String[] areqCtgCds = null;
	/**
	 * asset request name
	 */
	public String assetReqNm = null;
	/**
	 * grp-ID
	 */
	public String grpId = null;
	/**
	 * group name
	 */
	public String grpNm = null;
	/**
	 * group name's
	 */
	public String[] grpNms = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * lend request user-ID
	 */
	public String lendReqUserId = null;
	/**
	 * lend request user-ID's
	 */
	public String[] lendReqUserIds = null;
	/**
	 * lend request user name
	 */
	public String lendReqUserNm = null;
	/**
	 * lend request time stamp
	 */
	public Timestamp lendReqTimestamp = null;
	/**
	 * return request user-ID
	 */
	public String rtnReqUserId = null;
	/**
	 * return request user name
	 */
	public String rtnReqUserNm = null;
	/**
	 * return request time stamp
	 */
	public Timestamp rtnReqTimestamp = null;
	/**
	 * delete request user-ID
	 */
	public String delReqUserId = null;
	/**
	 * delete request user-ID's
	 */
	public String[] delReqUserIds = null;
	/**
	 * delete request user name
	 */
	public String delReqUserNm = null;
	/**
	 * delete request time stamp
	 */
	public Timestamp delReqTimestamp = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user-ID's
	 */
	public String[] delUserIds = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;
	/**
	 * assinee ID's
	 */
	private String[] assigneeIds = null;
	/**
	 * group ID's
	 */
	private String[] grpIds = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(AreqItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * delete-status-ID's
	 */
	public StatusFlg[] delStsIds = null;
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public AreqCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( AreqItems.areqId );
		super.setJoinCtg( true );
		super.setJoinMstone( true );
		super.setJoinUser(true);
		super.setKeyByJoinUsers(AreqItems.assigneeId);
	}

	@Override
	public AreqCondition setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	@Override
	public AreqCondition setJoinAccsHist(boolean isJoinAccsHist, JoinType join) {
		super.setJoinAccsHist(isJoinAccsHist, join);
		return this;
	}


	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( AreqItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, AreqItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( AreqItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, AreqItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId areq-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(AreqItems.lotId, lotId );
	}

	/**
	 * @return Gets the Lot ID's.
	 */
	public String[] getLotIds() {
		return lotIds;
	}

	/**
	 * @param lotIds
	 */
	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
		super.appendByJoinExecData( AreqItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, AreqItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(AreqItems.areqId, areqId );
	}
	/**
	 * areq-ID'sを設定します。
	 * @param pjtIds pjt-ID's
	 */
	public void setAreqIds(String... areqIds) {
	    this.areqIds = areqIds;
	    super.append( AreqItems.areqId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(areqIds, AreqItems.areqId.getItemName(), false, true, false) );
	}
	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	    super.append(AreqItems.pjtId, pjtId );
	}
	/**
	 * pjt-ID'sを設定します。
	 * @param pjtIds pjt-ID's
	 */
	public void setPjtIds(String... pjtIds) {
	    this.pjtIds = pjtIds;
	    super.append( AreqItems.pjtId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtIds, AreqItems.pjtId.getItemName(), false, true, false) );
	}

	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String keyword) {
	    this.keyword = keyword;
	    super.append( AreqItems.pjtId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[] {
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.pjtId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.pjtSubject.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.areqId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.summary.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.lendReqUserNm.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.delReqUserNm.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, AreqItems.assigneedNm.getItemName(), true, false, false),
	    			} ));
	}

	/**
	 * My Request UserID を設定します。
	 * @param myRequestUserId
	 */
	public void setContainsByMyRequest(String myRequestUserId){
		this.myRequestUserId = myRequestUserId;
		super.append( "myRequestUserId",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[] {
	    			SqlFormatUtils.getCondition( this.myRequestUserId, AreqItems.lendReqUserId.getItemName(), false, false, false),
	    			SqlFormatUtils.getCondition( this.myRequestUserId, AreqItems.delReqUserId.getItemName(), false, false, false),
	    			SqlFormatUtils.getCondition( this.myRequestUserId, AreqItems.assigneeId.getItemName(), false, false, false)
	    			} ));
	}

	/**
	 * pjt avl sequence numberを取得します。
	 * @return pjt avl sequence number
	 */
	public String getPjtAvlSeqNo() {
	    return pjtAvlSeqNo;
	}
	/**
	 * pjt avl sequence numberを設定します。
	 * @param pjtAvlSeqNo pjt avl sequence number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
	    this.pjtAvlSeqNo = pjtAvlSeqNo;
	    super.append(AreqItems.pjtAvlSeqNo, pjtAvlSeqNo );
	}
	/**
	 * asset request catalog codeを取得します。
	 * @return asset request catalog code
	 */
	public String getAreqCtgCd() {
	    return areqCtgCd;
	}
	/**
	 * asset request catalog codeを設定します。
	 * @param areqCtgCd asset request catalog code
	 */
	public void setAreqCtgCd(String areqCtgCd) {
	    this.areqCtgCd = areqCtgCd;
	    super.append(AreqItems.areqCtgCd, areqCtgCd );
	}
	/**
	 * asset request catalog code'sを設定します。
	 * @param areqCtgCds asset request catalog code's
	 */
	public void setAreqCtgCds(String... areqCtgCds) {
	    this.areqCtgCds = areqCtgCds;
	    super.append( AreqItems.areqCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(areqCtgCds, AreqItems.areqCtgCd.getItemName(), false, true, false) );
	}
	/**
	 * asset request nameを取得します。
	 * @return asset request name
	 */
	public String getAssetReqNm() {
	    return assetReqNm;
	}
	/**
	 * asset request nameを設定します。
	 * @param assetReqNm asset request name
	 */
	public void setAssetReqNm(String assetReqNm) {
	    this.assetReqNm = assetReqNm;
	    super.append(AreqItems.assetReqNm, assetReqNm );
	}
	/**
	 * grp-IDを取得します。
	 * @return grp-ID
	 */
	public String getGrpId() {
	    return grpId;
	}
	/**
	 * grp-IDを設定します。
	 * @param grpId grp-ID
	 */
	public void setGrpId(String grpId) {
	    this.grpId = grpId;
	    super.append(AreqItems.grpId, grpId );
	}
	/**
	 * group nameを取得します。
	 * @return group name
	 */
	public String getGrpNm() {
	    return grpNm;
	}
	/**
	 * group nameを設定します。
	 * @param grpNm group name
	 */
	public void setGrpNm(String grpNm) {
	    this.grpNm = grpNm;
	    super.append(AreqItems.grpNm, grpNm );
	}
	/**
	 * group name'sを設定します。
	 * @param grpNms group name's
	 */
	public void setGrpNms(String... grpNm) {
	    this.grpNms = grpNm;
	    super.append( AreqItems.grpNm.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpNms, AreqItems.grpNm.getItemName(), false, true, false) );
	}
	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	    super.append(AreqItems.content, content );
	}
	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return content;
	}
	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	    super.append(AreqItems.summary, summary );
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(AreqItems.stsId, stsId );
	}
	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String... stsIds) {
	    this.stsIds = stsIds;
	    super.append( AreqItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, AreqItems.stsId.getItemName(), false, true, false) );
	}
	public void setStsIdsNotEquals(String... stsIds) {
	    this.stsIds = stsIds;
	    super.append( AreqItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, AreqItems.stsId.getItemName(), false, false, true) );
	}
	/**
	 * lend request user-IDを取得します。
	 * @return lend request user-ID
	 */
	public String getLendReqUserId() {
	    return lendReqUserId;
	}
	/**
	 * lend request user-IDを設定します。
	 * @param lendReqUserId lend request user-ID
	 */
	public void setLendReqUserId(String lendReqUserId) {
	    this.lendReqUserId = lendReqUserId;
	    super.append(AreqItems.lendReqUserId, lendReqUserId );
	}
	/**
	 * lend request user-ID'sを設定します。
	 * @param lendReqUserIds lend request user-ID's
	 */
	public void setLendReqUserIds(String... lendReqUserIds) {
	    this.lendReqUserIds = lendReqUserIds;
	    super.append( AreqItems.lendReqUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lendReqUserIds, AreqItems.lendReqUserId.getItemName(), false, true, false) );
	}
	/**
	 * lend request user nameを取得します。
	 * @return lend request user name
	 */
	public String getLendReqUserNm() {
	    return lendReqUserNm;
	}
	/**
	 * lend request user nameを設定します。
	 * @param lendReqUserNm lend request user name
	 */
	public void setLendReqUserNm(String lendReqUserNm) {
	    this.lendReqUserNm = lendReqUserNm;
	    super.append(AreqItems.lendReqUserNm, lendReqUserNm );
	}
	/**
	 * lend request time stampを取得します。
	 * @return lend request time stamp
	 */
	public Timestamp getLendReqTimestamp() {
	    return lendReqTimestamp;
	}
	/**
	 * lend request time stampを設定します。
	 * @param lendReqTimestamp lend request time stamp
	 */
	public void setLendReqTimestamp(Timestamp lendReqTimestamp) {
	    this.lendReqTimestamp = lendReqTimestamp;
	    super.append(AreqItems.lendReqTimestamp, lendReqTimestamp );
	}
	/**
	 * return request user-IDを取得します。
	 * @return return request user-ID
	 */
	public String getRtnReqUserId() {
	    return rtnReqUserId;
	}
	/**
	 * return request user-IDを設定します。
	 * @param rtnReqUserId return request user-ID
	 */
	public void setRtnReqUserId(String rtnReqUserId) {
	    this.rtnReqUserId = rtnReqUserId;
	    super.append(AreqItems.rtnReqUserId, rtnReqUserId );
	}
	/**
	 * return request user nameを取得します。
	 * @return return request user name
	 */
	public String getRtnReqUserNm() {
	    return rtnReqUserNm;
	}
	/**
	 * return request user nameを設定します。
	 * @param rtnReqUserNm return request user name
	 */
	public void setRtnReqUserNm(String rtnReqUserNm) {
	    this.rtnReqUserNm = rtnReqUserNm;
	    super.append(AreqItems.rtnReqUserNm, rtnReqUserNm );
	}
	/**
	 * return request time stampを取得します。
	 * @return return request time stamp
	 */
	public Timestamp getRtnReqTimestamp() {
	    return rtnReqTimestamp;
	}
	/**
	 * return request time stampを設定します。
	 * @param rtnReqTimestamp return request time stamp
	 */
	public void setRtnReqTimestamp(Timestamp rtnReqTimestamp) {
	    this.rtnReqTimestamp = rtnReqTimestamp;
	    super.append(AreqItems.rtnReqTimestamp, rtnReqTimestamp );
	}
	/**
	 * delete request user-IDを取得します。
	 * @return delete request user-ID
	 */
	public String getDelReqUserId() {
	    return delReqUserId;
	}
	/**
	 * delete request user-IDを設定します。
	 * @param delReqUserId delete request user-ID
	 */
	public void setDelReqUserId(String delReqUserId) {
	    this.delReqUserId = delReqUserId;
	    super.append(AreqItems.delReqUserId, delReqUserId );
	}
	/**
	 * delete requestuser-ID'sを設定します。
	 * @param delReqUserIds delete request user-ID's
	 */
	public void setDelReqUserIds(String... delReqUserIds) {
	    this.delReqUserIds = delReqUserIds;
	    super.append( AreqItems.delReqUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(delReqUserIds, AreqItems.delReqUserId.getItemName(), false, true, false) );
	}
	/**
	 * delete request user nameを取得します。
	 * @return delete request user name
	 */
	public String getDelReqUserNm() {
	    return delReqUserNm;
	}
	/**
	 * delete request user nameを設定します。
	 * @param delReqUserNm delete request user name
	 */
	public void setDelReqUserNm(String delReqUserNm) {
	    this.delReqUserNm = delReqUserNm;
	    super.append(AreqItems.delReqUserNm, delReqUserNm );
	}
	/**
	 * delete request time stampを取得します。
	 * @return delete request time stamp
	 */
	public Timestamp getDelReqTimestamp() {
	    return delReqTimestamp;
	}
	/**
	 * delete request time stampを設定します。
	 * @param delReqTimestamp delete request time stamp
	 */
	public void setDelReqTimestamp(Timestamp delReqTimestamp) {
	    this.delReqTimestamp = delReqTimestamp;
	    super.append(AreqItems.delReqTimestamp, delReqTimestamp );
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	    super.append(AreqItems.delUserId, delUserId );
	}
	/**
	 * delete user-ID'sを取得します。
	 * @return delete user-ID's
	 */
	public String[] getDelUserIds() {
	    return delUserIds;
	}

	/**
	 * delete user-ID'sを設定します。
	 * @param delUserIds delete user-ID's
	 */
	public void setDelUserIds(String... delUserIds) {
	    this.delUserIds = delUserIds;
	    super.append(AreqItems.delUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(delUserIds, AreqItems.delUserId.getItemName(), false, true, false) );
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	    super.append(AreqItems.delUserNm, delUserNm );
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	    super.append(AreqItems.delTimestamp, delTimestamp );
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	    super.append(AreqItems.delCmt, delCmt );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( AreqItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * delete-status-IDsを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg[] getDelStsIds() {
	    return delStsIds;
	}

	/**
	 * delete-status-IDsを設定します。
	 * @param delStsId delete-status-IDs
	 */
	public void setDelStsIds(StatusFlg... delStsIds) {
	    this.delStsIds = delStsIds;
	    super.append( AreqItems.delStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(delStsIds, AreqItems.delStsId.getItemName(), true, false) );
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @return
	 */
	public String[] getAssigneeIds() {
		return assigneeIds;
	}

	/**
	 * @param assigneeIds
	 */
	public void setAssigneeIds(String[] assigneeIds) {
		this.assigneeIds = assigneeIds;
		super.appendByJoinExecData( AreqItems.assigneeId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(assigneeIds, AreqItems.assigneeId.getItemName(), false, true, false) );
	}

	/**
	 * @return
	 */
	public String[] getGrpIds() {
		return grpIds;
	}

	/**
	 * @param grpIds
	 */
	public void setGrpIds(String[] grpIds) {
		this.grpIds = grpIds;
		super.appendByJoinExecData( AreqItems.grpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(grpIds, AreqItems.grpId.getItemName(), false, true, false) );
	}

	/**
	 * @param mstoneStartDate
	 * @param mstoneEndDate
	 */
	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}


}