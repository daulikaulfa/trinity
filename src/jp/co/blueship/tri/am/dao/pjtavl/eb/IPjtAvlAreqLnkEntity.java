package jp.co.blueship.tri.am.dao.pjtavl.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the project approval asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPjtAvlAreqLnkEntity extends IEntityFooter {

	public String getPjtId();
	public void setPjtId(String pjtId);

	public String getPjtAvlSeqNo();
	public void setPjtAvlSeqNo(String pjtAvlSeqNo);

	public String getAreqId();
	public void setAreqId(String areqId);

}
