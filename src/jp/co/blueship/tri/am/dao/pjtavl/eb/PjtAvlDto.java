package jp.co.blueship.tri.am.dao.pjtavl.eb;

import java.util.List;

import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;

/**
 * pjt-avl request DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PjtAvlDto implements IPjtAvlDto {

	/**
	 * 変更情報
	 */
	public IPjtEntity pjtEntity = null;

	/**
	 * 最新の変更承認
	 */
	public IPjtAvlEntity pjtAvlEntity = null;
	/**
	 * 変更承認・申請(Lnk)
	 */
	public List<IPjtAvlAreqLnkEntity> pjtAvlAreqLnkEntityList;
	/**
	 * 変更情報を取得します。
	 *
	 * @return 変更情報Entity
	 */
	public IPjtEntity getPjtEntity() {
		return this.pjtEntity;
	}
	/**
	 * 変更情報を設定します。
	 *
	 * @param pjtEntity 変更情報Entity
	 */
	public void setPjtEntity(IPjtEntity pjtEntity)  {
		this.pjtEntity = pjtEntity;
	}

	/**
	 * 最新の変更承認を取得します。
	 *
	 * @return 最新の変更承認Entity
	 */
	public IPjtAvlEntity getPjtAvlEntity() {
		return this.pjtAvlEntity;
	}
	/**
	 * 最新の変更承認を設定します。
	 *
	 * @param pjtAvlEntity 変更承認Entity
	 */
	public void setPjtAvlEntity(IPjtAvlEntity pjtAvlEntity) {
		this.pjtAvlEntity = pjtAvlEntity;
	}
	/**
	 * 変更承認・申請(Lnk)を取得します。
	 * @return 変更承認・申請(Lnk)Entity
	 */
	public List<IPjtAvlAreqLnkEntity> getPjtAvlAreqLnkEntityList() {
		return this.pjtAvlAreqLnkEntityList;
	}
	/**
	 * 変更承認・申請(Lnk)を設定します。
	 * @param pjtAvlAreqLnkEntity 変更承認・申請(Lnk)
	 */
	public void setPjtAvlAreqLnkEntityList(List<IPjtAvlAreqLnkEntity> pjtAvlAreqLnkEntityList ) {
		this.pjtAvlAreqLnkEntityList = pjtAvlAreqLnkEntityList;
	}

}
