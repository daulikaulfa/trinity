package jp.co.blueship.tri.am.dao.pjtavl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * The implements of the Change-Information apploval numbering DAO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public class PjtAvlNumberingDao extends JdbcBaseDao<String> implements INumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private String SQL_SELECT_QUERY = "select to_char(now(), 'YYMMDDHH24MISS') as nextval";

	public String nextval() {
		try {
			List<?> rows = super.query(
					SQL_SELECT_QUERY,
					new ParameterizedRowMapper<String>() {
						@Override
						public String mapRow(ResultSet resultset, int i) throws SQLException {
							return entityMapping( resultset, i );
						}
					});

			return (String)rows.get(0);
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005009S, e,this.getTableAttribute().name() );
		}
	}

	@Override
	protected final String entityMapping( ResultSet rs, int row ) throws SQLException {
		return rs.getString("nextval");
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_PJT_AVL;
	}

	@Override
	protected ISqlBuilder append(ISqlBuilder builder, String entity) {
		return builder;
	}
}
