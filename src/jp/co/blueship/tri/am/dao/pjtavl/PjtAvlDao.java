package jp.co.blueship.tri.am.dao.pjtavl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlItems;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The implements of the Change-Information approval DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class PjtAvlDao extends JdbcBaseDao<IPjtAvlEntity> implements IPjtAvlDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_PJT_AVL;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IPjtAvlEntity entity ) {
		builder
			.append(PjtAvlItems.pjtId, entity.getPjtId(), true)
			.append(PjtAvlItems.pjtAvlSeqNo, entity.getPjtAvlSeqNo(), true)
			.append(PjtAvlItems.stsId, entity.getStsId())
			.append(PjtAvlItems.pjtAvlRejectUserId, entity.getPjtAvlRejectUserId())
			.append(PjtAvlItems.pjtAvlRejectUserNm, entity.getPjtAvlRejectUserNm())
			.append(PjtAvlItems.pjtAvlRejectTimestamp, entity.getPjtAvlRejectTimestamp())
			.append(PjtAvlItems.pjtAvlRejectCmt, entity.getPjtAvlRejectCmt())
			.append(PjtAvlItems.pjtAvlUserId, entity.getPjtAvlUserId())
			.append(PjtAvlItems.pjtAvlUserNm, entity.getPjtAvlUserNm())
			.append(PjtAvlItems.pjtAvlTimestamp, entity.getPjtAvlTimestamp())
			.append(PjtAvlItems.pjtAvlCmt, entity.getPjtAvlCmt())
			.append(PjtAvlItems.pjtAvlCancelUserId, entity.getPjtAvlCancelUserId())
			.append(PjtAvlItems.pjtAvlCancelUserNm, entity.getPjtAvlCancelUserNm())
			.append(PjtAvlItems.pjtAvlCancelTimestamp, entity.getPjtAvlCancelTimestamp())
			.append(PjtAvlItems.pjtAvlCancelCmt, entity.getPjtAvlCancelCmt())
			.append(PjtAvlItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(PjtAvlItems.regTimestamp, entity.getRegTimestamp())
			.append(PjtAvlItems.regUserId, entity.getRegUserId())
			.append(PjtAvlItems.regUserNm, entity.getRegUserNm())
			.append(PjtAvlItems.updTimestamp, entity.getUpdTimestamp())
			.append(PjtAvlItems.updUserId, entity.getUpdUserId())
			.append(PjtAvlItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IPjtAvlEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		PjtAvlEntity entity = new PjtAvlEntity();

		entity.setPjtId( rs.getString(PjtAvlItems.pjtId.getItemName()) );
		entity.setPjtAvlSeqNo( rs.getString(PjtAvlItems.pjtAvlSeqNo.getItemName()) );
		entity.setLotId( rs.getString(PjtAvlItems.lotId.getItemName()) );
		entity.setStsId( rs.getString(PjtAvlItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setPjtAvlRejectUserId( rs.getString(PjtAvlItems.pjtAvlRejectUserId.getItemName()) );
		entity.setPjtAvlRejectUserNm( rs.getString(PjtAvlItems.pjtAvlRejectUserNm.getItemName()) );
		entity.setPjtAvlRejectTimestamp( rs.getTimestamp(PjtAvlItems.pjtAvlRejectTimestamp.getItemName()) );
		entity.setPjtAvlRejectCmt( rs.getString(PjtAvlItems.pjtAvlRejectCmt.getItemName()) );
		entity.setPjtAvlUserId( rs.getString(PjtAvlItems.pjtAvlUserId.getItemName()) );
		entity.setPjtAvlUserNm( rs.getString(PjtAvlItems.pjtAvlUserNm.getItemName()) );
		entity.setPjtAvlTimestamp( rs.getTimestamp(PjtAvlItems.pjtAvlTimestamp.getItemName()) );
		entity.setPjtAvlCmt( rs.getString(PjtAvlItems.pjtAvlCmt.getItemName()) );
		entity.setPjtAvlCancelUserId( rs.getString(PjtAvlItems.pjtAvlCancelUserId.getItemName()) );
		entity.setPjtAvlCancelUserNm( rs.getString(PjtAvlItems.pjtAvlCancelUserNm.getItemName()) );
		entity.setPjtAvlCancelTimestamp( rs.getTimestamp(PjtAvlItems.pjtAvlCancelTimestamp.getItemName()) );
		entity.setPjtAvlCancelCmt( rs.getString(PjtAvlItems.pjtAvlCancelCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(PjtAvlItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(PjtAvlItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(PjtAvlItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(PjtAvlItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(PjtAvlItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(PjtAvlItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(PjtAvlItems.updUserNm.getItemName()) );

		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,P.LOT_ID")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT LOT_ID, PJT_ID AS P_PJT_ID, CHG_FACTOR_ID, CHG_FACTOR_NO, SUMMARY, ASSIGNEE_NM FROM AM_PJT) P ON PJT_ID = P.P_PJT_ID")
			;

			return buf.toString();
		}
	}
	
	
	public IEntityLimit<IPjtAvlEntity> findListPjtAvl(ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows) throws TriJdbcDaoException {
		try {
			int maxHitLimit = this.count(condition);
		
			ILimit limit = this.getPageLimit(pageNo, viewRows, maxHitLimit);

			String sql = this.getDaoTemplate().toSelectQuery(condition) + SqlFormatUtils.toSortString(sort)
					+ ((null == limit) ? "" : limit.getSqlLimit());
			String replacedSql = sql.replace("CL.DATA_CTG_CD = 'AM_PJT_AVL'", "CL.DATA_CTG_CD = 'AM_PJT'");
			replacedSql = replacedSql.replace("ML.DATA_CTG_CD = 'AM_PJT_AVL'", "ML.DATA_CTG_CD = 'AM_PJT'");
			List<IPjtAvlEntity> rows = super.query(replacedSql, new ParameterizedRowMapper<IPjtAvlEntity>() {
				@Override
				public IPjtAvlEntity mapRow(ResultSet resultset, int i) throws SQLException {
					return entityMapping(resultset, i);
				}
			}, condition);

			IEntityLimit<IPjtAvlEntity> home = new EntityLimit<IPjtAvlEntity>();
			home.setLimit(limit);
			home.setEntities(rows);

			return home;

		} catch (Exception e) {
			throw new TriJdbcDaoException(SmMessageId.SM005004S, e, this.getTableAttribute().name());
		}
	}
	

}
