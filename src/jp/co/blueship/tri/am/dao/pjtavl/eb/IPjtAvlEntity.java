package jp.co.blueship.tri.am.dao.pjtavl.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the project approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IPjtAvlEntity extends IEntityFooter, IEntityExecData {

	public String getPjtId();
	public void setPjtId(String pjtId);

	public String getPjtAvlSeqNo();
	public void setPjtAvlSeqNo(String pjtAvlSeqNo);

	public String getLotId();
	public void setLotId(String lotId);

	public String getStsId();
	public void setStsId(String stsId);

	public String getPjtAvlRejectUserId();
	public void setPjtAvlRejectUserId(String pjtAvlRejectUserId);

	public String getPjtAvlRejectUserNm();
	public void setPjtAvlRejectUserNm(String pjtAvlRejectUserNm);

	public Timestamp getPjtAvlRejectTimestamp();
	public void setPjtAvlRejectTimestamp(Timestamp pjtAvlRejectTimestamp);

	public String getPjtAvlRejectCmt();
	public void setPjtAvlRejectCmt(String pjtAvlRejectCmt);

	public String getPjtAvlUserId();
	public void setPjtAvlUserId(String pjtAvlUserId);

	public String getPjtAvlUserNm();
	public void setPjtAvlUserNm(String pjtAvlUserNm);

	public Timestamp getPjtAvlTimestamp();
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp);

	public String getPjtAvlCmt();
	public void setPjtAvlCmt(String pjtAvlCmt);

	public String getPjtAvlCancelUserId();
	public void setPjtAvlCancelUserId(String pjtAvlCancelUserId);

	public String getPjtAvlCancelUserNm();
	public void setPjtAvlCancelUserNm(String pjtAvlCancelUserNm);

	public Timestamp getPjtAvlCancelTimestamp();
	public void setPjtAvlCancelTimestamp(Timestamp pjtAvlCancelTimestamp);

	public String getPjtAvlCancelCmt();
	public void setPjtAvlCancelCmt(String pjtAvlCancelCmt);

	public String getCtgId();
	public String getCtgNm();

	public String getMstoneId();
	public String getMstoneNm();
}
