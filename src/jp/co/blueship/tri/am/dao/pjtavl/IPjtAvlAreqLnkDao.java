package jp.co.blueship.tri.am.dao.pjtavl;

import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the Change-Information approval asset request link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPjtAvlAreqLnkDao extends IJdbcDao<IPjtAvlAreqLnkEntity> {

}
