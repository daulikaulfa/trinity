package jp.co.blueship.tri.am.dao.pjtavl.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * project approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class PjtAvlEntity extends EntityFooter implements IPjtAvlEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * project approval sequence number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * project approval reject user-ID
	 */
	public String pjtAvlRejectUserId = null;
	/**
	 * project approval reject user name
	 */
	public String pjtAvlRejectUserNm = null;
	/**
	 * project approval reject time stamp
	 */
	public Timestamp pjtAvlRejectTimestamp = null;
	/**
	 * project approval reject comment
	 */
	public String pjtAvlRejectCmt = null;
	/**
	 * project approval user-ID
	 */
	public String pjtAvlUserId = null;
	/**
	 * project approval user name
	 */
	public String pjtAvlUserNm = null;
	/**
	 * project approval time stamp
	 */
	public Timestamp pjtAvlTimestamp = null;
	/**
	 * project approval comment
	 */
	public String pjtAvlCmt = null;
	/**
	 * project approval cancel user-ID
	 */
	public String pjtAvlCancelUserId = null;
	/**
	 * project approval cancel user name
	 */
	public String pjtAvlCancelUserNm = null;
	/**
	 * project approval cancel time stamp
	 */
	public Timestamp pjtAvlCancelTimestamp = null;
	/**
	 * project approval cancel comment
	 */
	public String pjtAvlCancelCmt = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category name
	 */
	public String ctgNm = null;
	/**
	 * category ID
	 */
	public String mstoneId = null;
	/**
	 * category ID
	 */
	public String mstoneNm = null;
	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * project approval sequence numberを取得します。
	 * @return project approval sequence number
	 */
	public String getPjtAvlSeqNo() {
	    return pjtAvlSeqNo;
	}
	/**
	 * project approval sequence numberを設定します。
	 * @param pjtAvlSeqNo project approval sequence number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
	    this.pjtAvlSeqNo = pjtAvlSeqNo;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * process status-IDを取得します。
	 * @return process status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * project approval reject user-IDを取得します。
	 * @return project approval reject user-ID
	 */
	public String getPjtAvlRejectUserId() {
	    return pjtAvlRejectUserId;
	}
	/**
	 * project approval reject user-IDを設定します。
	 * @param pjtAvlRejectUserId project approval reject user-ID
	 */
	public void setPjtAvlRejectUserId(String pjtAvlRejectUserId) {
	    this.pjtAvlRejectUserId = pjtAvlRejectUserId;
	}
	/**
	 * project approval reject user nameを取得します。
	 * @return project approval reject user name
	 */
	public String getPjtAvlRejectUserNm() {
	    return pjtAvlRejectUserNm;
	}
	/**
	 * project approval reject user nameを設定します。
	 * @param pjtAvlRejectUserNm project approval reject user name
	 */
	public void setPjtAvlRejectUserNm(String pjtAvlRejectUserNm) {
	    this.pjtAvlRejectUserNm = pjtAvlRejectUserNm;
	}
	/**
	 * project approval reject time stampを取得します。
	 * @return project approval reject time stamp
	 */
	public Timestamp getPjtAvlRejectTimestamp() {
	    return pjtAvlRejectTimestamp;
	}
	/**
	 * project approval reject time stampを設定します。
	 * @param pjtAvlRejectTimestamp project approval reject time stamp
	 */
	public void setPjtAvlRejectTimestamp(Timestamp pjtAvlRejectTimestamp) {
	    this.pjtAvlRejectTimestamp = pjtAvlRejectTimestamp;
	}
	/**
	 * project approval reject commentを取得します。
	 * @return project approval reject comment
	 */
	public String getPjtAvlRejectCmt() {
	    return pjtAvlRejectCmt;
	}
	/**
	 * project approval reject commentを設定します。
	 * @param pjtAvlRejectCmt project approval reject comment
	 */
	public void setPjtAvlRejectCmt(String pjtAvlRejectCmt) {
	    this.pjtAvlRejectCmt = pjtAvlRejectCmt;
	}
	/**
	 * project approval user-IDを取得します。
	 * @return project approval user-ID
	 */
	public String getPjtAvlUserId() {
	    return pjtAvlUserId;
	}
	/**
	 * project approval user-IDを設定します。
	 * @param pjtAvlUserId project approval user-ID
	 */
	public void setPjtAvlUserId(String pjtAvlUserId) {
	    this.pjtAvlUserId = pjtAvlUserId;
	}
	/**
	 * project approval user nameを取得します。
	 * @return project approval user name
	 */
	public String getPjtAvlUserNm() {
	    return pjtAvlUserNm;
	}
	/**
	 * project approval user nameを設定します。
	 * @param pjtAvlUserNm project approval user name
	 */
	public void setPjtAvlUserNm(String pjtAvlUserNm) {
	    this.pjtAvlUserNm = pjtAvlUserNm;
	}
	/**
	 * project approval time stampを取得します。
	 * @return project approval time stamp
	 */
	public Timestamp getPjtAvlTimestamp() {
	    return pjtAvlTimestamp;
	}
	/**
	 * project approval time stampを設定します。
	 * @param pjtAvlTimestamp project approval time stamp
	 */
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp) {
	    this.pjtAvlTimestamp = pjtAvlTimestamp;
	}
	/**
	 * project approval commentを取得します。
	 * @return project approval comment
	 */
	public String getPjtAvlCmt() {
	    return pjtAvlCmt;
	}
	/**
	 * project approval commentを設定します。
	 * @param pjtAvlCmt project approval comment
	 */
	public void setPjtAvlCmt(String pjtAvlCmt) {
	    this.pjtAvlCmt = pjtAvlCmt;
	}
	/**
	 * project approval cancel user-IDを取得します。
	 * @return project approval cancel user-ID
	 */
	public String getPjtAvlCancelUserId() {
	    return pjtAvlCancelUserId;
	}
	/**
	 * project approval cancel user-IDを設定します。
	 * @param pjtAvlCancelUserId project approval cancel user-ID
	 */
	public void setPjtAvlCancelUserId(String pjtAvlCancelUserId) {
	    this.pjtAvlCancelUserId = pjtAvlCancelUserId;
	}
	/**
	 * project approval cancel user nameを取得します。
	 * @return project approval cancel user name
	 */
	public String getPjtAvlCancelUserNm() {
	    return pjtAvlCancelUserNm;
	}
	/**
	 * project approval cancel user nameを設定します。
	 * @param pjtAvlCancelUserNm project approval cancel user name
	 */
	public void setPjtAvlCancelUserNm(String pjtAvlCancelUserNm) {
	    this.pjtAvlCancelUserNm = pjtAvlCancelUserNm;
	}
	/**
	 * project approval cancel time stampを取得します。
	 * @return project approval cancel time stamp
	 */
	public Timestamp getPjtAvlCancelTimestamp() {
	    return pjtAvlCancelTimestamp;
	}
	/**
	 * project approval cancel time stampを設定します。
	 * @param pjtAvlCancelTimestamp project approval cancel time stamp
	 */
	public void setPjtAvlCancelTimestamp(Timestamp pjtAvlCancelTimestamp) {
	    this.pjtAvlCancelTimestamp = pjtAvlCancelTimestamp;
	}
	/**
	 * project approval cancel commentを取得します。
	 * @return project approval cancel comment
	 */
	public String getPjtAvlCancelCmt() {
	    return pjtAvlCancelCmt;
	}
	/**
	 * project approval cancel commentを設定します。
	 * @param pjtAvlCancelCmt project approval cancel comment
	 */
	public void setPjtAvlCancelCmt(String pjtAvlCancelCmt) {
	    this.pjtAvlCancelCmt = pjtAvlCancelCmt;
	}

	@Override
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	@Override
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	@Override
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}
}
