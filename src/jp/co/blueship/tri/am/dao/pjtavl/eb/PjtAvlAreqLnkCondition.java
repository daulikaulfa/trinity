package jp.co.blueship.tri.am.dao.pjtavl.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlAreqLnkItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the Change-Information approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PjtAvlAreqLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_PJT_AVL_AREQ_LNK;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * pjt-ID's
	 */
	public String[] pjtIds = null;
	/**
	 * project-approval-sequence-number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * project-approval-sequence-number's
	 */
	public String[] pjtAvlSeqNos = null;
	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * areq-ID's
	 */
	public String[] areqIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(PjtAvlAreqLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public PjtAvlAreqLnkCondition(){
		super(attr);
	}

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}

	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	    super.append(PjtAvlAreqLnkItems.pjtId, pjtId );
	}

	/**
	 * pjt-ID'sを取得します。
	 * @return pjt-ID's
	 */
	public String[] getPjtIds() {
	    return pjtIds;
	}

	/**
	 * pjt-ID'sを設定します。
	 * @param pjtIds pjt-ID's
	 */
	public void setPjtIds(String[] pjtIds) {
	    this.pjtIds = pjtIds;
	    super.append( PjtAvlAreqLnkItems.pjtId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtIds, PjtAvlAreqLnkItems.pjtId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-sequence-numberを取得します。
	 * @return project-approval-sequence-number
	 */
	public String getPjtAvlSeqNo() {
	    return pjtAvlSeqNo;
	}

	/**
	 * project-approval-sequence-numberを設定します。
	 * @param pjtAvlSeqNo project-approval-sequence-number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
	    this.pjtAvlSeqNo = pjtAvlSeqNo;
	    super.append(PjtAvlAreqLnkItems.pjtAvlSeqNo, pjtAvlSeqNo );
	}

	/**
	 * project-approval-sequence-number'sを取得します。
	 * @return project-approval-sequence-number's
	 */
	public String[] getPjtLnkSeqNos() {
	    return pjtAvlSeqNos;
	}

	/**
	 * project-approval-sequence-number'sを設定します。
	 * @param pjtAvlSeqNos project-approval-sequence-number's
	 */
	public void setPjtAvlSeqNos(String[] pjtAvlSeqNos) {
	    this.pjtAvlSeqNos = pjtAvlSeqNos;
	    super.append( PjtAvlAreqLnkItems.pjtAvlSeqNo.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtAvlSeqNos, PjtAvlAreqLnkItems.pjtAvlSeqNo.getItemName(), false, true, false) );
	}

	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}

	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	    super.append(PjtAvlAreqLnkItems.areqId, areqId );
	}

	/**
	 * areq-ID'sを取得します。
	 * @return areq-ID's
	 */
	public String[] getAreqIds() {
	    return areqIds;
	}

	/**
	 * areq-ID'sを設定します。
	 * @param areqId areq-ID's
	 */
	public void setAreqIds(String[] areqIds) {
	    this.areqIds = areqIds;
	    super.append( PjtAvlAreqLnkItems.areqId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(areqIds, PjtAvlAreqLnkItems.areqId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( PjtAvlAreqLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}