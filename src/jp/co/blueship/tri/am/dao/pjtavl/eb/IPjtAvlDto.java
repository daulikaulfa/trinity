package jp.co.blueship.tri.am.dao.pjtavl.eb;

import java.util.List;

import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;

/**
 * The interface of the pjt-avl DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPjtAvlDto {
	/**
	 * 変更情報を取得します。
	 *
	 * @return 変更情報Entity
	 */
	public IPjtEntity getPjtEntity();
	/**
	 * 変更情報を設定します。
	 *
	 * @param pjtEntity 変更情報Entity
	 */
	public void setPjtEntity(IPjtEntity pjtEntity);

	/**
	 * 最新の変更承認を取得します。
	 *
	 * @return 最新の変更承認Entity
	 */
	public IPjtAvlEntity getPjtAvlEntity();
	/**
	 * 最新の変更承認を設定します。
	 *
	 * @param pjtAvlEntity 変更承認Entity
	 */
	public void setPjtAvlEntity(IPjtAvlEntity pjtAvlEntity);
	/**
	 * 変更承認・申請を取得します。
	 * @return 変更承認・申請Entity
	 */
	public List<IPjtAvlAreqLnkEntity> getPjtAvlAreqLnkEntityList();
	/**
	 * 変更承認・申請を設定します。
	 * @param pjtAvlAreqLnkEntity 変更承認・申請
	 */
	public void setPjtAvlAreqLnkEntityList(List<IPjtAvlAreqLnkEntity> pjtAvlAreqLnkEntity );
}
