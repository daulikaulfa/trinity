package jp.co.blueship.tri.am.dao.pjtavl.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * project approval asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PjtAvlAreqLnkEntity extends EntityFooter implements IPjtAvlAreqLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * project approval sequence number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * areq-ID
	 */
	public String areqId = null;

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * project approval sequence numberを取得します。
	 * @return project approval sequence number
	 */
	public String getPjtAvlSeqNo() {
	    return pjtAvlSeqNo;
	}
	/**
	 * project approval sequence numberを設定します。
	 * @param pjtAvlSeqNo project approval sequence number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
	    this.pjtAvlSeqNo = pjtAvlSeqNo;
	}
	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}

}
