package jp.co.blueship.tri.am.dao.pjtavl.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the Change-Information approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum PjtAvlItems implements ITableItem {
	pjtId("pjt_id"),
	pjtAvlSeqNo("pjt_avl_seq_no"),
	lotId("lot_id"),
	stsId("sts_id"),
	pjtAvlRejectUserId("pjt_avl_reject_user_id"),
	pjtAvlRejectUserNm("pjt_avl_reject_user_nm"),
	pjtAvlRejectTimestamp("pjt_avl_reject_timestamp"),
	pjtAvlRejectCmt("pjt_avl_reject_cmt"),
	pjtAvlUserId("pjt_avl_user_id"),
	pjtAvlUserNm("pjt_avl_user_nm"),
	pjtAvlTimestamp("pjt_avl_timestamp"),
	pjtAvlCmt("pjt_avl_cmt"),
	pjtAvlCancelUserId("pjt_avl_cancel_user_id"),
	pjtAvlCancelUserNm("pjt_avl_cancel_user_nm"),
	pjtAvlCancelTimestamp("pjt_avl_cancel_timestamp"),
	pjtAvlCancelCmt("pjt_avl_cancel_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private PjtAvlItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
