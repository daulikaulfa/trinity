package jp.co.blueship.tri.am.dao.pjtavl;

import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;


/**
 * The interface of the Change-Information approve DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPjtAvlDao extends IJdbcDao<IPjtAvlEntity> {

	public IEntityLimit<IPjtAvlEntity> findListPjtAvl(ISqlCondition condition, ISqlSort sort, int pageNo, int viewRows) throws TriJdbcDaoException;
}
