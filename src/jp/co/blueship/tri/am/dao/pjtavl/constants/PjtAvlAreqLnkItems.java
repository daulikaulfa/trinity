package jp.co.blueship.tri.am.dao.pjtavl.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the Change-Information approval asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum PjtAvlAreqLnkItems implements ITableItem {
	pjtId("pjt_id"),
	pjtAvlSeqNo("pjt_avl_seq_no"),
	areqId("areq_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private PjtAvlAreqLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
