package jp.co.blueship.tri.am.dao.pjtavl.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The SQL condition of the Change-Information approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PjtAvlCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_PJT_AVL;
	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * pjt-ID's
	 */
	public String[] pjtIds = null;
	/**
	 * project-approval-sequence-number
	 */
	public String pjtAvlSeqNo = null;
	/**
	 * project-approval-sequence-number's
	 */
	public String[] pjtAvlSeqNos = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * project-approval-reject-user-ID
	 */
	public String pjtAvlRejectUserId = null;
	/**
	 * project-approval-reject-user-ID's
	 */
	public String[] pjtAvlRejectUserIds = null;
	/**
	 * project-approval-reject-user-name
	 */
	public String pjtAvlRejectUserNm = null;
	/**
	 * project-approval-reject-time-stamp
	 */
	public Timestamp pjtAvlRejectTimestamp = null;
	/**
	 * project-approval-reject-comment
	 */
	public String pjtAvlRejectCmt = null;
	/**
	 * project-approval-user-ID
	 */
	public String pjtAvlUserId = null;
	/**
	 * project-approval-user-ID's
	 */
	public String[] pjtAvlUserIds = null;
	/**
	 * project-approval-user-name
	 */
	public String pjtAvlUserNm = null;
	/**
	 * project-approval-time-stamp
	 */
	public Timestamp pjtAvlTimestamp = null;
	/**
	 * project-approval-comment
	 */
	public String pjtAvlCmt = null;
	/**
	 * project-approval-cancel-user-ID
	 */
	public String pjtAvlCancelUserId = null;
	/**
	 * project-approval-cancel-user-ID's
	 */
	public String[] pjtAvlCancelUserIds = null;
	/**
	 * project-approval-cancel-user-name
	 */
	public String pjtAvlCancelUserNm = null;
	/**
	 * project-approval-cancel-time-stamp
	 */
	public Timestamp pjtAvlCancelTimestamp = null;
	/**
	 * project-approval-cancel-comment
	 */
	public String pjtAvlCancelCmt = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(PjtAvlItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;
	/**
	 * keyword
	 */
	public String[] keywords = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;
	
	public PjtAvlCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( PjtAvlItems.pjtId );
		super.setJoinCtg(true);
		super.setJoinMstone(true);
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId areq-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(PjtAvlItems.lotId, lotId );
	}

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}

	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	    super.append(PjtAvlItems.pjtId, pjtId );
	}

	/**
	 * pjt-ID'sを取得します。
	 * @return pjt-ID's
	 */
	public String[] getPjtIds() {
	    return pjtIds;
	}

	/**
	 * pjt-ID'sを設定します。
	 * @param pjtIds pjt-ID's
	 */
	public void setPjtIds(String[] pjtIds) {
	    this.pjtIds = pjtIds;
	    super.append( PjtAvlItems.pjtId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtIds, PjtAvlItems.pjtId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-sequence-numberを取得します。
	 * @return project-approval-sequence-number
	 */
	public String getPjtAvlSeqNo() {
	    return pjtAvlSeqNo;
	}

	/**
	 * project-approval-sequence-numberを設定します。
	 * @param pjtAvlSeqNo project-approval-sequence-number
	 */
	public void setPjtAvlSeqNo(String pjtAvlSeqNo) {
	    this.pjtAvlSeqNo = pjtAvlSeqNo;
	    super.append(PjtAvlItems.pjtAvlSeqNo, pjtAvlSeqNo );
	}

	/**
	 * project-approval-sequence-number'sを取得します。
	 * @return project-approval-sequence-number's
	 */
	public String[] getPjtAvlSeqNos() {
	    return pjtAvlSeqNos;
	}

	/**
	 * project-approval-sequence-number'sを設定します。
	 * @param pjtAvlSeqNos project-approval-sequence-number's
	 */
	public void setPjtAvlSeqNos(String[] pjtAvlSeqNos) {
	    this.pjtAvlSeqNos = pjtAvlSeqNos;
	    super.append( PjtAvlItems.pjtAvlSeqNo.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtAvlSeqNos, PjtAvlItems.pjtAvlSeqNo.getItemName(), false, true, false) );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(PjtAvlItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( PjtAvlItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, PjtAvlItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-reject-user-IDを取得します。
	 * @return project-approval-reject-user-ID
	 */
	public String getPjtAvlRejectUserId() {
	    return pjtAvlRejectUserId;
	}

	/**
	 * project-approval-reject-user-IDを設定します。
	 * @param pjtAvlRejectUserId project-approval-reject-user-ID
	 */
	public void setPjtAvlRejectUserId(String pjtAvlRejectUserId) {
	    this.pjtAvlRejectUserId = pjtAvlRejectUserId;
	    super.append(PjtAvlItems.pjtAvlRejectUserId, pjtAvlRejectUserId );
	}

	/**
	 * project-approval-reject-user-ID'sを取得します。
	 * @return project-approval-reject-user-ID's
	 */
	public String[] getPjtAvlRejectUserIds() {
	    return pjtAvlRejectUserIds;
	}

	/**
	 * project-approval-reject-user-ID'sを設定します。
	 * @param pjtAvlRejectUserIds project-approval-reject-user-ID's
	 */
	public void setPjtAvlRejectUserIds(String[] pjtAvlRejectUserIds) {
	    this.pjtAvlRejectUserIds = pjtAvlRejectUserIds;
	    super.append( PjtAvlItems.pjtAvlRejectUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtAvlRejectUserIds, PjtAvlItems.pjtAvlRejectUserId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-reject-user-nameを取得します。
	 * @return project-approval-reject-user-name
	 */
	public String getPjtAvlRejectUserNm() {
	    return pjtAvlRejectUserNm;
	}

	/**
	 * project-approval-reject-user-nameを設定します。
	 * @param pjtAvlRejectUserNm project-approval-reject-user-name
	 */
	public void setPjtAvlRejectUserNm(String pjtAvlRejectUserNm) {
	    this.pjtAvlRejectUserNm = pjtAvlRejectUserNm;
	    super.append(PjtAvlItems.pjtAvlRejectUserNm, pjtAvlRejectUserNm );
	}

	/**
	 * project-approval-reject-time-stampを取得します。
	 * @return project-approval-reject-time-stamp
	 */
	public Timestamp getPjtAvlRejectTimestamp() {
	    return pjtAvlRejectTimestamp;
	}

	/**
	 * project-approval-reject-time-stampを設定します。
	 * @param pjtAvlRejectTimestamp project-approval-reject-time-stamp
	 */
	public void setPjtAvlRejectTimestamp(Timestamp pjtAvlRejectTimestamp) {
	    this.pjtAvlRejectTimestamp = pjtAvlRejectTimestamp;
	    super.append(PjtAvlItems.pjtAvlRejectTimestamp, pjtAvlRejectTimestamp );
	}

	/**
	 * project-approval-reject-commentを取得します。
	 * @return project-approval-reject-comment
	 */
	public String getPjtAvlRejectCmt() {
	    return pjtAvlRejectCmt;
	}

	/**
	 * project-approval-reject-commentを設定します。
	 * @param pjtAvlRejectCmt project-approval-reject-comment
	 */
	public void setPjtAvlRejectCmt(String pjtAvlRejectCmt) {
	    this.pjtAvlRejectCmt = pjtAvlRejectCmt;
	    super.append(PjtAvlItems.pjtAvlRejectCmt, pjtAvlRejectCmt );
	}

	/**
	 * project-approval-user-IDを取得します。
	 * @return project-approval-user-ID
	 */
	public String getPjtAvlUserId() {
	    return pjtAvlUserId;
	}

	/**
	 * project-approval-user-IDを設定します。
	 * @param pjtAvlUserId project-approval-user-ID
	 */
	public void setPjtAvlUserId(String pjtAvlUserId) {
	    this.pjtAvlUserId = pjtAvlUserId;
	    super.append(PjtAvlItems.pjtAvlUserId, pjtAvlUserId );
	}

	/**
	 * project-approval-user-ID'sを取得します。
	 * @return project-approval-user-ID's
	 */
	public String[] getPjtAvlUserIds() {
	    return pjtAvlUserIds;
	}

	/**
	 * project-approval-user-ID'sを設定します。
	 * @param pjtAvlUserIds project-approval-user-ID's
	 */
	public void setPjtAvlUserIds(String[] pjtAvlUserIds) {
	    this.pjtAvlUserIds = pjtAvlUserIds;
	    super.append( PjtAvlItems.pjtAvlUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtAvlUserIds, PjtAvlItems.pjtAvlUserId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-user-nameを取得します。
	 * @return project-approval-user-name
	 */
	public String getPjtAvlUserNm() {
	    return pjtAvlUserNm;
	}

	/**
	 * project-approval-user-nameを設定します。
	 * @param pjtAvlUserNm project-approval-user-name
	 */
	public void setPjtAvlUserNm(String pjtAvlUserNm) {
	    this.pjtAvlUserNm = pjtAvlUserNm;
	    super.append(PjtAvlItems.pjtAvlUserNm, pjtAvlUserNm );
	}

	/**
	 * project-approval-time-stampを取得します。
	 * @return project-approval-time-stamp
	 */
	public Timestamp getPjtAvlTimestamp() {
	    return pjtAvlTimestamp;
	}

	/**
	 * project-approval-time-stampを設定します。
	 * @param pjtAvlTimestamp project-approval-time-stamp
	 */
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp) {
	    this.pjtAvlTimestamp = pjtAvlTimestamp;
	    super.append(PjtAvlItems.pjtAvlTimestamp, pjtAvlTimestamp );
	}

	/**
	 * project-approval-commentを取得します。
	 * @return project-approval-comment
	 */
	public String getPjtAvlCmt() {
	    return pjtAvlCmt;
	}

	/**
	 * project-approval-commentを設定します。
	 * @param pjtAvlCmt project-approval-comment
	 */
	public void setPjtAvlCmt(String pjtAvlCmt) {
	    this.pjtAvlCmt = pjtAvlCmt;
	    super.append(PjtAvlItems.pjtAvlCmt, pjtAvlCmt );
	}

	/**
	 * project-approval-cancel-user-IDを取得します。
	 * @return project-approval-cancel-user-ID
	 */
	public String getPjtAvlCancelUserId() {
	    return pjtAvlCancelUserId;
	}

	/**
	 * project-approval-cancel-user-IDを設定します。
	 * @param pjtAvlCancelUserId project-approval-cancel-user-ID
	 */
	public void setPjtAvlCancelUserId(String pjtAvlCancelUserId) {
	    this.pjtAvlCancelUserId = pjtAvlCancelUserId;
	    super.append(PjtAvlItems.pjtAvlCancelUserId, pjtAvlCancelUserId );
	}

	/**
	 * project-approval-cancel-user-ID'sを取得します。
	 * @return project-approval-cancel-user-ID's
	 */
	public String[] getPjtAvlCancelUserIds() {
	    return pjtAvlCancelUserIds;
	}

	/**
	 * project-approval-cancel-user-ID'sを設定します。
	 * @param pjtAvlCancelUserIds project-approval-cancel-user-ID's
	 */
	public void setPjtAvlCancelUserIds(String[] pjtAvlCancelUserIds) {
	    this.pjtAvlCancelUserIds = pjtAvlCancelUserIds;
	    super.append( PjtAvlItems.pjtAvlCancelUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtAvlCancelUserIds, PjtAvlItems.pjtAvlCancelUserId.getItemName(), false, true, false) );
	}

	/**
	 * project-approval-cancel-user-nameを取得します。
	 * @return project-approval-cancel-user-name
	 */
	public String getPjtAvlCancelUserNm() {
	    return pjtAvlCancelUserNm;
	}

	/**
	 * project-approval-cancel-user-nameを設定します。
	 * @param pjtAvlCancelUserNm project-approval-cancel-user-name
	 */
	public void setPjtAvlCancelUserNm(String pjtAvlCancelUserNm) {
	    this.pjtAvlCancelUserNm = pjtAvlCancelUserNm;
	    super.append(PjtAvlItems.pjtAvlCancelUserNm, pjtAvlCancelUserNm );
	}

	/**
	 * project-approval-cancel-time-stampを取得します。
	 * @return project-approval-cancel-time-stamp
	 */
	public Timestamp getPjtAvlCancelTimestamp() {
	    return pjtAvlCancelTimestamp;
	}

	/**
	 * project-approval-cancel-time-stampを設定します。
	 * @param pjtAvlCancelTimestamp project-approval-cancel-time-stamp
	 */
	public void setPjtAvlCancelTimestamp(Timestamp pjtAvlCancelTimestamp) {
	    this.pjtAvlCancelTimestamp = pjtAvlCancelTimestamp;
	    super.append(PjtAvlItems.pjtAvlCancelTimestamp, pjtAvlCancelTimestamp );
	}

	/**
	 * project-approval-cancel-commentを取得します。
	 * @return project-approval-cancel-comment
	 */
	public String getPjtAvlCancelCmt() {
	    return pjtAvlCancelCmt;
	}

	/**
	 * project-approval-cancel-commentを設定します。
	 * @param pjtAvlCancelCmt project-approval-cancel-comment
	 */
	public void setPjtAvlCancelCmt(String pjtAvlCancelCmt) {
	    this.pjtAvlCancelCmt = pjtAvlCancelCmt;
	    super.append(PjtAvlItems.pjtAvlCancelCmt, pjtAvlCancelCmt );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( PjtAvlItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(PjtAvlItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( PjtAvlItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, PjtAvlItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(PjtAvlItems.regUserNm, regUserNm );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(PjtAvlItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(PjtAvlItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( PjtAvlItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, PjtAvlItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(PjtAvlItems.updUserNm, updUserNm );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(PjtAvlItems.updTimestamp, updTimestamp );
	}

	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

	    this.keywords = keywords;
	    super.append( PjtAvlItems.lotId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[]{
	    			SqlFormatUtils.getCondition( this.keywords, PjtAvlItems.pjtId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtAvlItems.pjtAvlSeqNo.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtAvlItems.stsId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtAvlItems.regUserNm.getItemName(), true, false, false),
	    			}));
	}
	
	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}
}