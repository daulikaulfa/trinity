package jp.co.blueship.tri.am.dao.pjtavl;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlAreqLnkItems;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlAreqLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlAreqLnkEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the Change-Information approval asset request link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class PjtAvlAreqLnkDao extends JdbcBaseDao<IPjtAvlAreqLnkEntity> implements IPjtAvlAreqLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_PJT_AVL_AREQ_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IPjtAvlAreqLnkEntity entity ) {
		builder
			.append(PjtAvlAreqLnkItems.pjtId, entity.getPjtId(), true)
			.append(PjtAvlAreqLnkItems.pjtAvlSeqNo, entity.getPjtAvlSeqNo(), true)
			.append(PjtAvlAreqLnkItems.areqId, entity.getAreqId(), true)
			.append(PjtAvlAreqLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(PjtAvlAreqLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(PjtAvlAreqLnkItems.regUserId, entity.getRegUserId())
			.append(PjtAvlAreqLnkItems.regUserNm, entity.getRegUserNm())
			.append(PjtAvlAreqLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(PjtAvlAreqLnkItems.updUserId, entity.getUpdUserId())
			.append(PjtAvlAreqLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IPjtAvlAreqLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IPjtAvlAreqLnkEntity entity = new PjtAvlAreqLnkEntity();

		entity.setPjtId( rs.getString(PjtAvlAreqLnkItems.pjtId.getItemName()) );
		entity.setPjtAvlSeqNo( rs.getString(PjtAvlAreqLnkItems.pjtAvlSeqNo.getItemName()) );
		entity.setAreqId( rs.getString(PjtAvlAreqLnkItems.areqId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(PjtAvlAreqLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(PjtAvlAreqLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(PjtAvlAreqLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(PjtAvlAreqLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(PjtAvlAreqLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(PjtAvlAreqLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(PjtAvlAreqLnkItems.updUserNm.getItemName()) );

		return entity;
	}

}
