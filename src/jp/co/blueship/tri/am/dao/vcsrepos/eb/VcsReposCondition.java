package jp.co.blueship.tri.am.dao.vcsrepos.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.vcsrepos.constants.VcsReposItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the vcs repository entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class VcsReposCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_VCS_REPOS;

	/**
	 * vcs-repos-ID
	 */
	public String vcsReposId = null;
	/**
	 * vcs-repos-ID's
	 */
	public String[] vcsReposIds = null;
	/**
	 * vcs-catalog-code
	 */
	public String vcsCtgCd = null;
	/**
	 * vcs-catalog-code's
	 */
	public String[] vcsCtgCds = null;
	/**
	 * vcs-repository-url
	 */
	public String vcsReposUrl = null;
	/**
	 * vcs-repository-url's
	 */
	public String[] vcsReposUrls = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(VcsReposItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public VcsReposCondition(){
		super(attr);
	}

	/**
	 * vcs-repos-IDを取得します。
	 * @return vcs-repos-ID
	 */
	public String getVcsReposId() {
	    return vcsReposId;
	}

	/**
	 * vcs-repos-IDを設定します。
	 * @param vcsReposId vcs-repos-ID
	 */
	public void setVcsReposId(String vcsReposId) {
	    this.vcsReposId = vcsReposId;
	    super.append(VcsReposItems.vcsReposId, vcsReposId );
	}

	/**
	 * vcs-repos-ID'sを取得します。
	 * @return vcs-repos-ID's
	 */
	public String[] getVcsReposIds() {
	    return vcsReposIds;
	}

	/**
	 * vcs-repos-ID'sを設定します。
	 * @param vcsReposIds vcs-repos-ID's
	 */
	public void setVcsReposIds(String[] vcsReposIds) {
	    this.vcsReposIds = vcsReposIds;
	    super.append( VcsReposItems.vcsReposId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(vcsReposIds, VcsReposItems.vcsReposId.getItemName(), false, true, false) );
	}

	/**
	 * vcs-catalog-codeを取得します。
	 * @return vcs-catalog-code
	 */
	public String getVcsCtgCd() {
	    return vcsCtgCd;
	}

	/**
	 * vcs-catalog-codeを設定します。
	 * @param vcsCtgCd vcs-catalog-code
	 */
	public void setVcsCtgCd(String vcsCtgCd) {
	    this.vcsCtgCd = vcsCtgCd;
	    super.append(VcsReposItems.vcsCtgCd, vcsCtgCd );
	}

	/**
	 * vcs-catalog-code'sを取得します。
	 * @return vcs-catalog-code's
	 */
	public String[] getPjtLnkSeqNos() {
	    return vcsCtgCds;
	}

	/**
	 * vcs-catalog-code'sを設定します。
	 * @param vcsCtgCds vcs-catalog-code's
	 */
	public void setVcsCtgCds(String[] vcsCtgCds) {
	    this.vcsCtgCds = vcsCtgCds;
	    super.append( VcsReposItems.vcsCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(vcsCtgCds, VcsReposItems.vcsCtgCd.getItemName(), false, true, false) );
	}

	/**
	 * vcs-repository-urlを取得します。
	 * @return vcs-repository-url
	 */
	public String getVcsReposUrl() {
	    return vcsReposUrl;
	}

	/**
	 * vcs-repository-urlを設定します。
	 * @param vcsReposUrl vcs-repository-url
	 */
	public void setVcsReposUrl(String vcsReposUrl) {
	    this.vcsReposUrl = vcsReposUrl;
	    super.append(VcsReposItems.vcsReposUrl, vcsReposUrl );
	}

	/**
	 * vcs-repository-url'sを取得します。
	 * @return vcs-repository-url's
	 */
	public String[] getVcsReposUrls() {
	    return vcsReposUrls;
	}

	/**
	 * vcs-repository-url'sを設定します。
	 * @param vcsReposUrl vcs-repository-url's
	 */
	public void setVcsReposUrls(String[] vcsReposUrls) {
	    this.vcsReposUrls = vcsReposUrls;
	    super.append( VcsReposItems.vcsReposUrl.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(vcsReposUrls, VcsReposItems.vcsReposUrl.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( VcsReposItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}