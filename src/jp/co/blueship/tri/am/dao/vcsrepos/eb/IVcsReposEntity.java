package jp.co.blueship.tri.am.dao.vcsrepos.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * The interface of the vcs repository entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IVcsReposEntity extends IEntity {

	public String getVcsReposId();
	public void setVcsReposId(String vcsReposId);

	public String getVcsCtgCd();
	public void setVcsCtgCd(String vcsCtgCd);

	public String getVcsReposUrl();
	public void setVcsReposUrl(String vcsReposUrl);

	public String getVcsUserNm();
	public void setVcsUserNm(String vcsUserNm);

	public StatusFlg getDelStsId();
	public void setDelStsId(StatusFlg delStsId);

	public String getRegUserId();
	public void setRegUserId(String regUserId);

	public String getRegUserNm();
	public void setRegUserNm(String regUserNm);

	public Timestamp getRegTimestamp();
	public void setRegTimestamp(Timestamp regTimestamp);

	public String getUpdUserId();
	public void setUpdUserId(String updUserId);

	public String getUpdUserNm();
	public void setUpdUserNm(String updUserNm);

	public Timestamp getUpdTimestamp();
	public void setUpdTimestamp(Timestamp updTimestamp);
}
