package jp.co.blueship.tri.am.dao.vcsrepos;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.vcsrepos.constants.VcsReposItems;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.VcsReposEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the vcs repository DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class VcsReposDao extends JdbcBaseDao<IVcsReposEntity> implements IVcsReposDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_VCS_REPOS;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IVcsReposEntity entity ) {
		builder
			.append(VcsReposItems.vcsReposId, entity.getVcsReposId(), true)
			.append(VcsReposItems.vcsCtgCd, entity.getVcsCtgCd(), true)
			.append(VcsReposItems.vcsReposUrl, entity.getVcsReposUrl())
			.append(VcsReposItems.vcsUserNm, entity.getVcsUserNm())
			.append(VcsReposItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(VcsReposItems.regTimestamp, entity.getRegTimestamp())
			.append(VcsReposItems.regUserId, entity.getRegUserId())
			.append(VcsReposItems.regUserNm, entity.getRegUserNm())
			.append(VcsReposItems.updTimestamp, entity.getUpdTimestamp())
			.append(VcsReposItems.updUserId, entity.getUpdUserId())
			.append(VcsReposItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IVcsReposEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IVcsReposEntity entity = new VcsReposEntity();

		entity.setVcsReposId( rs.getString(VcsReposItems.vcsReposId.getItemName()) );
		entity.setVcsCtgCd( rs.getString(VcsReposItems.vcsCtgCd.getItemName()) );
		entity.setVcsReposUrl( rs.getString(VcsReposItems.vcsReposUrl.getItemName()) );
		entity.setVcsUserNm( rs.getString(VcsReposItems.vcsUserNm.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(VcsReposItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(VcsReposItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(VcsReposItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(VcsReposItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(VcsReposItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(VcsReposItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(VcsReposItems.updUserNm.getItemName()) );

		return entity;
	}

}
