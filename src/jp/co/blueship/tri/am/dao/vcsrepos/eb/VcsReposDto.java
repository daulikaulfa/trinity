package jp.co.blueship.tri.am.dao.vcsrepos.eb;


/**
 * vcs repository DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class VcsReposDto implements IVcsReposDto {
	/**
	 * VCS・リポジトリ
	 */
	public IVcsReposEntity vcsReposEntity = null;
	/**
	 * VCS・モジュール
	 */
	public IVcsMdlEntity vcsMdlEntity = null;

	/**
	 * VCS・リポジトリを取得します。
	 * @return VCS・リポジトリ
	 */
	public IVcsReposEntity getVcsReposEntity() {
	    return vcsReposEntity;
	}
	/**
	 * VCS・リポジトリを設定します。
	 * @param vcsReposEntity VCS・リポジトリ
	 */
	public void setVcsReposEntity(IVcsReposEntity vcsReposEntity) {
	    this.vcsReposEntity = vcsReposEntity;
	}
	/**
	 * VCS・モジュールを取得します。
	 * @return VCS・モジュール
	 */
	public IVcsMdlEntity getVcsMdlEntity() {
	    return vcsMdlEntity;
	}
	/**
	 * VCS・モジュールを設定します。
	 * @param vcsMdlEntity VCS・モジュール
	 */
	public void setVcsMdlEntity(IVcsMdlEntity vcsMdlEntity) {
	    this.vcsMdlEntity = vcsMdlEntity;
	}

}
