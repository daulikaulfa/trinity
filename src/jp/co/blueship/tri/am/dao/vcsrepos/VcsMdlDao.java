package jp.co.blueship.tri.am.dao.vcsrepos;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.vcsrepos.constants.VcsMdlItems;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.VcsMdlEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the vcs module DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class VcsMdlDao extends JdbcBaseDao<IVcsMdlEntity> implements IVcsMdlDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_VCS_MDL;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IVcsMdlEntity entity ) {
		builder
			.append(VcsMdlItems.mdlNm, entity.getMdlNm(), true)
			.append(VcsMdlItems.vcsReposId, entity.getVcsReposId())
			.append(VcsMdlItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(VcsMdlItems.regTimestamp, entity.getRegTimestamp())
			.append(VcsMdlItems.regUserId, entity.getRegUserId())
			.append(VcsMdlItems.regUserNm, entity.getRegUserNm())
			.append(VcsMdlItems.updTimestamp, entity.getUpdTimestamp())
			.append(VcsMdlItems.updUserId, entity.getUpdUserId())
			.append(VcsMdlItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IVcsMdlEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IVcsMdlEntity entity = new VcsMdlEntity();

		entity.setMdlNm( rs.getString(VcsMdlItems.mdlNm.getItemName()) );
		entity.setVcsReposId( rs.getString(VcsMdlItems.vcsReposId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(VcsMdlItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(VcsMdlItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(VcsMdlItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(VcsMdlItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(VcsMdlItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(VcsMdlItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(VcsMdlItems.updUserNm.getItemName()) );

		return entity;
	}

}
