package jp.co.blueship.tri.am.dao.vcsrepos.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the vcs repository entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum VcsReposItems implements ITableItem {
	vcsReposId("vcs_repos_id"),
	vcsCtgCd("vcs_ctg_cd"),
	vcsReposUrl("vcs_repos_url"),
	vcsUserNm("vcs_user_nm"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private VcsReposItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
