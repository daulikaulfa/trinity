package jp.co.blueship.tri.am.dao.vcsrepos.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.vcsrepos.constants.VcsMdlItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the Change-Information approval entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class VcsMdlCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_VCS_MDL;

	/**
	 * module-name
	 */
	public String mdlNm = null;
	/**
	 * module-name's
	 */
	public String[] mdlNms = null;
	/**
	 * vcs-repos-ID
	 */
	public String vcsReposId = null;
	/**
	 * vcs-repos-ID's
	 */
	public String[] vcsReposIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(VcsMdlItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public VcsMdlCondition(){
		super(attr);
	}

	/**
	 * module-nameを取得します。
	 * @return module-name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}

	/**
	 * module-nameを設定します。
	 * @param mdlNm module-name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	    super.append(VcsMdlItems.mdlNm, mdlNm );
	}

	/**
	 * module-name'sを取得します。
	 * @return module-name's
	 */
	public String[] getMdlNms() {
	    return mdlNms;
	}

	/**
	 * module-name'sを設定します。
	 * @param mdlNms module-name's
	 */
	public void setMdlNms(String[] mdlNms) {
	    this.mdlNms = mdlNms;
	    super.append( VcsMdlItems.mdlNm.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mdlNms, VcsMdlItems.mdlNm.getItemName(), false, true, false) );
	}

	/**
	 * vcs-repos-IDを取得します。
	 * @return vcs-repos-ID
	 */
	public String getVcsReposId() {
	    return vcsReposId;
	}

	/**
	 * vcs-repos-IDを設定します。
	 * @param vcsReposId vcs-repos-ID
	 */
	public void setVcsReposId(String vcsReposId) {
	    this.vcsReposId = vcsReposId;
	    super.append(VcsMdlItems.vcsReposId, vcsReposId );
	}

	/**
	 * vcs-repos-ID'sを取得します。
	 * @return vcs-repos-ID's
	 */
	public String[] getPjtLnkSeqNos() {
	    return vcsReposIds;
	}

	/**
	 * vcs-repos-ID'sを設定します。
	 * @param vcsReposIds vcs-repos-ID's
	 */
	public void setVcsReposIds(String[] vcsReposIds) {
	    this.vcsReposIds = vcsReposIds;
	    super.append( VcsMdlItems.vcsReposId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(vcsReposIds, VcsMdlItems.vcsReposId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( VcsMdlItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}