package jp.co.blueship.tri.am.dao.vcsrepos;

import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the vcs module DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IVcsMdlDao extends IJdbcDao<IVcsMdlEntity> {

}
