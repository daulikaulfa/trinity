package jp.co.blueship.tri.am.dao.vcsrepos;

import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the vcs repository DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IVcsReposDao extends IJdbcDao<IVcsReposEntity> {

}
