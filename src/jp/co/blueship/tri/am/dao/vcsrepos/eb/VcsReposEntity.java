package jp.co.blueship.tri.am.dao.vcsrepos.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * vcs repository entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class VcsReposEntity implements IVcsReposEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * vcs repos-ID
	 */
	public String vcsReposId = null;
	/**
	 * vcs catalog code
	 */
	public String vcsCtgCd = null;
	/**
	 * vcs repository url
	 */
	public String vcsReposUrl = null;
	/**
	 * vcs user name
	 */
	public String vcsUserNm = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	/**
	 * vcs repos-IDを取得します。
	 * @return vcs repos-ID
	 */
	public String getVcsReposId() {
	    return vcsReposId;
	}
	/**
	 * vcs repos-IDを設定します。
	 * @param vcsReposId vcs repos-ID
	 */
	public void setVcsReposId(String vcsReposId) {
	    this.vcsReposId = vcsReposId;
	}
	/**
	 * vcs catalog codeを取得します。
	 * @return vcs catalog code
	 */
	public String getVcsCtgCd() {
	    return vcsCtgCd;
	}
	/**
	 * vcs catalog codeを設定します。
	 * @param vcsCtgCd vcs catalog code
	 */
	public void setVcsCtgCd(String vcsCtgCd) {
	    this.vcsCtgCd = vcsCtgCd;
	}
	/**
	 * vcs repository urlを取得します。
	 * @return vcs repository url
	 */
	public String getVcsReposUrl() {
	    return vcsReposUrl;
	}
	/**
	 * vcs repository urlを設定します。
	 * @param vcsReposUrl vcs repository url
	 */
	public void setVcsReposUrl(String vcsReposUrl) {
	    this.vcsReposUrl = vcsReposUrl;
	}
	/**
	 * vcs user nameを取得します。
	 * @return vcs user name
	 */
	public String getVcsUserNm() {
	    return vcsUserNm;
	}
	/**
	 * vcs user nameを設定します。
	 * @param vcsUserNm vcs user name
	 */
	public void setVcsUserNm(String vcsUserNm) {
	    this.vcsUserNm = vcsUserNm;
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}
	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	}
	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}
	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	}
	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}
	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	}
	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}
	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	}
	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}
	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	}
	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}
	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	}
	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}
	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	}
}
