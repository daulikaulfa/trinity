package jp.co.blueship.tri.am.dao.vcsrepos.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * The interface of the vcs module entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IVcsMdlEntity extends IEntity {

	public String getMdlNm();
	public void setMdlNm(String mdlNm);

	public String getVcsReposId();
	public void setVcsReposId(String vcsReposId);

	public StatusFlg getDelStsId();
	public void setDelStsId(StatusFlg delStsId);

	public String getRegUserId();
	public void setRegUserId(String regUserId);

	public String getRegUserNm();
	public void setRegUserNm(String regUserNm);

	public Timestamp getRegTimestamp();
	public void setRegTimestamp(Timestamp regTimestamp);

	public String getUpdUserId();
	public void setUpdUserId(String updUserId);

	public String getUpdUserNm();
	public void setUpdUserNm(String updUserNm);

	public Timestamp getUpdTimestamp();
	public void setUpdTimestamp(Timestamp updTimestamp);

}
