package jp.co.blueship.tri.am.dao.vcsrepos.eb;



/**
 * The interface of the vcs repository DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IVcsReposDto {
	/**
	 * VCS・リポジトリを取得します。
	 *
	 * @return VCS・リポジトリEntity
	 */
	public IVcsReposEntity getVcsReposEntity();
	/**
	 * VCS・リポジトリを設定します。
	 *
	 * @param vcsReposEntity VCS・リポジトリEntity
	 */
	public void setVcsReposEntity(IVcsReposEntity vcsReposEntity);
	/**
	 * VCS・モジュールを取得します。
	 *
	 * @return VCS・モジュールEntity
	 */
	public IVcsMdlEntity getVcsMdlEntity();
	/**
	 * VCS・モジュールを設定します。
	 *
	 * @param vcsReposEntity VCS・モジュールEntity
	 */
	public void setVcsMdlEntity(IVcsMdlEntity vcsMdlEntity);

}
