package jp.co.blueship.tri.am.dao.pjt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the Change-Information entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IPjtEntity extends IEntityFooter, IEntityExecData {

	public String getPjtId();
	public void setPjtId(String pjtId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getPjtNm();
	public void setPjtNm(String pjtNm);

	public String getSummary();
	public void setSummary(String summary);

	public String getContent();
	public void setContent(String content);

	public String getChgFactorNo();
	public void setChgFactorNo(String chgFactorNo);

	public String getChgFactorId();
	public void setChgFactorId(String chgFactorId);

	public String getStsId();
	public void setStsId(String stsId);

	public String getPjtLatestAvlSeqNo();
	public void setPjtLatestAvlSeqNo(String pjtLatestAvlSeqNo);

	public Timestamp getPjtAvlTimestamp();
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp);

	public String getCloseUserId();
	public void setCloseUserId(String closeUserId);

	public String getCloseUserNm();
	public void setCloseUserNm(String closeUserNm);

	public Timestamp getCloseTimestamp();
	public void setCloseTimestamp(Timestamp closeTimestamp);

	public String getCloseCmt();
	public void setCloseCmt(String closeCmt);

	public String getTestCompUserId();
	public void setTestCompUserId(String testCompUserId);

	public String getTestCompUserNm();
	public void setTestCompUserNm(String testCompUserNm);

	public Timestamp getTestCompTimestamp();
	public void setTestCompTimestamp(Timestamp testCompTimestamp);

	public String getTestCompCmt();
	public void setTestCompCmt(String closeCmt);

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public String getAssigneeId();
	public void setAssigneeId(String assigneeId);

	public String getAssigneeNm();
	public void setAssigneeNm(String assigneeNm);

	public String getCtgId();
	public String getCtgNm();
	public String getMstoneId();
	public String getMstoneNm();
	public String getAssigneeIconPath();
	public String getRegUserIconPath();

}
