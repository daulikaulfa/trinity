package jp.co.blueship.tri.am.dao.pjt.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the Change-Information entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum PjtItems implements ITableItem {
	pjtId("pjt_id"),
	lotId("lot_id"),
	pjtNm("pjt_nm"),
	summary("summary"),
	content("content"),
	assigneeId("assignee_id"),
	assigneedNm("assignee_nm"),
	chgFactorNo("chg_factor_no"),
	chgFactorId("chg_factor_id"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	pjtLatestAvlSeqNo("pjt_latest_avl_seq_no"),
	closeUserId("close_user_id"),
	closeUserNm("close_user_nm"),
	closeTimestamp("close_timestamp"),
	closeCmt("close_cmt"),
	testCompUserId("test_comp_user_id"),
	testCompUserNm("test_comp_user_nm"),
	testCompTimestamp("test_comp_timestamp"),
	testCompCmt("test_comp_cmt"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),

	assigneeIconPath("assignee_id_icon_path"),
	regUserIconPath("reg_user_id_icon_path"),
	;

	private String element = null;

	private PjtItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
