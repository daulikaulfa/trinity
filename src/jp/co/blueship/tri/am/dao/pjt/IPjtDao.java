package jp.co.blueship.tri.am.dao.pjt;

import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the Change-Information DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IPjtDao extends IJdbcDao<IPjtEntity> {

}
