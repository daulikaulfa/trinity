package jp.co.blueship.tri.am.dao.pjt;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.constants.PjtAvlItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The implements of the Change-Information DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class PjtDao extends JdbcBaseDao<IPjtEntity> implements IPjtDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_PJT;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IPjtEntity entity ) {
		builder
			.append(PjtItems.pjtId, entity.getPjtId(), true)
			.append(PjtItems.lotId, entity.getLotId())
			.append(PjtItems.pjtNm, entity.getPjtNm())
			.append(PjtItems.summary, entity.getSummary())
			.append(PjtItems.content, entity.getContent())
			.append(PjtItems.assigneeId, entity.getAssigneeId())
			.append(PjtItems.assigneedNm, entity.getAssigneeNm())
			.append(PjtItems.chgFactorNo, entity.getChgFactorNo())
			.append(PjtItems.chgFactorId, entity.getChgFactorId())
			.append(PjtItems.stsId, entity.getStsId())
			.append(PjtItems.pjtLatestAvlSeqNo, entity.getPjtLatestAvlSeqNo())
			.append(PjtItems.closeUserId, entity.getCloseUserId())
			.append(PjtItems.closeUserNm, entity.getCloseUserNm())
			.append(PjtItems.closeTimestamp, entity.getCloseTimestamp())
			.append(PjtItems.closeCmt, entity.getCloseCmt())
			.append(PjtItems.testCompUserId, entity.getTestCompUserId())
			.append(PjtItems.testCompUserNm, entity.getTestCompUserNm())
			.append(PjtItems.testCompTimestamp, entity.getTestCompTimestamp())
			.append(PjtItems.testCompCmt, entity.getTestCompCmt())
			.append(PjtItems.delUserId, entity.getDelUserId())
			.append(PjtItems.delUserNm, entity.getDelUserNm())
			.append(PjtItems.delTimestamp, entity.getDelTimestamp())
			.append(PjtItems.delCmt, entity.getDelCmt())
			.append(PjtItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(PjtItems.regTimestamp, entity.getRegTimestamp())
			.append(PjtItems.regUserId, entity.getRegUserId())
			.append(PjtItems.regUserNm, entity.getRegUserNm())
			.append(PjtItems.updTimestamp, entity.getUpdTimestamp())
			.append(PjtItems.updUserId, entity.getUpdUserId())
			.append(PjtItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IPjtEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		PjtEntity entity = new PjtEntity();

		entity.setPjtId( rs.getString(PjtItems.pjtId.getItemName()) );
		entity.setLotId( rs.getString(PjtItems.lotId.getItemName()) );
		entity.setPjtNm( rs.getString(PjtItems.pjtNm.getItemName()) );
		entity.setSummary( rs.getString(PjtItems.summary.getItemName()) );
		entity.setContent( rs.getString(PjtItems.content.getItemName()) );
		entity.setChgFactorNo( rs.getString(PjtItems.chgFactorNo.getItemName()) );
		entity.setChgFactorId( rs.getString(PjtItems.chgFactorId.getItemName()) );
		entity.setStsId( rs.getString(PjtItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setPjtLatestAvlSeqNo( rs.getString(PjtItems.pjtLatestAvlSeqNo.getItemName()) );
		entity.setCloseUserId( rs.getString(PjtItems.closeUserId.getItemName()) );
		entity.setCloseUserNm( rs.getString(PjtItems.closeUserNm.getItemName()) );
		entity.setCloseTimestamp( rs.getTimestamp(PjtItems.closeTimestamp.getItemName()) );
		entity.setCloseCmt( rs.getString(PjtItems.closeCmt.getItemName()) );
		entity.setTestCompUserId( rs.getString(PjtItems.testCompUserId.getItemName()) );
		entity.setTestCompUserNm( rs.getString(PjtItems.testCompUserNm.getItemName()) );
		entity.setTestCompTimestamp( rs.getTimestamp(PjtItems.testCompTimestamp.getItemName()) );
		entity.setTestCompCmt( rs.getString(PjtItems.testCompCmt.getItemName()) );
		entity.setDelUserId( rs.getString(PjtItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(PjtItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(PjtItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(PjtItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(PjtItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(PjtItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(PjtItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(PjtItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(PjtItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(PjtItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(PjtItems.updUserNm.getItemName()) );

		entity.setAssigneeId(rs.getString(PjtItems.assigneeId.getItemName()));
		entity.setAssigneeNm(rs.getString(PjtItems.assigneedNm.getItemName()));
		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );

		entity.setPjtAvlTimestamp( rs.getTimestamp(PjtAvlItems.pjtAvlTimestamp.getItemName()) );
		entity.setAssigneeIconPath( rs.getString(PjtItems.assigneeIconPath.getItemName()) );
		entity.setRegUserIconPath( rs.getString(PjtItems.regUserIconPath.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,AV.PJT_AVL_TIMESTAMP")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT PJT_ID AS AV_PJT_ID, PJT_AVL_SEQ_NO AS AV_PJT_AVL_SEQ_NO, PJT_AVL_TIMESTAMP, DEL_STS_ID AS AV_DEL_STS_ID FROM AM_PJT_AVL) AV ON PJT_ID = AV.AV_PJT_ID AND PJT_LATEST_AVL_SEQ_NO = AV.AV_PJT_AVL_SEQ_NO AND AV.AV_DEL_STS_ID = FALSE")
			;

			return buf.toString();
		}
	}

}
