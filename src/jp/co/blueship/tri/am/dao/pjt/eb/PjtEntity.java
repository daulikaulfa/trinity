package jp.co.blueship.tri.am.dao.pjt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * Change-Information entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class PjtEntity extends EntityFooter implements IPjtEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category name
	 */
	public String ctgNm = null;
	/**
	 * category ID
	 */
	public String mstoneId = null;
	/**
	 * category ID
	 */
	public String mstoneNm = null;
	/**
	 * assignee Icon Path
	 */
	public String assigneeIconPath = null;
	/**
	 * reg User Icon Path
	 */
	public String regUserIconPath = null;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * project name
	 */
	public String pjtNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * context
	 */
	public String content = null;
	/**
	 * assigneeId
	 */
	public String assigneedId = null;
	/**
	 * assigneeNm
	 */
	public String assigneeNm = null;
	/**
	 * change factor number
	 */
	public String chgFactorNo = null;
	/**
	 * change factor-ID
	 */
	public String chgFactorId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * Latest-Avl-Seq-No
	 */
	public String pjtLatestAvlSeqNo = null;
	/**
	 * project approval time stamp
	 */
	public Timestamp pjtAvlTimestamp = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * test completed user-ID
	 */
	public String testCompUserId = null;
	/**
	 * test completed user name
	 */
	public String testCompUserNm = null;
	/**
	 * test completed time stamp
	 */
	public Timestamp testCompTimestamp = null;
	/**
	 * test completed comment
	 */
	public String testCompCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;

	@Override
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	@Override
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	@Override
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * project nameを取得します。
	 * @return project name
	 */
	public String getPjtNm() {
	    return pjtNm;
	}
	/**
	 * project nameを設定します。
	 * @param pjtNm project name
	 */
	public void setPjtNm(String pjtNm) {
	    this.pjtNm = pjtNm;
	}
	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}
	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	}
	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * change factor numberを取得します。
	 * @return change factor number
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}
	/**
	 * change factor numberを設定します。
	 * @param chgFactorNo change factor number
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	}
	/**
	 * change factor-IDを取得します。
	 * @return change factor-ID
	 */
	public String getChgFactorId() {
	    return chgFactorId;
	}
	/**
	 * change factor-IDを設定します。
	 * @param chgFactorId change factor-ID
	 */
	public void setChgFactorId(String chgFactorId) {
	    this.chgFactorId = chgFactorId;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * process status-IDを取得します。
	 * @return process status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * Latest-Avl-Seq-Noを取得します。
	 * @return Latest-Avl-Seq-No
	 */
	public String getPjtLatestAvlSeqNo() {
	    return pjtLatestAvlSeqNo;
	}
	/**
	 * Latest-Avl-Seq-Noを設定します。
	 * @param pjtLatestAvlSeqNo Latest-Avl-Seq-No
	 */
	public void setPjtLatestAvlSeqNo(String pjtLatestAvlSeqNo) {
	    this.pjtLatestAvlSeqNo = pjtLatestAvlSeqNo;
	}
	/**
	 * project approval time stampを取得します。
	 * @return project approval time stamp
	 */
	public Timestamp getPjtAvlTimestamp() {
	    return pjtAvlTimestamp;
	}
	/**
	 * project approval time stampを設定します。
	 * @param pjtAvlTimestamp project approval time stamp
	 */
	public void setPjtAvlTimestamp(Timestamp pjtAvlTimestamp) {
	    this.pjtAvlTimestamp = pjtAvlTimestamp;
	}
	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}
	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	}
	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}
	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	}
	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}
	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	}
	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}
	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	}
	/**
	 * test completed user-IDを取得します。
	 * @return test completed user-ID
	 */
	public String getTestCompUserId() {
	    return testCompUserId;
	}
	/**
	 * test completed user-IDを設定します。
	 * @param testCompUserId test completed user-ID
	 */
	public void setTestCompUserId(String testCompUserId) {
	    this.testCompUserId = testCompUserId;
	}
	/**
	 * test completed user nameを取得します。
	 * @return test completed user name
	 */
	public String getTestCompUserNm() {
	    return testCompUserNm;
	}
	/**
	 * test completed user nameを設定します。
	 * @param testCompUserNm test completed user name
	 */
	public void setTestCompUserNm(String testCompUserNm) {
	    this.testCompUserNm = testCompUserNm;
	}
	/**
	 * test completed time stampを取得します。
	 * @return test completed time stamp
	 */
	public Timestamp getTestCompTimestamp() {
	    return testCompTimestamp;
	}
	/**
	 * test completed time stampを設定します。
	 * @param testCompTimestamp test completed time stamp
	 */
	public void setTestCompTimestamp(Timestamp testCompTimestamp) {
	    this.testCompTimestamp = testCompTimestamp;
	}
	/**
	 * test completed commentを取得します。
	 * @return test completed comment
	 */
	public String getTestCompCmt() {
	    return testCompCmt;
	}
	/**
	 * test completed commentを設定します。
	 * @param testCompCmt test completed comment
	 */
	public void setTestCompCmt(String testCompCmt) {
	    this.testCompCmt = testCompCmt;
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}
	@Override
	public String getAssigneeId() {
		return assigneedId;
	}
	@Override
	public void setAssigneeId(String assigneeId) {
		this.assigneedId = assigneeId;

	}
	@Override
	public String getAssigneeNm() {
		return assigneeNm;
	}
	@Override
	public void setAssigneeNm(String assigneeNm) {
		this.assigneeNm = assigneeNm;
	}

	@Override
	public String getAssigneeIconPath() {
		return assigneeIconPath;
	}
	public void setAssigneeIconPath(String assigneeIconPath) {
		this.assigneeIconPath = assigneeIconPath;
	}
	@Override
	public String getRegUserIconPath() {
		return regUserIconPath;
	}
	public void setRegUserIconPath(String regUserIconPath) {
		this.regUserIconPath = regUserIconPath;
	}


}
