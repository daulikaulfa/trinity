package jp.co.blueship.tri.am.dao.pjt.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The SQL condition of the Change-Information entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class PjtCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_PJT;
	/**
	 * keyword
	 */
	public String[] keywords = null;

	/**
	 * pjt-ID
	 */
	public String pjtId = null;
	/**
	 * pjt-ID's
	 */
	public String[] pjtIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;

	/**
	 * lot-ID's
	 */
	private String[] lotIds = null;

	/**
	 * pjt name
	 */
	public String pjtNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * change factor number
	 */
	public String chgFactorNo = null;
	/**
	 * change factor-ID
	 */
	public String chgFactorId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user-ID's
	 */
	public String[] closeUserIds = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * t comment
	 */
	public String closeCmt = null;
	/**
	 * test complete user-ID
	 */
	public String testCompUserId = null;
	/**
	 * test complete user-ID's
	 */
	public String[] testCompUserIds = null;
	/**
	 * test complete user name
	 */
	public String testCompUserNm = null;
	/**
	 * test complete time stamp
	 */
	public Timestamp testCompTimestamp = null;
	/**
	 * test complete comment
	 */
	public String testCompCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user-ID's
	 */
	public String[] delUserIds = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;
	/**
	 * assinee ID's
	 */
	private String[] assigneeIds = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(PjtItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

	    this.keywords = keywords;
	    super.append( PjtItems.lotId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[]{
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.pjtId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.chgFactorId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.chgFactorNo.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.summary.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.regUserNm.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keywords, PjtItems.assigneedNm.getItemName(), true, false, false),
	    			}));
	}

	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public PjtCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( PjtItems.pjtId );
		super.setJoinUser( true );
		super.setKeyByJoinUsers(
				PjtItems.regUserId,
				PjtItems.assigneeId );
		super.setJoinCtg( true );
		super.setJoinMstone( true );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( PjtItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, PjtItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( PjtItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, PjtItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}

	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	    super.append(PjtItems.pjtId, pjtId );
	}

	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 * @param isLike 部分検索の場合、true
	 */
	public void setPjtId(String pjtId, boolean isLike) {
	    this.pjtId = pjtId;
	    super.append(PjtItems.pjtId.getItemName(), SqlFormatUtils.getCondition(pjtId, PjtItems.pjtId.getItemName(), isLike) );
	}

	/**
	 * pjt-ID'sを取得します。
	 * @return pjt-ID's
	 */
	public String[] getPjtIds() {
	    return pjtIds;
	}

	/**
	 * pjt-ID'sを設定します。
	 * @param pjtIds pjt-ID's
	 */
	public void setPjtIds(String[] pjtIds) {
	    this.pjtIds = pjtIds;
	    super.append( PjtItems.pjtId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(pjtIds, PjtItems.pjtId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(PjtItems.lotId, lotId );
	}

	/**
	 * @return
	 */
	public String[] getLotIds() {
		return lotIds;
	}

	/**
	 * @param lotIds
	 */
	/**
	 * @param lotIds
	 */
	public void setLotIds(String[] lotIds) {
		this.lotIds = lotIds;
		super.appendByJoinExecData( PjtItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, PjtItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * pjt nameを取得します。
	 * @return pjt-name
	 */
	public String getPjtNm() {
	    return pjtNm;
	}

	/**
	 * pjt nameを設定します。
	 * @param pjtNm pjt-name
	 */
	public void setPjtNm(String pjtNm) {
	    this.pjtNm = pjtNm;
	    super.append(PjtItems.pjtNm, pjtNm );
	}

	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}

	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	    super.append(PjtItems.summary, summary );
	}

	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}

	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	    super.append(PjtItems.content, content );
	}

	/**
	 * change factor numberを取得します。
	 * @return change-factor-number
	 */
	public String getChgFactorNo() {
	    return chgFactorNo;
	}

	/**
	 * change-factor-numberを設定します。
	 * @param chgFactorNo change-factor-number
	 */
	public void setChgFactorNo(String chgFactorNo) {
	    this.chgFactorNo = chgFactorNo;
	    super.append(PjtItems.chgFactorNo, chgFactorNo );
	}

	/**
	 * change-factor-numberを設定します。
	 * @param chgFactorNo change-factor-number
	 * @param isLike 部分検索の場合、true
	 */
	public void setChgFactorNo(String chgFactorNo, boolean isLike) {
		this.chgFactorNo = chgFactorNo;
		super.append(PjtItems.chgFactorNo.getItemName(), SqlFormatUtils.getCondition(chgFactorNo, PjtItems.chgFactorNo.getItemName(), isLike) );
	}

	/**
	 * change-factor-IDを取得します。
	 * @return change-factor-ID
	 */
	public String getChgFactorId() {
	    return chgFactorId;
	}

	/**
	 * change-factor-IDを設定します。
	 * @param change-factor-ID
	 */
	public void setChgFactorId(String chgFactorId) {
	    this.chgFactorId = chgFactorId;
	    super.append(PjtItems.chgFactorId, chgFactorId );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(PjtItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( PjtItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, PjtItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIdsByNotEquals() {
		return stsIds;
	}
	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIdsByNotEquals(String... stsIds) {
		this.stsIds = stsIds;
		super.append( PjtItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, PjtItems.stsId.getItemName(), false, false, true) );
	}

	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}

	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	    super.append(PjtItems.closeUserId, closeUserId );
	}

	/**
	 * close user-ID'sを取得します。
	 * @return close user-ID's
	 */
	public String[] getCloseUserIds() {
	    return closeUserIds;
	}

	/**
	 * close user-ID'sを設定します。
	 * @param closeUserIds close user-ID's
	 */
	public void setCloseUserIds(String[] closeUserIds) {
	    this.closeUserIds = closeUserIds;
	    super.append( PjtItems.closeUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(closeUserIds, PjtItems.closeUserId.getItemName(), false, true, false) );
	}

	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}

	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	    super.append(PjtItems.closeUserNm, closeUserNm );
	}

	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}

	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	    super.append(PjtItems.closeTimestamp, closeTimestamp );
	}

	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}

	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	    super.append(PjtItems.closeCmt, closeCmt );
	}

	/**
	 * cltest completedse user-IDを取得します。
	 * @return test completed user-ID
	 */
	public String getTestCompUserId() {
	    return testCompUserId;
	}

	/**
	 * test completed user-IDを設定します。
	 * @param testCompUserId test completed user-ID
	 */
	public void setTestCompUserId(String testCompUserId) {
	    this.testCompUserId = testCompUserId;
	    super.append(PjtItems.testCompUserId, testCompUserId );
	}

	/**
	 * test completed user-ID'sを取得します。
	 * @return test completed user-ID's
	 */
	public String[] getTestCompUserIds() {
	    return testCompUserIds;
	}

	/**
	 * test completed user-ID'sを設定します。
	 * @param testCompUserIds test completed user-ID's
	 */
	public void setTestCompUserIds(String[] closeUserIds) {
	    this.testCompUserIds = closeUserIds;
	    super.append( PjtItems.testCompUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(testCompUserIds, PjtItems.testCompUserId.getItemName(), false, true, false) );
	}

	/**
	 * test completed user nameを取得します。
	 * @return test completed user name
	 */
	public String getTestCompUserNm() {
	    return testCompUserNm;
	}

	/**
	 * test completed user nameを設定します。
	 * @param testCompUserNm test completed user name
	 */
	public void setTestCompUserNm(String testCompUserNm) {
	    this.testCompUserNm = testCompUserNm;
	    super.append(PjtItems.testCompUserNm, testCompUserNm );
	}

	/**
	 * test completed time stampを取得します。
	 * @return test completed time stamp
	 */
	public Timestamp getTestCompTimestamp() {
	    return testCompTimestamp;
	}

	/**
	 * test completed time stampを設定します。
	 * @param testCompTimestamp test completed time stamp
	 */
	public void setTestCompTimestamp(Timestamp testCompTimestamp) {
	    this.testCompTimestamp = testCompTimestamp;
	    super.append(PjtItems.testCompTimestamp, testCompTimestamp );
	}

	/**
	 * testComp commentを取得します。
	 * @return test completed comment
	 */
	public String getTestCompCmt() {
	    return testCompCmt;
	}

	/**
	 * testComp commentを設定します。
	 * @param testCompCmt test completed comment
	 */
	public void setTestCompCmt(String testCompCmt) {
	    this.testCompCmt = testCompCmt;
	    super.append(PjtItems.testCompCmt, testCompCmt );
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}

	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	    super.append(PjtItems.delUserId, delUserId );
	}

	/**
	 * delete user-ID'sを取得します。
	 * @return delete user-ID's
	 */
	public String[] getDelUserIds() {
	    return delUserIds;
	}

	/**
	 * delete user-ID'sを設定します。
	 * @param delUserIds delete user-ID's
	 */
	public void setDelUserIds(String[] delUserIds) {
	    this.delUserIds = delUserIds;
	    super.append( PjtItems.delUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(delUserIds, PjtItems.delUserId.getItemName(), false, true, false) );
	}

	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}

	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	    super.append(PjtItems.delUserNm, delUserNm );
	}

	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}

	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	    super.append(PjtItems.delTimestamp, delTimestamp );
	}

	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}

	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	    super.append(PjtItems.delCmt, delCmt );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( PjtItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @return
	 */
	public String[] getAssigneeIds() {
		return assigneeIds;
	}
	/**
	 * @param assigneeIds
	 */
	public void setAssigneeIds(String[] assigneeIds) {
		this.assigneeIds = assigneeIds;
		super.appendByJoinExecData( PjtItems.assigneeId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(assigneeIds, PjtItems.assigneeId.getItemName(), false, true, false) );
	}

	/**
	 * @param mstoneStartDate
	 * @param mstoneEndDate
	 */
	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}

}