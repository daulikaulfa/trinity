package jp.co.blueship.tri.am.dao.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 資産申請分類コードの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum AreqCtgCd {
	/**
	 * 貸出申請
	 */
	LendingRequest("40"),
	/**
	 * 返却申請
	 */
	ReturningRequest("50"),
	/**
	 * 削除申請
	 */
	RemovalRequest("60"),
	;

	private String value = null;

	private AreqCtgCd( String value ) {
		this.value = value;
	}

	public boolean equals( String value ) {
		AreqCtgCd type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	/**
	 * 指定された資産申請分類コードに対応する列挙型を取得します。
	 *
	 * @param areqCtgCd 資産申請分類コード
	 * @return 対応する列挙型
	 */
	public static AreqCtgCd value( String areqCtgCd ) {
		if ( TriStringUtils.isEmpty( areqCtgCd ) ) {
			return null;
		}

		for ( AreqCtgCd value : values() ) {
			if ( value.value().equals(areqCtgCd) )
				return value;
		}
		return null;
	}

}
