package jp.co.blueship.tri.am.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public enum AmTables implements ITableAttribute {

	AM_HEAD( "AH" ),
	AM_HEAD_BL( "AHB" ),
	AM_HEAD_BL_MDL_LNK( "AHBML" ),
	AM_HEAD_LOT_BL_LNK( "AHLML" ),
	AM_LOT_BL( "ALB" ),
	AM_LOT_BL_MDL_LNK( "ALBML" ),
	AM_LOT_BL_REQ_LNK( "ALBRL" ),
	AM_HEAD_LOT_BL( "AHLB" ),
	AM_HEAD_LOT_BL_MDL_LNK( "AHLBML" ),
	AM_HEAD_LOT_BL_REQ_LNK( "AHLBRL" ),
	AM_LOT( "AL" ),
	AM_LOT_GRP_LNK( "ALGL" ),
	AM_LOT_MAIL_GRP_LNK( "ALMGL" ),
	AM_LOT_MDL_LNK( "ALML" ),
	AM_LOT_REL_ENV_LNK( "ALREL" ),
	AM_PJT( "AP" ),
	AM_PJT_AVL( "APA" ),
	AM_PJT_AVL_AREQ_LNK( "APAAL" ),
	AM_AREQ( "AA" ),
	AM_AREQ_FILE( "AAF" ),
	AM_AREQ_BINARY_FILE( "AABF" ),
	AM_AREQ_ATTACHED_FILE( "AAAF" ),
	AM_VCS_REPOS( "AVR" ),
	AM_VCS_MDL( "AVM" ),
	AM_EXT( "AE" ),
	;

	private String alias = null;

	private AmTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static AmTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( AmTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
