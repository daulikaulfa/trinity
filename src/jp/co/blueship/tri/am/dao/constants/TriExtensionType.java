package jp.co.blueship.tri.am.dao.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 拡張子タイプの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum TriExtensionType {
	/**
	 * ASCII形式
	 */
	ascii("ASCII"),
	/**
	 *バイナリ形式
	 */
	binary("BINARY"),
	;

	private String value = null;

	private TriExtensionType( String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		TriExtensionType type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	/**
	 * 指定された拡張子タイプに対応する列挙型を取得します。
	 *
	 * @param type 拡張子タイプ
	 * @return 対応する列挙型
	 */
	public static TriExtensionType value( String type ) {
		if ( TriStringUtils.isEmpty( type ) ) {
			return binary;
		}

		for ( TriExtensionType value : values() ) {
			//拡張子タイプは大文字/小文字を意識しない
			if ( value.value().equalsIgnoreCase( type ) ) {
				return value;
			}
		}

		return binary;
	}

}
