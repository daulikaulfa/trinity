package jp.co.blueship.tri.am.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 */
public enum AmViewTables implements ITableAttribute {

	AM_ASSET_REGISTER( "AAR" ),
	;

	private String alias = null;

	private AmViewTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static AmViewTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( AmViewTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
