package jp.co.blueship.tri.am.dao.head.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the head entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IHeadEntity extends IEntityFooter {

	public String getVcsCtgCd();
	public void setVcsCtgCd(String vcsCtgCd);

	public String getMwPath();
	public void setMwPath(String mwPath);

	public String getHeadLatestVerTag();
	public void setHeadLatestVerTag(String headLatestVerTag);

	public String getHeadLatestBlTag();
	public void setHeadLatestBlTag(String headLatestBlTag);

	public String getHeadVerTag();
	public void setHeadVerTag(String headVerTag);

	public String getHeadBlTag();
	public void setHeadBlTag(String headBlTag);

}
