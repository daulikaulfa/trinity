package jp.co.blueship.tri.am.dao.head.eb;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.head.constants.HeadItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the head entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HeadCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = AmTables.AM_HEAD;

	/**
	 * vcs catalog code
	 */
	public String vcsCtgCd = null;
	/**
	 * mw path
	 */
	public String mwPath = null;
	/**
	 * head latest version tag
	 */
	public String headLatestVerTag = null;
	/**
	 * head latest baseline tag
	 */
	public String headLatestBlTag = null;
	/**
	 * head version tag
	 */
	public String headVerTag = null;
	/**
	 * head baseline tag
	 */
	public String headBlTag = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(HeadItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public HeadCondition(){
		super(attr);
	}

	/**
	 * vcs catalog codeを取得します。
	 * @return vcs catalog code
	 */
	public String getVcsCtgCd() {
	    return vcsCtgCd;
	}
	/**
	 * vcs catalog codeを設定します。
	 * @param vcsCtgCd vcs catalog code
	 */
	public void setVcsCtgCd(String vcsCtgCd) {
	    this.vcsCtgCd = vcsCtgCd;
	    super.append(HeadItems.vcsCtgCd, vcsCtgCd );
	}
	/**
	 * mw pathを取得します。
	 * @return mw path
	 */
	public String getMwPath() {
	    return mwPath;
	}
	/**
	 * mw pathを設定します。
	 * @param mwPath mw path
	 */
	public void setMwPath(String mwPath) {
	    this.mwPath = mwPath;
	    super.append(HeadItems.mwPath, mwPath );
	}
	/**
	 * head latest version tagを取得します。
	 * @return head latest version tag
	 */
	public String getHeadLatestVerTag() {
	    return headLatestVerTag;
	}
	/**
	 * head latest version tagを設定します。
	 * @param headLatestVerTag head latest version tag
	 */
	public void setHeadLatestVerTag(String headLatestVerTag) {
	    this.headLatestVerTag = headLatestVerTag;
	    super.append(HeadItems.headLatestVerTag, headLatestVerTag );
	}
	/**
	 * head latest baseline tagを取得します。
	 * @return head latest baseline tag
	 */
	public String getHeadLatestBlTag() {
	    return headLatestBlTag;
	}
	/**
	 * head latest baseline tagを設定します。
	 * @param headLatestBlTag head latest baseline tag
	 */
	public void setHeadLatestBlTag(String headLatestBlTag) {
	    this.headLatestBlTag = headLatestBlTag;
	    super.append(HeadItems.headLatestBlTag, headLatestBlTag );
	}

	/**
	 * head version tagを取得します。
	 * @return head version tag
	 */
	public String getHeadVerTag() {
	    return headVerTag;
	}
	/**
	 * head version tagを設定します。
	 * @param headVerTag head version tag
	 */
	public void setHeadVerTag(String headVerTag) {
	    this.headVerTag = headVerTag;
	    super.append(HeadItems.headVerTag, headVerTag );
	}
	/**
	 * head baseline tagを取得します。
	 * @return head baseline tag
	 */
	public String getHeaBlTag() {
	    return headBlTag;
	}
	/**
	 * head baseline tagを設定します。
	 * @param headBlTag head baseline tag
	 */
	public void setHeadBlTag(String headBlTag) {
	    this.headBlTag = headBlTag;
	    super.append(HeadItems.headBlTag, headBlTag );
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( HeadItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}