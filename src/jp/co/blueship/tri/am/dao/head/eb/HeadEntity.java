package jp.co.blueship.tri.am.dao.head.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * head entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class HeadEntity extends EntityFooter implements IHeadEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * vcs catalog code
	 */
	public String vcsCtgCd = null;
	/**
	 * mw path
	 */
	public String mwPath = null;
	/**
	 * head latest version tag
	 */
	public String headLatestVerTag = null;
	/**
	 * head latest baseline tag
	 */
	public String headLatestBlTag = null;
	/**
	 * head version tag
	 */
	public String headVerTag = null;
	/**
	 * head baseline tag
	 */
	public String headBlTag = null;

	/**
	 * vcs catalog codeを取得します。
	 * @return vcs catalog code
	 */
	public String getVcsCtgCd() {
	    return vcsCtgCd;
	}
	/**
	 * vcs catalog codeを設定します。
	 * @param vcsCtgCd vcs catalog code
	 */
	public void setVcsCtgCd(String vcsCtgCd) {
	    this.vcsCtgCd = vcsCtgCd;
	}
	/**
	 * mw pathを取得します。
	 * @return mw path
	 */
	public String getMwPath() {
	    return mwPath;
	}
	/**
	 * mw pathを設定します。
	 * @param mwPath mw path
	 */
	public void setMwPath(String mwPath) {
	    this.mwPath = mwPath;
	}
	/**
	 * head latest version tagを取得します。
	 * @return head latest version tag
	 */
	public String getHeadLatestVerTag() {
	    return headLatestVerTag;
	}
	/**
	 * head latest version tagを設定します。
	 * @param headLatestVerTag head latest version tag
	 */
	public void setHeadLatestVerTag(String headLatestVerTag) {
	    this.headLatestVerTag = headLatestVerTag;
	}
	/**
	 * head latest baseline tagを取得します。
	 * @return head latest baseline tag
	 */
	public String getHeadLatestBlTag() {
	    return headLatestBlTag;
	}
	/**
	 * head latest baseline tagを設定します。
	 * @param headLatestBlTag head latest baseline tag
	 */
	public void setHeadLatestBlTag(String headLatestBlTag) {
	    this.headLatestBlTag = headLatestBlTag;
	}
	/**
	 * head version tagを取得します。
	 * @return head version tag
	 */
	public String getHeadVerTag() {
	    return headVerTag;
	}
	/**
	 * head version tagを設定します。
	 * @param headVerTag head version tag
	 */
	public void setHeadVerTag(String headVerTag) {
	    this.headVerTag = headVerTag;
	}
	/**
	 * head baseline tagを取得します。
	 * @return head baseline tag
	 */
	public String getHeadBlTag() {
	    return headBlTag;
	}
	/**
	 * head baseline tagを設定します。
	 * @param headBlTag head baseline tag
	 */
	public void setHeadBlTag(String headBlTag) {
	    this.headBlTag = headBlTag;
	}

}
