package jp.co.blueship.tri.am.dao.head;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.head.constants.HeadItems;
import jp.co.blueship.tri.am.dao.head.eb.HeadEntity;
import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the head DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class HeadDao extends JdbcBaseDao<IHeadEntity> implements IHeadDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return AmTables.AM_HEAD;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IHeadEntity entity ) {
		builder
			.append(HeadItems.vcsCtgCd, entity.getVcsCtgCd(), true)
			.append(HeadItems.mwPath, entity.getMwPath())
			.append(HeadItems.headLatestVerTag, entity.getHeadLatestVerTag())
			.append(HeadItems.headLatestBlTag, entity.getHeadLatestBlTag())
			.append(HeadItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(HeadItems.regTimestamp, entity.getRegTimestamp())
			.append(HeadItems.regUserId, entity.getRegUserId())
			.append(HeadItems.regUserNm, entity.getRegUserNm())
			.append(HeadItems.updTimestamp, entity.getUpdTimestamp())
			.append(HeadItems.updUserId, entity.getUpdUserId())
			.append(HeadItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IHeadEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IHeadEntity entity = new HeadEntity();

  	  entity.setVcsCtgCd( rs.getString(HeadItems.vcsCtgCd.getItemName()) );
  	  entity.setMwPath( rs.getString(HeadItems.mwPath.getItemName()) );
  	  entity.setHeadLatestVerTag( rs.getString(HeadItems.headLatestVerTag.getItemName()) );
  	  entity.setHeadLatestBlTag( rs.getString(HeadItems.headLatestBlTag.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(HeadItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(HeadItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(HeadItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(HeadItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(HeadItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(HeadItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(HeadItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
