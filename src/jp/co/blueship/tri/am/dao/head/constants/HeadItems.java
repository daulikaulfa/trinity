package jp.co.blueship.tri.am.dao.head.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the head entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum HeadItems implements ITableItem {
	vcsCtgCd("vcs_ctg_cd"),
	mwPath("mw_path"),
	headLatestVerTag("head_latest_ver_tag"),
	headLatestBlTag("head_latest_bl_tag"),
	headVerTag("head_ver_tag"),
	headBlTag("head_bl_tag"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private HeadItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
