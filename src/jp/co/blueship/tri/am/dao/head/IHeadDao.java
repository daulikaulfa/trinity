package jp.co.blueship.tri.am.dao.head;

import jp.co.blueship.tri.am.dao.head.eb.IHeadEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the head DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IHeadDao extends IJdbcDao<IHeadEntity> {

}
