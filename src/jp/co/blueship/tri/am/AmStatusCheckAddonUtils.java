package jp.co.blueship.tri.am;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IEhAssetRelatedEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * ステータスチェックの共通処理Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class AmStatusCheckAddonUtils {

	/**
	 * 変更管理が承認取消可能かをチェックする
	 * @param applyNoAssetRelatedEntityMap 取消対象の申請番号とその関連申請情報エンティティのマップ
	 */

	public static void checkPjtApproveCancelByBeforeConfirm(
			Map<String,IEhAssetRelatedEntity> applyNoAssetRelatedEntityMap )
					throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( Map.Entry<String, IEhAssetRelatedEntity> relatedEntityEntry :
													applyNoAssetRelatedEntityMap.entrySet() ) {

				for ( IAreqDto entity :
								relatedEntityEntry.getValue().getAssetRelatedReturnEntity() ) {

					// 返却申請は手動で戻してもらう(V1準拠)
					if ( AmAreqStatusId.CheckinRequested.equals( entity.getAreqEntity().getStsId() )) {

						messageList.add		( AmMessageId.AM001099E );
						messageArgsList.add	( new String[] {	relatedEntityEntry.getKey(),
																entity.getAreqEntity().getAreqId() } );
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}

	}

	/**
	 * 変更管理を承認取消する際、関連する申請が取消可能かをチェックする
	 * @param relatedAssetApplyEntities 取消対象の関連申請情報エンティティ
	 * @param buildEntities 取消対象の申請と紐づく
	 */

	public static void checkPjtApproveCancelByRelatedAssetApply(
		IAreqEntity[] relatedAssetApplyEntities ) throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( IAreqEntity relatedAssetApplyEntity : relatedAssetApplyEntities ) {

				if ( StatusFlg.on.value().equals( relatedAssetApplyEntity.getDelStsId().value() )) {
					messageList.add		( AmMessageId.AM001089E );
					messageArgsList.add	( new String[] { relatedAssetApplyEntity.getAreqId() } );

				} else {

					String baseStatusId = relatedAssetApplyEntity.getStsId();
					String statusId		= relatedAssetApplyEntity.getProcStsId();

					// RETURN_APPLYは手動取消
					if ( !AmAreqStatusId.CheckoutRequested.equals( baseStatusId ) &&
							!AmAreqStatusId.RemovalRequested.equals( baseStatusId )) {
						messageList.add		( AmMessageId.AM001101E );
						messageArgsList.add	( new String[] { relatedAssetApplyEntity.getAreqId() } );

					} else if ( !AmAreqStatusId.CheckoutRequested.equals( statusId ) &&
									!AmAreqStatusIdForExecData.ReturnToCheckoutRequest.equals( statusId ) &&
									!AmAreqStatusId.RemovalRequested.equals( statusId ) ) {
						messageList.add		( AmMessageId.AM001101E );
						messageArgsList.add	( new String[] { relatedAssetApplyEntity.getAreqId() } );
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 申請台帳がダウンロード可能かをチェックする
	 * @param entity レポートエンティティ
	 * @throws BaseBusinessException
	 */
	//呼び出し元なし
	public static void checkReportApplyDownload( IRepEntity entity ) throws BaseBusinessException {
//		if ( !ReportStatusId.ReportComplete.getValue().equals( entity.getBaseStatusId() )) {
		if ( !DcmRepStatusId.ReportCreated.equals( entity.getStsId() )) {
			throw new ContinuableBusinessException( AmMessageId.AM001119E,entity.getRepId() );
		}
	}
}
