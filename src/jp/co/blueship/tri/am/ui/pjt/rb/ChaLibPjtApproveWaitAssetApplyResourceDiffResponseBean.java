package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDiff;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;

public class ChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean extends BaseResponseBean {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private static final String color = "color:" ;
	private static final String bgColor = "background-color:" ;

	/** 申請番号 */
	private String applyNo;

	/** 変更管理番号 */
	private String pjtNo;
	/**
	 * 選択された資産ファイル
	 */
	private String selectedResource ;

	/**
	 * diff結果による文字色【ADD】
	 */
	private String diffAddColor = color + sheet.getValue( AmDesignEntryKeyByDiff.addColor ) ;
	/**
	 * diff結果による文字色【CHANGE】
	 */
	private String diffChangeColor = color + sheet.getValue( AmDesignEntryKeyByDiff.changeColor ) ;
	/**
	 * diff結果による文字色【DELETE】
	 */
	private String diffDeleteColor = color + sheet.getValue( AmDesignEntryKeyByDiff.deleteColor ) ;
	/**
	 * diff結果による背景色【ADD】
	 */
	private String diffAddBgColor = bgColor + sheet.getValue( AmDesignEntryKeyByDiff.addBgColor ) ;
	/**
	 * diff結果による背景色【CHANGE】
	 */
	private String diffChangeBgColor = bgColor + sheet.getValue( AmDesignEntryKeyByDiff.changeBgColor ) ;
	/**
	 * diff結果による背景色【DELETE】
	 */
	private String diffDeleteBgColor = bgColor + sheet.getValue( AmDesignEntryKeyByDiff.deleteBgColor ) ;

	/**
	 *  チェック状況
	 */
	private List<IDiffElement> diffResultList = new ArrayList<IDiffElement>();
	/**
	 * 文字コード
	 */
	private String encoding = null ;

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getSelectedResource() {
		return selectedResource;
	}

	public void setSelectedResource(String selectedResource) {
		this.selectedResource = selectedResource;
	}
	public List<IDiffElement> getDiffResultList() {
		return diffResultList;
	}

	public void setDiffResultList(List<IDiffElement> diffResultList) {
		this.diffResultList = diffResultList;
	}

	public String getDiffAddBgColor() {
		return diffAddBgColor;
	}

	public void setDiffAddBgColor(String diffAddBgColor) {
		this.diffAddBgColor = diffAddBgColor;
	}

	public String getDiffAddColor() {
		return diffAddColor;
	}

	public void setDiffAddColor(String diffAddColor) {
		this.diffAddColor = diffAddColor;
	}

	public String getDiffChangeBgColor() {
		return diffChangeBgColor;
	}

	public void setDiffChangeBgColor(String diffChangeBgColor) {
		this.diffChangeBgColor = diffChangeBgColor;
	}

	public String getDiffChangeColor() {
		return diffChangeColor;
	}

	public void setDiffChangeColor(String diffChangeColor) {
		this.diffChangeColor = diffChangeColor;
	}

	public String getDiffDeleteBgColor() {
		return diffDeleteBgColor;
	}

	public void setDiffDeleteBgColor(String diffDeleteBgColor) {
		this.diffDeleteBgColor = diffDeleteBgColor;
	}

	public String getDiffDeleteColor() {
		return diffDeleteColor;
	}

	public void setDiffDeleteColor(String diffDeleteColor) {
		this.diffDeleteColor = diffDeleteColor;
	}


}
