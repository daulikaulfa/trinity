package jp.co.blueship.tri.am.ui.pjt.beans;

import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibPjtApproveWaitAssetApplyDetailViewProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibPjtApproveWaitAssetApplyDetailViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.PJT_APPROVE_WAIT_LIST,
																	ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW,
																	ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF } ;

	public FlowChaLibPjtApproveWaitAssetApplyDetailViewProsecutor() {
		super(null);
	}

	public FlowChaLibPjtApproveWaitAssetApplyDetailViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
			throws Exception {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean bean = null;
		if ( ! TriStringUtils.isEmpty( session.getAttribute( FLOW_ACTION_ID ) )
				&& session.getAttribute( FLOW_ACTION_ID ) instanceof FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean  ) {
			bean = (FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean)session.getAttribute( FLOW_ACTION_ID );
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean();
			bean.setChaLibPjtApproveWaitAssetApplyDetailViewServiceBean( new ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean() ) ;
			bean.setChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean( new ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean() ) ;
		}

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★");
			this.getLog().debug( "  :referer:=" + referer );
			this.getLog().debug( "  :forward:=" + forward );
			this.getLog().debug( "  :screenType:=" + screenType );
		}

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		String selectedLotNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}
		bean.setLockLotNo	( selectedLotNo );


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( selectedServerNo );


		// 遷移元画面ID処理
		if( referer.equals(ChaLibScreenID.PJT_APPROVE_WAIT_LIST) ){

		} else if( referer.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW) ){

		}

		// 遷移先画面ID処理
		if( forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW) ){
			ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean beanChild = bean.getChaLibPjtApproveWaitAssetApplyDetailViewServiceBean() ;
			beanChild.setApplyNo(reqInfo.getParameter("applyNo"));
			beanChild.setApplyUserName(reqInfo.getParameter("applyUser"));
			beanChild.setApplyUserGroup(reqInfo.getParameter("applyUserGroup"));
		} else if( forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF) ){
			ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean beanChild = bean.getChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean() ;
			beanChild.setApplyNo(reqInfo.getParameter("applyNo"));
			beanChild.setSelectedResource(reqInfo.getParameter("selectedResource"));
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean bean = (FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		// 遷移元画面ID処理
		if( forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_DETAIL_VIEW) ){
			resBean = AmCnvActionToServiceUtils.convertChaLibPjtApproveWaitAssetApplyDetailViewResponseBean(bean);
		}

		if( forward.equals(ChaLibScreenID.PJT_APPROVE_WAIT_ASSET_APPLY_RESOURCE_DIFF) ){
			resBean = AmCnvActionToServiceUtils.convertChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean(bean);
		}

		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		// DIFF画面対応
		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

}
