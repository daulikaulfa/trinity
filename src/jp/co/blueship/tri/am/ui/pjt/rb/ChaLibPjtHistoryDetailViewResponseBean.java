package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtHistoryDetailViewResponseBean extends BaseResponseBean {

	
	
	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String selectedPjtNo = null;
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = null;
	/** サーバ番号 */
	private String serverNo = null;
	/** 変更管理番号 */
	private String pjtNo = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 変更要因分類 */
	private String changeCauseClassify = null;
	/** 変更管理概要 */
	private String pjtSummary;
	/** 変更管理内容 */
	private String pjtContent;
	/** 変更管理ステータス */
	private String pjtStatus;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}

	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewBeanList() {
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
					List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	
	public String getChangeCauseClassify() {
		return changeCauseClassify;
	}
	public void setChangeCauseClassify( String changeCauseClassify ) {
		this.changeCauseClassify = changeCauseClassify;
	}
	
	public String getPjtSummary() {
		return pjtSummary;
	}
	public void setPjtSummary(String pjtSummary) {
		this.pjtSummary = pjtSummary;
	}
	
	public String getPjtContent() {
		return pjtContent;
	}
	public void setPjtContent(String pjtContent) {
		this.pjtContent = pjtContent;
	}
	
	public String getPjtStatus() {
		return pjtStatus;
	}
	public void setPjtStatus( String pjtStatus ) {
		this.pjtStatus = pjtStatus;
	}

}
