package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveAssetApplyDetailViewResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId = null;

	/** 申請番号 */
	private String applyNo;
	
	/** 申請ユーザ */
	private String applyUser;
	
	/** 申請グループ */
	private String applyGroup; 
	
	/** 申請日時 */
	private String applyDate;
	
	/** 変更承認日時 */
	private String pjtApproveDate;
	
	/** 変更承認者 */
	private String pjtApproveUser;
	
	/** 変更承認取消日時 */
	private String pjtApproveCancelDate;
	
	/** 変更承認取消者 */
	private String pjtApproveCancelUser;

	/** 申請件名 */
	private String applySubject;
	
	/** 申請内容 */
	private String applyContent;

	/** 変更管理番号 */
	private String pjtNo;
	
	/** 変更要因番号 */
	private String changeCauseNo; 
	
	/** 申請ステータス */
	private String status;
	
	/** 承認資産リスト */
	private List<String> approveFileList;
	
	/** 承認資産リスト（モジュール） */
	private List<String> approveBinaryFileList;
	
	/** 添付ファイルの名前 */
	private String returnAssetAppendFile ;
	
	/** 添付ファイルのリンクURL */
	private String returnAssetAppendFileLink ;
	
	
	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getApplyGroup() {
		return applyGroup;
	}

	public void setApplyGroup(String applyGroup) {
		this.applyGroup = applyGroup;
	}
	public String getApplyContent() {
		return applyContent;
	}

	public void setApplyContent(String applyContent) {
		this.applyContent = applyContent;
	}

	public String getApplySubject() {
		return applySubject;
	}

	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public List<String> getApproveFileList() {
		return approveFileList;
	}

	public void setApproveFileList(List<String> approveFileList) {
		this.approveFileList = approveFileList;
	}

	public List<String> getApproveBinaryFileList() {
		return approveBinaryFileList;
	}

	public void setApproveBinaryFileList(List<String> approveBinaryFileList) {
		this.approveBinaryFileList = approveBinaryFileList;
	}
	
	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getPjtApproveDate() {
		return pjtApproveDate;
	}

	public void setPjtApproveDate(String pjtApproveDate) {
		this.pjtApproveDate = pjtApproveDate;
	}

	public String getPjtApproveUser() {
		return pjtApproveUser;
	}

	public void setPjtApproveUser(String pjtApproveUser) {
		this.pjtApproveUser = pjtApproveUser;
	}

	public String getPjtApproveCancelDate() {
		return pjtApproveCancelDate;
	}

	public void setPjtApproveCancelDate(String pjtApproveCancelDate) {
		this.pjtApproveCancelDate = pjtApproveCancelDate;
	}

	public String getPjtApproveCancelUser() {
		return pjtApproveCancelUser;
	}

	public void setPjtApproveCancelUser(String pjtApproveCancelUser) {
		this.pjtApproveCancelUser = pjtApproveCancelUser;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}

	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}

	public String getReturnAssetAppendFileLink() {
		return returnAssetAppendFileLink;
	}

	public void setReturnAssetAppendFileLink(String returnAssetAppendFileLink) {
		this.returnAssetAppendFileLink = returnAssetAppendFileLink;
	}

}
