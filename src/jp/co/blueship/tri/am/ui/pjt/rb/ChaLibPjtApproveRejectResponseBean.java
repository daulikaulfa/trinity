package jp.co.blueship.tri.am.ui.pjt.rb;


import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoDetailViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveRejectResponseBean extends BaseResponseBean {
		
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 */
	private String selectedPjtNo = null;
	/** 申請番号 */
	private String selectedApplyNo = null;
	/** 変更管理編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;	
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private ApplyInfoDetailViewPjtApproveBean applyInfoDetailViewBean = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public String getSelectedApplyNo() {
		return selectedApplyNo;
	}
	public void setSelectedApplyNo( String selectedApplyNo ) {
		this.selectedApplyNo = selectedApplyNo;
	}
	
	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean(PjtDetailViewBean pjtDetailViewBean) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}
	
	public ApplyInfoDetailViewPjtApproveBean getApplyInfoDetailViewBean() {
		if ( null == applyInfoDetailViewBean ) {
			applyInfoDetailViewBean = new ApplyInfoDetailViewPjtApproveBean();
		}
		return applyInfoDetailViewBean;
	}
	public void setApplyInfoDetailViewBean( ApplyInfoDetailViewPjtApproveBean applyInfoDetailViewBean ) {
		this.applyInfoDetailViewBean = applyInfoDetailViewBean;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
}
