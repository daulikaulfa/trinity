package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class ChaLibPjtApproveWaitListResponseBean extends BaseResponseBean {
		
	/** ロット番号 */
	private String selectedLotNo = null;
	/** サーバ番号 */
	private String selectedServerNo = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList = null;
	/** 変更管理番号 */
	private String selectedPjtNoString = null;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public List<ApplyInfoViewPjtApproveBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtApproveBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList( List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}

	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public String getSelectedPjtNoString() {
		return selectedPjtNoString;
	}
	public void setSelectedPjtNoString( String[] selectedPjtNo ) {
		
		if ( null == selectedPjtNo ) return;
		
		StringBuilder stb = new StringBuilder();
		stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( selectedPjtNo ) ) ;
		stb.append("]");
		this.selectedPjtNoString = stb.toString();
	}
}
