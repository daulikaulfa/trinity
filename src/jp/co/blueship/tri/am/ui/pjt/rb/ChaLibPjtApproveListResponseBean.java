package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveListResponseBean extends BaseResponseBean {

	
	
	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** サーバ番号 */
	private String selectedServerNo = null;
	/** 承認履歴情報 */
	private List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList = null;
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}
	
	public List<PjtApproveHistoryViewBean> getPjtApproveHistoryViewBeanList() {
		return pjtApproveHistoryViewBeanList;
	}
	public void setPjtApproveHistoryViewBeanList(
					List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList ) {
		this.pjtApproveHistoryViewBeanList = pjtApproveHistoryViewBeanList;
	}
	
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
}
