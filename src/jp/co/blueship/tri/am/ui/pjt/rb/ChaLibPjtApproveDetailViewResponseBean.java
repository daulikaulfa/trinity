package jp.co.blueship.tri.am.ui.pjt.rb;


import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtApproveHistoryViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveDetailViewResponseBean extends BaseResponseBean {
		
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String selectedPjtNo = null;
	/** 変更管理詳細情報 */
	private PjtDetailViewBean pjtDetailViewBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList = null;
	/** 承認履歴情報 */
	private List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public PjtDetailViewBean getPjtDetailViewBean() {
		if ( null == pjtDetailViewBean ) {
			pjtDetailViewBean = new PjtDetailViewBean();
		}
		return pjtDetailViewBean;
	}
	public void setPjtDetailViewBean( PjtDetailViewBean pjtDetailViewBean ) {
		this.pjtDetailViewBean = pjtDetailViewBean;
	}

	public List<ApplyInfoViewPjtDetailBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtDetailBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
					List<ApplyInfoViewPjtDetailBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	public List<PjtApproveHistoryViewBean> getPjtApproveHistoryViewBeanList() {
		if ( null == pjtApproveHistoryViewBeanList ) {
			pjtApproveHistoryViewBeanList = new ArrayList<PjtApproveHistoryViewBean>();
		}
		return pjtApproveHistoryViewBeanList;
	}
	public void setPjtApproveHistoryViewBeanList(
					List<PjtApproveHistoryViewBean> pjtApproveHistoryViewBeanList ) {
		this.pjtApproveHistoryViewBeanList = pjtApproveHistoryViewBeanList;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
}
