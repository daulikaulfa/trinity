package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ApplyInfoViewPjtApproveBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveResponseBean extends BaseResponseBean {
		
	/** 変更管理番号 */
	private String pjtNoString = null;
	/** 変更管理番号 */
	private String[] selectedPjtNo = null;
	/** 変更管理編集入力情報 */
	private PjtEditInputBean pjtEditInputBean = null;
	/** 申請情報 */
	private List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	public String getPjtNoString() {
		return pjtNoString;
	}
	public void setPjtNoString( String pjtNoString ) {
		this.pjtNoString = pjtNoString;
	}
	
	public String[] getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String[] selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public PjtEditInputBean getPjtEditInputBean() {
		if ( null == pjtEditInputBean ) {
			pjtEditInputBean = new PjtEditInputBean();
		}
		return pjtEditInputBean;
	}
	public void setPjtEditInputBean( PjtEditInputBean pjtEditInputBean ) {
		this.pjtEditInputBean = pjtEditInputBean;
	}
	
	public List<ApplyInfoViewPjtApproveBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewPjtApproveBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
			List<ApplyInfoViewPjtApproveBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
}
