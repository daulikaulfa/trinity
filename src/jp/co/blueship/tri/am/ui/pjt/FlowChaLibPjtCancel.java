package jp.co.blueship.tri.am.ui.pjt;

import jp.co.blueship.tri.am.ui.lot.beans.FlowChaLibTopProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtCancelProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtDetailViewProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibPjtCancel extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowChaLibPjtCancelProsecutor());
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		if ( null != referer ) {
			for ( String screenFlow : FlowChaLibPjtCancelProsecutor.screenFlows ) {
				if ( screenFlow.equals( referer ) ) {
					ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtCancelProsecutor(bbe));
					return;
				}
			}
			
			if (referer.equals( ChaLibScreenID.PJT_DETAIL_VIEW )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtDetailViewProsecutor(bbe));
				return;
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibTopProsecutor(bbe));
	}

	@Override
	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter("forward");
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		String referer = reqInfo.getParameter("referer");
		if (null != referer) {
			for (String screenFlow : FlowChaLibPjtCancelProsecutor.screenFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}

			for (String screenFlow : FlowChaLibPjtCancelProsecutor.screenViewFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "ChaLibTop";
	}

}
