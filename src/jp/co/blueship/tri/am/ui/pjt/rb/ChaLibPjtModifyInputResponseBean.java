package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtModifyInputResponseBean extends BaseResponseBean {
		
	/** ロット番号 */
	private String selectedLotNo = null;
	/** 変更管理番号 **/
	private String pjtNo = null;
	/** 変更管理詳細入力情報 */
	private PjtDetailInputBean pjtDetailInputBean = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** 変更管理詳細情報 */
	private PjtDetailBean pjtDetailBean = null;
	/** 変更要因分類のコンボボックス */
	private List<String> changeCauseClassifyList = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 変更要因分類 */
	private String changeCauseClassify = null;
	/** 変更管理概要 */
	private String pjtSummary;
	/** 変更管理内容 */
	private String pjtContent;
	/** 変更管理ステータス */
	private String pjtStatus;
	

	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo( String pjtNo ) {
		this.pjtNo = pjtNo;
	}

	public PjtDetailInputBean getPjtDetailInputBean() {
		if ( null == pjtDetailInputBean ) {
			pjtDetailInputBean = new PjtDetailInputBean();
		}
		return pjtDetailInputBean;
	}
	public void setPjtDetailInputBean( PjtDetailInputBean pjtDetailInputBean ) {
		this.pjtDetailInputBean = pjtDetailInputBean;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	public PjtDetailBean getPjtDetailBean() {
		if ( null == pjtDetailBean ) {
			pjtDetailBean = new PjtDetailBean();
		}
		return pjtDetailBean;
	}
	public void setPjtDetailBean( PjtDetailBean pjtDetailBean ) {
		this.pjtDetailBean = pjtDetailBean;
	}	
	
	public List<String> getChangeCauseClassifyList() {
		return changeCauseClassifyList;
	}
	public void setChangeCauseClassifyList( List<String> changeCauseClassifyList ) {
		this.changeCauseClassifyList = changeCauseClassifyList;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	
	public String getChangeCauseClassify() {
		return changeCauseClassify;
	}
	public void setChangeCauseClassify( String changeCauseClassify ) {
		this.changeCauseClassify = changeCauseClassify;
	}
	
	public String getPjtSummary() {
		return pjtSummary;
	}
	public void setPjtSummary(String pjtSummary) {
		this.pjtSummary = pjtSummary;
	}
	
	public String getPjtContent() {
		return pjtContent;
	}
	public void setPjtContent(String pjtContent) {
		this.pjtContent = pjtContent;
	}
	
	public String getPjtStatus() {
		return pjtStatus;
	}
	public void setPjtStatus( String pjtStatus ) {
		this.pjtStatus = pjtStatus;
	}

}
