package jp.co.blueship.tri.am.ui.pjt.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.ui.beans.dto.Table;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibPjtApproveWaitAssetApplyDetailViewResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId = null;

	/** サーバ番号 */
	private String serverNo;

	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	/** 申請番号 */
	private String applyNo;
	
	/** 申請ユーザ */
	private String applyUser;
	
	/** 申請グループ */
	private String applyGroup; 
	
	/** 申請日時 */
	private String applyDate;

	/** 申請件名 */
	private String applySubject;
	
	/** 申請内容 */
	private String applyContent;

	/** 変更管理番号 */
	private String pjtNo;
	
	/** 変更要因番号 */
	private String changeCauseNo; 
	
	/** 返却資産リスト */
	private Table assetFileList;
	
	/** 承認資産リスト（モジュール） */
	private Table assetBinaryFileList;
	
	/** 承認対象の申請かどうか */
	private boolean isPjtApproveEnabled = false;
	
	/** 添付ファイルの名前 */
	private String returnAssetAppendFile ;
	
	/** 添付ファイルのリンクURL */
	private String returnAssetAppendFileLink ;
	
	
	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
	
	
	public String getServerNo() {
		return serverNo;
	}
	
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}

	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public String getApplyGroup() {
		return applyGroup;
	}

	public void setApplyGroup(String applyGroup) {
		this.applyGroup = applyGroup;
	}
	public String getApplyContent() {
		return applyContent;
	}

	public void setApplyContent(String applyContent) {
		this.applyContent = applyContent;
	}

	public String getApplySubject() {
		return applySubject;
	}

	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public Table getAssetFileList() {
		return assetFileList;
	}

	public void setAssetFileList(Table assetFileList) {
		this.assetFileList = assetFileList;
	}

	public Table getAssetBinaryFileList() {
		return assetBinaryFileList;
	}

	public void setAssetBinaryFileList( Table assetBinaryFileList ) {
		this.assetBinaryFileList = assetBinaryFileList;
	}
	
	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public boolean isPjtApproveEnabled() {
		return isPjtApproveEnabled;
	}

	public void setPjtApproveEnabled(boolean isPjtApproveEnabled) {
		this.isPjtApproveEnabled = isPjtApproveEnabled;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}

	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}

	public String getReturnAssetAppendFileLink() {
		return returnAssetAppendFileLink;
	}

	public void setReturnAssetAppendFileLink(String returnAssetAppendFileLink) {
		this.returnAssetAppendFileLink = returnAssetAppendFileLink;
	}

}
