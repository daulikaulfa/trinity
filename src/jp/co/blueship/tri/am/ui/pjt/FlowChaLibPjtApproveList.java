package jp.co.blueship.tri.am.ui.pjt;

import jp.co.blueship.tri.am.ui.lot.beans.FlowChaLibTopProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtApproveCancelProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtApproveDetailViewProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtApproveListProsecutor;
import jp.co.blueship.tri.am.ui.pjt.beans.FlowChaLibPjtApproveWaitListProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibPjtApproveList extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowChaLibPjtApproveListProsecutor());
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		
		if ( null != referer ) {
			if (referer.equals( ChaLibScreenID.PJT_APPROVE_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtApproveListProsecutor(bbe));
				return;
			}
			
			if (referer.equals( ChaLibScreenID.PJT_APPROVE_WAIT_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtApproveWaitListProsecutor(bbe));
				return;
			}
			
			if (referer.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtApproveCancelProsecutor(bbe));
				return;
			}
			
			if (referer.equals( ChaLibScreenID.PJT_APPROVE_DETAIL_VIEW )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibPjtApproveDetailViewProsecutor(bbe));
				return;
			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibTopProsecutor(bbe));
	}

	@Override
	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter( "forward" );
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		String referer = reqInfo.getParameter("referer");

		if ( null != referer ) {
			for (String screenFlow : FlowChaLibPjtApproveListProsecutor.screenViewFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "ChaLibTop";
	}

}
