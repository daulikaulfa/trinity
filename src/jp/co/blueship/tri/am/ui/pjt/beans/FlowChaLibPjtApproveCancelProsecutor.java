package jp.co.blueship.tri.am.ui.pjt.beans;

import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.AmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibPjtApproveCancelProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibPjtApproveCancelService";

	public static final String[] screenFlows = new String[] {
													ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM,
													ChaLibScreenID.COMP_PJT_APPROVE_CANCEL };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.PJT_APPROVE_LIST,
																	 } ;

	public FlowChaLibPjtApproveCancelProsecutor() {
		super(null);
	}

	public FlowChaLibPjtApproveCancelProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo( IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibPjtApproveCancelServiceBean bean		= new FlowChaLibPjtApproveCancelServiceBean();
		FlowChaLibPjtApproveCancelServiceBean seBean	= (FlowChaLibPjtApproveCancelServiceBean)session.getAttribute( FLOW_ACTION_ID );
		if ( seBean != null ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowChaLibPjtApproveCancelServiceBean();
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer		( referer );
		bean.setForward		( forward );
		bean.setScreenType	( screenType );

		String lotId	 = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setLockLotNo	( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( serverNo );


		if( referer.equals( ChaLibScreenID.PJT_APPROVE_LIST )) {

			bean.setSelectedPjtNo		( reqInfo.getParameter( "pjtNo" ));
			bean.setPjtAvlSeqNo			( reqInfo.getParameter( "approveDate" ));

		} else if ( referer.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {

			bean.setSelectedPjtNo		( reqInfo.getParameter( "pjtNo" ));
			bean.setPjtAvlSeqNo			( reqInfo.getParameter( "approveDate" ));
			bean.setPjtEditInputBean	(
				AmCnvServiceToActionUtils.convertPjtEditInputBean( reqInfo ));

		} else if ( referer.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL )) {

		}

		session.setAttribute( FLOW_ACTION_ID, bean );
		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowChaLibPjtApproveCancelServiceBean bean = (FlowChaLibPjtApproveCancelServiceBean)getServiceReturnInformation();

		BaseResponseBean resBean = null;

		String forward = bean.getForward();

		if ( forward.equals( ChaLibScreenID.PJT_APPROVE_CANCEL_CONFIRM )) {

			resBean = AmCnvActionToServiceUtils.convertChaLibPjtApproveCancelConfirmResponseBean( bean );

		} else if ( forward.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL )) {

			resBean = AmCnvActionToServiceUtils.convertChaLibCompPjtApproveCancelResponseBean( bean );
		}

		PreConditions.assertOf(resBean != null, "ResponseBean is null.");
		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		// RMI対策
		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
