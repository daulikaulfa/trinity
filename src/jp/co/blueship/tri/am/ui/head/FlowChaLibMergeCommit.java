package jp.co.blueship.tri.am.ui.head;

import jp.co.blueship.tri.am.ui.head.beans.FlowChaLibMergeBaselineDetailViewProsecutor;
import jp.co.blueship.tri.am.ui.head.beans.FlowChaLibMergeCommitProsecutor;
import jp.co.blueship.tri.am.ui.head.beans.FlowChaLibMergeLotListProsecutor;
import jp.co.blueship.tri.am.ui.lot.beans.FlowChaLibTopProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibMergeCommit extends PresentationController {

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowChaLibMergeCommitProsecutor());
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		if ( null != referer ) {
			for ( String screenFlow : FlowChaLibMergeCommitProsecutor.screenFlows ) {
				if ( screenFlow.equals( referer ) ) {
					ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibMergeCommitProsecutor(bbe));
					return;
				}
			}
			if (referer.equals( ChaLibScreenID.MERGE_LOT_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibMergeLotListProsecutor(bbe));
				return;
			}
			if (referer.equals( ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibMergeBaselineDetailViewProsecutor(bbe));
				return;
			}
		}
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowChaLibTopProsecutor(bbe));
	}

	@Override
	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter("forward");
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		String referer = reqInfo.getParameter("referer");

		if ( null != referer ) {
			for (String screenFlow : FlowChaLibMergeCommitProsecutor.screenFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
			for (String screenFlow : FlowChaLibMergeCommitProsecutor.screenViewFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "ChaLibTop";
	}

}
