package jp.co.blueship.tri.am.ui.head.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

public class ChaLibMergeHistoryBaselineListResponseBean extends BaseResponseBean {

	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** ロット詳細情報 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** ビルドパッケージ作成成功回数 */
	private int succeededRelUnitCount = 0;
	/** ビルドパッケージ作成失敗回数 */
	private int failedRelUnitCount = 0;
	/** ベースライン情報 */
	private List<LotBaselineViewGroupByVersionTag> baselineViewBeanList = null;
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** ステータス */
	private String lotStatus = null;
	/** 作成日時 */
	private String inputDate = null;
	/** 最新タグ */
	private String recentTag = null;
	/** 最新バージョン */
	private String recentVersion = null;
	/** モジュール名 */
	private String[] moduleName = null;
	/** モジュール名のリスト */
	private String module = null;



	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public LotDetailViewBean getLotDetailViewBean() {
		if ( null == lotDetailViewBean ) {
			lotDetailViewBean = new LotDetailViewBean();
		}
		return lotDetailViewBean;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}

	public int getSucceededRelUnitCount() {
		return succeededRelUnitCount;
	}
	public void setSucceededRelUnitCount(int succeededRelUnitCount) {
		this.succeededRelUnitCount = succeededRelUnitCount;
	}

	public int getFailedRelUnitCount() {
		return failedRelUnitCount;
	}
	public void setFailedRelUnitCount(int failedRelUnitCount) {
		this.failedRelUnitCount = failedRelUnitCount;
	}
	public List<LotBaselineViewGroupByVersionTag> getBaselineViewBeanList() {
		return baselineViewBeanList;
	}
	public void setBaselineViewBeanList(List<LotBaselineViewGroupByVersionTag> baselineViewBeanList) {
		this.baselineViewBeanList = baselineViewBeanList;
	}
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getRecentTag() {
		return recentTag;
	}
	public void setRecentTag(String recentTag) {
		this.recentTag = recentTag;
	}

	public String getRecentVersion() {
		/*if (null != recentVersion && recentVersion.length() > 0) {
			StringBuilder buf = new StringBuilder();
			buf.append("(");
			buf.append(recentVersion);
			buf.append(")");
			recentVersion = buf.toString();
		}*/

		return recentVersion;
	}
	public void setRecentVersion(String recentVersion) {
		this.recentVersion = recentVersion;
	}

	public String[] getModuleName() {
		return moduleName;
	}
	public void setModuleName(String[] moduleName) {
		this.moduleName = moduleName;
	}

	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}


	/** サーバ番号 */
	private String serverNo = null;
	/** サーバ名 */
	private String serverName = null;

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}

	public String getServerName() {
		return serverName;
	}
	public void setServerName( String serverName ) {
		this.serverName = serverName;
	}


	/** 公開フォルダ */
	private String publicFolder = null;
	/** 非公開フォルダ */
	private String privateFolder = null;

	public String getPublicFolder() {
		return publicFolder;
	}
	public void setPublicFolder( String publicFolder ) {
		this.publicFolder = publicFolder;
	}

	public String getPrivateFolder() {
		return privateFolder;
	}
	public void setPrivateFolder( String privateFolder ) {
		this.privateFolder = privateFolder;
	}

	String scmType = null;
	public String getScmType() {
		return scmType;
	}

	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
}
