package jp.co.blueship.tri.am.ui.head.beans;

import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibMergeCommitProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibMergeCommitService";

	public static final String[] screenFlows = new String[] {
													ChaLibScreenID.MERGECOMMIT_CONFIRM,
													ChaLibScreenID.COMP_MERGECOMMIT };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.MERGE_LOT_LIST,
																	ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW,
																	} ;

	public FlowChaLibMergeCommitProsecutor() {
		super(null);
	}

	public FlowChaLibMergeCommitProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo( IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibMergeCommitServiceBean bean = new FlowChaLibMergeCommitServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		bean.setMergeEditInputBean( new MergeEditInputBean() ) ;

		String lotId = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setSelectedLotId	( lotId );
		bean.setLockLotNo		( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId	( serverNo );

		if(referer.equals(ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW)) {
			bean.setSelectedLotVersionTag(reqInfo.getParameter("closeVersionTag"));
			//bean.setSelectedPjtNo(reqInfo.getParameterValues("selectedPjtNo"));
			//bean.setTargetApplyNo(reqInfo.getParameterValues("targetApplyNo"));

		} else if (referer.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM)) {

			MergeEditInputBean inputBean = new MergeEditInputBean();
			bean.setSelectedLotVersionTag(reqInfo.getParameter("closeVersionTag"));
			//bean.setSelectedPjtNo(reqInfo.getParameterValues("selectedPjtNo"));
			//bean.setTargetApplyNo(reqInfo.getParameterValues("targetApplyNo"));
			inputBean.setMergeComment(reqInfo.getParameter("commitComment"));
			bean.setMergeEditInputBean(inputBean);

			//マージエラーチェック
			WarningCheckBean warningCheckMergeCommitError = bean.getWarningCheckMergeCommitError() ;
			String confirmCheckMergeCommitError = reqInfo.getParameter( "confirmCheckMergeCommitError" ) ;
			warningCheckMergeCommitError.setConfirmCheck( ( null != confirmCheckMergeCommitError ) ? true : false );

			warningCheckMergeCommitError.setExistWarning		(
					( new Boolean( reqInfo.getParameter( "existWarningMergeCommit" ) )).booleanValue() );

		} else if (referer.equals(ChaLibScreenID.COMP_MERGECOMMIT)) {

		}

		session.setAttribute(FLOW_ACTION_ID, bean);
		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowChaLibMergeCommitServiceBean bean = (FlowChaLibMergeCommitServiceBean)getServiceReturnInformation();
		BaseResponseBean resBean = new BaseResponseBean();

		String forward = bean.getForward();

		if (forward.equals(ChaLibScreenID.MERGECOMMIT_CONFIRM)) {
			resBean = AmCnvActionToServiceUtils.convertChaLibMergeCommitConfirmResponseBean(bean);
		} else if (forward.equals(ChaLibScreenID.COMP_MERGECOMMIT)) {
			resBean = AmCnvActionToServiceUtils.convertChaLibCompMergeCommitResponseBean(bean);
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
