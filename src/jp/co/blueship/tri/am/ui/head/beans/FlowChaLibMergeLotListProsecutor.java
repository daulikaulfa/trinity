package jp.co.blueship.tri.am.ui.head.beans;

import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeLotListServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeLotListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibMergeLotListProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibMergeLotListService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.CONFLICTCHECK_LOT_LIST,
																	ChaLibScreenID.MERGE_LOT_LIST,
																	ChaLibScreenID.MERGE_HISTORY_LOT_LIST,
																	ChaLibScreenID.MERGE_BASELINE_DETAIL_VIEW,
																	ChaLibScreenID.MERGE_CONFLICT_CHECK_RESULT,
																	ChaLibScreenID.MERGECOMMIT_CONFIRM,
																	} ;

	public FlowChaLibMergeLotListProsecutor() {
		super(null);
	}

	public FlowChaLibMergeLotListProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo( IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		FlowChaLibMergeLotListServiceBean bean = new FlowChaLibMergeLotListServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);
		session.setAttribute(SessionScopeKeyConsts.ACTION_DETAIL_VIEW, "/flowChaLibMergePjtList");

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
			bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowChaLibMergeLotListServiceBean bean = (FlowChaLibMergeLotListServiceBean)getServiceReturnInformation();
		ChaLibMergeLotListResponseBean resBean = AmCnvActionToServiceUtils.convertChaLibMergeLotListResponseBean(bean);

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( "★★★" );
			this.getLog().debug( "  bean.getPageInfoView().getMaxPageNo()" + bean.getPageInfoView().getMaxPageNo() );
			this.getLog().debug( "  resBean.getSelectPageNo()" + resBean.getSelectPageNo() );
		}

		resBean.new MessageUtility().reflectMessage(bean);
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

}
