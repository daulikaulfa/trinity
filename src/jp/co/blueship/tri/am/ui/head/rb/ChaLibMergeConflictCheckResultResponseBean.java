package jp.co.blueship.tri.am.ui.head.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMergeConflictCheckResultResponseBean extends BaseResponseBean {

	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** 選択サーバ番号 */
	private String selectedServerNo = null;
	/** 選択パッケージクローズバージョンタグ */
	private String selectedCloseVersionTag = null ;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** ステータス */
	private String lotStatus = null;
	/** 作成日時 */
	private String inputDate = null;
	/** 最新タグ */
	private String recentTag = null;
	/** 最新バージョン */
	private String recentVersion = null;

	/** */
	private LotBaselineViewGroupByVersionTag baselineViewBean = null ;
	/** マージ判定結果View **/
	private List<MergeViewBean> mergeViewBeanList = null ;


	public List<MergeViewBean> getMergeViewBeanList() {
		return mergeViewBeanList;
	}
	public void setMergeViewBeanList(List<MergeViewBean> mergeViewBeanList) {
		this.mergeViewBeanList = mergeViewBeanList;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public String getSelectedCloseVersionTag() {
		return selectedCloseVersionTag;
	}
	public void setSelectedCloseVersionTag(String selectedCloseVersionTag) {
		this.selectedCloseVersionTag = selectedCloseVersionTag;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getRecentTag() {
		return recentTag;
	}
	public void setRecentTag(String recentTag) {
		this.recentTag = recentTag;
	}

	public String getRecentVersion() {
		/*if (null != recentVersion && recentVersion.length() > 0) {
			StringBuilder buf = new StringBuilder();
			buf.append("(");
			buf.append(recentVersion);
			buf.append(")");
			recentVersion = buf.toString();
		}*/

		return recentVersion;
	}
	public void setRecentVersion(String recentVersion) {
		this.recentVersion = recentVersion;
	}
	public LotBaselineViewGroupByVersionTag getBaselineViewBean() {
		return baselineViewBean;
	}
	public void setBaselineViewBean(LotBaselineViewGroupByVersionTag baselineViewBean) {
		this.baselineViewBean = baselineViewBean;
	}

}
