package jp.co.blueship.tri.am.ui.head.rb;

import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.domain.head.beans.dto.LotBaselineViewGroupByVersionTag;
import jp.co.blueship.tri.am.domain.head.beans.dto.MergeEditInputBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMergeCommitResponseBean extends BaseResponseBean {

	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** 選択されたビルドパッケージクローズバージョンタグ */
	private String selectedCloseVersionTag = null;

	/** ビルド情報 */
	private LotBaselineViewGroupByVersionTag baselineViewBean = null ;
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;

	/** マージ時 マージエラー警告 */
	private WarningCheckBean warningCheckMergeCommitError = new WarningCheckBean() ;

	/** 変更管理編集入力情報 */
	private MergeEditInputBean mergeEditInputBean = null;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	public LotBaselineViewGroupByVersionTag getBaselineViewBean() {
		return baselineViewBean;
	}
	public void setBaselineViewBean(LotBaselineViewGroupByVersionTag baselineViewBean) {
		this.baselineViewBean = baselineViewBean;
	}
	public String getSelectedCloseVersionTag() {
		return selectedCloseVersionTag;
	}
	public void setSelectedCloseVersionTag(String selectedCloseVersionTag) {
		this.selectedCloseVersionTag = selectedCloseVersionTag;
	}
	public WarningCheckBean getWarningCheckMergeCommitError() {
		return warningCheckMergeCommitError;
	}
	public void setWarningCheckMergeCommitError(
			WarningCheckBean warningCheckMergeCommitError) {
		this.warningCheckMergeCommitError = warningCheckMergeCommitError;
	}
	public MergeEditInputBean getMergeEditInputBean() {
		return mergeEditInputBean;
	}
	public void setMergeEditInputBean(MergeEditInputBean mergeEditInputBean) {
		this.mergeEditInputBean = mergeEditInputBean;
	}

}
