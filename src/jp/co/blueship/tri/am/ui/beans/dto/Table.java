package jp.co.blueship.tri.am.ui.beans.dto;

public class Table {
	private TableItem[] list;
	private PageInfo pageInfo;
	public TableItem[] getList() {
		return list;
	}
	public void setList(TableItem[] list) {
		this.list = list;
	}
	public PageInfo getPageInfo() {
		return pageInfo;
	}
	public void setPageInfo(PageInfo pageInfo) {
		this.pageInfo = pageInfo;
	}
}
