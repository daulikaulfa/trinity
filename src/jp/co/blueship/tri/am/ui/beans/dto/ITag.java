package jp.co.blueship.tri.am.ui.beans.dto;

public interface ITag {
	public String getValue();
	public void setValue(String value);
}
