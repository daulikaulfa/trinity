package jp.co.blueship.tri.am.ui.beans.dto;

public class TableItem {
	private ITag[] column;

	public ITag[] getColumn() {
		return column;
	}

	public void setColumn(ITag[] column) {
		this.column = column;
	}

}
