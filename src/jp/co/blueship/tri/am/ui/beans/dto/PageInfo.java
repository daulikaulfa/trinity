package jp.co.blueship.tri.am.ui.beans.dto;

public class PageInfo {
	private Integer maxPageNo;
	private Integer selectPageNo;
	public Integer getMaxPageNo() {
		return maxPageNo;
	}
	public void setMaxPageNo(Integer maxPageNo) {
		this.maxPageNo = maxPageNo;
	}
	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
}
