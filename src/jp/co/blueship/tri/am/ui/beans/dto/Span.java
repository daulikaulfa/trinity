package jp.co.blueship.tri.am.ui.beans.dto;

public class Span implements ITag{
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
