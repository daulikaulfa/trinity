package jp.co.blueship.tri.am.ui.beans.dto;


public class CheckBox implements ITag{
	private String value;
	private boolean checked = false;
	private boolean hidden = false;
	
	public boolean isHidden() {
		return hidden;
	}

	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
