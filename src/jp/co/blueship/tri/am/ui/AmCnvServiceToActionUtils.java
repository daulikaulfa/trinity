package jp.co.blueship.tri.am.ui;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.RelEnvViewBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtEditInputBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByChangec;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 業務サービスBeanとレスポンスBeanとの相互変換を行う。
 * <br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class AmCnvServiceToActionUtils {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ロット作成時に資産の重複の許可／不許可のデフォルト設定値を取得する。
	 * @return デフォルトで許可する場合はtrue。それ以外はfalseを戻します。
	 */
	public static boolean isDefaultAllowAssetDuplication() {

		boolean duplicationCheck = true;
		if ( !StatusFlg.on.value().equals( sheet.getValue(
				AmDesignEntryKeyByChangec.defaultAllowAssetDuplication ))) {
			duplicationCheck = false;
		}

		return duplicationCheck;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @return
	 */
	public static String[] convertSelectedEnvNoArray( IRequestInfo reqInfo ) {

		String[] selectedEnvNoArray = new String[0];

		if ( null != reqInfo.getParameter( "selectedEnvNo[]") ) {
			selectedEnvNoArray = reqInfo.getParameterValues( "selectedEnvNo[]" );
		}

		return selectedEnvNoArray;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param inBean
	 * @param selectedEnvNoArray
	 * @return
	 */
	public static List<RelEnvViewBean> convertRelEnvViewBean( IRequestInfo reqInfo, LotEditInputV3Bean inBean, String[] selectedEnvNoArray ) {
		List<RelEnvViewBean> selectedRelEnvViewBeanList = new ArrayList<RelEnvViewBean>();

		for( String selectedEnvNo : selectedEnvNoArray ) {
			for ( RelEnvViewBean envViewBean : inBean.getRelEnvViewBeanList() ) {
				if( envViewBean.getEnvNo().equals( selectedEnvNo ) ) {
					selectedRelEnvViewBeanList.add( envViewBean );
					break;
				}
			}
		}

		return selectedRelEnvViewBeanList;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibEntryInputBaseInfoBean
		convertChaLibEntryInputBaseInfoBean( IRequestInfo reqInfo ) throws IOException {

		ChaLibEntryInputBaseInfoBean bean = new ChaLibEntryInputBaseInfoBean();

		bean.setInLotNo			( reqInfo.getParameter( "selectedLotNo" ));
		bean.setInPrjNo			( reqInfo.getParameter( "selectedPjtNo" ));
		bean.setInGroupName		( reqInfo.getParameter( "selectedApplyGroup" ));
		bean.setInSubject		( reqInfo.getParameter( "lendApplySubject" ));
		bean.setInContent		( reqInfo.getParameter( "lendApplyComment" ));
		bean.setInUserName		( reqInfo.getParameter( "applyUserName" ));
		bean.setLendApplyMode	( reqInfo.getParameter( "lendApplyMode" ));

		bean.setLendApplyInputStreamBytes	(
				StreamUtils.convertInputStreamToBytes( (InputStream)reqInfo.getAttribute( "FILE_INPUT_STREAM2" ) ));
		bean.setLendApplyFileName			( (String)reqInfo.getAttribute( "FILE_NAME2" ));

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param inBean
	 * @return
	 */
	public static ChaLibEntryBaseInfoBean convertChaLibEntryBaseInfoBean( ChaLibEntryInputBaseInfoBean inBean ) {
		ChaLibEntryBaseInfoBean viewBean = new ChaLibEntryBaseInfoBean();

		viewBean.setPrjNo( inBean.getInPrjNo() );
		viewBean.setGroupName( inBean.getInGroupName() );
		viewBean.setSubject( inBean.getInSubject() );
		viewBean.setContent( inBean.getInContent() );
		viewBean.setApplyUser( inBean.getInUserName() );

		return viewBean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @return
	 */
	public static PjtEditInputBean convertPjtEditInputBean( IRequestInfo reqInfo ) {

		PjtEditInputBean inBean = new PjtEditInputBean();

		inBean.setPjtNo					( reqInfo.getParameter( "pjtNo" ));
		inBean.setApproveCancelComment	( reqInfo.getParameter( "approveCancelComment" ));

		return inBean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @return
	 */
	public static PjtDetailBean convertPjtDetailBean( IRequestInfo reqInfo ) {

		PjtDetailBean pjtBean = new PjtDetailBean();

		String pjtStatus = reqInfo.getParameter("pjtStatus");
		String pjtNo = reqInfo.getParameter("pjtNo");
		String lotId = reqInfo.getParameter("lotNo");
		String lotName = reqInfo.getParameter("lotName");
		String changeCauseNo = reqInfo.getParameter("changeCauseNo");
		String changeCauseClassify = reqInfo.getParameter("changeCauseClassify");
		String pjtSummary = reqInfo.getParameter("pjtSummary");
		String pjtContent = reqInfo.getParameter("pjtContent");

		pjtBean.setPjtStatus	( TriStringUtils.isEmpty(pjtStatus)? 		null: pjtStatus );
		pjtBean.setPjtNo		( TriStringUtils.isEmpty(pjtNo)? 			null: pjtNo );
		pjtBean.setLotNo		( TriStringUtils.isEmpty(lotId)? 			null: lotId );
		pjtBean.setLotName		( TriStringUtils.isEmpty(lotName)? 		null: lotName );
		pjtBean.setChangeCauseNo( TriStringUtils.isEmpty(changeCauseNo)? 	null: changeCauseNo );
		pjtBean.setChangeCauseClassify
								( TriStringUtils.isEmpty(changeCauseClassify)? null: changeCauseClassify );
		pjtBean.setPjtSummary	( TriStringUtils.isEmpty(pjtSummary)? 	null: pjtSummary  );
		pjtBean.setPjtContent	( TriStringUtils.isEmpty(pjtContent)? 	null: pjtContent );

		return pjtBean;
	}

	/**
	 * レポート用にチェックを入れた変更管理番号を保持する
	 * @param session ISessionInfo
	 * @param reqInfo IRequestInfo
	 * @return 保持された変更管理番号のセット
	 */
	public static Set<String> saveSelectedPjtNoForReport(
										ISessionInfo session, IRequestInfo reqInfo ) {

		String forward = reqInfo.getParameter( "forward" );
		String referer = reqInfo.getParameter( "referer" );

		// ロット詳細内遷移、または変更管理詳細閲覧画面からの遷移以外はクリア
		if ( !ChaLibScreenID.PJT_DETAIL_VIEW.equals( referer ) &&
				!ChaLibScreenID.LOT_DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		}
		if ( !"register".equals( forward ) && !ChaLibScreenID.LOT_DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		}


		// 選択された変更管理番号
		String[] pjtNoArray				= reqInfo.getParameterValues( "pjtNo[]" );
		Set<String> tmpSelectedPjtNo	= new HashSet<String>();
		if ( null != pjtNoArray) {
			for ( String pjtNo : pjtNoArray ) {
				tmpSelectedPjtNo.add( pjtNo );
			}
		}

		if ( "register".equals( forward ) && ChaLibScreenID.PJT_DETAIL_VIEW.equals( referer ) ) {
			return tmpSelectedPjtNo;
		}


		// 画面に表示されていた変更管理番号
		@SuppressWarnings("unchecked")
		Set<String> viewPjtNoSet		=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		// 選択済みの変更管理番号
		@SuppressWarnings("unchecked")
		Set<String> selectedPjtIdSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		if ( null == selectedPjtIdSet ) {
			selectedPjtIdSet = tmpSelectedPjtNo;
		}

		if ( null != viewPjtNoSet ) {

			// 今回、選択されなかった変更管理番号
			Set<String> unSelectedPjtNo = new HashSet<String>();
			for ( String pjtNo : viewPjtNoSet ) {

				if ( !tmpSelectedPjtNo.contains( pjtNo )) {
					unSelectedPjtNo.add( pjtNo );
				}
			}

			selectedPjtIdSet.addAll		( tmpSelectedPjtNo );
			selectedPjtIdSet.removeAll	( unSelectedPjtNo );
		}
		session.setAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO, selectedPjtIdSet );

		return selectedPjtIdSet;
	}
}
