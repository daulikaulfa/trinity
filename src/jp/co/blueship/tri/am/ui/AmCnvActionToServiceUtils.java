package jp.co.blueship.tri.am.ui;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibAssetResourceViewBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryAssetResourceInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibSelectLendEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibCompMasterDelCancelServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendEntryServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelHistoryListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelHistoryViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnListServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnResourceDiffServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckBaselineListServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckLotListServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibConflictCheckServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeCommitServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultDiffServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeConflictCheckResultServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryBaselineDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryBaselineListServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeHistoryLotListServiceBean;
import jp.co.blueship.tri.am.domain.head.dto.FlowChaLibMergeLotListServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCancelServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotCloseServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotEntryServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotHistoryListServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailViewBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveListServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveRejectServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveWaitListServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCancelServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtCloseServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtModifyServiceBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtTestCompleteServiceBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompLendEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompLendModifyResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompMasterDelCancelResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompMasterDelEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompMasterDelRejectResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompRtnEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendEntryAssetSelectResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendEntryConfirmResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendHistoryDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendListResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendModifyConfirmResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibLendModifyResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelEntryConfirmResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelHistoryListResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelHistoryViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibMasterDelListResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnEntryResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnHistoryDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnListResponseBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnResourceDiffResponseBean;
import jp.co.blueship.tri.am.ui.beans.dto.CheckBox;
import jp.co.blueship.tri.am.ui.beans.dto.ITag;
import jp.co.blueship.tri.am.ui.beans.dto.Image;
import jp.co.blueship.tri.am.ui.beans.dto.Span;
import jp.co.blueship.tri.am.ui.beans.dto.Table;
import jp.co.blueship.tri.am.ui.beans.dto.TableItem;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibConflictCheckBaselineDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibConflictCheckBaselineListResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibConflictCheckLotListResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibConflictCheckResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeBaselineDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeCommitResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeConflictCheckResultDiffResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeConflictCheckResultResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeHistoryBaselineDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeHistoryBaselineListResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeHistoryLotListResponseBean;
import jp.co.blueship.tri.am.ui.head.rb.ChaLibMergeLotListResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotCancelResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotCloseResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotEntryResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotHistoryDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotHistoryListResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotModifyResponseBean;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibTopResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveAssetApplyDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveAssetApplyResourceDiffResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveCancelResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveListResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveRejectResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveWaitAssetApplyDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtApproveWaitListResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtCancelResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtCloseResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtEntryConfirmResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtEntryInputResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtHistoryDetailViewResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtModifyConfirmResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtModifyInputResponseBean;
import jp.co.blueship.tri.am.ui.pjt.rb.ChaLibPjtTestCompleteResponseBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * 業務サービスBeanとレスポンスBeanとの相互変換を行う。
 *
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 * @version SP-20150601_V3L12R01
 * @author Takashi Ono
 */
public class AmCnvActionToServiceUtils {

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibTopResponseBean convertChaTopResponseBean(FlowChaLibTopServiceBean bean) {

		ChaLibTopResponseBean resBean = new ChaLibTopResponseBean();

		resBean.setLotViewBeanList	( bean.getLotViewBeanList() );

		// パスワード変更画面への遷移の場合はnullで入ってくる
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibLotEntryResponseBean(FlowChaLibLotEntryServiceBean bean) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotEditInputBean		( bean.getLotEditInputBean() ) ;
		resBean.setSelectableAllowAssetDuplication( bean.isSelectableAllowAssetDuplication() ) ;
		resBean.setSelectableAllowAssetDuplicationName( bean.getSelectableAllowAssetDuplicationName() ) ;
		resBean.setAllowAssetDuplicationVisible( resBean.isSelectableAllowAssetDuplication() /*|| bean.getLotEditInputBean().isAllowAssetDuplication()*/ );

		resBean.new MessageUtility().reflectComment( bean );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibLotModuleSelectResponseBean(FlowChaLibLotEntryServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibLotBuildEnvSelectResponseBean(FlowChaLibLotEntryServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibLotRelEnvSelectResponseBean(FlowChaLibLotEntryServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibLotEntryConfirmResponseBean(FlowChaLibLotEntryServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotEditInputBean		( bean.getLotEditInputBean() ) ;
		resBean.setSelectableAllowAssetDuplication( bean.isSelectableAllowAssetDuplication() ) ;
		resBean.setSelectableAllowAssetDuplicationName( bean.getSelectableAllowAssetDuplicationName() ) ;
		resBean.setAllowAssetDuplicationVisible( resBean.isSelectableAllowAssetDuplication() /*|| bean.getLotEditInputBean().isAllowAssetDuplication()*/ );

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotEntryResponseBean convertChaLibCompLotEntryResponseBean(FlowChaLibLotEntryServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotEntryResponseBean resBean = new ChaLibLotEntryResponseBean();

		resBean.setLotNo( bean.getLotNo() );
		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibLotModifyResponseBean(
			FlowChaLibLotModifyServiceBean bean ) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotEditInputBean		( bean.getLotEditInputBean() ) ;
		resBean.setSelectableAllowAssetDuplication( bean.isSelectableAllowAssetDuplication() ) ;
		resBean.setSelectableAllowAssetDuplicationName( bean.getSelectableAllowAssetDuplicationName() ) ;
		resBean.setAllowAssetDuplicationVisible( resBean.isSelectableAllowAssetDuplication() || bean.getLotEditInputBean().isAllowAssetDuplication() );

		resBean.new MessageUtility().reflectComment( bean );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibLotModuleSelectResponseBean(FlowChaLibLotModifyServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibLotBuildEnvSelectResponseBean(FlowChaLibLotModifyServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibLotRelEnvSelectResponseBean(FlowChaLibLotModifyServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibLotModifyConfirmResponseBean(FlowChaLibLotModifyServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotEditInputBean		( bean.getLotEditInputBean() ) ;
		resBean.setSelectableAllowAssetDuplication( bean.isSelectableAllowAssetDuplication() ) ;
		resBean.setSelectableAllowAssetDuplicationName( bean.getSelectableAllowAssetDuplicationName() ) ;
		resBean.setAllowAssetDuplicationVisible( resBean.isSelectableAllowAssetDuplication() || bean.getLotEditInputBean().isAllowAssetDuplication() );

		//エラー警告メッセージ
		resBean.setWarningCheckError( bean.getWarningCheckError() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotModifyResponseBean convertChaLibCompLotModifyResponseBean(FlowChaLibLotModifyServiceBean bean , IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ChaLibLotModifyResponseBean resBean = new ChaLibLotModifyResponseBean();

		resBean.setLotNo( bean.getLotNo() );
		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotHistoryListResponseBean convertChaLotHistoryListResponseBean(FlowChaLibLotHistoryListServiceBean bean) {

		ChaLibLotHistoryListResponseBean resBean = new ChaLibLotHistoryListResponseBean();

		resBean.setLotViewBeanList	( bean.getLotViewBeanList() );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotDetailViewResponseBean convertChaLotDetailViewResponseBean(
			FlowChaLibLotDetailViewServiceBean bean, ISessionInfo session ) {

		ChaLibLotDetailViewResponseBean resBean = new ChaLibLotDetailViewResponseBean();

		resBean.setLotNo			( bean.getLotDetailViewBean().getLotNo() );
		resBean.setLotName			( bean.getLotDetailViewBean().getLotName() );
		resBean.setLotSummary		( bean.getLotDetailViewBean().getLotSummary() );
		resBean.setLotContent		( bean.getLotDetailViewBean().getLotContent() );
		resBean.setAllowAssetDuplicationName( bean.getLotDetailViewBean().getAllowAssetDuplicationName() );
		resBean.setAllowAssetDuplicationVisible( bean.getLotDetailViewBean().isAllowAssetDuplicationVisible() );
		resBean.setUseMerge			( bean.getLotDetailViewBean().isUseMerge() );
		resBean.setLotStatus		( bean.getLotDetailViewBean().getLotStatus() );
		resBean.setInputDate		( bean.getLotDetailViewBean().getInputDate() );
		resBean.setRecentTag		( bean.getLotDetailViewBean().getRecentTag() );
		resBean.setRecentVersion	( bean.getLotDetailViewBean().getRecentVersion() );

		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for ( int i = 0; i < moduleName.length; i++ ) {
			if( i >0 ) {
				buff.append( ", " );
			}
			buff.append( moduleName[i] );
		}
		resBean.setModule			( buff.toString() );

		resBean.setSucceededRelUnitCount	( bean.getSucceededRelUnitCount() );
		resBean.setFailedRelUnitCount		( bean.getFailedRelUnitCount() );
		resBean.setPjtViewBeanList	( bean.getPjtViewBeanList() );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		resBean.setSelectedPjtIdArray( bean.getSelectedPjtIdArray() );

		session.setAttribute(
			SessionScopeKeyConsts.VIEW_REPORT_NO, getPjtNoSet( bean.getPjtViewBeanList() ));

		// 選択済みの変更管理番号
		@SuppressWarnings("unchecked")
		Set<String> selectedPjtIdSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );

		if ( null != selectedPjtIdSet ) {
			resBean.setSelectedPjtNoString( (String[])selectedPjtIdSet.toArray( new String[0] ));
		}

		//変更管理 詳細検索条件
		resBean.setPjtSearchBean( bean.getPjtSearchBean() ) ;

		// アクセス許可グループ
		resBean.setAllowListView( bean.getLotDetailViewBean().isAllowListView() );
		resBean.setAllowListContent( bean.getLotDetailViewBean().getAllowListContent() );

		String[] groupName = bean.getLotDetailViewBean().getGroupName();
		buff = new StringBuilder();
		for ( int i = 0; i < groupName.length; i++ ) {
			if( i > 0 ) {
				buff.append( ", " );
			}
			buff.append( groupName[i] );
		}
		resBean.setGroup( buff.toString() );

		// 選択されたメール送信先 特定グループ
		String[] selectedSpecifiedGroupNames = bean.getLotDetailViewBean().getSpecifiedGroupName();
		buff = new StringBuilder();
		for ( int i = 0; i < selectedSpecifiedGroupNames.length; i++ ) {
			if( i > 0 ) {
				buff.append( ", " );
			}
			buff.append( selectedSpecifiedGroupNames[i] );
		}
		resBean.setSelectedSpecifiedGroupNameString( buff.toString() );

		// 選択されたリリース環境 環境名
		{
			String[] selectedRelEnvNames = bean.getLotDetailViewBean().getRelEnvName();
			buff = new StringBuilder();
			for ( int i = 0; i < selectedRelEnvNames.length; i++ ) {
				if( i > 0 ) {
					buff.append( ", " );
				}
				buff.append( selectedRelEnvNames[i] );
			}
			resBean.setSelectedRelEnvNameString( buff.toString() );
		}

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setPublicFolder	( bean.getLotDetailViewBean().getPublicFolder() );
		resBean.setPrivateFolder( bean.getLotDetailViewBean().getPrivateFolder() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotHistoryDetailViewResponseBean convertChaLotHistoryDetailViewResponseBean(
			FlowChaLibLotHistoryDetailViewServiceBean bean, ISessionInfo session ) {

		ChaLibLotHistoryDetailViewResponseBean resBean = new ChaLibLotHistoryDetailViewResponseBean();

		resBean.setLotNo			( bean.getLotDetailViewBean().getLotNo() );
		resBean.setLotName			( bean.getLotDetailViewBean().getLotName() );
		resBean.setLotSummary		( bean.getLotDetailViewBean().getLotSummary() );
		resBean.setLotContent		( bean.getLotDetailViewBean().getLotContent() );
		resBean.setAllowAssetDuplicationName( bean.getLotDetailViewBean().getAllowAssetDuplicationName() );
		resBean.setAllowAssetDuplicationVisible( bean.getLotDetailViewBean().isAllowAssetDuplicationVisible() );
		resBean.setUseMerge			( bean.getLotDetailViewBean().isUseMerge() );
		resBean.setLotStatus		( bean.getLotDetailViewBean().getLotStatus() );
		resBean.setInputDate		( bean.getLotDetailViewBean().getInputDate() );
		resBean.setRecentTag		( bean.getLotDetailViewBean().getRecentTag() );
		resBean.setRecentVersion	( bean.getLotDetailViewBean().getRecentVersion() );

		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for ( int i = 0; i < moduleName.length; i++ ) {
			if( i >0 ) {
				buff.append( ", " );
			}
			buff.append( moduleName[i] );
		}
		resBean.setModule			( buff.toString() );

		resBean.setSucceededRelUnitCount	( bean.getSucceededRelUnitCount() );
		resBean.setFailedRelUnitCount		( bean.getFailedRelUnitCount() );
		resBean.setPjtViewBeanList	( bean.getPjtViewBeanList() );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );

		resBean.setEnableLotCancel( bean.isEnableLotCancel() ) ;

		session.setAttribute(
			SessionScopeKeyConsts.VIEW_REPORT_NO, getPjtNoSet( bean.getPjtViewBeanList() ));

		// 選択済みの変更管理番号
		@SuppressWarnings("unchecked")
		Set<String> selectedPjtIdSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );

		if ( null != selectedPjtIdSet ) {
			resBean.setSelectedPjtNoString( (String[])selectedPjtIdSet.toArray( new String[0] ));
		}

		// アクセス許可グループ
		resBean.setAllowListView( bean.getLotDetailViewBean().isAllowListView() );
		resBean.setAllowListContent( bean.getLotDetailViewBean().getAllowListContent() );

		String[] groupName = bean.getLotDetailViewBean().getGroupName();
		buff = new StringBuilder();
		for ( int i = 0; i < groupName.length; i++ ) {
			if( i > 0 ) {
				buff.append( ", " );
			}
			buff.append( groupName[i] );
		}
		resBean.setGroup( buff.toString() );

		// 選択されたメール送信先 特定グループ
		String[] selectedSpecifiedGroupNames = bean.getLotDetailViewBean().getSpecifiedGroupName();
		buff = new StringBuilder();
		for ( int i = 0; i < selectedSpecifiedGroupNames.length; i++ ) {
			if( i > 0 ) {
				buff.append( ", " );
			}
			buff.append( selectedSpecifiedGroupNames[i] );
		}
		resBean.setSelectedSpecifiedGroupNameString( buff.toString() );

		// 選択されたリリース環境 環境名
		{
			String[] selectedRelEnvNames = bean.getLotDetailViewBean().getRelEnvName();
			buff = new StringBuilder();
			for ( int i = 0; i < selectedRelEnvNames.length; i++ ) {
				if( i > 0 ) {
					buff.append( ", " );
				}
				buff.append( selectedRelEnvNames[i] );
			}
			resBean.setSelectedRelEnvNameString( buff.toString() );
		}

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setPublicFolder	( bean.getLotDetailViewBean().getPublicFolder() );
		resBean.setPrivateFolder( bean.getLotDetailViewBean().getPrivateFolder() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}

	/**
	 * 変更管理番号のセットを取得する
	 * @param pjtViewBeanList PjtViewBeanのリスト
	 * @return 変更管理番号のセット
	 */
	private static Set<String> getPjtNoSet( List<PjtViewBean> pjtViewBeanList ) {

		Set<String> pjtIdSet = new HashSet<String>();
		for ( PjtViewBean viewBean : pjtViewBeanList ) {
			pjtIdSet.add( viewBean.getPjtNo() );
		}

		return pjtIdSet;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtEntryInputResponseBean convertChaLibPjtEntryInputResponseBean(FlowChaLibPjtEntryServiceBean bean) {

		ChaLibPjtEntryInputResponseBean resBean = new ChaLibPjtEntryInputResponseBean();

		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(bean.getPjtDetailInputBean().getPjtDetailBean().getLotName());
		resBean.setChangeCauseClassifyList(bean.getPjtDetailInputBean().getChangeCauseClassifyList());
		resBean.setChangeCauseNo(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtSummary(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtEntryConfirmResponseBean convertChaLibPjtEntryConfirmResponseBean(FlowChaLibPjtEntryServiceBean bean) {

		ChaLibPjtEntryConfirmResponseBean resBean = new ChaLibPjtEntryConfirmResponseBean();

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();
		pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailInputBean().getPjtDetailBean());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(pjtDetailViewBean.getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(pjtDetailViewBean.getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(pjtDetailViewBean.getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtSummary(pjtDetailViewBean.getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(pjtDetailViewBean.getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtEntryConfirmResponseBean convertChaLibCompPjtEntryResponseBean(FlowChaLibPjtEntryServiceBean bean) {

		ChaLibPjtEntryConfirmResponseBean resBean = new ChaLibPjtEntryConfirmResponseBean();

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();
		pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailInputBean().getPjtDetailBean());
		resBean.setPjtNo(bean.getPjtNo());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(pjtDetailViewBean.getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(pjtDetailViewBean.getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(pjtDetailViewBean.getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtSummary(pjtDetailViewBean.getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(pjtDetailViewBean.getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtModifyInputResponseBean convertChaLibPjtModifyInputResponseBean(FlowChaLibPjtModifyServiceBean bean) {

		ChaLibPjtModifyInputResponseBean resBean = new ChaLibPjtModifyInputResponseBean();

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();
		String referer = bean.getReferer();
		if (referer.equals(ChaLibScreenID.PJT_MODIFY_INPUT) || referer.equals(ChaLibScreenID.PJT_MODIFY_CONFIRM)) {
			pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailViewBean().getPjtDetailBean());
		} else {
			pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailInputBean().getPjtDetailBean());
		}
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(bean.getPjtDetailInputBean().getPjtDetailBean().getLotName());
		resBean.setPjtNo(bean.getPjtDetailInputBean().getPjtDetailBean().getPjtNo());
		resBean.setChangeCauseNo(pjtDetailViewBean.getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(pjtDetailViewBean.getPjtDetailBean().getChangeCauseClassify());
		resBean.setChangeCauseClassifyList(bean.getPjtDetailInputBean().getChangeCauseClassifyList());
		resBean.setPjtStatus(bean.getPjtDetailInputBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(pjtDetailViewBean.getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(pjtDetailViewBean.getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtModifyConfirmResponseBean convertChaLibPjtModifyConfirmResponseBean(FlowChaLibPjtModifyServiceBean bean) {

		ChaLibPjtModifyConfirmResponseBean resBean = new ChaLibPjtModifyConfirmResponseBean();

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();
		pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailInputBean().getPjtDetailBean());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(pjtDetailViewBean.getPjtDetailBean().getLotName());
		resBean.setPjtNo(bean.getPjtDetailInputBean().getPjtDetailBean().getPjtNo());
		if ( null == resBean.getPjtNo() ) {
			resBean.setPjtNo( bean.getSelectedPjtNo() );
		}

		resBean.setChangeCauseNo(pjtDetailViewBean.getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(pjtDetailViewBean.getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtStatus(bean.getPjtDetailInputBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(pjtDetailViewBean.getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(pjtDetailViewBean.getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtModifyConfirmResponseBean convertChaLibLotCloseConfirmResponseBean(FlowChaLibPjtModifyServiceBean bean) {

		ChaLibPjtModifyConfirmResponseBean resBean = new ChaLibPjtModifyConfirmResponseBean();

		PjtDetailViewBean pjtDetailViewBean = new PjtDetailViewBean();
		pjtDetailViewBean.setPjtDetailBean(bean.getPjtDetailInputBean().getPjtDetailBean());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(pjtDetailViewBean.getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(pjtDetailViewBean.getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(pjtDetailViewBean.getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtNo(pjtDetailViewBean.getPjtDetailBean().getPjtNo());
		resBean.setPjtSummary(pjtDetailViewBean.getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(pjtDetailViewBean.getPjtDetailBean().getPjtContent());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotCloseResponseBean convertChaLibLotCloseConfirmResponseBean(
			FlowChaLibLotCloseServiceBean bean) {

		ChaLibLotCloseResponseBean resBean = new ChaLibLotCloseResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		if ( null != moduleName && 0 < moduleName.length ) {
			StringBuilder buff = new StringBuilder();
			for (int i=0; i<moduleName.length; i++) {
				if(i>0) {
					buff.append(", ");
				}
				buff.append(moduleName[i]);
			}
			resBean.setModule(buff.toString());
		}
		resBean.setPjtViewBeanList	( bean.getPjtViewBeanList() );
		// 完了画面への遷移時は bean.getPageInfoView() = null
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setScmType		( bean.getLotEditInputBean().getScmType() );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibLotCancelResponseBean convertChaLibLotCancelConfirmResponseBean(
			FlowChaLibLotCancelServiceBean bean) {

		ChaLibLotCancelResponseBean resBean = new ChaLibLotCancelResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());

		resBean.setLotEditInputBean( bean.getLotEditInputBean() ) ;

		resBean.setPreviousFlowAction( bean.getPreviousFlowAction() ) ;

		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		if ( null != moduleName && 0 < moduleName.length ) {
			StringBuilder buff = new StringBuilder();
			for (int i=0; i<moduleName.length; i++) {
				if(i>0) {
					buff.append(", ");
				}
				buff.append(moduleName[i]);
			}
			resBean.setModule(buff.toString());
		}
		resBean.setPjtViewBeanList	( bean.getPjtViewBeanList() );
		// 完了画面への遷移時は bean.getPageInfoView() = null
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setScmType		( bean.getLotEditInputBean().getScmType() );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtDetailViewResponseBean convertChaLibPjtDetailViewResponseBean(FlowChaLibPjtDetailViewServiceBean bean) {

		ChaLibPjtDetailViewResponseBean resBean = new ChaLibPjtDetailViewResponseBean();

		resBean.setPjtDetailViewBean(bean.getPjtDetailViewBean());
		resBean.setLotNo(bean.getPjtDetailViewBean().getPjtDetailBean().getLotNo());
		resBean.setPjtNo(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtNo());
		resBean.setLotName(bean.getPjtDetailViewBean().getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtStatus(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent());
		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtHistoryDetailViewResponseBean convertChaLibPjtHistoryDetailViewResponseBean(
	FlowChaLibPjtHistoryDetailViewServiceBean bean ) {

		ChaLibPjtHistoryDetailViewResponseBean resBean = new ChaLibPjtHistoryDetailViewResponseBean();

		resBean.setPjtDetailViewBean	( bean.getPjtDetailViewBean() );
		resBean.setLotNo				( bean.getPjtDetailViewBean().getPjtDetailBean().getLotNo() );
		resBean.setPjtNo				( bean.getPjtDetailViewBean().getPjtDetailBean().getPjtNo() );
		resBean.setLotName				( bean.getPjtDetailViewBean().getPjtDetailBean().getLotName() );
		resBean.setChangeCauseNo		( bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo() );
		resBean.setChangeCauseClassify	( bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify() );
		resBean.setPjtStatus			( bean.getPjtDetailViewBean().getPjtDetailBean().getPjtStatus() );
		resBean.setPjtSummary			( bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary() );
		resBean.setPjtContent			( bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent() );
		resBean.setApplyInfoViewBeanList( bean.getApplyInfoViewBeanList() );
		resBean.setPageNoNum			( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo			( bean.getPageInfoView().getSelectPageNo() );
		resBean.setServerNo				( bean.getLockServerId() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtCloseResponseBean convertChaLibPjtCloseResponseBean(FlowChaLibPjtCloseServiceBean bean) {

		ChaLibPjtCloseResponseBean resBean = new ChaLibPjtCloseResponseBean();

		resBean.setPjtDetailViewBean(bean.getPjtDetailViewBean());
		resBean.setPjtNo(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtNo());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(bean.getPjtDetailViewBean().getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtStatus(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent());
		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());

		if( null != bean.getPjtEditInputBean() ) {
			resBean.setPjtEditInputBean( bean.getPjtEditInputBean() ) ;
		}
		// 完了画面への遷移時は bean.getPageInfoView() = null
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtCancelResponseBean convertChaLibPjtCancelResponseBean(FlowChaLibPjtCancelServiceBean bean) {

		ChaLibPjtCancelResponseBean resBean = new ChaLibPjtCancelResponseBean();

		resBean.setPjtDetailViewBean(bean.getPjtDetailViewBean());
		resBean.setPjtNo(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtNo());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(bean.getPjtDetailViewBean().getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtStatus(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent());
		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());

		if( null != bean.getPjtEditInputBean() ) {
			resBean.setPjtEditInputBean( bean.getPjtEditInputBean() ) ;
		}
		// 完了画面への遷移時は bean.getPageInfoView() = null
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}
		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtTestCompleteResponseBean convertChaLibPjtTestCompleteResponseBean(FlowChaLibPjtTestCompleteServiceBean bean) {

		ChaLibPjtTestCompleteResponseBean resBean = new ChaLibPjtTestCompleteResponseBean();

		resBean.setPjtDetailViewBean(bean.getPjtDetailViewBean());
		resBean.setPjtNo(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtNo());
		resBean.setLotNo(bean.getSelectedLotNo());
		resBean.setLotName(bean.getPjtDetailViewBean().getPjtDetailBean().getLotName());
		resBean.setChangeCauseNo(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseNo());
		resBean.setChangeCauseClassify(bean.getPjtDetailViewBean().getPjtDetailBean().getChangeCauseClassify());
		resBean.setPjtStatus(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtStatus());
		resBean.setPjtSummary(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtSummary());
		resBean.setPjtContent(bean.getPjtDetailViewBean().getPjtDetailBean().getPjtContent());
		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());

		if( null != bean.getPjtEditInputBean() ) {
			resBean.setPjtEditInputBean( bean.getPjtEditInputBean() ) ;
		}

		// 完了画面への遷移時は bean.getPageInfoView() = null
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		}
		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveWaitListResponseBean convertChaLibPjtApproveWaitListResponseBean(
			FlowChaLibPjtApproveWaitListServiceBean bean ) {

		ChaLibPjtApproveWaitListResponseBean resBean = new ChaLibPjtApproveWaitListResponseBean();

		resBean.setSelectedLotNo		( bean.getSelectedLotNo() );
		resBean.setSelectedServerNo		( bean.getLockServerId() );
		resBean.setLotViewBeanList		( bean.getLotViewBeanList() );
		resBean.setApplyInfoViewBeanList( bean.getApplyInfoViewBeanList()) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibPjtApproveWaitAssetApplyDetailViewResponseBean convertChaLibPjtApproveWaitAssetApplyDetailViewResponseBean(FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean bean) {

		ChaLibPjtApproveWaitAssetApplyDetailViewResponseBean resBean = new ChaLibPjtApproveWaitAssetApplyDetailViewResponseBean();
		ChaLibPjtApproveWaitAssetApplyDetailViewServiceBean beanChild = bean.getChaLibPjtApproveWaitAssetApplyDetailViewServiceBean() ;

		resBean.setApplyNo			( beanChild.getApplyNo() );
		resBean.setApplyUser		( beanChild.getChaLibEntryBaseInfoBean().getApplyUser() );
		resBean.setApplyGroup		( beanChild.getChaLibEntryBaseInfoBean().getGroupName() );
		resBean.setApplyDate		( beanChild.getChaLibEntryBaseInfoBean().getApplyDate() );
		resBean.setApplySubject		( beanChild.getChaLibEntryBaseInfoBean().getSubject() );
		resBean.setApplyContent		( beanChild.getChaLibEntryBaseInfoBean().getContent() );
		resBean.setPjtNo			( beanChild.getChaLibEntryBaseInfoBean().getPrjNo() );
		resBean.setChangeCauseNo	( beanChild.getChaLibEntryBaseInfoBean().getChangeCauseNo() ) ;
		resBean.setPjtApproveEnabled( beanChild.isPjtApproveEnabled() );
		resBean.setLotNo			( bean.getLockLotNo() );
		resBean.setServerNo			( bean.getLockServerId() );
		resBean.setReturnAssetAppendFile			( beanChild.getChaLibEntryBaseInfoBean().getReturnAssetAppendFile() );
		resBean.setReturnAssetAppendFileLink			( beanChild.getChaLibEntryBaseInfoBean().getReturnAssetAppendFileLink() );

		List<TableItem> itemList = new ArrayList<TableItem>();
		for ( ChaLibAssetResourceViewBean resourceBean : beanChild.getChaLibEntryAssetResourceInfoBean().getAssetResourceViewBeanList() ){

			if ( resourceBean.isFile() ) {

				Span filePathSpan = new Span();
				filePathSpan.setValue( resourceBean.getRelativeResourcePath() );

				Span fileStatusSpan = new Span();
				fileStatusSpan.setValue(resourceBean.getFileStatus());

				Span fileStatusIdSpan = new Span();
				fileStatusIdSpan.setValue(resourceBean.getFileStatusId());

				Span diffEnabledSpan = new Span();
				if( !FileStatus.Delete.equals( resourceBean.getFileStatusId() ) ) {
					diffEnabledSpan.setValue( ( true == resourceBean.isDiffEnabled() ) ? "1" : "0" );
				} else {
					diffEnabledSpan.setValue( "0" ) ;
				}

				TableItem item = new TableItem();
				item.setColumn( new ITag[]{ filePathSpan, fileStatusSpan, diffEnabledSpan, fileStatusIdSpan } );
				itemList.add( item );
			}
		}

		Table assetListTable = new Table();
		assetListTable.setList( (TableItem[])itemList.toArray( new TableItem[]{} ));
		resBean.setAssetFileList( assetListTable );


		List<TableItem> moduleItemList = new ArrayList<TableItem>();
		for ( ChaLibAssetResourceViewBean resourceBean : beanChild.getChaLibEntryAssetResourceInfoBean().getAssetBinaryResourceViewBeanList() ){

			Span filePathSpan = new Span();
			filePathSpan.setValue( resourceBean.getRelativeResourcePath() );

			Span fileStatusSpan = new Span();
			fileStatusSpan.setValue( resourceBean.getFileStatus() );

			Span diffEnabledSpan = new Span();
			diffEnabledSpan.setValue( "0" ) ;

			TableItem item = new TableItem();
			item.setColumn( new ITag[]{ filePathSpan, fileStatusSpan, diffEnabledSpan } );
			moduleItemList.add( item );
		}

		Table binaryAssetListTable = new Table();
		binaryAssetListTable.setList( (TableItem[])moduleItemList.toArray( new TableItem[]{} ));
		resBean.setAssetBinaryFileList( binaryAssetListTable );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibPjtApproveAssetApplyDetailViewResponseBean convertChaLibPjtApproveAssetApplyDetailViewResponseBean(FlowChaLibPjtApproveAssetApplyDetailViewServiceBean bean) {

		ChaLibPjtApproveAssetApplyDetailViewResponseBean resBean = new ChaLibPjtApproveAssetApplyDetailViewResponseBean();
		ChaLibPjtApproveAssetApplyDetailViewServiceBean compositionBean = bean.getChaLibPjtApproveAssetApplyDetailViewServiceBean() ;

		resBean.setLotNo				( compositionBean.getLotNo() ) ;
		resBean.setApplyNo				( compositionBean.getApplyNo() );
		resBean.setPjtNo				( compositionBean.getBaseInfoViewBean().getPrjNo() );
		resBean.setChangeCauseNo		( compositionBean.getBaseInfoViewBean().getChangeCauseNo() ) ;
		resBean.setApplyGroup			( compositionBean.getBaseInfoViewBean().getGroupName() );
		resBean.setApplyDate			( compositionBean.getBaseInfoViewBean().getApplyDate() );
		resBean.setApplySubject			( compositionBean.getBaseInfoViewBean().getSubject() );
		resBean.setApplyContent			( compositionBean.getBaseInfoViewBean().getContent() );
		resBean.setApplyUser			( compositionBean.getBaseInfoViewBean().getApplyUser() );
		resBean.setPjtApproveDate		( compositionBean.getBaseInfoViewBean().getPjtApproveDate() );
		resBean.setPjtApproveUser		( compositionBean.getBaseInfoViewBean().getPjtApproveUser() );
		resBean.setPjtApproveCancelDate	( compositionBean.getBaseInfoViewBean().getPjtApproveCancelDate() );
		resBean.setPjtApproveCancelUser	( compositionBean.getBaseInfoViewBean().getPjtApproveCancelUser() );
		resBean.setStatus				( compositionBean.getStatusView() );
		resBean.setReturnAssetAppendFile( compositionBean.getBaseInfoViewBean().getReturnAssetAppendFile() );
		resBean.setReturnAssetAppendFileLink( compositionBean.getBaseInfoViewBean().getReturnAssetAppendFileLink() );


		List<String> assetsNameList  = new ArrayList<String>();

		for ( ChaLibAssetResourceViewBean viewBean : compositionBean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			String relativePath = viewBean.getRelativeResourcePath() ;
			if ( viewBean.isFile() ) {
				assetsNameList.add( relativePath );
			}
		}
		resBean.setApproveFileList( assetsNameList );


		List<String> assetsBinaryNameList  = new ArrayList<String>();

		for ( ChaLibAssetResourceViewBean viewBean : compositionBean.getAssetSelectViewBean().getAssetBinaryResourceViewBeanList() ) {
			String relativePath = viewBean.getRelativeResourcePath() ;
			if ( viewBean.isFile() ) {
				assetsBinaryNameList.add( relativePath );
			}
		}
		resBean.setApproveBinaryFileList( assetsBinaryNameList );

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveResponseBean convertChaLibPjtApproveConfirmResponseBean(FlowChaLibPjtApproveServiceBean bean) {

		ChaLibPjtApproveResponseBean resBean = new ChaLibPjtApproveResponseBean();

		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());
		if( null != bean.getPjtEditInputBean() ) {
			resBean.setPjtEditInputBean( bean.getPjtEditInputBean() ) ;
		}
		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveResponseBean convertChaLibCompPjtApproveResponseBean(FlowChaLibPjtApproveServiceBean bean) {

		ChaLibPjtApproveResponseBean resBean = new ChaLibPjtApproveResponseBean();

		resBean.setSelectedPjtNo( bean.getSelectedPjtNo() );

		String [] pjtNoList = bean.getSelectedPjtNo();
		if ( pjtNoList != null ) {
			StringBuilder buff = new StringBuilder();

			for ( String pjtNo : pjtNoList ) {

				if ( TriStringUtils.isEmpty( pjtNo ) ) continue;

				if( 0 < buff.length() ) {
					buff.append(",");
				}
				buff.append( pjtNo );
			}
			resBean.setPjtNoString( buff.toString() );
		}
		resBean.setPjtEditInputBean	( bean.getPjtEditInputBean() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveRejectResponseBean convertChaLibPjtApproveRejectConfirmResponseBean(FlowChaLibPjtApproveRejectServiceBean bean) {

		ChaLibPjtApproveRejectResponseBean resBean = new ChaLibPjtApproveRejectResponseBean();

		resBean.setSelectedApplyNo(bean.getSelectedApplyNo());
		resBean.setApplyInfoDetailViewBean(bean.getApplyInfoDetailViewBean());
		resBean.setPjtEditInputBean(bean.getPjtEditInputBean());
		resBean.setPjtDetailViewBean( bean.getPjtDetailViewBean() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveRejectResponseBean convertChaLibCompPjtApproveRejectResponseBean(FlowChaLibPjtApproveRejectServiceBean bean) {

		ChaLibPjtApproveRejectResponseBean resBean = new ChaLibPjtApproveRejectResponseBean();

		resBean.setSelectedApplyNo(bean.getSelectedApplyNo());
		resBean.setApplyInfoDetailViewBean(bean.getApplyInfoDetailViewBean());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveListResponseBean convertChaLibPjtApproveListResponseBean(
			FlowChaLibPjtApproveListServiceBean bean ) {

		ChaLibPjtApproveListResponseBean resBean = new ChaLibPjtApproveListResponseBean();

		resBean.setPjtApproveHistoryViewBeanList( bean.getPjtApproveHistoryViewBeanList() );
		resBean.setPageNoNum					( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo					( bean.getPageInfoView().getSelectPageNo() );
		resBean.setSelectedLotNo				( bean.getSelectedLotNo() );
		resBean.setSelectedServerNo				( bean.getLockServerId() );
		resBean.setLotViewBeanList				( bean.getLotViewBeanList() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveDetailViewResponseBean convertChaLibPjtApproveDetailViewResponseBean(FlowChaLibPjtApproveDetailViewServiceBean bean) {

		ChaLibPjtApproveDetailViewResponseBean resBean = new ChaLibPjtApproveDetailViewResponseBean();

		resBean.setSelectedLotNo( bean.getSelectedLotNo() ) ;
		resBean.setSelectedPjtNo( bean.getSelectedPjtNo() ) ;
		resBean.setPjtDetailViewBean(bean.getPjtDetailViewBean());
		resBean.setApplyInfoViewBeanList(bean.getApplyInfoViewBeanList());
		resBean.setPjtApproveHistoryViewBeanList(bean.getPjtApproveHistoryViewBeanList());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveCancelResponseBean convertChaLibPjtApproveCancelConfirmResponseBean(FlowChaLibPjtApproveCancelServiceBean bean) {

		ChaLibPjtApproveCancelResponseBean resBean = new ChaLibPjtApproveCancelResponseBean();

		resBean.setSelectedPjtNo				( bean.getSelectedPjtNo() );
		resBean.setSelectedApproveDate			( bean.getPjtAvlSeqNo() );
		resBean.setPjtEditInputBean				( bean.getPjtEditInputBean());
		resBean.setPjtDetailViewBean			( bean.getPjtDetailViewBean() );
		resBean.setApplyInfoViewBeanList		( bean.getApplyInfoViewBeanList() );
		resBean.setPjtApproveHistoryViewBeanList( bean.getPjtApproveHistoryViewBeanList() );
		resBean.setRelatedApplyInfoViewBeanList	( bean.getRelatedApplyInfoViewBeanList() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveCancelResponseBean convertChaLibCompPjtApproveCancelResponseBean(FlowChaLibPjtApproveCancelServiceBean bean) {

		ChaLibPjtApproveCancelResponseBean resBean = new ChaLibPjtApproveCancelResponseBean();

		resBean.setSelectedPjtNo				( bean.getSelectedPjtNo() );
		resBean.setSelectedApproveDate			( bean.getPjtAvlSeqNo() );
		resBean.setPjtDetailViewBean			( bean.getPjtDetailViewBean() );
		resBean.setApplyInfoViewBeanList		( bean.getApplyInfoViewBeanList() );
		resBean.setPjtApproveHistoryViewBeanList( bean.getPjtApproveHistoryViewBeanList() );
		resBean.setRelatedApplyInfoViewBeanList	( bean.getRelatedApplyInfoViewBeanList() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendListResponseBean convertChaLibLendRtnListResponseBean(
	FlowChaLibLendListServiceBean bean ) {

		ChaLibLendListResponseBean resBean = new ChaLibLendListResponseBean();

		resBean.setLotViewBeanList	( bean.getLotViewBeanList() );
		resBean.setLotNo			( bean.getSelectedLotNo() );
		resBean.setServerNo			( bean.getLockServerId() );
		resBean.setApplyViewBeanList( bean.getApplyViewBeanList() );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibRtnListResponseBean convertChaLibRtnListResponseBean(
			FlowChaLibRtnListServiceBean bean ) {

		ChaLibRtnListResponseBean resBean = new ChaLibRtnListResponseBean();

		resBean.setApplyViewBeanList( bean.getApplyViewBeanList() );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );
		resBean.setLotNo			( bean.getSelectedLotNo() );
		resBean.setServerNo			( bean.getLockServerId() );
		resBean.setLotViewBeanList	( bean.getLotViewBeanList() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendEntryResponseBean convertChaLibLendEntryResponseBean(FlowChaLibLendEntryServiceBean bean) {

		ChaLibLendEntryResponseBean resBean = new ChaLibLendEntryResponseBean();

		ChaLibEntryBaseInfoBean baseInfoBean = bean.getBaseInfoViewBean();
		ChaLibSelectLendEntryBaseInfoBean selectBaseInfoBean = bean.getSelectBaseInfoViewBean();

		resBean.setLotNo			( bean.getLotNo() );
		resBean.setPjtNo			( baseInfoBean.getPrjNo() );
		resBean.setChangeCauseNo	( baseInfoBean.getChangeCauseNo() ) ;
		resBean.setApplyUser		( bean.getUserName() );
		resBean.setApplyGroup		( baseInfoBean.getGroupName() );
		resBean.setLendApplySubject	( baseInfoBean.getSubject() );
		resBean.setLendApplyComment	( baseInfoBean.getContent() );

		//resBean.setLendApplyFile	( (String)reqInfo.getAttribute( "FILE_NAME2" ));
		resBean.setLendApplyFile	( bean.getInBaseInfoBean().getLendApplyFileName() );
		resBean.setLendApplyMode	( bean.getInBaseInfoBean().getLendApplyMode() );

		resBean.setLotViewBeanList	( selectBaseInfoBean.getSelectLotNo() );
		resBean.setPjtViewBeanList	( selectBaseInfoBean.getSelectPjtViewBeanList() );
		resBean.setApplyGroupList	( selectBaseInfoBean.getSelectGroupName() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendModifyResponseBean convertChaLibLendModifyResponseBean(FlowChaLibLendModifyServiceBean bean) {

		ChaLibLendModifyResponseBean resBean = new ChaLibLendModifyResponseBean();

		ChaLibEntryBaseInfoBean baseInfoBean = bean.getBaseInfoViewBean();

		resBean.setLotNo			( bean.getLotNo() );
		resBean.setPjtNo			( baseInfoBean.getPrjNo() );
		resBean.setChangeCauseNo	( baseInfoBean.getChangeCauseNo() ) ;

		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setApplyUser		( baseInfoBean.getApplyUser() );
		resBean.setApplyGrop		( baseInfoBean.getGroupName() );
		resBean.setLendApplySubject	( baseInfoBean.getSubject() );
		resBean.setLendApplyComment	( baseInfoBean.getContent() );

		if ( null != bean.getInBaseInfoBean() ) {
			resBean.setLendApplyFile	( bean.getInBaseInfoBean().getLendApplyFileName() );
			resBean.setLendApplyMode	( bean.getInBaseInfoBean().getLendApplyMode() );
		}

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendEntryAssetSelectResponseBean
		convertChaLibLendEntryAssetSelectResponseBean( GenericServiceBean bean ) {

		ChaLibLendEntryAssetSelectResponseBean resBean			= new ChaLibLendEntryAssetSelectResponseBean();
		ChaLibEntryAssetResourceInfoBean assetResourceInfoBean	= new ChaLibEntryAssetResourceInfoBean();

		if ( bean instanceof FlowChaLibLendEntryServiceBean ) {
			resBean.setFlow( "/flowChaLibLendEntry" );
			assetResourceInfoBean = ((FlowChaLibLendEntryServiceBean)bean).getAssetSelectViewBean();
			resBean.setDuplicationCheck( ((FlowChaLibLendEntryServiceBean)bean).isDuplicationCheck() );
		}

		if ( bean instanceof FlowChaLibLendModifyServiceBean ) {
			resBean.setFlow( "/flowChaLibLendModify" );
			assetResourceInfoBean = ((FlowChaLibLendModifyServiceBean)bean).getAssetSelectViewBean();
			resBean.setDuplicationCheck( ((FlowChaLibLendModifyServiceBean)bean).isDuplicationCheck() );
		}

		//カレントパス
		resBean.setPathInfoViewBeanList( assetResourceInfoBean.getPathInfoViewBeanList() ) ;

		Table table				= new Table();
		List<TableItem> items	= new ArrayList<TableItem>();

		for ( ChaLibAssetResourceViewBean viewBean : assetResourceInfoBean.getAssetResourceViewBeanList() ) {

			CheckBox resourceCheckBox = new CheckBox();
			resourceCheckBox.setValue	( viewBean.getResourcePath() );
			// チェックが入っているものはチェックを入れる。
			resourceCheckBox.setChecked	( viewBean.isChecked() );
			// 貸出中の場合は、チェックボックスを非表示とする
			resourceCheckBox.setHidden	( viewBean.isHidden() );

			Image resourceImage = new Image();
			if ( viewBean.isFile() ) {
				// ファイルの場合
				resourceImage.setValue( "File" );
			} else {
				// ディレクトリの場合
				resourceImage.setValue( "Directory" );
			}

			Span resourcePathSpan	= new Span();
			resourcePathSpan.setValue( viewBean.getResourceName() );

			TableItem tableitem		= new TableItem();
			tableitem.setColumn( new ITag[]{ resourceCheckBox, resourceImage, resourcePathSpan } );

			items.add( tableitem );

		}
		table.setList( (TableItem[])items.toArray( new TableItem[0] ));

		resBean.setResourceTable( table );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendEntryConfirmResponseBean convertChaLibLendEntryConfirmResponseBean(
			FlowChaLibLendEntryServiceBean bean ) {

		ChaLibLendEntryConfirmResponseBean resBean = new ChaLibLendEntryConfirmResponseBean();

		resBean.setLotNo			( bean.getLotNo() );

		resBean.setApplyUser		( bean.getInBaseInfoBean().getInUserName() );
		resBean.setApplyGrop		( bean.getInBaseInfoBean().getInGroupName() );
		resBean.setLendApplySubject	( bean.getInBaseInfoBean().getInSubject() );
		resBean.setLendApplyComment	( bean.getInBaseInfoBean().getInContent() );

		resBean.setPjtNo			( bean.getInBaseInfoBean().getInPrjNo() );
		resBean.setChangeCauseNo	( bean.getInBaseInfoBean().getInChangeCauseNo() ) ;

		// 絶対パス→相対パスへの変換
		String masterPath		= bean.getAssetSelectViewBean().getMasterPath();
		List<String> fileList	= new ArrayList<String>();

		if( bean.getInAssetSelectBean().getInAssetResourcePath() != null ){
			for ( String selectAsset : bean.getInAssetSelectBean().getInAssetResourcePath() ) {
				selectAsset = TriStringUtils.convertRelativePath( new File(masterPath), selectAsset );

				//selectAsset = selectAsset.replace(masterPath, "");
				fileList.add( selectAsset );
			}
		}
		resBean.setLendApplyAssetsNameList( fileList );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendModifyConfirmResponseBean convertChaLibLendModifyConfirmResponseBean(FlowChaLibLendModifyServiceBean bean) {

		ChaLibLendModifyConfirmResponseBean resBean = new ChaLibLendModifyConfirmResponseBean();

		resBean.setLotNo			( bean.getLotNo() );
		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setApplyUser		( bean.getInBaseInfoBean().getInUserName() );
		resBean.setApplyGrop		( bean.getInBaseInfoBean().getInGroupName() );
		resBean.setLendApplySubject	( bean.getInBaseInfoBean().getInSubject() );
		resBean.setLendApplyComment	( bean.getInBaseInfoBean().getInContent() );

		resBean.setPjtNo			( bean.getInBaseInfoBean().getInPrjNo() );
		resBean.setChangeCauseNo	( bean.getInBaseInfoBean().getInChangeCauseNo() ) ;

		// 絶対パス→相対パスへの変換
		String masterPath = bean.getAssetSelectViewBean().getMasterPath();
		List<String> fileList = new ArrayList<String>();
		if(bean.getInAssetSelectBean() != null){
			for ( String selectAsset : bean.getInAssetSelectBean().getInAssetResourcePath() ) {
				selectAsset = TriStringUtils.convertRelativePath( new File(masterPath), selectAsset );

				//selectAsset = selectAsset.replace(masterPath, "");
				fileList.add(selectAsset);
			}
		}

		resBean.setLendApplyAssetsNameList(fileList);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibCompLendEntryResponseBean convertChaLibCompLendEntryResponseBean(FlowChaLibLendEntryServiceBean bean) {

		ChaLibCompLendEntryResponseBean resBean = new ChaLibCompLendEntryResponseBean();

		resBean.setApplyNo( bean.getApplyNo() );
		resBean.setApplyDate( bean.getBaseInfoViewBean().getApplyDate() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibCompLendModifyResponseBean convertChaLibCompLendModifyResponseBean(FlowChaLibLendModifyServiceBean bean) {

		ChaLibCompLendModifyResponseBean resBean = new ChaLibCompLendModifyResponseBean();

//		resBean.setApplyNo(bean.getApplyNo());
//		resBean.setApplyDate(bean.getApplyDate());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendDetailViewResponseBean convertChaLibLendDetailViewResponseBean(FlowChaLibLendDetailViewServiceBean bean) {

		ChaLibLendDetailViewResponseBean resBean = new ChaLibLendDetailViewResponseBean();

		resBean.setLotNo			( bean.getLotNo() ) ;
		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setPjtNo			( bean.getBaseInfoViewBean().getPrjNo() );
		resBean.setChangeCauseNo	( bean.getBaseInfoViewBean().getChangeCauseNo() ) ;
		resBean.setApplyGrop		( bean.getBaseInfoViewBean().getGroupName() );
		resBean.setLendApplySubject	( bean.getBaseInfoViewBean().getSubject() );
		resBean.setLendApplyComment	( bean.getBaseInfoViewBean().getContent() );
		resBean.setApplyUser		( bean.getBaseInfoViewBean().getApplyUser() );
		resBean.setStatus			( bean.getStatusView() );

		List<String> assetsNameList  = new ArrayList<String>();
		String masterPath = bean.getAssetSelectViewBean().getMasterPath();
		for ( ChaLibAssetResourceViewBean viewBean : bean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			if ( viewBean.isFile() ) {
				String relativePath = TriStringUtils.convertRelativePath( new File(masterPath), viewBean.getResourcePath() );
				assetsNameList.add( relativePath );
				//assetsNameList.add(viewBean.getResourcePath().replace(masterPath, ""));
			}
		}
		resBean.setLendApplyAssetsNameList(assetsNameList);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibLendHistoryDetailViewResponseBean convertChaLibLendHistoryDetailViewResponseBean(FlowChaLibLendHistoryDetailViewServiceBean bean) {

		ChaLibLendHistoryDetailViewResponseBean resBean = new ChaLibLendHistoryDetailViewResponseBean();

		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setPjtNo			( bean.getBaseInfoViewBean().getPrjNo() );
		resBean.setChangeCauseNo	( bean.getBaseInfoViewBean().getChangeCauseNo() ) ;
		resBean.setApplyGrop		( bean.getBaseInfoViewBean().getGroupName() );
		resBean.setLendApplySubject	( bean.getBaseInfoViewBean().getSubject() );
		resBean.setLendApplyComment	( bean.getBaseInfoViewBean().getContent() );
		resBean.setApplyUser		( bean.getBaseInfoViewBean().getApplyUser() );
		resBean.setStatus			( bean.getStatusView() );

		List<String> assetsNameList  = new ArrayList<String>();
		String masterPath = bean.getAssetSelectViewBean().getMasterPath();
		for ( ChaLibAssetResourceViewBean viewBean : bean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			if ( viewBean.isFile() ) {
				String relativePath = TriStringUtils.convertRelativePath( new File(masterPath), viewBean.getResourcePath() );
				assetsNameList.add( relativePath );
				//assetsNameList.add(viewBean.getResourcePath().replace(masterPath, ""));
			}
		}
		resBean.setLendApplyAssetsNameList(assetsNameList);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibRtnDetailViewResponseBean convertChaLibRtnDetailViewResponseBean(FlowChaLibRtnDetailViewServiceBean bean , IRequestInfo reqInfo ) {

		ChaLibRtnDetailViewResponseBean resBean = new ChaLibRtnDetailViewResponseBean();

		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setPjtNo			( bean.getBaseInfoViewBean().getPrjNo() );
		resBean.setChangeCauseNo	( bean.getBaseInfoViewBean().getChangeCauseNo() ) ;
		resBean.setApplyGrop		( bean.getBaseInfoViewBean().getGroupName() );
		resBean.setRtnApplySubject	( bean.getBaseInfoViewBean().getSubject() );
		resBean.setRtnApplyComment	( bean.getBaseInfoViewBean().getContent() );
		resBean.setApplyUser		( bean.getBaseInfoViewBean().getApplyUser() );
		resBean.setLotNo			( bean.getBaseInfoViewBean().getLotNo() );
		resBean.setStatus			( bean.getStatusView() );
		resBean.setReturnAssetAppendFile		( bean.getReturnAssetAppendFile() );

		List<String> assetsNameList  = new ArrayList<String>();
		String masterPath = bean.getAssetSelectViewBean().getMasterPath();
		for ( ChaLibAssetResourceViewBean viewBean : bean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			if ( viewBean.isFile() ) {
				String relativePath = TriStringUtils.convertRelativePath( new File(masterPath), viewBean.getResourcePath() );
				assetsNameList.add( relativePath );

				//assetsNameList.add(viewBean.getResourcePath().replace(masterPath, ""));
			}
		}
		resBean.setRtnApplyAssetsNameList(assetsNameList);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibRtnHistoryDetailViewResponseBean convertChaLibRtnHistoryDetailViewResponseBean(FlowChaLibRtnHistoryDetailViewServiceBean bean) {

		ChaLibRtnHistoryDetailViewResponseBean resBean = new ChaLibRtnHistoryDetailViewResponseBean();

		resBean.setLotNo		( bean.getLotNo() ) ;
		resBean.setApplyNo		( bean.getApplyNo() );
		resBean.setPjtNo			( bean.getBaseInfoViewBean().getPrjNo() );
		resBean.setApplyGrop		( bean.getBaseInfoViewBean().getGroupName() );
		resBean.setRtnApplySubject	( bean.getBaseInfoViewBean().getSubject() );
		resBean.setRtnApplyComment	( bean.getBaseInfoViewBean().getContent() );
		resBean.setApplyUser		( bean.getBaseInfoViewBean().getApplyUser() );
		resBean.setStatus			( bean.getStatusView() );

		List<String> assetsNameList  = new ArrayList<String>();
		String masterPath = bean.getAssetSelectViewBean().getMasterPath();
		for ( ChaLibAssetResourceViewBean viewBean : bean.getAssetSelectViewBean().getAssetResourceViewBeanList() ) {
			if ( viewBean.isFile() ) {
				String relativePath = TriStringUtils.convertRelativePath( new File(masterPath), viewBean.getResourcePath() );
				assetsNameList.add( relativePath );

				//assetsNameList.add(viewBean.getResourcePath().replace(masterPath, ""));
			}
		}
		resBean.setRtnApplyAssetsNameList(assetsNameList);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibRtnEntryResponseBean convertChaLibRtnEntryConfirmResponseBean(FlowChaLibRtnEntryServiceBean bean) {

		ChaLibRtnEntryResponseBean resBean = new ChaLibRtnEntryResponseBean();
		resBean.setLotNo			( bean.getLotNo() );
		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setApplyUser		( bean.getChaLibEntryBaseInfoBean().getApplyUser() );
		resBean.setApplyGrop		( bean.getChaLibEntryBaseInfoBean().getGroupName() );
		resBean.setLendApplySubject	( bean.getChaLibEntryBaseInfoBean().getSubject() );
		resBean.setLendApplyComment	( bean.getChaLibEntryBaseInfoBean().getContent() );
		resBean.setPjtNo			( bean.getChaLibEntryBaseInfoBean().getPrjNo() );
		resBean.setProcId			( bean.getProcId() );

		resBean.setChangeCauseNo	( bean.getChaLibEntryBaseInfoBean().getChangeCauseNo() ) ;
		resBean.setServerNo			( bean.getLockServerId() );
		resBean.setReturnAssetAppendFile		( bean.getReturnAssetAppendFile() ) ;

		// ファイルの件数取得（フォルダパスも含まれるため）
		int fileCount = 0;
		for ( ChaLibAssetResourceViewBean resourceBan : bean.getChaLibEntryAssetResourceInfoBean().getAssetResourceViewBeanList() ){
			if ( resourceBan.isFile() ) {
				fileCount++;
			}
		}

		TableItem[] item = new TableItem[fileCount];
		int index = 0;
		String masterPath = bean.getChaLibEntryAssetResourceInfoBean().getMasterPath();

		for ( ChaLibAssetResourceViewBean resourceBan : bean.getChaLibEntryAssetResourceInfoBean().getAssetResourceViewBeanList() ){

			if ( resourceBan.isFile() ) {

				Span filePathSpan = new Span();
				String relativePath = TriStringUtils.convertRelativePath( new File(masterPath), resourceBan.getResourcePath() );
				filePathSpan.setValue( relativePath );

				//filePathSpan.setValue(resourceBan.getResourcePath().replace(masterPath, ""));

				Span fileStatusSpan = new Span();
				fileStatusSpan.setValue(resourceBan.getFileStatus());

				Span fileStatusIdSpan = new Span();
				fileStatusIdSpan.setValue(resourceBan.getFileStatusId());

				Span diffEnabledSpan = new Span();
				diffEnabledSpan.setValue( ( true == resourceBan.isDiffEnabled() ) ? "1" : "0" );

				item[index] = new TableItem();
				item[index].setColumn(new ITag[]{filePathSpan, fileStatusSpan, diffEnabledSpan, fileStatusIdSpan});

				index++;
			}
		}

		Table lendsListTable = new Table();
		lendsListTable.setList(item);
		resBean.setLendFileList(lendsListTable);

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibMasterDelListResponseBean convertChaLibMasterDelListResponseBean(
			FlowChaLibMasterDelListServiceBean bean ) {

		ChaLibMasterDelListResponseBean resBean = new ChaLibMasterDelListResponseBean();

		resBean.setMasterDelViewBeanList( bean.getTopMenuViewBeanList() );
		resBean.setPageNoNum			( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo			( bean.getPageInfoView().getSelectPageNo() );
		resBean.setLotNo				( bean.getSelectedLotNo() );
		resBean.setServerNo				( bean.getLockServerId() );
		resBean.setLotViewBeanList		( bean.getLotViewBeanList() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibMasterDelEntryResponseBean convertChaLibMasterDelEntryResponseBean(FlowChaLibMasterDelEntryServiceBean bean, IRequestInfo reqInfo) {

		ChaLibMasterDelEntryResponseBean resBean = new ChaLibMasterDelEntryResponseBean();

		resBean.setApplyUserName		(bean.getUserName());
		String referer = reqInfo.getParameter("referer");
		if ( referer.equals(ChaLibScreenID.PJT_DETAIL_VIEW) ) {
			resBean.setPjtNo				( reqInfo.getParameter("pjtNo") );
		} else {
			resBean.setPjtNo				( reqInfo.getParameter("selectedPjtNo") );
		}
		resBean.setLotNo				( bean.getSelectedLotNo());
		resBean.setDelApplySubject		( reqInfo.getParameter("delApplySubject") );
		resBean.setDelReason			( reqInfo.getParameter("delReason") );
		resBean.setPjtViewBeanList		( bean.getSelectPjtViewList() );
		resBean.setSelectUserGroup		( reqInfo.getParameter("selectedApplyGroup") );
		resBean.setApproveUserGroupList	( bean.getApprovUserGroupList());

		resBean.setDelApplyFile			( (String)reqInfo.getAttribute("FILE_NAME1") );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibMasterDelEntryConfirmResponseBean convertChaLibMasterDelEntryConfirmResponseBean(
																FlowChaLibMasterDelEntryServiceBean bean,
																IRequestInfo reqInfo) {

		ChaLibMasterDelEntryConfirmResponseBean resBean = new ChaLibMasterDelEntryConfirmResponseBean();
		resBean.setLotNo			( bean.getLotNo() ) ;
		resBean.setApplyUserName	( bean.getViewServiceBean().getDelApplyUser() 		);
		resBean.setPjtNo			( bean.getViewServiceBean().getPrjNo() 				);
		resBean.setChangeCauseNo	( bean.getViewServiceBean().getChgFactorNo() 		);
		resBean.setDelApplySubject	( bean.getViewServiceBean().getDelApplySubject() 	);
		resBean.setDelReason		( bean.getViewServiceBean().getDelReason() 			);
		resBean.setApplyUserGroup	( bean.getViewServiceBean().getUserGroup() 			);

		resBean.setDelFileList		( FluentList.from(bean.getViewServiceBean().getApplyFilePath()).asList());
		resBean.setDelBinaryFileList( FluentList.from(bean.getViewServiceBean().getApplyBinaryFilePath()).asList());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibCompMasterDelEntryResponseBean convertChaLibCompMasterDelEntryResponseBean(
			FlowChaLibMasterDelEntryServiceBean bean ) {

		ChaLibCompMasterDelEntryResponseBean resBean = new ChaLibCompMasterDelEntryResponseBean();
		resBean.setApplyNo( bean.getApplyNo() );
		resBean.setApplyDate( bean.getApplyDate() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibMasterDelDetailViewResponseBean convertChaLibMasterDelDetailViewResponseBean(
								FlowChaLibMasterDelDetailViewServiceBean bean, IRequestInfo reqInfo) {

		ChaLibMasterDelDetailViewResponseBean resBean = new ChaLibMasterDelDetailViewResponseBean();

		resBean.setLotNo			( bean.getLotNo()									);
		resBean.setDelApplyNo		( bean.getDelApplyNo()								);
		resBean.setApplyUserName	( bean.getViewServiceBean().getDelApplyUser()		);
		resBean.setPjtNo			( bean.getViewServiceBean().getPrjNo()				);
		resBean.setChangeCauseNo    ( bean.getViewServiceBean().getChgFactorNo()		);
		resBean.setDelReason		( bean.getViewServiceBean().getDelReason()			);
		resBean.setDelApplySubject	( bean.getViewServiceBean().getDelApplySubject()	);
		resBean.setApplyUserGroup	( bean.getViewServiceBean().getUserGroup()			);

		resBean.setDelFileList		( FluentList.from(bean.getViewServiceBean().getApplyFilePath()).asList());
		resBean.setDelBinaryFileList( FluentList.from(bean.getViewServiceBean().getApplyBinaryFilePath()).asList());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibCompMasterDelCancelResponseBean convertChaLibCompMasterDelCancelResponseBean( FlowChaLibCompMasterDelCancelServiceBean bean ) {

		ChaLibCompMasterDelCancelResponseBean resBean = new ChaLibCompMasterDelCancelResponseBean();

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static ChaLibMasterDelHistoryListResponseBean convertChaLibMasterDelHistoryListResponseBean(
			FlowChaLibMasterDelHistoryListServiceBean bean ) {

		ChaLibMasterDelHistoryListResponseBean resBean = new ChaLibMasterDelHistoryListResponseBean();

		resBean.setMasterDelViewBeanList( bean.getTopMenuViewBeanList() );
		resBean.setPageNoNum			( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo			( bean.getPageInfoView().getSelectPageNo() );
		resBean.setLotViewBeanList		( bean.getLotViewBeanList() );
		resBean.setSelectedLotNo		( bean.getSelectedLotNo() );
		resBean.setSelectedServerNo		( bean.getLockServerId() );

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibMasterDelHistoryViewResponseBean convertChaLibMasterDelHistoryViewResponseBean(
															FlowChaLibMasterDelHistoryViewServiceBean bean,
															IRequestInfo reqInfo) {

		ChaLibMasterDelHistoryViewResponseBean resBean = new ChaLibMasterDelHistoryViewResponseBean();

		resBean.setLotNo			( bean.getLotNo() ) ;
		resBean.setDelApplyNo		( bean.getDelApplyNo()								);
		resBean.setApplyUserName	( bean.getViewServiceBean().getDelApplyUser()		);
		resBean.setApplyUserGroup	( bean.getViewServiceBean().getUserGroup()			);
		resBean.setPjtNo			( bean.getViewServiceBean().getPrjNo()				);
		resBean.setChangeCauseNo	( bean.getViewServiceBean().getChgFactorNo() 		);
		resBean.setDelApplySubject	( bean.getViewServiceBean().getDelApplySubject()	);
		resBean.setDelReason		( bean.getViewServiceBean().getDelReason()			);

		resBean.setDelFileList		( FluentList.from(bean.getViewServiceBean().getApplyFilePath()).asList());
		resBean.setDelBinaryFileList( FluentList.from(bean.getViewServiceBean().getApplyBinaryFilePath()).asList());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param reqInfo
	 * @return
	 */
	public static ChaLibCompMasterDelRejectResponseBean convertChaLibCompMasterDelRejectResponseBean(IRequestInfo reqInfo) {

		ChaLibCompMasterDelRejectResponseBean resBean = new ChaLibCompMasterDelRejectResponseBean();

		resBean.setDelApplyNo(reqInfo.getParameter("delApplyNo"));

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @param bean
	 * @return
	 */
	public static BaseResponseBean convertChaLibCompRtnEntryResponseBean(
			FlowChaLibRtnEntryServiceBean bean ) {

		ChaLibCompRtnEntryResponseBean resBean = new ChaLibCompRtnEntryResponseBean();

		resBean.setLotNo			( bean.getLotNo() ) ;
		resBean.setApplyNo			( bean.getApplyNo() );
		resBean.setApplyUser		( bean.getApplyUserName() );
		resBean.setApplyGrop		( bean.getApplyUserGroup() );
		resBean.setDuplicationCheck	( bean.isDuplicationCheck() );
		resBean.setProcId			( bean.getProcId() );

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibConflictCheckLotListResponseBean convertChaLibConflictCheckLotListResponseBean(FlowChaLibConflictCheckLotListServiceBean bean) {

		ChaLibConflictCheckLotListResponseBean resBean = new ChaLibConflictCheckLotListResponseBean();

		resBean.setLotViewBeanList(bean.getLotViewBeanList());
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibConflictCheckBaselineListResponseBean convertChaLibConflictCheckBaselineListResponseBean(
			FlowChaLibConflictCheckBaselineListServiceBean bean ) {

		ChaLibConflictCheckBaselineListResponseBean resBean = new ChaLibConflictCheckBaselineListResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());
		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for (int i=0; i<moduleName.length; i++) {
			if(i>0) {
				buff.append(", ");
			}
			buff.append(moduleName[i]);
		}
		resBean.setModule(buff.toString());
		resBean.setSucceededRelUnitCount(bean.getSucceededRelUnitCount());
		resBean.setFailedRelUnitCount(bean.getFailedRelUnitCount());
		resBean.setBaselineViewBeanList(bean.getBaselineViewBeanList());
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibConflictCheckBaselineDetailViewResponseBean convertChaLibConflictCheckBaselineDetailViewResponseBean(
			FlowChaLibConflictCheckBaselineDetailViewServiceBean bean ) {

		ChaLibConflictCheckBaselineDetailViewResponseBean resBean = new ChaLibConflictCheckBaselineDetailViewResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());
		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for (int i=0; i<moduleName.length; i++) {
			if(i>0) {
				buff.append(", ");
			}
			buff.append(moduleName[i]);
		}
		resBean.setModule(buff.toString());
		resBean.setSucceededRelUnitCount(bean.getSucceededRelUnitCount());
		resBean.setFailedRelUnitCount(bean.getFailedRelUnitCount());
		resBean.setBaselineViewBean(bean.getBaselineViewBean());
		resBean.setBuildViewBeanList( bean.getBuildViewBeanList());
		resBean.setPjtViewBeanList( bean.getPjtViewBeanList() ) ;
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibConflictCheckResponseBean convertChaLibConflictCheckConfirmResponseBean(FlowChaLibConflictCheckServiceBean bean) {

		ChaLibConflictCheckResponseBean resBean = new ChaLibConflictCheckResponseBean();
		resBean.setSelectedLotNo( bean.getSelectedLotId() ) ;
		resBean.setSelectedCloseVersionTag( bean.getSelectedLotVersionTag() ) ;
		resBean.setBaselineViewBean( bean.getHeadBlViewBean() );

		if( null != bean.getMergeEditInputBean() ) {
			resBean.setMergeEditInputBean( bean.getMergeEditInputBean() ) ;
		}
//		マージエラー警告メッセージ
		resBean.setWarningCheckMergeCommitError( bean.getWarningCheckMergeCommitError() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibConflictCheckResponseBean convertChaLibCompConflictCheckResponseBean(FlowChaLibConflictCheckServiceBean bean) {

		ChaLibConflictCheckResponseBean resBean = new ChaLibConflictCheckResponseBean();

		resBean.setSelectedLotNo( bean.getSelectedLotId() ) ;
		resBean.setSelectedCloseVersionTag( bean.getSelectedLotVersionTag() ) ;
		resBean.setBaselineViewBean( bean.getHeadBlViewBean() );

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeLotListResponseBean convertChaLibMergeLotListResponseBean(FlowChaLibMergeLotListServiceBean bean) {

		ChaLibMergeLotListResponseBean resBean = new ChaLibMergeLotListResponseBean();

		resBean.setLotViewBeanList(bean.getLotViewBeanList());
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeBaselineDetailViewResponseBean convertChaLibMergeBaselineDetailViewResponseBean(
			FlowChaLibMergeBaselineDetailViewServiceBean bean ) {

		ChaLibMergeBaselineDetailViewResponseBean resBean = new ChaLibMergeBaselineDetailViewResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());
		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for (int i=0; i<moduleName.length; i++) {
			if(i>0) {
				buff.append(", ");
			}
			buff.append(moduleName[i]);
		}
		resBean.setModule(buff.toString());
		resBean.setSucceededRelUnitCount(bean.getSucceededRelUnitCount());
		resBean.setFailedRelUnitCount(bean.getFailedRelUnitCount());
		resBean.setBaselineViewBean(bean.getBaselineViewBean());
		resBean.setBuildViewBeanList( bean.getBuildViewBeanList() ) ;
		resBean.setPjtViewBeanList( bean.getPjtViewBeanList() ) ;
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());
		resBean.setMergeStatus( bean.getMergeStatus() ) ;
		resBean.setConflictComment( bean.getConflictComment() ) ;

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setPublicFolder	( bean.getLotDetailViewBean().getPublicFolder() );
		resBean.setPrivateFolder( bean.getLotDetailViewBean().getPrivateFolder() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeCommitResponseBean convertChaLibMergeCommitConfirmResponseBean(FlowChaLibMergeCommitServiceBean bean) {

		ChaLibMergeCommitResponseBean resBean = new ChaLibMergeCommitResponseBean();
		resBean.setSelectedLotNo( bean.getSelectedLotId() ) ;
		resBean.setSelectedCloseVersionTag( bean.getSelectedLotVersionTag() ) ;
		resBean.setBaselineViewBean(bean.getHeadBlViewBean());

		if( null != bean.getMergeEditInputBean() ) {
			resBean.setMergeEditInputBean( bean.getMergeEditInputBean() ) ;
		}
		//マージエラー警告メッセージ
		resBean.setWarningCheckMergeCommitError( bean.getWarningCheckMergeCommitError() ) ;

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeCommitResponseBean convertChaLibCompMergeCommitResponseBean(FlowChaLibMergeCommitServiceBean bean) {

		ChaLibMergeCommitResponseBean resBean = new ChaLibMergeCommitResponseBean();

		resBean.setSelectedLotNo( bean.getSelectedLotId() ) ;
		resBean.setSelectedCloseVersionTag( bean.getSelectedLotVersionTag() ) ;
		resBean.setBaselineViewBean( bean.getHeadBlViewBean() ) ;

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeConflictCheckResultResponseBean convertChaLibMergeConflictCheckResponseBean(
			FlowChaLibMergeConflictCheckResultServiceBean bean ) {

		ChaLibMergeConflictCheckResultResponseBean resBean = new ChaLibMergeConflictCheckResultResponseBean();

		resBean.setSelectedLotNo			( bean.getSelectedLotNo() ) ;
		resBean.setSelectedCloseVersionTag	( bean.getSelectedCloseVersionTag() ) ;
		resBean.setBaselineViewBean			( bean.getBaselineViewBean() ) ;
		resBean.setMergeViewBeanList		( bean.getMergeViewBeanList() ) ;
		resBean.setSelectedServerNo			( bean.getLockServerId() ) ;

		return resBean;
	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeHistoryLotListResponseBean convertChaLibMergeHistoryLotListResponseBean(FlowChaLibMergeHistoryLotListServiceBean bean) {

		ChaLibMergeHistoryLotListResponseBean resBean = new ChaLibMergeHistoryLotListResponseBean();

		resBean.setLotViewBeanList(bean.getLotViewBeanList());
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeHistoryBaselineListResponseBean convertChaLibMergeHistoryBaselineListResponseBean(
			FlowChaLibMergeHistoryBaselineListServiceBean bean ) {

		ChaLibMergeHistoryBaselineListResponseBean resBean = new ChaLibMergeHistoryBaselineListResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());
		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for (int i=0; i<moduleName.length; i++) {
			if(i>0) {
				buff.append(", ");
			}
			buff.append(moduleName[i]);
		}
		resBean.setModule(buff.toString());
		resBean.setSucceededRelUnitCount(bean.getSucceededRelUnitCount());
		resBean.setFailedRelUnitCount(bean.getFailedRelUnitCount());
		resBean.setBaselineViewBeanList(bean.getBaselineViewBeanList());
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setPublicFolder	( bean.getLotDetailViewBean().getPublicFolder() );
		resBean.setPrivateFolder( bean.getLotDetailViewBean().getPrivateFolder() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeHistoryBaselineDetailViewResponseBean convertChaLibMergeHistoryBaselineDetailViewResponseBean(
			FlowChaLibMergeHistoryBaselineDetailViewServiceBean bean ) {

		ChaLibMergeHistoryBaselineDetailViewResponseBean resBean = new ChaLibMergeHistoryBaselineDetailViewResponseBean();

		resBean.setLotDetailViewBean(bean.getLotDetailViewBean());
		resBean.setLotNo(bean.getLotDetailViewBean().getLotNo());
		resBean.setLotName(bean.getLotDetailViewBean().getLotName());
		resBean.setLotStatus(bean.getLotDetailViewBean().getLotStatus());
		resBean.setInputDate(bean.getLotDetailViewBean().getInputDate());
		resBean.setRecentTag(bean.getLotDetailViewBean().getRecentTag());
		resBean.setRecentVersion(bean.getLotDetailViewBean().getRecentVersion());
		String [] moduleName = bean.getLotDetailViewBean().getModuleName();
		StringBuilder buff = new StringBuilder();
		for (int i=0; i<moduleName.length; i++) {
			if(i>0) {
				buff.append(", ");
			}
			buff.append(moduleName[i]);
		}
		resBean.setModule(buff.toString());
		resBean.setSucceededRelUnitCount(bean.getSucceededRelUnitCount());
		resBean.setFailedRelUnitCount(bean.getFailedRelUnitCount());
		resBean.setBaselineViewBean(bean.getBaselineViewBean());
		resBean.setBuildViewBeanList( bean.getBuildViewBeanList());
		resBean.setPjtViewBeanList( bean.getPjtViewBeanList() ) ;
		resBean.setPageInfoView(bean.getPageInfoView());
		resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		resBean.setServerNo		( bean.getLotDetailViewBean().getServerNo() );
		resBean.setServerName	( bean.getLotDetailViewBean().getServerName() );

		resBean.setPublicFolder	( bean.getLotDetailViewBean().getPublicFolder() );
		resBean.setPrivateFolder( bean.getLotDetailViewBean().getPrivateFolder() );

		resBean.setScmType		( bean.getScmType() );


		return resBean;
	}
//	/**
//	 * レスポンスへの変換
//	 *
//	 * @param totalList
//	 * @param maxPageNumber
//	 * @return
//	 */
//	private static List<List<String>> DivideRsourceListByPageUnit(String[] totalList, int maxPageNumber) {
//
//		List<List<String>> retList = new ArrayList<List<String>>();
//		List<String> divList = new ArrayList<String>();
//		int count = 0;
//
//		for (String resource : totalList) {
//
//			count++;
//			divList.add(resource);
//
//			if (count == maxPageNumber) {
//				retList.add(divList);
//				count = 0;
//				divList = new ArrayList<String>();
//			}
//		}
//
//		if ( divList.size() > 0 ) {
//			retList.add(divList);
//		}
//
//		return retList;
//	}

	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibRtnResourceDiffResponseBean convertCompareReturnResourceResponseBean(
			FlowChaLibRtnResourceDiffServiceBean bean ) {

		ChaLibRtnResourceDiffResponseBean resBean = new ChaLibRtnResourceDiffResponseBean();

		resBean.setSelectedResource	( bean.getSelectedResource() ) ;
		resBean.setDiffResultList	( bean.getDiffResultList() ) ;
		resBean.setEncoding			( bean.getEncoding() ) ;
		//List<IDiffElement> diffResultList = resBean.getDiffResultList() ;
		//resBean.setLotViewBeanList(bean.getLotViewBeanList());
		//resBean.setPageInfoView(bean.getPageInfoView());
		//resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean convertChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean(FlowChaLibPjtApproveWaitAssetApplyDetailViewServiceBean bean) {

		ChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean resBean = new ChaLibPjtApproveWaitAssetApplyResourceDiffResponseBean();
		ChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean beanChild = bean.getChaLibPjtApproveWaitAssetApplyResourceDiffServiceBean() ;

		resBean.setSelectedResource	( beanChild.getSelectedResource() ) ;
		resBean.setDiffResultList	( beanChild.getDiffResultList() ) ;
		resBean.setEncoding			( beanChild.getEncoding() ) ;
		//List<IDiffElement> diffResultList = resBean.getDiffResultList() ;
		//resBean.setLotViewBeanList(bean.getLotViewBeanList());
		//resBean.setPageInfoView(bean.getPageInfoView());
		//resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibMergeConflictCheckResultDiffResponseBean convertChaLibMergeConflictCheckResultDiffBean(FlowChaLibMergeConflictCheckResultDiffServiceBean bean) {

		ChaLibMergeConflictCheckResultDiffResponseBean resBean = new ChaLibMergeConflictCheckResultDiffResponseBean();

		resBean.setSelectedResource( bean.getSelectedResource() ) ;
		resBean.setDiffResultList( bean.getDiffResultList() ) ;
		resBean.setEncoding( bean.getEncoding() ) ;
		//List<IDiffElement> diffResultList = resBean.getDiffResultList() ;
		//resBean.setLotViewBeanList(bean.getLotViewBeanList());
		//resBean.setPageInfoView(bean.getPageInfoView());
		//resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
	/**
	 * レスポンスへの変換
	 *
	 * @return
	 */
	public static ChaLibPjtApproveAssetApplyResourceDiffResponseBean convertChaLibPjtApproveAssetApplyResourceDiffResponseBean(FlowChaLibPjtApproveAssetApplyDetailViewServiceBean bean) {

		ChaLibPjtApproveAssetApplyResourceDiffResponseBean resBean = new ChaLibPjtApproveAssetApplyResourceDiffResponseBean();
		ChaLibPjtApproveAssetApplyResourceDiffServiceBean beanChild = bean.getChaLibPjtApproveAssetApplyResourceDiffServiceBean() ;

		resBean.setSelectedResource( beanChild.getSelectedResource() ) ;
		resBean.setDiffResultList( beanChild.getDiffResultList() ) ;
		resBean.setEncoding( beanChild.getEncoding() ) ;
		//List<IDiffElement> diffResultList = resBean.getDiffResultList() ;
		//resBean.setLotViewBeanList(bean.getLotViewBeanList());
		//resBean.setPageInfoView(bean.getPageInfoView());
		//resBean.setSelectPageNo(bean.getPageInfoView().getSelectPageNo());

		return resBean;
	}
}
