package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopViewServiceBean.TopMenuViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMasterDelHistoryListResponseBean extends BaseResponseBean {

	/** 一覧表示のリスト */
	private List<TopMenuViewBean> masterDelViewBeanList;

	/** ページ番号数 */
	private Integer pageNoNum;

	/** 選択ページ番号 */
	private Integer selectPageNo;

	/** ロット番号 */
	private String selectedLotNo = null;

	/** サーバ番号 */
	private String selectedServerNo = null;

	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	public List<TopMenuViewBean> getMasterDelViewBeanList() {
		return masterDelViewBeanList;
	}
	public void setMasterDelViewBeanList(List<TopMenuViewBean> masterDelViewBeanList) {
		this.masterDelViewBeanList = masterDelViewBeanList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

}

