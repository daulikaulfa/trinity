package jp.co.blueship.tri.am.ui.areq.beans;

import java.io.InputStream;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibMasterDelEntryInputServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibMasterDelEntryServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibMasterDelEntryProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibMasterDelEntryService";

	public static final String[] screenFlows = new String[] {
														ChaLibScreenID.MASTER_DEL_ENTRY,
														ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM,
														ChaLibScreenID.COMP_MASTER_DEL_ENTRY };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.MASTER_DEL_LIST,
																	ChaLibScreenID.PJT_DETAIL_VIEW,
																	 } ;

	public FlowChaLibMasterDelEntryProsecutor() {
		super(null);
	}

	public FlowChaLibMasterDelEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		info = ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();

		return info;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
			throws Exception {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibMasterDelEntryServiceBean bean = new FlowChaLibMasterDelEntryServiceBean();

		if ( !TriStringUtils.isEmpty( session.getAttribute( FLOW_ACTION_ID ) )
				&& session.getAttribute( FLOW_ACTION_ID ) instanceof FlowChaLibMasterDelEntryServiceBean  ) {
			bean = (FlowChaLibMasterDelEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());


		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setLockLotNo		( selectedLotNo );
		bean.setSelectedLotNo	( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( selectedServerNo );


		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( "  session:lotNo:=" + selectedLotNo );
			this.getLog().debug( "  referer:=" + referer );
			this.getLog().debug( "  forward:=" + forward );
			this.getLog().debug( "  screenType:=" + screenType );
		}

		if ( referer.equals(ChaLibScreenID.PJT_DETAIL_VIEW) ) {
			ChaLibMasterDelEntryInputServiceBean inBean = new ChaLibMasterDelEntryInputServiceBean();
			inBean.setInPrjNo( reqInfo.getParameter("pjtNo") );
			bean.setInputServiceBean( inBean );
		}

		if ( forward.equals(ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM) ) {

			// 通常処理時
			if ( referer.equals(ChaLibScreenID.MASTER_DEL_ENTRY) &&
				 this.getBussinessException() == null ) {

				ChaLibMasterDelEntryInputServiceBean inBean = new ChaLibMasterDelEntryInputServiceBean();
				inBean.setInApplyFileInputStreamBytes	(
						StreamUtils.convertInputStreamToBytes( (InputStream)reqInfo.getAttribute("FILE_INPUT_STREAM1") ));
				inBean.setInApplyFileInputStreamName	( (String)reqInfo.getAttribute("FILE_NAME1") );

				inBean.setInPrjNo( reqInfo.getParameter("selectedPjtNo") );

				inBean.setInDelApplySubject(reqInfo.getParameter("delApplySubject"));
				inBean.setInDelReason(reqInfo.getParameter("delReason"));
				inBean.setInUserGroup(reqInfo.getParameter("selectedApplyGroup"));

				bean.setInputServiceBean( inBean );
				session.setAttribute( FLOW_ACTION_ID, bean);
			}
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowChaLibMasterDelEntryServiceBean bean = (FlowChaLibMasterDelEntryServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		if ( forward.equals(ChaLibScreenID.MASTER_DEL_ENTRY) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibMasterDelEntryResponseBean(bean, reqInfo);
		} else if ( forward.equals(ChaLibScreenID.MASTER_DEL_ENTRY_CONFIRM) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibMasterDelEntryConfirmResponseBean(bean, reqInfo);
		} else if ( forward.equals(ChaLibScreenID.COMP_MASTER_DEL_ENTRY) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibCompMasterDelEntryResponseBean( bean );
		}

		PreConditions.assertOf(resBean != null, "ResponseBean is null.");
		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		// RMI対策
		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor( IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

}
