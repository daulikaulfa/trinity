package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMasterDelHistoryViewResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId;
	/** 削除申請番号 */
	private String delApplyNo;
	/** 変更管理番号 */
	private String pjtNo;
	/** 変更要因番号 */
	private String changeCauseNo;
	/** 申請件名 */
	private String delApplySubject;
	/** 削除申請理由 */
	private String delReason;
	/** 申請ユーザ */
	private String applyUserName;
	/** 申請グループ */
	private String applyUserGroup;
	/** 削除資産リスト（原本） */
	private List<String> delFileList;
	/** 削除資産リスト（モジュール） */
	private List<String> delBinaryFileList;
	
	
	public String getApplyUserName() {
		return applyUserName;
	}
	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}
	public List<String> getDelFileList() {
		return delFileList;
	}
	public void setDelFileList( List<String> delFileList ) {
		this.delFileList = delFileList;
	}
	public List<String> getDelBinaryFileList() {
		return delBinaryFileList;
	}
	public void setDelBinaryFileList( List<String> delBinaryFileList ) {
		this.delBinaryFileList = delBinaryFileList;
	}
	public String getDelReason() {
		return delReason;
	}
	public void setDelReason(String delReason) {
		this.delReason = delReason;
	}
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	public String getDelApplyNo() {
		return delApplyNo;
	}
	public void setDelApplyNo(String delApplyNo) {
		this.delApplyNo = delApplyNo;
	}
	public String getApplyUserGroup() {
		return applyUserGroup;
	}
	public void setApplyUserGroup(String applyUserGroup) {
		this.applyUserGroup = applyUserGroup;
	}
	public String getDelApplySubject() {
		return delApplySubject;
	}
	public void setDelApplySubject(String delApplySubject) {
		this.delApplySubject = delApplySubject;
	}
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	
}
