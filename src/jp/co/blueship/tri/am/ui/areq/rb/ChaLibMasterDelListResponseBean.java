package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopViewServiceBean.TopMenuViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMasterDelListResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId = null;

	/** サーバ番号 */
	private String serverNo = null;

	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	/** 一覧表示のリスト */
	private List<TopMenuViewBean> masterDelViewBeanList;

	/** ページ番号数 */
	private Integer pageNoNum;

	/** 選択ページ番号 */
	private Integer selectPageNo;

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}

	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public List<TopMenuViewBean> getMasterDelViewBeanList() {
		return masterDelViewBeanList;
	}
	public void setMasterDelViewBeanList(List<TopMenuViewBean> masterDelViewBeanList) {
		this.masterDelViewBeanList = masterDelViewBeanList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

}
