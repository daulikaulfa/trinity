package jp.co.blueship.tri.am.ui.areq.beans;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnHistoryDetailViewResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibRtnHistoryDetailViewProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibRtnHistoryDetailViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.RTN_HISTORY_DETAIL_VIEW,
																	ChaLibScreenID.PJT_HISTORY_DETAIL_VIEW,
																	 } ;

	public FlowChaLibRtnHistoryDetailViewProsecutor() {
		super(null);
	}

	public FlowChaLibRtnHistoryDetailViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
			throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibRtnHistoryDetailViewServiceBean bean = new FlowChaLibRtnHistoryDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		bean.setApplyNo(reqInfo.getParameter("applyNo"));
		session.setAttribute("applyNo", bean.getApplyNo());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String lotId	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setLockLotNo	( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( serverNo );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowChaLibRtnHistoryDetailViewServiceBean bean = (FlowChaLibRtnHistoryDetailViewServiceBean)getServiceReturnInformation();

		ChaLibRtnHistoryDetailViewResponseBean resBean = AmCnvActionToServiceUtils.convertChaLibRtnHistoryDetailViewResponseBean(bean);

		String[] selectResource = new String[resBean.getRtnApplyAssetsNameList().size()];
		for(int i = 0; i < resBean.getRtnApplyAssetsNameList().size(); i++){
			selectResource[i] = (String) resBean.getRtnApplyAssetsNameList().get(i);
		}

		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo,
			IRequestInfo reqInfo) {
	}

}
