package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibCompRtnEntryResponseBean extends BaseResponseBean {

	private String lotId ;

	private String applyNo ;

	private String applyUser ;

	private String applyGrop ;

	private String procId;
	// プレコンパイル実行フラグ
	private boolean preCompileExecute = false;
	// プレコンパイルメッセージ
	private String preCompileMessage;
	// プレコンパイル返却フォルダ内部コピー
	private String preCompileReturnFileCopyStatus;
	// プレコンパイル資産重複チェック
	private String preCompileDuplicationCheckStatus;
	// プレコンパイル拡張子チェック
	private String preCompileExtensionCheckStatus;
	// プレコンパイル構文チェック
	private String preCompileSyntaxCheckStatus;
	// ２重貸出チェック
	private boolean duplicationCheck;
	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean duplicationCheck ) {
		this.duplicationCheck = duplicationCheck;
	}

	public String getPreCompileDuplicationCheckStatus() {
		return preCompileDuplicationCheckStatus;
	}
	public void setPreCompileDuplicationCheckStatus(
			String preCompileDuplicationCheckStatus) {
		this.preCompileDuplicationCheckStatus = preCompileDuplicationCheckStatus;
	}
	public String getPreCompileExtensionCheckStatus() {
		return preCompileExtensionCheckStatus;
	}
	public void setPreCompileExtensionCheckStatus(
			String preCompileExtensionCheckStatus) {
		this.preCompileExtensionCheckStatus = preCompileExtensionCheckStatus;
	}
	public String getPreCompileMessage() {
		return preCompileMessage;
	}
	public void setPreCompileMessage(String preCompileMessage) {
		this.preCompileMessage = preCompileMessage;
	}
	public String getPreCompileReturnFileCopyStatus() {
		return preCompileReturnFileCopyStatus;
	}
	public void setPreCompileReturnFileCopyStatus(
			String preCompileReturnFileCopyStatus) {
		this.preCompileReturnFileCopyStatus = preCompileReturnFileCopyStatus;
	}
	public String getPreCompileSyntaxCheckStatus() {
		return preCompileSyntaxCheckStatus;
	}
	public void setPreCompileSyntaxCheckStatus(String preCompileSyntaxCheckStatus) {
		this.preCompileSyntaxCheckStatus = preCompileSyntaxCheckStatus;
	}
	public String getProcId() {
	    return procId;
	}
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	public boolean isPreCompileExecute() {
		return preCompileExecute;
	}
	public void setPreCompileExecute(boolean preCompileExecute) {
		this.preCompileExecute = preCompileExecute;
	}
	public String getApplyGrop() {
		return applyGrop;
	}
	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getApplyUser() {
		return applyUser;
	}
	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

}
