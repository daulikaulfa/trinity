package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.PathInfoViewBean;
import jp.co.blueship.tri.am.ui.beans.dto.Table;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLendEntryAssetSelectResponseBean extends BaseResponseBean {
	
	private List<PathInfoViewBean> pathInfoViewBeanList = new ArrayList<PathInfoViewBean>() ;
	private Table resourceTable;
	private String flow;
	// ２重貸出チェック
	private boolean duplicationCheck;

	public void setResourceTable(Table table) {
		this.resourceTable = table;
	}
	public Table getResourceTable() {
		return this.resourceTable;
	}			
	public String getFlow() {
		return flow;
	}
	public void setFlow(String flow) {
		this.flow = flow;
	}
	public List<PathInfoViewBean> getPathInfoViewBeanList() {
		return pathInfoViewBeanList;
	}
	public void setPathInfoViewBeanList(List<PathInfoViewBean> pathInfoViewBeanList) {
		this.pathInfoViewBeanList = pathInfoViewBeanList;
	}
	public boolean isDuplicationCheck() {
		return duplicationCheck;
	}
	public void setDuplicationCheck( boolean duplicationCheck ) {
		this.duplicationCheck = duplicationCheck;
	}
}
