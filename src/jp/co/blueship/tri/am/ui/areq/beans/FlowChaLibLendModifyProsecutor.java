package jp.co.blueship.tri.am.ui.areq.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibInputLendEntryAssetSelectBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibLendModifyServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.AmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibLendModifyProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibLendModifyService";

	public static final String[] screenFlows = new String[] {
														ChaLibScreenID.LEND_MODIFY,
														ChaLibScreenID.LEND_ENTRY_ASSET_SELECT,
														ChaLibScreenID.LEND_MODIFY_CONFIRM,
														ChaLibScreenID.COMP_LEND_MODIFY };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.LEND_LIST,
																	ChaLibScreenID.LEND_DETAIL_VIEW,
																	ChaLibScreenID.PJT_DETAIL_VIEW,
																	 } ;

	public FlowChaLibLendModifyProsecutor() {
		super(null);
	}

	public FlowChaLibLendModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		info = ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();

		return info;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
			throws Exception {

		this.getLog().debug( "prosecutor getBussinessInfo start!!" );

		try {
			SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
			session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

			FlowChaLibLendModifyServiceBean bean = null;
			if ( !TriStringUtils.isEmpty( session.getAttribute( FLOW_ACTION_ID ) )
					&& session.getAttribute( FLOW_ACTION_ID ) instanceof FlowChaLibLendModifyServiceBean  ) {
				bean = (FlowChaLibLendModifyServiceBean)session.getAttribute( FLOW_ACTION_ID );
				//ワーニングメッセージの初期化
				bean.setInfoMessage( null );
			} else {
				bean = new FlowChaLibLendModifyServiceBean();
				bean.setApplyNo((String)reqInfo.getParameter("applyNo"));
			}

			String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
			bean.setUserId(userId);
			bean.setUserName(userName());

			String referer = session.getReferer( this );
			String forward = session.getForward( this );
			String screenType = session.getScreenType( this );

			if ( this.getLog().isDebugEnabled() ) {
				this.getLog().debug( "  :referer:=" + referer );
				this.getLog().debug( "  :forward:=" + forward );
				this.getLog().debug( "  :screenType:=" + screenType );
			}

			bean.setReferer(referer);
			bean.setForward(forward);
			bean.setScreenType(screenType);

			String lotId	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
			bean.setLockLotNo	( lotId );

			String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
			bean.setLockServerId( serverNo );


			if ( referer.equals(ChaLibScreenID.LEND_MODIFY) ) {
				ChaLibEntryInputBaseInfoBean inputBean = AmCnvServiceToActionUtils.convertChaLibEntryInputBaseInfoBean( reqInfo );

				bean.setInBaseInfoBean(inputBean);
			}

			if ( referer.equals(ChaLibScreenID.LEND_ENTRY_ASSET_SELECT) ) {
				ChaLibInputLendEntryAssetSelectBean inputBean = new ChaLibInputLendEntryAssetSelectBean();
				if ( ! TriStringUtils.isEmpty( bean.getInAssetSelectBean() ) ) {
					inputBean = bean.getInAssetSelectBean();
				} else {
					bean.setInAssetSelectBean(inputBean);
				}

				String[] sessionResource = inputBean.getInAssetResourcePath();
				String[] requestResouce = reqInfo.getParameterValues("selectResource");

				String beforeDir = inputBean.getInDirectory();
				String afterDir = reqInfo.getParameter("selectDirectory");

				afterDir = (TriStringUtils.isEmpty(afterDir))? beforeDir: afterDir;

				if (	ScreenType.select.equals(screenType) ||
						ScreenType.next.equals(screenType) ) {

					if ( null == requestResouce )
						requestResouce = new String[]{};

					if( ! TriStringUtils.isEmpty( sessionResource ) ) {
						List<String> selectResourceList = new ArrayList<String>();

						if( ! TriStringUtils.isEmpty( beforeDir ) ) {
							for( String resource : sessionResource ) {
								String resourceDir = resource.substring( 0, resource.lastIndexOf("/"));

								if( !( resourceDir ).equals( beforeDir ) ){
									selectResourceList.add( resource );
								}
							}
						} else {
							for ( String resource : sessionResource ) {
								selectResourceList.add( resource );
							}
						}

						for ( String resource : requestResouce ) {
							selectResourceList.add( resource );
						}

						requestResouce = selectResourceList.toArray( new String[0] );
					}

					inputBean.setInAssetResourcePath( requestResouce );
				}

				if ( ScreenType.select.equals(screenType) )
					inputBean.setInDirectory( afterDir );

				ChaLibEntryBaseInfoBean baseInfo =
					AmCnvServiceToActionUtils.convertChaLibEntryBaseInfoBean( bean.getInBaseInfoBean() );

				bean.setBaseInfoViewBean( baseInfo );

			}

			if ( referer.equals(ChaLibScreenID.LEND_MODIFY_CONFIRM) ) {
			}

			session.setAttribute( FLOW_ACTION_ID, bean );

			return bean;
		} finally {
			this.getLog().debug( "prosecutor getBussinessInfo end!!" );
		}
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowChaLibLendModifyServiceBean bean = (FlowChaLibLendModifyServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		if ( forward.equals(ChaLibScreenID.LEND_MODIFY) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibLendModifyResponseBean(bean);
		}

		if ( forward.equals(ChaLibScreenID.LEND_ENTRY_ASSET_SELECT) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibLendEntryAssetSelectResponseBean(bean);
		}

		if ( forward.equals(ChaLibScreenID.LEND_MODIFY_CONFIRM) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibLendModifyConfirmResponseBean(bean);
		}

		if ( forward.equals(ChaLibScreenID.COMP_LEND_MODIFY) ) {
			resBean = AmCnvActionToServiceUtils.convertChaLibCompLendModifyResponseBean(bean);
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		// RMI対策
		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( FLOW_ACTION_ID , bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo,
			IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
