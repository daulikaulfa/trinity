package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibRtnEntryCheckResponseBean extends BaseResponseBean {

	private String lotId ;

	private String applyNo ;

	private String applyUser ;

	private String applyGrop ;

	private String procId;

	public String getApplyGrop() {
		return applyGrop;
	}

	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getProcId() {
	    return procId;
	}

	public void setProcId(String procId) {
	    this.procId = procId;
	}


}
