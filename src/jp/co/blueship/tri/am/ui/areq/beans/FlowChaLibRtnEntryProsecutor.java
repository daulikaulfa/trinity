package jp.co.blueship.tri.am.ui.areq.beans;

import java.io.InputStream;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompRtnEntryResponseBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class FlowChaLibRtnEntryProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibRtnEntryService";

	public static final String[] screenFlows = new String[] {
														ChaLibScreenID.RTN_ENTRY_CONFIRM,
														ChaLibScreenID.COMP_RTN_ENTRY };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.RTN_DETAIL_VIEW,
																	ChaLibScreenID.PJT_DETAIL_VIEW,
																	 } ;

	public FlowChaLibRtnEntryProsecutor() {
		super(null);
	}

	public FlowChaLibRtnEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {

		// 返却完了画面の場合は何もしない
		if( ((FlowChaLibRtnEntryServiceBean)info).getForward().equals( ChaLibScreenID.COMP_RTN_ENTRY ) ) {
			return info;
		}
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo)
			throws Exception {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibRtnEntryServiceBean bean = null;
		if ( !TriStringUtils.isEmpty( session.getAttribute( FLOW_ACTION_ID ) )
				&& session.getAttribute( FLOW_ACTION_ID ) instanceof FlowChaLibRtnEntryServiceBean  ) {
			bean = (FlowChaLibRtnEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowChaLibRtnEntryServiceBean();
		}

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★");
			this.getLog().debug( "  :referer:=" + referer );
			this.getLog().debug( "  :forward:=" + forward );
			this.getLog().debug( "  :screenType:=" + screenType );
		}

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		bean.setApplyNo(reqInfo.getParameter("applyNo"));
		bean.setLotNo(reqInfo.getParameter("lotNo"));

		String lotId	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setLockLotNo	( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( serverNo );
//
//		bean.setLockByThread( true ) ;

		// 遷移元画面ID処理
		if( referer.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM) ){
			bean.setLotNo(reqInfo.getParameter("lotNo"));
			bean.setApplyNo(reqInfo.getParameter("applyNo"));
			bean.setApplyUserName(reqInfo.getParameter("applyUser"));
			bean.setApplyUserGroup(reqInfo.getParameter("applyUserGroup"));

		} else if( referer.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){

		}

		// 遷移先画面ID処理
		if( forward.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM) ) {
			bean.setReturnAssetAppendFileInputStreamBytes(
					StreamUtils.convertInputStreamToBytes( (InputStream)reqInfo.getAttribute("FILE_INPUT_STREAM1") ));
			bean.setReturnAssetAppendFileInputStreamName	( (String)reqInfo.getAttribute("FILE_NAME1") );
		} else if( forward.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){

		} else if( forward.equals(ChaLibScreenID.COMP_RTN_ENTRY) ){

		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);

		BaseResponseBean resBean = null;
		FlowChaLibRtnEntryServiceBean bean = (FlowChaLibRtnEntryServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		// 遷移元画面ID処理
		if( forward.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM) ){
			resBean = AmCnvActionToServiceUtils.convertChaLibRtnEntryConfirmResponseBean(bean);
		}

		if( forward.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){
			resBean = AmCnvActionToServiceUtils.convertChaLibCompRtnEntryResponseBean(bean);
		}

		if(forward.equals(ChaLibScreenID.COMP_RTN_ENTRY)){
			resBean = new ChaLibCompRtnEntryResponseBean();
		}

		session.setAttribute( FLOW_ACTION_ID, bean );

		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo,
			IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
