package jp.co.blueship.tri.am.ui.areq.beans;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibCompRtnEntryResponseBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibCompRtnEntryProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibRtnEntryService";

	public FlowChaLibCompRtnEntryProsecutor() {
		super(null);
	}

	public FlowChaLibCompRtnEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo( IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibRtnEntryServiceBean bean = new FlowChaLibRtnEntryServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		bean.setLotNo( reqInfo.getParameter("lotNo"));
		bean.setApplyNo(reqInfo.getParameter("applyNo"));

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		// チェック（状況照会）画面からの遷移時はパラメータが存在しないため、ここでセット
		if ( referer == null ) referer = ChaLibScreenID.RTN_ENTRY_CHECK;
		if ( forward == null ) forward = ChaLibScreenID.COMP_RTN_ENTRY;

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★");
			this.getLog().debug("  referer:" + referer);
			this.getLog().debug("  forward:" + forward);
		}

		// 遷移元画面ID処理
		if( referer.equals(ChaLibScreenID.RTN_ENTRY_CONFIRM) ){

			bean.setApplyNo(reqInfo.getParameter("applyNo"));
			bean.setApplyUserName(reqInfo.getParameter("applyUser"));
			bean.setApplyUserGroup(reqInfo.getParameter("applyUserGroup"));

		} else if( referer.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){

		}

		// 遷移先画面ID処理
		if( forward.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){

		} else if( forward.equals(ChaLibScreenID.COMP_RTN_ENTRY) ){

		}

		String lotId	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setLockLotNo	( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( serverNo );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowChaLibRtnEntryServiceBean bean = (FlowChaLibRtnEntryServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		if( forward.equals(ChaLibScreenID.RTN_ENTRY_CHECK) ){

			resBean = AmCnvActionToServiceUtils.convertChaLibCompRtnEntryResponseBean(bean);

		} else if( forward.equals(ChaLibScreenID.COMP_RTN_ENTRY)){

			resBean = new ChaLibCompRtnEntryResponseBean();

		}
		PreConditions.assertOf(resBean != null, "ResponseBean is null.");
		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

}
