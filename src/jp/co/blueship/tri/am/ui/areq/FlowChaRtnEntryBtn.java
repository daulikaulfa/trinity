package jp.co.blueship.tri.am.ui.areq;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.am.constants.ChaLibCallScriptItemID;
import jp.co.blueship.tri.am.constants.ProcCtgCdByReturningRequest;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnEntryServiceBean.ChaLibRtnEntryCheckProcessViewBean;
import jp.co.blueship.tri.am.ui.areq.rb.ChaRtnEntryBtnResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.GenericService;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.ReverseAjaxService;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.SecondaryPresentationController;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


public class FlowChaRtnEntryBtn extends SecondaryPresentationController  {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private final ILog getLog() {
    	return log;
    }

	public static final String FLOW_ACTION_ID = "FlowChaRtnEntryBtnService";

	@Override
	protected GenericService getApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((ReverseAjaxService)service).getApplicationService(FLOW_ACTION_ID, serviceDto);
	}

	@Override
	protected GenericServiceBean getApplicationServiceBean(IGeneralServiceBean info) throws Exception {
		FlowChaLibRtnEntryServiceBean bean = (FlowChaLibRtnEntryServiceBean)info;
		return bean;
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, "FlowChaRtnEntryBtnService");

		FlowChaLibRtnEntryServiceBean bean = new FlowChaLibRtnEntryServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		// チェック（状況照会）画面からの遷移時はパラメータが存在しないため、ここでセット
		String referer = reqInfo.getParameter("referer");
		String forward = reqInfo.getParameter("forward");

		if ( referer == null ) referer = ChaLibScreenID.RTN_ENTRY_CHECK;
		if ( forward == null ) forward = ChaLibScreenID.RTN_ENTRY_CHECK;

		bean.setReferer(referer);
		bean.setForward(forward);

		bean.setLotNo((String) reqInfo.getAttribute("lotNo"));
		bean.setApplyNo((String) reqInfo.getAttribute("applyNo"));
		bean.setProcId((String) reqInfo.getAttribute("procId"));

		return bean;
	}

	@Override
	protected String getForward(IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, IGeneralServiceBean info) {
		return null;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★FlowChaRtnEntryBtnService::getRequestInfo:");
		}

		FlowChaLibRtnEntryServiceBean bean = (FlowChaLibRtnEntryServiceBean)getServiceReturnInformation();
		ChaRtnEntryBtnResponseBean resBean = new ChaRtnEntryBtnResponseBean();

		String preCompileMessage = "";

		if (bean.getCheckProcessViewBeanList() != null && bean.getCheckProcessViewBeanList().size() > 0){

			for ( ChaLibRtnEntryCheckProcessViewBean viewBean : bean.getCheckProcessViewBeanList() ) {

				if ( viewBean.getProcessName().equals(ProcCtgCdByReturningRequest.DUPLICATION_FILE_CHECK) ) {
					resBean.setPreCompileDuplicationCheckStatus(viewBean.getProcessStatus());
				} else if ( viewBean.getProcessName().equals(ProcCtgCdByReturningRequest.RETURN_FILE_COPY) ) {
					resBean.setPreCompileReturnFileCopyStatus(viewBean.getProcessStatus());
				} else if ( viewBean.getProcessName().equals(ProcCtgCdByReturningRequest.EXTENSION_CHECK) ) {
					resBean.setPreCompileExtensionCheckStatus(viewBean.getProcessStatus());
				} else if ( viewBean.getProcessName().equals(ProcCtgCdByReturningRequest.COMPILE_SYNTAX_CHECK) ) {
					resBean.setPreCompileSyntaxCheckStatus(viewBean.getProcessStatus());
				} else if ( viewBean.getProcessName().equals( ProcCtgCdByReturningRequest.CHARSET_CHECK )) {
					resBean.setPreCompileCharsetCheckStatus( viewBean.getProcessStatus() );
				} else if ( viewBean.getProcessName().equals( ProcCtgCdByReturningRequest.LINEFEED_CHECK )) {
					resBean.setPreCompileLinefeedCheckStatus( viewBean.getProcessStatus() );
				} else if ( viewBean.getProcessName().equals( ProcCtgCdByReturningRequest.CALL_SCRIPT )) {
					resBean.setPreCompileCallScriptStatus( viewBean.getProcessStatus() );
				}

			}

			// 機能ON/OFFの制御を取得
			if ( StatusFlg.off.value().equals(
					sheet.getValue(
							AmDesignBeanId.callScriptAtReturnApply,
							ChaLibCallScriptItemID.SummaryAtReturnApply.DO_EXEC.toString() )) ) {

				resBean.setPreCompileCallScriptAvailable(false);
			} else {
				resBean.setPreCompileCallScriptAvailable(true);
			}

			// メッセージの取得
			if ( null != bean.getErrMessage() ) {
//				IContextAdapter ca = ContextAdapterFactory.getMessageContextAdapter();
//				resBean.setPreCompileMessage(ca.getMessageManager().getMessage(bean.getErrMessage(), new String[]{}));
				resBean.setPreCompileMessage(bean.getErrMessage());
			} else {
				resBean.setPreCompileMessage("");
			}

			resBean.setCompleteStatus(bean.getCompleteStatus());
			resBean.setPreCompileExecute(true);

		} else {
			// プレコンパイル未実行
			preCompileMessage = "";
			resBean.setPreCompileMessage(preCompileMessage);

			resBean.setPreCompileExecute(false);
		}

		return resBean;
	}

	@Override
	protected IService getService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalServiceDWR");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	public IBaseResponseBean start(HttpServletRequest request, HttpServletResponse response, String lotIdParam , String applyNoParam, String procIdParam) throws Exception{
		request.setAttribute("lotNo", lotIdParam);
		request.setAttribute("applyNo", applyNoParam);
		request.setAttribute("procId", procIdParam);
		IBaseResponseBean baseBean = super.execute(request, response);
		return baseBean;
	}

	@Override
	protected String getCallbackJSFuncName() {

		return "analysePreCompileResultByExec";
	}

}
