package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLendEntryResponseBean extends BaseResponseBean {
		
	/** ロット番号 */
	private String lotId = null;

	/** 変更管理番号 */
	private String pjtNo;
	
	/** 変更要因番号 */
	private String changeCauseNo;
	
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = new ArrayList<PjtViewBean>();
	
	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	/** 申請ユーザ */
	private String applyUser;
	
	/** 申請グループリスト */
	private List<String> applyGroupList;
	
	/** 申請グループ */
	private String applyGroup;
	
	/** 貸出申請件名 */
	private String lendApplySubject;
	
	/** 貸出申請内容 */
	private String lendApplyComment;
	
	/** 貸出申請情報番号 */
	private String applyNo;

	/** 貸出申請モード */
	private String lendApplyMode;
	
	/** 貸出申請ファイル */
	private String lendApplyFile;
	
	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
	
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}

	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public String getApplyGroup() {
		return applyGroup;
	}

	public void setApplyGroup(String applyGroup) {
		this.applyGroup = applyGroup;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public String getLendApplyComment() {
		return lendApplyComment;
	}

	public void setLendApplyComment(String lendApplyComment) {
		this.lendApplyComment = lendApplyComment;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getLendApplySubject() {
		return lendApplySubject;
	}

	public void setLendApplySubject(String lendApplySubject) {
		this.lendApplySubject = lendApplySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}

	public void setPjtViewBeanList(List<PjtViewBean> pjtViewBeanList) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public List<String> getApplyGroupList() {
		return applyGroupList;
	}

	public void setApplyGroupList(List<String> applyGroupList) {
		this.applyGroupList = applyGroupList;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getLendApplyMode() {
		return lendApplyMode;
	}
	public void setLendApplyMode( String lendApplyMode ) {
		this.lendApplyMode = lendApplyMode;
	}
	
	public String getLendApplyFile() {
		return lendApplyFile;
	}
	public void setLendApplyFile( String lendApplyFile ) {
		this.lendApplyFile = lendApplyFile;
	}
}
