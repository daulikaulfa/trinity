package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibCompMasterDelRejectResponseBean extends BaseResponseBean {

	/** 申請番号 */
	private String delApplyNo;

	public String getDelApplyNo() {
		return delApplyNo;
	}

	public void setDelApplyNo(String delApplyNo) {
		this.delApplyNo = delApplyNo;
	}

}
