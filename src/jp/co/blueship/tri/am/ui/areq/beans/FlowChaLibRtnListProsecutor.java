package jp.co.blueship.tri.am.ui.areq.beans;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibRtnListServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.areq.rb.ChaLibRtnListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibRtnListProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibRtnListService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.RTN_LIST,
																	 } ;

	public FlowChaLibRtnListProsecutor() {
		super(null);
	}

	public FlowChaLibRtnListProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowChaLibRtnListServiceBean bean = new FlowChaLibRtnListServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String selectedLotNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String selectedServerNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );

		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
			bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
		}

		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId )) {
			selectedLotNo = lotId;
		}

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( "★★★" );
			this.getLog().debug( "  lotNo:=" + lotId );
			this.getLog().debug( "  savedPageNo:=" + savedPageNo );
			this.getLog().debug( "  referer:=" + referer );
			this.getLog().debug( "  forward:=" + forward );
			this.getLog().debug( "  screenType:=" + screenType );
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}

		bean.setLockServerId	( selectedServerNo );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowChaLibRtnListServiceBean bean = (FlowChaLibRtnListServiceBean)getServiceReturnInformation();
		ChaLibRtnListResponseBean resBean = AmCnvActionToServiceUtils.convertChaLibRtnListResponseBean( bean );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( "★★★" );
			this.getLog().debug( "  bean.getPageInfoView().getMaxPageNo():=" + bean.getPageInfoView().getMaxPageNo() );
			this.getLog().debug( "  resBean.getPageNoNum():=" + resBean.getPageNoNum() );
			this.getLog().debug( "  resBean.getSelectPageNo():=" + resBean.getSelectPageNo() );
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

}
