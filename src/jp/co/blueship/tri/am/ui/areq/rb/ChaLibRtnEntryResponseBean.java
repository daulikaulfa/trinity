package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.am.domain.lot.beans.dto.LotViewBean;
import jp.co.blueship.tri.am.ui.beans.dto.Table;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibRtnEntryResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId = null;

	/** サーバ番号 */
	private String serverNo = null;

	/** ロット情報 */
	private List<LotViewBean> lotViewBeanList = null;

	/** 申請番号 */
	private String applyNo;

	/** 申請ユーザ */
	private String applyUser;

	/** 申請グループ */
	private String applyGrop;

	/** 申請件名 */
	private String lendApplySubject;

	/** 申請内容 */
	private String lendApplyComment;

	/** 変更管理番号 */
	private String pjtNo;

	/** 変更要因番号 */
	private String changeCauseNo;

	/** 貸出資産リスト */
	private Table lendFileList;

	/** 添付ファイルの名前 */
	private String returnAssetAppendFile = null ;

	/** プロセス番号 **/
	private String procId = null;


	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getServerNo() {
		return serverNo;
	}

	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	public List<LotViewBean> getLotViewBeanList() {
		return lotViewBeanList;
	}

	public void setLotViewBeanList( List<LotViewBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

	public String getApplyGrop() {
		return applyGrop;
	}

	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}

	public String getLendApplyComment() {
		return lendApplyComment;
	}

	public void setLendApplyComment(String lendApplyComment) {
		this.lendApplyComment = lendApplyComment;
	}

	public String getLendApplySubject() {
		return lendApplySubject;
	}

	public void setLendApplySubject(String lendApplySubject) {
		this.lendApplySubject = lendApplySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public Table getLendFileList() {
		return lendFileList;
	}

	public void setLendFileList(Table lendFileList) {
		this.lendFileList = lendFileList;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}

	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}

	public String getProcId() {
	    return procId;
	}

	public void setProcId(String procId) {
	    this.procId = procId;
	}

}
