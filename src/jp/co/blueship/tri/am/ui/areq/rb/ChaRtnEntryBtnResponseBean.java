package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaRtnEntryBtnResponseBean extends BaseResponseBean {
	
	/** 申請番号 */
	private String applyNo;
	// プレコンパイル実行フラグ
	private boolean preCompileExecute = false;
	// プレコンパイルメッセージ
	private String preCompileMessage;
	// プレコンパイル返却フォルダ内部コピー
	private String preCompileReturnFileCopyStatus;
	// プレコンパイル資産重複チェック
	private String preCompileDuplicationCheckStatus;
	// プレコンパイル拡張子チェック
	private String preCompileExtensionCheckStatus;
	// プレコンパイル構文チェック
	private String preCompileSyntaxCheckStatus;
	// プレコンパイル文字コードチェック
	private String preCompileCharsetCheckStatus;
	// プレコンパイル改行コードチェック
	private String preCompileLinefeedCheckStatus;
	// プレコンパイル外部ツール呼び出し
	private String preCompileCallScriptStatus;
	// 完了ステータス
	private String completeStatus;
	
	// プレコンパイル外部ツール呼び出し[ON/OFF]
	private boolean preCompileCallScriptAvailable = false;
	
	public String getPreCompileDuplicationCheckStatus() {
		return preCompileDuplicationCheckStatus;
	}
	public void setPreCompileDuplicationCheckStatus(String preCompileDuplicationCheckStatus) {
		this.preCompileDuplicationCheckStatus = preCompileDuplicationCheckStatus;
	}
	public String getPreCompileExtensionCheckStatus() {
		return preCompileExtensionCheckStatus;
	}
	public void setPreCompileExtensionCheckStatus(String preCompileExtensionCheckStatus) {
		this.preCompileExtensionCheckStatus = preCompileExtensionCheckStatus;
	}
	public String getPreCompileMessage() {
		return preCompileMessage;
	}
	public void setPreCompileMessage(String preCompileMessage) {
		this.preCompileMessage = preCompileMessage;
	}
	public String getPreCompileReturnFileCopyStatus() {
		return preCompileReturnFileCopyStatus;
	}
	public void setPreCompileReturnFileCopyStatus(String preCompileReturnFileCopyStatus) {
		this.preCompileReturnFileCopyStatus = preCompileReturnFileCopyStatus;
	}
	public String getPreCompileSyntaxCheckStatus() {
		return preCompileSyntaxCheckStatus;
	}
	public void setPreCompileSyntaxCheckStatus(String preCompileSyntaxCheckStatus) {
		this.preCompileSyntaxCheckStatus = preCompileSyntaxCheckStatus;
	}
	public String getPreCompileCharsetCheckStatus() {
		return preCompileCharsetCheckStatus;
	}
	public void setPreCompileCharsetCheckStatus( String preCompileCharsetCheckStatus ) {
		this.preCompileCharsetCheckStatus = preCompileCharsetCheckStatus;
	}
	public String getPreCompileLinefeedCheckStatus() {
		return preCompileLinefeedCheckStatus;
	}
	public void setPreCompileLinefeedCheckStatus( String preCompileLinefeedCheckStatus ) {
		this.preCompileLinefeedCheckStatus = preCompileLinefeedCheckStatus;
	}
	public String getPreCompileCallScriptStatus() {
		return preCompileCallScriptStatus;
	}
	public void setPreCompileCallScriptStatus(String preCompileCallScriptStatus) {
		this.preCompileCallScriptStatus = preCompileCallScriptStatus;
	}
	public boolean isPreCompileExecute() {
		return preCompileExecute;
	}
	public void setPreCompileExecute(boolean preCompileExecute) {
		this.preCompileExecute = preCompileExecute;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	public String getCompleteStatus() {
		return completeStatus;
	}
	public void setCompleteStatus(String completeStatus) {
		this.completeStatus = completeStatus;
	}
	
	public boolean isPreCompileCallScriptAvailable() {
		return preCompileCallScriptAvailable;
	}
	public void setPreCompileCallScriptAvailable(
			boolean preCompileCallScriptAvailable) {
		this.preCompileCallScriptAvailable = preCompileCallScriptAvailable;
	}
	
}
