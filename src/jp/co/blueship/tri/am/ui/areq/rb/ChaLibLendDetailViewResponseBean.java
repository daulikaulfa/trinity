package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLendDetailViewResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId ;
	/** 申請番号 */
	private String applyNo;

	/** 申請ユーザ */
	private String applyUser;

	/** 申請グループ */
	private String applyGrop;

	/** 申請件名 */
	private String lendApplySubject;

	/** 申請内容 */
	private String lendApplyComment;

	/** 変更管理番号 */
	private String pjtNo;

	/** 変更要因番号 */
	private String changeCauseNo;

	/** 申請ステータス */
	private String status;

	/** 貸出資産名 */
	private List<String> lendApplyAssetsNameList;

	public String getApplyGrop() {
		return applyGrop;
	}

	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}

	public String getLendApplyComment() {
		return lendApplyComment;
	}

	public void setLendApplyComment(String lendApplyComment) {
		this.lendApplyComment = lendApplyComment;
	}

	public String getLendApplySubject() {
		return lendApplySubject;
	}

	public void setLendApplySubject(String lendApplySubject) {
		this.lendApplySubject = lendApplySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public List<String> getLendApplyAssetsNameList() {
		return lendApplyAssetsNameList;
	}

	public void setLendApplyAssetsNameList(List<String> lendApplyAssetsNameList) {
		this.lendApplyAssetsNameList = lendApplyAssetsNameList;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

}
