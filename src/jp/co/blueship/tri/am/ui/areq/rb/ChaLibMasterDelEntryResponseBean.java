package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibMasterDelEntryResponseBean extends BaseResponseBean {

	/** ロット番号 */
	private String lotId;

	/** 案件番号 */
	private String pjtNo;

	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = new ArrayList<PjtViewBean>();

	/** 申請件名 */
	private String delApplySubject;

	/** 削除理由 */
	private String delReason;

	/** 削除申請ファイル */
	private String delApplyFile;

	/** 申請ユーザ */
	private String applyUserName;

	/** 申請ユーザーグループ */
	private String selectUserGroup;

	/** 申請ユーザーグループリスト */
	private List<String> approveUserGroupList;

	public String getApplyUserName() {
		return applyUserName;
	}

	public void setApplyUserName(String applyUserName) {
		this.applyUserName = applyUserName;
	}

	public String getDelApplyFile() {
		return delApplyFile;
	}

	public void setDelApplyFile(String delApplyFile) {
		this.delApplyFile = delApplyFile;
	}

	public String getDelReason() {
		return delReason;
	}

	public void setDelReason(String delReason) {
		this.delReason = delReason;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}

	public void setPjtViewBeanList(List<PjtViewBean> pjtViewBeanList) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public List<String> getApproveUserGroupList() {
		return approveUserGroupList;
	}

	public void setApproveUserGroupList(List<String> approveUserGroupList) {
		this.approveUserGroupList = approveUserGroupList;
	}

	public String getSelectUserGroup() {
		return selectUserGroup;
	}

	public void setSelectUserGroup(String selectUserGroup) {
		this.selectUserGroup = selectUserGroup;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getDelApplySubject() {
		return delApplySubject;
	}

	public void setDelApplySubject(String delApplySubject) {
		this.delApplySubject = delApplySubject;
	}

}
