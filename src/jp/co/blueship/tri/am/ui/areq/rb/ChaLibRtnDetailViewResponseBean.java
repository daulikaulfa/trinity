package jp.co.blueship.tri.am.ui.areq.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibRtnDetailViewResponseBean extends BaseResponseBean {

	/** 申請番号 */
	private String applyNo;

	/** 申請ユーザ */
	private String applyUser;

	/** 申請グループ */
	private String applyGrop;

	/** 申請件名 */
	private String rtnApplySubject;

	/** 申請内容 */
	private String rtnApplyComment;

	/** 変更管理番号 */
	private String pjtNo;

	/** 変更要因番号 */
	private String changeCauseNo;

	/** 申請ステータス */
	private String status;

	/** 添付ファイルの名前 */
	private String returnAssetAppendFile ;

	/** 返却資産名 */
	private List<String> rtnApplyAssetsNameList;

	/** ロット番号 */
	private String lotId = null;


	public String getApplyGrop() {
		return applyGrop;
	}

	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRtnApplyComment() {
		return rtnApplyComment;
	}

	public void setRtnApplyComment(String rtnApplyComment) {
		this.rtnApplyComment = rtnApplyComment;
	}

	public String getRtnApplySubject() {
		return rtnApplySubject;
	}

	public void setRtnApplySubject(String rtnApplySubject) {
		this.rtnApplySubject = rtnApplySubject;
	}

	public List<String> getRtnApplyAssetsNameList() {
		return rtnApplyAssetsNameList;
	}

	public void setRtnApplyAssetsNameList(List<String> rtnApplyAssetsNameList) {
		this.rtnApplyAssetsNameList = rtnApplyAssetsNameList;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public String getReturnAssetAppendFile() {
		return returnAssetAppendFile;
	}

	public void setReturnAssetAppendFile(String returnAssetAppendFile) {
		this.returnAssetAppendFile = returnAssetAppendFile;
	}

}
