package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibCompLendEntryResponseBean extends BaseResponseBean {
	
	/** ログインユーザ */
	private String loginUserName = null;

	/** 申請情報番号 */
	private String applyNo;
	
	/** 申請日時 */
	private String applyDate;

	public String getApplyDate() {
		return applyDate;
	}

	public void setApplyDate(String applyDate) {
		this.applyDate = applyDate;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	
}
