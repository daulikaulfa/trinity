package jp.co.blueship.tri.am.ui.areq.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLendModifyResponseBean extends BaseResponseBean {
	
	/** ロット番号 */
	private String lotId ;
	
	/** 変更管理番号 */
	private String pjtNo;
	
	/** 変更要因番号 */
	private String changeCauseNo;
	
	/** 申請ユーザ */
	private String applyUser;
	
	/** 申請グループ */
	private String applyGrop;
	
	/** 貸出申請件名 */
	private String lendApplySubject;
	
	/** 貸出申請内容 */
	private String lendApplyComment;
	
	/** 申請番号 */
	private String applyNo ;
	
	/** 申請番号リンク */
	private String applyNolink;

	/** 貸出申請モード */
	private String lendApplyMode;
	
	/** 貸出申請ファイル */
	private String lendApplyFile;
	
	public String getApplyGrop() {
		return applyGrop;
	}

	public void setApplyGrop(String applyGrop) {
		this.applyGrop = applyGrop;
	}

	public String getLendApplyComment() {
		return lendApplyComment;
	}

	public void setLendApplyComment(String lendApplyComment) {
		this.lendApplyComment = lendApplyComment;
	}

	public String getApplyNo() {
		return applyNo;
	}

	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplyNolink() {
		return applyNolink;
	}

	public void setApplyNolink(String applyNolink) {
		this.applyNolink = applyNolink;
	}

	public String getLendApplySubject() {
		return lendApplySubject;
	}

	public void setLendApplySubject(String lendApplySubject) {
		this.lendApplySubject = lendApplySubject;
	}

	public String getPjtNo() {
		return pjtNo;
	}

	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getApplyUser() {
		return applyUser;
	}

	public void setApplyUser(String applyUser) {
		this.applyUser = applyUser;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}

	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	
	public String getLendApplyMode() {
		return lendApplyMode;
	}
	public void setLendApplyMode( String lendApplyMode ) {
		this.lendApplyMode = lendApplyMode;
	}
	
	public String getLendApplyFile() {
		return lendApplyFile;
	}
	public void setLendApplyFile( String lendApplyFile ) {
		this.lendApplyFile = lendApplyFile;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	
}
