package jp.co.blueship.tri.am.ui.lot.beans;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotHistoryDetailViewServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.AmCnvServiceToActionUtils;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotHistoryDetailViewResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibLotHistoryDetailViewProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibLotHistoryDetailViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] { 	ChaLibScreenID.LOT_HISTORY_LIST ,
																	ChaLibScreenID.LOT_HISTORY_DETAIL_VIEW ,
																	ChaLibScreenID.PJT_HISTORY_DETAIL_VIEW,
																	 } ;

	public FlowChaLibLotHistoryDetailViewProsecutor() {
		super( null );
	}

	public FlowChaLibLotHistoryDetailViewProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );

		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		String selectedLotNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String selectedServerNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );


		FlowChaLibLotHistoryDetailViewServiceBean bean = new FlowChaLibLotHistoryDetailViewServiceBean();

		String referer		= reqInfo.getParameter("referer");
		String forward		= reqInfo.getParameter( "forward" );
		String screenType	= reqInfo.getParameter( "screenType" );
		if ( this.getBussinessException() != null ) {
			forward = referer;
			screenType	= ScreenType.bussinessException;
		}
		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if ( null != referer ) {
			if ( referer.equals( ChaLibScreenID.LOT_HISTORY_DETAIL_VIEW )) {
				String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
				if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
					bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
				}
			}
		}

		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}
		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );

		bean.setLockServerId	( selectedServerNo );


		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());


		// レポート用チェックの保持
		AmCnvServiceToActionUtils.saveSelectedPjtNoForReport( session, reqInfo );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );

		FlowChaLibLotHistoryDetailViewServiceBean bean =
			(FlowChaLibLotHistoryDetailViewServiceBean)getServiceReturnInformation();
		ChaLibLotHistoryDetailViewResponseBean resBean =
			AmCnvActionToServiceUtils.convertChaLotHistoryDetailViewResponseBean( bean, session );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

}
