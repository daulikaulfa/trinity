package jp.co.blueship.tri.am.ui.lot.beans;

import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibTopServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibTopResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

public class FlowChaLibTopProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibTopService";

	public FlowChaLibTopProsecutor() {
		super( null );
	}

	public FlowChaLibTopProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );
		session.setAttribute( SessionScopeKeyConsts.ACTION_DETAIL_VIEW, "/flowChaLibLotDetailView" );

		FlowChaLibTopServiceBean bean = new FlowChaLibTopServiceBean();

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		if ( null == referer ) {
			// ログイン用処理
			bean.setUserId( reqInfo.getUserId() );
			bean.setUserName(userName());

			// FlowChangePasswordBtnProsecutorからのキラーパス
			String tempRef = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
			bean.setReferer( tempRef );

		} else {
			bean.setUserId( userId );
			bean.setUserName(userName());
		}

		if ( null != referer) {
			if ( referer.equals( ChaLibScreenID.LIB_TOP )) {
				String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
				if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
					bean.setSelectPageNo( Integer.parseInt( savedPageNo )) ;
				}
			}
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		FlowChaLibTopServiceBean bean = (FlowChaLibTopServiceBean)getServiceReturnInformation();
		ChaLibTopResponseBean resBean = AmCnvActionToServiceUtils.convertChaTopResponseBean( bean );

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( "★★★" );
			this.getLog().debug( "  bean.getPageInfoView().getMaxPageNo()" + bean.getPageInfoView().getMaxPageNo() );
			this.getLog().debug( "  resBean.getSelectPageNo()" + resBean.getSelectPageNo() );
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
		session.removeAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

}
