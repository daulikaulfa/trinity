package jp.co.blueship.tri.am.ui.lot.beans;

import jp.co.blueship.tri.am.beans.dto.PjtSearchBean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotDetailViewServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.AmCnvServiceToActionUtils;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotDetailViewResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V3L10.01
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class FlowChaLibLotDetailViewProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibLotDetailViewService";
	public static final String SEARCH_LOT_DETAIL_VIEW_PJT_LIST = FLOW_ACTION_ID + "lotDetailViewPjtList";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
																	ChaLibScreenID.LOT_DETAIL_VIEW ,
																	ChaLibScreenID.PJT_DETAIL_VIEW,
																	 } ;

	public FlowChaLibLotDetailViewProsecutor() {
		super( null );
	}

	public FlowChaLibLotDetailViewProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );

		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );
		session.setAttribute( SessionScopeKeyConsts.ACTION_DETAIL_VIEW, "/flowChaLibLotDetailView" );

		String selectedLotNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String selectedServerNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );

		FlowChaLibLotDetailViewServiceBean bean = new FlowChaLibLotDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String lotId = reqInfo.getParameter( "lotNo" );
		if ( ! TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}
		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );

		bean.setLockServerId	( selectedServerNo );
		bean.setSelectedPjtIdArray(reqInfo.getParameterValues("selectedPjtNos"));


		//詳細検索の検索条件を同一ロット内で保持する
		{
			PjtSearchBean pjtSearchBean = (PjtSearchBean)session.getAttribute( SEARCH_LOT_DETAIL_VIEW_PJT_LIST );

			if ( null == pjtSearchBean || ! bean.getSelectedLotNo().equals( pjtSearchBean.getSessionKey() ) ) {
				session.removeAttribute( SEARCH_LOT_DETAIL_VIEW_PJT_LIST );
				bean.setPjtSearchBean( null );
			} else {
				bean.setPjtSearchBean( pjtSearchBean );
			}

			//[検索]ボタン押下時、検索情報をセッションに保持する
			if ( ChaLibScreenID.LOT_DETAIL_VIEW .equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setPjtSearchBean( convertPjtSearchBean( reqInfo, bean.getPjtSearchBean() ) );
					bean.getPjtSearchBean().setSessionKey( bean.getSelectedLotNo() );

					session.setAttribute( SEARCH_LOT_DETAIL_VIEW_PJT_LIST, bean.getPjtSearchBean() );
				}
			}
		}

		if ( ChaLibScreenID.LOT_DETAIL_VIEW .equals( referer ) ) {
			String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
			if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
				bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
			}
		}

		// レポート用チェックの保持
		AmCnvServiceToActionUtils.saveSelectedPjtNoForReport( session, reqInfo );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );

		FlowChaLibLotDetailViewServiceBean bean =
			(FlowChaLibLotDetailViewServiceBean)getServiceReturnInformation();
		ChaLibLotDetailViewResponseBean resBean =
			AmCnvActionToServiceUtils.convertChaLotDetailViewResponseBean( bean, session );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public PjtSearchBean convertPjtSearchBean( IRequestInfo reqInfo, PjtSearchBean bean ) {

		if ( null == bean )
			bean = new PjtSearchBean();

		String[] selectedStatus = reqInfo.getParameterValues( "status[]" ) ;
		bean.setSelectedStatusString( selectedStatus ) ;

		String searchPjtNo = reqInfo.getParameter( "searchPjtNo" ) ;
		bean.setSearchPjtNo( searchPjtNo ) ;

		String searchChangeCauseNo = reqInfo.getParameter( "searchChangeCauseNo" ) ;
		bean.setSearchChangeCauseNo( searchChangeCauseNo ) ;

		String selectedPjtCount = reqInfo.getParameter( "searchPjtCount" ) ;
		bean.setSelectedPjtCount( selectedPjtCount ) ;

		return bean;
	}

}
