package jp.co.blueship.tri.am.ui.lot.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.am.beans.dto.GroupViewBean;
import jp.co.blueship.tri.am.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.BuildEnvViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.am.domain.lot.dto.FlowChaLibLotModifyServiceBean;
import jp.co.blueship.tri.am.ui.AmCnvActionToServiceUtils;
import jp.co.blueship.tri.am.ui.AmCnvServiceToActionUtils;
import jp.co.blueship.tri.am.ui.lot.rb.ChaLibLotModifyResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;

/**
 *
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Siti Hajar
 */
public class FlowChaLibLotModifyProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowChaLibLotModifyService";

	public static final String[] screenFlows = new String[] { 	ChaLibScreenID.LOT_MODIFY ,
																	ChaLibScreenID.LOT_MODULE_SELECT ,
																	ChaLibScreenID.LOT_BUILDENV_SELECT ,
																	ChaLibScreenID.LOT_REL_ENV_SELECT ,
																	ChaLibScreenID.LOT_MODIFY_CONFIRM ,
																	ChaLibScreenID.COMP_LOT_MODIFY };

	public FlowChaLibLotModifyProsecutor() {
		super( null );
	}

	public FlowChaLibLotModifyProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );

		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );
		session.setAttribute( SessionScopeKeyConsts.ACTION_DETAIL_VIEW, "/flowChaLibLotModify" );

		FlowChaLibLotModifyServiceBean seBean = (FlowChaLibLotModifyServiceBean)session.getAttribute( FLOW_ACTION_ID );

		FlowChaLibLotModifyServiceBean bean = null;
		if ( null == seBean ) {
			bean = new FlowChaLibLotModifyServiceBean();
		} else {
			bean = seBean;
			// ワーニングメッセージ、コメントの初期化
			bean.setInfoMessage		( null );
			bean.setInfoCommentIdList	( null );
			bean.setInfoMessageIdList( null );
		}
		if( null == bean.getLotEditInputBean() ) {
			bean.setLotEditInputBean( new LotEditInputV3Bean() );
		}
		LotEditInputV3Bean lotEditInputBean = bean.getLotEditInputBean();

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★allowAssetDuplication:" + reqInfo.getParameter("allowAssetDuplication"));
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		bean.setReferer( referer );
		bean.setForward( forward );
		bean.setScreenType( screenType );

		lotEditInputBean.setInputUserName( bean.getUserName() );
		lotEditInputBean.setInputUserId( bean.getUserId() ) ;

		if( ! TriStringUtils.isEmpty( reqInfo.getParameter("lotNo" ) ) ) {
			bean.setLotNo		( reqInfo.getParameter( "lotNo" ) );
			bean.setLockLotNo	( reqInfo.getParameter( "lotNo" ) );
		}

		String selectedServerNo	= (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( selectedServerNo );


		if( ChaLibScreenID.LIB_TOP.equals( referer ) ) {

		} else if ( ChaLibScreenID.LOT_MODIFY.equals( referer ) ) {

			if( null != reqInfo.getParameter("lotName" ) ) {
				lotEditInputBean.setLotName( reqInfo.getParameter( "lotName" ) );
			}
			if( null != reqInfo.getParameter("lotSummary" ) ) {
				lotEditInputBean.setLotSummary( reqInfo.getParameter( "lotSummary" ) );
			}
			if( null != reqInfo.getParameter("lotContent" ) ) {
				lotEditInputBean.setLotContent( reqInfo.getParameter( "lotContent" ) );
			}
			if( null != reqInfo.getParameter("editUseMerge" ) ) {
				lotEditInputBean.setEditUseMerge( true );
			} else {
				lotEditInputBean.setEditUseMerge( false );
			}

//			if( null != reqInfo.getParameter("directoryPathPublic" ) ) {
//				lotEditInputBean.setDirectoryPathPublic( reqInfo.getParameter( "directoryPathPublic" ) );
//			}
//			if( null != reqInfo.getParameter("directoryPathPrivate" ) ) {
//				lotEditInputBean.setDirectoryPathPrivate( reqInfo.getParameter( "directoryPathPrivate" ) );
//			}

			if ( bean.isSelectableAllowAssetDuplication() ) {
				if( null != reqInfo.getParameter("allowAssetDuplication" ) ) {
					lotEditInputBean.setAllowAssetDuplication( true );
				} else {
					lotEditInputBean.setAllowAssetDuplication( false );
				}
			}

			List<GroupViewBean> selectedGroupViewBeanList = new ArrayList<GroupViewBean>();
			String[] selectedGroupIdArray = new String[0];

			if ( null != reqInfo.getParameter( "selectedGroupId[]" )) {
				selectedGroupIdArray = reqInfo.getParameterValues( "selectedGroupId[]" );

				for ( String selectedGroupId : selectedGroupIdArray ) {
					for ( GroupViewBean groupViewBean : lotEditInputBean.getGroupViewBeanList() ) {

						if ( groupViewBean.getGroupId().equals( selectedGroupId )) {
							selectedGroupViewBeanList.add( groupViewBean );
							break;
						}
					}
				}
			}
			lotEditInputBean.setSelectedGroupIdString		( selectedGroupIdArray );
			lotEditInputBean.setSelectedGroupViewBeanList	( selectedGroupViewBeanList );


			List<GroupViewBean> selectedSpecifiedGroupList = new ArrayList<GroupViewBean>();
			String[] selectedSpecifiedGroupIdArray = new String[0];

			if ( null != reqInfo.getParameter( "selectedSpecifiedGroupId[]" )) {
				selectedSpecifiedGroupIdArray = reqInfo.getParameterValues( "selectedSpecifiedGroupId[]" );

				for ( String selectedSpecifiedGroupId : selectedSpecifiedGroupIdArray ) {
					for ( GroupViewBean groupViewBean : lotEditInputBean.getSpecifiedGroupList() ) {

						if ( groupViewBean.getGroupId().equals( selectedSpecifiedGroupId )) {
							selectedSpecifiedGroupList.add( groupViewBean );
							break;
						}
					}
				}
			}
			lotEditInputBean.setSelectedSpecifiedGroupIdString		( selectedSpecifiedGroupIdArray );
			lotEditInputBean.setSelectedSpecifiedGroupList	( selectedSpecifiedGroupList );


			// 既存データ対応
			if ( lotEditInputBean.isAllowModifyAllowListView() ) {
				if ( null != reqInfo.getParameter( "allowListView" )) {
					lotEditInputBean.setAllowListView( true );
				} else {
					lotEditInputBean.setAllowListView( false );
				}
			}

		} else if ( ChaLibScreenID.LOT_MODULE_SELECT.equals( referer ) ) {

			if( null != reqInfo.getParameter( "screenType" ) ) {

//				// サーバ情報
//				if ( null != reqInfo.getParameter( "serverNo" ) ) {
//
//					String selectedServerNo = reqInfo.getParameter( "serverNo" );
//
//					for ( ServerViewBean server : lotEditInputBean.getServerViewBeanList() ) {
//
//						if ( selectedServerNo.equals( server.getServerNo() )) {
//							lotEditInputBean.setSelectedServerViewBean( server );
//							break;
//						}
//					}
//				}

//				List<ModuleViewBean> selectedModuleViewBeanList = new ArrayList<ModuleViewBean>();
//				List<String> selectedModuleNameList = new ArrayList<String>();
//
//				if ( null != reqInfo.getParameter( "selectedModuleName[]") ) {
//
//					selectedModuleNameList.addAll( FluentList.from().asList() reqInfo.getParameterValues( "selectedModuleName[]" ) ) );
//
//					for( String selectedModuleName : selectedModuleNameList ) {
//						Iterator<ModuleViewBean> iter = lotEditInputBean.getModuleViewBeanList().iterator();
//						while( iter.hasNext() ) {
//							ModuleViewBean moduleViewBean = iter.next();
//							if( moduleViewBean.getModuleName().equals( selectedModuleName ) ) {
//								selectedModuleViewBeanList.add( moduleViewBean );
//								break;
//							}
//						}
//					}
//				}
//				//ロット作成時に選択したモジュールは固定で追加
//				for( ModuleViewBean preservedModule : lotEditInputBean.getPreservedModuleViewBeanList() ) {
//					selectedModuleViewBeanList.add( preservedModule );
//					selectedModuleNameList.add( preservedModule.getModuleName() );
//				}
//
//				lotEditInputBean.setSelectedModuleNameString( selectedModuleNameList.toArray( new String[ 0 ] ) );
//				lotEditInputBean.setSelectedModuleViewBeanList( selectedModuleViewBeanList );

			}

		} else if ( ChaLibScreenID.LOT_BUILDENV_SELECT.equals( referer ) ) {

			if ( null != reqInfo.getParameter( "buildEnvNo") ) {
				String buildEnvNo = reqInfo.getParameter( "buildEnvNo" );
				lotEditInputBean.setSelectedBuildEnvNo( buildEnvNo );
				if( true == TriStringUtils.isEmpty( buildEnvNo ) ) {
					lotEditInputBean.setBuildEnvBean( null );
				} else {
					Iterator<BuildEnvViewBean> iter = lotEditInputBean.getBuildEnvViewBeanEnableList().iterator();
					while( iter.hasNext() ) {
						BuildEnvViewBean buildEnvViewBean = iter.next();
						if( buildEnvViewBean.getEnvNo().equals( buildEnvNo ) ) {
							lotEditInputBean.setBuildEnvBean( buildEnvViewBean );
							break;
						}
					}
				}
			}
			if ( null != reqInfo.getParameter( "fullBuildEnvNo") ) {
				String buildEnvNo = reqInfo.getParameter( "fullBuildEnvNo" );
				lotEditInputBean.setSelectedFullBuildEnvNo( buildEnvNo );
				if( true == TriStringUtils.isEmpty( buildEnvNo ) ) {
					lotEditInputBean.setFullBuildEnvBean( null );
				} else {
					Iterator<BuildEnvViewBean> iter = lotEditInputBean.getBuildEnvViewBeanEnableList().iterator();
					while( iter.hasNext() ) {
						BuildEnvViewBean buildEnvViewBean = iter.next();
						if( buildEnvViewBean.getEnvNo().equals( buildEnvNo ) ) {
							lotEditInputBean.setFullBuildEnvBean( buildEnvViewBean );
							break;
						}
					}
				}
			}

		} else if ( ChaLibScreenID.LOT_REL_ENV_SELECT.equals( referer ) ) {

			// リリース環境情報
			if ( ScreenType.next.equals( screenType ) ) {
				String[] selectedEnvNoArray = AmCnvServiceToActionUtils.convertSelectedEnvNoArray(reqInfo);

				lotEditInputBean.setSelectedRelEnvNoString( selectedEnvNoArray );
				lotEditInputBean.setSelectedRelEnvViewBeanList(
						AmCnvServiceToActionUtils.convertRelEnvViewBean(reqInfo, lotEditInputBean, selectedEnvNoArray) );

			}
		} else if ( ChaLibScreenID.LOT_MODIFY_CONFIRM.equals( referer ) ) {

			//マージエラーチェック
			WarningCheckBean warningCheckError = bean.getWarningCheckError() ;
			String confirmCheckError = reqInfo.getParameter( "confirmWarningCheckError" ) ;
			warningCheckError.setConfirmCheck( ( null != confirmCheckError ) ? true : false );

			warningCheckError.setExistWarning		(
					( new Boolean( reqInfo.getParameter( "existWarning" ) )).booleanValue() );

		} else if ( ChaLibScreenID.COMP_LOT_MODIFY.equals( referer ) ) {

		}

		session.setAttribute( FLOW_ACTION_ID , bean );

		// レポート用チェックの保持
		AmCnvServiceToActionUtils.saveSelectedPjtNoForReport( session, reqInfo );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		FlowChaLibLotModifyServiceBean bean = (FlowChaLibLotModifyServiceBean) getServiceReturnInformation();

		ChaLibLotModifyResponseBean retBean = null;
		String forward = bean.getForward();

		if ( ChaLibScreenID.LOT_MODIFY.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibLotModifyResponseBean( bean );

		} else if ( ChaLibScreenID.LOT_MODULE_SELECT.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibLotModuleSelectResponseBean( bean , reqInfo , session );

		} else if ( ChaLibScreenID.LOT_BUILDENV_SELECT.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibLotBuildEnvSelectResponseBean( bean , reqInfo , session );

		} else if ( ChaLibScreenID.LOT_REL_ENV_SELECT.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibLotRelEnvSelectResponseBean( bean , reqInfo , session );

		} else if ( ChaLibScreenID.LOT_MODIFY_CONFIRM.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibLotModifyConfirmResponseBean( bean , reqInfo , session );

		} else if ( ChaLibScreenID.COMP_LOT_MODIFY.equals( forward ) ) {

			retBean = AmCnvActionToServiceUtils.convertChaLibCompLotModifyResponseBean( bean , reqInfo , session );

		}

		if ( null != retBean ) {

			retBean.setScmType( bean.getLotEditInputBean().getScmType() );

			retBean.new MessageUtility().reflectMessage( bean );
			retBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対策
		session.setAttribute( FLOW_ACTION_ID , bean );

		return retBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}
}
