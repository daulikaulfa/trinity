package jp.co.blueship.tri.am.ui.lot.rb;

import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtSearchBean;
import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * <br>
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Yukihiro Eguchi
 *
 * @version SP-20150601_V3L12R01
 * @author Yusna Marlina
 */
public class ChaLibLotDetailViewResponseBean extends BaseResponseBean {

	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** ビルドパッケージ作成成功回数 */
	private int succeededRelUnitCount = 0;
	/** ビルドパッケージ作成失敗回数 */
	private int failedRelUnitCount = 0;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** ロット概要 */
	private String lotSummary = null;
	/** ロット内容 */
	private String lotContent = null;
	/** 資産重複貸出の表示 */
	private boolean allowAssetDuplicationVisible = false;
	/** 資産重複貸出の表示名 */
	private String allowAssetDuplicationName = null;
	/** マージを利用する/しない設定を行う */
	private boolean useMerge = true;
	/** ステータス */
	private String lotStatus = null;
	/** 作成日時 */
	private String inputDate = null;
	/** 最新タグ */
	private String recentTag = null;
	/** 最新バージョン */
	private String recentVersion = null;
	/** モジュール名 */
	private String[] moduleName = null;
	/** モジュール名のリスト */
	private String module = null;
	/** 選択変更管理番号(レポート用) */
	private String selectedPjtNoString = null;
	/** 変更管理 詳細検索用Bean */
	private PjtSearchBean pjtSearchBean = null ;
	/** 一覧表示許可 */
	private boolean allowListView = false;
	/** 一覧表示文言 */
	private String allowListContent;
	/** グループ名のリスト */
	private String group = null;
	/** 選択されたメール送信先 特定グループIDの配列を文字列に変換したもの */
	private String selectedSpecifiedGroupNameString = null;
	/** 選択されたリリース環境 環境名の配列を文字列に変換したもの */
	private String selectedRelEnvNameString = null;
	/** 選択された変更情報ID(変更一括クローズ用) */
	private String[] selectedPjtIdArray;


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public int getSucceededRelUnitCount() {
		return succeededRelUnitCount;
	}
	public void setSucceededRelUnitCount(int succeededRelUnitCount) {
		this.succeededRelUnitCount = succeededRelUnitCount;
	}

	public int getFailedRelUnitCount() {
		return failedRelUnitCount;
	}
	public void setFailedRelUnitCount(int failedRelUnitCount) {
		this.failedRelUnitCount = failedRelUnitCount;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	public String getAllowAssetDuplicationName() {
		return allowAssetDuplicationName;
	}
	public void setAllowAssetDuplicationName(String allowAssetDuplicationName) {
		this.allowAssetDuplicationName = allowAssetDuplicationName;
	}
	public boolean isAllowAssetDuplicationVisible() {
		return allowAssetDuplicationVisible;
	}
	public void setAllowAssetDuplicationVisible(boolean allowAssetDuplicationVisible) {
		this.allowAssetDuplicationVisible = allowAssetDuplicationVisible;
	}
	public String getLotContent() {
		return lotContent;
	}
	public void setLotContent(String lotContent) {
		this.lotContent = lotContent;
	}
	public String getLotSummary() {
		return lotSummary;
	}
	public void setLotSummary(String lotSummary) {
		this.lotSummary = lotSummary;
	}
	public boolean isUseMerge() {
	    return useMerge;
	}
	public void setUseMerge(boolean useMerge) {
	    this.useMerge = useMerge;
	}
	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getRecentTag() {
		return recentTag;
	}
	public void setRecentTag(String recentTag) {
		this.recentTag = recentTag;
	}

	public String getRecentVersion() {
		/*if (null != recentVersion && recentVersion.length() > 0) {
			StringBuilder buf = new StringBuilder();
			buf.append("(");
			buf.append(recentVersion);
			buf.append(")");
			return buf.toString();
		}*/
		return recentVersion;
	}
	public void setRecentVersion(String recentVersion) {
		this.recentVersion = recentVersion;
	}

	public String[] getModuleName() {
		return moduleName;
	}
	public void setModuleName(String[] moduleName) {
		this.moduleName = moduleName;
	}

	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}

	public String getSelectedPjtNoString() {
		return selectedPjtNoString;
	}

	public void setSelectedPjtNoString( String[] pjtNoArray ) {

		StringBuilder stb = new StringBuilder();
		stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( pjtNoArray ) ) ;
		stb.append("]");
		this.selectedPjtNoString = stb.toString();
	}
	public PjtSearchBean getPjtSearchBean() {
		return pjtSearchBean;
	}
	public void setPjtSearchBean(PjtSearchBean pjtSearchBean) {
		this.pjtSearchBean = pjtSearchBean;
	}

	public boolean isAllowListView() {
		return allowListView;
	}
	public void setAllowListView( boolean value ) {
		this.allowListView = value;
	}
	public String getAllowListContent( ) {
		return this.allowListContent;
	}
	public void setAllowListContent( String content ) {
		this.allowListContent = content;
	}

	public String getGroup() {
		return group;
	}
	public void setGroup( String group ) {
		this.group = group;
	}


	/** サーバ番号 */
	private String serverNo = null;
	/** サーバ名 */
	private String serverName = null;

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}

	public String getServerName() {
		return serverName;
	}
	public void setServerName( String serverName ) {
		this.serverName = serverName;
	}


	/** 公開フォルダ */
	private String publicFolder = null;
	/** 非公開フォルダ */
	private String privateFolder = null;

	public String getPublicFolder() {
		return publicFolder;
	}
	public void setPublicFolder( String publicFolder ) {
		this.publicFolder = publicFolder;
	}

	public String getPrivateFolder() {
		return privateFolder;
	}
	public void setPrivateFolder( String privateFolder ) {
		this.privateFolder = privateFolder;
	}

	String scmType = null;
	public String getScmType() {
		return scmType;
	}

	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}

	public String getSelectedSpecifiedGroupNameString() {
		return selectedSpecifiedGroupNameString;
	}
	public void setSelectedSpecifiedGroupNameString(
			String selectedSpecifiedGroupNameString) {
		this.selectedSpecifiedGroupNameString = selectedSpecifiedGroupNameString;
	}
	public String getSelectedRelEnvNameString() {
		return selectedRelEnvNameString;
	}
	public void setSelectedRelEnvNameString(String selectedRelEnvNameString) {
		this.selectedRelEnvNameString = selectedRelEnvNameString;
	}
	public String getSelectedPjtIdArrayString() {
		   StringBuilder buf = new StringBuilder();
		   if( null != this.selectedPjtIdArray ) {
		      buf.append("[");
		      buf.append( TriStringUtils.convertArrayToString( this.selectedPjtIdArray ) ) ;
		      buf.append("]");
		   }
		   return buf.toString();
		}

	public String[] getSelectedPjtIdArray() {
		return selectedPjtIdArray;
	}
	public void setSelectedPjtIdArray(String[] selectedPjtIdArray) {
		this.selectedPjtIdArray = selectedPjtIdArray;
	}
}
