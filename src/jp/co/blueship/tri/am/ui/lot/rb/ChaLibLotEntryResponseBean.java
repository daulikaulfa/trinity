package jp.co.blueship.tri.am.ui.lot.rb;


import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLotEntryResponseBean extends BaseResponseBean {
		
	
	
	private String lotId = null ;
	private LotEditInputV3Bean lotEditInputBean = null ;
	private boolean selectableAllowAssetDuplication = false;
	private String selectableAllowAssetDuplicationName = null;
	private boolean allowAssetDuplicationVisible = false;
	
	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public LotEditInputV3Bean getLotEditInputBean() {
		return lotEditInputBean;
	}

	public void setLotEditInputBean(LotEditInputV3Bean lotEditInputBean) {
		this.lotEditInputBean = lotEditInputBean;
	}
	
	public boolean isSelectableAllowAssetDuplication() {
		return selectableAllowAssetDuplication;
	}
	public void setSelectableAllowAssetDuplication( boolean value ) {
		this.selectableAllowAssetDuplication = value;
	}

	public String getSelectableAllowAssetDuplicationName() {
		return selectableAllowAssetDuplicationName;
	}

	public void setSelectableAllowAssetDuplicationName(
			String selectableAllowAssetDuplicationName) {
		this.selectableAllowAssetDuplicationName = selectableAllowAssetDuplicationName;
	}

	public boolean isAllowAssetDuplicationVisible() {
		return allowAssetDuplicationVisible;
	}

	public void setAllowAssetDuplicationVisible(boolean allowAssetDuplicationVisible) {
		this.allowAssetDuplicationVisible = allowAssetDuplicationVisible;
	}
	
	
	String scmType = null;
	public String getScmType() {
		return scmType;
	}
	
	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
	
}
