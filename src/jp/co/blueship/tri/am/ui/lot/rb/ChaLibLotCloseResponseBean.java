package jp.co.blueship.tri.am.ui.lot.rb;

import java.util.List;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotDetailViewBean;
import jp.co.blueship.tri.am.domain.lot.beans.dto.LotEditInputV3Bean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ChaLibLotCloseResponseBean extends BaseResponseBean {
		
	
	
	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** 選択ロット番号 */
	private String selectedLotNo = null;
	/** ロット詳細情報 */
	private LotDetailViewBean lotDetailViewBean = null;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** コメント */
	private String comment = null;
	/** ステータス */
	private String lotStatus = null;
	/** 作成日時 */
	private String inputDate = null;
	/** 最新タグ */
	private String recentTag = null;
	/** 最新バージョン */
	private String recentVersion = null;
	/** モジュール名 */
	private String[] moduleName = null;
	/** モジュール名のリスト */
	private String module = null;
	/** ロット編集入力情報 */
	private LotEditInputV3Bean lotEditInputBean = null;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public LotDetailViewBean getLotDetailViewBean() {
		return lotDetailViewBean;
	}
	public void setLotDetailViewBean( LotDetailViewBean lotDetailViewBean ) {
		this.lotDetailViewBean = lotDetailViewBean;
	}
	
	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}
	
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}

	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus(String lotStatus) {
		this.lotStatus = lotStatus;
	}
	
	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	
	public String getRecentTag() {
		return recentTag;
	}
	public void setRecentTag(String recentTag) {
		this.recentTag = recentTag;
	}
	
	public String getRecentVersion() {
		/*if (null != recentVersion && recentVersion.length() > 0) {
			StringBuilder buf = new StringBuilder();
			buf.append("(");
			buf.append(recentVersion);
			buf.append(")");
			recentVersion = buf.toString();
		}*/

		return recentVersion;
	}
	public void setRecentVersion(String recentVersion) {
		this.recentVersion = recentVersion;
	}
	
	public String[] getModuleName() {
		return moduleName;
	}
	public void setModuleName(String[] moduleName) {
		this.moduleName = moduleName;
	}

	public String getModule() {
		return module;
	}
	public void setModule(String module) {
		this.module = module;
	}
	public LotEditInputV3Bean getLotEditInputBean() {
		return lotEditInputBean;
	}
	public void setLotEditInputBean(LotEditInputV3Bean lotEditInputBean) {
		this.lotEditInputBean = lotEditInputBean;
	}
	
	
	/** サーバ番号 */
	private String serverNo = null;
	/** サーバ名 */
	private String serverName = null;

	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getServerName() {
		return serverName;
	}
	public void setServerName( String serverName ) {
		this.serverName = serverName;
	}
	
	String scmType = null;
	public String getScmType() {
		return scmType;
	}
	
	public void setScmType( String scmType ) {
		this.scmType = scmType;
	}
}
