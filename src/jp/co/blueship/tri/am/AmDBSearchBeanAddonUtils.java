package jp.co.blueship.tri.am;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.beans.dto.PjtSearchBean;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByMerge;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class AmDBSearchBeanAddonUtils {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 変更管理レコードの詳細検索条件を生成する
	 * @param paramBean PjtSearchBeanオブジェクト
	 * @return PjtSearchBeanオブジェクト
	 */
	public static final PjtSearchBean setPjtSearchBean( PjtSearchBean pjtSearchBean ) {
		PjtSearchBean retPjtSearchBean = new PjtSearchBean() ;

		//変更管理ステータスのリストアップ
		List<String> selectStatusViewList = new ArrayList<String>();
		if ( StatusFlg.on.value().equals( sheet.getValue( AmDesignEntryKeyByMerge.checkStatusTestComplete ) ) ) {
			selectStatusViewList = DesignDefineSearchUtils.extractValue(
					AmDesignBeanId.statusId,
					AmDesignBeanId.lotDetailViewPjtListStatusIdSearchForTestComplete );
		}else{
			selectStatusViewList = DesignDefineSearchUtils.extractValue(
					AmDesignBeanId.statusId,
					AmDesignBeanId.lotDetailViewPjtListStatusIdSearch );
		}
		retPjtSearchBean.setSelectStatusViewList( selectStatusViewList );

		if ( null != pjtSearchBean ) {
			retPjtSearchBean.setSelectedStatusString	( pjtSearchBean.getSelectedStatusStringArray() );
			retPjtSearchBean.setSearchPjtCountList		( new ArrayList<ItemLabelsBean>() );
			retPjtSearchBean.setSelectedPjtCount		( pjtSearchBean.getSelectedPjtCount() );
			retPjtSearchBean.setSearchPjtNo				( pjtSearchBean.getSearchPjtNo() );
			retPjtSearchBean.setSearchChangeCauseNo		( pjtSearchBean.getSearchChangeCauseNo() );
		}

		//ステータスの選択状態
		if( null == pjtSearchBean ) {
			//初期表示時のみ
			List<String> selectedStatus = new ArrayList<String>();
			if ( StatusFlg.on.value().equals( sheet.getValue( AmDesignEntryKeyByMerge.checkStatusTestComplete ) ) ) {
				selectedStatus = DesignDefineSearchUtils.extractValue(
						AmDesignBeanId.statusId,
						AmDesignBeanId.lotDetailViewPjtListStatusIdSearchForTestComplete,
						StatusFlg.on.value() );
			}else{
				selectedStatus = DesignDefineSearchUtils.extractValue(
					AmDesignBeanId.statusId,
					AmDesignBeanId.lotDetailViewPjtListStatusIdSearch,
												StatusFlg.on.value() );
			}
			retPjtSearchBean.setSelectedStatusString( selectedStatus.toArray( new String[ 0 ] ) ) ;
		}

		//検索件数のリストアップ
		List<ItemLabelsBean> pjtCountList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( AmDesignBeanId.lotDetailViewPjtListCount );
		retPjtSearchBean.setSearchPjtCountList( pjtCountList ) ;

		//全体検索件数の選択
		if( null == pjtSearchBean || null == pjtSearchBean.getSelectedPjtCount() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( AmDesignBeanId.lotDetailViewPjtListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( AmDesignBeanId.lotDetailViewPjtListCount, StatusFlg.on.value() ) ;
			retPjtSearchBean.setSelectedPjtCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		return retPjtSearchBean ;
	}

}
