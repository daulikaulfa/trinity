package jp.co.blueship.tri.am;

import jp.co.blueship.tri.am.beans.dto.PjtViewBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposEntity;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;

/**
 * {@link jp.co.blueship.tri.fw.cmn.utils.collections.FluentList}のサポートUtils
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class AmFluentFunctionUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final TriPredicate<IAreqDto> isLendingRequest = new TriPredicate<IAreqDto>() {
		@Override
		public boolean evalute(IAreqDto dto) {
			return AmBusinessJudgUtils.isReturnApply(dto.getAreqEntity().getAreqCtgCd());
		}
	};

	public static final TriPredicate<IAreqDto> isReturningRequest = new TriPredicate<IAreqDto>() {
		@Override
		public boolean evalute(IAreqDto dto) {
			return AmBusinessJudgUtils.isLendApply(dto.getAreqEntity().getAreqCtgCd());
		}
	};

	public static final TriPredicate<IAreqDto> isRemovalRequest = new TriPredicate<IAreqDto>() {
		@Override
		public boolean evalute(IAreqDto dto) {
			return AmBusinessJudgUtils.isDeleteApply(dto.getAreqEntity().getAreqCtgCd());
		}
	};

	public static final TriFunction<AreqCtgCd, String> toValueByAreqCtgCd = new TriFunction<AreqCtgCd, String>() {
		@Override
		public String apply(AreqCtgCd input) {
			return input.value();
		}
	};

	public static final TriFunction<ILotGrpLnkEntity, String> toGroupIdFromGrpUserLnkEntity = new TriFunction<ILotGrpLnkEntity, String>() {
		@Override
		public String apply(ILotGrpLnkEntity entity) {
			return entity.getGrpId();
		}
	};

	public static final TriFunction<IGrpEntity, String> toGroupIdFromGroupEntity = new TriFunction<IGrpEntity, String>() {
		@Override
		public String apply(IGrpEntity entity) {
			return entity.getGrpId();
		}
	};

	public static final TriFunction<ILotMdlLnkEntity, String> toMdlNmsFromLotMdlLnk = new TriFunction<ILotMdlLnkEntity, String>() {
		@Override
		public String apply(ILotMdlLnkEntity input) {
			return input.getMdlNm();
		}
	};

	public static final TriFunction<IHeadBlEntity, String> toHeadBlIds = new TriFunction<IHeadBlEntity, String>() {
		@Override
		public String apply(IHeadBlEntity input) {
			return input.getHeadBlId();
		}
	};

	public static final TriFunction<ILotBlDto, ILotBlEntity> toEntitiesFromLotBlDto = new TriFunction<ILotBlDto, ILotBlEntity>() {
		@Override
		public ILotBlEntity apply(ILotBlDto input) {
			return input.getLotBlEntity();
		}
	};

	public static final TriFunction<ILotBlEntity, String> toDataIdsFromLotBl = new TriFunction<ILotBlEntity, String>() {
		@Override
		public String apply(ILotBlEntity input) {
			return input.getDataId();
		}
	};

	public static final TriFunction<IPjtEntity, String> toPjtIds = new TriFunction<IPjtEntity, String>() {
		@Override
		public String apply(IPjtEntity input) {
			return input.getPjtId();
		}
	};

	public static final TriFunction<ILotBlReqLnkEntity, String> toPjtIdsFromLotBlReqLnk = new TriFunction<ILotBlReqLnkEntity, String>() {
		@Override
		public String apply(ILotBlReqLnkEntity input) {
			return input.getPjtId();
		}
	};

	public static final TriFunction<IAreqEntity, String> toAreqIds = new TriFunction<IAreqEntity, String>() {
		@Override
		public String apply(IAreqEntity input) {
			return input.getAreqId();
		}
	};

	public static final TriFunction<IAreqDto, String> toAreqIdsFromAreqDto = new TriFunction<IAreqDto, String>() {
		@Override
		public String apply(IAreqDto input) {
			return input.getAreqEntity().getAreqId();
		}
	};

	public static final TriFunction<ILotBlReqLnkEntity, String> toAreqIdsFromLotBlReqLnk = new TriFunction<ILotBlReqLnkEntity, String>() {
		@Override
		public String apply(ILotBlReqLnkEntity input) {
			return input.getAreqId();
		}
	};

	public static final TriFunction<Object, IAreqDto> toAreqDto = new TriFunction<Object, IAreqDto>() {
		@Override
		public IAreqDto apply(Object input) {
			return (IAreqDto) input;
		}
	};

	public static final TriFunction<AreqCtgCd, String> toValueFromAreqCtgCd = new TriFunction<AreqCtgCd, String>() {
		@Override
		public String apply(AreqCtgCd input) {
			return input.value();
		}
	};

	public static final TriFunction<ILotBlDto, String> toLotBlIdsFromLotBlDto = new TriFunction<ILotBlDto, String>() {
		@Override
		public String apply(ILotBlDto input) {
			return input.getLotBlEntity().getLotBlId();
		}
	};

	public static final TriFunction<IVcsReposEntity, String> toModuleString = new TriFunction<IVcsReposEntity, String>() {
		@Override
		public String apply(IVcsReposEntity input) {
			return input.getVcsReposId();
		}
	};

	public static final TriFunction<String, ItemLabelsBean> toItemLabelsFromReferenceCategoryId = new TriFunction<String, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(String input) {
			return new ItemLabelsBean(sheet.getValue(AmDesignBeanId.changeCauseClassifyId, input), input);
		}
	};

	public static final TriFunction<IStatusId, ItemLabelsBean> toItemLabelsFromStatusId = new TriFunction<IStatusId, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IStatusId input) {
			return new ItemLabelsBean( sheet.getValue(AmDesignBeanId.statusId, input.getStatusId()), input.getStatusId());
		}
	};
	
	public static final TriFunction<IStatusId, String> toStatusIdFromStatus = new TriFunction<IStatusId, String>() {
		@Override
		public String apply(IStatusId input) {
			return input.getStatusId();
		}
	};

	public static final TriFunction<PjtViewBean, ItemLabelsBean> toItemLabelsFromPjtViewBean = new TriFunction<PjtViewBean, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(PjtViewBean pjtViewBean) {
			return new ItemLabelsBean(pjtViewBean.getPjtNo(), pjtViewBean.getPjtNo(), pjtViewBean.getChangeCauseNo());
		}
	};

	public static final TriFunction<ILotBlEntity, String> toBpIdsFromLotBl = new TriFunction<ILotBlEntity, String>() {
		@Override
		public String apply(ILotBlEntity lotBlEntity) {
			return lotBlEntity.getDataId();
		}
	};

	public static final TriFunction<ILotEntity, ItemLabelsBean> toItemLabelsFromLotEntity = new TriFunction<ILotEntity, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(ILotEntity lotEntity) {
			return new ItemLabelsBean(lotEntity.getLotNm(), lotEntity.getLotId());
		}
	};

	public static final TriFunction<String, ItemLabelsBean> toItemLabelsFromDataAttributeValues = new TriFunction<String, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(String input) {
			return new ItemLabelsBean( input, input);
		}
	};

	public static final TriFunction<ILotRelEnvLnkEntity, String> toBldEnvIdsFromLotRelEnvLnkEntity = new TriFunction<ILotRelEnvLnkEntity, String>(){
		@Override
		public String apply(ILotRelEnvLnkEntity input) {
			return input.getBldEnvId();
		}
	};
}
