package jp.co.blueship.tri.am.beans;

import java.io.Serializable;

import jp.co.blueship.tri.am.constants.MergeStatus;

public class MergeReport implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * インナークラス
	 *
	 */
	public class MergeLog {
		private String fileName = null ;	//ファイル名
		private MergeStatus mergeStatus = null ;//マージ結果ステータス

		/**
		 * ファイル名を取得する<br>
		 * @return ファイル名
		 */
		public String getFileName() {
			return fileName;
		}
		/**
		 * ファイル名をセットする<br>
		 * @param fileName ファイル名
		 */
		public void setFileName(String fileName) {
			this.fileName = fileName;
		}

		/**
		 * マージ結果ステータスを取得する<br>
		 * @return マージ結果ステータス
		 */
		public MergeStatus getMergeStatus() {
			return mergeStatus;
		}
		/**
		 * マージ結果ステータスをセットする<br>
		 * @param mergeStatus マージ結果ステータス
		 */
		public void setMergeStatus(MergeStatus mergeStatus) {
			this.mergeStatus = mergeStatus;
		}

	}
}
