package jp.co.blueship.tri.am.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class PjtSearchBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/**
	 * sessionに保持する場合、任意に一意とするキー値
	 */
	private String sessionKey = null;
	/**
	 * 詳細検索 ステータス
	 */
	private List<String> selectStatusViewList = null ;
	/**
	 * 詳細検索 選択ステータス
	 */
	//private String selectedStatusString = null;
	private String[] selectedStatusStringArray = null ;
	/**
	 * 詳細検索 変更管理番号
	 */
	private String searchPjtNo = null ;
	/**
	 * 詳細検索 変更要因番号
	 */
	private String searchChangeCauseNo = null ;
	/**
	 * 詳細検索 検索件数
	 */
	private List<ItemLabelsBean> searchPjtCountList = null ;
	/**
	 * 詳細検索 選択済み検索件数
	 */
	private String selectedPjtCount = null ;

	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getSelectedPjtCount() {
		return selectedPjtCount;
	}
	public void setSelectedPjtCount(String selectedPjtCount) {
		this.selectedPjtCount = selectedPjtCount;
	}
	public List<ItemLabelsBean> getSearchPjtCountList() {
		return searchPjtCountList;
	}
	public void setSearchPjtCountList(List<ItemLabelsBean> searchPjtCountList) {
		this.searchPjtCountList = searchPjtCountList;
	}
	public String getSearchChangeCauseNo() {
		return searchChangeCauseNo;
	}
	public void setSearchChangeCauseNo(String searchChangeCauseNo) {
		this.searchChangeCauseNo = searchChangeCauseNo;
	}
	public String getSearchPjtNo() {
		return searchPjtNo;
	}
	public void setSearchPjtNo(String searchPjtNo) {
		this.searchPjtNo = searchPjtNo;
	}

	public List<String> getSelectStatusViewList() {
		return selectStatusViewList;
	}
	public void setSelectStatusViewList(List<String> selectStatusViewList) {
		this.selectStatusViewList = selectStatusViewList;
	}

//	public String getSelectedStatusString() {
//		return selectedStatusString;
//	}

//	public void setSelectedStatusString( String[] statusArray ) {
//
//		StringBuilder buf = new StringBuilder();
//		buf.append("[");
//		buf.append( StringAddonUtil.convertArrayToString( statusArray ) ) ;
//		buf.append("]");
//
//		this.selectedStatusString = buf.toString();
//		this.selectedStatusStringArray = statusArray ;
//	}
//
//	public void setSelectedStatusString( String status ) {
//		this.selectedStatusString = status ;
//	}
	public void setSelectedStatusString( String[] statusArray ) {
		this.selectedStatusStringArray = statusArray;
	}
	public String getSelectedStatusString() {

		StringBuilder buf = new StringBuilder();
		if( null != this.selectedStatusStringArray ) {
			buf.append("[");
			buf.append( TriStringUtils.convertArrayToString( this.selectedStatusStringArray ) ) ;
			buf.append("]");
		}
		return buf.toString();
	}

	public String[] getSelectedStatusStringArray() {
		return selectedStatusStringArray;
	}
	//public void setSelectedStatusStringArray(String[] selectedStatusStringArray) {
	//	this.selectedStatusStringArray = selectedStatusStringArray;
	//}
}
