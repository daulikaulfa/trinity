package jp.co.blueship.tri.am.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・グループ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011
 */
public class GroupViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String groupId = null;
	private String groupName = null;
	
	public final String getGroupId() {
		return groupId;
	}

	public final void setGroupId( String id ) {
		this.groupId = id;
	}
	
	public final String getGroupName() {
		return groupName;
	}

	public final void setGroupName(String name) {
		this.groupName = name;
	}
}
