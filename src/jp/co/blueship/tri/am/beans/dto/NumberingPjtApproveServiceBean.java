package jp.co.blueship.tri.am.beans.dto;

import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

public class NumberingPjtApproveServiceBean extends NumberingServiceBean {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String targetPjtNo = null;

	public String getTargetPjtNo() {
		return targetPjtNo;
	}
	public void setTargetPjtNo( String targetPjtNo ) {
		this.targetPjtNo = targetPjtNo;
	}

}
