package jp.co.blueship.tri.am.beans.dto;

import java.io.Serializable;

/**
 * 変更管理・変更管理一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 変更概要 */
	private String pjtSummary = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** ステータス */
	private String pjtStatus = null;

	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getPjtSummary() {
		return pjtSummary;
	}
	public void setPjtSummary(String pjtSummary) {
		this.pjtSummary = pjtSummary;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getPjtStatus() {
		return pjtStatus;
	}
	public void setPjtStatus(String pjtStatus) {
		this.pjtStatus = pjtStatus;
	}
}
