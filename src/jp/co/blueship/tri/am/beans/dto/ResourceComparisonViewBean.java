package jp.co.blueship.tri.am.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.constants.FileStatus;

/**
 * A comparison of resource src and dest.
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ResourceComparisonViewBean {
	private ResourcePropertyView destResource = new ResourcePropertyView();
	private ResourcePropertyView srcResource = new ResourcePropertyView();
	private List<ResourceLineView> lines = new ArrayList<ResourceLineView>();

	public ResourcePropertyView getDestResource() {
		return destResource;
	}
	public void setDestResource(ResourcePropertyView destResource) {
		this.destResource = destResource;
	}

	public ResourcePropertyView getSrcResource() {
		return srcResource;
	}
	public void setSrcResource(ResourcePropertyView srcResource) {
		this.srcResource = srcResource;
	}

	public List<ResourceLineView> getLine() {
		return lines;
	}
	public ResourceComparisonViewBean setLine(List<ResourceLineView> lines) {
		this.lines = lines;
		return this;
	}

	public class ResourcePropertyView {
		private String resourcePath;
		private String resourceUpdTime;
		private String submitterNm;
		private String submitterIconPath;
		private boolean visible = true;


		public String getResourcePath() {
			return resourcePath;
		}
		public ResourcePropertyView setResourcePath(String resourcePath) {
			this.resourcePath = resourcePath;
			return this;
		}

		public String getResourceUpdTime() {
			return resourceUpdTime;
		}
		public ResourcePropertyView setResourceUpdTime(String resourceUpdTime) {
			this.resourceUpdTime = resourceUpdTime;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ResourcePropertyView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ResourcePropertyView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public boolean isVisible() {
			return this.visible;
		}
		public ResourcePropertyView setVisible(boolean visible) {
			this.visible = visible;
			return this;
		}
	}

	public class ResourceLineView {
		private FileStatus status = FileStatus.none;
		private String dest;
		private String destLineNo;
		private String src;
		private String srcLineNo;

		public FileStatus getStatus() {
			return status;
		}
		public ResourceLineView setStatus(FileStatus status) {
			this.status = status;
			return this;
		}

		public String getDest() {
			return dest;
		}
		public ResourceLineView setDest(String dest) {
			this.dest = dest;
			return this;
		}

		public String getDestLineNo() {
			return destLineNo;
		}
		public ResourceLineView setDestLineNo(String destLineNumber) {
			this.destLineNo = destLineNumber;
			return this;
		}

		public String getSrc() {
			return src;
		}
		public ResourceLineView setSrc(String src) {
			this.src = src;
			return this;
		}

		public String getSrcLineNo() {
			return srcLineNo;
		}
		public ResourceLineView setSrcLineNo(String srcLineNumber) {
			this.srcLineNo = srcLineNumber;
			return this;
		}
	}
}
