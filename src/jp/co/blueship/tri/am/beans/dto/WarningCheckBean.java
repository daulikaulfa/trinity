package jp.co.blueship.tri.am.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 警告時の確認メッセージを表示するためのコンポーネント
 *
 */
public class WarningCheckBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	List<String> commentList		= new ArrayList<String>();
	
	/** 警告あり */
	private boolean existWarning = false;
	/** 警告時の確認チェック */
	private boolean confirmCheck = false;
	
	public boolean isConfirmCheck() {
		return confirmCheck;
	}
	public void setConfirmCheck(boolean confirmCheck) {
		this.confirmCheck = confirmCheck;
	}
	public boolean isExistWarning() {
		return existWarning;
	}
	public void setExistWarning(boolean existWarning) {
		this.existWarning = existWarning;
	}
	public List<String> getCommentList() {
		return commentList;
	}
	public void setCommentList(List<String> commentList) {
		this.commentList = commentList;
	}
	
	
}
