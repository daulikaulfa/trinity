package jp.co.blueship.tri.am.beans.dto;

import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

public class NumberingPjtLotServiceBean extends NumberingServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択ロットID */
	private String selectedLotId = null;
	/** 選択ロットバージョンタグ */
	private String selectedLotVerTag = null;

	public String getSelectedLotId() {
		return selectedLotId;
	}
	public void setSelectedLotId( String selectedLotId ) {
		this.selectedLotId = selectedLotId;
	}
	public String getSelectedLotVersionTag() {
		return selectedLotVerTag;
	}
	public void setSelectedLotVersionTag(String selectedLotVerTag) {
		this.selectedLotVerTag = selectedLotVerTag;
	}

}
