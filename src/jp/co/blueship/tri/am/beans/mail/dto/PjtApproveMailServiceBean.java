package jp.co.blueship.tri.am.beans.mail.dto;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;

public class PjtApproveMailServiceBean extends MailServiceBean {

	/**
	 *
	 */
	private static final long serialVersionUID = 105066294417596111L;

	private IPjtEntity pjtEntity;
	private AreqDtoList applyEntityList;
	private String rejectApproveApplyNo;
	private String approveComment;


	public AreqDtoList getAssetApplyEntityList() {
		return applyEntityList;
	}
	public void setAssetApplyEntityList( AreqDtoList applyEntityList ) {
		this.applyEntityList = applyEntityList;
	}
	public IPjtEntity getPjtEntity() {
		return pjtEntity;
	}
	public void setPjtEntity( IPjtEntity pjtEntity ) {
		this.pjtEntity = pjtEntity;
	}
	public String getRejectApproveApplyNo() {
		return rejectApproveApplyNo;
	}
	public void setRejectApproveApplyNo( String rejectApproveApplyNo ) {
		this.rejectApproveApplyNo = rejectApproveApplyNo;
	}
	public String getApproveComment() {
		return approveComment;
	}
	public void setApproveComment(String approveComment) {
		this.approveComment = approveComment;
	}

}
