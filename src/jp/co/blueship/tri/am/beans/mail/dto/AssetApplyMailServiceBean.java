package jp.co.blueship.tri.am.beans.mail.dto;

import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;

public class AssetApplyMailServiceBean extends MailServiceBean {

	/**
	 *
	 */
	private static final long serialVersionUID = -6815449967817707022L;

	private IAreqEntity assetApplyEntity;
	private IPjtEntity pjtEntity;
	private ILotEntity pjtLotEntity;

	private String bldSrvId;
	private String bldSrvNm;

	private List<IAreqFileEntity> lendApplyFileList;
	private List<IAreqFileEntity> returnApplyFileList;
	private List<IAreqFileEntity> delApplyFileList;
	private List<IAreqBinaryFileEntity> delApplyBinaryFileList;

/*	private String changeCauseNo = null;
	private String applyNo = null;*/


	public IAreqEntity getAssetApplyEntity() {
		return assetApplyEntity;
	}
	public void setAssetApplyEntity(IAreqEntity assetApplyEntity) {
		this.assetApplyEntity = assetApplyEntity;
	}
	public IPjtEntity getPjtEntity() {
		return pjtEntity;
	}
	public void setPjtEntity(IPjtEntity pjtEntity) {
		this.pjtEntity = pjtEntity;
	}
	public ILotEntity getLotEntity() {
		return pjtLotEntity;
	}
	public void setLotEntity(ILotEntity pjtLotEntity) {
		this.pjtLotEntity = pjtLotEntity;
	}

	/**
	 * bldSrvIdを取得します。
	 * @return bldSrvId
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * bldSrvIdを設定します。
	 * @param bldSrvId bldSrvId
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	}
	/**
	 * bldSrvNmを取得します。
	 * @return bldSrvNm
	 */
	public String getBldSrvNm() {
	    return bldSrvNm;
	}
	/**
	 * bldSrvNmを設定します。
	 * @param bldSrvNm bldSrvNm
	 */
	public void setBldSrvNm(String bldSrvNm) {
	    this.bldSrvNm = bldSrvNm;
	}
	public List<IAreqFileEntity> getLendApplyFileList() {
		return lendApplyFileList;
	}
	public void setLendApplyFileList(List<IAreqFileEntity> lendApplyFileList) {
		this.lendApplyFileList = lendApplyFileList;
	}
	public List<IAreqFileEntity> getReturnApplyFileList() {
		return returnApplyFileList;
	}
	public void setReturnApplyFileList(List<IAreqFileEntity> returnApplyFileList) {
		this.returnApplyFileList = returnApplyFileList;
	}
	public List<IAreqBinaryFileEntity> getDelApplyBinaryFileList() {
		return delApplyBinaryFileList;
	}
	public void setDelApplyBinaryFileList(List<IAreqBinaryFileEntity> delApplyBinaryFileList) {
		this.delApplyBinaryFileList = delApplyBinaryFileList;
	}
	public List<IAreqFileEntity> getDelApplyFileList() {
		return delApplyFileList;
	}
	public void setDelApplyFileList(List<IAreqFileEntity> delApplyFileList) {
		this.delApplyFileList = delApplyFileList;
	}

}
