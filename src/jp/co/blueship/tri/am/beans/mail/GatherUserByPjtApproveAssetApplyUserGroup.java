package jp.co.blueship.tri.am.beans.mail;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.beans.mail.dto.PjtApproveMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPair;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 変更管理承認に含まれる資産申請（返却／削除）を行ったユーザが所属するグループの全ユーザ情報を取得します。 <br>
 * columnに指定可能な項目 <br>
 * ・申請情報の項目名 <br>
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherUserByPjtApproveAssetApplyUserGroup implements IDomain<PjtApproveMailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	private IUserDao userDao = null;
	private List<String> column = null;

	public GatherUserByPjtApproveAssetApplyUserGroup() {
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 *
	 * @param column 申請の該当項目名
	 */
	public final void setColumn(List<String> column) {
		this.column = column;
	}

	@Override
	public IServiceDto<PjtApproveMailServiceBean> execute(IServiceDto<PjtApproveMailServiceBean> serviceDto) {

		PjtApproveMailServiceBean paramBean = null;
		SendMailBean targetBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			targetBean = paramBean.getTarget();
			if (null == targetBean.getUsersView()) {
				targetBean.setUsersView(new ArrayList<IUserEntity>());
			}

			AreqDtoList entityList = paramBean.getAssetApplyEntityList();

			for (IAreqDto entity : entityList) {

				TriPair<String, String> userIdentify = userIdentifyFrom( entity.getAreqEntity() );
				PreConditions.assertOf(userIdentify.isPresent(), "UserID or UserName is not found from AreqEntity.");

				if (identifyName(userIdentify).substring(identifyName(userIdentify).length() - 2).equals("Id")) {

					this.populateUserEntityList(targetBean.getUsersView(), userIdentifyValue(userIdentify));

				} else {

					// ユーザ名からユーザエンティティ取得（同姓同名で複数のケースあり）
					UserCondition condition = new UserCondition();
					condition.setUserNm( userIdentifyValue(userIdentify) );
					List<IUserEntity> users = this.userDao.find(condition.getCondition());
					for (IUserEntity userEntity : users) {
						this.populateUserEntityList(targetBean.getUsersView(), userEntity.getUserId());
					}
				}
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005084S, e, "GatherUserByPjtApproveAssetApplyUserGroup");
		}

		return serviceDto;
	}

	private String userIdentifyValue(TriPair<String, String> userColumnAndValue) {
		return userColumnAndValue.getWife();
	}

	private String identifyName(TriPair<String, String> userColumnAndValue) {
		return userColumnAndValue.getHasband();
	}

	private TriPair<String, String> userIdentifyFrom(IAreqEntity entity) {

		for (String key : this.column) {
			String columnValue = (String) TriPropertyUtils.getProperty(entity, key);
			if (!TriStringUtils.isEmpty(columnValue)) {
				return new TriPair<String, String>(key, columnValue);
			}
		}

		return new TriPair<String, String>();
	}

	/**
	 * 指定したユーザが所属する全グループについて、それぞれ所属メンバーの ユーザエンティティを取得する。その際、重複は除外する。
	 *
	 * @param list
	 * @param user_id
	 */
	private void populateUserEntityList(List<IUserEntity> list, String user_id) {

		forEach(groupsFrom(user_id), retrieveUserByGroup(list));
	}

	private TriClojure<IGrpEntity> retrieveUserByGroup(final List<IUserEntity> list) {

		return new TriClojure<IGrpEntity>() {

			@Override
			public boolean apply(IGrpEntity grpEntity) {
				forEach(usersFrom(grpEntity.getGrpId()), addUserToResultList(list));
				return true;
			}
		};
	}

	private TriClojure<IUserEntity> addUserToResultList(final List<IUserEntity> list) {

		return new TriClojure<IUserEntity>() {

			@Override
			public boolean apply(IUserEntity user) {
				// 重複するエンティティは追加しない
				if (!isDuplicate(list, user)) {
					list.add(user);
				}
				return true;
			}
		};

	}

	private List<IUserEntity> usersFrom(String groupId) {
		return umFinderSupport.findUserByGroup(groupId);
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return umFinderSupport.findGroupByUserId(userId);
	}

	private static Boolean isDuplicate(List<IUserEntity> list, final IUserEntity user) {

		return FluentList.from(list).contains(new TriPredicate<IUserEntity>() {

			@Override
			public boolean evalute(IUserEntity entity) {
				return entity.getUserId().equals(user.getUserId());
			}
		});

	}

}
