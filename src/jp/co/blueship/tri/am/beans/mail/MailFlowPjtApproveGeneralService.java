package jp.co.blueship.tri.am.beans.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.beans.mail.dto.PjtApproveMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.CreateMailContentsException;
import jp.co.blueship.tri.fw.mail.beans.IMailCallback;
import jp.co.blueship.tri.fw.mail.beans.MailServiceSupport;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;

/**
 * 変更管理のアクション実行時、メール送信を行う汎用クラス。
 * <br>columnに指定可能な項目
 * <br>・コメントの項目
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class MailFlowPjtApproveGeneralService extends MailServiceSupport {

	private static final ILog log = TriLogFactory.getInstance();

	private AmFinderSupport support = null;

	public void setSupport(AmFinderSupport support) {
		this.support = support;
	}

	/**
	 * {@inheritDoc}
	 */
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {
			this.sendMail(
					this.getMailServiceBean( serviceDto ),
					new IMailCallback() {

						public Map<String, Object> getContents( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getContentsCallBack( bean, data );
						}

						public Map<String, Object> getSubject( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getSubjectCallBack( bean, data );
						}
					});

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( AmMessageId.AM005121S , e );
		}

	}

	/**
	 * メール送信の件名を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getSubjectCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {
		//エンティティ取得
		PjtApproveMailServiceBean mailBean = (PjtApproveMailServiceBean)bean;
		IPjtEntity pjtEntity = mailBean.getPjtEntity();

		if ( null != pjtEntity ){
			data.put("_pjtId", pjtEntity.getPjtId());
			data.put("_lotId", pjtEntity.getLotId());
			data.put("_chgFactorNo", pjtEntity.getChgFactorNo());
		}

		return data;
	}

	/**
	 * メール送信の内容を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getContentsCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {

		PjtApproveMailServiceBean mailBean = (PjtApproveMailServiceBean)bean;
		IPjtEntity pjtEntity = mailBean.getPjtEntity();

		if ( null != pjtEntity ){
			data.put("_pjtId", 			pjtEntity.getPjtId() );
			data.put("_chgFactorNo",	pjtEntity.getChgFactorNo() );
		}
		data.put("_approvalRejectedAreqId", mailBean.getRejectApproveApplyNo());
		data.put("_approvalComment", mailBean.getApproveComment());

		List<String> applyNoList = new ArrayList<String>();
		for ( IAreqDto entity : mailBean.getAssetApplyEntityList() ) {
			applyNoList.add( entity.getAreqEntity().getAreqId() );
		}
		data.put( "applyNoList", applyNoList );

		data.put("_date", 			TriDateUtils.convertViewDateFormat(bean.getSystemDate(), TriDateUtils.getYMDDateFormat() ) );
		data.put("_time", 			TriDateUtils.convertViewDateFormat(bean.getSystemDate(), TriDateUtils.getHMSDateFormat() ) );
		data.put("_sendFrom",		bean.getFrom().getUserView());
		data.put("_sendTo",			bean.getTo().getUserView());
		if ( null != bean.getCc().getUsersView()){
			List<String> ccUser = new ArrayList<String>();
			for(IUserEntity user : bean.getCc().getUsersView()){
				ccUser.add( user.getUserNm() );
			}

			data.put("sendCc",		ccUser);
			data.put("sendCcSize",	bean.getCc().getUsersView().size());
		}

		List<?> messageList = new ArrayList<String>();
		if ( null != bean.getBusinessThrowable() ) {
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			messageList = ca.getMessage( bean.getBusinessThrowable() );

			data.put( "messageSize", new Integer(messageList.size()) );
			data.put( "messageList", messageList );
		}

		return data;
	}

	/**
	 * メール送信先取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @return 生成したインスタンス
	 */
	private IServiceDto<MailServiceBean> getMailServiceBean( IServiceDto<IGeneralServiceBean> serviceDto ) {

		PjtApproveMailServiceBean destBean = new PjtApproveMailServiceBean();

		IServiceDto<MailServiceBean> mailServiceDto = new ServiceDto<MailServiceBean>();
		mailServiceDto.setServiceBean( destBean );

		TriPropertyUtils.copyProperties(destBean, serviceDto.getServiceBean());

		destBean.setVmResourceLoaderPath( AmDesignBusinessRuleUtils.getTemplatePath().getPath() );

		List<IEntity> entity = new ArrayList<IEntity>();

		IPjtEntity pjtEntity = destBean.getPjtEntity();
		ILotEntity lotEntity = support.findLotEntity( pjtEntity.getLotId() );

		entity.add( pjtEntity );
		entity.add( lotEntity );

		destBean.setEntity( entity );

		return mailServiceDto;
	}

}
