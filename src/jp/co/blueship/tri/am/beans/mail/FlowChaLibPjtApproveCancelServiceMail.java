package jp.co.blueship.tri.am.beans.mail;

import jp.co.blueship.tri.am.beans.mail.dto.PjtApproveMailServiceBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlDto;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtApproveCancelServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtApproveEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;


/**
 * 変更管理承認・変更管理承認取消完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibPjtApproveCancelServiceMail implements IDomain<FlowChaLibPjtApproveCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	private FlowChaLibPjtApproveEditSupport support = null;
	public void setSupport( FlowChaLibPjtApproveEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtApproveCancelServiceBean> execute( IServiceDto<FlowChaLibPjtApproveCancelServiceBean> serviceDto ) {

		FlowChaLibPjtApproveCancelServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL ) ){
				return serviceDto;
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_APPROVE_CANCEL )) {

				PjtApproveMailServiceBean successMailBean = new PjtApproveMailServiceBean();
				TriPropertyUtils.copyProperties( successMailBean, paramBean );
				IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

  				String selectedPjtNo = paramBean.getSelectedPjtNo();
				String selectedPjtAvlSeqNo = paramBean.getPjtAvlSeqNo();
				String approveComment = paramBean.getPjtEditInputBean().getApproveCancelComment();

				IPjtAvlDto pjtAvlDto = new PjtAvlDto();
				IPjtAvlEntity recEntity =
					this.support.findPjtAvlEntity( selectedPjtNo, selectedPjtAvlSeqNo, (StatusFlg)null );
				pjtAvlDto.setPjtAvlEntity( recEntity );
				pjtAvlDto.setPjtEntity( this.support.findPjtEntity( selectedPjtNo ) );

				AreqDtoList assetApplyEntities =
					this.support.getAssetApplyEntities( paramBean.getApplyInfoViewBeanList() );

				// 今回承認取消をされた変更管理番号と申請管理番号
				successMailBean.setPjtEntity			( pjtAvlDto.getPjtEntity() );
				successMailBean.setAssetApplyEntityList	( assetApplyEntities );
				successMailBean.setApproveComment		( approveComment );

				successMail.execute( mailServiceDto );

			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , paramBean.getFlowAction() ));
		}

		return serviceDto;
	}
}
