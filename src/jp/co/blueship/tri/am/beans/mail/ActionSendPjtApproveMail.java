package jp.co.blueship.tri.am.beans.mail;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.beans.mail.dto.PjtApproveMailServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;

/**
 * 業務詳細フロー（Action）内で、メール送信を行うための、ラッパークラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionSendPjtApproveMail extends ActionPojoAbstract<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IGeneralServiceBean info = serviceDto.getServiceBean();

		if ( null == info ) {
			return serviceDto;
		}

		try {
			PjtApproveMailServiceBean successMailBean = new PjtApproveMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, info );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

			// 今回承認のチェックをされた変更管理番号と申請管理番号
			IApproveParamInfo[] approveParamInfos = AmExtractMessageAddonUtils.getApproveParamInfoArray( paramList );

			for ( IApproveParamInfo approveParamInfo : approveParamInfos ) {

				successMailBean.setPjtEntity			( approveParamInfo.getPjtAvlDto().getPjtEntity() );
				successMailBean.setAssetApplyEntityList	( approveParamInfo.getAssetApplyEntities() );
				successMailBean.setApproveComment		( approveParamInfo.getComment() );

				successMail.execute( mailServiceDto );

			}
		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			String flowAction = (TriStringUtils.isEmpty( info.getFlowAction() ))? "": info.getFlowAction();
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , flowAction));
		}

		return serviceDto;
	}
}
