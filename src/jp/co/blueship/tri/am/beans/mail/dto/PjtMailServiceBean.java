package jp.co.blueship.tri.am.beans.mail.dto;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;

public class PjtMailServiceBean extends MailServiceBean {

	/**
	 *
	 */
	private static final long serialVersionUID = 2523213081815626092L;

	private IPjtAvlDto pjtAvlDto;
	private ILotEntity pjtLotEntity;

	public IPjtAvlDto getPjtAvlDto() {
		return pjtAvlDto;
	}
	public void setPjtAvlDto(IPjtAvlDto pjtDto) {
		this.pjtAvlDto = pjtDto;
	}
	public ILotEntity getLotEntity() {
		return pjtLotEntity;
	}
	public void setLotEntity(ILotEntity pjtLotEntity) {
		this.pjtLotEntity = pjtLotEntity;
	}
}
