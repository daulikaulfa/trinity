package jp.co.blueship.tri.am.beans.mail;

import jp.co.blueship.tri.am.AmEntityAddonUtils;
import jp.co.blueship.tri.am.beans.mail.dto.PjtMailServiceBean;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.am.dao.pjtavl.eb.PjtAvlDto;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailInputBean;
import jp.co.blueship.tri.am.domain.pjt.dto.FlowChaLibPjtEntryServiceBean;
import jp.co.blueship.tri.am.support.FlowChaLibPjtEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.fw.ui.constants.ChaLibScreenID;



/**
 * 変更管理情報登録のメール送信設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowChaLibPjtEntryServiceMail implements IDomain<FlowChaLibPjtEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	private FlowChaLibPjtEditSupport support = null;
	public void setSupport( FlowChaLibPjtEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowChaLibPjtEntryServiceBean> execute( IServiceDto<FlowChaLibPjtEntryServiceBean> serviceDto ) {

		FlowChaLibPjtEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( ChaLibScreenID.COMP_PJT_ENTRY ) &&
					!forwordID.equals( ChaLibScreenID.COMP_PJT_ENTRY ) ){
				return serviceDto;
			}

			if ( forwordID.equals( ChaLibScreenID.COMP_PJT_ENTRY )) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					String targetPjtNo	= paramBean.getPjtNo();

					PjtDetailInputBean pjtDetailInputBean = paramBean.getPjtDetailInputBean();

					PjtDetailBean pjtDetailBean	= pjtDetailInputBean.getPjtDetailBean();
					pjtDetailBean.setPjtNo( targetPjtNo );

					IPjtEntity pjtEntity = new PjtEntity();
					AmEntityAddonUtils.setPjtEntityPjtDetailBean(
							pjtEntity, pjtDetailBean, paramBean.getUserName() , paramBean.getUserId() );

					pjtEntity.setPjtId( targetPjtNo );

					ILotEntity pjtLotEntity = this.support.findLotEntity( paramBean.getSelectedLotNo() );

					PjtMailServiceBean successMailBean = new PjtMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					IPjtAvlDto pjtDto = new PjtAvlDto();
					pjtDto.setPjtEntity( pjtEntity );
					successMailBean.setPjtAvlDto( pjtDto );
					successMailBean.setLotEntity( pjtLotEntity );

					successMail.execute( mailServiceDto );

					return serviceDto;
				}
			}

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( AmMessageId.AM005085S, e , paramBean.getFlowAction() ));
		}

		return serviceDto;
	}

}
