package jp.co.blueship.tri.am.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.beans.dto.NumberingPjtApproveServiceBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.IApproveParamInfo;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.beans.NumberingServiceSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.INumberingCallback;
import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;


/**
 * 変更承認のアクション実行時、VCSタグ採番を行う汎用クラス。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class NumberingPjtApproveGeneralService extends NumberingServiceSupport<IGeneralServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IAmFinderSupport support = null;

	public void setSupport( IAmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> prmBLBean ) {


		try {
			NumberingPjtApproveServiceBean numberingServiceBean =
					this.getNumberingServiceBean( prmBLBean.getServiceBean(), prmBLBean.getParamList() );

			this.numbering(
					numberingServiceBean,
					new INumberingCallback() {

						public Map<String, Object> getNumbering( NumberingServiceBean bean, Map<String, Object> data ) {
							return getNumberingCallBack( bean, data );
						}
					});

			TriPropertyUtils.copyProperties( prmBLBean.getServiceBean(), numberingServiceBean );

			return prmBLBean;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005005S, e );
		}

	}

	/**
	 * 採番を取得します。
	 *
	 * @param bean 採番サービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> getNumberingCallBack( NumberingServiceBean bean, Map<String, Object> data ) {
		return data;
	}

	/**
	 * 採番取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @param paramList
	 * @return 生成したインスタンス
	 */
	private NumberingPjtApproveServiceBean getNumberingServiceBean( Object paramBean, List<Object> paramList ) {
		NumberingPjtApproveServiceBean destBean = new NumberingPjtApproveServiceBean();
		IApproveParamInfo paramInfo = AmExtractMessageAddonUtils.getApproveParamInfo( paramList );

		TriPropertyUtils.copyProperties(destBean, paramBean);
		destBean.setTargetPjtNo( paramInfo.getPjtAvlDto().getPjtEntity().getPjtId() );

		if ( ! TriStringUtils.isEmpty( destBean.getTargetPjtNo() ) ) {
			List<IEntity> entityList = new ArrayList<IEntity>();

			entityList.add( this.support.findPjtEntity( destBean.getTargetPjtNo() ) );

			destBean.setEntities( entityList );
		}

		return destBean;
	}

}
