package jp.co.blueship.tri.am.beans.lock;

import java.util.List;

import jp.co.blueship.tri.am.AmExtractMessageAddonUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.domain.areq.beans.dto.IReturnProcessParam;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcDetailsEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcDetailsCondition;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 返却資産申請に関連する業務排他の解除を行います。
 * <br>業務排他は、共通ルーチンで行われるため、ここでは、ステータスの更新のみを行う。
 * ここでステータス更新を行う。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionUnLockLendByReturnApply extends ActionPojoAbstract<IGeneralServiceBean> {

	private AmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( AmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReturnProcessParam param = AmExtractMessageAddonUtils.extractReturnProcessParam( paramList );
		if ( null == param ) {
			throw new TriSystemException( AmMessageId.AM005006S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		IAreqEntity areqEntity = null;
		{
			AreqCondition condition = new AreqCondition();
			condition.setAreqId(param.getApplyNo());
			areqEntity = support.getAreqDao().findByPrimaryKey(condition.getCondition());

			if ( null == areqEntity ) {
				throw new TriSystemException( AmMessageId.AM005003S );
			}
		}

		ProcDetailsCondition condition = new ProcDetailsCondition();
		condition.setProcId(paramBean.getProcId());
		condition.setProcCtgCd(param.getProcCtgCd());
		IProcDetailsEntity processEntity = support.getSmFinderSupport().getProcDetailsDao().findByPrimaryKey(condition.getCondition());

		// エラー有無判定
		if ( null == processEntity || ! TriStringUtils.isEmpty( processEntity.getMsgId() ) ) {
			support.getSmFinderSupport().updateExecDataSts(
					paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.CheckinRequestError, param.getApplyNo());
		} else {
			support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );
		}

		return serviceDto;
	}

}
