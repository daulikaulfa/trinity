package jp.co.blueship.tri.am.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.beans.dto.NumberingPjtLotServiceBean;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.support.FlowChaLibMergeEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.sm.beans.NumberingServiceSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.INumberingCallback;
import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

/**
 * ロットのアクション実行時、採番を行う汎用クラス。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class NumberingPjtLotGeneralService extends NumberingServiceSupport<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowChaLibMergeEditSupport support = null;

	public void setSupport( FlowChaLibMergeEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> prmBLBean ) {


		try {
			NumberingPjtLotServiceBean numberingServiceBean = this.getNumberingServiceBean( prmBLBean.getServiceBean() );

			this.numbering(
					numberingServiceBean,
					new INumberingCallback() {

						public Map<String, Object> getNumbering( NumberingServiceBean bean, Map<String, Object> data ) {
							return getNumberingCallBack( bean, data );
						}
					});

			TriPropertyUtils.copyProperties( prmBLBean.getServiceBean(), numberingServiceBean );

			return prmBLBean;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005083S , e);
		}
	}

	/**
	 * 採番を取得します。
	 *
	 * @param bean 採番サービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> getNumberingCallBack( NumberingServiceBean bean, Map<String, Object> data ) {
		return data;
	}

	/**
	 * 採番取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @return 生成したインスタンス
	 */
	private NumberingPjtLotServiceBean getNumberingServiceBean( Object paramBean ) {
		NumberingPjtLotServiceBean destBean = new NumberingPjtLotServiceBean();

		TriPropertyUtils.copyProperties(destBean, paramBean);

		if ( ! TriStringUtils.isEmpty( destBean.getSelectedLotId() )
			&& ! TriStringUtils.isEmpty( destBean.getSelectedLotVersionTag() ) ) {

			ILotBlEntity lotBlEntity = this.getLotBlEntityByVersionTagAtFirst(
															destBean.getSelectedLotId(),
															destBean.getSelectedLotVersionTag() );

			List<IEntity> entityList = new ArrayList<IEntity>();

			entityList.add( lotBlEntity );
			destBean.setEntities( entityList );
		}

		return destBean;
	}

	/**
	 * ベースラインタグをもとに、レコードを取得する。<br>
	 * @param lotId ロット番号
	 * @param lotVersionTag ロットチェックイン時のバージョンタグ

	 * @return IHeadLotBlEntity
	 */
	private final ILotBlEntity getLotBlEntityByVersionTagAtFirst( String lotId , String lotVersionTag ) {
		return this.support.getLotBlEntityByVersionTagAtFirst(lotId, lotVersionTag);
	}


}
