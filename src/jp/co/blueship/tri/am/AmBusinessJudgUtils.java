package jp.co.blueship.tri.am;

import java.util.HashSet;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;

/**
 * 指定した値の判定を行うユーティリティークラスです。
 * BusinessJudgUtilsの派生クラスです。依存性の解消のため、作成します。
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class AmBusinessJudgUtils {

	/**
	 * 申請区分が貸出申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isLendApply( String areqCtgCd ) {
		if ( AreqCtgCd.LendingRequest.equals(areqCtgCd) )
			return true;

		return false;
	}

	/**
	 * @param areqCtgCd
	 * @return
	 */
	public static final boolean isLendApply( AreqCtgCd areqCtgCd ) {
		if ( null == areqCtgCd )
			return false;

		if ( AreqCtgCd.LendingRequest.equals(areqCtgCd) )
			return true;

		return false;
	}

	/**
	 * 申請区分が貸出申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isLendApply( IAreqEntity entity ) {
		return isLendApply(  entity.getAreqCtgCd() );
	}
	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isReturnApply( String areqCtgCd ) {
		if ( AreqCtgCd.ReturningRequest.equals(areqCtgCd) )
			return true;

		return false;
	}
	/**
	 * @param areqCtgCd
	 * @return
	 */
	public static final boolean isReturnApply( AreqCtgCd areqCtgCd ) {
		if ( null == areqCtgCd )
			return false;

		if ( AreqCtgCd.ReturningRequest.equals(areqCtgCd) )
			return true;

		return false;
	}
	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isReturnApply( IAreqEntity entity ) {
		return isReturnApply(  entity.getAreqCtgCd() );
	}

	/**
	 * この申請が返却申請可能かどうかを判定します。
	 *
	 * @param entity
	 * @return
	 */
	public static final boolean isReturnApplyEnabled( IAreqEntity entity ) {
		if ( null == entity )
			return false;

		if ( StatusFlg.on.equals( entity.getDelStsId() ) )
			return false;

		if ( ! AmAreqStatusId.CheckoutRequested.equals( entity.getStsId() ) )
			return false;

		if ( AmAreqStatusIdForExecData.RequestingCheckin.getStatusId().equals( entity.getProcStsId() ) )
			return false;


		return true;
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDeleteApply( String areqCtgCd ) {
		if ( null == areqCtgCd )
			return false;

		if ( AreqCtgCd.RemovalRequest.equals(areqCtgCd) )
			return true;

		return false;
	}
	public static final boolean isDeleteApply( AreqCtgCd areqCtgCd ) {
		if ( AreqCtgCd.RemovalRequest.equals(areqCtgCd) )
			return true;

		return false;
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDeleteApply( IAreqEntity entity ) {
		return isDeleteApply(  entity.getAreqCtgCd() );
	}

	/**
	 * 申請が下書きかどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDraft( IAreqEntity entity ) {
		return isDraft( AmAreqStatusId.value( entity.getStsId() ) );
	}

	/**
	 * judge whether the request is "draft".<br>
	 * <br>
	 * 申請が下書きかどうかを判断します。<br>
	 *
	 * @param statusId
	 * @return retur true if request is draft.
	 */
	public static final boolean isDraft( AmAreqStatusId statusId ) {
		if(AmAreqStatusId.DraftCheckoutRequest.equals( statusId )){
			return true;
		}

		if(AmAreqStatusId.DraftCheckinRequest.equals( statusId )){
			return true;
		}

		if(AmAreqStatusId.DraftRemovalRequest.equals( statusId )){
			return true;
		}

		return false;
	}

	/**
	 * judge whether the request is "pending request".<br>
	 * <br>
	 * 申請が申請待ちかどうかを判断します。<br>
	 *
	 * @param entity
	 * @return return true if request is pending request
	 */
	public static final boolean isPendingRequest( IAreqEntity entity ){
		return isPendingRequest( AmAreqStatusId.value( entity.getStsId() ) );
	}

	/**
	 * judge whether the request is "pending request".<br>
	 * <br>
	 * 申請が申請待ちかどうかを判断します。<br>
	 *
	 * @param statusId
	 * @return return true if request is pending request
	 */
	public static final boolean isPendingRequest( AmAreqStatusId statusId ){
		if( AmAreqStatusId.PendingRemovalRequest.equals(statusId) ){
			return true;
		}
		return false;
	}

	/**
	 * この申請が変更承認待ちのステータスかどうかを判定します。
	 *
	 * @param entity
	 * @return
	 */
	public static final boolean isPjtApproveEnabled( IAreqEntity entity ) {
		if ( null == entity )
			return false;

		if ( StatusFlg.on.equals( entity.getDelStsId() ) )
			return false;

		//返却申請済み、又は削除申請済み、以外は対象外
		if ( ! AmAreqStatusId.CheckinRequested.equals( entity.getStsId() )
				&& ! AmAreqStatusId.RemovalRequested.equals( entity.getStsId() ) )
			return false;

		//承認中（処理中）であれば対象外
		if ( AmAreqStatusIdForExecData.ApprovingCheckinRequest.getStatusId().equals( entity.getProcStsId() )
				|| AmAreqStatusIdForExecData.ApprovingRemovalRequest.getStatusId().equals( entity.getProcStsId() ) )
			return false;


		return true;
	}

	/**
	 * この申請がビルドパッケージクローズかどうかを判定します。
	 * <br>ビルドパッケージを含む、それ以降のステータスを判定したい場合は、{@link isIncludedRelUnitClose}を使用して下さい。
	 *
	 * @param entity
	 * @return
	 */
	public static final boolean isRelUnitClose( IAreqEntity entity ) {
		if ( null == entity )
			return false;

		if ( AmAreqStatusId.BuildPackageClosed.equals( entity.getStsId() ) )
			return true;

		return false;
	}

	/**
	 * 変更情報の承認を取消可能かどうかを判定します。
	 *
	 * @param entity ロットEntity
	 * @return 取消可能であればtrue。それ以外はfalseを戻します。
	 */
	public static final boolean voidableApprovalByPjt( ILotEntity entity ) {
		if ( null == entity)
			return false;

		return true;
	}


	/**
	 * 資産申請のステータスがロック中かどうかを判定します。
	 *
	 * @param statusId 資産申請のステータスID
	 * @return 資産申請がロック中であればtrue。それ以外はfalseを戻します。
	 */
	public static final boolean isResourceLock( String statusId ) {
		if ( TriStringUtils.isEmpty(statusId) )
			return false;

		Set<String> lockId = new HashSet<String>();

		lockId.add( AmAreqStatusId.CheckoutRequested.getStatusId() );
		lockId.add( AmAreqStatusId.CheckinRequested.getStatusId() );
		lockId.add( AmAreqStatusId.RemovalRequested.getStatusId() );

		lockId.add( AmAreqStatusId.CheckinRequestApproved.getStatusId() );
		lockId.add( AmAreqStatusId.RemovalRequestApproved.getStatusId() );

		return lockId.contains( statusId );
	}

}
