package jp.co.blueship.tri.am;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqAttachedFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.domain.areq.beans.dto.ChaLibEntryInputBaseInfoBean;
import jp.co.blueship.tri.am.domain.pjt.beans.dto.PjtDetailBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * エンティティへの入出力の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008-2009
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class AmEntityAddonUtils {

	/**
	 * 貸出申請／貸出編集フローのエンティティ編集を行います。
	 *
	 * @param entity 編集するエンティティ
	 * @param lotEntity 申請に関連するロットエンティティ
	 * @param inputBaseInfoBean 入力情報
	 * @return
	 */
	public static void setAssetApplyEntityByFlowChaLibLendEntryService(
											IAreqEntity entity,
											ILotEntity lotEntity,
											ChaLibEntryInputBaseInfoBean inputBaseInfoBean) {

		entity.setAreqCtgCd			( AreqCtgCd.LendingRequest.value() );
		entity.setSummary			( inputBaseInfoBean.getInSubject() );
		entity.setContent			( inputBaseInfoBean.getInContent() );
		entity.setStsId				( AmAreqStatusId.CheckoutRequested.getStatusId() );
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( inputBaseInfoBean.getInUserName() );
		entity.setUpdUserId			( inputBaseInfoBean.getInUserId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setRtnReqDueDate		( inputBaseInfoBean.getCheckinDueDate() );
		entity.setAssigneeId		( inputBaseInfoBean.getAssigneeId() );
		entity.setAssigneeNm		( inputBaseInfoBean.getAssigneeNm() );

		if ( TriStringUtils.isEmpty(entity.getAreqId()) ) {
			entity.setPjtId			( inputBaseInfoBean.getInPrjNo() );
			entity.setGrpId			( inputBaseInfoBean.getInGroupId() );
			entity.setGrpNm			( inputBaseInfoBean.getInGroupName() );
			entity.setLendReqTimestamp( TriDateUtils.getSystemTimestamp() );
			entity.setLendReqUserNm	( inputBaseInfoBean.getInUserName() );
			entity.setLendReqUserId	( inputBaseInfoBean.getInUserId() );
		}
	}

	/**
	 * 貸出申請取消用
	 * @param dto
	 * @param paramBean
	 * @return
	 */
	public static void setAssetApplyEntityEntryCancelServiceBean(IAreqDto dto, GenericServiceBean paramBean) {
		Timestamp systemDate = TriDateUtils.getSystemTimestamp();

		paramBean.setSystemDate	( systemDate );
		paramBean.setUserName			( paramBean.getUserName() );

		// am_areqレコード論理削除
		IAreqEntity areqEntity = dto.getAreqEntity();
		areqEntity.setStsId			( AmAreqStatusId.CheckoutRequestRemoved.getStatusId() );
		areqEntity.setDelTimestamp	( systemDate );
		areqEntity.setDelUserNm		( paramBean.getUserName() );
		areqEntity.setDelUserId		( paramBean.getUserId() );
		areqEntity.setDelStsId		( StatusFlg.on );
		// am_areq_fileレコード論理削除
		for ( IAreqFileEntity areqFileEntiry : dto.getAreqFileEntities() ) {
			areqFileEntiry.setDelStsId( StatusFlg.on );
		}
		// am_areq_binary_file論理削除
		for ( IAreqBinaryFileEntity areqBinaryFileEntiry : dto.getAreqBinaryFileEntities() ) {
			areqBinaryFileEntiry.setDelStsId( StatusFlg.on );
		}
		// am_areq_attached_file論理削除
		for ( IAreqAttachedFileEntity areqAttachedFileEntiry : dto.getAreqAttachedFileEntities() ) {
			areqAttachedFileEntiry.setDelStsId( StatusFlg.on );
		}
	}

	/**
	 * 変更管理エンティティに変更管理情報を設定する。
	 * @param entity 変更管理エンティティ
	 * @param inputBean 変更管理情報
	 * @param userName 登録／更新ユーザ名
	 * @return
	 */
	public static void setPjtEntityPjtDetailBean(
			IPjtEntity entity, PjtDetailBean inputBean, String userName , String userId ) {

		entity.setChgFactorNo				( inputBean.getChangeCauseNo() );
		entity.setChgFactorId				( DesignSheetFactory.getDesignSheet().getKey(
				AmDesignBeanId.changeCauseClassifyId,
				inputBean.getChangeCauseClassify() ));
		entity.setSummary					( inputBean.getPjtSummary() );
		entity.setContent					( inputBean.getPjtContent() );
		entity.setAssigneeId(inputBean.getAssigneeId());
		entity.setAssigneeNm(inputBean.getAssigneeNm());
		entity.setLotId						( inputBean.getLotNo() );
		entity.setUpdTimestamp				( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm					( userName );
		entity.setUpdUserId					( userId );
		entity.setStsId						( AmPjtStatusId.ChangePropertyInProgress.getStatusId() );
		entity.setDelStsId					( StatusFlg.off );
	}

}
