package jp.co.blueship.tri.agent.bm.svc.flow.beans.dto;

import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.fw.agent.dto.ITaskBean;

/**
 * 業務シーケンス側で判断できない情報／レポート情報をダイレクトするための
 * メッセージインタフェースです。
 *
 */
public interface IBuildTaskBean extends ITaskBean {

	

	/**
	 * プロセスIDを取得します。
	 *
	 * @return プロセスID
	 */
	public String getProcId();

	/**
	 * ロット番号を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getLotNo();
	/**
	 * ビルド番号を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getBuildNo();

	/**
	 * 登録／更新者を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	@Override
	public String getInsertUpdateUser();

	/**
	 * 登録／更新者ＩＤを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	@Override
	public String getInsertUpdateUserId();

	/**
	 * 業務シーケンスに設定する、システム環境変数
	 *
	 * @return 取得した値を戻します。
	 */
	public List<ITaskPropertyEntity> getPropertys();

	/**
	 * 業務シーケンスに設定する、システム環境変数
	 *
	 * @param props システム環境変数
	 */
	public void setPropertys( List<ITaskPropertyEntity> props );

	/**
	 * タスクシーケンス実行中におけるエラー発生時の強制実行モード
	 * @return
	 */
	public boolean isForceExecuteMode();

	/**
	 * タスクシーケンス実行中におけるエラー発生時の強制実行モード
	 * @param forceExecuteMode 強制実行モード
	 */
	public void setForceExecuteMode( boolean forceExecuteMode ) ;

}
