package jp.co.blueship.tri.agent.bm.svc.flow;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.agent.cmn.utils.TaskServiceUtils;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowDao;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowResDao;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowCondition;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResCondition;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.ITaskFlowProcDao;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;

public class BuildTaskService implements IBuildTaskService {

	private static final ILog log = TriLogFactory.getInstance();

	private static final String SEP = SystemProps.LineSeparator.getProperty();

	private List<ITaskProc> taskProc = new ArrayList<ITaskProc>();
	private ITaskFlowDao taskFlowDao = null;
	private ITaskFlowResDao taskFlowResDao = null;
	private ITaskFlowProcDao taskFlowProcDao = null;

	public void setTaskFlowDao( ITaskFlowDao taskFlowDao ) {
		this.taskFlowDao = taskFlowDao;
	}

	public void setTaskFlowResDao( ITaskFlowResDao taskFlowResDao ) {
		this.taskFlowResDao = taskFlowResDao;
	}

	public void setTaskProc( List<ITaskProc> taskProc ) {
		this.taskProc = taskProc;
	}

	public void setTaskFlowProcDao( ITaskFlowProcDao buildProcessDao ) {
		this.taskFlowProcDao = buildProcessDao;
	}

	/**
     * 業務シーケンスをタイムライン単位に実行します。
     * <br>同一タイムラインのシステムに対し、順に起動指示を行います。
     *
     * @param request 依頼するタイムライン
     * @param bean 業務シーケンスで使用するメッセージ
     *
     * @throws Exception
     */

	public void execute( IBldTimelineEntity request, IBuildTaskBean bean ) throws Exception {

		if ( null == request || null == bean ) {
			return;
		}
		IBldTimelineAgentEntity[] ILineEntityArray = request.getLine() ;
		if( null == ILineEntityArray ) {
			return;
		}

		try {
			for( IBldTimelineAgentEntity iLineEntity : ILineEntityArray ) {
				execute( request , bean , iLineEntity ) ;
			}
		} catch( Exception e ) {
			throw e;
		}
    }
	/**
     * 業務シーケンスをタイムライン中のシステム単位に実行します。<br>
     * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 * @param request タイムライン全体
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 依頼するライン（システム）
	 * @throws Exception
	 */

	public void execute( IBldTimelineEntity request, IBuildTaskBean bean , IBldTimelineAgentEntity iLineEntity ) throws Exception {

		if ( null == request || null == bean ) {
			return;
		}
		if( null == iLineEntity ) {
			return;
		}
		LogHandler.info( log ,  TriLogMessage.LSM0001 , bean.getBuildNo() , bean.getInsertUpdateUser() ) ;

		ITaskFlowEntity taskFlowEntity = null ;
		try {
			LogHandler.info( log , TriLogMessage.LSM0002 , iLineEntity.getBldSrvId() , " " ,
					iLineEntity.getTaskFlowId() , iLineEntity.getTargetSeqNo().toString() ) ;
			if( null == iLineEntity.getTargetSeqNo() ) {
				return;
			}

			//一致するワークフローＮｏを探す
			taskFlowEntity = null;

			{
				TaskFlowCondition condition = new TaskFlowCondition();
				condition.setTaskFlowId( iLineEntity.getTaskFlowId() );
				taskFlowEntity = taskFlowDao.findByPrimaryKey( condition.getCondition() );
			}

			if( null == taskFlowEntity ) {
				throw new TriSystemException ( SmMessageId.SM004107F , iLineEntity.getTaskFlowId());
			}

			ITaskTargetEntity targetEntity = this.getTaskTargetEntity( iLineEntity.getTargetSeqNo(), taskFlowEntity.getTask() );
			if( null == targetEntity ) {
				throw new TriSystemException ( SmMessageId.SM004108F , iLineEntity.getTargetSeqNo().toString() );
			}

			this.executeTask( request, bean, iLineEntity, taskFlowEntity.getTask(), targetEntity );
		} catch( Exception e ) {
			throw e;
		}
    }
	/**
	 * 業務シーケンス単位にタスクを実行します。
	 * <br>
	 *
	 * @param request 依頼するタイムライン
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 該当タイムライン
	 * @param taskEntity ワークフロー
	 * @param targetEntity 業務シーケンス
	 * @throws Exception
	 */

	private final void executeTask( 	IBldTimelineEntity request,
										IBuildTaskBean bean,
										IBldTimelineAgentEntity iLineEntity,
										ITaskEntity taskEntity,
										ITaskTargetEntity targetEntity ) throws Exception {
		boolean isError = true;

		ITaskFlowProcEntity procEntity = new TaskFlowProcEntity();

		ITaskFlowResEntity taskFlowResEntity = new TaskFlowResEntity();
		taskFlowResEntity.setDataId( bean.getBuildNo() );
		taskFlowResEntity.setTaskFlowId( iLineEntity.getTaskFlowId() );
		taskFlowResEntity.setLotId( bean.getLotNo() );

		try {
			procEntity.setProcId			( bean.getProcId() );
//			procEntity.setLotId				( bean.getLotNo() );
			procEntity.setBldEnvId			( request.getBldEnvId() );
			procEntity.setBldLineNo			( request.getBldLineNo() );
			procEntity.setBldSrvId			( iLineEntity.getBldSrvId() );
			procEntity.setTaskFlowId		( iLineEntity.getTaskFlowId() );
			procEntity.setTargetSeqNo		( iLineEntity.getTargetSeqNo() );
			procEntity.setMsg				( "" );
			procEntity.setMsgId				( "" );
			procEntity.setUpdUserNm			( bean.getInsertUpdateUser() );

			procEntity.setStsId				( BmBpStatusIdForExecData.CreatingBuildPackage.getStatusId() );

			procEntity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
			procEntity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );

			taskFlowProcDao.insert( procEntity );

			if( false == bean.isForceExecuteMode() ||
				StatusFlg.on.value().equals( targetEntity.getForceExecute() ) ) {//強制実行チェック

				for( ITaskTaskTypeEntity entity: targetEntity.getTask() ) {
					LogHandler.info( log , TriLogMessage.LSM0003 , entity.getSequence() );
					this.taskDispatch( bean, targetEntity, taskEntity, entity );
				}
			}

			isError = false;

		} finally {
			taskFlowResEntity.setTask( taskEntity ) ;

			List<ITaskPropertyEntity> builtinProperties = bean.getPropertys();
			TaskServiceUtils.addAllTaskProperties(builtinProperties, taskEntity);

			TaskFlowResCondition condition = new TaskFlowResCondition();
			condition.setDataId( bean.getBuildNo() );
			condition.setTaskFlowId( iLineEntity.getTaskFlowId() );
			ITaskFlowResEntity curBpTaskRecEntity = taskFlowResDao.findByPrimaryKey( condition.getCondition() ) ;

			if( null != curBpTaskRecEntity ) {

				//既存Resultレコードを複写
				ITaskTargetEntity[] targetEntityArray = curBpTaskRecEntity.getTask().getTarget() ;
				for( ITaskTargetEntity curTargetEntity : targetEntityArray ) {

					if( null == targetEntity.getSequenceNo())
						continue;

					// 今処理したTask結果のresultはtaskEntityとして既に入っているので飛ばす
					if( targetEntity.getSequenceNo().equals( curTargetEntity.getSequenceNo() ) )
						continue;

					// 今処理したTask結果以外のresultを前回のから持ってくる
					ITaskTargetEntity targetEntityPast = this.getTaskTargetEntity( curTargetEntity.getSequenceNo() , taskEntity );

					targetEntityPast.setTask( curTargetEntity.getTask() ) ;
					for( ITaskResultTypeEntity resultTypeEntity : curTargetEntity.getTaskResult() ) {

						targetEntityPast.addTaskResult( resultTypeEntity ) ;

					}
				}

				taskFlowResEntity.setUpdUserId( bean.getInsertUpdateUserId() );
				taskFlowResEntity.setUpdUserNm( bean.getInsertUpdateUser() );

				taskFlowResDao.update( taskFlowResEntity ) ;
			} else {
				taskFlowResEntity.setRegUserId( bean.getInsertUpdateUserId() );
				taskFlowResEntity.setRegUserNm( bean.getInsertUpdateUser() );

				taskFlowResDao.insert( taskFlowResEntity ) ;
			}

			procEntity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );

			if ( isError ) {
				procEntity.setStsId		( BmBpStatusIdForExecData.BuildPackageError.getStatusId() );
			} else {
				procEntity.setStsId		( BmBpStatusId.BuildPackageCreated.getStatusId() );
			}

			taskFlowProcDao.update( procEntity );
		}
	}

	/**
	 * 指定されたシーケンス番号から、該当タスクを確定して取得します。
	 *
	 * @param SequenceNo 業務シーケンス番号
	 * @param taskEntity ワークフロー
	 * @return 取得したターゲットタスクを戻します。
	 */
	private final ITaskTargetEntity getTaskTargetEntity( Integer SequenceNo, ITaskEntity taskEntity ) throws Exception {

		ITaskTargetEntity targetEntity = null ;
		ITaskTargetEntity[] targetEntityArray = taskEntity.getTarget() ;
		if( null != targetEntityArray ) {
			for( ITaskTargetEntity target : targetEntityArray ) {
				if( SequenceNo.equals( target.getSequenceNo() ) ) {
					if( null == targetEntity ) {
						targetEntity = target ;
					} else {
						throw new TriSystemException ( SmMessageId.SM004109F , targetEntity.getSequenceNo().toString() );
					}
				}
			}
		}

		return targetEntity ;
	}

	/**
	 * タスク種別を判定し、それぞれの動作に振り分けを行う<br>
	 *
	 * @param bean メッセージ
	 * @param taskTargetEntity ターゲットエンティティ
	 * @param taskEntity ワークフロー
	 * @param task タスクエンティティ
	 * @throws Exception
	 */

	private final void taskDispatch(  IBuildTaskBean bean,
										ITaskTargetEntity targetEntity,
										ITaskEntity taskEntity,
										ITaskTaskTypeEntity task ) throws Exception {
		List<ITaskPropertyEntity> props = new ArrayList<ITaskPropertyEntity>();
		props.addAll( FluentList.from(taskEntity.getProperty()).asList() );
		if( null != bean.getPropertys() ) {
			props.addAll( bean.getPropertys() );
		}
		ITaskPropertyEntity[] propertys = props.toArray( new ITaskPropertyEntity[0] );

		for ( ITaskProc proc : taskProc ) {
			proc.setProperty( propertys );
			proc.execute( targetEntity, task );
		}
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {

		List<ITaskFlowProcEntity> foundList = taskFlowProcDao.find(conditionOf(timeLine, param, line).getCondition());

		if (TriCollectionUtils.isEmpty(foundList)) {
			taskFlowProcDao.insert(createTaskFlowProcEntity(timeLine, param, line, e));
		} else {
			taskFlowProcDao.update(createTaskFlowProcEntity(timeLine, param, line, e));
		}
	}

	private IJdbcCondition conditionOf(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line) {

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(param.getProcId());
		condition.setBldSrvId(deriveBldSrvId(line));
		condition.setBldEnvId(timeLine.getBldEnvId());
		condition.setBldLineNo(timeLine.getBldLineNo());
		condition.setTaskFlowId(deriveTaskFlowId(line));
		condition.setTargetSeqNo(deriveTargetSeqNo(line));

		return condition;
	}

	private ITaskFlowProcEntity createTaskFlowProcEntity(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line,
			Throwable e) {

		BuildProcessEntityBuilder builder = new BuildProcessEntityBuilder(timeLine, line);
		builder.setMessage(messageOf(e) + SEP + ExceptionUtils.getStackTraceString(e));
		builder.setProcStatusId(BmBpStatusIdForExecData.BuildPackageError.getStatusId());
		builder.setProcId(param.getProcId());

		return builder.getEntity();
	}

	private static String deriveBldSrvId(IBldTimelineAgentEntity line) {

		if (line == null) {
			return "";
		}

		return line.getBldSrvId();
	}

	private static String deriveTaskFlowId(IBldTimelineAgentEntity line) {

		if (line == null) {
			return "";
		}

		return line.getTaskFlowId();
	}

	private static Integer deriveTargetSeqNo(IBldTimelineAgentEntity line) {

		if (line == null) {
			return 0;
		}

		return line.getTargetSeqNo();
	}

	private String messageOf(Throwable e) {
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		if(ITranslatable.class.isInstance(e)) {
			return ac.getMessage(((ITranslatable)e).getMessageID(), ((ITranslatable)e).getMessageArgs());
		}

		return e.getMessage();
	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {
		LogHandler.debug( log , "agentとの疎通チェックを実行 " + this.getClass().getName() ) ;

		try {
			TaskFlowCondition condition = new TaskFlowCondition();
			condition.setTaskFlowId("dummy");
			taskFlowDao.count( condition.getCondition() ) ;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
		}

		return null ;
	}

	@Override
	public List<Object> executeLinkCheckByService( List<Object> paramList ) throws Exception {
		LogHandler.debug( log , "agentとの疎通チェックを実行 " + this.getClass().getName() ) ;

		try {
			TaskFlowCondition condition = new TaskFlowCondition();
			condition.setTaskFlowId("dummy");
			taskFlowDao.count( condition.getCondition() ) ;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
		}

		IRmiSvcDto remoteService = AgentExtractEntityAddonUtil.extractRemoteService( paramList ) ;

		AgentStatus status = new AgentStatus() ;
		status.setRemoteServiceEntity( remoteService ) ;
		status.setStatus( true ) ;

		paramList.add( status ) ;

		return paramList ;
	}
}
