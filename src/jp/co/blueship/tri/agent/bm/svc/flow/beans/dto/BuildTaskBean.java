package jp.co.blueship.tri.agent.bm.svc.flow.beans.dto;

import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;

public class BuildTaskBean implements IBuildTaskBean {

	

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String procId;
	private String lotId = null;
	private String buildNo = null;
	private String insertUpdateUser = null;
	private String insertUpdateUserId = null;
	private List<ITaskPropertyEntity> propertys = null;
	private boolean forceExecuteMode = false;

	@Override
	public String getProcId() {
		return procId;
	}

	public void setProcId(String procId) {
		this.procId = procId;
	}

	@Override
	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	@Override
	public String getBuildNo() {
		return buildNo;
	}

	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	@Override
	public String getInsertUpdateUser() {
		return insertUpdateUser;
	}

	public void setInsertUpdateUser(String insertUpdateUser) {
		this.insertUpdateUser = insertUpdateUser;
	}

	@Override
	public List<ITaskPropertyEntity> getPropertys() {
		return propertys;
	}

	@Override
	public void setPropertys(List<ITaskPropertyEntity> propertys) {
		this.propertys = propertys;
	}

	@Override
	public boolean isForceExecuteMode() {
		return forceExecuteMode;
	}

	@Override
	public void setForceExecuteMode(boolean forceExecuteMode) {
		this.forceExecuteMode = forceExecuteMode;
	}

	@Override
	public String getInsertUpdateUserId() {
		return insertUpdateUserId;
	}

	public void setInsertUpdateUserId(String insertUpdateUserId) {
		this.insertUpdateUserId = insertUpdateUserId;
	}

}
