package jp.co.blueship.tri.agent.bm.svc.flow;

import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.concurrent.ASyncTask;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;

/**
 * ビルド業務シーケンスを非同期実行コントローラによるスレッド配下で実行する際に業務シーケンスをラッピングするための<br/>
 * クラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class ASyncBuildTask extends ASyncTask<IBldTimelineEntity, IBuildTaskBean, IBldTimelineAgentEntity> {

	/**
	 * ビルドの業務シーケンスをタイムライン単位に実行します。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	@Override
	protected void execute(IBldTimelineEntity timeLine, IBuildTaskBean param) throws Exception {
		buildTaskService().execute(timeLine, param);
	}

	/**
	 * ビルドの業務シーケンスをタイムライン単位に実行します。<br/>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	@Override
	protected void execute(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line) throws Exception {
		buildTaskService().execute(timeLine, param, line);
	}

	/**
	 * ビルドの業務シーケンスのプロセステーブルにエラー発生情報を書き込みます。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 * @param e 例外情報
	 */
	@Override
	protected void writeProcessByIrregular(IBldTimelineEntity timeLine, IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		buildTaskService().writeProcessByIrregular(timeLine, param, line, e);
	}

	private IBuildTaskService buildTaskService() {

		PreConditions.assertOf(IBuildTaskService.class.isInstance(service()), "IBuildTaskService is not injected.");

		return (IBuildTaskService) service();
	}

}
