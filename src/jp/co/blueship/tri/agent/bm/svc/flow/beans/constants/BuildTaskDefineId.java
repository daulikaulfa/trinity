package jp.co.blueship.tri.agent.bm.svc.flow.beans.constants;


/**
 * ビルド・タスクで使用する内部環境変数の列挙型です。
 * <p>
 *
 * @author Yukihiro Eguchi
 *
 */
public enum BuildTaskDefineId {

	applyNo					( "{@applyNo}" ),					//資産申請番号
	buildNo					( "{@buildNo}" ),					//ビルド申請番号
	module					( "{@module}" ),					//資産が変更／追加／削除されたモジュール
	delApplyNo				( "{@delApplyNo}" ),				//資産削除申請番号
	masterDeletePath		( "{@masterDeletePath}" ),			//原本削除する資産のパス
	masterBinaryDeletePath	( "{@masterBinaryDeletePath}" ),	//原本バイナリ削除する資産のパス
	buildTag				( "{@buildTag}" ),					//ビルド申請時の原本へのタグ
	lotNo					( "{@lotNo}" ),						//ビルド申請時のロット番号
	workPath				( "{@workPath}" ) ,					//ロットの原本ディレクトリ
	workspace				( "{@workspace}" ) ,				//ロットの作業ディレクトリ
	historyPath				( "{@historyPath}" ) ,				//ロットの履歴格納ディレクトリ
	systemInBox				( "{@systemInBox}" ) ,				//ロットの内部返却ディレクトリ

	homePath				( "{@homePath}" ) ,					//ホームディレクトリ
	historyRelUnitDSLPath	( "{@historyRelUnitDSLPath}" ) ;	//RP DSLパス

	private String value = null ;

	private BuildTaskDefineId( String value ) {
		this.value = value ;
	}

	public String value() {
		return this.value ;
	}

	public static BuildTaskDefineId value( String value ) {
		for ( BuildTaskDefineId charset : values() ) {
			if ( charset.value().equals( value ) ) {
				return charset ;
			}
		}

		return null;
	}
}
