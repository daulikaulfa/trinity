package jp.co.blueship.tri.agent.bm;

import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.fw.agent.service.rpc.RmiServiceExporterWrapper;
import jp.co.blueship.tri.fw.agent.service.rpc.RmiServiceUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RemoteService;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;

public class BmAgentService {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String[] AGENT_DI_CONTEXT_FILES = { "Fw-Module-Context.xml", "Rel-Agent-Context.xml" };

	private static final Object mutex = new Object();

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		Contexts.getInstance().initBeanFactory(AGENT_DI_CONTEXT_FILES);

		if (!validate(args)) {
			System.exit(9);
		}

		String serverId = args[0];
		RemoteService serviceId = toServiceId(args[1]);

		System.out.println(contextAdapter().getMessage(BmMessageId.BM003012I, serverId));

		try {
			RmiServiceExporterWrapper rmiServiceExporter = RmiServiceUtils.getRmiServiceExporter(serverId, serviceId);
			rmiServiceExporter.afterPropertiesSet();
		} catch (Exception e) {
			System.err.println(contextAdapter().getMessage(SmMessageId.SM005193S, serverId));
			LogHandler.fatal(log, e);
			System.exit(9);
		}

		synchronized (mutex) {
			while (true) {
				try {
					mutex.wait();
				} catch (InterruptedException e) {
					LogHandler.fatal(log, e);
					TriTaskExecutor executor = (TriTaskExecutor) contextAdapter().getBean(TriTaskExecutor.BEAN_NAME);
					executor.shutDown();
					System.exit(9);
				}
			}
		}

	}

	private static boolean validate(String[] args) {

		if (TriStringUtils.isEmpty(args) || 2 != args.length) {
			System.err.println("usage: BmAgentService {@serverId} {@serviceId}");
			return false;
		}

		if (TriStringUtils.isEmpty(toServiceId(args[1]))) {
			System.err.println(contextAdapter().getMessage(SmMessageId.SM004212F));
			return false;
		}

		return true;
	}

	private static RemoteService toServiceId(String serviceId) {
		return RemoteService.getValueById(serviceId);
	}

	private static IContextAdapter contextAdapter() {
		return ContextAdapterFactory.getContextAdapter();
	}
}
