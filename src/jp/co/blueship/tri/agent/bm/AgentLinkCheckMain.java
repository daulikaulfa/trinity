package jp.co.blueship.tri.agent.bm;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import jp.co.blueship.tri.agent.cmn.service.flow.beans.AgentLinkCheck;
import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Agebt疎通確認
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class AgentLinkCheckMain {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String[] DI_CONTEXT_FILES = {
		"Rel-AgentLinkCheck-Context.xml",
		"Fw-Module-Context.xml",
		};

	Logger logger = Logger.getAnonymousLogger();

	private static final String SEP = SystemProps.LineSeparator.getProperty();

	/**
	 * agentとの疎通チェックを行います。
	 * @param args
	 */
	public static void main(String[] args) {

		// SpringのxmlからRMIサービスを起動
		Contexts.getInstance().initBeanFactory( DI_CONTEXT_FILES ) ;

		//アクション
		List<Object> paramList = new ArrayList<Object>();

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.addAll( paramList );

		try {
			LogHandler.info( log , TriLogMessage.LSM0012 ) ;

			//資産agentの疎通チェック
			IContextAdapter context = ContextAdapterFactory.getContextAdapter() ;

			AgentLinkCheck agentLinkCheck = (AgentLinkCheck)context.getBean( "agentLinkCheck"  ) ;
			innerServiceDto = agentLinkCheck.execute( innerServiceDto ) ;

		} finally {
			//資産agentの疎通チェック結果出力
			outputStatusAgentAsset( innerServiceDto.getParamList() ) ;

			LogHandler.info( log , TriLogMessage.LSM0013 ) ;
		}
	}

	/**
	 *
	 * @param paramList
	 */

	private static void outputStatusAgentAsset( List<Object> paramList ) {

		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		//agent疎通チェック結果によるメッセージ出力
		AgentStatus[] agentStatusArray = AgentExtractEntityAddonUtil.extractAgentStatusArray( paramList ) ;
		if( null != agentStatusArray ) {
			StringBuilder stb = new StringBuilder() ;
			stb.append( SEP ) ;
			for( AgentStatus status : agentStatusArray ) {
				String statusStr = status.isStatus() ? ac.getMessage(TriLogMessage.LSM0014) : ac.getMessage(TriLogMessage.LSM0015) ;
				IRmiSvcDto remoteService = status.getRemoteServiceEntity() ;
				String[] messageArgs = new String[] {
						statusStr ,
						remoteService.getRmiSvcEntity().getBldSrvId() ,
						remoteService.getRmiSvcEntity().getRmiSvcId() ,
						remoteService.getRmiSvcEntity().getRmiHostNm() ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()) ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()) ,

				} ;
				stb.append( ac.getMessage( SmMessageId.SM001017E , messageArgs ) + SEP ) ;
			}

			LogHandler.info( log , stb.toString() ) ;
		} else {
			LogHandler.info( log , TriLogMessage.LSM0016 ) ;
		}
	}
}

