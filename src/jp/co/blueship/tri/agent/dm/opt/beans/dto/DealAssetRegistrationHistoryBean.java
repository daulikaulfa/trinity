package jp.co.blueship.tri.agent.dm.opt.beans.dto;

import java.io.Serializable;

/**
 * 
 * 統合監視 資源登録履歴用
 *
 */
public class DealAssetRegistrationHistoryBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String drmsManagementNo = null ;	//ＤＲＭＳ管理番号
	private String registrationDate = null ;	//資源登録日時(YYYYMMDDhhmmss)
	private String dealDate = null ;			//資源配付日時(YYYYMMDDhhmmss)
	private String status = null ;				//配付成功可否（成功/失敗）
	
	
	/**
	 * ＤＲＭＳ管理番号を取得する
	 * @return ＤＲＭＳ管理番号
	 */
	public String getDrmsManagementNo() {
		return drmsManagementNo;
	}
	/**
	 * ＤＲＭＳ管理番号をセットする
	 * @param drmsManagementNo ＤＲＭＳ管理番号
	 */
	public void setDrmsManagementNo(String drmsManagementNo) {
		this.drmsManagementNo = drmsManagementNo;
	}
	/**
	 * 登録日時(YYYYMMDDhhmmss)を取得する
	 * @return 登録日時(YYYYMMDDhhmmss)
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * 登録日時(YYYYMMDDhhmmss)をセットする
	 * @param registrationDate 登録日時(YYYYMMDDhhmmss)
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	/**
	 * 資源配付日時(YYYYMMDDhhmmss)を取得する
	 * @return 資源配付日時(YYYYMMDDhhmmss)
	 */
	public String getDealDate() {
		return dealDate;
	}
	/**
	 * 資源配付日時(YYYYMMDDhhmmss)をセットする
	 * @param dealDate 資源配付日時(YYYYMMDDhhmmss)
	 */
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	/**
	 * 配付成功可否（成功/失敗）を取得する
	 * @return 配付成功可否（成功/失敗）
	 */
	public String getStatus() {
		return status;
	}
	/**
	 * 配付成功可否（成功/失敗）をセットする
	 * @param status 配付成功可否（成功/失敗）
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	
	
}
