package jp.co.blueship.tri.agent.dm.opt.beans;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.agent.ShellExecute;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

public abstract class DealAssetScriptCallService implements IDealAssetScriptCallService {

	private static final ILog log = TriLogFactory.getInstance();
//	protected static final int LENGTH_DRMS_NO = 20 ;//ＤＲＭＳ管理番号のパラメータ長
	protected static final int LENGTH_DRMS_NO = 8 ;//ＤＲＭＳ管理番号のパラメータ長
	protected static final int LENGTH_DATETIME = 14 ;//時刻(YYYYMMDDhhmmss)

	private String shell = null ;		//シェルコマンドパス
	private String[] args = null ;		//引数パラメータの配列
	private String[] env = null ;		//環境変数の配列
	private String workDir = null ;	//作業ディレクトリパス

	/**
	 * 引数パラメータの配列をセットします<br>
	 * @param args 引数パラメータの配列
	 */
	public void setArgs(String[] args) {
		this.args = args;
	}
	/**
	 * 環境変数群をセットします<br>
	 * @param env 環境変数群
	 */
	public void setEnv(String[] env) {
		this.env = env;
	}
	/**
	 * 作業ディレクトリパスをセットします<br>
	 * @param workDir 作業ディレクトリパス
	 */
	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}
	/**
	 * シェルのパスをセットします<br>
	 * @param shell シェルのパス
	 */
	public void setShell(String shell) {
		this.shell = shell;
	}
	/**
	 * 作業ディレクトリパスをチェックします<br>
	 * @param bean
	 */
	public void checkWorkDir( DealAssetServiceBean bean ) {

		this.workDir = bean.getWorkDir() ;
	}
	/**
	 * 環境変数をチェックします<br>
	 * @param bean
	 */
	public void checkEnv( DealAssetServiceBean bean ) {

		this.env = bean.getEnv() ;
	}
	/**
	 * 引数パラメータをチェックします<br>
	 * @param bean
	 */
	public abstract void checkArgs( DealAssetServiceBean bean ) ;
	/**
	 * 処理結果ログをチェックします<br>
	 * @param bean
	 * @param shellLog
	 */
	public abstract void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) ;

	/**
	 * スクリプト実行処理本体<br>
	 * @param bean
	 * @return 処理結果
	 */
	public DealAssetServiceResponseBean execute( DealAssetServiceBean bean ) {

		try {
			//引数パラメータの妥当性チェック
			this.checkArgs( bean ) ;
			//作業ディレクトリパスの妥当性チェック
			this.checkWorkDir( bean ) ;
			//環境変数の妥当性チェック
			this.checkEnv( bean ) ;

			StringBuilder stb = new StringBuilder() ;
			if( null != args ) {
				for( String arg : args ) {
					if( 0 != stb.length() ) {
						stb.append( " " ) ;
					}
					stb.append( arg ) ;
				}
			}
			LogHandler.debug( log , "引数パラメータ： " + stb.toString() ) ;

			//シェルスクリプトの起動
			ShellLog shellLog = this.callScript() ;

			DealAssetServiceResponseBean retBean = new DealAssetServiceResponseBean() ;
			//処理結果ログのセット
			this.checkLog( retBean , shellLog ) ;

			return retBean ;

		} catch ( Exception e ) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005137S, e );
		}
	}

	/**
	 * スクリプトを呼び出し、処理実行します。<br>
	 */
	public ShellLog callScript() {

		ShellExecute shellExecute = new ShellExecute() ;
		shellExecute.setLog( TriLogFactory.getInstance() ) ;

		StringBuilder stb = new StringBuilder() ;
		if( null != args ) {
			for( String arg : args ) {
				if( 0 != stb.length() ) {
					stb.append( " " ) ;
				}
				stb.append( arg ) ;
			}
		}

		String command = this.shell + " " + stb.toString() ;//シェル名 ＋ 引数パラメータ群
		shellExecute.setExecShellPath( command ) ;
		LogHandler.debug( log , "処理スクリプト： " + command ) ;

		shellExecute.setEnv( this.env ) ;
		shellExecute.setWorkDir( this.workDir ) ;

		try {
			shellExecute.execute() ;
		} catch( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005138S , e , this.shell ) ;
		}

		ShellLog log = new ShellLog() ;
		log.setExitValue( shellExecute.getExitValue() ) ;
		log.setOutLog( shellExecute.getOutLog() ) ;
		log.setErrLog( shellExecute.getErrLog() ) ;

		return log ;
	}
}

