package jp.co.blueship.tri.agent.dm.opt.beans;

import java.util.List;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetFtpBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.rm.support.RmFinderSupport;

/**
 * FlowRelDistributeEntry等の6zカスタマイズ統合監視系のイベントのサポートClass
 * <br>
 * <p>
 * 統合監視 配布資源登録等のための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 */
public class FlowDealAssetEditSupport extends RmFinderSupport {

//	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * パラメータリスト中から、資産配付Beanを取得して返します。
	 * @param paramList パラメータリスト
	 * @return 資産配付Bean
	 */
	public DealAssetServiceBean getDealAssetServiceBean( List<Object> paramList ) {
		for( Object obj : paramList ) {
			if( obj instanceof DealAssetServiceBean ) {
				return (DealAssetServiceBean)obj ;
			}
		}
		return null ;
	}

	/**
	 * パラメータリスト中から、ftp転送Beanを取得して返します。
	 * @param paramList パラメータリスト
	 * @return ftp転送Bean
	 */
	public DealAssetFtpBean getDealAssetFtpBean( List<Object> paramList ) {
		for( Object obj : paramList ) {
			if( obj instanceof DealAssetFtpBean ) {
				return (DealAssetFtpBean)obj ;
			}
		}
		return null ;
	}
}
