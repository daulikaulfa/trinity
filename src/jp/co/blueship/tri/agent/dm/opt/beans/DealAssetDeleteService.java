package jp.co.blueship.tri.agent.dm.opt.beans;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 統合監視 配付資源登録 「配付資源削除」の情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class DealAssetDeleteService extends DealAssetScriptCallService {

	/**
	 *
	 */
	public void checkArgs( DealAssetServiceBean bean ) {

		//対象環境
		if( TriStringUtils.isEmpty( bean.getTargetEnvironment() ) ) {
			throw new TriSystemException( SmMessageId.SM004127F ) ;
		}
		//ＤＲＭＳ管理番号
		if( TriStringUtils.isEmpty( bean.getDrmsManagementNo() ) ) {
			throw new TriSystemException( SmMessageId.SM004128F ) ;
		}
		if( LENGTH_DRMS_NO != bean.getDrmsManagementNo().length() ) {
			throw new TriSystemException( SmMessageId.SM004129F , String.valueOf(LENGTH_DRMS_NO) , bean.getDrmsManagementNo() ) ;
		}

		this.setArgs( new String[]{ 	bean.getTargetEnvironment() ,
										bean.getDrmsManagementNo() } ) ;
	}

	/**
	 *
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) {

		bean.setReturnCode( shellLog.getExitValue() ) ;
		bean.setOutLog( shellLog.getOutLog() ) ;
		bean.setErrLog( shellLog.getErrLog() ) ;

	}

}
