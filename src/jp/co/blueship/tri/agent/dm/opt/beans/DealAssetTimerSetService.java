package jp.co.blueship.tri.agent.dm.opt.beans;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 統合監視 配付資源登録 「タイマー設定」の情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class DealAssetTimerSetService extends DealAssetScriptCallService {

	/**
	 *
	 */
	public void checkArgs( DealAssetServiceBean bean ) {

		//対象環境
		if( TriStringUtils.isEmpty( bean.getTargetEnvironment() ) ) {
			throw new TriSystemException( SmMessageId.SM004127F ) ;
		}
		//ＤＲＭＳ管理番号
		if( TriStringUtils.isEmpty( bean.getDrmsManagementNo() ) ) {
			throw new TriSystemException( SmMessageId.SM004128F ) ;
		}
		if( LENGTH_DRMS_NO != bean.getDrmsManagementNo().length() ) {
			throw new TriSystemException( SmMessageId.SM004129F , String.valueOf(LENGTH_DRMS_NO) , bean.getDrmsManagementNo() ) ;
		}
		//配付時刻
		if( TriStringUtils.isEmpty( bean.getTimerDate() ) ) {
			throw new TriSystemException( SmMessageId.SM004131F ) ;
		}
		if( LENGTH_DATETIME != bean.getTimerDate().length() ) {
			throw new TriSystemException( SmMessageId.SM004146F , String.valueOf(LENGTH_DATETIME) , bean.getTimerDate() ) ;
		}
		//新規／変更フラグ
		if( TriStringUtils.isEmpty( bean.getNewOrUpdateFlg() ) ) {
			throw new TriSystemException( SmMessageId.SM004147F ) ;
		}
		if( true != "0".equals( bean.getNewOrUpdateFlg() ) &&
			true != "1".equals( bean.getNewOrUpdateFlg() ) ) {
			throw new TriSystemException( SmMessageId.SM004148F , bean.getNewOrUpdateFlg() ) ;
		}

		this.setArgs( new String[]{ 	bean.getTargetEnvironment() ,
										bean.getDrmsManagementNo() ,
										bean.getTimerDate() ,
										bean.getNewOrUpdateFlg() } ) ;
	}

	/**
	 *
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) {

		bean.setReturnCode( shellLog.getExitValue() ) ;
		bean.setOutLog( shellLog.getOutLog() ) ;
		bean.setErrLog( shellLog.getErrLog() ) ;

	}
}
