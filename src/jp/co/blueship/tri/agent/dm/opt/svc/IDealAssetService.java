package jp.co.blueship.tri.agent.dm.opt.svc;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;

/**
 * ＲＭＩサービス／クライアント双方で利用するインターフェイス
 *
 */
public interface IDealAssetService {

	/**
	 * ＲＭＩによる資源配付の各種処理を実行する
	 * @param action アクション名
	 * @param bean パラメータオブジェクト
	 * @return 処理結果
	 */
	public DealAssetServiceResponseBean execute( String action , DealAssetServiceBean bean ) ;
	
	
}
