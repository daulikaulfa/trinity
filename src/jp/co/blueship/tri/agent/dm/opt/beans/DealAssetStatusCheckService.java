package jp.co.blueship.tri.agent.dm.opt.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetStatusBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 統合監視 配付資源登録 「配付状況確認」の情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class DealAssetStatusCheckService extends DealAssetScriptCallService {

	/**
	 *
	 */
	public void checkArgs( DealAssetServiceBean bean ) {

//		if( StringAddonUtil.isNothing( bean.getTargetEnvironment() ) ) {
//			throw new SystemException( "運用上の誤り： 「対象環境」が設定されていません" ) ;
//		}
//
//		this.setArgs( new String[]{ bean.getTargetEnvironment() } ) ;
	}

	/**
	 * @param bean
	 * @param shellLog
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) {

		bean.setReturnCode( shellLog.getExitValue() ) ;
		bean.setOutLog( shellLog.getOutLog() ) ;
		bean.setErrLog( shellLog.getErrLog() ) ;

		if( DealAssetCallScriptReturnCode.SUCESS.getReturnCode().equals( bean.getReturnCode() ) ) {
			//正常終了
			this.makeRecords( bean ) ;

		} else if( DealAssetCallScriptReturnCode.NORMAL.getReturnCode().equals( bean.getReturnCode() ) ) {
			//正常終了（業務例外）
			bean.setDealStatusList( new ArrayList<DealAssetStatusBean>() ) ;
		} else {
			//異常終了（システム例外）
			bean.setDealStatusList( null ) ;
		}
	}

	/**
	 * 配布履歴確認スクリプト実行後の出力メッセージを編集し、DealAssetStatusBeanのリストにする<br>
	 * @param resBean 
	 */
	private void makeRecords( DealAssetServiceResponseBean resBean ) {

		final int ENTRY_SIZE = 4 ;

		List<DealAssetStatusBean> dealStatusList = new ArrayList<DealAssetStatusBean>() ;

		String[] entryArray = resBean.getOutLog().split( ":" ) ;
		for( String entry : entryArray ) {
			String[] attributeArray = entry.split( "," ) ;
			if( ENTRY_SIZE != attributeArray.length ) {
				throw new TriSystemException( SmMessageId.SM004145F , resBean.getOutLog() );
			}
			DealAssetStatusBean bean = new DealAssetStatusBean() ;

			bean.setTargetEnvironment( attributeArray[ 0 ].trim() ) ;
			bean.setDrmsManagementNo( attributeArray[ 1 ].trim() ) ;
			bean.setRegistrationDate( attributeArray[ 2 ].trim() ) ;
			bean.setDealDate( attributeArray[ 3 ].trim() ) ;

			dealStatusList.add( bean ) ;
		}
		resBean.setDealStatusList( dealStatusList ) ;
	}
}
