package jp.co.blueship.tri.agent.dm.opt.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetRegistrationHistoryBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 統合監視 配付資源登録 「特定環境履歴情報取得」の情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class DealAssetRegistrationHistoryCheckService extends DealAssetScriptCallService {

	/**
	 *
	 */
	public void checkArgs( DealAssetServiceBean bean ) {

		//対象環境
		if( TriStringUtils.isEmpty( bean.getTargetEnvironment() ) ) {
			throw new TriSystemException( SmMessageId.SM004127F ) ;
		}

		this.setArgs( new String[]{ bean.getTargetEnvironment() } ) ;
	}

	/**
	 * @param bean
	 * @param shellLog
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) {

		bean.setReturnCode( shellLog.getExitValue() ) ;
		bean.setOutLog( shellLog.getOutLog() ) ;
		bean.setErrLog( shellLog.getErrLog() ) ;

		if( DealAssetCallScriptReturnCode.SUCESS.getReturnCode().equals( bean.getReturnCode() ) ) {
			//正常終了
			this.makeRecords( bean ) ;

		} else if( DealAssetCallScriptReturnCode.NORMAL.getReturnCode().equals( bean.getReturnCode() ) ) {
			//正常終了（業務例外）
			bean.setRegistrationHistoryList( new ArrayList<DealAssetRegistrationHistoryBean>() ) ;
		} else {
			//異常終了（システム例外）
			bean.setRegistrationHistoryList( null ) ;
		}
	}

	/**
	 * 配布資源登録履歴スクリプト実行後の出力メッセージを編集し、DealAssetServiceResponseBeanのリストにする<br>
	 * @param resBean 
	 */
	private void makeRecords( DealAssetServiceResponseBean resBean ) {

		final int ENTRY_SIZE_MIN = 2 ;
		final int ENTRY_SIZE_MAX = 4 ;

		List<DealAssetRegistrationHistoryBean> registrationHistoryList = new ArrayList<DealAssetRegistrationHistoryBean>() ;

		String[] entryArray = resBean.getOutLog().split( ":" ) ;
		for( String entry : entryArray ) {
			String[] attributeArray = entry.trim().split( "," ) ;
			if( true != ( ENTRY_SIZE_MIN <= attributeArray.length && attributeArray.length <= ENTRY_SIZE_MAX ) ) {
				throw new TriSystemException( SmMessageId.SM004131F , resBean.getOutLog() ) ;
			}
			DealAssetRegistrationHistoryBean bean = new DealAssetRegistrationHistoryBean() ;

			bean.setDrmsManagementNo( attributeArray[ 0 ].trim() ) ;
			bean.setRegistrationDate( attributeArray[ 1 ].trim() ) ;
//			配付後のみ取得できる。配付前の場合は戻ってこない
			bean.setDealDate( ( 2 < attributeArray.length ) ? attributeArray[ 2 ].trim() : null ) ;
			bean.setStatus( ( 3 < attributeArray.length ) ? attributeArray[ 3 ].trim() : null ) ;

			registrationHistoryList.add( bean ) ;
		}
		resBean.setRegistrationHistoryList( registrationHistoryList ) ;
	}
}
