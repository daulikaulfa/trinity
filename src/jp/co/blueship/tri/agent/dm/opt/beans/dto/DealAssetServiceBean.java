package jp.co.blueship.tri.agent.dm.opt.beans.dto;

import java.io.Serializable;

/**
 * 統合監視 資源配付プロセス処理用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class DealAssetServiceBean implements Serializable {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String[] env = null ;				//環境変数群
	private String workDir = null ;			//作業ディレクトリパス
	
	private String targetEnvironment = null ;	//対象環境
	private String drmsManagementNo = null ;	//ＤＲＭＳ管理番号
	private String path = null ;				//資産登録先パス
	private String name = null ;				//資産登録ファイル名
	private String timerDate = null ;			//配付時刻(YYYYMMDDhhmmss)
	private String newOrUpdateFlg = null ;		//新規／変更フラグ
	
	private DealAssetFtpBean ftpBean = null;	//資源管理サーバFTP転送設定
	
	/**
	 * 対象環境を取得します
	 * @return 対象環境
	 */
	public String getTargetEnvironment() {
		return targetEnvironment;
	}
	/**
	 * 対象環境をセットします
	 * @param targetEnvironment 対象環境
	 */
	public void setTargetEnvironment(String targetEnvironment) {
		this.targetEnvironment = targetEnvironment;
	}
	/**
	 * ＤＲＭＳ管理番号を取得します
	 * @return ＤＲＭＳ管理番号
	 */
	public String getDrmsManagementNo() {
		return drmsManagementNo;
	}
	/**
	 * ＤＲＭＳ管理番号をセットします
	 * @param drmsManagementNo ＤＲＭＳ管理番号
	 */
	public void setDrmsManagementNo(String drmsManagementNo) {
		this.drmsManagementNo = drmsManagementNo;
	}
	/**
	 * 資産登録先パスを取得します
	 * @return 資産登録先パス
	 */
	public String getPath() {
		return path;
	}
	/**
	 * 資産登録先パスをセットします
	 * @param path 資産登録先パス
	 */
	public void setPath(String path) {
		this.path = path;
	}
	/**
	 * 資産登録先ファイル名を取得します
	 * @return 資産登録先ファイル名
	 */
	public String getName() {
		return name;
	}
	/**
	 * 資産登録先ファイル名をセットします
	 * @param name 資産登録先ファイル名
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 配付時刻(YYYYMMDDhhmmss)を取得します
	 * @return 配付時刻(YYYYMMDDhhmmss)
	 */
	public String getTimerDate() {
		return timerDate;
	}
	/**
	 * 配付時刻(YYYYMMDDhhmmss)をセットします
	 * @param timerDate 配付時刻(YYYYMMDDhhmmss)
	 */
	public void setTimerDate(String timerDate) {
		this.timerDate = timerDate;
	}
	/**
	 * 環境変数群を取得します
	 * @return 環境変数群
	 */
	public String[] getEnv() {
		return env;
	}
	/**
	 * 環境変数群をセットします
	 * @param env 環境変数群
	 */
	public void setEnv(String[] env) {
		this.env = env;
	}
	/**
	 * 作業ディレクトリパスを取得します
	 * @return 作業ディレクトリパス
	 */
	public String getWorkDir() {
		return workDir;
	}
	/**
	 * 作業ディレクトリパスをセットします
	 * @param workDir 作業ディレクトリパス
	 */
	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}
	/**
	 * 新規／変更フラグを取得します<br>
	 * @return 新規／変更フラグ
	 */
	public String getNewOrUpdateFlg() {
		return newOrUpdateFlg;
	}
	/**
	 * 新規／変更フラグをセットします
	 * @param newOrUpdateFlg 新規／変更フラグ
	 */
	public void setNewOrUpdateFlg(String newOrUpdateFlg) {
		this.newOrUpdateFlg = newOrUpdateFlg;
	}
	/**
	 * 資源管理サーバFTP転送設定を取得します
	 * 
	 * @return 資源管理サーバFTP転送設定
	 */
	public DealAssetFtpBean getFtpBean() {
		return ftpBean;
	}
	/**
	 * 資源管理サーバFTP転送設定をセットします
	 * 
	 * @param ftpBean 資源管理サーバFTP転送設定
	 */
	public void setFtpBean(DealAssetFtpBean ftpBean) {
		this.ftpBean = ftpBean;
	}
	
	
}
