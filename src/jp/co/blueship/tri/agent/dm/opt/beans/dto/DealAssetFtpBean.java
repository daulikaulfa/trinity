package jp.co.blueship.tri.agent.dm.opt.beans.dto;

import java.io.Serializable;

/**
 * 
 * 統合監視 資源登録 ＦＴＰ転送用
 *
 */
public class DealAssetFtpBean implements Serializable {
	
	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String srcPath = null ;			//転送元パス
	
	private String ftpServer = null ;			//ＦＴＰサーバ有効
	private String host = null ;				//ＦＴＰサーバホスト
	private String port = null ;				//ＦＴＰサーバポート番号
	private String user = null ;				//ＦＴＰアカウント
	private String pasv = null ;				//ＰＡＳＶモード設定
	private String controlEncoding = null ;	//文字コードエンコーディング
	private String srcFtpPath = null ;			//転送元FTPパス
	private String srcLocalPath = null ;		//転送元ローカルパス
	private String destLocalPath = null ;		//転送先ローカルパス
	private String srcExtension = null ;		//転送元ファイルの拡張子
	
	/**
	 * 転送元パスを取得します
	 * @return 転送元パス
	 */
	public String getSrcPath() {
		return srcPath;
	}
	/**
	 * 転送元パスをセットします
	 * @param srcPath 転送元パス
	 */
	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}
	/**
	 * 文字コードエンコーディングを取得します
	 * @return 
	 */
	public String getControlEncoding() {
		return controlEncoding;
	}
	/**
	 * 文字コードエンコーディングをセットします
	 * @param srcPath
	 */
	public void setControlEncoding(String controlEncoding) {
		this.controlEncoding = controlEncoding;
	}
	/**
	 * 転送先ローカルパスを取得します
	 * @return 
	 */
	public String getDestLocalPath() {
		return destLocalPath;
	}
	/**
	 * 転送先ローカルパスをセットします
	 * @param srcPath
	 */
	public void setDestLocalPath(String destLocalPath) {
		this.destLocalPath = destLocalPath;
	}
	/**
	 * ＦＴＰサーバ有効を取得します
	 * @return 
	 */
	public String getFtpServer() {
		return ftpServer;
	}
	/**
	 * ＦＴＰサーバ有効をセットします
	 * @param srcPath
	 */
	public void setFtpServer(String ftpServer) {
		this.ftpServer = ftpServer;
	}
	/**
	 * ＦＴＰサーバホストを取得します
	 * @return 
	 */
	public String getHost() {
		return host;
	}
	/**
	 * ＦＴＰサーバホストをセットします
	 * @param srcPath
	 */
	public void setHost(String host) {
		this.host = host;
	}
	/**
	 * ＰＡＳＶモード設定を取得します
	 * @return 
	 */
	public String getPasv() {
		return pasv;
	}
	/**
	 * ＰＡＳＶモード設定をセットします
	 * @param srcPath
	 */
	public void setPasv(String pasv) {
		this.pasv = pasv;
	}
	/**
	 * ＦＴＰサーバポート番号を取得します
	 * @return 
	 */
	public String getPort() {
		return port;
	}
	/**
	 * ＦＴＰサーバポート番号をセットします
	 * @param srcPath
	 */
	public void setPort(String port) {
		this.port = port;
	}
	/**
	 * 転送元ファイルの拡張子を取得します
	 * @return 
	 */
	public String getSrcExtension() {
		return srcExtension;
	}
	/**
	 * 転送元ファイルの拡張子をセットします
	 * @param srcPath
	 */
	public void setSrcExtension(String srcExtension) {
		this.srcExtension = srcExtension;
	}
	/**
	 * 転送元FTPパスを取得します
	 * @return 
	 */
	public String getSrcFtpPath() {
		return srcFtpPath;
	}
	/**
	 * 転送元FTPパスをセットします
	 * @param srcPath
	 */
	public void setSrcFtpPath(String srcFtpPath) {
		this.srcFtpPath = srcFtpPath;
	}
	/**
	 * 転送元ローカルパスを取得します
	 * @return 
	 */
	public String getSrcLocalPath() {
		return srcLocalPath;
	}
	/**
	 * 転送元ローカルパスをセットします
	 * @param srcPath
	 */
	public void setSrcLocalPath(String srcLocalPath) {
		this.srcLocalPath = srcLocalPath;
	}
	/**
	 * ＦＴＰアカウントを取得します
	 * @return 
	 */
	public String getUser() {
		return user;
	}
	/**
	 * ＦＴＰアカウントをセットします
	 * @param srcPath
	 */
	public void setUser(String user) {
		this.user = user;
	}
	
}
