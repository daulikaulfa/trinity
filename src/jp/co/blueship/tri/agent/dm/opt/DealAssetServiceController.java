package jp.co.blueship.tri.agent.dm.opt;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.svc.IDealAssetService;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;

/**
 *
 * 統合監視 資源配付のコントローラクラスです<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class DealAssetServiceController {

	private IDealAssetService service = null ;
	private String serviceName = null;

	/**
	 * 接続先のリモートサービス定義名が設定されます。
	 *
	 * @serviceName リモートサービス定義名
	 */
	public void setServiceName( String serviceName ) {
		this.serviceName = serviceName;
	}

	/**
	 * 統合監視 資源配付スクリプト呼び出しを行います<br>
	 * @param action コマンド
	 * @param パラメータオブジェクト
	 * @return 実行結果オブジェクトを格納したList
	 */
	public DealAssetServiceResponseBean execute( String action , DealAssetServiceBean bean ) {

		return service().execute( action , bean ) ;
	}

	private IDealAssetService service() {

		if(service == null) {
			service = (IDealAssetService)ContextAdapterFactory.getContextAdapter().getBean( serviceName );
		}

		return service;
	}
}
