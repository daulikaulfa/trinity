package jp.co.blueship.tri.agent.dm.opt.svc;

import jp.co.blueship.tri.agent.dm.opt.beans.IDealAssetScriptCallService;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 *
 * 統合監視サーバのスクリプトを起動するためのクラス<br>
 * ＲＭＩサーバサービスとして常駐、
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class DealAssetServiceImpl implements IDealAssetService {

	/**
	 * スクリプト実行処理を行います<br>
	 * @param action 実行アクション名文字列
	 * @param matterNo 統合管理システムの案件管理番号（変更要因番号）の配列
	 * @return 処理結果
	 */
	public DealAssetServiceResponseBean execute( String action , DealAssetServiceBean bean ) {

		DealAssetServiceResponseBean retBean = null ;

		try {

			IDealAssetScriptCallService service = this.getService( action ) ;
			retBean = service.execute( bean ) ;

		} catch ( Throwable e ) {
			ExceptionUtils.printStackTrace(e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException ( SmMessageId.SM005139S, e );
		}

		return retBean ;
	}

	/**
	 * サービスクラスを取得します
	 * @param flowName  コントローラ名
	 * @return  サービスクラスオブジェクト
	 */
	private IDealAssetScriptCallService getService( String flowName ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		IDealAssetScriptCallService service = (IDealAssetScriptCallService)ca.getBean( flowName ) ;

		return service ;
	}
}
