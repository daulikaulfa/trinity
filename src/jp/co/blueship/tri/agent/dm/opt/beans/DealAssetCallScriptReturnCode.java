package jp.co.blueship.tri.agent.dm.opt.beans;


/**
 * リターンコードの定義名の列挙型です。
 *
 */
public enum DealAssetCallScriptReturnCode {
	/**
	 * 正常終了
	 */
	SUCESS( 0 ),
	/**
	 * 正常終了（業務例外）
	 */
	NORMAL( 1 ) ;

	Integer returnCode ;

	private DealAssetCallScriptReturnCode( Integer value ) {
		returnCode = value ;
	}

	/**
	 * 該当するリターンコードの定義名を検索し、そのリターンコード値を取得する
	 * @param name 定義名
	 * @return リターンコード
	 */
	public static DealAssetCallScriptReturnCode getValue( String name ) {
		for ( DealAssetCallScriptReturnCode define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}
	/**
	 * リターンコード値を取得する
	 * @return リターンコード値
	 */
	public Integer getReturnCode() {
		return returnCode ;
	}
}
