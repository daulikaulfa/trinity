package jp.co.blueship.tri.agent.dm.opt.beans;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;

public interface IDealAssetScriptCallService {
	
	/**
	 * @param args
	 * @return
	 */
	public DealAssetServiceResponseBean execute( DealAssetServiceBean bean ) ;
	
	
	/**
	 * 引数パラメータの配列をセットします
	 * @param args 引数パラメータの配列
	 */
	public void setArgs(String[] args) ;
	/**
	 * 環境変数群をセットします
	 * @param env 環境変数群
	 */
	public void setEnv(String[] env) ;
	/**
	 * 作業ディレクトリパスをセットします
	 * @param workDir 作業ディレクトリパス
	 */
	public void setWorkDir(String workDir) ;
	/**
	 * シェルのパスをセットします
	 * @param shell シェルのパス
	 */
	public void setShell(String shell) ;
	
	/**
	 * 
	 */
	public void checkArgs( DealAssetServiceBean bean ) ;
	/**
	 * 作業ディレクトリパスをチェックします
	 * @param bean
	 */
	public void checkWorkDir( DealAssetServiceBean bean ) ;
	/**
	 * 環境変数をチェックします
	 * @param bean 
	 */
	public void checkEnv( DealAssetServiceBean bean ) ;
	/**
	 * 処理結果ログをチェックします
	 * @param bean
	 * @param shellLog
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) ;
}
