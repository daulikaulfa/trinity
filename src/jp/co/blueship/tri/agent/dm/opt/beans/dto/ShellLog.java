package jp.co.blueship.tri.agent.dm.opt.beans.dto;

/**
 * シェル実行結果ログを保持するクラス
 *
 */
public class ShellLog {
	
	int exitValue ;				//シェル実行終了値
	String outLog = null ;		//出力ログ
	String errLog = null ;		//エラーログ
	
	public int getExitValue() {
		return exitValue;
	}

	public void setExitValue(int exitValue) {
		this.exitValue = exitValue;
	}

	public String getErrLog() {
		return errLog;
	}

	public void setErrLog(String errLog) {
		this.errLog = errLog;
	}
	
	public String getOutLog() {
		return outLog;
	}
	
	public void setOutLog(String outLog) {
		this.outLog = outLog;
	}
}