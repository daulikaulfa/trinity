package jp.co.blueship.tri.agent.dm.opt;

import java.util.logging.Logger;

import jp.co.blueship.tri.fw.di.spring.Contexts;

//import jp.co.blueship.trinity.common.log.ILog;
//import jp.co.blueship.trinity.common.log.TriLogFactory;

/**
 * 統合監視 資産配付 ＲＭＩサーバ メインクラス
 *
 */
public class DmAgentService {

//	private static final ILog log = TriLogFactory.getInstance() ;

	Logger logger = Logger.getAnonymousLogger() ;

	private static final String[] DEAL_ASSET_AGENT_CONTEXT_FILES = {"Fw-Module-Context.xml", "Da-App-Context.xml"};
	private static final Object mutex = new Object() ;

	/**
	 * @param args
	 */
	public static void main( String[] args ) {

		// SpringのxmlからRMIサービスを起動
		Contexts.getInstance().initBeanFactory( DEAL_ASSET_AGENT_CONTEXT_FILES ) ;

		synchronized ( mutex ) {
			while( true ) {
				try {
					mutex.wait() ;
				} catch ( InterruptedException e ) {
					e.printStackTrace() ;
					System.exit( 9 ) ;
				}
			}
		}
	}
}

