package jp.co.blueship.tri.agent.dm.opt.beans;

import java.io.File;

import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetFtpBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.DealAssetServiceResponseBean;
import jp.co.blueship.tri.agent.dm.opt.beans.dto.ShellLog;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.FileIoType;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.ftp.FtpIo;
import jp.co.blueship.tri.fw.ftp.FtpParamBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;

/**
 * 統合監視 配付資源登録 「配付資源登録」の情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class DealAssetRegistrationService extends DealAssetScriptCallService {

	private FlowDealAssetEditSupport support = null;

	public void setSupport( FlowDealAssetEditSupport support ) {
		this.support = support;
	}

	/**
	 * スクリプト実行処理本体<br>
	 * @param bean
	 * @return 処理結果
	 */
	public DealAssetServiceResponseBean execute( DealAssetServiceBean bean ) {

		//リリース管理サーバ側にFTP Server設置の場合
		{
			if ( null == bean.getFtpBean() ) {
				throw new TriSystemException( SmMessageId.SM004132F );
			}

			String ftpServer = bean.getFtpBean().getFtpServer();

			if ( StatusFlg.on.value().equals( ftpServer ) ) {
				DealAssetServiceResponseBean retBean = this.getFile( bean );
				if ( 0 != retBean.getReturnCode() ) {
					return retBean;
				}
			}
		}

		return super.execute( bean );
	}

	/**
	 *
	 */
	public void checkArgs( DealAssetServiceBean bean ) {

//		対象環境
		if( TriStringUtils.isEmpty( bean.getTargetEnvironment() ) ) {
			throw new TriSystemException( SmMessageId.SM004127F );
		}
//		ＤＲＭＳ管理番号
		if( TriStringUtils.isEmpty( bean.getDrmsManagementNo() ) ) {
			throw new TriSystemException( SmMessageId.SM004128F );
		}
		if( LENGTH_DRMS_NO != bean.getDrmsManagementNo().length() ) {
			throw new TriSystemException( SmMessageId.SM004129F , String.valueOf(LENGTH_DRMS_NO) , bean.getDrmsManagementNo() );
		}
//		資源登録先パス
		if( TriStringUtils.isEmpty( bean.getPath() ) ) {
			throw new TriSystemException( SmMessageId.SM004133F );
		}

		this.setArgs( new String[]{ 	bean.getTargetEnvironment() ,
										bean.getDrmsManagementNo() ,
										bean.getPath() } );
	}

	/**
	 *
	 */
	public void checkLog( DealAssetServiceResponseBean bean , ShellLog shellLog ) {

		bean.setReturnCode( shellLog.getExitValue() );
		bean.setOutLog( shellLog.getOutLog() );
		bean.setErrLog( shellLog.getErrLog() );

	}

	/**
	 * デプロイメント登録サーバ側にDSL保管庫のRC成果物を転送する。
	 *
	 * @param bean
	 * @return 処理結果
	 */
	private DealAssetServiceResponseBean getFile( DealAssetServiceBean bean ) {

		DealAssetServiceResponseBean retBean = new DealAssetServiceResponseBean();
		retBean.setReturnCode( 0 );
		FtpIo ftpIo = null;
		try {

			String srcFtpPath = bean.getFtpBean().getSrcFtpPath();
			if( TriStringUtils.isEmpty( srcFtpPath ) ) {
				throw new TriSystemException( SmMessageId.SM004134F );
			}

			if( TriStringUtils.isEmpty( bean.getName() ) ) {
				throw new TriSystemException( SmMessageId.SM004135F );
			}

			String destLocalPath = bean.getFtpBean().getDestLocalPath();
			File destLocalFile = new File( destLocalPath );
			if( TriStringUtils.isEmpty( destLocalPath ) ) {
				throw new TriSystemException( SmMessageId.SM004136F );
			}
			if( true != destLocalFile.exists() || true != destLocalFile.isDirectory()  ) {
				throw new TriSystemException( SmMessageId.SM004137F , destLocalPath );
			}

			/** 既存資産が存在していれば削除する */
			destLocalPath = TriStringUtils.linkPathBySlash( destLocalPath, bean.getName() );
			destLocalFile = new File( destLocalPath );

			if ( destLocalFile.exists() ) {
				TriFileUtils.delete( destLocalFile );
			}

			ftpIo = new FtpIo( this.getFtpParamBean( bean ) );
			ftpIo.openConnection();

			/** 資産を転送する */
			srcFtpPath = TriStringUtils.linkPathBySlash( srcFtpPath, bean.getName() );

			if ( true != ftpIo.isExist( srcFtpPath ) || true != ftpIo.isFile( srcFtpPath ) ) {
				throw new TriSystemException( SmMessageId.SM004138F , srcFtpPath );
			}

			ftpIo.setFileType( FileIoType.BINARY_FILE_TYPE );
			ftpIo.getFile( destLocalPath, srcFtpPath );

			return retBean;
		} catch( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005135S, e );
		} finally {
			if( null != ftpIo ) {
				try {
					ftpIo.closeConnection();
				} catch( Exception e ) {
					throw new TriSystemException( SmMessageId.SM005136S, e );
				}
			}
		}
	}

	/**
	 * Ftp接続に用いるパラメータ群をセットする
	 * @param bean
	 * @return パラメータをセットしたFtpParamBean
	 * @throws Exception
	 */
	private FtpParamBean getFtpParamBean( DealAssetServiceBean bean ) throws Exception {

		FtpParamBean ftpParam = new FtpParamBean();

		DealAssetFtpBean ftpBean = bean.getFtpBean();

		String host = ftpBean.getHost();
		if( TriStringUtils.isEmpty( host ) ) {
			throw new TriSystemException( SmMessageId.SM004139F);
		}
		String port = ftpBean.getPort();
		if( TriStringUtils.isEmpty( port ) ) {
			throw new TriSystemException( SmMessageId.SM004140F);
		}
		String user = ftpBean.getUser();
		if( TriStringUtils.isEmpty( user ) ) {
			throw new TriSystemException( SmMessageId.SM004141F);
		}
		String pasv = ftpBean.getPasv();
		if( TriStringUtils.isEmpty( pasv ) ) {
			throw new TriSystemException( SmMessageId.SM004142F);
		}
		String controlEncoding = ftpBean.getControlEncoding();
		if( TriStringUtils.isEmpty( controlEncoding ) ) {
			throw new TriSystemException( SmMessageId.SM004143F);
		}

		ftpParam.setHost( host );
		if( true != TriStringUtils.isEmpty( port ) ) {
			ftpParam.setPort( Integer.parseInt( port ) );
		}
		ftpParam.setUser( user );
		ftpParam.setPass( support.getSmFinderSupport().findPasswordEntity( PasswordCategory.FTP , host , user ).getPassword() );

		if( true != TriStringUtils.isEmpty( pasv ) ) {
			ftpParam.setPasvMode( StatusFlg.on.value().toString().equals( pasv.toLowerCase() ) ? true : false );
		}
		if( true != TriStringUtils.isEmpty( controlEncoding ) ) {
			ftpParam.setControlEncoding( this.getCharset( controlEncoding ) );
		}

		return ftpParam;
	}
	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする
	 * <br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			throw new TriSystemException( SmMessageId.SM004144F , e , encoding );
		}
		return charset;
	}

}
