package jp.co.blueship.tri.agent.dm.opt.beans.dto;

import java.io.Serializable;

/**
 * 
 * 統合監視 配布状況確認用
 *
 */
public class DealAssetStatusBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String targetEnvironment = null ;	//対象環境
	private String drmsManagementNo = null ;	//ＤＲＭＳ管理番号
	private String registrationDate = null ;	//資源登録日時(YYYYMMDDhhmmss)
	private String dealDate = null ;			//資源配付日時(YYYYMMDDhhmmss)

	/**
	 * 対象環境を取得する
	 * @return 対象環境
	 */
	public String getTargetEnvironment() {
		return targetEnvironment;
	}
	/**
	 * 対象環境をセットする
	 * @param targetEnvironment 対象環境
	 */
	public void setTargetEnvironment(String targetEnvironment) {
		this.targetEnvironment = targetEnvironment;
	}
	/**
	 * ＤＲＭＳ管理番号を取得する
	 * @return ＤＲＭＳ管理番号
	 */
	public String getDrmsManagementNo() {
		return drmsManagementNo;
	}
	/**
	 * ＤＲＭＳ管理番号をセットする
	 * @param drmsManagementNo ＤＲＭＳ管理番号
	 */
	public void setDrmsManagementNo(String drmsManagementNo) {
		this.drmsManagementNo = drmsManagementNo;
	}
	/**
	 * 資源登録日時(YYYYMMDDhhmmss)を取得する
	 * @return 資源登録日時(YYYYMMDDhhmmss)
	 */
	public String getRegistrationDate() {
		return registrationDate;
	}
	/**
	 * 資源登録日時(YYYYMMDDhhmmss)をセットする
	 * @param registrationDate 資源登録日時(YYYYMMDDhhmmss)
	 */
	public void setRegistrationDate(String registrationDate) {
		this.registrationDate = registrationDate;
	}
	/**
	 * 資源配付日時(YYYYMMDDhhmmss)をセットする
	 * @param dealDate デプロイメント登録日時(YYYYMMDDhhmmss)
	 */
	public void setDealDate(String dealDate) {
		this.dealDate = dealDate;
	}
	/**
	 * デプロイメント登録日時(YYYYMMDDhhmmss)を取得する
	 * @return デプロイメント登録日時(YYYYMMDDhhmmss)
	 */
	public String getDealDate() {
		return dealDate;
	}
	
	
}
