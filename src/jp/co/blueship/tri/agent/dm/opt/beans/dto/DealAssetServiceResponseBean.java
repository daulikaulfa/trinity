package jp.co.blueship.tri.agent.dm.opt.beans.dto;

import java.io.Serializable;
import java.util.List;

/**
 * デプロイメント登録処理スクリプト実行時の応答情報に用いる
 *
 */
public class DealAssetServiceResponseBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private int returnCode ;			//終了状態コード
	private String outLog = null ;		//出力ログ
	private String errLog = null ;		//エラーログ
	
	private List<DealAssetRegistrationHistoryBean> registrationHistoryList = null ;	//特定環境履歴情報
	private List<DealAssetStatusBean> dealStatusList = null ;	//全環境最新情報
	
	/**
	 * エラーログを取得する
	 * @return エラーログ
	 */
	public String getErrLog() {
		return errLog;
	}
	/**
	 * エラーログをセットする
	 * @param errLog エラーログ
	 */
	public void setErrLog(String errLog) {
		this.errLog = errLog;
	}
	/**
	 * 出力ログを取得する
	 * @return 出力ログ
	 */
	public String getOutLog() {
		return outLog;
	}
	/**
	 * 出力ログをセットする
	 * @param outLog 出力ログ
	 */
	public void setOutLog(String outLog) {
		this.outLog = outLog;
	}
	/**
	 * 終了状態コードを取得する
	 * @return 終了状態コード
	 */
	public int getReturnCode() {
		return returnCode;
	}
	/**
	 * 終了状態コードをセットする
	 * @param returnCode 終了状態コード
	 */
	public void setReturnCode(int returnCode) {
		this.returnCode = returnCode;
	}
	/**
	 * 特定環境履歴情報を取得する<br>
	 * 【特定環境履歴情報取得】処理に用いる<br>
	 * @return 特定環境履歴情報
	 */
	public List<DealAssetRegistrationHistoryBean> getRegistrationHistoryList() {
		return registrationHistoryList;
	}
	/**
	 * 特定環境履歴情報をセットする<br>
	 * 【特定環境履歴情報取得】処理に用いる<br>
	 * @param registrationHistoryList 特定環境履歴情報
	 */
	public void setRegistrationHistoryList(
			List<DealAssetRegistrationHistoryBean> registrationHistoryList) {
		this.registrationHistoryList = registrationHistoryList;
	}
	/**
	 * 全環境最新情報を取得する<br>
	 * 【全環境最新情報取得】処理に用いる<br>
	 * @return
	 */
	public List<DealAssetStatusBean> getDealStatusList() {
		return dealStatusList;
	}
	/**
	 * 全環境最新情報をセットする<br>
	 * 【全環境最新情報取得】処理に用いる<br>
	 * @param dealStatusBean 全環境最新情報
	 */
	public void setDealStatusList(List<DealAssetStatusBean> dealStatusList) {
		this.dealStatusList = dealStatusList;
	}
	
	
}
