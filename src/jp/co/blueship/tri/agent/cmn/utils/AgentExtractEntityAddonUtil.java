package jp.co.blueship.tri.agent.cmn.utils;

import java.util.List;

import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.constants.RemoteServiceType;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。
 * ExtractEntityAddonUtilの派生クラスです。依存性を解消するために作成しました。
 * @author Takashi Ono
 *
 */
public class AgentExtractEntityAddonUtil {

	/**
	 * 指定されたリストから資産agentのステータス情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static AgentStatus extractAgentStatus(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof AgentStatus) {
				return (AgentStatus) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストから資産agentのステータス情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static AgentStatus[] extractAgentStatusArray(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof AgentStatus[]) {
				return (AgentStatus[]) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからリモートサービス情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IRmiSvcDto extractRemoteService(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof IRmiSvcDto) {
				return (IRmiSvcDto) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからリモートサービス種別情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static RemoteServiceType extractRemoteServiceType(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof RemoteServiceType) {
				return (RemoteServiceType) obj;
			}
		}

		return null;
	}

}
