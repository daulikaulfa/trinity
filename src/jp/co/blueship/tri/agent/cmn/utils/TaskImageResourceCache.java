package jp.co.blueship.tri.agent.cmn.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.IBldSrvDao;
import jp.co.blueship.tri.bm.dao.bldsrv.constants.BldSrvItems;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DaoName;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * 業務シーケンス・タスクのリソースを管理するクラスです。
 *
 */
public class TaskImageResourceCache {

	private static final ILog log = TriLogFactory.getInstance();

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	private static Map<String, Object> commonMap = new Hashtable<String, Object>();
	private static Map<String, Map<String, Object>> lotInfoMap = new Hashtable<String, Map<String, Object>>();
	private static Map<String, Map<String, Object>> propMap = new Hashtable<String, Map<String, Object>>();

	private static final int CACHE_EXPIRE_SEC = 60 * 10;// 10分

	private static boolean isInit = false;

	/**
	 * 初期化を行います。
	 *
	 */
	public static synchronized void init() {
		if (isInit)
			return;

		isInit = true;

		{
			// サーバーのキャッシング
			IBldSrvDao dao = (IBldSrvDao) ac.getBean(DaoName.BLD_SRV_DAO.toString());
			ISqlSort sort = new SortBuilder();
			sort.setElement(BldSrvItems.bldSrvId, TriSortOrder.Asc, 1);
			BldSrvCondition condition = new BldSrvCondition();
			List<IBldSrvEntity> list = dao.find(condition.getCondition(), sort);
			IBldSrvEntity[] entitys = list.toArray(new IBldSrvEntity[0]);

			commonMap.put(IBldSrvEntity.class.getSimpleName(), entitys);
		}
	}

	/**
	 * サーバ及びシステム情報のキャッシュ値が初期化済かどうかを設定します。
	 *
	 * @param init 初期化する場合、false。キャッシュ値をそのまま利用するならtrue。
	 */
	public static synchronized void setInit(boolean init) {
		LogHandler.debug(log, "__________setInit:=" + init);

		isInit = init;
	}

	public static void setLotInfoResourceCache(String userId, List<ILotDto> pjtLotEntityArray, List<IGrpUserLnkEntity> groupUserEntityArray) {

		Map<String, Object> resourceMap = new Hashtable<String, Object>();

		resourceMap.put(ILotDto.class.getSimpleName(), pjtLotEntityArray);
		resourceMap.put(IGrpUserLnkEntity.class.getSimpleName(), groupUserEntityArray);

		resourceMap.put(Date.class.getSimpleName(), getExpireDate(CACHE_EXPIRE_SEC));// キャッシュの保持期限

		lotInfoMap.put(userId, resourceMap);

		if (!isInit) {
			init();
		}
	}

	/**
	 * ビルド状況に関するエンティティをキャッシュさせます。
	 *
	 * @param lot ロットのエンティティ
	 * @param buildEnv ビルド環境のエンティティ
	 * @param buildEnvServer ビルドシステム環境のエンティティ
	 * @param bp ビルドのエンティティ
	 * @param timeline タイムラインのエンティティ
	 * @param task タスクのエンティティ
	 */
	public static void setBuildResourceCache(
			ILotEntity lotEntity,
			IBldEnvEntity bldEnvEntity,
			IBldEnvSrvEntity[] buildEnvServerEntityArray,
			IBpDto bpDto,
			IBldTimelineEntity[] timelineEntityArray,
			ITaskFlowEntity[] taskFlowEntities) {

		LogHandler.debug(log, logMessage(bpDto));

		Map<String, Object> resourceMap = new Hashtable<String, Object>();

		resourceMap.put(ILotEntity.class.getSimpleName(), lotEntity);
		resourceMap.put(IBpDto.class.getSimpleName(), bpDto);
		resourceMap.put(IBldEnvEntity.class.getSimpleName(), bldEnvEntity);
		resourceMap.put(IBldEnvSrvEntity.class.getSimpleName(), buildEnvServerEntityArray);
		resourceMap.put(IBldTimelineEntity.class.getSimpleName(), timelineEntityArray);
		resourceMap.put(ITaskFlowEntity.class.getSimpleName(), taskFlowEntities);

		resourceMap.put(Date.class.getSimpleName(), getExpireDate(CACHE_EXPIRE_SEC));// キャッシュの保持期限

		propMap.put(bpDto.getBpEntity().getBpId(), resourceMap);

		if (!isInit) {
			init();
		}

	}

	private static String logMessage(IBpDto bpDto) {
		return "__________setBuildResourceCache:buildNo:=" + ((null != bpDto) ? bpDto.getBpEntity().getBpId() : "null");
	}

	/**
	 * リリース状況に関するエンティティをキャッシュさせます。
	 *
	 * @param env リリース環境のエンティティ
	 * @param envServer リリースシステム環境のエンティティ
	 * @param rel リリースのエンティティ
	 * @param timeline タイムラインのエンティティ
	 * @param task タスクのエンティティ
	 */
	public static void setReleaseResourceCache(IBldEnvEntity envEntity, IBldEnvSrvEntity[] envServerEntityArray, IRpEntity relEntity,
			IBldTimelineEntity[] timelineEntityArray, ITaskFlowEntity[] taskEntityArray) {

		LogHandler.debug(log, logMessage(relEntity));

		Map<String, Object> resourceMap = new Hashtable<String, Object>();

		resourceMap.put(IBldEnvEntity.class.getSimpleName(), envEntity);
		resourceMap.put(IBldEnvSrvEntity.class.getSimpleName(), envServerEntityArray);
		resourceMap.put(IRpEntity.class.getSimpleName(), relEntity);
		resourceMap.put(IBldTimelineEntity.class.getSimpleName(), timelineEntityArray);
		resourceMap.put(ITaskFlowEntity.class.getSimpleName(), taskEntityArray);

		resourceMap.put(Date.class.getSimpleName(), getExpireDate(CACHE_EXPIRE_SEC));// キャッシュの保持期限

		propMap.put(relEntity.getRpId(), resourceMap);

		if (!isInit) {
			init();
		}

	}

	private static String logMessage(IRpEntity relEntity) {
		return "__________setReleaseResourceCache:relNo:=" + ((null != relEntity) ? relEntity.getRpId() : "null");
	}

	public static IBldSrvEntity[] getBldSrvEntity() {
		IBldSrvEntity[] entityArray = (IBldSrvEntity[]) commonMap.get(IBldSrvEntity.class.getSimpleName());

		return (null == entityArray) ? new IBldSrvEntity[0] : entityArray;
	}

	public static Map<String, Object> getLotInfo(String userId) {
		cleanExpiredCache(lotInfoMap);
		Map<String, Object> map = (Map<String, Object>) lotInfoMap.get(userId);
		return map;
	}

	public static Map<String, Object> getBuildResource(String buildNo) {
		cleanExpiredCache(propMap);
		Map<String, Object> map = (Map<String, Object>) propMap.get(buildNo);
		return map;
	}

	public static Map<String, Object> getRelResource(String relNo) {
		cleanExpiredCache(propMap);
		Map<String, Object> map = (Map<String, Object>) propMap.get(relNo);
		return map;
	}

	/**
	 * リソースキャッシュからキーを元にオブジェクトを取得します。
	 *
	 * サーバNoが重複している場合は、先に見つかったサーバNoのエンティティが返却されます。
	 *
	 * @param serverNo サーバNo
	 * @return サーバエンティティ
	 */
	public static IBldSrvEntity findUcfServerEntityByCache(String serverNo) {

		if (TriStringUtils.isEmpty(serverNo)) {
			throw new TriSystemException( SmMessageId.SM004116F );
		}

		IBldSrvEntity[] servers = getBldSrvEntity();

		for (IBldSrvEntity server : servers) {

			if (serverNo.equals(server.getBldSrvId())) {
				return server;
			}
		}
		throw new TriSystemException( SmMessageId.SM004117F , serverNo );
	}

	/**
	 * キャッシュの保存期限を過ぎたデータをチェックし、クリアする
	 */
	private synchronized static void cleanExpiredCache(Map<String, Map<String, Object>> map) {

		Date curDate = new Date();// 現在時刻

		Iterator<Entry<String, Map<String, Object>>> iter = map.entrySet().iterator();
		while (iter.hasNext()) {
			Entry<String, Map<String, Object>> entry = iter.next();

			Map<String, Object> innerMap = entry.getValue();
			String dateKey = Date.class.getSimpleName();
			if (innerMap.containsKey(dateKey)) {
				Date expireDate = (Date) innerMap.get(dateKey);
				if (curDate.after(expireDate)) {
					iter.remove();
				}
			}
		}
	}

	/**
	 * キャッシュの時間切れ時刻を生成する。
	 */
	private static Date getExpireDate(int expireSecs) {

		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.SECOND, expireSecs);
		return cal.getTime();
	}
}
