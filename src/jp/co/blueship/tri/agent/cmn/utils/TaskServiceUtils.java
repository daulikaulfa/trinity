package jp.co.blueship.tri.agent.cmn.utils;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 * TaskServiceに対する処理を提供するClass
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2007
 */
public class TaskServiceUtils {



	/**
	 * <code>properties</code>に含まれるすべてのITaskPropertyEntityオブジェクトを<code>taskEntity</code>に追加します。
	 *
	 * @param properties 追加されるITaskPropertyEntityオブジェクトのリスト
	 * @param taskEntity 追加する対象のITaskEntityオブジェクト
	 * @return この呼び出しの結果、{@link ITaskEntity#getProperty()}で返される配列が変更された場合は true
	 */
	public static boolean addAllTaskProperties(List<ITaskPropertyEntity> properties, ITaskEntity taskEntity) {

		ITaskPropertyEntity[] propertiesArray = taskEntity.getProperty();
		List<ITaskPropertyEntity> newProperties = new ArrayList<ITaskPropertyEntity>(FluentList.from(propertiesArray).asList());

		boolean result = newProperties.addAll(0, properties);
		if(result) {
			propertiesArray = newProperties.toArray( new ITaskPropertyEntity[0] );
			taskEntity.setProperty(propertiesArray);
			return true;
		}

		return false;
	}

}
