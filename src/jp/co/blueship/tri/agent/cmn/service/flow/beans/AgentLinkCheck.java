package jp.co.blueship.tri.agent.cmn.service.flow.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.bm.dao.rmisvc.IRmiSvcDao;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcCondition;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RemoteService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * Agentの疎通チェックを行います。
 *
 */
public class AgentLinkCheck implements IDomain<IGeneralServiceBean> {

	

	private static final ILog log = TriLogFactory.getInstance();

	private IRmiSvcDao rmiSvcDao = null ;

	private IBuildTaskService buildTaskService = null;
	private IReleaseTaskService releaseTaskService = null;

	/**
	 *
	 * @param ucfRemoteServiceDao
	 */
	public void setRmiSvcDao(IRmiSvcDao ucfRemoteServiceDao) {
		this.rmiSvcDao = ucfRemoteServiceDao;
	}

	public void setBuildTaskService( IBuildTaskService buildTaskService ) {
		this.buildTaskService = buildTaskService;
	}

	public void setReleaseTaskService( IReleaseTaskService releaseTaskService ) {
		this.releaseTaskService = releaseTaskService;
	}

	/**
	 * agentとの疎通チェックを行う。
	 *
	 */
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		LogHandler.debug( log , this.getClass().getName() + " " + "process start!!" ) ;

		try {

			RmiSvcCondition condition = new RmiSvcCondition();
			List<IRmiSvcEntity> list = this.rmiSvcDao.find(condition.getCondition());

			if( TriStringUtils.isEmpty( list ) ) {
				LogHandler.info( log , TriLogMessage.LSM0011 ) ;
				return serviceDto;
			}
			List<AgentStatus> statusList = new ArrayList<AgentStatus>() ;

			List<IRmiSvcEntity> remoteServiceEntityArray = list;
			for( IRmiSvcEntity entity : remoteServiceEntityArray ) {
				if( StatusFlg.on.equals( entity.getDelStsId() ) ) {
					continue ;
				}

				IRmiSvcDto rmiSvcDto = new RmiSvcDto();
				rmiSvcDto.setRmiSvcEntity( entity );
				List<Object> paramListWork = new ArrayList<Object>() ;
				paramListWork.add( rmiSvcDto ) ;

				List<Object> rtnList = null ;
				AgentStatus agentStatus = null ;
				boolean isErr = false ;
				try {
					RemoteService curServiceId = RemoteService.getValueById( entity.getRmiSvcId() ) ;
/* EP専用処理の為、削除
 * 						if( RemoteService.RMI_SERVICE_PROXY.equals( curServiceId ) ) {
							rtnList = RmiServiceUtils.executeActionPojoRemote( paramListWork , serverId , remoteServiceType , this.serviceName ) ;
						} else */if( RemoteService.BUILD_TASK_SERVICE.equals( curServiceId ) ) {
						if( null != buildTaskService ) {
							rtnList = buildTaskService.executeLinkCheckByService( paramListWork ) ;
						}
					} else if( RemoteService.RELEASE_TASK_SERVICE.equals( curServiceId ) ) {
						if( null != releaseTaskService ) {
							rtnList = releaseTaskService.executeLinkCheckByService( paramListWork ) ;
						}
					}
				} catch ( Exception e ) {
					isErr = true ;
					LogHandler.fatal( log , e ) ;
				} finally {
					if( ! isErr && null != rtnList ) {
						agentStatus = AgentExtractEntityAddonUtil.extractAgentStatus( rtnList ) ;
						if( null != agentStatus ) {
							statusList.add( agentStatus ) ;
						} else {
							isErr = true ;
						}
					}

					if( isErr ) {
						agentStatus = new AgentStatus() ;
						agentStatus.setStatus( false ) ;
						agentStatus.setRemoteServiceEntity( rmiSvcDto ) ;
						statusList.add( agentStatus ) ;
					}
				}
			}
			return serviceDto.add( statusList.toArray( new AgentStatus[ 0 ] ) ) ;

		} finally {
			LogHandler.debug( log , this.getClass().getName() + " " + "process end!!" ) ;
		}
	}


}
