package jp.co.blueship.tri.agent.cmn.service.flow.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.bm.dao.rmisvc.IRmiSvcDao;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcCondition;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RemoteService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * Agentの疎通チェックを行います。
 * <br>
 *
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class AgentLinkCheckEx implements IDomain<IGeneralServiceBean> {

	

	private static final ILog log = TriLogFactory.getInstance();

	private IRmiSvcDao rmiSvcDao = null ;

	private IBuildTaskService buildTaskService = null;
	private IReleaseTaskService releaseTaskService = null;

	private Properties checkServicesProps = null;
	private Properties checkServiceTypesProps = null;

	/**
	 * Agentの疎通確認を行うサービスを設定します。
	 *
	 * <DD>以下のキー値を設定します。</DD>
	 * <DD>"RMI_SERVICE_PROXY"</DD>
	 * <DD>"BUILD_TASK_SERVICE"</DD>
	 * <DD>"RELEASE_TASK_SERVICE"</DD>
	 * <br>
	 * 値に"1"が設定されている場合、対象のサービスがチェック対象となります。<br>
	 * それ以外、または"0"の場合は、疎通のチェックを行いません。
	 *
	 * @param properties プロパティリスト
	 */
	public void setCheckServices(Properties properties) {
		this.checkServicesProps = properties;
	}
	/**
	 * Agentの疎通確認を行うサービスプロパティ情報を取得します。
	 * @return properties プロパティリスト
	 */
	public Properties getCheckServices() {
		return this.checkServicesProps ;
	}

	/**
	 * Agentの疎通確認を行うサービスタイプを設定します。
	 *
	 * <DD>以下のキー値を設定します。</DD>
	 * <DD>"AGENT_CHANGEC"</DD>
	 * <DD>"AGENT_RELEASE"</DD>
	 * <br>
	 * 値に"1"が設定されている場合、対象のサービスがチェック対象となります。<br>
	 * それ以外、または"0"の場合は、疎通のチェックを行いません。
	 *
	 * @param properties プロパティリスト
	 */
	public void setCheckServiceTypes(Properties properties) {
		this.checkServiceTypesProps = properties;
	}
	/**
	 * Agentの疎通確認を行うサービスタイププロパティ情報を取得します。
	 * @return properties プロパティリスト
	 */
	public Properties getCheckServiceTypes() {
		return this.checkServiceTypesProps ;
	}

	/**
	 *
	 * @param ucfRemoteServiceDao
	 */
	public void setRmiSvcDao(IRmiSvcDao ucfRemoteServiceDao) {
		this.rmiSvcDao = ucfRemoteServiceDao;
	}

	public void setBuildTaskService( IBuildTaskService buildTaskService ) {
		this.buildTaskService = buildTaskService;
	}

	public void setReleaseTaskService( IReleaseTaskService releaseTaskService ) {
		this.releaseTaskService = releaseTaskService;
	}

	/**
	 * agentとの疎通チェックを行う。
	 *
	 */
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		LogHandler.debug( log , this.getClass().getName() + " " + "process start!!" ) ;

		try {
			if ( null == this.getCheckServices() || null == this.getCheckServiceTypes() ) {
				return serviceDto;
			}

			RmiSvcCondition condition = new RmiSvcCondition();
			List<IRmiSvcEntity> list = this.rmiSvcDao.find(condition.getCondition());

			if( TriStringUtils.isEmpty( list ) ) {
				LogHandler.info( log , TriLogMessage.LSM0011 ) ;
				return serviceDto;
			}
			List<AgentStatus> statusList = new ArrayList<AgentStatus>() ;

			List<IRmiSvcEntity> remoteServiceEntityArray = list;
			for( IRmiSvcEntity entity : remoteServiceEntityArray ) {
				if( StatusFlg.on.equals( entity.getDelStsId() ) ) {
					continue ;
				}

				List<Object> paramListWork = new ArrayList<Object>() ;

				paramListWork.add( entity ) ;

				List<Object> rtnList = null ;
				AgentStatus agentStatus = null ;
				boolean isErr = false ;
				try {
					if ( TriStringUtils.isEmpty( entity.getRmiSvcId() ) ) {
						continue;
					}

					RemoteService curServiceId = RemoteService.getValueById( entity.getRmiSvcId() ) ;
					if ( null == curServiceId ) {
						continue;
					}

					String checkService = this.getCheckServices().getProperty( curServiceId.name() );

					if ( ! StatusFlg.on.value().equals(checkService) ) {
						continue;
					}

/* EP専用処理の為、削除
 * 					if( RemoteService.RMI_SERVICE_PROXY.equals( curServiceId ) ) {
						rtnList = RmiServiceUtils.executeActionPojoRemote( paramListWork , serverId , remoteServiceType , this.serviceName ) ;
					} else */if( RemoteService.BUILD_TASK_SERVICE.equals( curServiceId ) ) {
						if( null != buildTaskService ) {
							rtnList = buildTaskService.executeLinkCheckByService( paramListWork ) ;
						}
					} else if( RemoteService.RELEASE_TASK_SERVICE.equals( curServiceId ) ) {
						if( null != releaseTaskService ) {
							rtnList = releaseTaskService.executeLinkCheckByService( paramListWork ) ;
						}
					}
				} catch ( Exception e ) {
					isErr = true ;
					LogHandler.fatal( log , e ) ;
				} finally {
					if( ! isErr && null != rtnList ) {
						agentStatus = AgentExtractEntityAddonUtil.extractAgentStatus( rtnList ) ;
						if( null != agentStatus ) {
							statusList.add( agentStatus ) ;
						} else {
							isErr = true ;
						}
					}

					if( isErr ) {
						agentStatus = new AgentStatus() ;
						agentStatus.setStatus( false ) ;
						IRmiSvcDto rmiSvcDto = new RmiSvcDto();
						rmiSvcDto.setRmiSvcEntity( entity );
						agentStatus.setRemoteServiceEntity( rmiSvcDto ) ;
						statusList.add( agentStatus ) ;
					}
				}
			}
			return serviceDto.add( statusList.toArray( new AgentStatus[ 0 ] ) ) ;

		} finally {
			LogHandler.debug( log , this.getClass().getName() + " " + "process end!!" ) ;
		}
	}


}
