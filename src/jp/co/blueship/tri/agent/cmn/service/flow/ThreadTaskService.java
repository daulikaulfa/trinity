package jp.co.blueship.tri.agent.cmn.service.flow;

import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.ASyncBuildTask;
import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.ASyncReleaseTask;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.concurrent.IASyncTask;
import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.task.ITaskService;

/**
 * 業務シーケンス処理を非同期で実装するクラスです。 <br>
 * このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ThreadTaskService implements ITaskService, IBuildTaskService, IReleaseTaskService {

	private ITaskService service;
	private IASyncTask aSyncTask;
	private IContextAdapter context;

	/**
	 * タスクサービスが設定されます。
	 *
	 * @param service タスクサービス
	 */
	public void setTaskService(ITaskService service) {
		this.service = service;
	}

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 *
	 * @param context コンテキスト
	 */
	public final void setContext(IContextAdapter context) {
		this.context = context;
	}

	/**
	 * 非同期で実行するタスクのラッパクラスが設定されます。
	 *
	 * @param aSyncTask
	 */
	public void setAsyncTask(IASyncTask aSyncTask) {
		this.aSyncTask = aSyncTask;
	}

	private final Object getBean(String beanName) {
		return context.getBean(beanName);
	}

	/**
	 * ビルド業務シーケンスをタイムライン単位に実行します。
	 *
	 * @param request 依頼するタイムライン
	 * @param bean 業務シーケンスで使用するメッセージ
	 *
	 * @throws Exception
	 */
	@Override
	public final void execute(IBldTimelineEntity request, IBuildTaskBean bean) throws Exception {

		PreConditions.assertOf(aSyncTask != null, "ASyncTask is not specified.");
		PreConditions.assertOf(ASyncBuildTask.class.isInstance(aSyncTask), "ASyncTask is not matched type with ASyncBuildTask.");

		((ASyncBuildTask) aSyncTask).setTimeLine(request);
		((ASyncBuildTask) aSyncTask).setParam(bean);
		executer().submit(aSyncTask);
	}

	/**
	 * リリース業務シーケンスをタイムライン単位に実行します。
	 *
	 * @param request 依頼するタイムライン
	 * @param bean 業務シーケンスで使用するメッセージ
	 *
	 * @throws Exception
	 */
	@Override
	public final void execute(IBldTimelineEntity request, IReleaseTaskBean bean) throws Exception {

		PreConditions.assertOf(aSyncTask != null, "ASyncTask is not specified.");
		PreConditions.assertOf(ASyncReleaseTask.class.isInstance(aSyncTask), "ASyncTask is not matched type with ASyncReleaseTask.");
		((ASyncReleaseTask) aSyncTask).setTimeLine(request);
		((ASyncReleaseTask) aSyncTask).setParam(bean);
		executer().submit(aSyncTask);
	}

	/**
	 * ビルド業務シーケンスをタイムライン中のシステム単位に実行します。<br>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param request タイムライン全体
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 依頼するライン（システム）
	 * @throws Exception
	 */
	@Override
	public final void execute(IBldTimelineEntity request, IBuildTaskBean bean,
			IBldTimelineAgentEntity iLineEntity) throws Exception {

		PreConditions.assertOf(aSyncTask != null, "ASyncTask is not specified.");
		PreConditions.assertOf(ASyncBuildTask.class.isInstance(aSyncTask), "ASyncTask is not matched type with ASyncBuildTask.");

		((ASyncBuildTask) aSyncTask).setTimeLine(request);
		((ASyncBuildTask) aSyncTask).setParam(bean);
		((ASyncBuildTask) aSyncTask).setLine(iLineEntity);
		executer().submit(aSyncTask);
	}

	/**
	 * リリース業務シーケンスをタイムライン中のシステム単位に実行します。<br>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param request タイムライン全体
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 依頼するライン（システム）
	 * @throws Exception
	 */
	@Override
	public final void execute(IBldTimelineEntity request, IReleaseTaskBean bean,
			IBldTimelineAgentEntity iLineEntity) throws Exception {

		PreConditions.assertOf(aSyncTask != null, "ASyncTask is not specified.");
		PreConditions.assertOf(ASyncReleaseTask.class.isInstance(aSyncTask), "ASyncTask is not matched type with ASyncReleaseTask.");

		((ASyncReleaseTask) aSyncTask).setTimeLine(request);
		((ASyncReleaseTask) aSyncTask).setParam(bean);
		((ASyncReleaseTask) aSyncTask).setLine(iLineEntity);
		executer().submit(aSyncTask);
	}

	private TriTaskExecutor executer() {
		return (TriTaskExecutor) getBean(TriTaskExecutor.BEAN_NAME);
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		((IBuildTaskService) this.service).writeProcessByIrregular(timeLine, param, line, e);
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IReleaseTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		((IReleaseTaskService) this.service).writeProcessByIrregular(timeLine, param, line, e);
	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {
		try {
			if (service instanceof IBuildTaskService) {
				((IBuildTaskService) this.service).executeLinkCheck();
			}
			if (service instanceof IReleaseTaskService) {
				((IReleaseTaskService) this.service).executeLinkCheck();
			}

		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005183S, e);
		}
		return null;
	}

	@Override
	public List<Object> executeLinkCheckByService(List<Object> paramList) throws Exception {

		List<Object> retParamList = null;
		try {
			if (service instanceof IBuildTaskService) {
				retParamList = ((IBuildTaskService) this.service).executeLinkCheckByService(paramList);
			}
			if (service instanceof IReleaseTaskService) {
				retParamList = ((IReleaseTaskService) this.service).executeLinkCheckByService(paramList);
			}

		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005183S, e);
		}
		return retParamList;
	}

}
