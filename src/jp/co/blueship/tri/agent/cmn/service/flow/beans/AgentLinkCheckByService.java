package jp.co.blueship.tri.agent.cmn.service.flow.beans;

import java.util.List;

import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.svc.beans.IActionPojoRemote;

/**
 * Agentの疎通チェックを行います。
 *
 */
public class AgentLinkCheckByService implements IActionPojoRemote<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * agentとの疎通チェックを行う。
	 *
	 */
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		LogHandler.debug( log , this.getClass().getName() + " " + "process start!!" ) ;

		List<Object> paramList = serviceDto.getParamList();

		try {
			IRmiSvcDto remoteService = AgentExtractEntityAddonUtil.extractRemoteService( paramList ) ;

			AgentStatus status = new AgentStatus() ;
			status.setRemoteServiceEntity( remoteService ) ;
			status.setStatus( true ) ;

			paramList.add( status ) ;

			return serviceDto;

		} finally {

			LogHandler.debug( log , this.getClass().getName() + " " + "process end!!" ) ;

		}
	}

}
