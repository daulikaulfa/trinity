package jp.co.blueship.tri.agent.cmn.service.rpc;

import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.task.ITaskService;

/**
 * 業務シーケンス処理をリモートサービスで利用するためのクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class TaskServiceExporter implements ITaskService, IBuildTaskService, IReleaseTaskService {

	private String serviceName = null;

	/**
	 * サービス名を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getServiceName() {
		return serviceName;
	}

	/**
	 * サービス名を設定します。
	 *
	 * @param serviceName サービス名
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	@Override
	public void execute(IBldTimelineEntity request, IBuildTaskBean bean) throws Exception {

		((IBuildTaskService) service()).execute(request, bean);
	}

	@Override
	public void execute(IBldTimelineEntity request, IBuildTaskBean bean, IBldTimelineAgentEntity iLineEntity) throws Exception {

		((IBuildTaskService) service()).execute(request, bean, iLineEntity);
	}

	@Override
	public void execute(IBldTimelineEntity request, IReleaseTaskBean bean) throws Exception {

		((IReleaseTaskService) service()).execute(request, bean);
	}

	@Override
	public void execute(IBldTimelineEntity request, IReleaseTaskBean bean,
			IBldTimelineAgentEntity iLineEntity) throws Exception {

		((IReleaseTaskService) service()).execute(request, bean, iLineEntity);
	}

	private IContextAdapter context() {
		return ContextAdapterFactory.getContextAdapter();
	}

	private ITaskService service() {
		return (ITaskService) context().getBean(this.getServiceName());
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		((IBuildTaskService) service()).writeProcessByIrregular(timeLine, param, line, e);
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IReleaseTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		((IReleaseTaskService) service()).writeProcessByIrregular(timeLine, param, line, e);

	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {

		boolean retryFlg = false;
		// BuildTaskServiceとして処理
		try {
			retryFlg = false;
			IBuildTaskService buildTaskService = (IBuildTaskService) service();
			return buildTaskService.executeLinkCheck();
		} catch (ClassCastException e) {
			retryFlg = true;
		}
		// ReleaseTaskServiceとして処理
		try {
			retryFlg = false;
			IReleaseTaskService releaseTaskService = (IReleaseTaskService) service();
			return releaseTaskService.executeLinkCheck();
		} catch (ClassCastException e) {
			retryFlg = true;
		}

		if (retryFlg) {
			throw new TriSystemException(SmMessageId.SM004115F , this.getServiceName() );
		}
		return null;
	}

	@Override
	public List<Object> executeLinkCheckByService(List<Object> paramList) throws Exception {

		boolean retryFlg = false;
		// BuildTaskServiceとして処理
		try {
			retryFlg = false;
			IBuildTaskService buildTaskService = (IBuildTaskService) service();
			return buildTaskService.executeLinkCheckByService(paramList);
		} catch (ClassCastException e) {
			retryFlg = true;
		}
		// ReleaseTaskServiceとして処理
		try {
			retryFlg = false;
			IReleaseTaskService releaseTaskService = (IReleaseTaskService) service();
			return releaseTaskService.executeLinkCheckByService(paramList);
		} catch (ClassCastException e) {
			retryFlg = true;
		}

		if (retryFlg) {
			throw new TriSystemException(SmMessageId.SM004115F , this.getServiceName() );
		}
		return paramList;
	}
}
