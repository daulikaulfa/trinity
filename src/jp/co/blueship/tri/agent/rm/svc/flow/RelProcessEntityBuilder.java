package jp.co.blueship.tri.agent.rm.svc.flow;

import static jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils.*;

import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;

/**
 * RelProcessEntityを構築するためのビルダクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class RelProcessEntityBuilder {

	private IBldTimelineEntity timeLine;
	private IBldTimelineAgentEntity line;
	private String procId;
	private String message;
	private String procStatusId;
	private StatusFlg compStsId;

	/**
	 * コンストラクタです。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param line 依頼するライン（システム）
	 * @param contextAdapter DIコンテナへアクセスするためのコンテキストアダプタ
	 */
	public RelProcessEntityBuilder(IBldTimelineEntity timeLine, IBldTimelineAgentEntity line) {
		this.timeLine = timeLine;
		this.line = line;
	}

	/**
	 * プロセスIDを設定します。
	 *
	 * @param procId プロセスID
	 */
	public void setProcId(String procId) {
		this.procId = procId;
	}

	/**
	 * エラー発生時のメッセージ内容を設定します。
	 *
	 * @param message
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	/**
	 * スタータスIDを設定します。
	 *
	 * @param statusId ステータスID
	 */
	public void setProcStatusId(String procStatusId) {
		this.procStatusId = procStatusId;
	}

	/**
	 * 完了スタータスIDを設定します。
	 *
	 * @param compStsId 完了ステータスID
	 */
	public void setCompStsId(StatusFlg compStsId) {
		this.compStsId = compStsId;
	}

	/**
	 * ITaskFlowProcEntityを構築して返します。
	 *
	 * @return ITaskFlowProcEntity
	 */
	public ITaskFlowProcEntity getEntity() {

		ITaskFlowProcEntity taskFlowProcEntity = entity();

		taskFlowProcEntity.setProcId(procId);
		taskFlowProcEntity.setBldSrvId(deriveBldSrvId());
		taskFlowProcEntity.setBldEnvId(timeLine.getBldEnvId());
		taskFlowProcEntity.setBldLineNo(timeLine.getBldLineNo());
		taskFlowProcEntity.setTaskFlowId(deriveTaskflowId());
		taskFlowProcEntity.setTargetSeqNo(deriveTargetSeqNo());
		taskFlowProcEntity.setStsId(procStatusId);
		taskFlowProcEntity.setMsgId("");
		taskFlowProcEntity.setMsg(defaultIfNull(message, ""));
		taskFlowProcEntity.setCompStsId(compStsId);

		taskFlowProcEntity.setProcStTimestamp(TriDateUtils.getSystemTimestamp());
		taskFlowProcEntity.setProcEndTimestamp(TriDateUtils.getSystemTimestamp());

		return taskFlowProcEntity;
	}

	private ITaskFlowProcEntity entity() {
		return new TaskFlowProcEntity();
	}

	private String deriveBldSrvId() {
		if (line == null) {
			return "";
		}

		return line.getBldSrvId();
	}

	private String deriveTaskflowId() {
		if (line == null) {
			return "";
		}

		return line.getTaskFlowId();
	}

	private Integer deriveTargetSeqNo() {
		if (line == null) {
			return 0;
		}

		return line.getTargetSeqNo();
	}
}
