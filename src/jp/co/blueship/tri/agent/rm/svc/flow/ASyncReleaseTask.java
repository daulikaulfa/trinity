package jp.co.blueship.tri.agent.rm.svc.flow;

import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.concurrent.ASyncTask;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;

/**
 * リリース業務シーケンスを非同期実行コントローラによるスレッド配下で実行する際に業務シーケンスをラッピングするための<br/>
 * クラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class ASyncReleaseTask extends ASyncTask<IBldTimelineEntity, IReleaseTaskBean, IBldTimelineAgentEntity> {

	/**
	 * リリースの業務シーケンスをタイムライン単位に実行します。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	@Override
	protected void execute(IBldTimelineEntity timeLine, IReleaseTaskBean param) throws Exception {
		releaseTaskService().execute(timeLine, param);
	}

	/**
	 * リリースの業務シーケンスをタイムライン単位に実行します。<br/>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 *
	 * @throws Exception タスク実行時エラーが発生した。
	 */
	@Override
	protected void execute(IBldTimelineEntity timeLine, IReleaseTaskBean param, IBldTimelineAgentEntity line) throws Exception {
		releaseTaskService().execute(timeLine, param, line);
	}

	private IReleaseTaskService releaseTaskService() {

		PreConditions.assertOf(IReleaseTaskService.class.isInstance(service()), "IReleaseTaskService is not injected.");
		return (IReleaseTaskService) service();
	}

	@Override
	protected void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IReleaseTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		releaseTaskService().writeProcessByIrregular(timeLine, param, line, e);
	}

}
