package jp.co.blueship.tri.agent.rm.svc.flow;

import java.util.List;

import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.task.ITaskService;

/**
 * 業務シーケンス（リリース）を実行するためインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IReleaseTaskService extends ITaskService {

    /**
     * 業務シーケンスをタイムライン単位に実行します。
     *
     * @param request 依頼するタイムライン
     * @param bean 業務シーケンスで使用するメッセージ
     *
     * @throws Exception
     */
    public void execute( IBldTimelineEntity request, IReleaseTaskBean bean ) throws Exception;

    /**
     * 業務シーケンスをタイムライン中のシステム単位に実行します。<br>
     * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 * @param request タイムライン全体
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 依頼するライン（システム）
	 * @throws Exception
	 */
    public void execute( IBldTimelineEntity request, IReleaseTaskBean bean , IBldTimelineAgentEntity iLineEntity ) throws Exception;

    /**
	 * リリースの業務シーケンスのプロセステーブルにエラー発生情報を書き込みます。
	 *
	 * @param timeLine 依頼するタイムライン
	 * @param param 業務シーケンスへの入力パラメタ
	 * @param line 依頼するライン（システム）
	 * @param e 例外情報
	 */
	public void writeProcessByIrregular(IBldTimelineEntity timeLine, IReleaseTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception;

    /**
	 * Agentとの疎通チェックを行います。<br>
	 * @throws Exception
	 */
	public List<AgentStatusTask> executeLinkCheck() throws Exception;

	/**
	 * Agentとの疎通チェックを行います。<br>
	 * @throws Exception
	 */
	public List<Object> executeLinkCheckByService( List<Object> paramList ) throws Exception;
}
