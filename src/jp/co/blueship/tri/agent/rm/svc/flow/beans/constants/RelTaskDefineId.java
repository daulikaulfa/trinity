package jp.co.blueship.tri.agent.rm.svc.flow.beans.constants;


/**
 * リリース・タスクで使用する内部環境変数の列挙型です。
 * <p>
 *
 * @author Yukihiro Eguchi
 *
 */
public enum RelTaskDefineId {

	mergeOrder				( "{@mergeOrder}" ),				//マージ順
	buildNo					( "{@buildNo}" ),					//ビルド番号
	relApplyNo				( "{@relApplyNo}" ),				//リリース申請番号
	relNo					( "{@relNo}" ),						//リリース番号
	masterDeletePath		( "{@masterDeletePath}" ),			//原本削除する資産のパス
	masterBinaryDeletePath	( "{@masterBinaryDeletePath}" ),	//原本バイナリ資産削除パス
	lotNo					( "{@lotNo}" ),						//ロット番号
	envNo					( "{@envNo}" ),						//リリース環境番号

	applyNo					( "{@applyNo}" ),					//資産申請番号
	module					( "{@module}" ),					//資産が変更／追加／削除されたモジュール
	delApplyNo				( "{@delApplyNo}" ),				//資産削除申請番号
	buildTag				( "{@buildTag}" ),					//ビルド申請時の原本へのタグ
	workPath				( "{@workPath}" ) ,					//ロットの原本ディレクトリ
	workspace				( "{@workspace}" ) ,				//ロットの作業ディレクトリ
	historyPath				( "{@historyPath}" ) ,				//ロットの履歴格納ディレクトリ
	systemInBox				( "{@systemInBox}" ) ,				//ロットの内部返却ディレクトリ

	homePath				( "{@homePath}" ) ,					//ホームディレクトリ
	historyRelUnitDSLPath	( "{@historyRelUnitDSLPath}" ) ,	//RP DSLパス
	historyRelDSLPath		( "{@historyRelDSLPath}" ) ,		//RC DSLパス
	relApplyAppendFileRootPath		( "{@relApplyAppendFileRootPath}" ) ;			//リリース申請時添付ファイルのルートパス

	

	private String value = null ;

	private RelTaskDefineId( String value ) {
		this.value = value ;
	}

	public String value() {
		return this.value ;
	}

	public static RelTaskDefineId getValue( String value ) {
		for ( RelTaskDefineId charset : values() ) {
			if ( charset.value().equals( value ) ) {
				return charset;
			}
		}

		return null;
	}
}
