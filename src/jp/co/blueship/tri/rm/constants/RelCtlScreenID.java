package jp.co.blueship.tri.rm.constants;

/**
 * リリース管理画面ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelCtlScreenID {
	

	/**
	 * リリーストップ画面
	 */
	public static final String TOP = "RelCtlTop";
	/**
	 * リリース基本情報入力画面
	 */
	public static final String BASE_INFO_INPUT = "RelCtlBaseInfoInput";
	/**
	 * リリース環境選択画面
	 */
	public static final String CONFIGURATION_SELECT = "RelCtlConfigurationSelect";
	/**
	 * ロット選択画面
	 */
	public static final String LOT_SELECT = "RelCtlLotSelect";
	/**
	 * ビルドパッケージ選択画面
	 */
	public static final String UNIT_SELECT = "RelCtlUnitSelect";
	/**
	 * リリース確認画面
	 */
	public static final String ENTRY_CONFIRM = "RelCtlEntryConfirm";
	/**
	 * リリース状況画面
	 */
	public static final String GENERATE_DETAIL_VIEW = "RelCtlGenerateDetailView";
	/**
	 * リリース履歴一覧画面
	 */
	public static final String HISTORY_LIST = "RelCtlHistoryList";
	/**
	 * リリース履歴詳細画面
	 */
	public static final String HISTORY_DETAIL_VIEW = "RelCtlHistoryDetailView";
	/**
	 * ビルドパッケージ詳細画面
	 */
	public static final String UNIT_DETAIL_VIEW = "RelCtlUnitDetailView";
	/**
	 * ビルドパッケージ作成受付完了画面
	 */
	public static final String COMP_REQUEST = "RelCtlCompRequest";

	/**
	 * リリース取消トップ画面
	 */
	public static final String CANCEL_TOP = "RelCtlCancelTop";
	/**
	 * リリース取消一覧画面
	 */
	public static final String CANCEL_LIST = "RelCtlCancelList";
	/**
	 * リリース取消受付確認画面
	 */
	public static final String CANCEL_COMFIRM = "RelCtlCancelConfirm";
	/**
	 * リリース取消受付完了画面
	 */
	public static final String COMP_CANCEL = "RelCtlCompCancel";
}
