package jp.co.blueship.tri.rm.constants;

/**
 * リリース申請管理画面項目名ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009-2012
 */

public class RelApplyScreenItemID {

	/**
	 * リリース申請情報入力
	 */
	public enum RelApplyInput {
		// 申請者
		REL_APPLY_USER("relApplyUser"),
		// 申請責任者
		RESPONSIBLE_USER("responsibleUser"),
		// 関連番号
		RELATION_NO("relationNo"),
		// 備考
		REMARKS("remarks"),
		// 添付ファイル
		APPEND_FILE("appendFile");

		private String itemName;

		RelApplyInput( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * リリース申請クローズ確認
	 */
	public enum RelApplyCloseCheck {
		// クローズコメント
		CLOSE_COMMENT( "closeComment" );

		private String itemName;

		RelApplyCloseCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}
}
