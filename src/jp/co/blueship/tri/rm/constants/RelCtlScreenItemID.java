package jp.co.blueship.tri.rm.constants;

/**
 * リリース管理画面項目名ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class RelCtlScreenItemID {

	/**
	 * リリース基本情報入力
	 */
	public enum RelBaseInfoInput {
		// リリース概要
		SUMMARY("summary"),
		// リリース内容
		CONTENT("content");

		private String itemName;

		RelBaseInfoInput( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * リリース取消確認
	 */
	public enum ReleaseCancelCheck {
		// 取消コメント
		CANCEL_COMMENT( "cancelComment" );

		private String itemName;

		ReleaseCancelCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}
}
