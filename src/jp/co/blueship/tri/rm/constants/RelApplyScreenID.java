package jp.co.blueship.tri.rm.constants;

/**
 * リリース申請管理画面ID
 * <br>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelApplyScreenID {

	/**
	 * リリース申請トップ画面
	 */
	public static final String TOP = "RelApplyTop";
	/**
	 * リリース申請パッケージ選択画面
	 */
	public static final String PACKAGE_LIST_SELECT = "RelApplyPackageListSelect";
	/**
	 * リリース申請画面
	 */
	public static final String ENTRY = "RelApplyEntry";
	/**
	 * リリース申請確認画面
	 */
	public static final String ENTRY_CONFIRM = "RelApplyEntryConfirm";
	/**
	 * リリース申請完了画面
	 */
	public static final String ENTRY_COMP = "RelApplyEntryComp";
	/**
	 * リリース申請保存完了画面
	 */
	public static final String SAVE_COMP = "RelApplyEntrySaveComp";
	/**
	 * リリース申請詳細閲覧画面
	 */
	public static final String DETAIL_VIEW = "RelApplyDetailView";
	/**
	 * リリース申請詳細編集画面
	 */
	public static final String MODIFY = "RelApplyModify";
	/**
	 * リリース申請詳細確認画面
	 */
	public static final String MODIFY_CONFIRM = "RelApplyModifyConfirm";
	/**
	 * リリース申請取消完了画面
	 */
	public static final String CANCEL_COMP = "RelApplyCancelComp";
	/**
	 * リリース申請編集保存完了画面
	 */
	public static final String MODIFY_SAVE_COMP = "RelApplyModifySaveComp";
	/**
	 * リリース申請編集完了画面
	 */
	public static final String MODIFY_COMP = "RelApplyModifyComp";
	/**
	 * リリース申請承認待ち一覧画面
	 */
	public static final String APPROVE_LIST_VIEW = "RelApplyApproveListView";
	/**
	 * リリース申請クローズ待ち一覧画面
	 */
	public static final String CLOSE_LIST_VIEW = "RelApplyCloseListView";
	/**
	 * リリース申請クローズ画面
	 */
	public static final String CLOSE = "RelApplyClose";
	/**
	 * リリース申請クローズ完了画面
	 */
	public static final String CLOSE_COMP = "RelApplyCloseComp";
	/**
	 * リリース申請クローズ詳細画面
	 */
	public static final String CLOSE_DETAIL_VIEW = "RelApplyCloseDetailView";
	/**
	 * リリース申請クローズ履歴一覧画面
	 */
	public static final String CLOSE_HISTORY_LIST_VIEW = "RelApplyCloseHistoryListView";
	/**
	 * リリース申請クローズ履歴詳細画面
	 */
	public static final String CLOSE_HISTORY_DETAIL_VIEW = "RelApplyCloseHistoryDetailView";
}
