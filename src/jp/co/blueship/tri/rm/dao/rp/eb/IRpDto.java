package jp.co.blueship.tri.rm.dao.rp.eb;

import java.util.List;

/**
 * リリースパッケージのDTOのインターフェースです。
 * @author takashi.ono
 *
 */
public interface IRpDto {
	/**
	 * リリースパッケージを取得します。
	 * @return
	 */
	public IRpEntity getRpEntity();
	/**
	 * リリースパッケージを設定します。
	 * @param entity
	 */
	public void setRpEntity( IRpEntity entity );
	/**
	 * RpBpのリンクテーブルリストを取得します。
	 * @return
	 */
	public List<IRpBpLnkEntity> getRpBpLnkEntities();
	/**
	 * RpBpのリンクテーブルリストを設定します。
	 * @param entity
	 */
	public void setRpBpLnkEntities( List<IRpBpLnkEntity> entities );
}
