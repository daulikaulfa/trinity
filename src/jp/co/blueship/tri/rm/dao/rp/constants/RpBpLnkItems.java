package jp.co.blueship.tri.rm.dao.rp.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the release package build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RpBpLnkItems implements ITableItem {
	rpId("rp_id"),
	bpId("bp_id"),
	mergeOdr("merge_odr"),
	rpStsId("rp_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private RpBpLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
