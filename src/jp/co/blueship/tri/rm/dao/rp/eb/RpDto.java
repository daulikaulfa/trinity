package jp.co.blueship.tri.rm.dao.rp.eb;

import java.util.List;

public class RpDto implements IRpDto {

	private IRpEntity rpEntity = null;
	private List<IRpBpLnkEntity> rpBpLnkEntities = null;
	@Override
	public IRpEntity getRpEntity() {
		return rpEntity;
	}

	@Override
	public void setRpEntity(IRpEntity entity) {
		this.rpEntity = entity;
	}

	@Override
	public List<IRpBpLnkEntity> getRpBpLnkEntities() {
		return rpBpLnkEntities;
	}

	@Override
	public void setRpBpLnkEntities(List<IRpBpLnkEntity> entities) {
		this.rpBpLnkEntities = entities;
	}

}
