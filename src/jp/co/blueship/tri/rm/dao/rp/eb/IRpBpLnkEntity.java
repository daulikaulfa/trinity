package jp.co.blueship.tri.rm.dao.rp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release package build package link link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRpBpLnkEntity extends IEntityFooter {

	public String getRpId();
	public void setRpId(String rpId);

	public String getBpId();
	public void setBpId(String bpId);

	public String getMergeOdr();
	public void setMergeOdr(String mergeOdr);

	public String getRpStsId();

}
