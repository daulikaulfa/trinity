package jp.co.blueship.tri.rm.dao.rp.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * release packagek build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RpBpLnkEntity extends EntityFooter implements IRpBpLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * rp-ID
	 */
	public String rpId = null;
	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * merge order
	 */
	public String mergeOdr = null;
	/**
	 * rp-status-ID
	 */
	public String rpStsId = null;

	/**
	 * rp-IDを取得します。
	 * @return rp-ID
	 */
	public String getRpId() {
	    return rpId;
	}
	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 */
	public void setRpId(String rpId) {
	    this.rpId = rpId;
	}
	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}
	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	}
	/**
	 * merge orderを取得します。
	 * @return merge order
	 */
	public String getMergeOdr() {
	    return mergeOdr;
	}
	/**
	 * merge orderを設定します。
	 * @param mergeOdr merge order
	 */
	public void setMergeOdr(String mergeOdr) {
	    this.mergeOdr = mergeOdr;
	}
	/**
	 * rp-sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRpStsId() {
		return rpStsId;
	}
	/**
	 * rp-sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRpStsId(String stsId) {
		this.rpStsId = stsId;
	}

}
