package jp.co.blueship.tri.rm.dao.rp.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.rp.constants.RpBpLnkItems;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;

/**
 * The SQL condition of the release package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class RpCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RP;
	/**
	 * keyword
	 */
	public String[] keywords = null;
	/**
	 * rp-ID
	 */
	public String rpId = null;
	/**
	 * rp-ID's
	 */
	public String[] rpIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;
	/**
	 * release package name
	 */
	public String rpNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * execute user-ID
	 */
	public String execUserId = null;
	/**
	 * execute user name
	 */
	public String execUserNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * release time stamp
	 */
	public Timestamp relTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RaItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public RpCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( RpItems.rpId );
		super.setJoinCtg(true);
		super.setJoinMstone(true);
	}

	@Override
	public ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	/**
	 * keyword
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

		this.keywords = keywords;
		super.append( RpItems.lotId.getItemName() + "Other",
				SqlFormatUtils.joinCondition( true,
						new String[]{
						SqlFormatUtils.getCondition(this.keywords, RpItems.rpId.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, RpItems.rpNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, RpItems.regUserNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, RpItems.summary.getItemName(), true, false, false),

				}));
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( RaItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, RaItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String... procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( RaItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, RaItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * rp-IDを取得します。
	 * @return rp-ID
	 */
	public String getRpId() {
	    return rpId;
	}

	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 */
	public void setRpId(String rpId) {
	    this.rpId = rpId;
	    super.append(RpItems.rpId, rpId );
	}

	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 * @param isLike 部分検索の場合、true
	 */
	public void setRpId(String rpId, boolean isLike) {
		this.rpId = rpId;
		super.append(RpBpLnkItems.rpId.getItemName(), SqlFormatUtils.getCondition(rpId, RpBpLnkItems.rpId.getItemName(), isLike) );
	}

	/**
	 * rp-ID'sを取得します。
	 * @return rp-ID's
	 */
	public String[] getRpIds() {
	    return rpIds;
	}

	/**
	 * rp-ID'sを設定します。
	 * @param rpIds rp-ID's
	 */
	public void setRpIds(String... rpIds) {
	    this.rpIds = rpIds;
	    super.append( RpItems.rpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(rpIds, RpItems.rpId.getItemName(), false, true, false) );
	}

	/**
	 * rp-ID'sを設定します。
	 * @param rpIds rp-ID's
	 */
	public void setRpIdsNotEquals(String... rpIds) {
	    this.rpIds = rpIds;
	    super.append( RpItems.rpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(rpIds, RpItems.rpId.getItemName(), false, false, true) );
	}

	/**
	 * rp-ID'sを設定します。
	 * @param isLike 部分検索の場合、true
	 * @param rpIds rp-ID's
	 */
	public void setRpIds(boolean isLike, String... rpIds ) {
	    this.rpIds = rpIds;
	    super.append( RpItems.rpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(rpIds, RpItems.rpId.getItemName(), isLike, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(RpItems.lotId, lotId );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String... lotIds) {
	    this.lotIds = lotIds;
	    super.append( RpItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, RpItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * release package nameを取得します。
	 * @return release package name
	 */
	public String getRpNm() {
	    return rpNm;
	}

	/**
	 * release package nameを設定します。
	 * @param rpNm release package name
	 */
	public void setRpNm(String rpNm) {
	    this.rpNm = rpNm;
	    super.append(RpItems.rpNm, rpNm );
	}

	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}

	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	    super.append(RpItems.summary, summary );
	}

	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}

	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	    super.append(RpItems.content, content );
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(RpItems.bldEnvId, bldEnvId );
	}

	/**
	 * execute user-IDを取得します。
	 * @return execute user-ID
	 */
	public String getExecUserId() {
	    return execUserId;
	}

	/**
	 * execute user-IDを設定します。
	 * @param execUserId execute user-ID
	 */
	public void setExecUserId(String execUserId) {
	    this.execUserId = execUserId;
	    super.append(RpItems.execUserId, execUserId );
	}

	/**
	 * execute user nameを取得します。
	 * @return execute user name
	 */
	public String getExecUserNm() {
	    return execUserNm;
	}

	/**
	 * execute user nameを設定します。
	 * @param execUserNm execute user name
	 */
	public void setExecUserNm(String execUserNm) {
	    this.execUserNm = execUserNm;
	    super.append(RpItems.execUserNm, execUserNm );
	}

	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(RpItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * PrcoStTimesをFromToで設定します
	 */
	public void setProcStTimeFromTo(String from, String to) {
		super.append(RpItems.procStTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, RpItems.procStTimestamp.getItemName(), true) );
	}

	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(RpItems.procEndTimestamp, procEndTimestamp );
	}

	/**
	 * release time stampを取得します。
	 * @return release time stamp
	 */
	public Timestamp getRelTimestamp() {
	    return relTimestamp;
	}

	/**
	 * release time stampを設定します。
	 * @param relTimestamp release time stamp
	 */
	public void setRelTimestamp(Timestamp relTimestamp) {
	    this.relTimestamp = relTimestamp;
	    super.append(RpItems.relTimestamp, procEndTimestamp );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(RpItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String... stsIds) {
	    this.stsIds = stsIds;
	    super.append( RpItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RpItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}

	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}

	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}

	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}

	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}

	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}

	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}

	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}

	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}

	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	}

	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}

	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	}

	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}

	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	}

	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}

	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RpItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}
	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}
	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}
	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @param mstoneStartDate
	 * @param mstoneEndDate
	 */
	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}
}