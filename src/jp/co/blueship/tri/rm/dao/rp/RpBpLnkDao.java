package jp.co.blueship.tri.rm.dao.rp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.constants.RpBpLnkItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkEntity;

/**
 * The implements of the release package build package link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RpBpLnkDao extends JdbcBaseDao<IRpBpLnkEntity> implements IRpBpLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RP_BP_LNK;
	}

	/**
	 * 絞り込みを最適化するため、特別に基本機能を個別にカスタマイズしています。
	 *
	 */
	private class CustomTemplate extends DaoTemplate {
		private final String name = RmTables.RM_RP_BP_LNK.name();

		private final String SQL_FROM_QUERY =
				" FROM " + name + " A"
				+ " LEFT JOIN (SELECT RP_ID AS R_RP_ID, STS_ID AS RP_STS_ID FROM RM_RP) R ON A.RP_ID = R.R_RP_ID";

		private final String SQL_SELECT_QUERY =
				"SELECT A.*,"
				+ "R.RP_STS_ID"
				+ SQL_FROM_QUERY;

		@Override
		public String toCountQuery(ISqlCondition condition) {
			final String SQL_COUNT_QUERY = "SELECT COUNT(A.*) " + SQL_FROM_QUERY;

			String sql =
					SQL_COUNT_QUERY
						+ condition.toQueryString();

			return sql;
		}

		@Override
		public String toSelectQuery(ISqlCondition condition) {
			StringBuffer buf = new StringBuffer();

			String sql = null;

			buf
			.append("SELECT * FROM ( ")
			.append(SQL_SELECT_QUERY)
			.append(condition.toQueryString())
			.append(") T");

			sql = buf.toString();

			return sql;
		}
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate() );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRpBpLnkEntity entity ) {
		builder
			.append(RpBpLnkItems.rpId, entity.getRpId(), true)
			.append(RpBpLnkItems.bpId, entity.getBpId(), true)
			.append(RpBpLnkItems.mergeOdr, (null == entity.getMergeOdr())? null: Integer.parseInt(entity.getMergeOdr()))
			.append(RpBpLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RpBpLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(RpBpLnkItems.regUserId, entity.getRegUserId())
			.append(RpBpLnkItems.regUserNm, entity.getRegUserNm())
			.append(RpBpLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(RpBpLnkItems.updUserId, entity.getUpdUserId())
			.append(RpBpLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRpBpLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  RpBpLnkEntity entity = new RpBpLnkEntity();

  	  entity.setRpId( rs.getString(RpBpLnkItems.rpId.getItemName()) );
  	  entity.setBpId( rs.getString(RpBpLnkItems.bpId.getItemName()) );
  	  entity.setMergeOdr( Integer.toString(rs.getInt(RpBpLnkItems.mergeOdr.getItemName())) );
  	  entity.setRpStsId( rs.getString(RpBpLnkItems.rpStsId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RpBpLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RpBpLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RpBpLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RpBpLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RpBpLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RpBpLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RpBpLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
