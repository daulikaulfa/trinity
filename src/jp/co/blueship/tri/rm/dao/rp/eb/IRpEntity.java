package jp.co.blueship.tri.rm.dao.rp.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IRpEntity extends IEntityFooter, IEntityExecData {

	public String getRpId();
	public void setRpId(String rpId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getRpNm();
	public void setRpNm(String rpNm);

	public String getSummary();
	public void setSummary(String summary);

	public String getContent();
	public void setContent(String content);

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getExecUserId();
	public void setExecUserId(String execUserId);

	public String getExecUserNm();
	public void setExecUserNm(String execUserNm);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	public Timestamp getRelTimestamp();
	public 	void setRelTimestamp(Timestamp relTimestamp);

	public String getProcId();
	public void setProcId(String procId);

	public String getStsId();
	public 	void setStsId(String stsId);

	public String getProcStsId();

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public String getCloseUserId();
	public void setCloseUserId(String closeUserId);

	public String getCloseUserNm();
	public void setCloseUserNm(String closeUserNm);

	public Timestamp getCloseTimestamp();
	public void setCloseTimestamp(Timestamp closeTimestamp);

	public String getCloseCmt();
	public void setCloseCmt(String closeCmt);

	public String getCtgId();
	public String getCtgNm();
	public String getMstoneId();
	public String getMstoneNm();

}
