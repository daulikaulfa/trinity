package jp.co.blueship.tri.rm.dao.rp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpEntity;

/**
 * The implements of the release agent attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class RpDao extends JdbcBaseDao<IRpEntity> implements IRpDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RP;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRpEntity entity ) {
		builder
			.append(RpItems.rpId, entity.getRpId(), true)
			.append(RpItems.lotId, entity.getLotId())
			.append(RpItems.rpNm, entity.getRpNm())
			.append(RpItems.summary, entity.getSummary())
			.append(RpItems.content, entity.getContent())
			.append(RpItems.bldEnvId, entity.getBldEnvId())
			.append(RpItems.execUserId, entity.getExecUserId())
			.append(RpItems.execUserNm, entity.getExecUserNm())
			.append(RpItems.procStTimestamp, entity.getProcStTimestamp() )
			.append(RpItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(RpItems.procId, entity.getProcId())
			.append(RpItems.relTimestamp, entity.getRelTimestamp())
			.append(RpItems.stsId, entity.getStsId())
			.append(RpItems.delUserId, entity.getDelUserId())
			.append(RpItems.delUserNm, entity.getDelUserNm())
			.append(RpItems.delTimestamp, entity.getDelTimestamp())
			.append(RpItems.delCmt, entity.getDelCmt())
			.append(RpItems.closeUserId, entity.getCloseUserId())
			.append(RpItems.closeUserNm, entity.getCloseUserNm())
			.append(RpItems.closeTimestamp, entity.getCloseTimestamp())
			.append(RpItems.closeCmt, entity.getCloseCmt())
			.append(RpItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RpItems.regTimestamp, entity.getRegTimestamp())
			.append(RpItems.regUserId, entity.getRegUserId())
			.append(RpItems.regUserNm, entity.getRegUserNm())
			.append(RpItems.updTimestamp, entity.getUpdTimestamp())
			.append(RpItems.updUserId, entity.getUpdUserId())
			.append(RpItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRpEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		RpEntity entity = new RpEntity();

		entity.setRpId( rs.getString(RpItems.rpId.getItemName()) );
		entity.setLotId( rs.getString(RpItems.lotId.getItemName()) );
		entity.setRpNm( rs.getString(RpItems.rpNm.getItemName()) );
		entity.setSummary( rs.getString(RpItems.summary.getItemName()) );
		entity.setContent( rs.getString(RpItems.content.getItemName()) );
		entity.setBldEnvId( rs.getString(RpItems.bldEnvId.getItemName()) );
		entity.setExecUserId( rs.getString(RpItems.execUserId.getItemName()) );
		entity.setExecUserNm( rs.getString(RpItems.execUserNm.getItemName()) );
		entity.setProcStTimestamp( rs.getTimestamp(RpItems.procStTimestamp.getItemName()) );
		entity.setProcEndTimestamp( rs.getTimestamp(RpItems.procEndTimestamp.getItemName()) );
		entity.setProcId( rs.getString(RpItems.procId.getItemName()) );
		entity.setRelTimestamp( rs.getTimestamp(RpItems.relTimestamp.getItemName()) );
		entity.setStsId( rs.getString(RpItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(RpItems.procStsId.getItemName()) );
		entity.setDelUserId( rs.getString(RpItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(RpItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(RpItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(RpItems.delCmt.getItemName()) );
		entity.setCloseUserId( rs.getString(RpItems.closeUserId.getItemName()) );
		entity.setCloseUserNm( rs.getString(RpItems.closeUserNm.getItemName()) );
		entity.setCloseTimestamp( rs.getTimestamp(RpItems.closeTimestamp.getItemName()) );
		entity.setCloseCmt( rs.getString(RpItems.closeCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(RpItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(RpItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(RpItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(RpItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(RpItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(RpItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(RpItems.updUserNm.getItemName()) );

		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );

		return entity;
	}

}
