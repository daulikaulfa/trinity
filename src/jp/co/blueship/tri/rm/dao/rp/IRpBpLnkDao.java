package jp.co.blueship.tri.rm.dao.rp;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;


/**
 * The interface of the release package build package link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRpBpLnkDao extends IJdbcDao<IRpBpLnkEntity> {

}
