package jp.co.blueship.tri.rm.dao.rp;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;


/**
 * The interface of the release package DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRpDao extends IJdbcDao<IRpEntity> {

}
