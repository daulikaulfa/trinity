package jp.co.blueship.tri.rm.dao.rp.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the release package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RpItems implements ITableItem {
	rpId("rp_id"),
	lotId("lot_id"),
	rpNm("rp_nm"),
	summary("summary"),
	content("content"),
	bldEnvId("bld_env_id"),
	execUserId("exec_user_id"),
	execUserNm("exec_user_nm"),
	procStTimestamp("proc_st_timestamp"),
	procEndTimestamp("proc_end_timestamp"),
	procId("proc_id"),
	relTimestamp("rel_timestamp"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	closeUserId("close_user_id"),
	closeUserNm("close_user_nm"),
	closeTimestamp("close_timestamp"),
	closeCmt("close_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private RpItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
