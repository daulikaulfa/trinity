package jp.co.blueship.tri.rm.dao.rp.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.rp.constants.RpBpLnkItems;

/**
 * The SQL condition of the release package build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RpBpLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RP_BP_LNK;

	/**
	 * rp-ID
	 */
	public String rpId = null;
	/**
	 * rp-ID's
	 */
	public String[] rpIds = null;
	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * bp-ID's
	 */
	public String[] bpIds = null;
	/**
	 * merge-order
	 */
	public String mergeOdr = null;
	/**
	 * sts-ID
	 */
	public String rpStsId = null;
	/**
	 * sts-ID's
	 */
	public String[] rpStsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RpBpLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RpBpLnkCondition(){
		super(attr);
	}

	/**
	 * rp-IDを取得します。
	 * @return rp-ID
	 */
	public String getRpId() {
	    return rpId;
	}

	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 */
	public void setRpId(String rpId) {
	    this.rpId = rpId;
	    super.append(RpBpLnkItems.rpId, rpId );
	}

	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 * @param isLike 部分検索の場合、true
	 */
	public void setRpId(String rpId,boolean isLike) {
	    this.rpId = rpId;
	    super.append(RpBpLnkItems.rpId.getItemName(), SqlFormatUtils.getCondition(rpId, RpBpLnkItems.rpId.getItemName(), isLike) );
	}

	/**
	 * rp-ID'sを取得します。
	 * @return rp-ID's
	 */
	public String[] getRpIds() {
	    return rpIds;
	}

	/**
	 * rp-ID'sを設定します。
	 * @param rpIds rp-ID's
	 */
	public void setRpIds(String... rpIds) {
	    this.rpIds = rpIds;
	    super.append( RpBpLnkItems.rpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(rpIds, RpBpLnkItems.rpId.getItemName(), false, true, false) );
	}

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}

	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	    super.append(RpBpLnkItems.bpId, bpId );
	}

	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 * @param isLike 部分検索の場合、true
	 */
	public void setBpId(String bpId, boolean isLike) {
		this.bpId = bpId;
		super.append(RpBpLnkItems.bpId.getItemName(), SqlFormatUtils.getCondition(bpId, RpBpLnkItems.bpId.getItemName(), isLike) );
	}

	/**
	 * bp-ID'sを取得します。
	 * @return bp-ID's
	 */
	public String[] getBpIds() {
	    return bpIds;
	}

	/**
	 * bp-ID'sを設定します。
	 * @param bpId bp-ID's
	 */
	public void setBpIds(String... bpIds) {
	    this.bpIds = bpIds;
	    super.append( RpBpLnkItems.bpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bpIds, RpBpLnkItems.bpId.getItemName(), false, true, false) );
	}

	/**
	 * merge-orderを取得します。
	 * @return merge-order
	 */
	public String getMergeOdr() {
	    return mergeOdr;
	}

	/**
	 * merge-orderを設定します。
	 * @param mergeOdr merge-order
	 */
	public void setMergeOdr(String mergeOdr) {
	    this.mergeOdr = mergeOdr;
	    super.append(RpBpLnkItems.mergeOdr, mergeOdr );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RpBpLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRpStsId() {
	    return rpStsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRpStsId(String stsId) {
	    this.rpStsId = stsId;
	    super.append(RpBpLnkItems.rpStsId, stsId );
	}
	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getRpStsIds() {
	    return rpStsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setRpStsIds(String... stsIds) {
	    this.rpStsIds = stsIds;
	    super.append( RpBpLnkItems.rpStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RpBpLnkItems.rpStsId.getItemName(), false, true, false) );
	}

}