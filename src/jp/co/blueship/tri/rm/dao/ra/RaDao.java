package jp.co.blueship.tri.rm.dao.ra;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;

/**
 * The implements of the release agent DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class RaDao extends JdbcBaseDao<IRaEntity> implements IRaDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RA;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRaEntity entity ) {
		builder
			.append(RaItems.raId, entity.getRaId(), true)
			.append(RaItems.lotId, entity.getLotId())
			.append(RaItems.bldEnvId, entity.getBldEnvId())
			.append(RaItems.reqTimestamp, entity.getReqTimestamp())
			.append(RaItems.reqGrpId, entity.getReqGrpId())
			.append(RaItems.reqGrpNm, entity.getReqGrpNm())
			.append(RaItems.reqUserId, entity.getReqUserId())
			.append(RaItems.reqUser, entity.getReqUser())
			.append(RaItems.reqResponsibility, entity.getReqResponsibility())
			.append(RaItems.reqResponsibilityId, entity.getReqResponsibilityId())
			.append(RaItems.preferredRelDate, entity.getPreferredRelDate())
			.append(RaItems.relationId, entity.getRelationId())
			.append(RaItems.stsId, entity.getStsId())
			.append(RaItems.remarks, entity.getRemarks())
			.append(RaItems.closeUserId, entity.getCloseUserId())
			.append(RaItems.closeUserNm, entity.getCloseUserNm())
			.append(RaItems.closeTimestamp, entity.getCloseTimestamp())
			.append(RaItems.closeCmt, entity.getCloseCmt())
			.append(RaItems.delUserId, entity.getDelUserId())
			.append(RaItems.delUserNm, entity.getDelUserNm())
			.append(RaItems.delTimestamp, entity.getDelTimestamp())
			.append(RaItems.delCmt, entity.getDelCmt())
			.append(RaItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RaItems.regTimestamp, entity.getRegTimestamp())
			.append(RaItems.regUserId, entity.getRegUserId())
			.append(RaItems.regUserNm, entity.getRegUserNm())
			.append(RaItems.updTimestamp, entity.getUpdTimestamp())
			.append(RaItems.updUserId, entity.getUpdUserId())
			.append(RaItems.updUserNm, entity.getUpdUserNm())
			.append(RaItems.avlUserId, entity.getAvlUserId())
			.append(RaItems.avlUserNm, entity.getAvlUserNm())
			.append(RaItems.avlTimestamp, entity.getAvlTimestamp())
			.append(RaItems.avlCmt, entity.getAvlCmt())
			.append(RaItems.avlCancelUserId, entity.getAvlCancelUserId())
			.append(RaItems.avlCancelUserNm, entity.getAvlCancelUserNm())
			.append(RaItems.avlCancelTimestamp, entity.getAvlCancelTimestamp())
			.append(RaItems.avlCancelCmt, entity.getAvlCancelCmt())
			;

		return builder;
	}

	@Override
	protected final IRaEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		RaEntity entity = new RaEntity();

		entity.setRaId( rs.getString(RaItems.raId.getItemName()) );
		entity.setLotId( rs.getString(RaItems.lotId.getItemName()) );
		entity.setBldEnvId( rs.getString(RaItems.bldEnvId.getItemName()) );
		entity.setBldEnvNm( rs.getString(RaItems.bldEnvNm.getItemName()) );
		entity.setReqTimestamp( rs.getTimestamp(RaItems.reqTimestamp.getItemName()) );
		entity.setReqGrpId( rs.getString(RaItems.reqGrpId.getItemName()) );
		entity.setReqGrpNm( rs.getString(RaItems.reqGrpNm.getItemName()) );
		entity.setReqUserId( rs.getString(RaItems.reqUserId.getItemName()) );
		entity.setReqUser( rs.getString(RaItems.reqUser.getItemName()) );
		entity.setReqResponsibility( rs.getString(RaItems.reqResponsibility.getItemName()) );
		entity.setReqResponsibilityId( rs.getString(RaItems.reqResponsibilityId.getItemName()) );
		entity.setPreferredRelDate( rs.getString(RaItems.preferredRelDate.getItemName()) );
		entity.setRelationId( rs.getString(RaItems.relationId.getItemName()) );
		entity.setStsId( rs.getString(RaItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setRemarks( rs.getString(RaItems.remarks.getItemName()) );
		entity.setCloseUserId( rs.getString(RaItems.closeUserId.getItemName()) );
		entity.setCloseUserNm( rs.getString(RaItems.closeUserNm.getItemName()) );
		entity.setCloseTimestamp( rs.getTimestamp(RaItems.closeTimestamp.getItemName()) );
		entity.setCloseCmt( rs.getString(RaItems.closeCmt.getItemName()) );
		entity.setDelUserId( rs.getString(RaItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(RaItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(RaItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(RaItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(RaItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(RaItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(RaItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(RaItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(RaItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(RaItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(RaItems.updUserNm.getItemName()) );
		entity.setAvlUserId( rs.getString(RaItems.avlUserId.getItemName()) );
		entity.setAvlUserNm( rs.getString(RaItems.avlUserNm.getItemName()) );
		entity.setAvlTimestamp( rs.getTimestamp(RaItems.avlTimestamp.getItemName()) );
		entity.setAvlCmt( rs.getString(RaItems.avlCmt.getItemName()) );
		entity.setAvlCancelUserId( rs.getString(RaItems.avlCancelUserId.getItemName()) );
		entity.setAvlCancelUserNm( rs.getString(RaItems.avlCancelUserNm.getItemName()) );
		entity.setAvlCancelTimestamp( rs.getTimestamp(RaItems.avlCancelTimestamp.getItemName()) );
		entity.setAvlCancelCmt( rs.getString(RaItems.avlCancelCmt.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  , E.BLD_ENV_NM")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT BLD_ENV_ID AS ENV_ID, BLD_ENV_NM FROM BM_BLD_ENV) E ON BLD_ENV_ID = E.ENV_ID")
			;

			return buf.toString();
		}
	}

}
