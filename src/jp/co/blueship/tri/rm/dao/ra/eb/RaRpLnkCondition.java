package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaRpLnkItems;

/**
 * The SQL condition of the release agent release package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RaRpLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RA_RP_LNK;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * ra-ID's
	 */
	public String[] raIds = null;
	/**
	 * rp-ID
	 */
	public String rpId = null;
	/**
	 * rp-ID's
	 */
	public String[] rpIds = null;
	/**
	 * sts-ID
	 */
	public String raStsId = null;
	/**
	 * sts-ID's
	 */
	public String[] raStsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RaRpLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RaRpLnkCondition(){
		super(attr);
	}

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}

	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	    super.append(RaRpLnkItems.raId, raId );
	}

	/**
	 * ra-ID'sを取得します。
	 * @return ra-ID's
	 */
	public String[] getRaIds() {
	    return raIds;
	}

	/**
	 * ra-ID'sを設定します。
	 * @param raIds ra-ID's
	 */
	public void setRaIds(String[] raIds) {
	    this.raIds = raIds;
	    super.append( RaRpLnkItems.raId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(raIds, RaRpLnkItems.raId.getItemName(), false, true, false) );
	}

	/**
	 * rp-IDを取得します。
	 * @return rp-ID
	 */
	public String getRpId() {
	    return rpId;
	}

	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 */
	public void setRpId(String rpId) {
	    this.rpId = rpId;
	    super.append(RaRpLnkItems.rpId, rpId );
	}

	/**
	 * rp-ID'sを取得します。
	 * @return rp-ID's
	 */
	public String[] getRpIds() {
	    return rpIds;
	}

	/**
	 * rp-ID'sを設定します。
	 * @param rpId rp-ID's
	 */
	public void setRpIds(String[] rpIds) {
	    this.rpIds = rpIds;
	    super.append( RaRpLnkItems.rpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(rpIds, RaRpLnkItems.rpId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RaRpLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRaStsId() {
	    return raStsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRaStsId(String stsId) {
	    this.raStsId = stsId;
	    super.append(RaRpLnkItems.raStsId, stsId );
	}
	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getRaStsIds() {
	    return raStsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setRaStsIds(String... stsIds) {
	    this.raStsIds = stsIds;
	    super.append( RaRpLnkItems.raStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RaRpLnkItems.raStsId.getItemName(), false, true, false) );
	}

}