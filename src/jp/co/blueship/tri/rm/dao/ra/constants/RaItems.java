package jp.co.blueship.tri.rm.dao.ra.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the release agent entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RaItems implements ITableItem {
	raId("ra_id"),
	lotId("lot_id"),
	bldEnvId("bld_env_id"),
	bldEnvNm("bld_env_nm"),
	reqTimestamp("req_timestamp"),
	reqGrpId("req_grp_id"),
	reqGrpNm("req_grp_nm"),
	reqUser("req_user"),
	reqUserId("req_user_id"),
	reqResponsibility("req_responsibility"),
	reqResponsibilityId("req_responsibility_id"),
	preferredRelDate("preferred_rel_date"),
	relationId("relation_id"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	remarks("remarks"),
	closeUserId("close_user_id"),
	closeUserNm("close_user_nm"),
	closeTimestamp("close_timestamp"),
	closeCmt("close_cmt"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	avlUserId("avl_user_id"),
	avlUserNm("avl_user_nm"),
	avlTimestamp("avl_timestamp"),
	avlCmt("avl_cmt"),
	avlCancelUserId("avl_cancel_user_id"),
	avlCancelUserNm("avl_cancel_user_nm"),
	avlCancelTimestamp("avl_cancel_timestamp"),
	avlCancelCmt("avl_cancel_cmt");

	private String element = null;

	private RaItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
