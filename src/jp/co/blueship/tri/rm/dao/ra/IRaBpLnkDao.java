package jp.co.blueship.tri.rm.dao.ra;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;


/**
 * The interface of the release agent build package link  DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaBpLnkDao extends IJdbcDao<IRaBpLnkEntity> {

}
