package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release apply build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaBpLnkEntity extends IEntityFooter {

	public String getRaId();
	public void setRaId(String raId);

	public String getBpId();
	public void setBpId(String bpId);

	public String getBpNm();
	public String getSummary();
	public String getContent();
	public String getRaStsId();

}
