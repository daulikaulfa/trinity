package jp.co.blueship.tri.rm.dao.ra.eb;

import java.util.ArrayList;
import java.util.Collection;
/**
 * Extractで安全にRaDtoのListを取り出すために作成されたクラスです。
 * @author Takashi ono
 *
 */
public class RaDtoList extends ArrayList<IRaDto> {

	/**
	 * 修正後はインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	public RaDtoList(){

	}
	public RaDtoList(Collection<IRaDto> c ){
		super(c);
	}
}
