package jp.co.blueship.tri.rm.dao.ra.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * release apply release package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RaRpLnkEntity extends EntityFooter implements IRaRpLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * rp-ID
	 */
	public String rpId = null;
	/**
	 * release package time stamp
	 */
	public Timestamp relTimestamp = null;
	/**
	 * ra-status-ID
	 */
	public String raStsId = null;
	@Override
	public String getRaId() {
	    return raId;
	}
	@Override
	public void setRaId(String raId) {
	    this.raId = raId;
	}
	@Override
	public String getRpId() {
	    return rpId;
	}
	@Override
	public void setRpId(String rpId) {
	    this.rpId = rpId;
	}
	@Override
	public Timestamp getRelTimestamp() {
	    return relTimestamp;
	}
	@Override
	public void setRelTimestamp(Timestamp relTimestamp) {
	    this.relTimestamp = relTimestamp;
	}
	/**
	 * ra-sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRaStsId() {
		return raStsId;
	}
	/**
	 * ra-sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRaStsId(String stsId) {
		this.raStsId = stsId;
	}

}
