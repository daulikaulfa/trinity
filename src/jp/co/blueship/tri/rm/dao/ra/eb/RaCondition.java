package jp.co.blueship.tri.rm.dao.ra.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;

/**
 * The SQL condition of the release agent attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class RaCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RA;

	/**
	 * keyword
	 */
	public String[] keywords = null;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * ra-ID's
	 */
	public String[] raIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;

	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * request time stamp
	 */
	public Timestamp reqTimestamp = null;
	/**
	 * request grp-ID
	 */
	public String reqGrpId = null;
	/**
	 * request user-ID
	 */
	public String reqUserId = null;
	/**
	 * request user
	 */
	public String reqUser = null;
	/**
	 * request responsibility
	 */
	public String reqResponsibility = null;
	/**
	 * preferred release date
	 */
	public String preferredRelDate = null;
	/**
	 * relation-ID
	 */
	public String relationId = null;
	/**
	 * relation-ID's
	 */
	public String[] relationIds = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;

	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category IDs
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone IDs
	 */
	private String[] mstoneIds = null;

	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RaItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * delete-status-ID's
	 */
	public String[] delStsIds = {StatusFlg.off.value(),StatusFlg.off.value()};
	/**
	 * process-status-ID's
	 */
	public String[] procStsIds = null;

	public RaCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( RaItems.raId );
		super.setJoinCtg(true);
		super.setJoinMstone(true);

	}

	@Override
	public ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	/**
	 * keyword
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

		this.keywords = keywords;
		super.append( RaItems.lotId.getItemName() + "Other",
				SqlFormatUtils.joinCondition( true,
						new String[]{
						SqlFormatUtils.getCondition(this.keywords, RaItems.raId.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, RaItems.regUserNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, RaItems.remarks.getItemName(), true, false, false),

				}));
	}

	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    this.appendByJoinExecData( RaItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, RaItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    this.appendByJoinExecData( RaItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, RaItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}

	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	    this.append(RaItems.raId, raId );
	}

	/**
	 * ra-ID'sを取得します。
	 * @return ra-ID's
	 */
	public String[] getRaIds() {
	    return raIds;
	}

	/**
	 * ra-ID'sを設定します。
	 * @param raIds dept-ID's
	 */
	public void setRaIds(String... raIds) {
	    this.raIds = raIds;
	    this.append( RaItems.raId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(raIds, RaItems.raId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    this.append(RaItems.lotId, lotId );
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    this.append(RaItems.bldEnvId, bldEnvId );
	}

	/**
	 * request time stampを取得します。
	 * @return request time stamp
	 */
	public Timestamp getReqTimestamp() {
	    return reqTimestamp;
	}

	/**
	 * request time stampを設定します。
	 * @param reqTimestamp request time stamp
	 */
	public void setReqTimestamp(Timestamp reqTimestamp) {
	    this.reqTimestamp = reqTimestamp;
	    this.append(RaItems.reqTimestamp, reqTimestamp );
	}

	/**
	 * request time stampをFromToで設定します。
	 * @param from
	 * @param to
	 */
	public void setReqTimeFromTo(Timestamp from, Timestamp to) {
		super.append(RaItems.reqTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, RaItems.reqTimestamp.getItemName()));
	}

	/**
	 * request grp-IDを取得します。
	 * @return request grp-ID
	 */
	public String getReqGrpId() {
	    return reqGrpId;
	}

	/**
	 * request grp-IDを設定します。
	 * @param reqGrpId request grp-ID
	 */
	public void setReqGrpId(String reqGrpId) {
	    this.reqGrpId = reqGrpId;
	    this.append(RaItems.reqGrpId, reqGrpId );
	}

	/**
	 * request user-IDを取得します。
	 * @return request user-ID
	 */
	public String getReqUserId() {
	    return reqUserId;
	}

	/**
	 * request user-IDを設定します。
	 * @param reqUserId request user-ID
	 */
	public void setReqUserId(String reqUserId) {
	    this.reqUserId = reqUserId;
	    this.append(RaItems.reqUserId, reqUserId );
	}

	/**
	 * request userを取得します。
	 * @return request user
	 */
	public String getReqUser() {
	    return reqUser;
	}

	/**
	 * request userを設定します。
	 * @param reqUser request user
	 */
	public void setReqUser(String reqUser) {
	    this.reqUser = reqUser;
	    this.append(RaItems.reqUser, reqUser );
	}

	/**
	 * request responsibilityを取得します。
	 * @return request responsibility
	 */
	public String getReqResponsibility() {
	    return reqResponsibility;
	}

	/**
	 * request responsibilityを設定します。
	 * @param reqResponsibility request responsibility
	 */
	public void setReqResponsibility(String reqResponsibility) {
	    this.reqResponsibility = reqResponsibility;
	    this.append(RaItems.reqResponsibility, reqResponsibility );
	}

	/**
	 * preferred release dateを取得します。
	 * @return preferred release date
	 */
	public String getPreferredRelDate() {
	    return preferredRelDate;
	}
	/**
	 * preferred release dateを設定します。
	 * @param preferredRelDate preferred release date
	 */
	public void setPreferredRelDate(String preferredRelDate) {
	    this.preferredRelDate = preferredRelDate;
	    this.append(RaItems.preferredRelDate, preferredRelDate );
	}

	/**
	 * relation-IDを取得します。
	 * @return relation-ID
	 */
	public String getRelationId() {
	    return relationId;
	}

	/**
	 * relation-IDを設定します。
	 * @param relationId relation-ID
	 */
	public void setRelationId(String relationId) {
	    this.relationId = relationId;
	    this.append(RaItems.relationId, relationId );
	}

	/**
	 * relation-ID'sを取得します。
	 * @return relation-ID's
	 */
	public String[] getRelationIds() {
	    return relationIds;
	}

	/**
	 * relation-ID'sを設定します。
	 * @param relationIds relation-ID's
	 */
	public void setRelationIds(String... relationIds) {
	    this.relationIds = relationIds;
	    this.append( RaItems.relationId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(relationIds, RaItems.relationId.getItemName(), false, true, false) );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String... stsIds) {
	    this.stsIds = stsIds;
	    this.append( RaItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RaItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    this.append(RaItems.stsId, stsId );
	}

	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}

	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	    this.append(RaItems.remarks, remarks );
	}

	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}

	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	    this.append(RaItems.closeUserId, closeUserId );
	}

	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}

	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	    this.append(RaItems.closeUserNm, closeUserNm );
	}

	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}

	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	    this.append(RaItems.closeTimestamp, closeTimestamp );
	}

	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}

	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	    this.append(RaItems.closeCmt, closeCmt );
	}

	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}

	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	    this.append(RaItems.delUserId, delUserId );
	}

	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}

	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	    this.append(RaItems.delUserNm, delUserNm );
	}

	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}

	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	    this.append(RaItems.delTimestamp, delTimestamp );
	}

	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}

	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	    this.append(RaItems.delCmt, delCmt );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    this.append( RaItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String... lotIds) {
	    this.lotIds = lotIds;
	    super.append( RaItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, RaItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}
	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}
}