package jp.co.blueship.tri.rm.dao.ra.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release apply entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaEntity extends IEntityFooter, IEntityExecData {

	public String getRaId();
	public void setRaId(String raId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getBldEnvNm();

	public Timestamp getReqTimestamp();
	public void setReqTimestamp(Timestamp reqTimestamp);

	public String getReqGrpId();
	public void setReqGrpId(String reqGrpId);

	public String getReqGrpNm();
	public void setReqGrpNm(String reqGrpNm);

	public String getReqUserId();
	public void setReqUserId(String reqUserId);

	public String getReqUser();
	public void setReqUser(String reqUser);

	public String getReqResponsibility();
	public void setReqResponsibility(String reqResponsibility);
	
	public String getReqResponsibilityId();
	public void setReqResponsibilityId(String reqResponsibilityId);

	public String getPreferredRelDate();
	public void setPreferredRelDate(String preferredRelDate);

	public String getRelationId();
	public void setRelationId(String relationId);

	public String getStsId();
	public void setStsId(String stsId);

	public String getRemarks();
	public void setRemarks(String remarks);

	public String getCloseUserId();
	public void setCloseUserId(String closeUserId);

	public String getCloseUserNm();
	public void setCloseUserNm(String closeUserNm);

	public Timestamp getCloseTimestamp();
	public void setCloseTimestamp(Timestamp closeTimestamp);

	public String getCloseCmt();
	public void setCloseCmt(String closeCmt);

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public String getProcStsId();
	
	public String getAvlUserId();
	public void setAvlUserId(String avlUserId);
	
	public String getAvlUserNm();
	public void setAvlUserNm(String avlUserNm);
	
	public Timestamp getAvlTimestamp();
	public void setAvlTimestamp(Timestamp avlTimestamp);
	
	public String getAvlCmt();
	public void setAvlCmt(String avlCmt);
	
	public String getAvlCancelUserId();
	public void setAvlCancelUserId(String avlCancelUserId);
	
	public String getAvlCancelUserNm();
	public void setAvlCancelUserNm(String avlCancelUserNm);
	
	public Timestamp getAvlCancelTimestamp();
	public void setAvlCancelTimestamp(Timestamp avlCancelTimestamp);
	
	public String getAvlCancelCmt();
	public void setAvlCancelCmt(String avlCancelCmt);
}
