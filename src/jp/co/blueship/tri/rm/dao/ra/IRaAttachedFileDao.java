package jp.co.blueship.tri.rm.dao.ra;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;


/**
 * The interface of the release agent attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaAttachedFileDao extends IJdbcDao<IRaAttachedFileEntity> {

}
