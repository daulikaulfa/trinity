package jp.co.blueship.tri.rm.dao.ra.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the release agent release package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RaRpLnkItems implements ITableItem {
	raId("ra_id"),
	rpId("rp_id"),
	relTimestamp("rel_Timestamp"),
	raStsId("ra_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private RaRpLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
