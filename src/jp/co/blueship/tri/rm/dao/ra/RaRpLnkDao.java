package jp.co.blueship.tri.rm.dao.ra;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaRpLnkItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkEntity;

/**
 * The implements of the release agent release package link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RaRpLnkDao extends JdbcBaseDao<IRaRpLnkEntity> implements IRaRpLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RA_RP_LNK;
	}

	/**
	 * リリース申請・リリースパッケージレコードは、絞り込みを最適化するため、特別に基本機能を個別にカスタマイズしています。
	 *
	 */
	private class CustomTemplate extends DaoTemplate {
		private final String name = RmTables.RM_RA_RP_LNK.name();

		private final String SQL_FROM_QUERY =
				" FROM " + name + " A"
				+ " LEFT JOIN (SELECT RP_ID AS R_RP_ID, REL_TIMESTAMP FROM RM_RP) R ON A.RP_ID = R.R_RP_ID "
				+ " LEFT JOIN (SELECT RA_ID AS Q_RA_ID, STS_ID AS RA_STS_ID FROM RM_RA) Q ON A.RA_ID = Q.Q_RA_ID";

		private final String SQL_SELECT_QUERY =
				"SELECT A.*,"
				+ "R.REL_TIMESTAMP,"
				+ "Q.RA_STS_ID"
				+ SQL_FROM_QUERY;

		@Override
		public String toCountQuery(ISqlCondition condition) {
			final String SQL_COUNT_QUERY = "SELECT COUNT(A.*) " + SQL_FROM_QUERY;

			String sql =
					SQL_COUNT_QUERY
						+ condition.toQueryString();

			return sql;
		}

		@Override
		public String toSelectQuery(ISqlCondition condition) {
			StringBuffer buf = new StringBuffer();

			String sql = null;

			buf
			.append("SELECT * FROM ( ")
			.append(SQL_SELECT_QUERY)
			.append(condition.toQueryString())
			.append(") T")
			;

			sql = buf.toString();

			return sql;
		}
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate() );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRaRpLnkEntity entity ) {
		builder
			.append(RaRpLnkItems.raId, entity.getRaId(), true)
			.append(RaRpLnkItems.rpId, entity.getRpId(), true)
			.append(RaRpLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RaRpLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(RaRpLnkItems.regUserId, entity.getRegUserId())
			.append(RaRpLnkItems.regUserNm, entity.getRegUserNm())
			.append(RaRpLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(RaRpLnkItems.updUserId, entity.getUpdUserId())
			.append(RaRpLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRaRpLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  RaRpLnkEntity entity = new RaRpLnkEntity();

  	  entity.setRaId( rs.getString(RaRpLnkItems.raId.getItemName()) );
  	  entity.setRpId( rs.getString(RaRpLnkItems.rpId.getItemName()) );
  	  entity.setRelTimestamp( rs.getTimestamp(RaRpLnkItems.relTimestamp.getItemName()) );
  	  entity.setRaStsId( rs.getString(RaRpLnkItems.raStsId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RaRpLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RaRpLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RaRpLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RaRpLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RaRpLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RaRpLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RaRpLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
