package jp.co.blueship.tri.rm.dao.ra.eb;

import java.util.List;

public class RaDto implements IRaDto {

	private IRaEntity raEntity;
	private List<IRaAttachedFileEntity> raAttachedEntities;
	private List<IRaBpLnkEntity> raBpLnkEntities;
	private List<IRaRpLnkEntity> raRpLnkEntities;

	@Override
	public IRaEntity getRaEntity() {
		return this.raEntity;
	}

	@Override
	public void setRaEntity(IRaEntity entity) {
		this.raEntity = entity;
	}

	@Override
	public List<IRaAttachedFileEntity> getRaAttachedFileEntities() {
		return this.raAttachedEntities;
	}

	@Override
	public void setRaAttachedFileEntities(List<IRaAttachedFileEntity> entities) {
		this.raAttachedEntities = entities;
	}

	@Override
	public List<IRaBpLnkEntity> getRaBpLnkEntities() {
	    return raBpLnkEntities;
	}

	@Override
	public void setRaBpLnkEntities(List<IRaBpLnkEntity> raBpLnkEntities) {
	    this.raBpLnkEntities = raBpLnkEntities;
	}

	@Override
	public List<IRaRpLnkEntity> getRaRpLnkEntities() {
	    return raRpLnkEntities;
	}

	@Override
	public void setRaRpLnkEntities(List<IRaRpLnkEntity> raRpLnkEntities) {
	    this.raRpLnkEntities = raRpLnkEntities;
	}

}
