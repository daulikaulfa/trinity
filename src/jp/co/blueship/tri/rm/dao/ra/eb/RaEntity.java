package jp.co.blueship.tri.rm.dao.ra.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * release apply entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RaEntity extends EntityFooter implements IRaEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build env name
	 */
	public String bldEnvNm = null;
	/**
	 * request time stamp
	 */
	public Timestamp reqTimestamp = null;
	/**
	 * request grp-ID
	 */
	public String reqGrpId = null;
	/**
	 * request grp-name
	 */
	public String reqGrpNm = null;
	/**
	 * request user-ID
	 */
	public String reqUserId = null;
	/**
	 * request user
	 */
	public String reqUser = null;
	/**
	 * request responsibility
	 */
	public String reqResponsibility = null;
	/**
	 * request responsibility-ID
	 */
	public String reqResponsibilityId = null;
	/**
	 * preferred release date
	 */
	public String preferredRelDate = null;
	/**
	 * relation-ID
	 */
	public String relationId = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * approved user id
	 */
	public String avlUserId = null;
	/**
	 * approved user name
	 */
	public String avlUserNm = null;
	/**
	 * approved time
	 */
	public Timestamp avlTimestamp = null;
	/**
	 * approved comment
	 */
	public String avlCmt = null;
	/**
	 * cancelled user id
	 */
	public String avlCancelUserId = null;
	/**
	 * cancelled user name
	 */
	public String avlCancelUserNm = null;
	/**
	 * cancelled time
	 */
	public Timestamp avlCancelTimestamp = null;
	/**
	 * cancelled comment
	 */
	public String avlCancelCmt = null;

	/**
	 * request grp-nameを取得します。
	 * @return request grp-name
	 */
	public String getReqGrpNm() {
	    return reqGrpNm;
	}
	/**
	 * request grp-nameを設定します。
	 * @param reqGrpNm request grp-name
	 */
	public void setReqGrpNm(String reqGrpNm) {
	    this.reqGrpNm = reqGrpNm;
	}
	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}
	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * build env nameを取得します。
	 * @return build env name
	 */
	public String getBldEnvNm() {
	    return bldEnvNm;
	}
	/**
	 * build env nameを設定します。
	 * @param bldEnvNm build env name
	 */
	public void setBldEnvNm(String bldEnvNm) {
	    this.bldEnvNm = bldEnvNm;
	}
	/**
	 * request time stampを取得します。
	 * @return request time stamp
	 */
	public Timestamp getReqTimestamp() {
	    return reqTimestamp;
	}
	/**
	 * request time stampを設定します。
	 * @param reqTimestamp request time stamp
	 */
	public void setReqTimestamp(Timestamp reqTimestamp) {
	    this.reqTimestamp = reqTimestamp;
	}
	/**
	 * request grp-IDを取得します。
	 * @return request grp-ID
	 */
	public String getReqGrpId() {
	    return reqGrpId;
	}
	/**
	 * request grp-IDを設定します。
	 * @param reqGrpId request grp-ID
	 */
	public void setReqGrpId(String reqGrpId) {
	    this.reqGrpId = reqGrpId;
	}
	/**
	 * request user-IDを取得します。
	 * @return request user-ID
	 */
	public String getReqUserId() {
	    return reqUserId;
	}
	/**
	 * request user-IDを設定します。
	 * @param reqUserId request user-ID
	 */
	public void setReqUserId(String reqUserId) {
	    this.reqUserId = reqUserId;
	}
	/**
	 * request userを取得します。
	 * @return request user
	 */
	public String getReqUser() {
	    return reqUser;
	}
	/**
	 * request userを設定します。
	 * @param reqUser request user
	 */
	public void setReqUser(String reqUser) {
	    this.reqUser = reqUser;
	}
	/**
	 * request responsibilityを取得します。
	 * @return request responsibility
	 */
	public String getReqResponsibility() {
	    return reqResponsibility;
	}
	/**
	 * request responsibilityを設定します。
	 * @param reqResponsibility request responsibility
	 */
	public void setReqResponsibility(String reqResponsibility) {
	    this.reqResponsibility = reqResponsibility;
	}
	/**
	 * request responsibility idを取得します。
	 * @return request responsibility id
	 */
	public String getReqResponsibilityId() {
		return reqResponsibilityId;
	}
	/**
	 * request responsibility idを設定します。
	 * @param reqResponsibilityId request responsibility id
	 */
	public void setReqResponsibilityId(String reqResponsibilityId) {
		this.reqResponsibilityId = reqResponsibilityId;
	}
	/**
	 * preferred release dateを取得します。
	 * @return preferred release date
	 */
	public String getPreferredRelDate() {
	    return preferredRelDate;
	}
	/**
	 * preferred release dateを設定します。
	 * @param preferredRelDate preferred release date
	 */
	public void setPreferredRelDate(String preferredRelDate) {
	    this.preferredRelDate = preferredRelDate;
	}
	/**
	 * relation-IDを取得します。
	 * @return relation-ID
	 */
	public String getRelationId() {
	    return relationId;
	}
	/**
	 * relation-IDを設定します。
	 * @param relationId relation-ID
	 */
	public void setRelationId(String relationId) {
	    this.relationId = relationId;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * process status-IDを取得します。
	 * @return process status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}
	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	}
	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}
	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	}
	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}
	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	}
	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}
	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	}
	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}
	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}
	
	public String getAvlUserId() {
		return avlUserId;
	}
	
	public void setAvlUserId(String avlUserId) {
		this.avlUserId = avlUserId;
	}
	
	public String getAvlUserNm() {
		return avlUserNm;
	}
	
	public void setAvlUserNm(String avlUserNm) {
		this.avlUserNm = avlUserNm;
	}
	
	public Timestamp getAvlTimestamp() {
		return avlTimestamp;
	}
	
	public void setAvlTimestamp(Timestamp avlTimestamp) {
		this.avlTimestamp = avlTimestamp;
	}
	
	public String getAvlCmt() {
		return avlCmt;
	}
	
	public void setAvlCmt(String avlCmt) {
		this.avlCmt = avlCmt;
	}
	
	public String getAvlCancelUserId() {
		return avlCancelUserId;
	}
	
	public void setAvlCancelUserId(String avlCancelUserId) {
		this.avlCancelUserId = avlCancelUserId;
	}
	
	public String getAvlCancelUserNm() {
		return avlCancelUserNm;
	}
	
	public void setAvlCancelUserNm(String avlCancelUserNm) {
		this.avlCancelUserNm = avlCancelUserNm;
	}
	
	public Timestamp getAvlCancelTimestamp() {
		return avlCancelTimestamp;
	}
	
	public void setAvlCancelTimestamp(Timestamp avlCancelTimestamp) {
		this.avlCancelTimestamp = avlCancelTimestamp;
	}
	
	public String getAvlCancelCmt() {
		return avlCancelCmt;
	}
	
	public void setAvlCancelCmt(String avlCancelCmt) {
		this.avlCancelCmt = avlCancelCmt;
	}
}
