package jp.co.blueship.tri.rm.dao.ra.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release apply release package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaRpLnkEntity extends IEntityFooter {

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId();
	/**
	 * ra-IDを設定します。
	 * @param rpId ra-ID
	 */
	public void setRaId(String raId);

	/**
	 * rp-IDを取得します。
	 * @return rp-ID
	 */
	public String getRpId();
	/**
	 * rp-IDを設定します。
	 * @param rpId rp-ID
	 */
	public void setRpId(String rpId);

	/**
	 * release package time stampを取得します。
	 * @return release package time stamp
	 */
	public Timestamp getRelTimestamp();
	/**
	 * release package time stampを設定します。
	 * @param relTimestamp release package time stamp
	 */
	public void setRelTimestamp(Timestamp regTimestamp);

	/**
	 * ra-Status-IDを取得します。
	 * @return Status-Id
	 */
	public String getRaStsId();

}
