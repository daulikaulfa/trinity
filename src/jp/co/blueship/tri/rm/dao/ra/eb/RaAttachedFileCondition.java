package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaAttachedFileItems;

/**
 * The SQL condition of the release agent build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RaAttachedFileCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RA_ATTACHED_FILE;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * ra-ID's
	 */
	public String[] raIds = null;
	/**
	 * attached file sequential number
	 */
	public String attachedFileSeqNo = null;
	/**
	 * file path
	 */
	public String filePath = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RaAttachedFileItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RaAttachedFileCondition(){
		super(attr);
	}

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}

	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	    super.append(RaAttachedFileItems.raId, raId );
	}

	/**
	 * ra-ID'sを取得します。
	 * @return ra-ID's
	 */
	public String[] getRaAttachedFileIds() {
	    return raIds;
	}

	/**
	 * ra-ID'sを設定します。
	 * @param raIds dept-ID's
	 */
	public void setRaAttachedFileIds(String[] raIds) {
	    this.raIds = raIds;
	    super.append( RaAttachedFileItems.raId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(raIds, RaAttachedFileItems.raId.getItemName(), false, true, false) );
	}

	/**
	 * attached file sequential numberを取得します。
	 * @return attached-file-sequential-number
	 */
	public String getAttachedFileSeqNo() {
	    return attachedFileSeqNo;
	}

	/**
	 * lot-IDを設定します。
	 * @param attachedFileSeqNo attached-file-sequential-number
	 */
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
	    this.attachedFileSeqNo = attachedFileSeqNo;
	    super.append(RaAttachedFileItems.attachedFileSeqNo, attachedFileSeqNo );
	}

	/**
	 * file pathを取得します。
	 * @return file-path
	 */
	public String getFilePath() {
	    return filePath;
	}

	/**
	 * file-pathを設定します。
	 * @param filePath file-path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	    super.append(RaAttachedFileItems.filePath, filePath );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RaAttachedFileItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}