package jp.co.blueship.tri.rm.dao.ra.eb;

import java.util.List;

/**
 * リリース申請のDTOのインターフェースです。
 * @author takashi.ono
 *
 */
public interface IRaDto {
	/**
	 * リリース申請を取得します。
	 * @return
	 */
	public IRaEntity getRaEntity();
	/**
	 * リリース申請を設定します。
	 * @param entity
	 */
	public void setRaEntity( IRaEntity entity );
	/**
	 * 申請添付ファイルを取得します。
	 * @return
	 */
	public List<IRaAttachedFileEntity> getRaAttachedFileEntities();
	/**
	 * 申請添付ファイルを設定します。
	 * @param entities
	 */
	public void setRaAttachedFileEntities( List<IRaAttachedFileEntity> entities );
	/**
	 * リリース申請・ビルドパッケージを取得します。
	 * @return
	 */
	public List<IRaBpLnkEntity> getRaBpLnkEntities();
	/**
	 * リリース申請・ビルドパッケージを設定します。
	 * @param entities
	 */
	public void setRaBpLnkEntities( List<IRaBpLnkEntity> entities );
	/**
	 * リリース申請・ビルドパッケージを取得します。
	 * @return
	 */
	public List<IRaRpLnkEntity> getRaRpLnkEntities();
	/**
	 * リリース申請・リリースパッケージを設定します。
	 * @param entities
	 */
	public void setRaRpLnkEntities( List<IRaRpLnkEntity> entities );

}
