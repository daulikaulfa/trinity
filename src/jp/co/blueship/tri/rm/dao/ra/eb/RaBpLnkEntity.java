package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * release apply build package link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RaBpLnkEntity extends EntityFooter implements IRaBpLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * bp name
	 */
	public String bpNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * ra-status-ID
	 */
	public String raStsId = null;

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}
	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	}
	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}
	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	}
	/**
	 * bp nameを取得します。
	 * @return bp name
	 */
	public String getBpNm() {
	    return bpNm;
	}
	/**
	 * bp nameを設定します。
	 * @param bpNm bp name
	 */
	public void setBpNm(String bpNm) {
	    this.bpNm = bpNm;
	}
	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}
	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	}
	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * ra-sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRaStsId() {
		return raStsId;
	}
	/**
	 * ra-sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRaStsId(String stsId) {
		this.raStsId = stsId;
	}

}
