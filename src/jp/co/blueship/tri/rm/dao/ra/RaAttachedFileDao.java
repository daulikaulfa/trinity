package jp.co.blueship.tri.rm.dao.ra;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaAttachedFileItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileEntity;

/**
 * The implements of the release agent attached file DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RaAttachedFileDao extends JdbcBaseDao<IRaAttachedFileEntity> implements IRaAttachedFileDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RA_ATTACHED_FILE;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRaAttachedFileEntity entity ) {
		builder
			.append(RaAttachedFileItems.raId, entity.getRaId(), true)
			.append(RaAttachedFileItems.attachedFileSeqNo, entity.getAttachedFileSeqNo())
			.append(RaAttachedFileItems.filePath, entity.getFilePath())
			.append(RaAttachedFileItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RaAttachedFileItems.regTimestamp, entity.getRegTimestamp())
			.append(RaAttachedFileItems.regUserId, entity.getRegUserId())
			.append(RaAttachedFileItems.regUserNm, entity.getRegUserNm())
			.append(RaAttachedFileItems.updTimestamp, entity.getUpdTimestamp())
			.append(RaAttachedFileItems.updUserId, entity.getUpdUserId())
			.append(RaAttachedFileItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRaAttachedFileEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  IRaAttachedFileEntity entity = new RaAttachedFileEntity();

  	  entity.setRaId( rs.getString(RaAttachedFileItems.raId.getItemName()) );
  	  entity.setAttachedFileSeqNo( rs.getString(RaAttachedFileItems.attachedFileSeqNo.getItemName()) );
  	  entity.setFilePath( rs.getString(RaAttachedFileItems.filePath.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RaAttachedFileItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RaAttachedFileItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RaAttachedFileItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RaAttachedFileItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RaAttachedFileItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RaAttachedFileItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RaAttachedFileItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
