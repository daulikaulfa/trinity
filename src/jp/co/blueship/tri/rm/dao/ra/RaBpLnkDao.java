package jp.co.blueship.tri.rm.dao.ra;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaBpLnkItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;

/**
 * The implements of the release agent build package link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RaBpLnkDao extends JdbcBaseDao<IRaBpLnkEntity> implements IRaBpLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_RA_BP_LNK;
	}

	/**
	 * リリース申請・ビルドパッケージレコードは、絞り込みを最適化するため、特別に基本機能を個別にカスタマイズしています。
	 *
	 */
	private class CustomTemplate extends DaoTemplate {
		private final String name = RmTables.RM_RA_BP_LNK.name();

		private final String SQL_FROM_QUERY =
				" FROM " + name + " A"
				+ " LEFT JOIN (SELECT BP_ID AS B_BP_ID, BP_NM, SUMMARY, CONTENT FROM BM_BP) B ON A.BP_ID = B.B_BP_ID "
				+ " LEFT JOIN (SELECT RA_ID AS Q_RA_ID, STS_ID AS RA_STS_ID FROM RM_RA) Q ON A.RA_ID = Q.Q_RA_ID";

		private final String SQL_SELECT_QUERY =
				"SELECT A.*,"
				+ "B.BP_NM,"
				+ "B.SUMMARY,"
				+ "B.CONTENT,"
				+ "Q.RA_STS_ID"
				+ SQL_FROM_QUERY;

		@Override
		public String toCountQuery(ISqlCondition condition) {
			final String SQL_COUNT_QUERY = "SELECT COUNT(A.*) " + SQL_FROM_QUERY;

			String sql =
					SQL_COUNT_QUERY
						+ condition.toQueryString();

			return sql;
		}

		@Override
		public String toSelectQuery(ISqlCondition condition) {
			StringBuffer buf = new StringBuffer();

			String sql = null;

			buf
			.append("SELECT * FROM ( ")
			.append(SQL_SELECT_QUERY)
			.append(condition.toQueryString())
			.append(") T")
			;

			sql = buf.toString();

			return sql;
		}
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate() );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRaBpLnkEntity entity ) {
		builder
			.append(RaBpLnkItems.raId, entity.getRaId(), true)
			.append(RaBpLnkItems.bpId, entity.getBpId(), true)
			.append(RaBpLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RaBpLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(RaBpLnkItems.regUserId, entity.getRegUserId())
			.append(RaBpLnkItems.regUserNm, entity.getRegUserNm())
			.append(RaBpLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(RaBpLnkItems.updUserId, entity.getUpdUserId())
			.append(RaBpLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRaBpLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  RaBpLnkEntity entity = new RaBpLnkEntity();

  	  entity.setRaId( rs.getString(RaBpLnkItems.raId.getItemName()) );
  	  entity.setBpId( rs.getString(RaBpLnkItems.bpId.getItemName()) );
  	  entity.setBpNm( rs.getString(RaBpLnkItems.bpNm.getItemName()) );
  	  entity.setSummary( rs.getString(RaBpLnkItems.summary.getItemName()) );
  	  entity.setContent( rs.getString(RaBpLnkItems.content.getItemName()) );
  	  entity.setRaStsId( rs.getString(RaBpLnkItems.raStsId.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(RaBpLnkItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(RaBpLnkItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(RaBpLnkItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(RaBpLnkItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(RaBpLnkItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(RaBpLnkItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(RaBpLnkItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
