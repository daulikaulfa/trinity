package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * release apply attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RaAttachedFileEntity extends EntityFooter implements IRaAttachedFileEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * attached file sequential number
	 */
	public String attachedFileSeqNo = null;
	/**
	 * file path
	 */
	public String filePath = null;

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}
	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	}
	/**
	 * attached file sequential numberを取得します。
	 * @return attached file sequential number
	 */
	public String getAttachedFileSeqNo() {
	    return attachedFileSeqNo;
	}
	/**
	 * attached file sequential numberを設定します。
	 * @param attachedFileSeqNo attached file sequential number
	 */
	public void setAttachedFileSeqNo(String attachedFileSeqNo) {
	    this.attachedFileSeqNo = attachedFileSeqNo;
	}
	/**
	 * file pathを取得します。
	 * @return file path
	 */
	public String getFilePath() {
	    return filePath;
	}
	/**
	 * file pathを設定します。
	 * @param filePath file path
	 */
	public void setFilePath(String filePath) {
	    this.filePath = filePath;
	}

}
