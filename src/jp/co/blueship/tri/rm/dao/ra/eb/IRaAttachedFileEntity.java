package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the release apply attached file entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRaAttachedFileEntity extends IEntityFooter {

	public String getRaId();
	public void setRaId(String raId);

	public String getAttachedFileSeqNo();
	public void setAttachedFileSeqNo(String attachedFileSeqNo);

	public String getFilePath();
	public void setFilePath(String filePath);

}
