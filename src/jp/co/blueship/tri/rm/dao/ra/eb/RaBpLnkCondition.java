package jp.co.blueship.tri.rm.dao.ra.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaBpLnkItems;

/**
 * The SQL condition of the release package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RaBpLnkCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_RA_BP_LNK;

	/**
	 * ra-ID
	 */
	public String raId = null;
	/**
	 * ra-ID's
	 */
	public String[] raIds = null;
	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * bp-ID's
	 */
	public String[] bpIds = null;
	/**
	 * sts-ID
	 */
	public String raStsId = null;
	/**
	 * sts-ID's
	 */
	public String[] raStsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RaBpLnkItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public RaBpLnkCondition(){
		super(attr);
	}

	/**
	 * ra-IDを取得します。
	 * @return ra-ID
	 */
	public String getRaId() {
	    return raId;
	}

	/**
	 * ra-IDを設定します。
	 * @param raId ra-ID
	 */
	public void setRaId(String raId) {
	    this.raId = raId;
	    super.append(RaBpLnkItems.raId, raId );
	}

	/**
	 * ra-ID'sを取得します。
	 * @return ra-ID's
	 */
	public String[] getRaIds() {
	    return raIds;
	}

	/**
	 * ra-ID'sを設定します。
	 * @param raIds ra-ID's
	 */
	public void setRaIds(String[] raIds) {
	    this.raIds = raIds;
	    super.append( RaBpLnkItems.raId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(raIds, RaBpLnkItems.raId.getItemName(), false, true, false) );
	}

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}

	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	    super.append(RaBpLnkItems.bpId, bpId );
	}

	/**
	 * bp-ID'sを取得します。
	 * @return bp-ID's
	 */
	public String[] getBpIds() {
	    return bpIds;
	}

	/**
	 * bp-ID'sを設定します。
	 * @param bpId bp-ID's
	 */
	public void setBpIds(String[] bpIds) {
	    this.bpIds = bpIds;
	    super.append( RaBpLnkItems.bpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bpIds, RaBpLnkItems.bpId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RaBpLnkItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getRaStsId() {
	    return raStsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setRaStsId(String stsId) {
	    this.raStsId = stsId;
	    super.append(RaBpLnkItems.raStsId, stsId );
	}
	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getRaStsIds() {
	    return raStsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setRaStsIds(String... stsIds) {
	    this.raStsIds = stsIds;
	    super.append( RaBpLnkItems.raStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RaBpLnkItems.raStsId.getItemName(), false, true, false) );
	}

}