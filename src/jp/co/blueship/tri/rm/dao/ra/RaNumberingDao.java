package jp.co.blueship.tri.rm.dao.ra;

import java.util.Date;

import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.dao.orm.INumberingDao;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.autonumbering.IAutoNumberingDao;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 * The implements of the Release Package Request numbering DAO.
 *
 * @version V4.02.00
 * @author anh.nguyenduc
 */
public class RaNumberingDao implements INumberingDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private IAutoNumberingDao numberingDao = null;

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param numberingDao アクセスインタフェース
	 */
	public void setAutoNumberingDao( IAutoNumberingDao numberingDao ) {
		this.numberingDao = numberingDao;
	}

	@Override
	public String nextval() {
		String key = RmTables.RM_RA.name();
		try {
			String prefix = DesignSheetFactory.getDesignSheet().getValue( RmDesignEntryKeyByRelApply.numberingPrefix );
			String nowDateFormat = TriDateUtils.getSystemDate( TriDateUtils.getYMDDateFormat( null, TriDateUtils.getDefaultTimeZone() ) );
			Date nowDate = TriDateUtils.getYMDDateFormat().parse(nowDateFormat);

			return prefix
					+ TriDateUtils.getYMD_FLATDateFormat().format(nowDate)
					+ numberingDao.nextval( key, nowDateFormat, "0000" );
		} catch ( Exception e ) {
			throw new TriJdbcDaoException( SmMessageId.SM005008S, e, key );
		}
	}

}
