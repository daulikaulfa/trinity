package jp.co.blueship.tri.rm.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum RmTables implements ITableAttribute {

	RM_CAL( "RC" ),
	RM_RA( "RA" ),
	/**
	 * dummy table for numbering
	 */
	RM_RA_TEMP( "RAT" ),
	RM_RA_ATTACHED_FILE( "RRAF" ),
	RM_RA_BP_LNK( "RRBL" ),
	RM_RA_RP_LNK( "RRRL" ),
	RM_RP( "RR" ),
	RM_RP_BP_LNK( "RRBL" );

	private String alias = null;

	private RmTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static RmTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( RmTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
