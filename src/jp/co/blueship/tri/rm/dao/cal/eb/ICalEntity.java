package jp.co.blueship.tri.rm.dao.cal.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the calender entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ICalEntity extends IEntityFooter {

	public String getPreferredRelDate();
	public void setPreferredRelDate(String preferredRelDate);

}
