package jp.co.blueship.tri.rm.dao.cal;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.rm.dao.cal.constants.CalItems;
import jp.co.blueship.tri.rm.dao.cal.eb.CalEntity;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 * The implements of the calendar DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class CalDao extends JdbcBaseDao<ICalEntity> implements ICalDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return RmTables.RM_CAL;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ICalEntity entity ) {
		builder
			.append(CalItems.preferredRelDate, entity.getPreferredRelDate(), true)
			.append(CalItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(CalItems.regTimestamp, entity.getRegTimestamp())
			.append(CalItems.regUserId, entity.getRegUserId())
			.append(CalItems.regUserNm, entity.getRegUserNm())
			.append(CalItems.updTimestamp, entity.getUpdTimestamp())
			.append(CalItems.updUserId, entity.getUpdUserId())
			.append(CalItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ICalEntity entityMapping( ResultSet rs, int row ) throws SQLException {
  	  ICalEntity entity = new CalEntity();

  	  entity.setPreferredRelDate( rs.getString(CalItems.preferredRelDate.getItemName()) );
  	  entity.setDelStsId( StatusFlg.value(rs.getBoolean(CalItems.delStsId.getItemName())) );
  	  entity.setRegTimestamp( rs.getTimestamp(CalItems.regTimestamp.getItemName()) );
  	  entity.setRegUserId( rs.getString(CalItems.regUserId.getItemName()) );
  	  entity.setRegUserNm( rs.getString(CalItems.regUserNm.getItemName()) );
  	  entity.setUpdTimestamp( rs.getTimestamp(CalItems.updTimestamp.getItemName()) );
  	  entity.setUpdUserId( rs.getString(CalItems.updUserId.getItemName()) );
  	  entity.setUpdUserNm( rs.getString(CalItems.updUserNm.getItemName()) );

  	  return entity;
	}

}
