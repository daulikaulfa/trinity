package jp.co.blueship.tri.rm.dao.cal;

import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;


/**
 * The interface of the calendar DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ICalDao extends IJdbcDao<ICalEntity> {

}
