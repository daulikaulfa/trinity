package jp.co.blueship.tri.rm.dao.cal.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.rm.dao.cal.constants.CalItems;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 * The SQL condition of the calendar entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class CalCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = RmTables.RM_CAL;

	/**
	 * preferred-release-date
	 */
	public String preferredRelDate = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(CalItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public CalCondition(){
		super(attr);
	}

	/**
	 * preferred-release-dateを取得します。
	 * @return preferred-release-date
	 */
	public String getPreferredRelDate() {
	    return preferredRelDate;
	}

	/**
	 * preferred-release-dateを設定します。
	 * @param preferredRelDate preferred-release-date
	 */
	public void SetPreferredRelDate(String preferredRelDate) {
	    this.preferredRelDate = preferredRelDate;
	    super.append(CalItems.preferredRelDate, preferredRelDate );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( CalItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
}