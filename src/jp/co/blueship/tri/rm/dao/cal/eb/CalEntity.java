package jp.co.blueship.tri.rm.dao.cal.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build calendar entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class CalEntity extends EntityFooter implements ICalEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * preferred release date
	 */
	public String preferredRelDate = null;

	/**
	 * preferred release dateを取得します。
	 * @return preferred release date
	 */
	public String getPreferredRelDate() {
	    return preferredRelDate;
	}
	/**
	 * preferred release dateを設定します。
	 * @param preferredRelDate preferred release date
	 */
	public void setPreferredRelDate(String preferredRelDate) {
	    this.preferredRelDate = preferredRelDate;
	}

}
