package jp.co.blueship.tri.rm;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * エンティティへの入出力の共通処理Class
 * EntityAddonUtilの派生クラスです。依存性を解消するために作成しました
 * @author Takashi Ono
 * All Rights Reserved, Copyright(c) Blueship  2009
 */


public class RmEntityAddonUtils {

	/**
	 * リリースエンティティからビルドパッケージ番号を取得する。
	 * @param entities リリースエンティティ
	 * @return ビルドパッケージ番号の配列
	 */
	public static String[] getBuildNo( IRpDto entity ) {

		Set<String> buildNoSet = new TreeSet<String>();
		for( IRpBpLnkEntity build : entity.getRpBpLnkEntities() ) {
			buildNoSet.add( build.getBpId() );
		}

		return (String[])buildNoSet.toArray( new String[0] );
	}
	/**
	 * ビルドパッケージエンティティからビルドパッケージIDの配列を取得する。
	 *
	 * @param entities
	 * @return ビルドパッケージIDの配列
	 */
	public static String[] getBuildNoFromBp( IBpEntity[] entities ){

		Set<String> buildNoSet = new TreeSet<String>();
		for(IBpEntity entity : entities) {
			buildNoSet.add(entity.getBpId());
		}
		return (String[])buildNoSet.toArray(new String[0]);
	}
	/**
	 * リリースエンティティからビルド番号・マージ順のマップを返します。
	 * @param relEntity リリースエンティティ
	 * @return ビルド番号・マージ順のマップ
	 */
	public static Map<String, String> buildNoMergeOrderMap(FlowRelCtlEditSupport support , IRpDto dto ) {

		Map<String, String> mergeOrderMap = new HashMap<String, String>();
		for ( IRpBpLnkEntity build : dto.getRpBpLnkEntities() ) {
			mergeOrderMap.put( build.getBpId(), support.getMergeOdr( dto.getRpEntity().getRpId() , build.getBpId() ) );
		}

		return mergeOrderMap;
	}

	/**
	 * ＲＰエンティティからビルドパッケージ番号を取得する。
	 * @param beans ＲＰエンティティ
	 * @return ビルドパッケージ番号の配列
	 */
	public static String[] getBuildNo( UnitBean[] beans ) {

		if ( null == beans ) {
			return null;
		}

		Set<String> buildNoSet = new TreeSet<String>();
		for( UnitBean bean : beans ) {
			buildNoSet.add( bean.getBuildNo() );
		}

		return (String[])buildNoSet.toArray( new String[0] );
	}

	/**
	 * グループユーザエンティティからグループIDのセットを取得します。
	 * @param groupUserEntitys グループユーザエンティティ
	 * @return グループIDのセット
	 */
	public static Set<String> getGroupIdSet( List<IGrpUserLnkEntity> groupUserEntitys ) {

		Set<String> groupIdSet = new HashSet<String>();
		for ( IGrpUserLnkEntity entity : groupUserEntitys ) {
			groupIdSet.add( entity.getGrpId() );
		}

		return groupIdSet;
	}

	/**
	 * UnitViewBeanをマージ順の昇順にソートします
	 * @param unitViewBeanList
	 */
	public static void sortUnitViewBean( List<UnitViewBean> unitViewBeanList ) {

		Collections.sort( unitViewBeanList,
				new Comparator<UnitViewBean>() {
					public int compare( UnitViewBean obj1, UnitViewBean obj2 ) {
						return obj1.getMergeOrder().compareTo( obj2.getMergeOrder() );
					}
				} );
	}


	/**
	 * UnitBeanをマージ順の昇順にソートします
	 * @param unitViewBeanList
	 */
	public static void sortUnitBean( List<UnitBean> unitBeanList ) {

		Collections.sort( unitBeanList,
				new Comparator<UnitBean>() {
					public int compare( UnitBean obj1, UnitBean obj2 ) {
						return obj1.getMergeOrder().compareTo( obj2.getMergeOrder() );
					}
				} );
	}


	/**
	 * Listをマージ順の昇順にソートします
	 * @param buildArray
	 */
	public static void sortBuildEntity( List<String> buildArray ) {

		Collections.sort( buildArray,
				new Comparator<String>() {
					public int compare( String obj1, String obj2 ) {
						return obj1.compareTo( obj2 );
					}
				} );
	}


}
