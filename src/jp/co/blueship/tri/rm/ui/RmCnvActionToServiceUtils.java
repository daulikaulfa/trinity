package jp.co.blueship.tri.rm.ui;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.EntryConfirmBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;


/**
 * 業務サービスBeanとレスポンスBeanとの相互変換を行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RmCnvActionToServiceUtils {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelCtlSearchBean convertReleaseSearchBean( IRequestInfo reqInfo, RelCtlSearchBean bean ) {

		if ( null == bean )
			bean = new RelCtlSearchBean();

		String searchLotNo = reqInfo.getParameter( "searchLotNo" );
		if ( ! TriStringUtils.isEmpty( searchLotNo ) ) {
			String[] s = searchLotNo.split(",");
			if( null == s  ) {
				bean.setSelectedLotNo( null ) ;
				bean.setSelectedServerNo( null ) ;
			} else if( 1 == s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
				bean.setSelectedServerNo( null ) ;
			} else if( 2 <= s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
				bean.setSelectedServerNo(s[1]) ;
			} else {
				bean.setSelectedLotNo( null ) ;
				bean.setSelectedServerNo( null ) ;
			}
		} else {
			bean.setSelectedLotNo( null ) ;
			bean.setSelectedServerNo( null ) ;
		}

		String searchRelNo = reqInfo.getParameter( "searchRelNo" ) ;
		bean.setSearchRelNo( searchRelNo ) ;

		String searchBuildNo = reqInfo.getParameter( "searchBuildNo" ) ;
		bean.setSearchBuildNo( searchBuildNo ) ;

		String searchSummary = reqInfo.getParameter( "searchSummary" ) ;
		bean.setSearchSummary( searchSummary ) ;

		String searchContent = reqInfo.getParameter( "searchContent" ) ;
		bean.setSearchContent( searchContent ) ;

		String searchPjtNo = reqInfo.getParameter( "searchPjtNo" ) ;
		bean.setSearchPjtNo( searchPjtNo ) ;

		String searchChangeCauseNo = reqInfo.getParameter( "searchChangeCauseNo" ) ;
		bean.setSearchChangeCauseNo( searchChangeCauseNo ) ;

		//String searchStatus = reqInfo.getParameter( "searchStatus" ) ;
		//bean.setSelectedStatus( searchStatus ) ;

		String selectedReleaseCount = reqInfo.getParameter( "searchReleaseCount" ) ;
		bean.setSelectedReleaseCount( selectedReleaseCount ) ;

		return bean;
	}
	public static BaseInfoBean convertInputBaseInfoBean( IRequestInfo reqInfo, FlowRelCtlEntryServiceBean bean ) {

		if ( ScreenType.bussinessException.equals(bean.getScreenType()) ) {
			return bean.getBaseInfoBean();
		}

		BaseInfoBean baseInfo = new BaseInfoBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();

		//リリース申請検索情報
		if( ScreenType.select.equals(bean.getScreenType())
			|| ScreenType.next.equals(bean.getScreenType()) ) {
			BaseInfoRelApplySearchBean searchBean = baseInfo.getRelApplySearchBean();

			String selectedLotNo = reqInfo.getParameter( "selectedLotNo" );
			if ( ! TriStringUtils.isEmpty( selectedLotNo ) ) {
				String[] s = selectedLotNo.split(",");
				if( 0 == s.length  ) {
					// nothing
				} else if( 1 == s.length ) {
					searchBean.setSelectedLotNo( s[0] );
				} else if( 2 <= s.length ) {
					searchBean.setSelectedLotNo( s[0] );
					searchBean.setSelectedServerNo(s[1]);
				}
			}

			String selectedRelEnvNo = reqInfo.getParameter( "selectedRelEnvNo" );
			searchBean.setSelectedRelEnvNo( selectedRelEnvNo );

			String selectedRelWishDate = reqInfo.getParameter( "selectedRelWishDate" );
			searchBean.setSelectedRelWishDate( selectedRelWishDate );

			String selectedRelApplyCount = reqInfo.getParameter( "selectedRelApplyCount" );
			searchBean.setSelectedRelApplyCount( selectedRelApplyCount );
		}

		String openDisplay = reqInfo.getParameter("openDisplay");
		baseInfo.setOpenDisplay( openDisplay );

		//リリース基本情報
		inputBean.setRelSummary(reqInfo.getParameter("relSummary"));
		inputBean.setRelContent(reqInfo.getParameter("relContent"));

		if( ScreenType.next.equals(bean.getScreenType()) ) {
			if ( null != reqInfo.getParameter("selectedRelApplyNo") ) {
				inputBean.setRelApplyNo(reqInfo.getParameter("selectedRelApplyNo"));
			}
		}

		baseInfo.setBaseInfoInputBean(inputBean);

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelBaseInfoInput:referer" );
			LogHandler.debug( log , "  relSummary:=" + reqInfo.getParameter("relSummary") );
			LogHandler.debug( log , "  relContent:=" + reqInfo.getParameter("relContent") );
			LogHandler.debug( log , "  relApplyNo:=" + reqInfo.getParameter("selectedRelApplyNo") );
		}

		return baseInfo;
	}

	public static EntryConfirmBean convertReleaseConfirmBean( ISessionInfo sesInfo ) {

		EntryConfirmBean confirm = new EntryConfirmBean();

		confirm.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
		confirm.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));


		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
		}

		return confirm;
	}

	public static BaseInfoBean convertEntryBaseInfoBean( ISessionInfo sesInfo ) {

		BaseInfoBean baseInfo = new BaseInfoBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();

		inputBean.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
		inputBean.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));
		baseInfo.setBaseInfoInputBean(inputBean);

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
		}

		return baseInfo;
	}

	public static ConfigurationSelectBean convertEntryConfigurationSelectBean( IRequestInfo reqInfo ) {

		ConfigurationSelectBean conf = new ConfigurationSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" );
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return conf;
	}

	public static LotSelectBean convertLotSelectBean( IRequestInfo reqInfo ) {

		LotSelectBean lot = new LotSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" );
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return lot;
	}

	public static UnitSelectBean convertUnitSelectBean( IRequestInfo reqInfo ) {

		UnitSelectBean unit = new UnitSelectBean();

		String savedPageNo = reqInfo.getParameter( "savedPageNo" );
		if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
//			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
		}

		return unit;
	}

	/**
	 * レポート用にチェックを入れたリリース番号を保持する
	 * @param session ISessionInfo
	 * @param reqInfo IRequestInfo
	 * @return 保持されたリリース番号のセット
	 */
	public static Set<String> saveSelectedRelNoForReport(
										ISessionInfo session, IRequestInfo reqInfo ) {

		String forward = reqInfo.getParameter( "forward" );
		String referer = reqInfo.getParameter( "referer" );

		// リリース履歴一覧内遷移、またはリリース履歴詳細画面からの遷移以外はクリア
		if ( !RelCtlScreenID.HISTORY_LIST.equals( referer ) &&
				!RelCtlScreenID.HISTORY_DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		}
		if ( !"register".equals( forward ) && !RelCtlScreenID.HISTORY_LIST.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		}


		// 選択されたリリース番号
		String[] relNoArray				= reqInfo.getParameterValues( "reportRelNo[]" );
		Set<String> tmpSelectedRelNo	= new HashSet<String>();
		if ( null != relNoArray) {
			for ( String pjtNo : relNoArray ) {
				tmpSelectedRelNo.add( pjtNo );
			}
		}

		if ( "register".equals( forward ) && RelCtlScreenID.HISTORY_DETAIL_VIEW.equals( referer )) {
			return tmpSelectedRelNo;
		}


		// 画面に表示されていたリリース番号
		@SuppressWarnings("unchecked")
		Set<String> viewRelNoSet		=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		// 選択済みのリリース番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelNoSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		if ( null == selectedRelNoSet ) {
			selectedRelNoSet = tmpSelectedRelNo;
		}

		if ( null != viewRelNoSet ) {

			// 今回、選択されなかったリリース番号
			Set<String> unSelectedRelNo = new HashSet<String>();
			for ( String relNo : viewRelNoSet ) {

				if ( !tmpSelectedRelNo.contains(relNo )) {
					unSelectedRelNo.add( relNo );
				}
			}

			selectedRelNoSet.addAll		( tmpSelectedRelNo );
			selectedRelNoSet.removeAll	( unSelectedRelNo );
		}
		session.setAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO, selectedRelNoSet );

		return selectedRelNoSet;
	}
	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplySearchBean convertRelApplyTopSearchBean( IRequestInfo reqInfo, RelApplySearchBean bean ) {

		if ( null == bean )
			bean = new RelApplySearchBean();

		String selectedLotNo = reqInfo.getParameter( "selectedLotNo" );
		if ( ! TriStringUtils.isEmpty( selectedLotNo ) ) {
			String[] s = selectedLotNo.split(",");
			if( null == s  ) {
				// nothing
			} else if( 1 == s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
			} else if( 2 <= s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
				bean.setSelectedServerNo(s[1]) ;
			} else {
				// nothing
			}
		}

		String selectedRelEnvNo = reqInfo.getParameter( "selectedRelEnvNo" ) ;
		bean.setSelectedRelEnvNo( selectedRelEnvNo ) ;

		String selectedRelWishDate = reqInfo.getParameter( "selectedRelWishDate" ) ;
		bean.setSelectedRelWishDate( selectedRelWishDate ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplySearchBean convertRelApplyTopSelectBean( IRequestInfo reqInfo, RelApplySearchBean bean ) {

		if ( null == bean )
			bean = new RelApplySearchBean();

		String relApplyNo = reqInfo.getParameter( "searchRelApplyNo" ) ;
		bean.setSearchRelApplyNo( relApplyNo ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplySearchBean convertRelApplyModifySelectBean( IRequestInfo reqInfo, RelApplySearchBean bean ) {

		String selectedGroupId = reqInfo.getParameter( "selectedGroupIdParam" ) ;
		bean.setSelectedGroupId( selectedGroupId ) ;

		String selectedRelEnvNo = reqInfo.getParameter( "selectedRelEnvNoParam" ) ;
		bean.setSelectedRelEnvNo( selectedRelEnvNo ) ;

		String selectedRelWishDate = reqInfo.getParameter( "selectedRelWishDateParam" ) ;
		bean.setSelectedRelWishDate( selectedRelWishDate ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplySearchBean convertRelApplySearchBean( IRequestInfo reqInfo, RelApplySearchBean bean ) {

		if ( null == bean )
			bean = new RelApplySearchBean();

		String selectedLotNo = reqInfo.getParameter( "selectedLotNo" );
		if ( ! TriStringUtils.isEmpty( selectedLotNo ) ) {
			String[] s = selectedLotNo.split(",");
			if( null == s  ) {
				// nothing
			} else if( 1 == s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
			} else if( 2 <= s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
				bean.setSelectedServerNo(s[1]) ;
			} else {
				// nothing
			}
		}

		String selectedRelEnvNo = reqInfo.getParameter( "selectedRelEnvNo" ) ;
		bean.setSelectedRelEnvNo( selectedRelEnvNo ) ;

		String selectedRelWishDate = reqInfo.getParameter( "selectedRelWishDate" ) ;
		bean.setSelectedRelWishDate( selectedRelWishDate ) ;

		String searchRelNo = reqInfo.getParameter( "searchRelNo" ) ;
		bean.setSearchRelNo( searchRelNo ) ;

		String searchBuildNo = reqInfo.getParameter( "searchBuildNo" ) ;
		bean.setSearchBuildNo( searchBuildNo ) ;

		String searchSummary = reqInfo.getParameter( "searchSummary" ) ;
		bean.setSearchSummary( searchSummary ) ;

		String searchContent = reqInfo.getParameter( "searchContent" ) ;
		bean.setSearchContent( searchContent ) ;

		String searchPjtNo = reqInfo.getParameter( "searchPjtNo" ) ;
		bean.setSearchPjtNo( searchPjtNo ) ;

		String searchChangeCauseNo = reqInfo.getParameter( "searchChangeCauseNo" ) ;
		bean.setSearchChangeCauseNo( searchChangeCauseNo ) ;

		String searchRelationNo = reqInfo.getParameter( "searchRelationNo" ) ;
		bean.setSearchRelationNo( searchRelationNo ) ;

		String relApplyNo = reqInfo.getParameter( "searchRelApplyNo" ) ;
		bean.setSearchRelApplyNo( relApplyNo ) ;

		String selectedRelApplyCount = reqInfo.getParameter( "selectedRelApplyCount" ) ;
		bean.setSelectedRelApplyCount( selectedRelApplyCount ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplyPackageListSearchBean convertRelApplyPackageListSearchBean( IRequestInfo reqInfo, RelApplyPackageListSearchBean bean ) {

		if ( null == bean )
			bean = new RelApplyPackageListSearchBean();

		String selectedLotNo = reqInfo.getParameter( "selectedLotNo" );
		if ( ! TriStringUtils.isEmpty( selectedLotNo ) ) {
			String[] s = selectedLotNo.split(",");
			if( null == s  ) {
				// nothing
			} else if( 1 == s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
			} else if( 2 <= s.length ) {
				bean.setSelectedLotNo( s[0] ) ;
				bean.setSelectedServerNo(s[1]) ;
			} else {
				// nothing
			}
		}

		String selectedRelUnitCount = reqInfo.getParameter( "selectedRelUnitCount" ) ;
		bean.setSelectedRelUnitCount( selectedRelUnitCount ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplyPackageListSearchBean convertRelApplyPackageListSelectBean( IRequestInfo reqInfo, RelApplyPackageListSearchBean bean ) {

		String selectedBuildNo = reqInfo.getParameter( "selectedBuildNo" ) ;
		bean.setSelectedBuildNo( selectedBuildNo ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplyPackageListSearchBean convertRelApplyEntrySelectBean( IRequestInfo reqInfo, RelApplyPackageListSearchBean bean ) {

		String selectedGroupId = reqInfo.getParameter( "selectedGroupIdParam" ) ;
		bean.setSelectedGroupId( selectedGroupId ) ;

		String selectedRelEnvNo = reqInfo.getParameter( "selectedRelEnvNoParam" ) ;
		bean.setSelectedRelEnvNo( selectedRelEnvNo ) ;

		String selectedRelWishDate = reqInfo.getParameter( "selectedRelWishDateParam" ) ;
		bean.setSelectedRelWishDate( selectedRelWishDate ) ;

		return bean;
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelApplyEntryViewBean convertRelApplyEntryViewBean( IRequestInfo reqInfo, RelApplyEntryViewBean bean ) {

		if ( null == bean )
			bean = new RelApplyEntryViewBean();

		String relApplyUser = reqInfo.getParameter( "relApplyUserParam" ) ;
		bean.setRelApplyUser( relApplyUser ) ;

		String relApplyPersonCharge = reqInfo.getParameter( "relApplyPersonChargeParam" ) ;
		bean.setRelApplyPersonCharge( relApplyPersonCharge ) ;

		String relatedNo = reqInfo.getParameter( "relatedNoParam" ) ;
		bean.setRelatedNo( relatedNo ) ;

		String remarks = reqInfo.getParameter( "remarksParam" ) ;
		bean.setRemarks( remarks ) ;

		//添付ファイル
		{
			for ( SessionScopeKeyConsts.AppendFileEnum appendFile: SessionScopeKeyConsts.AppendFileEnum.values() ) {
				InputStream stream = (InputStream)reqInfo.getAttribute( appendFile.getFileInputStream() );
				String name = (String)reqInfo.getAttribute( appendFile.getFileName() );

				if ( TriStringUtils.isEmpty(stream) ||  TriStringUtils.isEmpty(name) ) {
					continue;
				}

				RelApplyEntryViewBean.AppendFileBean appendFileBean = bean.newAppendFileBean();
				appendFileBean.setAppendFileInputStreamBytes( StreamUtils.convertInputStreamToBytes( stream ) );
				appendFileBean.setAppendFileInputStreamName( name );
				appendFileBean.setAppendFile( name );

				bean.getAppendFile().put(appendFile.getId(), appendFileBean);

				bean.getShowAppendFile().put( appendFile.getId(), reqInfo.getParameter( "showAppendFile" + appendFile.getId() ) );
			}
		}

		bean.setDelAppendFiles( reqInfo.getParameterValues( "delAppendFile" ) );

		List<String> id = new ArrayList<String>();
		id.addAll( FluentList.from(bean.getDelAppendFiles()).asList());

		return bean;
	}




//	public static BaseInfoBean convertInputBaseInfoBean( IRequestInfo reqInfo ) {
//
//		BaseInfoBean baseInfo = new BaseInfoBean();
//		BaseInfoInputBean inputBean = new BaseInfoInputBean();
//
//		LogHandler.debug( log , "  relSummary:=" + reqInfo.getParameter("relSummary") );
//		LogHandler.debug( log , "  relContent:=" + reqInfo.getParameter("relContent") );
//		inputBean.setRelSummary(reqInfo.getParameter("relSummary"));
//		inputBean.setRelContent(reqInfo.getParameter("relContent"));
//		baseInfo.setBaseInfoInputBean(inputBean);
//
//		if ( log.isDebugEnabled() ) {
//			LogHandler.debug( log , "FlowRelEntry:RelBaseInfoInput:referer" );
//			LogHandler.debug( log , "  relSummary:=" + reqInfo.getParameter("relSummary") );
//			LogHandler.debug( log , "  relContent:=" + reqInfo.getParameter("relContent") );
//		}
//
//		return baseInfo;
//	}
//
//	public static EntryConfirmBean convertReleaseConfirmBean( ISessionInfo sesInfo ) {
//
//		EntryConfirmBean confirm = new EntryConfirmBean();
//
//		confirm.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
//		confirm.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));
//
//
//		if ( log.isDebugEnabled() ) {
//			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
//			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
//			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
//			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
//			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
//			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
//			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
//			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
//		}
//
//		return confirm;
//	}
//
//	public static BaseInfoBean convertEntryBaseInfoBean( ISessionInfo sesInfo ) {
//
//		BaseInfoBean baseInfo = new BaseInfoBean();
//		BaseInfoInputBean inputBean = new BaseInfoInputBean();
//
//		inputBean.setRelSummary((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY));
//		inputBean.setRelContent((String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT));
//		baseInfo.setBaseInfoInputBean(inputBean);
//
//		if ( log.isDebugEnabled() ) {
//			LogHandler.debug( log , "FlowRelEntry:RelEntryBaseInfo:" );
//			LogHandler.debug( log , "  relSummary:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_SUMMARY)  );
//			LogHandler.debug( log , "  relContent:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_CONTENT) );
//			LogHandler.debug( log , "  relCopeSection:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_SECTION) );
//			LogHandler.debug( log , "  relCopeUser:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_COPE_USER) );
//			LogHandler.debug( log , "  distributionForm:=" + (String)sesInfo.getAttribute(SessionScopeKeyConsts.DISTRIBUTION_FORM) );
//			LogHandler.debug( log , "  relApproveUser:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_USER)  );
//			LogHandler.debug( log , "  relApproveDate:=" +  (String)sesInfo.getAttribute(SessionScopeKeyConsts.REL_APPROVE_DATE)  );
//		}
//
//		return baseInfo;
//	}
//
//	public static ConfigurationSelectBean convertEntryConfigurationSelectBean( IRequestInfo reqInfo ) {
//
//		ConfigurationSelectBean conf = new ConfigurationSelectBean();
//
//		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
//		if( ! StringAddonUtil.isNothing( savedPageNo ) ) {
////			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
//		}
//
//		return conf;
//	}
//
//	public static LotSelectBean convertLotSelectBean( IRequestInfo reqInfo ) {
//
//		LotSelectBean lot = new LotSelectBean();
//
//		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
//		if( ! StringAddonUtil.isNothing( savedPageNo ) ) {
////			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
//		}
//
//		return lot;
//	}
//
//	public static UnitSelectBean convertUnitSelectBean( IRequestInfo reqInfo ) {
//
//		UnitSelectBean unit = new UnitSelectBean();
//
//		String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
//		if( ! StringAddonUtil.isNothing( savedPageNo ) ) {
////			conf.setSelectPageNo(Integer.parseInt(savedPageNo));
//		}
//
//		return unit;
//	}
//
	/**
	 * レポート用にチェックを入れたリリース申請番号を保持する
	 * @param session ISessionInfo
	 * @param reqInfo IRequestInfo
	 * @return 保持された申請番号のセット
	 */
	public static Set<String> saveSelectedRelApplyNoForReport(
										ISessionInfo session, IRequestInfo reqInfo ) {

		String forward = reqInfo.getParameter( "forward" );
		String referer = reqInfo.getParameter( "referer" );

		// 詳細画面からの遷移以外はクリア
		if ( !RelApplyScreenID.DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		}
		if ( !"register".equals( forward ) && !RelApplyScreenID.DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		}


		// 選択されたリリース申請番号
		String[] relApplyNoArray				= reqInfo.getParameterValues( "relApplyNo[]" );
		Set<String> tmpSelectedRelApplyNo	= new HashSet<String>();
		if ( null != relApplyNoArray) {
			for ( String relApplyNo : relApplyNoArray ) {
				tmpSelectedRelApplyNo.add( relApplyNo );
			}
		}

		if ( "register".equals( forward ) && RelApplyScreenID.DETAIL_VIEW.equals( referer )) {
			return tmpSelectedRelApplyNo;
		}


		// 画面に表示されていたリリース申請番号
		@SuppressWarnings("unchecked")
		Set<String> viewRelApplyNoSet		=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		// 選択済みのリリース申請番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelApplyNoSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		if ( null == selectedRelApplyNoSet ) {
			selectedRelApplyNoSet = tmpSelectedRelApplyNo;
		}

		if ( null != viewRelApplyNoSet ) {

			// 今回、選択されなかったリリース申請番号
			Set<String> unSelectedRelApplyNo = new HashSet<String>();
			for ( String relNo : viewRelApplyNoSet ) {

				if ( !tmpSelectedRelApplyNo.contains(relNo )) {
					unSelectedRelApplyNo.add( relNo );
				}
			}

			selectedRelApplyNoSet.addAll		( tmpSelectedRelApplyNo );
			selectedRelApplyNoSet.removeAll	( unSelectedRelApplyNo );
		}
		session.setAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO, selectedRelApplyNoSet );

		return selectedRelApplyNoSet;
	}
}
