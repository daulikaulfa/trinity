package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelConfirmViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;

public class RelCtlCancelResponseBean extends BaseResponseBean {
	
	
	
	/** 選択された環境番号 */	
	private String selectedEnvNo = null;
	/** 環境名 */
	private String envName = null;
	/** 選択されたリリース番号 */
	private String selectedRelNoString = null;
	/** リリース編集入力情報 */
	private CancelInfoInputBean releaseInfoInputBean = null;
	/** 申請情報 */
	private List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList = null;
	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;
	
	
	public String getSelectedRelNoString() {
		return selectedRelNoString;
	}
	public void setSelectedRelNoString(String selectedRelNoString) {
		this.selectedRelNoString = selectedRelNoString;
	}
	
	
	public CancelInfoInputBean getReleaseInfoInputBean() {
		return releaseInfoInputBean;
	}
	public void setReleaseInfoInputBean(CancelInfoInputBean releaseInfoInputBean) {
		this.releaseInfoInputBean = releaseInfoInputBean;
	}
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}
	
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public List<CancelConfirmViewBean> getReleaseCancelConfirmViewBeanList() {
		return releaseCancelConfirmViewBeanList;
	}
	public void setReleaseCancelConfirmViewBeanList(
			List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList) {
		this.releaseCancelConfirmViewBeanList = releaseCancelConfirmViewBeanList;
	}
	
	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
	
}
