package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelCtlGenerateBtnResponseBean extends BaseResponseBean {

	private List<List<String>> imagePathList;
	/** メッセージ */
	private List<List<String>> imageMsgList;
	/** タイムラインリスト */
	private List<String> timelineList;

	public List<List<String>> getImagePathList() {
		return imagePathList;
	}

	public void setImagePathList(List<List<String>> imagePathList) {
		this.imagePathList = imagePathList;
	}

	public List<String> getTimelineList() {
		return timelineList;
	}

	public void setTimelineList(List<String> timelineList) {
		this.timelineList = timelineList;
	}

	public List<List<String>> getImageMsgList() {
		return imageMsgList;
	}

	public void setImageMsgList(List<List<String>> imageMsgList) {
		this.imageMsgList = imageMsgList;
	}

}
