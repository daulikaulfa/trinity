package jp.co.blueship.tri.rm.ui.rp.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlDetailViewServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvServiceToActionUtils;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlGenerateDetailViewResponseBean;

public class FlowRelCtlGenerateDetailViewProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelCtlDetailViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelCtlScreenID.GENERATE_DETAIL_VIEW };

	public FlowRelCtlGenerateDetailViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	public FlowRelCtlGenerateDetailViewProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelCtlDetailViewServiceBean bean = new FlowRelCtlDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		bean.setRelNo(reqInfo.getParameter("inRelNo"));
		bean.setSelectedEnvNo(reqInfo.getParameter("inConfigId"));

		//▼DWR用
		sesInfo.setAttribute("inRelNo", reqInfo.getParameter("inRelNo"));
		sesInfo.setAttribute("inConfigId", reqInfo.getParameter("inConfigId"));
		//▲DWR用


		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.RC_GENERATED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID );
		bean.setLockServerId	( selectedServerNo );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		FlowRelCtlDetailViewServiceBean bean = (FlowRelCtlDetailViewServiceBean)getServiceReturnInformation();

		RelCtlGenerateDetailViewResponseBean resBean = RmCnvServiceToActionUtils.convertRelGenerateDetailViewResponseBean( bean ) ;

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID, selectedServerNo );
		}
	}

}
