package jp.co.blueship.tri.rm.ui.rp;

import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitTopMenuProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.ui.rp.beans.FlowRelCtlCancelTopProsecutor;


public class FlowRelCtlCancelTop extends PresentationController {

	
	
	protected void addPresentationProsecutores( PresentationProsecutorManager ppm ) {
		ppm.addPresentationProsecutor( new FlowRelCtlCancelTopProsecutor() );
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		
		if ( null != referer ) {
			if (referer.equals( RelCtlScreenID.CANCEL_TOP )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelCtlCancelTopProsecutor(bbe));
				return;
			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitTopMenuProsecutor(bbe));
	}
	
	@Override
	protected String getForward(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo ) {
		return reqInfo.getParameter("forward");
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		
		String referer = reqInfo.getParameter("referer");

		if ( null != referer ) {
			for (String screenFlow : FlowRelCtlCancelTopProsecutor.screenViewFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "RelUnitTop";
	}
}
