package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;

public class RelCtlUnitSelectResponseBean extends BaseResponseBean {
	/** 一覧表示のリスト */
	private List<UnitViewBean> unitViewBeanList;
	/** ページ番号数 */
	private Integer pageNoNum;
	/** 選択ページ番号 */
	private Integer selectPageNo;
	/** 選択ビルドパッケージ番号 */
	private String selectedBuildNo;
	/** 選択マージ順 */
	private String selectedMergeOrderString = null;
	/** デフォルトマージ順 */
	private String defaultMergeOrderString = null;
	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;
	/** ビルドパッケージの選択可能フラグ */
	private boolean selectable = true;
	/** マージ順のリスト */
	private List<String> mergeOrderList = null;
	/** マージ順とRP番号の区切り文字 */	
	private String mergeOrderSeparatorChar = null;
	/** マージ順で未選択を示す文字 */	
	private String mergeOrderUnSelectedChar = null;
	/** 選択ロット番号 */
	private String selectedLotNo;
	/** 選択サーバ番号 */
	private String selectedServerNo;
	
	
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}
	public List<UnitViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList(List<UnitViewBean> unitViewBeanList) {
		this.unitViewBeanList = unitViewBeanList;
	}
	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public String getSelectedMergeOrderString() {
		return selectedMergeOrderString;
	}
	public void setSelectedMergeOrderString( String[] selectedMergeOrder ) {

		StringBuilder stb = new StringBuilder();
		//stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( selectedMergeOrder ) ) ;
		//stb.append("]");
		this.selectedMergeOrderString = stb.toString();
	}
	
	public String getDefaultMergeOrderString() {
		return defaultMergeOrderString;
	}
	public void setDefaultMergeOrderString( String[] defaultMergeOrder ) {

		StringBuilder stb = new StringBuilder();
		//stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( defaultMergeOrder ) ) ;
		//stb.append("]");
		this.defaultMergeOrderString = stb.toString();
	}
	
	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
	public boolean getSelectable() {
		return selectable;
	}
	public void setSelectable(boolean selectable) {
		this.selectable = selectable;
	}
	
	public List<String> getMergeOrderList() {
		return mergeOrderList;
	}
	public void setMergeOrderList( List<String> mergeOrderList ) {
		this.mergeOrderList = mergeOrderList;
	}
	
	public String getMergeOrderSeparatorChar() {
		return mergeOrderSeparatorChar;
	}
	public void setMergeOrderSeparatorChar( String mergeOrderSeparatorChar ) {
		this.mergeOrderSeparatorChar = mergeOrderSeparatorChar;
	}
	
	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}
	
	public String getMergeOrderUnSelectedChar() {
		return mergeOrderUnSelectedChar;
	}
	public void setMergeOrderUnSelectedChar( String mergeOrderUnSelectedChar ) {
		this.mergeOrderUnSelectedChar = mergeOrderUnSelectedChar;
	}
}
