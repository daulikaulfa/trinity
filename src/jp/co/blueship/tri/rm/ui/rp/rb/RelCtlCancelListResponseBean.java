package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;

public class RelCtlCancelListResponseBean extends BaseResponseBean {
	
	
	
	/** 
	 * 選択された環境番号 
	 */	
	private String selectedEnvNo = null;
	/** 
	 * 環境番号 
	 */
	private String envNo = null ;
	/** 
	 * 環境名 
	 */
	private String envName = null;
	/** 
	 * リリース情報 
	 */
	private List<CtlListViewBean> releaseViewBeanList = null;
	/** 
	 * 選択されたリリース番号 
	 */
	private String selectedRelNoString = null;
	/** 
	 * リリース 詳細検索用Bean 
	 */
	private RelCtlSearchBean relCtlSearchBean = null ;
	
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public RelCtlSearchBean getReleaseSearchBean() {
		return relCtlSearchBean;
	}
	public void setReleaseSearchBean(RelCtlSearchBean relCtlSearchBean) {
		this.relCtlSearchBean = relCtlSearchBean;
	}
	
	public List<CtlListViewBean> getReleaseViewBeanList() {
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(List<CtlListViewBean> releaseViewBeanList) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}
	public String getSelectedRelNoString() {
		return selectedRelNoString;
	}

	public void setSelectedRelNoString( String selectedRelNoString ) {
		
		StringBuilder buf = new StringBuilder();
		
		buf.append( "[" );
		buf.append( selectedRelNoString );
		buf.append( "]" );
		this.selectedRelNoString = buf.toString();
	}
	public String getEnvNo() {
		return envNo;
	}
	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}
}
