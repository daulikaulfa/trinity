package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;

public class RelCtlConfigurationSelectResponseBean extends BaseResponseBean {
	/** 一覧表示のリスト */
	private List<ConfigurationViewBean> confViewBeanList;
	/** ページ番号数 */
	private Integer pageNoNum;
	/** 選択ページ番号 */
	private Integer selectPageNo;
	/** 選択環境番号 */
	private String selectedEnvNo;

	public List<ConfigurationViewBean> getConfViewBeanList() {
		return confViewBeanList;
	}
	public void setConfViewBeanList(List<ConfigurationViewBean> confViewBeanList) {
		this.confViewBeanList = confViewBeanList;
	}
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}
	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}

}
