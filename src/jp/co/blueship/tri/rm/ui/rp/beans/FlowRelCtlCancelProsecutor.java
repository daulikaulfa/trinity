package jp.co.blueship.tri.rm.ui.rp.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvServiceToActionUtils;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlCancelResponseBean;

public class FlowRelCtlCancelProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelCtlCancelService";

	public static final String[] screenFlows = new String[] {
														RelCtlScreenID.CANCEL_COMFIRM,
														RelCtlScreenID.COMP_CANCEL };

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelCtlScreenID.CANCEL_LIST,
														 };

	public FlowRelCtlCancelProsecutor() {
		super( null );
	}

	public FlowRelCtlCancelProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelCtlCancelServiceBean bean = new FlowRelCtlCancelServiceBean();
		bean.setReleaseInfoInputBean( new CancelInfoInputBean() );

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );
		bean.setSelectedEnvNo	( reqInfo.getParameter( "envNo" ));

		if ( forward.equals( RelCtlScreenID.CANCEL_COMFIRM )) {
			bean.setSelectedRelNo	( reqInfo.getParameterValues( "relNo" ));
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId			( userId );
		bean.setUserName(userName());

		if ( referer.equals( RelCtlScreenID.CANCEL_COMFIRM )) {

			CancelInfoInputBean inputBean = new CancelInfoInputBean();
			inputBean.setCancelComment	( reqInfo.getParameter( "cancelComment" ));
			bean.setReleaseInfoInputBean	( inputBean );

			bean.setSelectedRelNo	( TriStringUtils.convertStringToArray( reqInfo.getParameter( "relNo" )));
		}


		// ロック用
		bean.setLockLotNo		( (String)session.getAttribute(SessionScopeKeyConsts.SELECTED_LOT_NO) );
		bean.setLockServerId	( (String)session.getAttribute(SessionScopeKeyConsts.SELECTED_SERVER_ID) );


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );


		FlowRelCtlCancelServiceBean bean = (FlowRelCtlCancelServiceBean)getServiceReturnInformation();

		RelCtlCancelResponseBean resBean = null ;
		//if (forward.equals(RelScreenID.RELEASE_CANCEL_COMFIRM ) ) {
			resBean = RmCnvServiceToActionUtils.convertReleaseCancelResponseBean( bean , reqInfo , session ) ;
		//}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}



	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}
}
