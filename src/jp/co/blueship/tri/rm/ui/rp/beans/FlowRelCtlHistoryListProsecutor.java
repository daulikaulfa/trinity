package jp.co.blueship.tri.rm.ui.rp.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryListServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlHistoryListResponseBean;

public class FlowRelCtlHistoryListProsecutor extends PresentationProsecutor {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final String FLOW_ACTION_ID = "FlowRelCtlHistoryListService";
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelCtlScreenID.TOP,
														RelCtlScreenID.HISTORY_LIST,
														RelCtlScreenID.HISTORY_DETAIL_VIEW };
	public static final String SEARCH_HISTORY_LIST = FLOW_ACTION_ID + "historyList";
	public static final String RESULT_HISTORY_LIST = FLOW_ACTION_ID + "resultList";

	public FlowRelCtlHistoryListProsecutor() {
		super( null );
	}

	public FlowRelCtlHistoryListProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelCtlHistoryListServiceBean bean = new FlowRelCtlHistoryListServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );
		bean.setSelectedEnvNo	( reqInfo.getParameter( "envNo" ));

//		詳細検索の検索条件を同一環境内で保持する
		{
			RelCtlSearchBean releaseSearchBean = (RelCtlSearchBean)session.getAttribute( SEARCH_HISTORY_LIST );

			if ( null == releaseSearchBean ||
					! bean.getSelectedEnvNo().equals( releaseSearchBean.getSessionKey() ) ) {
				session.removeAttribute( SEARCH_HISTORY_LIST );
				bean.setRelCtlSearchBean( null );
			} else {
				bean.setRelCtlSearchBean( releaseSearchBean );
			}

			//[検索]ボタン押下時、検索情報をセッションに保持する
			if ( RelCtlScreenID.HISTORY_LIST.equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setRelCtlSearchBean( RmCnvActionToServiceUtils.convertReleaseSearchBean( reqInfo, bean.getRelCtlSearchBean() ) );
					bean.getRelCtlSearchBean().setSessionKey( bean.getSelectedEnvNo() );

					session.setAttribute( SEARCH_HISTORY_LIST, bean.getRelCtlSearchBean() );
				}
			}
		}

		RmCnvActionToServiceUtils.saveSelectedRelNoForReport( session, reqInfo );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelCtlHistoryListResponseBean resBean = new RelCtlHistoryListResponseBean();
		FlowRelCtlHistoryListServiceBean bean =
			(FlowRelCtlHistoryListServiceBean)getServiceReturnInformation();

		resBean.setSelectedEnvNo		( bean.getSelectedEnvNo() );
		resBean.setEnvName				( bean.getEnvName() );
		resBean.setReleaseViewBeanList	( bean.getReleaseViewBeanList() );
		resBean.setReleaseSearchBean	( bean.getRelCtlSearchBean() );

		session.setAttribute(
				SessionScopeKeyConsts.VIEW_REPORT_NO, getRelNoSet( bean.getReleaseViewBeanList() ));

		// 選択済みのリリース番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelNoSet	=
				(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );

		if ( null != selectedRelNoSet ) {
			resBean.setSelectedRelNoString( (String[])selectedRelNoSet.toArray( new String[0] ));
		}

		resBean.setMergeOrderSeparatorChar(
				sheet.getValue( RmDesignEntryKeyByRelease.mergeOrderSeparatorChar ));

		if ( StatusFlg.on.value().equals( sheet.getValue(
				RmDesignBeanId.multiSelectable, RelCtlScreenID.UNIT_SELECT ))) {
			resBean.setMultiSelectable( true );
		}

		session.setAttribute( RESULT_HISTORY_LIST, bean.getReleaseViewBeanList() );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	/**
	 * リリース番号のセットを取得する
	 * @param releaseViewBeanList ReleaseViewBeanのリスト
	 * @return リリース番号のセット
	 */
	private Set<String> getRelNoSet( List<CtlListViewBean> releaseViewBeanList ) {

		Set<String> relNoSet = new HashSet<String>();
		for ( CtlListViewBean viewBean : releaseViewBeanList ) {
			relNoSet.add( viewBean.getRelNo() );
		}

		return relNoSet;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

}
