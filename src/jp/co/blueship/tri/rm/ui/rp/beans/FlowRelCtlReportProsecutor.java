package jp.co.blueship.tri.rm.ui.rp.beans;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.rp.dto.FlowRelCtlReportServiceBean;
import jp.co.blueship.tri.dcm.ui.rb.ReportRegisterResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlReportProsecutor extends PresentationProsecutor {


	public static final String FLOW_ACTION_ID = "FlowRelCtlReportService";

	public FlowRelCtlReportProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	public FlowRelCtlReportProsecutor() {
		super( null );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelCtlReportServiceBean bean = new FlowRelCtlReportServiceBean();

		bean.setSelectedRelEnvNo( reqInfo.getParameter( "relEnvNo" ) );
		bean.setReportId		( DcmReportType.RmRpReport.value() );

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		Set<String> selectedRelNo =
				RmCnvActionToServiceUtils.saveSelectedRelNoForReport( session, reqInfo );
		bean.setSelectedRelNo	( (String[])selectedRelNo.toArray( new String[0] ));


		// 詳細画面からの遷移
		if ( RelCtlScreenID.HISTORY_DETAIL_VIEW.equals( referer )) {

			bean.setSelectedLotNo		( reqInfo.getParameter( "lotNo" ) );

			bean.setLockLotNo			( reqInfo.getParameter( "lotNo" ));
			bean.setLockServerId		( reqInfo.getParameter( "serverNo" ));
			bean.setLockLotNoArray		( null );
			//bean.setLockServerIdArray	( null );

		} else if ( RelCtlScreenID.HISTORY_LIST.equals( referer ) ) {

			@SuppressWarnings("unchecked")
			List<CtlListViewBean> viewList =
				(List<CtlListViewBean>)session.getAttribute( FlowRelCtlHistoryListProsecutor.RESULT_HISTORY_LIST );


			// ロック用
			Set<String> lotNoList		= new LinkedHashSet<String>();
			Set<String> serverNoList	= new LinkedHashSet<String>();

			for ( CtlListViewBean view : viewList ) {

				if ( selectedRelNo.contains( view.getRelNo() )) {

					lotNoList.add	( view.getLotNo() );
					serverNoList.add( view.getServerNo() );
				}
			}
			bean.setLockLotNo			( null );
			bean.setLockServerId		( null );
			bean.setLockLotNoArray		( (String[])lotNoList.toArray( new String[0] ));
			//bean.setLockServerIdArray	( (String[])serverNoList.toArray( new String[0] ));
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ReportRegisterResponseBean resBean = new ReportRegisterResponseBean();

		FlowRelCtlReportServiceBean bean =
			(FlowRelCtlReportServiceBean)getServiceReturnInformation();

		resBean.setReportNo( bean.getReportNo() );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}
	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}
}
