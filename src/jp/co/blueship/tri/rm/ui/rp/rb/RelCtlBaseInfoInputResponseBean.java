package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean.RelApplyViewBean;

public class RelCtlBaseInfoInputResponseBean extends BaseResponseBean {
	
	/** ステータス */
	//private String statusView;
	
	/** ステータスリスト */
	//private List<String> relStatusList = new ArrayList<String>();
	
	/** リリース申請の詳細検索を行うための検索条件情報 */
	BaseInfoRelApplySearchBean relApplySearchBean = new BaseInfoRelApplySearchBean();

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;
		
	/** リリース概要 */
	private String relSummary;
	
	/** リリース内容 */
	private String relContent;

	/** リリース申請番号 */
	private String relApplyNo;

	/** リリース申請検索項目を表示する／しない */
	private String openDisplay = "0";
	
	public String getRelSummary() {
		return relSummary;
	}

	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getRelContent() {
		return relContent;
	}

	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}

	public BaseInfoRelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}

	public void setRelApplySearchBean(BaseInfoRelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}
	
	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		if ( null == relApplyViewBeanList ) {
			relApplyViewBeanList = new ArrayList<RelApplyViewBean>();
		}
		return relApplyViewBeanList;
	}
	public void setRelApplyViewBeanList(
			List<RelApplyViewBean> relApplyViewBeanList ) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}

	public String getRelApplyNo() {
		return relApplyNo;
	}

	public void setRelApplyNo(String relApplyNo) {
		this.relApplyNo = relApplyNo;
	}

	public String getOpenDisplay() {
		return openDisplay;
	}

	public void setOpenDisplay(String openDisplay) {
		this.openDisplay = openDisplay;
	}
}
