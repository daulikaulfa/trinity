package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlUnitDetailViewServiceBean.ReleaseViewBean;

public class RelCtlUnitDetailViewResponseBean extends BaseResponseBean {

	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** ロット番号 */
	private String lotId = null ;
	/** ロット名 */
	private String lotName = null;
	/** ビルドパッケージ番号 */
	private String selectedBuildNo = null;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	
	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	
}
