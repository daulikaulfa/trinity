package jp.co.blueship.tri.rm.ui.rp.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelCtlCompRequestResponseBean extends BaseResponseBean {

	/** リリース番号 */
	private String relNo = null;

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
}
