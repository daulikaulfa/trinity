package jp.co.blueship.tri.rm.ui.rp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.GenericService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.ReverseAjaxService;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.SecondaryPresentationController;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlDetailViewServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvServiceToActionUtils;

public class FlowRelGenerateDetailViewAjax extends SecondaryPresentationController {
//	private static final Log log = LogFactory.getLog("jp.co.blueship.trinity");

	public static final String FLOW_ACTION_ID = "FlowRelCtlDetailViewService";

	@Override
	protected GenericService getApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((ReverseAjaxService)service).getApplicationService(FLOW_ACTION_ID, serviceDto);
	}

	@Override
	protected GenericServiceBean getApplicationServiceBean(IGeneralServiceBean info) throws Exception {
		FlowRelCtlDetailViewServiceBean bean = (FlowRelCtlDetailViewServiceBean)info;
		return bean;
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		return null;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows(sesInfo, reqInfo);
		// DWRを使用した場合非同期のsubmitが残っていることがあり、本クラスが動作することで遷移後の画面のFLOW_ACTION_IDに影響を及ぼすケースがあるため
		// DWRを使用したクラスでのFLOW_ACTION_IDのセッションへの格納は行わない

		FlowRelCtlDetailViewServiceBean bean = new FlowRelCtlDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		// 画面遷移しないためパラメータが存在しないので、ここでセット
		String referer = reqInfo.getParameter("referer");
		String forward = reqInfo.getParameter("forward");

		if ( referer == null ) referer = RelCtlScreenID.GENERATE_DETAIL_VIEW;
		if ( forward == null ) forward = RelCtlScreenID.GENERATE_DETAIL_VIEW;

		bean.setReferer(referer);
		bean.setForward(forward);

		bean.setRelNo( (String)reqInfo.getAttribute("relNo") );

		CtlDetailViewBean viewBean = new CtlDetailViewBean();
		viewBean.setRelNo( bean.getRelNo() );
		bean.setReleaseDetailViewBean( viewBean );

		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.RC_GENERATED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		bean.setLockServerId( selectedServerNo );
		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID, selectedServerNo );
		}


		return bean;
	}

	@Override
	protected String getForward(IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, IGeneralServiceBean info) {
		return null;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowRelCtlDetailViewServiceBean bean = (FlowRelCtlDetailViewServiceBean)getServiceReturnInformation();

		resBean =	RmCnvServiceToActionUtils.convertRelGenerateDetailViewResponseBean(bean);

		if (resBean != null) {
			resBean.new MessageUtility().reflectMessage(bean);
		}

		return resBean;
	}

	@Override
	protected IService getService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalServiceDWR");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	@Override
	protected String getCallbackJSFuncName() {

		return "analyseCompileResult";
	}

	public IBaseResponseBean start(HttpServletRequest request, HttpServletResponse response, String relNoParam) throws Exception{
		request.setAttribute("relNo", relNoParam);
		IBaseResponseBean baseBean = super.execute(request, response);
		return baseBean;
	}

}
