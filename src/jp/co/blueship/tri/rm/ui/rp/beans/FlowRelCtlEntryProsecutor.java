package jp.co.blueship.tri.rm.ui.rp.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.EntryConfirmBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;
import jp.co.blueship.tri.rm.ui.RmCnvServiceToActionUtils;


public class FlowRelCtlEntryProsecutor extends PresentationProsecutor {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final String FLOW_ACTION_ID = "FlowRelCtlEntryService";

	public static final String[] screenFlows = new String[] {
														RelCtlScreenID.BASE_INFO_INPUT,
														RelCtlScreenID.LOT_SELECT,
														RelCtlScreenID.CONFIGURATION_SELECT,
														RelCtlScreenID.UNIT_SELECT,
														RelCtlScreenID.ENTRY_CONFIRM,
														RelCtlScreenID.COMP_REQUEST };

	public FlowRelCtlEntryProsecutor() {
		super(null);
	}

	public FlowRelCtlEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);
		FlowRelCtlEntryServiceBean seBean = (FlowRelCtlEntryServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelCtlEntryServiceBean bean = null;
		if (null == seBean) {
			bean = new FlowRelCtlEntryServiceBean();
		} else {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		bean.setMergeOrderSeparatorChar	(
				sheet.getValue( RmDesignEntryKeyByRelease.mergeOrderSeparatorChar ));
		bean.setMergeOrderUnSelectedChar(
				sheet.getValue( RmDesignEntryKeyByRelease.mergeOrderUnSelectedChar ));
		if ( StatusFlg.on.value().equals( sheet.getValue(
				RmDesignBeanId.multiSelectable, RelCtlScreenID.UNIT_SELECT ))) {
			bean.setMultiSelectable( true );
		}

		if ( referer.equals( RelCtlScreenID.TOP ) ) {
			//トップ画面からの遷移

		} else if (referer.equals(RelCtlScreenID.BASE_INFO_INPUT)) {
			//基本情報入力画面からの遷移

			BaseInfoBean baseInfo = RmCnvActionToServiceUtils.convertInputBaseInfoBean(reqInfo, bean);
			bean.setBaseInfoBean(baseInfo);
			session.setAttribute(SessionScopeKeyConsts.REL_SUMMARY, reqInfo.getParameter("relSummary"));
			session.setAttribute(SessionScopeKeyConsts.REL_CONTENT, reqInfo.getParameter("relContent"));

			if ( ScreenType.next.equals( screenType )
				&& ! TriStringUtils.isEmpty( baseInfo.getBaseInfoInputBean().getRelApplyNo() ) ) {

				if ( null != baseInfo.getRelApplySearchBean().getSelectedServerNo() ) {
					BaseInfoRelApplySearchBean searchBean = baseInfo.getRelApplySearchBean();

					bean.setSelectedLotNo	( searchBean.getSelectedLotNo() );
					bean.setLockLotNo		( searchBean.getSelectedLotNo() );
					bean.setSelectedServerNo( searchBean.getSelectedServerNo() );
					bean.setLockServerId	( searchBean.getSelectedServerNo());
				}
			}

		} else if (referer.equals(RelCtlScreenID.CONFIGURATION_SELECT)) {
			//環境選択画面からの遷移

			if ( null != reqInfo.getParameter("selectedEnvNo") ) {
				bean.setSelectedEnvNo(reqInfo.getParameter("selectedEnvNo"));
			}

		} else if (referer.equals(RelCtlScreenID.LOT_SELECT)) {
			//ロット選択画面からの遷移

			String[] lotNoServerNo	= null;

			if ( null != reqInfo.getParameterValues( "selectedLotNo" )) {
				String values = reqInfo.getParameter( "selectedLotNo" );
				lotNoServerNo = values.split( "," );
			}

			if ( null != lotNoServerNo ) {
				bean.setSelectedLotNo	( lotNoServerNo[0] );
				bean.setLockLotNo		( lotNoServerNo[0] );
				bean.setSelectedServerNo( lotNoServerNo[1] );
				bean.setLockServerId	( lotNoServerNo[1] );
			} else if ( TriStringUtils.isEmpty( bean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo() ) ) {
				bean.setSelectedLotNo	( null );
				bean.setLockLotNo		( null );
				bean.setSelectedServerNo( null );
				bean.setLockServerId	( null );
			}

		} else if ( referer.equals(RelCtlScreenID.UNIT_SELECT )) {
			//RP選択画面からの遷移

			if ( null != reqInfo.getParameter("selectedBuildNo[]") ) {

				String[] selectedUnitBeanArray = reqInfo.getParameterValues( "selectedBuildNo[]" );
				List<UnitBean> selectedUnitBeanList	= new ArrayList<UnitBean>();
				List<UnitBean> viewUnitBeanList		= new ArrayList<UnitBean>();

				for ( String selectedUnitBeanStr : selectedUnitBeanArray ) {

					String separator	= bean.getMergeOrderSeparatorChar();
					String unSelect		= bean.getMergeOrderUnSelectedChar();

					UnitBean selectedUnitBean = new UnitBean();

					// 複数選択
					if ( -1 < selectedUnitBeanStr.indexOf( separator ) ) {

						String[] unitBeans = selectedUnitBeanStr.split( separator );

						if ( !unSelect.equals( unitBeans[0] )) {
							selectedUnitBean.setMergeOrder	( unitBeans[0] );
							selectedUnitBean.setBuildNo		( unitBeans[1] );
						}

						UnitBean viewUnitBean = new UnitBean();
						viewUnitBean.setMergeOrder	( unitBeans[0] );
						viewUnitBean.setBuildNo		( unitBeans[1] );

						viewUnitBeanList.add( viewUnitBean );

					// ラジオボタン
					} else {
						selectedUnitBean.setBuildNo( selectedUnitBeanStr );
					}

					if ( !TriStringUtils.isEmpty( selectedUnitBean.getBuildNo() )) {
						selectedUnitBeanList.add( selectedUnitBean );
					}
				}

				bean.setSelectedUnitBean( (UnitBean[])selectedUnitBeanList.toArray( new UnitBean[0] ) );
				bean.setViewUnitBean	( (UnitBean[])viewUnitBeanList.toArray( new UnitBean[0] ) );
			}

		} else if ( referer.equals( RelCtlScreenID.ENTRY_CONFIRM )) {
			//作成確認画面からの遷移

			EntryConfirmBean confirm = RmCnvActionToServiceUtils.convertReleaseConfirmBean(session);
			bean.setReleaseConfirmBean(confirm);

		} else if (referer.equals(RelCtlScreenID.COMP_REQUEST)) {
			//作成依頼完了画面からの遷移

		}

		//ロットごとの排他設定を初期化する
		{
			if ( forward.equals( RelCtlScreenID.BASE_INFO_INPUT ) ) {

				bean.setLockLotNo	( null );
				bean.setLockServerId( null );
			}

			if ( forward.equals( RelCtlScreenID.CONFIGURATION_SELECT ) ||
				 forward.equals( RelCtlScreenID.LOT_SELECT ) ){

				if ( TriStringUtils.isEmpty( bean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo() ) ) {

					bean.setLockLotNo	( null );
					bean.setLockServerId( null );
				}
			}
		}

		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		BaseResponseBean retBean = null;
		String forward;

		FlowRelCtlEntryServiceBean bean = (FlowRelCtlEntryServiceBean) getServiceReturnInformation();

		forward = bean.getForward();

		if (forward.equals(RelCtlScreenID.BASE_INFO_INPUT)) {

			retBean = RmCnvServiceToActionUtils.convertRelBaseInfoInputResponseBean(bean, session);
//
//			bean.setLockLotNo	( null );
//			bean.setLockServerId( null );

		} else if (forward.equals(RelCtlScreenID.CONFIGURATION_SELECT)) {

			retBean = RmCnvServiceToActionUtils.convertRelConfigurationSelectResponseBean(bean, reqInfo, session);
//
//			bean.setLockLotNo	( null );
//			bean.setLockServerId( null );

		} else if (forward.equals(RelCtlScreenID.LOT_SELECT)) {

			retBean = RmCnvServiceToActionUtils.convertRelLotSelectResponseBean(bean, reqInfo, session);
//
//			bean.setLockLotNo	( null );
//			bean.setLockServerId( null );

		} else if ( forward.equals(RelCtlScreenID.UNIT_SELECT )) {

			retBean = RmCnvServiceToActionUtils.convertRelUnitResourceSelectResponseBean(bean, reqInfo, session);

		} else if ( forward.equals(RelCtlScreenID.ENTRY_CONFIRM )) {

			retBean = RmCnvServiceToActionUtils.convertRelEditConfirmResponseBean(bean, reqInfo, session);

		} else if (forward.equals(RelCtlScreenID.COMP_REQUEST)) {

			retBean = RmCnvServiceToActionUtils.convertRelCompRequestResponseBean(bean, reqInfo, session);

			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_LOT_NO, bean.getSelectedLotNo() );
			session.setAttribute( SessionScopeKeyConsts.RC_GENERATED_SERVER_ID, bean.getLockServerId() );

		}

		if ( null != retBean ) {
			retBean.new MessageUtility().reflectMessage( bean );
			retBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return retBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute(forward, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute(referer, screenFlows) )
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
