package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryDetailViewServiceBean.BaseInfoViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryDetailViewServiceBean.EnvViewBean;


public class RelCtlHistoryDetailViewResponseBean extends BaseResponseBean {
	
	
	
	/** リリース基本情報 */
	private BaseInfoViewBean baseInfoViewBean = null;
	/** ビルドパッケージ情報 */
	private List<UnitViewBean> unitViewBeanList = null;
	/** リリース環境情報 */
	private EnvViewBean envViewBean = null;
	/** ロット情報 */
	private LotViewBean lotViewBean = null;
	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;
	

	public BaseInfoViewBean getBaseInfoViewBean() {
		return baseInfoViewBean;
	}
	public void setBaseInfoViewBean( BaseInfoViewBean baseInfoViewBean ) {
		this.baseInfoViewBean = baseInfoViewBean;
	}
	
	public List<UnitViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}
	
	public EnvViewBean getEnvViewBean() {
		return envViewBean;
	}
	public void setEnvViewBean( EnvViewBean envViewBean ) {
		this.envViewBean = envViewBean;
	}
	
	public LotViewBean getLotViewBean() {
		return lotViewBean;
	}
	public void setLotViewBean( LotViewBean lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}
	
	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
}
