package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlTopMenuServiceBean.ReleaseViewBean;

public class RelCtlTopResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	/** マージ順とRP番号の区切り文字 */	
	private String mergeOrderSeparatorChar = null;
	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	
	public String getMergeOrderSeparatorChar() {
		return mergeOrderSeparatorChar;
	}
	public void setMergeOrderSeparatorChar( String mergeOrderSeparatorChar ) {
		this.mergeOrderSeparatorChar = mergeOrderSeparatorChar;
	}
	
	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
	
}
