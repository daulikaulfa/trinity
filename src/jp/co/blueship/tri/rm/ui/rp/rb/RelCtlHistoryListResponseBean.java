package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;

public class RelCtlHistoryListResponseBean extends BaseResponseBean {

	
	
	/** 環境番号 */
	private String selectedEnvNo = null;
	/** リリースパッケージ情報 */
	private List<CtlListViewBean> releaseViewBeanList = null;
	/** 選択変リリース番号(レポート用) */
	private String selectedRelNoString = null;
	/** 
	 * リリース 詳細検索用Bean 
	 */
	private RelCtlSearchBean relCtlSearchBean = null ;
	/** 環境名 */
	private String envName = null ;
	/**
	 * マージ順とRP番号の区切り文字
	 */	
	private String mergeOrderSeparatorChar = null;
	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;

	
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}
	
	public List<CtlListViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<CtlListViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<CtlListViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	
	public String getSelectedRelNoString() {
		return selectedRelNoString;
	}

	public void setSelectedRelNoString( String[] relNoArray ) {
		
		StringBuilder stb = new StringBuilder();
		stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( relNoArray ) ) ;
		stb.append("]");
		this.selectedRelNoString = stb.toString();
	}
	public RelCtlSearchBean getReleaseSearchBean() {
		return relCtlSearchBean;
	}
	public void setReleaseSearchBean(RelCtlSearchBean relCtlSearchBean) {
		this.relCtlSearchBean = relCtlSearchBean;
	}
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	
	public String getMergeOrderSeparatorChar() {
		return mergeOrderSeparatorChar;
	}
	public void setMergeOrderSeparatorChar( String mergeOrderSeparatorChar ) {
		this.mergeOrderSeparatorChar = mergeOrderSeparatorChar;
	}
	
	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
}
