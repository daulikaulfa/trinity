package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class RelCtlGenerateDetailViewResponseBean extends BaseResponseBean {

	/** ログインユーザ名 */
	private String loginUserName = null ;
	/** エラーメッセージ */
	private String errMessage;
	/** リリース番号 */
	private String relNo;
	/** リリース実行者 */
	private String executeUser;
	/** リリース実行者ＩＤ */
	private String executeUserId;

	/** リロードインターバル */
	private String reloadInterval = "";
	/** イメージパス */
	private List<List<String>> imagePathList;
	/** メッセージ */
	private List<List<String>> imageMsgList;
	/** タイムラインリスト */
	private List<String> timelineList;
	/** ビルドパッケージ状況メッセージ */
	private String releaseStatusMsg = null;

	/** サーバ番号 */
	private String selectedServerNo = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット番号（コンボ用）*/
	private List<ItemLabelsBean> lotList = null ;


	public String getReloadInterval() {
		return reloadInterval;
	}

	public void setReloadInterval(String reloadInterval) {
		this.reloadInterval = reloadInterval;
	}

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public List<List<String>> getImagePathList() {
		return imagePathList;
	}

	public void setImagePathList(List<List<String>> imagePathList) {
		this.imagePathList = imagePathList;
	}

	public String getErrMessage() {
		return errMessage;
	}

	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}

	public String getLoginUserName() {
		return loginUserName;
	}

	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}

	public String getReleaseStatusMsg() {
		return releaseStatusMsg;
	}

	public void setReleaseStatusMsg(String releaseStatusMsg) {
		this.releaseStatusMsg = releaseStatusMsg;
	}

	public String getExecuteUser() {
		return executeUser;
	}

	public void setExecuteUser(String executeUser) {
		this.executeUser = executeUser;
	}

	public List<String> getTimelineList() {
		return timelineList;
	}

	public void setTimelineList(List<String> timelineList) {
		this.timelineList = timelineList;
	}

	public List<List<String>> getImageMsgList() {
		return imageMsgList;
	}

	public void setImageMsgList(List<List<String>> imageMsgList) {
		this.imageMsgList = imageMsgList;
	}

	public String getExecuteUserId() {
		return executeUserId;
	}

	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public List<ItemLabelsBean> getLotList() {
		return lotList;
	}
	public void setLotList( List<ItemLabelsBean> lotList ) {
		this.lotList = lotList;
	}

}
