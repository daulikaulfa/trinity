package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;

public class RelCtlEntryConfirmResponseBean extends BaseResponseBean{

	/** リリース概要 */
	private String relSummary;

	/** リリース内容 */
	private String relContent;

	/** 環境番号 */
	private String confNo = null;

	/** 環境名 */
	private String confName = null;

	/** 環境概要 */
	private String confSummary = null;

	/** ロット番号 */
	private String lotId = null;

	/** ロット名 */
	private String lotName = null;

	/** 作成者 */
	private String inputUser = null;

	/** 作成者ＩＤ */
	private String inputUserId = null;

	/** 作成日 */
	private String inputDate = null;

	/** パッケージ番号 */
	private String buildNo = null;

	/** 作成日時 */
	private String generateDate = null;

	/** ステータス */
	private String buildStatus = null;

	/** ビルドパッケージ情報 */
	private List<UnitViewBean> unitViewBeanList = null;

	/** ビルドパッケージの複数選択可能フラグ */
	private boolean multiSelectable = false;


	public String getRelContent() {
		return relContent;
	}

	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}

	public String getRelSummary() {
		return relSummary;
	}

	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getConfNo() {
		return confNo;
	}
	public void setConfNo(String confNo) {
		this.confNo = confNo;
	}

	public String getConfName() {
		return confName;
	}
	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getConfSummary() {
		return confSummary;
	}
	public void setConfSummary(String confSummary) {
		this.confSummary = confSummary;
	}

	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	public String getGenerateDate() {
		return generateDate;
	}
	public void setGenerateDate( String generateDate ) {
		this.generateDate = generateDate;
	}

	public String getBuildStatus() {
		return buildStatus;
	}
	public void setBuildStatus(String buildStatus) {
		this.buildStatus = buildStatus;
	}

	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate( String inputDate) {
		this.inputDate = inputDate;
	}

	public List<UnitViewBean> getUnitViewBeanList() {
		if ( null == unitViewBeanList ) {
			unitViewBeanList = new ArrayList<UnitViewBean>();
		}
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}

	public String getInputUserId() {
		return inputUserId;
	}

	public void setInputUserId(String inputUserId) {
		this.inputUserId = inputUserId;
	}

	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}
}
