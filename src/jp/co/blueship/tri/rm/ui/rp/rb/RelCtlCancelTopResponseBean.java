package jp.co.blueship.tri.rm.ui.rp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelTopServiceBean.ReleaseViewBean;

public class RelCtlCancelTopResponseBean extends BaseResponseBean {
	
	
	
	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(List<ReleaseViewBean> releaseViewBeanList) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
}
