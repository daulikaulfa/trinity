package jp.co.blueship.tri.rm.ui.rp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.GenericService;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.SecondaryPresentationController;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlGenerateBtnServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlGenerateServiceBean.RelProcessViewBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlGenerateBtnResponseBean;


/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelGenerateBtnAjax extends SecondaryPresentationController {

	private static final ILog log = TriLogFactory.getInstance();

	private final ILog getLog() {
    	return log;
    }

	@Override
	protected GenericService getApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		return null;
	}

	@Override
	protected GenericServiceBean getApplicationServiceBean(IGeneralServiceBean info) throws Exception {
		return null;
	}

	protected IGeneralServiceBean callApplicationService(IService bbe, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((IGenericTransactionService)bbe).execute("FlowRelGenerateBtnService", serviceDto).getServiceBean();
	}

	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		// DWRを使用した場合非同期のsubmitが残っていることがあり、本クラスが動作することで遷移後の画面のFLOW_ACTION_IDに影響を及ぼすケースがあるため
		// DWRを使用したクラスでのFLOW_ACTION_IDのセッションへの格納は行わない

		FlowRelCtlGenerateBtnServiceBean bean = new FlowRelCtlGenerateBtnServiceBean();

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★FlowRelGenerateBtnDWR::getBussinessInfo:");
			this.getLog().debug("	inRelNo:=" + (String)sesInfo.getAttribute("inRelNo"));
		}

		bean.setInRelNo((String)sesInfo.getAttribute("inRelNo"));

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		return bean;
	}

	protected IService getService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	protected IBaseResponseBean getRequestInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★FlowRelGenerateBtnDWR::getRequestInfo:");
		}

		FlowRelCtlGenerateBtnServiceBean bean = (FlowRelCtlGenerateBtnServiceBean)getServiceReturnInformation();

		RelCtlGenerateBtnResponseBean resBean = new RelCtlGenerateBtnResponseBean();

		List<List<String>> imgPathList = new ArrayList<List<String>>();
		List<RelProcessViewBean> procBeanList = bean.getRelProcessViewBeanList();
		for ( RelProcessViewBean procBean : procBeanList ) {
			imgPathList.add(FluentList.from(procBean.getMsg()).asList());
		}
		resBean.setImagePathList(imgPathList);

//		List<List<String>> imgPathList = new ArrayList<List<String>>();
//		List<List<String>> messageList = new ArrayList<List<String>>();
//		List<String> timelineList = new ArrayList<String>();
//
//		List<RelProcessViewBean> procBeanList = bean.getRelProcessViewBeanList();
//		for ( RelProcessViewBean procBean : procBeanList ) {
//			imgPathList.add(FluentList.from().asList()procBean.getImg()));
//			messageList.add(FluentList.from().asList()procBean.getMsg()));
//
//			String timelineStatus = "" ;
//			for( String active : procBean.getActive() ) {//各ステータスをタイムラインとして集約
//				if( null != active ) {
//					if( "ERROR".equals( active ) ) {//ERRORは即エラー判定
//						timelineStatus = active ;
//						break ;
//					}
//					//ACTIVEとCOMPLETEでは、ACTIVEが勝つ
//					timelineStatus = ( true == "ACTIVE".equals( timelineStatus ) ) ? timelineStatus : active ;
//				}
//			}
//			timelineList.add( timelineStatus ) ;
//		}
//		resBean.setImagePathList	(imgPathList);
//		resBean.setImageMsgList		( messageList );
//		resBean.setTimelineList		( timelineList ) ;

		return resBean;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {
	}

	@Override
	protected String getForward(IRequestInfo arg0, ISessionInfo arg1, IApplicationInfo arg2, IGeneralServiceBean arg3) {
		return null;
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {
	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {
	}

	@Override
	protected String getCallbackJSFuncName() {
		return null;
	}

}
