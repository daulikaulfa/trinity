package jp.co.blueship.tri.rm.ui;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean.RelProcessViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.EntryConfirmBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelListServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelTopServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlDetailViewServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlTopMenuServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlTopMenuServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlUnitDetailViewServiceBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlBaseInfoInputResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlCancelListResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlCancelResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlCancelTopResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlCompRequestResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlConfigurationSelectResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlEntryConfirmResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlGenerateDetailViewResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlLotSelectResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlTopResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlUnitDetailViewResponseBean;
import jp.co.blueship.tri.rm.ui.rp.rb.RelCtlUnitSelectResponseBean;

/**
 *
 * @version V3L10.01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class RmCnvServiceToActionUtils {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static RelCtlBaseInfoInputResponseBean convertRelBaseInfoInputResponseBean(FlowRelCtlEntryServiceBean bean, ISessionInfo sesInfo) {

		RelCtlBaseInfoInputResponseBean resBean = new RelCtlBaseInfoInputResponseBean();

		BaseInfoBean baseInfoBean = new BaseInfoBean();
		BaseInfoRelApplySearchBean relApplySearchBean = new BaseInfoRelApplySearchBean();
		BaseInfoInputBean inputBean = new BaseInfoInputBean();

		if (bean != null) {
			baseInfoBean = bean.getBaseInfoBean();
			relApplySearchBean = baseInfoBean.getRelApplySearchBean();
			inputBean = baseInfoBean.getBaseInfoInputBean();
		}

		if (inputBean != null ) {
			resBean.setOpenDisplay( baseInfoBean.getOpenDisplay() );

			resBean.setRelSummary(inputBean.getRelSummary());
			resBean.setRelContent(inputBean.getRelContent());
			resBean.setRelApplyNo(inputBean.getRelApplyNo());

			resBean.setRelApplySearchBean( relApplySearchBean );
			resBean.setRelApplyViewBeanList(baseInfoBean.getRelApplyViewBeanList());

			LogHandler.debug( log , "convertRelBaseInfoInputResponseBean  relSummary:=" + inputBean.getRelSummary() );
			LogHandler.debug( log , "convertRelBaseInfoInputResponseBean  relContent:=" + inputBean.getRelContent() );
		}

		return resBean;

	}

	public static RelCtlConfigurationSelectResponseBean convertRelConfigurationSelectResponseBean(FlowRelCtlEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		ConfigurationSelectBean confInfo = bean.getConfSelectBean();

		RelCtlConfigurationSelectResponseBean resBean = new RelCtlConfigurationSelectResponseBean();
		resBean.setConfViewBeanList(confInfo.getConfViewBeanList());
		resBean.setSelectedEnvNo(bean.getSelectedEnvNo());

		return resBean;

	}

	public static RelCtlLotSelectResponseBean convertRelLotSelectResponseBean(
			FlowRelCtlEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		RelCtlLotSelectResponseBean resBean = new RelCtlLotSelectResponseBean();
		LotSelectBean lotBean = bean.getLotSelectBean();
		resBean.setLotViewBeanList(lotBean.getLotViewBeanList());

		if ( null != bean.getSelectedLotNo() ) {
			resBean.setSelectedLotNo( bean.getSelectedLotNo() );
		}
		if ( null != bean.getSelectedServerNo() ) {
			resBean.setSelectedServerNo( bean.getSelectedServerNo() );
		}

		LogHandler.debug( log , "convertRelLotSelectResponseBean  windowsId:=" + resBean.getWindowsId() );

		return resBean;

	}

	public static RelCtlUnitSelectResponseBean convertRelUnitResourceSelectResponseBean(
			FlowRelCtlEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		UnitSelectBean unitSelectBean = bean.getReleaseUnitSelectBean();

//		List<String> buildNo = new ArrayList<String>();
//		if ((String[])sesInfo.getAttribute("selectedBuildNo")  != null){
//			for( String value : (String[])sesInfo.getAttribute("selectedBuildNo")){
//				buildNo.add(value);
//			}
//		}

		RelCtlUnitSelectResponseBean resBean = new RelCtlUnitSelectResponseBean();
		resBean.setUnitViewBeanList( unitSelectBean.getUnitViewBeanList() );


//		resBean.setPageNoNum(resourceInfo.getPageNoInfoView().getMaxPageNo());
//		resBean.setSelectPageNo(resourceInfo.getPageNoInfoView().getSelectPageNo());

		resBean.setMultiSelectable( bean.getMultiSelectable() );

		//リリース申請からRCの場合は、参照のみ
		if ( StringUtils.isEmpty( bean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo() ) ) {
			resBean.setSelectable( false );
		} else {
			resBean.setSelectable( true );
		}

		// パッケージ複数選択
		if ( resBean.getMultiSelectable() ) {

			List<String> selectedMergeOrderList	= new ArrayList<String>();
			List<String> defaultMergeOrderList	= new ArrayList<String>();

			for ( UnitViewBean unitViewBean : unitSelectBean.getUnitViewBeanList() ) {
				defaultMergeOrderList.add( unitViewBean.getMergeOrder() );
			}

			// 初めてパッケージ選択画面を表示
			if ( TriStringUtils.isEmpty( bean.getViewUnitBean() )) {

				selectedMergeOrderList.addAll( defaultMergeOrderList );

			// いったん戻ってパッケージ選択画面を表示
			} else {

				Map<String, String> buildNoMergeOrderMap = new TreeMap<String, String>();
				for ( UnitBean viewUnitBean : bean.getViewUnitBean() ) {
					buildNoMergeOrderMap.put( viewUnitBean.getBuildNo(), viewUnitBean.getMergeOrder() );
				}

				for ( UnitViewBean unitViewBean : unitSelectBean.getUnitViewBeanList() ) {

					if ( buildNoMergeOrderMap.containsKey( unitViewBean.getBuildNo() )) {
						selectedMergeOrderList.add( buildNoMergeOrderMap.get( unitViewBean.getBuildNo() ));
					} else {
						selectedMergeOrderList.add( unitViewBean.getMergeOrder() );
					}
				}

			}
			resBean.setSelectedMergeOrderString	( (String[])selectedMergeOrderList.toArray( new String[0] ));
			resBean.setDefaultMergeOrderString	( (String[])defaultMergeOrderList.toArray( new String[0] ));

		// パッケージ単数選択
		} else {
			if ( !TriStringUtils.isEmpty( bean.getSelectedUnitBean() )) {
				resBean.setSelectedBuildNo( bean.getSelectedUnitBean()[0].getBuildNo() );
			}
		}

		resBean.setMergeOrderSeparatorChar	( bean.getMergeOrderSeparatorChar() );
		resBean.setMergeOrderUnSelectedChar	( bean.getMergeOrderUnSelectedChar() );
		resBean.setMergeOrderList			( unitSelectBean.getMergeOrderList() );

		resBean.setSelectedLotNo			( bean.getLockLotNo() );
		resBean.setSelectedServerNo			( bean.getLockServerId() );

		return resBean;

	}

	public static RelCtlEntryConfirmResponseBean convertRelEditConfirmResponseBean(
			FlowRelCtlEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		RelCtlEntryConfirmResponseBean resBean = new RelCtlEntryConfirmResponseBean();

		// リリース基本情報を表示
		BaseInfoInputBean baseBean = bean.getBaseInfoBean().getBaseInfoInputBean();
		resBean.setRelSummary(baseBean.getRelSummary());
		resBean.setRelContent(baseBean.getRelContent());

		// リリース基本情報を表示
		EntryConfirmBean relBean = bean.getReleaseConfirmBean();
		resBean.setRelSummary		( relBean.getRelSummary() );
		resBean.setRelContent		( relBean.getRelContent() );
		resBean.setConfNo			( relBean.getConfViewBean().getConfNo() );
		resBean.setConfName			( relBean.getConfViewBean().getConfName() );
		resBean.setConfSummary		( relBean.getConfViewBean().getConfSummary() );
		resBean.setLotNo			( relBean.getLotViewBean().getLotNo() );
		resBean.setLotName			( relBean.getLotViewBean().getLotName() );
		resBean.setInputUser		( relBean.getLotViewBean().getInputUser() );
		resBean.setInputUserId		( relBean.getLotViewBean().getInputUserId() );
		resBean.setInputDate		( relBean.getLotViewBean().getInputDate() );
		resBean.setUnitViewBeanList	( relBean.getUnitViewBeanList() );

		resBean.setMultiSelectable	( bean.getMultiSelectable() );

		// ビルドパッケージ選択で選択された値を表示
//		resBean.setResourceViewBeanList(bean.getConfSelectBean().getConfViewBeanList());
//
//		if( reqInfo.getParameter( "referer" ).equals( RelScreenID.ENTRY_CONFIRM )){
//			List<?> msg =
//				ContextAdapterFactory.getMessageContextAdapter().getMessage(
//					new BusinessException("MESREL0008", new String[]{ bean.getRelNo() }));
//
//			resBean.setMessageList(msg);
//			sesInfo.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, "FlowRelModifyService");
//		}

		return resBean;
	}

	public static RelCtlCompRequestResponseBean convertRelCompRequestResponseBean(FlowRelCtlEntryServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo) {

		RelCtlCompRequestResponseBean resBean = new RelCtlCompRequestResponseBean();

		// リリース番号を表示
		resBean.setRelNo(bean.getRelNo());

		return resBean;
	}

	public static RelCtlTopResponseBean convertRelTopResponseBean(
			FlowRelCtlTopMenuServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		List<ReleaseViewBean> viewBeanList = bean.getReleaseViewBeanList();
		for ( ReleaseViewBean viewBean : viewBeanList ) {
			if ( null == viewBean.getEnvName() || viewBean.getEnvName().trim().equals( "" )) {
				// ちゃんとした環境ではないと思うが、環境名が設定されていないとテーブルがつぶれてしまう
				viewBean.setEnvName( "N/A" );
			}
		}


		RelCtlTopResponseBean resBean = new RelCtlTopResponseBean();
		resBean.setReleaseViewBeanList( viewBeanList );

		resBean.setMergeOrderSeparatorChar(
				sheet.getValue( RmDesignEntryKeyByRelease.mergeOrderSeparatorChar ));

		return resBean;

	}

	public static RelCtlUnitDetailViewResponseBean convertRelUnitDetailViewResponseBean(
			FlowRelCtlUnitDetailViewServiceBean bean ) {

		RelCtlUnitDetailViewResponseBean resBean = new RelCtlUnitDetailViewResponseBean();

		resBean.setLotNo				( bean.getLotNo() ) ;
		resBean.setLotName				( bean.getLotName() );
		resBean.setSelectedBuildNo		( bean.getSelectedBuildNo() );
		resBean.setReleaseViewBeanList	( bean.getReleaseViewBeanList() );

		return resBean;
	}

	public static RelCtlGenerateDetailViewResponseBean convertRelGenerateDetailViewResponseBean(
			FlowRelCtlDetailViewServiceBean bean ) {

		RelCtlGenerateDetailViewResponseBean resBean = new RelCtlGenerateDetailViewResponseBean();

		resBean.setReleaseStatusMsg	( bean.getReleaseDetailViewBean().getRelStatusMsg() );
		resBean.setExecuteUser		( bean.getReleaseDetailViewBean().getCopeUser() );
		resBean.setExecuteUserId		( bean.getReleaseDetailViewBean().getCopeUserId() );
		resBean.setRelNo			( bean.getReleaseDetailViewBean().getRelNo() );

		List<List<String>> imgPathList = new ArrayList<List<String>>();
		List<List<String>> messageList = new ArrayList<List<String>>();
		List<String> timelineList = new ArrayList<String>() ;
		List<RelProcessViewBean> procBeanList = bean.getReleaseDetailViewBean().getRelProcessViewBeanList();
		for ( RelProcessViewBean procBean : procBeanList ) {
			imgPathList.add(getImgPathList(procBean.getImg()));
			messageList.add(FluentList.from(procBean.getMsg()).asList());
			String timelineStatus = "" ;
			for( String active : procBean.getActive() ) {//各ステータスをタイムラインとして集約
				if( null != active ) {
					if( "ERROR".equals( active ) ) {//ERRORは即エラー判定
						timelineStatus = active ;
						break ;
					}
					//ACTIVEとCOMPLETEでは、ACTIVEが勝つ
					timelineStatus = ( true == "ACTIVE".equals( timelineStatus ) ) ? timelineStatus : active ;
				}
			}
			timelineList.add( timelineStatus ) ;
		}
		resBean.setImagePathList	( imgPathList );
		resBean.setImageMsgList		( messageList );
		resBean.setTimelineList		( timelineList ) ;
		resBean.setReloadInterval	( sheet.getValue( BmDesignEntryKeyByBuild.reloadInterval ));

		resBean.setSelectedLotNo	( bean.getSelectedLotNo() );
		resBean.setSelectedServerNo	( bean.getLockServerId() );
		resBean.setLotList			( bean.getLotList() );

		return resBean;
	}

	/**
	 * アイコンIDからRm-Design-Sheetに定義されているアイコンのファイル名を取得します。
	 * @param ImgArray
	 * @return
	 */
	private static List<String> getImgPathList(String[] ImgArray) {
		List<String> imgList = new ArrayList<String>();
		for (String imgPath : ImgArray ) {
			imgList.add( sheet.getValue(RmDesignBeanId.resources,imgPath) );
		}
		return imgList;
	}

	public static RelCtlCancelTopResponseBean convertReleaseCancelResponseBean(
			FlowRelCtlCancelTopServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		List<FlowRelCtlCancelTopServiceBean.ReleaseViewBean> viewBeanList = bean.getReleaseViewBeanList();
		for ( FlowRelCtlCancelTopServiceBean.ReleaseViewBean viewBean : viewBeanList ) {
			if ( null == viewBean.getEnvName() || viewBean.getEnvName().trim().equals( "" )) {
				// ちゃんとした環境ではないと思うが、環境名が設定されていないとテーブルがつぶれてしまう
				viewBean.setEnvName( "N/A" );
			}
		}

		RelCtlCancelTopResponseBean resBean = new RelCtlCancelTopResponseBean();
		resBean.setReleaseViewBeanList( viewBeanList );
		resBean.setPageNoNum		( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo		( bean.getPageInfoView().getSelectPageNo() );

		return resBean;

	}

	public static RelCtlCancelListResponseBean convertReleaseCancelListResponseBean(
			FlowRelCtlCancelListServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		RelCtlCancelListResponseBean resBean = new RelCtlCancelListResponseBean();

		resBean.setReleaseViewBeanList		( bean.getRelCtlViewBeanList() );
		resBean.setSelectedEnvNo			( bean.getSelectedEnvNo() );
		resBean.setEnvNo					( bean.getEnvNo() ) ;
		resBean.setEnvName					( bean.getEnvName() );
		resBean.setReleaseSearchBean		( bean.getRelCtlSearchBean() );
		//resBean.setSelectedRelNoString( bean.getSelectedRelNo() ) ;

		return resBean;

	}

	public static RelCtlCancelResponseBean convertReleaseCancelResponseBean(
			FlowRelCtlCancelServiceBean bean, IRequestInfo reqInfo, ISessionInfo sesInfo ) {

		RelCtlCancelResponseBean resBean = new RelCtlCancelResponseBean();

		resBean.setSelectedEnvNo			( bean.getSelectedEnvNo() );
		resBean.setEnvName					( bean.getEnvName() );
		resBean.setSelectedRelNoString		( TriStringUtils.convertArrayToString( bean.getSelectedRelNo() ));
		resBean.setReleaseCancelConfirmViewBeanList( bean.getReleaseCancelConfirmViewBeanList() );

		if ( null != bean.getReleaseInfoInputBean() ) {
			resBean.setReleaseInfoInputBean( bean.getReleaseInfoInputBean() );
		}

		if ( StatusFlg.on.value().equals( sheet.getValue(
				RmDesignBeanId.multiSelectable, RelCtlScreenID.UNIT_SELECT ))) {
			resBean.setMultiSelectable( true );
		}

		return resBean;

	}

}
