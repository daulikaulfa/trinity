package jp.co.blueship.tri.rm.ui;

import javax.servlet.ServletException;

import jp.co.blueship.tri.fw.cmn.utils.AllowFilePathUtil;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.ui.SjisActionServlet;


public class SjisActionServletX extends SjisActionServlet {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		super.init();

		AllowFilePathUtil.init();

		ProductActivate.init();
	}
}
