package jp.co.blueship.tri.rm.ui.ra.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyViewBean;

public class RelApplyCloseResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** リリース申請情報 */
	private RelApplyViewBean relApplyViewBean=null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		return relApplyViewBeanList;
	}
	public void setRelApplyViewBeanList(List<RelApplyViewBean> relApplyViewBeanList) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}
	public RelApplyViewBean getRelApplyViewBean() {
		return relApplyViewBean;
	}
	public void setRelApplyViewBean(RelApplyViewBean relApplyViewBean) {
		this.relApplyViewBean = relApplyViewBean;
	}	
	
		
}
