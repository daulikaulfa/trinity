package jp.co.blueship.tri.rm.ui.ra.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseHistoryListViewServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;
import jp.co.blueship.tri.rm.ui.ra.rb.RelApplyCloseHistoryListViewResponseBean;

public class FlowRelApplyCloseHistoryListViewProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelApplyCloseHistoryListViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelApplyScreenID.CLOSE_HISTORY_LIST_VIEW,
														RelApplyScreenID.CLOSE_HISTORY_DETAIL_VIEW,
														RelApplyScreenID.CLOSE_LIST_VIEW,
														RelApplyScreenID.CLOSE_DETAIL_VIEW
														 };

	public FlowRelApplyCloseHistoryListViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}


	public FlowRelApplyCloseHistoryListViewProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelApplyCloseHistoryListViewServiceBean seBean = (FlowRelApplyCloseHistoryListViewServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelApplyCloseHistoryListViewServiceBean bean = null;
		if (null == seBean) {
			bean = new FlowRelApplyCloseHistoryListViewServiceBean();
		} else {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		// リクエスト情報取得
		{
			//[検索]ボタン押下時
			if ( RelApplyScreenID.CLOSE_HISTORY_LIST_VIEW.equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setRelApplySearchBean( RmCnvActionToServiceUtils.convertRelApplySearchBean( reqInfo, bean.getRelApplySearchBean() ) );
				}
			}
		}

		// ページ遷移はしない

		//ロットごとの排他設定を初期化する
		{
			if ( forward.equals( RelApplyScreenID.CLOSE_HISTORY_LIST_VIEW ) ) {

				bean.setLockLotNo	( null );
				bean.setLockServerId( null );
				session.setAttribute	( SessionScopeKeyConsts.SELECTED_LOT_NO, null );
				session.setAttribute	( SessionScopeKeyConsts.SELECTED_SERVER_ID, null );
			}
		}

		//サービスビーンごとセッションに保持する
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelApplyCloseHistoryListViewResponseBean resBean = new RelApplyCloseHistoryListViewResponseBean();
		FlowRelApplyCloseHistoryListViewServiceBean bean = (FlowRelApplyCloseHistoryListViewServiceBean)getServiceReturnInformation();

		resBean.setRelApplySearchBean(bean.getRelApplySearchBean());
		resBean.setRelApplyViewBeanList(bean.getRelApplyViewBeanList());

		// ?
		resBean.setSelectedRelApplyNoString(
				reqInfo.getParameter("selectedRelApplyNoParam"));

		if ( resBean != null ) {
			resBean.new MessageUtility().reflectMessage( bean );
			resBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

}
