package jp.co.blueship.tri.rm.ui.ra.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;
import jp.co.blueship.tri.rm.ui.ra.rb.RelApplyEntryResponseBean;

public class FlowRelApplyEntryProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelApplyEntryService";

	public static final String[] screenFlows = new String[] {
														RelApplyScreenID.PACKAGE_LIST_SELECT,
														RelApplyScreenID.ENTRY,
														RelApplyScreenID.ENTRY_CONFIRM,
														RelApplyScreenID.SAVE_COMP,
														RelApplyScreenID.ENTRY_COMP
														 };

	public FlowRelApplyEntryProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}


	public FlowRelApplyEntryProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelApplyEntryServiceBean seBean	= (FlowRelApplyEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );

		FlowRelApplyEntryServiceBean bean	= null;
		if ( seBean != null ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowRelApplyEntryServiceBean();
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		// リクエスト情報取得
		{
			//[トップ]→[パッケージ選択]画面遷移時
			if ( RelApplyScreenID.TOP.equals( referer )
					&& RelApplyScreenID.PACKAGE_LIST_SELECT.equals( forward ) ) {

			}
			//[パッケージ選択]自画面遷移時
			if ( RelApplyScreenID.PACKAGE_LIST_SELECT.equals( referer )
					&& RelApplyScreenID.PACKAGE_LIST_SELECT.equals( forward ) ) {

				//[検索]ボタン押下時
				if ( ScreenType.select.equals( screenType ) ) {

					bean.setRelApplyPackageListSearchBean( RmCnvActionToServiceUtils.convertRelApplyPackageListSearchBean( reqInfo, bean.getRelApplyPackageListSearchBean() ) );
				}
			}
			//[パッケージ選択]→[申請]画面遷移時
			if ( RelApplyScreenID.PACKAGE_LIST_SELECT.equals( referer )
					&& RelApplyScreenID.ENTRY.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplyPackageListSearchBean( RmCnvActionToServiceUtils.convertRelApplyPackageListSelectBean( reqInfo, bean.getRelApplyPackageListSearchBean() ) );
				}

				RelApplyPackageListSearchBean searchBean = bean.getRelApplyPackageListSearchBean();

				if ( null != searchBean.getSelectedServerNo() ) {
					bean.setLockLotNo		( searchBean.getSelectedLotNo() );
					bean.setLockServerId	( searchBean.getSelectedServerNo() );
					session.setAttribute	( SessionScopeKeyConsts.SELECTED_LOT_NO, searchBean.getSelectedLotNo() );
					session.setAttribute	( SessionScopeKeyConsts.SELECTED_SERVER_ID, searchBean.getSelectedServerNo() );
				}
			}
			//[申請]→[確認]画面遷移時
			if ( RelApplyScreenID.ENTRY.equals( referer )
					&& RelApplyScreenID.ENTRY_CONFIRM.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplyPackageListSearchBean( RmCnvActionToServiceUtils.convertRelApplyEntrySelectBean( reqInfo, bean.getRelApplyPackageListSearchBean() ) );
					bean.setRelApplyEntryViewBean( RmCnvActionToServiceUtils.convertRelApplyEntryViewBean( reqInfo, bean.getRelApplyEntryViewBean() ) );
				}
			}
			//[申請]→[保存]画面遷移時
			if ( RelApplyScreenID.ENTRY.equals( referer )
					&& RelApplyScreenID.SAVE_COMP.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplyPackageListSearchBean( RmCnvActionToServiceUtils.convertRelApplyEntrySelectBean( reqInfo, bean.getRelApplyPackageListSearchBean() ) );
					bean.setRelApplyEntryViewBean( RmCnvActionToServiceUtils.convertRelApplyEntryViewBean( reqInfo, bean.getRelApplyEntryViewBean() ) );
				}
			}
			//[確認]→[完了]画面遷移時
			if ( RelApplyScreenID.ENTRY_CONFIRM.equals( referer )
					&& RelApplyScreenID.ENTRY_COMP.equals( forward ) ) {

				// セッションの情報をそのまま使う
			}
		}

		// ページ遷移はしない

		//ロットごとの排他設定を初期化する
		{
			if ( forward.equals( RelApplyScreenID.PACKAGE_LIST_SELECT ) ) {

				bean.setLockLotNo	( null );
				bean.setLockServerId( null );
				session.setAttribute	( SessionScopeKeyConsts.SELECTED_LOT_NO, null );
				session.setAttribute	( SessionScopeKeyConsts.SELECTED_SERVER_ID, null );
			}
		}

		//サービスビーンごとセッションに保持する
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelApplyEntryResponseBean resBean = new RelApplyEntryResponseBean();
		FlowRelApplyEntryServiceBean bean	= (FlowRelApplyEntryServiceBean)getServiceReturnInformation();

		resBean.setRelApplyPackageListSearchBean(bean.getRelApplyPackageListSearchBean());
		resBean.setSearchLotNoList(bean.getSearchLotNoList());
		resBean.setSearchRelUnitCountList(bean.getSearchRelUnitCountList());
		resBean.setUnitViewBeanList(bean.getUnitViewBeanList());
		resBean.setGroupIdList(bean.getGroupIdList());
		resBean.setSearchRelEnvNoList(bean.getSearchRelEnvNoList());
		resBean.setSearchRelWishDateList(bean.getSearchRelWishDateList());
		resBean.setRelApplyEntryViewBean(bean.getRelApplyEntryViewBean());

		if ( resBean != null ) {
			resBean.new MessageUtility().reflectMessage( bean );
			resBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

		if ( null == this.getBussinessException()
			&& RelApplyScreenID.SAVE_COMP.equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( RelApplyScreenID.SAVE_COMP.equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
