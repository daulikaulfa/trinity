package jp.co.blueship.tri.rm.ui.ra.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyTopServiceBean;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;
import jp.co.blueship.tri.rm.ui.ra.rb.RelApplyModifyResponseBean;

public class FlowRelApplyModifyProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelApplyModifyService";

	public static final String[] screenFlows = new String[] {
														RelApplyScreenID.MODIFY,
														RelApplyScreenID.MODIFY_CONFIRM,
														RelApplyScreenID.MODIFY_SAVE_COMP,
														RelApplyScreenID.MODIFY_COMP
														 };

	public FlowRelApplyModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}


	public FlowRelApplyModifyProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelApplyModifyServiceBean seBean	= (FlowRelApplyModifyServiceBean)session.getAttribute( FLOW_ACTION_ID );

		FlowRelApplyModifyServiceBean bean	= null;
		if ( seBean != null ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowRelApplyModifyServiceBean();
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		// 上位のフローのセッション情報を取得
		FlowRelApplyTopServiceBean topServiceBean = (FlowRelApplyTopServiceBean)session.getAttribute(FlowRelApplyTopProsecutor.FLOW_ACTION_ID);

		// リクエスト情報取得
		{
			//[トップ]→[編集]画面遷移時
			if ( RelApplyScreenID.TOP.equals( referer )
					&& RelApplyScreenID.MODIFY.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplySearchBean( RmCnvActionToServiceUtils.convertRelApplyTopSelectBean( reqInfo, bean.getRelApplySearchBean() ) );
				}

				if ( null != topServiceBean.getRelApplySearchBean().getSelectedServerNo() ) {
					bean.setLockLotNo		( topServiceBean.getRelApplySearchBean().getSelectedLotNo() );
					bean.setLockServerId	( topServiceBean.getRelApplySearchBean().getSelectedServerNo() );
					session.setAttribute	( SessionScopeKeyConsts.SELECTED_LOT_NO, topServiceBean.getRelApplySearchBean().getSelectedLotNo() );
					session.setAttribute	( SessionScopeKeyConsts.SELECTED_SERVER_ID, topServiceBean.getRelApplySearchBean().getSelectedServerNo() );
				}
			}
			//[申請]→[確認]画面遷移時
			if ( RelApplyScreenID.MODIFY.equals( referer )
					&& RelApplyScreenID.MODIFY_CONFIRM.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplySearchBean( RmCnvActionToServiceUtils.convertRelApplyModifySelectBean( reqInfo, bean.getRelApplySearchBean() ) );
					bean.setRelApplyEntryViewBean( RmCnvActionToServiceUtils.convertRelApplyEntryViewBean( reqInfo, bean.getRelApplyEntryViewBean() ) );
				}
			}
			//[申請]→[保存]画面遷移時
			if ( RelApplyScreenID.MODIFY.equals( referer )
					&& RelApplyScreenID.MODIFY_SAVE_COMP.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					bean.setRelApplySearchBean( RmCnvActionToServiceUtils.convertRelApplyModifySelectBean( reqInfo, bean.getRelApplySearchBean() ) );
					bean.setRelApplyEntryViewBean( RmCnvActionToServiceUtils.convertRelApplyEntryViewBean( reqInfo, bean.getRelApplyEntryViewBean() ) );
				}
			}
			//[確認]→[完了]画面遷移時
			if ( RelApplyScreenID.MODIFY_CONFIRM.equals( referer )
					&& RelApplyScreenID.MODIFY_COMP.equals( forward ) ) {

				// セッションの情報をそのまま使う
			}
		}

		// ページ遷移はしない

		//サービスビーンごとセッションに保持する
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelApplyModifyResponseBean resBean = new RelApplyModifyResponseBean();
		FlowRelApplyModifyServiceBean bean	= (FlowRelApplyModifyServiceBean)getServiceReturnInformation();

		resBean.setRelApplySearchBean(bean.getRelApplySearchBean());
		resBean.setGroupIdList(bean.getGroupIdList());
		resBean.setSearchRelEnvNoList(bean.getSearchRelEnvNoList());
		resBean.setSearchRelWishDateList(bean.getSearchRelWishDateList());
		resBean.setRelApplyEntryViewBean(bean.getRelApplyEntryViewBean());

		if ( resBean != null ) {
			resBean.new MessageUtility().reflectMessage( bean );
			resBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

		if ( null == this.getBussinessException()
			&& RelApplyScreenID.SAVE_COMP.equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );

		if ( RelApplyScreenID.SAVE_COMP.equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
