package jp.co.blueship.tri.rm.ui.ra.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseListViewServiceBean.RelApplyViewBean;

public class RelApplyCloseListViewResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** 選択されたリリース申請番号 */
	private String selectedRelApplyNoString = null;
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;
	
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}
	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}
	
	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		if ( null == relApplyViewBeanList ) {
			relApplyViewBeanList = new ArrayList<RelApplyViewBean>();
		}
		return relApplyViewBeanList;
	}
	public void setRelApplyViewBeanList(
			List<RelApplyViewBean> relApplyViewBeanList ) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}
	
	public String getSelectedRelApplyNoString() {
		return selectedRelApplyNoString;
	}
	public void setSelectedRelApplyNoString( String selectedRelApplyNoString ) {
		
		StringBuilder buf = new StringBuilder();
		
		buf.append( "[" );
		buf.append( selectedRelApplyNoString );
		buf.append( "]" );
		this.selectedRelApplyNoString = buf.toString();
	}
	
	
	
}
