package jp.co.blueship.tri.rm.ui.ra;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.ui.ra.beans.FlowRelApplyEntryProsecutor;
import jp.co.blueship.tri.rm.ui.ra.beans.FlowRelApplyTopProsecutor;

public class FlowRelApplyEntry extends PresentationController{

	
	
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowRelApplyEntryProsecutor());
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {

		String referer = reqInfo.getParameter("referer");
		if (null != referer) {
			for ( String screenFlow : FlowRelApplyEntryProsecutor.screenFlows) {
				if ( screenFlow.equals( referer ) ) {
					ppm.addBusinessErrorPresentationProsecutor(new FlowRelApplyEntryProsecutor(bbe));
					return;
				}
			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowRelApplyTopProsecutor(bbe));
	}
	@Override
	protected String getForward(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter( "forward" );
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		
		String forward = reqInfo.getParameter("forward");
		String referer = reqInfo.getParameter("referer");

		if ( null != forward ) {
			
			if ( RelApplyScreenID.ENTRY_COMP.equals( forward )) {
				// 確認画面に戻ってもしょうがないので申請画面に戻る
				return RelApplyScreenID.ENTRY;
			}
		}
		
		if ( null != referer ) {
			for ( String screenFlow : FlowRelApplyEntryProsecutor.screenFlows ) {
				if ( screenFlow.equals( referer )) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return RelApplyScreenID.TOP;
	}

}
