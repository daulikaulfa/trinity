package jp.co.blueship.tri.rm.ui.ra.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;

public class RelApplyModifyResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** 
	 * グループ（コンボ用）
	 */
	private List<ItemLabelsBean> groupIdList = null ;
	/** 
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = null ;
	/** 
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = null ;
	
	/** リリース申請情報 */
	private RelApplyEntryViewBean relApplyEntryViewBean = null ;
	
	
	public List<ItemLabelsBean> getGroupIdList() {
		return groupIdList;
	}
	public void setGroupIdList(List<ItemLabelsBean> groupIdList) {
		this.groupIdList = groupIdList;
	}
	public RelApplyEntryViewBean getRelApplyEntryViewBean() {
		return relApplyEntryViewBean;
	}
	public void setRelApplyEntryViewBean(RelApplyEntryViewBean relApplyEntryViewBean) {
		this.relApplyEntryViewBean = relApplyEntryViewBean;
	}
	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}
	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}
	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}
	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}
	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}
	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
}
