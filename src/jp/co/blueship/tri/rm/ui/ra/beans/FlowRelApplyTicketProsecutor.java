package jp.co.blueship.tri.rm.ui.ra.beans;

import java.util.Set;

import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.ra.dto.FlowRelApplyTicketServiceBean;
import jp.co.blueship.tri.dcm.ui.dl.rb.DownloadResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.ui.RmCnvActionToServiceUtils;

public class FlowRelApplyTicketProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowRelApplyTicketService";

	public FlowRelApplyTicketProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	public FlowRelApplyTicketProsecutor() {
		super( null );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelApplyTicketServiceBean seBean = (FlowRelApplyTicketServiceBean)session.getAttribute(FLOW_ACTION_ID);

		FlowRelApplyTicketServiceBean bean = null;
		if (null == seBean) {
			bean = new FlowRelApplyTicketServiceBean();
		} else {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer	    = session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer	   ( referer );
		bean.setForward	   ( forward );
		bean.setScreenType ( screenType );

		// リクエスト情報取得
		{
			// 子画面を表示する場合セッションキーからロット番号とサーバ番号を取得する
			String lotId = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
			bean.setSelectedLotNo	( lotId );
			bean.setLockLotNo		( lotId );

			String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
			bean.setLockServerId	( serverNo );
			bean.setReportId		( DcmReportType.RmRaReport.value() );

			Set<String> selectedRelApplyNo =
					RmCnvActionToServiceUtils.saveSelectedRelApplyNoForReport( session, reqInfo );
			bean.setSelectedRelApplyNo ( (String[])selectedRelApplyNo.toArray( new String[0] ));

		}

		//サービスビーンごとセッションに保持する
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		DownloadResponseBean resBean = new DownloadResponseBean();

		FlowRelApplyTicketServiceBean bean = (FlowRelApplyTicketServiceBean)getServiceReturnInformation();

		resBean.setDownloadPath( bean.getDownloadPath() );
		resBean.setDownloadFile( bean.getDownloadFile() );
		resBean.setDownloadSize( bean.getDownloadSize() );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}
	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor( IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}
}
