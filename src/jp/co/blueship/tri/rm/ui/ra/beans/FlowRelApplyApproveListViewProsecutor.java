package jp.co.blueship.tri.rm.ui.ra.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyApproveListViewServiceBean;
import jp.co.blueship.tri.rm.ui.ra.rb.RelApplyApproveListViewResponseBean;

public class FlowRelApplyApproveListViewProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelApplyApproveListViewService";

	/**
	 * 参照系の遷移フロー。 <br>
	 * 業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
	// 要実装
	};

	public FlowRelApplyApproveListViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	public FlowRelApplyApproveListViewProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		sesInfo.removeAttribute(SessionScopeKeyConsts.TOP_SCREEN_NAME);

		SessionMultiWindows session = new SessionMultiWindows(sesInfo, reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelApplyApproveListViewServiceBean bean = new FlowRelApplyApproveListViewServiceBean();
		// ワーニングメッセージの初期化
		bean.setInfoMessage(null);

		String userId = (String) sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer(this);
		String forward = session.getForward(this);
		String screenType = session.getScreenType(this);

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		// リクエスト情報取得
		{
			// 要実装
		}

		// 必要であればページ遷移

		// ロットごとの排他設定を初期化する
		{
			if (forward.equals(RelApplyScreenID.APPROVE_LIST_VIEW)) {

				bean.setLockLotNo(null);
				bean.setLockServerId(null);
				session.setAttribute(SessionScopeKeyConsts.SELECTED_LOT_NO, null);
				session.setAttribute(SessionScopeKeyConsts.SELECTED_SERVER_ID, null);
			}
		}

		// サービスビーンごとセッションに保持する
		session.setAttribute(FLOW_ACTION_ID, bean);

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows(sesInfo, reqInfo);

		sesInfo.setAttribute(SessionScopeKeyConsts.TOP_SCREEN_NAME, RelApplyScreenID.APPROVE_LIST_VIEW);

		RelApplyApproveListViewResponseBean resBean = new RelApplyApproveListViewResponseBean();
		FlowRelApplyApproveListViewServiceBean bean = (FlowRelApplyApproveListViewServiceBean) getServiceReturnInformation();

		// 要実装

		if (resBean != null) {
			resBean.new MessageUtility().reflectMessage(bean);
			resBean.new MessageUtility().reflectMessage(getBussinessException());
		}

		// RMI対応
		session.setAttribute(FLOW_ACTION_ID, bean);

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService) ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		ISessionInfo session = new SessionMultiWindows(sesInfo, reqInfo);
		session.removeAttribute(FLOW_ACTION_ID);
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

}
