package jp.co.blueship.tri.rm.ui.ra.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean;

public class RelApplyCloseDetailViewResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** リリース申請詳細情報 */
	private RelApplyDetailViewBean relApplyDetailViewBean = null;

	public String getForward() {
		return forward;
	}

	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}

	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}

	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}

	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}

	public RelApplyDetailViewBean getRelApplyDetailViewBean() {
		return relApplyDetailViewBean;
	}

	public void setRelApplyDetailViewBean(RelApplyDetailViewBean relApplyDetailViewBean) {
		this.relApplyDetailViewBean = relApplyDetailViewBean;
	}
	
}
