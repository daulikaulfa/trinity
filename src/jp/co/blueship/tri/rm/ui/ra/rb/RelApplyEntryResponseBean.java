package jp.co.blueship.tri.rm.ui.ra.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean.UnitViewBean;

public class RelApplyEntryResponseBean extends BaseResponseBean {

	
	
	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	
	/** 検索条件情報 */
	private RelApplyPackageListSearchBean relApplyPackageListSearchBean = new RelApplyPackageListSearchBean() ;
	
	/** 
	 * ロット番号（コンボ用）
	 */
	private List<ItemLabelsBean> searchLotNoList = null ;
	/** 
	 * 詳細検索 検索件数 
	 */
	private List<ItemLabelsBean> searchRelUnitCountList = null ;
	/** 
	 * ビルドパッケージ情報（一覧選択用）
	 */
	private List<UnitViewBean> unitViewBeanList = null;
	/** 
	 * グループ（コンボ用）
	 */
	private List<ItemLabelsBean> groupIdList = null ;
	/** 
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = null ;
	/** 
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = null ;
	
	/** リリース申請情報 */
	private RelApplyEntryViewBean relApplyEntryViewBean = null ;
	
	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}
	
	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}
	
	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}
	
	public List<ItemLabelsBean> getGroupIdList() {
		return groupIdList;
	}
	public void setGroupIdList(List<ItemLabelsBean> groupIdList) {
		this.groupIdList = groupIdList;
	}
	public RelApplyPackageListSearchBean getRelApplyPackageListSearchBean() {
		return relApplyPackageListSearchBean;
	}
	public void setRelApplyPackageListSearchBean(
			RelApplyPackageListSearchBean relApplyPackageListSearchBean) {
		this.relApplyPackageListSearchBean = relApplyPackageListSearchBean;
	}
	public List<ItemLabelsBean> getSearchLotNoList() {
		return searchLotNoList;
	}
	public void setSearchLotNoList(List<ItemLabelsBean> searchLotNoList) {
		this.searchLotNoList = searchLotNoList;
	}
	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}
	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}
	public List<ItemLabelsBean> getSearchRelUnitCountList() {
		return searchRelUnitCountList;
	}
	public void setSearchRelUnitCountList(
			List<ItemLabelsBean> searchRelUnitCountList) {
		this.searchRelUnitCountList = searchRelUnitCountList;
	}
	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}
	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}
	public List<UnitViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList(List<UnitViewBean> unitViewBeanList) {
		this.unitViewBeanList = unitViewBeanList;
	}
	public RelApplyEntryViewBean getRelApplyEntryViewBean() {
		return relApplyEntryViewBean;
	}
	public void setRelApplyEntryViewBean(RelApplyEntryViewBean relApplyEntryViewBean) {
		this.relApplyEntryViewBean = relApplyEntryViewBean;
	}
	
}
