package jp.co.blueship.tri.rm.ui.ra.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseListViewServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyCloseEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.ui.ra.rb.RelApplyCloseResponseBean;

public class FlowRelApplyCloseProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelApplyCloseService";

	public static final String[] screenFlows = new String[] {
														RelApplyScreenID.CLOSE,
														RelApplyScreenID.CLOSE_COMP
														 };
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelApplyScreenID.CLOSE_LIST_VIEW
														 };

	public FlowRelApplyCloseProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}


	public FlowRelApplyCloseProsecutor() {
		super(null);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelApplyCloseServiceBean seBean	= (FlowRelApplyCloseServiceBean)session.getAttribute( FLOW_ACTION_ID );

		FlowRelApplyCloseServiceBean bean	= null;
		if ( seBean != null ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowRelApplyCloseServiceBean();
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		// 上位のフローのセッション情報を取得
		FlowRelApplyCloseListViewServiceBean closeListViewServiceBean = (FlowRelApplyCloseListViewServiceBean)session.getAttribute(FlowRelApplyCloseListViewProsecutor.FLOW_ACTION_ID);

		// リクエスト情報取得
		{
			//[一覧]→[クローズ]画面遷移時
			if ( RelApplyScreenID.CLOSE_LIST_VIEW.equals( referer )
					&& RelApplyScreenID.CLOSE.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					RelApplyViewBean viewBean = bean.newRelApplyViewBean();
					bean.setRelApplyViewBean(viewBean);

					String[] selectedRelApplyNo = reqInfo.getParameterValues("selectedRelApplyNoParam");
					viewBean.setSelectedRelApplyNo(selectedRelApplyNo);

					if ( null != closeListViewServiceBean.getRelApplySearchBean().getSelectedServerNo() ) {
						bean.setLockLotNo		( closeListViewServiceBean.getRelApplySearchBean().getSelectedLotNo() );
						bean.setLockServerId	( closeListViewServiceBean.getRelApplySearchBean().getSelectedServerNo() );
						session.setAttribute	( SessionScopeKeyConsts.SELECTED_LOT_NO, closeListViewServiceBean.getRelApplySearchBean().getSelectedLotNo() );
						session.setAttribute	( SessionScopeKeyConsts.SELECTED_SERVER_ID, closeListViewServiceBean.getRelApplySearchBean().getSelectedServerNo() );
					}
				}
			}
			//[クローズ]→[クローズ完了]画面遷移時
			if ( RelApplyScreenID.CLOSE.equals( referer )
					&& RelApplyScreenID.CLOSE_COMP.equals( forward ) ) {

				if ( !ScreenType.bussinessException.equals(bean.getScreenType()) ) {
					RelApplyCloseEntryViewBean closeBean = bean.newRelApplyCloseEntryViewBean();
					bean.setRelApplyCloseEntryViewBean(closeBean);

					String closeComment = reqInfo.getParameter("closeComment");
					closeBean.setCloseComment(closeComment);
				}
			}
		}

		// ページ遷移はしない

		//サービスビーンごとセッションに保持する
		session.setAttribute( FLOW_ACTION_ID , bean ) ;

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelApplyCloseResponseBean resBean = new RelApplyCloseResponseBean();
		FlowRelApplyCloseServiceBean bean	= (FlowRelApplyCloseServiceBean)getServiceReturnInformation();

		resBean.setRelApplyViewBeanList(bean.getRelApplyViewBeanList());
		resBean.setRelApplyViewBean(bean.getRelApplyViewBean());

		if ( resBean != null ) {
			resBean.new MessageUtility().reflectMessage( bean );
			resBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}
	}

	@Override
	protected void preProcessor(IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo) {
		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
