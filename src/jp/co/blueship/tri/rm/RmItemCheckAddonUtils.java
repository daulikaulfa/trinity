package jp.co.blueship.tri.rm;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenItemID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * 画面入力項目の必須チェック、形式チェックの共通処理Class
 * ItemCheckAddonUtilのRm版
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RmItemCheckAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース申請情報入力項目チェック
	 * @param define リソース管理XMLの列挙型
	 * @param searchBean
	 * @param viewBean 確認情報
	 */

	public static void checkRelApplyEntryInput(
			RmDesignBeanId define,
			RelApplyPackageListSearchBean searchBean,
			RelApplyEntryViewBean viewBean ) {

		checkRelApplyInput(
				define,
				searchBean.getSelectedGroupId(),
				searchBean.getSelectedRelEnvNo(),
				searchBean.getSelectedRelWishDate(),
				viewBean );
	}

	/**
	 * リリース申請情報入力項目チェック
	 * @param define リソース管理XMLの列挙型
	 * @param searchBean
	 * @param viewBean 確認情報
	 */

	public static void checkRelApplyModifyInput(
			RmDesignBeanId define,
			RelApplySearchBean searchBean,
			RelApplyEntryViewBean viewBean ) {

		checkRelApplyInput(
				define,
				searchBean.getSelectedGroupId(),
				searchBean.getSelectedRelEnvNo(),
				searchBean.getSelectedRelWishDate(),
				viewBean );
	}

	/**
	 * リリース申請情報入力項目チェック
	 * @param define リソース管理XMLの列挙型
	 * @param groupId 入力したグループID
	 * @param relEnvNo 入力したリリース環境ID
	 * @param relWishDate 入力したリリース希望日
	 * @param viewBean 確認情報
	 */

	private static void checkRelApplyInput(
			RmDesignBeanId define,
			String groupId,
			String relEnvNo,
			String relWishDate,
			RelApplyEntryViewBean viewBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			//申請グループ
			if ( !viewBean.isDraff() && TriStringUtils.isEmpty( groupId ) ) {
				messageList.add		( RmMessageId.RM001008E );
				messageArgsList.add	( new String[] {} );
			}

			//リリース環境
			if ( !viewBean.isDraff() && TriStringUtils.isEmpty( relEnvNo ) ) {
				messageList.add		( RmMessageId.RM001001E );
				messageArgsList.add	( new String[] {} );
			}

			//申請者
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							define,
							RelApplyScreenItemID.RelApplyInput.REL_APPLY_USER.toString() )) ) {

				if ( !viewBean.isDraff() && TriStringUtils.isEmpty( viewBean.getRelApplyUser() )) {
					messageList.add		( RmMessageId.RM001009E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//申請責任者
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							define,
							RelApplyScreenItemID.RelApplyInput.RESPONSIBLE_USER.toString() )) ) {

				if ( !viewBean.isDraff() && TriStringUtils.isEmpty( viewBean.getRelApplyPersonCharge() )) {
					messageList.add		( RmMessageId.RM001010E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//リリース希望日
			if ( TriStringUtils.isEmpty( relWishDate ) ) {
				if ( !viewBean.isDraff() ) {
					messageList.add		( RmMessageId.RM001002E );
					messageArgsList.add	( new String[] {} );
				}
			} else {
				if (!TriDateUtils.checkYMD(relWishDate)) {
					messageList.add		( RmMessageId.RM001040E );
					messageArgsList.add	( new String[] {} );
				} else {
					relWishDate = TriDateUtils.fillYMD(relWishDate);

					String today = TriDateUtils.getYMDDateFormat().format( TriDateUtils.getSystemTimestamp() );

					if (!TriDateUtils.before(today, relWishDate)) {
						messageList.add		( RmMessageId.RM001039E );
						messageArgsList.add	( new String[] {} );
					}
				}
			}

			//関連番号
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							define,
							RelApplyScreenItemID.RelApplyInput.RELATION_NO.toString() )) ) {

				if ( TriStringUtils.isEmpty( viewBean.getRelatedNo() )) {
					messageList.add		( RmMessageId.RM001011E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//備考
			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							define,
							RelApplyScreenItemID.RelApplyInput.REMARKS.toString() )) ) {

				if ( TriStringUtils.isEmpty( viewBean.getRemarks() )) {
					messageList.add		( RmMessageId.RM001012E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//添付ファイル
			for ( int i = 1; i <= 5; i++ ) {
				if ( !StatusFlg.off.value().equals(
						sheet.getValue(
								define,
								RelApplyScreenItemID.RelApplyInput.APPEND_FILE.toString() + Integer.toString(i) )) ) {
					Map<String, AppendFileBean> appendFileMap = viewBean.getAppendFile();

					SessionScopeKeyConsts.AppendFileEnum appendFileEnum = SessionScopeKeyConsts.AppendFileEnum.getValue("000" + i);
					if ( ! appendFileMap.containsKey(appendFileEnum.getId())
						|| TriStringUtils.isEmpty(appendFileMap.get(appendFileEnum.getId()).getAppendFile()) ) {
						messageList.add		( RmMessageId.RM001013E );
						messageArgsList.add	( new String[] {appendFileEnum.getId()} );
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}
	/**
	 * リリース申請情報入力項目チェック（存在確認）
	 * @param define リソース管理XMLの列挙型
	 * @param serviceBean
	 * @param searchBean
	 * @param viewBean 確認情報
	 */

	public static void checkRelApplyEntryConfirm(
			FlowRelApplyEditSupport support,
			IUmFinderSupport umFinderSupport,
			GenericServiceBean serviceBean,
			RelApplyPackageListSearchBean searchBean,
			RelApplyEntryViewBean viewBean ) {

		checkRelApplyConfirm(
				support,
				umFinderSupport,
				serviceBean.getUserId(),
				searchBean.getSelectedGroupId(),
				searchBean.getSelectedRelEnvNo(),
				searchBean.getSelectedRelWishDate(),
				viewBean );
	}

	/**
	 * リリース申請情報入力項目チェック（存在確認）
	 * @param define リソース管理XMLの列挙型
	 * @param serviceBean
	 * @param searchBean
	 * @param viewBean 確認情報
	 */

	public static void checkRelApplyModifyConfirm(
			FlowRelApplyEditSupport support,
			IUmFinderSupport umFinderSupport,
			GenericServiceBean serviceBean,
			RelApplySearchBean searchBean,
			RelApplyEntryViewBean viewBean ) {

		checkRelApplyConfirm(
				support,
				umFinderSupport,
				serviceBean.getUserId(),
				searchBean.getSelectedGroupId(),
				searchBean.getSelectedRelEnvNo(),
				searchBean.getSelectedRelWishDate(),
				viewBean );
	}

	/**
	 * リリース申請情報入力項目チェック（存在確認）
	 * @param support 業務支援サポート
	 * @param userId ログインユーザID
	 * @param groupId 入力したグループID
	 * @param relEnvNo 入力したリリース環境ID
	 * @param relWishDate 入力したリリース希望日
	 * @param viewBean 確認情報
	 */

	private static void checkRelApplyConfirm(
			FlowRelApplyEditSupport support,
			IUmFinderSupport umFinderSupport,
			String userId,
			String groupId,
			String relEnvNo,
			String relWishDate,
			RelApplyEntryViewBean viewBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			//申請グループ
			IGrpUserLnkEntity groupUserview = null;
			List<IGrpUserLnkEntity> groupUserviews = umFinderSupport.findGrpUserLnkByUserId( userId );
			IGrpEntity grpEntity = null;
			for (IGrpUserLnkEntity view : groupUserviews) {

				if(groupId.equals( view.getGrpId() ) ) {

					groupUserview = view;
					grpEntity = umFinderSupport.findGroupById( view.getGrpId() );
					break;
				}
			}
			if ( groupUserview == null ) {
				messageList.add		( RmMessageId.RM001005E );
				messageArgsList.add	( new String[] {} );
				grpEntity.setGrpNm("");
			}

			//リリース環境
			BldEnvCondition bcondition = new BldEnvCondition();
			bcondition.setBldEnvId(relEnvNo);
			IBldEnvEntity relEnvEntity = support.getBmFinderSupport().getBldEnvDao().find( bcondition.getCondition() ).get(0);
			if ( null == relEnvEntity || StatusFlg.on.value().equals(relEnvEntity.getDelStsId().value()) ) {
				messageList.add		( RmMessageId.RM001006E );
				messageArgsList.add	( new String[] {} );
			}

			//リリース希望日
			ICalEntity[] relCalendarEntitys = null;
			relCalendarEntitys = support.getRelCalendarEntity( relWishDate ) ;

			if ( TriStringUtils.isEmpty(relCalendarEntitys) ) {
				messageList.add		( RmMessageId.RM001007E );
				messageArgsList.add	( new String[] {} );
			}

			//ロット
			{
				IJdbcCondition condition = DBSearchConditionAddonUtil.getActiveLotCondition( new String[] {viewBean.getLotNo()} );

				if ( 0 == support.getLotDao().count(condition.getCondition()) ) {
					messageList.add		( RmMessageId.RM001014E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//ビルドパッケージ
			{
				Set<String> buildNoSet = new LinkedHashSet<String>();
				for (jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean unitBean : viewBean.getUnitBeanList()) {
					buildNoSet.add(unitBean.getBuildNo());
				}

				BpCondition condition = new BpCondition();
				condition.setBpIds( buildNoSet.toArray(new String[0]) );

				if ( buildNoSet.size() != support.getBmFinderSupport().getBpDao().count(condition.getCondition()) ) {
					messageList.add		( RmMessageId.RM001015E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//選択項目の移し換え
			if ( null != groupUserview ) {
				viewBean.setRelApplyGroupId( groupUserview.getGrpId() );
			}
			if ( null != grpEntity ) {
				viewBean.setRelApplyGroup( grpEntity.getGrpNm() );
			}
			if ( null != relEnvEntity ) {
				viewBean.setRelEnvNo( relEnvEntity.getBldEnvId() );
				viewBean.setRelEnvName( relEnvEntity.getBldEnvNm() );
			}
			if ( null != relCalendarEntitys && 0 < relCalendarEntitys.length ) {
				viewBean.setRelWishDate( relCalendarEntitys[0].getPreferredRelDate() );
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

}
