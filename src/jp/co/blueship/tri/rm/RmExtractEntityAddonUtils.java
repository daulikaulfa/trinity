package jp.co.blueship.tri.rm;

import java.util.List;

import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。
 * ExtractEntityAddonUtilの派生クラスです。依存性の解消のために作成しました。
 *
 * @author Takashi Ono
 *
 */public class RmExtractEntityAddonUtils {

	 /**
	  * 指定されたリストからリリース申請情報を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static IRaEntity extractRelApply(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof IRaEntity) {
				 return (IRaEntity) obj;
			 }
		 }

		 return null;
	 }
	 /**
	  * 指定されたリストからリリース申請Dtoを取得します。
	  *
	  * @param list
	  * @return
	  */
	 public static IRaDto extractRaDto(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof IRaDto) {
				 return (IRaDto) obj;
			 }
		 }

		 return null;
	 }

	 public static RaDtoList extractRaDtoList(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof RaDtoList ) {
				 return ( RaDtoList ) obj;
			 }
		 }

		 return null;
	 }

	 /**
	  * 指定されたリストからリリース情報の配列を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static IRpEntity[] extractRelArray(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof IRpEntity[]) {
				 return (IRpEntity[]) obj;
			 }
		 }

		 return null;
	 }

	 /**
	  * パラメータリストよりIReleaseTaskBeanを取得します。
	  *
	  * @param paramList パラメータリスト
	  * @return 取得した情報を戻します。
	  * @throws TriSystemException
	  */
	 public static final IReleaseTaskBean extractReleaseTask(List<Object> paramList) throws TriSystemException {

		 for (Object obj : paramList) {
			 if (obj instanceof IReleaseTaskBean) {
				 return ((IReleaseTaskBean) obj);
			 }
		 }

		 return null;
	 }
	 /**
	  * 指定されたリストから資産agentのステータス情報を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static AgentStatus[] extractAgentStatusArray(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof AgentStatus[]) {
				 return (AgentStatus[]) obj;
			 }
		 }

		 return null;
	 }
	 /**
	  * パラメータリストより外部入力パラメータの格納beanを取得します。
	  *
	  * @param paramList パラメータリスト
	  * @return 取得した情報を戻します。
	  * @throws TriSystemException
	  */
	 public static final GenericServiceBean getGenericServiceBean(List<Object> paramList) throws TriSystemException {

		 for (Object obj : paramList) {
			 if (obj instanceof GenericServiceBean) {
				 return ((GenericServiceBean) obj);
			 }
		 }

		 return null;
	 }
	 /**
	  * 指定されたリストからレポート情報エンティティを取得します。
	  *
	  * @param paramList パラメータリスト
	  * @return 取得した情報を戻します。
	  * @throws TriSystemException
	  */
	 public static final IRepEntity extractReport(List<Object> list) throws TriSystemException {

		 for (Object obj : list) {
			 if (obj instanceof IRepEntity) {
				 return ((IRepEntity) obj);
			 }
		 }

		 return null;
	 }

	 /**
	  * 指定されたリストからリリース情報を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static IRpDto extractRpDto(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof IRpDto) {
				 return (IRpDto) obj;
			 }
		 }

		 return null;
	 }

	 /**
	  * 指定されたリストからビルドパッケージビルド情報の配列を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static BpDtoList extractBpDtoList(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof BpDtoList) {
				 return (BpDtoList) obj;
			 }
		 }

		 return null;
	 }
	 /**
	  * 指定されたリストからロット情報を取得します。
	  *
	  * @param list 検索するリスト
	  * @return 取得した値を戻します。取得できない場合はnullを戻します。
	  */
	 public static ILotEntity extractPjtLot(List<Object> list) {

		 for (Object obj : list) {
			 if (obj instanceof ILotEntity) {
				 return (ILotEntity) obj;
			 }
		 }

		 return null;
	 }
	 /**
	  * パラメータリストより申請情報エンティティの配列を取得します。
	  *
	  * @param list パラメータリスト
	  * @return 取得した情報を戻します。
	  * @throws TriSystemException
	  */
	 public static final AreqDtoList extractAreqDtoList(List<Object> list) throws TriSystemException {

		 for (Object obj : list) {
			 if (obj instanceof AreqDtoList) {
				 return ((AreqDtoList) obj);
			 }
		 }

		 return null;
	 }
 }
