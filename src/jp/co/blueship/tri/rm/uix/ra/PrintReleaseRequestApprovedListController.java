package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.SearchCondition;;

/**
 * 
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/approved")
public class PrintReleaseRequestApprovedListController extends TriControllerSupport<FlowReleaseRequestApprovedListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmPrintReleaseRequestApprovedListService;
	}

	@Override
	protected FlowReleaseRequestApprovedListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovedListServiceBean bean = new FlowReleaseRequestApprovedListServiceBean();
		return bean;
	}
	
	@RequestMapping(value = "/list/print")
	public String print(FlowReleaseRequestApprovedListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();
		
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view)) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		model.getModel()
			.addAttribute("view", TriView.PrintReleaseRequestList.value())
			.addAttribute("printTabName", "Approved")
			.addAttribute("result", bean)
		;
		
		return view;
	}

	private void mapping(FlowReleaseRequestApprovedListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		int selectedPageNo = 1;
		if (null != requestInfo.getParameter("approved_pageNo")) {
			selectedPageNo = Integer.parseInt(requestInfo.getParameter("approved_pageNo"));
		}
		searchCondition.setBldEnvId		( requestInfo.getParameter("approved_condRelEnv") )
						.setStsId		( requestInfo.getParameter("approved_condStatus") )
						.setCtgId		( requestInfo.getParameter("approved_condCategory") )
						.setMstoneId	( requestInfo.getParameter("approved_condMilestone") )
						.setKeyword		( requestInfo.getParameter("approved_searchKeyword") )
						.setSelectedPageNo		
										( selectedPageNo )
						;
	}
	
}