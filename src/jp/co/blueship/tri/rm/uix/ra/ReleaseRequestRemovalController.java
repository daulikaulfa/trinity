package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestRemovalServiceBean;
/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
@Controller
@RequestMapping("/release/request")
public class ReleaseRequestRemovalController extends TriControllerSupport<FlowReleaseRequestRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestRemovalService;
	}

	@Override
	protected FlowReleaseRequestRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestRemovalServiceBean bean = new FlowReleaseRequestRemovalServiceBean();
		return bean;
	}

	@RequestMapping(value = "/remove")
	public String index(FlowReleaseRequestRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/release/request/redirect";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean ).addFlashAttribute("draft",bean.getResult().isRedirectToDraft());
		return view;
	}

	private void mapping(FlowReleaseRequestRemovalServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		if(RequestType.init.equals( bean.getParam().getRequestType())){
			if ( requestInfo.getParameter("raId") != null )
			bean.getParam().setSelectedRaId(requestInfo.getParameter("raId"));
		}
	}

}

