package jp.co.blueship.tri.rm.uix.ra;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.AppendFileBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean.RequestOption;
/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/release/request/edit")
public class ReleaseRequestEditController extends TriControllerSupport<FlowReleaseRequestEditServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestEditService;
	}

	@Override
	protected FlowReleaseRequestEditServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestEditServiceBean bean = new FlowReleaseRequestEditServiceBean();
		return bean;
	}

	@RequestMapping
	public String listView(FlowReleaseRequestEditServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if(bean.getResult().isCompleted() && RequestType.onChange.equals(bean.getParam().getRequestType())){
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				return "redirect:/release/request/redirect";
			}

			if( bean.getResult().isCompleted() 
					&& RequestType.submitChanges.equals(bean.getParam().getRequestType())
					&& SubmitMode.request.equals(bean.getParam().getInputInfo().getSubmitMode())) {
				
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/release/request/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestEdit.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasereqSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value="/upload")
	public @ResponseBody String upload(@RequestParam("userFile") MultipartFile userFile, FlowReleaseRequestEditServiceBean bean, TriModel model) {
		String message = "success";

		try {
			this.uploadFile(userFile, bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	private void uploadFile(MultipartFile userFile, FlowReleaseRequestEditServiceBean bean, TriModel model) throws IOException {
		List<AppendFileBean> appendFileList = bean.getParam().getInputInfo().getAppendFile();

		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			AppendFileBean fileBean = new ReleaseRequestEditInputBean().new AppendFileBean();
			fileBean.setAppendFileInputStreamBytes(fileBytes);
			fileBean.setAppendFileNm(userFile.getOriginalFilename());
			appendFileList.add(fileBean);
		}
	}

	@RequestMapping(value="/upload/delete")
	public @ResponseBody String delete(FlowReleaseRequestEditServiceBean bean, TriModel model) {
		String message = "success";


		try {
			bean.getParam().setRequestOption(RequestOption.deleteUplodedFile);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	private void mapping(FlowReleaseRequestEditServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		if(RequestType.init.equals( bean.getParam().getRequestType())){
			if ( requestInfo.getParameter("raId") != null ){
				bean.getParam().setSelectedRaId(requestInfo.getParameter("raId"));
			}

		}

		if (RequestType.onChange.equals( bean.getParam().getRequestType()) ){
			ReleaseRequestEditInputBean inputInfo = bean.getParam().getInputInfo();
			if(RequestOption.deleteUplodedFile.equals(bean.getParam().getRequestOption())) {
				inputInfo.setDeletedFileNm(requestInfo.getParameter("deletedFileNm"));
				inputInfo.setDeletedFileSeqNo(requestInfo.getParameter("deletedFileSeqNo"));
				return;
			}
		}

		if (RequestType.submitChanges.equals( bean.getParam().getRequestType()) ){
			bean.getParam().getInputInfo()
				.setSubmitterId 	( requestInfo.getParameter("submitterId"))
				.setGroupId     	( requestInfo.getParameter("groupId"))
				.setSupervisor  	( requestInfo.getParameter("supervisor"))
				.setRelEnv      	( requestInfo.getParameter("relEnv"))
				.setPreferredDate	( requestInfo.getParameter("preferredDate"))
				.setBpId			( requestInfo.getParameter("bpId"))
				.setRelatedId		( requestInfo.getParameter("relatedId") )
				.setRemarks			( requestInfo.getParameter("remarks"))
				.setCtgId       	( requestInfo.getParameter("ctgId"))
				.setMstoneId    	( requestInfo.getParameter("mstoneId"))
				.setSubmitMode		( SubmitMode.value(requestInfo.getParameter("SubmitMode")) )
				;
		}
	}
}