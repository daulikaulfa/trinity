package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;
/**
 *
 * @version V4.00.00
 * @author Sam
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
@Controller
@RequestMapping("/release/request")
public class ReleaseRequestListController extends TriControllerSupport<FlowReleaseRequestListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestListService;
	}

	@Override
	protected FlowReleaseRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestListServiceBean bean = new FlowReleaseRequestListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/error")
	public String error(FlowReleaseRequestListServiceBean bean,
			TriModel model) {
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowReleaseRequestListServiceBean bean, TriModel model ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = {"","/"})
	public String index(FlowReleaseRequestListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		boolean isDraft = false;
		try {
			if(model.getModel().asMap().get("draft") != null) {
				isDraft = (boolean)model.getModel().asMap().get("draft");
			}
			bean.getParam().setSelectedDraft(isDraft);
			bean.getParam().setRequestType(RequestType.init);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if (isDraft) {
			model.getModel().addAttribute("pagination", bean.getDraftPage());
		} else {
			model.getModel().addAttribute("pagination", bean.getPage());
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasereqSubmenu")
			.addAttribute("tabName", "List")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/search"})
	public String search(FlowReleaseRequestListServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		if (isDraft) {
			model.getModel().addAttribute("pagination", bean.getDraftPage());
		} else {
			model.getModel().addAttribute("pagination", bean.getPage());
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasereqSubmenu")
			.addAttribute("tabName", "List")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	@RequestMapping(value = "/filter/save" )
	public String save(FlowReleaseRequestListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";
		Boolean isDraft = bean.getParam().isSelectedDraft();

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() );

			if(isDraft){
				searchFilterBean.getParam().getInputInfo().setSearchFilter( bean.getParam().getSearchDraftCondition() );
			}else{
				searchFilterBean.getParam().getInputInfo().setSearchFilter( bean.getParam().getSearchCondition() );
			}

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}

	@RequestMapping(value = "/filter/search")
	public String receive (FlowReleaseRequestListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		Boolean isDraft = false;

		try {
			model.setRedirect(true);
			String json = null;

			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			SearchCondition condition = gson.fromJson(json, SearchCondition.class);

			isDraft = condition.isDraft();
			if ( isDraft ) {
				bean.getParam().setSelectedDraft(true);
				bean.getParam().setSearchDraftCondition(condition);
				bean.getParam().getSearchDraftCondition().setSelectedPageNo(1);
			} else {
				bean.getParam().setSelectedDraft(false);
				bean.getParam().setSearchCondition(condition);
				bean.getParam().getSearchCondition().setSelectedPageNo(1);
			}
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		if (isDraft) {
			model.getModel().addAttribute("pagination", bean.getDraftPage());
			model.getModel().addAttribute("selectedTabId", "1");
		} else {
			model.getModel().addAttribute("pagination", bean.getPage());
			model.getModel().addAttribute("selectedTabId", "0");
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("tabName", "List")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReleaseRequestListServiceBean bean, TriModel model){

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		if(RequestType.init.equals(bean.getParam().getRequestType())) {
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition(false) );
				bean.getParam().setSearchDraftCondition(bean.new SearchCondition(true) );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if(RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if (isDraft) {
				SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();
				bean.getParam().setSelectedDraft(true);

				String draftSelectedPageNo = requestInfo.getParameter("draft_pageNo");
				if (TriStringUtils.isNotEmpty(draftSelectedPageNo)) {
					searchDraftCondition.setSelectedPageNo(Integer.parseInt(draftSelectedPageNo));
				} else {
					searchDraftCondition.setSelectedPageNo(1);
				}

				searchDraftCondition
					.setBldEnvId(requestInfo.getParameter("draft_condRelEnv"))
					.setCtgId(requestInfo.getParameter("draft_condCategory"))
					.setMstoneId(requestInfo.getParameter("draft_condMilestone"))
					.setKeyword(requestInfo.getParameter("draft_searchKeyword"))
				;

			} else {
				SearchCondition searchCondition = bean.getParam().getSearchCondition();
				bean.getParam().setSelectedDraft(false);

				String selectedPageNo = requestInfo.getParameter("selectedPageNo");
				if (TriStringUtils.isNotEmpty(selectedPageNo)) {
					searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
				} else {
					searchCondition.setSelectedPageNo(1);
				}

				searchCondition
					.setBldEnvId(requestInfo.getParameter("condRelEnv"))
					.setStsId(requestInfo.getParameter("condStatus"))
					.setCtgId(requestInfo.getParameter("condCategory"))
					.setMstoneId(requestInfo.getParameter("condMilestone"))
					.setKeyword(requestInfo.getParameter("searchKeyword"))
				;
			}

		}
	}
}
