package jp.co.blueship.tri.rm.uix.ra;

import java.io.IOException;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.AppendFileBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean.RequestOption;;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/release/request/create")
public class ReleaseRequestController extends TriControllerSupport<FlowReleaseRequestServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestService;
	}

	@Override
	protected FlowReleaseRequestServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestServiceBean bean = new FlowReleaseRequestServiceBean();
		return bean;
	}

	@RequestMapping
	public String listView(FlowReleaseRequestServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if (bean.getResult().isCompleted() && RequestType.submitChanges.equals(bean.getParam().getRequestType())){
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				return "redirect:/release/request/redirect";
			}


		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestCreation.value())
			.addAttribute("selectedMenu" , "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/save")
	public String refresh(FlowReleaseRequestServiceBean bean,
						  TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.init);
			this.execute(getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestCreation.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasereqSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value="/upload")
	public @ResponseBody String upload(@RequestParam("userFile") MultipartFile userFile, FlowReleaseRequestServiceBean bean, TriModel model) {
		String message = "success";

		try {
			String seqNo = model.getRequestInfo().getParameter("seqNo");
			this.uploadFile(userFile, seqNo, bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	private void uploadFile(MultipartFile userFile, String seqNo, FlowReleaseRequestServiceBean bean, TriModel model) throws IOException {
		List<AppendFileBean> appendFileList = bean.getParam().getInputInfo().getAppendFile();

		if (!userFile.isEmpty()) {
			byte[] fileBytes = userFile.getBytes();
			AppendFileBean fileBean = new ReleaseRequestEditInputBean().new AppendFileBean();
			fileBean.setAppendFileInputStreamBytes(fileBytes);
			fileBean.setAppendFileNm(userFile.getOriginalFilename());
			fileBean.setSeqNo(seqNo);
			appendFileList.add(fileBean);
		}
	}

	@RequestMapping(value="/upload/delete")
	public @ResponseBody String delete(FlowReleaseRequestServiceBean bean, TriModel model) {
		String message = "success";
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			message = "fail";
			e.printStackTrace();
		}
		return message;
	}

	private void mapping(FlowReleaseRequestServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		if (RequestType.init.equals( bean.getParam().getRequestType())){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.onChange.equals( bean.getParam().getRequestType() )) {
			ReleaseRequestEditInputBean inputInfo = bean.getParam().getInputInfo();
			RequestOption requestOption = RequestOption.value(requestInfo.getParameter("RequestOption"));

			bean.getParam().setRequestOption(requestOption);
			if (RequestOption.deleteUplodedFile.equals(requestOption)) {
				inputInfo.setDeletedFileNm(requestInfo.getParameter("deletedFileNm"));
				inputInfo.setDeletedFileSeqNo(requestInfo.getParameter("deletedFileSeqNo"));
				return;
			}

			if (RequestOption.selectBpId.equals(requestOption)) {
				inputInfo.setBpId(requestInfo.getParameter("bpId"));
				inputInfo.setSubmitMode(SubmitMode.value(requestInfo.getParameter("SubmitMode")));
			}
		}

		if (RequestType.submitChanges.equals( bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			ReleaseRequestEditInputBean inputInfo = bean.getParam().getInputInfo()
				.setSubmitterId 	( requestInfo.getParameter("submitterId") )
				.setGroupId     	( requestInfo.getParameter("groupId") )
				.setSupervisor  	( requestInfo.getParameter("supervisor") )
				.setRelEnv      	( requestInfo.getParameter("relEnv") )
				.setPreferredDate	( requestInfo.getParameter("preferredDate") )
				.setRelatedId		( requestInfo.getParameter("relatedId") )
				.setRemarks			( requestInfo.getParameter("remarks") )
				.setCtgId       	( requestInfo.getParameter("ctgId") )
				.setMstoneId    	( requestInfo.getParameter("mstoneId") )
				.setSubmitMode 		( SubmitMode.value(requestInfo.getParameter("SubmitMode")) )
				;
			bean.getParam().setInputInfo(inputInfo);
		}
	}
}