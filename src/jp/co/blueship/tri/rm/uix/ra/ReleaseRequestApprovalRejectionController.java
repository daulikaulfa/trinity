package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalRejectionServiceBean;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/release/request/pending/reject")
public class ReleaseRequestApprovalRejectionController extends TriControllerSupport<FlowReleaseRequestApprovalRejectionServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovalRejectionService;
	}

	@Override
	protected FlowReleaseRequestApprovalRejectionServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovalRejectionServiceBean bean = new FlowReleaseRequestApprovalRejectionServiceBean();
		return bean;
	}

	@RequestMapping
	public String index(FlowReleaseRequestApprovalRejectionServiceBean bean, TriModel model) {
		String view = "redirect:/release/request/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	private void mapping(FlowReleaseRequestApprovalRejectionServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		if ( requestInfo.getParameter("raId") != null )
		bean.getParam().setSelectedRaId(requestInfo.getParameter("raId"));
	}

}

