package jp.co.blueship.tri.rm.uix.ra;

import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestAttachmentDownloadServiceBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/download")
public class ReleaseRequestAttachmentDownloadController extends TriControllerSupport<FlowReleaseRequestAttachmentDownloadServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestAttachmentDownloadService;
	}

	@Override
	protected FlowReleaseRequestAttachmentDownloadServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestAttachmentDownloadServiceBean bean = new FlowReleaseRequestAttachmentDownloadServiceBean();
		return bean;
	}

	@RequestMapping(value = { "/", "" })
	public void download(HttpServletResponse response,
						FlowReleaseRequestAttachmentDownloadServiceBean bean,
						TriModel model) {
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				StreamUtils.download(response, bean.getFileResponse().getFile());
			}

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}

		model.getModel().addAttribute("result", bean);
	}

	@RequestMapping("/validate")
	public String validate(FlowReleaseRequestAttachmentDownloadServiceBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			bean.getParam().setRequestType(RequestType.validate);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}
	
	@RequestMapping("/attachment")
	public void downloadAttachment( FlowReleaseRequestAttachmentDownloadServiceBean bean, TriModel model, HttpServletResponse response) {
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			StreamUtils.download(response, bean.getFileResponse().getFile());

		} catch ( Exception e ) {
			ExceptionUtils.printStackTrace(e);
		}
	}

	private void mapping (FlowReleaseRequestAttachmentDownloadServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedRaId		( requestInfo.getParameter("raId") );
		bean.getParam().setSelectedFileNm	( requestInfo.getParameter("fileName") );
	}
}
