package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.SearchCondition;

/**
 * @version V4.00.00
 * @author Hai Thach
 */
@Controller
@RequestMapping("/release/request/approved")
public class ReleaseRequestApprovedListController
				extends TriControllerSupport<FlowReleaseRequestApprovedListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovedListService;
	}

	@Override
	protected FlowReleaseRequestApprovedListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovedListServiceBean bean = new FlowReleaseRequestApprovedListServiceBean();
		return bean;
	}

	@RequestMapping(value= {"","/"})
	public String list (FlowReleaseRequestApprovedListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sessionInfo = model.getSessionInfo();
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sessionInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sessionInfo, bean));
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
			.addAttribute("selectedTabId", 3)
		;

		setPrev(model);
		return view;
	}

	private void mapping (FlowReleaseRequestApprovedListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			searchCondition
				.setBldEnvId( requestInfo.getParameter("approved_condRelEnv") )
				.setStsId	( requestInfo.getParameter("approved_condStatus") )
				.setCtgId	( requestInfo.getParameter("approved_condCategory") )
				.setMstoneId( requestInfo.getParameter("approved_condMilestone") )
				.setKeyword	( requestInfo.getParameter("approved_searchKeyword") )
			;

			if(requestInfo.getParameter("approved_pageNo") != null){
				searchCondition.setSelectedPageNo( Integer.parseInt(requestInfo.getParameter("approved_pageNo")) );
			}else {
				searchCondition.setSelectedPageNo(1);
			}

		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowReleaseRequestApprovedListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}


	@RequestMapping(value = "/filter/search" )
	public String receive(FlowReleaseRequestApprovedListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowReleaseRequestApprovedListServiceBean.SearchCondition condition = gson.fromJson(json, FlowReleaseRequestApprovedListServiceBean.SearchCondition.class);
			bean.getParam().setSearchCondition( condition );
			bean.getParam().setLinesPerPage(5);
			bean.getParam().getSearchCondition().setSelectedPageNo(1);

			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
			.addAttribute("selectedTabId", 3)
		;

		setPrev(model);
		return view;

	}
}
