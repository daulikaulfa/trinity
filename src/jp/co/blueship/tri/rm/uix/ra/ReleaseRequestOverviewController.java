package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestOverviewServiceBean;


/**
 * @version V4.00.00
 * @author Chung
 */
@Controller
public class ReleaseRequestOverviewController extends TriControllerSupport<FlowReleaseRequestOverviewServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestOverviewService;
	}

	@Override
	protected FlowReleaseRequestOverviewServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestOverviewServiceBean bean = new FlowReleaseRequestOverviewServiceBean();
		return bean;
	}

	@RequestMapping("/release/request/overview")
	public String overview(FlowReleaseRequestOverviewServiceBean bean , TriModel model){

		String view = "common/OverviewPopup::releaseRequestOverview";
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);

		return view;
	}

	private void mapping(FlowReleaseRequestOverviewServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedRaId( requestInfo.getParameter("raId") );
	}
}
