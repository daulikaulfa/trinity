package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition;

/**
 * @version V4.00.00
 * @author Hai Thach
 */
@Controller
@RequestMapping("/release/request/pending")
public class ReleaseRequestApprovalPendingListController
				extends TriControllerSupport<FlowReleaseRequestApprovalPendingListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovalPendingListService;
	}

	@Override
	protected FlowReleaseRequestApprovalPendingListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovalPendingListServiceBean bean = new FlowReleaseRequestApprovalPendingListServiceBean();
		return bean;
	}

	@RequestMapping(value= {"","/"})
	public String list (FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sessionInfo = model.getSessionInfo();
//			bean.getParam().setSelectedLotId((String) sessionInfo.getAttribute( TriSessionAttributes.SelectedLotId.value()));
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sessionInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sessionInfo, bean));
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("tabName", "Pending")
			.addAttribute("result", bean)
			.addAttribute("selectedTabId", 2)
		;

		setPrev(model);

		return view;
	}

	private void mapping (FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			searchCondition
				.setBldEnvId( requestInfo.getParameter("pending_condRelEnv") )
				.setCtgId	( requestInfo.getParameter("pending_condCategory") )
				.setMstoneId( requestInfo.getParameter("pending_condMilestone") )
				.setKeyword	( requestInfo.getParameter("pending_searchKeyword") )
				.setStsId	( requestInfo.getParameter("pending_condStatus") )
				.setSelectedPageNo
							( Integer.parseInt(requestInfo.getParameter("pending_pageNo")) )
			;
		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition condition = gson.fromJson(json, FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition.class);
			bean.getParam().setSearchCondition( condition );
			bean.getParam().getSearchCondition().setSelectedPageNo(1);

			this.searchMapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestList.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu", "releasereqSubmenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("tabName", "Pending")
			.addAttribute("result", bean)
			.addAttribute("selectedTabId", 2)
		;

		setPrev(model);
		return view;
	}


	private void searchMapping (FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			searchCondition
				.setBldEnvId( requestInfo.getParameter("pending_condRelEnv") )
				.setCtgId	( requestInfo.getParameter("pending_condCategory") )
				.setMstoneId( requestInfo.getParameter("pending_condMilestone") )
				.setKeyword	( requestInfo.getParameter("pending_searchKeyword") )
			;

			if (TriStringUtils.isNotEmpty(requestInfo.getParameter("pending_pageNo"))) {
				searchCondition.setSelectedPageNo( Integer.parseInt(requestInfo.getParameter("pending_pageNo")) );
			} else {
				searchCondition.setSelectedPageNo(1);
			}

		}
	}

}
