package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedDetailsServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedDetailsServiceBean.RequestParam;;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/approved")
public class ReleaseRequestApprovedDetailsController extends TriControllerSupport<FlowReleaseRequestApprovedDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovedDetailsService;
	}

	@Override
	protected FlowReleaseRequestApprovedDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovedDetailsServiceBean bean = new FlowReleaseRequestApprovedDetailsServiceBean();
		return bean;
	}

	@RequestMapping(value = {"/details"})
	public String changePropertyDetails( FlowReleaseRequestApprovedDetailsServiceBean bean, TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleaseRequestApprovedDetails.value())
			.addAttribute("selectedMenu"  , "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasereqSubmenu")
			.addAttribute( "result" , bean )
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReleaseRequestApprovedDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("raId") != null ) {
			param.setSelectedRaId( requestInfo.getParameter("raId") );
		}

		if(RequestType.onChange.equals(param.getRequestType())) {
			param.setSelectedFileNm(requestInfo.getParameter("fileName"));
		}

	}
}
