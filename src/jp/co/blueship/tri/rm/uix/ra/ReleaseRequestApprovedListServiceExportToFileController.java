package jp.co.blueship.tri.rm.uix.ra;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceExportToFileBean.RequestParam;
/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/approved/export")
public class ReleaseRequestApprovedListServiceExportToFileController
			extends TriControllerSupport<FlowReleaseRequestApprovedListServiceExportToFileBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovedListServiceExportToFile;
	}

	@Override
	protected FlowReleaseRequestApprovedListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovedListServiceExportToFileBean bean = new FlowReleaseRequestApprovedListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export (FlowReleaseRequestApprovedListServiceExportToFileBean bean, TriModel model) {
		String view = TriView.ReleaseRequestList.value()+"::approvedTable";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel()
			.addAttribute("tabName", "Approved")
			.addAttribute("result", bean)
		;

		return view;
	}

	@RequestMapping("/validate")
	public String validate(FlowReleaseRequestApprovedListServiceExportToFileBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}


	@RequestMapping("/file")
	public void exportFile(FlowReleaseRequestApprovedListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}
	}

	private void mapping(FlowReleaseRequestApprovedListServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));
		SearchCondition  searchCondition = bean.getParam().getSearchCondition();

		if ( RequestType.onChange.equals(param.getRequestType())) {
			searchCondition
				.setBldEnvId	( requestInfo.getParameter("approved_condRelEnv") )
				.setStsId		( requestInfo.getParameter("approved_condStatus") )
				.setCtgId		( requestInfo.getParameter("approved_condCategory") )
				.setMstoneId	( requestInfo.getParameter("approved_condMilestone") )
				.setKeyword		( requestInfo.getParameter("approved_searchKeyword") )
			;

		} else if ( RequestType.submitChanges.equals(param.getRequestType())) {
			String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
			ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
			param.setExportBean(fileBean);
			ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
			param.setSubmitOption(option);
		}
	}
}
