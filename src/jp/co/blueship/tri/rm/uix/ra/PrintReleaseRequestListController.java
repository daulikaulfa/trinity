package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;;

/**
 * 
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request")
public class PrintReleaseRequestListController extends TriControllerSupport<FlowReleaseRequestListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmPrintReleaseRequestListService;
	}

	@Override
	protected FlowReleaseRequestListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestListServiceBean bean = new FlowReleaseRequestListServiceBean();
		return bean;
	}
	
	@RequestMapping(value = "/list/print")
	public String print(FlowReleaseRequestListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();
		
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view)) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		model.getModel()
			.addAttribute("view", TriView.PrintReleaseRequestList.value())
			.addAttribute("printTabName", "List")
			.addAttribute("result", bean)
		;
		
		return view;
	}

	private void mapping(FlowReleaseRequestListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = bean.getParam().getSearchDraftCondition();

		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
		int selectedPageNo = 1;
		
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (isDraft){
			bean.getParam().setSelectedDraft(true);
			if (null != requestInfo.getParameter("draft_pageNo")) {
				selectedPageNo = Integer.parseInt(requestInfo.getParameter("draft_pageNo"));
			}
			
			searchDraftCondition.setBldEnvId( requestInfo.getParameter("draft_condRelEnv") )
								.setCtgId	( requestInfo.getParameter("draft_condCategory") )
								.setMstoneId( requestInfo.getParameter("draft_condMilestone") )
								.setKeyword	( requestInfo.getParameter("draft_searchKeyword") )
								.setSelectedPageNo
											( selectedPageNo )
								;

		} else {
			bean.getParam().setSelectedDraft(false);
			if (null != requestInfo.getParameter("selectedPageNo")) {
				selectedPageNo = Integer.parseInt(requestInfo.getParameter("selectedPageNo"));
			}
			
			searchCondition.setBldEnvId		( requestInfo.getParameter("condRelEnv") )
							.setStsId		( requestInfo.getParameter("condStatus") )
							.setCtgId		( requestInfo.getParameter("condCategory") )
							.setMstoneId	( requestInfo.getParameter("condMilestone") )
							.setKeyword		( requestInfo.getParameter("searchKeyword") )
							.setSelectedPageNo
											( selectedPageNo )
							;
		}	
	}
	
}
