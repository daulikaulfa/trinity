package jp.co.blueship.tri.rm.uix.ra;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceExportToFileBean.RequestParam;

@Controller
@RequestMapping("/release/request/export")
public class ReleaseRequestListServiceExportToFileController extends TriControllerSupport<FlowReleaseRequestListServiceExportToFileBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestListServiceExportToFile;
	}

	@Override
	protected FlowReleaseRequestListServiceExportToFileBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestListServiceExportToFileBean bean = new FlowReleaseRequestListServiceExportToFileBean();
		return bean;
	}

	@RequestMapping
	public String export (FlowReleaseRequestListServiceExportToFileBean bean, TriModel model) {
		String view = null;
		IRequestInfo requestInfo = model.getRequestInfo();

		if (null != requestInfo.getParameter("draft")) {
			Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));
			if (isDraft) {
				view = TriView.ReleaseRequestList.value()+"::draftTable";
			} else {
				view = TriView.ReleaseRequestList.value()+"::resultTable";
			}
		}

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}

		model.getModel()
			.addAttribute("tabName", "List")
			.addAttribute("result", bean)
		;

		return view;
	}

	@RequestMapping("/validate")
	public String validate(FlowReleaseRequestListServiceExportToFileBean bean, TriModel model) {
		String view = TriView.Messages.value() + "::message";
		try {
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		model.getModel().addAttribute("result", bean);
		return view;
	}

	@RequestMapping("/file")
	public void exportFile(FlowReleaseRequestListServiceExportToFileBean bean, TriModel model,
			HttpServletResponse response) {

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			String newFileName =
					DcmExportToFileUtils.getDownloadFileName(
							getServiceId(),
							bean.getParam().getSubmitOption().extention(),
							bean.getLanguage(),
							bean.getTimeZone());

			StreamUtils.download(
					response,
					bean.getFileResponse().getFile(),
					newFileName );

			TriFileUtils.delete(bean.getFileResponse().getFile());

		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		} finally {
			this.removeAttribute(model, bean.getHeader().getWindowsId());
		}
	}

	private void mapping(FlowReleaseRequestListServiceExportToFileBean bean, TriModel model)
			throws UnsupportedEncodingException {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		bean.getParam().setLotId(this.getSessionSelectedLot(sesInfo, bean));

		Boolean isDraft = Boolean.parseBoolean(requestInfo.getParameter("draft"));

		if ( RequestType.onChange.equals(param.getRequestType())) {
			if (isDraft) {
				SearchCondition searchCondition = bean.getParam().getSearchDraftCondition();
				bean.getParam().setSelectedDraft(true);
				searchCondition
					.setBldEnvId	( requestInfo.getParameter("draft_condRelEnv") )
					.setCtgId		( requestInfo.getParameter("draft_condCategory") )
					.setMstoneId	( requestInfo.getParameter("draft_condMilestone") )
					.setKeyword		( requestInfo.getParameter("draft_searchKeyword") )
				;

			} else {
				SearchCondition searchCondition = bean.getParam().getSearchCondition();
				bean.getParam().setSelectedDraft(false);
				searchCondition
					.setBldEnvId	( requestInfo.getParameter("condRelEnv") )
					.setStsId		( requestInfo.getParameter("condStatus") )
					.setCtgId		( requestInfo.getParameter("condCategory") )
					.setMstoneId	( requestInfo.getParameter("condMilestone") )
					.setKeyword		( requestInfo.getParameter("searchKeyword") )
				;
			}


		} else if ( RequestType.submitChanges.equals(param.getRequestType())) {
			String htmlTable = URLDecoder.decode(model.getRequestInfo().getParameter("table"), Charset.UTF_8.value());
			ExportToFileBean fileBean = DcmExportToFileUtils.createExportToFileBean(htmlTable);
			param.setExportBean(fileBean);
			ExportToFileSubmitOption option = ExportToFileSubmitOption.valueOf(requestInfo.getParameter("submitOption"));
			param.setSubmitOption(option);
		}
	}
}
