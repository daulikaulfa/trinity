package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition;;

/**
 * 
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/pending")
public class PrintReleaseRequestApprovalPendingListController extends TriControllerSupport<FlowReleaseRequestApprovalPendingListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmPrintReleaseRequestApprovalPendingListService;
	}

	@Override
	protected FlowReleaseRequestApprovalPendingListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovalPendingListServiceBean bean = new FlowReleaseRequestApprovalPendingListServiceBean();
		return bean;
	}
	
	@RequestMapping(value = "/list/print")
	public String print(FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();
		
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view)) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		model.getModel()
			.addAttribute("view", TriView.PrintReleaseRequestList.value())
			.addAttribute("printTabName", "Pending")
			.addAttribute("result", bean)
		;
		
		return view;
	}

	private void mapping(FlowReleaseRequestApprovalPendingListServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();

		ISessionInfo sesInfo = model.getSessionInfo();

		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		int selectedPageNo = 1;
		if (null != requestInfo.getParameter("pending_pageNo")) {
			selectedPageNo = Integer.parseInt(requestInfo.getParameter("pending_pageNo"));
		}
		searchCondition.setBldEnvId		( requestInfo.getParameter("pending_condRelEnv") )
						.setCtgId		( requestInfo.getParameter("pending_condCategory") )
						.setMstoneId	( requestInfo.getParameter("pending_condMilestone") )
						.setKeyword		( requestInfo.getParameter("pending_searchKeyword") )
						.setStsId		( requestInfo.getParameter("pending_condStatus") )
						.setSelectedPageNo		
										( selectedPageNo )
						;
	}
	
}