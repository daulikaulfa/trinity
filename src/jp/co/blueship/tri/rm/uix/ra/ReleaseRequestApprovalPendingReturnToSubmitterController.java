package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/release/request/pending/returnToSubmitter")
public class ReleaseRequestApprovalPendingReturnToSubmitterController
	extends TriControllerSupport<FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestApprovalPendingReturnToSubmitterService;
	}

	@Override
	protected FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean bean =
				new FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean();
		return bean;
	}

	@RequestMapping
	public String returnToSubmitter (FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean bean,
									TriModel model) {
		String view = "redirect:/release/request/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	private void mapping (FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean bean,
							TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedRaId( requestInfo.getParameter("raId") );
	}
}
