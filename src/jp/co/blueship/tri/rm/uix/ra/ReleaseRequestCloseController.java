package jp.co.blueship.tri.rm.uix.ra;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestCloseServiceBean;
/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 */
@Controller
@RequestMapping("/release/request/close")
public class ReleaseRequestCloseController extends TriControllerSupport<FlowReleaseRequestCloseServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleaseRequestCloseService;
	}

	@Override
	protected FlowReleaseRequestCloseServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleaseRequestCloseServiceBean bean = new FlowReleaseRequestCloseServiceBean();
		return bean;
	}

	@RequestMapping
	public String index(FlowReleaseRequestCloseServiceBean bean, TriModel model) {
		String view = "redirect:/release/request/redirect";

		try {
			bean.getParam().setRequestType(RequestType.submitChanges);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}
	
	@RequestMapping(value = "/validate")
	public String validate(FlowReleaseRequestCloseServiceBean bean, TriModel model) {
		String view = "common/Comment::releaseRequestApprovedClose";

		try {
			bean.getParam().setRequestType(RequestType.validate);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}
	

	private void mapping(FlowReleaseRequestCloseServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowReleaseRequestCloseServiceBean.RequestParam param = bean.getParam();

		if ( requestInfo.getParameter("raId") != null )
			param.setSelectedRaId(requestInfo.getParameter("raId"));
		
		if ( requestInfo.getParameter("comment") != null )
			param.getInputInfo().setComment(requestInfo.getParameter("comment"));
	}

}

