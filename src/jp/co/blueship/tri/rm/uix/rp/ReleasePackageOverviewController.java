package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageOverviewServiceBean;


/**
 * @version V4.00.00
 * @author Akahoshi
 */
@Controller
@RequestMapping("/release")
public class ReleasePackageOverviewController extends TriControllerSupport<FlowReleasePackageOverviewServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleasePackageOverviewService;
	}

	@Override
	protected FlowReleasePackageOverviewServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageOverviewServiceBean bean = new FlowReleasePackageOverviewServiceBean();
		return bean;
	}

	@RequestMapping("/overview")
	public String overview(FlowReleasePackageOverviewServiceBean bean , TriModel model){

		String view = "common/OverviewPopup::releasePackageOverview";
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);

		return view;
	}

	private void mapping(FlowReleasePackageOverviewServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedRpId( requestInfo.getParameter("rpId") );
	}
}
