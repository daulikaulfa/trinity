package jp.co.blueship.tri.rm.uix.rp;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationProcessingStatusServiceBean;;
/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/release")
public class ReleasePackageCreationProcessingStatusController extends TriControllerSupport<FlowReleasePackageCreationProcessingStatusServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleasePackageCreationProcessingStatusService;
	}

	@Override
	protected FlowReleasePackageCreationProcessingStatusServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageCreationProcessingStatusServiceBean bean = new FlowReleasePackageCreationProcessingStatusServiceBean();
		return bean;
	}

	@RequestMapping(value = "/progress")
	public String listView(FlowReleasePackageCreationProcessingStatusServiceBean bean, TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		if ( model.getModel().asMap().get("flashMessage") != null ) {
			List<String> messageInfo = (List<String>) model.getModel().asMap().get("flashMessage");
			bean.getMessageInfo().addFlashMessages(messageInfo);
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageCreationProcessingStatus.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReleasePackageCreationProcessingStatusServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ) {
		}
	}

}