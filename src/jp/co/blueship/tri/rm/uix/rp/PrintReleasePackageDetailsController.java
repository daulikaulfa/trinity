package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean;;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */

@Controller
@RequestMapping("/release/details/print")
public class PrintReleasePackageDetailsController extends TriControllerSupport<FlowReleasePackageDetailsServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmPrintFlowReleasePackageDetailsService;
	}

	@Override
	protected FlowReleasePackageDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageDetailsServiceBean bean = new FlowReleasePackageDetailsServiceBean();
		return bean;
	}

	@RequestMapping
	public String releasePackageDetails(FlowReleasePackageDetailsServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintReleasePackageDetails.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu", "releasepackageSubmenu")
			.addAttribute("result", bean)
		;

		return view;
	}

	private void mapping(FlowReleasePackageDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		FlowReleasePackageDetailsServiceBean.RequestParam param = bean.getParam();
		param.setSelectedRpId(requestInfo.getParameter("RpId"));

	}
}
