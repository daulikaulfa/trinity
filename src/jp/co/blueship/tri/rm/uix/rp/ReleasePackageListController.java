package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchCondition;;
/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/release")
public class ReleasePackageListController extends TriControllerSupport<FlowReleasePackageListServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleasePackageListService;
	}

	@Override
	protected FlowReleasePackageListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageListServiceBean bean = new FlowReleasePackageListServiceBean();
		return bean;
	}

	@RequestMapping(value = "/error")
	public String error(FlowReleasePackageListServiceBean bean,
			TriModel model) {
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/redirect")
	public String redirect( FlowReleasePackageListServiceBean bean, TriModel model ){
		return index( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = "/refresh")
	public String refresh(FlowReleasePackageListServiceBean bean, TriModel model){

		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.onChange);
			this.execute(getServiceId(), bean , model.setRedirect(true) );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageList.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  ,"releasepackageSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result"   , bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/","","/list"})
	public String index(FlowReleasePackageListServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			ISessionInfo sesInfo = model.getSessionInfo();
			if( TriStringUtils.isNotEmpty(bean.getParam().getSelectedLotId()) && 
					this.getSessionSelectedLot(sesInfo, bean) != bean.getParam().getSelectedLotId()){
				bean.getParam().setSearchCondition(bean.new SearchCondition() );
			}
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageList.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = {"/search"})
	public String search(FlowReleasePackageListServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {

			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageList.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "changeSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	private void mapping(FlowReleasePackageListServiceBean bean, TriModel model){

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		if(RequestType.init.equals(bean.getParam().getRequestType())){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}else if(RequestType.onChange.equals(bean.getParam().getRequestType())){

			int selectedPage = (TriStringUtils.isEmpty(requestInfo.getParameter("selectedPageNo")))? 1 : Integer.parseInt(requestInfo.getParameter("selectedPageNo"));
			searchCondition
				.setSelectedPageNo(selectedPage)
				.setEnvId(requestInfo.getParameter("condRelEnv"))
				.setCtgId(requestInfo.getParameter("condCategory"))
				.setMstoneId(requestInfo.getParameter("condMilestone"))
				.setStsId(requestInfo.getParameter("condStatus"))
				.setKeyword(requestInfo.getParameter("searchKeyword"))
			;
		}
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowReleasePackageListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";

		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowReleasePackageListServiceBean bean, TriModel model ){
		String view = TriTemplateView.MainTemplate.value();

		try {
			model.setRedirect(true);
			String json = null;
			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
				json = model.valueOf(TriModelAttributes.RedirectFilter);
				ISessionInfo sesInfo = model.getSessionInfo();
				bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
				
			}

			Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
			FlowReleasePackageListServiceBean.SearchCondition condition = gson.fromJson(json, FlowReleasePackageListServiceBean.SearchCondition.class);
			bean.getParam().setSearchCondition( condition );
			bean.getParam().setLinesPerPage(5);
			bean.getParam().getSearchCondition().setSelectedPageNo(1);

			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model );

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageList.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute("pagination",bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

}
