package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageRemovalServiceBean;
/**
 *
 * @version V4.00.00
 * @author Yusna Marlina
 *
 */
@Controller
@RequestMapping("/release/remove")
public class ReleasePackageRemovalController extends TriControllerSupport<FlowReleasePackageRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleasePackageRemovalService;
	}

	@Override
	protected FlowReleasePackageRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageRemovalServiceBean bean = new FlowReleasePackageRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String removal(FlowReleasePackageRemovalServiceBean bean,
			TriModel model) {
		String view = "redirect:/release/redirect";
		String ref = model.getRequestInfo().getParameter("referer");
		String fwd = model.getRequestInfo().getParameter("forward");
		
		try {
			bean.getParam().setRequestType( RequestType.submitChanges );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			
			bean.getParam().setRequestType( RequestType.init );
			if (bean.getResult().isCompleted()) {
				if (ref.equals(fwd) && fwd.equals(TriView.ReleasePackageList.value())) {
					view = "redirect:/release/refresh";
					
				} else {
					if (fwd.equals(TriView.ReleasePackageList.value())) {
						view = "redirect:/release/redirect";
					} else if (fwd.equals(TriView.ReleasePackageDetails.value())) {
						view = "redirect:/release/details";
					}
				}
				
			} else {
				if (ref.equals(TriView.ReleasePackageList.value())) {
					view = "redirect:/release/refresh";
				} else if (fwd.equals(TriView.ReleasePackageDetails.value())) {
					view = "redirect:/release/details";
				}
			}
			
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		return view;
	}

	@RequestMapping(value = "/validate")
	public String removalComment(FlowReleasePackageRemovalServiceBean bean,
			TriModel model) {

		String view = "common/Comment::releasePackageRemovalComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result", bean );
		return view;
	}

	private void mapping(FlowReleasePackageRemovalServiceBean bean, TriModel model) {
		
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowReleasePackageRemovalServiceBean.RequestParam param = bean.getParam();
		
		if ( requestInfo.getParameter("rpId") != null )
		param.setSelectedRpId(requestInfo.getParameter("rpId"));
	}

}

