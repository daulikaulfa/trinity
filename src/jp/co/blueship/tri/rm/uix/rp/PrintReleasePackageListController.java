package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchCondition;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/release/list/print")
public class PrintReleasePackageListController extends TriControllerSupport<FlowReleasePackageListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmPrintReleasePackageListService;
	}

	@Override
	protected FlowReleasePackageListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageListServiceBean bean = new FlowReleasePackageListServiceBean();
		return bean;
	}


	@RequestMapping
	public String print(FlowReleasePackageListServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.PrintReleasePackageList.value());
		model.getModel().addAttribute("result", bean);

		return view;
	}


	private void mapping(FlowReleasePackageListServiceBean bean, TriModel model) {

		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setLinesPerPage( 0 );
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		searchCondition.setEnvId	( requestInfo.getParameter("condRelEnv"))
					   .setCtgId	( requestInfo.getParameter("condCategory"))
					   .setMstoneId	( requestInfo.getParameter("condMilestone"))
					   .setStsId	( requestInfo.getParameter("condStatus"))
					   .setKeyword	( requestInfo.getParameter("searchKeyword"))
					   ;

	}
}
