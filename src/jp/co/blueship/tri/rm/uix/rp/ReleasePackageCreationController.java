package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean.ReleasePackageCreationInputInfo;;
/**
 *
 * @version V4.00.00
 * @author Sam
 *
 * @version V4.03.00
 * @author Anh Nguyen
 *
 */
@Controller
@RequestMapping("/release")
public class ReleasePackageCreationController extends TriControllerSupport<FlowReleasePackageCreationServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmReleasePackageCreationService;
	}

	@Override
	protected FlowReleasePackageCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageCreationServiceBean bean = new FlowReleasePackageCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create/refresh")
	public String reflesh(FlowReleasePackageCreationServiceBean bean,
						  TriModel model){
		String view = TriTemplateView.MainTemplate.value();

		try {
			bean.getParam().setRequestType(RequestType.init);
			this.execute(getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageCreation.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/create")
	public String listView(FlowReleasePackageCreationServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

			if(bean.getResult().isCompleted() && RequestType.submitChanges.equals(bean.getParam().getRequestType())){
				bean.getParam().setRequestType(RequestType.init);
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				model.getRedirectAttributes().addFlashAttribute("flashMessage",bean.getMessageInfo().getFlashMessages());
				return "redirect:/release/progress";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}
		
		model.getModel()
			.addAttribute("view", TriView.ReleasePackageCreation.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReleasePackageCreationServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		if(RequestType.init.equals( bean.getParam().getRequestType())){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}
		
		if( RequestType.onChange.equals( bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			bean.getParam().getInputInfo().setRaId(requestInfo.getParameter("raId"));
		}

		if (RequestType.submitChanges.equals( bean.getParam().getRequestType()) ){
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			ReleasePackageCreationInputInfo inputInfo = bean.getParam().getInputInfo()
				.setSubject		( requestInfo.getParameter("subjectId"))
				.setSummary		( requestInfo.getParameter("summaryId"))
				.setRaId		( requestInfo.getParameter("reqId"))
				.setEnvId		( requestInfo.getParameter("envId"))
				.setBpId		( requestInfo.getParameter("bpId"))
				.setCtgId		( requestInfo.getParameter("ctgId"))
				.setMstoneId	( requestInfo.getParameter("mstoneId"))
				;
			bean.getParam().setInputInfo(inputInfo);
		}
	}
}