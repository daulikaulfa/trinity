package jp.co.blueship.tri.rm.uix.rp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean;;
/**
 *
 * @version V4.00.00
 * @author Megumu Terasawa
 */


@Controller
@RequestMapping("/release")
public class ReleasePackageDetailsController extends TriControllerSupport<FlowReleasePackageDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.RmFlowReleasePackageDetailsService;
	}

	@Override
	protected FlowReleasePackageDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReleasePackageDetailsServiceBean bean = new FlowReleasePackageDetailsServiceBean();
		return bean;
	}
	@RequestMapping(value = "/details")
	public String releasePackageDetails(
			FlowReleasePackageDetailsServiceBean bean,
			TriModel model) {

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReleasePackageDetails.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu"  , "releasepackageSubmenu")
			.addAttribute( "result" , bean ); setPrev(model)
		;

		return view;
	}


	private void mapping(FlowReleasePackageDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();

		FlowReleasePackageDetailsServiceBean.RequestParam param = bean.getParam();
		if ( requestInfo.getParameter("RpId") != null ) {
			param.setSelectedRpId( requestInfo.getParameter("RpId") );
		}


	}
}
