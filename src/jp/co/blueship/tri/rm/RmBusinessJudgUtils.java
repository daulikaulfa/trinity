package jp.co.blueship.tri.rm;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;


/**
 * 指定した値の判定を行うユーティリティークラスです。
 * BusinessJudgUtilsの派生クラスです。依存性を解消するために作成しました。
 *
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RmBusinessJudgUtils {

	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	private static final boolean isReturnApply( String areqCtgCd ) {
		AreqCtgCd id = getAreqCtgCd( areqCtgCd );
		if ( null == id )
			return false;

		if ( AreqCtgCd.ReturningRequest.equals(id) )
			return true;

		return false;
	}
	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	//いろいろ
	public static final boolean isReturnApply( IAreqEntity entity ) {
		return isReturnApply(  entity.getAreqCtgCd() );
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	private static final boolean isDeleteApply( String areqCtgCd ) {
		AreqCtgCd id = getAreqCtgCd( areqCtgCd );
		if ( null == id )
			return false;

		if ( AreqCtgCd.RemovalRequest.equals(id) )
			return true;

		return false;
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDeleteApply( IAreqEntity entity ) {
		return isDeleteApply(  entity.getAreqCtgCd() );
	}

	/**
	 * 申請区分に該当する列挙型を取得します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、ApplyIdを戻します。それ以外はnullを戻します。
	 */
	private static final AreqCtgCd getAreqCtgCd( String areqCtgCd ) {
		if ( null == areqCtgCd )
			return null;

		AreqCtgCd id = AreqCtgCd.value( areqCtgCd );
		if ( null == id )
			return null;

		return id;
	}

	/**
	 * State will judge whether an error.
	 *
	 * @param entity IRpEntity
	 * @return Whether or not this result is a success.
	 */
	public static final boolean isSuccess( IRpEntity entity ) {
		if ( null == entity )
			return false;

		if ( RmRpStatusIdForExecData.ReleasePackageError.equals(entity.getProcStsId()) ) {
			return false;
		}

		return true;
	}

}
