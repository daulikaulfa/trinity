package jp.co.blueship.tri.rm;

import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;

/**
 * {@link jp.co.blueship.tri.fw.cmn.utils.collections.FluentList}のサポートUtils
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class RmFluentFunctionUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final TriFunction<IRaBpLnkEntity, String> toBpIdFromRaBpLnk = new TriFunction<IRaBpLnkEntity, String>() {
		public String apply(IRaBpLnkEntity input) {
			return input.getBpId();
		}
	};

	public static final TriFunction<IRaBpLnkEntity, String> toRaIdFromRaBpLnk = new TriFunction<IRaBpLnkEntity, String>() {
		public String apply(IRaBpLnkEntity input) {
			return input.getRaId();
		}
	};

	public static final TriFunction<IRaRpLnkEntity, String> toRaIdFromRaRpLnk = new TriFunction<IRaRpLnkEntity, String>() {
		public String apply(IRaRpLnkEntity input) {
			return input.getRaId();
		}
	};

	public static final TriFunction<IStatusId, ItemLabelsBean> toItemLabelsFromStatusId = new TriFunction<IStatusId, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IStatusId input) {
			return new ItemLabelsBean( sheet.getValue(RmDesignBeanId.statusId, input.getStatusId()), input.getStatusId());
		}
	};
	
	public static final TriFunction<IStatusId, String> toStatusIdFromStatus = new TriFunction<IStatusId, String>() {
		@Override
		public String apply(IStatusId input) {
			return input.getStatusId();
		}
	};

	public static final TriFunction<UnitViewBean, ItemLabelsBean> toItemLabelsFromUnitViewBean = new TriFunction<UnitViewBean, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(UnitViewBean input) {
			return new ItemLabelsBean(input.getBuildNo(), input.getBuildNo());
		}
	};
	
	public static final TriFunction<UnitBean, ItemLabelsBean> toItemLabelsFromUnitBean = new TriFunction<UnitBean, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(UnitBean input) {
			return new ItemLabelsBean(input.getBuildNo(), input.getBuildNo());
		}
	};
	
	public static final TriFunction<jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean.UnitViewBean, ItemLabelsBean> toItemLabelsFromUnitViewBeanRel = new TriFunction<jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean.UnitViewBean, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean.UnitViewBean input) {
			return new ItemLabelsBean(input.getBuildNo(), input.getBuildNo());
		}
	};
	
	public static final TriFunction<IRpBpLnkEntity, String> toRpIdsFromBpId = new TriFunction<IRpBpLnkEntity, String>() {
		@Override
		public String apply(IRpBpLnkEntity rpBpLnkEntity) {
			return rpBpLnkEntity.getRpId();
		}
	};

	public static final TriFunction<ConfigurationViewBean, ItemLabelsBean> toItemLabelsFromConfigurationViewBean = new TriFunction<ConfigurationViewBean, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(ConfigurationViewBean input) {
			return new ItemLabelsBean(input.getConfName(), input.getConfNo());
		}
	};
}
