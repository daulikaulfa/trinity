package jp.co.blueship.tri.rm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.constants.DmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

public class RmViewInfoAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * @param viewBean ロット一覧画面の表示項目
	 * @param entity ロットエンティティ
	 */
	public static void setLotViewBeanPjtLotEntity(
			LotViewBean viewBean, ILotEntity entity, String srvId ) {

		viewBean.setServerNo	( srvId );

		viewBean.setLotNo		( entity.getLotId() );
		viewBean.setLotName		( entity.getLotNm() );
		viewBean.setInputUser	( entity.getRegUserNm() );
		viewBean.setInputUserId	( entity.getRegUserId() );
		viewBean.setInputDate	( TriDateUtils.convertViewDateFormat( entity.getRegTimestamp() ) );
	}

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * @param viewBean ロット一覧画面の表示項目
	 * @param entity ロットエンティティ
	 */
	public static void setLotViewBeanPjtLotEntity(
			List<LotViewBean> viewBeanList, List<ILotDto> lotDto, List<IGrpUserLnkEntity> groupUserEntitys, String srvId ) {

		List<ILotEntity> lotList = new ArrayList<ILotEntity>();

		ViewInfoAddonUtil.setAccessablePjtLotEntity( lotDto, groupUserEntitys, lotList, null );

		for ( ILotEntity entity : lotList ) {
			LotViewBean viewBean = new LotViewBean();

			setLotViewBeanPjtLotEntity( viewBean, entity, srvId );

			viewBeanList.add( viewBean );
		}
	}

	/**
	 * 環境一覧画面の表示項目を、環境エンティティから取得して設定する。
	 * @param viewBean 環境一覧画面の表示項目
	 * @param entity 環境エンティティ
	 */
	public static void setConfViewBeanRelEnvEntity(
			ConfigurationViewBean viewBean, IBldEnvEntity entity ) {

		viewBean.setConfNo		( entity.getBldEnvId() );
		viewBean.setConfName	( entity.getBldEnvNm() );
		viewBean.setConfSummary ( entity.getRemarks() );
	}

	/**
	 * 画面の表示項目を、リリースエンティティから取得して設定する。
	 * @param relViewBeanList リリース情報閲覧のリスト
	 * @param rpDtoList リリースエンティティ
	 * @return リリース一覧画面表示ビーンのリスト
	 */
	public static void setRelCtlListViewBeanRelEntity( FlowRelCtlEditSupport support ,
			List<CtlListViewBean> relViewBeanList, List<IRpDto> rpDtoList, List<ILotEntity> lotEntities, String srvId ) {

		Map<String, String> lotNoServerNoMap = new HashMap<String, String>();

		for ( ILotEntity lotEntity : lotEntities ) {
			lotNoServerNoMap.put( lotEntity.getLotId(), srvId );
		}

		for( IRpDto rpDto : rpDtoList ) {
			CtlListViewBean relViewBean = new CtlListViewBean() ;

			setRelCtlViewBeanRelEntity( support , relViewBean, rpDto, lotNoServerNoMap.get( rpDto.getRpEntity().getLotId() ));
			relViewBeanList.add( relViewBean ) ;
		}
	}

	/**
	 * 画面の表示項目を、リリースエンティティから取得して設定する。
	 * @param ctlViewBean リリース情報閲覧の表示項目
	 * @param dto リリースエンティティ
	 * @return リリース一覧画面表示ビーンのリスト
	 */
	private static void setRelCtlViewBeanRelEntity( FlowRelCtlEditSupport support , CtlListViewBean ctlViewBean, IRpDto dto, String serverNo ) {

		ctlViewBean.setRelNo		( dto.getRpEntity().getRpId() ) ;
		ctlViewBean.setEnvNo		( dto.getRpEntity().getBldEnvId() ) ;
		ctlViewBean.setLotNo		( dto.getRpEntity().getLotId() ) ;
		ctlViewBean.setServerNo		( serverNo );
		ctlViewBean.setInputDate	( TriDateUtils.convertViewDateFormat( dto.getRpEntity().getRelTimestamp() ) );
		ctlViewBean.setSummary		( dto.getRpEntity().getSummary() ) ;
		ctlViewBean.setContent		( dto.getRpEntity().getContent() ) ;

		List<UnitBean> unitBeanList = convertUnitBean( support , dto.getRpEntity().getRpId() , dto.getRpBpLnkEntities() );
		Collections.sort( unitBeanList,
				new Comparator<UnitBean>() {
					public int compare( UnitBean obj1, UnitBean obj2 ) {
						return obj1.getMergeOrder().compareTo( obj2.getMergeOrder() );
					}
				} );
		ctlViewBean.setUnitBeanList	( unitBeanList );

		ctlViewBean.setRelStatus	( sheet.getValue( RmDesignBeanId.statusId, dto.getRpEntity().getProcStsId() ) ) ;

		IDmDoEntity doEntity = support.getDmDoEntity(dto.getRpEntity());

		if ( null != doEntity ) {
			String distributeProcStsId = support.getDmDoEntity(dto.getRpEntity()).getProcStsId();
			ctlViewBean.setDistributeStatus
										( sheet.getValue( DmDesignBeanId.distributeStatusId, distributeProcStsId ) ) ;
		}
	}

	/**
	 * RCエンティティのRP情報をビーンに変換する
	 *
	 * @param buildArray
	 * @return
	 */
	private static List<jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean>
			convertUnitBean( FlowRelCtlEditSupport support , String rpId , List<IRpBpLnkEntity> buildArray ) {

		List<jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean> unitList =
			new ArrayList<jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean>();

		for ( IRpBpLnkEntity build : buildArray ) {

			jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean unit =
				new jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean();

			unit.setBuildNo		( build.getBpId() );
			unit.setMergeOrder	( support.getMergeOdr(rpId, build.getBpId()) );

			unitList.add( unit );
		}


		return unitList;
	}

}
