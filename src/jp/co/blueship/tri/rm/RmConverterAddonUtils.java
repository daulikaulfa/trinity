package jp.co.blueship.tri.rm;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * エンティティ、ビーンの変換共通処理Class
 * <br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */

public class RmConverterAddonUtils {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * RAエンティティのRP情報をビーンに変換する
	 *
	 * @param buildEntity
	 * @return
	 */
	public static List<jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean>
			convertUnitBean( List<IRaBpLnkEntity> buildEntity ) {

		List<jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean> newList =
			new ArrayList<jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean>();

		if( buildEntity == null ) {
			return newList;
		}

		TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean> newSet =
			new TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean>();

		for ( IRaBpLnkEntity entity : buildEntity ) {

			jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean bean =
				new jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean();

			bean.setBuildNo		( entity.getBpId() );
			bean.setBuildName	( entity.getBpNm() );
			bean.setBuildSummary( entity.getSummary() );
			bean.setBuildContent( entity.getContent() );

			newSet.put(entity.getBpId(), bean);
		}

		newList.addAll( newSet.values() );

		return newList;
	}

	/**
	 * RAに含まれるRP情報をRPエンティティに変換する。
	 *
	 * @param finder
	 * @param unitList
	 * @return
	 */
	public static List<IBpEntity> convertBuildEntity(
			FinderSupport finder,
			List<jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean> unitList ) {

		List<IBpEntity> buildEntityList = new ArrayList<IBpEntity>();
		{

			Set<String> buildNoSet = new LinkedHashSet<String>();
			for (jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean unitBean : unitList) {
				buildNoSet.add(unitBean.getBuildNo());
			}

			BpCondition condition = new BpCondition();
			condition.setBpIds( buildNoSet.toArray(new String[0]) );

			List<IBpEntity> bpEntities = finder.getBmFinderSupport().getBpDao().find(condition.getCondition());
			buildEntityList.addAll( bpEntities );
		}

		return buildEntityList;
	}

	/**
	 * ビルドパッケージに含まれる申請情報の取得
	 *
	 * @param finder
	 * @param buildEntityList
	 * @return
	 */
	public static List<jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean>
			convertAssetApplyBeanList( FlowRelApplyEditSupport finder, List<IBpDto> buildEntityList ) {

		List<AssetApplyBean> assetApplyBeanList = new ArrayList<AssetApplyBean>();
		for ( IBpDto buildEntity : buildEntityList) {
 			List<IAreqEntity> assetApplyEntities = finder.getAssetApplyListFromBpDto( buildEntity ) ;
			for (IAreqEntity assetApplyEntity : assetApplyEntities) {

				IPjtEntity[] pjtEntities = finder.getPjtEntity(new String[]{assetApplyEntity.getPjtId()}) ;
				for (IPjtEntity pjtEntity : pjtEntities) {
					AssetApplyBean assetApplyBean = new AssetApplyBean();
					assetApplyBean.setBuildNoKey(buildEntity.getBpEntity().getBpId());
					assetApplyBean.setPjtNo(assetApplyEntity.getPjtId());
					assetApplyBean.setChangeCauseNo(pjtEntity.getChgFactorNo());
					assetApplyBean.setApplyNo(assetApplyEntity.getAreqId());
					assetApplyBeanList.add(assetApplyBean);
				}
			}
		}

		return assetApplyBeanList;
	}

	/**
	 * RAエンティティのRC情報をビーンに変換する
	 *
	 * @param finder
	 * @param relEntity
	 * @return
	 */
	public static List<jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean>
			convertRelBean( FinderSupport finder, List<IRaRpLnkEntity> raRpEntityList ) {

		List<jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean> newList =
			new ArrayList<jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean>();

		if( 0==raRpEntityList.size() ) {
			return newList;
		}

		IRaRpLnkEntity raRpEntity = raRpEntityList.get(0);
		RpCondition condition = (RpCondition)DBSearchConditionAddonUtil.getRpConditionByRelNo( new String[]{ raRpEntity.getRpId() } );
		condition.setDelStsId( null );

		List<IRpEntity> rpEntities = finder.getRmFinderSupport().getRpDao().find(condition.getCondition());

		TreeMap<String, RelBean> newSet = new TreeMap<String, RelBean>();

		for ( IRpEntity entity : rpEntities ) {
			if ( newSet.containsKey(entity.getRpId()) ) {
				continue;
			}
			jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean relbean =
					new jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean();

			relbean.setRelNo( raRpEntity.getRpId() );
			relbean.setRelInputDate( TriDateUtils.convertViewDateFormat( entity.getRelTimestamp() ) );

			newSet.put(raRpEntity.getRpId(), relbean);

			newSet.get(entity.getRpId()).setRelStatus(
					(sheet.getValue(RmDesignBeanId.statusId, entity.getProcStsId())));
		}

		newList.addAll(newSet.values());

		return newList;
	}

	/**
	 * RAエンティティの資産申請情報を変更要因Noに変換する
	 *
	 * @param areqEntities
	 * @return
	 */
	public static List<String> convertChangeCauseNoList( List<IAreqEntity> areqEntities ) {

		List<String> newList = new ArrayList<String>();
		if( areqEntities == null ) {
			return newList;
		}

		TreeSet<String> newSet = new TreeSet<String>();
		for ( IAreqEntity entity : areqEntities ) {

			newSet.add( entity.getChgFactorNo() );
		}
		newList.addAll(newSet);

		return newList;
	}

	/**
	 * RAエンティティの資産申請情報をビーンに変換する
	 *
	 * @param raBpLnkEntities
	 * @return
	 */
	public static List<jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean>
			convertAssetApplyBean(FlowRelApplyEditSupport support, List<IRaBpLnkEntity> raBpLnkEntities  ) {

		List<jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean> newList =
			new ArrayList<jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean>();

		if( raBpLnkEntities == null ) {
			return newList;
		}

		TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean> newSet = new TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean>();

		for ( IRaBpLnkEntity raBpLnkEntity : raBpLnkEntities ) {

			jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean bean =
				new jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean();

			for ( IAreqEntity areqEntity: support.findAreqEntitiesFromRaBpLnk(raBpLnkEntity) ) {

				bean.setBuildNoKey		( raBpLnkEntity.getBpId() );
				bean.setPjtNo			( areqEntity.getPjtId() );
				bean.setChangeCauseNo	( areqEntity.getChgFactorNo() );
				bean.setApplyNo			( areqEntity.getAreqId() );

				newSet.put( areqEntity.getChgFactorNo() , bean);
			}
		}
		newList.addAll(newSet.values());

		return newList;
	}

	/**
	 * 資産申請情報を資産申請ID(AreqId)配列に変換する
	 *
	 * @param areqEntities
	 * @return
	 */
	public static List<String> convertAssetApplyNoList( List<IAreqEntity> areqEntities  ) {

		List<String> newList = new ArrayList<String>();
		if( areqEntities == null ) {
			return newList;
		}

		TreeSet<String> newSet = new TreeSet<String>();

		for ( IAreqEntity entity : areqEntities ) {
			newSet.add(entity.getAreqId());
		}

		newList.addAll(newSet);

		return newList;
	}

	/**
	 * RPエンティティの資産申請情報を変更管理Noに変換する
	 *
	 * @param entityList
	 * @return
	 */
	public static List<String> convertPjtNoList(FinderSupport finder , List<IBpAreqLnkEntity> entityList ) {

		List<String> newList = new ArrayList<String>();

		if( entityList == null ) {
			return newList;
		}

		TreeSet<String> newSet = new TreeSet<String>();
		for ( IBpAreqLnkEntity entity : entityList ) {
			newSet.add( entity.getPjtId() );
		}

		newList.addAll(newSet);

		return newList;
	}

	/**
	 * RPエンティティの資産申請情報を変更要因Noに変換する
	 *
	 * @param finder
	 * @param buildApplyEntity
	 * @return
	 */
	public static List<String> convertChangeCauseNoList( FinderSupport finder, List<IBpAreqLnkEntity> buildApplyEntity ) {

		List<String> newList = new ArrayList<String>();

		if( buildApplyEntity == null ) {
			return newList;
		}

		PjtCondition condition = (PjtCondition)DBSearchConditionAddonUtil.getPjtCondition( convertPjtNoList( finder, buildApplyEntity ).toArray(new String[0]) );
		condition.setDelStsId( null );

		List<IPjtEntity> pjtEntities = finder.getPjtDao().find(condition.getCondition());

		TreeSet<String> newSet = new TreeSet<String>();
		for ( IPjtEntity entity : pjtEntities ) {

			newSet.add(entity.getChgFactorNo());
		}

		newList.addAll(newSet);

		return newList;
	}

	/**
	 * RPエンティティの資産申請情報を資産申請ID(AreqId)配列に変換する
	 *
	 * @param List<IBpAreqLnkEntity> entityList
	 * @return
	 */
	public static List<String> convertAssetApplyNoListByBpAreq( List<IBpAreqLnkEntity> entityList  ) {

		List<String> newList = new ArrayList<String>();
		if( entityList == null ) {
			return newList;
		}

		TreeSet<String> newSet = new TreeSet<String>();

		for ( IBpAreqLnkEntity entity : entityList ) {

			newSet.add(entity.getAreqId());
		}

		newList.addAll(newSet);

		return newList;
	}

}
