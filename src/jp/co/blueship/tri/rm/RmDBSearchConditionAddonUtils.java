package jp.co.blueship.tri.rm;

import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.fw.constants.status.BmBldEnvStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class RmDBSearchConditionAddonUtils {
	/**
	 * 進行中のリリース環境情報の検索条件を取得します。
	 * @return 進行中のリリース環境情報の検索条件
	 */
	public static final IJdbcCondition getActiveReleaseEnvCondition() {

		BldEnvCondition condition = new BldEnvCondition();

		String[] statusId	= { BmBldEnvStatusId.Running.getStatusId() };
		condition.setSvcIds( new String[]{
				ServiceId.RmReleasePackageCreationService.value(),
				ServiceId.RmReleasePackageCreationService.valueOfV3()} );
		condition.setStsIds	( statusId );

		return condition;
	}

}
