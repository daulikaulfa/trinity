package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean.RelApplyViewBean;

/**
 * リリースパッケージ・基本情報入力画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class BaseInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** リリース申請の詳細検索項目を表示する／表示しない */
	private String openDisplay = "0";
	
	/** リリース申請の詳細検索を行うための検索条件情報 */
	private BaseInfoRelApplySearchBean relApplySearchBean = null;
	
	/** ビルドパッケージ情報 */
	private BaseInfoInputBean baseInfoInputBean = null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;

	
	public BaseInfoInputBean getBaseInfoInputBean() {
		if ( null == baseInfoInputBean ) {
			baseInfoInputBean = new BaseInfoInputBean();
		}
		return baseInfoInputBean;
	}
	
	public void setBaseInfoInputBean( BaseInfoInputBean baseInfoInputBean ) {
		this.baseInfoInputBean = baseInfoInputBean;
	}
	
	public BaseInfoRelApplySearchBean getRelApplySearchBean() {
		if ( null == relApplySearchBean ) {
			relApplySearchBean = new BaseInfoRelApplySearchBean();
		}
		
		return relApplySearchBean;
	}
	
	public void setRelApplySearchBean(BaseInfoRelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}
	
	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		if ( null == relApplyViewBeanList ) {
			relApplyViewBeanList = new ArrayList<RelApplyViewBean>();
		}
		return relApplyViewBeanList;
	}
	
	public void setRelApplyViewBeanList(
			List<RelApplyViewBean> relApplyViewBeanList ) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}

	public String getOpenDisplay() {
		return openDisplay;
	}

	public void setOpenDisplay(String openDisplay) {
		this.openDisplay = openDisplay;
	}
}
