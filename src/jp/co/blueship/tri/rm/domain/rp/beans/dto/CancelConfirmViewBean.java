package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * リリース取消・取消確認用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class CancelConfirmViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String lotId = null ;
	/** ロット名 */
	private String lotName = null ;
	/** リリース番号 */
	private String relNo = null;
	/** リリース概要 */
	private String relSummary = null ;
	/** リリース内容 */
	private String relContent = null ;
	/** ビルドパッケージ情報 */
	private List<CancelUnitViewBean> cancelUnitViewBeanList = new ArrayList<CancelUnitViewBean>();
	/** サーバ番号 */
	private String serverNo = null ;
	
	
	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public List<CancelUnitViewBean> getCancelUnitViewBeanList() {
		return cancelUnitViewBeanList;
	}

	public void setCancelUnitViewBeanList(
			List<CancelUnitViewBean> cancelUnitViewBeanList) {
		this.cancelUnitViewBeanList = cancelUnitViewBeanList;
	}

	public CancelUnitViewBean newCancelUnitViewBean() {
		return new CancelUnitViewBean();
	}
	
	public class CancelUnitViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/** マージ順 */
		private String mergeOrder = null;
		/** ビルドパッケージ番号 */
		private String buildNo = null;
		/** ビルドパッケージ名 */
		private String buildName = null;
		/** ビルドパッケージ概要 */
		private String buildSummary = null;
		/** ビルド日時 */
		private String buildStartDate = null;
		/** 実行者 */
		private String executeUser = null;
		/** 実行者ＩＤ */
		private String executeUserId = null;
		/** ステータス */
		private String buildStatus = null;
		
		public String getMergeOrder() {
			return mergeOrder;
		}
		public void setMergeOrder( String mergeOrder ) {
			this.mergeOrder = mergeOrder;
		}
		public String getBuildName() {
			return buildName;
		}
		public void setBuildName(String buildName) {
			this.buildName = buildName;
		}
		public String getBuildNo() {
			return buildNo;
		}
		public void setBuildNo(String buildNo) {
			this.buildNo = buildNo;
		}
		public String getBuildStartDate() {
			return buildStartDate;
		}
		public void setBuildStartDate(String buildStartDate) {
			this.buildStartDate = buildStartDate;
		}
		public String getBuildStatus() {
			return buildStatus;
		}
		public void setBuildStatus(String buildStatus) {
			this.buildStatus = buildStatus;
		}
		public String getBuildSummary() {
			return buildSummary;
		}
		public void setBuildSummary(String buildSummary) {
			this.buildSummary = buildSummary;
		}
		public String getExecuteUser() {
			return executeUser;
		}
		public void setExecuteUser(String executeUser) {
			this.executeUser = executeUser;
		}
		public String getExecuteUserId() {
			return executeUserId;
		}
		public void setExecuteUserId(String executeUserId) {
			this.executeUserId = executeUserId;
		}	
	}

	public String getRelContent() {
		return relContent;
	}

	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}

	public String getRelSummary() {
		return relSummary;
	}

	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}

	public String getLotName() {
		return lotName;
	}

	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	
	public String getServerNo() {
		return serverNo;
	}

	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
}
