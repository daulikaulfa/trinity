package jp.co.blueship.tri.rm.domain.rp;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryDetailViewServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryDetailViewServiceBean.BaseInfoViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlHistoryDetailViewServiceBean.EnvViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・リリース履歴詳細画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlHistoryDetailViewService implements IDomain<FlowRelCtlHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlHistoryDetailViewServiceBean> execute( IServiceDto<FlowRelCtlHistoryDetailViewServiceBean> serviceDto ) {

		FlowRelCtlHistoryDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedRelNo = paramBean.getSelectedRelNo();
			ItemCheckAddonUtils.checkRelNo( selectedRelNo );

			IRpDto rpDto = this.support.findRpDto( selectedRelNo );
			ILotDto lotDto = null;
			// IRpEntity#lotNo 追加前のデータを考慮
			if ( TriStringUtils.isNotEmpty( rpDto.getRpEntity().getLotId() )) {
				lotDto = this.support.getAmFinderSupport().findLotDto( rpDto.getRpEntity().getLotId() );

				// グループの存在チェック
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
			}

			// 基本情報
			BaseInfoViewBean baseInfoViewBean = paramBean.newBaseInfoViewBean();
			setBaseInfoViewBean			( baseInfoViewBean, rpDto.getRpEntity() );
			paramBean.setBaseInfoViewBean	( baseInfoViewBean );

			// ロット情報
			LotViewBean lotViewBean = new LotViewBean();
			if ( null != lotDto ) {
				setLotViewBean			( lotViewBean, lotDto.getLotEntity() );
			}
			paramBean.setLotViewBean		( lotViewBean );

			// ビルドパッケージ情報
			paramBean.setUnitViewBeanList	( getUnitViewBean( rpDto ) );

			// リリース環境情報
			IBldEnvEntity relEnvEntity =
				this.support.getBmFinderSupport().findBldEnvEntity( rpDto.getRpEntity().getBldEnvId() );

			EnvViewBean envViewBean = paramBean.newEnvViewBean();
			setEnvViewBean			( envViewBean, relEnvEntity );
			paramBean.setEnvViewBean	( envViewBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005068S, e);
		}
	}

	/**
	 * ビルド番号からビルドエンティティを取得する
	 * @param buildNo ビルド番号
	 * @return ビルドエンティティ
	 */
	private IBpEntity[] getBuildEntity( String[] buildNo ) {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getBuildConditionByBuildNo( buildNo );
		ISqlSort sort				=
			(ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelHistoryDetailView();

		List<IBpEntity> entities = this.support.getBmFinderSupport().getBpDao().find( condition.getCondition(), sort );

		return entities.toArray(new IBpEntity[0]);
	}

	/**
	 * ロット情報を設定する。
	 * @param baseInfoViewBean リリース基本情報
	 * @param lotEntity ロットエンティティ
	 * @param
	 */
	private void setLotViewBean(
			LotViewBean lotViewBean, ILotEntity lotEntity ) {

		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		lotViewBean.setServerNo	( srvEntity.getBldSrvId() );

		lotViewBean.setLotNo	( lotEntity.getLotId() );
		lotViewBean.setLotName	( lotEntity.getLotNm() );
		lotViewBean.setInputDate( TriDateUtils.convertViewDateFormat( lotEntity.getRegTimestamp() ) );
		lotViewBean.setInputUser( lotEntity.getRegUserNm() );
		lotViewBean.setInputUserId( lotEntity.getRegUserId() );
	}

	/**
	 * リリース基本情報を設定する。
	 * @param baseInfoViewBean リリース基本情報
	 * @param relEntity リリースエンティティ
	 */
	private void setBaseInfoViewBean(
			BaseInfoViewBean baseInfoViewBean, IRpEntity relEntity ) {

		baseInfoViewBean.setRelNo			( relEntity.getRpId() );
		baseInfoViewBean.setRelStatus		(
				sheet.getValue(
						RmDesignBeanId.statusId, relEntity.getProcStsId() ));
		baseInfoViewBean.setRelSummary		( relEntity.getSummary() );
		baseInfoViewBean.setRelContent		( relEntity.getContent() );
		baseInfoViewBean.setExecuteUser		( relEntity.getExecUserNm() );
		baseInfoViewBean.setExecuteUserId	( relEntity.getExecUserId() );
		baseInfoViewBean.setRelStartDate	( TriDateUtils.convertViewDateFormat( relEntity.getProcStTimestamp() ) );
		baseInfoViewBean.setRelEndDate		( TriDateUtils.convertViewDateFormat( relEntity.getProcEndTimestamp() ) );
	}

	/**
	 * ビルドパッケージ情報を設定する。
	 * @param unitViewBeanList ビルドパッケージ情報のリスト
	 * @param relEntity リリースエンティティ
	 */
	private List<UnitViewBean> getUnitViewBean( IRpDto relEntity ) {

		IBpEntity[] buildEntities		= getBuildEntity( RmEntityAddonUtils.getBuildNo( relEntity ) );
		Map<String, String> mergeOrderMap	= RmEntityAddonUtils.buildNoMergeOrderMap( this.support , relEntity );

		return this.support.getUnitViewBean( buildEntities, mergeOrderMap );

	}

	/**
	 * リリース環境情報を設定する。
	 * @param envViewBean リリース環境情報
	 * @param relEnvEntity リリース環境エンティティ
	 */
	private void setEnvViewBean(
			EnvViewBean envViewBean, IBldEnvEntity relEnvEntity ) {

		envViewBean.setEnvNo		( relEnvEntity.getBldEnvId() );
		envViewBean.setenvName		( relEnvEntity.getBldEnvNm() );
	}
}
