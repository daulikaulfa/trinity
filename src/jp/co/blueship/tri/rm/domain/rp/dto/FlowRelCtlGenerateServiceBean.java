package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowRelCtlGenerateServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * リリース番号（表示用）
	 */
	private String relNo = null;

	/**
	 * リリース番号（入力用）
	 */
	private String inRelNo = null;

	/**
	 * ビルドパッケージ（入力用）
	 */
	private String inConfigId = null;

	/**
	 * リリース実行者
	 */
	public String copeUser = null;

	/**
	 * リリース実行者ＩＤ
	 */
	public String copeUserId = null;

	/**
	 * リリース状況メッセージ
	 */
	public String relStatusMsg = null;

	/**
	 * 最大カラム数
	 */
	private String maxColumns = null;

	/**
	 * リリース状況
	 */
	private List<RelProcessViewBean> relProcessViewBeanList = new ArrayList<RelProcessViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public RelProcessViewBean newRelProcessViewBean() {
		RelProcessViewBean bean = new RelProcessViewBean();
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 *
	 * @return 取得したリストを戻します。
	 */
	public List<RelProcessViewBean> getRelProcessViewBeanList() {
		return relProcessViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 *
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setRelProcessViewBeanList(List<RelProcessViewBean> list) {
		this.relProcessViewBeanList = list;
	}

	public class RelProcessViewBean {

		/**
		 * リソースパス
		 */
		private String img[] = null;

		/**
		 * メッセージ
		 */
		private String msg[] = null;

		private String active[] = null ;

		public String[] getImg() {
			return img;
		}

		public void setImg(String[] img) {
			this.img = img;
		}

		public String[] getMsg() {
			return msg;
		}

		public void setMsg(String[] msg) {
			this.msg = msg;
		}

		public String[] getActive() {
			return active;
		}

		public void setActive(String[] active) {
			this.active = active;
		}
	}

	public String getCopeUser() {
		return copeUser;
	}

	public void setCopeUser(String copeUser) {
		this.copeUser = copeUser;
	}

	public String getInRelNo() {
		return inRelNo;
	}

	public void setInRelNo(String inRelNo) {
		this.inRelNo = inRelNo;
	}

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getRelStatusMsg() {
		return relStatusMsg;
	}

	public void setRelStatusMsg(String relStatusMsg) {
		this.relStatusMsg = relStatusMsg;
	}

	public String getMaxColumns() {
		return maxColumns;
	}

	public void setMaxColumns(String maxColumns) {
		this.maxColumns = maxColumns;
	}

	public String getInConfigId() {
		return inConfigId;
	}

	public void setInConfigId(String inConfigId) {
		this.inConfigId = inConfigId;
	}

	public String getCopeUserId() {
		return copeUserId;
	}

	public void setCopeUserId(String copeUserId) {
		this.copeUserId = copeUserId;
	}

}
