package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.blueship.tri.agent.cmn.utils.TaskImageResourceCache;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.constants.ServerType;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean.RelProcessViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlDetailViewServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・リリース状況画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlDetailViewService implements IDomain<FlowRelCtlDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	//private static final String IMAGE_NEXT_ARROW_ON = "pci_wait_arrow2.gif";
	//private static final String IMAGE_NEXT_ARROW_OFF = "pci_wait_arrow2_off.gif";
	private static final String WAIT		= "wait" ;//"pci-wait_die.gif";

	private static final String STATUS_ACTIVE			= "ACTIVE" ;
	private static final String STATUS_COMPLETE		= "COMPLETE" ;
	private static final String STATUS_ERROR			= "ERROR" ;

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	private IContextAdapter ac;
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IServiceDto<FlowRelCtlDetailViewServiceBean> execute( IServiceDto<FlowRelCtlDetailViewServiceBean> serviceDto ) {

		FlowRelCtlDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			List<ILotDto> pjtLotEntityArray = null ;
			List<IGrpUserLnkEntity> groupUserEntityArray = null;
			//setSrvIdAndNm( paramBean );
			Map<String,Object> lotInfoMap = TaskImageResourceCache.getLotInfo( paramBean.getUserId() ) ;
			if ( null == lotInfoMap ) {
				pjtLotEntityArray = this.getActivePjtLotEntityArray() ;
				groupUserEntityArray = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				TaskImageResourceCache.setLotInfoResourceCache( paramBean.getUserId() , pjtLotEntityArray , groupUserEntityArray );
			} else {
				pjtLotEntityArray = (List<ILotDto>)lotInfoMap.get( ILotDto.class.getSimpleName() ) ;
				groupUserEntityArray = (List<IGrpUserLnkEntity>)lotInfoMap.get( IGrpUserLnkEntity.class.getSimpleName() ) ;
			}

			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

			// ロットコンボ
			paramBean.setLotList( this.makeLotList( pjtLotEntityArray , groupUserEntityArray, srvEntity.getBldSrvId() ) ) ;

			String selectedLotNo = paramBean.getSelectedLotNo();

			if ( TriStringUtils.isEmpty( selectedLotNo )) {
				setMessage( paramBean, TriLogMessage.LRM0007 );//ロットを選択してください。
				return serviceDto;
			} else {
				// グループの存在チェック
				ILotEntity pjtLotEntity	= this.getSelectedPjtLotEntity( pjtLotEntityArray , selectedLotNo ) ;
				ILotDto dto = this.support.getAmFinderSupport().findLotDto( pjtLotEntity );
				RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUserEntityArray );
			}

			// ラインが１件も存在しない場合
			if( true == checkRpTaskIsEmpty( selectedLotNo ) ) {
				setMessage( paramBean, TriLogMessage.LRM0003 );//リリースパッケージ処理は実行されていません。
				return serviceDto;
			}

			String envNo = paramBean.getSelectedEnvNo();
			String relNo = this.getRecentRelNo( paramBean );

			IBldEnvEntity envEntity = null;
			IBldEnvSrvEntity[] envServerEntity = new IBldEnvSrvEntity[0];
			IRpEntity relEntity = null;
			IBldTimelineEntity[] timeLineEntitys = new IBldTimelineEntity[0];
			ITaskFlowEntity[] taskFlowEntitys = new ITaskFlowEntity[0] ;

			Map<String,Object> resMap = TaskImageResourceCache.getBuildResource( relNo ) ;
			if ( null == resMap ) {
				relEntity = this.support.findRpEntity( relNo );
				envEntity = this.support.getBmFinderSupport().findBldEnvEntity( relEntity.getBldEnvId() );
				envServerEntity = this.support.getBldEnvSrvEntity( relEntity.getBldEnvId() );
				timeLineEntitys = this.support.getBldTimelineEntity( relEntity.getBldEnvId() );
				taskFlowEntitys = this.support.getTaskFlowEntities();

				TaskImageResourceCache.setReleaseResourceCache(
						envEntity, envServerEntity, relEntity, timeLineEntitys , taskFlowEntitys );
			} else {
				relEntity = (IRpEntity)resMap.get( IRpEntity.class.getSimpleName() ) ;
				envEntity = (IBldEnvEntity)resMap.get( IBldEnvEntity.class.getSimpleName() ) ;
				envServerEntity = (IBldEnvSrvEntity[])resMap.get( IBldEnvSrvEntity.class.getSimpleName() ) ;
				timeLineEntitys = (IBldTimelineEntity[])resMap.get( IBldTimelineEntity.class.getSimpleName() ) ;
				taskFlowEntitys = (ITaskFlowEntity[])resMap.get( ITaskFlowEntity.class.getSimpleName() ) ;
			}

			if ( null == envNo ) {
				paramBean.setSelectedEnvNo( relEntity.getBldEnvId() );
			}

			paramBean.setRelNo( relNo );

			if ( 0 == timeLineEntitys.length ) {
				throw new TriSystemException( RmMessageId.RM004004F , envNo);
			}

			//ラインの処理状況を取得
			TaskFlowProcCondition condition = new TaskFlowProcCondition();
			condition.setProcId( relEntity.getProcId() );
			ITaskFlowProcEntity[] process = this.support.getBmFinderSupport().getTaskFlowProcDao().find( condition.getCondition()).toArray(new ITaskFlowProcEntity[0]);
			if ( log.isDebugEnabled() ) {
//				for ( ITaskFlowProcEntity processEntity : process ) {
//					LogHandler.debug( log , "..........ITaskFlowProcEntity:getRelNo:=" + processEntity.getRelNo() + " statusId:=" + processEntity.getStatusId() );
//				}
			}

			// ラインが存在しない場合
			if(false == isProcessExist(process)) {
				CtlDetailViewBean releaseDetailViewBean = new CtlDetailViewBean();

				releaseDetailViewBean.setRelNo					( relNo );
				releaseDetailViewBean.setCopeUser				( relEntity.getExecUserNm() );
				releaseDetailViewBean.setCopeUserId				( relEntity.getExecUserId() );
				releaseDetailViewBean.setRelProcessViewBeanList	( new ArrayList<RelProcessViewBean>() );

				releaseDetailViewBean.setRelStatusMsg( contextAdapter().getMessage( TriLogMessage.LRM0003 ) );

				paramBean.setReleaseDetailViewBean( releaseDetailViewBean );
				return serviceDto;
			}

			//タイムラインの表示向きを縦→横に変換
//			Map<String, Map<String, ILineEntity>> lineMap = convertTimeLine( timeLineEntitys );
			//タイムラインの取得情報を正規化
			Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> lineMap = normalizeTimeLine( relNo , timeLineEntitys , envEntity.getBldEnvId() );

			//処理結果の表示向きを縦→横に変換
//			Map<String, Map<String, ITaskFlowProcEntity>> processMap = convertProcess( process );
			//処理結果の取得情報を正規化
			Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> processMap = normalizeProcess( process );

			//システムを変換
			Map<String, IBldSrvEntity> systemMap = convertSystem( TaskImageResourceCache.getBldSrvEntity() );//変更

//			タスクをMap化
			Map<String , ITaskEntity> taskMap = mapTask( taskFlowEntitys ) ;

			CtlDetailViewBean releaseDetailViewBean = this.makeCtlDetailViewBean(
					relEntity , timeLineEntitys , process ,
					lineMap , processMap , systemMap , taskMap ) ;

			paramBean.setReleaseDetailViewBean( releaseDetailViewBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005061S , e );
		}
	}

	private void setMessage( FlowRelCtlDetailViewServiceBean paramBean, TriLogMessage messageId) {

		CtlDetailViewBean releaseDetailViewBean = new CtlDetailViewBean();
		releaseDetailViewBean.setRelProcessViewBeanList	( new ArrayList<RelProcessViewBean>() );

		releaseDetailViewBean.setRelStatusMsg( contextAdapter().getMessage(messageId) );
		paramBean.setReleaseDetailViewBean( releaseDetailViewBean );
	}

	/**
	 * @param paramBean
	 * @deprecated
	 */
	@SuppressWarnings("unused")
	private void setSrvIdAndNm( FlowRelCtlDetailViewServiceBean paramBean ) {

		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
	}
	/**
	 *
	 * @param pjtLotEntityArray
	 * @param selectedLotNo
	 * @return
	 */
	private ILotEntity getSelectedPjtLotEntity( List<ILotDto> pjtLotEntityArray , String selectedLotNo ) {
		for( ILotDto pjtLotEntity : pjtLotEntityArray ) {
			if( pjtLotEntity.getLotEntity().getLotId().equals( selectedLotNo ) ) {
				return pjtLotEntity.getLotEntity() ;
			}
		}
		return null ;
	}

	/**
	 *
	 * @return
	 */
	private List<ILotDto> getActivePjtLotEntityArray() {
		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( null );
		ISqlSort sort			= DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelCtlGenerateDetailViewLotSelect();

		List<ILotEntity> entities	= this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
		List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( entities );
		return lotDto ;
	}

	/**
	 *
	 * @param pjtLotEntityArray
	 * @param groupUserEntityArray
	 * @return
	 */
	private List<ItemLabelsBean> makeLotList( List<ILotDto> pjtLotEntityArray , List<IGrpUserLnkEntity> groupUserEntityArray, String srvId ) {

		List<ILotEntity> lotList = new ArrayList<ILotEntity>();
		List<ItemLabelsBean> labelList = new ArrayList<ItemLabelsBean>() ;

		ViewInfoAddonUtil.setAccessablePjtLotEntity( pjtLotEntityArray  , groupUserEntityArray, lotList, null );

		String serverId = srvId;
		if ( null == serverId ) {
			serverId = ServerType.CORE.getValue();
		}
		for( ILotEntity pjtLotEntity : lotList ) {
			labelList.add( new ItemLabelsBean( pjtLotEntity.getLotNm(), pjtLotEntity.getLotId() , serverId )) ;
		}

		return labelList ;
	}

	/**
	 * ロットIDに対応したビルドパッケージがTaskFlowProc上に存在するかチェック
	 *
	 * @param lotId ロットId
	 * @return 無ければtrue
	 */
	private final boolean checkRpTaskIsEmpty( String lotId )
	{
		boolean ret = true;
		RpCondition rpCondition = new RpCondition();
		rpCondition.setLotId(lotId);
		List<IRpEntity> rpEntities = this.support.getRpDao().find(rpCondition.getCondition());
		if( 0<rpEntities.size() ) {
			List<String> procIds = new ArrayList<String>();
			for ( IRpEntity rpEntity : rpEntities ) {
				procIds.add(rpEntity.getProcId());
			}
			TaskFlowProcCondition condition = new TaskFlowProcCondition();
			condition.setProcIds(procIds.toArray(new String[0]));
			int taskCount = this.support.getBmFinderSupport().getTaskFlowProcDao().count( condition.getCondition() );
			if( 0<taskCount ) {
				ret = false;
			}
		}
		return ret;
	}
	/**
	 * ロットIDから最新のRpIdを取得する
	 *
	 * @param lotId ロットId
	 * @return 最新のRpId
	 */
	private final String getTaskRpId( String lotId )
	{
		String rpId = null;
		RpCondition rpCondition = new RpCondition();
		rpCondition.setLotId(lotId);
		ISqlSort sort = new SortBuilder();
		sort.setElement(RpItems.rpId, TriSortOrder.Desc, 1);
		List<IRpEntity> rpEntities = this.support.getRpDao().find(rpCondition.getCondition(), sort);
		if( rpEntities.size()>0 ) {
			for ( IRpEntity rpEntity : rpEntities ) {
				TaskFlowProcCondition condition = new TaskFlowProcCondition();
				condition.setProcId(rpEntity.getProcId());
				ITaskFlowProcEntity[] relProcessEntities = this.support.getBmFinderSupport().getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);
				if( relProcessEntities!=null && relProcessEntities.length>0 ){
					rpId = rpEntity.getRpId();
					break;
				}
			}
		}
		return rpId;
	}
	/**
	 * 最新のリリース情報を取得します。
	 * @param paramBean パラメタ情報
	 * @return 最新のリリース番号
	 */
	private final String getRecentRelNo( FlowRelCtlDetailViewServiceBean paramBean ) {

		String relNo = paramBean.getRelNo();

		if ( TriStringUtils.isEmpty( relNo )) {
			relNo = getTaskRpId( paramBean.getSelectedLotNo() );
		}

		return relNo;
	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param timelineEntitys ビルドパッケージ・タイムラインエンティティー
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> normalizeTimeLine(
						String relNo , IBldTimelineEntity[] timelineEntitys , String envNo) {

		IUcfServerEntityComparator comparator = new IUcfServerEntityComparator();

		Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> timeLineMap =
				new TreeMap<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>>( comparator);

		for ( IBldTimelineEntity timeline : timelineEntitys ) {
			for ( IBldTimelineAgentEntity line : timeline.getLine() ) {

				IBldSrvEntity server = TaskImageResourceCache.findUcfServerEntityByCache( line.getBldSrvId() );

				Map<IBldTimelineEntity, IBldTimelineAgentEntity> lineMap = null;
				if ( (! timeLineMap.containsKey( server ))
						|| (null == timeLineMap.get( server )) ) {

					lineMap = new TreeMap<IBldTimelineEntity, IBldTimelineAgentEntity>( new IBldTimelineEntityComparator() );
					timeLineMap.put( server, lineMap );
				}

				lineMap = timeLineMap.get( server );
				lineMap.put( timeline, line );

			}
		}

		// ↑の処理だと処理していないlineNoを詰めてしまうので、処理していない部分に明示的にnullをsetする
		for (Map<IBldTimelineEntity, IBldTimelineAgentEntity> map : timeLineMap.values()) {

			for ( IBldTimelineEntity entity : timelineEntitys ) {

				if( map.containsKey( entity ) ) {

				} else {
					map.put(entity, null);
				}

			}

		}
		return timeLineMap;
	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param processs ビルドパッケージ状況
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>>
												normalizeProcess( ITaskFlowProcEntity[] processs ) {

		Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> timeLineMap =
											new HashMap<IBldSrvEntity, Map<String, ITaskFlowProcEntity>>();

		for ( ITaskFlowProcEntity process : processs ) {

			IBldSrvEntity server = TaskImageResourceCache.findUcfServerEntityByCache(process.getBldSrvId());

			Map<String, ITaskFlowProcEntity> lineMap = null;
			if ( (! timeLineMap.containsKey( server ))
				|| (null == timeLineMap.get( server )) ) {
				lineMap = new HashMap<String, ITaskFlowProcEntity>();
				timeLineMap.put( server, lineMap );
			}
			lineMap = timeLineMap.get( server );
			lineMap.put( process.getBldLineNo().toString(), process );
		}

		return timeLineMap;
	}

	/**
	 * システム検索の効率化のため、マップに変換します。
	 *
	 * @param systems サーバ・システム情報
	 * @return システム単位にマップし直した情報を戻します。
	 */
	private final Map<String, IBldSrvEntity> convertSystem( IBldSrvEntity[] systems ) {

		Map<String, IBldSrvEntity> map = new HashMap<String, IBldSrvEntity>();

		for ( IBldSrvEntity system : systems ) {
			map.put(system.getBldSrvId(), system);
		}

		return map;
	}
	/**
	 * タスク情報を、workflowNoをキーとしたマップに格納して返す。
	 * @param taskEntitys タスク情報の配列
	 * @return マップ
	 * <pre>
	 * 		Key		: workflowNo
	 * 		Value	: ITaskEntity
	 * </pre>
	 */
	private final Map<String , ITaskEntity> mapTask( ITaskFlowEntity[] taskEntitys ) {
		Map<String , ITaskEntity> map = new HashMap<String , ITaskEntity>() ;

		for( ITaskFlowEntity taskFlowEntity : taskEntitys ) {
			map.put( taskFlowEntity.getTaskFlowId() , taskFlowEntity.getTask() ) ;
		}
		return map ;
	}

	/**
	 * タスクのターゲット情報（TaskTargetを、sequenceNoをキーとしたマップに格納して返す。
	 * @param taskEntity タスク情報
	 * @return マップ
	 * <pre>
	 * 		Key		: target.sequenceNo
	 * 		Value	: ITaskTargetEntity
	 * </pre>
	 */
	private final Map<String , ITaskTargetEntity> mapTarget( ITaskEntity taskEntity ) {
		Map<String , ITaskTargetEntity> map = new HashMap<String , ITaskTargetEntity>() ;

		ITaskTargetEntity[] targetEntitys = taskEntity.getTarget() ;
		for( ITaskTargetEntity targetEntity : targetEntitys ) {
			map.put( targetEntity.getSequenceNo().toString() , targetEntity ) ;
		}
		return map ;
	}

	private final boolean isProcessExist(ITaskFlowProcEntity[] entity) throws Exception {

		if( 0 == entity.length ) {
			return false;
		} else {
			return true;
		}
	}

	// isProcessExist() == true であること
	public enum ProcessState {
		PROCESSING,	// 実行中
		ERROR, 	 	// エラーによる停止
		COMPLETE	// 全タスク終了による停止

	}
	public final ProcessState evalProcessState( ITaskFlowProcEntity[] processEntities, IBldTimelineEntity[] timeLineEntities) {

		Set<String> processLineNoSet = new HashSet<String>();
		// プロセス中に１件でもエラーがあったらERROR
		boolean err_ = false;
		boolean active_ = false;
		for (ITaskFlowProcEntity processEntity : processEntities) {
			String state = processEntity.getStsId();

			processLineNoSet.add(processEntity.getBldLineNo().toString());

			if( RmRpStatusIdForExecData.CreatingReleasePackage.equals(state) ) {
				active_ = true;
			}
			if( RmRpStatusIdForExecData.ReleasePackageError.equals(state) ) {
				err_ = true;
			}
		}
		if( true == active_ ) {
			return ProcessState.PROCESSING;
		} else if( true == err_ ) {
			return ProcessState.ERROR;
		}

		// プロセス数がタイムライン数と一致していればCOMPLETE
		// プロセス数がタイムライン数を満たしていなければPROCESSING
		// プロセス数がタイムライン数を超えていてもCOMPLETE(debugログ対象)
		int maxLength = timeLineEntities.length;
		int processingLength = processLineNoSet.size();

		if( processingLength < maxLength ) {
			return ProcessState.PROCESSING;
		} else if(processingLength == maxLength) {
			return ProcessState.COMPLETE;
		} else {
			LogHandler.debug( log , "irregular:processingLength(" + processingLength + ") < maxLength(" + maxLength + ")");
			return ProcessState.COMPLETE;
		}
	}

	private final boolean isActiveServer( Map<String, ITaskFlowProcEntity> processLineMap ) {
		if ( null == processLineMap )
			return false;

		boolean isActive = false;

		for ( ITaskFlowProcEntity entity : processLineMap.values() ) {
			if ( ! RmRpStatusIdForExecData.CreatingReleasePackage.equals(entity.getStsId() ))
				continue;

			isActive = true;
			break;
		}

		return isActive;
	}

	@SuppressWarnings("unused")
	private final boolean isActiveSystem( Map<String, ITaskFlowProcEntity> processLineMap, String systemNo ) {
		if ( null == processLineMap )
			return false;

		boolean isActive = false;

		for ( ITaskFlowProcEntity entity : processLineMap.values() ) {
			if ( ! RmRpStatusIdForExecData.CreatingReleasePackage.equals(entity.getStsId() ))
				continue;

			isActive = true;
			break;
		}

		return isActive;
	}

	/**
	 *
	 * <p>
	 * inputの構造<br>
	 * <table>
	 * <tr><td>(list.get(0))[0]</td><td>(list.get(0))[1]</td><td>(list.get(0))[2]</td></tr>
	 * <tr><td>(list.get(1))[0]</td><td>(list.get(1))[1]</td><td>                </td></tr>
	 * <tr><td>(list.get(2))[0]</td><td>(list.get(2))[1]</td><td>(list.get(2))[2]</td></tr>
	 * <tr><td>(list.get(3))[0]</td><td>                </td><td>                </td></tr>
	 * </table>
	 * </p>
	 *
	 * <p>
	 * outputの構造<br>
	 * <table>
	 * <tr><td>(list.get(0))[0]</td><td>(list.get(1))[0]</td><td>(list.get(2))[0]</td><td>(list.get(3))[0]</td></tr>
	 * <tr><td>(list.get(0))[1]</td><td>(list.get(1))[1]</td><td>(list.get(2))[1]</td><td>                </td></tr>
	 * <tr><td>(list.get(0))[2]</td><td>                </td><td>(list.get(2))[2]</td><td>                </td></tr>
	 * </table>
	 * </p>
	 *
	 * @param generateDetailViewBean
	 * @return
	 */
	private final CtlDetailViewBean convertArrayStructure(CtlDetailViewBean releaseDetailViewBean) {

		List<RelProcessViewBean> prmList = releaseDetailViewBean.getRelProcessViewBeanList();
		if(log.isDebugEnabled()) {
			String strPrmArrStructure = this.showArrayStructure(prmList);
			LogHandler.debug( log , strPrmArrStructure );
		}

		// 空だったらとりあえず返す
		if(prmList.isEmpty())
			return releaseDetailViewBean;

		int col = 0;
		int row = 0;
		// タテヨコ変換なのでListのサイズがヨコに相当する
		col = prmList.size();
		// 変換前のヨコ(配列の数)は違う可能性があるのでMaxを持ってくる->タテに相当
		for (RelProcessViewBean bean : prmList) {

			String[] imgs = bean.getImg();
			if( null != imgs ) {

				int iL = imgs.length;
				if(row < iL)  row = iL;		// rowより大きければ更新
			}

			String[] msgs = bean.getMsg();
			if( null != msgs ) {

				int mL = msgs.length;
				if(row < mL) row = mL;		// rowより大きければ更新
			}

			String[] actives = bean.getActive();
			if( null != actives ) {

				int aL = actives.length;
				if(row < aL) row = aL;		// rowより大きければ更新
			}
		}

		// 始めに返すListの構造を作っておく
		List<RelProcessViewBean> newList = new ArrayList<RelProcessViewBean>(row);
		for (int i = 0; i < row; i++) {

			RelProcessViewBean newBean = releaseDetailViewBean.newRelProcessViewBean();
			newBean.setImg(new String[col]);
			newBean.setMsg(new String[col]);
			newBean.setActive(new String[col]);

			newList.add(newBean);
		}


		// タテヨコを入れ替えるので、iとjを逆にする
		for (int i = 0; i < prmList.size(); i++) {
			RelProcessViewBean bean = prmList.get(i);

//			 前段の処理でimgsのサイズとmsgsのサイズが同じの前提
			String[] imgs = bean.getImg();
			String[] msgs = bean.getMsg();
			String[] actives = bean.getActive() ;
			for (int j = 0; j < imgs.length; j++) {

				String img = null;
				if( j < imgs.length ) {
					img = imgs[j];
				}

				String msg = null;
				if( null != msgs && j < msgs.length ) {
					msg = msgs[j];
				}

				String active = null;
				if( null != actives && j < actives.length ) {
					active = actives[j];
				}

				// 添え字指定でオブジェクトを持ってくる
				// 始めに返すListの構造を作っているので、直指定が可能なはず
				String[] newImg = newList.get(j).getImg();
				String[] newMsg = newList.get(j).getMsg();
				String[] newActive = newList.get(j).getActive();

				newImg[i] = img;
				newMsg[i] = msg;
				newActive[i] = active;
			}
		}

		if(log.isDebugEnabled()) {
			String strNewArrStructure = this.showArrayStructure(newList);
			LogHandler.debug( log , strNewArrStructure );
		}

		// buildProcessViewBeanListを新しいので差し替え
		releaseDetailViewBean.setRelProcessViewBeanList(newList);
		return releaseDetailViewBean;
	}

	/*
	 * ヨコ表示用のコントロールブレイクの処理をとりあえず残しておく
	 * 時期をみて必要なくなったら削除予定
	 */
	/* 使用されないprivateメソッド
	 private final CtlDetailViewBean convertArrayStructure2(CtlDetailViewBean releaseDetailViewBean) {

//		//コントロール・ブレイク
//		String maxColumn = DesignProjectUtil.getValue( DesignProjectRelDefineId.taskMaxColumns );
//		if ( column == Integer.parseInt( maxColumn ) ) {
//			isBreakSystem = true;
//
//			viewBean.setImg( imgPathList.toArray(new String[imgPathList.size()] ));
//			relProcessViewBeanList.add( viewBean );
//
//			viewBean = releaseDetailViewBean.newRelProcessViewBean();
//			imgPathList = new ArrayList<String>();
//
//			column = 0;
//		}

		return releaseDetailViewBean;
	}*/

	private final String showArrayStructure(List<RelProcessViewBean> relProcessViewBeanList) {

		StringBuilder sb = new StringBuilder();

		if(relProcessViewBeanList.isEmpty()) {
			return "";
		}

		int i = 0;
		for (RelProcessViewBean bean : relProcessViewBeanList) {

			sb.append("bean(" + i + "):");
			sb.append("\t");

			String[] imgs = bean.getImg();

			if(null != imgs) {

				int j = 0;
				for (String img : imgs) {

					sb.append("img[" + j + "]=" + img + ",");
					sb.append("\t");
					j++;
				}
			}
			String[] msgs = bean.getMsg();

			if(null != msgs) {

				int k = 0;
				for (String msg : msgs) {

					sb.append("msg[" + k + "]=" + msg + ",");
					sb.append("\t");
					k++;
				}
			}

			String[] actives = bean.getActive();

			if(null != actives) {

				int l = 0;
				for (String active : actives) {

					sb.append("active[" + l + "]=" + active + ",");
					sb.append("\t");
					l++;
				}
			}

			sb.append("[EOD]");
			sb.append("\n");

			i++;
		}

		return sb.toString();
	}


	private class IUcfServerEntityComparator implements Comparator<IBldSrvEntity> {

		public int compare(IBldSrvEntity obj1, IBldSrvEntity obj2) {

			String serverNo1 = obj1.getBldSrvId();
			String serverNo2 = obj2.getBldSrvId();

			if ( StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
					return 1;
				}
			}

			if ( StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
					return -1;
				}
			}

			return serverNo1.compareTo( serverNo2 );
		}
	}

	private class IBldTimelineEntityComparator implements Comparator<IBldTimelineEntity> {

		public int compare(IBldTimelineEntity obj1, IBldTimelineEntity obj2) {

			Integer lineNo1 = obj1.getBldLineNo();
			Integer lineNo2 = obj2.getBldLineNo();

			return lineNo1.compareTo(lineNo2);
		}
	}

	private CtlDetailViewBean makeCtlDetailViewBean(
			IRpEntity relEntity ,
			IBldTimelineEntity[] timeLineEntitys ,
			ITaskFlowProcEntity[] process ,
			Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> lineMap ,
			Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> processMap ,
			Map<String, IBldSrvEntity> systemMap ,
			Map<String , ITaskEntity> taskMap
			) {

		List<RelProcessViewBean> relProcessViewBeanList = new ArrayList<RelProcessViewBean>();

//		boolean isActiveEnv = isActiveEnv( processMap );//未使用のローカル変数
		@SuppressWarnings("unused")
		boolean isBreakEnv = true;

		String activeServerNo = null;
		boolean isBreakServer = true;

		String activeSystemNo = null;
		boolean isBreakSystem = true;


		CtlDetailViewBean releaseDetailViewBean = new CtlDetailViewBean();

		for ( IBldSrvEntity serverEntity : lineMap.keySet() ) {

			if ( null == activeServerNo || ! serverEntity.getBldSrvId().equals( activeServerNo )) {
				activeServerNo = serverEntity.getBldSrvId();
				isBreakEnv = true;
				isBreakServer = true;
				isBreakSystem = true;
			}

			RelProcessViewBean viewBean = releaseDetailViewBean.newRelProcessViewBean();

			String serverStsId = null;
			String msgServer = null ;
			String osTypeId = null;
			String msgSystem = null ;
			List<String> taskStsIdList = new ArrayList<String>();
			List<String> messageList = new ArrayList<String>();
			List<String> activeList = new ArrayList<String>() ;

			Map<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMap = lineMap.get( serverEntity );
			Map<String, ITaskFlowProcEntity> processLineMap = processMap.get( serverEntity );

//			int column = 0; //使用されないローカル変数
//			for ( IBldTimelineEntity timeLineEntity : timeLineEntitys ) {
			for ( Map.Entry<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMapEntry : targetLineMap.entrySet() ) {

				IBldTimelineEntity timeLineEntity = targetLineMapEntry.getKey();
				IBldTimelineAgentEntity lineEntity = targetLineMapEntry.getValue();
//
//				if ( isBreakEnv ) {
//					if ( isActiveEnv ) {
//						imgPathList.add( envEntity.getImg()[IBldTimelineEntity.img.active.getValue()] );
//					} else {
//						imgPathList.add( envEntity.getImg()[IBldTimelineEntity.img.off.getValue()] );
//					}
//					isBreakEnv = false;
//				}

				if ( isBreakServer ) {
					if ( isActiveServer( processLineMap )) {
						serverStsId = serverEntity.getSrvStsIdActive();
					} else {
						serverStsId = serverEntity.getSrvStsIdOff();
					}
					msgServer = serverEntity.getBldSrvNm() ;
					isBreakServer = false;
				}

				if ( null != lineEntity ) {
					//タイムライン上に業務処理が存在する場合

					if ( null == activeSystemNo || ! lineEntity.getBldSrvId().equals( activeSystemNo )) {
						activeSystemNo = lineEntity.getBldSrvId();
						isBreakSystem = true;
					}

					if ( isBreakSystem ) {
						IBldSrvEntity bldSrvEntity = systemMap.get( lineEntity.getBldSrvId() ) ;
						if( null == bldSrvEntity ) {
							throw new TriSystemException(RmMessageId.RM005006S , lineEntity.getBldSrvId());
						}

						osTypeId = bldSrvEntity.getOsTyp();
						msgSystem = bldSrvEntity.getBldSrvNm();
						isBreakSystem = false;
					}
					//
					if ( null != processLineMap ) {
						ITaskFlowProcEntity processEntity = processLineMap.get( String.valueOf(timeLineEntity.getBldLineNo()) );

						if ( null != processEntity && RmRpStatusIdForExecData.CreatingReleasePackage.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_ON );
							taskStsIdList.add( lineEntity.getTargetStsIdActive() );
							activeList.add( STATUS_ACTIVE ) ;
						}

						if ( null != processEntity && RmRpStatusIdForExecData.ReleasePackageError.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdErr() );
							activeList.add( STATUS_ERROR ) ;
						}

						if ( null != processEntity && RmRpStatusId.ReleasePackageCreated.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdOff() );
							activeList.add( STATUS_COMPLETE ) ;
							LogHandler.debug( log , "DEBUG " + processEntity.getBldLineNo() + " " + processEntity.getStsId() ) ;
						}
						if ( null == processEntity ) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdOff() );
							activeList.add( null ) ;
						}
					} else {
						//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
						taskStsIdList.add( lineEntity.getTargetStsIdOff() );
						activeList.add( null ) ;
					}

//					column++;
				} else {
					//タイムライン上に業務処理が存在しない場合
					//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
					taskStsIdList.add( WAIT );
					activeList.add( null ) ;
//					column++;
				}
				//メッセージ
				if( null != lineEntity ) {
					ITaskEntity taskEntity = taskMap.get( lineEntity.getTaskFlowId() ) ;
					String message = "" ;
					if( null != taskEntity ) {
						Map<String,ITaskTargetEntity> targetMap = this.mapTarget( taskEntity ) ;
						if( true == targetMap.containsKey( lineEntity.getTargetSeqNo().toString() ) ) {
							message = targetMap.get( lineEntity.getTargetSeqNo().toString() ).getName() ;
						}
					}
					messageList.add( message ) ;
				} else {
					messageList.add( "" ) ;
				}
			}

//			画像アイコン用
			List<String> serverTaskStsIdList = new ArrayList<String>();
			serverTaskStsIdList.add( serverStsId );//server
			serverTaskStsIdList.add( osTypeId );//system
			//imgOutPathList.add( IMAGE_NEXT_ARROW_OFF );//arrow
			serverTaskStsIdList.addAll( taskStsIdList );
			//メッセージ用
			List<String> messageOutList = new ArrayList<String>();
			messageOutList.add( msgServer );//server
			messageOutList.add( msgSystem );//system
			//messageOutList.add( "" );//arrow
			messageOutList.addAll( messageList );

			//タイムライン装飾用
			List<String> activeOutList = new ArrayList<String>();
			activeOutList.add( null ) ;//server
			activeOutList.add( null ) ;//system
			//activeOutList.add( null ) ;//arrow
			activeOutList.addAll( activeList ) ;

			viewBean.setImg( serverTaskStsIdList.toArray(new String[serverTaskStsIdList.size()] ));
			viewBean.setMsg( messageOutList.toArray( new String[messageOutList.size()] ));
			viewBean.setActive( activeOutList.toArray( new String[activeOutList.size()] ));
			relProcessViewBeanList.add( viewBean );
		}

		releaseDetailViewBean.setRelNo					( relEntity.getRpId() );
		releaseDetailViewBean.setCopeUser				( relEntity.getExecUserNm() );
		releaseDetailViewBean.setCopeUserId				( relEntity.getExecUserId() );
		releaseDetailViewBean.setRelProcessViewBeanList	( relProcessViewBeanList );

		releaseDetailViewBean = convertArrayStructure(releaseDetailViewBean);

		ProcessState ps = evalProcessState(process, timeLineEntitys);
		switch (ps) {
		case PROCESSING:
			releaseDetailViewBean.setRelStatusMsg( contextAdapter().getMessage( TriLogMessage.LRM0004 ) );
			break;
		case ERROR:

			releaseDetailViewBean.setRelStatusMsg( contextAdapter().getMessage( TriLogMessage.LRM0005 ) );
			break;
		case COMPLETE:

			releaseDetailViewBean.setRelStatusMsg( contextAdapter().getMessage( TriLogMessage.LRM0006 ) );
			break;
		default:

			releaseDetailViewBean.setRelStatusMsg( "" );
			break;
		}

		return releaseDetailViewBean ;
	}

	private IContextAdapter contextAdapter() {

		if(ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return ac;
	}
}
