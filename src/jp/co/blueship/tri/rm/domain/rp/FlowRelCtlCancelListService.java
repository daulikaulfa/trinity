package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelListServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリース取消一覧画面の表示情報設定Class<br>
 * <br>
 * @version V3L10.02
 * @author Takashi Ono
 */
public class FlowRelCtlCancelListService implements IDomain<FlowRelCtlCancelListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlCancelListServiceBean> execute( IServiceDto<FlowRelCtlCancelListServiceBean> serviceDto ) {

		FlowRelCtlCancelListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( refererID.equals( RelCtlScreenID.CANCEL_LIST )) {
			}

			if ( forwordID.equals( RelCtlScreenID.CANCEL_LIST )) {
				//リリース情報の検索
				this.searchRelList(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005057S , e );
		}
	}

	/**
	 * リリースリストの詳細検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchRelList( FlowRelCtlCancelListServiceBean paramBean ) {

		boolean isSearch = false;
		boolean isAutoSearch = false;

		{

			String selectedEnvNo = paramBean.getSelectedEnvNo();
			ItemCheckAddonUtils.checkConfNo( selectedEnvNo );

			List<ILotEntity> pjtLotEntityList = this.getPjtLotEntityLimit().getEntities() ;
			if ( TriStringUtils.isEmpty( pjtLotEntityList )) {
				paramBean.setInfoMessage( RmMessageId.RM001029E );
			}
			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
			List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( pjtLotEntityList );

			List<IGrpUserLnkEntity> groupUserEntityArray = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			RelCtlSearchBean relCtlSearchBean = DBSearchBeanAddonUtil.setRelCtlSearchBean(
					paramBean.getRelCtlSearchBean(),
					RmDesignBeanId.rpCancelListCount ,
					lotDtoEntities ,
					groupUserEntityArray ,
					RmDesignBeanId.statusId,
					srvEntity.getBldSrvId() );

			paramBean.setRelCtlSearchBean( relCtlSearchBean );

			if ( !TriStringUtils.isEmpty( pjtLotEntityList ) ) {
				if ( TriStringUtils.isEmpty( relCtlSearchBean.getSearchLotNoList() )) {
					paramBean.setInfoMessage( RmMessageId.RM001034E );
				} else {
					DBSearchBeanAddonUtil.setRelCtlSearchBean(
							selectedEnvNo,
							relCtlSearchBean,
							lotDtoEntities);

					if ( TriStringUtils.isEmpty( relCtlSearchBean.getSearchLotNoList() ) ) {
						paramBean.setInfoMessage( RmMessageId.RM001036E );
					}
				}

				if ( TriStringUtils.isEmpty( relCtlSearchBean.getSearchLotNoList() ) ) {
					List<CtlListViewBean> newViewBeanList = new ArrayList<CtlListViewBean>();
					paramBean.setRelCtlViewBeanList( newViewBeanList );
				}
			}

			IBldEnvEntity entity = this.support.getBmFinderSupport().findBldEnvEntity( selectedEnvNo );
			paramBean.setEnvNo( entity.getBldEnvId() ) ;
			paramBean.setEnvName( entity.getBldEnvNm() );
			paramBean.setRelCtlSearchBean( paramBean.getRelCtlSearchBean() );


			String selectedLotNo = relCtlSearchBean.getSelectedLotNo();

			if( ScreenType.select.equals(paramBean.getScreenType()) ) {

				if ( TriStringUtils.isEmpty(selectedLotNo) ) {
					throw new BusinessException( SmMessageId.SM001002E );
				}

				isSearch = true;
			} else if ( ! TriStringUtils.isEmpty(selectedLotNo) ) {
				if ( ! ScreenType.bussinessException.equals(paramBean.getScreenType()) ) {
					isAutoSearch = true;
				} else {
					//当画面の検索項目での業務エラーか次画面遷移時の業務エラーかを判断できないため、キャッシュを表示する。
					paramBean.setRelCtlViewBeanList( paramBean.getRelCtlViewBeanList() );
				}
			}

			if ( isSearch || isAutoSearch ) {

				IJdbcCondition condition = DBSearchConditionAddonUtil.getRpConditionByCancelableRelease( selectedEnvNo , getRpIdsFromDmDoStsId() );

				int relCount = this.support.getRpDao().count( condition.getCondition() );

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					if ( relCount == 0
						&& isSearch
						&& ! isAutoSearch ) {
						throw new BusinessException( RmMessageId.RM001030E );
					}
				}

				IEntityLimit<IRpEntity> limit = this.getRelEntityLimit(condition, paramBean);
				List<IRpDto> rpDtoList = this.support.findRpDto(limit.getEntities());

				List<IRpEntity> relEntityList = this.support.applySearchCondition(
													rpDtoList, paramBean.getRelCtlSearchBean() );
				List<IRpDto> rpDtoSearchList = this.support.findRpDto(relEntityList);
				List<CtlListViewBean> relViewBeanList = new ArrayList<CtlListViewBean>();
				RmViewInfoAddonUtils.setRelCtlListViewBeanRelEntity( this.support , relViewBeanList, rpDtoSearchList, pjtLotEntityList, entity.getBldEnvId() );

				{
					relCtlSearchBean = paramBean.getRelCtlSearchBean();
					int count = 0;
					int maxCount = ( TriStringUtils.isEmpty( relCtlSearchBean.getSelectedReleaseCount() ) ) ? 0 : Integer.parseInt(relCtlSearchBean.getSelectedReleaseCount() ) ;

					//外部結合検索のため、全体件数を予測できないため、全体件数を指定せずに、ロジックで絞込み。
					//このケースでは、ページ制御を一覧でのみ実現可能。
					List<CtlListViewBean> newViewBeanList = new ArrayList<CtlListViewBean>();
					for ( CtlListViewBean bean : relViewBeanList ) {
						count++;

						if ( 0 != maxCount && count > maxCount )
							break;

						newViewBeanList.add( bean );
					}

					paramBean.setRelCtlViewBeanList( newViewBeanList );
				}
			}
		}
	}

	/**
	 * 検索条件であるロット情報を取得する。
	 *
	 * @return 取得したロット情報を戻します。
	 */
	private final IEntityLimit<ILotEntity> getPjtLotEntityLimit() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition();
		ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelCtlCancelListLotSelect() ;
		IEntityLimit<ILotEntity> limit = this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() , sort, 1 , 0 ) ;
		return limit ;
	}

	/**
	 * リリース情報を取得する。
	 *
	 * @param condition
	 * @param paramBean
	 * @return 取得したリリース情報を戻します。
	 */
	private final IEntityLimit<IRpEntity> getRelEntityLimit(
									IJdbcCondition condition,
									FlowRelCtlCancelListServiceBean paramBean ) {

		this.support.setRelCondition( (RpCondition)condition, paramBean.getRelCtlSearchBean() );

		ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelCtlCancelList();
		IEntityLimit<IRpEntity> limit = this.support.getRpDao().find( condition.getCondition() , sort , 1 , 0 ) ;

		return limit;
	}

	private String[] getRpIdsFromDmDoStsId(){

		String [] distributeBaseStatusId = {
				DmDoStatusId.JobRegistered.getStatusId(),
				DmDoStatusId.TimerConfigured.getStatusId() };

		DmDoCondition condition = new DmDoCondition();
		condition.setStsIds( distributeBaseStatusId );
		List<IDmDoEntity> entityList = this.support.getDmDoDao().find(condition.getCondition());
		List<String> rpIds = new ArrayList<String>();
		for ( IDmDoEntity entity : entityList ) {
			rpIds.add(entity.getRpId() );
		}
		return rpIds.toArray(new String[0]);
	}
}
