package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

/**
 * リリースパッケージ・環境一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ConfigurationViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 環境番号 */
	private String confNo = null;
	/** 環境名 */
	private String confName = null;
	/** 環境概要 */
	private String confSummary = null;

	public String getConfNo() {
		return confNo;
	}
	public void setConfNo(String confNo) {
		this.confNo = confNo;
	}

	public String getConfName() {
		return confName;
	}
	public void setConfName(String confName) {
		this.confName = confName;
	}

	public String getConfSummary() {
		return confSummary;
	}
	public void setConfSummary(String confSummary) {
		this.confSummary = confSummary;
	}
}
