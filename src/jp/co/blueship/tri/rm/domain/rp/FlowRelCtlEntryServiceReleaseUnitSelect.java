package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・ビルドパッケージ選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlEntryServiceReleaseUnitSelect implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelCtlScreenID.UNIT_SELECT ) &&
					!forwordID.equals( RelCtlScreenID.UNIT_SELECT ) ){
				return serviceDto;
			}


			if( refererID.equals( RelCtlScreenID.UNIT_SELECT )) {

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( RelCtlScreenID.UNIT_SELECT )) {

					checkBuildNo( paramBean.getSelectedUnitBean() );
					//this.support.checkReleasableRelUnit	( selectedBuildNo );
				}
			}

			if( forwordID.equals( RelCtlScreenID.UNIT_SELECT )) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();

					if ( StringUtils.isEmpty(baseInfoBean.getBaseInfoInputBean().getRelApplyNo()) ) {
						this.searchUnitList(paramBean, null);
					} else {
						//リリース申請からリリースする場合
						List<String> buildList = new ArrayList<String>();
						for ( UnitBean unit: paramBean.getSelectedUnitBean() ) {
							buildList.add( unit.getBuildNo() );
						}
						this.searchUnitList(paramBean, buildList);
					}
				}
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005067S, e );
		}
	}

	/**
	 * ビルドパッケージ番号が選択されているかどうかチェックする
	 *
	 * @param buildNo ビルドパッケージ番号
	 */

	private static void checkBuildNo( UnitBean[] unitBeans ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( TriStringUtils.isEmpty( unitBeans )) {
				messageList.add		( RmMessageId.RM001023E );
				messageArgsList.add	( new String[] {} );
			} else {

				List<String> mergeOrderList = new ArrayList<String>();
				for ( UnitBean unitBean : unitBeans ) {

					if ( mergeOrderList.contains( unitBean.getMergeOrder() )) {
						messageList.add		( RmMessageId.RM001026E );
						messageArgsList.add	( new String[] {} );
						break;
					} else {
						mergeOrderList.add( unitBean.getMergeOrder() );
					}
				}
			}

		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ビルドパッケージリストの検索を行う。
	 *
	 * @param paramBean
	 * @param buildList
	 */

	private void searchUnitList(
			FlowRelCtlEntryServiceBean paramBean, List<String> buildList ) {

		String selectedLotNo = paramBean.getSelectedLotNo();
		//ItemCheckAddonUtil.checkLotNo( selectedLotNo );


		// 指定されたロット番号を持ち、リリース可能なビルドパッケージ
		IJdbcCondition condition = null;
		if ( paramBean.getMultiSelectable() ) {
			condition =
				DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit4MultiSelectable( selectedLotNo );
		} else {
			condition =
				DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit( new String[]{ selectedLotNo } );
		}

		if ( null != buildList && 0 < buildList.size() ) {
			((BpCondition)condition).setBpIds(buildList.toArray(new String[0]));
		}

		ISqlSort sort =
			(ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelEntryReleaseUnitSelect();

		UnitSelectBean unitSelectBean = paramBean.getReleaseUnitSelectBean();

//				int selectedPageNo	=
//					(( 0 == unitSelectBean.getSelectPageNo() )? 1: unitSelectBean.getSelectPageNo() );
//				int maxPageNumber	=
//					DesignProjectUtil.getMaxPageNumber(
//						DesignProjectRelDefineId.maxPageNumberByRelList );

		int selectedPageNo	= 1;
		int maxPageNumber	= 0;

		IEntityLimit<IBpEntity> buildEntityLimit =
			this.support.getBmFinderSupport().getBpDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );

		if ( buildEntityLimit.getEntities().size() == 0 ) {
			throw new BusinessException( RmMessageId.RM001041E );
		}


		List<String> mergeOrderList = new ArrayList<String>();		// マージ順のコンボボックス
		unitSelectBean.setUnitViewBeanList	(
				this.support.getUnitViewBean(
						buildEntityLimit.getEntities().toArray(new IBpEntity[0]), mergeOrderList, paramBean.getMergeOrderUnSelectedChar() ));
		unitSelectBean.setMergeOrderList	( mergeOrderList );
//				unitSelectBean.setPageInfoView		(
//						AddonUtil.convertPageNoInfo( new PageNoInfo(), buildEntityLimit.getLimit() ));
//				unitSelectBean.setSelectPageNo		( selectedPageNo );


		paramBean.setReleaseUnitSelectBean( unitSelectBean );
	}

}
