package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelConfirmViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;

/**
 * リリース取消用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlCancelServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/** 選択された環境番号 */
	private String selectedEnvNo = null;
	/** 環境名 */
	private String envName = null;
	/** 選択されたリリース番号 */
	private String[] selectedRelNo = null;
	/** リリース編集入力情報 */
	private CancelInfoInputBean releaseInfoInputBean = null;
	/** 申請情報 */
	private List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList = new ArrayList<CancelConfirmViewBean>();

	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public List<CancelConfirmViewBean> getReleaseCancelConfirmViewBeanList() {
		return releaseCancelConfirmViewBeanList;
	}
	public void setReleaseCancelConfirmViewBeanList(
			List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList) {
		this.releaseCancelConfirmViewBeanList = releaseCancelConfirmViewBeanList;
	}
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}
	public String[] getSelectedRelNo() {
		return selectedRelNo;
	}
	public void setSelectedRelNo(String[] selectedRelNo) {
		this.selectedRelNo = selectedRelNo;
	}
	public CancelInfoInputBean getReleaseInfoInputBean() {
		return releaseInfoInputBean;
	}
	public void setReleaseInfoInputBean( CancelInfoInputBean releaseInfoInputBean ) {
		this.releaseInfoInputBean = releaseInfoInputBean;
	}
}
