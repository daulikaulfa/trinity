package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * リリースパッケージ・基本情報入力用（リリース申請の詳細検索を行うための検索条件情報）
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class BaseInfoRelApplySearchBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 
	 * 詳細検索 ロット番号（コンボ用）
	 */
	private List<ItemLabelsBean> searchLotNoList = new ArrayList<ItemLabelsBean>();
	/** 
	 * 詳細検索 ロット番号
	 */
	private List<String> searchLotNoValueList = new ArrayList<String>();
	/** 
	 * 詳細検索 選択済みロット番号 
	 */
	private String selectedLotNo = null;
	/** 
	 * 詳細検索 選択済みサーバ番号（ロット番号とカンマ区切りで連結用） 
	 */
	private String selectedServerNo = null;
	/** 
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = new ArrayList<ItemLabelsBean>();
	/** 
	 * 詳細検索 リリース環境
	 */
	private List<String> searchRelEnvNoValueList = new ArrayList<String>();
	/** 
	 * 詳細検索 選択済みリリース環境 
	 */
	private String selectedRelEnvNo = null;
	/** 
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = new ArrayList<ItemLabelsBean>();
	/** 
	 * 詳細検索 リリース希望日
	 */
	private List<String> searchRelWishDateValueList = new ArrayList<String>();
	/** 
	 * 詳細検索 選択済みリリース希望日 
	 */
	private String selectedRelWishDate = null;
	/** 
	 * 詳細検索 検索件数 
	 */
	private List<ItemLabelsBean> searchRelApplyCountList = new ArrayList<ItemLabelsBean>();
	/** 
	 * 詳細検索 選択済み検索件数 
	 */
	private String selectedRelApplyCount = null;

	public List<ItemLabelsBean> getSearchLotNoList() {
		return searchLotNoList;
	}
	public void setSearchLotNoList(List<ItemLabelsBean> searchLotNoList) {
		this.searchLotNoList = searchLotNoList;
	}
	public List<String> getSearchLotNoValueList() {
		return searchLotNoValueList;
	}
	public void setSearchLotNoValueList(List<String> searchLotNoValueList) {
		this.searchLotNoValueList = searchLotNoValueList;
	}
	public List<ItemLabelsBean> getSearchRelApplyCountList() {
		return searchRelApplyCountList;
	}
	public void setSearchRelApplyCountList(
			List<ItemLabelsBean> searchRelApplyCountList) {
		this.searchRelApplyCountList = searchRelApplyCountList;
	}
	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}
	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}
	public List<String> getSearchRelEnvNoValueList() {
		return searchRelEnvNoValueList;
	}
	public void setSearchRelEnvNoValueList(List<String> searchRelEnvNoValueList) {
		this.searchRelEnvNoValueList = searchRelEnvNoValueList;
	}
	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}
	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}
	public List<String> getSearchRelWishDateValueList() {
		return searchRelWishDateValueList;
	}
	public void setSearchRelWishDateValueList(
			List<String> searchRelWishDateValueList) {
		this.searchRelWishDateValueList = searchRelWishDateValueList;
	}
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}
	public String getSelectedRelApplyCount() {
		return selectedRelApplyCount;
	}
	public void setSelectedRelApplyCount(String selectedRelApplyCount) {
		this.selectedRelApplyCount = selectedRelApplyCount;
	}
	public String getSelectedRelEnvNo() {
		return selectedRelEnvNo;
	}
	public void setSelectedRelEnvNo(String selectedRelEnvNo) {
		this.selectedRelEnvNo = selectedRelEnvNo;
	}
	public String getSelectedRelWishDate() {
		return selectedRelWishDate;
	}
	public void setSelectedRelWishDate(String selectedRelWishDate) {
		this.selectedRelWishDate = selectedRelWishDate;
	}
}
