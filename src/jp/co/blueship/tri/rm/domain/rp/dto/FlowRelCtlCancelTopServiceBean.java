package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * リリース取消トップ画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlCancelTopServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;
	
	
	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ReleaseViewBean newReleaseViewBean() {
		ReleaseViewBean bean = new ReleaseViewBean();
		return bean;
	}
	
	public class ReleaseViewBean {

		/** 環境番号 */
		private String envNo = null;
		/** 環境名 */
		private String envName = null;
		/** 環境概要 */
		private String envSummary = null ;
		/** 環境内容 */
		private String envContent = null ;
		
		/** ロット番号 */
		private String lotId = null ;
		/** ロット名 */
		private String lotName = null ;
		/** リリース番号 */
		private String relNo = null;
		/** リリース登録日 */
		private String relInputDate = null;
		/** ビルドパッケージ番号 */
		private List<String> buildNoList = new ArrayList<String>();
		/** ビルドパッケージ作成日 */
		private List<String> buildDateList = new ArrayList<String>();
		/**
		 * このリリース環境の閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;
		
		
		public void setEnvNo( String envNo ) {
			this.envNo = envNo;
		}
		public String getEnvNo() {
			return envNo;
		}

		public void setEnvName( String envName ) {
			this.envName = envName;
		}
		public String getEnvName() {
			return envName;
		}

		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		public String getRelNo() {
			return relNo;
		}

		public void setRelInputDate( String relInputDate ) {
			this.relInputDate = relInputDate;
		}
		public String getRelInputDate() {
			return relInputDate;
		}
		
		public void setBuildNoList( List<String> buildNoList ) {
			this.buildNoList = buildNoList;
		}
		public List<String> getBuildNoList() {
			return buildNoList;
		}
		
		public void setBuildDateList( List<String> buildDateList ) {
			this.buildDateList = buildDateList;
		}
		public List<String> getBuildDateList() {
			return buildDateList;
		}
		public String getEnvContent() {
			return envContent;
		}
		public void setEnvContent(String envContent) {
			this.envContent = envContent;
		}
		public String getEnvSummary() {
			return envSummary;
		}
		public void setEnvSummary(String envSummary) {
			this.envSummary = envSummary;
		}
		public String getLotName() {
			return lotName;
		}
		public void setLotName(String lotName) {
			this.lotName = lotName;
		}
		public String getLotNo() {
			return lotId;
		}
		public void setLotNo(String lotId) {
			this.lotId = lotId;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled(boolean isViewLinkEnabled) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
	}


	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
}
