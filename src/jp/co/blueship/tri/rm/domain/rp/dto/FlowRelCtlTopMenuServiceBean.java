package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
/**
 * リリースパッケージ・トップ画面用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlTopMenuServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;


	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}


	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ReleaseViewBean newReleaseViewBean() {
		ReleaseViewBean bean = new ReleaseViewBean();
		return bean;
	}

	public class ReleaseViewBean {

		/** 環境番号 */
		private String envNo = null;
		/** 環境名 */
		private String envName = null;
		/** リリース番号 */
		private String relNo = null;
		/**
		 * リリース登録日時
		 */
		private String inputDate = null ;
		/** ビルドパッケージ情報 */
		private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
		/** ビルドパッケージ作成日 */
		private List<String> buildDateList = new ArrayList<String>();
		/**
		 * このリリース環境の閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;


		public void setEnvNo( String envNo ) {
			this.envNo = envNo;
		}
		public String getEnvNo() {
			return envNo;
		}

		public void setEnvName( String envName ) {
			this.envName = envName;
		}
		public String getEnvName() {
			return envName;
		}

		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		public String getRelNo() {
			return relNo;
		}

		public String getInputDate() {
			return inputDate;
		}

		public void setInputDate(String inputDate) {
			this.inputDate = inputDate;
		}

		public void setUnitBeanList( List<UnitBean> unitBeanList ) {
			this.unitBeanList = unitBeanList;
		}
		public List<UnitBean> getUnitBeanList() {
			return unitBeanList;
		}

		public void setBuildDateList( List<String> buildDateList ) {
			this.buildDateList = buildDateList;
		}
		public List<String> getBuildDateList() {
			return buildDateList;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled(boolean isViewLinkEnabled) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
	}
}
