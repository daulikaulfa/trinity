package jp.co.blueship.tri.rm.domain.rp.beans.dto;

/**
 * リリースパッケージ検索条件用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class SearchConditionBean {

	/** ビルドパッケージ番号 */
	private String inBuildNo = null;
	/** ビルドパッケージ名 */
	private String inBuildName = null;
	/** リリース番号 */
	private String inRelNo = null;
	/** 変更管理番号 */
	private String inPjtNo = null;
	/** 変更要因番号 */
	private String inChangeCauseNo = null;
	
	
	public String getInBuildNo() {
		return inBuildNo;
	}
	public void setInBuildNo( String inBuildNo ) {
		this.inBuildNo = inBuildNo;
	}
	
	public String getInBuildName() {
		return inBuildName;
	}
	public void setInBuildName( String inBuildName ) {
		this.inBuildName = inBuildName;
	}
	
	public String getInRelNo() {
		return inRelNo;
	}
	public void setInRelNo( String inRelNo ) {
		this.inRelNo = inRelNo;
	}
	
	public String getInPjtNo() {
		return inPjtNo;
	}
	public void setInPjtNo( String inPjtNo ) {
		this.inPjtNo = inPjtNo;
	}
	
	public String getInChangeCauseNo() {
		return inChangeCauseNo;
	}
	public void setInChangeCauseNo( String inChangeCauseNo ) {
		this.inChangeCauseNo = inChangeCauseNo;
	}
}
