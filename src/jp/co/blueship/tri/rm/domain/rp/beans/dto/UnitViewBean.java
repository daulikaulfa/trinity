package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

/**
 * リリースパッケージ・ビルドパッケージ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitViewBean implements Serializable {
	
	private static final long serialVersionUID = 1L;

	/** パッケージ番号 */
	private String buildNo = null;
	/** 作成日時 */
	private String generateDate = null;
	/** ステータス */
	private String buildStatus = null;
	/** 変更管理番号 */
	private String pjtNo = null;
	/** 選択されたマージ順 */
	private String mergeOrder = null;
	
	
	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	public String getGenerateDate() {
		return generateDate;
	}
	public void setGenerateDate( String generateDate ) {
		this.generateDate = generateDate;
	}

	public String getBuildStatus() {
		return buildStatus;
	}
	public void setBuildStatus(String buildStatus) {
		this.buildStatus = buildStatus;
	}

	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo( String pjtNo ) {
		this.pjtNo = pjtNo;
	}
	
	public String getMergeOrder() {
		return mergeOrder;
	}
	public void setMergeOrder( String mergeOrder ) {
		this.mergeOrder = mergeOrder;
	}
}
