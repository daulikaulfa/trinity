package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RemoteServiceType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlTopMenuServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlTopMenuServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリースパッケージ・トップ画面の表示情報設定Class<br>
 *
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlTopMenuService implements IDomain<FlowRelCtlTopMenuServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	/**
	 *
	 */

	@Override
	public IServiceDto<FlowRelCtlTopMenuServiceBean> execute( IServiceDto<FlowRelCtlTopMenuServiceBean> serviceDto ) {

		FlowRelCtlTopMenuServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();


			//アクション
			List<Object> paramList = new ArrayList<Object>();
			paramList.add( RemoteServiceType.AGENT_RELEASE ) ;

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.addAll( paramList );

			try {
				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}
			} finally {
				//agent疎通チェック結果によるメッセージ出力
				this.checkActiveAgents( paramList , paramBean ) ;
			}

			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
			ISqlSort sort = DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelTop();
			IBldEnvEntity[] envAllEntities = this.getRelEnvAllEntity( condition, sort );

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				if ( TriStringUtils.isEmpty( envAllEntities ) ) {
					paramBean.setInfoMessage( RmMessageId.RM001020E );
				}
			}

			// アクセス可能なロットの取得
			List<String> disableLinkLotNumbers = new ArrayList<String>();
			List<ILotEntity> lotEntities = this.getPjtLotEntityLimit().getEntities();
			List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto(lotEntities);

			List<ILotDto> accessableLotList =
				this.support.getAccessableLotEntity(support.getUmFinderSupport(), paramBean.getUserId(), lotDtoEntities, true, disableLinkLotNumbers );
			Map<String, ILotEntity> accessableLotMap = this.support.convertToLotMap( accessableLotList );

//			IGroupUserView[] groupUsers	= this.support.getGroupUserDao().find( paramBean.getUserId() );

			IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity(envAllEntities, accessableLotList);

			List<ReleaseViewBean> releaseViewBeanList = new ArrayList<ReleaseViewBean>();

			List<String> disableLinkEnvNumbers = this.support.getDisableLinkEnvNumbers( envEntities, accessableLotList, disableLinkLotNumbers );

			for ( IBldEnvEntity envEntity : envEntities ) {

				ReleaseViewBean viewBean = paramBean.newReleaseViewBean();
				viewBean.setEnvNo	( envEntity.getBldEnvId() );
				viewBean.setEnvName	( envEntity.getBldEnvNm() );

				if ( disableLinkEnvNumbers.contains( envEntity.getBldEnvId() )) {
					viewBean.setViewLinkEnabled( false );
				} else {
					viewBean.setViewLinkEnabled( true );
				}

				IRpEntity recentRelEntity =
					this.support.getRecentRpEntity( envEntity.getBldEnvId(), accessableLotMap.keySet().toArray(new String[0]) );

				if ( null != recentRelEntity ) {

					// グループの存在チェック
					//ILotEntity pjtLotEntity	= accessableLotMap.get( recentRelEntity.getLotNo() );
					//RelCommonAddonUtil.checkAccessableGroup( pjtLotEntity, this.support.getGroupDao(), groupUsers );
					IRpDto rpDto = this.support.findRpDto(recentRelEntity);
					viewBean.setRelNo		( rpDto.getRpEntity().getRpId() );
					viewBean.setInputDate	( TriDateUtils.convertViewDateFormat( rpDto.getRpEntity().getRelTimestamp() ) );

					List<IBpDto> buildEntities =
						this.support.getBmFinderSupport().findBpDto( RmEntityAddonUtils.getBuildNo( rpDto ) );
					Map<String, String> mergeOrderMap = RmEntityAddonUtils.buildNoMergeOrderMap( this.support , rpDto );

					List<UnitBean> unitBeanList	= viewBean.getUnitBeanList();
					List<String> buildDateList	= viewBean.getBuildDateList();

					for ( IBpDto buildEntity : buildEntities ) {

						UnitBean unitBean = new UnitBean();
						unitBean.setBuildNo		( buildEntity.getBpEntity().getBpId() );
						unitBean.setMergeOrder	( mergeOrderMap.get( buildEntity.getBpEntity().getBpId() ));

						unitBeanList.add		( unitBean );
						buildDateList.add	(
								TriDateUtils.convertViewDateFormat( buildEntity.getBpEntity().getProcEndTimestamp() ) );

					}
					RmEntityAddonUtils.sortUnitBean( unitBeanList );

					viewBean.setUnitBeanList	( unitBeanList );
					viewBean.setBuildDateList	( buildDateList );

				}

				releaseViewBeanList.add( viewBean );

			}

			paramBean.setReleaseViewBeanList( releaseViewBeanList );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005070S, e);
		}
	}

//	※agentがtaskのみに使われていたときの疎通チェックメッセージ生成ルール。
//	/**
//	 * agentの疎通チェック結果を取得し、メッセージ作成を行う。
//	 * @param paramList
//	 * @param paramBean
//	 */
//	private void checkActiveAgents( List<Object> paramList , FlowRelCtlTopMenuServiceBean paramBean ) {
//		AgentStatusTask[] agentStatusArray = ExtractEntityAddonUtil.extractAgentStatusTask( paramList ) ;
//
//		List<String> messageInfoList = new ArrayList<String>() ;
//		List<String[]> messageInfoArgsList = new ArrayList<String[]>() ;
//
//		if( null != agentStatusArray ) {
//			for( AgentStatusTask status : agentStatusArray ) {
//				if( true != status.isStatus() ) {
//					messageInfoList.add( MessageId.MESREL0016 ) ;
//					messageInfoArgsList.add( new String[] { /*status.getConfigLocations() ,*/ status.getServiceUrl() , status.getEnvNo() , status.getServerNo() } ) ;
//				}
//				log.debug( ( status.isStatus() ? "正常" : "無応答" ) + " " + status.getServiceUrl() + " " + status.getEnvNo() + " " + status.getServerNo() ) ;
//			}
//			if( 0 != messageInfoList.size() ) {
//				paramBean.setInfoMessageList( messageInfoList ) ;
//				paramBean.setInfoMessageArgsList( messageInfoArgsList ) ;
//			}
//		}
//	}

	/**
	 * agentの疎通チェック結果を取得し、メッセージ作成を行う。
	 * @param paramList
	 * @param paramBean
	 */

	private void checkActiveAgents( List<Object> paramList , FlowRelCtlTopMenuServiceBean paramBean ) {

		AgentStatus[] agentStatusArray = RmExtractEntityAddonUtils.extractAgentStatusArray( paramList ) ;
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		List<IMessageId> messageInfoList = new ArrayList<IMessageId>() ;
		List<String[]> messageInfoArgsList = new ArrayList<String[]>() ;

		if( null != agentStatusArray ) {
			for( AgentStatus status : agentStatusArray ) {
				IRmiSvcDto remoteService = status.getRemoteServiceEntity() ;
				//画面にはエラーのもののみ出力
				if( true != status.isStatus() ) {
					messageInfoList.add( RmMessageId.RM001036E ) ;
					String[] messageArgs = new String[] {
							remoteService.getRmiSvcEntity().getBldSrvId() ,
							remoteService.getRmiSvcEntity().getRmiSvcId() ,
							remoteService.getRmiSvcEntity().getRmiHostNm() ,
							String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()) ,
							String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()) ,
					} ;
					messageInfoArgsList.add( messageArgs ) ;
				}
				//ログにはすべてのステータスを出力
				String statusStr = status.isStatus() ? ac.getMessage(TriLogMessage.LSM0014) : ac.getMessage(TriLogMessage.LSM0015) ;
				String[] messageArgs = new String[] {
						statusStr ,
						remoteService.getRmiSvcEntity().getBldSrvId() ,
						remoteService.getRmiSvcEntity().getRmiSvcId() ,
						remoteService.getRmiSvcEntity().getRmiHostNm() ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()) ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()) ,
				} ;
				String message = ac.getMessage( paramBean.getLanguage(), RmMessageId.RM001037E , messageArgs ) ;
				LogHandler.info( log , message ) ;
			}

			if( 0 != messageInfoList.size() ) {
				paramBean.setInfoMessageIdList( messageInfoList ) ;
				paramBean.setInfoMessageArgsList( messageInfoArgsList ) ;
			}
		}
	}

	/**
	 * 検索条件であるロット情報を取得する。
	 *
	 * @return 取得したロット情報を戻します。
	 */
	private final IEntityLimit<ILotEntity> getPjtLotEntityLimit() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition();
		IEntityLimit<ILotEntity> limit = this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() , null, 1 , 0 ) ;
		return limit ;
	}

	/**
	 * リリース環境情報を取得する。
	 * @param condition
	 * @param sort
	 * @return 全環境情報
	 */
	private IBldEnvEntity[] getRelEnvAllEntity( IJdbcCondition condition, ISqlSort sort ) {
		List<IBldEnvEntity> entities =
			this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort );

		return entities.toArray(new IBldEnvEntity[0]);
	}

}
