package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

/**
 * リリース申請・ビルドパッケージに含まれる資産申請用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class AssetApplyBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** パッケージ番号 */
	private String buildNoKey = null;
	/** 変更管理番号 */
	private String pjtNo = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 申請番号 */
	private String applyNo = null;
	
	public String getBuildNoKey() {
		return buildNoKey;
	}
	public void setBuildNoKey(String buildNoKey) {
		this.buildNoKey = buildNoKey;
	}
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}
	
}
