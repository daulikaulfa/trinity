package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.EntryConfirmBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitSelectBean;

/**
 * リリースパッケージ・基本情報入力～ビルドパッケージ作成確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlEntryServiceBean extends ReportServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * リリース基本情報
	 */
	private BaseInfoBean baseInfoBean = null;
	/**
	 * 環境選択
	 */
	private ConfigurationSelectBean confSelectBean = null;
	/**
	 * 選択された環境番号
	 */
	private String selectedEnvNo = null;
	/**
	 * ロット選択
	 */
	private LotSelectBean lotSelectBean = null;
	/**
	 * 選択されたロット番号
	 */
	private String selectedLotNo = null;
	/**
	 * 選択されたサーバ番号
	 */
	private String selectedServerNo = null;
	/**
	 * ビルドパッケージ選択
	 */
	private UnitSelectBean releaseUnitSelectBean = null;
	/**
	 * 選択されたビルドパッケージ
	 */
	private UnitBean[] selectedUnitBean = null;
	/**
	 * ビルドパッケージ一覧
	 */
	private UnitBean[] viewUnitBean = null;
	/**
	 * リリースパッケージ確認
	 */
	private EntryConfirmBean releaseConfirmBean = null;
	/**
	 * リリース番号
	 */
	private String relNo = null;
	/**
	 * リリース状況照会
	 */
	private CtlDetailViewBean releaseDetailViewBean = null;
	/**
	 * マージ順とRP番号の区切り文字
	 */
	private String mergeOrderSeparatorChar = null;
	/**
	 * マージ順で未選択を示す文字
	 */
	private String mergeOrderUnSelectedChar = null;
	/**
	 * ビルドパッケージの複数選択可能フラグ
	 */
	private boolean multiSelectable = false;
	
	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public RelApplyViewBean newRelApplyViewBean() {
		RelApplyViewBean bean = new RelApplyViewBean();
		return bean;
	}

	public class RelApplyViewBean {

		/** リリース申請番号 */
		private String relApplyNo = null;
		/** 環境番号 */
		private String relEnvNo = null;
		/** 環境名 */
		private String relEnvName = null;
		/** リリース登録日 */
		private String relInputDate = null;
		/** ビルドパッケージ情報 */
		private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
		/** 資産申請情報 */
		private List<AssetApplyBean> assetApplyBeanList = new ArrayList<AssetApplyBean>();
		/** ステータスID */
		private String relApplyStatusId = null;
		/** ステータス */
		private String relApplyStatus = null;

		/**
		 * このロットの閲覧・編集リンクを有効にするかどうか
		 */
		private boolean isEditLinkEnabled = false;
		/**
		 * このロットの閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;


		public boolean isEditLinkEnabled() {
			return isEditLinkEnabled;
		}
		public void setEditLinkEnabled(boolean isEditLinkEnabled) {
			this.isEditLinkEnabled = isEditLinkEnabled;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled(boolean isViewLinkEnabled) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
		public void setRelApplyNo( String relNo ) {
			this.relApplyNo = relNo;
		}
		public String getRelApplyNo() {
			return relApplyNo;
		}

		public void setRelEnvNo( String relEnvNo ) {
			this.relEnvNo = relEnvNo;
		}
		public String getRelEnvNo() {
			return relEnvNo;
		}

		public void setRelEnvName( String relEnvName ) {
			this.relEnvName = relEnvName;
		}
		public String getRelEnvName() {
			return relEnvName;
		}

		public void setRelInputDate( String relInputDate ) {
			this.relInputDate = relInputDate;
		}
		public String getRelInputDate() {
			return relInputDate;
		}

		public void setUnitBeanList( List<UnitBean> unitBeanList ) {
			this.unitBeanList = unitBeanList;
		}
		public List<UnitBean> getUnitBeanList() {
			return unitBeanList;
		}

		public void setAssetApplyBeanList( List<AssetApplyBean> assetApplyBeanList ) {
			this.assetApplyBeanList = assetApplyBeanList;
		}
		public List<AssetApplyBean> getAssetApplyBeanList() {
			return assetApplyBeanList;
		}

		public String getRelApplyStatus() {
			return relApplyStatus;
		}
		public void setRelApplyStatus(String relApplyStatus) {
			this.relApplyStatus = relApplyStatus;
		}

		public String getRelApplyStatusId() {
			return relApplyStatusId;
		}
		public void setRelApplyStatusId(String relApplyStatusId) {
			this.relApplyStatusId = relApplyStatusId;
		}
	}

	public BaseInfoBean getBaseInfoBean() {
		if ( null == baseInfoBean ) {
			baseInfoBean = new BaseInfoBean();
		}
		return baseInfoBean;
	}
	public void setBaseInfoBean( BaseInfoBean baseInfoBean ) {
		this.baseInfoBean = baseInfoBean;
	}

	public ConfigurationSelectBean getConfSelectBean() {
		if ( null == confSelectBean ) {
			confSelectBean = new ConfigurationSelectBean();
		}
		return confSelectBean;
	}
	public void setConfSelectBean(ConfigurationSelectBean confSelectBean) {
		this.confSelectBean = confSelectBean;
	}

	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}

	public LotSelectBean getLotSelectBean() {
		if ( null == lotSelectBean ) {
			lotSelectBean = new LotSelectBean();
		}
		return lotSelectBean;
	}
	public void setLotSelectBean(LotSelectBean lotSelectBean) {
		this.lotSelectBean = lotSelectBean;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}

	public UnitSelectBean getReleaseUnitSelectBean() {
		if ( null == releaseUnitSelectBean ) {
			releaseUnitSelectBean = new UnitSelectBean();
		}
		return releaseUnitSelectBean;
	}
	public void setReleaseUnitSelectBean( UnitSelectBean releaseUnitSelectBean ) {
		this.releaseUnitSelectBean = releaseUnitSelectBean;
	}

	public UnitBean[] getSelectedUnitBean() {
		return selectedUnitBean;
	}
	public void setSelectedUnitBean( UnitBean[] selectedUnitBean ) {
		this.selectedUnitBean = selectedUnitBean;
	}

	public UnitBean[] getViewUnitBean() {
		return viewUnitBean;
	}
	public void setViewUnitBean( UnitBean[] viewUnitBean ) {
		this.viewUnitBean = viewUnitBean;
	}

	public EntryConfirmBean getReleaseConfirmBean() {
		if ( null == releaseConfirmBean ) {
			releaseConfirmBean = new EntryConfirmBean();
		}
		return releaseConfirmBean;
	}
	public void setReleaseConfirmBean( EntryConfirmBean releaseConfirmBean ) {
		this.releaseConfirmBean = releaseConfirmBean;
	}

	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public CtlDetailViewBean getReleaseDetailViewBean() {
		if ( null == releaseDetailViewBean ) {
			releaseDetailViewBean = new CtlDetailViewBean();
		}
		return releaseDetailViewBean;
	}
	public void setReleaseDetailViewBean(
			CtlDetailViewBean releaseDetailViewBean ) {
		this.releaseDetailViewBean = releaseDetailViewBean;
	}

	public String getMergeOrderSeparatorChar() {
		return mergeOrderSeparatorChar;
	}
	public void setMergeOrderSeparatorChar( String mergeOrderSeparatorChar ) {
		this.mergeOrderSeparatorChar = mergeOrderSeparatorChar;
	}

	public String getMergeOrderUnSelectedChar() {
		return mergeOrderUnSelectedChar;
	}
	public void setMergeOrderUnSelectedChar( String mergeOrderUnSelectedChar ) {
		this.mergeOrderUnSelectedChar = mergeOrderUnSelectedChar;
	}

	public boolean getMultiSelectable() {
		return multiSelectable;
	}
	public void setMultiSelectable( boolean multiSelectable ) {
		this.multiSelectable = multiSelectable;
	}

}
