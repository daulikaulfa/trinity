package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・ロット選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlEntryServiceLotSelect implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;
	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if( !refererID.equals( RelCtlScreenID.LOT_SELECT ) &&
					!forwordID.equals( RelCtlScreenID.LOT_SELECT ) ){
				return serviceDto;
			}

			if( refererID.equals( RelCtlScreenID.LOT_SELECT )) {

				String selectedLotNo = paramBean.getSelectedLotNo();
				String selectedServerNo = paramBean.getSelectedServerNo();

				if ( ScreenType.next.equals( screenType ) ) {
					ItemCheckAddonUtils.checkLotNo( selectedLotNo );

					// グループの存在チェック
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( selectedLotNo );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				}

				paramBean.setSelectedLotNo( selectedLotNo );
				paramBean.setSelectedServerNo( selectedServerNo );
			}

			if( forwordID.equals( RelCtlScreenID.LOT_SELECT )) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();

					if ( StringUtils.isEmpty(baseInfoBean.getBaseInfoInputBean().getRelApplyNo()) ) {
						this.searchLotList(paramBean);
					} else {
						//リリース申請からリリースする場合
						this.searchLot(paramBean);
					}
				}

			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005065S, e);
		}
	}

	/**
	 * リリース対象ロットリストの検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchLotList( FlowRelCtlEntryServiceBean paramBean ) {

		LotSelectBean lotSelectBean = paramBean.getLotSelectBean();

		//進行中のロットの存在確認
		{
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition();
			int lotCount = this.support.getAmFinderSupport().getLotDao().count( condition.getCondition() );

			if ( 0 == lotCount ) {
				throw new BusinessException( RmMessageId.RM001035E );
			}
		}

		// 活動中の全ロットを取得
		List<ILotEntity> accessableLotList = null;
		{
			LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();
			List<String> disableLinkLotNumbers	= new ArrayList<String>();

			accessableLotList = this.support.getAccessableLotNumbers( lotCondition, null, support.getUmFinderSupport(),paramBean, false, disableLinkLotNumbers, false );
		}

		if ( 0 == accessableLotList.size() ) {
			throw new BusinessException( RmMessageId.RM001034E );
		}

		// ロットエンティティ取得
		{
			String[] lotId = this.support.getLotNoSetByReleasableRelUnit(
					this.support.convertToLotNo(accessableLotList) ).toArray( new String[0] );

			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
			ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelEntityLotSelect();
			List<ILotEntity> lotEntities =
				this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
			accessableLotList = lotEntities;
		}

		if ( 0 == accessableLotList.size() ) {
			throw new BusinessException( BmMessageId.BM001000E );
		}
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( accessableLotList );
		for ( ILotDto entity : lotDtoEntities ) {

			LotViewBean viewBean = new LotViewBean();
			RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( viewBean, entity.getLotEntity(), srvEntity.getBldSrvId() );
			lotViewBeanList.add( viewBean );
		}

		String selectedEnvNo = paramBean.getSelectedEnvNo();

		lotViewBeanList =
			getLotViewBeanPjtLotEntity( selectedEnvNo, lotDtoEntities, lotViewBeanList );

		if ( TriStringUtils.isEmpty( lotViewBeanList )) {
			throw new BusinessException( RmMessageId.RM001036E );
		}

		lotSelectBean.setLotViewBeanList	( lotViewBeanList );

		paramBean.setLotSelectBean( lotSelectBean );
	}

	/**
	 * リリース対象ロットの検索を行う（リリース申請からリリースする場合）
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchLot( FlowRelCtlEntryServiceBean paramBean ) {

		LotSelectBean lotSelectBean = paramBean.getLotSelectBean();

		ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( paramBean.getSelectedLotNo() );

		if ( null == lotEntity ) {
			throw new BusinessException( BmMessageId.BM001000E );
		}
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

		List<ILotDto> lotDtoList = new ArrayList<ILotDto>();
		ILotDto dto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );
		lotDtoList.add(dto);

		List<IGrpUserLnkEntity> grpUserLnkEntityList = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
		List<LotViewBean> lotViewBeanList = new ArrayList<LotViewBean>();

		RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity(
				lotViewBeanList, lotDtoList, grpUserLnkEntityList ,srvEntity.getBldSrvId() );

		if ( TriStringUtils.isEmpty( lotViewBeanList )) {
			throw new BusinessException( RmMessageId.RM001034E );
		}

		lotSelectBean.setLotViewBeanList	( lotViewBeanList );

		paramBean.setLotSelectBean( lotSelectBean );
	}

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * <br>対象のリリース環境にアクセス可能なロット情報の絞り込みを行う。
	 *
	 * @param viewBean ロット一覧画面の表示項目
	 * @param entity ロットエンティティ
	 */
	private static List<LotViewBean> getLotViewBeanPjtLotEntity(
			String selectedEnvNo,
			List<ILotDto> lotAllEntityArray,
			List<LotViewBean> lotViewBeanList ) {

		if ( TriStringUtils.isEmpty(selectedEnvNo) ) {
			return lotViewBeanList;
		}

		try {
			List<LotViewBean> outLotViewBeanList = new ArrayList<LotViewBean>();

			Map<String, ILotDto> lotMap = ViewInfoAddonUtil.convertToMap(lotAllEntityArray);

			for( LotViewBean item : lotViewBeanList ) {
				ILotDto lotDto = lotMap.get(item.getLotNo());

				if ( TriStringUtils.isEmpty( lotDto.getLotRelEnvLnkEntities()) ) {
					//ロットにリリース環境が未指定ならば、旧データ互換として絞り込まない
					outLotViewBeanList.add( item );
				} else {
					for ( ILotRelEnvLnkEntity env: lotDto.getLotRelEnvLnkEntities() ) {
						if ( selectedEnvNo.equals( env.getBldEnvId() ) ) {
							outLotViewBeanList.add( item );
							break;
						}
					}
				}
			}

			return outLotViewBeanList;
		} finally {
		}
	}

}
