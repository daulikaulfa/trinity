package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;

/**
 * リリースパッケージ・ビルドパッケージ用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** パッケージ番号 */
	private String buildNo = null;
	/** パッケージ名 */
	private String buildName = null;
	/** パッケージ概要 */
	private String buildSummary = null;
	/** パッケージ件名 */
	private String buildContent = null;
	/** マージ順 */
	private String mergeOrder = "1";


	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	public String getMergeOrder() {
		return mergeOrder;
	}
	public void setMergeOrder( String mergeOrder ) {

		// マージ順指定機能は現時点では封印されている。
		// しかし、本項目はDB上NOT NULL制約であるため、デフォルト値を設定しておく必要がある。
		this.mergeOrder = TriObjectUtils.defaultIfNull(mergeOrder, "1");
	}
	public String getBuildContent() {
		return buildContent;
	}
	public void setBuildContent(String buildContent) {
		this.buildContent = buildContent;
	}
	public String getBuildName() {
		return buildName;
	}
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}
	public String getBuildSummary() {
		return buildSummary;
	}
	public void setBuildSummary(String buildSummary) {
		this.buildSummary = buildSummary;
	}
}
