package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlUnitDetailViewServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlUnitDetailViewServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリースパッケージ・ビルドパッケージ詳細画面(ポップアップ)の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlUnitDetailViewService implements IDomain<FlowRelCtlUnitDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlUnitDetailViewServiceBean> execute( IServiceDto<FlowRelCtlUnitDetailViewServiceBean> serviceDto ) {

		FlowRelCtlUnitDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( RelCtlScreenID.UNIT_DETAIL_VIEW ) &&
					!forward.equals( RelCtlScreenID.UNIT_DETAIL_VIEW )) {
				return serviceDto;
			}

			if ( forward.equals(RelCtlScreenID.UNIT_DETAIL_VIEW) ) {

				String selectedBuildNo = paramBean.getSelectedBuildNo();
				ItemCheckAddonUtils.checkBuildNo( selectedBuildNo );
				String[] selectedRpIds = getRpIdFromSelecedBpId( selectedBuildNo );
				IJdbcCondition condition =
					DBSearchConditionAddonUtil.getRpConditionByRpIds( selectedRpIds  );
				ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelUnitDetailView();

				List<IRpEntity> rpEntities =
					this.support.getRpDao().find( condition.getCondition(), sort );

				List<ReleaseViewBean> releaseViewBeanList =
												new ArrayList<ReleaseViewBean>();

				for ( IRpEntity entity : rpEntities ) {

					ReleaseViewBean viewBean = paramBean.newReleaseViewBean();

					viewBean.setRelNo		( entity.getRpId() );
					viewBean.setEnvName		( getEnvName( entity.getBldEnvId() ));
					viewBean.setRelInputDate( TriDateUtils.convertViewDateFormat( entity.getRelTimestamp() ) );

					releaseViewBeanList.add( viewBean );
				}

				paramBean.setReleaseViewBeanList	( releaseViewBeanList );

				ILotEntity pjtLotEntity = this.getLotEntity( selectedBuildNo ) ;
				paramBean.setLotNo			( pjtLotEntity.getLotId() ) ;
				paramBean.setLotName			( pjtLotEntity.getLotNm() ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005071S, e);
		}
	}

	/**
	 * 環境名を取得する。
	 * @param envNo 環境番号
	 * @return 環境名
	 */
	private String getEnvName( String envNo ) {

		IBldEnvEntity entity = this.support.getBmFinderSupport().findBldEnvEntity( envNo );

		return entity.getBldEnvNm();
	}

	/**
	 * ロット情報を取得する。
	 * @param buildNo ビルドパッケージ番号
	 * @return ロット情報
	 */
	private ILotEntity getLotEntity( String buildNo ) {

		IBpEntity buildEntity	= this.support.getBmFinderSupport().findBpEntity( buildNo );
		ILotEntity lotEntity		= this.support.getAmFinderSupport().findLotEntity( buildEntity.getLotId() );

		return lotEntity ;
	}

	private String[] getRpIdFromSelecedBpId( String selectedBuildNo ){

		List<IRpBpLnkEntity> lnkEntityList = this.support.findRpBpLnkEntitiesFromBpId(selectedBuildNo);
		List<String> selectedRpId = new ArrayList<String>();
		for ( IRpBpLnkEntity lnkEntity : lnkEntityList ){
			selectedRpId.add(lnkEntity.getRpId());
		}
		return selectedRpId.toArray( new String[0] );
	}
}
