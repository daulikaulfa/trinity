package jp.co.blueship.tri.rm.domain.rp;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・リリース状況画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlEntryService implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support;
	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> pojoList = null;

	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelCtlScreenID.GENERATE_DETAIL_VIEW ) &&
					!forwordID.equals( RelCtlScreenID.COMP_REQUEST ) ){
				return serviceDto;
			}

			if ( refererID.equals( RelCtlScreenID.GENERATE_DETAIL_VIEW )) {
			}

			if ( forwordID.equals( RelCtlScreenID.COMP_REQUEST )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					paramBean.setRelNo( this.support.getRpNumberingDao().nextval() );

					// RMI対策
					paramBean.setRelNo( paramBean.getRelNo() );

					// グループの存在チェック
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					//下層のアクションを実行する
					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
					innerServiceDto.setServiceBean( paramBean );

					{
						for ( IDomain<IGeneralServiceBean> pojo : this.pojoList ) {
							innerServiceDto = pojo.execute( innerServiceDto );
						}
					}
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005062S, e);
		}
	}
}
