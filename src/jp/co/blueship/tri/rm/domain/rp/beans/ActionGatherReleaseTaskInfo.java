package jp.co.blueship.tri.rm.domain.rp.beans;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.agent.rm.svc.flow.beans.constants.RelTaskDefineId;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.ReleaseTaskBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskPropertyEntity;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.rm.RmBusinessJudgUtils;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * ＲＣのタスクに必要な情報収集を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 *
 */
public class ActionGatherReleaseTaskInfo extends ActionPojoAbstract<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support = null;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		try {

			//FlowRelCtlEntryServiceBean paramBean = (FlowRelCtlEntryServiceBean)RmExtractEntityAddonUtils.getGenericServiceBean( paramList );
			FlowRelCtlEntryServiceBean paramBean = serviceDto.getServiceBean();
			if ( null == paramBean ) {
				throw new TriSystemException( RmMessageId.RM005010S );
			}

			List<Object> paramList = serviceDto.getParamList();

			ReleaseTaskBean bean = (ReleaseTaskBean)RmExtractEntityAddonUtils.extractReleaseTask( paramList );
			if ( null == bean ) {
				throw new TriSystemException( RmMessageId.RM005005S );
			}

			ILotEntity pjtLotEntity = RmExtractEntityAddonUtils.extractPjtLot( paramList );
			if ( null == pjtLotEntity ) {
				throw new TriSystemException( RmMessageId.RM005011S );
			}
			IRpDto rpDto = RmExtractEntityAddonUtils.extractRpDto( paramList );
			if ( null == rpDto ) {
				throw new TriSystemException( RmMessageId.RM005012S );
			}
			IRaEntity relApplyEntity = RmExtractEntityAddonUtils.extractRelApply( paramList );
			if ( null == relApplyEntity ) {
				// リリース申請を経由せずにリリース作成するケースがあるため、エラーにはしない
			}
			BpDtoList bpDtoList = RmExtractEntityAddonUtils.extractBpDtoList( paramList );
			if ( null == bpDtoList ) {
				throw new TriSystemException( RmMessageId.RM005013S );
			}
			ItemCheckAddonUtils.checkBuildNo( RmEntityAddonUtils.getBuildNoFromBp( getBpEntity( bpDtoList ) ) );
			List<IAreqDto> assetApplyEntities = RmExtractEntityAddonUtils.extractAreqDtoList( paramList );
			if ( null == assetApplyEntities ) {
				throw new TriSystemException( RmMessageId.RM005014S );
			}

			List<ITaskPropertyEntity> propertys = new ArrayList<ITaskPropertyEntity>();

			bean.setLotNo				( rpDto.getRpEntity().getLotId() ) ;
			bean.setRelNo				( rpDto.getRpEntity().getRpId() );
			bean.setInsertUpdateUser	( paramBean.getUserName() );
			bean.setInsertUpdateUserId	( paramBean.getUserId() );
			bean.setProcId				( paramBean.getProcId() );

			// リリース情報
			setRelInfo			( propertys, rpDto, pjtLotEntity, bpDtoList );

			// リリース申請情報
			setRelApplyInfo		( propertys, relApplyEntity );

			// ロット情報
			setLotInfo			( propertys, rpDto.getRpEntity(), pjtLotEntity );

			// 申請情報
			setAssetApplyInfo	( propertys, rpDto.getRpEntity(), bpDtoList, assetApplyEntities );

			// ホームパス
			{
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.homePath.toString() );
				propertyEntity.setValue	( sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath )) ;

				propertys.add( propertyEntity );
			}

			bean.setPropertys( propertys );
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005073S, e );
		}
	}
	private IBpEntity[] getBpEntity(BpDtoList dtoList){
		List<IBpEntity> entityList = new ArrayList<IBpEntity>();

		for ( IBpDto dto : dtoList ) {
			entityList.add( dto.getBpEntity() );
		}
		return entityList.toArray( new IBpEntity[0]);
	}
	/**
	 * タスクの引数にリリース情報を設定する。
	 * @param propertys タスクの引数
	 * @param entity リリース情報
	 */
	private void setRelInfo(
			List<ITaskPropertyEntity> propertys, IRpDto entity, ILotEntity pjtLotEntity, BpDtoList buildEntities ) {

		// リリース番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.relNo.toString() );
			propertyEntity.setValue	( entity.getRpEntity().getRpId() );

			propertys.add( propertyEntity );
		}

		// リリース環境番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.envNo.toString() );
			propertyEntity.setValue	( entity.getRpEntity().getBldEnvId() );

			propertys.add( propertyEntity );
		}

		// ＲＣ生成物出力先ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.historyRelDSLPath.toString() );
			propertyEntity.setValue	( RmDesignBusinessRuleUtils.getHistoryRelDSLPath( pjtLotEntity, entity.getRpEntity() ).getPath() );

			propertys.add( propertyEntity );
		}



		// リリースパッケージ関連
		// 念のためソートしなおし
		sortMergeOrder( entity.getRpBpLnkEntities() );

		List<String> mergeOrderList	= new ArrayList<String>();
		List<String> buildNoList	= new ArrayList<String>();

		for ( IRpBpLnkEntity build : entity.getRpBpLnkEntities() ) {
			mergeOrderList.add	( this.support.getMergeOdr( build.getRpId() , build.getBpId() ) );
			buildNoList.add		( build.getBpId() );
		}

		// ＲＰのマージ順
		{
			if ( ! TriStringUtils.isEmpty( mergeOrderList )) {

				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.mergeOrder.toString() );
				propertyEntity.setValue	( convertValue( mergeOrderList.toArray( new String[0] ) ));

				propertys.add( propertyEntity );
			}
		}

		// ＲＰ番号
		{
			if ( ! TriStringUtils.isEmpty( buildNoList )) {

				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.buildNo.toString() );
				propertyEntity.setValue	( convertValue( buildNoList.toArray( new String[0] ) ));

				propertys.add( propertyEntity );
			}
		}

		// ＲＰ生成物出力先ディレクトリ
		{
			if ( ! TriStringUtils.isEmpty( buildNoList )) {

				List<String> dslPathList = new ArrayList<String>();

				Map<String, IBpEntity> buildNoMap = new HashMap<String, IBpEntity>();
				for ( IBpDto buildEntity : buildEntities ) {
					buildNoMap.put( buildEntity.getBpEntity().getBpId(), buildEntity.getBpEntity() );
				}
				for ( String buildNo : buildNoList ) {

					IBpEntity buildEntity = buildNoMap.get( buildNo );

					dslPathList.add( RmDesignBusinessRuleUtils.getHistoryRelUnitDSLPath( pjtLotEntity, buildEntity ).getPath() );
				}


				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.historyRelUnitDSLPath.toString() );
				propertyEntity.setValue	( convertValue( dslPathList.toArray( new String[0] ) ));

				propertys.add( propertyEntity );

			}
		}
	}

	/**
	 * タスクの引数にリリース申請情報を設定する。
	 * <code>relApplyEntity</code>が<code>null</code>の場合は、リリース申請していないことを想定し、
	 * 一律空文字を設定する
	 * @param propertys タスクの引数
	 * @param entity リリース申請情報
	 */
	private void setRelApplyInfo(
			List<ITaskPropertyEntity> propertys, IRaEntity relApplyEntity ) {

		// リリース申請番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.relApplyNo.toString() );
			propertyEntity.setValue	( !TriStringUtils.isEmpty(relApplyEntity) ? relApplyEntity.getRaId() : "" );


			propertys.add( propertyEntity );
		}

		// リリース申請時の添付ファイルのパス（idの1個上の階層のフォルダまで）
		{
			String uniquePath = "";

			if( null != relApplyEntity ) {

				String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

				String relApplyNo = relApplyEntity.getRaId();
				String relApplyNoPath = TriStringUtils.linkPathBySlash( basePath , relApplyNo );

				String uniqueKey = "";
				uniquePath =  TriStringUtils.linkPathBySlash( relApplyNoPath , uniqueKey ) ;
			}

			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.relApplyAppendFileRootPath.toString() );
			propertyEntity.setValue	( !TriStringUtils.isEmpty(uniquePath) ? uniquePath : "" );

			propertys.add( propertyEntity );
		}

	}

	/**
	 * タスクの引数にロット情報を設定する。
	 * @param propertys タスクの引数
	 * @param entity リリース情報
	 */
	private void setLotInfo(
			List<ITaskPropertyEntity> propertys, IRpEntity entity, ILotEntity pjtLotEntity ) {

		// ロット番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.lotNo.toString() );
			propertyEntity.setValue	( pjtLotEntity.getLotId() );

			propertys.add( propertyEntity );
		}

		// ロットの原本ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.workPath.toString() );
			propertyEntity.setValue	( pjtLotEntity.getLotMwPath() );

			propertys.add( propertyEntity );
		}

		// ロットの作業ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.workspace.toString() );
			propertyEntity.setValue	( pjtLotEntity.getWsPath() );

			propertys.add( propertyEntity );
		}

		// ロットの履歴格納ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.historyPath.toString() );
			propertyEntity.setValue	( pjtLotEntity.getHistPath() );

			propertys.add( propertyEntity );
		}

		// ロットの内部返却ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.systemInBox.toString() );
			propertyEntity.setValue	( pjtLotEntity.getSysInBox() );

			propertys.add( propertyEntity );
		}
	}

	/**
	 * タスクの引数に申請情報を設定する。
	 * @param propertys タスクの引数
	 * @param lotId ロット番号
	 * @param entity リリース情報
	 */
	private void setAssetApplyInfo(
			List<ITaskPropertyEntity> propertys, IRpEntity entity, List<IBpDto> bpDtoList, List<IAreqDto>  assetApplyEntities ) {

		Set<String> moduleNameSet	= new TreeSet<String>();
		Set<String> buildTagSet		= new TreeSet<String>();

		for ( IBpDto bpDto : bpDtoList ){

			for ( IBpMdlLnkEntity moduleEntity : bpDto.getBpMdlLnkEntities() ) {
				moduleNameSet.add( moduleEntity.getMdlNm() );
			}

			if ( TriStringUtils.isNotEmpty( bpDto.getBpEntity().getBpCloseVerTag() ) ){
				buildTagSet.add( bpDto.getBpEntity().getBpCloseVerTag() );
			}
		}

		// 返却と削除に分ける
		List<IAreqDto> delAssetList = new ArrayList<IAreqDto>();

		List<String> rtnAssetNoList = new ArrayList<String>();
		List<String> delAssetNoList = new ArrayList<String>();

		for ( IAreqDto applyDto : assetApplyEntities ) {
			IAreqEntity applyEntity = applyDto.getAreqEntity();
			if ( RmBusinessJudgUtils.isDeleteApply( applyEntity ) ) {
				delAssetList.add	( applyDto );
				delAssetNoList.add	( applyEntity.getAreqId() );
			} else {
				rtnAssetNoList.add	( applyEntity.getAreqId() );
			}
		}


		// パス情報：パスが重複しているかもしれないので、念のためTreeSetにしておく
		Set<String> masterPathSet		= new TreeSet<String>();
		Set<String> masterBinaryPathSet	= new TreeSet<String>();

		for ( IAreqDto delApplyEntity : delAssetList ) {

			if ( null != delApplyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {

				for ( IAreqFileEntity assetFileEntity : delApplyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {

					String masterPath = assetFileEntity.getFilePath();
					masterPathSet.add( masterPath );
				}
			}

			if ( null != delApplyEntity.getAreqBinaryFileEntities(AreqCtgCd.RemovalRequest) ) {

				for ( IAreqBinaryFileEntity assetFileEntity: delApplyEntity.getAreqBinaryFileEntities(AreqCtgCd.RemovalRequest) ) {

					String masterPath = assetFileEntity.getFilePath();
					masterBinaryPathSet.add( masterPath );
				}
			}
		}


		// 原本削除のパス
		if( ! TriStringUtils.isEmpty( masterPathSet ) ) {
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.masterDeletePath.toString() );
			propertyEntity.setValue	( convertValue( (String[])masterPathSet.toArray( new String[0] ) ));

			propertys.add( propertyEntity );
		}

		// モジュール削除のパス
		if( ! TriStringUtils.isEmpty( masterBinaryPathSet ) ) {
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.masterBinaryDeletePath.toString() );
			propertyEntity.setValue	( convertValue( (String[])masterBinaryPathSet.toArray( new String[0] ) ));

			propertys.add( propertyEntity );
		}

		// 返却申請番号
		if( !TriStringUtils.isEmpty( rtnAssetNoList ) ) {

			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.applyNo.toString() );
			propertyEntity.setValue	( convertValue( (String[])rtnAssetNoList.toArray( new String[0] ) ));

			propertys.add( propertyEntity );
		}

		// 削除申請番号
		if( !TriStringUtils.isEmpty( delAssetNoList ) ) {

			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( RelTaskDefineId.delApplyNo.toString() );
			propertyEntity.setValue	( convertValue( (String[])delAssetNoList.toArray( new String[0] ) ));

			propertys.add( propertyEntity );
		}

		// 変更のあったモジュール名
		{
			if( !TriStringUtils.isEmpty( moduleNameSet ) ) {
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.module.toString() );
				propertyEntity.setValue	( convertValue( (String[])moduleNameSet.toArray( new String[0] ) ));

				propertys.add( propertyEntity );
			}
		}

		// ビルドタグ
		{
			if( !TriStringUtils.isEmpty( buildTagSet ) ) {
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( RelTaskDefineId.buildTag.toString() );
				propertyEntity.setValue	( convertValue( (String[])buildTagSet.toArray( new String[0] ) ));

				propertys.add( propertyEntity );
			}
		}
	}

	/**
	 * RpBpLnkEntityをマージ順の昇順にソートします
	 * @param buildArray
	 */
	private static void sortMergeOrder( List<IRpBpLnkEntity> buildArray ) {

		Collections.sort( buildArray,
				new Comparator<IRpBpLnkEntity>() {
					public int compare( IRpBpLnkEntity obj1, IRpBpLnkEntity obj2 ) {
						return obj1.getMergeOdr().compareTo( obj2.getMergeOdr() );
					}
				} );
	}
	/**
	 * 指定された文字列配列をカンマ(,)編集した単一文字列に変換します。
	 *
	 * @param values
	 * @return 編集した単一文字列を戻します。<code>values</code>に何もない場合は、空文字を戻します。
	 */
	private final String convertValue( String[] values ) {

		if ( TriStringUtils.isEmpty( values ) ) {
			return "";
		}

		String ret = TriStringUtils.convertArrayToString( values ) ;

		return ret ;
	}


}
