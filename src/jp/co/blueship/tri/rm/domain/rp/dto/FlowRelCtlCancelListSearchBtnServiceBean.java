package jp.co.blueship.tri.rm.domain.rp.dto;

import jp.co.blueship.tri.rm.domain.rp.beans.dto.SearchConditionBean;

/**
 * 一覧画面から検索を行うフロー
 *
 */
public class FlowRelCtlCancelListSearchBtnServiceBean extends FlowRelCtlCancelListServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 検索条件 */
	private SearchConditionBean releaseSearchConditionBean = null;
	
	
	public SearchConditionBean getReleaseSearchConditionBean() {
		return releaseSearchConditionBean;
	}
	public void setReleaseSearchConditionBean( SearchConditionBean releaseSearchConditionBean ) {
		this.releaseSearchConditionBean = releaseSearchConditionBean;
	}
}

