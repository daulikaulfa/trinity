package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;
/**
 * リリースパッケージ・リリース履歴一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlHistoryListServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 環境番号 */
	private String selectedEnvNo = null;
	/** リリースパッケージ情報 */
	private List<CtlListViewBean> releaseViewBeanList = null;
	/** 検索条件情報 */
	private RelCtlSearchBean relCtlSearchBean = null;
	/** 環境名 */
	private String envName = null ;
	
	
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}
	
	public List<CtlListViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<CtlListViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<CtlListViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}
	public RelCtlSearchBean getRelCtlSearchBean() {
		return relCtlSearchBean;
	}
	public void setRelCtlSearchBean(RelCtlSearchBean relCtlSearchBean) {
		this.relCtlSearchBean = relCtlSearchBean;
	}
	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	
}
