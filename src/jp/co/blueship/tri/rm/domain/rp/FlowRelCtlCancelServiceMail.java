package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.rm.beans.mail.dto.RelCtlMailServiceBean;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリース取消受付完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlCancelServiceMail implements IDomain<FlowRelCtlCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelCtlEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IServiceDto<FlowRelCtlCancelServiceBean> execute( IServiceDto<FlowRelCtlCancelServiceBean> serviceDto ) {

		FlowRelCtlCancelServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelCtlScreenID.COMP_CANCEL ) &&
					!forwordID.equals( RelCtlScreenID.COMP_CANCEL )) {
				return serviceDto;
			}

			if ( refererID.equals( RelCtlScreenID.COMP_CANCEL )) {

			}

			if ( forwordID.equals( RelCtlScreenID.COMP_CANCEL )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					//基礎情報のコピー
					RelCtlMailServiceBean successMailBean = new RelCtlMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					//選択されたリリース番号
					String[] relNoList = paramBean.getSelectedRelNo();
					RpCondition rpCondition = new RpCondition();
					rpCondition.setRpId( relNoList[0] );
					rpCondition.setDelStsId( null );
					IRpEntity rpEntity = support.getRpDao().findByPrimaryKey( rpCondition.getCondition() );

					//環境
					IBldEnvEntity envEntity = new BldEnvEntity();
					String envNo 	= rpEntity.getBldEnvId();
					String envName 	= this.support.getBmFinderSupport().findBldEnvEntity( envNo ).getBldEnvNm();
					envEntity.setBldEnvId	( envNo );
					envEntity.setBldEnvNm( envName );

					//ロット番号
					ILotEntity pjtLotEntity = new LotEntity();
					String lotId 	= rpEntity.getLotId();
					String lotName 	= this.support.getAmFinderSupport().findLotEntity(lotId).getLotNm();

					pjtLotEntity.setLotId	( lotId );
					pjtLotEntity.setLotNm	( lotName );

					//ビルドパッケージ番号リスト
					List<Map<String,Object>> releaseList = new ArrayList<Map<String,Object>>();
					for ( String rpId : relNoList ) {
						Map<String,Object> releaseMap =new HashMap<String,Object>();

						RpBpLnkCondition condition = new RpBpLnkCondition();
						condition.setRpId(rpId);
						condition.setDelStsId( null );
						List<IRpBpLnkEntity> rpBpLnkEntities = this.support.getRpBpLnkDao().find(condition.getCondition());

						List<String> packageNoList = new ArrayList<String>();
						for ( IRpBpLnkEntity rpBpLnkEntity : rpBpLnkEntities ) {
							packageNoList.add( rpBpLnkEntity.getBpId() );
						}
						releaseMap.put( "rpId", rpId );
						releaseMap.put( "bpIdList",packageNoList );

						releaseList.add(releaseMap);
					}

					//rpEntity.setContent( paramBean.getReleaseInfoInputBean().getCancelComment() );
					successMailBean.setRelEnvEntity	( envEntity );
					successMailBean.setRelEntity(rpEntity);
					successMailBean.setPjtLotEntity	( pjtLotEntity );
					successMailBean.setReleaseList	( releaseList );

					successMail.execute( mailServiceDto );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( RmMessageId.RM005031S, e , paramBean.getFlowAction()) ) ;
		}

		return serviceDto;
	}


}
