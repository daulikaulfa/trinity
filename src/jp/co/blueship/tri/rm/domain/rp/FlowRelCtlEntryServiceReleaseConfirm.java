package jp.co.blueship.tri.rm.domain.rp;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.RmBusinessJudgUtils;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.EntryConfirmBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリースパッケージ・リリース確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlEntryServiceReleaseConfirm implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}
	private ActionStatusMatrixList statusMatrixAction = null;
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( !refererID.equals( RelCtlScreenID.ENTRY_CONFIRM ) &&
					!forwordID.equals( RelCtlScreenID.ENTRY_CONFIRM ) ){
				return serviceDto;
			}

			// 初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;


			String[] selectedBpIds = RmEntityAddonUtils.getBuildNo( paramBean.getSelectedUnitBean() );

			if( refererID.equals( RelCtlScreenID.ENTRY_CONFIRM )) {

				if ( ScreenType.next.equals( paramBean.getScreenType() ) ) {
					if( paramBean.isStatusMatrixV3() ) {
						List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto( selectedBpIds );

						List<IRaBpLnkEntity> raBpLnkEntities = support.findRaBpLnkEntitiesFromBpId( selectedBpIds );
						String[] raIds = FluentList.from(raBpLnkEntities).map( RmFluentFunctionUtils.toRaIdFromRaBpLnk ).toArray(new String[0]);

						List<IRpEntity> rpEntityList = this.support.getRpEntityByRelEnv( paramBean.getSelectedEnvNo() );
						List<IRpDto> rpDtoList = this.support.findRpDto(rpEntityList);

						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setAreqIds( this.support.getAreqIds( bpDtoList ) )
						.setBpIds( selectedBpIds )
						.setRaIds( raIds )
						.setRpIds( this.support.getRpIds( rpDtoList ) );

						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if( forwordID.equals( RelCtlScreenID.ENTRY_CONFIRM )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					ItemCheckAddonUtils.checkBuildNo	( selectedBpIds );
				}

				if ( ! TriStringUtils.isEmpty( selectedBpIds ) ) {

					ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( paramBean.getSelectedLotNo() );
					List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto( selectedBpIds );
					// INC-0911049791
					checkBuildResource		( lotEntity, bpDtoList );
					// 資産の重複チェック（警告）
					if ( 1 < bpDtoList.size() ) {
						checkAssetDuplication	( lotEntity, bpDtoList , paramBean );
					}


					EntryConfirmBean releaseComfirmBean = paramBean.getReleaseConfirmBean();

					// 基本情報
					setBaseInfoInputReleaseConfirm(
							paramBean.getBaseInfoBean().getBaseInfoInputBean(), releaseComfirmBean );

					// 環境情報
					String envNo = paramBean.getSelectedEnvNo();
					setEnvReleaseConfirm( envNo, releaseComfirmBean );

					// ロット情報
					String lotId = paramBean.getSelectedLotNo();
					setLotReleaseConfirm( lotId, releaseComfirmBean );

					// ビルドパッケージ情報
					setBuildReleaseConfirm(
							selectedBpIds, paramBean.getSelectedUnitBean(), releaseComfirmBean );

					paramBean.setReleaseConfirmBean( releaseComfirmBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005066S, e);
		}
	}

	/**
	 * 基本情報入力ビーンからリリース確認ビーンに値をセットする。
	 *
	 */
	private void setBaseInfoInputReleaseConfirm(
			BaseInfoInputBean baseInputBean, EntryConfirmBean releaseComfirmBean ) {

		releaseComfirmBean.setRelSummary	( baseInputBean.getRelSummary() );
		releaseComfirmBean.setRelContent	( baseInputBean.getRelContent() );
	}

	/**
	 * ロットの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setLotReleaseConfirm(
			String lotId, EntryConfirmBean releaseComfirmBean ) {

		ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( lotId );

		LotViewBean lotViewBean = new LotViewBean();
		RmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( lotViewBean, pjtLotEntity, this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

		releaseComfirmBean.setLotViewBean( lotViewBean );
	}

	/**
	 * ロットの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setEnvReleaseConfirm(
			String envNo, EntryConfirmBean releaseComfirmBean ) {

		IBldEnvEntity envEntity = this.support.getBmFinderSupport().findBldEnvEntity( envNo );

		ConfigurationViewBean confViewBean = new ConfigurationViewBean();
		RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( confViewBean, envEntity );

		releaseComfirmBean.setConfViewBean( confViewBean );
	}

	/**
	 * ビルドパッケージの情報をリリース確認ビーンに値をセットする。
	 *
	 */
	private void setBuildReleaseConfirm(
			String[] selectedBuildNo, UnitBean[] unitArray, EntryConfirmBean releaseComfirmBean ) {

		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getBuildConditionByBuildNo( selectedBuildNo );
		ISqlSort sort			= (ISqlSort)DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelEntryConfirm();

//		int selectedPageNo	=
//			(( 0 == releaseComfirmBean.getSelectPageNo() )? 1: releaseComfirmBean.getSelectPageNo() );
//		int maxPageNumber	=
//			DesignProjectUtil.getMaxPageNumber(
//					DesignProjectRelDefineId.maxPageNumberByRelList );

		int selectedPageNo	= 1;
		int maxPageNumber	= 0;

		IEntityLimit<IBpEntity> limit =
			this.support.getBmFinderSupport().getBpDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );


		Map<String, String> mergeOrderMap = new HashMap<String, String>();
		for ( UnitBean unitBean : unitArray ) {
			mergeOrderMap.put( unitBean.getBuildNo(), unitBean.getMergeOrder() );
		}

		releaseComfirmBean.setUnitViewBeanList	(
				this.support.getUnitViewBean( limit.getEntities().toArray(new IBpEntity[0]), mergeOrderMap ));
//		releaseComfirmBean.setPageInfoView		(
//				AddonUtil.convertPageNoInfo( new PageNoInfo(), buildEntityLimit.getLimit() ));
//		releaseComfirmBean.setSelectPageNo		( selectedPageNo );
	}

	/**
	 * ビルドパッケージの資産が存在するか、ディレクトリの存在チェックを行う。
	 *
	 */

	private void checkBuildResource( ILotEntity lotEntity, List<IBpDto> buildEntities ) throws Exception {

		FileFilter filter = BusinessFileUtils.getFileFilter();

		for ( IBpDto buildEntity : buildEntities) {

			File relUnitDSLPath = RmDesignBusinessRuleUtils.getHistoryRelUnitDSLPath(lotEntity, buildEntity.getBpEntity());
			// ディレクトリが存在
			if (relUnitDSLPath != null && relUnitDSLPath.isDirectory()) {
				// ディレクトリにファイルが存在するかチェック
				if( false == TriFileUtils.checkExistFiles( relUnitDSLPath.getPath() , false , filter ) ) {
					throw new ContinuableBusinessException( RmMessageId.RM001025E, new String[]{ buildEntity.getBpEntity().getBpId() , relUnitDSLPath.getPath() } );
				}
			} else {
				throw new ContinuableBusinessException( RmMessageId.RM001025E, new String[]{ buildEntity.getBpEntity().getBpId() , (null == relUnitDSLPath)? "": relUnitDSLPath.getPath() } );
			}
		}
	}

	/**
	 * 資産の重複チェックを行う。以下の条件を満たす場合、警告する。
	 * <p>二重貸出が許可
	 * <p>マージされるビルドパッケージ間で資産が重複
	 * <p>重複した資産の内容が異なる
	 * <p>重複した資産のいずれかのステータスが"承認済"（両方クローズ済はセーフ）
	 *
	 */

	private void checkAssetDuplication(
			ILotEntity lotEntity, List<IBpDto> buildEntities, FlowRelCtlEntryServiceBean paramBean ) {

		// ２重貸出チェック実施（重複貸出が許可されている場合）
		if ( lotEntity.getIsAssetDup().parseBoolean() ) {

			// 選択されたビルドパッケージに含まれている申請番号と、それを含むビルド番号リスト
			Map<String, List<String>> assetApplyNoBuildNoListMap =
				getAssetApplyNoBuildNoListMap( buildEntities );

			// 選択されたビルドパッケージに含まれている申請情報
			IAreqEntity[] applyEntities	=
				this.support.getAssetApplyEntity( buildEntities );

			List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
			for ( IAreqEntity areqEntity: applyEntities ) {
				IAreqDto areqDto = new AreqDto();
				areqDto.setAreqEntity( areqEntity );

				AreqFileCondition fileCondition = new AreqFileCondition();
				fileCondition.setAreqId( areqEntity.getAreqId() );

				areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
				areqDtoEntities.add( areqDto );
			}

			// 資産パスと申請情報リストのマップ
			Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap =
				getReturnAssetPathAsseApplyEntityMap( areqDtoEntities );

			// 選択されたビルドパッケージ内のチェック
			// 承認の資産同士の重複はエラー
			// 承認とクローズの資産同士の重複は、時間の関係をチェックしたうえでエラー
			checkAssetDuplicate(
					lotEntity, assetPathAssetApplyEntityListMap, assetApplyNoBuildNoListMap, paramBean );

		}
	}


	/**
	 * ビルドパッケージエンティティから、ビルドパッケージエンティティを構成する申請情報番号と
	 * ビルドパッケージ番号リストのマップを作成する。
	 * @param buildEntities ビルドパッケージエンティティ
	 * @return 申請情報番号とビルドパッケージ番号リストのマップ
	 */
	private Map<String, List<String>> getAssetApplyNoBuildNoListMap( List<IBpDto> buildEntities ) {

		Map<String, List<String>> assetApplyNoBuildNoListMap = new HashMap<String, List<String>>();

		for ( IBpDto entity : buildEntities ) {

			for ( IBpAreqLnkEntity buildApplyEntity : entity.getBpAreqLnkEntities() ) {

				String applyNo = buildApplyEntity.getAreqId();

				if ( !assetApplyNoBuildNoListMap.containsKey( applyNo )) {
					assetApplyNoBuildNoListMap.put( applyNo, new ArrayList<String>() );
				}

				assetApplyNoBuildNoListMap.get( applyNo ).add( entity.getBpEntity().getBpId() );
			}
		}

		return assetApplyNoBuildNoListMap;
	}


	/**
	 * 資産とその資産を含む申請情報リストのマップを取得する。
	 * @param entities 申請情報配列
	 * @return 資産とその資産を含む申請情報リストのマップ
	 */
	private Map<String,List<IAreqEntity>>
		getReturnAssetPathAsseApplyEntityMap( List<IAreqDto> entities ) {

		Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap =
										new HashMap<String,List<IAreqEntity>>();

		for ( IAreqDto entity : entities ) {

			if ( !RmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() ) ) continue;

			for ( IAreqFileEntity fileEntity : entity.getAreqFileEntities(AreqCtgCd.ReturningRequest) ) {

				String path = fileEntity.getFilePath();

				if ( !assetPathAssetApplyEntityListMap.containsKey( path )) {
					assetPathAssetApplyEntityListMap.put( path, new ArrayList<IAreqEntity>() );
				}

				assetPathAssetApplyEntityListMap.get( path ).add( entity.getAreqEntity() );
			}
		}

		return assetPathAssetApplyEntityListMap;
	}

	/**
	 * ビルドパッケージに含まれるの資産同士が重複していないかをチェックする
	 * @param lotEntity ロットエンティティ
	 * @param assetPathAssetApplyEntityListMap 資産パスと申請情報リストのマップ
	 * @param assetApplyNoBuildNoListMap 申請番号とそれを含むビルドパッケージ番号のマップ
	 * @param paramBean FlowRelCtlEntryServiceBean
	 */

	private void checkAssetDuplicate(
			ILotEntity lotEntity,
			Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap,
			Map<String, List<String>> assetApplyNoBuildNoListMap,
			FlowRelCtlEntryServiceBean paramBean ) {

		List<IMessageId> messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( Map.Entry<String,List<IAreqEntity>> entry : assetPathAssetApplyEntityListMap.entrySet() ) {

				String assetPath = entry.getKey();
				List<IAreqEntity> checkEntityList = entry.getValue();

				// 重複あり
				if ( 1 >= checkEntityList.size() ) {
					continue;
				}
				// 最初の承認ステータスを比較元に
				IAreqEntity origEntity = null;
				for ( IAreqEntity entity : checkEntityList ) {

					if ( AmAreqStatusId.CheckinRequestApproved.equals( entity.getStsId() )) {
						origEntity = entity;
						break;
					}
				}

				// この重複パスには全部クローズ済み
				if ( null == origEntity ) continue;

				// 残りを比較先としてまわす
				for ( IAreqEntity destEntity : checkEntityList ) {

					// 比較元は除外する
					if ( origEntity.getAreqId().equals( destEntity.getAreqId() )) {
						continue;
					}
					// 内容が同じならセーフ
					if ( isSameContents( lotEntity, origEntity, destEntity, assetPath ) ) {
						continue;
					}

					List<String> origBuildNoList = assetApplyNoBuildNoListMap.get( origEntity.getAreqId() );
					List<String> destBuildNoList = assetApplyNoBuildNoListMap.get( destEntity.getAreqId() );

					for ( String origBuildNo : origBuildNoList ) {

						for ( String destBuildNo : destBuildNoList ) {

							messageList.add		( RmMessageId.RM001027E );
							messageArgsList.add	( new String[] {
									origBuildNo,
									origEntity.getPjtId(),
									origEntity.getAreqId(),
									assetPath,
									destBuildNo,
									destEntity.getPjtId(),
									destEntity.getAreqId() } );

						}
					}
				}
			}
		} catch ( IOException e ) {
			throw new TriSystemException ( RmMessageId.RM005026S , e );
		} finally {
			if( null == paramBean.getInfoMessageIdList() ) {
				paramBean.setInfoMessageIdList( messageList );
			} else {
				paramBean.getInfoMessageIdList().addAll( messageList ) ;
			}
			if( null == paramBean.getInfoMessageArgsList() ) {
				paramBean.setInfoMessageArgsList( messageArgsList );
			} else {
				paramBean.getInfoMessageArgsList().addAll( messageArgsList ) ;
			}
		}
	}

	/**
	 * 別々の返却申請情報に付属する同じパスの資産が、同じ内容であるかどうかをチェックする
	 * @param lotEntity ロットエンティティ
	 * @param origEntity 比較元返却申請エンティティ
	 * @param destEntity 比較先返却申請エンティティ
	 * @param assetPath 資産パス
	 * @return 内容が同じであればtrue、そうでなければfalse
	 */
	private boolean isSameContents(
			ILotEntity lotEntity, IAreqEntity origEntity,
			IAreqEntity destEntity, String assetPath ) throws IOException {

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotEntity );

		File targetFile = new File(
				RmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, origEntity ),
				assetPath );

		File checkFile = new File(
				RmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, destEntity ),
				assetPath );

		return TriFileUtils.isSame( targetFile, checkFile );

	}

//	/**
//	 * リリース登録中のリリースエンティティを取得する。
//	 *
//	 */
//	private IRelEntity[] getActiveRelEntity() {
//
//		ICondition condition = ListSearchAddonUtil.getActiveRelCondition();
//
//		IRelEntityLimit entityLimit = this.support.getRelDao().find( condition, null, 1, 0 );
//
//		return entityLimit.getEntity();
//	}
}
