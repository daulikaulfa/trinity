package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * リリースパッケージ・ビルドパッケージ選択画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	/** ビルドパッケージ情報 */
	private List<UnitViewBean> unitViewBeanList = null;
	/** マージ順のリスト */
	private List<String> mergeOrderList = null;
	
	public List<UnitViewBean> getUnitViewBeanList() {
		if ( null == unitViewBeanList ) {
			unitViewBeanList = new ArrayList<UnitViewBean>();
		}
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}

	public List<String> getMergeOrderList() {
		return mergeOrderList;
	}
	public void setMergeOrderList( List<String> mergeOrderList ) {
		this.mergeOrderList = mergeOrderList;
	}

//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
	
}
