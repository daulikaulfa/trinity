package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlListViewBean;

/**
 * リリース取消一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlCancelListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択された環境番号 */	
	private String selectedEnvNo = null;
	/** 環境番号 */
	private String envNo = null ;
	/** 環境名 */	
	private String envName = null;
	/** リリース情報 */
	private List<CtlListViewBean> relCtlViewBeanList = new ArrayList<CtlListViewBean>();
	/** 検索条件情報 */
	private RelCtlSearchBean relCtlSearchBean = null;


	public String getEnvName() {
		return envName;
	}
	public void setEnvName(String envName) {
		this.envName = envName;
	}
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo(String selectedEnvNo) {
		this.selectedEnvNo = selectedEnvNo;
	}
	public List<CtlListViewBean> getRelCtlViewBeanList() {
		return relCtlViewBeanList;
	}
	public void setRelCtlViewBeanList( List<CtlListViewBean> releaseViewBeanList ) {
		this.relCtlViewBeanList = releaseViewBeanList;
	}
	public RelCtlSearchBean getRelCtlSearchBean() {
		return relCtlSearchBean;
	}
	public void setRelCtlSearchBean(RelCtlSearchBean relCtlSearchBean) {
		this.relCtlSearchBean = relCtlSearchBean;
	}
	public String getEnvNo() {
		return envNo;
	}
	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}

}
