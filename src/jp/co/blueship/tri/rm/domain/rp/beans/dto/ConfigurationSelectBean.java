package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * リリースパッケージ・環境選択画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ConfigurationSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	/** 環境情報 */
	private List<ConfigurationViewBean> confViewBeanList = null;

	
	public List<ConfigurationViewBean> getConfViewBeanList() {
		if ( null == confViewBeanList ) {
			confViewBeanList = new ArrayList<ConfigurationViewBean>();
		}
		return confViewBeanList;
	}
	public void setConfViewBeanList( List<ConfigurationViewBean> confViewBeanList ) {
		this.confViewBeanList = confViewBeanList;
	}

//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
	
}
