package jp.co.blueship.tri.rm.domain.rp.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.cmn.utils.TaskImageResourceCache;
import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.ReleaseTaskBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqBinaryFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリースパッケージ・リリースの依頼Class<br>
 * <br>
 * @version V3L10.02
 * @author Takashi Ono
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class RelCtlEntryServiceRelease implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;

	private FlowRelCtlEditSupport support = null;
	private TaskFinderSupport taskSupport = null;
	private IReleaseTaskService releaseTaskService = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	public void setTaskSupport(TaskFinderSupport taskSupport) {
		this.taskSupport = taskSupport;
	}

	public void setReleaseTaskService( IReleaseTaskService releaseTaskService ) {
		this.releaseTaskService = releaseTaskService;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute(IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
			try {

				String relNo = paramBean.getRelNo();
				ItemCheckAddonUtils.checkRelNo( relNo );


				//トランザクション処理を見直したため、以下の待ち処理は不要。
				IRpDto rpDto = this.support.findRpDto( relNo );
				String envNo = rpDto.getRpEntity().getBldEnvId();
				IBldEnvEntity relEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity( envNo );
				IBldEnvSrvEntity[] relEnvServerEntitys = this.support.getBldEnvSrvEntity( envNo );

				//▼タイムラインごとにサービスを依頼する
				IReleaseTaskBean bean = getReleaseTaskBean( paramBean, rpDto );

				IBldTimelineEntity[] entitys = getRelTimeLine( envNo );
				ITaskFlowEntity[] taskEntitys = this.support.getTaskFlowEntities();

				//キャッシュ設定
				TaskImageResourceCache.setInit( false );
				TaskImageResourceCache.setReleaseResourceCache(
						relEnvEntity, relEnvServerEntitys, rpDto.getRpEntity(), entitys , taskEntitys );

				List<String> exceptionList = null ;
				bean.setForceExecuteMode( false ) ;
				for ( IBldTimelineEntity request : entitys ) {
					//例外が発生してもタイムラインを継続させる。
					try {
						this.releaseTaskService.execute( request, bean );
					} catch( java.rmi.ConnectException ex ) {
						this.taskSupport.writeReleaseProcessByIrregular( request, (IReleaseTaskBean)bean, null, ex );
						throw new TriSystemException( RmMessageId.RM005003S , ex ) ;
					} catch ( TriRuntimeException e ) {
						if( null == exceptionList ) {
							exceptionList = new ArrayList<String>() ;
						}
						exceptionList.add( e.getStackTraceString() ) ;
						bean.setForceExecuteMode( true ) ;
					} catch( Exception e ) {
						if( null == exceptionList ) {
							exceptionList = new ArrayList<String>() ;
						}
						exceptionList.add( ExceptionUtils.getStackTraceString(e) ) ;
						bean.setForceExecuteMode( true ) ;
					}
				}
				//					タイムラインがすべて終了後、当初の例外を発生させる ＆ relProcessのエラーメッセージを収集する
				if( null != exceptionList && 0 != exceptionList.size() ) {
					String errorMessage = this.makeErrorMessage( relNo, paramBean.getProcId() ) ;

					StringBuilder stb = new StringBuilder() ;
					for( String stackTrace : exceptionList ) {
						stb.append( stackTrace ) ;
					}
					Exception exception = new BusinessException( RmMessageId.RM005004S , SEP +
							errorMessage + SEP + SEP +
							stb.toString() );
					throw exception ;
				}

			} finally{

				//レポート機能は見直し中
				//
				//					if ( null != relEntity ) {
				//						IReportEntity repEntity = this.insertReportEntity( paramBean, paramBean, systemDate );
				//
				//						//下層のアクションを実行する
				//						{
				//							List<Object> paramList = new ArrayList<Object>();
				//
				//							IReportRelCtlReportParamInfo actionParam = new ReportRelCtlReportParamInfo();
				//							actionParam.setUserId	( paramBean.getUserId() );
				//							actionParam.setUserName	( paramBean.getUserName() );
				//							actionParam.setReportId	( paramBean.getReportId() );
				//
				//							paramList.add( this.support );
				//							paramList.add( actionParam );
				//							paramList.add( relEntity );
				//							paramList.add( this.getRelTaskRecEntity(relEntity) );
				//							paramList.add( repEntity );
				//
				//							for ( IActionPojo action : actions ) {
				//								action.execute( paramList );
				//							}
				//						}
				//					}

			}

			return serviceDto;

		} catch ( BusinessException be ) {
			//ビジネスエラーメール用
			paramBean.setBusinessThrowable( be ) ;
			throw be;
		} catch ( TriRuntimeException be ) {
			//システムエラーメール用
			paramBean.setSystemThrowable( be ) ;
			throw be;
		} catch ( Exception e ) {
			//システムエラーメール用
			paramBean.setSystemThrowable( e ) ;
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005056S , e );
		}
	}

	/**
	 * リリースパッケージ環境タスクのタイムライン情報を取得する。
	 * @param envNo 環境番号
	 * @return リリースパッケージ環境タスクのタイムライン情報
	 */
	private final IBldTimelineEntity[] getRelTimeLine( String envNo ) {

		IBldTimelineEntity[] entities = this.support.getBldTimelineEntity( envNo );

		if( 0 == entities.length ){
			throw new TriSystemException(RmMessageId.RM004004F , envNo);
		}
		return entities;
	}

	/**
	 * リリースパッケージタスクの引数オブジェクトを取得する。
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 * @param relEntity リリースエンティティ
	 * @return リリースパッケージタスクの引数オブジェクト
	 */
	private final IReleaseTaskBean getReleaseTaskBean(
			FlowRelCtlEntryServiceBean paramBean, IRpDto relEntity ) {

		ReleaseTaskBean bean				= new ReleaseTaskBean();
		ILotEntity pjtLotEntity				= this.support.getAmFinderSupport().findLotEntity( relEntity.getRpEntity().getLotId() );
		String relApplyNo = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();
		IRaEntity relApplyEntity			= TriStringUtils.isEmpty(relApplyNo) ? null : this.support.findRaEntity( relApplyNo );
		BpDtoList buildEntities			= this.support.getBuildEntity( RmEntityAddonUtils.getBuildNo( relEntity ));
		AreqDtoList assetApplyEntities		= getAssetApplyEntityArray( buildEntities );

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
		paramList.add( paramBean );
		paramList.add( bean );
		paramList.add( pjtLotEntity );
		paramList.add( relEntity );
		paramList.add( relApplyEntity );
		paramList.add( buildEntities );
		paramList.add( assetApplyEntities );

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.addAll( paramList );

		for ( IDomain<IGeneralServiceBean> action : this.actions ) {
			action.execute( innerServiceDto );
		}

		bean = (ReleaseTaskBean)RmExtractEntityAddonUtils.extractReleaseTask( paramList );
		if ( null == bean ) {
			throw new TriSystemException(RmMessageId.RM005005S);
		}

		return bean;
	}

	/**
	 * IBuildEntityにひもづいたIAreqEntityを取得する
	 * @param buildEntities
	 * @return
	 */
	private final AreqDtoList getAssetApplyEntityArray( List<IBpDto> buildEntities ) {

		AreqDtoList areqDtoEntities = new AreqDtoList();
		Set<String> applyNoSet = new HashSet<String>();

		for ( IBpDto buildEntity : buildEntities ) {

			for ( IBpAreqLnkEntity buildApplyEntity : buildEntity.getBpAreqLnkEntities() ) {
				applyNoSet.add( buildApplyEntity.getAreqId() );
			}
		}

		// 全量資産
		if ( 0 == applyNoSet.size() ) return areqDtoEntities;


		// 申請情報取得
		IJdbcCondition delApplyCondition =
				DBSearchConditionAddonUtil.getAreqCondition( (String[])applyNoSet.toArray( new String[0] ) );

		ISqlSort sort = FlowRelCtlEditSupport.getAssetApplySort();

		List<IAreqEntity> areqEntities =
				this.support.getAmFinderSupport().getAreqDao().find( delApplyCondition.getCondition(), sort, 1, 0 ).getEntities();

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );
			areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );

			AreqBinaryFileCondition bfileCondition = new AreqBinaryFileCondition();
			bfileCondition.setAreqId( areqEntity.getAreqId() );
			areqDto.setAreqBinaryFileEntities( this.support.getAmFinderSupport().getAreqBinaryFileDao().find( bfileCondition.getCondition() ) );

			areqDtoEntities.add( areqDto );
		}

		return areqDtoEntities;
	}


	//	/**
	//	 * リリースパッケージの業務タスク実行履歴情報エンティティを取得する。
	//	 * @param buildEntity
	//	 * @return 実行履歴情報エンティティの配列
	//	 */
	//	private IRelTaskRecEntity[] getRelTaskRecEntity( IRpEntity relEntity ) {
	//
	//		RelTaskRecCondition condition = new RelTaskRecCondition();
	//		condition.setRelNo( relEntity.getRelNo() );
	//		condition.setRelStartDate( relEntity.getRelStartDate(), relEntity.getRelStartDate() );
	//
	//		IRelTaskRecEntityLimit relTaskRecEntityLimit =
	//			this.support.getRelTaskRecDao().find( condition, null, 1, 0 );
	//
	//		return relTaskRecEntityLimit.getEntity();
	//	}
	//
	//	/**
	//	 * レポートレコードの登録
	//	 * <br>
	//	 * <br>ステータスを「処理中」としてレポートレコードを登録する。
	//	 *
	//	 * @param paramBean
	//	 * @param paramBean
	//	 * @param systemDate システム日時
	//	 * @return 登録したレコード情報
	//	 */
	//	private final IReportEntity insertReportEntity(
	//			FlowRelCtlEntryServiceBean paramBean,
	//			FlowRelCtlEntryServiceBean paramBean,
	//			String systemDate  ) {
	//
	//		IReportEntity entity = new ReportEntity();
	//
	//		String date = DateAddonUtil.getSystemDate();
	//
	//		entity.setReportNo			( this.support.getReportNumberingDao().nextval() );
	//		entity.setReportId			( ReportEntity.ReportId.RelCtlReport.getValue() );
	//		entity.setGroupName			( paramBean.getGroupName() );
	//		entity.setExecuteUser		( paramBean.getUserName() );
	//		entity.setExecuteUserId		( paramBean.getUserId() );
	//		entity.setExecuteStartDate	( ( null == systemDate )? date : systemDate );
	//		//entity.setExecuteEndDate	(  );
	//		entity.setInsertUpdateUser	( paramBean.getUserName() );
	//		entity.setInsertUpdateUserId( paramBean.getUserId() );
	//		entity.setInsertUpdateDate	( date );
	//		entity.setBaseStatusId		( IReportEntity.StatusId.UnEnter.getValue() );
	//		entity.setStatusId			( IReportEntity.StatusId.ReportActive.getValue() );
	//		entity.setDelStatusId		( StatusFlg.off.value() ) ;
	//
	//		this.support.getReportDao().insert( entity );
	//
	//		return entity;
	//	}

	/**
	 *
	 * @param relNo
	 * @param procId
	 * @return
	 */
	private String makeErrorMessage( String relNo, String procId ) {
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		StringBuilder stb = new StringBuilder() ;

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId( procId );
		ITaskFlowProcEntity[] relProcessEntityArray = this.support.getBmFinderSupport().getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]) ;
		for( int i = 0 ; i < relProcessEntityArray.length ; i++ ) {
			ITaskFlowProcEntity relProcessEntity = relProcessEntityArray[ i ] ;

			if( RmRpStatusIdForExecData.ReleasePackageError.equals( relProcessEntity.getStsId() ) ) {
				String[] messageArgs = new String[] {
						relNo,
						relProcessEntity.getProcId(),
						relProcessEntity.getBldEnvId(),
						relProcessEntity.getBldLineNo().toString(),
						relProcessEntity.getBldSrvId(),
						relProcessEntity.getTaskFlowId(),
						relProcessEntity.getMsg()
				} ;
				stb.append( ac.getMessage( RmMessageId.RM001028E , messageArgs ) + SEP ) ;
			}
		}

		return stb.toString() ;
	}
}
