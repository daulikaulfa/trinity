package jp.co.blueship.tri.rm.domain.rp.beans;

import java.io.File;
import java.io.IOException;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;


/**
 * ＲＣ取消時のＤＳＬ削除処理を行います。
 *
 */
public class ActionDeleteRelCtlDSL extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		ILotEntity pjtLotEntity = RmExtractEntityAddonUtils.extractPjtLot( serviceDto.getParamList() ) ;
		if ( TriStringUtils.isEmpty( pjtLotEntity ) ) {
			throw new TriSystemException( RmMessageId.RM005008S ) ;
		}

		IRpEntity[] relEntities = RmExtractEntityAddonUtils.extractRelArray( serviceDto.getParamList() );
		if ( TriStringUtils.isEmpty( relEntities ) ) {
			throw new TriSystemException( RmMessageId.RM005009S ) ;
		}

		File relDslPath = null;

		try {

			for ( IRpEntity relEntity : relEntities ) {

				relDslPath = RmDesignBusinessRuleUtils.getHistoryRelDSLPath( pjtLotEntity , relEntity ) ;
				if( relDslPath.exists() ) {
					TriFileUtils.delete( relDslPath ) ;
				}
			}
		} catch ( IOException ioe ) {
			throw new TriSystemException( RmMessageId.RM004005F, ioe , relDslPath.getPath()) ;
		}

		return serviceDto;
	}
}
