package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

/**
 * リリース取消 情報入力用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class CancelInfoInputBean implements Serializable {

	private static final long serialVersionUID = 1L;


	/** リリース概要 */
	private String relSummary = null;
	/** リリース番号 */
	private String relNo = null;
	/** 取消コメント */
	private String cancelComment = null;
	
	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
	public String getRelSummary() {
		return relSummary;
	}
	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}
	public String getCancelComment() {
		return cancelComment;
	}
	public void setCancelComment( String cancelComment ) {
		this.cancelComment = cancelComment;
	}
}
