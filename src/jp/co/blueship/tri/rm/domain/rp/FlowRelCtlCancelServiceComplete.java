package jp.co.blueship.tri.rm.domain.rp;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpDto;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリース取消受付完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelCtlCancelServiceComplete implements IDomain<FlowRelCtlCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IServiceDto<FlowRelCtlCancelServiceBean> execute( IServiceDto<FlowRelCtlCancelServiceBean> serviceDto ) {

		FlowRelCtlCancelServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelCtlScreenID.COMP_CANCEL ) &&
					!forwordID.equals( RelCtlScreenID.COMP_CANCEL )) {
				return serviceDto;
			}

			if ( refererID.equals( RelCtlScreenID.COMP_CANCEL )) {

			}

			if ( forwordID.equals( RelCtlScreenID.COMP_CANCEL )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String[] relNo = paramBean.getSelectedRelNo();
					ItemCheckAddonUtils.checkRelNo( relNo );

					CancelInfoInputBean releaseInfoInputBean = paramBean.getReleaseInfoInputBean();

					IRpEntity[] entities = this.support.getRpEntity( relNo ) ;

					String userName		= paramBean.getUserName();
					String userId		= paramBean.getUserId();

					// リリース情報の更新
					this.updateRelEntityToCancel( entities, releaseInfoInputBean, userName , userId );

					//リリース申請情報の更新
					this.updateRelApply( entities );

					// ロット番号でリリースエンティティを取りまとめ
					Map<String, List<IRpEntity>> lotNoRelMap = getLotNoRelMap( entities );
					List<ILotEntity> lotEntities =
						this.support.getPjtLotEntityList( (String[])lotNoRelMap.keySet().toArray( new String[0] ));
					IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
					List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );

					// グループの存在チェック
					List<IGrpUserLnkEntity> groupUsers = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					for ( ILotDto lotEntity : lotDto ) {
						RelCommonAddonUtil.checkAccessableGroup( lotEntity, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
					}

					this.deleteRelDSLToCancel( lotEntities, lotNoRelMap, paramBean, srvEntity ) ;

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005058S, e);
		}
	}

	/**
	 * リリース情報を取消に更新する
	 * @param entities リリースエンティティ
	 * @param unitInfoInputBean 編集情報
	 * @param userName ユーザ名
	 */
	private void updateRelEntityToCancel(
			IRpEntity[] entities, CancelInfoInputBean releaseInfoInputBean,
			String userName , String userId ) {

		for ( IRpEntity entity : entities ) {

			entity.setDelCmt			( releaseInfoInputBean.getCancelComment() );
			entity.setDelTimestamp		( TriDateUtils.getSystemTimestamp() );
			entity.setDelUserNm			( userName );
			entity.setDelUserId			( userId );
			entity.setDelStsId			( StatusFlg.on );
			entity.setStsId				( RmRpStatusId.ReleasePackageRemoved.getStatusId() );
			entity.setUpdUserNm			( userName );
			entity.setUpdUserId			( userId );

			this.support.getRpDao().update( entity );

			// rm_rp_bp_lnk 論理削除
			RpBpLnkCondition rpBpCondition = new RpBpLnkCondition();
			rpBpCondition.setRpId( entity.getRpId() );
			IRpBpLnkEntity rpBpEntity = new RpBpLnkEntity();
			rpBpEntity.setDelStsId( StatusFlg.on );
			this.support.getRpBpLnkDao().update( rpBpCondition.getCondition(), rpBpEntity );

			if ( DesignSheetUtils.isRecord() ) {
				// 履歴用DTO作成
				IRpDto rpDto = new RpDto();
				rpDto.setRpEntity(entity);
				RpBpLnkCondition bpCondition = new RpBpLnkCondition();
				bpCondition.setRpId(entity.getRpId());
				rpDto.setRpBpLnkEntities(support.getRpBpLnkDao().find(bpCondition.getCondition()));
				// dmDo取得
				DmDoCondition doCondition = new DmDoCondition();
				doCondition.setRpId(entity.getRpId());
				List<IDmDoEntity> doEntities = support.getDmDoDao().find(doCondition.getCondition());
				// リリース申請リンクテーブルよりリリース申請ID一覧取得
				RaRpLnkCondition raRpCondition = new RaRpLnkCondition();
				raRpCondition.setRpId( entity.getRpId() );
				List<IRaRpLnkEntity> raRpEntities = this.support.getRaRpLnkDao().find( raRpCondition.getCondition() );
				List<String> raIds = new ArrayList<String>();
				for ( IRaRpLnkEntity raRpEntity : raRpEntities ) {
					raIds.add( raRpEntity.getRaId() );
				}

				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.Remove.getStatusId() );

				support.getRpHistDao().insert( histEntity , rpDto , doEntities, raIds);
			}

			this.support.getUmFinderSupport().cleaningCtgLnk(RmTables.RM_RP, entity.getRpId());
			this.support.getUmFinderSupport().cleaningMstoneLnk(RmTables.RM_RP, entity.getRpId());
		}
	}

	/**
	 * リリース申請情報を更新する。
	 * @param entities リリースエンティティ
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 */
	private final void updateRelApply(
			IRpEntity[] entities ) {

		List<String> relList = new ArrayList<String>();

		ISqlSort sort = new SortBuilder();

		for ( IRpEntity entity : entities ) {
			relList.add( entity.getRpId() );
		}

		// リリース申請リンクテーブルよりリリース申請ID一覧取得
		RaRpLnkCondition raRpCondition = new RaRpLnkCondition();
		raRpCondition.setRpIds( relList.toArray( new String[0] ) );
		List<IRaRpLnkEntity> raRpEntities = this.support.getRaRpLnkDao().find( raRpCondition.getCondition() );
		List<String> raIds = new ArrayList<String>();
		for ( IRaRpLnkEntity raRpEntity : raRpEntities ) {
			raIds.add( raRpEntity.getRaId() );
		}

		if( 0<raIds.size() ) {	// raIdsが0件だと全件検索対象になる為の対策

			// 更新対象のリリース申請取得
			RaCondition condition = new RaCondition();
			condition.setRaIds( raIds.toArray( new String[0] ) );

			List<IRaEntity> raEntities = this.support.getRaDao().find( condition.getCondition(), sort );
			for ( IRaEntity relApplyEntity: raEntities ) {

				relApplyEntity.setStsId			( RmRaStatusId.ReleasePackageRemoved.getStatusId() );

				this.support.getRaDao().update(relApplyEntity);
			}
			this.support.getSmFinderSupport().cleaningExecDataSts(RmTables.RM_RA, raIds.toArray( new String[0] ) );
		}

	}

	/**
	 * リリース情報をもとに、ＤＳＬが存在する場合、削除する。
	 * @param lotEntities
	 * @param lotNoRelMap
	 * @param paramBean
	 * @param srvEntity
	 * @throws RemoteException
	 */
	private void deleteRelDSLToCancel(
			List<ILotEntity> lotEntities,
			Map<String, List<IRpEntity>> lotNoRelMap,
			FlowRelCtlCancelServiceBean paramBean,
			IBldSrvEntity srvEntity ) throws RemoteException {

		// lockServerIdが排他以外でも使用されているため、基本的には同じはずだが書き換わる前の排他用のlockServerIdをとっておく。
		String lockServerId = paramBean.getLockServerId();

		try {
			String serverId = srvEntity.getBldSrvId();

			for ( ILotEntity pjtLotEntity : lotEntities ) {

				List<IRpEntity> relList = lotNoRelMap.get( pjtLotEntity.getLotId() );

				List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
				paramList.add( pjtLotEntity );
				paramList.add( (IRpEntity[])relList.toArray( new IRpEntity[0] ));
				paramBean.setLockServerId( serverId );

				paramList.add( paramBean );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}
			}
		} finally {
			paramBean.setLockServerId( lockServerId );
		}
	}


	/**
	 * ロット番号-リリースエンティティのリストのマップを取得する
	 * @param entities
	 * @return
	 */
	private Map<String, List<IRpEntity>> getLotNoRelMap( IRpEntity[] entities ) {

		Map<String, List<IRpEntity>> lotNoRelMap = new HashMap<String, List<IRpEntity>>() ;
		for( IRpEntity relEntity : entities ) {

			if ( !lotNoRelMap.containsKey( relEntity.getLotId() )) {
				lotNoRelMap.put( relEntity.getLotId(), new ArrayList<IRpEntity>() ) ;
			}

			List<IRpEntity> relList = lotNoRelMap.get( relEntity.getLotId() );
			relList.add( relEntity );
		}

		return lotNoRelMap;

	}

}
