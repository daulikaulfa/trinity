package jp.co.blueship.tri.rm.domain.rp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * リリースパッケージ・ビルドパッケージ詳細用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlUnitDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** ビルドパッケージ番号 */
	private String selectedBuildNo = null;
	/** リリースパッケージ情報 */
	private List<ReleaseViewBean> releaseViewBeanList = null;


	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}

	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}

	public List<ReleaseViewBean> getReleaseViewBeanList() {
		if ( null == releaseViewBeanList ) {
			releaseViewBeanList = new ArrayList<ReleaseViewBean>();
		}
		return releaseViewBeanList;
	}
	public void setReleaseViewBeanList(
			List<ReleaseViewBean> releaseViewBeanList ) {
		this.releaseViewBeanList = releaseViewBeanList;
	}


	/**
	 * リリースパッケージ情報
	 */
	public ReleaseViewBean newReleaseViewBean() {
		ReleaseViewBean bean = new ReleaseViewBean();
		return bean;
	}

	public class ReleaseViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/** リリース番号 */
		private String relNo = null;
		/** 環境名 */
		private String envName = null;
		/** リリース登録日 */
		private String relInputDate = null;

		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		public String getRelNo() {
			return relNo;
		}

		public void setEnvName( String envName ) {
			this.envName = envName;
		}
		public String getEnvName() {
			return envName;
		}

		public void setRelInputDate( String relInputDate ) {
			this.relInputDate = relInputDate;
		}
		public String getRelInputDate() {
			return relInputDate;
		}
	}
}
