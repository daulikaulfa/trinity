package jp.co.blueship.tri.rm.domain.rp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.LotViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;

/**
 * リリースパッケージ・リリース詳細画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlHistoryDetailViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** リリース番号 */
	private String selectedRelNo = null;
	/** リリース基本情報 */
	private BaseInfoViewBean baseInfoViewBean = null;
	/** ビルドパッケージ情報 */
	private List<UnitViewBean> unitViewBeanList = new ArrayList<UnitViewBean>();
	/** リリース環境情報 */
	private EnvViewBean envViewBean = null;
	/** ロット情報 */
	private LotViewBean lotViewBean = null;
	
	
	public String getSelectedRelNo() {
		return selectedRelNo;
	}
	public void setSelectedRelNo( String selectedRelNo ) {
		this.selectedRelNo = selectedRelNo;
	}

	public BaseInfoViewBean getBaseInfoViewBean() {
		return baseInfoViewBean;
	}
	public void setBaseInfoViewBean( BaseInfoViewBean baseInfoViewBean ) {
		this.baseInfoViewBean = baseInfoViewBean;
	}
	
	public List<UnitViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}
	
	public EnvViewBean getEnvViewBean() {
		if ( null == envViewBean ) {
			envViewBean = new EnvViewBean();
		}
		return envViewBean;
	}
	public void setEnvViewBean( EnvViewBean envViewBean ) {
		this.envViewBean = envViewBean;
	}
	
	public LotViewBean getLotViewBean() {
		if ( null == lotViewBean ) {
			lotViewBean = new LotViewBean();
		}
		return lotViewBean;
	}
	public void setLotViewBean( LotViewBean lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}

	
	/**
	 * リリース基本情報
	 */
	public BaseInfoViewBean newBaseInfoViewBean() {
		BaseInfoViewBean bean = new BaseInfoViewBean();
		return bean;
	}

	public class BaseInfoViewBean implements Serializable {
		
		private static final long serialVersionUID = 1L;

		/** 
		 * リリース番号 
		 */
		private String relNo = null;
		/** 
		 * ステータス 
		 */
		private String relStatus = null;
		/** 
		 * リリース概要 
		 */
		private String relSummary = null;
		/** 
		 * リリース内容 
		 */
		private String relContent = null;
		/** 
		 * 実行者 
		 */
		private String executeUser = null;
		/** 
		 * 実行者ＩＤ 
		 */
		private String executeUserId = null;
		/** 
		 * 実行開始日時 
		 */
		private String relStartDate = null;
		/** 
		 * 実行終了日時
		  */
		private String relEndDate = null;
		
		
		public String getExecuteUserId() {
			return executeUserId;
		}
		public void setExecuteUserId(String executeUserId) {
			this.executeUserId = executeUserId;
		}
		public String getRelNo() {
			return relNo;
		}
		public void setRelNo( String relNo ) {
			this.relNo = relNo;
		}
		
		public void setRelStatus( String relStatus ) {
			this.relStatus = relStatus;
		}
		public String getRelStatus() {
			return relStatus;
		}
		
		public String getRelContent() {
			return relContent;
		}
		public void setRelContent( String relContent ) {
			this.relContent = relContent;
		}

		public String getRelSummary() {
			return relSummary;
		}
		public void setRelSummary( String relSummary ) {
			this.relSummary = relSummary;
		}
		public String getExecuteUser() {
			return executeUser;
		}
		public void setExecuteUser(String executeUser) {
			this.executeUser = executeUser;
		}
		public String getRelEndDate() {
			return relEndDate;
		}
		public void setRelEndDate(String relEndDate) {
			this.relEndDate = relEndDate;
		}
		public String getRelStartDate() {
			return relStartDate;
		}
		public void setRelStartDate(String relStartDate) {
			this.relStartDate = relStartDate;
		}
		
	}
	
	
	/**
	 * リリース環境情報
	 */	
	public EnvViewBean newEnvViewBean() {
		EnvViewBean bean = new EnvViewBean();
		return bean;
	}
	
	public class EnvViewBean implements Serializable {
		
		private static final long serialVersionUID = 1L;

		/** 環境番号 */
		private String envNo = null;
		/** 環境名 */
		private String envName = null;
		/** 環境概要 */
		private String envSummary = null;
		
		
		public void setEnvNo( String envNo ) {
			this.envNo = envNo;
		}
		public String getEnvNo() {
			return envNo;
		}

		public void setenvName( String envName ) {
			this.envName = envName;
		}
		public String getenvName() {
			return envName;
		}

		public void setEnvSummary( String envSummary ) {
			this.envSummary = envSummary;
		}
		public String getEnvSummary() {
			return envSummary;
		}
	}
}
