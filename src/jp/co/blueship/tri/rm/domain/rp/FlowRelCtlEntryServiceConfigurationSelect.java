package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmViewInfoAddonUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationSelectBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・環境選択画面の表示情報設定Class<br>
 *
 * @version V3L10.02
 * @author Takashi Ono
 */
public class FlowRelCtlEntryServiceConfigurationSelect implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support;
	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if( !refererID.equals( RelCtlScreenID.CONFIGURATION_SELECT ) &&
					!forwordID.equals( RelCtlScreenID.CONFIGURATION_SELECT ) ){
				return serviceDto;
			}

			if( refererID.equals( RelCtlScreenID.CONFIGURATION_SELECT )) {

				String selectedConfNo = paramBean.getSelectedEnvNo();

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( RelCtlScreenID.CONFIGURATION_SELECT )) {
					ItemCheckAddonUtils.checkConfNo( selectedConfNo );
				}

				paramBean.setSelectedEnvNo( selectedConfNo );
			}

			if( forwordID.equals( RelCtlScreenID.CONFIGURATION_SELECT )) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();

					if ( StringUtils.isEmpty(baseInfoBean.getBaseInfoInputBean().getRelApplyNo()) ) {
						this.searchEnvList(paramBean);
					} else {
						//リリース申請からリリースする場合
						this.searchEnv(paramBean);
					}
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005064S, e);
		}
	}

	/**
	 * リリース環境リストの検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchEnvList( FlowRelCtlEntryServiceBean paramBean ) {
		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort sort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect();

		ConfigurationSelectBean confSelectBean = paramBean.getConfSelectBean();

//				int selectedPageNo	=
//					(( 0 == confSelectBean.getSelectPageNo() )? 1: confSelectBean.getSelectPageNo() );
//				int maxPageNumber =
//					DesignProjectUtil.getMaxPageNumber(
//							DesignProjectRelDefineId.maxPageNumberByRelList );

		List<IBldEnvEntity> bldEnvEntities =
			this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort );

		if ( bldEnvEntities.size() == 0 ) {
			throw new BusinessException( RmMessageId.RM001020E );
		}

		// アクセス可能なロットの取得
		List<ILotEntity> lotEntities = this.getPjtLotEntityLimit().getEntities();
		List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto(lotEntities);

		List<ILotDto> accessableLotList =
			this.support.getAccessableLotEntity(support.getUmFinderSupport(), paramBean.getUserId(), lotDto, false, null );

		IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( bldEnvEntities.toArray(new IBldEnvEntity[0]), accessableLotList );
		if ( envEntities.length == 0 ) {
			throw new BusinessException( RmMessageId.RM001036E );
		}

		List<ConfigurationViewBean> confViewBeanList = new ArrayList<ConfigurationViewBean>();

		for ( IBldEnvEntity entity : envEntities ) {
			ConfigurationViewBean viewBean = new ConfigurationViewBean();

			RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( viewBean, entity );

			confViewBeanList.add( viewBean );
		}

		confSelectBean.setConfViewBeanList	( confViewBeanList );
//				confSelectBean.setPageInfoView		(
//						AddonUtil.convertPageNoInfo( new PageNoInfo(), envEntityLimit.getLimit() ));
//				confSelectBean.setSelectPageNo		( selectedPageNo );

		paramBean.setConfSelectBean( confSelectBean );
	}

	/**
	 * リリース環境の検索を行う（リリース申請からリリースする場合）
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchEnv( FlowRelCtlEntryServiceBean paramBean ) {

		ConfigurationSelectBean confSelectBean = paramBean.getConfSelectBean();

		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvId(paramBean.getSelectedEnvNo() );
		IBldEnvEntity envEntity =
			this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition()).get(0);

		if ( null == envEntity ) {
			throw new BusinessException( RmMessageId.RM001020E );
		}

		List<ConfigurationViewBean> confViewBeanList = new ArrayList<ConfigurationViewBean>();
		ConfigurationViewBean viewBean = new ConfigurationViewBean();

		RmViewInfoAddonUtils.setConfViewBeanRelEnvEntity( viewBean, envEntity );
		confViewBeanList.add( viewBean );

		confSelectBean.setConfViewBeanList	( confViewBeanList );

		paramBean.setConfSelectBean( confSelectBean );
	}

	/**
	 * 検索条件であるロット情報を取得する。
	 *
	 * @return 取得したロット情報を戻します。
	 */
	private final IEntityLimit<ILotEntity> getPjtLotEntityLimit() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition();
		IEntityLimit<ILotEntity> limit = this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() , null, 1 , 0 ) ;
		return limit ;
	}

}
