package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;

/**
 * リリースパッケージ・基本情報入力用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class BaseInfoInputBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 
	 * 選択されたリリース申請番号 
	 */
	private String relApplyNo = null ;
	/** リリース概要 */
	private String relSummary = null;
	/** リリース内容 */
	private String relContent = null;
	
	private String ctgId = null;

	private String mstoneId = null;
	
	public String getRelContent() {
		return relContent;
	}
	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}

	public String getRelSummary() {
		return relSummary;
	}
	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}
	public String getRelApplyNo() {
		return relApplyNo;
	}
	public void setRelApplyNo(String relApplyNo) {
		this.relApplyNo = relApplyNo;
	}

	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}
}
