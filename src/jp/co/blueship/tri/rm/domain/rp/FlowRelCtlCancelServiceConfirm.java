package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.constants.RelUnitScreenItemID;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelConfirmViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelConfirmViewBean.CancelUnitViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリース取消受付確認画面の表示情報設定Class<br>
 *
 * @version V3L10.02
 * @author Takashi Ono
 */
public class FlowRelCtlCancelServiceConfirm implements IDomain<FlowRelCtlCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support = null;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}
	private ActionStatusMatrixList statusMatrixAction = null;
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IServiceDto<FlowRelCtlCancelServiceBean> execute( IServiceDto<FlowRelCtlCancelServiceBean> serviceDto ) {

		FlowRelCtlCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelCtlScreenID.CANCEL_COMFIRM ) &&
					!forwordID.equals( RelCtlScreenID.CANCEL_COMFIRM )) {
				return serviceDto;
			}


			String[] rpIds = paramBean.getSelectedRelNo();

			if ( refererID.equals( RelCtlScreenID.CANCEL_COMFIRM )) {

				if ( ScreenType.next.equals( screenType )) {

					CancelInfoInputBean releaseInfoInputBean = paramBean.getReleaseInfoInputBean();
					checkReleaseCancel( releaseInfoInputBean );

					if( paramBean.isStatusMatrixV3() ) {
						List<IRpEntity> entities = this.getRelEntity( rpIds ).getEntities();
						List<IRpDto> rpDtoList = this.support.findRpDto(entities);
	
						List<IRaRpLnkEntity> raRpLnkEntities = this.support.findRaRpLnkEntitiesFromRpId(rpIds);
	
						String[] raIds = FluentList.from(raRpLnkEntities).map( RmFluentFunctionUtils.toRaIdFromRaRpLnk ).toArray(new String[0]);
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( this.getLotNoByRelEntity( rpDtoList ) )
						.setBpIds( this.support.getBuildNoFromRpDto( rpDtoList ) )
						.setRaIds( raIds )
						.setRpIds( rpIds );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( RelCtlScreenID.CANCEL_COMFIRM  )) {

				ItemCheckAddonUtils.checkRelNo( rpIds );

				IEntityLimit<IRpEntity> limit = this.getRelEntity( rpIds );
				List<IRpDto> rpDtoList = this.support.findRpDto(limit.getEntities());
				// トップ画面でリリース環境をしぼっているので、どのリリースエンティティでも属するリリース環境は同じ
				this.setRelEnvInfo( limit.getEntities().get(0), paramBean );

				IBpEntity[] buildEntities			= this.getBuildEntity( rpDtoList );
				ILotEntity[] pjtLotEntities 			= this.getPjtLotEntity() ;
				Map<String, ILotDto> lotMap = new HashMap<String, ILotDto>();

				// チェック用
				//IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
				for ( ILotEntity lotEntity : pjtLotEntities ) {
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( lotEntity );
					lotMap.put( lotEntity.getLotId(), dto );
				}
				// グループの存在チェック
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				for ( IRpDto relEntity : rpDtoList ) {
					RelCommonAddonUtil.checkAccessableGroup( lotMap.get( relEntity.getRpEntity().getLotId() ), this.support.getUmFinderSupport().getGrpDao(), groupUsers );
				}


				this.setServiceBeanSearchResult( rpDtoList , buildEntities , pjtLotEntities , paramBean );

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005059S, e);
		}
	}

	/**
	 * リリースエンティティからビルド情報を設定する
	 * @param entity リリースエンティティ
	 * @param paramBean FlowReleaseCancelServiceBean
	 */
	private final void setRelEnvInfo(
			IRpEntity entity, FlowRelCtlCancelServiceBean paramBean ) {

		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvId(entity.getBldEnvId());
		IBldEnvEntity envEntity = this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition() ).get(0);

		paramBean.setSelectedEnvNo( envEntity.getBldEnvId() );
		paramBean.setEnvName( envEntity.getBldEnvNm() );

	}

	/**
	 * リリース番号からリリースエンティティを取得する
	 * @param relNo リリース番号
	 * @param selectPageNo 選択ページ
	 * @return リリースエンティティリミット
	 */
	private final IEntityLimit<IRpEntity> getRelEntity( String[] relNo ) {

		IJdbcCondition condition	=
			DBSearchConditionAddonUtil.getRpConditionByRelNo( relNo );

		ISqlSort sort				=
			DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelCtlCancelConfirm();

		return this.support.getRpDao().find( condition.getCondition(), sort, 1, 0 );
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param limit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param pjtEntities 変更管理エンティティ
	 * @param applyEntities 申請情報エンティティ
	 * @param paramBean  FlowRelUnitCancelServiceBean
	 */
	private final void setServiceBeanSearchResult( List<IRpDto> rpDtoList ,
													IBpEntity[] buildEntities ,
													ILotEntity[] pjtLotEntities ,
													FlowRelCtlCancelServiceBean paramBean ) {

		List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList =
											new ArrayList<CancelConfirmViewBean>();

		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

		setRelCtlCancelConfirmViewBeanRelEntity(
				releaseCancelConfirmViewBeanList , rpDtoList , buildEntities , pjtLotEntities, srvEntity.getBldSrvId() ) ;

		paramBean.setReleaseCancelConfirmViewBeanList( releaseCancelConfirmViewBeanList );
	}
	/**
	 * 以下の情報よりRCキャンセル確認情報を設定する。
	 * <li>リリース情報
	 * <li>ビルド情報
	 * <li>ロット情報
	 *
	 * @param releaseCancelConfirmViewBeanList
	 * @param rpDtoList
	 * @param buildEntities
	 * @param pjtLotEntities
	 */
	private void setRelCtlCancelConfirmViewBeanRelEntity(
									List<CancelConfirmViewBean> releaseCancelConfirmViewBeanList,
									List<IRpDto> rpDtoList,
									IBpEntity[] buildEntities ,
									ILotEntity[] pjtLotEntities,
									String srvId ) {

		Map<String, IBpEntity> buildNoEntityMap = new HashMap<String, IBpEntity>();
		for ( IBpEntity buildEntity : buildEntities ) {
			buildNoEntityMap.put( buildEntity.getBpId(), buildEntity );
		}
		Map<String, ILotEntity> lotNoEntityMap = new HashMap<String, ILotEntity>();
		for ( ILotEntity pjtLotEntity : pjtLotEntities ) {
			lotNoEntityMap.put( pjtLotEntity.getLotId(), pjtLotEntity );
		}


		for ( IRpDto rpDto : rpDtoList ) {

			CancelConfirmViewBean viewBean = new CancelConfirmViewBean();
			viewBean.setLotNo( rpDto.getRpEntity().getLotId() );

			ILotEntity pjtLotEntity = lotNoEntityMap.get( rpDto.getRpEntity().getLotId() ) ;
			if( null != pjtLotEntity ) {
				viewBean.setLotName( pjtLotEntity.getLotNm() ) ;
			}
			viewBean.setServerNo( srvId );
			viewBean.setRelNo( rpDto.getRpEntity().getRpId() );
			viewBean.setRelSummary( rpDto.getRpEntity().getSummary() ) ;
			viewBean.setRelContent( rpDto.getRpEntity().getContent() ) ;

			List<CancelUnitViewBean> releaseCancelUnitViewBeanList = viewBean.getCancelUnitViewBeanList();

			for ( IRpBpLnkEntity build : rpDto.getRpBpLnkEntities() ) {

				CancelUnitViewBean unitViewBean = viewBean.newCancelUnitViewBean();

				IBpEntity buildEntity = buildNoEntityMap.get( build.getBpId() );

				if ( null != buildEntity ) {
					unitViewBean.setMergeOrder		( support.getMergeOdr( build.getRpId() , build.getBpId()) );
					unitViewBean.setBuildNo			( buildEntity.getBpId() );
					unitViewBean.setBuildName		( buildEntity.getBpNm() );
					unitViewBean.setBuildStartDate	(
							TriDateUtils.convertViewDateFormat( buildEntity.getProcStTimestamp() ) ) ;
					unitViewBean.setBuildSummary	( buildEntity.getSummary() );
					unitViewBean.setExecuteUser		( buildEntity.getExecUserNm() );
					unitViewBean.setExecuteUserId	( buildEntity.getExecUserId() );
					unitViewBean.setBuildStatus		(
							sheet.getValue( RmDesignBeanId.statusId, buildEntity.getProcStsId() ) );
				} else {
					unitViewBean.setBuildNo			( build.getBpId() );
				}
				releaseCancelUnitViewBeanList.add( unitViewBean );
			}

			viewBean.setCancelUnitViewBeanList( releaseCancelUnitViewBeanList );

			releaseCancelConfirmViewBeanList.add( viewBean );
		}
	}

	/**
	 * リリースエンティティから、含まれるビルドパッケージエンティティを取得する
	 *
	 * <br>※画面表示項目のため、削除済のRU情報も取得する。
	 * 更新系の処理で使用するとデータの不整合が発生する場合があるため、
	 * あくまで画面表示用のみに利用すること。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @return 変更管理エンティティ
	 */
	private final IBpEntity[] getBuildEntity( List<IRpDto> relEntityArray ) {

		String[] buildNo = this.support.getBuildNoFromRpDto( relEntityArray );

		BpCondition condition	= (BpCondition)DBSearchConditionAddonUtil.getBuildConditionByBuildNo( buildNo );
		condition.setDelStsId(  null  );

		List<IBpEntity> bpEntities	= this.support.getBmFinderSupport().getBpDao().find( condition.getCondition() );

		return bpEntities.toArray(new IBpEntity[0]);
	}
	/**
	 * ロット情報エンティティを取得する
	 *
	 * @return 変更管理エンティティ
	 */
	private final ILotEntity[] getPjtLotEntity() {

		LotCondition condition	= new LotCondition() ;
		List<ILotEntity> lotEntities	= this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() );

		return lotEntities.toArray(new ILotEntity[0]);
	}
	/**
	 *
	 * @param dtoList
	 * @return
	 */
	private String[] getLotNoByRelEntity( List<IRpDto> dtoList ) {

		Set<String> lotNoSet = new HashSet<String>() ;
		for( IRpDto dto : dtoList ) {
			lotNoSet.add( dto.getRpEntity().getLotId() ) ;
		}
		return lotNoSet.toArray( new String[ 0 ] ) ;
	}

	/**
	 * リリースが取消可能かをチェックする
	 * @param releaseInfoInputBean リリース編集入力情報
	 */

	private static void checkReleaseCancel( CancelInfoInputBean releaseInfoInputBean )
			throws BaseBusinessException {

		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						RmDesignBeanId.flowRelCtlCancelCheck,
						RelUnitScreenItemID.RelUnitCancelCheck.CANCEL_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( releaseInfoInputBean.getCancelComment() )) {
				throw new ContinuableBusinessException( BmMessageId.BM001012E );
			}
		}

	}

}
