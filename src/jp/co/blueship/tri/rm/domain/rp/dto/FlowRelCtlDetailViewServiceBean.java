package jp.co.blueship.tri.rm.domain.rp.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CtlDetailViewBean;

/**
 * リリースパッケージ・リリース状況確認用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlDetailViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択された環境番号 */	
	private String selectedEnvNo = null;
	/** リリース番号 */	
	private String relNo = null;
	/** リリース状況照会 */
	private CtlDetailViewBean releaseDetailViewBean = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット番号（コンボ用）*/
	private List<ItemLabelsBean> lotList = null ;
	
	public String getSelectedEnvNo() {
		return selectedEnvNo;
	}
	public void setSelectedEnvNo( String selectedEnvNo ) {
		this.selectedEnvNo = selectedEnvNo;
	}
	
	public String getRelNo() {
		return relNo;
	}
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
	
	public CtlDetailViewBean getReleaseDetailViewBean() {
		return releaseDetailViewBean;
	}
	public void setReleaseDetailViewBean(
			CtlDetailViewBean releaseDetailViewBean) {
		this.releaseDetailViewBean = releaseDetailViewBean;
	}
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public List<ItemLabelsBean> getLotList() {
		return lotList;
	}
	public void setLotList( List<ItemLabelsBean> lotList ) {
		this.lotList = lotList;
	}
}
