package jp.co.blueship.tri.rm.domain.rp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ServerType;

/**
 * リリースパッケージ一覧情報
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class CtlListViewBean implements Serializable {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 2L;
	/** 
	 * リリース番号 
	 */
	private String relNo = null;
	/** 
	 * リリース概要 
	 */
	private String summary = null ;
	/** 
	 * リリース内容 
	 */
	private String content = null ;
	/** 
	 * リリース登録日時 
	 */
	private String inputDate = null ;
	/** 
	 * リリース環境番号 
	 */
	private String envNo = null ;
	/** 
	 * ロット番号 
	 */
	private String lotId = null ;
	/** 
	 * サーバ番号 
	 */
	private String serverNo = null ;
	/** 
	 * ビルドパッケージ情報 
	 */
	private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
	/** 
	 * ステータス 
	 */
	private String relStatus = null;
	/** 
	 * 配布ステータス 
	 */
	private String distributeStatus = null ;
	
	public String getDistributeStatus() {
		return distributeStatus;
	}

	public void setDistributeStatus(String distributeStatus) {
		this.distributeStatus = distributeStatus;
	}

	public String getInputDate() {
		return inputDate;
	}

	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}

	public String getLotNo() {
		return lotId;
	}

	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	
	public String getServerNo() {
		if ( TriStringUtils.isEmpty( this.serverNo ) || "undefined".equals( this.serverNo ) ) {
			this.serverNo = ServerType.CORE.getValue();
		}
		return serverNo;
	}

	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getRelStatus() {
		return relStatus;
	}

	public void setRelStatus(String relStatus) {
		this.relStatus = relStatus;
	}

	public String getRelNo() {
		return relNo;
	}

	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}

	public String getEnvNo() {
		return envNo;
	}

	public void setEnvNo(String envNo) {
		this.envNo = envNo;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}
	
	public void setUnitBeanList( List<UnitBean> unitBeanList ) {
		this.unitBeanList = unitBeanList;
	}
	public List<UnitBean> getUnitBeanList() {
		return unitBeanList;
	}

	
}
