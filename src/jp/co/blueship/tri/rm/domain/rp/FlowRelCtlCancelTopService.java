package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelTopServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelTopServiceBean.ReleaseViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリース取消トップ画面の表示情報設定Class<br>
 *
 * @version V3L10.02
 * @author Takashi Ono
 */
public class FlowRelCtlCancelTopService implements IDomain<FlowRelCtlCancelTopServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support = null;
	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlCancelTopServiceBean> execute( IServiceDto<FlowRelCtlCancelTopServiceBean> serviceDto ) {

		FlowRelCtlCancelTopServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			int selectedPageNo	=
				(( 0 == paramBean.getSelectPageNo() ) ? 1 : paramBean.getSelectPageNo() );

			IJdbcCondition condition = DBSearchConditionAddonUtil.getActivelRelEnvCondition();
			ISqlSort sort = DBSearchSortAddonUtil.getRelSortFromDesignDefineByRelCtlCancelTop();

			int maxPageNumber =
				sheet.intValue(
						RmDesignEntryKeyByRelease.maxPageNumberByCancelTop );

			IBldEnvEntity[] envAllEntities = this.getRelEnvAllEntity( condition, sort );

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				if ( TriStringUtils.isEmpty( envAllEntities.length ) ) {
					throw new BusinessException( RmMessageId.RM001020E );
				}
			}

			// アクセス可能なロットの取得
			List<String> disableLinkLotNumbers = new ArrayList<String>();
			List<ILotEntity> lotEntities =this.getPjtLotEntityLimit().getEntities() ;
			List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );
			List<ILotDto> accessableLotList =
				this.support.getAccessableLotEntity(support.getUmFinderSupport(), paramBean.getUserId(), lotDto, true, disableLinkLotNumbers );

			IEntityLimit<IBldEnvEntity> envLimit =
				this.support.getRelEnvEntity(
					this.support.convertToEnvNo( envAllEntities, accessableLotList ),
					condition, sort,
					selectedPageNo, maxPageNumber );

			List<ReleaseViewBean> releaseViewBeanList = new ArrayList<ReleaseViewBean>();

			List<String> disableLinkEnvNumbers = this.support.getDisableLinkEnvNumbers( envLimit.getEntities().toArray(new IBldEnvEntity[0]), accessableLotList, disableLinkLotNumbers );

			for ( IBldEnvEntity envEntity : envLimit.getEntities() ) {

				ReleaseViewBean viewBean = paramBean.newReleaseViewBean();
				viewBean.setEnvNo	( envEntity.getBldEnvId() );
				viewBean.setEnvName	( envEntity.getBldEnvNm() );
//				viewBean.setEnvSummary( envEntity.getSummary() ) ;
//				viewBean.setEnvContent( envEntity.getContent() ) ;

				if ( disableLinkEnvNumbers.contains( envEntity.getBldEnvId() )) {
					viewBean.setViewLinkEnabled( false );
				} else {
					viewBean.setViewLinkEnabled( true );
				}

				releaseViewBeanList.add( viewBean );

			}

			paramBean.setPageInfoView			(
					RmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), envLimit.getLimit() ));
			paramBean.setSelectPageNo			( selectedPageNo );
			paramBean.setReleaseViewBeanList( releaseViewBeanList );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005060S, e );
		}
	}

	/**
	 * 検索条件であるロット情報を取得する。
	 *
	 * @return 取得したロット情報を戻します。
	 */
	private final IEntityLimit<ILotEntity> getPjtLotEntityLimit() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition();
		IEntityLimit<ILotEntity> limit = this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() , null, 1 , 0 ) ;
		return limit ;
	}

	/**
	 * リリース環境情報を取得する。
	 * @param condition
	 * @param sort
	 * @return 全環境情報
	 */
	private final IBldEnvEntity[] getRelEnvAllEntity( IJdbcCondition condition, ISqlSort sort ) {

		List<IBldEnvEntity> entities =
			this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort );

		return entities.toArray(new IBldEnvEntity[0]);
	}


}
