package jp.co.blueship.tri.rm.domain.rp;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.constants.RelCtlScreenItemID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoRelApplySearchBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ・基本情報入力画面の表示情報設定Class<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelCtlEntryServiceBaseInfoInput implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support;
	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelCtlScreenID.BASE_INFO_INPUT ) &&
					!forwordID.equals( RelCtlScreenID.BASE_INFO_INPUT ) ){
				return serviceDto;
			}


			BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();

			//リリース申請情報の検索
			this.searchRelApplyList(paramBean);

			if( refererID.equals( RelCtlScreenID.BASE_INFO_INPUT )) {

				//次へ
				if ( ScreenType.next.equals( screenType )
					&& !forwordID.equals( RelCtlScreenID.BASE_INFO_INPUT )) {
					checkRelBaseInfoInput( baseInfoBean );

					if ( ! TriStringUtils.isEmpty(baseInfoBean.getBaseInfoInputBean().getRelApplyNo()) ) {
						String relApplyNo = baseInfoBean.getBaseInfoInputBean().getRelApplyNo();
						IRaDto raDto = this.support.findRaDto(relApplyNo);

						//リリース申請からリリースする場合、予め選択済みとしておく
						paramBean.setSelectedEnvNo( raDto.getRaEntity().getBldEnvId() );

						List<UnitBean> selectedUnitBeanList	= new ArrayList<UnitBean>();
						for (IRaBpLnkEntity raBpLnkEntity : raDto.getRaBpLnkEntities() ) {
							UnitBean selectedUnitBean = new UnitBean();
							selectedUnitBean.setBuildNo( raBpLnkEntity.getBpId() );
							selectedUnitBean.setBuildName( raBpLnkEntity.getBpNm() );
							selectedUnitBean.setBuildSummary( raBpLnkEntity.getSummary() );
							selectedUnitBean.setBuildContent( raBpLnkEntity.getContent() );

							selectedUnitBeanList.add( selectedUnitBean );

						}
						paramBean.setSelectedUnitBean( selectedUnitBeanList.toArray( new UnitBean[0] ) );

					}
				}

				paramBean.setBaseInfoBean( baseInfoBean );
			}


			if( forwordID.equals( RelCtlScreenID.BASE_INFO_INPUT  )) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005063S, e);
		}
	}

	/**
	 * リリース申請リストの詳細検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchRelApplyList( FlowRelCtlEntryServiceBean paramBean ) {

		BaseInfoBean retBaseInfoBean = paramBean.getBaseInfoBean();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		boolean isSearch = false;

		/*
		 * 条件の作成
		 */
		BaseInfoRelApplySearchBean relApplySearchBean = DBSearchBeanAddonUtil
				.setRelApplySearchBean(paramBean.getBaseInfoBean().getRelApplySearchBean(),
						RmDesignBeanId.rpEntryListCount);

		/*
		 * 選択可能なロット番号リスト取得
		 */

		// 活動中の全ロットを取得
		List<ILotEntity> accessableLotList = null;
		{
			LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();
			List<String> disableLinkLotNumbers	= new ArrayList<String>();

			accessableLotList = this.support.getAccessableLotNumbers( lotCondition, null, support.getUmFinderSupport(), paramBean, false, disableLinkLotNumbers, false );
		}
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		// ロットエンティティ取得
		{
			String[] lotId = this.support.getLotNoSetByReleasableRelUnit(
					this.support.convertToLotNo(accessableLotList) ).toArray( new String[0] );

			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
			ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelEntityLotSelect();
			List<ILotEntity> lotEntities =
				this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
			accessableLotList = lotEntities;
			relApplySearchBean.setSearchLotNoList( ItemLabelsUtils.conv( accessableLotList, srvEntity.getBldSrvId() ) );
		}
		List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( accessableLotList );

		/*
		 * 選択可能なリリース環境リスト取得
		 */
		// リリース環境エンティティ取得
		{
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
			ISqlSort sort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelEntryConfigurationSelect();
			IBldEnvEntity[] envAllEntities = this.support.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort, 1, 0 ).getEntities().toArray(new IBldEnvEntity[0]);

			IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

			relApplySearchBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );
		}

		/*
		 * 選択可能なリリース希望日リスト取得
		 */
		// リリースカレンダーエンティティ取得
		{
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelCalendarCondition();
			ISqlSort sort				= DBSearchSortAddonUtil.getRelCalendarSortFromDesignDefineByRelEntryBaseInfoInput();
			IEntityLimit<ICalEntity> limit =
				this.support.getCalDao().find( condition.getCondition(), sort, 1, 0 );
			ICalEntity[] relCalendarEntitys = limit.getEntities().toArray(new ICalEntity[0]) ;

			relApplySearchBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );
		}

		/*
		 * 空のリリース申請情報一覧を作成
		 */
		List<RelApplyViewBean> releaseApplyViewBeanList = new ArrayList<RelApplyViewBean>();

		String selectedLotNo = relApplySearchBean.getSelectedLotNo();
		String selectedRelEnvNo = relApplySearchBean.getSelectedRelEnvNo();
		String selectedRelWishDate = relApplySearchBean.getSelectedRelWishDate();
		String selectedRelApplyCount = relApplySearchBean.getSelectedRelApplyCount();

		/*
		 * 検索条件有無チェック（検索時のみ）
		 */
		if( ScreenType.select.equals(paramBean.getScreenType()) ) {
			// ロットNo
			if ( TriStringUtils.isEmpty(selectedLotNo) ) {
				messageList.add( RmMessageId.RM001000E );
				messageArgsList.add( new String[] {} );
			}
			// リリース環境No
			if ( TriStringUtils.isEmpty(selectedRelEnvNo) ) {
				messageList.add( RmMessageId.RM001001E );
				messageArgsList.add( new String[] {} );
			}
			// リリース希望日
			if ( TriStringUtils.isEmpty(selectedRelWishDate) ) {
				messageList.add( RmMessageId.RM001002E );
				messageArgsList.add( new String[] {} );
			}
			// 検索件数
			if ( TriStringUtils.isEmpty(selectedRelApplyCount) ) {
				messageList.add( RmMessageId.RM001003E );
				messageArgsList.add( new String[] {} );
			}

			try {
				Integer.valueOf(selectedRelApplyCount);
			} catch (NumberFormatException e) {
				messageList.add( RmMessageId.RM001004E );
				messageArgsList.add( new String[] {} );
			}

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

			isSearch = true;
		}

		BaseInfoBean baseInfoBean = paramBean.getBaseInfoBean();
		if ( StringUtils.isNotEmpty(baseInfoBean.getBaseInfoInputBean().getRelApplyNo()) ) {
			isSearch = true;
		}

		if ( isSearch ) {
			int iSelectedRelApplyCount = Integer.valueOf(selectedRelApplyCount);

			// リリース申請一覧を検索

			// 共通の条件
			int pageNo = 1;
			int viewRows = iSelectedRelApplyCount;
			ISqlSort sort = DBSearchSortAddonUtil.getRelApplySortFromDesignDefineByRelEntryBaseInfoInput();

			// 閲覧可能なリリース申請条件
			RaCondition visibleCondition = new RaCondition();
			visibleCondition.setLotId(selectedLotNo);
			visibleCondition.setBldEnvId(selectedRelEnvNo);
			visibleCondition.setPreferredRelDate(selectedRelWishDate);
			visibleCondition.setProcStsIds(new String[]{
					 RmRaStatusId.ReleaseRequested.getStatusId(),
						RmRaStatusId.ReleaseRequestApproved.getStatusId(),
						RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled.getStatusId(),
						RmRaStatusIdForExecData.ReleasePackageError.getStatusId(),
						RmRaStatusId.ReleasePackageRemoved.getStatusId()
					});

			// 閲覧可能なリリース申請を取得
			IEntityLimit<IRaEntity> raLimit = support.getRaDao().find(visibleCondition.getCondition(), sort, pageNo, viewRows);
			RaDtoList raDtoList = support.findRaDtoList( raLimit.getEntities() );

			List<RelApplyViewBean> vBeanList = new ArrayList<RelApplyViewBean>();
			for (IRaDto raDto : raDtoList) {

				IRaEntity raEntity = raDto.getRaEntity();

				RelApplyViewBean bean = paramBean.newRelApplyViewBean();
				bean.setRelApplyNo( raEntity.getRaId() );
				bean.setRelEnvNo( raEntity.getBldEnvId() );
				bean.setRelEnvName( raEntity.getBldEnvNm() );
				bean.setRelApplyStatusId( raEntity.getProcStsId() );
				bean.setRelApplyStatus(sheet.getValue(	RmDesignBeanId.statusId, raEntity.getProcStsId() ));
				bean.setUnitBeanList(this.getBuildNoList( raDto.getRaBpLnkEntities() ));
				bean.setAssetApplyBeanList(this.getChangeCauseNoList( raDto.getRaBpLnkEntities() ));
				bean.setEditLinkEnabled( false );
				bean.setViewLinkEnabled( true );

				vBeanList.add(bean);
			}

			// 一覧をマージ
			releaseApplyViewBeanList.addAll(vBeanList);
		}

		/*
		 * 戻りに設定
		 */
		retBaseInfoBean.setRelApplySearchBean( relApplySearchBean );
		retBaseInfoBean.setRelApplyViewBeanList( releaseApplyViewBeanList );
	}

	private List<UnitBean> getBuildNoList(List<IRaBpLnkEntity> buildEntity) {

		List<UnitBean> newList = new ArrayList<UnitBean>();
		if( buildEntity == null ) {
			return newList;
		}

		TreeMap<String, UnitBean> newSet = new TreeMap<String, UnitBean>();
		for (IRaBpLnkEntity entity : buildEntity) {

			UnitBean bean = new UnitBean();
			bean.setBuildNo( entity.getBpId() );
			bean.setBuildName( entity.getBpNm() );
			bean.setBuildSummary( entity.getSummary() );
			bean.setBuildContent( entity.getContent() );

			newSet.put(entity.getBpId(), bean);
		}
		newList.addAll(newSet.values());

		return newList;
	}

	private List<AssetApplyBean> getChangeCauseNoList(List<IRaBpLnkEntity> raBpLnkEntities) {

		List<AssetApplyBean> newList = new ArrayList<AssetApplyBean>();
		if( raBpLnkEntities == null ) {
			return newList;
		}

		TreeMap<String, AssetApplyBean> newSet = new TreeMap<String, AssetApplyBean>();
		for (IRaBpLnkEntity raBpLnkEntity : raBpLnkEntities) {

			for ( IAreqEntity areqEntity: support.findAreqEntitiesFromRaBpLnk( raBpLnkEntity ) ) {
				AssetApplyBean bean = new AssetApplyBean();
				bean.setBuildNoKey( raBpLnkEntity.getBpId() );
				bean.setPjtNo( areqEntity.getPjtId() );
				String chgFactorNo = areqEntity.getChgFactorNo();
				bean.setChangeCauseNo( chgFactorNo );

				newSet.put(chgFactorNo, bean);
			}
		}
		newList.addAll(newSet.values());

		return newList;
	}

	/**
	 * リリース基本情報入力項目チェック
	 * @param baseInfoBean リリース基本入力情報
	 */

	private static void checkRelBaseInfoInput( BaseInfoBean baseInfoBean ) {

		BaseInfoInputBean baseInfoInputBean = baseInfoBean.getBaseInfoInputBean();

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							RmDesignBeanId.flowRelCtlBaseInfoInputCheck,
							RelCtlScreenItemID.RelBaseInfoInput.SUMMARY.toString() )) ) {

				if ( TriStringUtils.isEmpty( baseInfoInputBean.getRelSummary() )) {
					messageList.add		( RmMessageId.RM001018E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							RmDesignBeanId.flowRelCtlBaseInfoInputCheck,
							RelCtlScreenItemID.RelBaseInfoInput.CONTENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( baseInfoInputBean.getRelContent() )) {
					messageList.add		( RmMessageId.RM001019E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

}
