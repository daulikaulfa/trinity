package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.beans.mail.dto.RaMailDto;
import jp.co.blueship.tri.rm.beans.mail.dto.RelApplyMailServiceBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請クローズ時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelApplyCloseServiceMail implements IDomain<FlowRelApplyCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelApplyEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyCloseServiceBean> execute( IServiceDto<FlowRelApplyCloseServiceBean> serviceDto ) {

		FlowRelApplyCloseServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.CLOSE_COMP ) &&
					!forwordID.equals( RelApplyScreenID.CLOSE_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.CLOSE_COMP )) {

			}

			if ( forwordID.equals( RelApplyScreenID.CLOSE_COMP )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					// リリース申請情報
					RaDtoList raDtoList = RmExtractEntityAddonUtils.extractRaDtoList( paramBean.getParamList() );
					ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( raDtoList.get(0).getRaEntity().getLotId() );

					//基礎情報のコピー
					RelApplyMailServiceBean successMailBean = new RelApplyMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					successMailBean.setLotEntity	( pjtLotEntity );
					successMailBean.setComment( paramBean.getRelApplyCloseEntryViewBean().getCloseComment() );

					//対象のリリース申請情報（複数）
					List<RaMailDto> relApplyList = new ArrayList<RaMailDto>();
					Map<String, IAreqEntity> assetMap = new TreeMap<String, IAreqEntity>();
					Map<String, IRpEntity> relMap = new TreeMap<String, IRpEntity>();

					for ( IRaDto raDto: raDtoList ) {
						RaMailDto raMailDto = new RaMailDto();
						TriPropertyUtils.copyProperties( raMailDto, raDto );
						raMailDto.setPackageList( this.support.getPackageList(raMailDto) );

						relApplyList.add( raMailDto );

						for ( IAreqEntity assetApply: this.support.getAssetApplyEntity( raMailDto ) ) {
							assetMap.put(assetApply.getAreqId(), assetApply);
						}

						List<IRaRpLnkEntity> raRpLnkEntities = raDto.getRaRpLnkEntities();
						if ( TriCollectionUtils.isNotEmpty(raRpLnkEntities) ) {
							for ( IRaRpLnkEntity raRpLnkEntity: raRpLnkEntities ) {
								String rpId = raRpLnkEntity.getRpId();
								IRpEntity rpEntity = this.support.findRpEntity(rpId);
								relMap.put(rpEntity.getRpId(), rpEntity);
							}
						}
					}

					successMailBean.setRelApplyEntityList( relApplyList );
					successMailBean.setAssetApplyEntityList( new ArrayList<IAreqEntity>( assetMap.values() ) );
					successMailBean.setRelMap( relMap );

					successMail.execute( mailServiceDto );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( RmMessageId.RM005031S, e , paramBean.getFlowAction()) ) ;
		}

		return serviceDto;
	}

}
