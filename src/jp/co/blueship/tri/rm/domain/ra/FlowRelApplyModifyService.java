package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts.AppendFileEnum;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmItemCheckAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * リリース申請・リリース申請詳細編集画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyModifyService implements IDomain<FlowRelApplyModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean> execute( IServiceDto<FlowRelApplyModifyServiceBean> serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelApplyScreenID.MODIFY ) &&
					!forwardID.equals( RelApplyScreenID.MODIFY ) ){
				return serviceDto;
			}

			if( refererID.equals( RelApplyScreenID.MODIFY )) {
				if ( ScreenType.next.equals( screenType ) ) {
					if ( RelApplyScreenID.SAVE_COMP.equals(forwardID) ) {
						RmItemCheckAddonUtils.checkRelApplyModifyInput(
								RmDesignBeanId.flowRelApplySaveInputCheck,
								paramBean.getRelApplySearchBean(),
								paramBean.getRelApplyEntryViewBean() );
					} else {
						RmItemCheckAddonUtils.checkRelApplyModifyInput(
								RmDesignBeanId.flowRelApplyInputCheck,
								paramBean.getRelApplySearchBean(),
								paramBean.getRelApplyEntryViewBean() );
					}

					RmItemCheckAddonUtils.checkRelApplyModifyConfirm(
							support,
							support.getUmFinderSupport(),
							paramBean,
							paramBean.getRelApplySearchBean(),
							paramBean.getRelApplyEntryViewBean());
				}
			}

			if ( refererID.equals( RelApplyScreenID.TOP )
					&& forwardID.equals( RelApplyScreenID.MODIFY ) ) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					String relApplyNo = paramBean.getRelApplySearchBean().getSearchRelApplyNo();
					IRaDto raDto = this.support.findRaDto(relApplyNo);
					ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( raDto.getRaEntity().getLotId() );
					List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( pjtLotEntity.getLotId() );
					lotDtoEntities.add(lotDto);

					String lotName = pjtLotEntity.getLotNm();

					RelApplyEntryViewBean bean = new RelApplyEntryViewBean();

					/*
					 * 表示項目構築
					 */

					IRaEntity raEntity = raDto.getRaEntity();
					List<IAreqEntity> areqEntities = support.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() );

 					bean.setRelApplyNo					( raEntity.getRaId() );
					bean.setLotNo						( raEntity.getLotId() );
					bean.setLotName						( lotName );
					paramBean.setSystemDate				( TriDateUtils.getSystemTimestamp() );
					bean.setRelApplyDate				( TriDateUtils.convertViewDateFormat( paramBean.getSystemDate() ) ) ;
					bean.setRelApplyGroupId				( raEntity.getReqGrpId() );
					bean.setRelEnvNo					( raEntity.getBldEnvId() );
					bean.setRelEnvName					( raEntity.getBldEnvNm() );
					bean.setRelApplyUser				( raEntity.getReqUser() );
					bean.setRelApplyUserId				( raEntity.getRegUserId() );
					bean.setRelApplyPersonCharge		( raEntity.getReqResponsibility() );
					bean.setRelWishDate					( raEntity.getPreferredRelDate() );
					bean.setUnitBeanList				( RmConverterAddonUtils.convertUnitBean( raDto.getRaBpLnkEntities() ) ) ;
					bean.setChangeCauseNoList			( RmConverterAddonUtils.convertChangeCauseNoList( areqEntities ) );
					bean.setAssetApplyNoList			( RmConverterAddonUtils.convertAssetApplyNoList( areqEntities ) );
					bean.setRelatedNo					( raEntity.getRelationId() );
					bean.setRemarks						( raEntity.getRemarks() );
					bean.setAppendFile					( this.getAppendFileMap( raDto.getRaAttachedFileEntities() , bean) );

					/*
					 * 選択可能なリリース環境リスト取得
					 */
					IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
					IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

					paramBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );
					paramBean.getRelApplySearchBean().setSelectedRelEnvNo( bean.getRelEnvNo() );

					/*
					 * 選択可能なリリース希望日リスト取得
					 */
					ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

					paramBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );
					paramBean.getRelApplySearchBean().setSelectedRelWishDate( bean.getRelWishDate() );

					/*
					 * 選択可能なグループリスト取得
					 */
					List<IGrpUserLnkEntity> groupUserview = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					paramBean.setGroupIdList( ItemLabelsUtils.conv( support.getUmFinderSupport(), groupUserview ) );
					paramBean.getRelApplySearchBean().setSelectedGroupId( bean.getRelApplyGroupId() );

					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean(bean);
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005049S, e);
		}
	}

	/**
	 * 添付ファイルマップを取得する
	 *
	 * @param entities
	 * @param bean
	 * @return
	 */
	private Map<String, AppendFileBean> getAppendFileMap(List<IRaAttachedFileEntity> entities, RelApplyEntryViewBean bean) {

		Map<String, AppendFileBean> appendFile = bean.newAppendFile();
		if( entities == null ) {
			return appendFile;
		}

		Integer maxId = 1;
		for (IRaAttachedFileEntity entity : entities) {

			AppendFileBean appendFileBean = bean.newAppendFileBean();
			appendFileBean.setAppendFile( entity.getFilePath() );

			appendFile.put(entity.getAttachedFileSeqNo(), appendFileBean);

			if ( maxId < Integer.valueOf(entity.getAttachedFileSeqNo()) ) {
				maxId = Integer.valueOf(entity.getAttachedFileSeqNo());
			}
		}

		Map<String, String> showMap = bean.getShowAppendFile();

		showMap.put( AppendFileEnum.AppendFile1.getId(), "1" );
		showMap.put( AppendFileEnum.AppendFile2.getId(), ( 1 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile3.getId(), ( 2 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile4.getId(), ( 3 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile5.getId(), ( 4 < maxId )?  "1": "0" );

		return appendFile;
	}

}
