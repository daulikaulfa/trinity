package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;


/**
 * リリース申請・リリース編集完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyModifyServiceComp implements IDomain<FlowRelApplyModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> pojoList = null;

	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean> execute( IServiceDto<FlowRelApplyModifyServiceBean> serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwardID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.MODIFY_COMP ) &&
					!forwardID.equals( RelApplyScreenID.MODIFY_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.MODIFY_CONFIRM )
					&& forwardID.equals( RelApplyScreenID.MODIFY_COMP ) ) {
				//アクション
				List<Object> paramList = new ArrayList<Object>();
				paramList.add( paramBean.getRelApplyEntryViewBean() ) ;
				try {
					// 下層のアクションを実行する
					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
					innerServiceDto.setServiceBean( paramBean );

					{
						for ( IDomain<IGeneralServiceBean> pojo : this.pojoList ) {
							innerServiceDto = pojo.execute( innerServiceDto );
						}
					}
				} finally {

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005050S, e);
		}
	}

}
