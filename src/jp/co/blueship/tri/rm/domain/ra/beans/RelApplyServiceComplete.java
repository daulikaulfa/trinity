package jp.co.blueship.tri.rm.domain.ra.beans;

import java.util.List;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請完了処理Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelApplyServiceComplete implements IDomain<FlowRelApplyEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyEntryServiceBean> execute(IServiceDto<FlowRelApplyEntryServiceBean> serviceDto ) {

		FlowRelApplyEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			/*
			 * 申請情報取得
			 */
			RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

			if ( !refererID.equals( RelApplyScreenID.ENTRY_COMP) &&
					!forwardID.equals( RelApplyScreenID.ENTRY_COMP ) ) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.ENTRY_COMP ) ) {

			}

			if ( refererID.equals( RelApplyScreenID.ENTRY_CONFIRM )
					&& forwardID.equals( RelApplyScreenID.ENTRY_COMP ) ) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					/*
					 * ビルドパッケージに含まれる申請情報の取得
					 */
					List<IBpEntity> buildEntityList = RmConverterAddonUtils.convertBuildEntity( this.support, bean.getUnitBeanList() );
					List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(buildEntityList);
					List<AssetApplyBean> assetApplyBeanList = RmConverterAddonUtils.convertAssetApplyBeanList( this.support, bpDtoList );

					String oldNo = bean.getRelApplyNo();

					// リリース申請番号の採番
					String relApplyNo = this.support.nextvalByRaId();
					bean.setRelApplyNo(relApplyNo);

					String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );
					BusinessFileUtils.moveAppendFile( basePath, oldNo, relApplyNo );

					// リリース申請情報の新規登録
					this.insertRelApply(
							paramBean,
							TriDateUtils.getSystemDate(), assetApplyBeanList);
					this.support.getUmFinderSupport().registerCtgLnk( bean.getCtgId(), RmTables.RM_RA, relApplyNo );
					this.support.getUmFinderSupport().registerMstoneLnk( bean.getMstoneId(), RmTables.RM_RA, relApplyNo );
					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean(bean);
					
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005036S, e);
		}
	}

	/**
	 * リリース申請情報を保存する
	 *
	 */
	private void insertRelApply(
			FlowRelApplyEntryServiceBean paramBean,
			String systemDate,
			List<AssetApplyBean> assetApplyBeanList ) {

		RelApplyEntryViewBean viewBean = paramBean.getRelApplyEntryViewBean();

		IRaEntity entity = new RaEntity() ;

		// 属性
		entity.setLotId				( viewBean.getLotNo() ) ;
		entity.setRaId				( viewBean.getRelApplyNo() ) ;	// 採番した番号
		entity.setReqTimestamp		( paramBean.getSystemDate() ) ;
		entity.setReqUserId			( viewBean.getRelApplyUserId() ) ;
		entity.setReqUser			( viewBean.getRelApplyUser() ) ;
		entity.setReqGrpId			( viewBean.getRelApplyGroupId() ) ;
		entity.setReqGrpNm			( viewBean.getRelApplyGroup() ) ;
		entity.setBldEnvId			( viewBean.getRelEnvNo() ) ;
		entity.setPreferredRelDate	( viewBean.getRelWishDate() ) ;
		entity.setRelationId		( viewBean.getRelatedNo() ) ;

		// システム属性
		entity.setStsId				( RmRaStatusId.ReleaseRequested.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );

		// 要素
		entity.setRemarks( viewBean.getRemarks() ) ;

		// 添付ファイル
		this.support.insertRaAttachedFile( viewBean );

		this.support.deleteRaBpLnk( viewBean );
		this.support.insertRaBpLnk( viewBean );

		// 関連要素(rel)
		// 申請のタイミングでは行わない

		// 関連要素(responsibleUser)
		entity.setReqResponsibility(viewBean.getRelApplyPersonCharge());
		entity.setReqResponsibilityId(viewBean.getRelApplyPersonChargeId());
		// 関連要素(responsibleGroup)
		// 申請承認グループ用 未実装


		this.support.getRaDao().insert( entity );

		// Ra Dto作成
		IRaDto raDto = support.findRaDto(entity.getRaId());
		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Request.getStatusId() );

			support.getRaHistDao().insert( histEntity , raDto);
		}

		//メール送信用にリリース申請をキャッシュする
		paramBean.getParamList().add( raDto );
	}
}
