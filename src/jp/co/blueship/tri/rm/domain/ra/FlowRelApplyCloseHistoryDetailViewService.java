package jp.co.blueship.tri.rm.domain.ra;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean.AppendFileBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseHistoryDetailViewServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請クローズ履歴詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyCloseHistoryDetailViewService implements IDomain<FlowRelApplyCloseHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyCloseHistoryDetailViewServiceBean> execute( IServiceDto<FlowRelApplyCloseHistoryDetailViewServiceBean> serviceDto ) {

		FlowRelApplyCloseHistoryDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelApplyScreenID.CLOSE_HISTORY_DETAIL_VIEW ) &&
					!forwardID.equals( RelApplyScreenID.CLOSE_HISTORY_DETAIL_VIEW ) ){
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.CLOSE_HISTORY_LIST_VIEW )
					&& forwardID.equals( RelApplyScreenID.CLOSE_HISTORY_DETAIL_VIEW ) ) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					String relApplyNo = paramBean.getRelApplySearchBean().getSearchRelApplyNo();
					IRaDto raDto = this.support.findRaDto(relApplyNo);
					ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity(raDto.getRaEntity().getLotId());

					String lotName = pjtLotEntity.getLotNm();

					RelApplyDetailViewBean bean = new RelApplyDetailViewBean();
					paramBean.setRelApplyDetailViewBean(bean);

					IRaEntity raEntity = raDto.getRaEntity();
					List<IAreqEntity> areqEntities = support.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() );

					bean.setRelApplyNo				( raEntity.getRaId() );
					bean.setLotNo					( raDto.getRaEntity().getLotId() );
					bean.setLotName					( lotName );
					bean.setRelApplyDate			( TriDateUtils.convertViewDateFormat( raDto.getRaEntity().getReqTimestamp() ) );
					bean.setRelApplyGroupId			( raEntity.getReqGrpId() );
					bean.setRelApplyGroup			( raEntity.getReqGrpNm() );
					bean.setRelEnvNo				( raEntity.getBldEnvId() );
					bean.setRelEnvName				( raEntity.getBldEnvNm() );
					bean.setRelApplyUser			( raEntity.getRegUserNm() );
					bean.setRelApplyUserId			( raEntity.getRegUserId() );
					bean.setRelApplyPersonCharge	( raEntity.getReqResponsibility() );
					bean.setRelWishDate				( raEntity.getPreferredRelDate() );
					bean.setUnitBeanList			( RmConverterAddonUtils.convertUnitBean( raDto.getRaBpLnkEntities() ) ) ;
					bean.setChangeCauseNoList		( RmConverterAddonUtils.convertChangeCauseNoList( areqEntities ) );
					bean.setAssetApplyNoList		( RmConverterAddonUtils.convertAssetApplyNoList( areqEntities ) );
					bean.setRelatedNo				( raEntity.getRelationId() );
					bean.setRemarks					( raEntity.getRemarks() );
					bean.setAppendFile				( this.getAppendFileList( raDto.getRaAttachedFileEntities() , bean) );
					this.support.setAppendFileLink	( paramBean.getRelApplyDetailViewBean() );

					bean.setRelBeanList				( RmConverterAddonUtils.convertRelBean(this.support, raDto.getRaRpLnkEntities() ) ) ;

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005039S, e);
		}
	}

	/**
	 * 添付ファイルマップを取得する
	 *
	 * @param entities
	 * @param bean
	 * @return
	 */
	private Map<String, AppendFileBean> getAppendFileList(List<IRaAttachedFileEntity> entities, RelApplyDetailViewBean bean) {

		Map<String, AppendFileBean> appendFile = bean.newAppendFile();
		if( entities == null ) {
			return appendFile;
		}

		for (IRaAttachedFileEntity entity : entities) {

			AppendFileBean appendFileBean = bean.newAppendFileBean();
			appendFileBean.setAppendFile( entity.getFilePath() );

			appendFile.put(entity.getAttachedFileSeqNo(), appendFileBean);
		}

		return appendFile;
	}

}
