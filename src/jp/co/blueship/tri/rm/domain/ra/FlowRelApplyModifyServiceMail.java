package jp.co.blueship.tri.rm.domain.ra;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.beans.mail.dto.RaMailDto;
import jp.co.blueship.tri.rm.beans.mail.dto.RelApplyMailServiceBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelApplyModifyServiceMail implements IDomain<FlowRelApplyModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelApplyEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean> execute( IServiceDto<FlowRelApplyModifyServiceBean> serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.MODIFY_COMP ) &&
					!forwordID.equals( RelApplyScreenID.MODIFY_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.MODIFY_COMP )) {

			}

			if ( forwordID.equals( RelApplyScreenID.MODIFY_COMP )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					// ビルドパッケージ情報
					IRaDto raDto = RmExtractEntityAddonUtils.extractRaDto( paramBean.getParamList() );
					ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( raDto.getRaEntity().getLotId() );

					//基礎情報のコピー
					RelApplyMailServiceBean successMailBean = new RelApplyMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					successMailBean.setLotEntity	( pjtLotEntity );

					RaMailDto raMailDto = new RaMailDto();
					TriPropertyUtils.copyProperties( raMailDto, raDto );
					raMailDto.setPackageList( this.support.getPackageList(raMailDto) );
					successMailBean.setRelApplyEntity( raMailDto );

					successMailBean.setAssetApplyEntityList( this.support.getAssetApplyEntity( raDto ) );

					successMail.execute( mailServiceDto );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( RmMessageId.RM005031S, e ,paramBean.getFlowAction()) ) ;
		}

		return serviceDto;
	}

}
