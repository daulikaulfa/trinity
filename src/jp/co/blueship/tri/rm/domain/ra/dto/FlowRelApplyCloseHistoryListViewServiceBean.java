package jp.co.blueship.tri.rm.domain.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;
/**
 * リリース申請・クローズ履歴一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyCloseHistoryListViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList=null;	
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RelApplyViewBean newRelApplyViewBean() {
		RelApplyViewBean bean = new RelApplyViewBean();
		return bean;
	}

	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}
	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}

	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		if ( null == relApplyViewBeanList ) {
			relApplyViewBeanList = new ArrayList<RelApplyViewBean>();
		}
		return relApplyViewBeanList;
	}
	public void setRelApplyViewBeanList(
			List<RelApplyViewBean> releaseApplyViewBeanList ) {
		this.relApplyViewBeanList = releaseApplyViewBeanList;
	}	
		
	public class RelApplyViewBean {

		/** リリース申請番号 */
		private String relApplyNo = null;		
		/** 環境番号 */
		private String relEnvNo = null;
		/** 環境名 */
		private String relEnvName = null;
		/** リリース番号 */
		private String relNo = null;
		/** リリース登録日 */
		private String relInputDate = null;
		
		/** ビルドパッケージ情報 */
		private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
		
		/** 申請情報 */
		private List<AssetApplyBean> assetApplyBeanList = new ArrayList<AssetApplyBean>();
		
		/** ビルドパッケージ作成日 */
		private List<String> buildDateList = new ArrayList<String>();
		/** リリース申請ステータス */
		private String relApplyStatus = null;
		/** リリース申請ステータスID */
		private String relApplyStatusId = null;
		
		/**
		 * このロットの閲覧・編集リンクを有効にするかどうか
		 */
		private boolean isEditLinkEnabled = false;
		/**
		 * このロットの閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;
		
		public List<AssetApplyBean> getAssetApplyBeanList() {
			return assetApplyBeanList;
		}
		public void setAssetApplyBeanList(List<AssetApplyBean> assetApplyBeanList) {
			this.assetApplyBeanList = assetApplyBeanList;
		}
		public List<String> getBuildDateList() {
			return buildDateList;
		}
		public void setBuildDateList(List<String> buildDateList) {
			this.buildDateList = buildDateList;
		}
		public boolean isEditLinkEnabled() {
			return isEditLinkEnabled;
		}
		public void setEditLinkEnabled(boolean isEditLinkEnabled) {
			this.isEditLinkEnabled = isEditLinkEnabled;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled(boolean isViewLinkEnabled) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
		public String getRelApplyNo() {
			return relApplyNo;
		}
		public void setRelApplyNo(String relApplyNo) {
			this.relApplyNo = relApplyNo;
		}
		public String getRelApplyStatus() {
			return relApplyStatus;
		}
		public void setRelApplyStatus(String relApplyStatus) {
			this.relApplyStatus = relApplyStatus;
		}
		public String getRelApplyStatusId() {
			return relApplyStatusId;
		}
		public void setRelApplyStatusId(String relApplyStatusId) {
			this.relApplyStatusId = relApplyStatusId;
		}
		public String getRelEnvName() {
			return relEnvName;
		}
		public void setRelEnvName(String relEnvName) {
			this.relEnvName = relEnvName;
		}
		public String getRelEnvNo() {
			return relEnvNo;
		}
		public void setRelEnvNo(String relEnvNo) {
			this.relEnvNo = relEnvNo;
		}
		public String getRelInputDate() {
			return relInputDate;
		}
		public void setRelInputDate(String relInputDate) {
			this.relInputDate = relInputDate;
		}
		public String getRelNo() {
			return relNo;
		}
		public void setRelNo(String relNo) {
			this.relNo = relNo;
		}
		public List<UnitBean> getUnitBeanList() {
			return unitBeanList;
		}
		public void setUnitBeanList(List<UnitBean> unitBeanList) {
			this.unitBeanList = unitBeanList;
		}
		
		

	}
}
