package jp.co.blueship.tri.rm.domain.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;
/**
 * リリース申請・トップ画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyTopServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;
	
	
	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}
	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}
	
	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		if ( null == relApplyViewBeanList ) {
			relApplyViewBeanList = new ArrayList<RelApplyViewBean>();
		}
		return relApplyViewBeanList;
	}
	public void setRelApplyViewBeanList(
			List<RelApplyViewBean> relApplyViewBeanList ) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RelApplyViewBean newRelApplyViewBean() {
		RelApplyViewBean bean = new RelApplyViewBean();
		return bean;
	}
	
	public class RelApplyViewBean {

		/** リリース申請番号 */
		private String relApplyNo = null;
		/** 環境番号 */
		private String relEnvNo = null;
		/** 環境名 */
		private String relEnvName = null;
		/** リリース登録日 */
		private String relInputDate = null;
		/** ビルドパッケージ情報 */
		private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
		/** 資産申請情報 */
		private List<AssetApplyBean> assetApplyBeanList = new ArrayList<AssetApplyBean>();
		/** ステータスID */
		private String relApplyStatusId = null;
		/** ステータス */
		private String relApplyStatus = null;
		
		/**
		 * このロットの閲覧・編集リンクを有効にするかどうか
		 */
		private boolean isEditLinkEnabled = false;
		/**
		 * このロットの閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;
		
		
		public boolean isEditLinkEnabled() {
			return isEditLinkEnabled;
		}
		public void setEditLinkEnabled(boolean isEditLinkEnabled) {
			this.isEditLinkEnabled = isEditLinkEnabled;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled(boolean isViewLinkEnabled) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
		public void setRelApplyNo( String relNo ) {
			this.relApplyNo = relNo;
		}
		public String getRelApplyNo() {
			return relApplyNo;
		}

		public void setRelEnvNo( String relEnvNo ) {
			this.relEnvNo = relEnvNo;
		}
		public String getRelEnvNo() {
			return relEnvNo;
		}

		public void setRelEnvName( String relEnvName ) {
			this.relEnvName = relEnvName;
		}
		public String getRelEnvName() {
			return relEnvName;
		}

		public void setRelInputDate( String relInputDate ) {
			this.relInputDate = relInputDate;
		}
		public String getRelInputDate() {
			return relInputDate;
		}
		
		public void setUnitBeanList( List<UnitBean> unitBeanList ) {
			this.unitBeanList = unitBeanList;
		}
		public List<UnitBean> getUnitBeanList() {
			return unitBeanList;
		}
		
		public void setAssetApplyBeanList( List<AssetApplyBean> assetApplyBeanList ) {
			this.assetApplyBeanList = assetApplyBeanList;
		}
		public List<AssetApplyBean> getAssetApplyBeanList() {
			return assetApplyBeanList;
		}
		
		public String getRelApplyStatus() {
			return relApplyStatus;
		}
		public void setRelApplyStatus(String relApplyStatus) {
			this.relApplyStatus = relApplyStatus;
		}
		
		public String getRelApplyStatusId() {
			return relApplyStatusId;
		}
		public void setRelApplyStatusId(String relApplyStatusId) {
			this.relApplyStatusId = relApplyStatusId;
		}
	}

}
