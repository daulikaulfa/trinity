package jp.co.blueship.tri.rm.domain.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;
/**
 * リリース申請・クローズ画面／クローズ完了画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyCloseServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** リリース申請情報 */
	private RelApplyViewBean relApplyViewBean=null;

	/** リリース申請クローズ入力情報 */
	private RelApplyCloseEntryViewBean relApplyCloseEntryViewBean=null;

	/** リリース申請検索情報 */
	private RelApplySearchBean relApplySearchBean=null;

	/** リリース申請情報 */
	private List<RelApplyViewBean> relApplyViewBeanList = null;

	/**
	 * @return relApplyViewBean
	 */
	public RelApplyViewBean getRelApplyViewBean() {
		return relApplyViewBean;
	}

	/**
	 * @param relApplyViewBean 設定する relApplyViewBean
	 */
	public void setRelApplyViewBean(RelApplyViewBean relApplyViewBean) {
		this.relApplyViewBean = relApplyViewBean;
	}	
	
	/**
	 * @return relApplySearchBean
	 */
	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}

	/**
	 * @param relApplySearchBean 設定する relApplySearchBean
	 */
	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}

	public List<RelApplyViewBean> getRelApplyViewBeanList() {
		return relApplyViewBeanList;
	}

	public void setRelApplyViewBeanList(List<RelApplyViewBean> relApplyViewBeanList) {
		this.relApplyViewBeanList = relApplyViewBeanList;
	}	
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RelApplyViewBean newRelApplyViewBean() {
		RelApplyViewBean bean = new RelApplyViewBean();
		return bean;
	}
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RelApplyCloseEntryViewBean newRelApplyCloseEntryViewBean() {
		RelApplyCloseEntryViewBean bean = new RelApplyCloseEntryViewBean();
		return bean;
	}
	
	public class RelApplyViewBean {

		/** ロット番号 */
		private String lotId = null;
		/** ロット名 */
		private String lotName = null;
		/** 選択されたリリース申請番号 */
		private String[] selectedRelApplyNo = null;
		/** リリース申請番号 */
		private String relApplyNo = null;
		/** リリース申請日時 */
		private String relApplyDate = null;		
		/** リリース申請グループ */
		private String  relApplyGroupName = null;
		/** リリース環境番号 */
		private String envNo = null;
		/** リリース環境名 */
		private String envName = null;
		/** リリース申請グループID */
		private String  relApplyGroupId = null;
		/** リリース申請者名 */
		private String  relApplyUser = null;
		/** リリース申請者ID */
		private String  relApplyUserId = null;
		/** リリース申請責任者名 */
		private String  responsibleUser = null;
		/** リリース申請責任者ID */
		private String  responsibleUserId = null;
		/** リリース希望日 */
		private String  relWishDate = null;
		/** ビルドパッケージリスト */
		private List<UnitBean> unitBeanList = new ArrayList<UnitBean>();
		/** ビルドパッケージ作成日 */
		private List<String> buildDateList = new ArrayList<String>();
		/** 変更要因番号 */
		private List<AssetApplyBean> changeCouseNoNameBeanList = new ArrayList<AssetApplyBean>();
		/** 貸出申請番号 */
		private List<String> applyNoBeanList = new ArrayList<String>();
		/** 関連番号 */
		private String relationNo = null;
		/** 備考 */
		private String relApplyRemarks = null;		
		/** 添付ファイル */
		private List<UnitBean> relApplyappendFileList = new ArrayList<UnitBean>();
		/** リリース番号 */
		private List<RelBean> relNoBeanList = new ArrayList<RelBean>();
		/** リリース申請ステータス */
		private String relApplyStatusId = null;
		/** リリース申請ステータス */
		private String relApplyStatus = null;

		
		
		public List<String> getApplyNoBeanList() {
			return applyNoBeanList;
		}

		public void setApplyNoBeanList(List<String> applyNoBeanList) {
			this.applyNoBeanList = applyNoBeanList;
		}

		public List<String> getBuildDateList() {
			return buildDateList;
		}

		public void setBuildDateList(List<String> buildDateList) {
			this.buildDateList = buildDateList;
		}

		public List<AssetApplyBean> getChangeCouseNoNameBeanList() {
			return changeCouseNoNameBeanList;
		}

		public void setChangeCouseNoNameBeanList(List<AssetApplyBean> changeCouseNoNameBeanList) {
			this.changeCouseNoNameBeanList = changeCouseNoNameBeanList;
		}

		public String getEnvName() {
			return envName;
		}

		public void setEnvName(String envName) {
			this.envName = envName;
		}

		public String getEnvNo() {
			return envNo;
		}

		public void setEnvNo(String envNo) {
			this.envNo = envNo;
		}

		public String getLotName() {
			return lotName;
		}

		public void setLotName(String lotName) {
			this.lotName = lotName;
		}

		public String getLotNo() {
			return lotId;
		}

		public void setLotNo(String lotId) {
			this.lotId = lotId;
		}

		public List<UnitBean> getRelApplyappendFileList() {
			return relApplyappendFileList;
		}

		public void setRelApplyappendFileList(List<UnitBean> relApplyappendFileList) {
			this.relApplyappendFileList = relApplyappendFileList;
		}

		public String getRelApplyDate() {
			return relApplyDate;
		}

		public void setRelApplyDate(String relApplyDate) {
			this.relApplyDate = relApplyDate;
		}

		public String getRelApplyGroupId() {
			return relApplyGroupId;
		}

		public void setRelApplyGroupId(String relApplyGroupId) {
			this.relApplyGroupId = relApplyGroupId;
		}

		public String getRelApplyGroupName() {
			return relApplyGroupName;
		}

		public void setRelApplyGroupName(String relApplyGroupName) {
			this.relApplyGroupName = relApplyGroupName;
		}

		public String getRelApplyNo() {
			return relApplyNo;
		}

		public void setRelApplyNo(String relApplyNo) {
			this.relApplyNo = relApplyNo;
		}

		public String getRelApplyRemarks() {
			return relApplyRemarks;
		}

		public void setRelApplyRemarks(String relApplyRemarks) {
			this.relApplyRemarks = relApplyRemarks;
		}

		public String getRelApplyUser() {
			return relApplyUser;
		}

		public void setRelApplyUser(String relApplyUser) {
			this.relApplyUser = relApplyUser;
		}

		public String getRelApplyUserId() {
			return relApplyUserId;
		}

		public void setRelApplyUserId(String relApplyUserId) {
			this.relApplyUserId = relApplyUserId;
		}

		public String getRelationNo() {
			return relationNo;
		}

		public void setRelationNo(String relationNo) {
			this.relationNo = relationNo;
		}

		public List<RelBean> getRelNoBeanList() {
			return relNoBeanList;
		}

		public void setRelNoBeanList(List<RelBean> relNoBeanList) {
			this.relNoBeanList = relNoBeanList;
		}

		public String getRelWishDate() {
			return relWishDate;
		}

		public void setRelWishDate(String relWishDate) {
			this.relWishDate = relWishDate;
		}

		public String getResponsibleUser() {
			return responsibleUser;
		}

		public void setResponsibleUser(String responsibleUser) {
			this.responsibleUser = responsibleUser;
		}

		public String getResponsibleUserId() {
			return responsibleUserId;
		}

		public void setResponsibleUserId(String responsibleUserId) {
			this.responsibleUserId = responsibleUserId;
		}

		public String[] getSelectedRelApplyNo() {
			return selectedRelApplyNo;
		}

		public void setSelectedRelApplyNo(String[] selectedRelApplyNo) {
			this.selectedRelApplyNo = selectedRelApplyNo;
		}

		public List<UnitBean> getUnitBeanList() {
			return unitBeanList;
		}

		public void setUnitBeanList(List<UnitBean> unitBeanList) {
			this.unitBeanList = unitBeanList;
		}

		public String getRelApplyStatusId() {
			return relApplyStatusId;
		}

		public void setRelApplyStatusId(String relApplyStatus) {
			this.relApplyStatusId = relApplyStatus;
		}

		public String getRelApplyStatus() {
			return relApplyStatus;
		}

		public void setRelApplyStatus(String relApplyStatus) {
			this.relApplyStatus = relApplyStatus;
		}

		
	}
		
	public class RelApplyCloseEntryViewBean {

		/** コメント */
		private String closeComment = null;

		public String getCloseComment() {
			return closeComment;
		}

		public void setCloseComment(String closeComment) {
			this.closeComment = closeComment;
		}		
		
	}

	public RelApplyCloseEntryViewBean getRelApplyCloseEntryViewBean() {
		return relApplyCloseEntryViewBean;
	}

	public void setRelApplyCloseEntryViewBean(
			RelApplyCloseEntryViewBean relApplyCloseEntryViewBean) {
		this.relApplyCloseEntryViewBean = relApplyCloseEntryViewBean;
	}
		

}

