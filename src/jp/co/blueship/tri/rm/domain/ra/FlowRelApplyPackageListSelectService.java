package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean.UnitViewBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * リリース申請・リリース申請パッケージ選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelApplyPackageListSelectService implements IDomain<FlowRelApplyEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyEntryServiceBean> execute( IServiceDto<FlowRelApplyEntryServiceBean> serviceDto ) {

		FlowRelApplyEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			/*
			 * 条件の作成
			 */
			RelApplyPackageListSearchBean searchBean = DBSearchBeanAddonUtil
					.setRelUnitSearchBeanByRelApplyEntry(paramBean.getRelApplyPackageListSearchBean());

			if ( !refererID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT ) &&
					!forwardID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT ) ){
				return serviceDto;
			}


			if( refererID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT )) {

				if ( ScreenType.next.equals( screenType ) ) {

					if( null != searchBean ) {
						ItemCheckAddonUtils.checkBuildNo( searchBean.getSelectedBuildNo() );
					}
				}
			}

			if ( refererID.equals( RelApplyScreenID.TOP )
					&& forwardID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT ) ) {
				this.searchRelApplyList(paramBean);
			}

			if ( refererID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT )
					&& forwardID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT ) ) {
				if ( ScreenType.select.equals( screenType ) ) {

					this.searchRelApplyList(paramBean);
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005052S, e);
		}
	}

	/**
	 * リリース申請リストの詳細検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchRelApplyList( FlowRelApplyEntryServiceBean paramBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		boolean isSearch = false;

		/*
		 * 条件の作成
		 */
		RelApplyPackageListSearchBean searchBean = DBSearchBeanAddonUtil
				.setRelUnitSearchBeanByRelApplyEntry(paramBean.getRelApplyPackageListSearchBean());

		//検索件数のリストアップ
		List<ItemLabelsBean> countList = DesignDefineSearchUtils.mappingItemLabelsBeanByKeyParseInt( RmDesignBeanId.raPackageListSelectListCount );
		paramBean.setSearchRelUnitCountList( countList ) ;

		//全体検索件数の選択
		if( TriStringUtils.isEmpty(searchBean.getSelectedRelUnitCount()) || null == paramBean.getSearchRelUnitCountList() ) {
			//初期表示時のみ
			Map<String,String> srcIdMap = sheet.getKeyMap( RmDesignBeanId.raPackageListSelectListCount );

			List<String> selectedCount = DesignDefineSearchUtils.extractKey( RmDesignBeanId.raPackageListSelectListCount, StatusFlg.on.value() ) ;
			searchBean.setSelectedRelUnitCount( ( 0 == selectedCount.size() )? srcIdMap.keySet().iterator().next(): selectedCount.get( 0 ) ) ;
		}

		/*
		 * 選択可能なロット番号リスト取得
		 */
		// グループ情報
		List<IGrpUserLnkEntity> groupUserEntitys = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );

		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		// ロットエンティティ取得
		{
			String[] lotId = this.support.getLotNoByReleasableRelUnit();
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
			ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelApplyTop();
			List<ILotEntity> lotEntities =
				this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
			List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );

			// グループによるロットアクセスの絞込み
			List<ILotEntity> lotList = new ArrayList<ILotEntity>();
			ViewInfoAddonUtil.setAccessablePjtLotEntity( lotDto, groupUserEntitys, lotList, null );

			paramBean.setSearchLotNoList( ItemLabelsUtils.conv(lotList, srvEntity.getBldSrvId() ) );
		}

		/*
		 * 空のビルドパッケージ情報一覧を作成
		 */
		List<UnitViewBean> unitViewBeanList = new ArrayList<UnitViewBean>() ;

		String selectedLotNo = searchBean.getSelectedLotNo();
		String selectedRelApplyCount = searchBean.getSelectedRelUnitCount();

		/*
		 * 検索条件有無チェック（検索時のみ）
		 */
		if( ScreenType.select.equals(paramBean.getScreenType()) ) {

			// ロットNo
			if ( TriStringUtils.isEmpty(selectedLotNo) ) {
				messageList.add( RmMessageId.RM001000E );
				messageArgsList.add( new String[] {} );
			}
			// 検索件数
			if ( TriStringUtils.isEmpty(selectedRelApplyCount) ) {
				messageList.add( RmMessageId.RM001003E );
				messageArgsList.add( new String[] {} );
			}

			try {
				Integer.valueOf(selectedRelApplyCount);
			} catch (NumberFormatException e) {
				messageList.add( RmMessageId.RM001004E );
				messageArgsList.add( new String[] {} );
			}

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

			isSearch = true;
		}

		if ( isSearch ) {
			int iSelectedRelApplyCount = 0;
			if(paramBean.isStatusMatrixV3()) {
				iSelectedRelApplyCount = Integer.valueOf(selectedRelApplyCount);
			}


			// ビルドパッケージ一覧を検索
			IBpDao buildDao = this.support.getBmFinderSupport().getBpDao();
			int pageNo = 1;
			int viewRows = iSelectedRelApplyCount;
			ISqlSort buildSort	= (ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelApplyPackageListSelect();
			IJdbcCondition buildCondition = DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit( new String[]{selectedLotNo} ) ;
			List<IBpEntity> buildEntityArray = buildDao.find( buildCondition.getCondition() , buildSort , pageNo, viewRows ).getEntities();
			List<IBpDto> bpDtoList= this.support.getBmFinderSupport().findBpDto(buildEntityArray);
			for (IBpDto entity : bpDtoList) {

				List<IBpAreqLnkEntity> buildApplyEntity = entity.getBpAreqLnkEntities();

				UnitViewBean bean = paramBean.newUnitViewBean();
				bean.setBuildNo( entity.getBpEntity().getBpId() );
				bean.setBuildName( entity.getBpEntity().getBpNm() );
				bean.setChangeCauseNoList( RmConverterAddonUtils.convertChangeCauseNoList( this.support, buildApplyEntity ) );
				bean.setAssetApplyNoList(new ArrayList<String>( FluentList.from(this.support.getApplyNo( entity )).asList()));

				unitViewBeanList.add(bean);
			}

		}

		/*
		 * 戻りに設定
		 */
		paramBean.setRelApplyPackageListSearchBean( searchBean );
		paramBean.setUnitViewBeanList( unitViewBeanList ) ;
	}

}
