package jp.co.blueship.tri.rm.domain.ra;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.RmItemCheckAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelApplyEntryServiceConfirm implements IDomain<FlowRelApplyEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRelApplyEntryServiceBean> execute( IServiceDto<FlowRelApplyEntryServiceBean> serviceDto ) {

		FlowRelApplyEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( refererID.equals( RelApplyScreenID.ENTRY_CONFIRM )) {

				if ( ScreenType.next.equals( screenType )) {
					RelApplyEntryViewBean viewBean = paramBean.getRelApplyEntryViewBean();

					RmItemCheckAddonUtils.checkRelApplyEntryConfirm(
							support,
							support.getUmFinderSupport(),
							paramBean,
							paramBean.getRelApplyPackageListSearchBean(),
							paramBean.getRelApplyEntryViewBean());

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( viewBean.getLotNo() )
						.setPjtIds( (null == viewBean.getPjtNoList())? null: viewBean.getPjtNoList().toArray(new String[0]) )
						.setAreqIds( (null == viewBean.getAssetApplyNoList())? null: viewBean.getAssetApplyNoList().toArray(new String[0]) )
						.setBpIds( support.getBuildNo(viewBean.getUnitBeanList() ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( refererID.equals( RelApplyScreenID.ENTRY )
					&& forwardID.equals( RelApplyScreenID.ENTRY_CONFIRM ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					this.executeForward( paramBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005047S, e);
		}
	}

	/**
	 * 当画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */
	private final void executeForward( FlowRelApplyEntryServiceBean paramBean ) {

		RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

		// リリース申請仮番号の採番
		if ( TriStringUtils.isEmpty(bean.getRelApplyNo()) ) {
			//確認画面で添付ファイルを格納するために仮番を発行しているため、再度採番されないようにする
			String relApplyNo = this.support.nextvalByRaTempId();
			bean.setRelApplyNo(relApplyNo);
		}

		//添付ファイル保存
		this.support.saveUploadFile( bean, bean, false );
		this.support.setAppendFileLink( bean );

		/*
		 * 戻りに設定
		 */
		paramBean.setRelApplyEntryViewBean(bean);

	}

}
