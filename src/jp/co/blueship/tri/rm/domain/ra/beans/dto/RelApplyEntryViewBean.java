package jp.co.blueship.tri.rm.domain.ra.beans.dto;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts.AppendFileEnum;

public class RelApplyEntryViewBean {

	private String lotId = null;
	private String lotName = null;
	private String relApplyNo = null;
	private String relApplyDate = null;
	private String relApplyGroupId = null;
	private String relApplyGroup = null;
	private String relEnvNo = null;
	private String relEnvName = null;
	private String relApplyUserId = null;
	private String relApplyUser = null;
	private String relApplyPersonChargeId = null;
	private String relApplyPersonCharge = null;
	private String relWishDate = null;
	private List<UnitBean> unitBeanList = null;
	private List<String> pjtNoList = null;
	private List<String> changeCauseNoList = null;
	private List<String> assetApplyNoList = null;
	private String relatedNo = null;
	private String remarks = null;
	private String mstoneId = null;
	private String ctgId = null;
	private boolean isDraff = false;
	private Map<String, AppendFileBean> appendFile = new TreeMap<String, AppendFileBean>();
	/** 添付ファイルを削除する／しない */
	private String[] delAppendFiles = new String[0];

	/** 添付ファイルを表示する／しない */
	private Map<String, String> showAppendFile = new TreeMap<String, String>();
	{
		for ( AppendFileEnum appendFile : AppendFileEnum.values() ) {
			showAppendFile.put(appendFile.getId(), "0");
		}

		showAppendFile.put(AppendFileEnum.AppendFile1.getId(), "1");
	}

	public class AppendFileBean {
		/**
		 * 添付ファイルの名前
		 */
		private String appendFile;
		/**
		 * 添付ファイルリンク
		 */
		private String appendFileLink;

		/**
		 * 添付ファイルのストリーム
		 */
		private byte[] appendFileInputStreamBytes;
		/**
		 * 添付ファイルのストリーム名
		 */
		private String appendFileInputStreamName;

		public String getAppendFile() {
			return appendFile;
		}
		public void setAppendFile(String appendFile) {
			this.appendFile = appendFile;
		}
		public byte[] getAppendFileInputStreamBytes() {
			return appendFileInputStreamBytes;
		}
		public void setAppendFileInputStreamBytes(byte[] appendFileInputStreamBytes) {
			this.appendFileInputStreamBytes = appendFileInputStreamBytes;
		}
		public String getAppendFileInputStreamName() {
			return appendFileInputStreamName;
		}
		public void setAppendFileInputStreamName(String appendFileInputStreamName) {
			this.appendFileInputStreamName = appendFileInputStreamName;
		}
		public String getAppendFileLink() {
			return appendFileLink;
		}
		public void setAppendFileLink(String appendFileLink) {
			this.appendFileLink = appendFileLink;
		}
	}

	public Map<String, AppendFileBean> newAppendFile() {
		return new TreeMap<String, AppendFileBean>();
	}

	public String getRelApplyAppendFile1() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile1.getId();

		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFile(): null;
	}

	public String getRelApplyAppendFile2() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile2.getId();

		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFile(): null;
	}

	public String getRelApplyAppendFile3() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile3.getId();

		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFile(): null;
	}

	public String getRelApplyAppendFile4() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile4.getId();

		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFile(): null;
	}

	public String getRelApplyAppendFile5() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile5.getId();

		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFile(): null;
	}

	public String getShowAppendFile1() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile1.getId();

		return showAppendFile.get(id);
	}

	public String getShowAppendFile2() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile2.getId();

		return showAppendFile.get(id);
	}

	public String getShowAppendFile3() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile3.getId();

		return showAppendFile.get(id);
	}
	public String getShowAppendFile4() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile4.getId();

		return showAppendFile.get(id);
	}

	public String getShowAppendFile5() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile5.getId();

		return showAppendFile.get(id);
	}

	public String getRelApplyAppendFileLink1() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile1.getId();
		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFileLink(): null;
	}

	public String getRelApplyAppendFileLink2() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile2.getId();
		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFileLink(): null;
	}

	public String getRelApplyAppendFileLink3() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile3.getId();
		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFileLink(): null;
	}

	public String getRelApplyAppendFileLink4() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile4.getId();
		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFileLink(): null;
	}

	public String getRelApplyAppendFileLink5() {
		String id = SessionScopeKeyConsts.AppendFileEnum.AppendFile5.getId();
		return (appendFile.containsKey(id))? appendFile.get(id).getAppendFileLink(): null;
	}

	public AppendFileBean newAppendFileBean() {
		return new AppendFileBean();
	}

	public String getRelApplyDate() {
		return relApplyDate;
	}
	public void setRelApplyDate(String relApplyDate) {
		this.relApplyDate = relApplyDate;
	}
	public String getRelApplyNo() {
		return relApplyNo;
	}
	public void setRelApplyNo(String relApplyNo) {
		this.relApplyNo = relApplyNo;
	}
	public String getRelatedNo() {
		return relatedNo;
	}
	public void setRelatedNo(String relatedNo) {
		this.relatedNo = relatedNo;
	}

	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRelApplyGroupId() {
		return relApplyGroupId;
	}
	public void setRelApplyGroupId(String relApplyGroupId) {
		this.relApplyGroupId = relApplyGroupId;
	}
	public String getRelEnvNo() {
		return relEnvNo;
	}
	public void setRelEnvNo(String relEnvNo) {
		this.relEnvNo = relEnvNo;
	}
	public String getRelWishDate() {
		return relWishDate;
	}
	public void setRelWishDate(String relWishDate) {
		this.relWishDate = relWishDate;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	public String getRelApplyPersonCharge() {
		return relApplyPersonCharge;
	}
	public void setRelApplyPersonCharge(String relApplyPersonCharge) {
		this.relApplyPersonCharge = relApplyPersonCharge;
	}
	public String getRelApplyUser() {
		return relApplyUser;
	}
	public void setRelApplyUser(String relApplyUser) {
		this.relApplyUser = relApplyUser;
	}
	public List<String> getAssetApplyNoList() {
		return assetApplyNoList;
	}
	public void setAssetApplyNoList(List<String> assetApplyNoList) {
		this.assetApplyNoList = assetApplyNoList;
	}
	public List<String> getChangeCauseNoList() {
		return changeCauseNoList;
	}
	public void setChangeCauseNoList(List<String> changeCauseNoList) {
		this.changeCauseNoList = changeCauseNoList;
	}
	public List<String> getPjtNoList() {
		return pjtNoList;
	}
	public void setPjtNoList(List<String> pjtNoList) {
		this.pjtNoList = pjtNoList;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getRelApplyPersonChargeId() {
		return relApplyPersonChargeId;
	}
	public void setRelApplyPersonChargeId(String relApplyPersonChargeId) {
		this.relApplyPersonChargeId = relApplyPersonChargeId;
	}
	public String getRelApplyUserId() {
		return relApplyUserId;
	}
	public void setRelApplyUserId(String relApplyUserId) {
		this.relApplyUserId = relApplyUserId;
	}
	public List<UnitBean> getUnitBeanList() {
		return unitBeanList;
	}
	public void setUnitBeanList(List<UnitBean> unitBeanList) {
		this.unitBeanList = unitBeanList;
	}
	public String getRelApplyGroup() {
		return relApplyGroup;
	}
	public void setRelApplyGroup(String relApplyGroup) {
		this.relApplyGroup = relApplyGroup;
	}
	public String getRelEnvName() {
		return relEnvName;
	}
	public void setRelEnvName(String relEnvName) {
		this.relEnvName = relEnvName;
	}
	public Map<String, AppendFileBean> getAppendFile() {
		return appendFile;
	}
	public void setAppendFile(Map<String, AppendFileBean> appendFile) {
		this.appendFile = appendFile;
	}

	public String[] getDelAppendFiles() {
		if ( null == delAppendFiles ) {
			return new String[0];
		}

		return delAppendFiles;
	}

	public void setDelAppendFiles(String[] delAppendFiles) {
		if ( null == delAppendFiles ) {
			this.delAppendFiles = new String[0];
		} else {
			this.delAppendFiles = delAppendFiles;
		}
	}

	public String getSelectedDelAppendIdString() {
		int i = 0;
		StringBuilder buf = new StringBuilder();
		buf.append("[");
		for(String id :delAppendFiles) {
			if (i > 0) {
				buf.append(",");
			}
			buf.append(id);
			i++;
		}
		buf.append("]");
		return buf.toString();
	}

	public Map<String, String> getShowAppendFile() {
		return showAppendFile;
	}

	public void setShowAppendFile(Map<String, String> showAppendFile) {
		this.showAppendFile = showAppendFile;
	}

	/**
	 * @return the ctgId
	 */
	public String getCtgId() {
		return ctgId;
	}

	/**
	 * @param ctgId the ctgId to set
	 */
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	/**
	 * @return the mstoneId
	 */
	public String getMstoneId() {
		return mstoneId;
	}

	/**
	 * @param mstoneId the mstoneId to set
	 */
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	public boolean isDraff() {
		return isDraff;
	}

	public void setDraff(boolean isDraff) {
		this.isDraff = isDraff;
	}
}
