package jp.co.blueship.tri.rm.domain.ra.beans.dto;

import java.io.Serializable;

/**
 * リリース申請・リリース用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** リリース番号 */
	private String relNo = null;
	/** リリース物作成日 */
	private String relInputDate = null;
	/** リリース概要 */
	private String relSummary = null;
	/** リリース内容 */
	private String relContent = null;
	/** リリースステータス */
	private String relStatus = null;
	/**
	 * @return relContent
	 */
	public String getRelContent() {
		return relContent;
	}
	/**
	 * @param relContent 設定する relContent
	 */
	public void setRelContent(String relContent) {
		this.relContent = relContent;
	}
	/**
	 * @return relInputDate
	 */
	public String getRelInputDate() {
		return relInputDate;
	}
	/**
	 * @param relInputDate 設定する relInputDate
	 */
	public void setRelInputDate(String relInputDate) {
		this.relInputDate = relInputDate;
	}
	/**
	 * @return relNo
	 */
	public String getRelNo() {
		return relNo;
	}
	/**
	 * @param relNo 設定する relNo
	 */
	public void setRelNo(String relNo) {
		this.relNo = relNo;
	}
	/**
	 * @return relStatus
	 */
	public String getRelStatus() {
		return relStatus;
	}
	/**
	 * @param relStatus 設定する relStatus
	 */
	public void setRelStatus(String relStatus) {
		this.relStatus = relStatus;
	}
	/**
	 * @return relSummary
	 */
	public String getRelSummary() {
		return relSummary;
	}
	/**
	 * @param relSummary 設定する relSummary
	 */
	public void setRelSummary(String relSummary) {
		this.relSummary = relSummary;
	}
	
}