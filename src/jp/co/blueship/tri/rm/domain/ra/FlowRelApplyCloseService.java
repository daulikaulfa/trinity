package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.constants.RelApplyScreenItemID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyCloseEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請クローズ画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyCloseService implements IDomain<FlowRelApplyCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRelApplyCloseServiceBean> execute( IServiceDto<FlowRelApplyCloseServiceBean> serviceDto ) {

		FlowRelApplyCloseServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();


			//選択されたリリース申請番号を取得
			RelApplyViewBean sBean = paramBean.getRelApplyViewBean();

			String[] raIds = sBean.getSelectedRelApplyNo();

			if ( !refererID.equals( RelApplyScreenID.CLOSE  ) &&
					!forwardID.equals( RelApplyScreenID.CLOSE)) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.CLOSE )) {

				if ( ScreenType.next.equals( screenType )) {

					checkRelApplyClose(
							paramBean.getRelApplyCloseEntryViewBean() );

					if( paramBean.isStatusMatrixV3() ) {
						List<IRaDto> raDtoList = this.support.findRaDto( raIds );
	
						String[] rpIds = support.getRpIdsFromRaDto( raDtoList );
						String[] bpIds = support.getBpIdsFromRaDto( raDtoList );
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( support.getLotNo( raDtoList ) )
						.setPjtIds( support.getPjtNo( raDtoList ) )
						.setAreqIds( support.getAssetApplyNo( raDtoList ) )
						.setBpIds( bpIds )
						.setRaIds( raIds )
						.setRpIds( rpIds );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
					}
				}
			}

			/* リリース申請情報の取得 */
			if ( refererID.equals( RelApplyScreenID.CLOSE_LIST_VIEW )
					&& forwardID.equals( RelApplyScreenID.CLOSE ) ) {

				if ( ScreenType.select.equals( screenType )) {

					//リリース申請番号が選択されているかをチェック
					ItemCheckAddonUtils.checkRelApplyNo( raIds );

					//relApplyEntityを取得
					List<IRaDto> raDtoList = this.support.findRaDto( raIds );
					List<RelApplyViewBean> vBeanList = new ArrayList<RelApplyViewBean>();
					for ( IRaDto raDto : raDtoList ) {

						String lotId = raDto.getRaEntity().getLotId();

						ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity(lotId);

						String lotName = lotEntity.getLotNm();

						IRaEntity raEntity = raDto.getRaEntity();
						List<IAreqEntity> areqEntities = support.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() );

						RelApplyViewBean bean = paramBean.newRelApplyViewBean();
						bean.setRelApplyNo				( raEntity.getRaId() );
						bean.setLotNo					( raEntity.getLotId() );
						bean.setLotName					( lotName );
						bean.setRelNoBeanList			( RmConverterAddonUtils.convertRelBean(this.support, raDto.getRaRpLnkEntities()) );
						bean.setUnitBeanList			( RmConverterAddonUtils.convertUnitBean(raDto.getRaBpLnkEntities()) );
						bean.setChangeCouseNoNameBeanList( RmConverterAddonUtils.convertAssetApplyBean( this.support, raDto.getRaBpLnkEntities() ) );
						bean.setApplyNoBeanList			( RmConverterAddonUtils.convertAssetApplyNoList( areqEntities ) );
						bean.setEnvName					( raEntity.getBldEnvNm() );
						bean.setRelApplyUser			( raEntity.getReqUser() );
						bean.setRelApplyStatusId		( raEntity.getProcStsId() );
						bean.setRelApplyStatus			( (sheet.getValue(RmDesignBeanId.statusId, raEntity.getProcStsId())));

						vBeanList.add(bean);
					}

					paramBean.setRelApplyViewBeanList(vBeanList);
				}
			}


			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005042S, e);
		}
	}

	/**
	 * リリース申請クローズ入力項目チェック
	 * @param viewBean 確認情報
	 */
	private static void checkRelApplyClose(
			RelApplyCloseEntryViewBean relApplyCloseEntryViewBean ) {

		//コメント
		if ( !StatusFlg.off.value().equals(
				sheet.getValue(
						RmDesignBeanId.flowRelApplyCloseCheck,
						RelApplyScreenItemID.RelApplyCloseCheck.CLOSE_COMMENT.toString() )) ) {

			if ( TriStringUtils.isEmpty( relApplyCloseEntryViewBean.getCloseComment() )) {
				throw new ContinuableBusinessException( RmMessageId.RM001016E );
			}
		}

	}

}
