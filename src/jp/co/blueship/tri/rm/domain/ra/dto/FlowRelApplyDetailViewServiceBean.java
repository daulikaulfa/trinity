package jp.co.blueship.tri.rm.domain.ra.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean;

/**
 * リリース管理・リリース申請詳細観覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyDetailViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** リリース申請詳細情報 */
	private RelApplyDetailViewBean relApplyDetailViewBean = null;

	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}

	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}

	public RelApplyDetailViewBean getRelApplyDetailViewBean() {
		return relApplyDetailViewBean;
	}

	public void setRelApplyDetailViewBean(RelApplyDetailViewBean relApplyDetailViewBean) {
		this.relApplyDetailViewBean = relApplyDetailViewBean;
	}
	
}
