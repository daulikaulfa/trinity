package jp.co.blueship.tri.rm.domain.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
/**
 * リリース申請・リリース申請画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyEntryServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	/** 検索条件情報 */
	private RelApplyPackageListSearchBean relApplyPackageListSearchBean = new RelApplyPackageListSearchBean() ;
	
	/** 
	 * ロット番号（コンボ用）
	 */
	private List<ItemLabelsBean> searchLotNoList = null ;
	/** 
	 * 詳細検索 検索件数 
	 */
	private List<ItemLabelsBean> searchRelUnitCountList = null ;
	/** 
	 * ビルドパッケージ情報（一覧選択用）
	 */
	private List<UnitViewBean> unitViewBeanList = null;
	/** 
	 * グループ（コンボ用）
	 */
	private List<ItemLabelsBean> groupIdList = null ;
	/** 
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = null ;
	/** 
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = null ;
	
	/** リリース申請情報 */
	private RelApplyEntryViewBean relApplyEntryViewBean = null ;
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public UnitViewBean newUnitViewBean() {
		UnitViewBean bean = new UnitViewBean();
		return bean;
	}
	
	public class UnitViewBean {

		/** パッケージ番号 */
		private String buildNo = null;
		/** パッケージ名 */
		private String buildName = null;
		/** 変更要因番号リスト */
		private List<String> changeCauseNoList = new ArrayList<String>();
		/** 申請リスト */
		private List<String> assetApplyNoList = new ArrayList<String>();
		
		public String getBuildName() {
			return buildName;
		}
		public void setBuildName(String buildName) {
			this.buildName = buildName;
		}
		public String getBuildNo() {
			return buildNo;
		}
		public void setBuildNo(String buildNo) {
			this.buildNo = buildNo;
		}
		public List<String> getChangeCauseNoList() {
			return changeCauseNoList;
		}
		public void setChangeCauseNoList(List<String> changeCauseNoList) {
			this.changeCauseNoList = changeCauseNoList;
		}
		public List<String> getAssetApplyNoList() {
			return assetApplyNoList;
		}
		public void setAssetApplyNoList(List<String> assetApplyNoList) {
			this.assetApplyNoList = assetApplyNoList;
		}
		
	}

	public RelApplyPackageListSearchBean getRelApplyPackageListSearchBean() {
		return relApplyPackageListSearchBean;
	}

	public void setRelApplyPackageListSearchBean(
			RelApplyPackageListSearchBean relApplyPackageListSearchBean) {
		this.relApplyPackageListSearchBean = relApplyPackageListSearchBean;
	}

	public List<ItemLabelsBean> getSearchLotNoList() {
		return searchLotNoList;
	}
	public void setSearchLotNoList(List<ItemLabelsBean> searchLotNoList) {
		this.searchLotNoList = searchLotNoList;
	}
	
	public RelApplyEntryViewBean getRelApplyEntryViewBean() {
		return relApplyEntryViewBean;
	}

	public void setRelApplyEntryViewBean(RelApplyEntryViewBean relApplyEntryViewBean) {
		this.relApplyEntryViewBean = relApplyEntryViewBean;
	}

	public List<UnitViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}

	public void setUnitViewBeanList(List<UnitViewBean> unitViewBeanList) {
		this.unitViewBeanList = unitViewBeanList;
	}

	public List<ItemLabelsBean> getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(List<ItemLabelsBean> groupIdList) {
		this.groupIdList = groupIdList;
	}

	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}

	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}

	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}

	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}

	public List<ItemLabelsBean> getSearchRelUnitCountList() {
		return searchRelUnitCountList;
	}

	public void setSearchRelUnitCountList(
			List<ItemLabelsBean> searchRelUnitCountList) {
		this.searchRelUnitCountList = searchRelUnitCountList;
	}
	
}
