package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請編集保存完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyModifySaveServiceComp implements IDomain<FlowRelApplyModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;
	private List<IDomain<IGeneralServiceBean>> pojoList = null;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}
	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean> execute( IServiceDto<FlowRelApplyModifyServiceBean> serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwardID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) &&
					!forwardID.equals( RelApplyScreenID.MODIFY_SAVE_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.MODIFY )
					&& forwardID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) ) {
				//アクション
				List<Object> paramList = new ArrayList<Object>();
				paramList.add( paramBean.getRelApplyEntryViewBean() ) ;
				try {
					if( paramBean.isStatusMatrixV3() ) {
						RelApplyEntryViewBean viewBean = paramBean.getRelApplyEntryViewBean();
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( viewBean.getLotNo() )
						.setPjtIds( (null == viewBean.getPjtNoList())? null: viewBean.getPjtNoList().toArray(new String[0]) )
						.setAreqIds( (null == viewBean.getAssetApplyNoList())? null: viewBean.getAssetApplyNoList().toArray(new String[0]) )
						.setBpIds( support.getBuildNo(viewBean.getUnitBeanList() ) )
						.setRaIds( viewBean.getRelApplyNo() );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
					}

					// 下層のアクションを実行する
					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
					innerServiceDto.setServiceBean( paramBean );

					{
						for ( IDomain<IGeneralServiceBean> pojo : this.pojoList ) {
							innerServiceDto = pojo.execute( innerServiceDto );
						}
					}
				} finally {

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005048S, e);
		}
	}

}
