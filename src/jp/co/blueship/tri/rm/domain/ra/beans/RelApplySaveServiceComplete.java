package jp.co.blueship.tri.rm.domain.ra.beans;

import java.util.List;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請保存完了処理Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelApplySaveServiceComplete implements IDomain<FlowRelApplyEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyEntryServiceBean> execute(IServiceDto<FlowRelApplyEntryServiceBean> serviceDto ) {

		FlowRelApplyEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();


			if ( !refererID.equals( RelApplyScreenID.SAVE_COMP ) &&
					!forwardID.equals( RelApplyScreenID.SAVE_COMP ) ) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.SAVE_COMP ) ) {

			}

			if ( refererID.equals( RelApplyScreenID.ENTRY )
					&& forwardID.equals( RelApplyScreenID.SAVE_COMP ) ) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

					/*
					 * ビルドパッケージに含まれる申請情報の取得
					 */
					List<IBpEntity> buildEntityList = RmConverterAddonUtils.convertBuildEntity( this.support, bean.getUnitBeanList() );
					List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(buildEntityList);
					List<AssetApplyBean> assetApplyBeanList = RmConverterAddonUtils.convertAssetApplyBeanList( this.support, bpDtoList );

					// リリース申請仮番号の採番
					if ( TriStringUtils.isEmpty( bean.getRelApplyNo() ) ) {
						//確認画面で添付ファイルを格納するために仮番を発行しているため、トピックで戻って保存した際に再度採番されないようにする
						String relApplyNo = this.support.nextvalByRaTempId();
						bean.setRelApplyNo(relApplyNo);
					}

					//添付ファイルの保存
					this.support.saveUploadFile( bean, bean, false );

					// リリース申請情報の新規登録
					this.insertRelApply(paramBean.getRelApplyEntryViewBean(),
							paramBean.getUserId(), paramBean.getUserName(),
							TriDateUtils.getSystemDate(), assetApplyBeanList);
					
					this.support.getUmFinderSupport().registerCtgLnk( bean.getCtgId(), RmTables.RM_RA, bean.getRelApplyNo() );
					this.support.getUmFinderSupport().registerMstoneLnk( bean.getMstoneId(), RmTables.RM_RA, bean.getRelApplyNo() );
					
					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean(bean);
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005035S, e);
		}
	}

	/**
	 * リリース申請情報を確定する
	 *
	 */
	private void insertRelApply(RelApplyEntryViewBean bean, String userId,
			String userName, String systemDate,
			List<AssetApplyBean> assetApplyBeanList ) {

		IRaEntity entity = new RaEntity() ;

		// 属性
		entity.setLotId( bean.getLotNo() ) ;
		entity.setRaId( bean.getRelApplyNo() ) ;	// 採番した番号
//		entity.setRelApplyDate(  ) ;	// 保存の場合、申請日時は登録しない
		entity.setReqUserId( bean.getRelApplyUserId() ) ;
		entity.setReqUser( bean.getRelApplyUser() ) ;
		entity.setReqGrpId( bean.getRelApplyGroupId() ) ;
		entity.setReqGrpNm( bean.getRelApplyGroup() ) ;
		entity.setBldEnvId( bean.getRelEnvNo() ) ;
		entity.setPreferredRelDate( bean.getRelWishDate() ) ;
		entity.setRelationId( bean.getRelatedNo() ) ;

		// システム属性
		entity.setStsId				( RmRaStatusId.DraftReleaseRequest.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setUpdUserNm			( userName );
		entity.setUpdUserId			( userId );

		// 要素
		entity.setRemarks( bean.getRemarks() ) ;

		//RaBpリンクテーブルの更新
		this.support.deleteRaBpLnk(bean);
		this.support.insertRaBpLnk(bean);
		//添付ファイル
		this.support.insertRaAttachedFile( bean );

		// 関連要素(rel)
		// 保存のタイミングでは行わない

		// 関連要素(responsibleUser)
		entity.setReqResponsibility(bean.getRelApplyPersonCharge());
		// 関連要素(responsibleGroup)
		// 申請承認グループ用 未実装

		this.support.getRaDao().insert( entity );

		// 保存では履歴管理しない

	}
}
