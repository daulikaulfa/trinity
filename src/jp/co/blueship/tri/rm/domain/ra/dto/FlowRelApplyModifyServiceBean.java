package jp.co.blueship.tri.rm.domain.ra.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
/**
 * リリース申請・リリース申請画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyModifyServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 検索条件情報 */
	private RelApplySearchBean relApplySearchBean = null;

	/** 
	 * グループ（コンボ用）
	 */
	private List<ItemLabelsBean> groupIdList = null ;
	/** 
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = null ;
	/** 
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = null ;
	
	/** リリース申請情報 */
	private RelApplyEntryViewBean relApplyEntryViewBean = null ;
	
	
	public List<ItemLabelsBean> getGroupIdList() {
		return groupIdList;
	}

	public void setGroupIdList(List<ItemLabelsBean> groupIdList) {
		this.groupIdList = groupIdList;
	}

	public RelApplyEntryViewBean getRelApplyEntryViewBean() {
		return relApplyEntryViewBean;
	}

	public void setRelApplyEntryViewBean(RelApplyEntryViewBean relApplyEntryViewBean) {
		this.relApplyEntryViewBean = relApplyEntryViewBean;
	}

	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}

	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}

	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}

	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}

	public RelApplySearchBean getRelApplySearchBean() {
		return relApplySearchBean;
	}

	public void setRelApplySearchBean(RelApplySearchBean relApplySearchBean) {
		this.relApplySearchBean = relApplySearchBean;
	}

}
