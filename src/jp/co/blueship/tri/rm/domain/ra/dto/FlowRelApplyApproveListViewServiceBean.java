package jp.co.blueship.tri.rm.domain.ra.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
/**
 * リリース申請・承認待ち一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012
 */
public class FlowRelApplyApproveListViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
}
