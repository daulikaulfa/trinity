package jp.co.blueship.tri.rm.domain.ra.beans;

import java.util.List;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請編集保存完了処理Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelApplyModifySaveServiceComplete implements IDomain<FlowRelApplyModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean>  execute(IServiceDto<FlowRelApplyModifyServiceBean>  serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) &&
					!forwardID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) ) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) ) {

			}

			if ( refererID.equals( RelApplyScreenID.MODIFY )
					&& forwardID.equals( RelApplyScreenID.MODIFY_SAVE_COMP ) ) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

					// すでにあるリリース申請情報を取得
					IRaEntity entity = this.support.findRaEntity( bean.getRelApplyNo() ) ;

					/*
					 * ビルドパッケージに含まれる申請情報の取得
					 */
					List<IBpEntity> buildEntityList = RmConverterAddonUtils.convertBuildEntity( this.support, bean.getUnitBeanList() );
					List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(buildEntityList);
					List<AssetApplyBean> assetApplyBeanList = RmConverterAddonUtils.convertAssetApplyBeanList( this.support, bpDtoList );

					// リリース申請（仮）番号をすでにもっているので改めて採番はしない

					//添付ファイルの保存
					this.support.saveUploadFile( bean, bean, true );

					// リリース申請情報の更新
					this.updateRelApply(entity, paramBean.getRelApplyEntryViewBean(),
							paramBean.getUserId(), paramBean.getUserName(),
							TriDateUtils.getSystemDate(), assetApplyBeanList);

					this.support.getUmFinderSupport().updateCtgLnk( bean.getCtgId(), RmTables.RM_RA, bean.getRelApplyNo() );
					this.support.getUmFinderSupport().updateMstoneLnk( bean.getMstoneId(), RmTables.RM_RA, bean.getRelApplyNo() );
					
					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean(bean);
				}
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005033S, e);
		}
	}

	/**
	 * リリース申請情報を保存する
	 *
	 */
	private void updateRelApply(
			IRaEntity entity,
			RelApplyEntryViewBean bean,
			String userId,
			String userName,
			String systemDate,
			List<AssetApplyBean> assetApplyBeanList ) {

		// 属性
		entity.setLotId( bean.getLotNo() ) ;
		entity.setRaId( bean.getRelApplyNo() ) ;
//		entity.setRelApplyDate(  ) ;	// 保存の場合、申請日時は変更しない
		entity.setReqUser( bean.getRelApplyUser() ) ;
		entity.setReqGrpId( bean.getRelApplyGroupId() ) ;
		entity.setBldEnvId( bean.getRelEnvNo() ) ;
		entity.setPreferredRelDate( bean.getRelWishDate() ) ;
		entity.setRelationId( bean.getRelatedNo() ) ;

		// システム属性
//		entity.setBaseStatusId		(  );	// 保存の場合、ステータスは変更しない
//		entity.setStatusId			(  );	// 保存の場合、ステータスは変更しない
		entity.setDelStsId		( StatusFlg.off );
		entity.setUpdUserNm		( userName );
		entity.setUpdUserId		( userId );
		// 要素
		entity.setRemarks( bean.getRemarks() ) ;

		// 関連要素(build)
		this.support.deleteRaBpLnk(bean);
		this.support.insertRaBpLnk(bean);

		// 指定された添付ファイルを除外する（添付ファイルの削除）
		for ( String id: bean.getDelAppendFiles() ) {
			if ( bean.getAppendFile().containsKey(id) ) {
				bean.getAppendFile().remove(id);
			}
		}

		//添付ファイル
		this.support.insertRaAttachedFile(bean);

		// 関連要素(rel)
		// 保存のタイミングでは行わない

		// 関連要素(responsibleUser)
		entity.setReqResponsibility(bean.getRelApplyPersonCharge()) ;

		// 関連要素(responsibleGroup)
		// 申請承認グループ用 未実装


		this.support.getRaDao().update( entity );

		// 保存では履歴管理しない

	}
}
