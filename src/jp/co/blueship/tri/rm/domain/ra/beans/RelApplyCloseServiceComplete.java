package jp.co.blueship.tri.rm.domain.ra.beans;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyCloseEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請クローズ完了処理Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelApplyCloseServiceComplete implements IDomain<FlowRelApplyCloseServiceBean> {

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyCloseServiceBean> execute(IServiceDto<FlowRelApplyCloseServiceBean> serviceDto ) {

		FlowRelApplyCloseServiceBean paramBean = null;

		try {

			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwardID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.CLOSE_COMP ) &&
					!forwardID.equals( RelApplyScreenID.CLOSE_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.CLOSE_COMP )) {

			}

			if ( refererID.equals( RelApplyScreenID.CLOSE )
					&& forwardID.equals( RelApplyScreenID.CLOSE_COMP ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					RelApplyViewBean relApplyViewBean = paramBean.getRelApplyViewBean();
					RelApplyCloseEntryViewBean relApplyCloseEntryViewBean = paramBean.getRelApplyCloseEntryViewBean();

					String[] relApplyNo = relApplyViewBean.getSelectedRelApplyNo();
		 			ItemCheckAddonUtils.checkRelApplyNo( relApplyNo );

					RaDtoList entities = this.support.getRelApplyEntity( relApplyNo );

					// クローズにおけるビルドパッケージエンティティは同一ロット配下なので、最初のロット番号を使う
					ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( entities.get(0).getRaEntity().getLotId() );

					//関連するDBデータの更新
					// ビルドパッケージ情報の更新
					this.updateRelApplyEntityToClose(
							paramBean,
							entities ,
							relApplyViewBean ,
							relApplyCloseEntryViewBean,
							lotEntity ) ;

				}

			}

			return serviceDto;

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005002S , e );
		}
	}

	/**
	 * ビルドパッケージ情報をクローズ成功／失敗に更新する
	 * @param paramBean DTOパラメタ
	 * @param raDtoList ビルドパッケージエンティティ
	 * @param relApplyViewBean 編集情報
	 * @param relApplyCloseEntryViewBean 編集情報
	 * @param lotEntity ロットエンティティ
	 * @param systemDate 更新日時
	 */
	private void updateRelApplyEntityToClose(
											FlowRelApplyCloseServiceBean paramBean,
											RaDtoList raDtoList,
											RelApplyViewBean relApplyViewBean,
											RelApplyCloseEntryViewBean relApplyCloseEntryViewBean,
											ILotEntity lotEntity ) throws Exception {
		try {
			String userName		= paramBean.getUserName();
			String userId		= paramBean.getUserId();

			for ( IRaDto raDto : raDtoList ) {
				IRaEntity raEntity = raDto.getRaEntity();

				raEntity.setCloseCmt		( relApplyCloseEntryViewBean.getCloseComment() );
				raEntity.setCloseTimestamp	( TriDateUtils.getSystemTimestamp() );
				raEntity.setCloseUserNm		( userName );
				raEntity.setCloseUserId		( userId );
				raEntity.setStsId			( RmRaStatusId.ReleaseRequestClosed.getStatusId() );
				raEntity.setUpdUserNm		( userName );
				raEntity.setUpdUserId		( userId );

				this.support.getRaDao().update( raEntity );

				this.support.getSmFinderSupport().cleaningExecDataSts(RmTables.RM_RA, raEntity.getRaId());

				if ( DesignSheetUtils.isRecord() ) {
					raDto = this.support.findRaDto( raEntity.getRaId() );

					IHistEntity histEntity = new HistEntity();
					histEntity.setActSts( UmActStatusId.Close.getStatusId() );

					support.getRaHistDao().insert( histEntity , raDto);
				}
			}

			//メール送信用にリリース申請をキャッシュする
			paramBean.getParamList().add( raDtoList );

		} catch ( Exception e ) {
			throw e ;
		}
	}

}
