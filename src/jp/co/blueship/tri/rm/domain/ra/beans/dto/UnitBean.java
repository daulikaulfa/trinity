package jp.co.blueship.tri.rm.domain.ra.beans.dto;

import java.io.Serializable;

/**
 * リリース申請・ビルドパッケージ用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** パッケージ番号 */
	private String buildNo = null;
	/** パッケージ名 */
	private String buildName = null;
	/** パッケージ概要 */
	private String buildSummary = null;
	/** パッケージ件名 */
	private String buildContent = null;
	
	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}
	public String getBuildName() {
		return buildName;
	}
	public void setBuildName(String buildName) {
		this.buildName = buildName;
	}
	public String getBuildSummary() {
		return buildSummary;
	}
	public void setBuildSummary(String buildSummary) {
		this.buildSummary = buildSummary;
	}
	public String getBuildContent() {
		return buildContent;
	}
	public void setBuildContent(String buildContent) {
		this.buildContent = buildContent;
	}
	
}
