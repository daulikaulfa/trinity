package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseListViewServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseListViewServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請クローズ待ち一覧画面の表示情報設定Class<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelApplyCloseListViewService implements IDomain<FlowRelApplyCloseListViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowRelApplyCloseListViewServiceBean> execute( IServiceDto<FlowRelApplyCloseListViewServiceBean> serviceDto ) {

		FlowRelApplyCloseListViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( refererID.equals( RelApplyScreenID.CLOSE_LIST_VIEW )) {
			}

			if ( forwordID.equals( RelApplyScreenID.CLOSE_LIST_VIEW )) {
				//リリース申請情報の検索
				this.searchRelApplyList(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005041S, e);
		}
	}

	/**
	 * リリース申請リストの詳細検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchRelApplyList( FlowRelApplyCloseListViewServiceBean paramBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		boolean isSearch = false;
		boolean isAutoSearch = false;

		/*
		 * 条件の作成
		 */
		RelApplySearchBean relApplySearchBean =DBSearchBeanAddonUtil
					.setRelApplySearchBean(paramBean.getRelApplySearchBean(),
							RmDesignBeanId.raCloseListViewListCount);

		/*
		 * 選択可能なロット番号リスト取得
		 */
		// グループ情報
		List<IGrpUserLnkEntity> groupUserEntitys = umFinderSupport.findGrpUserLnkByUserId( paramBean.getUserId() );
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		// ロットエンティティ取得
		List<ILotEntity> accessableLotList = new ArrayList<ILotEntity>();
		{
			String[] lotId = this.support.getLotNoByReleasableRelUnit();
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
			ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelApplyTop();
			List<ILotEntity> lotEntities =
				this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
			List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );

			// グループによるロットアクセスの絞込み
			ViewInfoAddonUtil.setAccessablePjtLotEntity( lotDto, groupUserEntitys, accessableLotList, null );

			relApplySearchBean.setSearchLotNoList( ItemLabelsUtils.conv(accessableLotList, srvEntity.getBldSrvId() ) );
		}
		List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( accessableLotList );

		/*
		 * 選択可能なリリース環境リスト取得
		 */
		// リリース環境エンティティ取得
		IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
		IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity(envAllEntities, lotDtoEntities);

		relApplySearchBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );

		/*
		 * 選択可能なリリース希望日リスト取得
		 */
		ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

		relApplySearchBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );

		/*
		 * 空のリリース申請情報一覧を作成
		 */
		List<RelApplyViewBean> releaseApplyViewBeanList = new ArrayList<RelApplyViewBean>();

		String selectedLotNo = relApplySearchBean.getSelectedLotNo();
		String selectedRelEnvNo = relApplySearchBean.getSelectedRelEnvNo();
		String selectedRelWishDate = relApplySearchBean.getSelectedRelWishDate();
		String selectedRelApplyCount = relApplySearchBean.getSelectedRelApplyCount();

		/*
		 * 検索条件有無チェック（検索時のみ）
		 */
		if( ScreenType.select.equals(paramBean.getScreenType()) ) {
			// ロットNo
			if ( TriStringUtils.isEmpty(selectedLotNo) ) {
				messageList.add( RmMessageId.RM001000E );
				messageArgsList.add( new String[] {} );
			}

			// リリース環境No
			if ( TriStringUtils.isEmpty(selectedRelEnvNo) ) {
				messageList.add( RmMessageId.RM001001E );
				messageArgsList.add( new String[] {} );
			}

			// リリース希望日
			if ( TriStringUtils.isEmpty(selectedRelWishDate) ) {
				messageList.add( RmMessageId.RM001002E );
				messageArgsList.add( new String[] {} );
			}

			// 検索件数
			if ( TriStringUtils.isEmpty(selectedRelApplyCount) ) {
				messageList.add( RmMessageId.RM001003E );
				messageArgsList.add( new String[] {} );
			}

			try {
				Integer.valueOf(selectedRelApplyCount);
			} catch (NumberFormatException e) {
				messageList.add( RmMessageId.RM001004E );
				messageArgsList.add( new String[] {} );
			}

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

			isSearch = true;
		} else if ( ! TriStringUtils.isEmpty(selectedLotNo)
				&& ! TriStringUtils.isEmpty(selectedRelEnvNo)
				&& ! TriStringUtils.isEmpty(selectedRelWishDate)
				&& ! TriStringUtils.isEmpty(selectedRelApplyCount) ) {
			if ( ! ScreenType.bussinessException.equals(paramBean.getScreenType()) ) {
				isAutoSearch = true;
			} else {
				//当画面の検索項目での業務エラーか次画面遷移時の業務エラーかを判断できないため、キャッシュを表示する。
				releaseApplyViewBeanList = paramBean.getRelApplyViewBeanList();
			}
		}

		if ( isSearch || isAutoSearch ) {
			int iSelectedRelApplyCount = Integer.valueOf(selectedRelApplyCount);

			// リリース申請一覧を検索

			// 共通の条件
			int pageNo = 1;
			int viewRows = iSelectedRelApplyCount;
			ISqlSort sort = DBSearchSortAddonUtil.getRelApplySortFromDesignDefineByRelApplyCloseListView();

			// クローズ可能なリリース申請条件
			RaCondition condition = new RaCondition();
			condition.setLotId(selectedLotNo);
			condition.setBldEnvId(selectedRelEnvNo);
			condition.setPreferredRelDate(selectedRelWishDate);
			condition.setStsIds(new String[]{
					 RmRaStatusId.ReleaseRequested.getStatusId(),
					 RmRaStatusId.ReleaseRequestApproved.getStatusId(),
					 RmRaStatusId.ReleasePackageCreated.getStatusId(),
					 RmRaStatusId.ReleasePackageRemoved.getStatusId(),
					});
			condition.setProcStsIds(new String[]{
					RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled.getStatusId(),
					});


			// クローズ可能なリリース申請を取得
			IEntityLimit<IRaEntity> raLimit = support.getRaDao().find(condition.getCondition(), sort, pageNo, viewRows);
			RaDtoList raDtoList = support.findRaDtoList(raLimit.getEntities());

			List<RelApplyViewBean> beanList = new ArrayList<RelApplyViewBean>();

			for (IRaDto raDto : raDtoList) {
				String[] bpIds = FluentList.from(raDto.getRaBpLnkEntities()).map( RmFluentFunctionUtils.toBpIdFromRaBpLnk ).toArray(new String[0]);

				List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto( bpIds );

				IRaEntity raEntity = raDto.getRaEntity();

				RelApplyViewBean bean = paramBean.newRelApplyViewBean();
				bean.setRelApplyNo			( raEntity.getRaId() );
				bean.setRelEnvNo			( raEntity.getBldEnvId() );
				bean.setRelEnvName			( raEntity.getBldEnvNm() );
				bean.setRelApplyStatusId	( raEntity.getProcStsId() );
				bean.setRelApplyStatus		( (sheet.getValue(RmDesignBeanId.statusId, raEntity.getProcStsId())));
				bean.setUnitBeanList		( RmConverterAddonUtils.convertUnitBean( raDto.getRaBpLnkEntities() ) );
				bean.setAssetApplyBeanList	( RmConverterAddonUtils.convertAssetApplyBeanList(this.support , bpDtoList) );

				beanList.add(bean);
			}

			// 一覧をマージ
			releaseApplyViewBeanList.addAll(beanList);
		}

		/*
		 * 戻りに設定
		 */
		paramBean.setRelApplySearchBean( relApplySearchBean );
		paramBean.setRelApplyViewBeanList(releaseApplyViewBeanList);

		if ( 0 == paramBean.getRelApplyViewBeanList().size()
			&& isSearch
			&& ! isAutoSearch ) {
			throw new BusinessException( RmMessageId.RM001017E );
		}
	}
}
