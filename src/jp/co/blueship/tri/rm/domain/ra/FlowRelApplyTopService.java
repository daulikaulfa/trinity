package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.ViewInfoAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyTopServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyTopServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * リリース申請・トップ画面の表示情報設定Class<br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelApplyTopService implements IDomain<FlowRelApplyTopServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyTopServiceBean> execute( IServiceDto<FlowRelApplyTopServiceBean> serviceDto ) {

		FlowRelApplyTopServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( refererID.equals( RelApplyScreenID.TOP )) {
			}

			if ( forwordID.equals( RelApplyScreenID.TOP )) {
				//リリース申請情報の検索
				this.searchRelApplyList(paramBean);
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005055S, e);
		}
	}

	/**
	 * リリース申請リストの詳細検索を行う。
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private void searchRelApplyList( FlowRelApplyTopServiceBean paramBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		boolean isSearch = false;
		boolean isAutoSearch = false;

		/*
		 * 条件の作成
		 */
		RelApplySearchBean relApplySearchBean = DBSearchBeanAddonUtil
				.setRelApplySearchBean(paramBean.getRelApplySearchBean(),
						RmDesignBeanId.raTopListCount);

		/*
		 * 選択可能なロット番号リスト取得
		 */
		// グループ情報
		List<IGrpUserLnkEntity> groupUserEntitys = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
		IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
		// ロットエンティティ取得
		List<ILotEntity> accessableLotList = new ArrayList<ILotEntity>();
		{
			String[] lotId = this.support.getLotNoByReleasableRelUnit();
			IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
			ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelApplyTop();
			List<ILotEntity> lotEntities =
				this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
			List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );

			// グループによるロットアクセスの絞込み
			ViewInfoAddonUtil.setAccessablePjtLotEntity( lotDto, groupUserEntitys, accessableLotList, null );

			relApplySearchBean.setSearchLotNoList( ItemLabelsUtils.conv(accessableLotList, srvEntity.getBldSrvId() ) );
		}
		List<ILotDto> lotDtoEntities = this.support.getAmFinderSupport().findLotDto( accessableLotList );

		/*
		 * 選択可能なリリース環境リスト取得
		 */
		// リリース環境エンティティ取得
		IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
		IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity(envAllEntities, lotDtoEntities);

		relApplySearchBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );

		/*
		 * 選択可能なリリース希望日リスト取得
		 */
		ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

		relApplySearchBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );

		/*
		 * 空のリリース申請情報一覧を作成
		 */
		List<RelApplyViewBean> releaseApplyViewBeanList = new ArrayList<RelApplyViewBean>();

		String selectedLotNo = relApplySearchBean.getSelectedLotNo();
		String selectedRelEnvNo = relApplySearchBean.getSelectedRelEnvNo();
		String selectedRelWishDate = relApplySearchBean.getSelectedRelWishDate();
		String selectedRelApplyCount = relApplySearchBean.getSelectedRelApplyCount();

		/*
		 * 検索条件有無チェック（検索時のみ）
		 */
		if(  ScreenType.select.equals(paramBean.getScreenType()) ) {
			// ロットNo
			if ( TriStringUtils.isEmpty(selectedLotNo) ) {
				messageList.add( RmMessageId.RM001000E );
				messageArgsList.add( new String[] {} );
			}

			// リリース環境No
			if ( TriStringUtils.isEmpty(selectedRelEnvNo) ) {
				messageList.add( RmMessageId.RM001001E );
				messageArgsList.add( new String[] {} );
			}

			// リリース希望日
			if ( TriStringUtils.isEmpty(selectedRelWishDate) ) {
				messageList.add( RmMessageId.RM001002E );
				messageArgsList.add( new String[] {} );
			}

			// 検索件数
			if ( TriStringUtils.isEmpty(selectedRelApplyCount) ) {
				messageList.add( RmMessageId.RM001003E );
				messageArgsList.add( new String[] {} );
			}

			try {
				Integer.valueOf(selectedRelApplyCount);
			} catch (NumberFormatException e) {
				messageList.add( RmMessageId.RM001004E );
				messageArgsList.add( new String[] {} );
			}

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );

			isSearch = true;
		} else 	if ( ! ScreenType.bussinessException.equals(paramBean.getScreenType()) ) {
			if ( ! TriStringUtils.isEmpty(selectedLotNo)
				&& ! TriStringUtils.isEmpty(selectedRelEnvNo)
				&& ! TriStringUtils.isEmpty(selectedRelWishDate)
				&& ! TriStringUtils.isEmpty(selectedRelApplyCount) ) {
				isAutoSearch = true;
			}
		}

		if ( isSearch || isAutoSearch ) {
			int iSelectedRelApplyCount = Integer.valueOf(selectedRelApplyCount);

			// リリース申請一覧を検索

			// 共通の条件
			int pageNo = 1;
			int viewRows = iSelectedRelApplyCount;
			ISqlSort sort = DBSearchSortAddonUtil.getRelApplySortFromDesignDefineByRelApplyTop();

			// 編集可能なリリース申請条件
			RaCondition condition = new RaCondition();
			condition.setLotId(selectedLotNo);
			condition.setBldEnvId(selectedRelEnvNo);
			condition.setPreferredRelDate(selectedRelWishDate);
			condition.setProcStsIds(new String[]{
					 RmRaStatusId.DraftReleaseRequest.getStatusId(),
					 RmRaStatusIdForExecData.ReturnToPendingReleaseRequest.getStatusId()
					});
			// 自分のユーザのみ
			condition.setReqUserId( paramBean.getUserId() );

			// 閲覧可能なリリース申請条件
			RaCondition visibleCondition = new RaCondition();
			visibleCondition.setLotId(selectedLotNo);
			visibleCondition.setBldEnvId(selectedRelEnvNo);
			visibleCondition.setPreferredRelDate(selectedRelWishDate);
			visibleCondition.setProcStsIds(new String[]{
					 	RmRaStatusId.ReleaseRequested.getStatusId(),
						RmRaStatusId.ReleaseRequestApproved.getStatusId(),
						RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled.getStatusId(),
						RmRaStatusId.ReleasePackageCreated.getStatusId(),
						RmRaStatusIdForExecData.CreatingReleasePackage.getStatusId(),
						/*RmRaStatusIdForExecData.REL_WAIT.getStatusId(),*/
						RmRaStatusIdForExecData.ReleasePackageError.getStatusId(),
						RmRaStatusId.ReleasePackageRemoved.getStatusId()
					});


			// 編集可能なリリース申請を取得
			IEntityLimit<IRaEntity> raLimit = support.getRaDao().find(condition.getCondition(), sort, pageNo, viewRows);
			RaDtoList raDtoList = support.findRaDtoList( raLimit.getEntities() );

			List<RelApplyViewBean> beanList = new ArrayList<RelApplyViewBean>();
			for (IRaDto raDto : raDtoList) {
				String[] bpIds = FluentList.from(raDto.getRaBpLnkEntities()).map( RmFluentFunctionUtils.toBpIdFromRaBpLnk ).toArray(new String[0]);

				List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto( bpIds );

				IRaEntity raEntity = raDto.getRaEntity();

				RelApplyViewBean bean = paramBean.newRelApplyViewBean();
				bean.setRelApplyNo			( raEntity.getRaId() );
				bean.setRelEnvNo			( raEntity.getBldEnvId() );
				bean.setRelEnvName			( raEntity.getBldEnvNm() );
				bean.setRelApplyStatusId	( raEntity.getProcStsId() );
				bean.setRelApplyStatus		((sheet.getValue(RmDesignBeanId.statusId, raDto.getRaEntity().getProcStsId())));
				bean.setUnitBeanList		( RmConverterAddonUtils.convertUnitBean(raDto.getRaBpLnkEntities()) );
				bean.setAssetApplyBeanList	( RmConverterAddonUtils.convertAssetApplyBeanList( this.support ,bpDtoList) );
				bean.setEditLinkEnabled		( true );
				bean.setViewLinkEnabled		( true );

				beanList.add(bean);
			}
			// 閲覧可能なリリース申請を取得
			IEntityLimit<IRaEntity> vRaLimit = support.getRaDao().find(visibleCondition.getCondition(), sort, pageNo, viewRows);
			RaDtoList vRaDtoList = this.support.findRaDtoList( vRaLimit.getEntities() );

			List<RelApplyViewBean> vBeanList = new ArrayList<RelApplyViewBean>();
			for (IRaDto raDto : vRaDtoList) {
				String[] bpIds = FluentList.from(raDto.getRaBpLnkEntities()).map( RmFluentFunctionUtils.toBpIdFromRaBpLnk ).toArray(new String[0]);

				List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(bpIds);

				IRaEntity raEntity = raDto.getRaEntity();

				RelApplyViewBean bean = paramBean.newRelApplyViewBean();
				bean.setRelApplyNo			( raEntity.getRaId() );
				bean.setRelEnvNo			( raEntity.getBldEnvId() );
				bean.setRelEnvName			( raEntity.getBldEnvNm() );
				bean.setRelApplyStatusId	( raEntity.getProcStsId() );
				bean.setRelApplyStatus		( (sheet.getValue(
						RmDesignBeanId.statusId, raEntity.getProcStsId())));
				bean.setUnitBeanList		( RmConverterAddonUtils.convertUnitBean( raDto.getRaBpLnkEntities() ) );
				bean.setAssetApplyBeanList	( RmConverterAddonUtils.convertAssetApplyBeanList( this.support, bpDtoList) );
				bean.setEditLinkEnabled		( false );
				bean.setViewLinkEnabled		( true );

				vBeanList.add(bean);
			}

			// 一覧をマージ
			releaseApplyViewBeanList.addAll(beanList);
			releaseApplyViewBeanList.addAll(vBeanList);

			if ( 0 == releaseApplyViewBeanList.size() && ! isAutoSearch ) {
				throw new BusinessException( RmMessageId.RM001033E );
			}

			// 指定の件数に補正
			if(viewRows < releaseApplyViewBeanList.size()) {
				releaseApplyViewBeanList = releaseApplyViewBeanList.subList(0, viewRows);
			}
		}

		/*
		 * 戻りに設定
		 */
		paramBean.setRelApplySearchBean( relApplySearchBean );
		paramBean.setRelApplyViewBeanList( releaseApplyViewBeanList );
	}

}
