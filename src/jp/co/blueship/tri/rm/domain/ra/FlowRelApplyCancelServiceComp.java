package jp.co.blueship.tri.rm.domain.ra;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCancelServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請取消完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public class FlowRelApplyCancelServiceComp implements IDomain<FlowRelApplyCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRelApplyCancelServiceBean> execute( IServiceDto<FlowRelApplyCancelServiceBean> serviceDto ) {

		FlowRelApplyCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwardID = paramBean.getForward();

			if ( !refererID.equals( RelApplyScreenID.CANCEL_COMP ) &&
					!forwardID.equals( RelApplyScreenID.CANCEL_COMP )) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.CANCEL_COMP )) {

			}

			if ( refererID.equals( RelApplyScreenID.DETAIL_VIEW )
					&& forwardID.equals( RelApplyScreenID.CANCEL_COMP ) ) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String relApplyNo = paramBean.getRelApplySearchBean().getSearchRelApplyNo();

					ItemCheckAddonUtils.checkRelApplyNo( relApplyNo );

					List<IRaDto> raDtoList = this.support.findRaDto( new String[]{ relApplyNo } );
					if ( TriCollectionUtils.isEmpty(raDtoList)  ){
						throw new TriSystemException(RmMessageId.RM005077S , relApplyNo);
					}
					IRpEntity[] relEntities =
						this.support.getRpEntityByRelNo( support.getRpIdsFromRaDto( raDtoList ) );

					String userName		= paramBean.getUserName();
					String userId		= paramBean.getUserId();

					//ステータスマトリクスチェック
					if( paramBean.isStatusMatrixV3() ){

						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( raDtoList.get(0).getRaEntity().getLotId() )
						.setPjtIds( support.getPjtNo( raDtoList ) )
						.setAreqIds( support.getAssetApplyNo( raDtoList ) )
						.setBpIds( support.getBpIdsFromRaDto( raDtoList ) )
						.setRaIds( raDtoList.get(0).getRaEntity().getRaId() )
						.setRpIds( support.getRelNoFromRpEntity( relEntities ) );

						StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
					}

					// リリース申請情報の更新
					this.updateRelAplyEntityToCancel( raDtoList.get(0), userName , userId );

					//メール送信用にリリース申請をキャッシュする
					paramBean.getParamList().add( raDtoList.get(0) );

					// ロット番号でリリースエンティティを取りまとめ
					List<ILotEntity> lotEntities =
						this.support.getPjtLotEntityList( new String[]{raDtoList.get(0).getRaEntity().getLotId()} );
					List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( lotEntities );

					// グループの存在チェック
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					for ( ILotDto lot : lotDto ) {
						RelCommonAddonUtil.checkAccessableGroup( lot, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
					}


					RelApplyDetailViewBean bean = new RelApplyDetailViewBean();
					paramBean.setRelApplyDetailViewBean(bean);

					bean.setRelApplyNo(raDtoList.get(0).getRaEntity().getRaId());

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005037S, e);
		}
	}

	/**
	 * リリース情報を取消に更新する
	 * @param entity リリース申請エンティティ
	 * @param userName ユーザ名
	 * @param userId ユーザID
	 * @param systemDate 更新日時
	 */
	private void updateRelAplyEntityToCancel(IRaDto dto,
			String userName, String userId ) {
		IRaEntity entity = dto.getRaEntity();

		String stsId = RmRaStatusId.ReleaseRequestRemoved.getStatusId();

		if( entity.getStsId().equals( RmRaStatusId.DraftReleaseRequest.getStatusId() ) ){
			stsId = RmRaStatusId.DraftReleaseRequestRemoved.getStatusId();

		}else if( entity.getStsId().equals( RmRaStatusId.PendingReleaseRequest.getStatusId() ) ){
			stsId = RmRaStatusId.PendingReleaseRequestRemoved.getStatusId();
		}

		entity.setDelTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setDelUserNm			( userName );
		entity.setDelUserId			( userId );
		entity.setDelStsId			( StatusFlg.on );
		entity.setStsId				( stsId );
		entity.setUpdUserNm			( userName );
		entity.setUpdUserId			( userId );

		this.support.getRaDao().update( entity );

		// rm_ra_bp_lnk 論理削除
		RaBpLnkCondition bpCondition = new RaBpLnkCondition();
		bpCondition.setRaId( entity.getRaId() );
		IRaBpLnkEntity bpEntity = new RaBpLnkEntity();
		bpEntity.setDelStsId( StatusFlg.on );
		this.support.getRaBpLnkDao().update( bpCondition.getCondition(), bpEntity );

		// rm_ra_attached_file 論理削除
		RaAttachedFileCondition fileCondition = new RaAttachedFileCondition();
		fileCondition.setRaId( entity.getRaId() );
		RaAttachedFileEntity fileEntity = new RaAttachedFileEntity();
		fileEntity.setDelStsId( StatusFlg.on );
		this.support.getRaAttachedFileDao().update( fileCondition.getCondition(), fileEntity );

		// rm_ra_rp_lnk 論理削除
		RaRpLnkCondition rpCondition = new RaRpLnkCondition();
		rpCondition.setRaId( entity.getRaId() );
		IRaRpLnkEntity rpEntity = new RaRpLnkEntity();
		rpEntity.setDelStsId( StatusFlg.on );
		this.support.getRaRpLnkDao().update( rpCondition.getCondition(), rpEntity );

		if ( DesignSheetUtils.isRecord() ) {
			// 履歴用DTO作成
			RaCondition condition = new RaCondition();
			condition.setRaId(entity.getRaId());
			condition.setDelStsId( StatusFlg.on );
			IRaDto raDto = support.findRaDto( condition.getCondition() ).get(0);

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Remove.getStatusId() );

			support.getRaHistDao().insert( histEntity , raDto);
		}
	}

}
