package jp.co.blueship.tri.rm.domain.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmItemCheckAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請・リリース申請画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelApplyEntryService implements IDomain<FlowRelApplyEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyEntryServiceBean> execute( IServiceDto<FlowRelApplyEntryServiceBean> serviceDto ) {

		FlowRelApplyEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			/*
			 * 条件の作成
			 */
			RelApplyPackageListSearchBean searchBean = DBSearchBeanAddonUtil
					.setRelUnitSearchBeanByRelApplyEntry(paramBean.getRelApplyPackageListSearchBean());

			if ( !refererID.equals( RelApplyScreenID.ENTRY ) &&
					!forwardID.equals( RelApplyScreenID.ENTRY ) ){
				return serviceDto;
			}

			if( refererID.equals( RelApplyScreenID.ENTRY )) {
				if ( ScreenType.next.equals( screenType ) ) {
					if ( RelApplyScreenID.SAVE_COMP.equals(forwardID) ) {
						RmItemCheckAddonUtils.checkRelApplyEntryInput(
								RmDesignBeanId.flowRelApplySaveInputCheck,
								paramBean.getRelApplyPackageListSearchBean(),
								paramBean.getRelApplyEntryViewBean() );
					} else {
						RmItemCheckAddonUtils.checkRelApplyEntryInput(
								RmDesignBeanId.flowRelApplyInputCheck,
								paramBean.getRelApplyPackageListSearchBean(),
								paramBean.getRelApplyEntryViewBean() );
					}

					RmItemCheckAddonUtils.checkRelApplyEntryConfirm(
							support,
							support.getUmFinderSupport(),
							paramBean,
							paramBean.getRelApplyPackageListSearchBean(),
							paramBean.getRelApplyEntryViewBean());
				}
			}

			if ( refererID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT )
					&& forwardID.equals( RelApplyScreenID.ENTRY ) ) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					/*
					 * 選択項目取得
					 */
					String lotId = searchBean.getSelectedLotNo() ;
					String buildNo = searchBean.getSelectedBuildNo() ;

					//ビルド情報の取得
					IBpDto bpDto = this.support.getBmFinderSupport().findBpDto(buildNo);
					IBpEntity bpEntity = bpDto.getBpEntity();

					//ロット情報の取得
					List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( lotId );
					lotDtoEntities.add(lotDto);

					//ビルド関連資産申請情報の取得
					List<IBpAreqLnkEntity> buildApplyEntityArray = bpDto.getBpAreqLnkEntities() ;

					/*
					 * 表示項目構築
					 */
					RelApplyEntryViewBean relApplyEntryViewBean = new RelApplyEntryViewBean() ;
					relApplyEntryViewBean.setLotNo					( lotId ) ;
					relApplyEntryViewBean.setLotName				( lotDto.getLotEntity().getLotNm() ) ;
					relApplyEntryViewBean.setRelApplyNo				( "" ) ;		// 採番してないので空で表示
					paramBean.setSystemDate							( TriDateUtils.getSystemTimestamp() );
					relApplyEntryViewBean.setRelApplyDate			( TriDateUtils.convertViewDateFormat( paramBean.getSystemDate() ) ) ;
					relApplyEntryViewBean.setRelApplyGroupId		( "" ) ;		// 空で初期表示
					relApplyEntryViewBean.setRelEnvNo				( "" ) ;		// 空で初期表示
					relApplyEntryViewBean.setRelApplyUserId			( paramBean.getUserId() ) ;
					relApplyEntryViewBean.setRelApplyUser			( "" ) ;		// 諸事情により名前は表示しない
					relApplyEntryViewBean.setRelApplyPersonChargeId	( "" ) ;		// 空とする
					relApplyEntryViewBean.setRelApplyPersonCharge	( "" ) ;
					relApplyEntryViewBean.setRelWishDate			( "" ) ;		// 空で初期表示

					List<IRaBpLnkEntity> raBpLnkEntities = new ArrayList<IRaBpLnkEntity>();
					{
						RaBpLnkEntity raBpEntity = new RaBpLnkEntity();
						raBpEntity.setBpId(bpEntity.getBpId());
						raBpEntity.setBpNm(bpEntity.getBpNm());
						raBpEntity.setSummary(bpEntity.getSummary());
						raBpEntity.setContent(bpEntity.getContent());

						raBpLnkEntities.add( raBpEntity );
					}

					relApplyEntryViewBean.setUnitBeanList			( RmConverterAddonUtils.convertUnitBean( raBpLnkEntities ) ) ;
					relApplyEntryViewBean.setPjtNoList				( RmConverterAddonUtils.convertPjtNoList(this.support , buildApplyEntityArray) ) ;
					relApplyEntryViewBean.setChangeCauseNoList		( RmConverterAddonUtils.convertChangeCauseNoList(this.support, buildApplyEntityArray) ) ;
					relApplyEntryViewBean.setAssetApplyNoList		( RmConverterAddonUtils.convertAssetApplyNoListByBpAreq(buildApplyEntityArray) ) ;

					relApplyEntryViewBean.setRelatedNo				("") ;			// 空で初期表示
					relApplyEntryViewBean.setRemarks				("") ;			// 空で初期表示

					/*
					 * 選択可能なリリース環境リスト取得
					 */
					IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
					IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

					paramBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );

					/*
					 * 選択可能なリリース希望日リスト取得
					 */
					ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

					paramBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );

					/*
					 * 選択可能なグループリスト取得
					 */
					List<IGrpUserLnkEntity> groupUserview = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );

					paramBean.setGroupIdList( ItemLabelsUtils.conv( support.getUmFinderSupport(), groupUserview ) );

					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean( relApplyEntryViewBean ) ;
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005045S, e);
		}
	}

}
