package jp.co.blueship.tri.rm.domain.ra.beans.dto;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class RelApplyDetailViewBean {

	private String lotId = null ;
	private String lotName = null ;
	private String relApplyNo = null ;
	private String relApplyDate = null ;
	private String relApplyGroupId = null ;
	private String relApplyGroup = null ;
	private String relEnvNo = null ;
	private String relEnvName = null ;
	private String relApplyUserId = null ;
	private String relApplyUser = null ;
	private String relApplyPersonChargeId = null ;
	private String relApplyPersonCharge = null ;
	private String relWishDate = null ;
	private List<UnitBean> unitBeanList = null ;
	private List<String> buildDateList = null;
	private List<String> pjtNoList = null ;
	private List<String> changeCauseNoList = null ;
	private List<String> assetApplyNoList = null ;
	private String relationNo = null ;
	private String remarks = null ;
	private Map<String, AppendFileBean> appendFile = new TreeMap<String, AppendFileBean>();
	
	public class AppendFileBean {
		/**
		 * 添付ファイルの名前
		 */
		private String appendFile;
		/**
		 * 添付ファイルリンク
		 */
		private String appendFileLink;
		
		public String getAppendFile() {
			return appendFile;
		}
		public void setAppendFile(String appendFile) {
			this.appendFile = appendFile;
		}
		public String getAppendFileLink() {
			return appendFileLink;
		}
		public void setAppendFileLink(String appendFileLink) {
			this.appendFileLink = appendFileLink;
		}
	}
	
	public Map<String, AppendFileBean> newAppendFile() {
		return new TreeMap<String, AppendFileBean>();
	}
	
	public AppendFileBean newAppendFileBean() {
		return new AppendFileBean();
	}
	
	public String getRelApplyAppendFile1() {
		return (appendFile.containsKey("0001"))? appendFile.get("0001").getAppendFile(): null;
	}
	
	public String getRelApplyAppendFile2() {
		return (appendFile.containsKey("0002"))? appendFile.get("0002").getAppendFile(): null;
	}
	
	public String getRelApplyAppendFile3() {
		return (appendFile.containsKey("0003"))? appendFile.get("0003").getAppendFile(): null;
	}
	
	public String getRelApplyAppendFile4() {
		return (appendFile.containsKey("0004"))? appendFile.get("0004").getAppendFile(): null;
	}
	
	public String getRelApplyAppendFile5() {
		return (appendFile.containsKey("0005"))? appendFile.get("0005").getAppendFile(): null;
	}
	
	public String getRelApplyAppendFileLink1() {
		return (appendFile.containsKey("0001"))? appendFile.get("0001").getAppendFileLink(): null;
	}
	
	public String getRelApplyAppendFileLink2() {
		return (appendFile.containsKey("0002"))? appendFile.get("0002").getAppendFileLink(): null;
	}
	
	public String getRelApplyAppendFileLink3() {
		return (appendFile.containsKey("0003"))? appendFile.get("0003").getAppendFileLink(): null;
	}
	
	public String getRelApplyAppendFileLink4() {
		return (appendFile.containsKey("0004"))? appendFile.get("0004").getAppendFileLink(): null;
	}
	
	public String getRelApplyAppendFileLink5() {
		return (appendFile.containsKey("0005"))? appendFile.get("0005").getAppendFileLink(): null;
	}
	
	private List<RelBean> relBeanList = null ;

	
	public String getRelApplyDate() {
		return relApplyDate;
	}
	public void setRelApplyDate(String relApplyDate) {
		this.relApplyDate = relApplyDate;
	}
	public String getRelApplyNo() {
		return relApplyNo;
	}
	public void setRelApplyNo(String relApplyNo) {
		this.relApplyNo = relApplyNo;
	}
	public String getRelationNo() {
		return relationNo;
	}
	public void setRelatedNo(String relationNo) {
		this.relationNo = relationNo;
	}
	
	public String getRemarks() {
		return remarks;
	}
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	public String getRelApplyGroupId() {
		return relApplyGroupId;
	}
	public void setRelApplyGroupId(String relApplyGroupId) {
		this.relApplyGroupId = relApplyGroupId;
	}
	public String getRelEnvNo() {
		return relEnvNo;
	}
	public void setRelEnvNo(String relEnvNo) {
		this.relEnvNo = relEnvNo;
	}
	public String getRelWishDate() {
		return relWishDate;
	}
	public void setRelWishDate(String relWishDate) {
		this.relWishDate = relWishDate;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}
	public String getRelApplyPersonCharge() {
		return relApplyPersonCharge;
	}
	public void setRelApplyPersonCharge(String relApplyPersonCharge) {
		this.relApplyPersonCharge = relApplyPersonCharge;
	}
	public String getRelApplyUser() {
		return relApplyUser;
	}
	public void setRelApplyUser(String relApplyUser) {
		this.relApplyUser = relApplyUser;
	}
	public List<String> getAssetApplyNoList() {
		return assetApplyNoList;
	}
	public void setAssetApplyNoList(List<String> assetApplyNoList) {
		this.assetApplyNoList = assetApplyNoList;
	}
	public List<String> getChangeCauseNoList() {
		return changeCauseNoList;
	}
	public void setChangeCauseNoList(List<String> changeCauseNoList) {
		this.changeCauseNoList = changeCauseNoList;
	}
	public List<String> getPjtNoList() {
		return pjtNoList;
	}
	public void setPjtNoList(List<String> pjtNoList) {
		this.pjtNoList = pjtNoList;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getRelApplyPersonChargeId() {
		return relApplyPersonChargeId;
	}
	public void setRelApplyPersonChargeId(String relApplyPersonChargeId) {
		this.relApplyPersonChargeId = relApplyPersonChargeId;
	}
	public String getRelApplyUserId() {
		return relApplyUserId;
	}
	public void setRelApplyUserId(String relApplyUserId) {
		this.relApplyUserId = relApplyUserId;
	}
	public List<UnitBean> getUnitBeanList() {
		return unitBeanList;
	}
	public void setUnitBeanList(List<UnitBean> unitBeanList) {
		this.unitBeanList = unitBeanList;
	}
	public String getRelApplyGroup() {
		return relApplyGroup;
	}
	public void setRelApplyGroup(String relApplyGroup) {
		this.relApplyGroup = relApplyGroup;
	}
	public String getRelEnvName() {
		return relEnvName;
	}
	public void setRelEnvName(String relEnvName) {
		this.relEnvName = relEnvName;
	}
	public List<String> getBuildDateList() {
		return buildDateList;
	}
	public void setBuildDateList(List<String> buildDateList) {
		this.buildDateList = buildDateList;
	}
	public void setRelationNo(String relationNo) {
		this.relationNo = relationNo;
	}
	public List<RelBean> getRelBeanList() {
		return relBeanList;
	}
	public void setRelBeanList(List<RelBean> relBeanList) {
		this.relBeanList = relBeanList;
	}
	public Map<String, AppendFileBean> getAppendFile() {
		return appendFile;
	}
	public void setAppendFile(Map<String, AppendFileBean> appendFile) {
		this.appendFile = appendFile;
	}

}
