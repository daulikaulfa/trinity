package jp.co.blueship.tri.rm.domain.ra.beans;

import java.util.List;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.AssetApplyBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;


/**
 * リリース申請編集完了処理Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelApplyModifyServiceComplete implements IDomain<FlowRelApplyModifyServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyModifyServiceBean> execute(IServiceDto<FlowRelApplyModifyServiceBean> serviceDto ) {

		FlowRelApplyModifyServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwardID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			/*
			 * 申請情報取得
			 */
			RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

			if ( !refererID.equals( RelApplyScreenID.MODIFY_COMP) &&
					!forwardID.equals( RelApplyScreenID.MODIFY_COMP ) ) {
				return serviceDto;
			}

			if ( refererID.equals( RelApplyScreenID.MODIFY_COMP ) ) {

			}

			if ( refererID.equals( RelApplyScreenID.MODIFY_CONFIRM )
					&& forwardID.equals( RelApplyScreenID.MODIFY_COMP ) ) {

				if ( ! ScreenType.bussinessException.equals( screenType ) ) {

					/*
					 * ビルドパッケージに含まれる申請情報の取得
					 */
					List<IBpEntity> buildEntityList = RmConverterAddonUtils.convertBuildEntity( this.support, bean.getUnitBeanList() );
					List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(buildEntityList);
					List<AssetApplyBean> assetApplyBeanList = RmConverterAddonUtils.convertAssetApplyBeanList( this.support, bpDtoList );

					// すでにあるリリース申請情報を取得
					IRaEntity entity = this.support.findRaEntity( bean.getRelApplyNo() ) ;
					String beforeRelApplyNo = bean.getRelApplyNo();

					// 前ステータスが保存中のときのみ改めて採番
					// 承認却下状態の場合、すでに仮番号でない番号で発番されているため、改めて採番しない
					if( RmRaStatusId.DraftReleaseRequest.equals( entity.getProcStsId() )) {
						String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

						String oldNo = bean.getRelApplyNo();

						// リリース申請番号の採番
						String relApplyNo = this.support.nextvalByRaId();
						bean.setRelApplyNo(relApplyNo);

						BusinessFileUtils.moveAppendFile( basePath, oldNo, relApplyNo );
					}

					// リリース申請情報の更新
					this.updateRelApply(
							paramBean,
							beforeRelApplyNo,
							entity,
							TriDateUtils.getSystemDate(),
							assetApplyBeanList);
					
					this.support.getUmFinderSupport().cleaningCtgLnk( RmTables.RM_RA, beforeRelApplyNo );
					this.support.getUmFinderSupport().cleaningMstoneLnk( RmTables.RM_RA, beforeRelApplyNo );
					this.support.getUmFinderSupport().registerCtgLnk( bean.getCtgId(), RmTables.RM_RA, bean.getRelApplyNo() );
					this.support.getUmFinderSupport().registerMstoneLnk( bean.getMstoneId(), RmTables.RM_RA, bean.getRelApplyNo() );
					
					/*
					 * 戻りに設定
					 */
					paramBean.setRelApplyEntryViewBean(bean);

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005034S, e);
		}
	}

	/**
	 * リリース申請情報を確定する
	 *
	 */
	private void updateRelApply(
			FlowRelApplyModifyServiceBean paramBean,
			String beforeRelApplyNo,
			IRaEntity entity,
			String systemDate,
			List<AssetApplyBean> assetApplyBeanList) {

		RelApplyEntryViewBean bean = paramBean.getRelApplyEntryViewBean();

		// 属性
		entity.setLotId				( bean.getLotNo() ) ;
		entity.setRaId				( bean.getRelApplyNo() ) ;		// 採番した番号
		entity.setReqTimestamp		( paramBean.getSystemDate() ) ;
		entity.setReqUserId			( paramBean.getUserId() ) ;
		entity.setReqUser			( bean.getRelApplyUser() ) ;
		entity.setReqGrpId			( bean.getRelApplyGroupId() ) ;
		entity.setReqGrpNm			( bean.getRelApplyGroup() ) ;
		entity.setBldEnvId			( bean.getRelEnvNo() ) ;
		entity.setPreferredRelDate	( bean.getRelWishDate() );
		entity.setRelationId		( bean.getRelatedNo() ) ;
		// システム属性
		entity.setStsId				( RmRaStatusId.ReleaseRequested.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );

		// 要素
		entity.setRemarks( bean.getRemarks() ) ;

		//RaBpLnkの更新
		this.support.deleteRaBpLnk(bean);
		this.support.insertRaBpLnk(bean);

		// 指定された添付ファイルを除外する（添付ファイルの削除）
		for ( String id: bean.getDelAppendFiles() ) {
			if ( bean.getAppendFile().containsKey(id) ) {
				bean.getAppendFile().remove(id);
			}
		}

		//添付ファイル
		this.support.insertRaAttachedFile(bean);

		// 関連要素(rel)
		// 申請のタイミングでは行わない

		// 関連要素(responsibleUser)
		entity.setReqResponsibility( bean.getRelApplyPersonCharge() );
		// 関連要素(responsibleGroup)
		// 申請承認グループ用 未実装


		if(bean.getRelApplyNo().equals(beforeRelApplyNo)) {
			this.support.getRaDao().update( entity );
		} else {
			RaCondition condition = new RaCondition();
			condition.setRaId(beforeRelApplyNo);
			this.support.getRaDao().delete( condition.getCondition() );
			this.support.getRaDao().insert( entity );
		}
		
		//chenged to "Requested" the view status.
		this.support.getSmFinderSupport().cleaningExecDataSts( RmTables.RM_RA, entity.getRaId() );

		// Ra Dto作成
		IRaDto raDto = support.findRaDto(entity.getRaId());
		if ( DesignSheetUtils.isRecord() ) {

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getRaHistDao().insert( histEntity , raDto);
		}

		//メール送信用にリリース申請をキャッシュする
		paramBean.getParamList().add( raDto );
	}
}
