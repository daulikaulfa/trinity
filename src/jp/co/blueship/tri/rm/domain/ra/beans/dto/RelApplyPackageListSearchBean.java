package jp.co.blueship.tri.rm.domain.ra.beans.dto;


public class RelApplyPackageListSearchBean {
	
	/** 
	 * sessionに保持する場合、任意に一意とするキー値 
	 */
	private String sessionKey = null;
	/** 
	 * 詳細検索 選択済みロット番号 
	 */
	private String selectedLotNo = null ;
	/** 
	 * 詳細検索 選択済みサーバ番号（ロット番号とカンマ区切りで連結用） 
	 */
	private String selectedServerNo = null ;
	
	/** 
	 * 詳細検索 選択済み検索件数 
	 */
	private String selectedRelUnitCount = null ;
	
	/** 
	 * 詳細検索 選択済みビルドパッケージ番号 
	 */
	private String selectedBuildNo = null ;
	
	/** 
	 * 詳細検索 選択済みグループID
	 */
	private String selectedGroupId = null ;
	
	/** 
	 * 詳細検索 選択済みリリース環境 
	 */
	private String selectedRelEnvNo = null ;
	
	/** 
	 * 詳細検索 選択済みリリース希望日 
	 */
	private String selectedRelWishDate = null ;
	
	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo(String selectedBuildNo) {
		this.selectedBuildNo = selectedBuildNo;
	}
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public String getSelectedGroupId() {
		return selectedGroupId;
	}
	public void setSelectedGroupId(String selectedGroupId) {
		this.selectedGroupId = selectedGroupId;
	}
	public String getSelectedRelEnvNo() {
		return selectedRelEnvNo;
	}
	public void setSelectedRelEnvNo(String selectedRelEnvNo) {
		this.selectedRelEnvNo = selectedRelEnvNo;
	}
	public String getSelectedRelWishDate() {
		return selectedRelWishDate;
	}
	public void setSelectedRelWishDate(String selectedRelWishDate) {
		this.selectedRelWishDate = selectedRelWishDate;
	}
	public String getSelectedRelUnitCount() {
		return selectedRelUnitCount;
	}
	public void setSelectedRelUnitCount(String selectedRelUnitCount) {
		this.selectedRelUnitCount = selectedRelUnitCount;
	}
}
