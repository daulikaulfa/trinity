package jp.co.blueship.tri.rm.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * リリース申請の詳細検索を行うための検索条件情報です。
 *
 * @author blueship
 *
 */
public class RelApplySearchBean implements Serializable {

	private static final long serialVersionUID = 1L;

	

	/**
	 * sessionに保持する場合、任意に一意とするキー値
	 */
	private String sessionKey = null;
	/**
	 * 詳細検索 ロット番号（コンボ用）
	 */
	private List<ItemLabelsBean> searchLotNoList = null ;
	/**
	 * 詳細検索 ロット番号
	 */
	private List<String> searchLotNoValueList = null ;
	/**
	 * 詳細検索 選択済みロット番号
	 */
	private String selectedLotNo = null ;
	/**
	 * 詳細検索 選択済みサーバ番号（ロット番号とカンマ区切りで連結用）
	 */
	private String selectedServerNo = null ;
	/**
	 * 詳細検索 リリース環境（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelEnvNoList = null ;
	/**
	 * 詳細検索 リリース環境
	 */
	private List<String> searchRelEnvNoValueList = null ;
	/**
	 * 詳細検索 選択済みリリース環境
	 */
	private String selectedRelEnvNo = null ;
	/**
	 * 詳細検索 リリース希望日（コンボ用）
	 */
	private List<ItemLabelsBean> searchRelWishDateList = null ;
	/**
	 * 詳細検索 リリース希望日
	 */
	private List<String> searchRelWishDateValueList = null ;
	/**
	 * 詳細検索 選択済みリリース希望日
	 */
	private String selectedRelWishDate = null ;
	/**
	 * 詳細検索 グループ（コンボ用）
	 */
	private List<ItemLabelsBean> searchGroupIdList = null ;
	/**
	 * 詳細検索 グループ
	 */
	private List<String> searchGroupIdValueList = null ;
	/**
	 * 詳細検索 選択済みグループ
	 */
	private String selectedGroupId = null ;
	/**
	 * 詳細検索 ステータス
	 */
	//private List<ItemLabelsBean> searchStatusList = null ;
	/**
	 * 詳細検索 選択済みステータス
	 */
	//private String selectedStatus = null ;
	/**
	 * 詳細検索 リリース申請番号
	 */
	private String searchRelApplyNo = null;
	/**
	 * 詳細検索 ビルドパッケージ番号
	 */
	private String searchBuildNo = null ;
	/**
	 * 詳細検索 リリース番号
	 */
	private String searchRelNo = null;
	/**
	 * 詳細検索 リリース概要
	 */
	private String searchSummary = null ;
	/**
	 * 詳細検索 リリース内容
	 */
	private String searchContent = null ;
	/**
	 * 詳細検索 変更管理番号
	 */
	private String searchPjtNo = null;
	/**
	 * 詳細検索 変更要因番号
	 */
	private String searchChangeCauseNo = null ;
	/**
	 * 詳細検索 関連番号
	 */
	private String searchRelationNo = null ;
	/**
	 * 詳細検索 検索件数
	 */
	private List<ItemLabelsBean> searchRelApplyCountList = null ;
	/**
	 * 詳細検索 選択済み検索件数
	 */
	private String selectedRelApplyCount = null ;

	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public List<ItemLabelsBean> getSearchLotNoList() {
		return searchLotNoList;
	}
	public void setSearchLotNoList(List<ItemLabelsBean> searchLotNoList) {
		this.searchLotNoList = searchLotNoList;
	}
	public List<String> getSearchLotNoValueList() {
		return searchLotNoValueList;
	}
	public void setSearchLotNoValueList(List<String> searchLotNoValueList) {
		this.searchLotNoValueList = searchLotNoValueList;
	}
//	public List<ItemLabelsBean> getSearchStatusList() {
//		return searchStatusList;
//	}
//	public void setSearchStatusList(List<ItemLabelsBean> searchStatusList) {
//		this.searchStatusList = searchStatusList;
//	}
//	public String getSelectedStatus() {
//		return selectedStatus;
//	}
//	public void setSelectedStatus(String selectedStatus) {
//		this.selectedStatus = selectedStatus;
//	}
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}
	public List<ItemLabelsBean> getSearchRelEnvNoList() {
		return searchRelEnvNoList;
	}
	public void setSearchRelEnvNoList(List<ItemLabelsBean> searchRelEnvNoList) {
		this.searchRelEnvNoList = searchRelEnvNoList;
	}
	public List<String> getSearchRelEnvNoValueList() {
		return searchRelEnvNoValueList;
	}
	public void setSearchRelEnvNoValueList(List<String> searchRelEnvNoValueList) {
		this.searchRelEnvNoValueList = searchRelEnvNoValueList;
	}
	public String getSelectedRelEnvNo() {
		return selectedRelEnvNo;
	}
	public void setSelectedRelEnvNo(String selectedRelEnvNo) {
		this.selectedRelEnvNo = selectedRelEnvNo;
	}
	public List<ItemLabelsBean> getSearchRelWishDateList() {
		return searchRelWishDateList;
	}
	public void setSearchRelWishDateList(List<ItemLabelsBean> searchRelWishDateList) {
		this.searchRelWishDateList = searchRelWishDateList;
	}
	public List<String> getSearchRelWishDateValueList() {
		return searchRelWishDateValueList;
	}
	public void setSearchRelWishDateValueList(
			List<String> searchRelWishDateValueList) {
		this.searchRelWishDateValueList = searchRelWishDateValueList;
	}
	public String getSelectedRelWishDate() {
		return selectedRelWishDate;
	}
	public void setSelectedRelWishDate(String selectedRelWishDate) {
		this.selectedRelWishDate = selectedRelWishDate;
	}

	public String getSearchRelApplyNo() {
		return searchRelApplyNo;
	}
	public void setSearchRelApplyNo(String searchRelApplyNo) {
		this.searchRelApplyNo = searchRelApplyNo;
	}
	public String getSearchBuildNo() {
		return searchBuildNo;
	}
	public void setSearchBuildNo(String searchBuildNo) {
		this.searchBuildNo = searchBuildNo;
	}
	public String getSearchRelNo() {
		return searchRelNo;
	}
	public void setSearchRelNo(String searchRelNo) {
		this.searchRelNo = searchRelNo;
	}
	public String getSearchSummary() {
		return searchSummary;
	}
	public void setSearchSummary(String searchSummary) {
		this.searchSummary = searchSummary;
	}
	public String getSearchContent() {
		return searchContent;
	}
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}
	public String getSearchPjtNo() {
		return searchPjtNo;
	}
	public void setSearchPjtNo(String searchPjtNo) {
		this.searchPjtNo = searchPjtNo;
	}
	public String getSearchChangeCauseNo() {
		return searchChangeCauseNo;
	}
	public void setSearchChangeCauseNo(String searchChangeCauseNo) {
		this.searchChangeCauseNo = searchChangeCauseNo;
	}
	public String getSearchRelationNo() {
		return searchRelationNo;
	}
	public void setSearchRelationNo(String searchRelationNo) {
		this.searchRelationNo = searchRelationNo;
	}

	public List<ItemLabelsBean> getSearchRelApplyCountList() {
		return searchRelApplyCountList;
	}
	public void setSearchRelApplyCountList(
			List<ItemLabelsBean> searchRelApplyCountList) {
		this.searchRelApplyCountList = searchRelApplyCountList;
	}
	public String getSelectedRelApplyCount() {
		return selectedRelApplyCount;
	}
	public void setSelectedRelApplyCount(String selectedRelApplyCount) {
		this.selectedRelApplyCount = selectedRelApplyCount;
	}
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}
	public List<ItemLabelsBean> getSearchGroupIdList() {
		return searchGroupIdList;
	}
	public void setSearchGroupIdList(List<ItemLabelsBean> searchGroupIdList) {
		this.searchGroupIdList = searchGroupIdList;
	}
	public List<String> getSearchGroupIdValueList() {
		return searchGroupIdValueList;
	}
	public void setSearchGroupIdValueList(List<String> searchGroupIdValueList) {
		this.searchGroupIdValueList = searchGroupIdValueList;
	}
	public String getSelectedGroupId() {
		return selectedGroupId;
	}
	public void setSelectedGroupId(String selectedGroupId) {
		this.selectedGroupId = selectedGroupId;
	}

}
