package jp.co.blueship.tri.rm.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * リリースの詳細検索を行うための検索条件情報です。
 * 
 * @author blueship
 *
 */
public class RelCtlSearchBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 
	 * sessionに保持する場合、任意に一意とするキー値 
	 */
	private String sessionKey = null;
	/** 
	 * 詳細検索 ロット番号（コンボ用）
	 */
	private List<ItemLabelsBean> searchLotNoList = null ;
	/** 
	 * 詳細検索 ロット番号
	 */
	private List<String> searchLotNoValueList = null ;
	/** 
	 * 詳細検索 選択済みロット番号 
	 */
	private String selectedLotNo = null ;
	/** 
	 * 詳細検索 選択済みサーバ番号（ロット番号とカンマ区切りで連結用） 
	 */
	private String selectedServerNo = null ;
	/** 
	 * 詳細検索 ステータス 
	 */
	//private List<ItemLabelsBean> searchStatusList = null ;
	/** 
	 * 詳細検索 選択済み検索件数 
	 */
	//private String selectedStatus = null ;
	/** 
	 * 詳細検索 リリース番号 
	 */
	private String searchRelNo = null;
	/** 
	 * 詳細検索 ビルドパッケージ番号 
	 */
	private String searchBuildNo = null ;
	/** 
	 * 詳細検索 リリース概要 
	 */
	private String searchSummary = null ;
	/** 
	 * 詳細検索 リリース内容 
	 */
	private String searchContent = null ;
	/** 
	 * 詳細検索 変更管理番号 
	 */
	private String searchPjtNo = null;
	/** 
	 * 詳細検索 変更要因番号 
	 */
	private String searchChangeCauseNo = null ;
	/** 
	 * 詳細検索 検索件数 
	 */
	private List<ItemLabelsBean> searchReleaseCountList = null ;
	/** 
	 * 詳細検索 選択済み検索件数 
	 */
	private String selectedReleaseCount = null ;
	
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}

	public String getSearchContent() {
		return searchContent;
	}
	public void setSearchContent(String searchContent) {
		this.searchContent = searchContent;
	}

	public String getSearchBuildNo() {
		return searchBuildNo;
	}
	public void setSearchBuildNo(String searchBuildNo) {
		this.searchBuildNo = searchBuildNo;
	}
	public List<ItemLabelsBean> getSearchReleaseCountList() {
		return searchReleaseCountList;
	}
	public void setSearchReleaseCountList(
			List<ItemLabelsBean> searchReleaseCountList) {
		this.searchReleaseCountList = searchReleaseCountList;
	}
	public String getSearchSummary() {
		return searchSummary;
	}
	public void setSearchSummary(String searchSummary) {
		this.searchSummary = searchSummary;
	}
	public String getSelectedReleaseCount() {
		return selectedReleaseCount;
	}
	public void setSelectedReleaseCount(String selectedReleaseCount) {
		this.selectedReleaseCount = selectedReleaseCount;
	}
	public String getSearchRelNo() {
		return searchRelNo;
	}
	public void setSearchRelNo(String searchRelNo) {
		this.searchRelNo = searchRelNo;
	}

	public List<ItemLabelsBean> getSearchLotNoList() {
		return searchLotNoList;
	}
	public void setSearchLotNoList(List<ItemLabelsBean> searchLotNoList) {
		this.searchLotNoList = searchLotNoList;
	}
	public List<String> getSearchLotNoValueList() {
		return searchLotNoValueList;
	}
	public void setSearchLotNoValueList(List<String> searchLotNoValueList) {
		this.searchLotNoValueList = searchLotNoValueList;
	}
//	public List<ItemLabelsBean> getSearchStatusList() {
//		return searchStatusList;
//	}
//	public void setSearchStatusList(List<ItemLabelsBean> searchStatusList) {
//		this.searchStatusList = searchStatusList;
//	}
//	public String getSelectedStatus() {
//		return selectedStatus;
//	}
//	public void setSelectedStatus(String selectedStatus) {
//		this.selectedStatus = selectedStatus;
//	}
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}
	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}
	public String getSearchChangeCauseNo() {
		return searchChangeCauseNo;
	}
	public void setSearchChangeCauseNo(String searchChangeCauseNo) {
		this.searchChangeCauseNo = searchChangeCauseNo;
	}
	public String getSearchPjtNo() {
		return searchPjtNo;
	}
	public void setSearchPjtNo(String searchPjtNo) {
		this.searchPjtNo = searchPjtNo;
	}

	
}
