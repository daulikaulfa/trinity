package jp.co.blueship.tri.rm.beans.mail;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.CreateMailContentsException;
import jp.co.blueship.tri.fw.mail.beans.IMailCallback;
import jp.co.blueship.tri.fw.mail.beans.MailServiceSupport;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.beans.mail.dto.RaMailDto;
import jp.co.blueship.tri.rm.beans.mail.dto.RelApplyMailServiceBean;
import jp.co.blueship.tri.rm.beans.mail.dto.RaMailDto.RaRpLnkMailEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;



/**
 * リリース申請のアクション実行時、メール送信を行う汎用クラス。
 * <br>columnに指定可能な項目
 * <br>・コメントの項目
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class MailFlowRelApplyGeneralService extends MailServiceSupport {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * {@inheritDoc}
	 */
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {
			this.sendMail(
					this.getMailServiceBean( serviceDto ),
					new IMailCallback() {

						public Map<String, Object> getContents( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getContentsCallBack( bean, data );
						}

						public Map<String, Object> getSubject( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getSubjectCallBack( bean, data );
						}
					});

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( RmMessageId.RM005072S, e );
		}

	}

	/**
	 * メール送信の件名を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getSubjectCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {

		RelApplyMailServiceBean mailBean = ( RelApplyMailServiceBean )bean;
		ILotEntity pjtLotEntity	= mailBean.getLotEntity();

		if ( null != pjtLotEntity ) {
			data.put( "_lotId",		mailBean.getLotEntity().getLotId() );
			data.put( "_lotName",	mailBean.getLotEntity().getLotNm() );
		}

		return data;
	}

	/**
	 * メール送信の内容を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getContentsCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {

		RelApplyMailServiceBean mailBean = ( RelApplyMailServiceBean )bean;

		RaMailDto raMailDto	= mailBean.getRelApplyEntity();
		ILotEntity pjtLotEntity		= mailBean.getLotEntity();

		IRaEntity raEntity = raMailDto.getRaEntity();

		data.put( "raEntity", raMailDto.getRaEntity() );
		data.put( "raList", mailBean.getRelApplyEntityList() );

		data.put( "_envNo",		raEntity.getBldEnvId() );
		data.put( "_envName",	raEntity.getBldEnvNm() );
		data.put( "_preferredRelDate",raEntity.getPreferredRelDate() );
		data.put( "_lotId",		TriStringUtils.isEmpty(pjtLotEntity.getLotId())? "": pjtLotEntity.getLotId() );
		data.put( "_lotName",	TriStringUtils.isEmpty(pjtLotEntity.getLotNm())? "": pjtLotEntity.getLotNm() );
		data.put( "_date",		TriDateUtils.convertViewDateFormat( bean.getSystemDate(), TriDateUtils.getYMDDateFormat() ));
		data.put( "_time",		TriDateUtils.convertViewDateFormat( bean.getSystemDate(), TriDateUtils.getHMSDateFormat() ));

		String applyDate = TriDateUtils.convertViewDateFormat( raEntity.getReqTimestamp(), TriDateUtils.getYMDDateFormat() );
		data.put( "_requestDate",	(TriStringUtils.isEmpty(applyDate)? "":applyDate) );

		String applyTime = TriDateUtils.convertViewDateFormat( raEntity.getReqTimestamp(), TriDateUtils.getHMSDateFormat() );
		data.put( "_requestTime",	(TriStringUtils.isEmpty(applyTime)? "":applyTime) );

		data.put( "_sendFrom",	bean.getFrom().getUserView() );
		data.put( "_sendTo",	bean.getTo().getUserView() );
		data.put( "_comment",	TriStringUtils.isEmpty(mailBean.getComment())? "": mailBean.getComment() );

		if ( null != bean.getCc().getUsersView()){
			List<String> ccUser = new ArrayList<String>();
			for(IUserEntity user : bean.getCc().getUsersView()){
				ccUser.add( user.getUserNm() );
			}

			data.put("sendCc",		ccUser);
			data.put("sendCcSize",	bean.getCc().getUsersView().size());
		}

		List<?> messageList = new ArrayList<Object>();
		if ( null != bean.getBusinessThrowable() ) {
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			messageList = ca.getMessage( bean.getBusinessThrowable() );

			data.put( "messageSize", new Integer( messageList.size() ));
			data.put( "messageList", messageList );
		}

		return data;
	}

	/**
	 * メール送信先取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @return 生成したインスタンス
	 */
	private IServiceDto<MailServiceBean> getMailServiceBean( IServiceDto<IGeneralServiceBean> serviceDto ) {
		RelApplyMailServiceBean destBean = new RelApplyMailServiceBean();

		IServiceDto<MailServiceBean> mailServiceDto = new ServiceDto<MailServiceBean>();
		mailServiceDto.setServiceBean( destBean );

		TriPropertyUtils.copyProperties(destBean, serviceDto.getServiceBean());

		destBean.setVmResourceLoaderPath( RmDesignBusinessRuleUtils.getTemplatePath().getPath() );
		Map<String, IRpEntity> relMap =destBean.getRelMap();


		List<IEntity> entityList = new ArrayList<IEntity>();

		//リリース申請クローズ
		if ( ! TriStringUtils.isEmpty(destBean.getRelApplyEntityList()) ) {
			for ( RaMailDto entity: destBean.getRelApplyEntityList() ) {
				this.initBean( entity, relMap );

				entityList.add( entity );
			}

			destBean.setRelApplyEntity( destBean.getRelApplyEntityList().get(0) );
			destBean.setRelApplyEntityList( destBean.getRelApplyEntityList() );
		} else {
			//リリース申請／リリース申請取消
			RaMailDto relApplyEntity = destBean.getRelApplyEntity();
			this.initBean( relApplyEntity, relMap );

			entityList.add( relApplyEntity );
			destBean.setRelApplyEntity( relApplyEntity );
		}


		ILotEntity pjtLotEntity = destBean.getLotEntity();

		entityList.add( pjtLotEntity );

		destBean.setEntity( entityList );

		return mailServiceDto;
	}

	/**
	 * エンティティの値にnullがあれば""（空文字）に変換する。
	 *
	 * @param dto
	 */
	private void initBean( RaMailDto dto, Map<String, IRpEntity> relMap ) {

		RaEntity raEntity = (RaEntity)dto.getRaEntity();

		raEntity.setBldEnvNm		( TriStringUtils.isEmpty(raEntity.getBldEnvNm())? 		"": raEntity.getBldEnvNm() );
		raEntity.setBldEnvId		( TriStringUtils.isEmpty(raEntity.getBldEnvId())?			"": raEntity.getBldEnvId() );
		raEntity.setLotId			( TriStringUtils.isEmpty(raEntity.getLotId())? 				"": raEntity.getLotId() );
		raEntity.setRaId			( TriStringUtils.isEmpty(raEntity.getRaId())? 				"": raEntity.getRaId() );
		raEntity.setReqUser			( TriStringUtils.isEmpty(raEntity.getReqUser())? 			"": raEntity.getReqUser() );
		raEntity.setRelationId		( TriStringUtils.isEmpty(raEntity.getRelationId())? 		"": raEntity.getRelationId() );
		raEntity.setPreferredRelDate( TriStringUtils.isEmpty(raEntity.getPreferredRelDate())?	"": raEntity.getPreferredRelDate() );
		raEntity.setRemarks			( TriStringUtils.isEmpty(raEntity.getRemarks())? 			"": raEntity.getRemarks() );

		//メール用にダミーを設定
		if ( TriStringUtils.isEmpty(raEntity.getReqResponsibility()) ) {
			raEntity.setReqResponsibility("");
		}

		if ( ! TriStringUtils.isEmpty(dto) ) {
			List<IRaRpLnkEntity> raRpLnkEntities = new ArrayList<IRaRpLnkEntity>();
			for ( IRaRpLnkEntity raRpLnkEntity: dto.getRaRpLnkEntities() ) {
				RaRpLnkMailEntity inner = (RaRpLnkMailEntity)dto.newRaRpLnkEntity();
				String rpId = raRpLnkEntity.getRpId();
				inner.setRpId(rpId);
				inner.setRelTimestamp(raRpLnkEntity.getRelTimestamp());
				inner.setFormatDate( TriDateUtils.convertViewDateFormat( raRpLnkEntity.getRelTimestamp(), TriDateUtils.getYMDDateFormat() ) );
				inner.setFormatTime( TriDateUtils.convertViewDateFormat( raRpLnkEntity.getRelTimestamp(), TriDateUtils.getHMSDateFormat() ) );
				IRpEntity relEntity = relMap.get(rpId);
				inner.setStatusId(relEntity.getProcStsId());
				inner.setStatus(sheet.getValue(RmDesignBeanId.statusId, relEntity.getProcStsId()));

				raRpLnkEntities.add( inner );
			}
			dto.setRaRpLnkEntities( raRpLnkEntities );
		}
	}
}
