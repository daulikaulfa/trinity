package jp.co.blueship.tri.rm.beans.mail.dto;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

public class RelApplyMailServiceBean extends MailServiceBean {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String comment;
	private ILotEntity pjtLotEntity;
	private RaMailDto relApplyEntity;
	private List<RaMailDto> relApplyEntityList = new ArrayList<RaMailDto>();
	private List<IAreqEntity> assetApplyEntityList;
	private Map<String, IRpEntity> relMap = new TreeMap<String, IRpEntity>();


	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}
	public ILotEntity getLotEntity() {
		return pjtLotEntity;
	}
	public void setLotEntity(ILotEntity pjtLotEntity) {
		this.pjtLotEntity = pjtLotEntity;
	}
	public RaMailDto getRelApplyEntity() {
		return relApplyEntity;
	}
	public void setRelApplyEntity(RaMailDto relApplyEntity) {
		this.relApplyEntity = relApplyEntity;
	}
	public List<RaMailDto> getRelApplyEntityList() {
		return relApplyEntityList;
	}
	public void setRelApplyEntityList(List<RaMailDto> relApplyEntityList) {
		this.relApplyEntityList = relApplyEntityList;
	}
	public List<IAreqEntity> getAssetApplyEntityList() {
		return assetApplyEntityList;
	}
	public void setAssetApplyEntityList(List<IAreqEntity> assetApplyEntityList) {
		this.assetApplyEntityList = assetApplyEntityList;
	}
	public Map<String, IRpEntity> getRelMap() {
		return relMap;
	}
	public void setRelMap(Map<String, IRpEntity> relMap) {
		this.relMap = relMap;
	}
}
