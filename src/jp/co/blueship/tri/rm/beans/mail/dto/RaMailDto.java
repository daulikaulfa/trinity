package jp.co.blueship.tri.rm.beans.mail.dto;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkEntity;

public class RaMailDto extends RaDto implements IEntity {

	private static final long serialVersionUID = 1L;

	//RP番号単位のMap情報
	private Collection<Map<String,Object>> packageList = new ArrayList<Map<String,Object>>();

	public Collection<Map<String, Object>> getPackageList() {
		return packageList;
	}

	public void setPackageList(Collection<Map<String, Object>> packageList) {
		this.packageList = packageList;
	}

	public IRaRpLnkEntity newRaRpLnkEntity() {
		return new RaRpLnkMailEntity();
	}

	public class RaRpLnkMailEntity extends RaRpLnkEntity {
		private static final long serialVersionUID = 1L;

		private String formatDate;
		private String formatTime;
		//* リリースステータスID *
		private String statusId;
		//* リリースステータス *
		private String status;


		public String getFormatDate() {
			return formatDate;
		}
		public void setFormatDate(String formatDate) {
			this.formatDate = formatDate;
		}
		public String getFormatTime() {
			return formatTime;
		}
		public void setFormatTime(String formatTime) {
			this.formatTime = formatTime;
		}
		public String getStatusId() {
			return statusId;
		}
		public void setStatusId(String statusId) {
			this.statusId = statusId;
		}
		public String getStatus() {
			return status;
		}
		public void setStatus(String status) {
			this.status = status;
		}
	}

}
