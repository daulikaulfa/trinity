package jp.co.blueship.tri.rm.beans.mail.dto;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

public class RelCtlMailServiceBean extends MailServiceBean {

	/**
	 * 修正後インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private IRpEntity relEntity;
	private ILotEntity pjtLotEntity;
	private IBldEnvEntity relEnvEntity;
	private List<IAreqEntity> assetApplyEntityList;
	private List<Map<String,Object>> releaseList;


	public IBldEnvEntity getRelEnvEntity() {
		return relEnvEntity;
	}
	public void setRelEnvEntity( IBldEnvEntity relEnvEntity ) {
		this.relEnvEntity = relEnvEntity;
	}
	public ILotEntity getPjtLotEntity() {
		return pjtLotEntity;
	}
	public void setPjtLotEntity(ILotEntity pjtLotEntity) {
		this.pjtLotEntity = pjtLotEntity;
	}
	public List<IAreqEntity> getAssetApplyEntityList() {
		return assetApplyEntityList;
	}
	public void setAssetApplyEntityList( List<IAreqEntity> assetApplyEntityList ) {
		this.assetApplyEntityList = assetApplyEntityList;
	}
	public List<Map<String,Object>> getReleaseList() {
		return releaseList;
	}
	public void setReleaseList(List<Map<String,Object>> releaseList) {
		this.releaseList = releaseList;
	}
	public IRpEntity getRelEntity() {
		return relEntity;
	}
	public void setRelEntity(IRpEntity relEntity) {
		this.relEntity = relEntity;
	}
}
