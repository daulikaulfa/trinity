package jp.co.blueship.tri.rm.beans.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;
import jp.co.blueship.tri.rm.beans.mail.dto.RelCtlMailServiceBean;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;


/**
 * リリース管理・リリース作成時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionSendRelCtlEntryMail implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelCtlEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute( IServiceDto<FlowRelCtlEntryServiceBean> serviceDto ) {

		FlowRelCtlEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
			//基礎情報のコピー
			RelCtlMailServiceBean successMailBean = new RelCtlMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, paramBean );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

			// 環境情報
			String selectedEnvNo = paramBean.getSelectedEnvNo();
			IBldEnvEntity envEntity = this.support.getBmFinderSupport().findBldEnvEntity( selectedEnvNo );


			// ロット情報
			String selectedLotNo = paramBean.getSelectedLotNo();
			ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity(selectedLotNo);


			//リリース番号
			String rpId = paramBean.getRelNo();
			IRpEntity relEntity = null ;
			try {
				relEntity = this.support.findRpEntity( rpId );
			} catch ( TriSystemException e ) {
				//ロック待ちなどでリリースレコード生成までたどり着かなかった場合、メールを送信しない
				LogHandler.info( log , TriLogMessage.LRM0002 , rpId ) ;
				return serviceDto ;
			}

			// ビルドパッケージ情報
			List<Map<String,Object>> releaseList = new ArrayList<Map<String,Object>>();
			Map<String,Object> releaseMap = new HashMap<String,Object>();
			List<String> bpIdList = new ArrayList<String>();

			for ( UnitBean bean : paramBean.getSelectedUnitBean() ){
				bpIdList.add( bean.getBuildNo() );
			}
			releaseMap.put	( "rpId", rpId );
			releaseMap.put	( "bpIdList", bpIdList );

			releaseList.add( releaseMap );


			successMailBean.setRelEntity	( relEntity );
			successMailBean.setRelEnvEntity	( envEntity );
			successMailBean.setPjtLotEntity	( lotEntity );
			successMailBean.setAssetApplyEntityList( getAssetApplyEntity( bpIdList ) );

			successMailBean.setReleaseList(releaseList);

			successMail.execute( mailServiceDto );

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( RmMessageId.RM005031S, e , paramBean.getFlowAction()) ) ;
		}

		return serviceDto;
	}


	/**
	 * ビルドパッケージに含まれる承認済申請情報を取得する
	 * @param bpIdList
	 * @return
	 */
	private List<IAreqEntity> getAssetApplyEntity( List<String> bpIdList ) {

		List<IBpDto> buildEntities = this.support.getBmFinderSupport().findBpDto( bpIdList.toArray( new String[0] ) );
		IAreqEntity[] assetApplyEntities = this.support.getAssetApplyEntity( buildEntities );


		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

			if ( !AmAreqStatusId.BuildPackageClosed.equals( assetApplyEntity.getStsId() ) ) {
				assetApplyEntityList.add( assetApplyEntity );
			}
		}

		return assetApplyEntityList;
	}

}
