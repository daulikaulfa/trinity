package jp.co.blueship.tri.rm.beans.lock;

import java.sql.Timestamp;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.ExecDataStsEntity;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.eb.IExecDataStsEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.RmFinderSupport;

/**
 * リリースパッケージ作成に関連する業務排他を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionLockRelByRelEntry implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private RmFinderSupport support = null;
	public void setSupport( RmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute(IServiceDto<FlowRelCtlEntryServiceBean> serviceDto) {

		FlowRelCtlEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();
			String raId = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();

			IRpEntity relEntity = this.insertRpEntity( paramBean );
			paramBean.setRelNo( relEntity.getRpId() );

			insertRpBpLnk( relEntity.getRpId(), paramBean);

			if( TriStringUtils.isNotEmpty(raId) ) {
				this.insertRaRpLnk( paramBean, relEntity );
			}
			
			this.cleaningProcessRecord( paramBean );
			this.registerProcessRecord( paramBean );

			BaseInfoInputBean baseInfoInputBean = paramBean.getBaseInfoBean().getBaseInfoInputBean();
			this.support.getUmFinderSupport().registerCtgLnk(baseInfoInputBean.getCtgId(), RmTables.RM_RP, relEntity.getRpId());
			this.support.getUmFinderSupport().registerMstoneLnk(baseInfoInputBean.getMstoneId(), RmTables.RM_RP, relEntity.getRpId());
			
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005028S, e);
		}
	}

	/**
	 * リリース情報を登録する。
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 * @param systemDate システム日時
	 * @return 登録したリリース情報
	 */
	private final IRpEntity insertRpEntity(
			FlowRelCtlEntryServiceBean paramBean ) {

		IRpEntity relEntity = new RpEntity();
		String relNo = paramBean.getRelNo();

		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		relEntity.setBldEnvId(			 paramBean.getSelectedEnvNo() );
		relEntity.setLotId				( paramBean.getSelectedLotNo() );
		relEntity.setRpId				( relNo );
		relEntity.setSummary			(
				paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelSummary() );
		relEntity.setContent			(
				paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelContent() );
		relEntity.setRelTimestamp		( systemTimestamp );
		relEntity.setExecUserNm			( paramBean.getUserName() );
		relEntity.setExecUserId			( paramBean.getUserId() );
		relEntity.setProcId				( paramBean.getProcId() );
		relEntity.setProcStTimestamp	( systemTimestamp );
		relEntity.setStsId				( RmRpStatusId.Unprocessed.getStatusId() );
		relEntity.setDelStsId			( StatusFlg.off );
		relEntity.setUpdUserNm			( paramBean.getUserName() );
		relEntity.setUpdUserId			( paramBean.getUserId() );

		this.support.getRpDao().insert( relEntity );
		return relEntity;
	}

	/**
	 * RpBpリンクテーブルを登録する。
	 * @param rpId リリースパッケージID
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 */
	private void insertRpBpLnk( String rpId , FlowRelCtlEntryServiceBean paramBean ){

		IRpBpLnkEntity entity = new RpBpLnkEntity();
		UnitBean[] beans = paramBean.getSelectedUnitBean();
		for (UnitBean bean : beans) {
			entity.setRpId( rpId );
			entity.setBpId( bean.getBuildNo() );
			entity.setMergeOdr( bean.getMergeOrder() );
		}
		this.support.getRpBpLnkDao().insert(entity);

	}

	/**
	 * RaRpリンクテーブルの登録をする。
	 * <br>RP作成エラー等の場合に再度リリース申請を選択してRP作成を行った場合、
	 * 前回RP作成時のLink情報を物理削除し、新規に登録を行う。
	 *
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 * @param rpEntity リリース情報
	 */
	private final void insertRaRpLnk(
			FlowRelCtlEntryServiceBean paramBean, IRpEntity rpEntity ) {

		String raId = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();

		RaRpLnkCondition raRpCondition = new RaRpLnkCondition();
		raRpCondition.setRaId( raId );
		this.support.getRaRpLnkDao().delete( raRpCondition.getCondition() );

		IRaRpLnkEntity raRpEntity = new RaRpLnkEntity();

		raRpEntity.setRpId( rpEntity.getRpId() );
		raRpEntity.setRaId( raId );

		this.support.getRaRpLnkDao().insert( raRpEntity );

	}

	private void cleaningProcessRecord( FlowRelCtlEntryServiceBean paramBean ) {
		IExecDataStsEntity entity = new ExecDataStsEntity();
		entity.setDelStsId( StatusFlg.on );

		support.getSmFinderSupport().cleaningExecDataSts( RmTables.RM_RP, paramBean.getRelNo() );

		String raId = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();

		if ( StringUtils.isNotEmpty( raId ) ) {
			support.getSmFinderSupport().cleaningExecDataSts( RmTables.RM_RA, raId );
		}

	}

	private void registerProcessRecord(
			FlowRelCtlEntryServiceBean paramBean ) {
		String procId = paramBean.getProcId();
		String rpId = paramBean.getRelNo();

		support.getSmFinderSupport().registerExecDataSts(
				procId, RmTables.RM_RP, RmRpStatusIdForExecData.CreatingReleasePackage, rpId);

		String raId = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();

		if ( StringUtils.isNotEmpty( raId ) ) {
			support.getSmFinderSupport().registerExecDataSts(
					procId, RmTables.RM_RA, RmRaStatusIdForExecData.CreatingReleasePackage, raId);
		}
	}

}
