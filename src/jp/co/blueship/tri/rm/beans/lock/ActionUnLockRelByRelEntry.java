package jp.co.blueship.tri.rm.beans.lock;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * リリースパッケージ作成に関連する業務排他の解除を行います。 <br>
 * 予期しないエラーとなった場合、「作成中」になってしまうステータスを処理します。
 *
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 *
 */
public class ActionUnLockRelByRelEntry implements IDomain<FlowRelCtlEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlEntryServiceBean> execute(IServiceDto<FlowRelCtlEntryServiceBean> serviceDto) {

		FlowRelCtlEntryServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			IRpEntity relEntity = null;
			boolean relSuccess = false;

			String relNo = paramBean.getRelNo();
			relEntity = this.support.findRpEntity(relNo);

			relSuccess = isReleaseComplete(relNo,paramBean.getProcId());

			updateRpEntity(paramBean, relEntity, relSuccess);

			if (StringUtils.isNotEmpty(paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo())) {
				this.updateRaEntity(paramBean, relSuccess);
			}

			if (relSuccess) {
				support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
				paramBean.setProcMgtStatusId(SmProcMgtStatusId.Success);
			} else {
				this.updateProcessRecordForError(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005028S, e);
		}
	}

	/**
	 * リリース登録が成功したかどうかをチェックする。
	 *
	 * @param relNo リリース番号
	 * @param procId プロセスID
	 * @return 成功していればtrue、そうでなければfalse
	 */
	private boolean isReleaseComplete(String relNo, String procId) {

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(procId);
		ITaskFlowProcEntity[] entities = this.support.getBmFinderSupport().getTaskFlowProcDao().find(condition.getCondition()).toArray(new ITaskFlowProcEntity[0]);
		;

		if (0 == entities.length) {
			// プロセスレコードが一件もない場合、なんらかのエラーが発生したとみなす
			LogHandler.fatal(log, ContextAdapterFactory.getContextAdapter().getMessage(RmMessageId.RM004009F, relNo), new TriSystemException(RmMessageId.RM004009F, relNo));
			return false;
		}

		for (ITaskFlowProcEntity entity : entities) {
			if (RmRpStatusIdForExecData.ReleasePackageError.equals(entity.getStsId()))
				return false;
		}

		return true;
	}

	/**
	 * Taskの結果を受けて、リリース情報を更新する。
	 *
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 * @param entity リリースエンティティ
	 * @param systemDate システム日時
	 * @param relSuccess リリース登録が成功したかどうか
	 */
	private void updateRpEntity(FlowRelCtlEntryServiceBean paramBean, IRpEntity entity, boolean relSuccess) {

		if (null == entity)
			return;

		if (relSuccess) {
			entity.setStsId(RmRpStatusId.ReleasePackageCreated.getStatusId());
		}
		entity.setProcEndTimestamp(TriDateUtils.getSystemTimestamp());
		entity.setUpdUserNm(paramBean.getUserName());
		entity.setUpdUserId(paramBean.getUserId());

		this.support.getRpDao().update(entity);

	}

	/**
	 * リリース申請情報を更新する。
	 *
	 * @param paramBean FlowRelEntryServiceBeanオブジェクト
	 * @param relSuccess リリース登録が成功したかどうか
	 */
	private final void updateRaEntity(FlowRelCtlEntryServiceBean paramBean, boolean relSuccess) {

		RaCondition condition = new RaCondition();
		condition.setRaId(paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo());
		IRaEntity relApplyEntity = this.support.getRaDao().findByPrimaryKey(condition.getCondition());

		if (relSuccess) {
			relApplyEntity.setStsId(RmRaStatusId.ReleasePackageCreated.getStatusId());
		}

		this.support.getRaDao().update(relApplyEntity);

		if (DesignSheetUtils.isRecord()) {
			// 履歴用DTO作成
			IRaDto raDto = support.findRaDto(relApplyEntity.getRaId());

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRaHistDao().insert( histEntity , raDto);
		}
	}

	private void updateProcessRecordForError(FlowRelCtlEntryServiceBean paramBean) {
		String procId = paramBean.getProcId();
		String rpId = paramBean.getRelNo();

		support.getSmFinderSupport().updateExecDataSts(
				procId, RmTables.RM_RP, RmRpStatusIdForExecData.ReleasePackageError, rpId);

		String raId = paramBean.getBaseInfoBean().getBaseInfoInputBean().getRelApplyNo();

		if (StringUtils.isNotEmpty(raId)) {
			support.getSmFinderSupport().updateExecDataSts(
					procId, RmTables.RM_RA, RmRaStatusIdForExecData.ReleasePackageError, raId);
		}
	}

}
