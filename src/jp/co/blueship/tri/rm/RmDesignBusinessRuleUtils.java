package jp.co.blueship.tri.rm;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDealAssetFtp;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 * DesignBusinessRuleUtilsの派生クラスです。依存性を解消するために作成しました。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class RmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ページ制御情報からページ番号情報に設定します。
	 * @param dest ページ番号情報
	 * @param src ページ制御
	 * @return ページ番号情報を戻します。
	 */
	public static final IPageNoInfo convertPageNoInfo( IPageNoInfo dest, ILimit src ) {

		if ( null == dest ){
			throw new TriSystemException( RmMessageId.RM005015S );
		}
		if ( null == src ) {
			dest.setMaxPageNo( new Integer(0) );
			dest.setSelectPageNo( new Integer(0) );
			return dest;
		}

		dest.setMaxPageNo( new Integer(src.getPageBar().getMaximum()) );
		dest.setSelectPageNo( new Integer(src.getPageBar().getValue()) );

		dest.setViewRows(src.getViewRows());
		dest.setMaxRows(src.getMaxRows());

		return dest;
	}

	/**
	 * ワークスペースフォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getWorkspacePath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( RmMessageId.RM005017S );
		}
		if ( null == entity.getWsPath() ){
			throw new TriSystemException( RmMessageId.RM004006F );
		}
		file = new File( entity.getWsPath() );

		return file;
	}

	/**
	 * 履歴フォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getHistoryPath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( RmMessageId.RM005017S );
		}
		if ( null == entity.getHistPath() ){
			throw new TriSystemException( RmMessageId.RM004007F , entity.getHistPath());
		}
		file = new File( entity.getHistPath() );

		if ( ! file.isDirectory() ) {
			throw new TriSystemException( RmMessageId.RM004008F , file.getPath());
		}

		return file;
	}

	/**
	 * 内部(SystemInBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxPath( ILotEntity lotEntity ) throws TriSystemException  {

		if ( null == lotEntity ) {
			throw new TriSystemException( RmMessageId.RM005016S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getSysInBox() ) ) {
			throw new TriSystemException( RmMessageId.RM005018S );
		}

		File file = new File( 	lotEntity.getSysInBox()  );

		return file;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxAssetBasePath( ILotEntity lotEntity ) throws TriSystemException  {

		File systemInBox = getSystemInBoxPath( lotEntity );
		return systemInBox;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxAssetBasePath( List<Object> list ) throws TriSystemException  {

		ILotEntity lotEntity = RmExtractEntityAddonUtils.extractPjtLot(list);
		return getSystemInBoxAssetBasePath( lotEntity );
	}


	/**
	 * ビルドパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param buildEntity ビルドパッケージエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelUnitDSLPath( ILotEntity lotEntity, IBpEntity buildEntity ) throws TriSystemException {

		File file = null;
		File historyFile = getHistoryPath( lotEntity );

		if ( null == buildEntity ){
			throw new TriSystemException( RmMessageId.RM005019S );
		}
		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relUnitDSLRelationPath ),
				buildEntity.getBpId() )
				);

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLPath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {

		File file = null;
		File historyFile = getHistoryPath( lotEntity );

		if ( null == relEntity ){
			throw new TriSystemException( RmMessageId.RM005020S );
		}
		String relativePath = TriStringUtils.linkPath( relEntity.getBldEnvId(), relEntity.getRpId() );

		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relDSLRelationPath ),
				relativePath )
				);

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の配付資源ファイル名を取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLFilePath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {

		File file = null;
		File historyFile = getHistoryRelDSLPath( lotEntity, relEntity );

		String historyFileName = TriStringUtils.linkPath(historyFile.getPath(), relEntity.getBldEnvId());
		file = new File( historyFileName +  sheet.getValue( DmDesignEntryKeyByDealAssetFtp.srcExtension ) );

		return file;
	}

	/**
	 * サービス単位のログの管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param flowAction 実行サービス名ID
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspaceLogPath( ILotEntity lotEntity, String flowActionId ) throws TriSystemException {

		File file = null;
		File workspaceFile = getWorkspacePath( lotEntity );

		if ( null == flowActionId ){
			throw new TriSystemException( RmMessageId.RM005021S );
		}
		file = new File( workspaceFile, TriStringUtils.linkPath(
												sheet.getValue( UmDesignEntryKeyByCommon.logRelationPath ),
												flowActionId )
										 );

		return file;
	}

	/**
	 * 返却資産承認の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static final File getReturnAssetPath( List<Object> list )
			throws TriSystemException {

		return new File( getSystemInBoxAssetBasePath( list ).getPath());
	}

	/**
	 * 返却資産承認の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( RmMessageId.RM005022S );
		}

		File file = new File( 	getReturnAssetPath( list ).getPath(),
								getAssetApplyPathName( entity ));
		return file;
	}

	/**
	 * 申請の格納フォルダ名を取得します。
	 *
	 * @param entity 貸出返却情報申請
	 * @return 取得した格納フォルダ名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static final String getAssetApplyPathName( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( RmMessageId.RM005023S );
		}

		return entity.getAreqId();
	}

	/**
	 * レポートのファイル名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得したファイル名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReportFile( List<Object> list )
			throws TriSystemException {

		IRepEntity entity = RmExtractEntityAddonUtils.extractReport( list );
		if ( null == entity ){
			throw new TriSystemException( RmMessageId.RM005023S );
		}
		File path = new File( sheet.getValue(DcmDesignEntryKeyByReport.reportFilePath) );
		if ( ! path.isDirectory() )
			path.mkdirs();

		String extension = sheet.getValue( DcmDesignEntryKeyByReport.reportFileExtension ) ;
		extension = ( '.' != extension.charAt( 0 ) ) ? "." + extension : extension ;//.が拡張子文字列の先頭になければ付加

		return new File(path, entity.getRepId() + extension );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlEntity モジュールエンティティー
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	public static final String convertPathInModule( String relativePath, ILotMdlLnkEntity mdlEntity ) {
		return convertPathInModule( relativePath, mdlEntity.getMdlNm() );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlName モジュール名
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	private static final String convertPathInModule( String relativePath, String mdlName ) {
		String tempPath = TriStringUtils.convertPath( relativePath );

		if ( tempPath.startsWith("/") )
			tempPath = StringUtils.substringAfter(tempPath, "/");

		if( StringUtils.substringBefore( tempPath, "/" ).equals( mdlName) ) {
			return tempPath.substring( tempPath.indexOf( "/" ) + 1 ) ;//「モジュール名」部分および先頭の"/"を取り除く
		}

		return null;
	}

	/**
	 * テンプレートフォルダを取得します。
	 *
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getTemplatePath() throws TriSystemException {

		return new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
							sheet.getValue( UmDesignEntryKeyByCommon.templatesRelativePath ));
	}
	
	public static File getAttachmentFilePath(String raId, String fileName, String fileSeqNo) {
		String basePath = DesignSheetFactory.getDesignSheet().getValue(RmDesignEntryKeyByRelApply.relApplyAppendFilePath);
		
		String applyNo = raId;
		String applyNoPath = TriStringUtils.linkPathBySlash( basePath , applyNo ) ;

		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( applyNoPath , uniqueKey ) ;

		String append1 = fileSeqNo ;
		String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , append1 ) ;
		
		return new File(appendPath1 , fileName);
	}
}
