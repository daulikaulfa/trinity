package jp.co.blueship.tri.rm.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelApplyInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelApplyInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;


/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelApplyTicketEditSupport extends RmFinderSupport {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース申請票の対象となるリリース申請情報を取得します。
	 *
	 * @param buildNo ビルドパッケージ番号の配列
	 * @return
	 */
	public IRaEntity[] getRelApplyEntityByRelApplyAssetRegister( String[] relApplyNo ) {

		if ( TriStringUtils.isEmpty( relApplyNo )) {
			return new IRaEntity[0];
		}

		RaCondition condition = new RaCondition();
		condition.setRaIds( relApplyNo );
		ISqlSort sort = this.getRelApplySortByRelApplyAssetRegister();
		List<IRaEntity> raEntities = this.getRaDao().find( condition.getCondition(), sort );

		return raEntities.toArray(new IRaEntity[0]);
	}

	/**
	 * リリース申請票の対象となるビルドパッケージを取得します。
	 *
	 * @param relApplyEntity リリース申請
	 * @return
	 */
	public List<IBpEntity> getBuildEntityByBuildAssetRegister( IRaDto relApplyEntity ) {

		if ( TriStringUtils.isEmpty( relApplyEntity )) {
			return new ArrayList<IBpEntity>();
		}

		Set<String> bpIdSet = new HashSet<String>();
		for ( IRaBpLnkEntity raBpLnkEntity : relApplyEntity.getRaBpLnkEntities() ) {
			bpIdSet.add( raBpLnkEntity.getBpId() );
		}

		if ( 0 == bpIdSet.size() )
			return new ArrayList<IBpEntity>();

		ISqlSort sort = this.getBuildSortByRelApplyAssetRegister();
		List<IBpEntity> entities = this.getBmFinderSupport().findBpEntities(sort, (String[])bpIdSet.toArray( new String[0] ));

		return entities;
	}

	/**
	 *  リリース申請票のリリース申請情報ソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getRelApplySortByRelApplyAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap(DcmDesignBeanId.relApplyAssetRegisterRelApplyOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.relApplyAssetRegisterRelApplyOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getRelApplySortByRelApplyAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  リリース申請票のビルドパッケージソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getBuildSortByRelApplyAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.relApplyAssetRegisterRelUnitOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.relApplyAssetRegisterRelUnitOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getBuildSortByRelApplyAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 * リリース申請情報エンティティをレポート用に変換する
	 * @param dtoList
	 * @return
	 */
	public List<IReportRelApplyInfo> convertReportRelApplyEntity( List<IRaDto> dtoList ) {

		List<IReportRelApplyInfo> reportList = new ArrayList<IReportRelApplyInfo>();

		for ( IRaDto dto : dtoList ) {

			IReportRelApplyInfo reportEntity = new ReportRelApplyInfo();

			TriPropertyUtils.copyProperties( reportEntity, dto.getRaEntity() );

			// リリース責任者
			List<String> responsibleUserReportForm = new ArrayList<String>();
			responsibleUserReportForm.add( dto.getRaEntity().getReqResponsibility() );
			reportEntity.setResponsibleUserReportForm( responsibleUserReportForm );

			// リリース予定日を標準の日付フォーマットに変換する
			reportEntity.setRelWishDateReportForm(
					TriDateUtils.convertDateFormat( reportEntity.getPreferredRelDate(), null ) );

			// 添付ファイル
			List<String> appendFileReportForm = new ArrayList<String>();
			for ( IRaAttachedFileEntity appendFileEntity : dto.getRaAttachedFileEntities() ) {
				appendFileReportForm.add( appendFileEntity.getFilePath() );
			}
			reportEntity.setAppendFileReportForm( appendFileReportForm );

			// 申請番号
			List<String> applyNoForm = new ArrayList<String>();
			// 変更管理番号
			Set<String> pjtNoReportForm = new TreeSet<String>();
			// 変更要因番号
			Set<String> changeCauseNoReportForm = new TreeSet<String>();
			for ( IAreqEntity areqEntity : findAreqEntitiesFromRaBpLnk( dto.getRaBpLnkEntities() ) ) {
				applyNoForm.add( areqEntity.getAreqId() );
				pjtNoReportForm.add( areqEntity.getPjtId() );
				changeCauseNoReportForm.add( areqEntity.getChgFactorNo()  );
			}
			reportEntity.setApplyNoForm( applyNoForm );
			reportEntity.setPjtNoReportForm( new ArrayList<String>( pjtNoReportForm ) );
			reportEntity.setChangeCauseNoReportForm( new ArrayList<String>( changeCauseNoReportForm ) );

			reportList.add( reportEntity );
		}

		return reportList;
	}

	/**
	 * リリース申請IDから資産申請エンティティリストを取得します。
	 * @param raId リリース申請ID
	 * @return AreqEntity 資産申請のリスト
	 */
	public List<IAreqEntity> getAreqEntityFromRaId( String raId ) {

		List<IAreqEntity> areqEntities = new ArrayList<IAreqEntity>();
		List<IRaBpLnkEntity> lnkEntities = getRaBpLnkEntitiesFromRaId(raId);
		for ( IRaBpLnkEntity entity : lnkEntities ){
			IBpDto bpDto = this.getBmFinderSupport().findBpDto( entity.getBpId(), BmTables.BM_BP_AREQ_LNK );

			for ( IBpAreqLnkEntity bpAreqEntity : bpDto.getBpAreqLnkEntities() ) {
				areqEntities.add( this.getAmFinderSupport().findAreqEntity( bpAreqEntity.getAreqId() ) );
			}
		}
		return areqEntities;
	}

	private List<IRaBpLnkEntity> getRaBpLnkEntitiesFromRaId( String raId ) {

		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setRaId(raId);
		List<IRaBpLnkEntity> lnkEntities = this.getRaBpLnkDao().find(condition.getCondition());

		return lnkEntities;
	}

}
