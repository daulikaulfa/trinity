package jp.co.blueship.tri.rm.support;

import jp.co.blueship.tri.dm.dao.dmdo.IDmDoDao;
import jp.co.blueship.tri.fw.um.dao.hist.IDmDoHistDao;
import jp.co.blueship.tri.rm.dao.cal.ICalDao;
import jp.co.blueship.tri.rm.dao.ra.IRaDao;
import jp.co.blueship.tri.rm.dao.rp.IRpBpLnkDao;
import jp.co.blueship.tri.rm.dao.rp.IRpDao;


/**
 * RM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IRmDaoFinder {

	/**
	 * rpDaoを取得します。
	 * @return rpDao
	 */
	public IRaDao getRaDao();

	/**
	 * rpDaoを取得します。
	 * @return rpDao
	 */
	public IRpDao getRpDao();

	/**
	 * rpBpLnkDaoを取得します。
	 * @return rpBpLnkDao
	 */
	public IRpBpLnkDao getRpBpLnkDao();

	public IDmDoDao getDmDoDao();

	/**
	 * デプロイメント連携履歴DTOを取得します。
	 * @return dmDoHist
	 */
	public IDmDoHistDao getDmDoHistDao ();

	public ICalDao getCalDao();
}
