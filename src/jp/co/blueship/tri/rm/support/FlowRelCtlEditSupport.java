package jp.co.blueship.tri.rm.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvSrvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineAgentCondition;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineCondition;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowCondition;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriNumberUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.beans.dto.RelCtlSearchBean;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;

/**
 * FlowRelEntry等のリリース更新系のイベントのサポートClass <br>
 * <p>
 * リリースのための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * @version V3L10.02
 * @author Takashi Ono
 */
public class FlowRelCtlEditSupport extends RmFlowRelCmnEditSupport {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース可能なビルドパッケージを持つロットのロット番号取得
	 *
	 * @return ロット番号
	 */
	public String[] getLotNoByReleasableRelUnit() {
		return (String[]) this.getLotNoSetByReleasableRelUnit().toArray(new String[0]);
	}

	/**
	 * リリース可能なビルドパッケージを持つロットのロット番号取得
	 *
	 * @return ロット番号
	 */
	public Set<String> getLotNoSetByReleasableRelUnit() {

		// リリース登録可能なビルドパッケージ
		IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit();

		List<IBpEntity> bpEntities = getBmFinderSupport().getBpDao().find(condition.getCondition());

		// ロット番号を抽出
		HashSet<String> lotNoSet = new HashSet<String>();
		for (IBpEntity entity : bpEntities) {
			lotNoSet.add(entity.getLotId());
		}

		return lotNoSet;
	}

	/**
	 * リリース可能なビルドパッケージを持つロットのロット番号取得
	 *
	 * @param lotNoArrays アクセス可能なロット番号の配列
	 * @return ロット番号
	 */
	public Set<String> getLotNoSetByReleasableRelUnit(String[] lotNoArrays) {

		// リリース登録可能なビルドパッケージ
		IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit();

		// アクセス可能なロット番号
		{
			if (TriStringUtils.isEmpty(lotNoArrays)) {
				// ダミーを設定して、全件検索を防止する。
				((BpCondition) condition).setLotIds(new String[] { "" });
			} else {

				((BpCondition) condition).setLotIds(lotNoArrays);
			}
		}

		List<IBpEntity> bpEntities = getBmFinderSupport().getBpDao().find(condition.getCondition());

		// ロット番号を抽出
		HashSet<String> lotNoSet = new HashSet<String>();
		for (IBpEntity entity : bpEntities) {
			lotNoSet.add(entity.getLotId());
		}

		return lotNoSet;
	}

	/**
	 * ビルドパッケージエンティティからビルドパッケージ画面用ビーンを作成します。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @param mergeOrderCombo マージ順のコンボ
	 * @param unSelectedChar 未選択を示す文字
	 * @return ビルドパッケージ画面用ビーンのリスト
	 */
	public List<UnitViewBean> getUnitViewBean(IBpEntity[] entities, List<String> mergeOrderCombo, String unSelectedChar) {

		List<IBpEntity> buildList = new ArrayList<IBpEntity>();
		for (IBpEntity entity : entities) {
			buildList.add(entity);
		}

		Map<String, String> mergeOrderMap = new HashMap<String, String>();// ビルド番号とマージ順のマップ

		for (int mergeNo = 0; mergeNo < buildList.size(); mergeNo++) {

			String mergeNoStr = String.valueOf(mergeNo);

			mergeOrderCombo.add(mergeNoStr);
			mergeOrderMap.put(buildList.get(mergeNo).getBpId(), mergeNoStr);
		}
		mergeOrderCombo.add(unSelectedChar); // 未選択を示す記号

		List<UnitViewBean> unitViewBeanList = new ArrayList<UnitViewBean>();

		for (IBpEntity entity : entities) {
			UnitViewBean viewBean = new UnitViewBean();

			setUnitViewBeanBuildEntity(viewBean, entity, mergeOrderMap.get(entity.getBpId()));

			unitViewBeanList.add(viewBean);
		}

		RmEntityAddonUtils.sortUnitViewBean(unitViewBeanList);

		return unitViewBeanList;
	}
	/**
	 * ビルドパッケージ一覧画面の表示項目を、ビルドパッケージエンティティから取得して設定する。
	 * @param viewBean ビルドパッケージ一覧画面の表示項目
	 * @param entity ビルドパッケージエンティティ
	 */
	private void setUnitViewBeanBuildEntity(
			UnitViewBean viewBean, IBpEntity entity, String mergeNo ) {

		viewBean.setBuildNo		( entity.getBpId() );
		viewBean.setGenerateDate( TriDateUtils.convertViewDateFormat( entity.getProcEndTimestamp() ) );
		viewBean.setBuildStatus	(
				sheet.getValue( RmDesignBeanId.statusId, entity.getProcStsId() ));
		viewBean.setMergeOrder	( mergeNo );
	}
	/**
	 * ビルドパッケージエンティティからビルドパッケージ画面用ビーンを作成します。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @param mergeOrder ビルド番号とマージ順のマップ
	 * @return ビルドパッケージ画面用ビーンのリスト
	 */
	public List<UnitViewBean> getUnitViewBean(IBpEntity[] entities, Map<String, String> mergeOrderMap) {

		List<UnitViewBean> unitViewBeanList = new ArrayList<UnitViewBean>();

		for (IBpEntity entity : entities) {
			UnitViewBean viewBean = new UnitViewBean();

			setUnitViewBeanBuildEntity(viewBean, entity, mergeOrderMap.get(entity.getBpId()));

			unitViewBeanList.add(viewBean);
		}

		RmEntityAddonUtils.sortUnitViewBean(unitViewBeanList);

		return unitViewBeanList;
	}

	/**
	 * リリースパッケージ環境のリリースサーバ環境を取得する。
	 *
	 * @param envNo 環境番号
	 * @return リリースパッケージ環境のリリースサーバ環境情報
	 */
	public IBldEnvSrvEntity[] getBldEnvSrvEntity(String envNo) {

		BldEnvSrvCondition condition = new BldEnvSrvCondition();
		condition.setBldEnvId(envNo);

		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvSrvItems.bldSrvId, TriSortOrder.Asc, 1);

		List<IBldEnvSrvEntity> bldEnvSrvEntities = getBmFinderSupport().getBldEnvSrvDao().find(condition.getCondition(), sort);

		if (TriStringUtils.isEmpty(bldEnvSrvEntities)){
			throw new TriSystemException(RmMessageId.RM004000F , envNo);
		}
		return bldEnvSrvEntities.toArray(new IBldEnvSrvEntity[0]);
	}

	/**
	 * リリースパッケージ環境タスクのタイムライン情報を取得する。
	 *
	 * @param envNo 環境番号
	 * @return リリースパッケージ環境タスクのタイムライン情報
	 */
	public IBldTimelineEntity[] getBldTimelineEntity( String envNo ) {

		BldTimelineCondition condition = new BldTimelineCondition();
		condition.setBldEnvId(envNo);

		ISqlSort sort = DBSearchSortAddonUtil.getRelTimeLineSort();

		List<IBldTimelineEntity> bldTimelineEntities =
			this.getBmFinderSupport().getBldTimelineDao().find( condition.getCondition(), sort );
		for ( IBldTimelineEntity entity : bldTimelineEntities ) {
			BldTimelineAgentCondition agentCondition = new BldTimelineAgentCondition();
			agentCondition.setBldEnvId(entity.getBldEnvId());
			agentCondition.setBldLineNo(entity.getBldLineNo());
			List<IBldTimelineAgentEntity> agentEntities = this.getBmFinderSupport().getBldTimelineAgentDao().find(agentCondition.getCondition());
			if( agentEntities!=null && agentEntities.size()>0 ) {
				entity.setLine(agentEntities.toArray(new IBldTimelineAgentEntity[0]));
			}
		}
		return bldTimelineEntities.toArray(new IBldTimelineEntity[0]);
	}

	/**
	 * タスク情報をすべて取得する。
	 * @return タスク情報
	 */
	public ITaskFlowEntity[] getTaskFlowEntities() {

		TaskFlowCondition condition = new TaskFlowCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(TaskFlowItems.taskFlowId, TriSortOrder.Asc, 1);

		IEntityLimit<ITaskFlowEntity> entityLimit =
			this.getBmFinderSupport().getTaskFlowDao().find( condition.getCondition(), sort, 1, 0 );

		if( 0 == entityLimit.getEntities().size() ){
			throw new TriSystemException (BmMessageId.BM004002F);
		}
		return entityLimit.getEntities().toArray( new ITaskFlowEntity[0] );
	}

	/**
	 * リリース情報を取得します。
	 *
	 * <code>interval</code>に指定された間隔で、<code>times</code>に指定された回数リトライします。
	 * <code>interval</code>、<code>times</code>が<code>0</code>
	 * 未満で指定された場合、デフォルトとして 以下の値で実行されます。 <br>
	 * <br>
	 * <code>interval = 0</code> <br>
	 * <code>times = 0</code> <br>
	 * <br>
	 * すなわち、リトライはされず1回だけ実行されます。 <br>
	 * また、<code>times</code>は<strong>リトライ</strong>回数のため、 <br>
	 * <br>
	 * <code>times = 3</code> <br>
	 * <br>
	 * とした場合、処理は<strong>4</strong>回実行されることに注意してください。
	 *
	 * @param relNo リリース番号
	 * @param times リトライ回数
	 * @param interval リトライ間隔(ミリ秒)
	 * @return 取得したリリース情報エンティティを戻します。
	 */
	public final IRpEntity getRpEntity(String relNo, int times, long interval) {

		if (TriStringUtils.isEmpty(relNo)){
			throw new TriSystemException(RmMessageId.RM005000S);
		}
		RpCondition condition = new RpCondition();
		condition.setRpId(relNo);
		IRpEntity relEntity = this.getRpDao().findByPrimaryKey(condition.getCondition());

		if (null != relEntity){
			return relEntity;
		}

		if (times <= 0) {
			throw new TriSystemException(RmMessageId.RM004002F , relNo);
		}

		for (int i = 0; i < times; i++) {

			condition.setRpId(relNo);
			relEntity = this.getRpDao().findByPrimaryKey(condition.getCondition());
			if (null != relEntity) {
				break;
			}

			try {
				Thread.sleep(TriNumberUtils.defaultIfMinus(interval, 0));
			} catch (InterruptedException e) {
				throw new TriSystemException(RmMessageId.RM004011F , e , relNo);
			}
		}

		if (null == relEntity){
			throw new TriSystemException(RmMessageId.RM004002F , relNo);
		}
		return relEntity;
	}

	/**
	 * ビルドパッケージエンティティを取得する
	 *
	 * @param buildNo ビルドパッケージ番号
	 * @return ビルドパッケージエンティティ
	 */
	public BpDtoList getBuildEntity(String[] buildNo) {

		IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByBuildNo(buildNo);
		List<IBpEntity> entities = this.getBmFinderSupport().getBpDao().find(condition.getCondition());

		return this.getBmFinderSupport().findBpDtoList( entities );
	}

	/**
	 * 資産申請IDからビルドパッケージIDを検索して返します。
	 * @param areqId
	 * @return BdId
	 */
	public String findBpIdFromLinkDB( String areqId ){

		PreConditions.assertOf(areqId != null, "areqId is null. ");
		BpAreqLnkCondition condition = new BpAreqLnkCondition();
		condition.setAreqId( areqId );
		List<IBpAreqLnkEntity> lnkEntity = this.getBmFinderSupport().getBpAreqLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(lnkEntity)){
			throw new TriSystemException (BmMessageId.BM005092S );
		}
		return lnkEntity.get(0).getBpId();

	}
	/**
	 * リリース申請エンティティを取得する
	 *
	 * @return リリース申請エンティティリミット
	 */
	public IEntityLimit<IRaEntity> getRelApplyEntity(IJdbcCondition condition, ISqlSort sort, int selectPageNo, int maxPageNumber) {

		IEntityLimit<IRaEntity> limit = this.getRaDao().find(condition.getCondition(), sort, selectPageNo, maxPageNumber);

		if (TriStringUtils.isEmpty(limit.getEntities())) {
			throw new TriSystemException(RmMessageId.RM004003F);
		}

		return limit;
	}

	/**
	 * リリース番号からリリース申請エンティティを取得する リリース申請せずにリリース取消するなど、
	 * リリースが関連していない状態のリリース申請に対しても検索対象とするため 検索結果が0件のときにエラーとはしない
	 *
	 * @param relNo リリース番号
	 * @return リリース申請エンティティリミット
	 */
	public IEntityLimit<IRaEntity> getRelApplyEntityByRelNo(String[] relNo) {

		IJdbcCondition condition = DBSearchConditionAddonUtil.getRelApplyConditionByRelNo(relNo);
		IEntityLimit<IRaEntity> limit = this.getRaDao().find(condition.getCondition(), null, 1, 0);

		return limit;
	}

	/**
	 * 変更管理番号から変更管理エンティティを取得する
	 *
	 * @param pjtNo 変更管理番号
	 * @return 変更管理エンティティ
	 */
	public IPjtEntity[] getPjtEntity(String[] pjtNo) {

		String[] conditionPjtNo = pjtNo;

		if (TriStringUtils.isEmpty(conditionPjtNo)) {
			// ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[] { "" };
		}

		IJdbcCondition condition = DBSearchConditionAddonUtil.getPjtCondition(conditionPjtNo);
		List<IPjtEntity> entities = this.getAmFinderSupport().getPjtDao().find(condition.getCondition());

		return entities.toArray(new IPjtEntity[0]);
	}

	/**
	 * ビルドパッケージエンティティから申請番号を取得する。
	 *
	 * @param bpDtoList ビルドパッケージエンティティ
	 * @return 申請番号の配列
	 */
	public String[] getAreqIds( List<IBpDto> bpDtoList) {

		List<IBpAreqLnkEntity> entityList = new ArrayList<IBpAreqLnkEntity>();
		for (IBpDto entity : bpDtoList) {
			entityList.addAll( entity.getBpAreqLnkEntities() );
		}

		Set<String> applyNoSet = new TreeSet<String>();

		for (IBpAreqLnkEntity entity : entityList) {
			applyNoSet.add( entity.getAreqId() );
		}

		return (String[]) applyNoSet.toArray(new String[0]);
	}

	public String[] getApplyNo( IBpDto bpDto) {

		List<IBpAreqLnkEntity> entityList = new ArrayList<IBpAreqLnkEntity>();
		entityList.addAll( bpDto.getBpAreqLnkEntities() );

		Set<String> applyNoSet = new TreeSet<String>();

		for (IBpAreqLnkEntity entity : entityList) {
			applyNoSet.add( entity.getAreqId() );
		}

		return (String[]) applyNoSet.toArray(new String[0]);
	}
	/**
	 * リリースエンティティからビルドパッケージ番号を取得する。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @return 申請番号の配列
	 */
	public String[] getBuildNoFromRpDto(List<IRpDto> entities) {

		Set<String> buildNoSet = new TreeSet<String>();
		for (IRpDto entity : entities) {
			for (String buildNo : RmEntityAddonUtils.getBuildNo(entity)) {
				buildNoSet.add(buildNo);
			}
		}

		return (String[]) buildNoSet.toArray(new String[0]);
	}

	/**
	 * リリース申請エンティティからリリース申請番号を取得する。
	 *
	 * @param entities リリース申請エンティティ
	 * @return リリース申請番号の配列
	 */
	public String[] getRaIds( RaDtoList raDtoList) {

		Set<String> relApplyNoSet = new HashSet<String>();

		for ( IRaDto raDto : raDtoList ) {
			relApplyNoSet.add( raDto.getRaEntity().getRaId() );
		}

		return (String[]) relApplyNoSet.toArray(new String[0]);
	}

	/**
	 * リリース申請エンティティからロット番号を取得する。
	 *
	 * @param dtoList リリース申請DTO
	 * @return ロット番号の配列
	 */
	public String[] getLotNo( List<IRaDto> dtoList ) {

		Set<String> lotNoSet = new HashSet<String>();

		for (IRaDto raDto : dtoList) {
			lotNoSet.add(raDto.getRaEntity().getLotId());
		}

		return (String[]) lotNoSet.toArray(new String[0]);
	}

	/**
	 * 対象リリース環境のビルドパッケージエンティティを取得する
	 *
	 * @param envNo 対象リリース環境番号
	 * @return ビルドパッケージエンティティ
	 */
	public List<IRpEntity> getRpEntityByRelEnv(String envNo) {

		RpCondition condition = new RpCondition();
		condition.setBldEnvId(envNo);

		IEntityLimit<IRpEntity> limit = this.getRpDao().find(condition.getCondition(), null, 1, 0);

		return limit.getEntities();
	}

	/**
	 * 対象リリース申請エンティティを取得する
	 *
	 * @param envNo 対象リリース申請番号
	 * @return リリース申請エンティティ
	 */
	public RaDtoList getRelApplyEntity(String[] relApplyNo) {

		RaCondition condition = new RaCondition();
		condition.setRaIds(relApplyNo);

		List<IRaEntity> raEntities = this.getRaDao().find(condition.getCondition());

		return this.findRaDtoList( raEntities );
	}

	/**
	 * 対象リリースのリリースエンティティを取得する relNoが空の場合は、全件検索せず0件で返す
	 *
	 * @param relNo 対象リリース番号
	 * @return リリースエンティティ
	 */
	public IRpEntity[] getRpEntityByRelNo(String[] relNo) {

		IJdbcCondition condition = DBSearchConditionAddonUtil.getRpConditionByRelNo(relNo);
		IEntityLimit<IRpEntity> limit = this.getRpDao().find(condition.getCondition(), null, 1, 0);

		return limit.getEntities().toArray(new IRpEntity[0]);
	}

	/**
	 * 対象リリース環境のビルドパッケージエンティティを取得する
	 *
	 * @param envNo 対象リリース環境番号
	 * @return ビルドパッケージエンティティ
	 */
	public IRpEntity[] getRpEntity(String[] relNo) {

		RpCondition condition = new RpCondition();
		condition.setRpIds(relNo);

		IEntityLimit<IRpEntity> limit = this.getRpDao().find(condition.getCondition(), null, 1, 0);

		return limit.getEntities().toArray(new IRpEntity[0]);
	}

	/**
	 * リリースエンティティからリリース番号を取得する。
	 *
	 * @param entities リリースエンティティ
	 * @return リリース番号の配列
	 */
	public String[] getRpIds(List<IRpDto> rpDtoList ) {

		Set<String> relNoSet = new HashSet<String>();

		for (IRpDto rpDto : rpDtoList ) {
			relNoSet.add(rpDto.getRpEntity().getRpId());
		}

		return (String[]) relNoSet.toArray(new String[0]);
	}

	public String[] getRelNoFromRpEntity(IRpEntity[] rpEntity ) {

		Set<String> relNoSet = new HashSet<String>();

		for (IRpEntity entity : rpEntity) {
			relNoSet.add(entity.getRpId());
		}

		return (String[]) relNoSet.toArray(new String[0]);
	}

	/**
	 * 申請情報のソート情報を取得する。
	 *
	 * @return 申請情報のソート情報
	 */
	public static ISqlSort getAssetApplySort() {

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.areqId, TriSortOrder.Desc, 1);

		return sort;
	}

	/**
	 * リリース環境に紐づく、最新のリリース情報を取得する。
	 *
	 * @param envNo リリース環境番号
	 * @param lotId ロット番号
	 * @return リリース情報
	 */
	public final IRpEntity getRecentRpEntity(String envNo, String[] lotId) {

		IRpEntity relEntry = null;

		if (0 != lotId.length) {

			// リリース設定日の降順
			ISqlSort sort = new SortBuilder();
			sort.setElement(RpItems.relTimestamp, TriSortOrder.Desc, 1);

			RpCondition condition = new RpCondition();

			String[] statusId = { RmRpStatusId.ReleasePackageCreated.getStatusId(), RmRpStatusId.ReleasePackageClosed.getStatusId() };
			condition.setBldEnvId(envNo);
			condition.setStsIds(statusId);
			condition.setLotIds(lotId);

			IEntityLimit<IRpEntity> limit = this.getRpDao().find(condition.getCondition(), sort, 1, 1);

			relEntry = (0 == limit.getEntities().size()) ? null : limit.getEntities().get(0);
		}

		return relEntry;
	}

	/**
	 * リリースの検索条件に情報を設定する
	 *
	 * @param condition
	 * @param relCtlSearchBean
	 */
	public void setRelCondition(RpCondition condition, RelCtlSearchBean relCtlSearchBean) {

		if (TriStringUtils.isNotEmpty(relCtlSearchBean.getSelectedLotNo())) {
			condition.setLotId(relCtlSearchBean.getSelectedLotNo());
		} else {
			List<String> lotNoList = relCtlSearchBean.getSearchLotNoValueList();
			condition.setLotIds((String[]) lotNoList.toArray(new String[0]));
		}

		if ( TriStringUtils.isNotEmpty(relCtlSearchBean.getSearchRelNo())) {
			if ( TriStringUtils.isNotEmpty(relCtlSearchBean.getSearchBuildNo()) ){
				//RelNoあり且つBuildNoあり
				condition.setRpId( getRpIdFromLnk( relCtlSearchBean.getSearchBuildNo(), relCtlSearchBean.getSearchRelNo() ) );
			} else {
				//RelNoあり
				condition.setRpId(relCtlSearchBean.getSearchRelNo(), true);
			}
		} else if ( TriStringUtils.isNotEmpty(relCtlSearchBean.getSearchBuildNo()) ) {
			//BuildNoあり
			condition.setRpIds(true, getRpIdFromBpId(relCtlSearchBean.getSearchBuildNo() ) );
		}

		if ( TriStringUtils.isNotEmpty(relCtlSearchBean.getSearchSummary())) {
			condition.setSummary(relCtlSearchBean.getSearchSummary());
		}
		if ( TriStringUtils.isNotEmpty(relCtlSearchBean.getSearchContent())) {
			condition.setContent(relCtlSearchBean.getSearchContent());
		}

		// if ( !StringAddonUtil.isNothing(
		// releaseSearchBean.getSelectedStatus() ) ) {
		// condition.setStatusId( new String[]{
		// releaseSearchBean.getSelectedStatus() } );
		// }

	}

	private String getRpIdFromLnk( String bpId, String rpId ) {

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setBpId(bpId, true);
		condition.setRpId(rpId, true);
		condition.setRpStsIds(
				RmRpStatusId.ReleasePackageCreated.getStatusId(),
				RmRpStatusId.ReleasePackageClosed.getStatusId());

		return this.getRpBpLnkDao().findByPrimaryKey( condition.getCondition() ).getRpId();

	}

	private String[] getRpIdFromBpId( String bpId ) {

		List<IRpBpLnkEntity> lnkEntityList = this.findRpBpLnkEntitiesFromBpId( bpId );
		List<String> rpIds = new ArrayList<String>();
		for (IRpBpLnkEntity lnkEntity : lnkEntityList ) {
			rpIds.add( lnkEntity.getRpId() );
		}
		return rpIds.toArray( new String[0] );
	}
	/**
	 * リリース一覧検索の結果に、検索条件を適用する。
	 *
	 * @param entities リリースエンティティ
	 * @param relCtlSearchBean 検索条件格納ビーン
	 * @param 検索条件に合致したリリースビーンのリスト
	 */
	public List<IRpEntity> applySearchCondition(List<IRpDto> entities, RelCtlSearchBean relCtlSearchBean) {

		List<IRpEntity> relList = new ArrayList<IRpEntity>();

		String searchPjtNo = relCtlSearchBean.getSearchPjtNo();
		String searchChangeCauseNo = relCtlSearchBean.getSearchChangeCauseNo();


		// RCごとに検索条件に該当するものを判定して抽出する
		for (IRpDto relEntity : entities) {

			if (TriStringUtils.isEmpty(searchPjtNo) && TriStringUtils.isEmpty(searchChangeCauseNo)) {
				// 検索条件指定なし
				relList.add(relEntity.getRpEntity());
			} else {

				String[] buildNoArray = RmEntityAddonUtils.getBuildNo(relEntity);

				if (TriStringUtils.isEmpty(buildNoArray))
					continue;

				PjtCondition condition = new PjtCondition();

				if ( TriStringUtils.isNotEmpty(searchPjtNo)) {
					condition.setPjtId(searchPjtNo, true);
				}

				if ( TriStringUtils.isNotEmpty(searchChangeCauseNo))
					condition.setChgFactorNo(searchChangeCauseNo, true);

				int pjtCount = this.getAmFinderSupport().getPjtDao().count(condition.getCondition());
				if (0 == pjtCount)
					continue;

				relList.add(relEntity.getRpEntity());
			}
		}

		return relList;
	}

	/**
	 * ビルドパッケージエンティティから申請情報エンティティを取得します。
	 *
	 * @param bpDtoList ビルドパッケージエンティティ
	 * @param selectPageNo 選択ページ
	 * @return 申請情報エンティティ
	 */
	public IAreqEntity[] getAssetApplyEntity(List<IBpDto> bpDtoList) {

		// ビルドパッケージされた申請情報
		String[] applyNo = getAreqIds(bpDtoList);

		if (TriStringUtils.isEmpty(applyNo)) {

			// IBuildApplyEntityの登録前に死んだビルドエンティティ対応
			// 全資産コンパイルもかな？
			return new IAreqEntity[] {};

		} else {

			return this.getAssetApplyEntity(applyNo);
		}
	}

	/**
	 * 申請番号から申請情報エンティティを取得する
	 *
	 * @param applyNo 申請番号
	 * @return 申請情報エンティティ
	 */
	private IAreqEntity[] getAssetApplyEntity(String[] applyNo) {

		if (TriStringUtils.isEmpty(applyNo)) {
			throw new TriSystemException(RmMessageId.RM005001S);
		}
		IJdbcCondition condition = DBSearchConditionAddonUtil.getAreqCondition(applyNo);
		List<IAreqEntity> areqEntities = this.getAmFinderSupport().getAreqDao().find(condition.getCondition());

		return areqEntities.toArray(new IAreqEntity[0]);
	}

	/**
	 * ロット番号からロットエンティティを取得する。
	 *
	 * @param lotNoArray
	 * @return
	 */
	public List<ILotEntity> getPjtLotEntityList(String[] lotNoArray) {

		ISqlSort sort = new SortBuilder();
		LotCondition condition = new LotCondition();
		condition.setLotIds(lotNoArray);

		return this.getAmFinderSupport().getLotDao().find(condition.getCondition(), sort, 1, 0).getEntities();

	}
	/**
	 * ビルドパッケージIDとビルドパッケージIDからマージ順を取得します。
	 * @param rpId
	 * @param bpId
	 * @return
	 */
	public String getMergeOdr(String rpId , String bpId){
		return findRpBpLnkEntity(rpId,bpId).getMergeOdr();
	}

}
