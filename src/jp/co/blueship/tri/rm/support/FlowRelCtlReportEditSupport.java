package jp.co.blueship.tri.rm.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;


/**
 * レポート系イベントのサポートClass
 * <br>
 * <p>
 * レポート編集を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlReportEditSupport extends RmFinderSupport {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース／資産管理台帳の対象となるビルドパッケージを取得します。
	 *
	 * @param relNo リリース番号の配列
	 * @return
	 */
	public List<IRpEntity> getRelEntityByRelAssetRegister( String[] relNo ) {
		if ( TriStringUtils.isEmpty(relNo) ) {
			return new ArrayList<IRpEntity>();
		}

		RpCondition condition = new RpCondition();
		condition.setRpIds( relNo );
		ISqlSort sort = this.getRelSortByRelCtlAssetRegister();
		IEntityLimit<IRpEntity> limit = this.getRpDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities();
	}

	/**
	 * リリース／資産管理台帳の対象となるビルドパッケージを取得します。
	 *
	 * @param buildNo ビルドパッケージ番号の配列
	 * @return
	 */
	public List<IBpEntity> getBuildEntityByRelAssetRegister( String[] buildNo ) {
		if ( TriStringUtils.isEmpty(buildNo) ) {
			return new ArrayList<IBpEntity>();
		}

		BpCondition condition = new BpCondition();
		condition.setBpIds( buildNo );
		ISqlSort sort = this.getBuildSortByRelAssetRegister();
		IEntityLimit<IBpEntity> limit = this.getBmFinderSupport().getBpDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities();
	}

	/**
	 * リリース／資産管理台帳の対象となる変更管理情報を取得します。
	 *
	 * @param buildEntity ビルドパッケージリスト
	 * @return
	 */
	public IPjtEntity[] getPjtEntityByRelAssetRegister( IBpDto buildEntity ) {

		if ( TriStringUtils.isEmpty( buildEntity )) {
			return new IPjtEntity[0];
		}

		Set<String> pjtIdSet = new HashSet<String>();
		for ( IBpAreqLnkEntity buildApplyEntity : buildEntity.getBpAreqLnkEntities() ) {
			pjtIdSet.add( this.getAmFinderSupport().findAreqEntity(buildApplyEntity.getAreqId()).getPjtId() );
		}

		if ( 0 == pjtIdSet.size() )
			return new IPjtEntity[0];

		PjtCondition condition = new PjtCondition();
		condition.setPjtIds( (String[])pjtIdSet.toArray( new String[0] ) );
		ISqlSort sort = this.getPjtSortByRelAssetRegister();
		IEntityLimit<IPjtEntity> limit = this.getAmFinderSupport().getPjtDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new IPjtEntity[0]);
	}

	/**
	 * リリース／資産管理台帳の対象となる申請情報を取得します。
	 *
	 * @param buildEntitys ビルドパッケージリスト
	 * @return
	 */
	public IAreqEntity[] getAssetApplyEntityByRelAssetRegister( IBpDto buildEntity ) {

		if ( TriStringUtils.isEmpty( buildEntity )) {
			return new IAreqEntity[0];
		}

		Set<String> applyNoSet = new HashSet<String>();
		for ( IBpAreqLnkEntity buildApplyEntity : buildEntity.getBpAreqLnkEntities() ) {
			applyNoSet.add( buildApplyEntity.getAreqId() );
		}

		if ( 0 == applyNoSet.size() )
			return new IAreqEntity[0];

		AreqCondition condition = new AreqCondition();
		condition.setAreqIds( (String[])applyNoSet.toArray( new String[0] ) );
		ISqlSort sort = this.getAssetApplySortByRelAssetRegister();
		IEntityLimit<IAreqEntity> limit = this.getAmFinderSupport().getAreqDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new IAreqEntity[0]);
	}

	/**
	 *  リリース／資産管理台帳のリリースソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getRelSortByRelCtlAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.relAssetRegisterRelCtlOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.relAssetRegisterRelCtlOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getBuildSortByRelCtlAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  リリース／資産管理台帳のビルドパッケージソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getBuildSortByRelAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.relAssetRegisterRelUnitOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.relAssetRegisterRelUnitOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getBuildSortByRelAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  リリース／資産管理台帳の変更管理ソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getPjtSortByRelAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.relAssetRegisterPjtOrder );
		Map<String, String> sortOrder = sheet.getKeyMap(  DcmDesignBeanId.relAssetRegisterPjtOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getPjtSortByRelAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  リリース／資産管理台帳の申請ソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getAssetApplySortByRelAssetRegister() {
		ISqlSort sort = new SortBuilder();

		sort.setElement(PjtItems.regTimestamp, TriSortOrder.Asc, 1);

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getAssetApplySortByRelAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 * リリースエンティティを取得する
	 * @param relNo リリース番号
	 * @return リリースエンティティ
	 */
	public IRpEntity[] getRelEntity( String[] relNo ) {

		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getRpConditionByRelNo( relNo );

		IEntityLimit<IRpEntity> limit = this.getRpDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IRpEntity[0]);
	}

	/**
	 * ビルドパッケージIDとビルドパッケージIDからマージ順を取得します。
	 * @param rpId
	 * @param BpId
	 * @return
	 */
	public String getMergeOdr(String rpId , String BpId){
		return findRpBpLnkEntity(rpId,BpId).getMergeOdr();
	}

}
