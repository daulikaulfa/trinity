package jp.co.blueship.tri.rm.support;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.dao.cal.eb.CalCondition;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyDetailViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;



/**
 * FlowRelApplyEntry等のリリース申請系のイベントのサポートClass
 * <br>
 * <p>
 * リリース申請のための業務サービス処理を支援するサポートクラスです。
 * </p>
 * リリース業務と深く関連するためFlowRelCtlEditSupportを継承します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Eguchi Yukihiro
 */
public class FlowRelApplyEditSupport extends FlowRelCtlEditSupport {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース環境情報を取得する。
	 * @return 全環境情報
	 */
	public IBldEnvEntity[] getRelEnvEntityForPullDown() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelEnvCondition();
		ISqlSort sort			= DBSearchSortAddonUtil.getRelEnvSortFromDesignDefineByRelApplyTop();

		List<IBldEnvEntity> bldEnvEntities =
			this.getBmFinderSupport().getBldEnvDao().find( condition.getCondition(), sort );

		return bldEnvEntities.toArray(new IBldEnvEntity[0]);
	}
	/**
	 * リリースカレンダー情報を取得する。
	 * @return 全リリースカレンダー情報
	 */
	public ICalEntity[] getRelCalendarEntityForPullDown() {

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActivelRelCalendarCondition();
		ISqlSort sort			=  DBSearchSortAddonUtil.getRelCalendarSortFromDesignDefineByRelApplyTop();

		List<ICalEntity> calEntities =
			this.getCalDao().find( condition.getCondition(), sort );

		return calEntities.toArray(new ICalEntity[0]);
	}

	/**
	 * 指定したリリース日付に該当するリリース環境情報を取得する。
	 * @param releaseDate リリース日
	 * @return 全環境情報
	 */
	public ICalEntity[] getRelCalendarEntity( String releaseDate ) {

		if ( TriStringUtils.isEmpty(releaseDate) ) {
			return new ICalEntity[0];
		}

		CalCondition condition = DBSearchConditionAddonUtil.getActivelRelCalendarCondition();
		condition.SetPreferredRelDate(releaseDate);

		ISqlSort sort			= DBSearchSortAddonUtil.getRelCalendarSortFromDesignDefineByRelApplyTop();

		List<ICalEntity> calEntities =
			this.getCalDao().find( condition.getCondition(), sort );

		return calEntities.toArray(new ICalEntity[0]);
	}

	/**
	 * リリースエンティティからビルドパッケージ番号を取得する。
	 * @param entities ビルドパッケージエンティティ
	 * @return 申請番号の配列
	 */
	public String[] getBuildNo( List<UnitBean> unitBeanList ) {

		if ( null == unitBeanList ) {
			return new String[0];
		}

		Set<String> buildNoSet = new TreeSet<String>();

		for (UnitBean unitBean : unitBeanList) {
			buildNoSet.add( unitBean.getBuildNo() );
		}

		return (String[])buildNoSet.toArray( new String[0] );
	}

	/**
	 * リリース申請エンティティからリリース番号を取得する。
	 * @param dtoList リリース申請DTO
	 * @return リリース番号の配列
	 */
	public String[] getRpIdsFromRaDto( List<IRaDto> dtoList ) {

		Set<String> relNoSet = new TreeSet<String>();

		for ( IRaDto dto : dtoList ) {
			if ( null == dto.getRaRpLnkEntities() ) {
				continue;
			}

			for ( IRaRpLnkEntity raRpLnkEntity: dto.getRaRpLnkEntities() ) {
				relNoSet.add( raRpLnkEntity.getRpId() );
			}
		}

		return (String[])relNoSet.toArray( new String[0] );
	}

	/**
	 * リリース申請エンティティからビルドパッケージ番号を取得する。
	 * @param entities ビルドパッケージエンティティ
	 * @return ビルドパッケージ番号の配列
	 */
	public String[] getBpIdsFromRaDto( List<IRaDto> dtoList ) {

		Set<String> bpIdSet = new TreeSet<String>();

		for ( IRaDto raDto : dtoList ) {
			List<IRaBpLnkEntity> raBpLnkEntities = raDto.getRaBpLnkEntities();

			if ( null == raBpLnkEntities ) {
				continue;
			}

			for ( IRaBpLnkEntity raBpLnkEntity : raBpLnkEntities ) {
				bpIdSet.add( raBpLnkEntity.getBpId() );
			}
		}

		return (String[])bpIdSet.toArray( new String[0] );
	}

	/**
	 * リリース申請DTOから変更管理番号を取得する。
	 * @param dtoList リリース申請DTO
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNo( List<IRaDto> dtoList ) {

		Set<String> pjtIdSet = new TreeSet<String>();

		for ( IRaDto raDto : dtoList ) {
			for ( IAreqEntity areqEntity: this.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() ) ) {
				pjtIdSet.add( areqEntity.getPjtId() );
			}
		}

		return (String[])pjtIdSet.toArray( new String[0] );
	}

	/**
	 * リリース申請DTOから資産申請番号を取得する。
	 * @param raDtoList リリース申請DTO
	 * @return 資産申請番号の配列
	 */
	public String[] getAssetApplyNo( List<IRaDto> dtoList ) {

		Set<String> assetApplyNoSet = new TreeSet<String>();
		for ( IRaDto raDto : dtoList ) {
			for ( IAreqEntity areqEntity: this.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() ) ) {
				assetApplyNoSet.add( areqEntity.getAreqId() );
			}
		}

		return (String[])assetApplyNoSet.toArray( new String[0] );
	}

	/**
	 * 指定されたアップロードファイルをサーバに保存します。
	 *
	 * @param paramBean
	 * @param retBean
	 */
	public void saveUploadFile( RelApplyEntryViewBean paramBean, RelApplyEntryViewBean retBean, boolean isModify ) {
		if ( 0 == paramBean.getAppendFile().size() ) {
			return;
		}

		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		// 申請番号フォルダをつなげる
		String relApplyNo = paramBean.getRelApplyNo();
		String relApplyNoPath = TriStringUtils.linkPathBySlash( basePath , relApplyNo );

		// ユニークになるよう日時フォルダをつなげる
		String uniqueKey = "" ;
		String uniquePath = TriStringUtils.linkPathBySlash( relApplyNoPath , uniqueKey );

		// 前回コピー用
		String uniqueKeyLast = BusinessFileUtils.getLastUniqueKey(basePath, relApplyNo);

		// 添付ファイルごとのフォルダをつなげる
		for ( String id: paramBean.getAppendFile().keySet() ) {
			AppendFileBean appendFilebean = paramBean.getAppendFile().get(id);

			// 編集かつ、新たにファイルを指定されなかった場合
			if(isModify
					&& TriStringUtils.isEmpty(appendFilebean.getAppendFileInputStreamName()) ) {

				BusinessFileUtils.copyAppendFile(
						basePath,
						relApplyNo,
						uniqueKeyLast,
						uniqueKey,
						id.toString(),
						appendFilebean.getAppendFile() );

			}

			if ( TriStringUtils.isEmpty(appendFilebean.getAppendFileInputStreamBytes()) ) {
				continue;
			}

			String appendPath1 = TriStringUtils.linkPathBySlash( uniquePath , id.toString() );

			BusinessFileUtils.saveAppendFile(
					basePath,
					relApplyNo,
					uniqueKey,
					id.toString(),
					StreamUtils.convertBytesToInputStreamToBytes( appendFilebean.getAppendFileInputStreamBytes() ),
					appendFilebean.getAppendFileInputStreamName() );


			if ( log.isDebugEnabled() ) {
				String filePath = new File(
						appendPath1 ,
						appendFilebean.getAppendFileInputStreamName() ).getPath();

				LogHandler.debug( log , "★★★:filePath:=" + filePath );
			}
		}

		this.clearAppendFileCache(paramBean, retBean);
	}

	/**
	 * セッションにキャッシュしている添付ファイルのバイナリイメージをクリアする。
	 *
	 * @param paramBean
	 * @param retBean
	 */
	public void clearAppendFileCache( RelApplyEntryViewBean paramBean, RelApplyEntryViewBean retBean ) {
		if ( 0 == paramBean.getAppendFile().size() ) {
			return;
		}

		// 添付ファイルごとのフォルダをつなげる
		for ( String id: paramBean.getAppendFile().keySet() ) {
			AppendFileBean appendFilebean = paramBean.getAppendFile().get(id);

			appendFilebean.setAppendFileInputStreamBytes( null );
		}
	}

	/**
	 * 添付ファイルのリンク情報を設定する。
	 *
	 * @param retBean
	 */
	public void setAppendFileLink( RelApplyEntryViewBean retBean ) {
		if ( 0 == retBean.getAppendFile().size() ) {
			return;
		}

		String saveDirURL = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFileURL ) ;
		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		// 申請番号フォルダをつなげる
		String relApplyNo = retBean.getRelApplyNo();
		String relApplyNoPath = TriStringUtils.linkPathBySlash( basePath , relApplyNo );

		// 添付ファイルごとのフォルダをつなげる
		for ( String id: retBean.getAppendFile().keySet() ) {
			RelApplyEntryViewBean.AppendFileBean appendFilebean = retBean.getAppendFile().get(id);

			if ( TriStringUtils.isEmpty(appendFilebean.getAppendFile()) ) {
				appendFilebean.setAppendFileLink(null);
				continue;
			}

			// 最新のユニークのフォルダをつなげる
			String uniqueKey = "";
			String uniquePath = TriStringUtils.linkPathBySlash( relApplyNoPath , uniqueKey ) ;


			String appendPath = TriStringUtils.linkPathBySlash( uniquePath , id.toString() );

			File file = new File(appendPath, appendFilebean.getAppendFile());
			String linkUrl = BusinessFileUtils.getAppendFileURL(saveDirURL, relApplyNo, uniqueKey, id, appendFilebean.getAppendFile() );

			if(false == file.exists()) {
				// ファイルがない場合
				appendFilebean.setAppendFileLink( null );
				continue;
			}
			if(TriStringUtils.isEmpty(linkUrl)) {
				// URLがつくれなかった場合
				appendFilebean.setAppendFileLink( null );
				continue;
			}

			appendFilebean.setAppendFileLink( linkUrl );
		}
	}

	/**
	 * 添付ファイルのリンク情報を設定する。
	 *
	 * @param retBean
	 */
	public void setAppendFileLink( RelApplyDetailViewBean retBean ) {
		if ( 0 == retBean.getAppendFile().size() ) {
			return;
		}

		String saveDirURL = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFileURL ) ;
		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		// 申請番号フォルダをつなげる
		String relApplyNo = retBean.getRelApplyNo();
		String relApplyNoPath = TriStringUtils.linkPathBySlash( basePath , relApplyNo );

		// 添付ファイルごとのフォルダをつなげる
		for ( String id: retBean.getAppendFile().keySet() ) {
			RelApplyDetailViewBean.AppendFileBean appendFilebean = retBean.getAppendFile().get(id);

			if ( TriStringUtils.isEmpty(appendFilebean.getAppendFile()) ) {
				appendFilebean.setAppendFileLink(null);
				continue;
			}

			// 最新のユニークのフォルダをつなげる

			String appendPath = TriStringUtils.linkPathBySlash( relApplyNoPath , id.toString() );

			File file = new File(appendPath, appendFilebean.getAppendFile());
			String linkUrl = BusinessFileUtils.getAppendFileURL(saveDirURL, relApplyNo, "", id, appendFilebean.getAppendFile() );

			if(false == file.exists()) {
				// ファイルがない場合
				appendFilebean.setAppendFileLink( null );
				continue;
			}
			if(TriStringUtils.isEmpty(linkUrl)) {
				// URLがつくれなかった場合
				appendFilebean.setAppendFileLink( null );
				continue;
			}

			appendFilebean.setAppendFileLink( linkUrl );
		}
	}

	/**
	 * メール送信情報・パッケージリストを取得する。
	 *
	 * @param relApplyEntity
	 * @return
	 */
	public Collection<Map<String,Object>> getPackageList(
			IRaDto raDto ) {

		Map<String, Collection<IAreqEntity>> areqMap = new TreeMap<String, Collection<IAreqEntity>>();

		//Map初期化
		for ( IRaBpLnkEntity raBpLnkEntity : raDto.getRaBpLnkEntities() ) {
			areqMap.put(raBpLnkEntity.getBpId(), new ArrayList<IAreqEntity>());
		}

		//RP単位に資産申請をMap変換
		for ( IRaBpLnkEntity raBpLnkEntity: raDto.getRaBpLnkEntities() ) {
			Collection<IAreqEntity> applyList = areqMap.get( raBpLnkEntity.getBpId() );
			applyList.addAll( findAreqEntitiesFromRaBpLnk( raBpLnkEntity ) );
		}

		//RP単位に資産情報を処理する
		Collection<Map<String,Object>> packageList = new ArrayList<Map<String,Object>>();

		for ( String buildNo: areqMap.keySet() ) {
			Collection<IAreqEntity> applyList = areqMap.get(buildNo);

			//変更管理単位のMap
			Map<String, Map<String,Object>> dupPjtMap = new TreeMap<String, Map<String,Object>>();

			for ( IAreqEntity buildApply: applyList ) {
				String pjtNo			= buildApply.getPjtId();
				String changeCauseNo 	= this.getAmFinderSupport().findPjtEntity(pjtNo).getChgFactorNo();
				String applyNo			= buildApply.getAreqId();

				if ( ! dupPjtMap.containsKey(pjtNo) ) {
					Map<String,Object> pjtMap = new HashMap<String,Object>();
					pjtMap.put("pjtId", pjtNo);
					pjtMap.put("chgFactorNo", changeCauseNo);
					pjtMap.put("applyList", new ArrayList<String>());
					dupPjtMap.put( pjtNo, pjtMap );
				}

				Map<String,Object> pjtMap = dupPjtMap.get(pjtNo);
				@SuppressWarnings("unchecked")
				List<String> applys = (List<String>)pjtMap.get("applyList");
				applys.add( applyNo );
			}

			//申請資産をソート
			for ( Map<String,Object> pjtMap: dupPjtMap.values() ) {
				@SuppressWarnings("unchecked")
				List<String> applys = (List<String>)pjtMap.get("applyList");
				Collections.sort(applys);
			}

			Map<String,Object> packageMap = new HashMap<String,Object>();
			packageMap.put("bpId", buildNo);
			packageMap.put("pjtList", dupPjtMap.values());

			packageList.add( packageMap );
		}

		return packageList;
	}

	/**
	 * 資産申請IDからビルドパッケージIDを検索して返します。
	 * @param areqId
	 * @return BdId
	 */
	public String findBpId( String areqId ){

		PreConditions.assertOf(areqId != null, "areqId is null. ");
		BpAreqLnkCondition condition = new BpAreqLnkCondition();
		condition.setAreqId( areqId );
		List<IBpAreqLnkEntity> lnkEntity = this.getBmFinderSupport().getBpAreqLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(lnkEntity)){
			throw new TriSystemException (BmMessageId.BM005092S );
		}
		return lnkEntity.get(0).getBpId();

	}

	/**
	 * リリース申請に含まれる承認済申請情報を取得する
	 * @param raDto
	 * @return
	 */
	public List<IAreqEntity> getAssetApplyEntity( IRaDto raDto ) {

		Set<String> bpIdSet = new TreeSet<String>();
		for ( IRaBpLnkEntity raBpLnkEntity: raDto.getRaBpLnkEntities() ) {
			bpIdSet.add( raBpLnkEntity.getBpId() );
		}

		List<IBpDto> bpDtoList = this.getBmFinderSupport().findBpDto( bpIdSet.toArray( new String[0] ) );
		IAreqEntity[] assetApplyEntities = this.getAssetApplyEntityFromBpDto( bpDtoList );


		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

			if ( !AmAreqStatusId.BuildPackageClosed.equals( assetApplyEntity.getStsId() ) ) {
				assetApplyEntityList.add( assetApplyEntity );
			}
		}

		return assetApplyEntityList;
	}

	/**
	 * リリース申請に含まれる承認済申請情報を取得する(構造がかわるかもしれないので仮)
	 * @param relApplyEntity
	 * @return
	 */
	public List<IAreqEntity> getAssetApplyListFromBpDto( List<IBpDto> bpDtoList ) {

		IAreqEntity[] assetApplyEntities = this.getAssetApplyEntityFromBpDto( bpDtoList );


		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

			if ( !AmAreqStatusId.BuildPackageClosed.equals( assetApplyEntity.getStsId() ) ) {
				assetApplyEntityList.add( assetApplyEntity );
			}
		}

		return assetApplyEntityList;
	}

	public List<IAreqEntity> getAssetApplyListFromBpDto( IBpDto bpDto ) {

		IAreqEntity[] assetApplyEntities = this.getAssetApplyEntityFromBpDto( bpDto );


		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

			if ( !AmAreqStatusId.BuildPackageClosed.equals( assetApplyEntity.getStsId() ) ) {
				assetApplyEntityList.add( assetApplyEntity );
			}
		}

		return assetApplyEntityList;
	}
	/**
	 * ビルドパッケージエンティティから申請情報エンティティを取得します。
	 *
	 * @param bpDtoList ビルドパッケージエンティティ
	 * @param selectPageNo 選択ページ
	 * @return 申請情報エンティティ
	 */
	private IAreqEntity[] getAssetApplyEntityFromBpDto(List<IBpDto> bpDtoList) {

		// ビルドパッケージされた申請情報
		String[] applyNo = getAreqIds(bpDtoList);

		if (TriStringUtils.isEmpty(applyNo)) {

			// IBuildApplyEntityの登録前に死んだビルドエンティティ対応
			// 全資産コンパイルもかな？
			return new IAreqEntity[] {};

		} else {

			return this.getAssetApplyEntityFromNo(applyNo);
		}
	}

	private IAreqEntity[] getAssetApplyEntityFromBpDto( IBpDto bpDto) {

		// ビルドパッケージされた申請情報
		String[] applyNo = getApplyNo(bpDto);

		if (TriStringUtils.isEmpty(applyNo)) {

			// IBuildApplyEntityの登録前に死んだビルドエンティティ対応
			// 全資産コンパイルもかな？
			return new IAreqEntity[] {};

		} else {

			return this.getAssetApplyEntityFromNo(applyNo);
		}
	}

	/**
	 * 申請番号から申請情報エンティティを取得する
	 *
	 * @param areqIds 申請番号
	 * @return 申請情報エンティティ
	 */
	private IAreqEntity[] getAssetApplyEntityFromNo(String[] areqIds) {

		if (TriStringUtils.isEmpty(areqIds)) {
			throw new TriSystemException(RmMessageId.RM005001S);
		}
		List<IAreqEntity> entities = this.getAmFinderSupport().findAreqEntities(areqIds);

		return entities.toArray(new IAreqEntity[0]);
	}
	/**
	 * 申請添付ファイル情報を更新します。
	 *
	 * @param bean
	 */
	public void insertRaAttachedFile( RelApplyEntryViewBean bean ) {

		RaAttachedFileCondition condition = new RaAttachedFileCondition();
		condition.setRaId( bean.getRelApplyNo() );
		this.getRaAttachedFileDao().delete( condition.getCondition() );

		List<IRaAttachedFileEntity> appendFileEntityList = new ArrayList<IRaAttachedFileEntity>();
		for ( String id: bean.getAppendFile().keySet() ) {
			AppendFileBean appendFile = bean.getAppendFile().get(id);

			IRaAttachedFileEntity appendFileEntity = new RaAttachedFileEntity();
			appendFileEntity.setRaId(bean.getRelApplyNo());
			appendFileEntity.setAttachedFileSeqNo( id );
			appendFileEntity.setFilePath( appendFile.getAppendFile() );

			appendFileEntityList.add( appendFileEntity );
		}
		this.getRaAttachedFileDao().insert(appendFileEntityList);

	}

	/**
	 * リリース申請情報を保存する際のリリース申請・ビルドパッケージリンクテーブルを追加します。
	 * @param viewBean
	 */
	public void insertRaBpLnk(RelApplyEntryViewBean viewBean){

		List<UnitBean> unitBeanList = viewBean.getUnitBeanList();
		List<IRaBpLnkEntity> entityList = new ArrayList<IRaBpLnkEntity>();
		for (UnitBean unitBean : unitBeanList ) {
			IRaBpLnkEntity entity = new RaBpLnkEntity();
			entity.setRaId( viewBean.getRelApplyNo() );
			entity.setBpId( unitBean.getBuildNo() );
			entityList.add( entity );
		}
		this.getRaBpLnkDao().insert(entityList);

	}
	/**
	 * リリース申請情報を保存する際のリリース申請・ビルドパッケージリンクテーブルを削除します。
	 * @param viewBean
	 */
	public void deleteRaBpLnk(RelApplyEntryViewBean viewBean){

		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setRaId( viewBean.getRelApplyNo() );
		this.getRaBpLnkDao().delete(condition.getCondition());

	}

	/**
	 * リリース申請IDから資産申請エンティティリストを取得します。
	 * @param raId リリース申請ID
	 * @return AreqEntity 資産申請のリスト
	 */
	public List<IAreqEntity> getAreqEntityFromRaId( String raId ) {

		List<IAreqEntity> areqEntities = new ArrayList<IAreqEntity>();
		List<IRaBpLnkEntity> lnkEntities = getRaBpLnkEntitiesFromRaId(raId);
		for ( IRaBpLnkEntity entity : lnkEntities ){
			IBpDto bpDto = this.getBmFinderSupport().findBpDto( entity.getBpId(), BmTables.BM_BP_AREQ_LNK );

			for ( IBpAreqLnkEntity bpAreqEntity : bpDto.getBpAreqLnkEntities() ) {
				areqEntities.add( this.getAmFinderSupport().findAreqEntity( bpAreqEntity.getAreqId() ) );
			}
		}
		return areqEntities;
	}

	private List<IRaBpLnkEntity> getRaBpLnkEntitiesFromRaId( String raId ) {

		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setRaId(raId);
		List<IRaBpLnkEntity> lnkEntities = this.getRaBpLnkDao().find(condition.getCondition());

		return lnkEntities;
	}


}
