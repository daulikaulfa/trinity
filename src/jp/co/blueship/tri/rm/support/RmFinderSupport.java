package jp.co.blueship.tri.rm.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.BmFluentFunctionUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaAttachedFileCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.RaDtoList;
import jp.co.blueship.tri.rm.dao.ra.eb.RaRpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpDto;


/**
 * BM関連情報を検索するためのサービス機能を提供するクラスです。
 *
 * @version V3L10.01
 * @version V3L10.02
 * @author Yukihiro Eguchi
 * @author Takashi Ono
 *
 */
public class RmFinderSupport extends RmDaoFinder implements IRmFinderSupport {

	private ISmFinderSupport smFinderSupport = null;
	private IUmFinderSupport umFinderSupport = null;
	private IAmFinderSupport amFinderSupport = null;
	private IBmFinderSupport bmFinderSupport = null;
	private IDcmFinderSupport dcmFinderSupport = null;


	@Override
	public ISmFinderSupport getSmFinderSupport() {
	    return smFinderSupport;
	}

	/**
	 * smFinderSupportを設定します。
	 * @param smFinderSupport smFinderSupport
	 */
	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
	    this.smFinderSupport = smFinderSupport;
	}

	@Override
	public IUmFinderSupport getUmFinderSupport() {
	    return umFinderSupport;
	}

	/**
	 * umFinderSupportを設定します。
	 * @param umFinderSupport umFinderSupport
	 */
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
	    this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IAmFinderSupport getAmFinderSupport() {
	    return amFinderSupport;
	}

	/**
	 * amFinderSupportを設定します。
	 * @param amFinderSupport amFinderSupport
	 */
	public void setAmFinderSupport(IAmFinderSupport amFinderSupport) {
	    this.amFinderSupport = amFinderSupport;
	}

	@Override
	public IBmFinderSupport getBmFinderSupport() {
	    return bmFinderSupport;
	}

	/**
	 * bmFinderSupportを設定します。
	 * @param bmFinderSupport bmFinderSupport
	 */
	public void setBmFinderSupport(IBmFinderSupport bmFinderSupport) {
	    this.bmFinderSupport = bmFinderSupport;
	}

	@Override
	public IDcmFinderSupport getDcmFinderSupport() {
	    return dcmFinderSupport;
	}

	/**
	 * dcmFinderSupportを設定します。
	 * @param dcmFinderSupport dcmFinderSupport
	 */
	public void setDcmFinderSupport(IDcmFinderSupport dcmFinderSupport) {
	    this.dcmFinderSupport = dcmFinderSupport;
	}

	@Override
	public final String nextvalByRaTempId() {
		return this.getRaTempNumberingDao().nextval();
	}

	@Override
	public final String nextvalByRaId() {
		return this.getRaNumberingDao().nextval();
	}

	public List<IAreqEntity> findAreqEntitiesFromRaBpLnk( IRaBpLnkEntity raBpLnkEntity ) {

		IBpDto bpDto = getBmFinderSupport().findBpDto( raBpLnkEntity.getBpId(), BmTables.BM_BP_AREQ_LNK );

		String[] areqIds = FluentList.from(bpDto.getBpAreqLnkEntities()).map(
				BmFluentFunctionUtils.toAreqIdFromBpAreqLnk).toArray(new String[0]);

		return getAmFinderSupport().findAreqEntities(areqIds);
	}

	public List<IAreqEntity> findAreqEntitiesFromRaBpLnk( List<IRaBpLnkEntity> raBpLnkEntities ) {

		Map<String, IAreqEntity> areqMap = new HashMap<String, IAreqEntity>();

		for ( IRaBpLnkEntity raBpLnkEntity: raBpLnkEntities ) {
			for ( IAreqEntity areqEntity: findAreqEntitiesFromRaBpLnk(raBpLnkEntity) ) {
				areqMap.put(areqEntity.getAreqId(), areqEntity);
			}
		}

		return new ArrayList<IAreqEntity>( areqMap.values() );
	}

	@Override
	public IRpEntity findRpEntity( String rpId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(rpId) ){
			throw new TriSystemException(RmMessageId.RM005076S, "empty");
		}

		RpCondition condition = new RpCondition();
		condition.setRpId( rpId );

		IRpEntity rpEntity = this.getRpDao().findByPrimaryKey( condition.getCondition() );

		if ( null == rpEntity ){
			throw new TriSystemException(RmMessageId.RM005076S , rpId);
		}

		return rpEntity;
	}

	@Override
	public List<IRpEntity> findRpEntities( String... rpIds ) {

		if ( TriStringUtils.isEmpty(rpIds) ){
			throw new TriSystemException(RmMessageId.RM005076S, "empty");
		}

		RpCondition condition = new RpCondition();
		if ( 1 == rpIds.length ) {
			condition.setRpId( rpIds[0] );
		} else {
			condition.setRpIds( rpIds );
		}

		List<IRpEntity> rpEntities = this.getRpDao().find( condition.getCondition() );

		return rpEntities;
	}

	@Override
	public final IRpBpLnkEntity findRpBpLnkEntity( String rpId, String bpId ) {
		if ( TriStringUtils.isEmpty(rpId) ){
			throw new TriSystemException(RmMessageId.RM005076S, "empty");
		}
		if ( TriStringUtils.isEmpty(bpId) ){
			throw new TriSystemException(RmMessageId.RM005078S, "empty");
		}

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setRpId( rpId );
		condition.setBpId( bpId );
		IRpBpLnkEntity rpBpEntity = this.getRpBpLnkDao().findByPrimaryKey(condition.getCondition());

		if ( null == rpBpEntity ) {
			throw new TriSystemException(RmMessageId.RM005079S , rpId, bpId) ;
		}

		return rpBpEntity;
	}

	/**
	 * リリースパッケージ・ビルドパッケージを取得します。
	 *
	 * @param rpId リリースパッケージID
	 * @return 取得したリリースパッケージ・ビルドパッケージエンティティを戻します。
	 */
	private final List<IRpBpLnkEntity> findRpBpLnkEntities( String rpId ) {
		if ( TriStringUtils.isEmpty(rpId) ){
			throw new TriSystemException(RmMessageId.RM005076S, "empty");
		}
		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setRpId( rpId );
		List<IRpBpLnkEntity> entities = this.getRpBpLnkDao().find( condition.getCondition() );

		return entities;
	}

	public List<IRaBpLnkEntity> findRaBpLnkEntitiesFromBpId( String... bpIds ) {

		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setBpIds(bpIds);
  		condition.setRaStsIds(
				RmRaStatusId.ReleaseRequested.getStatusId(),
				RmRaStatusId.ReleaseRequestApproved.getStatusId(),
				RmRaStatusId.ReleaseRequestClosed.getStatusId(),
				RmRaStatusId.ReleasePackageCreated.getStatusId(),
				RmRaStatusId.DraftReleaseRequest.getStatusId());

		return this.getRaBpLnkDao().find( condition.getCondition() );
	}

	public List<IRpBpLnkEntity> findRpBpLnkEntitiesFromBpId( String... bpIds ) {

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setBpIds(bpIds);
  		condition.setRpStsIds(
				RmRpStatusId.ReleasePackageCreated.getStatusId(),
				RmRpStatusId.ReleasePackageClosed.getStatusId());

		return this.getRpBpLnkDao().find( condition.getCondition() );
	}

	public List<IRaRpLnkEntity> findRaRpLnkEntitiesFromRpId( String... rpIds ) {

		RaRpLnkCondition condition = new RaRpLnkCondition();
		condition.setRpIds(rpIds);
		condition.setRaStsIds(
				RmRaStatusId.ReleaseRequested.getStatusId(),
				RmRaStatusId.ReleaseRequestApproved.getStatusId(),
				RmRaStatusId.ReleaseRequestClosed.getStatusId(),
				RmRaStatusId.ReleasePackageCreated.getStatusId());

		return this.getRaRpLnkDao().find( condition.getCondition() );
	}

	/**
	 * リリースパッケージEntityの子Entityを取得しDTOに設定します。
	 *
	 * @param rpDto リリースパッケージDTO
	 * @param rpEntity リリースパッケージEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IRpDto setRpDto( IRpDto rpDto, IRpEntity rpEntity, RmTables... tables ) {
		rpDto.setRpEntity( rpEntity );

		RmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new RmTables[]{
					RmTables.RM_RP_BP_LNK,
			};
		}

		String rpId = rpEntity.getRpId();

		for ( RmTables table: findTables ) {
			if ( table.equals( RmTables.RM_RP_BP_LNK ) ) {
				rpDto.setRpBpLnkEntities(this.findRpBpLnkEntities( rpId ));
			}
		}

		return rpDto;
	}

	@Override
	public IRpDto findRpDto(String rpId, RmTables... tables) {
		IRpEntity rpEntity = findRpEntity( rpId );
		return this.setRpDto(new RpDto(), rpEntity, tables);
	}

	@Override
	public final List<IRpDto> findRpDto( String[] rpIds, RmTables... tables ) {
		List<IRpEntity> rpEntities = findRpEntities( rpIds );

		return this.findRpDto( rpEntities );
	}

	@Override
	public IRpDto findRpDto(IRpEntity rpEntity, RmTables... tables) {
		return this.setRpDto(new RpDto(), rpEntity, tables);
	}

	@Override
	public final List<IRpDto> findRpDto( ISqlCondition condition, RmTables... tables ) {
		List<IRpDto> rpDtoList = new ArrayList<IRpDto>();

		List<IRpEntity> rpEntities = this.getRpDao().find( condition );

		for ( IRpEntity IRpEntity: rpEntities ) {
			rpDtoList.add( this.setRpDto( new RpDto(), IRpEntity, tables) );
		}

		return rpDtoList;
	}

	@Override
	public List<IRpDto> findRpDto( List<IRpEntity> rpEntities, RmTables... tables ) {
		List<IRpDto> rpDtoList = new ArrayList<IRpDto>();

		for ( IRpEntity rpEntity : rpEntities ) {
			rpDtoList.add( this.setRpDto( new RpDto(), rpEntity, tables ) );
		}

		return rpDtoList;
	}

	/**
	 * リリース申請Entityの子Entityを取得しDTOに設定します。
	 *
	 * @param raDto リリース申請DTO
	 * @param raEntity リリース申請Entity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IRaDto setRaDto( IRaDto raDto, IRaEntity raEntity, RmTables... tables ) {
		raDto.setRaEntity( raEntity );

		RmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new RmTables[]{
					RmTables.RM_RA_ATTACHED_FILE,
					RmTables.RM_RA_BP_LNK,
					RmTables.RM_RA_RP_LNK,
			};
		}

		String raId = raEntity.getRaId();

		for ( RmTables table: findTables ) {
			if ( table.equals( RmTables.RM_RA_ATTACHED_FILE ) ) {
				raDto.setRaAttachedFileEntities(this.findRaAttachedFileEntities( raId ));
				continue;
			}
			if ( table.equals( RmTables.RM_RA_BP_LNK ) ) {
				raDto.setRaBpLnkEntities(this.findRaBpLnkEntities( raId ));
				continue;
			}
			if ( table.equals( RmTables.RM_RA_RP_LNK ) ) {
				raDto.setRaRpLnkEntities(this.findRaRpLnkEntities( raId ));
				continue;
			}
		}

		return raDto;
	}

	@Override
	public IRaEntity findRaEntity( String raId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(raId) ){
			throw new TriSystemException(RmMessageId.RM005077S, "empty");
		}

		RaCondition condition = new RaCondition();
		condition.setRaId( raId );

		IRaEntity raEntity = this.getRaDao().findByPrimaryKey( condition.getCondition() );

		if ( null == raEntity ){
			throw new TriSystemException(RmMessageId.RM005077S , raId);
		}

		return raEntity;
	}

	@Override
	public List<IRaEntity> findRaEntities( String... raIds ) {

		if ( TriStringUtils.isEmpty(raIds) ){
			throw new TriSystemException(RmMessageId.RM005077S, "empty");
		}

		RaCondition condition = new RaCondition();
		if ( 1 == raIds.length ) {
			condition.setRaId( raIds[0] );
		} else {
			condition.setRaIds( raIds );
		}

		List<IRaEntity> raEntities = this.getRaDao().find( condition.getCondition() );

		return raEntities;
	}

	/**
	 * 申請添付ファイルを取得します。
	 *
	 * @param raId リリース申請ID
	 * @return 取得した申請添付ファイルエンティティを戻します。
	 */
	private final List<IRaAttachedFileEntity> findRaAttachedFileEntities( String raId ) {
		if ( TriStringUtils.isEmpty(raId) ){
			throw new TriSystemException(RmMessageId.RM005077S, "empty");
		}
		RaAttachedFileCondition condition = new RaAttachedFileCondition();
		condition.setRaId( raId );
		List<IRaAttachedFileEntity> entities = this.getRaAttachedFileDao().find( condition.getCondition() );

		return entities;
	}

	/**
	 * リリース申請・ビルドパッケージを取得します。
	 *
	 * @param raId リリース申請ID
	 * @return 取得したリリース申請・ビルドパッケージエンティティを戻します。
	 */
	private final List<IRaBpLnkEntity> findRaBpLnkEntities( String raId ) {
		if ( TriStringUtils.isEmpty(raId) ){
			throw new TriSystemException(RmMessageId.RM005077S, "empty");
		}
		RaBpLnkCondition condition = new RaBpLnkCondition();
		condition.setRaId( raId );
		List<IRaBpLnkEntity> entities = this.getRaBpLnkDao().find( condition.getCondition() );

		return entities;
	}

	/**
	 * リリース申請・リリースパッケージを取得します。
	 *
	 * @param raId リリース申請ID
	 * @return 取得したリリース申請・リリースパッケージエンティティを戻します。
	 */
	private final List<IRaRpLnkEntity> findRaRpLnkEntities( String raId ) {

		if ( TriStringUtils.isEmpty(raId) ){
			throw new TriSystemException(RmMessageId.RM005077S, "empty");
		}
		RaRpLnkCondition condition = new RaRpLnkCondition();
		condition.setRaId( raId );
		List<IRaRpLnkEntity> entities = this.getRaRpLnkDao().find( condition.getCondition() );

		return entities;
	}

	@Override
	public IRaDto findRaDto( String raId, RmTables... tables ) throws TriSystemException {
		IRaEntity raEntity = findRaEntity( raId );
		return this.findRaDto( raEntity, tables );
	}

	@Override
	public List<IRaDto> findRaDto( String[] raIds, RmTables... tables ) {
		List<IRaEntity> raEntities = findRaEntities( raIds );

		return this.findRaDto( raEntities, tables );
	}

	@Override
	public IRaDto findRaDto( IRaEntity raEntity, RmTables... tables ) {
		return this.setRaDto(new RaDto(), raEntity, tables);
	}

	@Override
	public List<IRaDto> findRaDto( ISqlCondition condition, RmTables... tables ) {
		List<IRaDto> rpDtoList = new ArrayList<IRaDto>();

		List<IRaEntity> raEntities = this.getRaDao().find( condition );

		for ( IRaEntity raEntity: raEntities ) {
			rpDtoList.add( this.findRaDto( raEntity, tables ) );
		}

		return rpDtoList;
	}

	@Override
	public List<IRaDto> findRaDto( List<IRaEntity> raEntities, RmTables... tables ) {
		List<IRaDto> rpDtoList = new ArrayList<IRaDto>();

		for ( IRaEntity raEntity : raEntities ) {
			rpDtoList.add( this.findRaDto( raEntity, tables ) );
		}

		return rpDtoList;
	}

	public RaDtoList findRaDtoList( List<IRaEntity> raEntities, RmTables... tables ){

		RaDtoList raDtoList = new RaDtoList();
		for ( IRaEntity raEntity : raEntities ) {
			raDtoList.add( this.findRaDto( raEntity, tables ) );
		}
		return raDtoList;
	}

	@Override
	public IDmDoEntity findDmDoEntity(String mgtVer) {

		DmDoCondition condition = new DmDoCondition();
		condition.setMgtVer( mgtVer );

		IDmDoEntity entity = this.getDmDoDao().findByPrimaryKey( condition.getCondition() );

		return entity;
	}
	@Override
	public IDmDoEntity getDmDoEntity(IRpEntity entity ) {

		if ( TriStringUtils.isEmpty( entity.getRpId() ) ){
			throw new TriSystemException(SmMessageId.DEFAULT);
		}
		DmDoCondition condition = new DmDoCondition();
		condition.setRpId( entity.getRpId() );
		List<IDmDoEntity> dmDoEntities = this.getDmDoDao().find(condition.getCondition()) ;
		return ( 0 == dmDoEntities.size() )? null: dmDoEntities.get(0);

	}
	@Override
	public IDmDoDto getDmDoDto( String rpId ) {

		IDmDoDto dmDoDto = new DmDoDto();
		IRpEntity rpEntity = findRpEntity(rpId);
		IDmDoEntity dmDoEntity = getDmDoEntity(rpEntity);
		dmDoDto.setRpEntity(rpEntity);
		dmDoDto.setDmDoEntity(dmDoEntity);

		return dmDoDto;
	}
}
