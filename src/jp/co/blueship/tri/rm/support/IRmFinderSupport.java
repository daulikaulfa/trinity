package jp.co.blueship.tri.rm.support;

import java.util.List;

import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoDto;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * BM関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IRmFinderSupport extends IRmDaoFinder {
	/**
	 * smFinderSupportを取得します。
	 * @return smFinderSupport
	 */
	public ISmFinderSupport getSmFinderSupport();
	/**
	 * umFinderSupportを取得します。
	 * @return umFinderSupport
	 */
	public IUmFinderSupport getUmFinderSupport();
	/**
	 * amFinderSupportを取得します。
	 * @return amFinderSupport
	 */
	public IAmFinderSupport getAmFinderSupport();
	/**
	 * bmFinderSupportを取得します。
	 * @return bmFinderSupport
	 */
	public IBmFinderSupport getBmFinderSupport();
	/**
	 * dcmFinderSupportを取得します。
	 * @return dcmFinderSupport
	 */
	public IDcmFinderSupport getDcmFinderSupport();


	/**
	 * リリース申請ID（仮番号）を採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByRaTempId();

	/**
	 * リリース申請IDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByRaId();

	public List<IRaBpLnkEntity> findRaBpLnkEntitiesFromBpId( String... bpIds );

	public List<IRpBpLnkEntity> findRpBpLnkEntitiesFromBpId( String... bpIds );

	/**
	 * リリースパッケージ情報を取得する
	 * @param bpId リリースパッケージID
	 * @return 取得したリリースパッケージ情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IRpEntity findRpEntity( String rpId ) throws TriSystemException;

	/**
	 * リリースパッケージ情報を取得する
	 * @param bpIds リリースパッケージID
	 * @return 取得したリリースパッケージ情報エンティティを戻します。
	 */
	public List<IRpEntity> findRpEntities( String... rpIds );

	/**
	 * リリースパッケージ・ビルドパッケージを取得します。
	 *
	 * @param rpId リリースパッケージID
	 * @param bpId ビルドパッケージID
	 * @return 取得したリリースパッケージ・ビルドパッケージエンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IRpBpLnkEntity findRpBpLnkEntity( String rpId, String bpId );

	/**
	 * リリースパッケージDTOを取得します。
	 *
	 * @param rpId リリースパッケージID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RP_BP_LNK}、
	 * @return 取得したリリースパッケージDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IRpDto findRpDto( String rpId, RmTables... tables ) throws TriSystemException;

	/**
	 * リリースパッケージDTOのListを取得します。
	 *
	 * @param rpIds リリースパッケージID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RP_BP_LNK}、
	 * @return 取得したリリースパッケージDTOのListを戻します。
	 */
	public List<IRpDto> findRpDto( String[] rpIds, RmTables... tables );

	/**
	 * リリースパッケージからビルドパッケージDTOを取得します。
	 * @param rpEntity リリースパッケージEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RP_BP_LNK}、
	 * @return 取得したリリースパッケージDTO
	 */
	public IRpDto findRpDto( IRpEntity rpEntity, RmTables... tables );

	/**
	 * リリースパッケージDTOのListを取得します。
	 *
	 * @param condition SQL Conditon
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RP_BP_LNK}、
	 * @return 取得したリリースパッケージDTOのListを戻します。
	 */
	public List<IRpDto> findRpDto( ISqlCondition condition, RmTables... tables );

	/**
	 * リリースパッケージのリストからリリースパッケージDTOを取得します。
	 * @param rpEntities リリースパッケージEntityのリスト
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RP_BP_LNK}、
	 * @return 取得したリリースパッケージDTOのList
	 */
	public List<IRpDto> findRpDto( List<IRpEntity> rpEntities, RmTables... tables );

	/**
	 * リリース申請情報を取得する
	 * @param raId リリース申請ID
	 * @return 取得したリリース申請情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IRaEntity findRaEntity( String raId ) throws TriSystemException;

	/**
	 * リリース申請情報を取得する
	 * @param raIds リリース申請ID
	 * @return 取得したリリース申請情報エンティティを戻します。
	 */
	public List<IRaEntity> findRaEntities( String... raIds );

	/**
	 * リリース申請DTOを取得します。
	 *
	 * @param raId リリース申請ID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RA_ATTACHED_FILE}、
	 * 					{@link RmTables.RM_RA_BP_LNK}、
	 * 					{@link RmTables.RM_RA_RP_LNK}、
	 * @return 取得したリリース申請DTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IRaDto findRaDto( String raId, RmTables... tables ) throws TriSystemException;

	/**
	 * リリース申請DTOのListを取得します。
	 *
	 * @param raIds リリース申請ID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RA_ATTACHED_FILE}、
	 * 					{@link RmTables.RM_RA_BP_LNK}、
	 * 					{@link RmTables.RM_RA_RP_LNK}、
	 * @return 取得したリリース申請DTOのListを戻します。
	 */
	public List<IRaDto> findRaDto( String[] raIds, RmTables... tables );

	/**
	 * リリース申請からリリース申請DTOを取得します。
	 * @param raEntity リリースパッケージEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RA_ATTACHED_FILE}、
	 * 					{@link RmTables.RM_RA_BP_LNK}、
	 * 					{@link RmTables.RM_RA_RP_LNK}、
	 * @return 取得したリリース申請DTO
	 */
	public IRaDto findRaDto( IRaEntity raEntity, RmTables... tables );

	/**
	 * リリース申請DTOのListを取得します。
	 *
	 * @param condition SQL Conditon
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RA_ATTACHED_FILE}、
	 * 					{@link RmTables.RM_RA_BP_LNK}、
	 * 					{@link RmTables.RM_RA_RP_LNK}、
	 * @return 取得したリリース申請DTOのListを戻します。
	 */
	public List<IRaDto> findRaDto( ISqlCondition condition, RmTables... tables );

	/**
	 * リリース申請のリストからビルドパッケージDTOを取得します。
	 * @param raEntities リリース申請Entityのリスト
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link RmTables.RM_RA_ATTACHED_FILE}、
	 * 					{@link RmTables.RM_RA_BP_LNK}、
	 * 					{@link RmTables.RM_RA_RP_LNK}、
	 * @return 取得したリリース申請DTOのList
	 */
	public List<IRaDto> findRaDto( List<IRaEntity> raEntities, RmTables... tables );

	/**
	 * デプロイメント連係情報を取得します。<br>
	 * <br>
	 * get deployment entity by using management version<br>
	 *
	 * @param mgtVer management Version
	 * @return dmDoEntity
	 */
	public IDmDoEntity findDmDoEntity(String mgtVer );
	/**
	 * デプロイメント連係情報を取得します。
	 * RpIdは主キーではありませんが、dmDoEntityは一意に決まります。
	 *
	 * @param entity リリースパッケージエンティティ
	 * @return dmDoEntity デプロイメント連係情報エンティティ
	 */
	public IDmDoEntity getDmDoEntity(IRpEntity entity );
	/**
	 * デプロイメント連携DTOを取得します。
	 * @param rpId
	 * @return
	 */
	public IDmDoDto getDmDoDto( String rpId );

}
