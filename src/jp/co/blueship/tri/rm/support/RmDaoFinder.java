package jp.co.blueship.tri.rm.support;

import jp.co.blueship.tri.dm.dao.dmdo.IDmDoDao;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.um.dao.hist.IDmDoHistDao;
import jp.co.blueship.tri.rm.dao.cal.ICalDao;
import jp.co.blueship.tri.rm.dao.ra.IRaAttachedFileDao;
import jp.co.blueship.tri.rm.dao.ra.IRaBpLnkDao;
import jp.co.blueship.tri.rm.dao.ra.IRaDao;
import jp.co.blueship.tri.rm.dao.ra.IRaRpLnkDao;
import jp.co.blueship.tri.rm.dao.ra.RaNumberingDao;
import jp.co.blueship.tri.rm.dao.ra.RaTempNumberingDao;
import jp.co.blueship.tri.rm.dao.rp.IRpBpLnkDao;
import jp.co.blueship.tri.rm.dao.rp.IRpDao;

/**
 * RM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public abstract class RmDaoFinder extends FinderSupport implements IRmDaoFinder {
	private RaTempNumberingDao raTempNumberingDao = null;
	private RaNumberingDao raNumberingDao = null;

	private IRaDao raDao = null;
	private IRaBpLnkDao raBpLnkDao = null;
	private IRaRpLnkDao raRpLnkDao = null;
	private IRaAttachedFileDao raAttachedFileDao = null;
	private ICalDao calDao = null;

	private IRpDao rpDao = null;
	private IRpBpLnkDao rpBpLnkDao = null;

	private IDmDoDao dmDoDao = null;

	private IDmDoHistDao dmDoHistDao = null;

	public RaTempNumberingDao getRaTempNumberingDao() {
		return raTempNumberingDao;
	}

	public void setRaTempNumberingDao(RaTempNumberingDao raTempNumberingDao) {
		this.raTempNumberingDao = raTempNumberingDao;
	}

	public RaNumberingDao getRaNumberingDao() {
		return raNumberingDao;
	}

	public void setRaNumberingDao(RaNumberingDao raNumberingDao) {
		this.raNumberingDao = raNumberingDao;
	}

	@Override
	public IRaDao getRaDao() {
	    return raDao;
	}

	/**
	 * raDaoを設定します。
	 * @param raDao raDao
	 */
	public void setRaDao(IRaDao raDao) {
	    this.raDao = raDao;
	}

	/**
	 * raBpLnkDaoを取得します。
	 * @return raBpLnkDao
	 */
	public IRaBpLnkDao getRaBpLnkDao() {
	    return raBpLnkDao;
	}

	/**
	 * raBpLnkDaoを設定します。
	 * @param raBpLnkDao raBpLnkDao
	 */
	public void setRaBpLnkDao(IRaBpLnkDao raBpLnkDao) {
	    this.raBpLnkDao = raBpLnkDao;
	}

	/**
	 * raRpLnkDaoを取得します。
	 * @return raRpLnkDao
	 */
	public IRaRpLnkDao getRaRpLnkDao() {
	    return raRpLnkDao;
	}

	/**
	 * raRpLnkDaoを設定します。
	 * @param raRpLnkDao raRpLnkDao
	 */
	public void setRaRpLnkDao(IRaRpLnkDao raRpLnkDao) {
	    this.raRpLnkDao = raRpLnkDao;
	}

	/**
	 * raAttachedFileDaoを取得します。
	 * @return raAttachedFileDao
	 */
	public IRaAttachedFileDao getRaAttachedFileDao() {
	    return raAttachedFileDao;
	}

	/**
	 * raAttachedFileDaoを設定します。
	 * @param raAttachedFileDao raAttachedFileDao
	 */
	public void setRaAttachedFileDao(IRaAttachedFileDao raAttachedFileDao) {
	    this.raAttachedFileDao = raAttachedFileDao;
	}

	/**
	 * calDaoを取得します。
	 * @return calDao
	 */
	public ICalDao getCalDao() {
	    return calDao;
	}

	/**
	 * calDaoを設定します。
	 * @param calDao calDao
	 */
	public void setCalDao(ICalDao calDao) {
	    this.calDao = calDao;
	}

	@Override
	public IRpDao getRpDao() {
	    return rpDao;
	}

	/**
	 * rpDaoを設定します。
	 * @param rpDao rpDao
	 */
	public void setRpDao(IRpDao rpDao) {
	    this.rpDao = rpDao;
	}

	@Override
	public IRpBpLnkDao getRpBpLnkDao() {
	    return rpBpLnkDao;
	}

	/**
	 * rpBpLnkDaoを設定します。
	 * @param rpBpLnkDao rpBpLnkDao
	 */
	public void setRpBpLnkDao(IRpBpLnkDao rpBpLnkDao) {
	    this.rpBpLnkDao = rpBpLnkDao;
	}

	/**
	 * dmDoDaoを取得します。
	 * @return dmDoDao
	 */
	public IDmDoDao getDmDoDao() {
	    return dmDoDao;
	}

	/**
	 * dmDoDaoを設定します。
	 * @param dmDoDao dmDoDao
	 */
	public void setDmDoDao(IDmDoDao dmDoDao) {
	    this.dmDoDao = dmDoDao;
	}

	public IDmDoHistDao getDmDoHistDao (){
		return this.dmDoHistDao;
	}

	public void setDmDoHistDao (IDmDoHistDao dmDoHistDao){
		this.dmDoHistDao = dmDoHistDao;
	}


}
