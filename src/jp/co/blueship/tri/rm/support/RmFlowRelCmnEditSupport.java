package jp.co.blueship.tri.rm.support;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotRelEnvLnkEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;


/**
 * FlowRelEntry等のリリース更新系のイベントのサポートClass
 * <br>
 * <p>
 * リリースのための業務サービス処理を支援するサポートクラスです。
 * </p>
 * 依存性解消のため,FlowRelCmnEditSupportから派生したクラスです。
 **/
public class RmFlowRelCmnEditSupport extends RmFinderSupport {

	/**
	 * 指定されたユーザがアクセス可能なロット情報を取得する
	 * @param userId ユーザID
	 * @param lotDto 絞り込み対象のロットエンティティ
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return 指定されたユーザがアクセス可能なロットエンティティ
	 */
	public List<ILotDto> getAccessableLotEntity(
			IUmFinderSupport umFinderSupport,
			String userId,
			List<ILotDto> lotDto,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers ) {
		return getAccessableLotEntity( this, umFinderSupport, userId, lotDto, isIncludeAllowList, disableLinkLotNumbers );
	}

	/**
	 * 指定されたユーザがアクセス可能なロット情報を取得する
	 * @param support データアクセス支援
	 * @param userId ユーザID
	 * @param lotDto 絞り込み対象のロットエンティティ
	 * @param isIncludeAllowList 表示のみのロットを含めるかどうか
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return 指定されたユーザがアクセス可能なロットエンティティ
	 */
	private List<ILotDto> getAccessableLotEntity(
			FinderSupport support,
			IUmFinderSupport umFinderSupport,
			String userId,
			List<ILotDto> lotDto,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers ) {

		// グループ情報
		List<IGrpUserLnkEntity> groupUserEntitys = umFinderSupport.findGrpUserLnkByUserId( userId );
		Set<String> groupIdSet = RmEntityAddonUtils.getGroupIdSet( groupUserEntitys );

		List<ILotDto> accessableLotList = new ArrayList<ILotDto>();
		List<String> accessableLotNumbers = new ArrayList<String>();

		for ( ILotDto lot : lotDto ) {

			if ( TriStringUtils.isEmpty( lot.getLotGrpLnkEntities() )) {

				accessableLotList.add( lot );
				accessableLotNumbers.add( lot.getLotEntity().getLotId() );

			} else {

				for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

					if ( groupIdSet.contains( group.getGrpId() )) {

						accessableLotList.add( lot );
						accessableLotNumbers.add( lot.getLotEntity().getLotId() );
						break;
					}
				}
			}
			// 一覧表示許可あり
			if ( isIncludeAllowList ) {
				if ( !accessableLotNumbers.contains( lot.getLotEntity().getLotId() ) && StatusFlg.on.value().equals( lot.getLotEntity().getAllowListView().value() ) ) {

					accessableLotList.add( lot );
					accessableLotNumbers.add( lot.getLotEntity().getLotId() );
					disableLinkLotNumbers.add( lot.getLotEntity().getLotId() );
				}
			}
		}

		return accessableLotList;
	}

	/**
	 * ロット情報エンティティからアクセス可能なリリース環境エンティティの配列を取得する
	 * <br>旧データ互換のため、ロットにリリース環境が設定されてないデータでも動作を保障する。
	 * <br>ロットにリリース環境が設定されていない場合、進行中の全リリース環境を取得する。
	 *
	 * @param envAllEntities 全リリース環境情報
	 * @param lotList
	 * @return
	 */
	public IBldEnvEntity[] getAccessableRelEnvEntity( IBldEnvEntity[] envAllEntities, List<ILotDto> lotList ) {
		Map<String, IBldEnvEntity> envMap = new HashMap<String, IBldEnvEntity>();
		List<IBldEnvEntity> envList = new ArrayList<IBldEnvEntity>();

		for ( IBldEnvEntity env : envAllEntities ) {
			envMap.put(env.getBldEnvId(), env);
		}

		for ( String envNo: convertToEnvNo( envAllEntities, lotList ) ) {
			envList.add( envMap.get( envNo ) );
		}

		return envList.toArray( new IBldEnvEntity[0] );
	}



	/**
	 * ロットの検索条件にアクセス可能なロット番号を取得する
	 *
	 * @param condition ロット検索条件
	 * @param sort ソート条件
	 * @param retBean GenericServiceBean オブジェクト
	 * @param isIncludeAllowList 表示のみのロットを含めるかどうか
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
	 * @param isSetMessage メッセージを設定するかどうか
	 * @return アクセス可能なロット
	 */
	public List<ILotEntity> getAccessableLotNumbers(
			IJdbcCondition condition,
			ISqlSort sort,
			IUmFinderSupport umFinderSupport,
			GenericServiceBean retBean,
			boolean isIncludeAllowList,
			List<String> disableLinkLotNumbers,
			boolean isSetMessage ) {

		List<ILotEntity> lotEntities = this.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort );
		if ( ! ScreenType.bussinessException.equals( retBean.getScreenType() ) ) {
			if ( isSetMessage && TriStringUtils.isEmpty( lotEntities )) {
				retBean.setInfoMessage( RmMessageId.RM001035E );
			}
		}
		List<ILotDto> lotDto = this.getAmFinderSupport().findLotDto(lotEntities);

		// ユーザが所属しているグループ
		List<String> groupIdList = new ArrayList<String>();
		List<IGrpUserLnkEntity> groupUserEntities = umFinderSupport.findGrpUserLnkByUserId( retBean.getUserId() );
		for ( IGrpUserLnkEntity groupUserEntity : groupUserEntities ) {
			groupIdList.add( groupUserEntity.getGrpId() );
		}


		List<ILotEntity> accessableLotList = new ArrayList<ILotEntity>();
		List<String> accessableLotNumbers = new ArrayList<String>();

		for ( ILotDto lot : lotDto ) {

			// アクセス許可グループ指定なし＝全員アクセス可能
			if ( TriStringUtils.isEmpty( lot.getLotGrpLnkEntities() )) {
				accessableLotList.add( lot.getLotEntity() );
				accessableLotNumbers.add( lot.getLotEntity().getLotId() );

			// グループ指定あり
			} else {
				// 自分の所属グループがロットにアクセス許可があるかチェック
				for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

					if ( groupIdList.contains( group.getGrpId() )) {
						accessableLotList.add( lot.getLotEntity() );
						accessableLotNumbers.add( lot.getLotEntity().getLotId() );
						break;
					}
				}
			}


			// 一覧表示許可あり
			if ( isIncludeAllowList ) {
				if ( !accessableLotNumbers.contains( lot.getLotEntity().getLotId() ) && StatusFlg.on.value().equals( lot.getLotEntity().getAllowListView().value() ) ) {

					accessableLotList.add( lot.getLotEntity() );
					accessableLotNumbers.add( lot.getLotEntity().getLotId() );
					disableLinkLotNumbers.add( lot.getLotEntity().getLotId() );
				}
			}
		}

		return accessableLotList;
	}

	/**
	 * ロット情報エンティティからロット番号の配列を取得する
	 *
	 * @param pjtLotEntityList
	 * @return
	 */
	public String[] convertToLotNo( List<ILotEntity> pjtLotEntityList ) {
		Set<String> lotNoSet = new LinkedHashSet<String>();

		for ( ILotEntity lot : pjtLotEntityList ) {
			lotNoSet.add( lot.getLotId() );
		}

		return lotNoSet.toArray( new String[0] );
	}
	/**
	 * ロット情報エンティティからロット番号の配列を取得する
	 *
	 * @param lotList
	 * @return
	 */
	public Map<String, ILotEntity> convertToLotMap( List<ILotDto> lotList ) {
		Map<String, ILotEntity> lotMap = new LinkedHashMap<String, ILotEntity>();

		for ( ILotDto lot : lotList ) {
			lotMap.put(lot.getLotEntity().getLotId(), lot.getLotEntity());
		}

		return lotMap;
	}

	/**
	 * ロット情報エンティティからリリース環境番号の配列を取得する
	 *
	 * @param envAllEntities
	 * @param lotList
	 * @return
	 */
	public String[] convertToEnvNo( IBldEnvEntity[] envAllEntities, List<ILotDto> lotList ) {
		Set<String> envNo = new HashSet<String>();
		List<String> envNoList = new ArrayList<String>();

		String[] envArray = convertToEnvNo( envAllEntities );

		for ( ILotDto lot : lotList ) {
			if ( TriStringUtils.isEmpty(lot.getLotRelEnvLnkEntities()) ) {
				envNo.addAll( FluentList.from(envArray).asList());
			} else {
				for ( ILotRelEnvLnkEntity env : lot.getLotRelEnvLnkEntities() ) {
					envNo.add( env.getBldEnvId() );
				}
			}
		}

		//ソート順を保障する
		for ( IBldEnvEntity env: envAllEntities ) {
			if ( envNo.contains( env.getBldEnvId() ) ) {
				envNoList.add( env.getBldEnvId() );
			}
		}

		return envNoList.toArray( new String[0] );
	}
	/**
	 * ロット情報エンティティからロット番号の配列を取得する
	 *
	 * @param envAllEntities 全リリース環境情報
	 * @param lotList
	 * @return
	 */
	private static String[] convertToEnvNo( IBldEnvEntity[] envEntities ) {
		Set<String> envNo = new LinkedHashSet<String>();

		//ソート順を保障する
		for ( IBldEnvEntity env : envEntities ) {
			envNo.add( env.getBldEnvId() );
		}

		return envNo.toArray( new String[0] );
	}
	/**
	 * リリース環境情報を取得する。
	 * @param envNoArray 対象となるリリース環境番号の配列
	 * @param sort ソート順
	 * @param selectedPageNo  表示するページ番号
	 * @param maxPageNumber １画面の表示件数
	 * @return 全環境情報
	 */
	//r
	public IEntityLimit<IBldEnvEntity> getRelEnvEntity( String[] envNoArray, IJdbcCondition condition, ISqlSort sort, int selectedPageNo, int maxPageNumber ) {

		BldEnvCondition innerCondition = (BldEnvCondition)condition;
		innerCondition.setBldEnvIds( envNoArray );

		if ( TriStringUtils.isEmpty( envNoArray ) ) {
			//ダミーを設定して、全件検索を防止する。
			innerCondition.setBldEnvIds( new String[]{ "" } );
		}

		IEntityLimit<IBldEnvEntity> limit =
			this.getBmFinderSupport().getBldEnvDao().find( innerCondition.getCondition(), sort, selectedPageNo, maxPageNumber );

		return limit;
	}

	/**
	 * 一覧表示のみ可能なリリース環境番号を取得する。
	 *
	 * @param relEnvEntities チェック対象となるリリース環境の配列
	 * @param accessableLotList アクセス可能なロットリスト
	 * @param disableLinkLotNumbers 一覧表示のみ可能なロット番号リスト
	 * @return
	 */
	public List<String> getDisableLinkEnvNumbers( IBldEnvEntity[] relEnvEntities, List<ILotDto> accessableLotList, List<String> disableLinkLotNumbers ) {
		List<String> disableLinkEnvNumbers = new ArrayList<String>();

		Map<String, Boolean> envMap = new HashMap<String, Boolean>();

		for ( ILotDto lotEntity : accessableLotList ) {
			boolean isAccessable = ! disableLinkLotNumbers.contains( lotEntity.getLotEntity().getLotId() );

			if ( TriStringUtils.isEmpty( lotEntity.getLotRelEnvLnkEntities() ) ) {
				for ( IBldEnvEntity env : relEnvEntities ) {
					if ( envMap.containsKey( env.getBldEnvId() ) && envMap.get( env.getBldEnvId() ) ) {
						continue;
					}

					envMap.put( env.getBldEnvId(), isAccessable );
				}
			} else {
				for ( ILotRelEnvLnkEntity env : lotEntity.getLotRelEnvLnkEntities() ) {
					if ( envMap.containsKey( env.getBldEnvId() ) && envMap.get( env.getBldEnvId() ) ) {
						continue;
					}
					envMap.put( env.getBldEnvId(), isAccessable );
				}

			}
		}

		for ( IBldEnvEntity env: relEnvEntities ) {
			if ( envMap.containsKey( env.getBldEnvId() ) && envMap.get( env.getBldEnvId() ) ) {
				continue;
			}

			disableLinkEnvNumbers.add( env.getBldEnvId() );
		}

		return disableLinkEnvNumbers;
	}
}
