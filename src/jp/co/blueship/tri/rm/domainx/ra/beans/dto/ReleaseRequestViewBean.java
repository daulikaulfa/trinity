package jp.co.blueship.tri.rm.domainx.ra.beans.dto;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ReleaseRequestViewBean {
	private String raId;
	private String bpId;
	private String envId;
	private String envNm;
	private String submitterNm;
	private String submitterIconPath;

	public String getRaId() {
		return raId;
	}
	public ReleaseRequestViewBean setRaId(String raId) {
		this.raId = raId;
		return this;
	}

	public String getBpId() {
		return bpId;
	}
	public ReleaseRequestViewBean setBpId(String bpId) {
		this.bpId = bpId;
		return this;
	}

	public String getEnvId() {
		return envId;
	}
	public ReleaseRequestViewBean setEnvId(String envId) {
		this.envId = envId;
		return this;
	}

	public String getEnvNm() {
		return envNm;
	}
	public ReleaseRequestViewBean setEnvNm(String envNm) {
		this.envNm = envNm;
		return this;
	}

	public String getSubmitterNm() {
		return submitterNm;
	}
	public ReleaseRequestViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}
	public ReleaseRequestViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}

}
