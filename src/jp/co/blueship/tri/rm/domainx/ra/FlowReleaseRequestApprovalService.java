package jp.co.blueship.tri.rm.domainx.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestApprovalService implements IDomain<FlowReleaseRequestApprovalServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovalServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovalServiceBean> serviceDto) {

		FlowReleaseRequestApprovalServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}

	}

	public void submitChanges(FlowReleaseRequestApprovalServiceBean serviceBean) {

		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		// Status Matrix Check
		{
			List<IRaDto> raDtoList = new ArrayList<IRaDto>();
			raDtoList.add( this.support.findRaDto( raEntity ) );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( raEntity.getLotId() )
					.setBpIds		( support.getBpIdsFromRaDto( raDtoList ) )
					.setRaIds		( raEntity.getRaId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		raEntity.setStsId(RmRaStatusId.ReleaseRequestApproved.getStatusId());
		raEntity.setAvlUserId(serviceBean.getUserId());
		raEntity.setAvlUserNm(serviceBean.getUserName());
		raEntity.setAvlTimestamp(TriDateUtils.getSystemTimestamp());
		this.support.getRaDao().update(raEntity);

		this.support.getSmFinderSupport().cleaningExecDataSts(RmTables.RM_RA, raId);

		//Recorded history
		if ( DesignSheetUtils.isRecord() ) {
			IRaDto raDto = support.findRaDto(raEntity.getRaId());

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.Approve.getStatusId() );
			((FinderSupport)support).getRaHistDao().insert( histEntity , raDto);
		}
		
		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(RmMessageId.RM003021I);
	}

}
