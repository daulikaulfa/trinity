package jp.co.blueship.tri.rm.domainx.ra.beans.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ReleaseRequestDetailsViewBean {
	private String submitterNm;
	private String submitterIconPath;
	private String submitterGroup;
	private String supervisor;
	private String supervisorIcon;
	private String relEnvNm;
	private String preferredDate;
	private String bpId;
	private String relatedId;
	private String remarks;
	private List<String> attachmentFileNms = new ArrayList<String>();
	private String ctgNm;
	private String mstoneNm;
	private String approvedDate;

	public String getSubmitterNm() {
		return submitterNm;
	}

	public ReleaseRequestDetailsViewBean setSubmitterNm(String submitterNm) {
		this.submitterNm = submitterNm;
		return this;
	}

	public String getSubmitterIconPath() {
		return submitterIconPath;
	}

	public ReleaseRequestDetailsViewBean setSubmitterIconPath(String submitterIconPath) {
		this.submitterIconPath = submitterIconPath;
		return this;
	}

	public String getSubmitterGroup() {
		return submitterGroup;
	}

	public ReleaseRequestDetailsViewBean setSubmitterGroup(String submitterGroup) {
		this.submitterGroup = submitterGroup;
		return this;
	}

	public String getSupervisor() {
		return supervisor;
	}

	public ReleaseRequestDetailsViewBean setSupervisor(String supervisor) {
		this.supervisor = supervisor;
		return this;
	}

	public String getSupervisorIcon() {
		return supervisorIcon;
	}

	public ReleaseRequestDetailsViewBean setSupervisorIcon(String supervisorIcon) {
		this.supervisorIcon = supervisorIcon;
		return this;
	}

	public String getRelEnvNm() {
		return relEnvNm;
	}

	public ReleaseRequestDetailsViewBean setRelEnvNm(String relEnvNm) {
		this.relEnvNm = relEnvNm;
		return this;
	}

	public String getPreferredDate() {
		return preferredDate;
	}

	public ReleaseRequestDetailsViewBean setPreferredDate(String preferredDate) {
		this.preferredDate = preferredDate;
		return this;
	}

	public String getBpId() {
		return bpId;
	}

	public String getRelatedId() {
		return relatedId;
	}

	public ReleaseRequestDetailsViewBean setRelatedId(String relatedId) {
		this.relatedId = relatedId;
		return this;
	}

	public ReleaseRequestDetailsViewBean setBpId(String bpId) {
		this.bpId = bpId;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}

	public ReleaseRequestDetailsViewBean setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	public List<String> getAttachmentFileNms() {
		return attachmentFileNms;
	}

	public ReleaseRequestDetailsViewBean setAttachmentFileNms(List<String> attachmentFileNms) {
		this.attachmentFileNms = attachmentFileNms;
		return this;
	}

	public String getCtgNm() {
		return ctgNm;
	}

	public ReleaseRequestDetailsViewBean setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
		return this;
	}

	public String getMstoneNm() {
		return mstoneNm;
	}

	public ReleaseRequestDetailsViewBean setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
		return this;
	}

	public String getApprovedDate() {
		return approvedDate;
	}

	public ReleaseRequestDetailsViewBean setApprovedDate(String approvedDate) {
		this.approvedDate = approvedDate;
		return this;
	}
}
