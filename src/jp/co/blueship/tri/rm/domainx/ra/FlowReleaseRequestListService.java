package jp.co.blueship.tri.rm.domainx.ra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.DraftReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.RequestParam;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowReleaseRequestListService implements IDomain<FlowReleaseRequestListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;

	public void setSupport (FlowRelApplyEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestListServiceBean> execute(
			IServiceDto<FlowReleaseRequestListServiceBean> serviceDto) {
		FlowReleaseRequestListServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String lotId = serviceBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "LotId is not specified");

			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType()) ) {
				this.init(serviceBean);
			}

			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType()) ) {
				this.onChange(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void init (FlowReleaseRequestListServiceBean serviceBean) {
		this.generateDropBox(serviceBean);

		this.onChange(serviceBean);
	}

	private void generateDropBox (FlowReleaseRequestListServiceBean serviceBean) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		SearchCondition searchDraftCondition = serviceBean.getParam().getSearchDraftCondition();
		String lotId = serviceBean.getParam().getSelectedLotId();

		searchDraftCondition.setReleaseEnvViews( this.getReleaseEnvLabels() );
		searchCondition.setReleaseEnvViews( this.getReleaseEnvLabels() );

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchDraftCondition.setCtgViews( FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );
		searchCondition.setCtgViews( searchDraftCondition.getCtgViews() );


		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchDraftCondition.setMstoneViews( FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );
		searchCondition.setMstoneViews( searchDraftCondition.getMstoneViews() );

		TriPropertyUtils.copyProperties(searchCondition, searchDraftCondition);

		{
			List<IStatusId> relApplyStatus = new ArrayList<IStatusId>();
			relApplyStatus.add(RmRaStatusId.ReleaseRequested);
			relApplyStatus.add(RmRaStatusIdForExecData.ReturnToPendingReleaseRequest);
			relApplyStatus.add(RmRaStatusIdForExecData.ReleaseRequestCancelled);
			relApplyStatus.add(RmRaStatusId.ReleaseRequestApproved);
			relApplyStatus.add(RmRaStatusIdForExecData.ReleaseRequestRejected);
			relApplyStatus.add(RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled);
			relApplyStatus.add(RmRaStatusId.ReleaseRequestClosed);
			relApplyStatus.add(RmRaStatusIdForExecData.CreatingReleasePackage);
			relApplyStatus.add(RmRaStatusId.ReleasePackageCreated);
			relApplyStatus.add(RmRaStatusIdForExecData.ReleasePackageError);
			relApplyStatus.add(RmRaStatusId.ReleasePackageRemoved);
					
			searchCondition.setStatusViews(FluentList.from(relApplyStatus).map(RmFluentFunctionUtils.toItemLabelsFromStatusId).asList());
		}
	}

	private List<ItemLabelsBean> getReleaseEnvLabels() {

		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Asc, 1);

		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		return FluentList.from( envList ).map(
				new TriFunction<IBldEnvEntity, ItemLabelsBean>() {
					@Override
					public ItemLabelsBean apply(IBldEnvEntity input) {
						return new ItemLabelsBean(input.getBldEnvNm() , input.getBldEnvId());
					}}
				).asList();
	}

	private void onChange (FlowReleaseRequestListServiceBean serviceBean) {
		ISqlSort sort = new SortBuilder();
		sort.setElement(RaItems.raId, TriSortOrder.Desc, 1);

		SearchCondition searchCondition = (serviceBean.getParam().isSelectedDraft()) ?
				serviceBean.getParam().getSearchDraftCondition() : serviceBean.getParam().getSearchCondition();

		int linesPerPage = serviceBean.getParam().getLinesPerPage();
		int pageNo = searchCondition.getSelectedPageNo();

		IEntityLimit<IRaEntity> limit = support.getRaDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				pageNo,
				linesPerPage);

		IPageNoInfo page = RmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());

		if (serviceBean.getParam().isSelectedDraft()) {
			serviceBean.setDraftPage(page);
			serviceBean.setDraftReleaseRequestViews( getDraftReleaseRequestView (serviceBean, limit.getEntities()));

		} else {
			serviceBean.setPage(page);
			serviceBean.setReleaseRequestViews( getReleaseRequestView (serviceBean, limit.getEntities()));
		}
	}

	private RaCondition getCondition( FlowReleaseRequestListServiceBean serviceBean ) {
		RequestParam param = serviceBean.getParam();
		String lotId = param.getSelectedLotId();

		SearchCondition searchCondition = (param.isSelectedDraft()) ? param.getSearchDraftCondition() : param.getSearchCondition();
		RaCondition condition = new RaCondition();

		condition.setLotId(lotId);

		if ( TriStringUtils.isNotEmpty(searchCondition.getBldEnvId())) {
			condition.setBldEnvId(searchCondition.getBldEnvId());
		}

		if (param.isSelectedDraft()) {
			condition.setProcStsIds( new String[] {RmRaStatusId.DraftReleaseRequest.getStatusId()} );
		} else {
			if ( TriStringUtils.isNotEmpty(searchCondition.getStsId()) ) {
				condition.setProcStsIds( new String[] { searchCondition.getStsId() } );
			} else {
				condition.setProcStsIdsByNotEquals( new String[] {RmRaStatusId.DraftReleaseRequest.getStatusId()} );;
			}
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getCtgId()) ) {
			condition.setCtgId(searchCondition.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getMstoneId()) ) {
			condition.setMstoneId(searchCondition.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getKeyword())) {
			condition.setContainsByKeyword(searchCondition.getKeyword());
		}

		return condition;
	}

	private List<DraftReleaseRequestView> getDraftReleaseRequestView
											(FlowReleaseRequestListServiceBean serviceBean,
											List<IRaEntity> raList) {
		List<DraftReleaseRequestView> viewList = new ArrayList<DraftReleaseRequestView>();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone());

		for (IRaEntity entity : raList) {
			IRaDto raDto = this.support.findRaDto(entity, RmTables.RM_RA_BP_LNK);
			List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();

			DraftReleaseRequestView view = serviceBean.new DraftReleaseRequestView()
				.setPreferredDate	( entity.getPreferredRelDate())
				.setSavedDateTime	( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMDHM) );

			view.setRaId			( entity.getRaId() )
				.setBpId			( raBpLnkEntity.get(0).getBpId() )
				.setEnvId			( entity.getBldEnvId() )
				.setEnvNm			( entity.getBldEnvNm())
				.setSubmitterNm		( entity.getReqUser() )
				.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath(entity.getReqUserId()) )
			;

			viewList.add(view);
		}

		return viewList;
	}

	private List<ReleaseRequestView> getReleaseRequestView
											(FlowReleaseRequestListServiceBean serviceBean,
											List<IRaEntity> raList) {
		List<ReleaseRequestView> viewList = new ArrayList<ReleaseRequestView>();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone());

		for (IRaEntity entity : raList) {
			IRaDto raDto = this.support.findRaDto(entity, RmTables.RM_RA_BP_LNK);
			List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();

			ReleaseRequestView view = serviceBean.new ReleaseRequestView()
				.setRequestDate		( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMDHM) )
				.setStsId			( entity.getProcStsId() )
				.setStatus			( sheet.getValue(RmDesignBeanId.statusId, entity.getProcStsId()) );

			view.setRaId			( entity.getRaId() )
				.setBpId			( raBpLnkEntity.get(0).getBpId() )
				.setEnvId			( entity.getBldEnvId() )
				.setEnvNm			( entity.getBldEnvNm() )
				.setSubmitterNm		( entity.getReqUser() )
				.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath(entity.getReqUserId()) )
			;

			viewList.add(view);
		}

		return viewList;
	}
}
