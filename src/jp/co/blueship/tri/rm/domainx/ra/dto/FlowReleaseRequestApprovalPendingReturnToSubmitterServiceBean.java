package jp.co.blueship.tri.rm.domainx.ra.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestCompletion result = new RequestCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestApprovalPendingReturnToSubmitterServiceBean setResult(RequestCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;

		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId (String raId) {
			this.raId = raId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestCompletion setCompleted (boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
