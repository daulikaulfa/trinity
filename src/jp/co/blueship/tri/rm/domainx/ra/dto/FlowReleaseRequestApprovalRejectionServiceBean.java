package jp.co.blueship.tri.rm.domainx.ra.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowReleaseRequestApprovalRejectionServiceBean extends DomainServiceBean {
	
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestApprovalRejectionServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	
	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;

		public String getSelectedRaId() {
			return raId;
		}

		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}
	}
	
	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
