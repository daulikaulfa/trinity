package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestAttachmentDownloadServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestAttachmentDownloadService implements IDomain<FlowReleaseRequestAttachmentDownloadServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestAttachmentDownloadServiceBean> execute(
			IServiceDto<FlowReleaseRequestAttachmentDownloadServiceBean> serviceDto) {

		FlowReleaseRequestAttachmentDownloadServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(null != serviceBean, "ServiceBean is not specified");

			if (RequestType.onChange.equals( serviceBean.getParam().getRequestType()) ) {
				String raId = serviceBean.getParam().getSelectedRaId();
				PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");
				this.onChange(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void onChange (FlowReleaseRequestAttachmentDownloadServiceBean serviceBean) {
		String raId = serviceBean.getParam().getSelectedRaId();
		String selectedFileNm = serviceBean.getParam().getSelectedFileNm();

		IRaDto raDto = this.support.findRaDto(raId, RmTables.RM_RA_ATTACHED_FILE);

		IRaAttachedFileEntity selectedFileEntity = null;
		for (IRaAttachedFileEntity fileEntity : raDto.getRaAttachedFileEntities()) {
			if (fileEntity.getFilePath().equals(selectedFileNm)) {
				selectedFileEntity = fileEntity;
			}
		}

		if (selectedFileEntity == null) {
			serviceBean.getResult().setCompleted(false);
			return;
		}

		File attachmentFilePath = RmDesignBusinessRuleUtils.getAttachmentFilePath(raId, selectedFileNm, selectedFileEntity.getAttachedFileSeqNo());

		serviceBean.getFileResponse().setFile(attachmentFilePath);
		serviceBean.getResult().setCompleted(true);
	}
}
