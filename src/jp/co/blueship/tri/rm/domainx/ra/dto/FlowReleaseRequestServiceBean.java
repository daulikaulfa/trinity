package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleaseRequestServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleaseRequestView releaseView = new ReleaseRequestView();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowRelApplyEntryServiceBean());
	}
	public RequestParam getParam() {
		return param;
	}

	public ReleaseRequestView getReleaseView() {
		return releaseView;
	}
	public FlowReleaseRequestServiceBean setReleaseView(ReleaseRequestView releaseView) {
		this.releaseView = releaseView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ReleaseRequestEditInputBean inputInfo = new ReleaseRequestEditInputBean();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public ReleaseRequestEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ReleaseRequestEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

	}
	
	/**
	 * Class to show already appended files for Dropzone.(In case if we get an error during submit time).
	 */
	public class ReleaseRequestView {
		private List<ReleaseRequestAttachmentFile> attachmentFiles = new ArrayList<ReleaseRequestAttachmentFile>();

		public List<ReleaseRequestAttachmentFile> getAttachmentFiles() {
			return attachmentFiles;
		}

		public ReleaseRequestView setAttachmentFiles(List<ReleaseRequestAttachmentFile> attachmentFiles) {
			this.attachmentFiles = attachmentFiles;
			return this;
		}

		/**
		 * Class for already added files to show them in Dropzone.
		 */
		public class ReleaseRequestAttachmentFile {
			private String name;
			private String seqNo;
			private long size;

			public String getName() {
				return name;
			}
			public ReleaseRequestAttachmentFile setName(String fileNm) {
				this.name = fileNm;
				return this;
			}

			public String getSeqNo() {
				return seqNo;
			}
			public ReleaseRequestAttachmentFile setSeqNo(String seqNo) {
				this.seqNo = seqNo;
				return this;
			}
			
			public long getSize() {
				return size;
			}
			public ReleaseRequestAttachmentFile setSize(long size) {
				this.size = size;
				return this;
			}
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		fileUpload( "fileUpload" ),
		deleteUplodedFile( "deleteFile" ),
		selectSubmitter( "selectSubmitter" ),
		selectSuperVisor( "selectSuperVisor" ),
		selectGroup( "selectGroup" ),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone"),
		selectBpId("selectBpId")
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String raId;
		private boolean completed = false;

		public String getRaId() {
			return raId;
		}
		public RequestsCompletion setRaId(String raId) {
			this.raId = raId;
			return this;
		}
		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

	}
}
