package jp.co.blueship.tri.rm.domainx.ra.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowReleaseRequestCloseServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowRelApplyCloseServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestCloseServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;
		private ReleaseRequestCloseInputInfo inputInfo = new ReleaseRequestCloseInputInfo();
		
		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}
		
		public ReleaseRequestCloseInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ReleaseRequestCloseInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}
	
	public class ReleaseRequestCloseInputInfo {
		private String comment = null;

		public String getComment() {
			return comment;
		}

		public ReleaseRequestCloseInputInfo setComment(String comment) {
			this.comment = comment;
			return this;
		}
	}
	
	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
