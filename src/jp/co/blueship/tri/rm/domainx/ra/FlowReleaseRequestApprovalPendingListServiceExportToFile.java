package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.PendingReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceExportToFileBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestApprovalPendingListServiceExportToFile implements IDomain<FlowReleaseRequestApprovalPendingListServiceExportToFileBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> releaseRequestPendingList = null;

	public void setReleaseRequestPendingList(IDomain<IGeneralServiceBean> releaseRequestPendingList) {
		this.releaseRequestPendingList = releaseRequestPendingList;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovalPendingListServiceExportToFileBean> execute(
			IServiceDto<FlowReleaseRequestApprovalPendingListServiceExportToFileBean> serviceDto) {
		FlowReleaseRequestApprovalPendingListServiceExportToFileBean serviceBean = serviceDto.getServiceBean();
		FlowReleaseRequestApprovalPendingListServiceBean innerServiceBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");
			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType()) ) {
				this.onChange(serviceBean);
			}
			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerServiceBean);
		}
	}

	private void onChange(FlowReleaseRequestApprovalPendingListServiceExportToFileBean serviceBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowReleaseRequestApprovalPendingListServiceBean innerService = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerService, serviceBean);
		dto.setServiceBean(innerService);

		{
			innerService.getParam().setRequestType(RequestType.init);
			innerService.getParam().setSelectedLotId(serviceBean.getParam().getLotId());
			releaseRequestPendingList.execute(dto);
		}

		{
			innerService.getParam().setRequestType(RequestType.onChange);
			this.beforeExecution(serviceBean, innerService);
			releaseRequestPendingList.execute(dto);
			this.afterExecution(innerService, serviceBean);
		}
	}

	private void beforeExecution (FlowReleaseRequestApprovalPendingListServiceExportToFileBean src,
									FlowReleaseRequestApprovalPendingListServiceBean dest) {

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			SearchCondition srcSearchCondtion = src.getParam().getSearchCondition();
			SearchCondition destSearchCondtion = dest.getParam().getSearchCondition();

			destSearchCondtion.setBldEnvId	( srcSearchCondtion.getBldEnvId() );
			destSearchCondtion.setCtgId		( srcSearchCondtion.getCtgId() );
			destSearchCondtion.setMstoneId	( srcSearchCondtion.getMstoneId() );
			destSearchCondtion.setKeyword	( srcSearchCondtion.getKeyword() );
			destSearchCondtion.setSelectedPageNo(1);
		}
	}

	private void afterExecution (FlowReleaseRequestApprovalPendingListServiceBean src,
									FlowReleaseRequestApprovalPendingListServiceExportToFileBean dest) {

		if (RequestType.onChange.equals(dest.getParam().getRequestType())) {
			List<PendingReleaseRequestView> srcViews = src.getReleaseRequestViews();
			List<PendingReleaseRequestView> destViews = new ArrayList<PendingReleaseRequestView>();

			for (int i = 0; i < srcViews.size(); i++) {
				PendingReleaseRequestView view = srcViews.get(i);
				destViews.add(view);
			}
			dest.setReleaseRequestViews(destViews);
		}
	}

	private void submitChanges(FlowReleaseRequestApprovalPendingListServiceExportToFileBean serviceBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = serviceBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(serviceBean, serviceBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(serviceBean, serviceBean.getParam().getExportBean());
		}

		serviceBean.getFileResponse().setFile(file);
	}

}
