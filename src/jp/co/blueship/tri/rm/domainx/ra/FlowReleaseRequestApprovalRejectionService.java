package jp.co.blueship.tri.rm.domainx.ra;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalRejectionServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestApprovalRejectionService implements IDomain<FlowReleaseRequestApprovalRejectionServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovalRejectionServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovalRejectionServiceBean> serviceDto) {

		FlowReleaseRequestApprovalRejectionServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void submitChanges(FlowReleaseRequestApprovalRejectionServiceBean serviceBean) {

		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		// Status Matrix Check
		{
			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( raEntity.getLotId() )
					.setRaIds		( raEntity.getRaId() )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		raEntity.setStsId(RmRaStatusId.ReleaseRequestRemoved.getStatusId());
		this.support.getRaDao().update(raEntity);

		this.support.getSmFinderSupport().cleaningExecDataSts(RmTables.RM_RA, raId);
		this.support.getSmFinderSupport().registerExecDataSts(
				serviceBean.getProcId(),
				RmTables.RM_RA,
				RmRaStatusIdForExecData.ReleaseRequestRejected,
				raId);
		
		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(RmMessageId.RM003020I, raId);
	}
}
