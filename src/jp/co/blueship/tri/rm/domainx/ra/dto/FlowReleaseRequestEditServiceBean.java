package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleaseRequestEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleaseRequestDetailsView detailsView = new ReleaseRequestDetailsView();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowRelApplyModifyServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public ReleaseRequestDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowReleaseRequestEditServiceBean setDetailsView(ReleaseRequestDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;
		private ReleaseRequestEditInputBean inputInfo = new ReleaseRequestEditInputBean();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}
		public ReleaseRequestEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ReleaseRequestEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * Class to show already appended files(for Dropzone).
	 */
	public class ReleaseRequestDetailsView {
		private boolean isDraft = false;

		private List<ReleaseRequestAttachmentFiles> attachmentFiles = new ArrayList<ReleaseRequestAttachmentFiles>();

		public boolean isDraft(){
			return isDraft;
		}
		public ReleaseRequestDetailsView setIsDraft(boolean isDraft){
			this.isDraft = isDraft;
			return this;
		}

		public List<ReleaseRequestAttachmentFiles> getAttachmentFiles() {
			return attachmentFiles;
		}
		public ReleaseRequestDetailsView setAttachmentFiles(List<ReleaseRequestAttachmentFiles> attachmentFiles) {
			this.attachmentFiles = attachmentFiles;
			return this;
		}

		/**
		 * Class for already added files to show them in Dropzone.
		 */
		public class ReleaseRequestAttachmentFiles {
			private String name;
			private String seqNo;
			private long size;

			public String getName() {
				return name;
			}
			public ReleaseRequestAttachmentFiles setName(String fileNm) {
				this.name = fileNm;
				return this;
			}

			public String getSeqNo() {
				return seqNo;
			}
			public ReleaseRequestAttachmentFiles setSeqNo(String seqNo) {
				this.seqNo = seqNo;
				return this;
			}

			public long getSize() {
				return size;
			}
			public ReleaseRequestAttachmentFiles setSize(long size) {
				this.size = size;
				return this;
			}
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		fileUpload( "fileUpload" ),
		deleteUplodedFile( "deleteFile" ),
		selectSubmitter( "selectSubmitter" ),
		selectSuperVisor( "selectSuperVisor" ),
		selectGroup( "selectGroup" ),
		refreshCategory("refreshCategory"),
		refreshMilestone("refreshMilestone")
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

	}
}
