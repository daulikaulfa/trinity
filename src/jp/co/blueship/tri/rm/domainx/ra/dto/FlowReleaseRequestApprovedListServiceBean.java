package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleaseRequestApprovedListServiceBean  extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ApprovedReleaseRequestView> releaseRequestViews = new ArrayList<ApprovedReleaseRequestView>();

	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<ApprovedReleaseRequestView> getReleaseRequestViews() {
		return releaseRequestViews;
	}
	public FlowReleaseRequestApprovedListServiceBean setReleaseRequestViews(
			List<ApprovedReleaseRequestView> releaseRequestViews) {
		this.releaseRequestViews = releaseRequestViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReleaseRequestApprovedListServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 20;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter{
		@Expose private String bldEnvId = null;
		@Expose private String stsId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getBldEnvId() {
			return bldEnvId;
		}
		public SearchCondition setBldEnvId(String bldEnvId) {
			this.bldEnvId = bldEnvId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public SearchCondition setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	public class ApprovedReleaseRequestView extends jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestViewBean {
		private String requestDate;
		private String preferredDate;
		private String stsId;
		private String status;

		public String getRequestDate() {
			return requestDate;
		}
		public ApprovedReleaseRequestView setRequestDate(String requestDate) {
			this.requestDate = requestDate;
			return this;
		}

		public String getPreferredDate() {
			return preferredDate;
		}
		public ApprovedReleaseRequestView setPreferredDate(String preferredDate) {
			this.preferredDate = preferredDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ApprovedReleaseRequestView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ApprovedReleaseRequestView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

}
