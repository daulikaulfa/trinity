package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedDetailsServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedDetailsServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

public class FlowReleaseRequestApprovedDetailsService implements IDomain<FlowReleaseRequestApprovedDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovedDetailsServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovedDetailsServiceBean> serviceDto) {

		FlowReleaseRequestApprovedDetailsServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "SelectedRaId is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if (RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.onChange(serviceBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}

		return serviceDto;
	}

	private void init (FlowReleaseRequestApprovedDetailsServiceBean serviceBean) {
		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);
		
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone());

		ReleaseRequestView detailsView = serviceBean.getDetailsView();
		detailsView.setStsId				( raEntity.getProcStsId() )
					.setStatus				( sheet.getValue(RmDesignBeanId.statusId, raEntity.getProcStsId()) )
					.setSubmitterNm			( raEntity.getReqUser() )
					.setSubmitterIconPath	( this.support.getUmFinderSupport().getIconPath(raEntity.getReqUserId()) )
					.setSubmitterGroup		( raEntity.getReqGrpNm() )
					.setSupervisor			( raEntity.getReqResponsibility() )
					.setSupervisorIcon		( this.support.getUmFinderSupport().getIconPath(raEntity.getReqResponsibilityId()) )
					.setRelEnvNm			( raEntity.getBldEnvNm() )
					.setPreferredDate		( raEntity.getPreferredRelDate() )
					.setBpId				( this.getBpId(raEntity) )
					.setRelatedId			( raEntity.getRelationId() )
					.setRemarks				( raEntity.getRemarks() )
					.setAttachmentFileNms	( this.getFileNames(raEntity) )
					.setCtgNm				( this.getCtgName() )
					.setMstoneNm			( this.getMstoneName() )
					.setApprovedDate		( TriDateUtils.convertViewDateFormat(raEntity.getAvlTimestamp(), formatYMDHM) )
		;
	}

	private void onChange(FlowReleaseRequestApprovedDetailsServiceBean serviceBean) {
		String selectedFileNm = serviceBean.getParam().getSelectedFileNm();
		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
		List<IRaAttachedFileEntity> files = raDto.getRaAttachedFileEntities();
		String dir = "";
		for (IRaAttachedFileEntity file : files) {
			if(file.getFilePath().equals(selectedFileNm)) {
				dir = file.getAttachedFileSeqNo();
			}
		}

		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		String raPath = TriStringUtils.linkPathBySlash(basePath, raId);

		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( raPath , uniqueKey ) ;

		String dirPath = TriStringUtils.linkPathBySlash(uniquePath, dir);
		String filePath = TriStringUtils.linkPathBySlash(dirPath, selectedFileNm);
		File downloadFile = new File(filePath);
		serviceBean.getFileResponse().setFile(downloadFile);

	}

	private String getBpId (IRaEntity raEntity ) {
		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_BP_LNK);
		List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();

		if (raBpLnkEntity.isEmpty()) {
			return null;
		}

		return raBpLnkEntity.get(0).getBpId();
	}

	private List<String> getFileNames (IRaEntity raEntity) {
		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
		List<IRaAttachedFileEntity> files = raDto.getRaAttachedFileEntities();
		List<String> fileNames = new ArrayList<String>();
		for (IRaAttachedFileEntity file : files) {
			fileNames.add(file.getFilePath());
		}

		return (fileNames.isEmpty()) ? null : fileNames;
	}

	private String getCtgName() {
		CtgLnkCondition ctgLnkCondition = new CtgLnkCondition();
		ctgLnkCondition.setDataCtgCd(RmTables.RM_RA.name());
		List<ICtgLnkEntity> ctgLnk = this.support.getUmFinderSupport().getCtgLnkDao().find(ctgLnkCondition.getCondition());

		if ( ctgLnk.isEmpty() ) {
			return null;
		}

		ICtgEntity ctgEntity = this.support.getUmFinderSupport().findCtgByPrimaryKey(ctgLnk.get(0).getCtgId());
		return ctgEntity.getCtgNm();
	}

	private String getMstoneName() {
		MstoneLnkCondition mstoneLnkCondition = new MstoneLnkCondition();
		mstoneLnkCondition.setDataCtgCd(RmTables.RM_RA.name());
		List<IMstoneLnkEntity> mstoneLnk = this.support.getUmFinderSupport().getMstoneLnkDao().find(mstoneLnkCondition.getCondition());

		if ( mstoneLnk.isEmpty() ) {
			return null;
		}

		IMstoneEntity mstoneEntity = this.support.getUmFinderSupport().findMstoneByPrimaryKey(mstoneLnk.get(0).getMstoneId());
		return mstoneEntity.getMstoneNm();
	}
}
