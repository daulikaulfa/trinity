package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleaseRequestApprovalPendingListServiceBean  extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<PendingReleaseRequestView> releaseRequestViews = new ArrayList<PendingReleaseRequestView>();

	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<PendingReleaseRequestView> getReleaseRequestViews() {
		return releaseRequestViews;
	}
	public FlowReleaseRequestApprovalPendingListServiceBean setReleaseRequestViews(
			List<PendingReleaseRequestView> releaseRequestViews) {
		this.releaseRequestViews = releaseRequestViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReleaseRequestApprovalPendingListServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 20;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter{
		@Expose private String bldEnvId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		@Expose private String stsId = null;
		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getBldEnvId() {
			return bldEnvId;
		}
		public SearchCondition setBldEnvId(String bldEnvId) {
			this.bldEnvId = bldEnvId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public SearchCondition setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
		/**
		 * @return the statusViews
		 */
		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		/**
		 * @param statusViews the statusViews to set
		 */
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}
		/**
		 * @return the stsId
		 */
		public String getStsId() {
			return stsId;
		}
		/**
		 * @param stsId the stsId to set
		 */
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		
	}

	public class PendingReleaseRequestView extends jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestViewBean {
		private String requestDate;
		private String preferredDate;
		private String stsId;

		public String getRequestDate() {
			return requestDate;
		}
		public PendingReleaseRequestView setRequestDate(String requestDate) {
			this.requestDate = requestDate;
			return this;
		}

		public String getPreferredDate() {
			return preferredDate;
		}
		public PendingReleaseRequestView setPreferredDate(String preferredDate) {
			this.preferredDate = preferredDate;
			return this;
		}
		/**
		 * @return the stsId
		 */
		public String getStsId() {
			return stsId;
		}
		/**
		 * @param stsId the stsId to set
		 */
		public PendingReleaseRequestView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		
		
	}
}
