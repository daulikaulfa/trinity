package jp.co.blueship.tri.rm.domainx.ra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.PendingReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.RequestParam;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowReleaseRequestApprovalPendingListService implements IDomain<FlowReleaseRequestApprovalPendingListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;

	public void setSupport (FlowRelApplyEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovalPendingListServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovalPendingListServiceBean> serviceDto) {

		FlowReleaseRequestApprovalPendingListServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String lotId = serviceBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "LotId is not specified");

			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
			AmLibraryAddonUtils.checkAccessableGroup(lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.onChange(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}

	}

	private void init (FlowReleaseRequestApprovalPendingListServiceBean serviceBean) {
		this.generateDropBox(serviceBean);

		this.onChange(serviceBean);
	}

	private void generateDropBox (FlowReleaseRequestApprovalPendingListServiceBean serviceBean) {
		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		
		List<IStatusId> relApplyStatus = this.getStatusList();
		
		searchCondition.setStatusViews(FluentList.from(relApplyStatus).map(RmFluentFunctionUtils.toItemLabelsFromStatusId).asList());

		String lotId = serviceBean.getParam().getSelectedLotId();
		searchCondition.setReleaseEnvViews( this.getReleaseEnvLabels() );

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews( FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews( FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );

	}

	private List<ItemLabelsBean> getReleaseEnvLabels() {

		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Asc, 1);

		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		return FluentList.from( envList ).map(
				new TriFunction<IBldEnvEntity, ItemLabelsBean>() {
					@Override
					public ItemLabelsBean apply(IBldEnvEntity input) {
						return new ItemLabelsBean(input.getBldEnvNm() , input.getBldEnvId());
					}}
				).asList();
	}

	private void onChange (FlowReleaseRequestApprovalPendingListServiceBean serviceBean) {
		ISqlSort sort = new SortBuilder();
		sort.setElement(RaItems.raId, TriSortOrder.Desc, 1);

		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();

		int linesPerPage = serviceBean.getParam().getLinesPerPage();
		int pageNo = searchCondition.getSelectedPageNo();

		IEntityLimit<IRaEntity> limit = support.getRaDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				pageNo,
				linesPerPage);

		IPageNoInfo page = RmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		serviceBean.setPage(page);
		serviceBean.setReleaseRequestViews( getReleaseRequestView(serviceBean, limit.getEntities()) );
	}

	private RaCondition getCondition (FlowReleaseRequestApprovalPendingListServiceBean serviceBean) {
		RequestParam param = serviceBean.getParam();
		String lotId = param.getSelectedLotId();

		SearchCondition searchCondition = param.getSearchCondition();
		RaCondition raCondition = new RaCondition();

		raCondition.setLotId(lotId);

		if ( TriStringUtils.isNotEmpty(searchCondition.getBldEnvId()) ) {
			raCondition.setBldEnvId(searchCondition.getBldEnvId());
		}
		
		if ( TriStringUtils.isNotEmpty(searchCondition.getStsId())) {
			raCondition.setProcStsIds( new String[] {searchCondition.getStsId()} );
		} else {
			List<IStatusId> relApplyStatus = this.getStatusList();
			raCondition.setProcStsIds(FluentList.from(relApplyStatus).map(RmFluentFunctionUtils.toStatusIdFromStatus).toArray(new String[0]));
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getCtgId()) ) {
			raCondition.setCtgId(searchCondition.getCtgId());
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getMstoneId()) ) {
			raCondition.setMstoneId(searchCondition.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchCondition.getKeyword())) {
			raCondition.setContainsByKeyword(searchCondition.getKeyword());
		}

		return raCondition;
	}

	private List<IStatusId> getStatusList() {
		List<IStatusId> relApplyStatus = new ArrayList<IStatusId>();
		relApplyStatus.add(RmRaStatusId.ReleaseRequested);
		relApplyStatus.add(RmRaStatusIdForExecData.ReturnToPendingReleaseRequest);
		relApplyStatus.add(RmRaStatusIdForExecData.ReleaseRequestCancelled);
		relApplyStatus.add(RmRaStatusIdForExecData.CreatingReleasePackage);
		relApplyStatus.add(RmRaStatusId.ReleasePackageCreated);
		relApplyStatus.add(RmRaStatusIdForExecData.ReleasePackageError);
		relApplyStatus.add(RmRaStatusId.ReleasePackageRemoved);
		return relApplyStatus;
	}

	private List<PendingReleaseRequestView> getReleaseRequestView(
												FlowReleaseRequestApprovalPendingListServiceBean serviceBean,
												List<IRaEntity> raList) {
		List<PendingReleaseRequestView> viewList = new ArrayList<PendingReleaseRequestView>();
		SimpleDateFormat formatYMDHMS = TriDateUtils.getYMDHMSDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone());

		for (IRaEntity entity : raList) {
			IRaDto raDto = this.support.findRaDto(entity, RmTables.RM_RA_BP_LNK);
			List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();
		
			PendingReleaseRequestView view = serviceBean.new PendingReleaseRequestView()
				.setPreferredDate	( entity.getPreferredRelDate() )
				.setRequestDate		( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMDHMS) )
				.setStsId			( entity.getProcStsId() )
			;

			view.setRaId			( entity.getRaId() )
				.setBpId			( raBpLnkEntity.get(0).getBpId() )
				.setEnvId			( entity.getBldEnvId() )
				.setEnvNm			( entity.getBldEnvNm() )
				.setSubmitterNm		( entity.getReqUser() )
				.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath(entity.getReqUserId()) )
			;

			viewList.add(view);
		}

		return viewList;
	}
}
