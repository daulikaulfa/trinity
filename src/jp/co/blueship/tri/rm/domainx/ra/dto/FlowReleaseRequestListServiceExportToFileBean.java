package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.DraftReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowReleaseRequestListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReleaseRequestListFileResponse fileResponse = new ReleaseRequestListFileResponse();
	private List<ReleaseRequestView> releaseRequestViews = new ArrayList<ReleaseRequestView>();
	private List<DraftReleaseRequestView> draftReleaseRequestViews = new ArrayList<DraftReleaseRequestView>();

	{
		this.setInnerService( new FlowReleaseRequestListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReleaseRequestListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReleaseRequestListServiceExportToFileBean setFileResponse(ReleaseRequestListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<ReleaseRequestView> getReleaseRequestViews() {
		return releaseRequestViews;
	}

	public FlowReleaseRequestListServiceExportToFileBean setReleaseRequestViews(List<ReleaseRequestView> releaseRequestViews) {
		this.releaseRequestViews = releaseRequestViews;
		return this;
	}

	public List<DraftReleaseRequestView> getDraftReleaseRequestViews() {
		return draftReleaseRequestViews;
	}

	public FlowReleaseRequestListServiceExportToFileBean setDraftReleaseRequestViews(List<DraftReleaseRequestView> draftReleaseRequestViews) {
		this.draftReleaseRequestViews = draftReleaseRequestViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private boolean draft = false;
		private SearchCondition searchCondition = new FlowReleaseRequestListServiceBean().new SearchCondition(false);
		private SearchCondition searchDraftCondition = new FlowReleaseRequestListServiceBean().new SearchCondition(true);
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;
		private ExportToFileBean exportBean = null;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public boolean isSelectedDraft() {
			return draft;
		}
		public RequestParam setSelectedDraft(boolean draft) {
			this.draft = draft;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public SearchCondition getSearchDraftCondition() {
			return searchDraftCondition;
		}
		public RequestParam setSearchDraftCondition(SearchCondition searchDraftCondition) {
			this.searchDraftCondition = searchDraftCondition;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class ReleaseRequestListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
