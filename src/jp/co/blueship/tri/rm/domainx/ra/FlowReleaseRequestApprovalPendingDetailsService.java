package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingDetailsServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingDetailsServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleaseRequestApprovalPendingDetailsService implements IDomain<FlowReleaseRequestApprovalPendingDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovalPendingDetailsServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovalPendingDetailsServiceBean> serviceDto) {

		FlowReleaseRequestApprovalPendingDetailsServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "SelectedRaId is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if (RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.onChange(serviceBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}

		return serviceDto;
	}

	private void init (FlowReleaseRequestApprovalPendingDetailsServiceBean serviceBean) {
		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		ReleaseRequestView detailsView = serviceBean.getDetailsView();

		boolean isEditEnable = true;

		if( RmRaStatusId.ReleaseRequestRemoved.equals( raEntity.getStsId() )
				|| RmRaStatusId.ReleaseRequestClosed.equals( raEntity.getStsId()) ){

			isEditEnable = false;
		}

		detailsView.setIsEditEnable			( isEditEnable )
					.setStsId				( raEntity.getProcStsId() )
					.setStatus				( sheet.getValue(RmDesignBeanId.statusId, raEntity.getProcStsId()) )
					.setSubmitterNm			( raEntity.getReqUser() )
					.setSubmitterIconPath	( this.support.getUmFinderSupport().getIconPath(raEntity.getReqUserId()) )
					.setSubmitterGroup		( raEntity.getReqGrpNm() )
					.setSupervisor			( raEntity.getReqResponsibility() )
					.setSupervisorIcon		( this.support.getUmFinderSupport().getIconPath(raEntity.getReqResponsibilityId()) )
					.setRelEnvNm			( raEntity.getBldEnvNm() )
					.setPreferredDate		( raEntity.getPreferredRelDate() )
					.setBpId				( this.getBpId(raEntity) )
					.setRelatedId			( raEntity.getRelationId() )
					.setRemarks				( raEntity.getRemarks() )
					.setAttachmentFileNms	( this.getFileNames(raEntity) )
					.setCtgNm				( this.getCtgName(raEntity) )
					.setMstoneNm			( this.getMstoneName(raEntity) )
		;
	}

	private void onChange(FlowReleaseRequestApprovalPendingDetailsServiceBean serviceBean) {
		String selectedFileNm = serviceBean.getParam().getSelectedFileNm();
		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
		List<IRaAttachedFileEntity> files = raDto.getRaAttachedFileEntities();
		String dir = "";
		for (IRaAttachedFileEntity file : files) {
			if(file.getFilePath().equals(selectedFileNm)) {
				dir = file.getAttachedFileSeqNo();
			}
		}

		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		String raPath = TriStringUtils.linkPathBySlash(basePath, raId);

		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( raPath , uniqueKey ) ;

		String dirPath = TriStringUtils.linkPathBySlash(uniquePath, dir);
		String filePath = TriStringUtils.linkPathBySlash(dirPath, selectedFileNm);
		File downloadFile = new File(filePath);
		serviceBean.getFileResponse().setFile(downloadFile);
	}

	private String getBpId (IRaEntity raEntity ) {
		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_BP_LNK);
		List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();

		if (raBpLnkEntity.isEmpty()) {
			return null;
		}

		return raBpLnkEntity.get(0).getBpId();
	}

	private List<String> getFileNames (IRaEntity raEntity) {
		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
		List<IRaAttachedFileEntity> files = raDto.getRaAttachedFileEntities();
		List<String> fileNames = new ArrayList<String>();
		for (IRaAttachedFileEntity file : files) {
			fileNames.add(file.getFilePath());
		}

		return (fileNames.isEmpty()) ? null : fileNames;
	}

	private String getCtgName( IRaEntity raEntity ) {
		CtgLnkCondition ctgLnkCondition = new CtgLnkCondition();
		ctgLnkCondition.setDataCtgCd(RmTables.RM_RA.name());
		ctgLnkCondition.setDataId(raEntity.getRaId());
		List<ICtgLnkEntity> ctgLnk = this.support.getUmFinderSupport().getCtgLnkDao().find(ctgLnkCondition.getCondition());
		if ( ctgLnk.isEmpty() ) {
			return null;
		}

		ICtgEntity ctgEntity = this.support.getUmFinderSupport().findCtgByPrimaryKey(ctgLnk.get(0).getCtgId());
		return ctgEntity.getCtgNm();
	}

	private String getMstoneName( IRaEntity raEntity ) {
		MstoneLnkCondition mstoneLnkCondition = new MstoneLnkCondition();
		mstoneLnkCondition.setDataCtgCd(RmTables.RM_RA.name());
		mstoneLnkCondition.setDataId(raEntity.getRaId());
		List<IMstoneLnkEntity> mstoneLnk = this.support.getUmFinderSupport().getMstoneLnkDao().find(mstoneLnkCondition.getCondition());

		if ( mstoneLnk.isEmpty() ) {
			return null;
		}

		IMstoneEntity mstoneEntity = this.support.getUmFinderSupport().findMstoneByPrimaryKey(mstoneLnk.get(0).getMstoneId());
		return mstoneEntity.getMstoneNm();
	}
}
