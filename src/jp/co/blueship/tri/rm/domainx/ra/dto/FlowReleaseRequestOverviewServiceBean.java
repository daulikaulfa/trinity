package jp.co.blueship.tri.rm.domainx.ra.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * @version V4.00.00
 * @author Chung
 */
public class FlowReleaseRequestOverviewServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleaseRequestOverview detailsView = new ReleaseRequestOverview();

	public RequestParam getParam() {
		return param;
	}

	public ReleaseRequestOverview getDetailsView() {
		return detailsView;
	}
	public FlowReleaseRequestOverviewServiceBean setDetailsView(ReleaseRequestOverview detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;

		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}
	}

	public class ReleaseRequestOverview {
		private String subject;
		private String stsId;
		private String status;
		private String createdBy;
		private String startTime;
		private String endTime;

		public String getSubject() {
			return subject;
		}
		public ReleaseRequestOverview setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReleaseRequestOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleaseRequestOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public ReleaseRequestOverview setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public ReleaseRequestOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public ReleaseRequestOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

	}
}
