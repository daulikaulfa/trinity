package jp.co.blueship.tri.rm.domainx.ra;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.RmItemCheckAddonUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaBpLnkEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyPackageListSearchBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyEntryServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.AppendFileBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean.ReleaseRequestView.ReleaseRequestAttachmentFile;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean.RequestOption;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestServiceBean.RequestParam;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;
import org.mozilla.javascript.edu.emory.mathcs.backport.java.util.Arrays;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestService implements IDomain<FlowReleaseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support = null;
	private IDomain<IGeneralServiceBean> packageListService = null;
	private IDomain<IGeneralServiceBean> entryConfirmService = null;
	private IDomain<IGeneralServiceBean> saveCompleteService = null;
	private IDomain<IGeneralServiceBean> entryCompleteService = null;
	private IDomain<IGeneralServiceBean> entryMailService = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	public void setPackageListService(IDomain<IGeneralServiceBean> packageListService) {
		this.packageListService = packageListService;
	}

	public void setEntryConfirmService(IDomain<IGeneralServiceBean> entryConfirmService) {
		this.entryConfirmService = entryConfirmService;
	}

	public void setSaveCompleteService(IDomain<IGeneralServiceBean> saveCompleteService) {
		this.saveCompleteService = saveCompleteService;
	}

	public void setEntryCompleteService(IDomain<IGeneralServiceBean> entryCompleteService) {
		this.entryCompleteService = entryCompleteService;
	}

	public void setEntryMailService(IDomain<IGeneralServiceBean> entryMailService) {
		this.entryMailService = entryMailService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReleaseRequestServiceBean> execute(
			IServiceDto<FlowReleaseRequestServiceBean> serviceDto) {

		FlowReleaseRequestServiceBean serviceBean = serviceDto.getServiceBean();
		FlowRelApplyEntryServiceBean innerServiceBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String lotId = serviceBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if (RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.onChange(serviceBean);
			}

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.createMockFiles(serviceBean);
				SubmitMode submitMode = serviceBean.getParam().getInputInfo().getSubmitMode();

				if ( SubmitMode.draft.equals(submitMode) ) {
					this.saveAsDraft(serviceBean);

				} else if ( SubmitMode.request.equals(submitMode) ) {
					this.submitChanges(serviceBean);

				} else {
					PreConditions.assertOf(false, "SubmitMode is not specified!");
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerServiceBean);
		}
	}

	private void init(FlowReleaseRequestServiceBean serviceBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyEntryServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		dto.setServiceBean(innerBean);

		{
			innerBean.setUserId(serviceBean.getUserId());
			innerBean.setScreenType(ScreenType.next);
			innerBean.setReferer(RelApplyScreenID.TOP);
			innerBean.setForward(RelApplyScreenID.PACKAGE_LIST_SELECT);
			packageListService.execute(dto);
		}

		this.beforeExecution(serviceBean, innerBean);
		{
			innerBean.setReferer(RelApplyScreenID.PACKAGE_LIST_SELECT);
			innerBean.setForward(RelApplyScreenID.PACKAGE_LIST_SELECT);
			packageListService.execute(dto);
		}
		this.afterExecution(innerBean, serviceBean);
	}

	private void onChange(FlowReleaseRequestServiceBean serviceBean) {
		PreConditions.assertOf(serviceBean.getParam().getRequestOption() != null, "RequestOption is not specified");

		RequestOption selectedOption = serviceBean.getParam().getRequestOption();
		String selectedLotId = serviceBean.getParam().getSelectedLotId();

		if (RequestOption.deleteUplodedFile.equals(selectedOption)) {
			removeAttachedFile(serviceBean);
			serviceBean.getParam().setRequestOption(RequestOption.none);
			return;
		}

		if (RequestOption.refreshCategory.equals(selectedOption)) {
			List<ICtgEntity> ctgEntityList = this.support.getUmFinderSupport().findCtgByLotId(selectedLotId);
			serviceBean.getParam().getInputInfo().setCtgViews(FluentList.from(ctgEntityList).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );
		}

		if (RequestOption.refreshMilestone.equals(selectedOption)) {
			List<IMstoneEntity> mstoneEntityList = this.support.getUmFinderSupport().findMstoneByLotId(selectedLotId);
			serviceBean.getParam().getInputInfo().setMstoneViews(FluentList.from(mstoneEntityList).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );
		}

		if (RequestOption.selectBpId.equals(selectedOption)) {
			IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
			FlowRelApplyEntryServiceBean innerBean = serviceBean.getInnerService();
			TriPropertyUtils.copyProperties(innerBean, serviceBean);
			innerBean.setScreenType(ScreenType.next);
			dto.setServiceBean(innerBean);

			this.beforeExecution(serviceBean, innerBean);
			{
				innerBean.setReferer(RelApplyScreenID.PACKAGE_LIST_SELECT);
				innerBean.setForward(RelApplyScreenID.ENTRY);
				packageListService.execute(dto);
				this.entryService(serviceBean, innerBean);

			}
			this.afterExecution(innerBean, serviceBean);
		}
	}

	private void saveAsDraft(FlowReleaseRequestServiceBean serviceBean) {
		if( TriStringUtils.isEmpty(serviceBean.getParam().getInputInfo().getBpId()) ) {
			throw new ContinuableBusinessException(RmMessageId.RM001038E);
		}
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyEntryServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		innerBean.setScreenType(ScreenType.next);
		dto.setServiceBean(innerBean);

		// Status Matrix Check
		{
			String lotId = serviceBean.getParam().getSelectedLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( serviceBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setBpIds( serviceBean.getParam().getInputInfo().getBpId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(serviceBean, innerBean);
		{
			innerBean.setReferer(RelApplyScreenID.ENTRY);
 			innerBean.setForward(RelApplyScreenID.SAVE_COMP);
			this.entryService(serviceBean, innerBean);
			saveCompleteService.execute(dto);
		}
		this.afterExecution(innerBean, serviceBean);
	}

	private void submitChanges(FlowReleaseRequestServiceBean serviceBean) {
		if( TriStringUtils.isEmpty(serviceBean.getParam().getInputInfo().getBpId()) ) {
			throw new ContinuableBusinessException(RmMessageId.RM001038E);
		}
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyEntryServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		dto.setServiceBean(innerBean);

		// Status Matrix Check
		{
			String lotId = serviceBean.getParam().getSelectedLotId();
			String[] pjtIds = (innerBean.getRelApplyEntryViewBean().getPjtNoList() == null )?null:innerBean.getRelApplyEntryViewBean().getPjtNoList().toArray(new String[0]);
			String[] areqIds = (innerBean.getRelApplyEntryViewBean().getAssetApplyNoList() == null )?null:innerBean.getRelApplyEntryViewBean().getAssetApplyNoList().toArray(new String[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( serviceBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( lotId )
			.setPjtIds( pjtIds )
			.setAreqIds( areqIds )
			.setBpIds( serviceBean.getParam().getInputInfo().getBpId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(serviceBean, innerBean);
		{
			innerBean.setReferer(RelApplyScreenID.ENTRY);
			innerBean.setForward(RelApplyScreenID.ENTRY_CONFIRM);
			this.entryService(serviceBean, innerBean);
			entryConfirmService.execute(dto);
		}

		{
			innerBean.setReferer(RelApplyScreenID.ENTRY_CONFIRM);
			innerBean.setForward(RelApplyScreenID.ENTRY_COMP);
			this.entryConfirmService(innerBean);
			entryCompleteService.execute(dto);
			entryMailService.execute(dto);
		}

		this.afterExecution(innerBean, serviceBean);
	}

	private void beforeExecution(FlowReleaseRequestServiceBean src, FlowRelApplyEntryServiceBean dest) {
		src.getResult().setCompleted(false);
		RequestParam param = src.getParam();
		ReleaseRequestEditInputBean srcInfo = param.getInputInfo();
		RelApplyPackageListSearchBean searchBean = dest.getRelApplyPackageListSearchBean();
		RelApplyEntryViewBean viewBean = dest.getRelApplyEntryViewBean();

		if ( RequestType.init.equals(param.getRequestType()) ) {
			dest.setScreenType(ScreenType.select);
			searchBean.setSelectedLotNo( param.getSelectedLotId() );
			dest.setUserId(src.getUserId());
		}

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.setScreenType(ScreenType.next);
			searchBean.setSelectedBuildNo( param.getInputInfo().getBpId() );
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			SubmitMode submitMode = src.getParam().getInputInfo().getSubmitMode();

			if (SubmitMode.draft.equals(submitMode)) {
				dest.setScreenType(ScreenType.next);
				searchBean.setSelectedBuildNo(srcInfo.getBpId());
				searchBean.setSelectedGroupId(srcInfo.getGroupId());
				searchBean.setSelectedLotNo( param.getSelectedLotId() );
				searchBean.setSelectedRelEnvNo(srcInfo.getRelEnv());
				searchBean.setSelectedRelWishDate(srcInfo.getPreferredDate());

				viewBean.setRelApplyUserId(srcInfo.getSubmitterId());
				viewBean.setRelApplyUser( this.getUserName(srcInfo.getSubmitterId()) );
				viewBean.setRelApplyPersonChargeId(srcInfo.getSupervisor());
				viewBean.setRelApplyPersonCharge( srcInfo.getSupervisor() );
				viewBean.setRemarks( srcInfo.getRemarks() );
				viewBean.setRelWishDate( srcInfo.getPreferredDate() );
				viewBean.setAppendFile(this.getAppendFileBeanMap(srcInfo));
				viewBean.setCtgId( srcInfo.getCtgId() );
				viewBean.setMstoneId( srcInfo.getMstoneId() );
				viewBean.setRelatedNo( srcInfo.getRelatedId() );
			}

			if (SubmitMode.request.equals(submitMode)) {
				dest.setUserId(src.getUserId());
				dest.setUserName(src.getUserName());
				searchBean.setSelectedGroupId(srcInfo.getGroupId());
				searchBean.setSelectedBuildNo(srcInfo.getBpId());
				searchBean.setSelectedRelEnvNo(srcInfo.getRelEnv());
				searchBean.setSelectedRelWishDate(srcInfo.getPreferredDate());

				viewBean.setRelApplyUserId(srcInfo.getSubmitterId());
				viewBean.setRelApplyUser( this.getUserName(srcInfo.getSubmitterId()) );
				viewBean.setRelApplyPersonChargeId(srcInfo.getSupervisor());
				viewBean.setRelApplyPersonCharge( srcInfo.getSupervisor() );
				viewBean.setRemarks( srcInfo.getRemarks() );
				viewBean.setRelWishDate( srcInfo.getPreferredDate() );
				viewBean.setAppendFile(this.getAppendFileBeanMap(srcInfo));
				viewBean.setCtgId( srcInfo.getCtgId() );
				viewBean.setMstoneId( srcInfo.getMstoneId() );
				viewBean.setRelatedNo( srcInfo.getRelatedId() );
			}
		}
	}

	private void afterExecution(FlowRelApplyEntryServiceBean src, FlowReleaseRequestServiceBean dest) {
		if (RequestType.init.equals(dest.getParam().getRequestType())) {
			this.generateLists(src, dest);
			dest.getResult().setCompleted(true);
		}

		if (RequestType.onChange.equals(dest.getParam().getRequestType())) {
			dest.getResult().setCompleted(true);
		}

		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.getResult().setCompleted(true);
			dest.getResult().setRaId( src.getRelApplyEntryViewBean().getRelApplyNo());
			SubmitMode submitMode = dest.getParam().getInputInfo().getSubmitMode();

			if ( SubmitMode.draft.equals(submitMode) ) {
				dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003015I);

			} else if ( SubmitMode.request.equals(submitMode) ) {
				dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003023I);

			}
		}
	}

	private void generateLists(FlowRelApplyEntryServiceBean src, FlowReleaseRequestServiceBean dest) {
		ReleaseRequestEditInputBean inputInfo = dest.getParam().getInputInfo();
		String lotId = dest.getParam().getSelectedLotId();

		if (RequestType.init.equals(dest.getParam().getRequestType())) {
			// build package
			inputInfo.setBuildPackageViews(FluentList.from(src.getUnitViewBeanList()).map(RmFluentFunctionUtils.toItemLabelsFromUnitViewBeanRel).asList());

			// get category
			List<ICtgEntity> ctgEntityList = this.support.getUmFinderSupport().findCtgByLotId(lotId);
			inputInfo.setCtgViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());

			// get milestones
			List<IMstoneEntity> mstoneEntityList = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
			inputInfo.setMstoneViews(FluentList.from( mstoneEntityList).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());


			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
			List<IGrpEntity> grpEntity = this.support.getUmFinderSupport().findGroupByUserId(dest.getUserId());
			List<String> groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto,  grpEntity);

			List<IUserEntity> userEntities = this.support.getUmFinderSupport().findUserByGroups(groupIds.toArray(new String[] {}));
			inputInfo.setSubmitterViews(FluentList.from( userEntities ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());
			inputInfo.setSupervisorViews(FluentList.from( userEntities ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

			// group views
			List<IGrpEntity> groupUserview = support.getUmFinderSupport().findGroupByGroupIds( groupIds.toArray(new String[] {}) );
			inputInfo.setGroupViews(ItemLabelsUtils.conv( groupUserview.toArray(new IGrpEntity[] {}) ));

			// get environment no list
			List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
			lotDtoEntities.add(lotDto);
			IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
			IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

			inputInfo.setReleaseEnvViews(ItemLabelsUtils.conv(envEntities));
		}
	}

	private void removeAttachedFile(FlowReleaseRequestServiceBean serviceBean) {
		String deletedFileNm = serviceBean.getParam().getInputInfo().getDeletedFileNm();
		String deletedFileSeqNo = serviceBean.getParam().getInputInfo().getDeletedFileSeqNo();
		List<AppendFileBean> appendFilesList =  serviceBean.getParam().getInputInfo().getAppendFile();
		for(int i = 0; i < appendFilesList.size(); i++) {
			if(appendFilesList.get(i).getAppendFileNm().equals(deletedFileNm)
					&& appendFilesList.get(i).getSeqNo().equals(deletedFileSeqNo)) {
				appendFilesList.remove(i);
				return;
			}
		}
	}

	private Map<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> getAppendFileBeanMap(ReleaseRequestEditInputBean srcInfo) {
		Map<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> appendFileMap =
				new TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean>();

		RelApplyEntryViewBean entryViewBean = new RelApplyEntryViewBean();
		for ( SessionScopeKeyConsts.AppendFileEnum appendFile: SessionScopeKeyConsts.AppendFileEnum.values() ) {
			List<AppendFileBean> appendFileBeanList = srcInfo.getAppendFile();
			int id = Integer.valueOf(appendFile.getId()) - 1;
			if ( id >= appendFileBeanList.size()) {
				break;
			}
			AppendFileBean srcAppendFileBean = appendFileBeanList.get(id);
			if (TriStringUtils.isEmpty(srcAppendFileBean)) {
				continue;
			}

			RelApplyEntryViewBean.AppendFileBean destAppendFileBean = entryViewBean.newAppendFileBean();
			destAppendFileBean.setAppendFileInputStreamBytes( srcAppendFileBean.getAppendFileInputStreamBytes() );
			destAppendFileBean.setAppendFileInputStreamName( srcAppendFileBean.getAppendFileNm() );
			destAppendFileBean.setAppendFile( srcAppendFileBean.getAppendFileNm() );

			appendFileMap.put(appendFile.getId(), destAppendFileBean);
		}
		return appendFileMap;
	}

	private String getUserName (String userId) {
		if(TriStringUtils.isEmpty(userId)) {
			return "";
		}
		IUserEntity userEntity = this.support.getUmFinderSupport().findUserByUserId(userId);
		String userName = userEntity.getUserNm();
		return userName;
	}

	private void entryConfirmService(FlowRelApplyEntryServiceBean innerBean) {
		RelApplyEntryViewBean viewBean = innerBean.getRelApplyEntryViewBean();
		RelApplyPackageListSearchBean listSearch = innerBean.getRelApplyPackageListSearchBean();

		this.checkRelApplyEntryConfirm(
				support,
				support.getUmFinderSupport(),
				innerBean.getUserId(),
				listSearch.getSelectedGroupId(),
				listSearch.getSelectedRelEnvNo(),
				listSearch.getSelectedRelWishDate(),
				viewBean);

		StatusCheckDto statusDto = new StatusCheckDto()
		.setServiceBean( innerBean )
		.setFinder( support )
		.setActionList( statusMatrixAction )
		.setLotIds( viewBean.getLotNo() )
		.setBpIds( support.getBuildNo(viewBean.getUnitBeanList() ) );

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
	}

	private void entryService(FlowReleaseRequestServiceBean serviceBean, FlowRelApplyEntryServiceBean innerBean) {
		RelApplyPackageListSearchBean listSearch = innerBean.getRelApplyPackageListSearchBean();
		RelApplyEntryViewBean viewBean = innerBean.getRelApplyEntryViewBean();
		
		if (serviceBean.getParam().getInputInfo().getSubmitMode() == SubmitMode.draft) {
			viewBean.setDraff(true);
		}

		String refererID	= innerBean.getReferer();
		String forwardID	= innerBean.getForward();
		String screenType	= innerBean.getScreenType();

		RelApplyPackageListSearchBean searchBean = DBSearchBeanAddonUtil
				.setRelUnitSearchBeanByRelApplyEntry(innerBean.getRelApplyPackageListSearchBean());

		if( refererID.equals( RelApplyScreenID.ENTRY )) {
			if ( ScreenType.next.equals( screenType ) ) {
				if ( RelApplyScreenID.SAVE_COMP.equals(forwardID) ) {
					RmItemCheckAddonUtils.checkRelApplyEntryInput(
							RmDesignBeanId.flowRelApplySaveInputCheck,
							listSearch,
							viewBean );
				} else {
					RmItemCheckAddonUtils.checkRelApplyEntryInput(
							RmDesignBeanId.flowRelApplyInputCheck,
							listSearch,
							viewBean );
				}

				this.checkRelApplyEntryConfirm(
						support,
						support.getUmFinderSupport(),
						innerBean.getUserId(),
						listSearch.getSelectedGroupId(),
						listSearch.getSelectedRelEnvNo(),
						listSearch.getSelectedRelWishDate(),
						viewBean);
			}
		}

		if ( refererID.equals( RelApplyScreenID.PACKAGE_LIST_SELECT )
				&& forwardID.equals( RelApplyScreenID.ENTRY ) ) {
			String lotId = searchBean.getSelectedLotNo() ;
			String buildNo = searchBean.getSelectedBuildNo() ;

			//ビルド情報の取得
			IBpDto bpDto = this.support.getBmFinderSupport().findBpDto(buildNo);
			IBpEntity bpEntity = bpDto.getBpEntity();

			//ロット情報の取得
			List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( lotId );
			lotDtoEntities.add(lotDto);

			//ビルド関連資産申請情報の取得
			List<IBpAreqLnkEntity> buildApplyEntityArray = bpDto.getBpAreqLnkEntities() ;

			/*
			 * 表示項目構築
			 */
			RelApplyEntryViewBean relApplyEntryViewBean = new RelApplyEntryViewBean() ;
			relApplyEntryViewBean.setLotNo					( lotId ) ;
			relApplyEntryViewBean.setLotName				( lotDto.getLotEntity().getLotNm() ) ;
			relApplyEntryViewBean.setRelApplyNo				( "" ) ;		// 採番してないので空で表示
			innerBean.setSystemDate							( TriDateUtils.getSystemTimestamp() );
			relApplyEntryViewBean.setRelApplyDate			( TriDateUtils.convertViewDateFormat( innerBean.getSystemDate() ) ) ;
			relApplyEntryViewBean.setRelApplyGroupId		( "" ) ;		// 空で初期表示
			relApplyEntryViewBean.setRelEnvNo				( "" ) ;		// 空で初期表示
			relApplyEntryViewBean.setRelApplyUserId			( innerBean.getUserId() ) ;
			relApplyEntryViewBean.setRelApplyUser			( "" ) ;		// 諸事情により名前は表示しない
			relApplyEntryViewBean.setRelApplyPersonChargeId	( "" ) ;		// 空とする
			relApplyEntryViewBean.setRelApplyPersonCharge	( "" ) ;
			relApplyEntryViewBean.setRelWishDate			( "" ) ;		// 空で初期表示

			List<IRaBpLnkEntity> raBpLnkEntities = new ArrayList<IRaBpLnkEntity>();
			{
				RaBpLnkEntity raBpEntity = new RaBpLnkEntity();
				raBpEntity.setBpId(bpEntity.getBpId());
				raBpEntity.setBpNm(bpEntity.getBpNm());
				raBpEntity.setSummary(bpEntity.getSummary());
				raBpEntity.setContent(bpEntity.getContent());

				raBpLnkEntities.add( raBpEntity );
			}

			relApplyEntryViewBean.setUnitBeanList			( RmConverterAddonUtils.convertUnitBean( raBpLnkEntities ) ) ;
			relApplyEntryViewBean.setPjtNoList				( RmConverterAddonUtils.convertPjtNoList(this.support , buildApplyEntityArray) ) ;
			relApplyEntryViewBean.setChangeCauseNoList		( RmConverterAddonUtils.convertChangeCauseNoList(this.support, buildApplyEntityArray) ) ;
			relApplyEntryViewBean.setAssetApplyNoList		( RmConverterAddonUtils.convertAssetApplyNoListByBpAreq(buildApplyEntityArray) ) ;

			relApplyEntryViewBean.setRelatedNo				("") ;			// 空で初期表示
			relApplyEntryViewBean.setRemarks				("") ;			// 空で初期表示

			/*
			 * 選択可能なリリース環境リスト取得
			 */
			innerBean.setSearchRelEnvNoList( serviceBean.getParam().getInputInfo().getReleaseEnvViews() );

			/*
			 * 選択可能なリリース希望日リスト取得
			 */
			ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

			innerBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );

			/*
			 * 選択可能なグループリスト取得
			 */
			innerBean.setGroupIdList( serviceBean.getParam().getInputInfo().getGroupViews() );

			/*
			 * 戻りに設定
			 */
			innerBean.setRelApplyEntryViewBean( relApplyEntryViewBean ) ;
		}
	}

	private void createMockFiles(FlowReleaseRequestServiceBean serviceBean) {
		List<ReleaseRequestAttachmentFile> mockFiles = new ArrayList<ReleaseRequestAttachmentFile>();
		for(AppendFileBean fileBean : serviceBean.getParam().getInputInfo().getAppendFile()) {
			ReleaseRequestAttachmentFile mockFile = serviceBean.getReleaseView().new ReleaseRequestAttachmentFile();
			mockFile.setName(fileBean.getAppendFileNm());
			mockFile.setSeqNo(fileBean.getSeqNo());
			mockFile.setSize(fileBean.getAppendFileInputStreamBytes().length);

			mockFiles.add(mockFile);
		}
		serviceBean.getReleaseView().setAttachmentFiles(mockFiles);
	}

	public void checkRelApplyEntryConfirm(
			FlowRelApplyEditSupport support,
			IUmFinderSupport umFinderSupport,
			String userId,
			String groupId,
			String relEnvNo,
			String relWishDate,
			RelApplyEntryViewBean viewBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			//申請グループ
			IGrpUserLnkEntity groupUserview = null;
			List<IGrpUserLnkEntity> groupUserviews = umFinderSupport.findGrpUserLnkByUserId( userId );
			IGrpEntity grpEntity = null;
			for (IGrpUserLnkEntity view : groupUserviews) {

				if(groupId.equals( view.getGrpId() ) ) {

					groupUserview = view;
					grpEntity = umFinderSupport.findGroupById( view.getGrpId() );
					break;
				}
			}
			if ( !viewBean.isDraff() && groupUserview == null ) {
				messageList.add		( RmMessageId.RM001005E );
				messageArgsList.add	( new String[] {} );
				grpEntity.setGrpNm("");
			}

			//リリース環境
			BldEnvCondition bcondition = new BldEnvCondition();
			bcondition.setBldEnvId(relEnvNo);
			IBldEnvEntity relEnvEntity = null;
			if (TriStringUtils.isNotEmpty(relEnvNo)) {
				relEnvEntity = support.getBmFinderSupport().getBldEnvDao().find( bcondition.getCondition() ).get(0);
			}
			if ( null == relEnvEntity || StatusFlg.on.value().equals(relEnvEntity.getDelStsId().value()) ) {
				if (!viewBean.isDraff() ) {
					messageList.add		( RmMessageId.RM001006E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//リリース希望日
			if ( !viewBean.isDraff() && TriStringUtils.isEmpty(relWishDate) ) {
				messageList.add		( RmMessageId.RM001007E );
				messageArgsList.add	( new String[] {} );
			}

			//ビルドパッケージ
			{
				Set<String> buildNoSet = new LinkedHashSet<String>();
				for (jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean unitBean : viewBean.getUnitBeanList()) {
					buildNoSet.add(unitBean.getBuildNo());
				}

				BpCondition condition = new BpCondition();
				condition.setBpIds( buildNoSet.toArray(new String[0]) );

				if ( buildNoSet.size() != support.getBmFinderSupport().getBpDao().count(condition.getCondition()) ) {
					messageList.add		( RmMessageId.RM001015E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//選択項目の移し換え
			if ( null != groupUserview ) {
				viewBean.setRelApplyGroupId( groupUserview.getGrpId() );
			}
			if ( null != grpEntity ) {
				viewBean.setRelApplyGroup( grpEntity.getGrpNm() );
			}
			if ( null != relEnvEntity ) {
				viewBean.setRelEnvNo( relEnvEntity.getBldEnvId() );
				viewBean.setRelEnvName( relEnvEntity.getBldEnvNm() );
			}
			if ( null != relWishDate ) {
				viewBean.setRelWishDate(relWishDate);
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}
}
