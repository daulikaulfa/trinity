package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.io.File;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyDetailViewServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestDetailsViewBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleaseRequestApprovalPendingDetailsServiceBean  extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReportListFileResponse fileResponse = new ReportListFileResponse();
	private ReleaseRequestView detailsView = new ReleaseRequestView();

	{
		this.setInnerService(new FlowRelApplyDetailViewServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowReleaseRequestApprovalPendingDetailsServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReportListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReleaseRequestApprovalPendingDetailsServiceBean setFileResponse(ReportListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public ReleaseRequestView getDetailsView() {
		return detailsView;
	}

	public FlowReleaseRequestApprovalPendingDetailsServiceBean setDetailsView(ReleaseRequestView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId = null;
		private String selectedFileNm = null;

		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}

		public String getSelectedFileNm() {
			return selectedFileNm;
		}
		public RequestParam setSelectedFileNm(String selectedFileNm) {
			this.selectedFileNm = selectedFileNm;
			return this;
		}
	}

	public class ReportListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

	}

	public class ReleaseRequestView extends ReleaseRequestDetailsViewBean {
		private String stsId;
		private String status;
		private boolean isEditEnable = true;

		public String getStsId() {
			return stsId;
		}
		public ReleaseRequestView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleaseRequestView setStatus(String status) {
			this.status = status;
			return this;
		}

		public boolean isEditEnable() {
			return isEditEnable;
		}

		public ReleaseRequestView setIsEditEnable(boolean isEditEnable) {
			this.isEditEnable = isEditEnable;
			return this;
		}
	}
}
