package jp.co.blueship.tri.rm.domainx.ra;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCancelServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestRemovalServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowReleaseRequestRemovalService implements IDomain<FlowReleaseRequestRemovalServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowReleaseRequestRemovalServiceBean> execute(
			IServiceDto<FlowReleaseRequestRemovalServiceBean> serviceDto) {

		FlowReleaseRequestRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelApplyCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String raId = paramBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");
			this.submitChanges(paramBean);

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}

		return serviceDto;
	}

	private void submitChanges(FlowReleaseRequestRemovalServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);
		IRaEntity raEntity = null;
		// Status Matrix Check
		{
			String raId = paramBean.getParam().getSelectedRaId();
			List<IRaDto> raDtoList = this.support.findRaDto( new String[]{ raId } );
			raEntity = raDtoList.get(0).getRaEntity();
			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( raEntity.getLotId() )
			.setRaIds( raEntity.getRaId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer(RelApplyScreenID.DETAIL_VIEW);
			serviceBean.setForward(RelApplyScreenID.CANCEL_COMP);
			completeService.execute(dto);
			mailService.execute(dto);
		}

		if(TriView.ReleaseRequestList.value().equals(paramBean.getReferer()) && RmRaStatusId.DraftReleaseRequest.equals(raEntity.getStsId())) {
			paramBean.getResult().setRedirectToDraft(true);
		}
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(RmMessageId.RM003022I, paramBean.getParam().getSelectedRaId());
	}

	private void beforeExecution(FlowReleaseRequestRemovalServiceBean src, FlowRelApplyCancelServiceBean dest) {
		src.getResult().setCompleted(false);
		RelApplySearchBean relApplySearchBean = new RelApplySearchBean();
		relApplySearchBean.setSearchRelApplyNo(src.getParam().getSelectedRaId());
		dest.setUserId( src.getUserId());
		dest.setUserName( src.getUserName());
		dest.setRelApplySearchBean(relApplySearchBean);
	}
}
