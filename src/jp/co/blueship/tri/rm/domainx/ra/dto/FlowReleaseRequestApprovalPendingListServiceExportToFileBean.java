package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.PendingReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovalPendingListServiceBean.SearchCondition;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestApprovalPendingListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReleaseRequestListFileResponse fileResponse = new ReleaseRequestListFileResponse();
	private List<PendingReleaseRequestView> releaseRequestViews = new ArrayList<PendingReleaseRequestView>();

	{
		this.setInnerService( new FlowReleaseRequestApprovalPendingListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestApprovalPendingListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReleaseRequestListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReleaseRequestApprovalPendingListServiceExportToFileBean setFileResponse(ReleaseRequestListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<PendingReleaseRequestView> getReleaseRequestViews() {
		return releaseRequestViews;
	}

	public FlowReleaseRequestApprovalPendingListServiceExportToFileBean setReleaseRequestViews(List<PendingReleaseRequestView> releaseRequestViews) {
		this.releaseRequestViews = releaseRequestViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchCondition searchCondition = new FlowReleaseRequestApprovalPendingListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;
		private ExportToFileBean exportBean = null;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class ReleaseRequestListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
