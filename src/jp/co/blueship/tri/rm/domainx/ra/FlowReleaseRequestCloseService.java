package jp.co.blueship.tri.rm.domainx.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyCloseEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyCloseServiceBean.RelApplyViewBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestCloseServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestCloseService implements IDomain<FlowReleaseRequestCloseServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> closeService = null;
	private IDomain<IGeneralServiceBean> closeCompleteService = null;
	private IDomain<IGeneralServiceBean> closeMailService = null;
	private FlowRelApplyEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowRelApplyEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setCloseMailService(IDomain<IGeneralServiceBean> closeMailService) {
		this.closeMailService = closeMailService;
	}

	public void setCloseService(IDomain<IGeneralServiceBean> closeService) {
		this.closeService = closeService;
	}

	public void setCloseCompleteService(IDomain<IGeneralServiceBean> closeCompleteService) {
		this.closeCompleteService = closeCompleteService;
	}

	@Override
	public IServiceDto<FlowReleaseRequestCloseServiceBean> execute(
			IServiceDto<FlowReleaseRequestCloseServiceBean> serviceDto) {

		FlowReleaseRequestCloseServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelApplyCloseServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String raId = paramBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");

			if (RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}

		return serviceDto;
	}

	private void validate(FlowReleaseRequestCloseServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String raId = paramBean.getParam().getSelectedRaId();
			List<IRaDto> raDtoList = new ArrayList<IRaDto>();
			raDtoList.add( this.support.findRaDto( raId ) );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( support.getLotNo( raDtoList ) )
					.setRaIds( raId )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer(RelApplyScreenID.CLOSE_LIST_VIEW);
			serviceBean.setForward(RelApplyScreenID.CLOSE);
			closeService.execute(dto);

		}
	}

	private void submitChanges(FlowReleaseRequestCloseServiceBean paramBean) {

		{
			String raId = paramBean.getParam().getSelectedRaId();
			List<IRaDto> raDtoList = new ArrayList<IRaDto>();
			raDtoList.add( this.support.findRaDto( raId ) );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( support.getLotNo( raDtoList ) )
					.setRaIds( raId )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyCloseServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer(RelApplyScreenID.CLOSE);
			serviceBean.setForward(RelApplyScreenID.CLOSE_COMP);
			closeService.execute(dto);
			closeCompleteService.execute(dto);
			closeMailService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(RmMessageId.RM003012I);
	}

	private void beforeExecution(FlowReleaseRequestCloseServiceBean src, FlowRelApplyCloseServiceBean dest) {

		dest.setUserId(src.getUserId());
		dest.setUserName(src.getUserName());

		if (RequestType.validate.equals(src.getParam().getRequestType())) {
			dest.setScreenType(ScreenType.select);
			RelApplyViewBean relApplyViewBean = dest.newRelApplyViewBean();
			relApplyViewBean.setSelectedRelApplyNo(new String[] {src.getParam().getSelectedRaId()});
			dest.setRelApplyViewBean(relApplyViewBean);
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			src.getResult().setCompleted(false);
			dest.setScreenType(ScreenType.next);
			RelApplyCloseEntryViewBean relCloseViewBean = dest.newRelApplyCloseEntryViewBean();
			relCloseViewBean.setCloseComment(src.getParam().getInputInfo().getComment());
			dest.setRelApplyCloseEntryViewBean(relCloseViewBean);
		}
	}
}
