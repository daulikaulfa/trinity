package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.DraftReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestListServiceExportToFileBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestListServiceExportToFile implements IDomain<FlowReleaseRequestListServiceExportToFileBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> releaseRequestList = null;

	public void setReleaseRequestList(IDomain<IGeneralServiceBean> releaseRequestList) {
		this.releaseRequestList = releaseRequestList;
	}

	@Override
	public IServiceDto<FlowReleaseRequestListServiceExportToFileBean> execute(
			IServiceDto<FlowReleaseRequestListServiceExportToFileBean> serviceDto) {
		FlowReleaseRequestListServiceExportToFileBean serviceBean = serviceDto.getServiceBean();
		FlowReleaseRequestListServiceBean innerServiceBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");
			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType()) ) {
				this.onChange(serviceBean);
			}
			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerServiceBean);
		}

	}

	private void onChange (FlowReleaseRequestListServiceExportToFileBean serviceBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowReleaseRequestListServiceBean innerService = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerService, serviceBean);
		dto.setServiceBean(innerService);

		{
			innerService.getParam().setRequestType(RequestType.init);
			innerService.getParam().setSelectedLotId(serviceBean.getParam().getLotId());
			releaseRequestList.execute(dto);
		}

		{
			innerService.getParam().setRequestType(RequestType.onChange);
			this.beforeExecution(serviceBean, innerService);
			releaseRequestList.execute(dto);
			this.afterExecution(innerService, serviceBean);
		}
	}

	private void beforeExecution (FlowReleaseRequestListServiceExportToFileBean src,
									FlowReleaseRequestListServiceBean dest) {

		if ( RequestType.onChange.equals(src.getParam().getRequestType()) ) {
			SearchCondition srcSearchCondition = src.getParam().getSearchCondition();
			SearchCondition srcSearchDraftCondition = src.getParam().getSearchDraftCondition();

			SearchCondition destSearchCondition = dest.getParam().getSearchCondition();
			SearchCondition destSearchDraftCondition = dest.getParam().getSearchDraftCondition();

			if (src.getParam().isSelectedDraft()) {

				dest.getParam().setSelectedDraft(true);
				destSearchDraftCondition.setBldEnvId	( srcSearchDraftCondition.getBldEnvId() );
				destSearchDraftCondition.setCtgId		( srcSearchDraftCondition.getCtgId() );
				destSearchDraftCondition.setMstoneId	( srcSearchDraftCondition.getMstoneId() );
				destSearchDraftCondition.setKeyword		( srcSearchDraftCondition.getKeyword() );
				destSearchDraftCondition.setSelectedPageNo(1);

			} else {

				dest.getParam().setSelectedDraft(false);
				destSearchCondition.setBldEnvId		( srcSearchCondition.getBldEnvId() );
				destSearchCondition.setStsId		( srcSearchCondition.getStsId() );
				destSearchCondition.setCtgId		( srcSearchCondition.getCtgId() );
				destSearchCondition.setMstoneId		( srcSearchCondition.getMstoneId() );
				destSearchCondition.setKeyword		( srcSearchCondition.getKeyword() );
				destSearchCondition.setSelectedPageNo(1);
			}

			dest.getParam().setLinesPerPage(0);
		}
	}

	private void afterExecution (FlowReleaseRequestListServiceBean src,
								FlowReleaseRequestListServiceExportToFileBean dest) {

		if ( RequestType.onChange.equals(dest.getParam().getRequestType()) ) {
			List<ReleaseRequestView> srcViews = src.getReleaseRequestViews();
			List<DraftReleaseRequestView> srcDraftViews = src.getDraftReleaseRequestViews();

			List<ReleaseRequestView> destViews = new ArrayList<ReleaseRequestView>();
			List<DraftReleaseRequestView> destDraftViews = new ArrayList<DraftReleaseRequestView>();

			if ( dest.getParam().isSelectedDraft() ) {
				for (int i = 0; i < srcDraftViews.size(); i++) {
					DraftReleaseRequestView view = srcDraftViews.get(i);
					destDraftViews.add(view);
				}
				dest.setDraftReleaseRequestViews(destDraftViews);

			} else {
				for (int i = 0; i < srcViews.size(); i++) {
					ReleaseRequestView view = srcViews.get(i);
					destViews.add(view);
				}
				dest.setReleaseRequestViews(destViews);
			}
		}
	}

	private void submitChanges(FlowReleaseRequestListServiceExportToFileBean serviceBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = serviceBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(serviceBean, serviceBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(serviceBean, serviceBean.getParam().getExportBean());
		}

		serviceBean.getFileResponse().setFile(file);
	}
}
