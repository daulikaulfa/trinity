package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.ApprovedReleaseRequestView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedListServiceExportToFileBean;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleaseRequestApprovedListServiceExportToFile implements IDomain<FlowReleaseRequestApprovedListServiceExportToFileBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> releaseRequestApprovedList = null;

	public void setReleaseRequestApprovedList(IDomain<IGeneralServiceBean> releaseRequestApprovedList) {
		this.releaseRequestApprovedList = releaseRequestApprovedList;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovedListServiceExportToFileBean> execute(
			IServiceDto<FlowReleaseRequestApprovedListServiceExportToFileBean> serviceDto) {

		FlowReleaseRequestApprovedListServiceExportToFileBean serviceBean = serviceDto.getServiceBean();
		FlowReleaseRequestApprovedListServiceBean innerServiceBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");
			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType()) ) {
				this.onChange(serviceBean);
			}
			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerServiceBean);
		}
	}

	private void onChange(FlowReleaseRequestApprovedListServiceExportToFileBean serviceBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowReleaseRequestApprovedListServiceBean innerService = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerService, serviceBean);
		dto.setServiceBean(innerService);

		{
			innerService.getParam().setRequestType(RequestType.init);
			innerService.getParam().setSelectedLotId(serviceBean.getParam().getLotId());
			releaseRequestApprovedList.execute(dto);
		}

		{
			innerService.getParam().setRequestType(RequestType.onChange);
			this.beforeExecution(serviceBean, innerService);
			releaseRequestApprovedList.execute(dto);
			this.afterExecution(innerService, serviceBean);
		}
	}

	private void beforeExecution (FlowReleaseRequestApprovedListServiceExportToFileBean src,
									FlowReleaseRequestApprovedListServiceBean dest) {

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			SearchCondition srcSearchCondtion = src.getParam().getSearchCondition();
			SearchCondition destSearchCondtion = dest.getParam().getSearchCondition();

			destSearchCondtion.setBldEnvId	( srcSearchCondtion.getBldEnvId() );
			destSearchCondtion.setStsId		( srcSearchCondtion.getStsId() );
			destSearchCondtion.setCtgId		( srcSearchCondtion.getCtgId() );
			destSearchCondtion.setMstoneId	( srcSearchCondtion.getMstoneId() );
			destSearchCondtion.setKeyword	( srcSearchCondtion.getKeyword() );
			destSearchCondtion.setSelectedPageNo(1);
		}
	}

	private void afterExecution (FlowReleaseRequestApprovedListServiceBean src,
									FlowReleaseRequestApprovedListServiceExportToFileBean dest) {

		if (RequestType.onChange.equals(dest.getParam().getRequestType())) {
			List<ApprovedReleaseRequestView> srcViews = src.getReleaseRequestViews();
			List<ApprovedReleaseRequestView> destViews = new ArrayList<ApprovedReleaseRequestView>();

			for (int i = 0; i < srcViews.size(); i++) {
				ApprovedReleaseRequestView view = srcViews.get(i);
				destViews.add(view);
			}
			dest.setReleaseRequestViews(destViews);
		}
	}

	private void submitChanges(FlowReleaseRequestApprovedListServiceExportToFileBean serviceBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = serviceBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(serviceBean, serviceBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(serviceBean, serviceBean.getParam().getExportBean());
		}

		serviceBean.getFileResponse().setFile(file);
	}

}
