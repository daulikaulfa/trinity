package jp.co.blueship.tri.rm.domainx.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts.AppendFileEnum;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.ItemLabelsUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.RmConverterAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.RmItemCheckAddonUtils;
import jp.co.blueship.tri.rm.beans.dto.RelApplySearchBean;
import jp.co.blueship.tri.rm.constants.RelApplyScreenID;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaAttachedFileEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean;
import jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.ra.dto.FlowRelApplyModifyServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.AppendFileBean;
import jp.co.blueship.tri.rm.domainx.ra.beans.dto.ReleaseRequestEditInputBean.SubmitMode;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean.ReleaseRequestDetailsView;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean.ReleaseRequestDetailsView.ReleaseRequestAttachmentFiles;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean.RequestOption;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestEditServiceBean.RequestParam;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestEditService implements IDomain<FlowReleaseRequestEditServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyEditSupport support = null;
	private IDomain<IGeneralServiceBean> modifyConfirmService = null;
	private IDomain<IGeneralServiceBean> modifySaveCompleteService = null;
	private IDomain<IGeneralServiceBean> modifyCompleteService = null;
	private IDomain<IGeneralServiceBean> modifyMailService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	public void setModifyConfirmService(IDomain<IGeneralServiceBean> modifyConfirmService) {
		this.modifyConfirmService = modifyConfirmService;
	}

	public void setModifySaveCompleteService(IDomain<IGeneralServiceBean> modifySaveCompleteService) {
		this.modifySaveCompleteService = modifySaveCompleteService;
	}

	public void setModifyCompleteService(IDomain<IGeneralServiceBean> modifyCompleteService) {
		this.modifyCompleteService = modifyCompleteService;
	}

	public void setModifyMailService(IDomain<IGeneralServiceBean> modifyMailService) {
		this.modifyMailService = modifyMailService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReleaseRequestEditServiceBean> execute(
			IServiceDto<FlowReleaseRequestEditServiceBean> serviceDto) {

		FlowReleaseRequestEditServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelApplyModifyServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String raId = paramBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "SelectedRaId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.createMockFiles(paramBean);
				if (SubmitMode.changes.equals(paramBean.getParam().getInputInfo().getSubmitMode())) {
					this.saveChanges(paramBean);

				} else if (SubmitMode.request.equals(paramBean.getParam().getInputInfo().getSubmitMode())) {
					this.submitChanges(paramBean);
				}
			}
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}

		return serviceDto;
	}

	private void init(FlowReleaseRequestEditServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);
		{
			serviceBean.setReferer(RelApplyScreenID.TOP);
			serviceBean.setForward(RelApplyScreenID.MODIFY);
			this.modifyService(serviceBean);
		}
		this.afterExecution(serviceBean, paramBean);
	}

	private void onChange(FlowReleaseRequestEditServiceBean paramBean) {
		if (RequestOption.deleteUplodedFile.equals(paramBean.getParam().getRequestOption())) {
			removeAttachedFile(paramBean);
			paramBean.getParam().setRequestOption(RequestOption.none);
			return;
		}
	}

	private void saveChanges(FlowReleaseRequestEditServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			IRaEntity raEntity = support.findRaEntity( paramBean.getParam().getSelectedRaId() );

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( raEntity.getLotId() )
			.setBpIds( support.getBuildNo(serviceBean.getRelApplyEntryViewBean().getUnitBeanList() ) )
			.setRaIds( paramBean.getParam().getSelectedRaId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		this.beforeExecution(paramBean, serviceBean);
		{
			serviceBean.setReferer(RelApplyScreenID.MODIFY);
			serviceBean.setForward(RelApplyScreenID.MODIFY_SAVE_COMP);
			this.modifyService(serviceBean);
			modifySaveCompleteService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void submitChanges(FlowReleaseRequestEditServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyModifyServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			IRaEntity raEntity = support.findRaEntity( paramBean.getParam().getSelectedRaId() );

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( raEntity.getLotId() )
			.setBpIds( support.getBuildNo(serviceBean.getRelApplyEntryViewBean().getUnitBeanList() ) )
			.setRaIds( paramBean.getParam().getSelectedRaId() );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}

		this.beforeExecution(paramBean, serviceBean);
		{
			serviceBean.setReferer(RelApplyScreenID.MODIFY);
			serviceBean.setForward(RelApplyScreenID.MODIFY_CONFIRM);
			this.modifyService(serviceBean);
			modifyConfirmService.execute(dto);
		}
		{
			serviceBean.setReferer(RelApplyScreenID.MODIFY_CONFIRM);
			serviceBean.setForward(RelApplyScreenID.MODIFY_COMP);
			this.modifyConfirmService(serviceBean);
			modifyCompleteService.execute(dto);
			modifyMailService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void beforeExecution(FlowReleaseRequestEditServiceBean src, FlowRelApplyModifyServiceBean dest) {
		src.getResult().setCompleted(false);
		RequestParam param = src.getParam();
		ReleaseRequestEditInputBean srcInfo = param.getInputInfo();

		IRaEntity raEntity = support.findRaEntity( param.getSelectedRaId() );

		if (RequestType.init.equals(param.getRequestType())) {
			dest.setScreenType(ScreenType.select);
			RelApplySearchBean relApplySearchBean = new RelApplySearchBean();
			relApplySearchBean.setSearchRelApplyNo(src.getParam().getSelectedRaId());
			dest.setRelApplySearchBean(relApplySearchBean);
		}

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {
			IUserEntity submitter = null;
			if(TriStringUtils.isNotEmpty(srcInfo.getSubmitterId())) {
				submitter = this.support.getUmFinderSupport().findUserByUserId( srcInfo.getSubmitterId() );
			}
			//IUserEntity supervisor = this.support.getUmFinderSupport().findUserByUserId( srcInfo.getSupervisor() );

			RelApplySearchBean relApplySearchBean = new RelApplySearchBean();
			relApplySearchBean.setSearchBuildNo			( srcInfo.getBpId() );
			relApplySearchBean.setSelectedRelWishDate	( srcInfo.getPreferredDate() );
			relApplySearchBean.setSelectedRelEnvNo		( srcInfo.getRelEnv() );
			relApplySearchBean.setSelectedGroupId		( srcInfo.getGroupId() );
			dest.setRelApplySearchBean(relApplySearchBean);

			RelApplyEntryViewBean relViewBean = new RelApplyEntryViewBean();
			relViewBean.setRelApplyDate			( TriDateUtils.convertViewDateFormat( src.getSystemDate() ) );
			relViewBean.setRelApplyGroupId		( srcInfo.getGroupId() );

			relViewBean.setRelApplyUserId		( srcInfo.getSubmitterId() );
			if (null != submitter) {
				relViewBean.setRelApplyUser		( submitter.getUserNm() );
			}

			relViewBean.setRelApplyPersonChargeId
												( srcInfo.getSupervisor() );
			
			relViewBean.setRelApplyPersonCharge	( srcInfo.getSupervisor() );
			

			relViewBean.setRelEnvNo				( srcInfo.getRelEnv() );
			relViewBean.setRemarks				( srcInfo.getRemarks() );
			relViewBean.setRelWishDate			( srcInfo.getPreferredDate() );
			relViewBean.setLotNo				( raEntity.getLotId() );
			relViewBean.setRelApplyNo			( param.getSelectedRaId() );
			relViewBean.setCtgId				( srcInfo.getCtgId() );
			relViewBean.setMstoneId				( srcInfo.getMstoneId() );
			relViewBean.setRelatedNo			( srcInfo.getRelatedId() );

			List<UnitBean> unitBeans = new ArrayList<UnitBean>();
			UnitBean ub = new UnitBean();
			ub.setBuildNo(srcInfo.getBpId());
			IBpEntity bpEntity = this.support.getBmFinderSupport().findBpEntity(srcInfo.getBpId());
			if (null != bpEntity) {
				ub.setBuildName( bpEntity.getBpNm() );
				ub.setBuildContent( bpEntity.getContent() );
				ub.setBuildSummary( bpEntity.getSummary() );
			}
			unitBeans.add(ub);
			relViewBean.setUnitBeanList(unitBeans);;
			relViewBean.setDraff(src.getDetailsView().isDraft());
			dest.setRelApplyEntryViewBean(relViewBean);
			dest.setUserId(src.getUserId());
			dest.setUserName(src.getUserName());
			dest.getRelApplyEntryViewBean().setAppendFile(this.getAppendFileBeanMap(srcInfo));
		}
	}

	private void afterExecution(FlowRelApplyModifyServiceBean src, FlowReleaseRequestEditServiceBean dest) {
		if (RequestType.init.equals(dest.getParam().getRequestType())) {
			this.generateLists(src, dest);
			this.setIsDraft( dest.getDetailsView(), dest.getParam().getSelectedRaId() );

			dest.getResult().setCompleted(true);
		}

		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.getResult().setCompleted(true);
			
			if (SubmitMode.changes.equals(dest.getParam().getInputInfo().getSubmitMode())) {
				if(dest.getDetailsView().isDraft()) {
					dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003015I);
				} else {
					dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003016I);
				}
			} else if (SubmitMode.request.equals(dest.getParam().getInputInfo().getSubmitMode())) {
				dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003023I);
			}
		}
	}

	private void generateLists(FlowRelApplyModifyServiceBean src, FlowReleaseRequestEditServiceBean dest) {
		ReleaseRequestEditInputBean inputInfo = dest.getParam().getInputInfo();
		RelApplyEntryViewBean relViewBean = src.getRelApplyEntryViewBean();

		inputInfo.setBpId			( relViewBean.getUnitBeanList().get(0).getBuildNo() );
		inputInfo.setBuildPackageViews(FluentList.from(relViewBean.getUnitBeanList()).map(RmFluentFunctionUtils.toItemLabelsFromUnitBean).asList() );
		inputInfo.setGroupId		( relViewBean.getRelApplyGroupId() );
		inputInfo.setPreferredDate	( relViewBean.getRelWishDate() );
		inputInfo.setRelEnv			( relViewBean.getRelEnvNo() );
		inputInfo.setSubmitterId	( relViewBean.getRelApplyUser() );
		inputInfo.setSupervisor		( relViewBean.getRelApplyPersonCharge() );
		inputInfo.setRemarks		( relViewBean.getRemarks() );
		inputInfo.setCtgId			( relViewBean.getCtgId() );
		inputInfo.setMstoneId		( relViewBean.getMstoneId() );
		inputInfo.setRelatedId		( relViewBean.getRelatedNo() );

		// get users list
		UserCondition userCondition = new UserCondition();
		List<IUserEntity> userEntityLis = this.support.getUmFinderSupport().getUserDao()
				.find(userCondition.getCondition());
		inputInfo.setSubmitterViews(
				FluentList.from(userEntityLis).map(UmFluentFunctionUtils.toItemLabelsFromUserEntity).asList());

		// group views
		inputInfo.setGroupViews(src.getGroupIdList());
		inputInfo.setSupervisorViews(
				FluentList.from(userEntityLis).map(UmFluentFunctionUtils.toItemLabelsFromUserEntity).asList());

		// get environment no list
		inputInfo.setReleaseEnvViews(src.getSearchRelEnvNoList());

		//get append files
		inputInfo.getAppendFile().addAll(this.getAppendFileList(src.getRelApplyEntryViewBean().getAppendFile(), dest));

		//get view for already appended files(mock files which we'll show in dropzone)
		dest.setDetailsView(this.getReleaseRequestEditView(inputInfo.getAppendFile()));

		String raId = dest.getParam().getSelectedRaId();

		IRaEntity raEntity = this.support.findRaEntity(raId);
		String lotId = raEntity.getLotId();

		// get categories
		List<ICtgEntity> ctgEntityList = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		inputInfo.setCtgViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());

		// get milestones
		List<IMstoneEntity> mstoneEntityList = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());

	}


	private void setIsDraft( ReleaseRequestDetailsView releaseRequestDetailsView, String raId ){

		IRaEntity entity = this.support.findRaEntity( raId );

		if( RmRaStatusId.DraftReleaseRequest.equals( entity.getStsId() ) ){
			releaseRequestDetailsView.setIsDraft( true );

		}else{
			releaseRequestDetailsView.setIsDraft( false );
		}
	}


	private ReleaseRequestDetailsView getReleaseRequestEditView(List<AppendFileBean> appendFileBeanList) {
		ReleaseRequestDetailsView releaseRequestEditView = new FlowReleaseRequestEditServiceBean().new ReleaseRequestDetailsView();
		List<ReleaseRequestAttachmentFiles> appendFileView = releaseRequestEditView.getAttachmentFiles();
		for (int i = 0; i < appendFileBeanList.size(); i++) {
			ReleaseRequestAttachmentFiles fileView = releaseRequestEditView.new ReleaseRequestAttachmentFiles();
			AppendFileBean fileBean = appendFileBeanList.get(i);
			fileView.setName(fileBean.getAppendFileNm());
			fileView.setSeqNo(fileBean.getSeqNo());

			File appendFile = new File(fileBean.getAppendFilePath());
			fileView.setSize(TriFileUtils.sizeOf(appendFile));
			appendFileView.add(fileView);
		}
		releaseRequestEditView.setAttachmentFiles(appendFileView);
		return releaseRequestEditView;
	}

	private List<AppendFileBean> getAppendFileList(
			Map<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> appendFileMap,
			FlowReleaseRequestEditServiceBean dest) {

		List<AppendFileBean> appendFileList = new ArrayList<AppendFileBean>();
		for (Entry<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> legacyAppendFileEntry : appendFileMap.entrySet()) {
			AppendFileBean appendFileBean = new ReleaseRequestEditInputBean().new AppendFileBean();
			appendFileBean
				.setAppendFileNm(legacyAppendFileEntry.getValue().getAppendFile())
				.setAppendFilePath(this.getFilePath(legacyAppendFileEntry.getValue().getAppendFile(), dest.getParam().getSelectedRaId()))
				.setSeqNo(legacyAppendFileEntry.getKey())
				.setAppendFileInputStreamBytes( TriFileUtils.readInputStreamBytes(appendFileBean.getAppendFilePath()) )
			;

			appendFileList.add(appendFileBean);
		}

		return appendFileList;
	}

	private String getFilePath(String appendFileNm, String raId) {
		IRaEntity raEntity = this.support.findRaEntity(raId);

		IRaDto raDto = this.support.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
		List<IRaAttachedFileEntity> files = raDto.getRaAttachedFileEntities();
		String dir = "";
		for (IRaAttachedFileEntity file : files) {
			if(file.getFilePath().equals(appendFileNm)) {
				dir = file.getAttachedFileSeqNo();
			}
		}

		String basePath = sheet.getValue( RmDesignEntryKeyByRelApply.relApplyAppendFilePath );

		String raPath = TriStringUtils.linkPathBySlash(basePath, raId);

		String uniqueKey = "";
		String uniquePath = TriStringUtils.linkPathBySlash( raPath , uniqueKey ) ;

		String dirPath = TriStringUtils.linkPathBySlash(uniquePath, dir);
		String filePath = TriStringUtils.linkPathBySlash(dirPath, appendFileNm);

		return filePath;
	}

	private void removeAttachedFile(FlowReleaseRequestEditServiceBean paramBean) {
		String deletedFileNm = paramBean.getParam().getInputInfo().getDeletedFileNm();
		List<AppendFileBean> appendFilesList =  paramBean.getParam().getInputInfo().getAppendFile();
		for(int i = 0; i < appendFilesList.size(); i++) {
			if(appendFilesList.get(i).getAppendFileNm().equals(deletedFileNm)) {
				appendFilesList.remove(i);
				return;
			}
		}
	}

	private Map<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> getAppendFileBeanMap(ReleaseRequestEditInputBean srcInfo) {
		Map<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean> appendFileMap =
				new TreeMap<String, jp.co.blueship.tri.rm.domain.ra.beans.dto.RelApplyEntryViewBean.AppendFileBean>();

		RelApplyEntryViewBean entryViewBean = new RelApplyEntryViewBean();
		for ( SessionScopeKeyConsts.AppendFileEnum appendFile: SessionScopeKeyConsts.AppendFileEnum.values() ) {
			List<AppendFileBean> appendFileBeanList = srcInfo.getAppendFile();
			int id = Integer.valueOf(appendFile.getId()) - 1;
			if ( id >= appendFileBeanList.size()) {
				break;
			}
			AppendFileBean srcAppendFileBean = appendFileBeanList.get(id);
			if (TriStringUtils.isEmpty(srcAppendFileBean)) {
				continue;
			}

			RelApplyEntryViewBean.AppendFileBean destAppendFileBean = entryViewBean.newAppendFileBean();
			destAppendFileBean.setAppendFileInputStreamBytes( srcAppendFileBean.getAppendFileInputStreamBytes() );
			destAppendFileBean.setAppendFileInputStreamName( srcAppendFileBean.getAppendFileNm() );
			destAppendFileBean.setAppendFile( srcAppendFileBean.getAppendFileNm() );

			appendFileMap.put(appendFile.getId(), destAppendFileBean);
		}
		return appendFileMap;
	}

	private void modifyService(FlowRelApplyModifyServiceBean paramBean) {
		String refererID	= paramBean.getReferer();
		String forwardID	= paramBean.getForward();
		String screenType	= paramBean.getScreenType();

		if( refererID.equals( RelApplyScreenID.MODIFY )) {
			if ( ScreenType.next.equals( screenType ) ) {
				if ( RelApplyScreenID.SAVE_COMP.equals(forwardID) ) {
					RmItemCheckAddonUtils.checkRelApplyModifyInput(
							RmDesignBeanId.flowRelApplySaveInputCheck,
							paramBean.getRelApplySearchBean(),
							paramBean.getRelApplyEntryViewBean() );
				} else {
					RmItemCheckAddonUtils.checkRelApplyModifyInput(
							RmDesignBeanId.flowRelApplyInputCheck,
							paramBean.getRelApplySearchBean(),
							paramBean.getRelApplyEntryViewBean() );
				}

				this.checkRelApplyModifyConfirm(
						support,
						support.getUmFinderSupport(),
						paramBean.getUserId(),
						paramBean.getRelApplySearchBean().getSelectedGroupId(),
						paramBean.getRelApplySearchBean().getSelectedRelEnvNo(),
						paramBean.getRelApplySearchBean().getSelectedRelWishDate(),
						paramBean.getRelApplyEntryViewBean());
			}
		}

		if ( refererID.equals( RelApplyScreenID.TOP )
				&& forwardID.equals( RelApplyScreenID.MODIFY ) ) {
			if ( ! ScreenType.bussinessException.equals( screenType ) ) {

				String relApplyNo = paramBean.getRelApplySearchBean().getSearchRelApplyNo();
				IRaDto raDto = this.support.findRaDto(relApplyNo);
				ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( raDto.getRaEntity().getLotId() );
				List<ILotDto> lotDtoEntities = new ArrayList<ILotDto>();
				ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( pjtLotEntity.getLotId() );
				lotDtoEntities.add(lotDto);

				String lotName = pjtLotEntity.getLotNm();

				RelApplyEntryViewBean bean = new RelApplyEntryViewBean();
				List<UnitBean> unitBeans = new ArrayList<UnitBean>();
				
				IBpDao buildDao = this.support.getBmFinderSupport().getBpDao();
				int pageNo = 1;
				int viewRows = 0;
				ISqlSort buildSort	= (ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelApplyPackageListSelect();
				IJdbcCondition buildCondition = DBSearchConditionAddonUtil.getBuildConditionByReleasableRelUnit( new String[]{raDto.getRaEntity().getLotId()} ) ;
				List<IBpEntity> buildEntityArray = buildDao.find( buildCondition.getCondition() , buildSort , pageNo, viewRows ).getEntities();
				for (IBpEntity bpEntity : buildEntityArray) {

					UnitBean unitBean = new UnitBean();
					unitBean.setBuildNo		( bpEntity.getBpId());
					unitBean.setBuildName		( bpEntity.getBpNm() );

					unitBeans.add(unitBean);
				}
				
				/*
				 * 表示項目構築
				 */
				IRaEntity raEntity = raDto.getRaEntity();
				List<IAreqEntity> areqEntities = support.findAreqEntitiesFromRaBpLnk( raDto.getRaBpLnkEntities() );

				ICtgLnkEntity ctgLnkEntity = support.getUmFinderSupport().findCtgLnkByPrimaryKey(raEntity.getRaId(), RmTables.RM_RA.name());
				String ctgId = null;
				if( ctgLnkEntity != null ) {
					ctgId = ctgLnkEntity.getCtgId();
				}

				IMstoneLnkEntity mstoneLnkEntity = support.getUmFinderSupport().findMstoneLnkByPrimaryKey( raEntity.getRaId(), RmTables.RM_RA.name() );
				String mstoneId = null;
				if ( mstoneLnkEntity != null ) {
					mstoneId = mstoneLnkEntity.getMstoneId();
				}

				bean.setRelApplyNo					( raEntity.getRaId() );
				bean.setLotNo						( raEntity.getLotId() );
				bean.setLotName						( lotName );
				paramBean.setSystemDate				( TriDateUtils.getSystemTimestamp() );
				bean.setRelApplyDate				( TriDateUtils.convertViewDateFormat( paramBean.getSystemDate() ) ) ;
				bean.setRelApplyGroupId				( raEntity.getReqGrpId() );
				bean.setRelEnvNo					( raEntity.getBldEnvId() );
				bean.setRelEnvName					( raEntity.getBldEnvNm() );
				bean.setRelApplyUser				( raEntity.getReqUser() );
				bean.setRelApplyUserId				( raEntity.getReqUserId() );
				bean.setRelApplyPersonCharge		( raEntity.getReqResponsibility() );
				bean.setRelApplyPersonChargeId		( raEntity.getReqResponsibilityId() );
				bean.setRelWishDate					( raEntity.getPreferredRelDate() );
				bean.setUnitBeanList				( unitBeans );				
				bean.setChangeCauseNoList			( RmConverterAddonUtils.convertChangeCauseNoList( areqEntities ) );
				bean.setAssetApplyNoList			( RmConverterAddonUtils.convertAssetApplyNoList( areqEntities ) );
				bean.setRelatedNo					( raEntity.getRelationId() );
				bean.setRemarks						( raEntity.getRemarks() );
				bean.setAppendFile					( this.getAppendFileMap( raDto.getRaAttachedFileEntities() , bean) );
				bean.setCtgId						( ctgId );
				bean.setMstoneId					( mstoneId );

				/*
				 * 選択可能なリリース環境リスト取得
				 */
				IBldEnvEntity[] envAllEntities = this.support.getRelEnvEntityForPullDown() ;
				IBldEnvEntity[] envEntities = this.support.getAccessableRelEnvEntity( envAllEntities, lotDtoEntities );

				paramBean.setSearchRelEnvNoList( ItemLabelsUtils.conv(envEntities) );
				paramBean.getRelApplySearchBean().setSelectedRelEnvNo( bean.getRelEnvNo() );

				/*
				 * 選択可能なリリース希望日リスト取得
				 */
				ICalEntity[] relCalendarEntitys = this.support.getRelCalendarEntityForPullDown() ;

				paramBean.setSearchRelWishDateList( ItemLabelsUtils.conv(relCalendarEntitys) );
				paramBean.getRelApplySearchBean().setSelectedRelWishDate( bean.getRelWishDate() );

				/*
				 * 選択可能なグループリスト取得
				 */
				List<IGrpUserLnkEntity> groupUserview = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				paramBean.setGroupIdList( ItemLabelsUtils.conv( support.getUmFinderSupport(), groupUserview ) );
				paramBean.getRelApplySearchBean().setSelectedGroupId( bean.getRelApplyGroupId() );

				/*
				 * 戻りに設定
				 */
				paramBean.setRelApplyEntryViewBean(bean);
			}
		}
	}

	private void modifyConfirmService (FlowRelApplyModifyServiceBean paramBean) {
		RelApplyEntryViewBean viewBean = paramBean.getRelApplyEntryViewBean();

		this.checkRelApplyModifyConfirm(
				support,
				support.getUmFinderSupport(),
				paramBean.getUserId(),
				paramBean.getRelApplySearchBean().getSelectedGroupId(),
				paramBean.getRelApplySearchBean().getSelectedRelEnvNo(),
				paramBean.getRelApplySearchBean().getSelectedRelWishDate(),
				paramBean.getRelApplyEntryViewBean());

		StatusCheckDto statusDto = new StatusCheckDto()
		.setServiceBean( paramBean )
		.setFinder( support )
		.setActionList( statusMatrixAction )
		.setLotIds( viewBean.getLotNo() )
		.setBpIds( support.getBuildNo(viewBean.getUnitBeanList() ) )
		.setRaIds( viewBean.getRelApplyNo() );

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
	}

	private void checkRelApplyModifyConfirm(
			FlowRelApplyEditSupport support,
			IUmFinderSupport umFinderSupport,
			String userId,
			String groupId,
			String relEnvNo,
			String relWishDate,
			RelApplyEntryViewBean viewBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			//申請グループ
			IGrpUserLnkEntity groupUserview = null;
			List<IGrpUserLnkEntity> groupUserviews = umFinderSupport.findGrpUserLnkByUserId( userId );
			IGrpEntity grpEntity = null;
			for (IGrpUserLnkEntity view : groupUserviews) {

				if(groupId.equals( view.getGrpId() ) ) {

					groupUserview = view;
					grpEntity = umFinderSupport.findGroupById( view.getGrpId() );
					break;
				}
			}
			if (  !viewBean.isDraff() &&  groupUserview == null ) {
				messageList.add		( RmMessageId.RM001005E );
				messageArgsList.add	( new String[] {} );
				grpEntity.setGrpNm("");
			}

			//リリース環境
			IBldEnvEntity relEnvEntity = null;
			if (TriStringUtils.isNotEmpty(relEnvNo)) {
				BldEnvCondition bcondition = new BldEnvCondition();
				bcondition.setBldEnvId(relEnvNo);
				relEnvEntity = support.getBmFinderSupport().getBldEnvDao().find( bcondition.getCondition() ).get(0);
			}
			if ( null == relEnvEntity || StatusFlg.on.value().equals(relEnvEntity.getDelStsId().value()) ) {
				if (!viewBean.isDraff() ) {
					messageList.add		( RmMessageId.RM001006E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//リリース希望日
			if (  !viewBean.isDraff() &&  TriStringUtils.isEmpty(relWishDate) ) {
				messageList.add		( RmMessageId.RM001007E );
				messageArgsList.add	( new String[] {} );
			}

			//ビルドパッケージ
			{
				Set<String> buildNoSet = new LinkedHashSet<String>();
				for (jp.co.blueship.tri.rm.domain.ra.beans.dto.UnitBean unitBean : viewBean.getUnitBeanList()) {
					buildNoSet.add(unitBean.getBuildNo());
				}

				BpCondition condition = new BpCondition();
				condition.setBpIds( buildNoSet.toArray(new String[0]) );

				if ( buildNoSet.size() != support.getBmFinderSupport().getBpDao().count(condition.getCondition()) ) {
					messageList.add		( RmMessageId.RM001015E );
					messageArgsList.add	( new String[] {} );
				}
			}

			//選択項目の移し換え
			if ( null != groupUserview ) {
				viewBean.setRelApplyGroupId( groupUserview.getGrpId() );
			}
			if ( null != grpEntity ) {
				viewBean.setRelApplyGroup( grpEntity.getGrpNm() );
			}
			if ( null != relEnvEntity ) {
				viewBean.setRelEnvNo( relEnvEntity.getBldEnvId() );
				viewBean.setRelEnvName( relEnvEntity.getBldEnvNm() );
			}
			if ( null != relWishDate ) {
				viewBean.setRelWishDate( relWishDate );
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Create mock files which we'll show in Dropzone in case if any error occurs during of execute time.
	 * @param paramBean
	 */
	private void createMockFiles(FlowReleaseRequestEditServiceBean paramBean) {
		List<ReleaseRequestAttachmentFiles> mockFiles = new ArrayList<ReleaseRequestAttachmentFiles>();
		for(AppendFileBean fileBean : paramBean.getParam().getInputInfo().getAppendFile()) {
			ReleaseRequestAttachmentFiles mockFile = paramBean.getDetailsView().new ReleaseRequestAttachmentFiles();
			mockFile.setName(fileBean.getAppendFileNm());
			mockFile.setSeqNo(fileBean.getSeqNo());
			mockFile.setSize(fileBean.getAppendFileInputStreamBytes().length);

			mockFiles.add(mockFile);
		}
		paramBean.getDetailsView().setAttachmentFiles(mockFiles);
	}

	/**
	 * 添付ファイルマップを取得する
	 *
	 * @param entities
	 * @param bean
	 * @return
	 */
	private Map<String, RelApplyEntryViewBean.AppendFileBean>
		getAppendFileMap(List<IRaAttachedFileEntity> entities, RelApplyEntryViewBean bean) {

		Map<String, RelApplyEntryViewBean.AppendFileBean> appendFile = bean.newAppendFile();
		if( entities == null ) {
			return appendFile;
		}

		Integer maxId = 1;
		for (IRaAttachedFileEntity entity : entities) {

			RelApplyEntryViewBean.AppendFileBean appendFileBean = bean.newAppendFileBean();
			appendFileBean.setAppendFile( entity.getFilePath() );

			appendFile.put(entity.getAttachedFileSeqNo(), appendFileBean);

			if ( maxId < Integer.valueOf(entity.getAttachedFileSeqNo()) ) {
				maxId = Integer.valueOf(entity.getAttachedFileSeqNo());
			}
		}

		Map<String, String> showMap = bean.getShowAppendFile();

		showMap.put( AppendFileEnum.AppendFile1.getId(), "1" );
		showMap.put( AppendFileEnum.AppendFile2.getId(), ( 1 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile3.getId(), ( 2 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile4.getId(), ( 3 < maxId )?  "1": "0" );
		showMap.put( AppendFileEnum.AppendFile5.getId(), ( 4 < maxId )?  "1": "0" );

		return appendFile;
	}
}
