package jp.co.blueship.tri.rm.domainx.ra;

import java.text.SimpleDateFormat;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestOverviewServiceBean;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestOverviewServiceBean.ReleaseRequestOverview;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * @version V4.00.00
 * @author Chung
 */
public class FlowReleaseRequestOverviewService  implements IDomain<FlowReleaseRequestOverviewServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleaseRequestOverviewServiceBean> execute(
			IServiceDto<FlowReleaseRequestOverviewServiceBean> serviceDto) {

		FlowReleaseRequestOverviewServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf( serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "SelectedRaId is not specified");

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void init(FlowReleaseRequestOverviewServiceBean serviceBean){

		String raId = serviceBean.getParam().getSelectedRaId();

		//Check group accessibility
		IRaEntity raEntity = this.support.findRaEntity(raId);
		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(raEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

		AmLibraryAddonUtils.checkAccessableGroup( lotDto,
				  this.support.getUmFinderSupport().getGrpDao(),
				  this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()) );

		//Get Release Request Details
		ReleaseRequestOverview releaseRequestView = this.getReleaseRequestOverview(serviceBean, raEntity);

		//Set Release Request Details
		serviceBean.setDetailsView(releaseRequestView);
	}


	private ReleaseRequestOverview getReleaseRequestOverview(FlowReleaseRequestOverviewServiceBean serviceBean,
															 IRaEntity raEntity){

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone());

		ReleaseRequestOverview view = serviceBean.new ReleaseRequestOverview()
									.setStsId		( raEntity.getProcStsId() )
									.setStatus		( sheet.getValue(RmDesignBeanId.statusId, raEntity.getProcStsId()) )
									.setSubject		( raEntity.getRemarks() )
									.setCreatedBy	( raEntity.getRegUserNm() )
									.setEndTime		( TriDateUtils.convertViewDateFormat(raEntity.getCloseTimestamp(), format) )
									.setStartTime	( TriDateUtils.convertViewDateFormat(raEntity.getRegTimestamp(),format) )
									;

		return view;
	}
}