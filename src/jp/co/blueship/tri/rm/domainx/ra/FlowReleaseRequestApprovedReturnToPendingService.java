package jp.co.blueship.tri.rm.domainx.ra;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.NullTimestamp;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.domainx.ra.dto.FlowReleaseRequestApprovedReturnToPendingServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowReleaseRequestApprovedReturnToPendingService implements IDomain<FlowReleaseRequestApprovedReturnToPendingServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelApplyEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelApplyEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReleaseRequestApprovedReturnToPendingServiceBean> execute(
			IServiceDto<FlowReleaseRequestApprovedReturnToPendingServiceBean> serviceDto) {

		FlowReleaseRequestApprovedReturnToPendingServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			String raId = serviceBean.getParam().getSelectedRaId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(raId), "Seleceted RaId is not specified");

			if (RequestType.validate.equals(serviceBean.getParam().getRequestType())) {
				this.validate(serviceBean);
			}

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {
				this.submitChanges(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void validate(FlowReleaseRequestApprovedReturnToPendingServiceBean serviceBean) {
		String raId = serviceBean.getParam().getSelectedRaId();

		IRaEntity raEntity = this.support.findRaEntity(raId);

		// Status Matrix Check
		{
			List<IRaDto> raDtoList = new ArrayList<IRaDto>();
			raDtoList.add( this.support.findRaDto( raEntity ) );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( serviceBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( raEntity.getLotId() )
					.setRaIds		( raEntity.getRaId() )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );
		}
	}

	private void submitChanges(FlowReleaseRequestApprovedReturnToPendingServiceBean serviceBean) {
		String raId = serviceBean.getParam().getSelectedRaId();
		IRaEntity raEntity = this.support.findRaEntity(raId);

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( serviceBean )
				.setFinder		( support )
				.setActionList	( statusMatrixAction )
				.setLotIds		( raEntity.getLotId() )
				.setRaIds		( raEntity.getRaId() )
				;
		StatusMatrixCheckUtils.checkStatusMatrix( statusDto  );

		raEntity.setStsId(RmRaStatusId.ReleaseRequested.getStatusId());
		raEntity.setAvlUserId("");
		raEntity.setAvlUserNm("");
		raEntity.setAvlTimestamp(new NullTimestamp());
		raEntity.setAvlCancelUserId(serviceBean.getUserId());
		raEntity.setAvlCancelUserNm(serviceBean.getUserName());
		raEntity.setAvlCancelTimestamp(TriDateUtils.getSystemTimestamp());
		this.support.getRaDao().update(raEntity);

		this.support.getSmFinderSupport().cleaningExecDataSts(RmTables.RM_RA, raId);
		this.support.getSmFinderSupport().registerExecDataSts(
				serviceBean.getProcId(),
				RmTables.RM_RA,
				RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled,
				raId);

		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(RmMessageId.RM003013I, raId);
	}

}
