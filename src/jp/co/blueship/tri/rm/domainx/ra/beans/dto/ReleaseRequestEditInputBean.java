package jp.co.blueship.tri.rm.domainx.ra.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class ReleaseRequestEditInputBean {
	private String submitterId;
	private String groupId;
	private String supervisor;
	private String relEnv;
	private String preferredDate;
	private String bpId;
	private String relatedId;
	private String remarks;
	private String ctgId;
	private String mstoneId;
	private SubmitMode submitMode = SubmitMode.none;
	private List<AppendFileBean> appendFile = new ArrayList<AppendFileBean>();
	private String deletedFileNm;
	private String deletedFileSeqNo;

	private List<ItemLabelsBean> submitterViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> supervisorViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> buildPackageViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
	private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

	public String getSubmitterId() {
		return submitterId;
	}
	public ReleaseRequestEditInputBean setSubmitterId(String submitterId) {
		this.submitterId = submitterId;
		return this;
	}

	public String getGroupId() {
		return groupId;
	}
	public ReleaseRequestEditInputBean setGroupId(String groupId) {
		this.groupId = groupId;
		return this;
	}

	public String getSupervisor() {
		return supervisor;
	}
	public ReleaseRequestEditInputBean setSupervisor(String supervisor) {
		this.supervisor = supervisor;
		return this;
	}

	public String getRelEnv() {
		return relEnv;
	}
	public ReleaseRequestEditInputBean setRelEnv(String relEnv) {
		this.relEnv = relEnv;
		return this;
	}

	public String getPreferredDate() {
		return preferredDate;
	}
	public ReleaseRequestEditInputBean setPreferredDate(String preferredDate) {
		this.preferredDate = preferredDate;
		return this;
	}

	public String getBpId() {
		return bpId;
	}
	public ReleaseRequestEditInputBean setBpId(String bpId) {
		this.bpId = bpId;
		return this;
	}

	public String getRelatedId() {
		return relatedId;
	}

	public ReleaseRequestEditInputBean setRelatedId(String relatedId) {
		this.relatedId = relatedId;
		return this;
	}

	public String getRemarks() {
		return remarks;
	}
	public ReleaseRequestEditInputBean setRemarks(String remarks) {
		this.remarks = remarks;
		return this;
	}

	public String getCtgId() {
		return ctgId;
	}
	public ReleaseRequestEditInputBean setCtgId(String ctgId) {
		this.ctgId = ctgId;
		return this;
	}

	public String getMstoneId() {
		return mstoneId;
	}
	public ReleaseRequestEditInputBean setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		return this;
	}

	public SubmitMode getSubmitMode() {
		return submitMode;
	}
	public ReleaseRequestEditInputBean setSubmitMode(SubmitMode submitMode) {
		this.submitMode = submitMode;
		return this;
	}

	public List<AppendFileBean> getAppendFile() {
		return appendFile;
	}
	public ReleaseRequestEditInputBean setAppendFile(List<AppendFileBean> appendFile) {
		this.appendFile = appendFile;
		return this;
	}

	public List<ItemLabelsBean> getSubmitterViews() {
		return submitterViews;
	}
	public ReleaseRequestEditInputBean setSubmitterViews(List<ItemLabelsBean> submitterViews) {
		this.submitterViews = submitterViews;
		return this;
	}

	public List<ItemLabelsBean> getGroupViews() {
		return groupViews;
	}
	public ReleaseRequestEditInputBean setGroupViews(List<ItemLabelsBean> groupViews) {
		this.groupViews = groupViews;
		return this;
	}

	public List<ItemLabelsBean> getSupervisorViews() {
		return supervisorViews;
	}
	public ReleaseRequestEditInputBean setSupervisorViews(List<ItemLabelsBean> supervisorViews) {
		this.supervisorViews = supervisorViews;
		return this;
	}

	public List<ItemLabelsBean> getBuildPackageViews() {
		return buildPackageViews;
	}
	public ReleaseRequestEditInputBean setBuildPackageViews(List<ItemLabelsBean> buildPackageViews) {
		this.buildPackageViews = buildPackageViews;
		return this;
	}

	public List<ItemLabelsBean> getReleaseEnvViews() {
		return releaseEnvViews;
	}
	public ReleaseRequestEditInputBean setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
		this.releaseEnvViews = releaseEnvViews;
		return this;
	}

	public List<ItemLabelsBean> getCtgViews() {
		return ctgViews;
	}
	public ReleaseRequestEditInputBean setCtgViews(List<ItemLabelsBean> ctgViews) {
		this.ctgViews = ctgViews;
		return this;
	}

	public List<ItemLabelsBean> getMstoneViews() {
		return mstoneViews;
	}
	public ReleaseRequestEditInputBean setMstoneViews(List<ItemLabelsBean> mstoneViews) {
		this.mstoneViews = mstoneViews;
		return this;
	}

	public String getDeletedFileNm() {
		return deletedFileNm;
	}
	public ReleaseRequestEditInputBean setDeletedFileNm(String deletedFileNm) {
		this.deletedFileNm = deletedFileNm;
		return this;
	}

	public String getDeletedFileSeqNo() {
		return deletedFileSeqNo;
	}
	public ReleaseRequestEditInputBean setDeletedFileSeqNo(String deletedFileSeqNo) {
		this.deletedFileSeqNo = deletedFileSeqNo;
		return this;
	}

	public class AppendFileBean {
		private String appendFileNm;
		private String seqNo;
		private String appendFilePath;
		private byte[] appendFileInputStreamBytes;

		public String getAppendFileNm() {
			return appendFileNm;
		}

		public AppendFileBean setAppendFileNm(String appendFileNm) {
			this.appendFileNm = appendFileNm;
			return this;
		}

		public String getSeqNo() {
			return seqNo;
		}

		public AppendFileBean setSeqNo(String seqNo) {
			this.seqNo = seqNo;
			return this;
		}

		public String getAppendFilePath() {
			return appendFilePath;
		}

		public AppendFileBean setAppendFilePath(String appendFilePath) {
			this.appendFilePath = appendFilePath;
			return this;
		}

		public byte[] getAppendFileInputStreamBytes() {
			return appendFileInputStreamBytes;
		}

		public AppendFileBean setAppendFileInputStreamBytes(byte[] appendFileInputStreamBytes) {
			this.appendFileInputStreamBytes = appendFileInputStreamBytes;
			return this;
		}

	}

	public enum SubmitMode {
		none( "" ),
		/**
		 * CreateService Only
		 */
		draft( "draft" ),
		/**
		 * CreateService / EditService
		 */
		request( "request" ),
		/**
		 * EditService Only
		 */
		changes ( "changes" );

		private String value = null;

		private SubmitMode( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SubmitMode mode = value( value );

			if ( null == mode ) return false;
			if ( ! this.equals(mode) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SubmitMode value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SubmitMode mode : values() ) {
				if ( mode.value().equals( value ) ) {
					return mode;
				}
			}

			return none;
		}
	}
}
