package jp.co.blueship.tri.rm.domainx.ra.dto;

import java.io.File;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowReleaseRequestAttachmentDownloadServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private AttachmentFileResponse fileResponse = new AttachmentFileResponse();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleaseRequestAttachmentDownloadServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public AttachmentFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReleaseRequestAttachmentDownloadServiceBean setFileResponse(AttachmentFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String raId;
		private String fileNm;

		public String getSelectedRaId() {
			return raId;
		}
		public RequestParam setSelectedRaId(String raId) {
			this.raId = raId;
			return this;
		}

		public String getSelectedFileNm() {
			return fileNm;
		}
		public RequestParam setSelectedFileNm(String fileNm) {
			this.fileNm = fileNm;
			return this;
		}
	}

	public class AttachmentFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
