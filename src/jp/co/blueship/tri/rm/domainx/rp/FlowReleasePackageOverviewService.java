package jp.co.blueship.tri.rm.domainx.rp;

import java.text.SimpleDateFormat;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageOverviewServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageOverviewServiceBean.ReleasePackageOverview;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowReleasePackageOverviewService implements IDomain<FlowReleasePackageOverviewServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleasePackageOverviewServiceBean> execute(
			IServiceDto<FlowReleasePackageOverviewServiceBean> serviceDto) {

		FlowReleasePackageOverviewServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf( serviceBean != null, "ServiceBean is not specified");

			String rpId = serviceBean.getParam().getSelectedRpId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(rpId), "SelectedRpId is not specified");

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType()))
				this.init(serviceBean);

			if ( RequestType.onChange.equals(serviceBean.getParam().getRequestType()))
				this.init(serviceBean);


			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private void init(FlowReleasePackageOverviewServiceBean serviceBean){

		String rpId = serviceBean.getParam().getSelectedRpId();

		//Check group accessibility
		IRpEntity rpEntity = this.support.findRpEntity(rpId);
		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(rpEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

		AmLibraryAddonUtils.checkAccessableGroup( lotDto,
				  this.support.getUmFinderSupport().getGrpDao(),
				  this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()) );

		//Get Release Package Details
		ReleasePackageOverview releasePackageView = this.getReleasePackageOverview(serviceBean, rpEntity);

		//Set Release Package Details
		serviceBean.setDetailsView(releasePackageView);
	}


	private ReleasePackageOverview getReleasePackageOverview(FlowReleasePackageOverviewServiceBean serviceBean,
															 IRpEntity rpEntity){

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone());

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());

		ReleasePackageOverview view = serviceBean.new ReleasePackageOverview()
									.setStsId		( rpEntity.getProcStsId() )
									.setStatus		( sheet.getValue(RmDesignBeanId.statusId, rpEntity.getProcStsId()) )
									.setSubject		( rpEntity.getSummary() )
									.setEnvId		( rpEntity.getBldEnvId() )
									.setEnvNm		( bldEnvEntity.getBldEnvNm() )
									.setCreatedBy	( rpEntity.getRegUserNm() )
									.setEndTime		( TriDateUtils.convertViewDateFormat(rpEntity.getProcEndTimestamp(), format) )
									.setStartTime	( TriDateUtils.convertViewDateFormat(rpEntity.getProcStTimestamp(),format) )
									;

		return view;
	}
}
