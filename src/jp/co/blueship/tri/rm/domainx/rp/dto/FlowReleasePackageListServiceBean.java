package jp.co.blueship.tri.rm.domainx.rp.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
public class FlowReleasePackageListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ReleasePackageView> releasePackageViews = new ArrayList<ReleasePackageView>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<ReleasePackageView> getReleasePackageViews() {
		return releasePackageViews;
	}
	public FlowReleasePackageListServiceBean setReleasePackageViews( List<ReleasePackageView> releasePackageViews ) {
		this.releasePackageViews = releasePackageViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReleasePackageListServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchServiceType searchServiceType = SearchServiceType.none;
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();
		private int linesPerPage = 20;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
		
		public SearchServiceType getSearchServiceType() {
			return searchServiceType;
		}

		public RequestParam setSearchServiceType(SearchServiceType searchServiceType) {
			this.searchServiceType = searchServiceType;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		@Expose private String envId = null;
		@Expose private String stsId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getEnvId() {
			return envId;
		}
		public SearchCondition setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public SearchCondition setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}

	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder rpId = TriSortOrder.Desc;

		public TriSortOrder getRpId() {
			return rpId;
		}
		public OrderBy setRpId(TriSortOrder rpId) {
			this.rpId = rpId;
			return this;
		}
	}

	/**
	 * Release Package
	 */
	public class ReleasePackageView {
		private boolean isSuccess = false;

		private String rpId;
		private String bpId;
		private String envId;
		private String envNm;
		private String subject;
		private String createdBy;
		private String createdByIconPath;
		private String createdDate;
		private String updDate;
		private String stsId;
		private String status;
		private String startTime;
		private String endTime;

		public String getStartTime() {
			return startTime;
		}
		public ReleasePackageView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}
		
		public String getEndTime() {
			return endTime;
		}
		public ReleasePackageView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}
		
		public boolean isError() {
			return !isSuccess;
		}
		public boolean isSuccess() {
			return isSuccess;
		}
		public ReleasePackageView setSuccess(boolean isSuccess) {
			this.isSuccess = isSuccess;
			return this;
		}

		public String getRpId() {
			return rpId;
		}
		public ReleasePackageView setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public ReleasePackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public ReleasePackageView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvironmentNm() {
			return envNm;
		}
		public ReleasePackageView setEnvironmentNm(String envNm) {
			this.envNm = envNm;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public ReleasePackageView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public ReleasePackageView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public ReleasePackageView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}

		public String getCreatedDate() {
			return createdDate;
		}
		public ReleasePackageView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public String getUpdDate() {
			return updDate;
		}
		public ReleasePackageView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReleasePackageView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleasePackageView setStatus(String status) {
			this.status = status;
			return this;
		}

	}
	
	public enum SearchServiceType {
		none( "" ),
		SearchSite( "SearchSite" ),
		SearchLot( "SearchLot" ),
		;

		private String value = null;

		private SearchServiceType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SearchServiceType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SearchServiceType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SearchServiceType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

}
