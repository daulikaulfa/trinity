package jp.co.blueship.tri.rm.domainx.rp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowReleasePackageCreationServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowRelCtlEntryServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleasePackageCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ReleasePackageCreationInputInfo inputInfo = new ReleasePackageCreationInputInfo();
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ReleasePackageCreationInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ReleasePackageCreationInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none,
		selectReleaseRequest,
		;
	}

	/**
	 * Input Information
	 */
	public class ReleasePackageCreationInputInfo {
		private String subject;
		private String summary;
		private String raId;
		private String envId;
		private String bpId;
		private String ctgId;
		private String mstoneId;
		private List<ItemLabelsBean> releaseRequestViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> releaseEnvViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> buildPackageViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getSubject() {
			return subject;
		}
		public ReleasePackageCreationInputInfo setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public ReleasePackageCreationInputInfo setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		/**
		 * @return Release Request ID
		 */
		public String getRaId() {
			return raId;
		}
		/**
		 * @param raId Release Request ID
		 * @return
		 */
		public ReleasePackageCreationInputInfo setRaId(String raId) {
			this.raId = raId;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public ReleasePackageCreationInputInfo setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public ReleasePackageCreationInputInfo setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public ReleasePackageCreationInputInfo setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public ReleasePackageCreationInputInfo setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public List<ItemLabelsBean> getReleaseRequestViews() {
			return releaseRequestViews;
		}

		public ReleasePackageCreationInputInfo setReleaseRequestViews(List<ItemLabelsBean> releaseRequestViews) {
			this.releaseRequestViews = releaseRequestViews;
			return this;
		}

		public List<ItemLabelsBean> getReleaseEnvViews() {
			return releaseEnvViews;
		}
		public ReleasePackageCreationInputInfo setReleaseEnvViews(List<ItemLabelsBean> releaseEnvViews) {
			this.releaseEnvViews = releaseEnvViews;
			return this;
		}

		public List<ItemLabelsBean> getBuildPackageViews() {
			return buildPackageViews;
		}

		public ReleasePackageCreationInputInfo setBuildPackageViews(List<ItemLabelsBean> buildPackageViews) {
			this.buildPackageViews = buildPackageViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public ReleasePackageCreationInputInfo setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public ReleasePackageCreationInputInfo setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}

	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String rpId;
		private boolean completed = false;

		public String getRpId() {
			return rpId;
		}
		public RequestsCompletion setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}
		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

	}
}
