package jp.co.blueship.tri.rm.domainx.rp;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.CancelInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlCancelServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageRemovalServiceBean;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageRemovalService implements IDomain<FlowReleasePackageRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> confirmService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private FlowRelCtlEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setSupport( FlowRelCtlEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setConfirmService(IDomain<IGeneralServiceBean> confirmService) {
		this.confirmService = confirmService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowReleasePackageRemovalServiceBean> execute(
			IServiceDto<FlowReleasePackageRemovalServiceBean> serviceDto) {

		FlowReleasePackageRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelCtlCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );

			String rpId = paramBean.getParam().getSelectedRpId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty(rpId), "Seleceted RpId is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}

		return serviceDto;
	}

	private void validate(FlowReleasePackageRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelCtlCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String rpId = paramBean.getParam().getSelectedRpId();
			List<IRpEntity> rpEntities = this.support.findRpEntities( new String[]{rpId} );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( rpEntities.get(0).getLotId() )
					.setRpIds( rpId );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );

		}
		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer(RelCtlScreenID.CANCEL_LIST);
			serviceBean.setForward(RelCtlScreenID.CANCEL_COMFIRM);
			confirmService.execute(dto);
		}
	}

	private void submitChanges(FlowReleasePackageRemovalServiceBean paramBean) {

		{
			String rpId = paramBean.getParam().getSelectedRpId();
			List<IRpEntity> rpEntities = this.support.findRpEntities( new String[]{rpId} );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean( paramBean )
					.setFinder( support )
					.setActionList( statusMatrixAction )
					.setLotIds( rpEntities.get(0).getLotId() )
					.setRpIds( rpId );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelCtlCancelServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer(RelCtlScreenID.CANCEL_COMFIRM);
			serviceBean.setForward(RelCtlScreenID.COMP_CANCEL);
			confirmService.execute(dto);
			completeService.execute(dto);
			mailService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void beforeExecution(FlowReleasePackageRemovalServiceBean src,
								 FlowRelCtlCancelServiceBean dest) {
		src.getResult().setCompleted(false);

		dest.setUserId( src.getUserId() );
		dest.setUserName( src.getUserName() );
		dest.setFlowAction( src.getFlowAction() );
		dest.setLanguage( src.getLanguage() );

		String[] selectedRpId = { src.getParam().getSelectedRpId() };
		dest.setSelectedRelNo( selectedRpId );

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {

			CancelInfoInputBean cancelInfoInputBean = new CancelInfoInputBean();
			cancelInfoInputBean.setRelNo( selectedRpId[0] );
			cancelInfoInputBean.setCancelComment("");
			dest.setReleaseInfoInputBean(cancelInfoInputBean);
		}
	}

	private void afterExecution(FlowRelCtlCancelServiceBean src,
			FlowReleasePackageRemovalServiceBean dest) {

		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003010I);
		}
	}

}
