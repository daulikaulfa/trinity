package jp.co.blueship.tri.rm.domainx.rp;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmEntityAddonUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.BaseInfoInputBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.ConfigurationViewBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitBean;
import jp.co.blueship.tri.rm.domain.rp.beans.dto.UnitViewBean;
import jp.co.blueship.tri.rm.domain.rp.dto.FlowRelCtlEntryServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageCreationServiceBean.ReleasePackageCreationInputInfo;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Anh Nguyen
 */
public class FlowReleasePackageCreationService implements IDomain<FlowReleasePackageCreationServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;
	private IDomain<IGeneralServiceBean> infoInputService = null;
	private IDomain<IGeneralServiceBean> configSelectService = null;
	private IDomain<IGeneralServiceBean> relUnitSelectService = null;
	private IDomain<IGeneralServiceBean> relConfirmService = null;
	private IDomain<IGeneralServiceBean> createService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	public void setInfoInputService(IDomain<IGeneralServiceBean> infoInputService) {
		this.infoInputService = infoInputService;
	}

	public void setConfigSelectService(IDomain<IGeneralServiceBean> configSelectService) {
		this.configSelectService = configSelectService;
	}

	public void setRelUnitSelectService(IDomain<IGeneralServiceBean> relUnitSelectService) {
		this.relUnitSelectService = relUnitSelectService;
	}

	public void setRelConfirmService(IDomain<IGeneralServiceBean> relConfirmService) {
		this.relConfirmService = relConfirmService;
	}

	public void setCreateService(IDomain<IGeneralServiceBean> createService) {
		this.createService = createService;
	}

	@Override
	public IServiceDto<FlowReleasePackageCreationServiceBean> execute(
			IServiceDto<FlowReleasePackageCreationServiceBean> serviceDto) {

		FlowReleasePackageCreationServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelCtlEntryServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}
			
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onchange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal( log, e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
		return serviceDto;
	}

	private void init(FlowReleasePackageCreationServiceBean paramBean) {


		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelCtlEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);

		beforeExecutionOfInit( paramBean, serviceBean );

		dto.setServiceBean(serviceBean);

		{
			serviceBean.setReferer(RelCtlScreenID.BASE_INFO_INPUT);
			serviceBean.setForward(RelCtlScreenID.CONFIGURATION_SELECT);
			configSelectService.execute(dto);
		}

		{
			serviceBean.setReferer(RelCtlScreenID.LOT_SELECT);
			serviceBean.setForward(RelCtlScreenID.UNIT_SELECT);
			relUnitSelectService.execute(dto);
		}

		afterExecutionOfInit( serviceBean, paramBean );
	}


	private void beforeExecutionOfInit( FlowReleasePackageCreationServiceBean src,
	 									FlowRelCtlEntryServiceBean dest) {

		String lotId = src.getParam().getSelectedLotId();

		src.getResult().setCompleted(false);

		dest.setSelectedLotNo( lotId );
		dest.getBaseInfoBean().getRelApplySearchBean().setSelectedRelApplyCount("0");
		dest.getBaseInfoBean().getRelApplySearchBean().setSelectedLotNo( src.getParam().getSelectedLotId() );
	}

	private void afterExecutionOfInit( FlowRelCtlEntryServiceBean src,
									   FlowReleasePackageCreationServiceBean dest) {

		this.generateDropBox(src, dest);
	}
	
	private void onchange(FlowReleasePackageCreationServiceBean paramBean){
		String raId = paramBean.getParam().getInputInfo().getRaId();
		paramBean.getParam().getSelectedLotId();
	
		IRaDto raDto = this.support.getRmFinderSupport().findRaDto(raId,RmTables.RM_RA,RmTables.RM_RA_BP_LNK);
		if( raDto != null ){
			List<IRaBpLnkEntity> raBpLnkEntity = raDto.getRaBpLnkEntities();
			if (!raBpLnkEntity.isEmpty()) {
				paramBean.getParam().getInputInfo().setBpId(raBpLnkEntity.get(0).getBpId());
			}
			IRaEntity raEntity = raDto.getRaEntity();
			if(raEntity != null){
				paramBean.getParam().getInputInfo().setEnvId(raEntity.getBldEnvId());
			}
		}
	}


	private void submitChanges(FlowReleasePackageCreationServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelCtlEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);

		beforeExecutionOfSubmitChanges( paramBean, serviceBean );

		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			String[] selectedBpIds = RmEntityAddonUtils.getBuildNo( serviceBean.getSelectedUnitBean() );
			String raId = paramBean.getParam().getInputInfo().getRaId();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( paramBean.getParam().getSelectedLotId() )
					.setBpIds		( selectedBpIds )
					.setRaIds		( raId )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}
		
		{
			serviceBean.setReferer(RelCtlScreenID.BASE_INFO_INPUT);
			serviceBean.setForward(RelCtlScreenID.CONFIGURATION_SELECT);
			infoInputService.execute(dto);
		}

		{
			serviceBean.setReferer(RelCtlScreenID.CONFIGURATION_SELECT);
			serviceBean.setForward(RelCtlScreenID.LOT_SELECT);
			configSelectService.execute(dto);
		}

		{
			serviceBean.setReferer(RelCtlScreenID.UNIT_SELECT);
			serviceBean.setForward(RelCtlScreenID.ENTRY_CONFIRM);
			relUnitSelectService.execute(dto);
			relConfirmService.execute(dto);
		}

		{
			serviceBean.setReferer(RelCtlScreenID.ENTRY_CONFIRM);
			serviceBean.setForward(RelCtlScreenID.COMP_REQUEST);
			relConfirmService.execute(dto);
			createService.execute(dto);
		}

		afterExecutionOfSubmitChanges( serviceBean, paramBean );
	}

	private void beforeExecutionOfSubmitChanges( FlowReleasePackageCreationServiceBean src,
	 											 FlowRelCtlEntryServiceBean dest) {

		src.getResult().setCompleted(false);

		if( TriStringUtils.isEmpty(src.getParam().getInputInfo().getBpId()) ) {
			throw new ContinuableBusinessException(RmMessageId.RM001038E);
		}

		ReleasePackageCreationInputInfo srcInfo = src.getParam().getInputInfo();
		dest.setSelectedLotNo( src.getParam().getSelectedLotId() );

		dest.setFlowAction	( src.getFlowAction() );
		dest.setProcId		( src.getProcId() );
		dest.setUserId		( src.getUserId() );
		dest.setUserName	( src.getUserName() );
		dest.setSelectedEnvNo( srcInfo.getEnvId() );

		BaseInfoInputBean baseInfoInputBean = dest.getBaseInfoBean().getBaseInfoInputBean();
		baseInfoInputBean.setRelSummary	( srcInfo.getSubject() );
		baseInfoInputBean.setRelContent	( srcInfo.getSummary() );
		baseInfoInputBean.setCtgId		( srcInfo.getCtgId() );
		baseInfoInputBean.setMstoneId	( srcInfo.getMstoneId() );
		baseInfoInputBean.setRelApplyNo	( srcInfo.getRaId() );

		UnitBean unitBean = new UnitBean();
		unitBean.setBuildNo( srcInfo.getBpId() );
		unitBean.setMergeOrder("1");
		UnitBean[] unitBeanList = {unitBean};
		dest.setSelectedUnitBean(unitBeanList);
	}


	private void afterExecutionOfSubmitChanges( FlowRelCtlEntryServiceBean src,
												FlowReleasePackageCreationServiceBean dest) {
		
		dest.setLockByThread(src.isLockByThread());
		dest.getResult().setCompleted(true);
		dest.getResult().setRpId( src.getRelNo());
		dest.getMessageInfo().addFlashTranslatable(RmMessageId.RM003011I);
	}

	private void generateDropBox( FlowRelCtlEntryServiceBean src,
								  FlowReleasePackageCreationServiceBean dest) {

		ReleasePackageCreationInputInfo inputInfo = dest.getParam().getInputInfo();
		String lotId = dest.getParam().getSelectedLotId();
		RaCondition condition = getCondition(dest);

		List<ConfigurationViewBean> relEnvViews = src.getConfSelectBean().getConfViewBeanList();
		inputInfo.setReleaseEnvViews( FluentList.from( relEnvViews ).map( RmFluentFunctionUtils.toItemLabelsFromConfigurationViewBean).asList());

		List<UnitViewBean> unitViewList = src.getReleaseUnitSelectBean().getUnitViewBeanList();
		inputInfo.setBuildPackageViews( FluentList.from( unitViewList).map( RmFluentFunctionUtils.toItemLabelsFromUnitViewBean).asList());

		List<ICtgEntity> ctgEntityList = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		inputInfo.setCtgViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());

		List<IMstoneEntity> mstoneEntityList = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());

		List<IRaDto> releaseRequestViewList = this.support.getRmFinderSupport().findRaDto(condition.getCondition(), RmTables.RM_RA);
		inputInfo.setReleaseRequestViews(FluentList.from( releaseRequestViewList ).map( UmFluentFunctionUtils.toItemLabelsFromRaDto).asList());
	}

	private RaCondition getCondition (FlowReleasePackageCreationServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();

		RaCondition raCondition = new RaCondition();

		raCondition.setLotId( lotId );
		raCondition.setStsIds(new String[]{
					 				RmRaStatusId.ReleaseRequested.getStatusId(),
					 				RmRaStatusId.ReleaseRequestApproved.getStatusId()
									});
		return raCondition;
	}

}
