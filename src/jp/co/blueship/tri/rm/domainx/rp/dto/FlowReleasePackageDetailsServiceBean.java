package jp.co.blueship.tri.rm.domainx.rp.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowReleasePackageDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleasePackageView detailsView = new ReleasePackageView();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public ReleasePackageView getDetailsView() {
		return detailsView;
	}
	public FlowReleasePackageDetailsServiceBean setDetailsView(ReleasePackageView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String rpId = null;

		public String getSelectedRpId() {
			return rpId;
		}
		public RequestParam setSelectedRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}
	}

	public class ReleasePackageView {
		private String rpId;
		private String stsId;
		private String status;
		private String subject;
		private String summary;
		private String raId;
		private String envId;
		private String envNm;
		private String bpId;
		private String ctgNm;
		private String mstoneNm;
		private String createdBy;
		private String createdByIconPath;//
		private String startTime;
		private String endTime;
		
		public String getRpId() {
			return rpId;
		}
		public ReleasePackageView setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReleasePackageView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleasePackageView setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public ReleasePackageView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public ReleasePackageView setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public String getRaId() {
			return raId;
		}
		public ReleasePackageView setRaId(String raId) {
			this.raId = raId;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public ReleasePackageView setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvNm() {
			return envNm;
		}
		public ReleasePackageView setEnvNm(String envNm) {
			this.envNm = envNm;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public ReleasePackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getCtgNm() {
			return ctgNm;
		}
		public ReleasePackageView setCtgNm(String ctgNm) {
			this.ctgNm = ctgNm;
			return this;
		}

		public String getMstoneNm() {
			return mstoneNm;
		}
		public ReleasePackageView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public ReleasePackageView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public ReleasePackageView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public ReleasePackageView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public ReleasePackageView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

	}
}
