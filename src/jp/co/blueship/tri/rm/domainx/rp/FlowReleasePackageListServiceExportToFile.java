package jp.co.blueship.tri.rm.domainx.rp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.ReleasePackageView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchServiceType;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceExportToFileBean;

public class FlowReleasePackageListServiceExportToFile implements IDomain<FlowReleasePackageListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> releasePackageList = null;

	public void setReleasePackageList(IDomain<IGeneralServiceBean> releasePackageList) {
		this.releasePackageList = releasePackageList;
	}

	@Override
	public IServiceDto<FlowReleasePackageListServiceExportToFileBean> execute(
			IServiceDto<FlowReleasePackageListServiceExportToFileBean> serviceDto) {
		FlowReleasePackageListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowReleasePackageListServiceBean innerServiceBean = paramBean.getInnerService();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowReleasePackageListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowReleasePackageListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowReleasePackageListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			serviceBean.getParam().setSearchServiceType(SearchServiceType.SearchSite);
			releasePackageList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		releasePackageList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowReleasePackageListServiceExportToFileBean src, FlowReleasePackageListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);

			destSearchCondition.setEnvId(srcSearchConditon.getEnvId());
			destSearchCondition.setStsId(srcSearchConditon.getStsId());
			destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
			destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
		}
	}

	private void afterExecution(FlowReleasePackageListServiceBean src, FlowReleasePackageListServiceExportToFileBean dest) {
		List<ReleasePackageView> srcSearchSiteViews = src.getReleasePackageViews();
		List<ReleasePackageView> destSearchSiteViews = new ArrayList<ReleasePackageView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcSearchSiteViews.size(); i++) {
				ReleasePackageView view = srcSearchSiteViews.get(i);
				destSearchSiteViews.add(view);
			}
			dest.setReleasePackageViews(destSearchSiteViews);
		}
	}
}
