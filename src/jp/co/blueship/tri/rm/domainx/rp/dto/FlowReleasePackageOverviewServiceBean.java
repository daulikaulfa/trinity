package jp.co.blueship.tri.rm.domainx.rp.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowReleasePackageOverviewServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleasePackageOverview detailsView = new ReleasePackageOverview();

	public RequestParam getParam() {
		return param;
	}

	public ReleasePackageOverview getDetailsView() {
		return detailsView;
	}
	public FlowReleasePackageOverviewServiceBean setDetailsView(ReleasePackageOverview detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String rpId = null;

		public String getSelectedRpId() {
			return rpId;
		}
		public RequestParam setSelectedRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}
	}

	public class ReleasePackageOverview {
		private String subject;
		private String stsId;
		private String status;
		private String createdBy;
		private String startTime;
		private String endTime;
		private String envId;
		private String envNm;

		public String getSubject() {
			return subject;
		}
		public ReleasePackageOverview setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReleasePackageOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleasePackageOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public ReleasePackageOverview setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public ReleasePackageOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public ReleasePackageOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getEnvId() {
			return envId;
		}
		public ReleasePackageOverview setEnvId(String envId) {
			this.envId = envId;
			return this;
		}

		public String getEnvNm() {
			return envNm;
		}
		public ReleasePackageOverview setEnvNm(String envNm) {
			this.envNm = envNm;
			return this;
		}

	}
}
