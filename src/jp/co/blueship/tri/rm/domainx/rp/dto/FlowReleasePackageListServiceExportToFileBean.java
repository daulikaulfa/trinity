package jp.co.blueship.tri.rm.domainx.rp.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.ReleasePackageView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchCondition;
/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowReleasePackageListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReleasePackageListFileResponse fileResponse = new ReleasePackageListFileResponse();
	private List<ReleasePackageView> releasePackageView = new ArrayList<ReleasePackageView>();

	{
		this.setInnerService( new FlowReleasePackageListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReleasePackageListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReleasePackageListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReleasePackageListServiceExportToFileBean setFileResponse(ReleasePackageListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<ReleasePackageView> getReleasePackageViews() {
		return releasePackageView;
	}

	public void setReleasePackageViews(List<ReleasePackageView> releasePackageView) {
		this.releasePackageView = releasePackageView;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowReleasePackageListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class ReleasePackageListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
