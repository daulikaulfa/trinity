package jp.co.blueship.tri.rm.domainx.rp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.rm.RmBusinessJudgUtils;
import jp.co.blueship.tri.rm.RmDBSearchConditionAddonUtils;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.OrderBy;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.ReleasePackageView;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.RequestParam;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageListService implements IDomain<FlowReleasePackageListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlEditSupport support = null;
	public void setSupport (FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleasePackageListServiceBean> execute(
			IServiceDto<FlowReleasePackageListServiceBean> serviceDto) {
		FlowReleasePackageListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * Call when initializing by service
	 *
	 * @param serviceBean
	 */
	private void init ( FlowReleasePackageListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();
		PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "ServiceBean is not specified");

		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		this.generateDropBox (lotId, searchCondition);

		this.onChange(serviceBean);
	}

	/**
	 * @param lotId Target lot-ID
	 * @param searchCondition Set the generated search conditions
	 */
	private void generateDropBox( String lotId, SearchCondition searchCondition) {
		searchCondition.setReleaseEnvViews( this.getReleaseEnvLabels() );

		List<IStatusId> rpStatuses = new ArrayList<IStatusId>();
		rpStatuses.add(RmRpStatusIdForExecData.CreatingReleasePackage);
		rpStatuses.add(RmRpStatusId.ReleasePackageCreated);
		rpStatuses.add(RmRpStatusIdForExecData.ReleasePackageError);
		rpStatuses.add(RmRpStatusId.ReleasePackageRemoved);
		
		if ( DesignSheetUtils.isRelApplyEnable() ) {
			rpStatuses.add(RmRpStatusId.ReleasePackageClosed);
		}
		
		searchCondition.setStatusViews(FluentList.from( rpStatuses ).map( RmFluentFunctionUtils.toItemLabelsFromStatusId).asList() );

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );

	}

	private List<ItemLabelsBean> getReleaseEnvLabels() {

		IJdbcCondition condition = RmDBSearchConditionAddonUtils.getActiveReleaseEnvCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvItems.bldEnvNm, TriSortOrder.Asc, 1);

		List<IBldEnvEntity> envList = this.support.getBmFinderSupport().getBldEnvDao().find(condition.getCondition(), sort);
		return FluentList.from( envList ).map(
				new TriFunction<IBldEnvEntity, ItemLabelsBean>() {
					@Override
					public ItemLabelsBean apply(IBldEnvEntity input) {
						return new ItemLabelsBean(input.getBldEnvNm() , input.getBldEnvId());
					}}
				).asList();
	}

	/**
	 * Call when request by service
	 *
	 * @param serviceBean
	 */
	private void onChange ( FlowReleasePackageListServiceBean serviceBean ) {
		ISqlSort sort = getSortOrder( serviceBean.getParam() );

		int linesPerPage = serviceBean.getParam().getLinesPerPage();

		int pageNo = serviceBean.getParam().getSearchCondition().getSelectedPageNo();

		IEntityLimit<IRpEntity> limit = support.getRpDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				pageNo,
				linesPerPage);

		IPageNoInfo page = RmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		serviceBean.setPage(page);
		serviceBean.setReleasePackageViews( getRpViewBean(serviceBean, limit.getEntities()) );
	}

	/**
	 * @param param
	 */
	private ISqlSort getSortOrder( RequestParam param ) {
		ISqlSort sort = new SortBuilder();

		if( null == param.getOrderBy() ) {
			return sort;
		}

		OrderBy orderBy = param.getOrderBy();

		if ( null != orderBy.getRpId() ) {
			sort.setElement(RpItems.rpId, orderBy.getRpId(), sort.getLatestSeq() + 1);
		}

		return sort;
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private RpCondition getCondition(FlowReleasePackageListServiceBean serviceBean) {

		RequestParam param = serviceBean.getParam();
		String lotId = serviceBean.getParam().getSelectedLotId();

		SearchCondition searchParam = param.getSearchCondition();
		RpCondition condition = new RpCondition();

		condition.setLotId(lotId);

		if ( TriStringUtils.isNotEmpty(searchParam.getEnvId()) ) {
			condition.setBldEnvId( searchParam.getEnvId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getStsId()) ) {
			condition.setProcStsIds( searchParam.getStsId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId( searchParam.getCtgId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId( searchParam.getMstoneId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword( searchParam.getKeyword());
		}

		return condition;
	}

	/**
	 * @param serviceBean ServiceBean
	 * @param rpList Release Package Entities
	 * @return
	 */
	private List<ReleasePackageView> getRpViewBean(
										FlowReleasePackageListServiceBean serviceBean,
										List<IRpEntity> rpList) {
		List<ReleasePackageView> viewBeanList = new ArrayList<ReleasePackageView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IRpEntity entity : rpList) {
			RpBpLnkCondition condition = new RpBpLnkCondition();
			condition.setRpId( entity.getRpId() );
			List<IRpBpLnkEntity> rpBpLnkEntityList = this.support.getRpBpLnkDao().find( condition.getCondition() );

			for (IRpBpLnkEntity rpBpLnkEntity : rpBpLnkEntityList) {
				IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(entity.getBldEnvId());

				ReleasePackageView view = serviceBean.new ReleasePackageView()
						.setBpId( rpBpLnkEntity.getBpId() )
						.setRpId( entity.getRpId() )
						.setEnvId( entity.getBldEnvId() )
						.setEnvironmentNm( bldEnvEntity.getBldEnvNm() )
						.setSubject( entity.getSummary() )
						.setCreatedBy( entity.getRegUserNm() )
						.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(entity.getRegUserId()) )
						.setCreatedDate( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMD) )
						.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) )
						.setStsId( entity.getProcStsId() )
						.setStatus( sheet.getValue(RmDesignBeanId.statusId, entity.getProcStsId()) )
						.setSuccess( RmBusinessJudgUtils.isSuccess(entity) )
						.setStartTime( TriDateUtils.convertViewDateFormat(entity.getProcStTimestamp(), formatYMD) )
						.setEndTime( TriDateUtils.convertViewDateFormat(entity.getProcEndTimestamp(), formatYMD) )
				;

				viewBeanList.add(view);
			}
		}
		return viewBeanList;
	}
}
