package jp.co.blueship.tri.rm.domainx.rp;

import java.text.SimpleDateFormat;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaRpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean;
import jp.co.blueship.tri.rm.domainx.rp.dto.FlowReleasePackageDetailsServiceBean.ReleasePackageView;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowReleasePackageDetailsService implements IDomain<FlowReleasePackageDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelCtlEditSupport support = null;

	public void setSupport(FlowRelCtlEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReleasePackageDetailsServiceBean> execute(
			IServiceDto<FlowReleasePackageDetailsServiceBean> serviceDto) {

		FlowReleasePackageDetailsServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf( serviceBean != null, "ServiceBean is not specified");

			String rpId = serviceBean.getParam().getSelectedRpId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(rpId), "SelectedRpId is not specified");

			if ( RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				//Check group accessibility
				IRpEntity rpEntity = this.support.findRpEntity(rpId);
				ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(rpEntity.getLotId(), AmTables.AM_LOT_GRP_LNK);

				AmLibraryAddonUtils.checkAccessableGroup( lotDto,
						  this.support.getUmFinderSupport().getGrpDao(),
						  this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()) );

				//Get Release Package Details
				ReleasePackageView releasePackageView = this.getReleasePackageDetails(serviceBean, rpEntity);

				//Set Release Package Details
				serviceBean.setDetailsView(releasePackageView);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005085S, e, serviceBean.getFlowAction());
		}
	}

	private ReleasePackageView getReleasePackageDetails ( FlowReleasePackageDetailsServiceBean serviceBean,
														  IRpEntity rpEntity) {

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone());

		IBldEnvEntity bldEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity(rpEntity.getBldEnvId());
		IRpDto rpDto = this.support.findRpDto(rpEntity, RmTables.RM_RP_BP_LNK);
		List<IRpBpLnkEntity> rpBpLnkEntity = rpDto.getRpBpLnkEntities();
		String bpId = (rpBpLnkEntity.size() > 0) ? rpBpLnkEntity.get(0).getBpId() : null;

		List<IRaRpLnkEntity> raRpLnkEntity = this.support.findRaRpLnkEntitiesFromRpId(rpEntity.getRpId());
		String raId = (raRpLnkEntity.size() > 0) ? raRpLnkEntity.get(0).getRaId() : null;

		ReleasePackageView view = serviceBean.new ReleasePackageView()
									.setRpId		( rpEntity.getRpId() )
									.setStsId		( rpEntity.getProcStsId() )
									.setStatus		( sheet.getValue(RmDesignBeanId.statusId, rpEntity.getProcStsId()) )
									.setSubject		( rpEntity.getSummary() )
									.setSummary		( rpEntity.getContent() )
									.setRaId		( raId )
									.setEnvId		( rpEntity.getBldEnvId() )
									.setEnvNm		( bldEnvEntity.getBldEnvNm() )
									.setBpId		( bpId )
									.setCtgNm		( rpEntity.getCtgNm() )
									.setMstoneNm	( rpEntity.getMstoneNm() )
									.setCreatedBy	( rpEntity.getRegUserNm() )
									.setStartTime	( TriDateUtils.convertViewDateFormat(rpEntity.getProcStTimestamp(), format) )
									.setEndTime		( TriDateUtils.convertViewDateFormat(rpEntity.getProcEndTimestamp(), format) );

		return view;
	}

}
