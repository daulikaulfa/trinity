package jp.co.blueship.tri.rm.domainx.rp.dto;

import jp.co.blueship.tri.bm.task.TaskDetailsViewBean;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReleasePackageCreationProcessingStatusServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private ReleasePackageCreationProcessingStatusOverview overview = new ReleasePackageCreationProcessingStatusOverview();
	private TaskDetailsViewBean detailsView = new TaskDetailsViewBean();

	public RequestParam getParam() {
		return param;
	}

	public ReleasePackageCreationProcessingStatusOverview getOverview() {
		return overview;
	}
	public FlowReleasePackageCreationProcessingStatusServiceBean setOverview(ReleasePackageCreationProcessingStatusOverview overview) {
		this.overview = overview;
		return this;
	}

	public TaskDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowReleasePackageCreationProcessingStatusServiceBean setDetailsView(TaskDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;

		public String getSelectedLotId() {
			return lotId;
		}

		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

	}

	public class ReleasePackageCreationProcessingStatusOverview {
		private String rpId;
		private String createdBy;
		private String createdByIconPath;
		private String startTime;
		private String endTime;
		private ProcessStatus result = ProcessStatus.none;
		private IProgressBar bar;
		private String remainingTime;

		public String getRpId() {
			return rpId;
		}
		public ReleasePackageCreationProcessingStatusOverview setRpId(String rpId) {
			this.rpId = rpId;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public ReleasePackageCreationProcessingStatusOverview setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public ReleasePackageCreationProcessingStatusOverview setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public ReleasePackageCreationProcessingStatusOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public ReleasePackageCreationProcessingStatusOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public boolean isActive() {
			return ProcessStatus.Active.equals( result );
		}
		public boolean isCompleted() {
			return ProcessStatus.Completed.equals( result );
		}
		public boolean isError() {
			return ProcessStatus.Error.equals( result );
		}
		public ReleasePackageCreationProcessingStatusOverview setResult(ProcessStatus result) {
			this.result = result;
			return this;
		}

		public IProgressBar getBar() {
			return bar;
		}
		public ReleasePackageCreationProcessingStatusOverview setBar(IProgressBar bar) {
			this.bar = bar;
			return this;
		}

		public String getRemainingTime() {
			return remainingTime;
		}
		public ReleasePackageCreationProcessingStatusOverview setRemainingTime(String remainingTime) {
			this.remainingTime = remainingTime;
			return this;
		}
	}

}
