package jp.co.blueship.tri.dcm.domainx.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.rp.dto.FlowRelCtlReportServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportReleaseCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportReleaseCreationServiceBean.ReleasePackageView;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.FlowRelCtlEditSupport;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowReportReleaseCreationService implements IDomain<FlowReportReleaseCreationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;
	private FlowRelCtlEditSupport reportListSupport;
	private IDomain<IGeneralServiceBean> reportCreateService;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setFwSupport(IFwFinderSupport fwSupport) {
		this.fwSupport = fwSupport;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	public void setReportListSupport(FlowRelCtlEditSupport reportEditSupport) {
		this.reportListSupport = reportEditSupport;
	}

	public void setReportCreateService(IDomain<IGeneralServiceBean> reportCreateService) {
		this.reportCreateService = reportCreateService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReportReleaseCreationServiceBean> execute(
			IServiceDto<FlowReportReleaseCreationServiceBean> serviceDto) {
		FlowReportReleaseCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				 this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowReportReleaseCreationServiceBean paramBean) {
		this.onChange(paramBean);
	}

	private void onChange(FlowReportReleaseCreationServiceBean paramBean) {
		int linesPerPage = paramBean.getParam().getLinesPerPage();
		int pageNo = paramBean.getParam().getSelectedPageNo();

		removeNotSelectedReleasePackageId(paramBean);
		IEntityLimit<IRpEntity> limit = this.reportListSupport.getRpDao()
				.find(this.getCondition(paramBean).getCondition(), pageNo, linesPerPage);

		IPageNoInfo page = DcmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setReleasePackageViews(getReleasePackageViewsFromEntity(paramBean, limit.getEntities()));
		setSelectedReleasePackage(paramBean);
		paramBean.setPage(page);
	}

	private void removeNotSelectedReleasePackageId(FlowReportReleaseCreationServiceBean paramBean) {
		Set<String> currentPageReleaseIds = new HashSet<String>();
		for(ReleasePackageView rpView : paramBean.getReleasePackageViews()) {
			currentPageReleaseIds.add(rpView.getRpId());
		}
		//Get current NOT checked id from current paramBean
		currentPageReleaseIds.removeAll(Arrays.asList(paramBean.getParam().getListSelection().getSelectedIds()));
		//Remove not checked id from all id
		paramBean.getParam().getListSelection().getSelectedIdSet().removeAll(currentPageReleaseIds);

		Set<String> allSelectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		String[] arrIds = allSelectedIds.toArray(new String[allSelectedIds.size()]);
		paramBean.getParam().getListSelection().setSelectedIds(arrIds);

	}

	private void setSelectedReleasePackage(FlowReportReleaseCreationServiceBean paramBean) {
		Set<String> selectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		for(ReleasePackageView rpView : paramBean.getReleasePackageViews()) {
			if (selectedIds.contains(rpView.getRpId())) {
				rpView.setSelected(true);
			}
		}
	}

	private List<ReleasePackageView> getReleasePackageViewsFromEntity(FlowReportReleaseCreationServiceBean paramBean,
			List<IRpEntity> entities) {
		List<ReleasePackageView> reportViews = new ArrayList<ReleasePackageView>();
		List<IRpDto> rpDtoList = this.reportListSupport.findRpDto(entities);
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		for (IRpDto dto : rpDtoList) {
			ReleasePackageView view = paramBean.new ReleasePackageView()
				.setRpId(dto.getRpEntity().getRpId())
				.setBpId(dto.getRpBpLnkEntities().get(0).getBpId())
				.setSubmitterNm(dto.getRpEntity().getExecUserNm())
				.setSubmitterIconPath(this.umSupport.getIconPath(dto.getRpEntity().getExecUserId()))
				.setCreatedDate(TriDateUtils.convertViewDateFormat(dto.getRpEntity().getProcStTimestamp(), formatYMD))
				.setStsId(dto.getRpEntity().getProcStsId())
				.setStatus( sheet.getValue(RmDesignBeanId.statusId, dto.getRpEntity().getProcStsId()) )
			;

			IBldEnvEntity bldEnvEntity = this.reportListSupport.getBmFinderSupport().findBldEnvEntity(dto.getRpEntity().getBldEnvId());
			view.setRelEnv(bldEnvEntity.getBldEnvNm());

			reportViews.add(view);
		}

		return reportViews;
	}

	private RpCondition getCondition(FlowReportReleaseCreationServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();
		RpCondition condition = new RpCondition();
		condition.setLotId(lotId);

		return condition;
	}

	private void submitChanges(FlowReportReleaseCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelCtlReportServiceBean serviceBean = paramBean.getInnerService();
		dto.setServiceBean(serviceBean);
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		this.beforeExecution(paramBean, serviceBean);

		// Status Matrix Check
		{
			String lotId = paramBean.getParam().getSelectedLotId();
			String[] rpIds = paramBean.getParam().getListSelection().getSelectedIds();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( reportListSupport )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotId )
					.setRpIds		( rpIds )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		reportCreateService.execute(dto);
		
		this.afterExecution( serviceBean, paramBean );
	}

	private void beforeExecution(FlowReportReleaseCreationServiceBean paramBean,
			FlowRelCtlReportServiceBean serviceBean) {
		removeNotSelectedReleasePackageId(paramBean);
		serviceBean.setUserId(paramBean.getUserId());
		serviceBean.setUserName(paramBean.getUserName());
		serviceBean.setReportId(DcmReportType.RmRpReport.value());
		serviceBean.setSelectedLotNo(paramBean.getParam().getSelectedLotId());
		serviceBean.setSelectedRelNo(paramBean.getParam().getListSelection().getSelectedIds());
		serviceBean.setScreenType(ScreenType.next);
		serviceBean.setProcId(paramBean.getProcId());
		serviceBean.setFlowAction("FlowRelCtlReportService");
		serviceBean.setReferer(RelCtlScreenID.HISTORY_LIST);
		if (paramBean.getParam().getListSelection().getSelectedIds().length == 0) {
			throw new ContinuableBusinessException(DcmMessageId.DCM001004E);
		}
	}
	
	private void afterExecution( FlowRelCtlReportServiceBean src,
			FlowReportReleaseCreationServiceBean dest ) {
		
		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(DcmMessageId.DCM003003I);
		}
	}

}
