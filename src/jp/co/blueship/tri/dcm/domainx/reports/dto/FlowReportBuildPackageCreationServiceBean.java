package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReportBuildPackageCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<BuildPackageView> buildPackageViews = new ArrayList<BuildPackageView>();
	private IPageNoInfo page = new PageNoInfo();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowRelUnitReportServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public List<BuildPackageView> getBuildPackageViews() {
		return buildPackageViews;
	}
	public FlowReportBuildPackageCreationServiceBean setBuildPackageViews(List<BuildPackageView> buildPackageViews) {
		this.buildPackageViews = buildPackageViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReportBuildPackageCreationServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportBuildPackageCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private int linesPerPage = 20;
		private int selectedPageNo = 1;
		private ListSelection listSelection = new ListSelection();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public int getSelectedPageNo() {
			return selectedPageNo;
		}
		public RequestParam setSelectedPageNo(int selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public ListSelection getListSelection() {
			return listSelection;
		}
		public RequestParam setListSelection(ListSelection listSelection) {
			this.listSelection = listSelection;
			return this;
		}
	}

	/**
	 * List Selection
	 */
	public class ListSelection extends jp.co.blueship.tri.fw.svc.beans.dto.ListSelection {

	}

	public class BuildPackageView {
		private boolean isSelected = false;
		private String bpId;
		private String bpNm;
		private String submitterIconPath;
		private String submitterNm;
		private String createdDate;
		private String stsId;
		private String status;

		public boolean isSelected() {
			return isSelected;
		}
		public BuildPackageView setSelected(boolean isSelected) {
			this.isSelected = isSelected;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public BuildPackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getBpNm() {
			return bpNm;
		}
		public BuildPackageView setBpNm(String bpNm) {
			this.bpNm = bpNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public BuildPackageView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public BuildPackageView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getCreatedDate() {
			return createdDate;
		}
		public BuildPackageView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public BuildPackageView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public BuildPackageView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String repId;
		private boolean completed = false;

		public String getRepId() {
			return repId;
		}
		public RequestsCompletion setRepId(String repId) {
			this.repId = repId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
