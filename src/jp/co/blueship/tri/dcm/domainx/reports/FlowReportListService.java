package jp.co.blueship.tri.dcm.domainx.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.rep.constants.RepItems;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ListSelection.ReportSelect;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ReportView;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.RequestOption;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.SearchCondition;
import jp.co.blueship.tri.dcm.support.FlowReportSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowReportListService implements IDomain<FlowReportListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowReportSupport support;

	public void setSupport(FlowReportSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowReportListServiceBean> execute(IServiceDto<FlowReportListServiceBean> serviceDto) {
		FlowReportListServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowReportListServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();

		this.onChange(paramBean);
		this.generateDropBox(lotId, paramBean);
	}

	private void onChange(FlowReportListServiceBean paramBean) {
		if(RequestOption.selectResource.equals(paramBean.getParam().getRequestOption())) {
			setSelectedReports(paramBean);
			return;
		}
		if(RequestOption.unselectResource.equals(paramBean.getParam().getRequestOption())) {
			paramBean.getParam().getListSelection().getSelectedIdSet().clear();
			paramBean.getParam().getListSelection().getReportSelect().clear();
			clearSelectedReportView(paramBean);
			return;
		}
		int linesPerPage = paramBean.getParam().getLinesPerPage();
		int pageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();

		ISqlSort sort = new SortBuilder();
		sort.setElement(RepItems.updTimestamp, TriSortOrder.Desc, 1);
		
		IEntityLimit<IRepEntity> limit =
				this.support.getRepDao().find(this.getCondition(paramBean).getCondition(), sort, pageNo, linesPerPage);
		IPageNoInfo page = DcmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setReportViews(getReportViewsFromEntity(paramBean, limit.getEntities()));
		setSelectedReportView(paramBean);
		paramBean.setPage(page);
	}

	private void setSelectedReportView(FlowReportListServiceBean paramBean) {
		Set<String> selectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		for(ReportView reportView : paramBean.getReportViews()) {
			if (selectedIds.contains(reportView.getReportId())) {
				reportView.setSelected(true);
			} else {
				reportView.setSelected(false);
			}
		}
	}

	private void clearSelectedReportView(FlowReportListServiceBean paramBean) {
		for(ReportView reportView : paramBean.getReportViews()) {
			reportView.setSelected(false);
		}
		paramBean.getParam().setRequestOption(RequestOption.none);
	}

	private void setSelectedReports(FlowReportListServiceBean paramBean) {
		Set<String> selectedIdSet = paramBean.getParam().getListSelection().getSelectedIdSet();
		for(ReportSelect report : paramBean.getParam().getListSelection().getReportSelect()) {
			if(report.isSelected()) {
				selectedIdSet.add(report.getRepId());
			} else {
				selectedIdSet.remove(report.getRepId());
			}
		}
		paramBean.getParam().setRequestOption(RequestOption.none);
	}

	private List<ReportView> getReportViewsFromEntity(FlowReportListServiceBean paramBean, List<IRepEntity> entities) {
		List<ReportView> reportViews = new ArrayList<ReportView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		for (IRepEntity entity : entities) {
			ReportView view = paramBean.new ReportView();
			view.setReportId(entity.getRepId()).setType(DcmReportType.value(entity.getRepCtgCd()))
					.setSubmitterNm(entity.getExecUserNm())
					.setSubmitterIconPath(this.support.getUmFinderSupport().getIconPath(entity.getExecUserId()))
					.setCreatedDate(TriDateUtils.convertViewDateFormat(entity.getProcStTimestamp(), formatYMD))
					.setStsId(entity.getProcStsId());
			if (DcmRepStatusId.ReportCreated.equals(entity.getProcStsId())) {
				view.setDownloadEnable(true);
			}

			reportViews.add(view);
		}

		return reportViews;
	}

	private RepCondition getCondition(FlowReportListServiceBean paramBean) {
		SearchCondition searchParam = paramBean.getParam().getSearchCondition();

		String lotId = paramBean.getParam().getSelectedLotId();
		RepCondition condition = new RepCondition();
		condition.setLotId(lotId);

		if (TriStringUtils.isNotEmpty(searchParam.getType().value())) {
			condition.setRepCtgCd(searchParam.getType().value());
		}

		if (TriStringUtils.isNotEmpty(searchParam.getStsId())) {
			condition.setProcStsId(new String[] { searchParam.getStsId() });
		}

		if (TriStringUtils.isNotEmpty(searchParam.getCtgId())) {
			condition.setCtgId(searchParam.getCtgId());
		}

		if (TriStringUtils.isNotEmpty(searchParam.getMstoneId())) {
			condition.setMstoneId(searchParam.getMstoneId());
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		return condition;
	}

	private void generateDropBox(String lotId, FlowReportListServiceBean paramBean) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		searchCondition.setStatusViews(
				DesignDefineSearchUtils.mappingItemLabelsBean(DcmDesignBeanId.reportListStatusIdSearch));
		
		Map<String, String> reportTypeMap = new LinkedHashMap<String, String>();
		for ( DcmReportType reportType : DcmReportType.values() ) {
			if( TriStringUtils.isNotEmpty(reportType.value()) ) {
				if( !DesignSheetUtils.isRelApplyEnable() && reportType == DcmReportType.RmRaReport ) {
					continue;
				}
				
				reportTypeMap.put(reportType.value(), reportType.wordKey());
			}
		}
		searchCondition.setTypeViews(DesignDefineSearchUtils.mappingItemLabelsBean(reportTypeMap));

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(
				FluentList.from(ctgEntities).map(UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(
				FluentList.from(mstoneEntities).map(UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());

	}
}
