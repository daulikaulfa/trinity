package jp.co.blueship.tri.dcm.domainx.reports;

import static jp.co.blueship.tri.am.ws.AmWSFunctions.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.AmResourceSelectionUtils;
import jp.co.blueship.tri.am.dao.areq.eb.IViewAssetRegisterEntity;
import jp.co.blueship.tri.am.dao.areq.eb.ViewAssetRegisterCondition;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.domain.areq.beans.dto.AssetRegisterCondition;
import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibAssetRegisterServiceBean;
import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.am.support.FlowChaLibReportSupport;
import jp.co.blueship.tri.am.ws.vo.AssetRegisterVo;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean.RequestParam;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportAssetRegisterCreationServiceBean.SearchCondition;
import jp.co.blueship.tri.dcm.support.FlowReportSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

public class FlowReportAssetRegisterCreationService implements IAmDomain<FlowReportAssetRegisterCreationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	IContextAdapter ca = ContextAdapterFactory.getContextAdapter();


	private IDomain<IGeneralServiceBean> reportService = null;
	private FlowReportSupport support = null;
	private FlowChaLibReportSupport amSupport = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setReportService(IDomain<IGeneralServiceBean> service) {
		this.reportService = service;
	}

	public void setSupport(FlowReportSupport support) {
		this.support = support;
	}

	public void setAmSupport(FlowChaLibReportSupport amSupport) {
		this.amSupport = amSupport;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReportAssetRegisterCreationServiceBean> execute(
			IServiceDto<FlowReportAssetRegisterCreationServiceBean> serviceDto) {

		FlowReportAssetRegisterCreationServiceBean paramBean = serviceDto.getServiceBean();

		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		} finally {

		}

	}

	public void init(FlowReportAssetRegisterCreationServiceBean serviceBean) {

		String userId = serviceBean.getUserId();
		String lotId = serviceBean.getParam().getSelectedLotId();

		ILotEntity lotEntity = this.amSupport.findLotEntity(lotId);
		serviceBean.getDetailsView().setLatestBaselineTag(lotEntity.getLotLatestBlTag());

		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();

		this.generateDropBox(lotId, userId, searchCondition, serviceBean.getLanguage());
	}

	public void generateDropBox(String lotId, String userId, SearchCondition searchCondition, String language) {

		PjtCondition condition = new PjtCondition();
		condition.setLotId(lotId);

		List<IPjtEntity> pjtEntities = this.amSupport.getPjtDao().find(condition.getCondition());
		searchCondition.setPjtViews(FluentList.from(pjtEntities).map(AmFluentFunctionUtils.toPjtIds).asList());

		ILotDto lotDto = this.amSupport.findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
		List<IGrpEntity> grpEntity = this.amSupport.getUmFinderSupport().findGroupByUserId(userId);

		String[] groupIds = AmResourceSelectionUtils.getSelectedGroupList(lotDto, grpEntity).toArray(new String[0]);
		searchCondition.setGroupViews(FluentList.from(this.amSupport.getUmFinderSupport().findGroupByGroupIds(groupIds))
				.map(UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		List<IUserEntity> userEntityList = this.amSupport.getUmFinderSupport().findUserByGroups(groupIds);
		searchCondition.setSubmitterViews(
				FluentList.from(userEntityList).map(UmFluentFunctionUtils.toItemLabelsFromUserEntity).asList());
		for (ItemLabelsBean label : searchCondition.getSubmitterViews()) {
			for (IUserEntity entity : userEntityList) {
				if (entity.getUserId().equals(label.getValue())) {
					label.setSubValue(UmDesignBusinessRuleUtils.getUserIconSharePath(entity));
				}
			}
		}

		ILotDto mdlLotDot = this.amSupport.findLotDto(lotId, AmTables.AM_LOT_MDL_LNK);
		List<ILotMdlLnkEntity> mdlEntities = mdlLotDot.getIncludeMdlEntities(true);
		List<String> moduleNameList = new ArrayList<String>();
		for (ILotMdlLnkEntity moduleEntity : mdlEntities) {
			moduleNameList.add(moduleEntity.getMdlNm());
		}
		searchCondition.setModuleViews(moduleNameList);

		Set<String> referenceSet = new HashSet<String>();
		for (IPjtEntity entity : pjtEntities) {
			String referenceId = entity.getChgFactorNo();
			if (!referenceSet.contains(referenceId)) {
				referenceSet.add(referenceId);
			}
		}
		searchCondition.setReferenceViews(new ArrayList<>(referenceSet));

		List<ItemLabelsBean> statusViews = new ArrayList<>();
		for (AmAreqStatusId value : AmAreqStatusId.values()) {
			statusViews.add(this.getStatusLabel(value, language));
		}

		searchCondition.setStatusViews(statusViews);

		List<ItemLabelsBean> procStatusViews = new ArrayList<>();
		for (AmAreqStatusIdForExecData value : AmAreqStatusIdForExecData.values()) {
			procStatusViews.add(this.getStatusLabel(value, language));
		}
		searchCondition.setProcStatusViews(procStatusViews);

	}

	public void submitChanges(FlowReportAssetRegisterCreationServiceBean serviceBean) throws IOException {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowChaLibAssetRegisterServiceBean innerServiceBean = serviceBean.getInnerService();
		dto.setServiceBean(innerServiceBean);

		this.beforeExecution(serviceBean, innerServiceBean);

		// Status Matrix Check
		{
			String lotId = serviceBean.getParam().getSelectedLotId();
			String[] areqIds = this.getAreqIds( serviceBean );
			String[] bpIds = this.getBpIds( areqIds );

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotId )
					.setBpIds		( bpIds )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		reportService.execute(dto);
		FlowChaLibAssetRegisterServiceBean responseBean = (FlowChaLibAssetRegisterServiceBean) dto.getServiceBean();

		List<AssetRegisterVo> results = FluentList.from(responseBean.getAssetRegisterViewList())
				.map(TO_ASSET_REGISTER_VO).asList();
		File report = this.createCSV(results);

		AssetRegisterCondition paramCondition = responseBean.getAssetRegisterCondition();
		IRepEntity entity = this.insertReportEntity(innerServiceBean);
		paramCondition.setFilePath(entity.getRepId());

		this.changeFileName(report,innerServiceBean.getAssetRegisterCondition().getFilePath()[0]);
		
		this.afterExecution(innerServiceBean, serviceBean);
	}

	private void changeFileName(File report , String rpId) throws IOException {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("'report'_yyyyMMdd_HHmmss");
		String fileName = simpleDateFormat.format(new Date(System.currentTimeMillis()));

		File oldFile = report;
		File newFile = null;

		String absolutePath = oldFile.getAbsolutePath();
		String filePath = absolutePath.substring(0, absolutePath.lastIndexOf(File.separator));
		File path = new File( filePath );
		if ( ! path.isDirectory() )
			path.mkdirs();
		newFile = new File(path, rpId + ".csv" );
		Files.move(oldFile.toPath(), newFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
	}

	private void beforeExecution(FlowReportAssetRegisterCreationServiceBean serviceBean,
			FlowChaLibAssetRegisterServiceBean innerServiceBean) {
		AssetRegisterCondition condition = new AssetRegisterCondition();
		RequestParam param = serviceBean.getParam();
		SearchCondition searchCondition = param.getSearchCondition();
		condition.setLotId(param.getSelectedLotId());
		condition.setMdlNm(searchCondition.getModuleNm());
		condition.setPjtId(searchCondition.getPjtId());
		condition.setChgFactorNo(searchCondition.getReferenceId());
		condition.setReqUserId(searchCondition.getSubmitterId());
		condition.setGrpNm(searchCondition.getGroupId());
		condition.setStsId(searchCondition.getStsId());
		condition.setProcStsId(searchCondition.getProcStsId());
		condition.setFilePath(searchCondition.getFilePath());
		condition.setTargetMW(searchCondition.isTargetMW());
		innerServiceBean.setAssetRegisterCondition(condition);
		innerServiceBean.setUserId(serviceBean.getUserId());
		innerServiceBean.setUserName(serviceBean.getUserName());

	}
	
	private void afterExecution( FlowChaLibAssetRegisterServiceBean src,
			FlowReportAssetRegisterCreationServiceBean dest ) {
		
		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(DcmMessageId.DCM003001I);
		}
	}

	private File createCSV(List<AssetRegisterVo> results) throws IOException {
		Path directory = Paths.get(sheet.getValue(DcmDesignEntryKeyByReport.reportFilePath));
		ExportToFileBean fileBean = new ExportToFileBean();
		this.exportHeader(fileBean);
		this.exportResult(fileBean, results);
		return DcmExportToFileUtils.createCsvFile(directory, fileBean);
	}

	private void exportHeader(ExportToFileBean fileBean) {
		List<String> header = fileBean.getTitleHeader();
		AssetRegisterVo vo = new AssetRegisterVo();
		Gson gson = new GsonBuilder().serializeNulls().setPrettyPrinting().create();
		String json = gson.toJson(vo);
		String[] jsonFields = json.split(",");
		for (String field : jsonFields) {
			header.add(field.substring(field.indexOf("\"") + 1, field.lastIndexOf("\"")));
		}
	}

	private void exportResult(ExportToFileBean fileBean, List<AssetRegisterVo> results) {
		List<List<String>> content = fileBean.getContents();
		for (AssetRegisterVo result : results) {
			List<String> row = new ArrayList<String>();
			row.add(result.getMdlNm());
			row.add(result.getFilePath());
			row.add(result.getLotId());
			row.add(result.getLotNm());
			row.add(result.getPjtId());
			row.add(result.getChgFactorNo());
			row.add(result.getAreqId());
			row.add(result.getAreqCtgCode());
			row.add(result.getGrpId());
			row.add(result.getGrpNm());
			row.add(result.getAssetReqNm());
			row.add(result.getContent());
			row.add(result.getLendReqUserId());
			row.add(result.getLendReqUserNm());
			row.add(result.getLendReqDateTime());
			row.add(result.getRtnReqUserId());
			row.add(result.getRtnReqUserNm());
			row.add(result.getRtnReqDateTime());
			row.add(result.getDelReqUserId());
			row.add(result.getDelReqUserNm());
			row.add(result.getDelReqDateTime());
			row.add(result.getLendFileRevision());
			row.add(result.getLendFileByteSize());
			row.add(result.getLendFileUpdateDateTime());
			row.add(result.getRtnFileRevision());
			row.add(result.getRtnFileByteSize());
			row.add(result.getRtnFileUpdateDateTime());
			row.add(result.getDelFileRevision());
			row.add(result.getDelFileByteSize());
			row.add(result.getDelFileUpdateDateTime());
			row.add(result.getChgAppDateTime());
			row.add(result.getStsId());
			row.add(result.getProcStsId());
			row.add(result.getResourceLock());
			content.add(row);
		}
	}



	private final IRepEntity insertReportEntity(
			FlowChaLibAssetRegisterServiceBean paramBean ) {



		IRepEntity entity = new RepEntity();
		entity.setRepId				( this.support.nextvalByRepId() );
		entity.setRepCtgCd			( DcmReportType.AmRsReport.value() );
		entity.setExecUserNm		( paramBean.getUserName() );
		entity.setExecUserId		( paramBean.getUserId() );
		entity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setStsId				( DcmRepStatusId.ReportCreated.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setLotId				(paramBean.getAssetRegisterCondition().getLotId()[0]);
		this.support.getRepDao().insert( entity );

		return entity;
	}

	private String[] getBpIds( String[] areqIds ){

		BpAreqLnkCondition condition = new BpAreqLnkCondition();
		condition.setAreqIds( areqIds );

		List<IBpAreqLnkEntity> entityList = this.support.getBmFinderSupport().getBpAreqLnkDao().find( condition.getCondition() );
		List<String> bpIdList = new ArrayList<String>();

		for( IBpAreqLnkEntity entity : entityList ){
			if( !bpIdList.contains( entity.getBpId() ) ){
				bpIdList.add( entity.getBpId() );
			}
		}

		return bpIdList.toArray(new String[]{});
	}

	private String[] getAreqIds( FlowReportAssetRegisterCreationServiceBean paramBean ){

		ViewAssetRegisterCondition condition = this.getViewAssetRegisterCondition(paramBean);

		List<IViewAssetRegisterEntity> entityList = this.amSupport.getViewAssetRegisterDao().find(condition);
		List<String> areqIdList = new ArrayList<String>();

		for( IViewAssetRegisterEntity entity : entityList ){
			if( !areqIdList.contains( entity.getAreqId() ) ){
				areqIdList.add( entity.getAreqId() );
			}
		}

		return areqIdList.toArray(new String[]{});
	}

	private ViewAssetRegisterCondition getViewAssetRegisterCondition( FlowReportAssetRegisterCreationServiceBean paramBean ){

		String lotId = paramBean.getParam().getSelectedLotId();
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		ViewAssetRegisterCondition condition = new ViewAssetRegisterCondition();

		condition.setLotId( lotId );

		condition.setMdlNm( searchCondition.getModuleNm() );
		condition.setFilePath( searchCondition.getFilePath() );
		condition.setPjtId( searchCondition.getPjtId() );
		condition.setChgFactorNo( searchCondition.getReferenceId() );
		condition.setReqUserId( searchCondition.getSubmitterId() );
		condition.setGrpNm( searchCondition.getGroupId() );
		condition.setStsId( searchCondition.getStsId() );
		condition.setProcStsId( searchCondition.getProcStsId() );

		return condition;
	}

	private ItemLabelsBean getStatusLabel(IStatusId iStsId, String language) {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		ItemLabelsBean itemLabelsBean = new ItemLabelsBean();
		itemLabelsBean.setValue(iStsId.getStatusId());
		itemLabelsBean.setLabel(ca.getValue(iStsId.getMessageKey(), language));
		return itemLabelsBean;
	}

}
