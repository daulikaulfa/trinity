package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domain.areq.dto.FlowChaLibAssetRegisterServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */

public class FlowReportAssetRegisterCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private ConditionsSettingsView detailsView = new ConditionsSettingsView();
	private AssetRegisterFileResponse fileResponse = new AssetRegisterFileResponse();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowChaLibAssetRegisterServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public ConditionsSettingsView getDetailsView() {
		return detailsView;
	}
	public void setDetailsView(ConditionsSettingsView detailsView) {
		this.detailsView = detailsView;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowReportAssetRegisterCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public AssetRegisterFileResponse getFileResponse() {
		return fileResponse;
	}

	public FlowReportAssetRegisterCreationServiceBean setFileResponse(AssetRegisterFileResponse fileResponse ) {
		this.fileResponse = fileResponse;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private SearchCondition searchCondition = new SearchCondition();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}
	}

	public class ConditionsSettingsView {
		private String latestBaselineTag;

		public String getLatestBaselineTag() {
			return latestBaselineTag;
		}
		public ConditionsSettingsView setLatestBaselineTag(String latestBaselineTag) {
			this.latestBaselineTag = latestBaselineTag;
			return this;
		}
	}

	/**
	 * Search Condition
	 */

	public class SearchCondition {
		private List<String> pjtViews = new ArrayList<String>();
		private List<String> referenceViews = new ArrayList<String>();
		private List<ItemLabelsBean> submitterViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> procStatusViews = new ArrayList<ItemLabelsBean>();
		private List<String> moduleViews = new ArrayList<String>();

		private String[] lotId;
		private String[] lotNm;
		private String[] moduleNm;
		private String[] pjtId;
		private String[] referenceId;
		private String[] submitterId;
		private String[] groupId;
		private String[] stsId;
		private String[] procStsId;
		private String filePath;
		/**
		 * Return if true, Create list from master resources and requested resources.
		 */
		public boolean targetMW = true;

		public List<String> getPjtViews() {
			return pjtViews;
		}
		public SearchCondition setPjtViews(List<String> pjtViews) {
			this.pjtViews = pjtViews;
			return this;
		}

		public List<String> getReferenceViews() {
			return referenceViews;
		}
		public SearchCondition setReferenceViews(List<String> referenceViews) {
			this.referenceViews = referenceViews;
			return this;
		}

		public List<ItemLabelsBean> getSubmitterViews() {
			return submitterViews;
		}
		public SearchCondition setSubmitterViews(List<ItemLabelsBean> submitterViews) {
			this.submitterViews = submitterViews;
			return this;
		}

		public List<ItemLabelsBean> getGroupViews() {
			return groupViews;
		}
		public SearchCondition setGroupViews(List<ItemLabelsBean> groupViews) {
			this.groupViews = groupViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getProcStatusViews() {
			return procStatusViews;
		}
		public SearchCondition setProcStatusViews(List<ItemLabelsBean> procStatusViews) {
			this.procStatusViews = procStatusViews;
			return this;
		}

		public List<String> getModuleViews() {
			return moduleViews;
		}
		public void setModuleViews(List<String> moduleViews) {
			this.moduleViews = moduleViews;
		}
		public String[] getLotId() {
			return lotId;
		}
		public SearchCondition setLotId(String... lotId) {
			this.lotId = lotId;
			return this;
		}

		public String[] getLotNm() {
			return lotNm;
		}
		public SearchCondition setLotNm(String... lotNm) {
			this.lotNm = lotNm;
			return this;
		}

		public String[] getModuleNm() {
			return moduleNm;
		}
		public SearchCondition setModuleNm(String... moduleNm) {
			this.moduleNm = moduleNm;
			return this;
		}

		public String[] getPjtId() {
			return pjtId;
		}
		public SearchCondition setPjtId(String... pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String[] getReferenceId() {
			return referenceId;
		}
		public SearchCondition setReferenceId(String... referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String[] getSubmitterId() {
			return submitterId;
		}
		public SearchCondition setSubmitterId(String... submitterId) {
			this.submitterId = submitterId;
			return this;
		}

		public String[] getGroupId() {
			return groupId;
		}
		public SearchCondition setGroupId(String... groupId) {
			this.groupId = groupId;
			return this;
		}

		public String[] getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String... stsId) {
			this.stsId = stsId;
			return this;
		}

		public String[] getProcStsId() {
			return procStsId;
		}
		public SearchCondition setProcStsId(String... procStsId) {
			this.procStsId = procStsId;
			return this;
		}

		public String getFilePath() {
			return filePath;
		}
		public SearchCondition setFilePath(String filePath) {
			this.filePath = filePath;
			return this;
		}

		public boolean isTargetMW() {
			return targetMW;
		}
		public SearchCondition setTargetMW(boolean targetMW) {
			this.targetMW = targetMW;
			return this;
		}
	}

	/**
	 * Created File
	 */
	public class AssetRegisterFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public AssetRegisterFileResponse setFile(File file) {
			this.file = file;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}


