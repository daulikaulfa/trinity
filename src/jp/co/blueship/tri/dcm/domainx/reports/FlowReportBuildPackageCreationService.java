package jp.co.blueship.tri.dcm.domainx.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportBuildPackageCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportBuildPackageCreationServiceBean.BuildPackageView;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowReportBuildPackageCreationService implements IDomain<FlowReportBuildPackageCreationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;
	private FlowRelUnitEditSupport reportListSupport;
	private IDomain<IGeneralServiceBean> reportCreateService;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setFwSupport(IFwFinderSupport fwSupport) {
		this.fwSupport = fwSupport;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	public void setReportListSupport(FlowRelUnitEditSupport reportEditSupport) {
		this.reportListSupport = reportEditSupport;
	}

	public void setReportCreateService(IDomain<IGeneralServiceBean> reportCreateService) {
		this.reportCreateService = reportCreateService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReportBuildPackageCreationServiceBean> execute(
			IServiceDto<FlowReportBuildPackageCreationServiceBean> serviceDto) {
		FlowReportBuildPackageCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowReportBuildPackageCreationServiceBean paramBean) {
		this.onChange(paramBean);
	}

	private void onChange(FlowReportBuildPackageCreationServiceBean paramBean) {
		int linesPerPage = paramBean.getParam().getLinesPerPage();
		int pageNo = paramBean.getParam().getSelectedPageNo();

		removeNotSelectedBuildPackageId(paramBean);
		IEntityLimit<IBpEntity> limit = this.reportListSupport.getBpDao()
				.find(this.getCondition(paramBean).getCondition(), pageNo, linesPerPage);

		IPageNoInfo page = DcmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setBuildPackageViews(getBuildPackageViewsFromEntity(paramBean, limit.getEntities()));
		setSelectedBuildPackageId(paramBean);
		paramBean.setPage(page);
	}


	private void submitChanges(FlowReportBuildPackageCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitReportServiceBean serviceBean = paramBean.getInnerService();
		dto.setServiceBean(serviceBean);
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		this.beforeExecution(paramBean, serviceBean);

		// Status Matrix Check
		{
			String lotId = paramBean.getParam().getSelectedLotId();
			String[] bpIds = paramBean.getParam().getListSelection().getSelectedIds();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( reportListSupport )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotId )
					.setBpIds		( bpIds )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		reportCreateService.execute(dto);
		
		this.afterExecution( serviceBean, paramBean );
	}

	private void removeNotSelectedBuildPackageId(FlowReportBuildPackageCreationServiceBean paramBean) {
		Set<String> currentPageBuildIds = new HashSet<String>();
		for(BuildPackageView bpView : paramBean.getBuildPackageViews()) {
			currentPageBuildIds.add(bpView.getBpId());
		}
		//Get current NOT checked id from current paramBean
		currentPageBuildIds.removeAll(Arrays.asList(paramBean.getParam().getListSelection().getSelectedIds()));
		//Remove not checked id from all id
		paramBean.getParam().getListSelection().getSelectedIdSet().removeAll(currentPageBuildIds);

		Set<String> allSelectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		String[] arrIds = allSelectedIds.toArray(new String[allSelectedIds.size()]);
		paramBean.getParam().getListSelection().setSelectedIds(arrIds);

	}

	private void setSelectedBuildPackageId(FlowReportBuildPackageCreationServiceBean paramBean) {
		Set<String> selectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		for(BuildPackageView bpView : paramBean.getBuildPackageViews()) {
			if (selectedIds.contains(bpView.getBpId())) {
				bpView.setSelected(true);
			}
		}
	}

	private List<BuildPackageView> getBuildPackageViewsFromEntity(FlowReportBuildPackageCreationServiceBean paramBean,
			List<IBpEntity> entities) {
		List<BuildPackageView> reportViews = new ArrayList<BuildPackageView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		for (IBpEntity entity : entities) {
			BuildPackageView view = paramBean.new BuildPackageView()
					.setBpId(entity.getBpId())
					.setBpNm(entity.getBpNm())
					.setSubmitterNm(entity.getExecUserNm())
					.setSubmitterIconPath( this.umSupport.getIconPath(entity.getExecUserId()) )
					.setCreatedDate(TriDateUtils.convertViewDateFormat(entity.getProcStTimestamp(), formatYMD))
					.setStsId(entity.getProcStsId())
					.setStatus( sheet.getValue(RmDesignBeanId.statusId, entity.getProcStsId()) )
			;

			reportViews.add(view);
		}

		return reportViews;
	}

	private BpCondition getCondition(FlowReportBuildPackageCreationServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();
		BpCondition condition = new BpCondition();
		condition.setLotId(lotId);

		return condition;
	}

	private void beforeExecution(FlowReportBuildPackageCreationServiceBean paramBean,
			FlowRelUnitReportServiceBean serviceBean) {
		removeNotSelectedBuildPackageId(paramBean);

		serviceBean.setUserId(paramBean.getUserId());
		serviceBean.setUserName(paramBean.getUserName());
		serviceBean.setReportId(DcmReportType.BmBpReport.value());
		serviceBean.setSelectedLotNo(paramBean.getParam().getSelectedLotId());
		serviceBean.setSelectedBuildNo(paramBean.getParam().getListSelection().getSelectedIds());
		serviceBean.setScreenType(ScreenType.next);
		serviceBean.setProcId(paramBean.getProcId());
		serviceBean.setFlowAction("FlowRelUnitReportService");

		if (paramBean.getParam().getListSelection().getSelectedIds().length == 0) {
			throw new ContinuableBusinessException(DcmMessageId.DCM001004E);
		}
	}
	
	private void afterExecution( FlowRelUnitReportServiceBean src,
			FlowReportBuildPackageCreationServiceBean dest ) {
		
		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(DcmMessageId.DCM003002I);
		}
	}

}
