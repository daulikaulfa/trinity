package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.domain.ra.dto.FlowRelApplyTicketServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReportReleaseRequestCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ReleaseRequestView> releaseRequestViews = new ArrayList<ReleaseRequestView>();
	private IPageNoInfo page = new PageNoInfo();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowRelApplyTicketServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public List<ReleaseRequestView> getReleaseRequestViews() {
		return releaseRequestViews;
	}
	public FlowReportReleaseRequestCreationServiceBean setReleaseRequestViews(List<ReleaseRequestView> releaseRequestViews) {
		this.releaseRequestViews = releaseRequestViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReportReleaseRequestCreationServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportReleaseRequestCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private int linesPerPage = 20;
		private int selectedPageNo = 1;
		private ListSelection listSelection = new ListSelection();

		public String getSelectedLotId() {
			return lotId;
		}

		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}

		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public int getSelectedPageNo() {
			return selectedPageNo;
		}

		public void setSelectedPageNo(int selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
		}

		public ListSelection getListSelection() {
			return listSelection;
		}
		public RequestParam setListSelection(ListSelection listSelection) {
			this.listSelection = listSelection;
			return this;
		}
	}

	/**
	 * List Selection
	 */
	public class ListSelection extends jp.co.blueship.tri.fw.svc.beans.dto.ListSelection {

	}

	public class ReleaseRequestView {
		private boolean isSelected = false;
		private String raId;
		private String bpId;
		private String relEnv;
		private String submitterNm;
		private String submitterIconPath;
		private String submittedDate;
		private String stsId;
		private String status;

		public boolean isSelected() {
			return isSelected;
		}
		public ReleaseRequestView setSelected(boolean isSelected) {
			this.isSelected = isSelected;
			return this;
		}

		public String getRaId() {
			return raId;
		}
		public ReleaseRequestView setRaId(String raId) {
			this.raId = raId;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public ReleaseRequestView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getRelEnv() {
			return relEnv;
		}
		public ReleaseRequestView setRelEnv(String relEnv) {
			this.relEnv = relEnv;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ReleaseRequestView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ReleaseRequestView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getSubmittedDate() {
			return submittedDate;
		}
		public ReleaseRequestView setSubmittedDate(String submittedDate) {
			this.submittedDate = submittedDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReleaseRequestView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReleaseRequestView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String repId;
		private boolean completed = false;

		public String getRepId() {
			return repId;
		}
		public RequestsCompletion setRepId(String repId) {
			this.repId = repId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
