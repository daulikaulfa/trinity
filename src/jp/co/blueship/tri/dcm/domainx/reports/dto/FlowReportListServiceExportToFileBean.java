package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ReportView;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;


/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author sam
 */
public class FlowReportListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReportListFileResponse fileResponse = new ReportListFileResponse();
	private List<ReportView> reportViews = new ArrayList<ReportView>();

	{
		this.setInnerService( new FlowReportListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReportListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReportListServiceExportToFileBean setFileResponse(ReportListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<ReportView> getReportViews() {
		return reportViews;
	}

	public void setReportViews(List<ReportView> reportViews) {
		this.reportViews = reportViews;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowReportListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class ReportListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
