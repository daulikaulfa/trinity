package jp.co.blueship.tri.dcm.domainx.reports;

import java.io.File;
import java.io.IOException;

import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowReportDownloadService implements IDomain<FlowReportDownloadServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> downloadService = null;

	public void setDownloadService(IDomain<IGeneralServiceBean> downloadService) {
		this.downloadService = downloadService;
	}

	@Override
	public IServiceDto<FlowReportDownloadServiceBean> execute(IServiceDto<FlowReportDownloadServiceBean> serviceDto) {
		FlowReportDownloadServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelReportDownloadServiceBean innerServiceBean = null;
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			innerServiceBean = paramBean.getInnerService();

			String repId = paramBean.getParam().getSelectedRepId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(repId), "SelectedRepId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	private void init(FlowReportDownloadServiceBean paramBean) throws IOException {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelReportDownloadServiceBean serviceBean = paramBean.getInnerService();

		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		downloadService.execute(dto);

		this.afterExecution(paramBean, serviceBean);
	}

	private void beforeExecution(FlowReportDownloadServiceBean paramBean,
			FlowRelReportDownloadServiceBean serviceBean) {
		serviceBean.setReportNo(paramBean.getParam().getSelectedRepId());
		serviceBean.setScreenType(ScreenType.next);
		serviceBean.setUserId(paramBean.getUserId());
	}

	private void afterExecution(FlowReportDownloadServiceBean paramBean, FlowRelReportDownloadServiceBean serviceBean) {
		File report = new File(serviceBean.getDownloadPath(), serviceBean.getDownloadFile());
		paramBean.getFileResponse().setFile(report);
	}

}
