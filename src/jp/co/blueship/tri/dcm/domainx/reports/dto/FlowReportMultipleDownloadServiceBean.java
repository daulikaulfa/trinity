package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.io.File;

import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowReportMultipleDownloadServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private File report;

	private RequestParam param = new RequestParam();
	{
		this.setInnerService(new FlowRelReportDownloadServiceBean());
	}

	public File getReport() {
		return report;
	}

	public void setReport(File report) {
		this.report = report;
	}

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private String[] repId = null;

		public String getSelectedLotId() {
			return lotId;
		}

		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String[] getRepId() {
			return repId;
		}

		public RequestParam setRepId(String[] reportId) {
			this.repId = reportId;
			return this;
		}
	}

}
