package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowReportListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ReportView> reportViews = new ArrayList<ReportView>();
	private IPageNoInfo page = new PageNoInfo();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public List<ReportView> getReportViews() {
		return reportViews;
	}
	public FlowReportListServiceBean setReportViews(List<ReportView> reportViews) {
		this.reportViews = reportViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowReportListServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchCondition searchCondition = new SearchCondition();
		private RequestOption requestOption = RequestOption.none;
		private int linesPerPage = 20;
		private ListSelection listSelection = new ListSelection();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public ListSelection getListSelection() {
			return listSelection;
		}
		public RequestParam setListSelection(ListSelection listSelection) {
			this.listSelection = listSelection;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter{
		@Expose private DcmReportType type = DcmReportType.none;
		@Expose private String stsId;
		@Expose private String ctgId;
		@Expose private String mstoneId;
		@Expose private String keyword;
		private Integer selectedPageNo = 1;

		private List<ItemLabelsBean> typeViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public DcmReportType getType() {
			return type;
		}
		public SearchCondition setType(DcmReportType type) {
			this.type = type;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getTypeViews() {
			return typeViews;
		}
		public SearchCondition setTypeViews(List<ItemLabelsBean> typeViews) {
			this.typeViews = typeViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}
	
	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		selectResource( "selectResource" ),
		unselectResource( "unselectResource" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}

	}

	/**
	 * List Selection
	 */
	public class ListSelection {
		private Set<String> selectedIdSet = new LinkedHashSet<String>();
		private List<ReportSelect> reportSelect = new ArrayList<ReportSelect>();

		public Set<String> getSelectedIdSet() {
			return selectedIdSet;
		}
		public void setSelectedIdSet(Set<String> selectedIdSet) {
			this.selectedIdSet = selectedIdSet;
		}
		
		public List<ReportSelect> getReportSelect() {
			return reportSelect;
		}

		public void setReportSelect(List<ReportSelect> reportSelect) {
			this.reportSelect = reportSelect;
		}
		
		/**
		 * Class for convert selected reports from json.
		 */
		public class ReportSelect {
			private String repId;
			private boolean selected;

			public String getRepId() {
				return repId;
			}

			public void setRepId(String reportId) {
				this.repId = reportId;
			}

			public boolean isSelected() {
				return selected;
			}

			public void setSelected(boolean selected) {
				this.selected = selected;
			}
		}
	}

	public class ReportView {
		private boolean isDownloadEnable;
		private boolean isSelected = false;
		private String repId;
		private DcmReportType type;
		private String submitterNm;
		private String submitterIconPath;
		private String createdDate;
		private String stsId;
		private String status;

		public boolean isDownloadEnable() {
			return isDownloadEnable;
		}
		public ReportView setDownloadEnable(boolean isDownloadEnable) {
			this.isDownloadEnable = isDownloadEnable;
			return this;
		}

		public boolean isSelected() {
			return isSelected;
		}
		public void setSelected(boolean isSelected) {
			this.isSelected = isSelected;
		}
		public String getReportId() {
			return repId;
		}
		public ReportView setReportId(String reportId) {
			this.repId = reportId;
			return this;
		}

		public DcmReportType getType() {
			return type;
		}
		public ReportView setType(DcmReportType type) {
			this.type = type;
			return this;
		}

		public String getSubmitterNm() {
			return submitterNm;
		}
		public ReportView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ReportView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getCreatedDate() {
			return createdDate;
		}
		public ReportView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public ReportView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public ReportView setStatus(String status) {
			this.status = status;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private List<String> selectedIds = new ArrayList<String>();

		public List<String> getSelectedIds() {
			return selectedIds;
		}
		public void setSelectedIds(List<String> selectedIds) {
			this.selectedIds = selectedIds;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
