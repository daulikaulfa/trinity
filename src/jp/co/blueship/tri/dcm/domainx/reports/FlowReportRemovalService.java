package jp.co.blueship.tri.dcm.domainx.reports;

import java.util.List;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportRemovalServiceBean;
import jp.co.blueship.tri.dcm.support.FlowReportSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowReportRemovalService implements IDomain<FlowReportRemovalServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> removalService = null;
	private FlowReportSupport support = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setRemovalService(IDomain<IGeneralServiceBean> removalService) {
		this.removalService = removalService;
	}

	public void setSupport( FlowReportSupport support ){
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReportRemovalServiceBean> execute(IServiceDto<FlowReportRemovalServiceBean> serviceDto) {
		FlowReportRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelReportDeleteServiceBean innerServiceBean = null;
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			innerServiceBean = paramBean.getInnerService();

			String[] repIds = paramBean.getParam().getSelectedRepIds();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(repIds), "SelectedRepIds is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	private void init(FlowReportRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelReportDeleteServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		// Status Matrix Check
		{
			String[] repIds = paramBean.getParam().getSelectedRepIds();

			RepCondition condition = new RepCondition();
			condition.setRepIds( repIds );

			List<IRepEntity> entityList = this.support.getRepDao().find( condition.getCondition() );
			String[] lotIds = new String[]{};

			if( entityList.size() != 0 ){
				lotIds = new String[]{entityList.get(0).getLotId()};
			}

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( this.support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotIds )
					.setRepIds		( paramBean.getParam().getSelectedRepIds() )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		removalService.execute(dto);
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(DcmMessageId.DCM003006I);
	}

	private void beforeExecution(FlowReportRemovalServiceBean paramBean, FlowRelReportDeleteServiceBean serviceBean) {
		serviceBean.setUserId(paramBean.getUserId());
		serviceBean.setSelectedDelete(paramBean.getParam().getSelectedRepIds());
	}
}
