package jp.co.blueship.tri.dcm.domainx.reports;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.ra.dto.FlowRelApplyTicketServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportReleaseRequestCreationServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportReleaseRequestCreationServiceBean.ReleaseRequestView;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.support.FlowRelApplyEditSupport;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowReportReleaseRequestCreationService implements IDomain<FlowReportReleaseRequestCreationServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;
	private FlowRelApplyEditSupport reportListSupport;
	private IDomain<IGeneralServiceBean> reportCreateService;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setFwSupport(IFwFinderSupport fwSupport) {
		this.fwSupport = fwSupport;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	public void setReportListSupport(FlowRelApplyEditSupport reportEditSupport) {
		this.reportListSupport = reportEditSupport;
	}

	public void setReportCreateService(IDomain<IGeneralServiceBean> reportCreateService) {
		this.reportCreateService = reportCreateService;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowReportReleaseRequestCreationServiceBean> execute(
			IServiceDto<FlowReportReleaseRequestCreationServiceBean> serviceDto) {
		FlowReportReleaseRequestCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				 this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowReportReleaseRequestCreationServiceBean paramBean) {
		this.onChange(paramBean);
	}

	private void onChange(FlowReportReleaseRequestCreationServiceBean paramBean) {
		int linesPerPage = paramBean.getParam().getLinesPerPage();
		int pageNo = paramBean.getParam().getSelectedPageNo();

		removeNotSelectedReleaseRequestId(paramBean);
		IEntityLimit<IRaEntity> limit = this.reportListSupport.getRaDao()
				.find(this.getCondition(paramBean).getCondition(), pageNo, linesPerPage);

		IPageNoInfo page = DcmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setReleaseRequestViews(getReleaseRequestViewsFromEntity(paramBean, limit.getEntities()));
		setSelectedReleaseRequest(paramBean);
		paramBean.setPage(page);
	}

	private void removeNotSelectedReleaseRequestId(FlowReportReleaseRequestCreationServiceBean paramBean) {
		Set<String> currentPageReleaseIds = new HashSet<String>();
		for(ReleaseRequestView raView : paramBean.getReleaseRequestViews()) {
			currentPageReleaseIds.add(raView.getRaId());
		}
		//Get current NOT checked id from current paramBean
		currentPageReleaseIds.removeAll(Arrays.asList(paramBean.getParam().getListSelection().getSelectedIds()));
		//Remove not checked id from all id
		paramBean.getParam().getListSelection().getSelectedIdSet().removeAll(currentPageReleaseIds);

		Set<String> allSelectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		String[] arrIds = allSelectedIds.toArray(new String[allSelectedIds.size()]);
		paramBean.getParam().getListSelection().setSelectedIds(arrIds);

	}

	private void setSelectedReleaseRequest(FlowReportReleaseRequestCreationServiceBean paramBean) {
		Set<String> selectedIds = paramBean.getParam().getListSelection().getSelectedIdSet();
		for(ReleaseRequestView raView : paramBean.getReleaseRequestViews()) {
			if (selectedIds.contains(raView.getRaId())) {
				raView.setSelected(true);
			}
		}
	}

	private List<ReleaseRequestView> getReleaseRequestViewsFromEntity(
			FlowReportReleaseRequestCreationServiceBean paramBean, List<IRaEntity> entities) {
		List<ReleaseRequestView> reportViews = new ArrayList<ReleaseRequestView>();
		List<IRaDto> raDtoList = this.reportListSupport.findRaDto(entities);
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		for (IRaDto dto : raDtoList) {

			ReleaseRequestView view = paramBean.new ReleaseRequestView()
				.setRaId(dto.getRaEntity().getRaId())
				.setBpId(dto.getRaBpLnkEntities().get(0).getBpId())
				.setSubmitterNm(dto.getRaEntity().getRegUserNm())
				.setSubmitterIconPath(this.umSupport.getIconPath(dto.getRaEntity().getRegUserId()))
				.setSubmittedDate(TriDateUtils.convertViewDateFormat(dto.getRaEntity().getRegTimestamp(), formatYMD))
				.setStsId(dto.getRaEntity().getProcStsId())
				.setStatus( sheet.getValue(RmDesignBeanId.statusId, dto.getRaEntity().getProcStsId()) )
			;

			IBldEnvEntity bldEnvEntity = this.reportListSupport.getBmFinderSupport().findBldEnvEntity(dto.getRaEntity().getBldEnvId());
			view.setRelEnv(bldEnvEntity.getBldEnvNm());

			reportViews.add(view);
		}

		return reportViews;
	}

	private RaCondition getCondition(FlowReportReleaseRequestCreationServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();
		RaCondition condition = new RaCondition();
		condition.setLotId(lotId);

		return condition;
	}

	private void submitChanges(FlowReportReleaseRequestCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelApplyTicketServiceBean serviceBean = paramBean.getInnerService();
		dto.setServiceBean(serviceBean);
		TriPropertyUtils.copyProperties(serviceBean, paramBean);

		removeNotSelectedReleaseRequestId(paramBean);
		String[] raIds = paramBean.getParam().getListSelection().getSelectedIds();
		if (raIds.length == 0) {
			throw new ContinuableBusinessException(DcmMessageId.DCM001004E);
		}

		// Status Matrix Check
		{
			String lotId = paramBean.getParam().getSelectedLotId();

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( serviceBean )
					.setFinder		( reportListSupport )
					.setActionList	( statusMatrixAction )
					.setLotIds		( lotId )
					.setRaIds		( raIds )
					;
			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		for (int i = 0; i < raIds.length; i++) {
			serviceBean.setSelectedRelApplyNo(new String[] { raIds[i] });

			this.beforeExecution(paramBean, serviceBean);
			reportCreateService.execute(dto);
		}

		this.afterExecution( serviceBean, paramBean );
	}

	private void beforeExecution(FlowReportReleaseRequestCreationServiceBean paramBean,
			FlowRelApplyTicketServiceBean serviceBean) {
		serviceBean.setUserId(paramBean.getUserId());
		serviceBean.setUserName(paramBean.getUserName());
		serviceBean.setReportId(DcmReportType.RmRaReport.value());
		serviceBean.setSelectedLotNo(paramBean.getParam().getSelectedLotId());
		serviceBean.setScreenType(ScreenType.next);
		serviceBean.setProcId(paramBean.getProcId());
		serviceBean.setFlowAction("FlowRelApplyTicketService");
	}

	private void afterExecution( FlowRelApplyTicketServiceBean src,
			FlowReportReleaseRequestCreationServiceBean dest ) {

		if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(DcmMessageId.DCM003004I);
		}
	}

}
