package jp.co.blueship.tri.dcm.domainx.reports.dto;

import java.io.File;

import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowReportDownloadServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ReportFileResponse fileResponse = new ReportFileResponse();

	{
		this.setInnerService(new FlowRelReportDownloadServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportDownloadServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ReportFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowReportDownloadServiceBean setFileResponse(ReportFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String repId;

		public String getSelectedRepId() {
			return repId;
		}
		public RequestParam setSelectedRepId(String repId) {
			this.repId = repId;
			return this;
		}
	}

	public class ReportFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
