package jp.co.blueship.tri.dcm.domainx.reports.dto;

import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowReportRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService(new FlowRelReportDeleteServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowReportRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String[] repIds = {};

		public String[] getSelectedRepIds() {
			return repIds;
		}
		public RequestParam setSelectedRepIds(String... reportIds) {
			this.repIds = reportIds;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
