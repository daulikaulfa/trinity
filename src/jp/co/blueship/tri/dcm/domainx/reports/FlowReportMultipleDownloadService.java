package jp.co.blueship.tri.dcm.domainx.reports;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportMultipleDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowReportMultipleDownloadService implements IDomain<FlowReportMultipleDownloadServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> downloadService = null;

	public void setDownloadService(IDomain<IGeneralServiceBean> downloadService) {
		this.downloadService = downloadService;
	}

	@Override
	public IServiceDto<FlowReportMultipleDownloadServiceBean> execute(
			IServiceDto<FlowReportMultipleDownloadServiceBean> serviceDto) {
		FlowReportMultipleDownloadServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelReportDownloadServiceBean innerServiceBean = null;
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			innerServiceBean = paramBean.getInnerService();

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}
	}

	private void init(FlowReportMultipleDownloadServiceBean paramBean) throws IOException {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelReportDownloadServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);
		List<File> reports = new ArrayList<File>();

		String[] reportIds = paramBean.getParam().getRepId();
		for (int i = 0; i < reportIds.length; i++) {
			serviceBean.setReportNo(reportIds[i]);

			this.beforeExecution(paramBean, serviceBean);
			downloadService.execute(dto);
			this.afterExecution(paramBean, serviceBean, reports);
		}
		File zipReport = createZipReport(reports, paramBean);
		paramBean.setReport(zipReport);
	}

	private File createZipReport(List<File> reports, FlowReportMultipleDownloadServiceBean paramBean)
			throws IOException {
		Path directory = Paths
				.get(DcmDesignBusinessRuleUtils.getDownloadTempPath(paramBean.getUserId()).getAbsolutePath());
		File zipReport = Files.createTempFile(directory, "trinity_report-", ".zip").toFile();
		FileOutputStream fos = new FileOutputStream(zipReport);
		ZipOutputStream zos = new ZipOutputStream(fos);
		addToZipFile(reports, zos);

		zos.close();
		fos.close();
		return zipReport;
	}

	private void addToZipFile(List<File> reports, ZipOutputStream zos) throws IOException {
		for (int i = 0; i < reports.size(); i++) {
			File report = reports.get(i);
			FileInputStream fis = new FileInputStream(report);
			ZipEntry zipEntry = new ZipEntry(report.getName());
			zos.putNextEntry(zipEntry);

			byte[] bytes = new byte[1024];
			int length;
			while ((length = fis.read(bytes)) >= 0) {
				zos.write(bytes, 0, length);
			}

			zos.closeEntry();
			fis.close();
		}

	}

	private void beforeExecution(FlowReportMultipleDownloadServiceBean paramBean,
			FlowRelReportDownloadServiceBean serviceBean) {
		serviceBean.setScreenType(ScreenType.next);
		serviceBean.setUserId(paramBean.getUserId());
	}

	private void afterExecution(FlowReportMultipleDownloadServiceBean paramBean,
			FlowRelReportDownloadServiceBean serviceBean, List<File> reports) {
		File report = new File(serviceBean.getDownloadPath(), serviceBean.getDownloadFile());
		reports.add(report);
	}

}
