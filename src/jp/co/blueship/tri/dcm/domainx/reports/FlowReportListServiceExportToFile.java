package jp.co.blueship.tri.dcm.domainx.reports;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ReportView;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.SearchCondition;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceExportToFileBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;

public class FlowReportListServiceExportToFile implements IDomain<FlowReportListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> reportList = null;

	public void setReportList(IDomain<IGeneralServiceBean> reportList) {
		this.reportList = reportList;
	}

	@Override
	public IServiceDto<FlowReportListServiceExportToFileBean> execute(
			IServiceDto<FlowReportListServiceExportToFileBean> serviceDto) {
		FlowReportListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowReportListServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if(RequestType.onChange.equals(paramBean.getParam().getRequestType())){
				this.onChange(paramBean);
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005030S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowReportListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowReportListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowReportListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			reportList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		reportList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowReportListServiceExportToFileBean src, FlowReportListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

			destSearchCondition.setStsId(srcSearchConditon.getStsId());
			destSearchCondition.setType(srcSearchConditon.getType());
			destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
			destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);

	}

	private void afterExecution(FlowReportListServiceBean src, FlowReportListServiceExportToFileBean dest) {
		List<ReportView> srcReportViews = src.getReportViews();
		List<ReportView> destReportViews = new ArrayList<ReportView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcReportViews.size(); i++) {
				ReportView view = srcReportViews.get(i);
				destReportViews.add(view);
			}
			dest.setReportViews(destReportViews);
		}
	}
}
