package jp.co.blueship.tri.dcm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。
 * ExtractEntityAddonUtilからの派生です。依存性の解消のために派生させています。
 *
 * @author Takashi Ono
 *
 */

public class DcmExtractEntityAddonUtil {

	/**
	 * 指定されたリストからリリース情報の配列を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IRepEntity[] extractReportArray(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof IRepEntity[]) {
				return ((IRepEntity[]) obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりエンティティのリスト、配列を取得します。
	 *
	 * @param list パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static List<IEntity> extractEntities(List<Object> list) {

		List<IEntity> entityList = new ArrayList<IEntity>();

		for (Object obj : list) {
			if (obj instanceof IEntity) {
				entityList.add((IEntity) obj);
			}
			if (obj instanceof IEntity[]) {
				entityList.addAll(FluentList.from((IEntity[]) obj).asList());
			}
		}
		return entityList;
	}

	/**
	 * 指定されたリストからロット情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotEntity extractPjtLot(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof ILotEntity) {
				return (ILotEntity) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからロットDto情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotDto extractLotDto(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof ILotDto) {
				return (ILotDto) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからレポート情報エンティティを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IRepEntity extractReport(List<Object> list) throws TriSystemException {

		for (Object obj : list) {
			if (obj instanceof IRepEntity) {
				return ((IRepEntity) obj);
			}
		}

		return null;
	}
	
	public static final ReportServiceBean extractReportServiceBean(List<Object> list) throws TriSystemException {

		for (Object obj : list) {
			if (obj instanceof ReportServiceBean) {
				return ((ReportServiceBean) obj);
			}
		}

		return null;
	}
}
