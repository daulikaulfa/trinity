package jp.co.blueship.tri.dcm;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmTaskAddonUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.dcm.beans.dto.IReportAssetApplyInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportBuildInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportPjtInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportAssetApplyInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportBuildInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportPjtInfo;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskDefineId;
import jp.co.blueship.tri.fw.cmn.utils.AssetCounterInfo;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;

/**
 * エンティティ、ビーンの変換共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ReportConverterAddonUtil {


	/**
	 * 変更管理エンティティをレポート用に変換する
	 * @param entities
	 * @return
	 */
	public static List<IReportPjtInfo> convertReportPjtEntity( IPjtEntity[] entities ) {

		List<IReportPjtInfo> reportEntityList = new ArrayList<IReportPjtInfo>();

		for ( IPjtEntity entity : entities ) {
			IReportPjtInfo reportEntity = new ReportPjtInfo();
			TriPropertyUtils.copyProperties( reportEntity, entity );

			reportEntityList.add( reportEntity );
		}

		return reportEntityList;

	}

	/**
	 * ビルドエンティティをレポート用に変換する
	 * @param entities
	 * @param support
	 * @return
	 */
	public static  List<IReportBuildInfo> convertReportBuildEntity( List<IBpDto> entities, FinderSupport support ) {

		List<IReportBuildInfo> reportEntityList = new ArrayList<IReportBuildInfo>();

		for ( IBpDto entity : entities ) {

			Map<String, IAssetCounterInfo> applyNoCounterMap =
					BmTaskAddonUtils.getAssetCount( entity, support.getBmFinderSupport(), new ArrayList<ITaskFlowResEntity>() );

			IReportBuildInfo reportEntity = new ReportBuildInfo();
			TriPropertyUtils.copyProperties( reportEntity, entity.getBpEntity() );
			AreqCondition condition = new AreqCondition();
			condition.setLotId(entity.getBpEntity().getLotId());
			List<IAreqEntity> areqEntityList = support.getAmFinderSupport().getAreqDao().find(condition.getCondition());
			reportEntity.setBuildApply(areqEntityList.toArray(new IAreqEntity[0]));

			//総資産数
			boolean isConcentrate = false;
			int lendAssetSumCount = 0;
			int changeAssetSumCount = 0;
			int deleteAssetSumCount = 0;
			int addAssetSumCount = 0;

			IAssetCounterInfo counter = new AssetCounterInfo();
			if ( applyNoCounterMap.containsKey( TaskDefineId.deleteBinaryAssetCount.value() ) ) {
				counter = applyNoCounterMap.get( TaskDefineId.deleteBinaryAssetCount.value() );
			}

			for ( String key : applyNoCounterMap.keySet() ) {
				if ( TaskDefineId.deleteBinaryAssetCount.value().equals( key ) ) {
					continue;
				}

				IAssetCounterInfo assetCount = applyNoCounterMap.get( key );

				if (  null != assetCount.getAddCount()
						|| null != assetCount.getChangeCount()
						|| null != assetCount.getDelCount() ) {
					isConcentrate = true;
				}

				// 申請情報
				IAreqEntity assetApplyentity = support.getAmFinderSupport().findAreqEntity( key );

				if ( DcmBusinessJudgUtils.isReturnApply( assetApplyentity )) {
					// 貸出資産数
					if ( null != assetCount.getLendCount() ) {
						lendAssetSumCount += assetCount.getLendCount();
					}

					// 追加資産数
					if ( null != assetCount.getAddCount() ) {
						addAssetSumCount += assetCount.getAddCount();
					}

					// 変更資産数
					if ( null != assetCount.getChangeCount() ) {
						changeAssetSumCount += assetCount.getChangeCount();
					}

				} else if ( DcmBusinessJudgUtils.isDeleteApply( assetApplyentity )) {
					// 削除資産数
					if ( null != assetCount.getDelCount() ) {
						deleteAssetSumCount += assetCount.getDelCount();
					}
				}

			}

			reportEntity.setLendAssetSumCount( Integer.toString(lendAssetSumCount) );

			if ( isConcentrate ) {
				reportEntity.setAddAssetSumCount( Integer.toString(addAssetSumCount) );
				reportEntity.setChangeAssetSumCount( Integer.toString(changeAssetSumCount) );
				reportEntity.setDelAssetSumCount( Integer.toString(deleteAssetSumCount) );
			}

			if ( null != counter.getDelCount() ) {
				reportEntity.setDelBinaryAssetSumCount( counter.getDelCount().toString() );
			}

			reportEntityList.add( reportEntity );
		}

		return reportEntityList;

	}

	/**
	 * リリースエンティティからレポート用ビルドエンティティを取得する。
	 * @param entities
	 * @param mergeOrderMap
	 * @param support
	 * @return
	 */
	public static  List<IReportBuildInfo> convertReportBuildEntity(
			List<IBpDto> buildEntities,
			Map<String, String> mergeOrderMap,
			FinderSupport support ) {

		List<IReportBuildInfo> reportEntityList = new ArrayList<IReportBuildInfo>();

		for ( IBpDto entity : buildEntities ) {

			Map<String, IAssetCounterInfo> applyNoCounterMap =
					BmTaskAddonUtils.getAssetCount( entity, support.getBmFinderSupport(), new ArrayList<ITaskFlowResEntity>() );

			IReportBuildInfo reportEntity = new ReportBuildInfo();
			TriPropertyUtils.copyProperties( reportEntity, entity.getBpEntity() );
			AreqCondition condition = new AreqCondition();
			condition.setLotId(entity.getBpEntity().getLotId());
			List<IAreqEntity> areqEntityList = support.getAmFinderSupport().getAreqDao().find(condition.getCondition());
			reportEntity.setBuildApply(areqEntityList.toArray(new IAreqEntity[0]));

			//総資産数
			boolean isConcentrate = false;
			int lendAssetSumCount = 0;
			int changeAssetSumCount = 0;
			int deleteAssetSumCount = 0;
			int addAssetSumCount = 0;

			IAssetCounterInfo counter = new AssetCounterInfo();
			if ( applyNoCounterMap.containsKey( TaskDefineId.deleteBinaryAssetCount.value() ) ) {
				counter = applyNoCounterMap.get( TaskDefineId.deleteBinaryAssetCount.value() );
			}

			for ( String key : applyNoCounterMap.keySet() ) {
				if ( TaskDefineId.deleteBinaryAssetCount.value().equals( key ) ) {
					continue;
				}

				IAssetCounterInfo assetCount = applyNoCounterMap.get( key );

				if (  null != assetCount.getAddCount()
						|| null != assetCount.getChangeCount()
						|| null != assetCount.getDelCount() ) {
					isConcentrate = true;
				}

				// 申請情報
				IAreqEntity assetApplyentity = support.getAmFinderSupport().findAreqEntity( key );

				if ( DcmBusinessJudgUtils.isReturnApply( assetApplyentity )) {
					// 貸出資産数
					if ( null != assetCount.getLendCount() ) {
						lendAssetSumCount += assetCount.getLendCount();
					}

					// 追加資産数
					if ( null != assetCount.getAddCount() ) {
						addAssetSumCount += assetCount.getAddCount();
					}

					// 変更資産数
					if ( null != assetCount.getChangeCount() ) {
						changeAssetSumCount += assetCount.getChangeCount();
					}

				} else if ( DcmBusinessJudgUtils.isDeleteApply( assetApplyentity )) {
					// 削除資産数
					if ( null != assetCount.getDelCount() ) {
						deleteAssetSumCount += assetCount.getDelCount();
					}
				}

			}

			reportEntity.setLendAssetSumCount( Integer.toString(lendAssetSumCount) );

			if ( isConcentrate ) {
				reportEntity.setAddAssetSumCount( Integer.toString(addAssetSumCount) );
				reportEntity.setChangeAssetSumCount( Integer.toString(changeAssetSumCount) );
				reportEntity.setDelAssetSumCount( Integer.toString(deleteAssetSumCount) );
			}

			if ( null != counter.getDelCount() ) {
				reportEntity.setDelBinaryAssetSumCount( counter.getDelCount().toString() );
			}

			String mergeOrder = mergeOrderMap.get( entity.getBpEntity().getBpId() );
			if ( null == mergeOrder ) mergeOrder = "";

			reportEntity.setMergeOrder( mergeOrder );

			reportEntityList.add( reportEntity );
		}

		sortUnitBean( reportEntityList );

		return reportEntityList;

	}

	/**
	 * IReportBuildInfoをマージ順の昇順にソートします
	 * @param unitViewBeanList
	 */
	private static void sortUnitBean( List<IReportBuildInfo> reportEntityList ) {

		Collections.sort( reportEntityList,
				new Comparator<IReportBuildInfo>() {
			public int compare( IReportBuildInfo obj1, IReportBuildInfo obj2 ) {
				return obj1.getMergeOrder().compareTo( obj2.getMergeOrder() );
			}
		} );
	}

	/**
	 * 申請情報エンティティをレポート用に変換する
	 * @param entities
	 * @return
	 */
	public static List<IReportAssetApplyInfo> convertReportAssetApplyEntity(
			IAreqEntity[] entities, Map<String, IAssetCounterInfo> applyNoCounterMap ) {

		List<IReportAssetApplyInfo> reportList = new ArrayList<IReportAssetApplyInfo>();

		for ( IAreqEntity entity : entities ) {

			IReportAssetApplyInfo reportEntity = new ReportAssetApplyInfo();

			TriPropertyUtils.copyProperties( reportEntity, entity );

			IAssetCounterInfo counter = applyNoCounterMap.get( reportEntity.getAreqId() );

			if ( null != counter ) {

				// FlowRelUnitHistoryDetailViewService#setApplyInfoViewBeanAssetCount にも
				// 同様のロジックがある
				if ( DcmBusinessJudgUtils.isReturnApply( entity )) {

					if ( null != counter.getLendCount() ) {
						reportEntity.setLendAssetCount		( counter.getLendCount().toString() );
					}
					if ( null != counter.getChangeCount() ) {
						reportEntity.setChangeAssetCount	( counter.getChangeCount().toString() );
					}
					if ( null != counter.getAddCount() ) {
						reportEntity.setAddAssetCount		( counter.getAddCount().toString() );
					}

				} else if ( DcmBusinessJudgUtils.isDeleteApply( entity )) {

					if ( null != counter.getDelCount() ) {
						reportEntity.setDelAssetCount		( counter.getDelCount().toString() );
					}
				}
			}

			reportList.add( reportEntity );
		}

		return reportList;
	}


}
