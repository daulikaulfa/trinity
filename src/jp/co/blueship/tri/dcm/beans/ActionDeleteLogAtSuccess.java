package jp.co.blueship.tri.dcm.beans;

import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates.*;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * リリース・作成レポートに関連する業務排他を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionDeleteLogAtSuccess extends ActionPojoAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		List<Object> paramList = serviceDto.getParamList();

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList ) ;
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		try {
			deleteLog(paramList,logBusinessFlow);
		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}

	/**
	 * RmDesignEntryKeyByLogsとBmDesignEntryKeyByLogsを判断し、deleteLogを実行します。
	 * @param paramList
	 * @param logBusinessFlow
	 */
	private void deleteLog( List<Object> paramList ,LogBusinessFlow logBusinessFlow) {

		RmDesignEntryKeyByLogs logsDefineId =
				(RmDesignEntryKeyByLogs)FluentList.from(paramList).atFirstOf(isInstanceOf(RmDesignEntryKeyByLogs.class));
		if( null != logsDefineId ) {
			logBusinessFlow.deleteLog( logsDefineId );
			return;
		}

		BmDesignEntryKeyByLogs logsBmDefineId =
				(BmDesignEntryKeyByLogs)FluentList.from(paramList).atFirstOf(isInstanceOf(BmDesignEntryKeyByLogs.class));
		if (null != logsBmDefineId ) {
			logBusinessFlow.deleteLog( logsBmDefineId );
			return;
		}
		throw new TriSystemException(DcmMessageId.DCM005006S);
	}

}
