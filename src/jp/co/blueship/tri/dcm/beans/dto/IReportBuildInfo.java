package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;

/**
 * レポート用のビルドエンティティのインタフェースです。
 *
 */

public  interface IReportBuildInfo extends IBpEntity, IReportInfo {

	public String getMergeOrder();
	public void setMergeOrder( String mergeOrder );

	public IAreqEntity[] getBuildApply();
	public void setBuildApply(IAreqEntity[] entity);

	/**
	 * 貸出資産総数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLendAssetSumCount();
	/**
	 * 貸出資産総数を設定します。
	 * @param lendAssetSumCount 貸出資産総数
	 */
	public void setLendAssetSumCount( String lendAssetSumCount );

	/**
	 * 追加資産総数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getAddAssetSumCount();
	/**
	 * 追加資産総数を設定します。
	 * @param addAssetSumCount 追加資産総数
	 */
	public void setAddAssetSumCount( String addAssetSumCount );

	/**
	 * 変更資産総数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getChangeAssetSumCount();
	/**
	 * 変更資産総数を設定します。
	 * @param changeAssetSumCount 変更資産総数
	 */
	public void setChangeAssetSumCount( String changeAssetSumCount );

	/**
	 * 削除資産総数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getDelAssetSumCount();
	/**
	 * 削除資産総数を設定します。
	 * @param delAssetSumCount 削除資産総数
	 */
	public void setDelAssetSumCount( String delAssetSumCount );

	/**
	 * バイナリ削除資産総数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getDelBinaryAssetSumCount();
	/**
	 * バイナリ削除資産総数を設定します。
	 * @param delAssetSumCount バイナリ削除資産総数
	 */
	public void setDelBinaryAssetSumCount( String delBinaryAssetSumCount );
}
