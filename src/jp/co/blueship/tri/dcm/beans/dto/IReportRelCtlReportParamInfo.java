package jp.co.blueship.tri.dcm.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;


/**
 * アプリケーション層で使用する、リリース／資産管理台帳の情報を格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IReportRelCtlReportParamInfo extends ICommonParamInfo, Serializable {

	

	/**
	 * 実行ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserName();
	/**
	 * 実行ユーザを設定します。
	 * @param value 実行ユーザ
	 */
	public void setUserName( String value );

	/**
	 * 実行ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * 実行ユーザＩＤを設定します。
	 * @param value 実行ユーザＩＤ
	 */
	public void setUserId( String value );

	/**
	 * レポート番号を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getRepId();
	/**
	 * レポート番号を設定します。
	 *
	 * @param repId レポート番号
	 */
	public void setRepId(String repId);
	/**
	 * レポート分類コードを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getRepCtgCd();
	/**
	 * レポート分類コードを設定します。
	 *
	 * @param repCtgCd レポート分類コード
	 */
	public void setRepCtgCd(String repCtgCd);
	/**
	 * 抽出対象のロット番号を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getLotNo() ;
	/**
	 * 抽出対象のロット番号を設定します。
	 *
	 * @param lotId ロット番号
	 */
	public void setLotNo(String lotId);
	/**
	 * 抽出対象のリリース番号を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String[] getRelNo();
	/**
	 * 抽出対象のリリースを設定します。
	 *
	 * @param pjtNo ビルドパッケージ
	 */
	public void setRelNo(String[] relNo);
	/**
	 * 抽出対象のリリース環境を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getRelEnvNo();
	/**
	 * 抽出対象のリリース環境を設定します。
	 *
	 * @param pjtNo リリース環境
	 */
	public void setRelEnvNo(String relEnvNo);

	/**
	 * 帳票出力が正常に終了したかどうかを判定します。
	 *
	 * @return 正常に終了した場合、true。それ以外はfalseを戻します。
	 */
	public boolean isSuccess();
	/**
	 * 帳票出力が正常に終了したかどうかをを設定します。
	 *
	 * @param isSuccess 帳票作成の成否判定
	 */
	public void setSuccess( boolean isSuccess );

}
