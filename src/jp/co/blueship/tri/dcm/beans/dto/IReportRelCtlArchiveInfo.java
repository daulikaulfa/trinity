package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用のアーカイブエントリ（リリース用）エンティティのインタフェースです。
 *
 */
public interface IReportRelCtlArchiveInfo extends IReportInfo {
	
	
	
	/**
	 * リリース番号を取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public String getRelNo();
	/**
	 * リリース番号を設定します
	 * 
	 * @param buildNo リリース番号
	 */
	public void setRelNo( String relNo );
	
	/**
	 * アーカイブファイル格納トップパスを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public String getArchiveTopPath();
	/**
	 * アーカイブファイル格納トップパスを設定します
	 * 
	 * @param archiveTopPath アーカイブファイル格納トップパス
	 */
	public void setArchiveTopPath( String archiveTopPath );
	
	/**
	 * アーカイブファイルグループを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public IReportArchiveEntryGroupInfo[] getArchiveEntryGroup();
	/**
	 * アーカイブファイルグループを設定します
	 * 
	 * @param archiveFile アーカイブファイル
	 */
	public void setArchiveEntryGroup( IReportArchiveEntryGroupInfo[] archiveEntryGroup );
	
	/**
	 * アーカイブファイルを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public IReportArchiveEntryInfo[] getArchiveEntry();
	/**
	 * アーカイブファイルを設定します
	 * 
	 * @param archiveFile アーカイブファイル
	 */
	public void setArchiveEntry( IReportArchiveEntryInfo[] archiveEntry );
}
