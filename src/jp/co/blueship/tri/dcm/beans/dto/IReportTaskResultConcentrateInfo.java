package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;

public interface IReportTaskResultConcentrateInfo extends ITaskResultConcentrateEntity, IReportInfo {
	
	
	
	/**
	 * 返却資産と作業フォルダとの資産の比較結果を取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public IReportTaskDiffWrapperInfo[] getDiffWrapper();
	/**
	 * 返却資産と作業フォルダとの資産の比較結果を設定します。
	 * 
	 * @param diff 比較結果
	 */
	public void setDiffWrapper( IReportTaskDiffWrapperInfo[] diff );

	/**
	 * 結果情報の新しいインスタンスを生成します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public IReportTaskDiffWrapperInfo newDiffWrapper();
	
	/**
	 * 
	 * 返却資産と作業フォルダとの資産の比較結果情報のインナーインターフェイス
	 *
	 */
	public interface IReportTaskDiffWrapperInfo extends IReportInfo {

		/**
		 * 
		 * 差分比較した変更管理番号を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public String getPjtNo();
		/**
		 * 差分比較した変更管理番号を設定します。
		 * 
		 * @param value 変更管理番号
		 */
		public void setPjtNo( String value );
		
		/**
		 * 
		 * 差分比較した変更要因番号を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public String getChangeCauseNo();
		/**
		 * 差分比較した変更要因番号を設定します。
		 * 
		 * @param value 変更要因番号
		 */
		public void setChangeCauseNo( String value );
		
		/**
		 * 返却資産と作業フォルダとの資産の比較結果を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public IReportTaskDiffInfo[] getDiff();
		/**
		 * 返却資産と作業フォルダとの資産の比較結果を設定します。
		 * 
		 * @param diff 比較結果
		 */
		public void setDiff( IReportTaskDiffInfo[] diff );

		/**
		 * 結果情報の新しいインスタンスを生成します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public IReportTaskDiffInfo newDiff();
		
		
		/**
		 * 
		 * 返却資産と作業フォルダとの資産の比較結果情報のインナーインターフェイス
		 *
		 */
		public interface IReportTaskDiffInfo extends ITaskDiffEntity, IReportInfo {
			
			/**
			 * 差分比較結果を取得します。
			 * 
			 * @return 取得した情報を戻します。
			 */
			public IReportTaskResultInfo[] getResult();
			/**
			 * 差分比較結果を設定します。
			 * 
			 * @param result 差分比較結果
			 */
			public void setResult( IReportTaskResultInfo[] result );

			/**
			 * 結果情報の新しいインスタンスを生成します。
			 * 
			 * @return 取得した情報を戻します。
			 */
			public IReportTaskResultInfo newResult();
			
		}
		
		/**
		 * 
		 * 実行結果エンティティのインナーインターフェイス
		 *
		 */
		public interface IReportTaskResultInfo extends ITaskResultEntity, IReportInfo {
			
			public String getLendRevision();
			public void setLendRevision( String lendRevision );
			
			public String getCloseRevision();
			public void setCloseRevision( String closeRevision );
			
			public String getMergeRevision();
			public void setMergeRevision( String mergeRevision );
			
			public String getByteSize();
			public void setByteSize( String byteSize );
			
			public String getInsertUpdateDate();
			public void setInsertUpdateDate( String insertUpdateDate );
		}
	}
}
