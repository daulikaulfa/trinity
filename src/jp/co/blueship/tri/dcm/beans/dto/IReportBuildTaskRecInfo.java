package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;

/**
 * レポート用のビルドタスク記録エンティティのインタフェースです。
 *
 */
public  interface IReportBuildTaskRecInfo extends IReportInfo {

	/**
	 * ビルド番号を取得します。
	 *
	 * @return 取得した情報を戻します
	 */
	public String getBpId();
	/**
	 * ビルド番号を設定します
	 *
	 * @param value ビルド番号
	 */
	public void setBpId( String value );
	/**
	 *
	 * @return
	 */
	public ITaskEntity getTaskEntity() ;
	/**
	 *
	 * @param entity
	 */
	public void setTaskEntity( ITaskEntity entity ) ;

	/**
	 * タスクの新規インスタンスを生成します。
	 *
	 * @return 生成した情報を戻します
	 */
	public ITaskEntity newTaskEntity();
}
