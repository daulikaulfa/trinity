package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpEntity;

public class ReportRelInfo extends RpEntity implements IReportRelInfo {

	private static final long serialVersionUID = 3L;

	private String identifierId	= null;

	private IBpEntity[] build;
	public IBpEntity[] getBuild(){
		return this.build;
	}
	public void setBuild(IBpEntity[] entity){
		this.build = entity;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getRpId();
	}
}
