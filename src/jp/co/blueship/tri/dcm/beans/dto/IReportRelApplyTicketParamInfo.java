package jp.co.blueship.tri.dcm.beans.dto;

import java.io.Serializable;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

public interface IReportRelApplyTicketParamInfo extends ICommonParamInfo, Serializable {
	
	

	/**
	 * 実行ユーザを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserName();
	/**
	 * 実行ユーザを設定します。
	 * @param value 実行ユーザ
	 */
	public void setUserName( String value );
	
	/**
	 * 実行ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * 実行ユーザＩＤを設定します。
	 * @param value 実行ユーザＩＤ
	 */
	public void setUserId( String value );
	
	/**
	 * レポート番号を取得します。
	 * 
	 * @return 取得した値を戻します。
	 */
	public String getReportNo();
	/**
	 * レポート番号を設定します。
	 * 
	 * @param reportNo レポート番号
	 */
	public void setReportNo(String reportNo);
	/**
	 * 帳票区分を取得します。
	 * 
	 * @return 取得した値を戻します。
	 */
	public String getReportId();
	/**
	 * 帳票区分を設定します。
	 * 
	 * @param reportId 帳票区分
	 */
	public void setReportId(String reportId);
	/**
	 * 抽出対象のロット番号を取得します。
	 * 
	 * @return 取得した値を戻します。
	 */
	public String getLotNo() ;
	/**
	 * 抽出対象のロット番号を設定します。
	 * 
	 * @param lotId ロット番号
	 */
	public void setLotNo(String lotId);
	/**
	 * 抽出対象のリリース申請番号を取得します。
	 * 
	 * @return 取得した値を戻します。
	 */
	public String[] getRelApplyNo();
	/**
	 * 抽出対象のリリース申請番号を設定します。
	 * 
	 * @param relApplyNo リリース申請番号
	 */
	public void setRelApplyNo(String[] relApplyNo);
	

}
