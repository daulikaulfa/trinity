package jp.co.blueship.tri.dcm.beans.dto;

public class ReportRelCtlCreateLogInfo implements IReportRelCtlCreateLogInfo {

	

	private static final long serialVersionUID = 1L;
	
	private String relNo				= null;
	private IReportStringInfo[] log	= null;
	private String identifierId		= null;
	
	
	public String getRelNo() {
		return this.relNo;
	}
	public void setRelNo( String relNo ) {
		this.relNo = relNo;
	}		
	
	public IReportStringInfo[] getLog() {
		return this.log;
	}
	public void setLog( IReportStringInfo[] log ) {
		this.log = log;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		
		StringBuilder bf = new StringBuilder();
		bf.append( this.getRelNo() );
		for ( IReportStringInfo log : this.getLog() ) {
			bf.append( log.getValue() );
		}
		
		return bf.toString();
	}
}
