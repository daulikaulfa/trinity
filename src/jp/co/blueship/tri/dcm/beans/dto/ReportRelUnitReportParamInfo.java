package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class ReportRelUnitReportParamInfo extends CommonParamInfo implements IReportRelUnitReportParamInfo {
	
	private static final long serialVersionUID = 1L;

	

	private String userName = null;
	private String reportNo = null;
	private String reportId = null;
	private String lotId = null;
	private String[] buildNo = null;
	private boolean isSuccess = false;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String[] getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String[] buildNo) {
		this.buildNo = buildNo;
	}
	public String getRepCtgCd() {
		return reportId;
	}
	public void setRepCtgCd(String reportId) {
		this.reportId = reportId;
	}
	public String getRepId() {
		return reportNo;
	}
	public void setRepId(String reportNo) {
		this.reportNo = reportNo;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
}
