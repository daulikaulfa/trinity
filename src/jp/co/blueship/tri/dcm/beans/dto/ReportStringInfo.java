package jp.co.blueship.tri.dcm.beans.dto;

public class ReportStringInfo implements IReportStringInfo {

	
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String value			= null;
	private String identifierId	= null;
	
	
	public String getValue() {
		return this.value;
	}
	public void setValue( String value ) {
		this.value = value;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		return this.getValue();
	}
}
