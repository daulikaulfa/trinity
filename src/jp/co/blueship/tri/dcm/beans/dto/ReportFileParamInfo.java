package jp.co.blueship.tri.dcm.beans.dto;

import java.io.File;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class ReportFileParamInfo extends CommonParamInfo implements IReportFileParamInfo {

	
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private File reportFile		= null;
	private byte[] contentsArray	= null;
	
	
	public byte[] getContentsArray() {
		return contentsArray;
	}
	public File getReportFile() {
		return reportFile;
	}
	public void setContentsArray( byte[] contentsArray ) {
		this.contentsArray = contentsArray;
		
	}
	public void setReportFile( File reportFile ) {
		this.reportFile = reportFile;
		
	}
}
