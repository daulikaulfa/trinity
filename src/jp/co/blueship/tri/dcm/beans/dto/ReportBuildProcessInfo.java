package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;

public class ReportBuildProcessInfo extends TaskFlowProcEntity implements IReportBuildProcessInfo {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String processName		= null;
	private String bldSrvNm			= null;
	private String identifierId		= null;

	public String getBpId() {
		return this.bpId;
	}
	public void setBpId( String bpId ) {
		this.bpId = bpId;
	}

	public String getProcessName() {
		return this.processName;
	}
	public void setProcessName( String processName ) {
		this.processName = processName;
	}

	public String getBldSrvNm() {
		return this.bldSrvNm;
	}
	public void setBldSrvNm( String bldSrvNm ) {
		this.bldSrvNm = bldSrvNm;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getBpId() + this.getBldLineNo() + this.getTaskFlowId() + this.getTargetSeqNo();
	}
}
