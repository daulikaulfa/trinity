package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class ReportRelCtlReportParamInfo extends CommonParamInfo implements IReportRelCtlReportParamInfo {
	
	private static final long serialVersionUID = 1L;
	
	

	private String userName = null;
	private String reportNo = null;
	private String reportId = null;
	private String lotId = null;
	private String relEnvNo = null;
	private String[] relNo = null;
	private boolean isSuccess = false;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String[] getRelNo() {
		return relNo;
	}
	public void setRelNo(String[] relNo) {
		this.relNo = relNo;
	}
	public String getRepCtgCd() {
		return reportId;
	}
	public void setRepCtgCd(String reportId) {
		this.reportId = reportId;
	}
	public String getRelEnvNo() {
		return relEnvNo;
	}
	public void setRelEnvNo(String relEnvNo) {
		this.relEnvNo = relEnvNo;
	}
	public String getRepId() {
		return reportNo;
	}
	public void setRepId(String reportNo) {
		this.reportNo = reportNo;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	
}
