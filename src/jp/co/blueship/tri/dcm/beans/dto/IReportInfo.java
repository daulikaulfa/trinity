package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * レポート用エンティティのインタフェースです。
 *
 */
public interface IReportInfo extends IEntity {
	
	
	
	/**
	 * 識別ＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getIdentifierId();
	/**
	 * 識別ＩＤを設定します。
	 * @param identifierId 識別ＩＤ
	 */	
	public void setIdentifierId( String identifierId );
	
	/**
	 * キー値を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getKey();	
}
