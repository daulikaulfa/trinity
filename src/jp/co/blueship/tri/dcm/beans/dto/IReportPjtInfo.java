package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;

/**
 * レポート用の変更管理エンティティのインタフェースです。
 *
 */
public  interface IReportPjtInfo extends IPjtEntity, IReportInfo {

}
