package jp.co.blueship.tri.dcm.beans.dto;

/**
 * レポート用のビルドプロセス（集約詳細）エンティティのインタフェースです。
 *
 */
public interface IReportBuildProcessConcentrateDetailInfo extends IReportBuildProcessInfo {
	
	
	
	/**
	 * 基準パスを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public String getBasePath();
	/**
	 * 基準パスを設定します
	 * 
	 * @param basePath 基準パス
	 */
	public void setBasePath( String basePath );
}
