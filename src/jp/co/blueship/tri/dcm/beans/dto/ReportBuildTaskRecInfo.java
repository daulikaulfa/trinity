package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskEntity;

public class ReportBuildTaskRecInfo implements IReportBuildTaskRecInfo {

	private static final long serialVersionUID = 1L;

	private String identifierId	= null;

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getBpId();
	}

	private String bpId = null ;

	private ITaskEntity taskEntity = null ;

	/**
	 * ビルド番号を取得します。
	 *
	 * @return 取得した情報を戻します
	 */
	public String getBpId() {
		return bpId;
	}
	/**
	 * ビルド番号を設定します
	 *
	 * @param value ビルド番号
	 */
	public void setBpId(String bpId) {
		this.bpId = bpId;
	}

	public ITaskEntity getTaskEntity() {
		return taskEntity;
	}
	public void setTaskEntity(ITaskEntity taskEntity) {
		this.taskEntity = taskEntity;
	}
	public ITaskEntity newTaskEntity() {
		return new TaskEntity();
	}
}
