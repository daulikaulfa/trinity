package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * レポート用のビルドエンティティのインタフェースです。
 *
 */
public  interface IReportRelInfo extends IRpEntity, IReportInfo {

	public IBpEntity[] getBuild();
	public void setBuild(IBpEntity[] entity);

}
