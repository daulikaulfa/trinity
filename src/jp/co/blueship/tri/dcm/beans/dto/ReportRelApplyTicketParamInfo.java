package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.CommonParamInfo;

public class ReportRelApplyTicketParamInfo extends CommonParamInfo implements IReportRelApplyTicketParamInfo {
	
	private static final long serialVersionUID = 1L;

	

	private String userName = null;
	private String reportNo = null;
	private String reportId = null;
	private String lotId = null;
	private String[] relApplyNo = null;
	
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String[] getRelApplyNo() {
		return relApplyNo;
	}
	public void setRelApplyNo(String[] relApplyNo) {
		this.relApplyNo = relApplyNo;
	}
	public String getReportId() {
		return reportId;
	}
	public void setReportId(String reportId) {
		this.reportId = reportId;
	}
	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo(String reportNo) {
		this.reportNo = reportNo;
	}
}
