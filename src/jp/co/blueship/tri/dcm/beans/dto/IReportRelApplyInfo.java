package jp.co.blueship.tri.dcm.beans.dto;

import java.util.List;

import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;

public interface IReportRelApplyInfo extends IRaEntity, IReportInfo {

    public String getRelWishDateReportForm();
    public void setRelWishDateReportForm( String relWishDateReportForm );

    public List<String> getResponsibleUserReportForm();
    public void setResponsibleUserReportForm( List<String> responsibleUserReportForm );

    public List<String> getAppendFileReportForm();
    public void setAppendFileReportForm( List<String> appendFileReportForm );

    public List<String> getApplyNoForm();
    public void setApplyNoForm( List<String> applyNoForm );

    public List<String> getPjtNoReportForm();
    public void setPjtNoReportForm( List<String> pjtNoReportForm );

    public List<String> getChangeCauseNoReportForm();
    public void setChangeCauseNoReportForm( List<String> changeCauseNoReportForm );

}
