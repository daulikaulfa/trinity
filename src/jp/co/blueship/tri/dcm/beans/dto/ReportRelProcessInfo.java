package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;


public class ReportRelProcessInfo extends TaskFlowProcEntity implements IReportRelProcessInfo {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String processName		= null;
	private String serverName		= null;
	private String identifierId	= null;


	public String getProcessName() {
		return this.processName;
	}
	public void setProcessName( String processName ) {
		this.processName = processName;
	}

	public String getServerName() {
		return this.serverName;
	}
	public void setServerName( String serverName ) {
		this.serverName = serverName;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getRpId() + this.getBldLineNo() + this.getTaskFlowId() + this.getTargetSeqNo();
	}
}
