package jp.co.blueship.tri.dcm.beans.dto;

public class ReportRelCtlArchiveInfo implements IReportRelCtlArchiveInfo {

	
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String relNo			= null;
	private String archiveTopPath	= null;
	private IReportArchiveEntryGroupInfo[] archiveEntryGroup = null;
	private IReportArchiveEntryInfo[] archiveEntry = null;
	private String identifierId	= null;
	
	
	public String getRelNo() {
		return this.relNo;
	}
	public void setRelNo( String relNo ) {
		this.relNo = relNo;
	}
	
	public String getArchiveTopPath() {
		return this.archiveTopPath;
	}
	public void setArchiveTopPath( String archiveTopPath ) {
		this.archiveTopPath = archiveTopPath;
	}
	
	public IReportArchiveEntryGroupInfo[] getArchiveEntryGroup() {
		return this.archiveEntryGroup;
	}
	public void setArchiveEntryGroup( IReportArchiveEntryGroupInfo[] archiveEntryGroup ) {
		this.archiveEntryGroup = archiveEntryGroup;
	}
	
	public IReportArchiveEntryInfo[] getArchiveEntry() {
		return this.archiveEntry;
	}
	public void setArchiveEntry( IReportArchiveEntryInfo[] archiveEntry ) {
		this.archiveEntry = archiveEntry;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		
		StringBuilder bf = new StringBuilder();
		bf.append( this.getRelNo() );
		for ( IReportArchiveEntryGroupInfo entry : this.getArchiveEntryGroup() ) {
			bf.append( entry.getName() );
		}
		for ( IReportArchiveEntryInfo entry : this.getArchiveEntry() ) {
			bf.append( entry.getName() );
		}
		
		return bf.toString();
	}
}
