package jp.co.blueship.tri.dcm.beans.dto;

public class ReportRelUnitCreateLogInfo implements IReportRelUnitCreateLogInfo {

	

	private static final long serialVersionUID = 1L;
	
	private String buildNo				= null;
	private IReportStringInfo[] log	= null;
	private String identifierId		= null;
	
	
	public String getBuildNo() {
		return this.buildNo;
	}
	public void setBuildNo( String buildNo ) {
		this.buildNo = buildNo;
	}		
	
	public IReportStringInfo[] getLog() {
		return this.log;
	}
	public void setLog( IReportStringInfo[] log ) {
		this.log = log;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		
		StringBuilder bf = new StringBuilder();
		bf.append( this.getBuildNo() );
		for ( IReportStringInfo log : this.getLog() ) {
			bf.append( log.getValue() );
		}
		
		return bf.toString();
	}
}
