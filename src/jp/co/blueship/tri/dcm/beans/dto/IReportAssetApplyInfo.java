package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;

/**
 * レポート用の申請情報エンティティのインタフェースです。
 *
 */
public interface IReportAssetApplyInfo extends IAreqEntity, IReportInfo {

	

	/**
	 * 貸出資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLendAssetCount();
	/**
	 * 貸出資産数を設定します。
	 * @param lendAssetCount 貸出資産数
	 */
	public void setLendAssetCount( String lendAssetCount );

	/**
	 * 追加資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getAddAssetCount();
	/**
	 * 追加資産数を設定します。
	 * @param addAssetCount 追加資産数
	 */
	public void setAddAssetCount( String addAssetCount );

	/**
	 * 変更資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getChangeAssetCount();
	/**
	 * 変更資産数を設定します。
	 * @param changeAssetCount 変更資産数
	 */
	public void setChangeAssetCount( String changeAssetCount );

	/**
	 * 削除資産数を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getDelAssetCount();
	/**
	 * 削除資産数を設定します。
	 * @param delCAssetount 削除資産数
	 */
	public void setDelAssetCount( String delAssetCount );
}
