package jp.co.blueship.tri.dcm.beans.dto;

import java.util.List;


/**
 * レポート用のアーカイブエントリグループエンティティのインタフェースです。
 *
 */
public  interface IReportArchiveEntryGroupInfo extends IReportInfo {
	
	
		
	public String getName();
	public void setName( String name );
	
	public List<IReportArchiveEntryInfo> getArchiveEntryList();
	public void setArchiveEntryList( List<IReportArchiveEntryInfo> achiveEntryList );
	public void addArchiveEntry( IReportArchiveEntryInfo achiveEntry );
}
