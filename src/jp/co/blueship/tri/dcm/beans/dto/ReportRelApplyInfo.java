package jp.co.blueship.tri.dcm.beans.dto;

import java.util.List;

import jp.co.blueship.tri.rm.dao.ra.eb.RaEntity;

public class ReportRelApplyInfo extends RaEntity implements IReportRelApplyInfo {

	private static final long serialVersionUID = 1L;

	private String relWishDateReportForm = null;
	private String identifierId = null;

	private List<String> responsibleUserReportForm = null;
	private List<String> appendFileReportForm = null;
	private List<String> applyNoForm = null;
	private List<String> pjtNoReportForm = null;
	private List<String> changeCauseNoReportForm = null;

	public String getRelWishDateReportForm() {
		return this.relWishDateReportForm;
	}

	public void setRelWishDateReportForm(String relWishDateReportForm) {
		this.relWishDateReportForm = relWishDateReportForm;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}

	public void setIdentifierId(String identifierId) {
		this.identifierId = identifierId;
	}

	public List<String> getResponsibleUserReportForm() {
		return this.responsibleUserReportForm;
	}

	public void setResponsibleUserReportForm(
			List<String> responsibleUserReportForm) {
		this.responsibleUserReportForm = responsibleUserReportForm;
	}

	public List<String> getAppendFileReportForm() {
		return this.appendFileReportForm;
	}

	public void setAppendFileReportForm(List<String> appendFileReportForm) {
		this.appendFileReportForm = appendFileReportForm;
	}

	public List<String> getApplyNoForm() {
		return this.applyNoForm;
	}

	public void setApplyNoForm(List<String> applyNoForm) {
		this.applyNoForm = applyNoForm;
	}

	public List<String> getPjtNoReportForm() {
		return this.pjtNoReportForm;
	}

	public void setPjtNoReportForm(List<String> pjtNoReportForm) {
		this.pjtNoReportForm = pjtNoReportForm;
	}

	public List<String> getChangeCauseNoReportForm() {
		return this.changeCauseNoReportForm;
	}

	public void setChangeCauseNoReportForm(List<String> changeCauseNoReportForm) {
		this.changeCauseNoReportForm = changeCauseNoReportForm;
	}

	public String getKey() {
		return this.getRaId();
	}
}
