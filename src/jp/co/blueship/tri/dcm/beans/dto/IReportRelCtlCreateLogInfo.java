package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用の作成ログ（リリース用）エンティティのインタフェースです。
 *
 */
public  interface IReportRelCtlCreateLogInfo extends IReportInfo {
	
	
		
	public String getRelNo();
	public void setRelNo( String relNo );
	
	public IReportStringInfo[] getLog();
	public void setLog( IReportStringInfo[] log );
}
