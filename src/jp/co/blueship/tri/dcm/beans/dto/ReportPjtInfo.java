package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.pjt.eb.PjtEntity;

public class ReportPjtInfo extends PjtEntity implements IReportPjtInfo {

	private static final long serialVersionUID = 2L;

	private String identifierId	= null;

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getPjtId();
	}
}
