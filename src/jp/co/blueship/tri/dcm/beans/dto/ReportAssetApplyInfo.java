package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;

public class ReportAssetApplyInfo extends AreqEntity implements IReportAssetApplyInfo {

	

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String lendAssetCount		= null;
	private String addAssetCount		= null;
	private String changeAssetCount	= null;
	private String delAssetCount		= null;
	private String identifierId		= null;


	public String getLendAssetCount() {
		return lendAssetCount;
	}
	public void setLendAssetCount( String lendAssetCount ) {
		this.lendAssetCount = lendAssetCount;
	}

	public String getAddAssetCount() {
		return addAssetCount;
	}
	public void setAddAssetCount( String addAssetCount ) {
		this.addAssetCount = addAssetCount;
	}

	public String getChangeAssetCount() {
		return changeAssetCount;
	}
	public void setChangeAssetCount( String changeAssetCount ) {
		this.changeAssetCount = changeAssetCount;
	}

	public String getDelAssetCount() {
		return delAssetCount;
	}
	public void setDelAssetCount( String delAssetCount ) {
		this.delAssetCount = delAssetCount;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getKey() {
		return this.getAreqId();
	}
}
