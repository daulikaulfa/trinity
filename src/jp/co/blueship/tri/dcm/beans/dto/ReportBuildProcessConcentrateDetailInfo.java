package jp.co.blueship.tri.dcm.beans.dto;

public class ReportBuildProcessConcentrateDetailInfo extends ReportBuildProcessInfo implements IReportBuildProcessConcentrateDetailInfo {

	
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String basePath = null;
	
	public String getBasePath() {
		return basePath;
	}
	public void setBasePath( String basePath ) {
		this.basePath = basePath;
	}
}
