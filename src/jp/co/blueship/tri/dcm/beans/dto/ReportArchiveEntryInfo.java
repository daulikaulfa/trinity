package jp.co.blueship.tri.dcm.beans.dto;

public class ReportArchiveEntryInfo implements IReportArchiveEntryInfo {

	
		
	private static final long serialVersionUID = 1L;
	
	private String name			= null;
	private String size			= null;
	private String time			= null;
	private String groupName		= null;
	private String userId			= null;
	private String userName		= null;
	private String mode			= null;
	private IReportArchiveEntryGroupInfo[] archiveEntryGroup = null;
	private String identifierId	= null;
	
	public String getName() {
		return this.name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	public String getSize() {
		return this.size;
	}
	public void setSize( String size ) {
		this.size = size;
	}
	
	public String getTime() {
		return this.time;
	}
	public void setTime( String time ) {
		this.time = time;
	}

	public String getGroupName() {
		return this.groupName;
	}
	public void setGroupName( String groupName ) {
		this.groupName = groupName;
	}
	
	public String getUserName() {
		return this.userName;
	}
	public void setUserName( String userName ) {
		this.userName = userName;
	}
	
	public String getUserId() {
		return this.userId;
	}
	public void setUserId( String userId ) {
		this.userId = userId;
	}
	
	public String getMode() {
		return this.mode;
	}
	public void setMode( String mode ) {
		this.mode = mode;
	}
	
	public IReportArchiveEntryGroupInfo[] getArchiveEntryGroup() {
		return this.archiveEntryGroup;
	}
	public void setArchiveEntryGroup( IReportArchiveEntryGroupInfo[] archiveEntryGroup ) {
		this.archiveEntryGroup = archiveEntryGroup;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public int compareTo( IReportArchiveEntryInfo dst ) {
		return this.getKey().compareTo( dst.getKey() );
	}
	
	public String getKey() {
		return this.getName();
	}
}
