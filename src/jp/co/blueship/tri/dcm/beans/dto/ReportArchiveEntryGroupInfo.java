package jp.co.blueship.tri.dcm.beans.dto;

import java.util.ArrayList;
import java.util.List;

public class ReportArchiveEntryGroupInfo implements IReportArchiveEntryGroupInfo {

	
		
	private static final long serialVersionUID = 1L;
	
	private String name			= null;
	private List<IReportArchiveEntryInfo> archiveEntryList	= new ArrayList<IReportArchiveEntryInfo>();
	private String identifierId	= null;
	
	public String getName() {
		return this.name;
	}
	public void setName( String name ) {
		this.name = name;
	}
	
	public List<IReportArchiveEntryInfo> getArchiveEntryList() {
		return this.archiveEntryList;
	}
	public void setArchiveEntryList( List<IReportArchiveEntryInfo> archiveEntryList ) {
		this.archiveEntryList = archiveEntryList;
	}
	public void addArchiveEntry( IReportArchiveEntryInfo achiveEntry ) {
		this.archiveEntryList.add( achiveEntry );
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		return this.getName();
	}
}
