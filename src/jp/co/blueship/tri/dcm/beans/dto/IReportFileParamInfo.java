package jp.co.blueship.tri.dcm.beans.dto;

import java.io.File;
import java.io.Serializable;

import jp.co.blueship.tri.fw.domain.dto.ICommonParamInfo;

/**
 * アプリケーション層で、共通に使用するレポートファイル情報を格納するインタフェースです。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 */
public interface IReportFileParamInfo extends ICommonParamInfo, Serializable {
	
	

	
	/**
	 * レポートファイルを取得します。
	 * @return 取得した値を戻します。
	 */
	public File getReportFile();
	/**
	 * レポートファイルを設定します。
	 * @param value レポートファイル
	 */
	public void setReportFile( File reportFile );
	
	/**
	 * レポートファイルの内容を取得します。
	 * @return 取得した値を戻します。
	 */
	public byte[] getContentsArray();
	/**
	 * レポートファイルの内容を設定します。
	 * @param value レポートファイルの内容
	 */
	public void setContentsArray( byte[] contentsArray );
}