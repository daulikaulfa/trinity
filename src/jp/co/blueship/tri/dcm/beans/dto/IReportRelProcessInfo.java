package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;

/**
 * レポート用のリリースプロセスエンティティのインタフェースです。
 *
 */

public interface IReportRelProcessInfo extends ITaskFlowProcEntity, IReportInfo {


	/**
	 * 処理名を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getProcessName();
	/**
	 * 処理名を設定します。
	 * @param processName 処理名
	 */
	public void setProcessName( String processName );

	/**
	 * サーバ名を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getServerName();
	/**
	 * サーバ名を設定します。
	 * @param serverName サーバ名
	 */
	public void setServerName( String serverName );
}
