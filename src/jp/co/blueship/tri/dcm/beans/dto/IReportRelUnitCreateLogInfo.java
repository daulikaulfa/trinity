package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用の作成ログ（ビルド用）エンティティのインタフェースです。
 *
 */
public  interface IReportRelUnitCreateLogInfo extends IReportInfo {
	
	
		
	public String getBuildNo();
	public void setBuildNo( String buildNo );
	
	public IReportStringInfo[] getLog();
	public void setLog( IReportStringInfo[] log );
}
