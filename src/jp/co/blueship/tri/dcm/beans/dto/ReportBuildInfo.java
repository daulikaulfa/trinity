package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;

public class ReportBuildInfo extends BpEntity implements IReportBuildInfo {

	private static final long serialVersionUID = 3L;

	private String identifierId			= null;
	private String mergeOrder				= null;
	private String lendAssetSumCount		= null;
	private String addAssetSumCount		= null;
	private String changeAssetSumCount		= null;
	private String delAssetSumCount		= null;
	private String delBinaryAssetSumCount	= null;

	private IAreqEntity[] buildApply;

	public IAreqEntity[] getBuildApply(){
		return this.buildApply;
	}

	public void setBuildApply(IAreqEntity[] entity){
		this.buildApply = entity;
	}

	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}

	public String getMergeOrder() {
		return this.mergeOrder;
	}
	public void setMergeOrder( String mergeOrder ) {
		this.mergeOrder = mergeOrder;
	}

	public String getKey() {
		return this.getBpId();
	}
	public String getAddAssetSumCount() {
		return addAssetSumCount;
	}
	public void setAddAssetSumCount(String addAssetSumCount) {
		this.addAssetSumCount = addAssetSumCount;
	}
	public String getChangeAssetSumCount() {
		return changeAssetSumCount;
	}
	public void setChangeAssetSumCount(String changeAssetSumCount) {
		this.changeAssetSumCount = changeAssetSumCount;
	}
	public String getDelAssetSumCount() {
		return delAssetSumCount;
	}
	public void setDelAssetSumCount(String delAssetSumCount) {
		this.delAssetSumCount = delAssetSumCount;
	}
	public String getDelBinaryAssetSumCount() {
		return delBinaryAssetSumCount;
	}
	public void setDelBinaryAssetSumCount(String delBinaryAssetSumCount) {
		this.delBinaryAssetSumCount = delBinaryAssetSumCount;
	}
	public String getLendAssetSumCount() {
		return lendAssetSumCount;
	}
	public void setLendAssetSumCount(String lendAssetSumCount) {
		this.lendAssetSumCount = lendAssetSumCount;
	}
}
