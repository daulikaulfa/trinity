package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskResultConcentrateEntity;

public class ReportTaskResultConcentrateInfo extends TaskResultConcentrateEntity implements IReportTaskResultConcentrateInfo {

	
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;
	
	private String identifierId = null;
	private IReportTaskDiffWrapperInfo[] diffWrapper = new IReportTaskDiffWrapperInfo[0];

	public IReportTaskDiffWrapperInfo newDiffWrapper() {
		return new ReportTaskDiffWrapperInfo();
	}
	
	public IReportTaskDiffWrapperInfo[] getDiffWrapper() {
		return this.diffWrapper;
	}
	public void setDiffWrapper( IReportTaskDiffWrapperInfo[] diffWrapper ) {
		this.diffWrapper = diffWrapper;
	}
	
	public String getIdentifierId() {
		return this.identifierId;
	}
	public void setIdentifierId( String identifierId ) {
		this.identifierId = identifierId;
	}
	
	public String getKey() {
		return this.getSequence();
	}

	
	/**
	 * 
	 * インナークラス
	 *
	 */
	public class ReportTaskDiffWrapperInfo implements IReportTaskDiffWrapperInfo {
		
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String identifierId	= null;
		private String pjtNo			= null;
		private String changeCauseNo	= null;
		private IReportTaskDiffInfo[] diff = new IReportTaskDiffInfo[0];

		public IReportTaskDiffInfo newDiff() {
			return new ReportTaskDiffInfo();
		}		
		public IReportTaskDiffInfo[] getDiff() {
			return this.diff;
		}
		public void setDiff( IReportTaskDiffInfo[] diff ) {
			this.diff = diff;
		}
		
		public String getIdentifierId() {
			return this.identifierId;
		}
		public void setIdentifierId( String identifierId ) {
			this.identifierId = identifierId;
		}
		
		public String getPjtNo() {
			return this.pjtNo;
		}
		public void setPjtNo( String value ) {
			this.pjtNo = value;
		}
		
		public String getChangeCauseNo() {
			return this.changeCauseNo;
		}
		public void setChangeCauseNo( String value ) {
			this.changeCauseNo = value;
		}
		
		public String getKey() {
			return this.getPjtNo();
		}
		
		
		public class ReportTaskDiffInfo extends TaskDiffEntity implements IReportTaskDiffInfo {
			
			/**
			 * 修正後、インクリメントすること
			 */
			private static final long serialVersionUID = 1L;

			private String identifierId				= null;
			private IReportTaskResultInfo[] result	= new IReportTaskResultInfo[0];
			
			public String getIdentifierId() {
				return this.identifierId;
			}
			public void setIdentifierId( String identifierId ) {
				this.identifierId = identifierId;
			}
	
			public IReportTaskResultInfo[] getResult() {
				return this.result;
			}
			public void setResult( IReportTaskResultInfo[] result ) {
				this.result = result;
			}

			public IReportTaskResultInfo newResult() {
				return new ReportTaskResultInfo();
			}
			
			public String getKey() {
				return this.getApplyNo();
			}
		}


		public class ReportTaskResultInfo extends TaskResultEntity implements IReportTaskResultInfo {
			
			/**
			 * 修正後、インクリメントすること
			 */
			private static final long serialVersionUID = 1L;
		
			private String byteSize			= null;
			private String insertUpdateDate	= null;
			private String mergeRevision		= null;
			private String lendRevision		= null;
			private String closeRevision		= null;
			private String identifierId		= null;
			
			public String getByteSize() {
				return byteSize;
			}
			public void setByteSize( String byteSize ) {
				this.byteSize = byteSize;
			}
			
			public String getInsertUpdateDate() {
				return insertUpdateDate;
			}
			public void setInsertUpdateDate( String insertUpdateDate ) {
				this.insertUpdateDate = insertUpdateDate;
			}
		
			public String getMergeRevision() {
				return mergeRevision;
			}
			public void setMergeRevision( String mergeRevision ) {
				this.mergeRevision = mergeRevision;
			}
			
			public String getLendRevision() {
				return lendRevision;
			}
			public void setLendRevision( String lendRevision ) {
				this.lendRevision = lendRevision;				
			}
		
			public String getCloseRevision() {
				return closeRevision;
			}
			public void setCloseRevision( String closeRevision ) {
				this.closeRevision = closeRevision;				
			}
			
			public String getIdentifierId() {
				return this.identifierId;
			}
			public void setIdentifierId( String identifierId ) {
				this.identifierId = identifierId;
			}
			
			public String getKey() {
				return this.getSequence();
		
			}
		}
	}
}
