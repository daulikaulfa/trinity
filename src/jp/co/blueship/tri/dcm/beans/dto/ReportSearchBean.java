package jp.co.blueship.tri.dcm.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * レポートの詳細検索を行うための検索条件情報です。
 * 
 * @author blueship
 *
 */
public class ReportSearchBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 
	 * sessionに保持する場合、任意に一意とするキー値 
	 */
	private String sessionKey = null;
	/** 
	 * 詳細検索 ステータス 
	 */
	private List<String> selectStatusViewList = null ;
	/** 
	 * 詳細検索 選択ステータス 
	 */
	private String[] selectedStatusStringArray = null ;
	/** 
	 * 詳細検索 レポート番号 
	 */
	private String searchReportNo = null;
	/** 
	 * 詳細検索 レポート区分 
	 */
	private List<ItemLabelsBean> searchReportIdList = null ;
	/** 
	 * 詳細検索 選択済みレポート区分 
	 */
	private String selectedReportId = null ;
	/** 
	 * 詳細検索 出力依頼者 
	 */
	private String searchExecuteUser = null;
	/** 
	 * 詳細検索 出力依頼開始日 
	 */
	private String searchExecuteStartDate = null;
	/** 
	 * 詳細検索 出力依頼終了日 
	 */
	private String searchExecuteEndDate = null;
	/** 
	 * 詳細検索 検索件数 
	 */
	private List<ItemLabelsBean> searchCountList = null ;
	/** 
	 * 詳細検索 選択済み検索件数 
	 */
	private String selectedCount = null ;
	
	public String getSearchExecuteEndDate() {
		return searchExecuteEndDate;
	}
	public void setSearchExecuteEndDate(String searchExecuteEndDate) {
		this.searchExecuteEndDate = searchExecuteEndDate;
	}
	public String getSearchExecuteUser() {
		return searchExecuteUser;
	}
	public void setSearchExecuteUser(String searchExecuteUser) {
		this.searchExecuteUser = searchExecuteUser;
	}
	public List<ItemLabelsBean> getSearchCountList() {
		return searchCountList;
	}
	public void setSearchCountList(
			List<ItemLabelsBean> searchCountList) {
		this.searchCountList = searchCountList;
	}
	public List<ItemLabelsBean> getSearchReportIdList() {
		return searchReportIdList;
	}
	public void setSearchReportIdList(List<ItemLabelsBean> searchReportIdList) {
		this.searchReportIdList = searchReportIdList;
	}
	public String getSearchReportNo() {
		return searchReportNo;
	}
	public void setSearchReportNo(String searchReportNo) {
		this.searchReportNo = searchReportNo;
	}
	public String getSearchExecuteStartDate() {
		return searchExecuteStartDate;
	}
	public void setSearchExecuteStartDate(String searchExecuteStartDate) {
		this.searchExecuteStartDate = searchExecuteStartDate;
	}
	public String getSelectedCount() {
		return selectedCount;
	}
	public void setSelectedCount(String selectedCount) {
		this.selectedCount = selectedCount;
	}
	public String getSelectedReportId() {
		return selectedReportId;
	}
	public void setSelectedReportId(String selectedReportId) {
		this.selectedReportId = selectedReportId;
	}
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	public List<String> getSelectStatusViewList() {
		return selectStatusViewList;
	}
	public void setSelectStatusViewList(List<String> selectStatusViewList) {
		this.selectStatusViewList = selectStatusViewList;
	}

	public void setSelectedStatusString( String[] statusArray ) {
		this.selectedStatusStringArray = statusArray;
	}
	public String getSelectedStatusString() {

		StringBuilder buf = new StringBuilder();
		if( null != this.selectedStatusStringArray ) {
			buf.append("[");	
			buf.append( TriStringUtils.convertArrayToString( this.selectedStatusStringArray ) ) ;
			buf.append("]");
		}
		return buf.toString();
	}
	
	public String[] getSelectedStatusStringArray() {
		return selectedStatusStringArray;
	}
	
}
