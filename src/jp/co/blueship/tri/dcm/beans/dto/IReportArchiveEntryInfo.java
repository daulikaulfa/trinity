package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用のアーカイブエントリエンティティのインタフェースです。
 *
 */
public  interface IReportArchiveEntryInfo extends IReportInfo, Comparable<IReportArchiveEntryInfo> {
	
	
		
	public String getName();
	public void setName( String name );
	
	public String getSize();
	public void setSize( String size );
	
	public String getTime();
	public void setTime( String time );
	
	public String getGroupName();
	public void setGroupName( String groupName );
	
	public String getUserName();
	public void setUserName( String userName );
	
	public String getUserId();
	public void setUserId( String userId );
	
	public String getMode();
	public void setMode( String mode );
	
	public IReportArchiveEntryGroupInfo[] getArchiveEntryGroup();
	public void setArchiveEntryGroup( IReportArchiveEntryGroupInfo[] archiveEntryGroup );
}
