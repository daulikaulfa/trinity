package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用の文字列エンティティのインタフェースです。
 *
 */
public interface IReportStringInfo extends IReportInfo {
	
	
	
	/**
	 * 文字列を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getValue();
	/**
	 * 文字列を設定します。
	 * @param processName 処理名
	 */
	public void setValue( String value );
}
