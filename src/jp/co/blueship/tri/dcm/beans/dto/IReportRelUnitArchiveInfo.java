package jp.co.blueship.tri.dcm.beans.dto;


/**
 * レポート用のアーカイブエントリ（ビルド用）エンティティのインタフェースです。
 *
 */
public interface IReportRelUnitArchiveInfo extends IReportInfo {
	
	
	
	/**
	 * ビルドパッケージ番号を取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public String getBuildNo();
	/**
	 * ビルドパッケージ番号を設定します
	 * 
	 * @param buildNo ビルドパッケージ番号
	 */
	public void setBuildNo( String buildNo );
	
	/**
	 * アーカイブファイル格納トップパスを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public String getArchiveTopPath();
	/**
	 * アーカイブファイル格納トップパスを設定します
	 * 
	 * @param archiveTopPath アーカイブファイル格納トップパス
	 */
	public void setArchiveTopPath( String archiveTopPath );
	
	/**
	 * アーカイブファイルグループを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public IReportArchiveEntryGroupInfo[] getArchiveEntryGroup();
	/**
	 * アーカイブファイルグループを設定します
	 * 
	 * @param archiveFile アーカイブファイル
	 */
	public void setArchiveEntryGroup( IReportArchiveEntryGroupInfo[] archiveEntryGroup );
	
	/**
	 * アーカイブファイルを取得します。
	 * 
	 * @return 取得した情報を戻します
	 */
	public IReportArchiveEntryInfo[] getArchiveEntry();
	/**
	 * アーカイブファイルを設定します
	 * 
	 * @param archiveFile アーカイブファイル
	 */
	public void setArchiveEntry( IReportArchiveEntryInfo[] archiveEntry );
}
