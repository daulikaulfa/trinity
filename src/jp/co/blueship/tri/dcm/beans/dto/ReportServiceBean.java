package jp.co.blueship.tri.dcm.beans.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class ReportServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	private String reportId = new String();
	private String GroupName = new String();

	public String getReportId() {
		return reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

}
