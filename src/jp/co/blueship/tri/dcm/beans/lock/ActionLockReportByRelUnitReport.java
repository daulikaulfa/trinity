package jp.co.blueship.tri.dcm.beans.lock;

import java.util.List;

import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitReportParamInfo;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelUnitReportEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * ビルドパッケージ・作成レポートに関連する業務排他を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionLockReportByRelUnitReport extends ActionPojoAbstract<FlowRelUnitReportServiceBean> {

	private FlowRelUnitReportEditSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowRelUnitReportEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitReportServiceBean> execute( IServiceDto<FlowRelUnitReportServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		List<Object> paramList = serviceDto.getParamList();

		IReportRelUnitReportParamInfo param = ExtractMessageAddonUtil.extractReportRelUnitReportParamInfo( paramList );
		if ( null == param ) {
			throw new TriSystemException( DcmMessageId.DCM005004S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		try {
			IRepEntity reportEntity = this.insertReportEntity( param );
			paramList.add( reportEntity );
			support.getSmFinderSupport().cleaningExecDataSts(DcmTables.DCM_REP, param.getRepId());
			support.getSmFinderSupport().registerExecDataSts(
					paramBean.getProcId(), DcmTables.DCM_REP, DcmRepStatusIdForExecData.CreatingReport, param.getRepId());

		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}

	/**
	 * レポートレコードの登録
	 * <br>
	 * <br>ステータスを「処理中」としてレポートレコードを登録する。
	 *
	 * @param paramBean
	 * @return 登録したレコード情報
	 */
	private final IRepEntity insertReportEntity(
			IReportRelUnitReportParamInfo paramBean ) {

		IRepEntity entity = new RepEntity();

		entity.setRepId				( paramBean.getRepId() );
		entity.setRepCtgCd			( paramBean.getRepCtgCd() );
		entity.setExecUserNm		( paramBean.getUserName() );
		entity.setExecUserId		( paramBean.getUserId() );
		entity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setStsId				( DcmRepStatusId.Unprocessed.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setLotId				( paramBean.getLotNo() );

		this.support.getRepDao().insert( entity );

		return entity;
	}

}
