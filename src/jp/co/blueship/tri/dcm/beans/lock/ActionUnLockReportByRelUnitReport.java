package jp.co.blueship.tri.dcm.beans.lock;

import java.util.List;

import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitReportParamInfo;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelUnitReportEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * ビルドパッケージ・作成レポートに関連する業務排他の解除を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionUnLockReportByRelUnitReport extends ActionPojoAbstract<FlowRelUnitReportServiceBean> {

	private FlowRelUnitReportEditSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowRelUnitReportEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitReportServiceBean> execute( IServiceDto<FlowRelUnitReportServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		List<Object> paramList = serviceDto.getParamList();

		IReportRelUnitReportParamInfo param = ExtractMessageAddonUtil.extractReportRelUnitReportParamInfo( paramList );
		if ( null == param ) {
			throw new TriSystemException( DcmMessageId.DCM005004S );
		}

		try {
			IRepEntity repEntity = DcmExtractEntityAddonUtil.extractReport( paramList );
			if ( null == repEntity ){
				return serviceDto;
			}

			IGeneralServiceBean paramBean = serviceDto.getServiceBean();

			if ( param.isSuccess() ) {
				this.updateReportEntityBySuccess( repEntity );
				support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
				paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );
			} else {
				this.updateReportEntityByError( repEntity );
				support.getSmFinderSupport().updateExecDataSts(
						paramBean.getProcId(), DcmTables.DCM_REP, DcmRepStatusIdForExecData.ReportError, param.getRepId());
			}

		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}

		return serviceDto;
	}

	/**
	 * （正常）レポートレコードの更新
	 * <br>
	 * <br>ステータスを「処理完了」としてレポートレコードを更新する。
	 *
	 * @param finder
	 * @param entity
	 */
	private final void updateReportEntityBySuccess( IRepEntity entity ) {

		entity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setStsId				( DcmRepStatusId.ReportCreated.getStatusId() );

		support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

	/**
	 * （異常）レポートレコードの更新
	 * <br>
	 * <br>ステータスを「処理エラー」としてレポートレコードを更新する。
	 *
	 * @param finder
	 * @param entity
	 */
	private final void updateReportEntityByError( IRepEntity entity ) {

		entity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );

		support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

}
