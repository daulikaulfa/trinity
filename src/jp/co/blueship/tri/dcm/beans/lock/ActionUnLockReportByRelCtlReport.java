package jp.co.blueship.tri.dcm.beans.lock;

import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlReportParamInfo;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.support.FlowRelCtlReportEditSupport;

/**
 * リリース・作成レポートに関連する業務排他の解除を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionUnLockReportByRelCtlReport extends ActionPojoAbstract<IGeneralServiceBean> {

	private FlowRelCtlReportEditSupport support = null;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowRelCtlReportEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		this.outputBLStartLog( this.getClass().toString() );

		IReportRelCtlReportParamInfo param = ExtractMessageAddonUtil.extractReportRelCtlReportParamInfo( serviceDto.getParamList() );
		if ( null == param ) {
			throw new TriSystemException( RmMessageId.RM005007S );
		}

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		try {
			IRepEntity reportEntity = RmExtractEntityAddonUtils.extractReport( serviceDto.getParamList() );
			if ( null == reportEntity )
				return serviceDto;

			if ( param.isSuccess() ) {
				this.updateReportEntityBySuccess( reportEntity );
				support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
				paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );
			} else {
				this.updateReportEntityByError( reportEntity );
				support.getSmFinderSupport().updateExecDataSts(
						paramBean.getProcId(), DcmTables.DCM_REP, DcmRepStatusIdForExecData.ReportError, param.getRepId());
			}

			return serviceDto;

		} finally {
			this.outputBLEndLog( this.getClass().toString() );
		}
	}

	/**
	 * （正常）レポートレコードの更新
	 * <br>
	 * <br>ステータスを「処理完了」としてレポートレコードを更新する。
	 *
	 * @param finder
	 * @param entity
	 */
	private final void updateReportEntityBySuccess( IRepEntity entity ) {


		entity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setStsId				( DcmRepStatusId.ReportCreated.getStatusId() );

		support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

	/**
	 * （異常）レポートレコードの更新
	 * <br>
	 * <br>ステータスを「処理エラー」としてレポートレコードを更新する。
	 *
	 * @param finder
	 * @param entity
	 */
	private final void updateReportEntityByError( IRepEntity entity ) {

		entity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );

		support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

}
