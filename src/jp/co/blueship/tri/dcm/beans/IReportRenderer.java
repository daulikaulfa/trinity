package jp.co.blueship.tri.dcm.beans;

import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * レポートのレンタリングを行うためのインタフェースです。
 * 
 */
public interface IReportRenderer {
	
	

	/**
	 * 任意のレポート情報をレンダリングしてファイル化します。
	 * 
	 * @param paramList レンダリングに必要な情報パラメータリスト
	 * @param append 追記の場合true
	 * @return
	 */
	public void render( List<Object> paramList, boolean append ) throws TriSystemException;
	
	/**
	 * レンダリングしたレポート情報を文字列化して取得します。
	 * 
	 * @param reportFile レポートファイル
	 * @return
	 */
	public String getRendererFormat();
}
