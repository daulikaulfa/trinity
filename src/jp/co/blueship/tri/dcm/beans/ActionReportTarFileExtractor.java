package jp.co.blueship.tri.dcm.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;

import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * Tarファイル展開Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2010
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class ActionReportTarFileExtractor extends ActionReportArchiveFileExtractor {

	protected String[] getExtensions() {
		return new String[]{ ".tar" };
	}

	/**
	 * 指定されたTarファイルの中身（ファイル）のリストを取得する。
	 * @param archiveFile tarファイル
	 * @param charset tarファイルの文字コード
	 * @param entryGroupMap グループ別tarエントリのリスト
	 * @param entryTopPathMap tarエントリの格納トップパス
	 * @param extensionMap 展開対象拡張子のマップ
	 * @param loopLevel 何重目のアーカイブかのカウント
	 * @throws TriSystemException
	 */
	public void setEntry(
			File archiveFile, Charset charset,
			Map<String,IReportArchiveEntryGroupInfo> entryGroupMap,
			Map<String[],String> entryTopPathMap,
			Map<String,String> extensionMap,
			List<String> groupNameList,
			int loopLevel )
					throws TriSystemException {

		TarInputStream tin	= null;
		TarEntry tarEntry	= null;

		try {

			InputStream is	= new FileInputStream( archiveFile );
			tin				= new TarInputStream( is );

			// 並びかえ
			List<EntryWrapper> entryWrapperList = new ArrayList<EntryWrapper>();

			while ( ( tarEntry = tin.getNextEntry() ) != null ) {

				if ( tarEntry.isDirectory() ) continue;
				// bugfix版ant.jarで使用していた部分をコメントアウト
//				String entryName = new String( tin.getByteName(), charset.getValue() );
//				tarEntry.setName( entryName );

				entryWrapperList.add( new EntryWrapper( tarEntry ) );
			}
			Collections.sort( entryWrapperList );

			loopLevel = loopLevel + 1;

			for ( EntryWrapper entryWrapper :  entryWrapperList ) {

				boolean setFlag = true;

				for ( ActionReportArchiveFileExtractor extractor : getArchiveFileExtractorList() ) {

					if ( extractor.isExtraction( entryWrapper.getName(), loopLevel, extensionMap )) {

						List<String> childGroupNameList =
								setArchiveEntry( entryWrapper, entryGroupMap, entryTopPathMap, loopLevel, groupNameList );
						setFlag = false;

						File inputFile	= entryWrapper.getTarEntry().getFile();
						if( null == inputFile ) {
							break ;
						}
						extractor.setArchiveFileExtractorList( getArchiveFileExtractorList() );
						extractor.setEntry( inputFile, charset, entryGroupMap, entryTopPathMap, extensionMap, childGroupNameList, loopLevel );
						break;
					}
				}

				if ( setFlag ) {
					setArchiveEntry( entryWrapper, entryGroupMap, entryTopPathMap, loopLevel, groupNameList );
				}
			}

		} catch ( IOException ioe ) {
			throw new TriSystemException( DcmMessageId.DCM004001F, ioe );
		} finally {
			TriFileUtils.close(tin);
		}

	}
}
