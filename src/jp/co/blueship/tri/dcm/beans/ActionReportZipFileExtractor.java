package jp.co.blueship.tri.dcm.beans;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;

import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * Zipファイル展開Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2010
 */
public class ActionReportZipFileExtractor extends ActionReportArchiveFileExtractor {

	protected String[] getExtensions() {
		return new String[]{ ".zip", ".jar", ".war", ".ear" };
	}

	/**
	 * 指定されたZipファイルの中身（ファイル）のリストを取得する。
	 * @param archiveFile zipファイル
	 * @param charset zipファイルの文字コード
	 * @param entryGroupMap グループ別zipエントリのリスト
	 * @param entryTopPathMap zipエントリの格納トップパス
	 * @param extensionMap 展開対象拡張子のマップ
	 * @param groupNameList このアーカイブファイルがどのグループに属するのかのリスト
	 * @param loopLevel 何重目のアーカイブかのカウント
	 * @throws TriSystemException
	 */
	public void setEntry(
			File archiveFile, Charset charset,
			Map<String,IReportArchiveEntryGroupInfo> entryGroupMap,
			Map<String[],String> entryTopPathMap,
			Map<String,String> extensionMap,
			List<String> groupNameList,
			int loopLevel )
					throws TriSystemException {

		ZipFile zipFile = null;

		try {

			zipFile = new ZipFile( archiveFile, charset.value() );


			// 並びかえ
			List<EntryWrapper> entryWrapperList = new ArrayList<EntryWrapper>();
			for ( Enumeration<?> e = zipFile.getEntries(); e.hasMoreElements(); ) {

				ZipEntry zipEntry = (ZipEntry)e.nextElement();
				if ( zipEntry.isDirectory() ) continue;

				entryWrapperList.add( new EntryWrapper( zipEntry ) );
			}
			Collections.sort( entryWrapperList );


			loopLevel = loopLevel + 1;

			for ( EntryWrapper entryWrapper :  entryWrapperList ) {

				boolean setFlag	= true;

				for ( ActionReportArchiveFileExtractor extractor : getArchiveFileExtractorList() ) {

					if ( extractor.isExtraction( entryWrapper.getName(), loopLevel, extensionMap )) {

						List<String> childGroupNameList =
								setArchiveEntry( entryWrapper, entryGroupMap, entryTopPathMap, loopLevel, groupNameList );
						setFlag = false;


						InputStream inputStream = null;
						File inputFile			= null;

						try {
							inputStream	= zipFile.getInputStream( entryWrapper.getZipEntry() );
							inputFile	= createTemporaryFile( inputStream );
							if( null == inputFile ) {
								break ;
							}
							extractor.setArchiveFileExtractorList( getArchiveFileExtractorList() );
							extractor.setEntry( inputFile, charset, entryGroupMap, entryTopPathMap, extensionMap, childGroupNameList, loopLevel );
						} finally {
							if ( null != inputStream ) inputStream.close();
							if ( null != inputFile && inputFile.exists() ) inputFile.delete();
						}

						break;
					}
				}

				if ( setFlag ) {
					setArchiveEntry( entryWrapper, entryGroupMap, entryTopPathMap, loopLevel, groupNameList );
				}
			}

		} catch ( IOException ioe ) {
			throw new TriSystemException( DcmMessageId.DCM004002F, ioe );
		} finally {
			TriFileUtils.closeOfZipFile(zipFile);
		}
	}

	/**
	 * 一時ファイルを作成する。
	 * @param is
	 * @return
	 * @throws IOException
	 */
	private File createTemporaryFile( InputStream is ) throws IOException {

		File tempFile = null;
		BufferedInputStream inBuffer	= null;
		BufferedOutputStream outBuffer	= null;

		try {

			tempFile = File.createTempFile( "ZipEntry_", ".tmp" );
			tempFile.deleteOnExit();

			inBuffer	= new BufferedInputStream( is );
			outBuffer	= new BufferedOutputStream( new FileOutputStream( tempFile ) );

			int contents = 0;
			while (( contents = inBuffer.read() ) != -1 ) {
				outBuffer.write( contents );
			}

		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
		}

		return tempFile;
	}
}
