package jp.co.blueship.tri.dcm.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.zip.ZipEntry;

import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportArchiveEntryInfo;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * アーカイブファイル展開の抽象Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2010
 */
public abstract class ActionReportArchiveFileExtractor {
	
	

	private List<ActionReportArchiveFileExtractor> extractorList = null;

	public final void setArchiveFileExtractorList(
			List<ActionReportArchiveFileExtractor> extractorList ) {

		this.extractorList = new ArrayList<ActionReportArchiveFileExtractor>();
		for ( ActionReportArchiveFileExtractor extractor : extractorList ) {
			this.extractorList.add	( extractor );	
		}
	}
	protected List<ActionReportArchiveFileExtractor> getArchiveFileExtractorList() {
		return this.extractorList;
	}
	
	/**
	 * ファイル名から、解凍対象のファイルかを判定する。
	 * @param fileName ファイル名
	 * @param loopLevel
	 * @param extensionMap 展開対象拡張子のマップ
	 * @return 解凍対象であればtrue、そうでなければfalse
	 */
	public boolean isExtraction( String fileName, int loopLevel, Map<String,String> extensionMap ) {
		
		String extensionString = extensionMap.get( String.valueOf( loopLevel ) );
		
		// 回りきった or 拡張子の指定なし
		if ( TriStringUtils.isEmpty( extensionString )) return false;
		
		String[] extensions = extensionString.split( "," );
		
		for ( String extension : extensions ) {
		
			if ( fileName.toLowerCase().endsWith( extension )) {
				
				for ( String getExtension : getExtensions() ) {
				
					if ( fileName.toLowerCase().endsWith( getExtension )) {
						return true;		
					}
				}
			}
		}
		
		return false;
	}
	
	protected abstract String[] getExtensions();
	
	/**
	 * 指定されたアーカイブファイルの中身（ファイル）のリストを取得する。
	 * @param archiveFile アーカイブファイル
	 * @param charset アーカイブファイルの文字コード
	 * @param entryGroupMap グループ別アーカイブエントリのリスト
	 * @param entryTopPathMap アーカイブエントリの格納トップパス
	 * @param extensionMap 展開対象拡張子のマップ
	 * @param groupNameList このアーカイブファイルがどのグループに属するのかのリスト
	 * @param loopLevel 何重目のアーカイブかのカウント
	 * @throws TriSystemException
	 */
	public abstract void setEntry(
			File archiveFile, Charset charset,
			Map<String,IReportArchiveEntryGroupInfo> entryGroupMap,
			Map<String[],String> entryTopPathMap,
			Map<String,String> extensionMap,
			List<String> groupNameList,
			int loopLevel )
		throws TriSystemException;
	
	/**
	 * 指定されたアーカイブファイルの中身の情報を取得する。
	 * @param EntryWrapper アーカイブファイルの中身
	 * @param entryGroupMap グループ別アーカイブエントリのリスト
	 * @param entryTopPathMap アーカイブエントリの格納トップパス
	 * @param loopLevel 0:最初のアーカイブファイル（ここには入ってこない）、1:子のアーカイブファイル....
	 * @param groupNameList このアーカイブがどのグループに属するかのリスト
	 * @throws IOException
	 */
	protected List<String> setArchiveEntry(
			EntryWrapper entryWrapper,
			Map<String,IReportArchiveEntryGroupInfo> entryGroupMap,
			Map<String[],String> entryTopPathMap,
			int loopLevel,
			List<String> groupNameList ) {
		
		String[] entryPaths = TriStringUtils.convertPath( entryWrapper.getName() ).split( "/" ); 
		List<String> childGroupNameList = new ArrayList<String>();
		
		for ( String[] entryTopPath : entryTopPathMap.keySet() ) {
			
			if ( 1 == loopLevel && startWith( entryPaths, entryTopPath ) || 1 < loopLevel ) {
			
				String groupName = entryTopPathMap.get( entryTopPath );
				if ( null != groupNameList && !groupNameList.contains( groupName )) continue;
				
				
				IReportArchiveEntryGroupInfo entryGroup = entryGroupMap.get( groupName );
				if ( null == entryGroup ) {
					entryGroup = new ReportArchiveEntryGroupInfo();
					entryGroup.setIdentifierId	( "ARCHIVE_ENTRY" );
					entryGroup.setName			( groupName );
					
					entryGroupMap.put	( groupName, entryGroup );
				}
				
				
				IReportArchiveEntryInfo entry = new ReportArchiveEntryInfo();
				
				entry.setIdentifierId		( "ARCHIVE_ENTRY" );
				//entry.setName				( join( entryPaths, entryTopPath ));
				entry.setName				( attachIndent( TriStringUtils.convertPath( entryWrapper.getName() ), loopLevel ));
				setReportEntry				( entry, entryWrapper );
				
				entryGroup.addArchiveEntry	( entry );
				
				childGroupNameList.add( entryGroup.getName() );
			}
		}
		
		return childGroupNameList;
	}

	/**
	 * エントリ名にインデントを付与する。
	 * @return
	 */
	private String attachIndent( String entryName, int loopLevel ) {
		
		StringBuilder sb = new StringBuilder();
		
		for ( int i = 0; i < loopLevel - 1; i++ ) {
			sb.append( "  " );
		}
		sb.append( entryName ); 
		
		return sb.toString();
	}
	
	/**
	 * レポート用エントリーに情報を設定する。
	 * @param reportEntry
	 * @param entry
	 */
	private void setReportEntry( IReportArchiveEntryInfo reportEntry, EntryWrapper entry ) {
		
		reportEntry.setTime			( TriDateUtils.getDefaultDateFormat().format( entry.getTime() ));
		reportEntry.setSize			( Long.toString( entry.getSize() ));
		reportEntry.setUserName		( entry.getUserName() );
		reportEntry.setGroupName	( entry.getGroupName() );
		reportEntry.setMode			( entry.getMode() );
	}
	
	/**
	 * 文字配列が、指定された文字配列で始まるかをチェックする。
	 * @param strings 文字配列
	 * @param starts 始まりの文字配列
	 * @return 始まっていればtrue、そうでなければfalse
	 */
	public boolean startWith( String[] strings, String[] starts ) {
		 
		if ( "*".equals( starts[ starts.length - 1 ] )) {
			
			starts = chop( starts );
			

			if ( TriStringUtils.isEmpty( starts ) || ( 1 == starts.length && "".equals( starts[0] ))) {
			
				// 開始文字列が空白の場合、全エントリＯＫ
				return true;
				
			} else {
				
				// 開始文字列が空白でない場合、開始文字列をパス分解したものと、対象の文字列を頭から比較する 
				if ( starts.length < strings.length ) {
					
					for ( int i = 0; i < starts.length; i++ ) {
						
						if ( !starts[ i ].equals( strings[ i ] )) {
							return false;
						}
					}
					return true;
				}
			}
			
			return false;
			
		} else {
			
			if ( TriStringUtils.isEmpty( starts ) || ( 1 == starts.length && "".equals( starts[0] ))) {
				
				// 開始文字列が空白の場合、全エントリＯＫ
				if ( 1 == strings.length ) {
					return true;
				} else {
					return false;
				}
				
			} else {
				
				// 開始文字列が空白でない場合、開始文字列をパス分解したものと、対象の文字列を頭から比較する 
				if ( starts.length == strings.length + 1 ) {
					
					for ( int i = 0; i < starts.length; i++ ) {
						
						if ( !starts[ i ].equals( strings[ i ] )) {
							return false;
						}
					}
					return true;				
				}
			}
			
			return false;
		}
	}
	
	/**
	 * 配列の最後の１要素を削除する
	 * @param array
	 * @return
	 */
	private String[] chop( String[] array ) {
		
		List<String> list = new ArrayList<String>();
		for ( int i = 0; i < array.length - 1; i++ ) {
			list.add( array[ i ] );
		}
		
		return (String[])list.toArray( new String[0] );
	}
	
	/**
	 * ZipEntry、TarEntryの被せ物Class
	 *
	 */
	protected class EntryWrapper implements Comparable<EntryWrapper> {
		
		/** Shift used in formatting permissions */
		private int[] shft		= { 6, 3, 0 };
		/** Format strings used in permissions */
		private String rwx[]	= { "---", "--x", "-w-", "-wx", "r--", "r-x", "rw-", "rwx" };

		private ZipEntry zipEntry	= null;
		private TarEntry tarEntry	= null;
		
		public int compareTo( EntryWrapper arg0 ) {
			return this.getName().compareTo( arg0.getName() );
		}
		
		public ZipEntry getZipEntry() {
			return zipEntry;
		}
		
		public TarEntry getTarEntry() {
			return tarEntry;
		}
		
		public EntryWrapper( ZipEntry zipEntry ) {
			this.zipEntry = zipEntry;
			this.tarEntry = null;
		}
		
		public EntryWrapper( TarEntry tarEntry ) {
			this.tarEntry = tarEntry;
			this.zipEntry = null;
		}
		
		public String getName() {
			
			String name = null;
			if ( null != this.zipEntry ) {
				name = this.zipEntry.getName();
			} else if ( null != this.tarEntry ) {
				name = this.tarEntry.getName();
			}
			
			return name;
		}

		public Date getTime() {
			
			Date time = null;
			if ( null != this.zipEntry ) {
				time = new Date( this.zipEntry.getTime() );
			} else if ( null != this.tarEntry ) {
				time = this.tarEntry.getModTime();
			}
			
			return time;
		}

		public long getSize() {
			
			long size = -1;
			if ( null != this.zipEntry ) {
				size = this.zipEntry.getSize();
			} else if ( null != this.tarEntry ) {
				size = this.tarEntry.getSize();
			}
			
			return size;
		}
		
		public String getGroupName() {
			
			String groupName = null;
			if ( null != this.tarEntry ) {
				groupName = this.tarEntry.getGroupName();
			}
			
			return groupName;
		}
		
		public String getUserName() {
			
			String userName = null;
			if ( null != this.tarEntry ) {
				userName = this.tarEntry.getUserName();
			}
			
			return userName;
		}
		
		public String getMode() {
			
			String strMode = null;
			if ( null != this.tarEntry ) {
				
				StringBuilder bf	= new StringBuilder();
				int mode		= this.tarEntry.getMode();

			    for ( int i = 0; i < 3; i++ ) {
			      bf.append( rwx[ mode >> shft[i] & 007 ] );
			    }
			    
			    strMode = bf.toString();
			}
			
			return strMode;
		}
	}
}
