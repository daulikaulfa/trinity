
package jp.co.blueship.tri.dcm.beans;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.nio.channels.FileChannel;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.AreaReference;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.util.CellRangeAddress;

import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportInfo;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmHeadBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.DcmProcessStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetProduct;
import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Eguchi Yukihiro
 *
 * @version V4.02.00
 * @author anh.Nguyen Duc
 *
 */
public class ReportRendererExcelPOIFormat {

	/** １セルに入力可能な最大文字数 **/
	private static final int CELL_STRING_MAX = 32767 - 767 ;//32767が最大文字数。「上限越え省略メッセージ」と余裕分をみて差し引いている

	/** １セルに入力可能な最大文字数の上限を超えた場合の省略メッセージ **/
	private static final String CELL_STRING_OVER_MESSAGE = " 【セル出力可能な上限文字数に達したので以降省略しました。】" ;

	/** シートごとにカテゴリごとのループ回数(シート) */
	private Map<String,Map<String, Integer>> sheetLoopCountMap = null;

	/** シートごとにカテゴリごとのループ回数(行) */
	private Map<String,Map<String, Integer>> rowLoopCountMap = null;

	/** シートごとにシートがテンプレートに対して何行下方にシフトしたか */
	private Map<String,Integer> totalShiftRowsNumberMap = null;

	/** シートごとのヘッダ作成済みフラグ */
	private Map<String, List<IEntity>> isHeaderCreatedMap = null;

	/** マッピング情報 */
	private List<CategoryMappingInfo> categoryInfoList = null;

	/** テンプレートファイルのファイルパス */
	private File templateFile = null;

	/** 出力情報ファイルのファイルパス */
	private File outputInfoFile = null;

	/** レポート出力対象全エンティティ */
	private List<IEntity> allEntityList = null;

	/** テンプレートのシート名のプレフィックス */
	private final String TEMPLATE_SHEET_PREFIX = "_";

	/** 項目位置マークの開始文字列 */
	@SuppressWarnings("el-syntax")
	private final String ITEM_MARK_START = "${";

	/** 項目位置マークの終了文字列 */
	private final String ITEM_MARK_END = "}";

	/** レポートの最大行数に到達したシート名 */
	private List<String> reachedMaxRowsSheetNameList = null;

	private static final ILog log = TriLogFactory.getInstance();

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private void initInfo() {

		this.sheetLoopCountMap = new HashMap<String,Map<String, Integer>>();
		this.rowLoopCountMap = new HashMap<String,Map<String, Integer>>();
		this.totalShiftRowsNumberMap = new HashMap<String, Integer>();
		this.isHeaderCreatedMap = new HashMap<String,List<IEntity>>();
		this.categoryInfoList = new ArrayList<CategoryMappingInfo>();
		this.allEntityList = new ArrayList<IEntity>();
		this.reachedMaxRowsSheetNameList = new ArrayList<String>();
	}

	/**
	 * エンティティをレポート形式にパースする
	 * @param entityList エンティティ
	 * @param workbook エクセル
	 * @return パースした内容の文字列化
	 */
	protected String parseEntityList( List<IEntity> entityList, HSSFWorkbook workbook, boolean isLegacy, TimeZone timeZone ) throws TriSystemException {

		// 処理対象エンティティリスト
		Map<Integer, List<IEntity>> targetEntityListMap = new TreeMap<Integer, List<IEntity>>();

		// エンティティをレポート情報の記載順にグループ化して並べ替える
		for ( IEntity entity : entityList ) {

			List<CategoryMappingInfo> infoList = getCategoryMappingInfoList( entity );
			// 今回、出力しないカテゴリ
			if ( 0 == infoList.size() ) continue;

			for ( CategoryMappingInfo info : infoList ) {

				List<IEntity> tmpEntityList = targetEntityListMap.get( new Integer( info.getNo() ));
				if ( null == tmpEntityList ) {
					tmpEntityList = new ArrayList<IEntity>();
					targetEntityListMap.put( new Integer( info.getNo() ), tmpEntityList );
				}
				tmpEntityList.add( entity );
			}
		}

		// 子エンティティ対象リスト
		Map<Integer, List<IEntity>> subEntityListMap = new TreeMap<Integer, List<IEntity>>();
		subEntityListMap.putAll( targetEntityListMap );

		// 処理済みエンティティリスト
		Map<Integer, List<IEntity>> finEntityListMap = new TreeMap<Integer, List<IEntity>>();

		// 追加済み子エンティティリスト
		Map<Integer, List<IEntity>> addedSubEntityListMap = new TreeMap<Integer, List<IEntity>>();

		// まず、ヘッダカテゴリ分だけ先に追加
		for ( Integer no : targetEntityListMap.keySet() ) {

			List<IEntity> finEntityList = new ArrayList<IEntity>();
			finEntityListMap.put( no, finEntityList );

			addedSubEntityListMap.put( no, new ArrayList<IEntity>() );


			CategoryMappingInfo parentCategoryInfo = getCategoryMappingInfo( no );

			if ( parentCategoryInfo.isHeaderCategory() ) {

				// ヘッダカテゴリに属するエンティティはそのまま追加
				List<IEntity> targetEntityList = targetEntityListMap.get( no );

				this.allEntityList.addAll	( targetEntityList );
				finEntityList.addAll		( targetEntityList );
			}
		}

		// エンティティをレポート情報の記載どおりに（親子関係も含めて）並べ替える
		setSubEntity( targetEntityListMap, subEntityListMap, finEntityListMap, addedSubEntityListMap, isLegacy, timeZone );

		for ( IEntity entity : this.allEntityList ) {
			parseEntity( entity, workbook, isLegacy, timeZone );
		}

		return "";
	}

	/**
	 * エンティティをレポート情報の記載どおりに（親子関係も含めて）並べ替える
	 * @param targetEntityListMap 処理対象エンティティリスト
	 * @param subEntityListMap 子エンティティ対象リスト
	 * @param finEntityListMap 処理済みエンティティリスト
	 * @param addedSubEntityListMap 追加済みエンティティリスト
	 */
	private void setSubEntity(
		Map<Integer, List<IEntity>> targetEntityListMap,
		Map<Integer, List<IEntity>> subEntityListMap,
		Map<Integer, List<IEntity>> finEntityListMap,
		Map<Integer, List<IEntity>> addedSubEntityListMap, boolean isLegacy, TimeZone timeZone ) {

		// ヘッダ以外
		for ( Integer no : targetEntityListMap.keySet() ) {

			List<IEntity> finEntityList = finEntityListMap.get( no );

			CategoryMappingInfo parentCategoryInfo = getCategoryMappingInfo( no );
			if ( parentCategoryInfo.isHeaderCategory() ) continue;


			// 処理対象エンティティリスト
			List<IEntity> targetEntityList = targetEntityListMap.get( no );

			if ( !parentCategoryInfo.isListCategory() ) {

				// ヘッダカテゴリに属するエンティティはそのまま追加
				// ループしないカテゴリに属するエンティティはそのまま追加
				this.allEntityList.addAll	( targetEntityList );
				finEntityList.addAll		( targetEntityList );

			} else {

				// カテゴリに該当するエンティティを取得
				for ( IEntity parentEntity : targetEntityList ) {

					if ( finEntityList.contains( parentEntity )) continue;

					Map<Integer, List<IEntity>> subTargetEntityListMap =
						getSubEntityListMap( parentEntity, parentCategoryInfo, subEntityListMap, addedSubEntityListMap, isLegacy, timeZone );
					finEntityList.add( parentEntity );

					setSubEntity( subTargetEntityListMap, subEntityListMap, finEntityListMap, addedSubEntityListMap, isLegacy, timeZone );
				}
			}
		}
	}

	/**
	 * 親カテゴリ情報のリンク先子カテゴリに対応するエンティティを取得する。
	 * @param parentEntity 親エンティティ
	 * @param parentCategoryInfo 親カテゴリ情報
	 * @param subEntityListMap 子エンティティの候補
	 * @param addedSubEntityListMap 追加済みエンティティリスト
	 * @return 対応するエンティティ
	 */
	private Map<Integer, List<IEntity>> getSubEntityListMap(
			IEntity parentEntity, CategoryMappingInfo parentCategoryInfo,
			Map<Integer, List<IEntity>> subEntityListMap,
			Map<Integer, List<IEntity>> addedSubEntityListMap, boolean isLegacy, TimeZone timeZone ) {

		this.allEntityList.add( parentEntity );

		// 配下のカテゴリとのリンク情報を取得
		Map<String, List<ItemMappingInfo>> subItemInfoListMap = parentCategoryInfo.getKeyMappingInfoListMap();
		// 配下のカテゴリ名でループ
		Map<Integer, List<IEntity>> subTargetEntityListMap = new TreeMap<Integer, List<IEntity>>();
		for ( String keySubCategoryName : subItemInfoListMap.keySet() ) {

			// 配下のカテゴリとのリンク用の項目情報
			List<ItemMappingInfo> subKeyItemInfoList = subItemInfoListMap.get( keySubCategoryName );
			//if ( null == subKeyItemInfoList ) continue;

			CategoryMappingInfo subCategoryInfo = getCategoryMappingInfo( keySubCategoryName );
			if ( null == subCategoryInfo ) {
				throw new TriSystemException(DcmMessageId.DCM004003F, keySubCategoryName);
			}

			if ( !subCategoryInfo.isDoOutput() ) continue;

			// シート名が違う場合はスルー
			if ( !subCategoryInfo.getTemplateSheetName().equals( parentCategoryInfo.getTemplateSheetName() )) continue;


			// 親より後に記載されたカテゴリのみ対象
			if ( subCategoryInfo.getNo() <= parentCategoryInfo.getNo() ) continue;


			// 縦方向展開の場合、子カテゴリが展開範囲に完全に入っている必要がある
			if ( ListSpreadMothod.Vertical.equals( parentCategoryInfo.getListSpreadMothod() )) {
				if ( !isContain( parentCategoryInfo, subCategoryInfo )) continue;
			}


			// 配下のカテゴリ情報の、親カテゴリとのリンク情報を取得
			List<ItemMappingInfo> parentKeyItemInfoList =
				subCategoryInfo.getKeyMappingInfoListMap().get( parentCategoryInfo.getCategoryName() );
			if ( null == parentKeyItemInfoList ) {
				throw new TriSystemException(DcmMessageId.DCM004004F , keySubCategoryName , parentCategoryInfo.getCategoryName());
			}
			if ( subKeyItemInfoList.size() != parentKeyItemInfoList.size() ) {
				throw new TriSystemException(DcmMessageId.DCM004033F ,  keySubCategoryName , parentCategoryInfo.getCategoryName());
			}

			List<IEntity> subEntityList =
				getSubEntity(	parentEntity, subKeyItemInfoList, subCategoryInfo,
								parentKeyItemInfoList, subEntityListMap, addedSubEntityListMap, isLegacy, timeZone );

			subTargetEntityListMap.put( new Integer( subCategoryInfo.getNo() ), subEntityList );
		}

		return subTargetEntityListMap;
	}

	/**
	 * カテゴリ名に該当するカテゴリ情報を取得する。
	 * @param no カテゴリ番号
	 * @return カテゴリ名に該当するカテゴリ情報
	 * @throws TriSystemException
	 */
	private CategoryMappingInfo getCategoryMappingInfo( Integer no ) {

		for ( CategoryMappingInfo info : this.categoryInfoList ) {
			if ( no ==  info.getNo() ) {
				return info;
			}
		}

		return null;
	}

	/**
	 * カテゴリ名に該当するカテゴリ情報を取得する。
	 * @param categoryName カテゴリ名
	 * @return カテゴリ名に該当するカテゴリ情報
	 */
	private CategoryMappingInfo getCategoryMappingInfo( String categoryName ) {

		for ( CategoryMappingInfo info : this.categoryInfoList ) {

			if ( categoryName.equals( info.getCategoryName() )) {
				return info;
			}
		}

		return null;
	}

	/**
	 *
	 * @param parentEntity 親エンティティ
	 * @param subKeyItemInfoList 親エンティティが持つ、子エンティティへのリンク情報（項目情報）
	 * @param subInfo 子エンティティのカテゴリ情報
	 * @param parentKeyItemInfoList 子エンティティが持つ、親エンティティへのリンク情報（項目情報）
	 * @param subEntityListMap 子エンティティの候補
	 * @param addedSubEntityListMap 追加済み子エンティティリスト
	 * @return
	 * @throws TriSystemException
	 */
	private List<IEntity> getSubEntity(
			IEntity parentEntity, List<ItemMappingInfo> subKeyItemInfoList,
			CategoryMappingInfo subInfo, List<ItemMappingInfo> parentKeyItemInfoList,
			Map<Integer, List<IEntity>> subEntityListMap,
			Map<Integer, List<IEntity>> addedSubEntityListMap, boolean isLegacy, TimeZone timeZone ) throws TriSystemException {

		List<IEntity> retEntityList = new ArrayList<IEntity>();

		// 配下のカテゴリ情報に該当するエンティティを取得
		List<IEntity> subEntityList = subEntityListMap.get( new Integer( subInfo.getNo() ));
		if ( null == subEntityList ) return retEntityList;

		List<IEntity> addedEntityList = addedSubEntityListMap.get( new Integer( subInfo.getNo() ));


		// リンク項目の内容をチェックして仕分ける
		List<IEntity> tmpEntityList = subEntityList;
		for ( int i = 0; i < subKeyItemInfoList.size(); i++ ) {

			ItemMappingInfo subItemMapInfo		= subKeyItemInfoList.get( i );
			ItemMappingInfo parentItemMapInfo	= parentKeyItemInfoList.get( i );

			List<String> parentValueList = new ArrayList<String>();
			setValueList( parentEntity, subItemMapInfo, parentValueList, isLegacy, timeZone );
			if ( 0 == parentValueList.size() ) continue;

			tmpEntityList = getSubEntity(
					tmpEntityList, parentItemMapInfo, parentValueList, isLegacy, timeZone );
		}

		// 複数項目で展開方法がない場合、親と１対１で対応
		if ( ListSpreadMothod.Nothing.equals( subInfo.getListSpreadMothod() )) {
			retEntityList.add( tmpEntityList.get( 0 ));
		} else {

			// 子エンティティ群の中で、同じキーを持つエンティティは除外する
			List<String> keyValueList = new ArrayList<String>();
			for ( IEntity tmpEntity : tmpEntityList ) {

				if ( addedEntityList.contains( tmpEntity )) continue;

				if ( !( tmpEntity instanceof IReportInfo )) {
					throw new TriSystemException(DcmMessageId.DCM005007S , tmpEntity.getClass().getSimpleName());
				}

				String keyValue = ((IReportInfo)tmpEntity).getKey();
				if ( !keyValueList.contains( keyValue )) {
					retEntityList.add		( tmpEntity );
					addedEntityList.add		( tmpEntity );
					keyValueList.add		( keyValue );
				}
			}
		}

		return retEntityList;
	}


	/**
	 * エンティティのリストのうち、指定された値を持つエンティティを取得する。
	 * @param entityList エンティティリスト
	 * @param info 項目情報
	 * @param valueList 値
	 * @return 指定された値を持つエンティティのリスト
	 */
	private List<IEntity> getSubEntity(
			List<IEntity> entityList, ItemMappingInfo info, List<String> valueList, boolean isLegacy, TimeZone timeZone ) throws TriSystemException {

		List<IEntity> retEntityList = new ArrayList<IEntity>();

		for ( IEntity subEntity : entityList ) {

			List<String> subValueList = new ArrayList<String>();
			setValueList( subEntity, info, subValueList, isLegacy, timeZone );
			if ( 1 != subValueList.size() ) {
				throw new TriSystemException(DcmMessageId.DCM004005F , info.getCategoryName() , info.getItemName());
			}

			if ( valueList.contains( subValueList.get( 0 ) )) {
				retEntityList.add	( subEntity );
			}
		}

		return retEntityList;

	}

	/**
	 * テンプレートファイルをレポートファイルにコピーする
	 * @param templateFile テンプレートファイル
	 * @param reportFile レポートファイル
	 */
	private void copyFile( File templateFile, File reportFile ) {

		FileInputStream fis = null;
		FileOutputStream fos = null;
		FileChannel srcChannel = null;
		FileChannel dstChannel = null;

		try {
			fis = new FileInputStream( templateFile );
			fos = new FileOutputStream( reportFile );

			srcChannel = fis.getChannel();
			dstChannel = fos.getChannel();

			srcChannel.transferTo( 0, srcChannel.size(), dstChannel );

		} catch ( FileNotFoundException e ) {
			throw new TriSystemException(DcmMessageId.DCM005008S,e);
		} catch ( IOException e ) {
			throw new TriSystemException(DcmMessageId.DCM005008S,e);
		} finally {
			TriFileUtils.close(srcChannel);
			TriFileUtils.close(dstChannel);
			TriFileUtils.close(fis);
			TriFileUtils.close(fos);
		}
	}

	/**
	 * レポートのエクセルデータを取得する
	 * @param reportFile 既存のレポートファイル(追記先)
	 * @param append 追記であればtrue
	 * @return エクセルデータ
	 * @throws TriSystemException
	 */
	protected HSSFWorkbook getReportWorkBook( File reportFile, boolean append ) throws TriSystemException {

		boolean createFlag = false;

		if ( !append || !reportFile.exists() ) {

			createFlag = true;
			copyFile( getReportTemplateFile(), reportFile );
			initInfo();
		}

		FileInputStream input = null;
		POIFSFileSystem poifs = null;
		HSSFWorkbook workbook = null;

		try {

			input		= new FileInputStream	( reportFile );
			poifs		= new POIFSFileSystem	( input );
			workbook	= new HSSFWorkbook		( poifs );

			if ( 0 == this.categoryInfoList.size() ) {
				setOutputInfo( workbook );
			}

			if ( createFlag ) {

				// 他のファイルからスタイルがコピーできないため、テンプレートのシートを
				// 保存して隠しておく
				Set<String> targetSheetNameSet = getTargetSheetName();
				for ( String targetSheetName : targetSheetNameSet ) {

					int targetSheetIndex = workbook.getSheetIndex( targetSheetName );
					if ( -1 == targetSheetIndex ) {
						throw new TriSystemException(DcmMessageId.DCM004006F , targetSheetName);
					}

					HSSFSheet cloneSheet	= workbook.cloneSheet( targetSheetIndex );
					int cloneIndex			= workbook.getSheetIndex( cloneSheet );
					workbook.setSheetName	( cloneIndex, this.TEMPLATE_SHEET_PREFIX + targetSheetName );
					workbook.setSheetHidden	( cloneIndex, true );
				}
			}

		} catch ( FileNotFoundException e ) {
			throw new TriSystemException(DcmMessageId.DCM005009S , e);
		} catch ( IOException e ) {
			throw new TriSystemException(DcmMessageId.DCM005009S , e);
		} finally {
			TriFileUtils.close(input);
		}

		return workbook;

	}

	/**
	 * レポート出力情報のエクセルデータを取得する
	 * @return 出力情報エクセルデータ
	 * @throws TriSystemException
	 */
	private HSSFWorkbook getOutputInfoWorkBook() throws TriSystemException {

		FileInputStream input	= null;
		POIFSFileSystem poifs	= null;
		HSSFWorkbook workbook	= null;
		File tmpFile			= null;

		try {

			File outputInfoFile = getReportOutputInfoFile();

			tmpFile = File.createTempFile(
					outputInfoFile.getParentFile().getName(), outputInfoFile.getName(), outputInfoFile.getParentFile() );
			tmpFile.deleteOnExit();
			copyFile( outputInfoFile, tmpFile );

			input		= new FileInputStream	( tmpFile );
			poifs		= new POIFSFileSystem	( input );
			workbook	= new HSSFWorkbook		( poifs );

		} catch ( FileNotFoundException e ) {
			throw new TriSystemException(DcmMessageId.DCM005010S , e);
		} catch ( IOException e ) {
			throw new TriSystemException(DcmMessageId.DCM005010S , e);
		} finally {
			TriFileUtils.close(input);
			if ( null != tmpFile && tmpFile.exists() ) {
				tmpFile.delete();
			}
		}

		return workbook;

	}

	/**
	 * マッピング対象のシート名を取得する
	 * @return マッピング対象のシート名
	 */
	private Set<String> getTargetSheetName() {

		Set<String> targetSheetNameSet = new HashSet<String>();
		for ( CategoryMappingInfo info : this.categoryInfoList ) {
			if ( info.isDoOutput() ) {
				setTargetSheetName( targetSheetNameSet, info );
			}
		}

		return targetSheetNameSet;
	}

	/**
	 * マッピング対象のシート名を設定する
	 * @param targetSheetName シート名のセット
	 * @param info マッピング情報
	 */
	private void setTargetSheetName( Set<String> targetSheetNameSet, CategoryMappingInfo info ) {

		targetSheetNameSet.add( info.getSheetName() );

	}

	/**
	 * レポートの書き込みを行う
	 * @param reportFile 書き込み先のファイル
	 * @param workbook 書き込むエクセルデータ
	 * @param finish レポート完成フラグ（true：レポート完成、false：とりあえず書き込み）
	 * @throws TriSystemException
	 */
	protected void writeReport( File reportFile, HSSFWorkbook workbook, boolean finish ) throws TriSystemException {

		if ( finish ) {
			initSheet( workbook );
		}

		BufferedOutputStream output	= null;

		try {
			output	= new BufferedOutputStream	( new FileOutputStream( reportFile ) );

			workbook.write( output );

		} catch ( FileNotFoundException e ) {
			throw new TriSystemException(DcmMessageId.DCM005011S , e);
		} catch ( IOException e ) {
			throw new TriSystemException(DcmMessageId.DCM005011S , e);
		} finally {
			TriFileUtils.close(output);
		}
	}

	/**
	 * シートを初期化する
	 * @param targetSheet コピー先シート
	 * @param templateSheet コピー元シート
	 */
	private void initSheet( HSSFWorkbook workbook ) {

		for ( CategoryMappingInfo categoryInfo : this.categoryInfoList ) {

			// テンプレートとして隠していたシートを削除
			int hiddenIndex = workbook.getSheetIndex( this.TEMPLATE_SHEET_PREFIX + categoryInfo.getTemplateSheetName() );
			if ( -1 < hiddenIndex ) {
				workbook.removeSheetAt( hiddenIndex );
			}
		}

		// 項目位置マークを削除
		for ( int sheetIndex = 0; sheetIndex < workbook.getNumberOfSheets(); sheetIndex++ ) {

			HSSFSheet targetSheet	= workbook.getSheetAt( sheetIndex );

			for ( int rowIndex = 0; rowIndex <= targetSheet.getLastRowNum(); rowIndex++ ) {

				HSSFRow row = targetSheet.getRow( rowIndex );
				if ( null == row ) continue;

				for ( int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++ ) {

					HSSFCell cell = row.getCell( cellIndex );
					if ( null == cell ) continue;

					String value = getStringValue( cell );
					List<String> markList = getItemMarkList( value );

					if ( null != markList ) {
						for ( String mark : markList ) {
							setStringValue( cell, StringUtils.replace( value, mark, "" ));
						}
					}
				}
			}
		}
	}

	/**
	 * セルを取得する
	 * @param row HSSFRow
	 * @param index
	 * @return セル
	 */
	private HSSFCell getCell( HSSFRow row, int index ) {

		HSSFCell cell = row.getCell( index );
		if ( null == cell ) {
			cell = row.createCell( index );
		}

		return cell;
	}

	/**
	 * 存在するシートを取得する
	 * @param workbook HSSFWorkbook
	 * @return シート
	 */
	private HSSFSheet getExistSheet( HSSFWorkbook workbook, String sheetName ) {

		HSSFSheet sheet = workbook.getSheet( sheetName );
		if ( null == sheet ) {
			throw new TriSystemException(DcmMessageId.DCM004007F , sheetName);
	}

		return sheet;
	}

	/**
	 * 行を取得する
	 * @param sheet HSSFSheet
	 * @param index
	 * @return 行
	 */
	private HSSFRow getRow( HSSFWorkbook workbook, HSSFSheet sheet, int index ) {

		HSSFRow row = sheet.getRow( index + getTotalShiftRowsNumber( workbook, sheet ));
		if ( null == row ) {
			row = sheet.createRow( index + getTotalShiftRowsNumber( workbook, sheet ));
		}

		return row;
	}
	/**
	 * シートがテンプレートに対して何行シフトしたかの値を取得する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @return シフトした行数
	 */
	private int getTotalShiftRowsNumber( HSSFWorkbook workbook, HSSFSheet sheet ) {

		String sheetName = workbook.getSheetName( workbook.getSheetIndex( sheet ));

		Integer val = this.totalShiftRowsNumberMap.get( sheetName );
		if ( null == val ) {
			val = new Integer(0);
			this.totalShiftRowsNumberMap.put( sheetName, val );
		}

		return val.intValue();
	}

	/**
	 * シートがテンプレートに対して何行シフトしたかの値を加算する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param addVal 加算する値
	 */
	private void addTotalShiftRowsNumber( HSSFWorkbook workbook, HSSFSheet sheet, int addVal ) {

		int val = getTotalShiftRowsNumber( workbook, sheet );
		this.totalShiftRowsNumberMap.put(
			workbook.getSheetName( workbook.getSheetIndex( sheet )), new Integer( val + addVal ));
	}

	/**
	 * 同一シートでカテゴリのデータが何回ループしたかの値を取得する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param categoryName カテゴリ名
	 * @return ループした行数
	 */
	private int getRowLoopCount( HSSFWorkbook workbook, HSSFSheet sheet, String categoryName ) {

		String sheetName = workbook.getSheetName( workbook.getSheetIndex( sheet ));

		Map<String, Integer> countMap = this.rowLoopCountMap.get( sheetName );
		if ( null == countMap ) {
			countMap = new HashMap<String, Integer>();
			countMap.put( categoryName, new Integer(0) );

			this.rowLoopCountMap.put( sheetName, countMap );
		}


		Integer val = countMap.get( categoryName );
		if ( null == val ) {
			val = new Integer(0);
			countMap.put( categoryName, val );
		}

		return countMap.get( categoryName ).intValue();
	}

	/**
	 * 同一シートでカテゴリのデータが何回ループしたかの値を加算する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param categoryName カテゴリ名
	 * @param addVal 加算する値
	 */
	private void addRowLoopCount(
			HSSFWorkbook workbook, HSSFSheet sheet, String categoryName, int addVal ) {

		int val = getRowLoopCount( workbook, sheet, categoryName );

		Map<String, Integer> countMap =
			this.rowLoopCountMap.get( workbook.getSheetName( workbook.getSheetIndex( sheet )));
		countMap.put( categoryName, new Integer( val + addVal ));
	}

	/**
	 * シートをまたがってカテゴリのデータが何回ループしたかの値を取得する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param categoryName カテゴリ名
	 * @return ループした行数
	 */
	private int getSheetLoopCount( HSSFWorkbook workbook, HSSFSheet sheet, String categoryName ) {

		String sheetName = workbook.getSheetName( workbook.getSheetIndex( sheet ));

		Map<String, Integer> countMap = this.sheetLoopCountMap.get( sheetName );
		if ( null == countMap ) {
			countMap = new HashMap<String, Integer>();
			countMap.put( categoryName, new Integer(0) );

			this.sheetLoopCountMap.put( sheetName, countMap );
		}

		Integer val = countMap.get( categoryName );
		if ( null == val ) {
			val = new Integer(0);
			countMap.put( categoryName, val );
		}

		return countMap.get( categoryName ).intValue();
	}

	/**
	 * シートをまたがってカテゴリのデータが何回ループしたかの値を加算する
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param categoryName カテゴリ名
	 * @param addVal 加算する値
	 */
	private void addSheetLoopCount(
			HSSFWorkbook workbook, HSSFSheet sheet, String categoryName, int addVal ) {

		int val = getSheetLoopCount( workbook, sheet, categoryName );

		Map<String, Integer> countMap =
			this.sheetLoopCountMap.get( workbook.getSheetName( workbook.getSheetIndex( sheet )));
		countMap.put( categoryName, new Integer( val + addVal ));
	}

	/**
	 * 値のタイプから値を加工する
	 * @param itemInfo 項目マッピング情報
	 * @param value 値(加工前)
	 * @return 値(加工後)
	 */
	private String convertValue( ItemMappingInfo itemInfo, String value, boolean isLegacy ) {

		if ( itemInfo.isCodeItem() ) {
			value = getCodeValue( itemInfo, value, isLegacy );
		}

		if ( !TriStringUtils.isEmpty( value ) ) {

			if ( !TriStringUtils.isEmpty( itemInfo.getPrefix() )) {
				value = itemInfo.getPrefix() + value;
			}

			if ( !TriStringUtils.isEmpty( itemInfo.getSuffix() )) {
				value = value + itemInfo.getSuffix();
			}
		}

		if ( TriStringUtils.isEmpty( value ) ) {
			value = itemInfo.getNullValue();
		}

		return value;
	}


	/**
	 * コード値を取得する
	 * @param itemInfo 項目マッピング情報
	 * @return コード値
	 */
	private String getCodeValue( ItemMappingInfo itemInfo, String codeId, boolean isLegacy) {

		String value = null;

		String designName = itemInfo.getCodeDesignName();

		try {
			IDesignBeanId.Sheet enumSheet = IDesignBeanId.Sheet.enumValue( designName );
			if ( null == enumSheet ) {
				LogHandler.info( log , TriLogMessage.LDCM0001 );
				return value;
			}

			if ( isLegacy ){
				@SuppressWarnings("deprecation")
				Map<String,String> map = sheet.getKeyMap( enumSheet, itemInfo.getCodeBeanId() );
				value = map.get( codeId );
			}else {
				value = this.getValueFromProperties(enumSheet, itemInfo.getCodeBeanId(), codeId);

			}

//			Field field = DesignDefineUtil.class.getDeclaredField( designName );
//			field.setAccessible( true );
//
//			DesignSheet sheet = (DesignSheet)field.get( null );
//			if ( null == sheet ) {
//				DesignDefineUtil.init();
//			}
//			if ( null != sheet ){
//				Map<String,String> map = sheet.getKeyMap( itemInfo.getCodeBeanId() );
//				value = map.get( codeId );
//			}else{
//				LogHandler.info( log , "DesignSheet is null" );
//			}

//		} catch ( NoSuchFieldException e ) {
//			throw new TriSystemException(DcmMessageId.DCM004008F , e , designName);
//		} catch ( IllegalAccessException e ) {
//			throw new TriSystemException(DcmMessageId.DCM004009F , e , designName);
		} catch ( ClassCastException e ) {
			throw new TriSystemException(DcmMessageId.DCM004009F , e , designName);
		}

		return value;

	}

	/**
	 * Ｎカテゴリ項目を別シートに展開する
	 * @param workbook エクセル
	 * @param categoryInfo マッピング情報
	 */
	private void spreadOtherSheet(
			HSSFWorkbook workbook, CategoryMappingInfo categoryInfo, boolean isLegacy, TimeZone timeZone ) throws ReachedMaxRowsException {

		String templateSheetName = categoryInfo.getTemplateSheetName();

		HSSFSheet hiddenTemplateSheet = getExistSheet( workbook, this.TEMPLATE_SHEET_PREFIX + templateSheetName );
		int count = getSheetLoopCount( workbook, hiddenTemplateSheet, categoryInfo.getCategoryName() );

		if ( 0 < count ) {

			String oldSheetName = categoryInfo.getSheetName();
			String newSheetName = null;

			HSSFSheet templateSheet = workbook.getSheet( templateSheetName );
			if ( null == templateSheet ) {
				newSheetName = templateSheetName;
			} else {
				newSheetName = templateSheetName + count;
			}

			HSSFSheet newSheet = workbook.cloneSheet( workbook.getSheetIndex( hiddenTemplateSheet ));
			workbook.setSheetName( workbook.getSheetIndex( newSheet ), newSheetName );


			// マッピング先のシート名を更新
			updateSheetName( oldSheetName, newSheetName );

			// 新しくシートを作成したのでヘッダ情報を反映する
			List<IEntity> headerEntityList = new ArrayList<IEntity>();
			for ( CategoryMappingInfo info : this.categoryInfoList ) {

				if ( info.isHeaderCategory() ) {
					headerEntityList.addAll( info.getFinishedEntityList() );
					info.getFinishedEntityList().clear();
				}
			}

			for ( IEntity entity : headerEntityList ) {
				parseEntity( entity, workbook, isLegacy, timeZone );
			}
		}

		addSheetLoopCount( workbook, hiddenTemplateSheet, categoryInfo.getCategoryName(), 1 );
	}

	/**
	 * Ｎカテゴリ項目を行(縦方向)展開する
	 * @param workbook エクセル
	 * @param categoryInfo マッピング情報
	 */
	private void spreadRow( HSSFWorkbook workbook, CategoryMappingInfo categoryInfo )
		throws ReachedMaxRowsException {

		/** レポートの最大行数 */
		final int maxRows = DesignSheetFactory.getDesignSheet().intValue( DcmDesignBeanId.report, DcmDesignEntryKeyByReport.reportMaxRows.name() ) - 1;

		HSSFSheet targetSheet = getExistSheet( workbook, categoryInfo.getSheetName() );
		int count = getRowLoopCount( workbook, targetSheet, categoryInfo.getCategoryName() );

		if ( 0 < count ) {

			int shiftRowsNumber		= categoryInfo.getEndRowIndex() - categoryInfo.getStartRowIndex() + 1;
			int startRowIndex		= categoryInfo.getStartRowIndex() + shiftRowsNumber;
			int endRowIndex			= categoryInfo.getEndRowIndex() + shiftRowsNumber;
			int startCellIndex		= categoryInfo.getStartCellIndex();
			int endCellIndex		= categoryInfo.getEndCellIndex();

			int totalShiftRowsNumber = getTotalShiftRowsNumber( workbook, targetSheet );

			HSSFSheet templateSheet	=
				getExistSheet( workbook, this.TEMPLATE_SHEET_PREFIX + categoryInfo.getTemplateSheetName() );


			int lastRows = templateSheet.getLastRowNum() + totalShiftRowsNumber + shiftRowsNumber;
			if ( maxRows <= lastRows ) {

				HSSFRow lastRow = targetSheet.getRow( maxRows );
				if ( null == lastRow ) {
					lastRow = targetSheet.createRow( maxRows );
				}
				HSSFCell lastCell = getCell( lastRow, 0 );
				throw new ReachedMaxRowsException( targetSheet.getSheetName(), lastCell );
			}


			targetSheet.shiftRows(
					startRowIndex + totalShiftRowsNumber, lastRows, shiftRowsNumber, true, false );

			// shiftRowsするとリセットされるので、スタイルをコピー
			for ( int rowIndex = startRowIndex; rowIndex <= endRowIndex; rowIndex++ ) {

				HSSFRow targetRow	= getRow( workbook, targetSheet, rowIndex );
				HSSFRow templateRow	= getRow( workbook, templateSheet, rowIndex - shiftRowsNumber );

				// 罫線が先勝ちのようなので？？？、後勝ちにする
				// => エクセル上での罫線の設定の仕方にコツがあたたた by inoue
//				HSSFRow preTargetRow	= null;
//				if ( rowIndex == startRowIndex ) {
//					preTargetRow	= getRow( workbook, targetSheet, rowIndex - 1 );
//				}

				targetRow.setHeightInPoints( templateRow.getHeightInPoints() );

				for ( int cellIndex = startCellIndex; cellIndex <= endCellIndex; cellIndex++ ) {

					HSSFCell targetCell		= getCell( targetRow, cellIndex );
					HSSFCell templateCell	= getCell( templateRow, cellIndex );

					targetCell.setCellValue	( templateCell.getRichStringCellValue() );
					targetCell.setCellStyle	( templateCell.getCellStyle() );

					// 罫線が先勝ちのようなので？？？、後勝ちにする
					// => HSSFCellStyle#cloneStyleFrom を多用すると、エクセルファイルが壊れます by inoue
//					if ( null != preTargetRow ) {
//
//						HSSFCell preTargetCell = getCell( preTargetRow, cellIndex );
//						HSSFCellStyle newStyle = workbook.createCellStyle();
//						newStyle.cloneStyleFrom( preTargetCell.getCellStyle() );
//
//						newStyle.setBorderBottom		( templateCell.getCellStyle().getBorderTop() );
//						newStyle.setBottomBorderColor	( templateCell.getCellStyle().getTopBorderColor() );
//
//						preTargetCell.setCellStyle( newStyle );
//					}
				}
			}

			// shiftRowsするとリセットされるので、セルの結合情報のコピー
			int mergedNum = templateSheet.getNumMergedRegions();
			for ( int mergedIndex = 0; mergedIndex < mergedNum; mergedIndex++ ) {

				CellRangeAddress cra = templateSheet.getMergedRegion( mergedIndex );

				// 結合情報が、shiftRows以降の場合
				if ( categoryInfo.getStartRowIndex() <= cra.getFirstRow() &&
						startCellIndex <= cra.getFirstColumn() ) {

					CellRangeAddress newCra = new CellRangeAddress(
							cra.getFirstRow() + totalShiftRowsNumber + shiftRowsNumber,
							cra.getLastRow() + totalShiftRowsNumber + shiftRowsNumber,
							cra.getFirstColumn(),
							cra.getLastColumn() );

					targetSheet.addMergedRegion( newCra );

				}
			}

//			if ( targetSheet.getSheetName().startsWith( "ビルドパッケージレポート" )) {
//				this.writeReport( new File( "C:\\temp\\report", totalShiftRowsNumber + "_" + categoryInfo.getCategoryName() + "_" + count + ".xls" ) , workbook );
//			}

			addTotalShiftRowsNumber	( workbook, targetSheet, shiftRowsNumber );
			resetRowLoopCount		( workbook, targetSheet, categoryInfo );

		}

		addRowLoopCount( workbook, targetSheet, categoryInfo.getCategoryName(), 1 );
	}

	/**
	 * 指定されたカテゴリの展開範囲に、展開範囲が完全に含まれるカテゴリのループ回数をリセットする
	 * @param workbook ワークブック
	 * @param sheet 対象のシート
	 * @param targetInfo 対象カテゴリ
	 */
	private void resetRowLoopCount(
			HSSFWorkbook workbook, HSSFSheet sheet,  CategoryMappingInfo targetInfo ) {

		String sheetName = workbook.getSheetName( workbook.getSheetIndex( sheet ));

		Map<String, Integer> countMap = this.rowLoopCountMap.get( sheetName );
		if ( null != countMap ) {

			// 同じシートに展開で
			for ( CategoryMappingInfo info : this.categoryInfoList ) {
				if ( info.isDoOutput() &&
						!targetInfo.getCategoryName().equals( info.getCategoryName() ) &&
						targetInfo.getSheetName().equals( info.getSheetName() ) &&
						info.isListCategory() ) {

					// 展開範囲が完全に含まれているカテゴリをリセットする
					if ( isContain( targetInfo, info )) {
						countMap.put( info.getCategoryName(), new Integer(0) );
					}
				}
			}
		}
	}

	/**
	 * 子のカテゴリ情報の展開範囲が、親のそれに完全に含まれているかをチェックする
	 * @param parentInfo 親カテゴリ情報
	 * @param subInfo 子カテゴリ情報
	 * @return 含まれていればtrue、そうれでなければfalse
	 */
	private boolean isContain( CategoryMappingInfo parentInfo, CategoryMappingInfo subInfo ) {

		if (	parentInfo.getStartRowIndex() <= subInfo.getStartRowIndex() &&
				parentInfo.getStartCellIndex() <= subInfo.getStartCellIndex() &&
				subInfo.getEndRowIndex() <= parentInfo.getEndRowIndex() &&
				subInfo.getEndCellIndex() <= parentInfo.getEndCellIndex() ) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * シート名を更新する
	 * @param oldSheetName 古いシート名
	 * @param newSheetName 新しいシート名
	 */
	private void updateSheetName( String oldSheetName, String newSheetName ) {

		for ( CategoryMappingInfo categoryInfo : this.categoryInfoList ) {

			if ( categoryInfo.isDoOutput() ) {
				if ( oldSheetName.equals( categoryInfo.getSheetName() )) {
					categoryInfo.setSheetName( newSheetName );
				}
			}
		}
	}

	/**
	 * フィールド情報に該当するエンティティリストを取得する。
	 * @param entity エンティティ
	 * @param valueList フィールドが文字列であれば、その値を格納する。
	 * @return フィールドがエンティティであれば、そのリスト
	 */
	private List<IEntity> setValueList( IEntity entity, ItemMappingInfo mappingInfo, List<String> valueList, boolean isLegacy, TimeZone timeZone ) {

		List<IEntity> entityList	= new ArrayList<IEntity>();
		entityList.add( entity );

		List<IEntity> tmpEntityList	= new ArrayList<IEntity>();

		for ( FieldInfo fieldInfo : mappingInfo.getItemFieldInfoList() ) {

			for ( IEntity tmpEntity : entityList ) {

				Field field = null;
				for ( @SuppressWarnings("rawtypes")
					Class clazz = tmpEntity.getClass(); clazz != Object.class; clazz = clazz.getSuperclass() ) {

					try {
						field = clazz.getDeclaredField( fieldInfo.getName() );
						field.setAccessible( true );

						if ( !fieldInfo.isCategory() && !fieldInfo.isList() ) {

							// 文字列

							String value ;
							if (field.get(tmpEntity) instanceof Timestamp ) {
								if( timeZone != null ){
									value = TriDateUtils.convertViewDateFormat( (Timestamp)field.get( tmpEntity ), timeZone );	
								}else {
									value = TriDateUtils.convertViewDateFormat( (Timestamp)field.get( tmpEntity ) );
								}
							}else if (field.get(tmpEntity) instanceof Integer ) {
								value = field.get( tmpEntity ).toString();
							}else{
								value = (String)field.get( tmpEntity );
							}
							valueList.add( convertValue( mappingInfo, value, isLegacy ));

						} else if ( fieldInfo.isCategory() && !fieldInfo.isList() ) {

							// エンティティ
							tmpEntityList.add( (IEntity)field.get( tmpEntity ) );

						} else if ( !fieldInfo.isCategory() && fieldInfo.isList() ) {

							if ( field.get( tmpEntity ) instanceof String[] ) {

								// 文字配列
								for ( String value : (String[])field.get( tmpEntity ) ) {
									valueList.add( convertValue( mappingInfo, value, isLegacy ));
								}

							} else if ( field.get( tmpEntity ) instanceof List ) {

								// 文字列のリスト
								@SuppressWarnings("unchecked")
								List<String> values = (List<String>)field.get( tmpEntity );
								for ( String value : values ) {
									valueList.add( convertValue( mappingInfo, value, isLegacy ));
								}
							}

						} else {

							if ( field.get( tmpEntity ) instanceof IEntity[] ) {

								// エンティティ配列
								tmpEntityList.addAll( FluentList.from((IEntity[])field.get( tmpEntity )).asList());

							} else if ( field.get( tmpEntity ) instanceof List ) {

								// エンティティのリスト
								@SuppressWarnings("unchecked")
								List<IEntity> tmpList = (List<IEntity>)field.get( tmpEntity );
								tmpEntityList.addAll( tmpList );
							}
						}

					} catch ( NoSuchFieldException e ) {
						LogHandler.info( log , e.toString() );
					} catch ( IllegalAccessException e ) {
						throw new TriSystemException(DcmMessageId.DCM004010F , e , mappingInfo.getCategoryName() , mappingInfo.getItemName() , fieldInfo.getName());
					} catch ( ClassCastException e ) {
						throw new TriSystemException(DcmMessageId.DCM004010F , e , mappingInfo.getCategoryName() , mappingInfo.getItemName() , fieldInfo.getName());
					}
				}

				if ( null == field ) {
					throw new TriSystemException(DcmMessageId.DCM004011F ,  mappingInfo.getCategoryName() , mappingInfo.getItemName() , fieldInfo.getName());
				}
			}

			entityList		= tmpEntityList;
			tmpEntityList	= new ArrayList<IEntity>();
		}

		return entityList;
	}


	/**
	 * エンティティをレポート形式にパースする
	 * @param entity エンティティ
	 * @param workbook エクセル
	 * @return パースした内容の文字列化
	 */
	protected String parseEntity( IEntity entity, HSSFWorkbook workbook, boolean isLegacy, TimeZone timeZone ) throws TriSystemException {

		/** レポートの最大行数 */
		final int maxRows = DesignSheetFactory.getDesignSheet().intValue( DcmDesignBeanId.report, DcmDesignEntryKeyByReport.reportMaxRows.name() ) - 1;

		try {
			CategoryMappingInfo categoryInfo = getCategoryMappingInfo( entity );
			if ( null == categoryInfo ) return "";

			if ( categoryInfo.isListCategory() &&
					ListSpreadMothod.Sheet.equals( categoryInfo.getListSpreadMothod() )) {
				// シートのコピー
				spreadOtherSheet( workbook, categoryInfo, isLegacy, timeZone);
			}
			if ( this.reachedMaxRowsSheetNameList.contains( categoryInfo.getSheetName() )) return "";

			if ( categoryInfo.isListCategory() &&
					ListSpreadMothod.Vertical.equals( categoryInfo.getListSpreadMothod() )) {
				// 行(縦方向)展開
				spreadRow( workbook, categoryInfo );
			}

			HSSFSheet sheet = getExistSheet( workbook, categoryInfo.getSheetName() );

			// ヘッダは１シート１度しか作成しない
			String sheetName = workbook.getSheetName( workbook.getSheetIndex( sheet ));
			if ( categoryInfo.isHeaderCategory() ) {

				List<IEntity> createHeaderEntityList = this.isHeaderCreatedMap.get( sheetName );
				if ( null != createHeaderEntityList && createHeaderEntityList.contains( entity )) {
					return "";
				}
			}

			for ( ItemMappingInfo itemInfo : categoryInfo.getItemMappingInfoList() ) {

				List<String> valueList		= new ArrayList<String>();
				List<IEntity> entityList	= setValueList( entity, itemInfo, valueList, isLegacy, timeZone );

				// 完全にカテゴリ項目（エンティティ）
				if ( 0 == valueList.size() ) {

					for ( IEntity value : entityList ) {
						parseEntity( value, workbook, isLegacy, timeZone );
					}

				} else if ( 1 == valueList.size() ) {

					HSSFRow row		= getRow( workbook, sheet, itemInfo.getRowIndex() );
					HSSFCell cell	= getCell( row, itemInfo.getCellIndex() );

					if ( maxRows <= cell.getRowIndex() ) {
						throw new ReachedMaxRowsException( sheet.getSheetName(), cell );
					}
					setText( valueList.get( 0 ), cell, itemInfo );

					replaceSheetName( workbook, sheet, sheetName, valueList.get( 0 ), itemInfo );

				} else {

					HSSFRow row		= getRow( workbook, sheet, itemInfo.getRowIndex() );
					HSSFCell cell	= getCell( row, itemInfo.getCellIndex() );

					if ( maxRows <= cell.getRowIndex() ) {
						throw new ReachedMaxRowsException( sheet.getSheetName(), cell );
					}
                    String convertText = convertListText( valueList, workbook, cell, itemInfo );

                    setText( convertText, cell, itemInfo );

                    replaceSheetName( workbook, sheet, sheetName, convertText, itemInfo );
				}
			}

			// ヘッダ作成済み情報設定
			if ( categoryInfo.isHeaderCategory() ) {

				List<IEntity> createHeaderEntityList = this.isHeaderCreatedMap.get( sheetName );
				if ( null == createHeaderEntityList ) {
					createHeaderEntityList = new ArrayList<IEntity>();
				}
				createHeaderEntityList.add( entity );

				this.isHeaderCreatedMap.put( sheetName, createHeaderEntityList );
			}
		} catch ( ReachedMaxRowsException e ) {
			this.reachedMaxRowsSheetNameList.add( e.getSheetName() );
			setStringValue( e.getCell(), "Reached Max Rows ---" );
		}

		return "";

	}

	/**
	 * シート名の項目マークを値で置換する。
	 *
	 * @param sheetName シート名
	 * @param value 値
	 * @param info
	 */
	private void replaceSheetName(
			HSSFWorkbook workbook, HSSFSheet sheet, String sheetName, String value, ItemMappingInfo info ) {

		List<String> markList = getItemMarkList( sheetName );
		if ( null != markList ) {

			for ( String mark : markList ) {
				if ( mark.equals( info.getIndexMark() )) {

					String newSheetName = StringUtils.replace( sheetName, mark, value );

					workbook.setSheetName	( workbook.getSheetIndex( sheet ), newSheetName );
					updateSheetName			( sheetName, newSheetName );

					updateSheetName( sheetName, newSheetName, this.sheetLoopCountMap );
					updateSheetName( sheetName, newSheetName, this.rowLoopCountMap );
					updateSheetName( sheetName, newSheetName, this.totalShiftRowsNumberMap );
					updateSheetName( sheetName, newSheetName, this.isHeaderCreatedMap );
					updateSheetName( sheetName, newSheetName, this.reachedMaxRowsSheetNameList );

				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private void updateSheetName( String oldSheetName, String newSheetName, @SuppressWarnings("rawtypes") Map sheetNameKeyMap ) {

		if ( sheetNameKeyMap.containsKey( oldSheetName )) {
			sheetNameKeyMap.put		( newSheetName, sheetNameKeyMap.get( oldSheetName ));
			sheetNameKeyMap.remove	( oldSheetName );
		}
	}
	private void updateSheetName( String oldSheetName, String newSheetName, List<String> sheetNameList ) {

		if ( sheetNameList.contains( oldSheetName )) {
			sheetNameList.add	( newSheetName );
			sheetNameList.remove( oldSheetName );
		}
	}

	/**
	 * 値を指定されたセルにマッピングする
	 * @param value 値
	 * @param cell 対象のセル
	 * @param itemInfo 項目マッピング情報
	 */
	private void setText( String value, HSSFCell cell, ItemMappingInfo itemInfo ) {

		String cellValue		= getStringValue( cell );
		String replacedValue	= StringUtils.replace( cellValue, itemInfo.getIndexMark(), value );
		replacedValue = this.trimCellMaxString( replacedValue ) ;

		//HSSFCellStyle style = cell.getCellStyle();

		try {

			if ( itemInfo.isDateItem() ) {
				cell.setCellValue	( TriDateUtils.getDefaultDateFormat().parse( replacedValue ));
			} else if ( itemInfo.isNumericItem() ) {
				cell.setCellValue	( Integer.parseInt( replacedValue ));
			} else {
				// とりあえず文字列
				cell.setCellType	( HSSFCell.CELL_TYPE_STRING );
				cell.setCellValue	( new HSSFRichTextString( replacedValue ) );
			}
		} catch ( ParseException e ) {
			// 何か起きたら無理やり文字列で設定する
			cell.setCellType	( HSSFCell.CELL_TYPE_STRING );
			cell.setCellValue	( new HSSFRichTextString( replacedValue ) );
		} catch ( NumberFormatException e ) {
			// 何か起きたら無理やり文字列で設定する
			cell.setCellType	( HSSFCell.CELL_TYPE_STRING );
			cell.setCellValue	( new HSSFRichTextString( replacedValue ) );
		} finally {
			// 値をセットすると罫線が消えてしまうことがあるので、値のセット後にスタイルをセットしなおす
			//cell.setCellStyle( style );
		}
	}

	/**
	 * 値のリストを指定されたセル内で、指定されたセパレータで接続してマッピングする
	 * @param valueList 値のリスト
	 * @param workbook エクセルデータ
	 * @param cell 対象のセル
	 * @param itemInfo 項目マッピング情報
	 */
	private String convertListText(
			List<String> valueList, HSSFWorkbook workbook, HSSFCell cell, ItemMappingInfo itemInfo ) {

		String separator = itemInfo.getJoinString();
		if ( null == separator ) separator = "";

		StringBuilder bf = new StringBuilder();

		for ( String value : valueList ) {

			if ( 0 < bf.length() ) {
				bf.append( separator );
			}
			bf.append( value );

		}
        return bf.toString();
	}

	/**
	 * 指定エンティティ名に該当するカテゴリマッピング情報のリストを取得する
	 * @param entityName エンティティ名
	 * @return マッピング情報のリスト
	 * @throws TriSystemException
	 */
	private List<CategoryMappingInfo> getCategoryMappingInfoList( IEntity entity ) throws TriSystemException {

		List<CategoryMappingInfo> tmpInfoList = getTempCategoryMappingInfoList( entity );
		if ( 0 == tmpInfoList.size() ) {
			throw new TriSystemException(DcmMessageId.DCM004012F , entity.getClass().getSimpleName());
		}

		List<CategoryMappingInfo> retInfoList = new ArrayList<CategoryMappingInfo>();
		for ( CategoryMappingInfo info : tmpInfoList ) {
			if ( info.isDoOutput() ) {
				retInfoList.add( info );
			}
		}

		return retInfoList;
	}

	/**
	 * 指定エンティティ名に該当するカテゴリマッピング情報を取得する
	 * @param entityName エンティティ名
	 * @return マッピング情報
	 * @throws TriSystemException
	 */
	private CategoryMappingInfo getCategoryMappingInfo( IEntity entity ) throws TriSystemException {

		CategoryMappingInfo retInfo = null;

		List<CategoryMappingInfo> tmpInfoList = getTempCategoryMappingInfoList( entity );
		if ( 0 == tmpInfoList.size() ) {
			throw new TriSystemException(DcmMessageId.DCM004012F , entity.getClass().getSimpleName());
		}


		for ( CategoryMappingInfo tmpInfo : tmpInfoList ) {

			if ( !tmpInfo.isDoOutput() ) continue;

			if ( !tmpInfo.getFinishedEntityList().contains( entity )) {
				tmpInfo.getFinishedEntityList().add( entity );
				retInfo = tmpInfo;
				break;
			}
		}

		return retInfo;
	}

	/**
	 * 指定エンティティ名に該当するカテゴリマッピング情報のリストを取得する
	 * @param entityName エンティティ名
	 * @return マッピング情報のリスト
	 * @throws TriSystemException
	 */
	private List<CategoryMappingInfo> getTempCategoryMappingInfoList( IEntity entity ) throws TriSystemException {

		@SuppressWarnings("rawtypes")
		Class entityClass = entity.getClass();

		List<CategoryMappingInfo> tmpInfoList = new ArrayList<CategoryMappingInfo>();

		for ( CategoryMappingInfo info : this.categoryInfoList ) {

			if ( entityClass.getSimpleName().equals( info.getCategoryEntityName() )) {

				String infoIdentifiedId		= info.getIdentifierId();
				String entityIdentifiedId	= null;
				if ( entity instanceof IReportInfo ) {
					entityIdentifiedId = ((IReportInfo)entity).getIdentifierId();
				}

				if ( TriStringUtils.isEmpty( infoIdentifiedId ) &&
						TriStringUtils.isEmpty( entityIdentifiedId )) {
					tmpInfoList.add( info );
				} else if ( !TriStringUtils.isEmpty( infoIdentifiedId ) &&
						!TriStringUtils.isEmpty( entityIdentifiedId )) {

					if ( infoIdentifiedId.equals( entityIdentifiedId )) {
						tmpInfoList.add( info );
					}
				}
			}
		}

		return tmpInfoList;
	}

	/**
	 * カテゴリマッピング情報を設定する
	 * @param outputInfoWB 出力情報エクセル
	 * @param reportWB レポートエクセル
	 * @param categoryInfoList カテゴリ固定情報
	 * @param itemInfoList 項目固定情報
	 * @param spreadMethodInfoMap リストカテゴリ／項目の展開表示情報
	 * @param sheetIndex 項目位置情報
	 * @return カテゴリマッピング情報のリスト
	 */
	private void setReportOutputInfo(
			HSSFWorkbook outputInfoWB, HSSFWorkbook reportWB,
			List<CategoryOutputInfo> categoryInfoList,
			List<ItemOutputInfo> itemInfoList,
			Map<String,String> spreadMethodInfoMap,
			Map<String,Map<String,ItemIndex>> sheetIndexMap ) throws TriSystemException {

		String sheetName = sheet.getValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.SHEET_NAME );

		HSSFSheet reportInfoSheet = getExistSheet( outputInfoWB, sheetName );
		if ( null == reportInfoSheet ) {
			throw new TriSystemException(DcmMessageId.DCM004013F , sheetName);
		}

		int contentsStartRowIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.CONTENTS_START_ROW );

		int categoryNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.CATEGORY_NAME_INDEX );

		int itemNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_NAME_INDEX );

		int doOutputIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.DO_OUTPUT_INDEX );

		int outputSheetNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.OUTPUT_SHEET_NAME_INDEX );

		int isHeaderIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.IS_HEADER_CATEGORY_INDEX );

		int spreadMethodIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.CATEGORY_LIST_SPREAD_METHOD_INDEX );

		int spreadNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.CATEGORY_LIST_SPREAD_NAME_INDEX );

		int itemOutputMarkIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_OUTPUT_MARK_INDEX );

		int joinStringIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_LIST_JOIN_STRING_INDEX );

		int prefixIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_PREFIX_INDEX );

		int suffixIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_SUFFIX_INDEX );

		int nullValueIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.ITEM_NULL_VALUE_INDEX );

		int isDateIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.IS_DATE_INDEX );

		int isNumericIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigReportOutputInfo, ReportOutputInfo.IS_NUMERIC_INDEX );

		CategoryMappingInfo categoryMappingInfo	= null;
		CategoryOutputInfo categoryInfo			= null;
		String targetCategoryName				= null;

		for ( int rowIndex = contentsStartRowIndex; rowIndex <= reportInfoSheet.getLastRowNum(); rowIndex++ ) {

			HSSFRow row = getRow( outputInfoWB, reportInfoSheet, rowIndex );

			String doOutputString = getStringValue( getCell( row, doOutputIndex ) );
			if ( TriStringUtils.isEmpty( doOutputString ) ) continue;

			boolean doOutput	= convertBooleanValue( doOutputString );
			String categoryName	= getStringValue( getCell( row, categoryNameIndex ));

			if ( !TriStringUtils.isEmpty( categoryName )) {

				targetCategoryName = categoryName;

				categoryInfo		= getCategoryOutputInfo( categoryInfoList, categoryName );
				categoryMappingInfo	= new CategoryMappingInfo();
				categoryMappingInfo.setNo( this.categoryInfoList.size() );

				this.categoryInfoList.add( categoryMappingInfo );

				categoryMappingInfo.setCategoryName			( categoryName );
				categoryMappingInfo.setCategoryEntityName	( categoryInfo.getEntityName() );
				categoryMappingInfo.setIdentifierId			( categoryInfo.getIdentifierId() );
				categoryMappingInfo.setDoOutput				( doOutput );

				if ( doOutput ) {

					String outputSheetName = getStringValue( getCell( row, outputSheetNameIndex ) );
					categoryMappingInfo.setSheetName		( outputSheetName );
					categoryMappingInfo.setTemplateSheetName( outputSheetName );

					categoryMappingInfo.setHeaderCategory	(
							convertBooleanValue( getStringValue( getCell( row, isHeaderIndex ) )));
					categoryMappingInfo.setListCategory		(
							convertBooleanValue( categoryInfo.getIsList() ));

					if ( categoryMappingInfo.isListCategory() ) {

						String spreadMethodView			= getStringValue( getCell( row, spreadMethodIndex ) );
						String spreadMethodValue		= spreadMethodInfoMap.get( spreadMethodView );
						if ( null == spreadMethodValue ) {
							throw new TriSystemException(DcmMessageId.DCM004014F , spreadMethodView);
						}
						ListSpreadMothod spreadMethod 	= ListSpreadMothod.getValue( spreadMethodValue );
						if ( null == spreadMethod ) {
							throw new TriSystemException(DcmMessageId.DCM004015F , spreadMethodValue);
						}
						categoryMappingInfo.setListSpreadMothod( spreadMethod );

						if ( spreadMethod.equals( ListSpreadMothod.Vertical )) {

							String spreadName = getStringValue( getCell( row, spreadNameIndex ) );
							if ( TriStringUtils.isEmpty( spreadName )) {
								throw new TriSystemException(DcmMessageId.DCM004016F , categoryName);
							}

							HSSFName name = reportWB.getName( spreadName );
							if ( null == name ) {
								throw new TriSystemException(DcmMessageId.DCM004017F , spreadName);
							}
							AreaReference ref = new AreaReference( name.getRefersToFormula() );

							categoryMappingInfo.setStartRowIndex	( ref.getFirstCell().getRow() );
							categoryMappingInfo.setStartCellIndex	( ref.getFirstCell().getCol() );
							categoryMappingInfo.setEndRowIndex		( ref.getLastCell().getRow() );
							categoryMappingInfo.setEndCellIndex		( ref.getLastCell().getCol() );
						}
					}
				}
			}



			if ( !doOutput ) continue;

			String itemName = getStringValue( getCell( row, itemNameIndex ) );

			if ( !TriStringUtils.isEmpty( itemName ) ) {

				if ( null == categoryMappingInfo ) {
					throw new TriSystemException(DcmMessageId.DCM004018F , itemName);
				}

				if ( !categoryMappingInfo.isDoOutput() ) continue;

				ItemOutputInfo itemInfo	=
					getItemOutputInfo( itemInfoList, targetCategoryName, itemName );
				if ( null == itemInfo ) {
					throw new TriSystemException(DcmMessageId.DCM004019F , targetCategoryName , itemName);
				}

				ItemMappingInfo itemMappingInfo	= new ItemMappingInfo();

				itemMappingInfo.setCategoryName	( itemInfo.getCategoryName() );
				itemMappingInfo.setItemName		( itemInfo.getItemName() );

				List<FieldInfo> fieldInfoList = itemInfo.getFieldInfoList();
				FieldInfo lastFieldInfo	= fieldInfoList.get( fieldInfoList.size() - 1 );
				itemMappingInfo.setItemFieldInfoList( fieldInfoList );

				if ( !lastFieldInfo.isCategory() ) {

					Map<String,ItemIndex> itemIndexMap = sheetIndexMap.get( categoryMappingInfo.getSheetName() );
					if ( null == itemIndexMap ) {
						throw new TriSystemException(DcmMessageId.DCM004020F , categoryMappingInfo.getSheetName());
					}

					String indexMark = getStringValue( getCell( row, itemOutputMarkIndex ));
					if ( TriStringUtils.isEmpty( indexMark )) {
						throw new TriSystemException(DcmMessageId.DCM004021F , targetCategoryName , itemName);
					}

					ItemIndex itemIndex	= itemIndexMap.get( indexMark );
					if ( null == itemIndex ) {
						throw new TriSystemException(DcmMessageId.DCM004022F , targetCategoryName , itemName);
					}

					itemMappingInfo.setIndexMark	( indexMark );
					itemMappingInfo.setRowIndex		( itemIndex.getRowIndex() );
					itemMappingInfo.setCellIndex	( itemIndex.getCellIndex() );

					itemMappingInfo.setCodeItem	( convertBooleanValue( itemInfo.isCode() ));
					if ( itemMappingInfo.isCodeItem() ) {
						itemMappingInfo.setCodeDesignName	( itemInfo.getCodeSheetName() );
						itemMappingInfo.setCodeBeanId		( itemInfo.getCodeBeanId() );
					}

					if ( lastFieldInfo.isList() ) {
						itemMappingInfo.setJoinString( getStringValue( getCell( row, joinStringIndex ) ));
					}

					itemMappingInfo.setPrefix		( getStringValue( getCell( row, prefixIndex ) ));
					itemMappingInfo.setSuffix		( getStringValue( getCell( row, suffixIndex ) ));
					itemMappingInfo.setNullValue	( getStringValue( getCell( row, nullValueIndex ) ));
					itemMappingInfo.setDateItem		(
							convertBooleanValue( getStringValue( getCell( row, isDateIndex ) )));
					itemMappingInfo.setNumericItem	(
							convertBooleanValue( getStringValue( getCell( row, isNumericIndex ) )));
				}

				categoryMappingInfo.addItemMappingInfo( itemMappingInfo );

				// 他のカテゴリとのリンク情報を登録
				categoryMappingInfo.getKeyMappingInfoListMap().putAll(
						getLinkCategoryItemMappingInfoListMap( itemInfoList, targetCategoryName ));
			}
		}

	}

	/**
	 * カテゴリ名に該当するカテゴリ固定情報を、カテゴリ固定情報のリストから取得する。
	 * @param categoryInfoList カテゴリ固定情報のリスト
	 * @param categoryName カテゴリ名
	 * @return カテゴリ固定情報
	 * @throws TriSystemException
	 */
	private CategoryOutputInfo getCategoryOutputInfo(
		List<CategoryOutputInfo> categoryInfoList, String categoryName ) throws TriSystemException {

		CategoryOutputInfo retCategoryInfo = null;

		for ( CategoryOutputInfo categoryInfo : categoryInfoList ) {

			if ( categoryName.equals( categoryInfo.getCategoryName() )) {
				retCategoryInfo = categoryInfo;
				break;
			}
		}

		if ( null == retCategoryInfo ) {
			throw new TriSystemException(DcmMessageId.DCM004023F , categoryName);
		}

		return retCategoryInfo;
	}

	/**
	 * カテゴリ名に該当する項目固定情報のうち、カテゴリとのリンク項目となるリストを取得する。
	 * @param itemInfoList 項目固定情報のリスト
	 * @param categoryName カテゴリ名
	 * @return カテゴリリンク項目固定情報のリスト
	 * @throws TriSystemException
	 */
	private Map<String, List<ItemMappingInfo>> getLinkCategoryItemMappingInfoListMap(
		List<ItemOutputInfo> itemInfoList, String categoryName ) throws TriSystemException {

		Map<String, List<ItemMappingInfo>> categoryLinkMap = new LinkedHashMap<String, List<ItemMappingInfo>>();

		for ( ItemOutputInfo itemInfo : itemInfoList ) {

			List<String> linkCategoryNameList = itemInfo.getLinkCategoryNameList();

			if ( categoryName.equals( itemInfo.getCategoryName() ) && 0 < linkCategoryNameList.size() ) {


				ItemMappingInfo itemMappingInfo	= new ItemMappingInfo();

				itemMappingInfo.setCategoryName	( itemInfo.getCategoryName() );
				itemMappingInfo.setItemName		( itemInfo.getItemName() );
				itemMappingInfo.setItemFieldInfoList
												( itemInfo.getFieldInfoList() );

				for ( String linkCategoryName : linkCategoryNameList ) {

					List<ItemMappingInfo> infoList = categoryLinkMap.get( linkCategoryName );
					if ( null == infoList ) {
						infoList = new ArrayList<ItemMappingInfo>();
						categoryLinkMap.put( linkCategoryName, infoList );
					}
					infoList.add( itemMappingInfo );
				}
			}
		}

		return categoryLinkMap;
	}

	/**
	 * カテゴリ名、および項目名に該当する項目固定情報を、項目固定情報のリストから取得する。
	 * @param itemInfoList 項目固定情報のリスト
	 * @param categoryName カテゴリ名
	 * @param itemName 項目名
	 * @return 項目固定情報
	 * @throws TriSystemException
	 */
	private ItemOutputInfo getItemOutputInfo(
		List<ItemOutputInfo> itemInfoList, String categoryName, String itemName ) throws TriSystemException {

		ItemOutputInfo retItemInfo = null;

		for ( ItemOutputInfo itemInfo : itemInfoList ) {

			if ( categoryName.equals( itemInfo.getCategoryName() ) && itemName.equals( itemInfo.getItemName() )) {
				retItemInfo = itemInfo;
				break;
			}
		}

		return retItemInfo;
	}

	/**
	 * カテゴリ固定出力情報を取得する
	 * @param workbook 出力情報エクセル
	 * @return カテゴリ固定出力情報のリスト
	 */
	private List<CategoryOutputInfo> getCategoryOutputInfo( HSSFWorkbook workbook ) throws TriSystemException {

		String sheetName = sheet.getValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.SHEET_NAME );

		HSSFSheet categoryInfoSheet = getExistSheet( workbook, sheetName );
		if ( null == categoryInfoSheet ) {
			throw new TriSystemException(DcmMessageId.DCM004024F , sheetName);
		}

		int contentsStartRowIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.CONTENTS_START_ROW );

		int categoryNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.CATEGORY_NAME_INDEX );

		int entityNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.ENTITY_NAME_INDEX );

		int identifierIdIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.IDENTIFIER_ID_INDEX );

		int isListIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigCategoryInfo, CategoryOutputInfo.IS_LIST_INDEX );

		List<CategoryOutputInfo> infoList = new ArrayList<CategoryOutputInfo>();

		for ( int rowIndex = contentsStartRowIndex; rowIndex <= categoryInfoSheet.getLastRowNum(); rowIndex++ ) {

			HSSFRow row = getRow( workbook, categoryInfoSheet, rowIndex );

			String categoryName = getStringValue( getCell( row, categoryNameIndex ) );
			if ( TriStringUtils.isEmpty( categoryName )) continue;

			CategoryOutputInfo info = new CategoryOutputInfo();
			info.setCategoryName( categoryName );
			info.setEntityName	( getStringValue( getCell( row, entityNameIndex ) ));
			info.setIdentifierId( getStringValue( getCell( row, identifierIdIndex ) ));
			info.setIsList		( getStringValue( getCell( row, isListIndex ) ));

			infoList.add( info );
		}

		return infoList;
	}

	/**
	 * 項目固定出力情報を取得する
	 * @param workbook 出力情報エクセル
	 * @return 項目固定出力情報のリスト
	 */
	private List<ItemOutputInfo> getItemOutputInfo( HSSFWorkbook workbook ) throws TriSystemException {

		String sheetName = sheet.getValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.SHEET_NAME );

		HSSFSheet itemInfoSheet = getExistSheet( workbook, sheetName );
		if ( null == itemInfoSheet ) {
			throw new TriSystemException(DcmMessageId.DCM004025F , sheetName);
		}

		int contentsStartRowIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.CONTENTS_START_ROW );

		int categoryNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.CATEGORY_NAME_INDEX );

		int itemNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.ITEM_NAME_INDEX );

		int fieldNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.FIELD_NAME_INDEX );

		int isCodeIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.IS_CODE_INDEX );

		int codeSheetNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.CODE_SHEET_NAME_INDEX );

		int codeBeanIdIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.CODE_BEAN_ID_INDEX );

		int isListIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.IS_LIST_INDEX );

		int isCategoryIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.IS_CATEGORY_INDEX );

		int isLinkCategoryNameIndex = sheet.intValue(
				DcmDesignBeanId.reportConfigItemInfo, ItemOutputInfo.LINK_CATEGORY_NAME_INDEX );

		List<ItemOutputInfo> infoList = new ArrayList<ItemOutputInfo>();

		for ( int rowIndex = contentsStartRowIndex; rowIndex <= itemInfoSheet.getLastRowNum(); rowIndex++ ) {

			HSSFRow row = getRow( workbook, itemInfoSheet, rowIndex );

			String categoryName	= getStringValue( getCell( row, categoryNameIndex ) );
			if ( TriStringUtils.isEmpty( categoryName )) continue;

			String itemName		= getStringValue( getCell( row, itemNameIndex ) );

			boolean addFlag	= false;
			ItemOutputInfo info = getItemOutputInfo( infoList, categoryName, itemName );
			if ( null == info ) {

				addFlag	= true;
				info	= new ItemOutputInfo();

				info.setCategoryName	( categoryName );
				info.setItemName		( itemName );
			}

			info.setCode			( getStringValue( getCell( row, isCodeIndex ) ));
			info.setCodeSheetName	( getStringValue( getCell( row, codeSheetNameIndex ) ));
			info.setCodeBeanId		( getStringValue( getCell( row, codeBeanIdIndex ) ));

			String[] linkCategoryNames = getStringValue( getCell( row, isLinkCategoryNameIndex )).split( "," );
			if ( !TriStringUtils.isEmpty( linkCategoryNames )) {
				for ( String linkCategoryName : linkCategoryNames ) {
					if ( !TriStringUtils.isEmpty( linkCategoryName )) {
						info.getLinkCategoryNameList().add( linkCategoryName );
					}
				}
			}

			FieldInfo fieldInfo = new FieldInfo();
			fieldInfo.setName		( getStringValue( getCell( row, fieldNameIndex ) ));
			fieldInfo.setList		( convertBooleanValue( getStringValue( getCell( row, isListIndex ) )));
			fieldInfo.setCategory	( convertBooleanValue(getStringValue( getCell( row, isCategoryIndex ) )));

			info.getFieldInfoList().add( fieldInfo );

			if ( addFlag ) {
				infoList.add( info );
			}
		}

		return infoList;
	}

	/**
	 * リストカテゴリ／項目展開の表示情報を取得する
	 * @return リストカテゴリ／項目展開の表示情報
	 */
	private Map<String,String> getListSpreadMethodInfo() throws TriSystemException {

		Map<String,String> spreadMethodInfoMap	= new HashMap<String,String>();
		Map<String,String> listViewInfoMap		=
			sheet.getKeyMap( DcmDesignBeanId.reportConfigListViewInfo );

		for ( ListSpreadMothod define : ListSpreadMothod.values() ) {
			String spreadMethodView = listViewInfoMap.get( define.getValue() );
			spreadMethodInfoMap.put( spreadMethodView, define.getValue() );
		}

		return spreadMethodInfoMap;
	}

	/**
	 * セルの値を文字列にして取得する
	 * @param cell セル
	 * @return セルの値(文字列)
	 */
	private String getStringValue( HSSFCell cell ) {

		String ret = null;

		if ( HSSFCell.CELL_TYPE_STRING == cell.getCellType() ) {
			ret = cell.getRichStringCellValue().toString();
		} else if ( HSSFCell.CELL_TYPE_NUMERIC == cell.getCellType() ) {

			if ( HSSFDateUtil.isCellDateFormatted( cell )) {

				String dateString = cell.getDateCellValue().toString();
				if ( TriDateUtils.isDefaultDateFormat( dateString )) {
					ret = TriDateUtils.convertViewDateFormat( dateString );
				} else {
					ret = dateString;
				}

			} else {
				ret = Integer.toString( (int)(cell.getNumericCellValue()) );
			}

		} else if ( HSSFCell.CELL_TYPE_BOOLEAN == cell.getCellType() ) {
			ret = Boolean.toString( cell.getBooleanCellValue() );
		} else {
			// とりあえず文字列扱い
			ret = cell.getRichStringCellValue().toString();
		}

		return ret;
	}

	/**
	 * セルに文字列をセットする
	 * @param cell セル
	 * @param value 値
	 */
	private void setStringValue( HSSFCell cell, String value ) {

		HSSFCellStyle style = cell.getCellStyle();

		value = this.trimCellMaxString( value ) ;

		cell.setCellType	( HSSFCell.CELL_TYPE_STRING );
		cell.setCellValue	( new HSSFRichTextString( value ));
		cell.setCellStyle	( style );
	}

	/**
	 * 文字列をbooleanに変換する
	 * @param stringValue 文字列
	 * @return boolean値
	 */
	private boolean convertBooleanValue( String stringValue ) {

		boolean ret = false;

		if (! TriStringUtils.isEmpty( stringValue )) {

			ret = Boolean.valueOf( stringValue ).booleanValue();
		}

		return ret;
	}

	/**
	 * 出力情報データ設定処理
	 * @param reportWB レポートエクセル
	 */
	private void setOutputInfo( HSSFWorkbook reportWB ) throws TriSystemException {

		// 出力情報が記載されたエクセルを取得
		HSSFWorkbook outputInfoWB = getOutputInfoWorkBook();

		// 項目位置情報を取得
		Map<String,Map<String,ItemIndex>> sheetIndexMap = getItemIndexMap( reportWB );

		// カテゴリ固定情報を取得
		List<CategoryOutputInfo> categoryInfoList = getCategoryOutputInfo( outputInfoWB );

		// 項目固定情報を取得
		List<ItemOutputInfo> itemInfoList = getItemOutputInfo( outputInfoWB );

		// リストカテゴリ／項目の表示方法取得
		Map<String,String> spreadMethodInfoMap = getListSpreadMethodInfo();

		// マッピング情報取得
		setReportOutputInfo(	outputInfoWB, reportWB, categoryInfoList,
								itemInfoList, spreadMethodInfoMap, sheetIndexMap );
	}

	/**
	 * 項目インデックス情報を取得する
	 * @param reportWB レポートファイルエクセル
	 * @return シート名 - 項目マーク(${xxx}) - 項目インデックス情報のマップ
	 */
	private Map<String, Map<String, ItemIndex>> getItemIndexMap( HSSFWorkbook reportWB ) {

		Map<String, Map<String, ItemIndex>> sheetMap = new LinkedHashMap<String, Map<String, ItemIndex>>();

		for ( int sheetIndex = 0; sheetIndex < reportWB.getNumberOfSheets(); sheetIndex++ ) {

			HSSFSheet targetSheet = reportWB.getSheetAt( sheetIndex );

			LinkedHashMap<String, ItemIndex> itemMap = new LinkedHashMap<String, ItemIndex>();
			sheetMap.put( targetSheet.getSheetName(), itemMap );

			for ( int rowIndex = 0; rowIndex <= targetSheet.getLastRowNum(); rowIndex++ ) {

				HSSFRow row = targetSheet.getRow( rowIndex );
				if ( null == row ) continue;

				for ( int cellIndex = 0; cellIndex < row.getLastCellNum(); cellIndex++ ) {

					HSSFCell cell = row.getCell( cellIndex );
					if ( null == cell ) continue;

					List<String> markList = getItemMarkList( getStringValue( cell ));
					if ( null != markList ) {
						for ( String mark : markList ) {
							itemMap.put( mark, new ItemIndex( rowIndex, cellIndex ));
						}
					}
				}
			}
		}

		return sheetMap;
	}

	/**
	 * 文字列から項目マークを抽出する
	 * @param target 対象文字列
	 * @return 項目マーク（項目マークを含まない場合はnull）
	 */
	private List<String> getItemMarkList( String target ) {

		int startIndex	= 0;
		int endIndex	= 0;
		List<String> itemMarkList = new ArrayList<String>();

		for ( int index = 0; index < target.length(); index++ ) {

			startIndex	= target.indexOf( ITEM_MARK_START, index + endIndex );
			endIndex	= target.indexOf( ITEM_MARK_END, index + endIndex );

			if ( -1 == startIndex || -1 == endIndex ) {
				break;
			}

			if ( startIndex >= endIndex ) {
				break;
			}

			itemMarkList.add( target.substring( startIndex, endIndex + 1 ));
		}

		if ( 0 == itemMarkList.size() ) {
			return null;
		} else {
			return itemMarkList;
		}
	}


	/**
	 * レポートのテンプレートファイルを取得する
	 * @return  レポートのテンプレートファイル
	 * @throws TriSystemException
	 */
	protected File getReportTemplateFile() {
		return this.templateFile;
	}
	/**
	 * レポートのテンプレートファイルを設定する
	 * templateFile  レポートのテンプレートファイル
	 * @throws TriSystemException
	 */
	protected void setReportTemplateFile( File templateFile ) throws TriSystemException {

		if ( !templateFile.exists() ) {
			throw new TriSystemException(DcmMessageId.DCM004026F , templateFile.getAbsolutePath());
		}
		this.templateFile = templateFile;
	}

	/**
	 * レポートの出力情報ファイルを取得する
	 * @return  レポートの出力情報ファイル
	 * @throws TriSystemException
	 */
	protected File getReportOutputInfoFile() {
		return this.outputInfoFile;
	}
	/**
	 * レポートの出力情報ファイルを取得する
	 * @return  レポートの出力情報ファイル
	 * @throws TriSystemException
	 */
	protected void setReportOutputInfoFile( File outputInfoFile ) throws TriSystemException {

		if ( !outputInfoFile.exists() ) {
			throw new TriSystemException(DcmMessageId.DCM004027F , outputInfoFile.getAbsolutePath());
		}
		this.outputInfoFile = outputInfoFile;
	}

	/** レポート出力情報 */
	public class ReportOutputInfo {

		public static final String SHEET_NAME							= "SheetName";
		public static final String CONTENTS_START_ROW					= "ContentsStartRow";

		public static final String CATEGORY_NAME_INDEX				= "CategoryNameIndex";
		public static final String ITEM_NAME_INDEX					= "ItemNameIndex";
		public static final String DO_OUTPUT_INDEX					= "DoOutputIndex";
		public static final String OUTPUT_SHEET_NAME_INDEX			= "OutputSheetNameIndex";
		public static final String IS_HEADER_CATEGORY_INDEX			= "IsHeaderCategoryIndex";
		public static final String CATEGORY_LIST_SPREAD_METHOD_INDEX	= "CategoryListSpreadMethodIndex";
		public static final String CATEGORY_LIST_SPREAD_NAME_INDEX	= "CategoryListSpreadNameIndex";
		public static final String ITEM_OUTPUT_MARK_INDEX				= "ItemOutputMarkIndex";
		public static final String ITEM_LIST_JOIN_STRING_INDEX		= "ItemListJoinStringIndex";
		public static final String ITEM_PREFIX_INDEX					= "ItemPrefixIndex";
		public static final String ITEM_SUFFIX_INDEX					= "ItemSuffixIndex";
		public static final String ITEM_NULL_VALUE_INDEX				= "ItemNullValueIndex";
		public static final String IS_DATE_INDEX						= "IsDateIndex";
		public static final String IS_NUMERIC_INDEX					= "IsNumericIndex";

	}

	/** カテゴリ固定出力情報 */
	public class CategoryOutputInfo {

		public static final String SHEET_NAME				= "SheetName";
		public static final String CONTENTS_START_ROW		= "ContentsStartRow";

		public static final String CATEGORY_NAME_INDEX	= "CategoryNameIndex";
		public static final String ENTITY_NAME_INDEX		= "EntityNameIndex";
		public static final String IDENTIFIER_ID_INDEX	= "IdentifierIdIndex";
		public static final String IS_LIST_INDEX			= "IsListIndex";

		/** カテゴリ名 */
		private String categoryName = null;
		/** エンティティ名 */
		private String entityName = null;
		/** 識別ＩＤ */
		private String identifierId = null;
		/** リストカテゴリかどうか */
		private String isList = null;


		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName( String categoryName ) {
			this.categoryName = categoryName;
		}
		public String getEntityName() {
			return this.entityName;
		}
		public void setEntityName( String entityName ) {
			this.entityName = entityName;
		}
		public String getIdentifierId() {
			return this.identifierId;
		}
		public void setIdentifierId( String identifierId ) {
			this.identifierId = identifierId;
		}
		public String getIsList() {
			return this.isList;
		}
		public void setIsList( String isList ) {
			this.isList = isList;
		}
	}

	/** 項目固定出力情報 */
	public class ItemOutputInfo {

		public static final String SHEET_NAME					= "SheetName";
		public static final String CONTENTS_START_ROW			= "ContentsStartRow";

		public static final String CATEGORY_NAME_INDEX		= "CategoryNameIndex";
		public static final String ITEM_NAME_INDEX			= "ItemNameIndex";
		public static final String FIELD_NAME_INDEX			= "FieldNameIndex";
		public static final String IS_CODE_INDEX				= "IsCodeIndex";
		public static final String CODE_SHEET_NAME_INDEX		= "CodeSheetNameIndex";
		public static final String CODE_BEAN_ID_INDEX			= "CodeBeanIdIndex";
		public static final String IS_LIST_INDEX				= "IsListIndex";
		public static final String IS_CATEGORY_INDEX			= "IsCategoryIndex";
		public static final String LINK_CATEGORY_NAME_INDEX	= "LinkCategoryNameIndex";

		/** カテゴリ名 */
		private String categoryName = null;
		/** 項目名 */
		private String itemName = null;
		/** フィールド情報 */
		private List<FieldInfo> fieldInfoList = new ArrayList<FieldInfo>();
		/** コード項目かどうか */
		private String isCode = null;
		/** コード定義カスタマイズシート名 */
		private String codeSheetName = null;
		/** コードのBeanID */
		private String codeBeanId = null;
		/** リンクカテゴリ名 */
		private List<String> linkCategoryNameList = new ArrayList<String>();


		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName( String categoryName ) {
			this.categoryName = categoryName;
		}

		public String getItemName() {
			return this.itemName;
		}
		public void setItemName( String itemName ) {
			this.itemName = itemName;
		}

		public List<FieldInfo> getFieldInfoList() {
			return this.fieldInfoList;
		}
		public void setFieldInfoList( List<FieldInfo> fieldInfoList ) {
			this.fieldInfoList = fieldInfoList;
		}

		public String isCode() {
			return this.isCode;
		}
		public void setCode( String isCode ) {
			this.isCode = isCode;
		}

		public String getCodeSheetName() {
			return this.codeSheetName;
		}
		public void setCodeSheetName( String codeSheetName ) {
			this.codeSheetName = codeSheetName;
		}

		public String getCodeBeanId() {
			return this.codeBeanId;
		}
		public void setCodeBeanId( String codeBeanId ) {
			this.codeBeanId = codeBeanId;
		}

		public List<String> getLinkCategoryNameList() {
			return this.linkCategoryNameList;
		}
		public void setLinkCategoryNameList( List<String> linkCategoryNameList ) {
			this.linkCategoryNameList = linkCategoryNameList;
		}

	}

	public class FieldInfo {

		/** フィールド名 */
		private String name			= null;
		/** リスト項目かどうか */
		private boolean isList		= false;
		/** カテゴリ項目かどうか */
		private boolean isCategory	= false;

		public String getName() {
			return this.name;
		}
		public void setName( String name ) {
			this.name = name;
		}

		public boolean isList() {
			return this.isList;
		}
		public void setList( boolean isList ) {
			this.isList = isList;
		}

		public boolean isCategory() {
			return this.isCategory;
		}
		public void setCategory( boolean isCategory ) {
			this.isCategory = isCategory;
		}
	}



	/** カテゴリごとのデータ貼り付け先情報 */
	public class CategoryMappingInfo {

		/** No */
		private int no = 0;
		/** カテゴリ名 */
		private String categoryName = null;
		/** カテゴリ名（エンティティ名） */
		private String categoryEntityName = null;
		/** ヘッダカテゴリかどうか */
		private boolean isHeaderCategory = false;
		/** Ｎカテゴリかどうか */
		private boolean isListCategory = false;
		/** 項目ごとのデータ貼り付け情報 */
		private List<ItemMappingInfo> itemMappingInfoList = new ArrayList<ItemMappingInfo>();
		/** 展開方法 */
		private ListSpreadMothod listSpreadMothod = null;
		/** 出力シート名 */
		private String sheetName = null;
		/** テンプレートシート名 */
		private String templateSheetName = null;
		/** 展開コピー元開始セル位置(縦方向) */
		private int startRowIndex = 0;
		/** 展開コピー元開始セル位置(横方向) */
		private int startCellIndex = 0;
		/** 展開コピー元終了セル位置(縦方向) */
		private int endRowIndex = 0;
		/** 展開コピー元終了セル位置(横方向) */
		private int endCellIndex = 0;
		/** 出力カテゴリかどうか */
		private boolean isDoOutput = false;
		/** 識別ＩＤ */
		private String identifierId = null;
		/** キー情報(リンク先カテゴリ名,項目マッピング情報) */
		private Map<String, List<ItemMappingInfo>> keyMappingInfoListMap = new LinkedHashMap<String, List<ItemMappingInfo>>();
		/** 処理済エンティティ */
		private List<IEntity> finishedEntityList = new ArrayList<IEntity>();


		public Map<String, List<ItemMappingInfo>> getKeyMappingInfoListMap() {
			return this.keyMappingInfoListMap;
		}
		public void setKeyMappingInfoListMap( Map<String, List<ItemMappingInfo>> keyMappingInfoListMap ) {
			this.keyMappingInfoListMap = keyMappingInfoListMap;
		}

		public int getNo() {
			return this.no;
		}
		public void setNo( int no ) {
			this.no = no;
		}

		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName( String categoryName ) {
			this.categoryName = categoryName;
		}

		public String getCategoryEntityName() {
			return this.categoryEntityName;
		}
		public void setCategoryEntityName( String categoryEntityName ) {
			this.categoryEntityName = categoryEntityName;
		}

		public boolean isHeaderCategory() {
			return this.isHeaderCategory;
		}
		public void setHeaderCategory( boolean isHeaderCategory ) {
			this.isHeaderCategory = isHeaderCategory;
		}

		public boolean isListCategory() {
			return this.isListCategory;
		}
		public void setListCategory( boolean isListCategory ) {
			this.isListCategory = isListCategory;
		}

		public ListSpreadMothod getListSpreadMothod() {
			return this.listSpreadMothod;
		}
		public void setListSpreadMothod( ListSpreadMothod listSpreadMothod ) {
			this.listSpreadMothod = listSpreadMothod;
		}

		public List<ItemMappingInfo> getItemMappingInfoList() {
			return this.itemMappingInfoList;
		}
		public void setItemMappingInfoList( List<ItemMappingInfo> itemMappingInfoList ) {
			this.itemMappingInfoList = itemMappingInfoList;
		}
		public void addItemMappingInfo( ItemMappingInfo itemMappingInfo ) {
			this.itemMappingInfoList.add( itemMappingInfo );
		}

		public String getSheetName() {
			return this.sheetName;
		}
		public void setSheetName( String sheetName ) {
			this.sheetName = sheetName;
		}

		public String getTemplateSheetName() {
			return this.templateSheetName;
		}
		public void setTemplateSheetName( String templateSheetName ) {
			this.templateSheetName = templateSheetName;
		}

		public int getStartRowIndex() {
			return this.startRowIndex;
		}
		public void setStartRowIndex( int startRowIndex ) {
			this.startRowIndex = startRowIndex;
		}

		public int getStartCellIndex() {
			return this.startCellIndex;
		}
		public void setStartCellIndex( int startCellIndex ) {
			this.startCellIndex = startCellIndex;
		}

		public int getEndRowIndex() {
			return this.endRowIndex;
		}
		public void setEndRowIndex( int endRowIndex ) {
			this.endRowIndex = endRowIndex;
		}

		public int getEndCellIndex() {
			return this.endCellIndex;
		}
		public void setEndCellIndex( int endCellIndex ) {
			this.endCellIndex = endCellIndex;
		}

		public boolean isDoOutput() {
			return this.isDoOutput;
		}
		public void setDoOutput( boolean isDoOutput ) {
			this.isDoOutput = isDoOutput;
		}

		public String getIdentifierId() {
			return this.identifierId;
		}
		public void setIdentifierId( String identifierId ) {
			this.identifierId = identifierId;
		}

		public List<IEntity> getFinishedEntityList() {
			return this.finishedEntityList;
		}
		public void setFinishedEntityList( List<IEntity> finishedEntityList ) {
			this.finishedEntityList = finishedEntityList;
		}
	}

	/** 項目ごとのデータ貼り付け先情報 */
	public class ItemMappingInfo {

		/** カテゴリ名 */
		private String categoryName = null;
		/** 項目名 */
		private String itemName = null;
		/** フィールド情報 */
		private List<FieldInfo> itemFieldInfoList = new ArrayList<FieldInfo>();
		/** リスト項目の結合文字列 */
		private String joinString = null;
		/** コード値かどうか */
		private boolean isCodeItem = false;
		/** コード値のデザイン名 */
		private String codeDesignName = null;
		/** コード値のBeanID */
		private String codeBeanId = null;
		/** セル位置(縦方向) */
		private int rowIndex = 0;
		/** セル位置(横方向) */
		private int cellIndex = 0;
		/** 日付項目かどうか */
		private boolean isDateItem = false;
		/** 数値項目かどうか */
		private boolean isNumericItem = false;
		/** プレフィックス */
		private String prefix = null;
		/** サフィックス */
		private String suffix = null;
		/** Null置換文字列 */
		private String nullValue = null;
		/** 項目位置マーク */
		private String indexMark = null;


		public String getCategoryName() {
			return this.categoryName;
		}
		public void setCategoryName( String categoryName ) {
			this.categoryName = categoryName;
		}

		public String getItemName() {
			return this.itemName;
		}
		public void setItemName( String itemName ) {
			this.itemName = itemName;
		}

		public List<FieldInfo> getItemFieldInfoList() {
			return this.itemFieldInfoList;
		}
		public void setItemFieldInfoList( List<FieldInfo> fieldInfoList ) {
			this.itemFieldInfoList = fieldInfoList;
		}
		public void addItemFieldInfoList( FieldInfo itemFieldInfo ) {
			this.itemFieldInfoList.add( itemFieldInfo );
		}

		public String getJoinString() {
			return this.joinString;
		}
		public void setJoinString( String joinString ) {
			this.joinString = joinString;
		}

		public boolean isCodeItem() {
			return this.isCodeItem;
		}
		public void setCodeItem( boolean isCodeItem ) {
			this.isCodeItem = isCodeItem;
		}

		public String getCodeDesignName() {
			return this.codeDesignName;
		}
		public void setCodeDesignName( String codeDesignName ) {
			this.codeDesignName = codeDesignName;
		}

		public String getCodeBeanId() {
			return this.codeBeanId;
		}
		public void setCodeBeanId( String codeBeanId ) {
			this.codeBeanId = codeBeanId;
		}

		public int getRowIndex() {
			return this.rowIndex;
		}
		public void setRowIndex( int rowIndex ) {
			this.rowIndex = rowIndex;
		}

		public int getCellIndex() {
			return this.cellIndex;
		}
		public void setCellIndex( int cellIndex ) {
			this.cellIndex = cellIndex;
		}

		public boolean isDateItem() {
			return this.isDateItem;
		}
		public void setDateItem( boolean isDateItem ) {
			this.isDateItem = isDateItem;
		}

		public boolean isNumericItem() {
			return this.isNumericItem;
		}
		public void setNumericItem( boolean isNumericItem ) {
			this.isNumericItem = isNumericItem;
		}

		public String getPrefix() {
			return this.prefix;
		}
		public void setPrefix( String prefix ) {
			this.prefix = prefix;
		}

		public String getSuffix() {
			return this.suffix;
		}
		public void setSuffix( String suffix ) {
			this.suffix = suffix;
		}

		public String getNullValue() {
			return this.nullValue;
		}
		public void setNullValue( String nullValue ) {
			this.nullValue = nullValue;
		}

		public String getIndexMark() {
			return this.indexMark;
		}
		public void setIndexMark( String indexMark ) {
			this.indexMark = indexMark;
		}
	}

	/** リストカテゴリの繰返展開方法の列挙型 */
	public enum ListSpreadMothod {

		/** なし */
		Nothing("Nothing"),
		/** 縦方向 */
		Vertical("Vertical"),
		/** 別シート */
		Sheet("Sheet");

		private String value = null;

		private ListSpreadMothod( String value ) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}

		public static ListSpreadMothod getValue( String name ) {
			for ( ListSpreadMothod define : values() ) {
				if ( define.getValue().equals( name ) )
					return define;
			}

			return null;
		}
	}

	private class ItemIndex {

		private int rowIndex	= 0;
		private int cellIndex	= 0;

		public ItemIndex( int rowIndex, int cellIndex ) {
			this.rowIndex	= rowIndex;
			this.cellIndex	= cellIndex;
		}

		public int getRowIndex() {
			return this.rowIndex;
		}

		public int getCellIndex() {
			return this.cellIndex;
		}
	}


	/**
	 * レポートが最大行に到達した場合の例外
	 *
	 */
	private class ReachedMaxRowsException extends RuntimeException {

		private static final long serialVersionUID = 1L;

		private String _sheetName	= null;
		private HSSFCell _cell		= null;

		public ReachedMaxRowsException( String sheetName, HSSFCell cell ) {
			this._sheetName	= sheetName;
			this._cell		= cell;
		}

		public String getSheetName() {
			return _sheetName;
		}

		public HSSFCell getCell() {
			return _cell;
		}
	}

	/**
	 * 引数で与えられた文字列が、１セルに入力可能な文字数の上限を超えていないかチェックする。<br>
	 * 超えていない場合にはそのままの文字列を返す。<br>
	 * 超えていた場合には、文字列をカットして上限超えメッセージを付加して文字列を返す。<br>
	 * @param value 入力文字数
	 * @return 編集後文字列
	 */
	private String trimCellMaxString( String value ) {
		if( TriStringUtils.isEmpty( value ) ) {
			return value ;
		}

		if( CELL_STRING_MAX <= value.length() ) {
			value = value.substring( 0 , CELL_STRING_MAX ) ;
			value += CELL_STRING_OVER_MESSAGE ;
		}

		return value ;
	}

	protected boolean isLegacyReport( List<Object> paramList ) {
		LookAndFeel lookAndFeel = LookAndFeel.none;

		if ( sheet instanceof DesignSheetProduct ){
			DesignSheetProduct sheetProduct = (DesignSheetProduct) sheet;
			String userId = DcmExtractEntityAddonUtil.extractReportServiceBean(paramList).getUserId();

			lookAndFeel = sheetProduct.getFinder().getUmFinderSupport().getLookAndFeel( userId );
		}

		if( LookAndFeel.Trinity.equals( lookAndFeel ) ){
			return false;

		}else{
			return true;
		}
	}
	
	protected TimeZone getTimeZone( List<Object> paramList ) {
		TimeZone timeZone = DcmExtractEntityAddonUtil.extractReportServiceBean(paramList).getTimeZone();
		return timeZone;
	}

	private String getValueFromProperties(IDesignBeanId.Sheet sheet, String beanId, String codeId) {

		if (TriStringUtils.isEmpty(beanId)){
			return null;
		}

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		String key = null;

		if ( IDesignBeanId.Sheet.rmDesignSheet == sheet && RmDesignBeanId.statusId.name().equals(beanId) ){

			if ( null != RmRpStatusId.value(codeId) ){
				key = RmRpStatusId.value(codeId).getMessageKey();

			}else if ( null != RmRpStatusIdForExecData.value(codeId) ){
				key = RmRpStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != RmRaStatusId.value(codeId) ){
				key = RmRaStatusId.value(codeId).getMessageKey();

			}else if ( null != RmRaStatusIdForExecData.value(codeId) ){
				key = RmRaStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != BmBpStatusId.value(codeId) ){
				key = BmBpStatusId.value(codeId).getMessageKey();

			}else if ( null != BmBpStatusIdForExecData.value(codeId) ){
				key = BmBpStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != AmPjtStatusIdForExecData.value(codeId) ){
				key = AmPjtStatusIdForExecData.value(codeId).getMessageKey();
			}
		}


		if ( IDesignBeanId.Sheet.dcmDesignSheet == sheet ){

			if ( DcmDesignBeanId.statusId.name().equals(beanId) ){
				if ( null != DcmRepStatusId.value(codeId) ){
					key = DcmRepStatusId.value(codeId).getMessageKey();

				}else if ( null != DcmRepStatusIdForExecData.value(codeId) ){
					key = DcmRepStatusIdForExecData.value(codeId).getMessageKey();
				}
			}

			if ( DcmDesignBeanId.reportId.name().equals(beanId)){
				if ( null != DcmReportType.value(codeId) ){
					key = DcmReportType.value(codeId).wordKey();
				}
			}

			if ( DcmDesignBeanId.changeStatusId.name().equals(beanId)){
				if (codeId.equals(Status.CHANGE.toString())){
					codeId = FileStatus.Edit.getStatusId();
				}
				
				if ( null != FileStatus.value(codeId) ){
					key = FileStatus.value(codeId).getStatusV4();
				}
			}

			if ( DcmDesignBeanId.processStatusId.name().equals(beanId)){
				if ( null != DcmProcessStatusId.value(codeId) ){
					key = DcmProcessStatusId.value(codeId).getMessageKey();
				}
			}
		}


		if ( IDesignBeanId.Sheet.amDesignSheet == sheet && AmDesignBeanId.statusId.name().equals(beanId)){

			if ( null != AmAreqStatusId.value(codeId) ){
				key = AmAreqStatusId.value(codeId).getMessageKey();

			}else if ( null != AmAreqStatusIdForExecData.value(codeId) ){
				key = AmAreqStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != AmHeadBlStatusId.value(codeId) ){
				key = AmHeadBlStatusId.value(codeId).getMessageKey();

			}else if ( null != AmHeadBlStatusIdForExecData.value(codeId) ){
				key = AmHeadBlStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != AmLotBlStatusId.value(codeId) ){
				key = AmLotBlStatusId.value(codeId).getMessageKey();

			}else if ( null != AmLotBlStatusIdForExecData.value(codeId) ){
				key = AmLotBlStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != AmLotStatusId.value(codeId) ){
				key = AmLotStatusId.value(codeId).getMessageKey();

			}else if ( null != AmLotStatusIdForExecData.value(codeId) ){
				key = AmLotStatusIdForExecData.value(codeId).getMessageKey();

			}else if ( null != AmPjtStatusId.value(codeId) ){
				key = AmPjtStatusId.value(codeId).getMessageKey();

			}else if ( null != AmPjtStatusIdForExecData.value(codeId) ){
				key = AmPjtStatusIdForExecData.value(codeId).getMessageKey();
			}
		}

		if( null == key  ){
			return null;

		}else{
			return ca.getValue( key, SystemProps.TriSystemLanguage.getProperty() );
		}
	}
}
