package jp.co.blueship.tri.dcm;

import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.session.request.RequestInfoFactory;
import jp.co.blueship.tri.fw.sm.ui.portal.rb.SCRE0000ResponseBean;

/**
 *
 * @version V3L10R01
 *
 * @version SP-201511-1_V3L14R01
 * @author Yukihiro Eguchi
 */
public class DownloadUtil extends Action {

	public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
	throws Exception
	{
		IRequestInfo reqInfo = null;

		try{

			reqInfo = RequestInfoFactory.getRequestContext(RequestInfoFactory.ASSIGN_HTTPSERVLETREUEST);
			reqInfo.init(request);
			reqInfo.setCharacterEncoding( this.getServlet().getInitParameter(RequestInfoFactory.SERVLET_ENCODING) );

			String fileName = reqInfo.getParameter("fileName");
			String filePath = reqInfo.getParameter("filePath");

			// HTTP RESPONSE をリセット
			response.reset();

			// 保存ダイアログに表示する日本語ファイル名のエンコーディング
			String file = new String(fileName.getBytes(Charset.WINDOWS_31J.value()), Charset.ISO_8859_1.value());

			// HTTP RESPONSE のヘッダを設定
			response.setContentType("text/plain");
			response.setHeader("Content-Disposition", "attachment;filename=\"" + file + "\"");

			// ファイルの内容を書き込む
			OutputStream os = response.getOutputStream();
			InputStream is = new FileInputStream( TriStringUtils.linkPath( filePath , file ) );

			byte[] buf = new byte[4096];
			int len;
			while ((len = is.read(buf)) != -1) {
				os.write(buf, 0, len);
			}

			is.close();
			os.flush();

			return mapping.findForward(null);

		} catch (Exception e) {
			response.reset();
			SCRE0000ResponseBean resBean = new SCRE0000ResponseBean();
			resBean.new Utility().reflectResponseBean(new TriSystemException(DcmMessageId.DCM005028S,e));
			request.setAttribute("responseBean", resBean);
			return mapping.findForward("downloadError");
		}
	}
}
