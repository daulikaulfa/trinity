package jp.co.blueship.tri.dcm;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;


/**
 * 指定した値の判定を行うユーティリティークラスです。
 * BusinessJudgUtilsの派生クラスです。依存性の解消のために作成します。
 * @author Takashi Ono
 *
 */
public class DcmBusinessJudgUtils {

	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isReturnApply( String areqCtgCd ) {
		AreqCtgCd id = getAreqCtgCd( areqCtgCd );
		if ( null == id )
			return false;

		if ( AreqCtgCd.ReturningRequest.equals(id) )
			return true;

		return false;
	}
	/**
	 * 申請区分が返却申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isReturnApply( IAreqEntity entity ) {
		return isReturnApply(  entity.getAreqCtgCd() );
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDeleteApply( String areqCtgCd ) {
		AreqCtgCd id = getAreqCtgCd( areqCtgCd );
		if ( null == id )
			return false;

		if ( AreqCtgCd.RemovalRequest.equals(id) )
			return true;

		return false;
	}
	/**
	 * 申請区分が削除申請かどうかを判断します。
	 *
	 * @param entity 申請エンティティ
	 * @return 該当する場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDeleteApply( IAreqEntity entity ) {
		return isDeleteApply(  entity.getAreqCtgCd() );
	}

	/**
	 * 申請区分に該当する列挙型を取得します。
	 *
	 * @param areqCtgCd 申請区分
	 * @return 該当する場合、ApplyIdを戻します。それ以外はnullを戻します。
	 */
	private static final AreqCtgCd getAreqCtgCd( String areqCtgCd ) {
		if ( null == areqCtgCd )
			return null;

		AreqCtgCd id = AreqCtgCd.value( areqCtgCd );
		if ( null == id )
			return null;

		return id;
	}
}
