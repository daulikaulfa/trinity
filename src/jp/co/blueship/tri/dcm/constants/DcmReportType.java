package jp.co.blueship.tri.dcm.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;


/**
 * Enumeration type of report.
 * <br>帳票区分の列挙型
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public enum DcmReportType {
	none("", ""),
	/**
	 * Resource Report
	 * <br>原本資産レポート
	 */
	AmRsReport("AM-RS-00", "Constants.dcmReport.AM-RS-00.text"),
	/**
	 * Build Package Report
	 * <br>ビルドパッケージ
	 */
	BmBpReport("BM-BP-00", "Constants.dcmReport.BM-BP-00.text"),
	/**
	 * Release Package Report
	 * <br>リリースパッケージ
	 */
	RmRpReport("RM-RP-00", "Constants.dcmReport.RM-RP-00.text"),
	/**
	 * Release Request Report
	 * <br>リリース申請票
	 */
	RmRaReport("RM-RA-00", "Constants.dcmReport.RM-RA-00.text");

	private String value = null;
	private String wordKey = null;

	private DcmReportType( String value, String wordKey ) {
		this.value = value;
		this.wordKey = wordKey;
	}

	public boolean equals( String value ) {
		DcmReportType inner = value( value );

		if ( ! this.equals(inner) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public String wordKey() {
		return this.wordKey;
	}

	/**
	 * 指定された値に対応する列挙型を取得します。
	 *
	 * @param value 値
	 * @return 対応する列挙型
	 */
	public static DcmReportType value( String value ) {
		if ( TriStringUtils.isEmpty( value ) ) {
			return none;
		}

		for ( DcmReportType inner : values() ) {
			if ( inner.value.equals( value ) ) {
				return inner;
			}
		}

		return none;
	}

}
