package jp.co.blueship.tri.dcm.ui.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class ReportRegisterResponseBean extends BaseResponseBean {

	

	/** レポート番号 */
	private String reportNo = null;


	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}

}
