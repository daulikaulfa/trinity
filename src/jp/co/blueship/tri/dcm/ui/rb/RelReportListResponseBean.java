package jp.co.blueship.tri.dcm.ui.rb;

import java.util.List;

import jp.co.blueship.tri.dcm.beans.dto.ReportSearchBean;
import jp.co.blueship.tri.dcm.domain.dl.beans.dto.RelReportListViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelReportListResponseBean extends BaseResponseBean {

	

	/**
	 * ページ番号数
	 */
	private Integer pageNoNum = null;
	/**
	 * 選択ページ番号
	 */
	private Integer selectPageNo = null;
	/**
	 * レポート情報
	 */
	private List<RelReportListViewBean> reportListViewBeanList = null;
	/**
	 * リリース 詳細検索用Bean
	 */
	private ReportSearchBean searchBean = null ;


	public List<RelReportListViewBean> getReportAssetViewBeanList() {
		return reportListViewBeanList;
	}
	public void setReportAssetViewBeanList( List<RelReportListViewBean> reportAssetViewBeanList ) {
		this.reportListViewBeanList = reportAssetViewBeanList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}
	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	public ReportSearchBean getSearchBean() {
		return searchBean;
	}
	public void setSearchBean(ReportSearchBean searchBean) {
		this.searchBean = searchBean;
	}

}
