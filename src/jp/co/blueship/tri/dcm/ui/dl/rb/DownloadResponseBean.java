package jp.co.blueship.tri.dcm.ui.dl.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;


public class DownloadResponseBean extends BaseResponseBean {

	private String downloadPath;
	private String downloadFile;
	private String downloadSize;
	
	/**
	 * @return downloadFile
	 */
	public String getDownloadFile() {
		return downloadFile;
	}
	/**
	 * @param downloadFile 設定する downloadFile
	 */
	public void setDownloadFile(String downloadFile) {
		this.downloadFile = downloadFile;
	}
	/**
	 * @return downloadSize
	 */
	public String getDownloadSize() {
		return downloadSize;
	}
	/**
	 * @param downloadSize 設定する downloadSize
	 */
	public void setDownloadSize(String downloadSize) {
		this.downloadSize = downloadSize;
	}
	/**
	 * @return downloadPath
	 */
	public String getDownloadPath() {
		return downloadPath;
	}
	/**
	 * @param downloadPath 設定する downloadPath
	 */
	public void setDownloadPath(String downloadPath) {
		this.downloadPath = downloadPath;
	}
	
}
