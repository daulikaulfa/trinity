package jp.co.blueship.tri.dcm.ui.dl.beans;

import jp.co.blueship.tri.dcm.beans.dto.ReportSearchBean;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportListServiceBean;
import jp.co.blueship.tri.dcm.ui.DcmCnvServiceToActionUtils;
import jp.co.blueship.tri.dcm.ui.dl.constants.RelReportScreenID;
import jp.co.blueship.tri.dcm.ui.rb.RelReportListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelReportListProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelReportListService";
	public static final String SEARCH_REPORT_LIST = FLOW_ACTION_ID + "reportList";
	public static final String RESULT_REPORT_LIST = FLOW_ACTION_ID + "resultList";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelReportScreenID.LIST };

	public FlowRelReportListProsecutor() {
		super( null );
	}

	public FlowRelReportListProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		sesInfo.removeAttribute	( SessionScopeKeyConsts.TOP_SCREEN_NAME );

		FlowRelReportListServiceBean bean = new FlowRelReportListServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );


		//詳細検索の検索条件を同一環境内で保持する
		{
			ReportSearchBean searchBean = (ReportSearchBean)session.getAttribute( SEARCH_REPORT_LIST );

			if ( null == searchBean ||
					! RelReportScreenID.LIST.equals( referer ) ) {
				session.removeAttribute( SEARCH_REPORT_LIST );
				bean.setSearchBean( null );
			} else {
				bean.setSearchBean( searchBean );
			}

			//[検索]ボタン押下時、検索情報をセッションに保持する
			if ( RelReportScreenID.LIST.equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setSearchBean( convertReportSearchBean( reqInfo, bean.getSearchBean() ) );

					session.setAttribute( SEARCH_REPORT_LIST, bean.getSearchBean() );
				}
			}
		}

		if ( RelReportScreenID.LIST.equals( referer ) ) {
			String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
			if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
				bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
			}
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		sesInfo.setAttribute( SessionScopeKeyConsts.TOP_SCREEN_NAME, RelReportScreenID.LIST );

		FlowRelReportListServiceBean bean =
			(FlowRelReportListServiceBean)getServiceReturnInformation();

		RelReportListResponseBean resBean =
			DcmCnvServiceToActionUtils.convertReportListResponseBean( bean );

		session.setAttribute( RESULT_REPORT_LIST, resBean.getReportAssetViewBeanList() );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	private ReportSearchBean convertReportSearchBean( IRequestInfo reqInfo, ReportSearchBean bean ) {

		if ( null == bean )
			bean = new ReportSearchBean();

		String[] selectedStatus = reqInfo.getParameterValues( "status[]" ) ;
		bean.setSelectedStatusString( selectedStatus ) ;

		String searchReportNo = reqInfo.getParameter( "searchReportNo" ) ;
		bean.setSearchReportNo( searchReportNo ) ;

		String selectedReportId = reqInfo.getParameter( "searchReportId" ) ;
		bean.setSelectedReportId( selectedReportId ) ;

		String searchExecuteUser = reqInfo.getParameter( "searchExecuteUser" ) ;
		bean.setSearchExecuteUser( searchExecuteUser ) ;

		String searchExecuteStartDate = reqInfo.getParameter( "searchExecuteStartDate" ) ;
		bean.setSearchExecuteStartDate( searchExecuteStartDate ) ;

		String searchExecuteEndDate = reqInfo.getParameter( "searchExecuteEndDate" ) ;
		bean.setSearchExecuteEndDate( searchExecuteEndDate ) ;

		String selectedCount = reqInfo.getParameter( "searchCount" ) ;
		bean.setSelectedCount( selectedCount ) ;

		return bean;
	}

}
