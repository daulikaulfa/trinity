package jp.co.blueship.tri.dcm.ui.dl.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.domain.dl.beans.dto.RelReportListViewBean;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.dcm.ui.DcmCnvServiceToActionUtils;
import jp.co.blueship.tri.dcm.ui.dl.constants.RelReportScreenID;
import jp.co.blueship.tri.dcm.ui.rb.RelReportListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class FlowRelReportDeleteProsecutor extends PresentationProsecutor {



	public static final String FLOW_ACTION_ID = "FlowRelReportDeleteService";
	public static final String SEARCH_REPORT_LIST = FLOW_ACTION_ID + "reportList";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelReportScreenID.LIST };

	public FlowRelReportDeleteProsecutor() {
		super( null );
	}

	public FlowRelReportDeleteProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		sesInfo.removeAttribute	( SessionScopeKeyConsts.TOP_SCREEN_NAME );

		FlowRelReportDeleteServiceBean bean = new FlowRelReportDeleteServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		//削除対象として選択されたレコード
		String[] selectedDelete = reqInfo.getParameterValues( "delete[]" ) ;
		bean.setSelectedDelete( selectedDelete ) ;


		// ロック用
		List<String> reportNoList = null ;
		if( ! TriStringUtils.isEmpty( selectedDelete ) ) {
			reportNoList = FluentList.from(selectedDelete).asList();
		} else {
			reportNoList = new ArrayList<String>() ;
		}

		List<String> lotNoList		= new ArrayList<String>();
		List<String> serverNoList	= new ArrayList<String>();

		@SuppressWarnings("unchecked")
		List<RelReportListViewBean> reportList =
			(List<RelReportListViewBean>)session.getAttribute( FlowRelReportListProsecutor.RESULT_REPORT_LIST );

		for ( RelReportListViewBean viewBean : reportList ) {

			if ( reportNoList.contains( viewBean.getReportNo() )) {

				lotNoList.add	( viewBean.getLotNo() );
				serverNoList.add( viewBean.getServerNo() );
			}
		}

		bean.setLockLotNoArray		( (String[])lotNoList.toArray( new String[0] ));
		//bean.setLockServerIdArray	( (String[])serverNoList.toArray( new String[0] ));


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		sesInfo.setAttribute( SessionScopeKeyConsts.TOP_SCREEN_NAME, RelReportScreenID.LIST );

		FlowRelReportDeleteServiceBean bean =
			(FlowRelReportDeleteServiceBean)getServiceReturnInformation();

		RelReportListResponseBean resBean =
			DcmCnvServiceToActionUtils.convertReportListResponseBean( bean );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

}
