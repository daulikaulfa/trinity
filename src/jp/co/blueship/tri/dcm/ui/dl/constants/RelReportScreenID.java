package jp.co.blueship.tri.dcm.ui.dl.constants;


/**
 * レポート用画面IDの定義です。
 */

public class RelReportScreenID {

	/**
	 * レポート出力依頼一覧画面
	 */
	public static final String LIST = "RelReportList";
	/**
	 * レポート・ダウンロード画面
	 */
	public static final String DOWNLOAD = "RelReportDownload";

}
