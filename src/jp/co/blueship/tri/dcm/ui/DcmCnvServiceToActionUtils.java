package jp.co.blueship.tri.dcm.ui;

import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportListServiceBean;
import jp.co.blueship.tri.dcm.ui.rb.RelReportListResponseBean;

public class DcmCnvServiceToActionUtils {

	public static RelReportListResponseBean convertReportListResponseBean(
			FlowRelReportListServiceBean bean ) { 
	
		RelReportListResponseBean resBean = new RelReportListResponseBean();
		
		resBean.setReportAssetViewBeanList	( bean.getReportAssetViewBeanList() );
		resBean.setSearchBean( bean.getSearchBean() );
		
		resBean.setPageNoNum				( bean.getPageInfoView().getMaxPageNo() );
		resBean.setSelectPageNo				( bean.getPageInfoView().getSelectPageNo() );
				
		return resBean;
		
	}
	
	public static RelReportListResponseBean convertReportListResponseBean(
			FlowRelReportDeleteServiceBean bean ) { 
	
		RelReportListResponseBean resBean = new RelReportListResponseBean();
	
		return resBean;
		
	}
}
