package jp.co.blueship.tri.dcm;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.dcm.beans.ActionReportArchiveFileExtractor;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * レポート ファイル操作の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ReportFileAddonUtil {

	/**
	 * 指定されたディレクトリ配下のファイルを取得する
	 * @param topDir トップディレクトリ
	 * @param extensionList 拡張子のリスト
	 * @return ファイルのリスト
	 */
	public static File[] getFiles( File topDir, List<String> extensionList ) throws TriSystemException {

		List<File> fileList = new ArrayList<File>();;

		try {
			for ( String extension : extensionList ) {
				fileList.addAll( getFiles( topDir, getFileExtensionFilter( extension )));
			}

		} catch( IOException ioe ) {
			throw new TriSystemException( DcmMessageId.DCM004031F, ioe );
		}

		File[] files = (File[])fileList.toArray( new File[0] );
		BusinessFileUtils.sortDefaultFileList( files );

		return 	files;
	}

	/**
	 * 指定されたファイル拡張子のフィルタを取得する
	 * @param extension
	 * @return
	 */
	private static FileFilter getFileExtensionFilter( String extension ) {

		final String _extension = extension;

		return new FileFilter() {
			public boolean accept( File file ) {
				boolean ret = file.getName().toLowerCase().endsWith( _extension );
				return ret;
			}
		};
	}

	/**
	 *
	 * 指定されたディレクトリ配下の、フィルタの条件を満たすファイルを取得する
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param filter
	 * @return List 取得したList
	 * @throws IOException
	 */
	private static List<File> getFiles( File topDir, FileFilter filter ) throws IOException {

		List<File> flist = new ArrayList<File>();

		if ( !topDir.exists() || !topDir.isDirectory() ) return flist;


		File[] filteredFiles = topDir.listFiles( filter );
		for( File targetFile : filteredFiles ) {

			if( targetFile.isFile() ) {
				flist.add( targetFile );
			}
		}

		File[] subDirs = topDir.listFiles();
		for( File subDir : subDirs ) {

			if( subDir.isDirectory() ) {
				flist.addAll( getFiles( subDir, filter ));
			}
		}

		return flist;
	}

	/**
	 * ファイルのパスを指定トップパスからの相対パスとし、その相対パスを指定された情報（開始パス）に従い分類する
	 * @param files ファイルのリスト
	 * @param topDir トップパス
	 * @param filePathSet 分類情報（開始パス）
	 * @param extractor
	 * @return 分類情報（開始パス）- ファイルのリストのマップ
	 */
	public static Map<String[], List<File>> groupFile(
			File[] files, File topDir, Set<String[]> filePathSet,
			ActionReportArchiveFileExtractor extractor ) {

		Map<String[], List<File>> groupFileMap = new LinkedHashMap<String[], List<File>>();

		for ( String[] groupPath : filePathSet ) {

			for ( File file : files ) {

				String[] path = TriStringUtils.convertRelativePath(
						topDir, TriStringUtils.convertPath( file.getPath() )).split( "/" );

				if ( extractor.startWith( path, groupPath )) {

					List<File> groupFileList = groupFileMap.get( groupPath );
					if ( null == groupFileList ) {
						groupFileList = new ArrayList<File>();
						groupFileMap.put( groupPath, groupFileList );
					}
					groupFileList.add( file );
				}
			}
		}

		return groupFileMap;
	}
}
