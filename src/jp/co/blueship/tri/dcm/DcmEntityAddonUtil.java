package jp.co.blueship.tri.dcm;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;

/**
 * エンティティへの入出力の共通処理Class
 * EntityAddonUtilの派生クラスです。依存性を解消するために作成しました
 * @author Takashi Ono
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class DcmEntityAddonUtil {

	/**
	 * リリースエンティティからビルドパッケージ番号を取得する。
	 * @param entities リリースエンティティ
	 * @return ビルドパッケージ番号の配列
	 */
	public static String[] getBuildNo( IRpDto entity ) {

		Set<String> buildNoSet = new TreeSet<String>();
		for( IRpBpLnkEntity build : entity.getRpBpLnkEntities() ) {
			buildNoSet.add( build.getBpId() );
		}

		return (String[])buildNoSet.toArray( new String[0] );
	}

	/**
	 * グループユーザエンティティからグループIDのセットを取得します。
	 * @param groupUserEntitys グループユーザエンティティ
	 * @return グループIDのセット
	 */
	public static Set<String> getGroupIdSet( List<IGrpUserLnkEntity> groupUserEntitys ) {

		Set<String> groupIdSet = new HashSet<String>();
		for ( IGrpUserLnkEntity entity : groupUserEntitys ) {
			groupIdSet.add( entity.getGrpId() );
		}

		return groupIdSet;
	}
}
