package jp.co.blueship.tri.dcm;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.dcm.beans.ActionReportArchiveFileExtractor;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportArchiveEntryInfo;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * レポートの共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ReportAddonUtil {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * 文字列から置換用の変数文字列を抽出する
	 * @param target 対象文字列
	 * @return 置換用の変数文字列（項目マークを含まない場合はnull）
	 */
	public static List<String> getVariableList( String target ) {

		int startIndex	= 0;
		int endIndex	= 0;
		List<String> itemMarkList = new ArrayList<String>();

		for ( int index = 0; index < target.length(); index++ ) {

			startIndex	= target.indexOf( "@{", index + endIndex );
			endIndex	= target.indexOf( "}", index + endIndex );

			if ( -1 == startIndex || -1 == endIndex ) {
				break;
			}

			if ( startIndex >= endIndex ) {
				break;
			}

			itemMarkList.add( target.substring( startIndex, endIndex + 1 ));
		}

		if ( 0 == itemMarkList.size() ) {
			return null;
		} else {
			return itemMarkList;
		}
	}

	/**
	 * IReportArchiveEntryGroupInfoのリストを取得する
	 * @param files ファイルリスト
	 * @param topDir ファイルリストが格納されているディレクトリ
	 * @param filePathMap ファイル分類情報
	 * @param extractorList ActionReportArchiveFileExtractor
	 * @param charset アーカイブファイル作成時の文字コード
	 * @param entryPathMap アーカイブファイルの中身の分類情報
	 * @param extensionMap 展開対象拡張子のマップ
	 * @return
	 * @throws TriSystemException
	 */
	public static List<IReportArchiveEntryGroupInfo> getArchiveEntryGroupInfo(
			File[] files, File topDir, Map<String[], String> filePathMap,
			List<ActionReportArchiveFileExtractor> extractorList,
			Charset charset, Map<String[],String> entryPathMap,
			Map<String,String> extensionMap ) throws TriSystemException {

		Map<String[], List<File>> groupMap	=
			ReportFileAddonUtil.groupFile(
					files, topDir, filePathMap.keySet(), extractorList.get( 0 ));


		List<IReportArchiveEntryGroupInfo> groupInfoList = new ArrayList<IReportArchiveEntryGroupInfo>();

		for ( String[] groupPath : groupMap.keySet() ) {

			IReportArchiveEntryGroupInfo groupInfo = new ReportArchiveEntryGroupInfo();

			groupInfo.setIdentifierId	( "ARCHIVE_FILE" );
			groupInfo.setName			( filePathMap.get( groupPath ));


			List<IReportArchiveEntryInfo> achiveEntryList = new ArrayList<IReportArchiveEntryInfo>();

			for ( File file : groupMap.get( groupPath )) {

				IReportArchiveEntryInfo archiveEntryEntity = new ReportArchiveEntryInfo();

				archiveEntryEntity.setIdentifierId( "ARCHIVE_FILE" );
				archiveEntryEntity.setName(
						TriStringUtils.convertRelativePath( topDir, TriStringUtils.convertPath( file.getPath() )));
				archiveEntryEntity.setTime( TriDateUtils.getDefaultDateFormat().format( file.lastModified() ));
				archiveEntryEntity.setSize( Long.toString( file.length() ));

				Map<String,IReportArchiveEntryGroupInfo> entryGroupMap = new LinkedHashMap<String, IReportArchiveEntryGroupInfo>();

				int loopLevel = 0;

				for ( ActionReportArchiveFileExtractor extractor : extractorList ) {

					if ( extractor.isExtraction( file.getName(), loopLevel, extensionMap )) {

						InputStream inputStream = null;
						try {
							inputStream = new FileInputStream( file );

							extractor.setArchiveFileExtractorList( extractorList );
							extractor.setEntry( file, charset, entryGroupMap, entryPathMap, extensionMap, null, loopLevel );
						} catch ( FileNotFoundException e ) {
							throw new TriSystemException( DcmMessageId.DCM004030F, e );
						} finally {
							try {
								if ( null != inputStream ) {
									inputStream.close();
								}
							} catch ( IOException e ) {
								LogHandler.fatal( log , e ) ;
							}
						}
					}
				}

				archiveEntryEntity.setArchiveEntryGroup(
					(IReportArchiveEntryGroupInfo[])entryGroupMap.values().toArray( new IReportArchiveEntryGroupInfo[0] ));

				achiveEntryList.add( archiveEntryEntity );
			}
			groupInfo.setArchiveEntryList( achiveEntryList );

			groupInfoList.add( groupInfo );
		}

		return groupInfoList;
	}

	/**
	 * IReportArchiveEntryGroupInfoが持つIReportArchiveEntryInfoのうち、アーカイブファイルのエントリ情報を
	 * 持つIReportArchiveEntryInfoを抽出する。
	 * @param groupInfoList
	 * @return
	 */
	public static List<IReportArchiveEntryInfo> getArchiveEntryInfoWithArchiveEntry(
			List<IReportArchiveEntryGroupInfo> groupInfoList ) {

		List<IReportArchiveEntryInfo> entryInfoList = new ArrayList<IReportArchiveEntryInfo>();

		for ( IReportArchiveEntryGroupInfo group : groupInfoList ) {

			for ( IReportArchiveEntryInfo entry : group.getArchiveEntryList() ) {

				if ( !TriStringUtils.isEmpty( entry.getArchiveEntryGroup() )) {

					entryInfoList.add( entry );
				}
			}
		}

		return entryInfoList;
	}


	/**
	 * Mapのvalue（カンマ区切り）を分解してキーと値とし、新しいMapを作成する。
	 * @param origMap "キー-カンマ区切りの値"のマップ
	 * @return 新しいMap
	 */
	public static Map<String, String> getMap( Map<String, String> origMap ) {

		Map<String, String> orderMap = new LinkedHashMap<String, String>();
		for ( String value : origMap.values() ) {

			String[] values = value.split( "," );
			String path = values[ 0 ];
			String pathName = "";
			if ( 1 < values.length ) {
				pathName = values[ 1 ];
			}
			orderMap.put( path, pathName );
		}

		return orderMap;
	}
}
