package jp.co.blueship.tri.dcm.support;

import jp.co.blueship.tri.dcm.dao.rep.IRepDao;
import jp.co.blueship.tri.dcm.dao.rep.RepNumberingDao;

/**
 * DCM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IDcmDaoFinder {
	public RepNumberingDao getRepNumberingDao();
	public IRepDao getRepDao();

}
