package jp.co.blueship.tri.dcm.support;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.dcm.beans.dto.ReportSearchBean;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.fw.cmn.utils.DesignDefineSearchUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.rm.support.RmFinderSupport;



/**
 * レポート系イベントのサポートClass
 * <br>
 * <p>
 * レポート編集を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelReportEditSupport extends RmFinderSupport {

	/**
	 * リリースの検索条件に情報を設定する
	 * @param condition
	 * @param searchBean
	 */
	public void setReportCondition(
			RepCondition condition, ReportSearchBean searchBean ) {

		if ( TriStringUtils.isNotEmpty( searchBean.getSelectedReportId() ) ) {
			condition.setRepCtgCd( searchBean.getSelectedReportId() );
		}

		if ( TriStringUtils.isNotEmpty( searchBean.getSearchReportNo() )) {
			condition.setRepId(searchBean.getSearchReportNo(), true );
		}

		if ( TriStringUtils.isNotEmpty( searchBean.getSearchExecuteUser() )) {
			condition.setExecUserNm( searchBean.getSearchExecuteUser() );
		}

		String[] selectedStatusArray = searchBean.getSelectedStatusStringArray() ;
		condition.setProcStsId( convertStatusToStatusId( selectedStatusArray ) ) ;

	}

	/**
	 * ステータス名称をステータスＩＤに変換します
	 *
	 * @param statusArray
	 * @return
	 */
	public static final String[] convertStatusToStatusId( String[] statusArray ) {
		List<String> statusIdList = new ArrayList<String>() ;

		Map<String,String> reverseMap = DesignDefineSearchUtils.reverseMap( DesignSheetFactory.getDesignSheet().getKeyMap( DcmDesignBeanId.statusId ) );

		if ( TriStringUtils.isEmpty( statusArray ) ) {
			statusIdList.addAll( reverseMap.values() );

		} else {

			for( String status : statusArray ) {
				statusIdList.add( reverseMap.get( status ) ) ;
			}
		}

		return statusIdList.toArray( new String[ 0 ] );
	}

	/**
	 * ロット番号からロットエンティティを取得する。
	 * @param lotNoArray
	 * @return
	 */
	public List<ILotEntity> getPjtLotEntityArray( String[] lotNoArray ) {

		ISqlSort sort = new SortBuilder() ;
		LotCondition condition = new LotCondition() ;
		condition.setLotIds( lotNoArray ) ;

		return this.getAmFinderSupport().getLotDao().find( condition.getCondition() , sort, 1 , 0 ).getEntities() ;

	}
}
