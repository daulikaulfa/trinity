package jp.co.blueship.tri.dcm.support;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;



/**
 * レポート系イベントのサポートClass
 * <br>
 * <p>
 * レポート編集を行うための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitReportEditSupport extends BmFinderSupport {


	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ビルドパッケージ／資産管理台帳の対象となるビルドパッケージを取得します。
	 *
	 * @param buildNo ビルドパッケージ番号の配列
	 * @return
	 */
	public List<IBpEntity> getBuildEntityByBuildAssetRegister( String[] buildNo ) {

		if ( TriStringUtils.isEmpty( buildNo )) {
			return new ArrayList<IBpEntity>();
		}

		BpCondition condition = new BpCondition();
		condition.setBpIds( buildNo );
		ISqlSort sort = (ISqlSort) this.getBuildSortByBuildAssetRegister();
		IEntityLimit<IBpEntity> limit = this.getBpDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities();
	}

	/**
	 * ビルドパッケージ／資産管理台帳の対象となる変更管理情報を取得します。
	 *
	 * @param buildEntity ビルドパッケージリスト
	 * @return
	 */
	public IPjtEntity[] getPjtEntityByBuildAssetRegister( IBpDto buildEntity ) {

		if ( TriStringUtils.isEmpty( buildEntity )) {
			return new IPjtEntity[0];
		}

		Set<String> pjtIdSet = new HashSet<String>();
		for ( IBpAreqLnkEntity bpAreqLnkEntity : buildEntity.getBpAreqLnkEntities() ) {
			pjtIdSet.add( bpAreqLnkEntity.getPjtId() );
		}

		if ( 0 == pjtIdSet.size() )
			return new IPjtEntity[0];

		PjtCondition condition = new PjtCondition();
		condition.setPjtIds( (String[])pjtIdSet.toArray( new String[0] ) );
		ISqlSort sort = this.getPjtSortByBuildAssetRegister();
		IEntityLimit<IPjtEntity> limit = this.getAmFinderSupport().getPjtDao().find( condition.getCondition(),  sort, 1, 0 );

		return limit.getEntities().toArray(new IPjtEntity[0]);
	}

	/**
	 * ビルドパッケージ／資産管理台帳の対象となる申請情報を取得します。
	 *
	 * @param buildEntity ビルドパッケージリスト
	 * @return
	 */
	public IAreqEntity[] getAssetApplyEntityByBuildAssetRegister( IBpDto buildEntity ) {

		if ( TriStringUtils.isEmpty( buildEntity )) {
			return new IAreqEntity[0];
		}

		Set<String> applyNoSet = new HashSet<String>();
		for ( IBpAreqLnkEntity buildApplyEntity : buildEntity.getBpAreqLnkEntities() ) {
			applyNoSet.add( buildApplyEntity.getAreqId() );
		}

		if ( 0 == applyNoSet.size() )
			return new IAreqEntity[0];

		AreqCondition condition = new AreqCondition();
		condition.setAreqIds( (String[])applyNoSet.toArray( new String[0] ) );
		ISqlSort sort = this.getAssetApplySortByBuildAssetRegister();
		IEntityLimit<IAreqEntity> limit = this.getAmFinderSupport().getAreqDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new IAreqEntity[0]);
	}

	/**
	 *  ビルドパッケージ／資産管理台帳のビルドパッケージソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getBuildSortByBuildAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.buildAssetRegisterRelUnitOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.buildAssetRegisterRelUnitOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getBuildSortByBuildAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  ビルドパッケージ／資産管理台帳の変更管理ソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getPjtSortByBuildAssetRegister() {
		ISqlSort sort = new SortBuilder();

		Map<String, String> sortItems = sheet.getKeyMap( DcmDesignBeanId.buildAssetRegisterPjtOrder );
		Map<String, String> sortOrder = sheet.getKeyMap( DcmDesignBeanId.buildAssetRegisterPjtOrderBy );

		sort = DBSearchSortAddonUtil.getSortFromDesignDefine( sortItems, sortOrder, sort );

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getPjtSortByBuildAssetRegister:=" + sort.toSortString() );

		return sort;
	}

	/**
	 *  ビルドパッケージ／資産管理台帳の申請ソート順序を取得します。
	 *
	 * @return ソート順
	 */
	private final ISqlSort getAssetApplySortByBuildAssetRegister() {
		ISqlSort sort = new SortBuilder();

		sort.setElement(PjtItems.regTimestamp, TriSortOrder.Asc, 1);

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "getAssetApplySortByBuildAssetRegister:=" + sort.toSortString() );

		return sort;
	}

}
