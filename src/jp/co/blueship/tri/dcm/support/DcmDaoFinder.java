package jp.co.blueship.tri.dcm.support;

import jp.co.blueship.tri.dcm.dao.rep.IRepDao;
import jp.co.blueship.tri.dcm.dao.rep.RepNumberingDao;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;

/**
 * DCM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public abstract class DcmDaoFinder extends FinderSupport implements IDcmDaoFinder {

	private RepNumberingDao repNumberingDao = null;
	private IRepDao repDao = null;

	public RepNumberingDao getRepNumberingDao() {
	    return repNumberingDao;
	}
	public void setRepNumberingDao(RepNumberingDao repNumberingDao) {
	    this.repNumberingDao = repNumberingDao;
	}

	public IRepDao getRepDao() {
		return repDao;
	}
	public void setRepDao(IRepDao repDao) {
		this.repDao = repDao;
	}

}
