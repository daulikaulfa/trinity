package jp.co.blueship.tri.dcm.support;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * DCM関連情報を検索するためのサービス機能を提供するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class DcmFinderSupport extends DcmDaoFinder implements IDcmFinderSupport {
	private ISmFinderSupport smFinderSupport = null;
	private IUmFinderSupport umFinderSupport = null;

	@Override
	public ISmFinderSupport getSmFinderSupport() {
	    return smFinderSupport;
	}

	/**
	 * smFinderSupportを設定します。
	 * @param smFinderSupport smFinderSupport
	 */
	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
	    this.smFinderSupport = smFinderSupport;
	}

	@Override
	public IUmFinderSupport getUmFinderSupport() {
	    return umFinderSupport;
	}

	/**
	 * umFinderSupportを設定します。
	 * @param umFinderSupport umFinderSupport
	 */
	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
	    this.umFinderSupport = umFinderSupport;
	}

	@Override
	public final String nextvalByRepId() {
		return this.getRepNumberingDao().nextval();
	}

	@Override
	public IRepEntity findRepEntity( String repId ) throws TriSystemException {

		PreConditions.assertOf(repId != null, "Error in method usage : ReportId was not selected.");

		RepCondition condition = new RepCondition();
		condition.setRepId(repId);
		IRepEntity reportEntity = this.getRepDao().findByPrimaryKey( condition.getCondition() );

		if ( null == reportEntity )
			throw new TriSystemException(DcmMessageId.DCM004036F , repId);

		return reportEntity;
	}

}
