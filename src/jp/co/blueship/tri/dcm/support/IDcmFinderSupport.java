package jp.co.blueship.tri.dcm.support;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * DCM関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public interface IDcmFinderSupport extends IDcmDaoFinder {
	/**
	 * smFinderSupportを取得します。
	 * @return smFinderSupport
	 */
	public ISmFinderSupport getSmFinderSupport();

	/**
	 * umFinderSupportを取得します。
	 * @return umFinderSupport
	 */
	public IUmFinderSupport getUmFinderSupport();
	/**
	 * レポートIDを採番します。
	 *
	 * @return 採番したユニークID
	 */
	public String nextvalByRepId();

	/**
	 *
	 * @param repId Report ID
	 * @return
	 */
	public IRepEntity findRepEntity( String repId ) throws TriSystemException;

}
