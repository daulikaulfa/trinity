package jp.co.blueship.tri.dcm;

import java.util.List;

import jp.co.blueship.tri.dcm.beans.dto.IReportFileParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelApplyTicketParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlReportParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitReportParamInfo;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * リリース管理を扱うユーティリティークラスです。
 * <br>キャッシュエンティティを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ExtractMessageAddonUtil {

	

	/**
	 * パラメータリストよりビルドパッケージ作成・レポート情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IReportRelUnitReportParamInfo extractReportRelUnitReportParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IReportRelUnitReportParamInfo ) {
				return ((IReportRelUnitReportParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりリリース／資産管理台帳のレポート情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IReportRelCtlReportParamInfo extractReportRelCtlReportParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IReportRelCtlReportParamInfo ) {
				return ((IReportRelCtlReportParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりレポートファイル情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IReportFileParamInfo extractReportFileParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IReportFileParamInfo ) {
				return ((IReportFileParamInfo)obj);
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりリリース申請・レポート情報パラメタを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IReportRelApplyTicketParamInfo extractReportRelApplyTicketParamInfo( List<Object> paramList )
			throws TriSystemException {

		for ( Object obj : paramList ) {
			if ( obj instanceof IReportRelApplyTicketParamInfo ) {
				return ((IReportRelApplyTicketParamInfo)obj);
			}
		}

		return null;
	}

}
