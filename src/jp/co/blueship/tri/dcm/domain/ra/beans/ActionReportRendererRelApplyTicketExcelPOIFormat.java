package jp.co.blueship.tri.dcm.domain.ra.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.beans.IReportRenderer;
import jp.co.blueship.tri.dcm.beans.ReportRendererExcelPOIFormat;
import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;

public class ActionReportRendererRelApplyTicketExcelPOIFormat extends ReportRendererExcelPOIFormat implements IReportRenderer {

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private String language = SystemProps.TriSystemLanguage.getProperty();

	/**
	 * コンストラクタ
	 *
	 */
	public ActionReportRendererRelApplyTicketExcelPOIFormat() {

	}

	/**
	 * テンプレート定義ファイルのパスを取得する<br>
	 *
	 */
	private void getTemplateFilePaths() {

		/** テンプレートファイルのパス */
		this.setReportTemplateFile		(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.reportTemplateFileRelativePathRelApplyTicketAssetRegister ))));
		/** 出力情報ファイルのパス */
		this.setReportOutputInfoFile	(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.reportOutputInfoFileRelativePathRelApplyTicketAssetRegister ))));

	}


	private void getTemplateFilePathsV4() {

		/** テンプレートファイルのパス */
		this.setReportTemplateFile		(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.V4ReportTemplateFileRelativePathReleaseRequestReportAssetRegister ))));
		/** 出力情報ファイルのパス */
		this.setReportOutputInfoFile	(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.reportOutputInfoFileRelativePathRelApplyTicketAssetRegister ))));

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.business.common.addon.report.IReportRenderer#getRendererFormat(java.util.List)
	 */
	public void render( List<Object> paramList, boolean append ) throws TriSystemException {

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList );
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		try {

			logBusinessFlow.writeLogWithDateBegin( this.getClass().getCanonicalName() );

			TimeZone timeZone = null;
			boolean isLegacy = this.isLegacyReport(paramList);
			if ( isLegacy ){
				this.getTemplateFilePaths() ;
			} else {
				this.getTemplateFilePathsV4();
				timeZone = getTimeZone(paramList);
			}
			File reportFile = DcmDesignBusinessRuleUtils.getReportFile( paramList );

			//業務フローログにヘッダ情報を出力
			this.outLogHeader( logBusinessFlow , reportFile );

			HSSFWorkbook workbook = getReportWorkBook( reportFile, append );

			List<IEntity> entityList = DcmExtractEntityAddonUtil.extractEntities( paramList );
			parseEntityList( entityList, workbook, isLegacy, timeZone );

			writeReport( reportFile, workbook, true );

		} catch( TriRuntimeException be ) {

			logBusinessFlow.writeLog( be.getStackTraceString() );
			throw be;

		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005017S , e );
		} finally {
			logBusinessFlow.writeLogWithDateEnd( this.getClass().getCanonicalName() );
		}
	}

	public String getRendererFormat() {
		return "";
	}

	/**
	 * ログのヘッダ部分を出力する
	 *
	 */
	private void outLogHeader( LogBusinessFlow logBusinessFlow , File outputFile ) throws IOException {

		final String TAB = "\t";
		StringBuilder stb = new StringBuilder();
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		//レポート定義パス
		String reportDefPathLabel = ac.getMessage(TriLogMessage.LRM0008,
				SEP +TAB + this.getReportTemplateFile().getCanonicalPath() + SEP +
				TAB + this.getReportOutputInfoFile().getCanonicalPath() );
		stb.append( reportDefPathLabel + SEP );
		//レポート出力パス
		String reportOutputPathLabel = ac.getMessage(TriLogMessage.LRM0009,
				SEP + TAB + outputFile.getCanonicalPath() );
		stb.append( reportOutputPathLabel + SEP );

		logBusinessFlow.writeLog( stb.toString() );
	}

	/*
	 * ファイル名をロケール対応したものにする
	 */
	private String getLocaleFileName( String srcFileName ) {
		String localeFileName = srcFileName;

		int extIndex = srcFileName.lastIndexOf( '.' );
		if( extIndex > 0 ) {
			// 拡張子有無チェック
			String fileName = srcFileName.substring( 0, extIndex );
			String fileExt = srcFileName.substring( extIndex );
			int sepIndex = fileName.lastIndexOf( '_' );
			if( sepIndex > 0 ) {
				// ファイル名にロケール文字列「_**」がある場合、使用中の言語設定に変更
					fileName = fileName.substring( 0, sepIndex ) + "_" + language;
			} else {
					fileName = fileName + "_" + language;
			}
			localeFileName = fileName + fileExt;
		}
		return localeFileName;
	}
}
