package jp.co.blueship.tri.dcm.domain.ra.beans;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.ReportConverterAddonUtil;
import jp.co.blueship.tri.dcm.beans.IReportRenderer;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelApplyTicketParamInfo;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.ra.dto.FlowRelApplyTicketServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.support.FlowRelApplyTicketEditSupport;

public class ActionReportWriterByRelApplyTicket extends ActionPojoAbstract<FlowRelApplyTicketServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IReportRenderer actionReportRenderer = null;
	public void setActionReportRenderer( IReportRenderer actionReportRenderer ) {
		this.actionReportRenderer = actionReportRenderer;
	}

	private FlowRelApplyTicketEditSupport support = null;
	public void setSupport(FlowRelApplyTicketEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelApplyTicketServiceBean> execute( IServiceDto<FlowRelApplyTicketServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReportRelApplyTicketParamInfo param =
						ExtractMessageAddonUtil.extractReportRelApplyTicketParamInfo( paramList );
		if ( null == param ) {
			throw new TriSystemException( DcmMessageId.DCM005005S );
		}

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList );
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		try {
			// 下層のアクションに渡すパラメータリスト
			List<Object> entityList = new ArrayList<Object>();

			// ログ出力クラス
			entityList.add( logBusinessFlow );

			// レポート情報
			IRepEntity repEntity = DcmExtractEntityAddonUtil.extractReport( paramList );
			entityList.add( repEntity );
			// ロット情報
			ILotEntity lotEntity = DcmExtractEntityAddonUtil.extractPjtLot( paramList );
			entityList.add( lotEntity );

			// リリース申請情報
			List<IRaDto> raDtoList = this.support.findRaDto( param.getRelApplyNo() );
			entityList.addAll( this.support.convertReportRelApplyEntity( raDtoList ) );

			for (IRaDto raDto : raDtoList) {

				// ビルドパッケージ
				List<IBpEntity> bpEntities = this.support.getBuildEntityByBuildAssetRegister( raDto );
				List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(bpEntities);
				entityList.addAll( ReportConverterAddonUtil.convertReportBuildEntity( bpDtoList, support ));

			}
			entityList.add( serviceDto.getServiceBean() );
			// レポート出力
			this.writeReport( entityList );

		} catch ( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() );
			throw be;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) );
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005019S , e );
		}

		return serviceDto;

	}

	/**
	 * レポートを出力
	 *
	 * @param entity
	 * @param paramList
	 * @throws IOException
	 */
	private final void writeReport( List<Object> paramList ) {

		this.actionReportRenderer.render( paramList, false );
		String format = this.actionReportRenderer.getRendererFormat();

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "writeReport:=" + format );

	}
}
