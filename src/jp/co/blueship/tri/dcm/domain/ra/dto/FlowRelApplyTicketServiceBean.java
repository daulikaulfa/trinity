package jp.co.blueship.tri.dcm.domain.ra.dto;

import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;

public class FlowRelApplyTicketServiceBean extends ReportServiceBean {

	private static final long serialVersionUID = 1L;

	

	/** リリース申請番号 */
	private String[] selectedRelApplyNo = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** レポート番号 */
	private String reportNo = null;
    /** ダウンロードファイルパス */
	private String downloadPath = null;
    /** ダウンロードファイル名 */
	private String downloadFile = null;
    /** ダウンロードファイルサイズ */
	private String downloadSize = null;

	public String[] getSelectedRelApplyNo() {
		return selectedRelApplyNo;
	}
	public void setSelectedRelApplyNo(String[] selectedRelApplyNo) {
		this.selectedRelApplyNo = selectedRelApplyNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}

	public String getDownloadFile() {
		return downloadFile;
	}
	public void setDownloadFile( String downloadFile ) {
		this.downloadFile = downloadFile;
	}

	public String getDownloadSize() {
		return downloadSize;
	}
	public void setDownloadSize( String downloadSize ) {
		this.downloadSize = downloadSize;
	}

	public String getDownloadPath() {
		return downloadPath;
	}
	public void setDownloadPath( String downloadPath ) {
		this.downloadPath = downloadPath;
	}
}
