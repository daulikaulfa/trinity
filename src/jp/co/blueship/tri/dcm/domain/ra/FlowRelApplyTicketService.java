package jp.co.blueship.tri.dcm.domain.ra;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelApplyTicketParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelApplyTicketParamInfo;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.dcm.domain.ra.dto.FlowRelApplyTicketServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;
import jp.co.blueship.tri.rm.RmExtractEntityAddonUtils;
import jp.co.blueship.tri.rm.support.FlowRelApplyTicketEditSupport;


public class FlowRelApplyTicketService implements IDomain<FlowRelApplyTicketServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelApplyTicketEditSupport support = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowRelApplyTicketEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}
	IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IServiceDto<FlowRelApplyTicketServiceBean> execute( IServiceDto<FlowRelApplyTicketServiceBean> serviceDto ) {

		FlowRelApplyTicketServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String reportID		= paramBean.getReportId();

			if ( ! DcmReportType.RmRaReport.value().equals( reportID )) {
				return serviceDto;
			}

			// メイン処理実行
			this.executeForward( paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			//ダイアログ画面を表示させる場合,BaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException( RmMessageId.RM005054S , e );
		}
	}

	/**
	 * レポート出力依頼画面への遷移
	 *
	 * @param paramBean FlowRelApplyTicketServiceBean
	 * @param paramBean	 FlowRelApplyTicketServiceBean
	 */

	private final void executeForward( FlowRelApplyTicketServiceBean paramBean ) {

		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() )) {

			// ロットの選択チェック
			ItemCheckAddonUtils.checkLotNo	( paramBean.getSelectedLotNo() );
			// ビルドパッケージ番号の選択チェック
			ItemCheckAddonUtils.checkRelApplyNo	( paramBean.getSelectedRelApplyNo() );

			// ロット情報取得
			List<ILotDto> lotDtoList = new ArrayList<ILotDto>();
			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );
			lotDtoList.add( lotDto );
			// ユーザのグループ情報取得
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );

			// グループの存在チェック
			RelCommonAddonUtil.checkAccessableGroup( lotDtoList, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

			// アクション名タイトルラベル取得
			String flowActionTitle = LogBusinessFlow.makeFlowActionTitle( paramBean.getFlowAction() );

			LogBusinessFlow logBusinessFlow = new LogBusinessFlow();

			try {
				// アクションに渡すパラメータリスト
				List<Object> paramList = new CopyOnWriteArrayList<Object>();

				// パラメータ格納beanを格納
				paramList.add( paramBean );
				// ロット情報を格納
				paramList.add( lotDto.getLotEntity() );

				// レポート情報パラメタ
				IReportRelApplyTicketParamInfo actionParam = new ReportRelApplyTicketParamInfo();
				// レポート番号の自動採番
				paramBean.setReportNo( this.support.getDcmFinderSupport().nextvalByRepId() );
				// レポート情報パラメタに値を設定
				this.setInfoToReportRelApplyTicketParamInfo( actionParam, paramBean );
				// レポート情報パラメタを格納
				paramList.add( actionParam );

				// ログの出力処理
				this.writeLog( logBusinessFlow, flowActionTitle, lotDto.getLotEntity(), paramBean, actionParam );
				// ログ出力クラスを格納
				paramList.add( logBusinessFlow );
				// ログの削除設定を格納
				paramList.add( RmDesignEntryKeyByLogs.deleteLogAtSuccessRelApplyTicket );

				// レポート情報エンティティ
				IRepEntity entity = new RepEntity();
				// レポート情報エンティティに値を設定
				this.setInfoToReportEntity(entity, actionParam);
				// レポート情報エンティティを格納
				paramList.add( entity );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					// アクションを実行
					action.execute( innerServiceDto );
				}

				// レポート情報を更新
				this.updateReportEntity( paramList );

				// 戻り値にファイル情報を設定
				File reportFile = RmDesignBusinessRuleUtils.getReportFile( paramList );
				paramBean.setDownloadPath( reportFile.getParent() );
				paramBean.setDownloadFile( reportFile.getName() );
				paramBean.setDownloadSize( String.valueOf( reportFile.length() ));

			} catch( TriRuntimeException be ) {
				logBusinessFlow.writeLog( be.getStackTraceString() );

				throw be;
			} catch ( Exception e ) {
				logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( RmMessageId.RM005025S , e );
			} finally {
				if ( null != flowActionTitle ) {
					logBusinessFlow.writeLogWithDateEnd( flowActionTitle );
				}
			}
		}
	}

	/**
	 * レポート情報パラメタに値を設定する。
	 *
	 * @param actionParam
	 * @param paramBean
	 */
	private void setInfoToReportRelApplyTicketParamInfo(
			IReportRelApplyTicketParamInfo actionParam ,
			FlowRelApplyTicketServiceBean paramBean ) {

		actionParam.setUserId	  ( paramBean.getUserId() );
		actionParam.setUserName	  ( paramBean.getUserName() );
		actionParam.setReportNo   ( paramBean.getReportNo() );
		actionParam.setReportId	  ( paramBean.getReportId() );
		actionParam.setLotNo	  ( paramBean.getSelectedLotNo() );
		actionParam.setRelApplyNo ( paramBean.getSelectedRelApplyNo() );
	}

	/**
	 * レポート情報エンティティに値を設定する。
	 *
	 * @param entity
	 * @param actionParam
	 */
	private void setInfoToReportEntity(
			IRepEntity entity ,
			IReportRelApplyTicketParamInfo actionParam ) {

		entity.setRepId				( actionParam.getReportNo() );
		entity.setRepCtgCd			( actionParam.getReportId() );
		entity.setExecUserNm		( actionParam.getUserName() );
		entity.setExecUserId		( actionParam.getUserId() );
		entity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( actionParam.getUserName() );
		entity.setUpdUserId			( actionParam.getUserId() );
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setStsId				( DcmRepStatusId.Unprocessed.getStatusId() );
		entity.setDelStsId			( StatusFlg.off );
		entity.setLotId				( actionParam.getLotNo() );
	}

	/**
	 * ログの出力処理
	 *
	 * @param logBusinessFlow	ログ出力クラス
	 * @param flowActionTitle
	 * @param lotEntity
	 * @param paramBean
	 * @param actionParam
	 * @throws Exception
	 */
	private void writeLog(
			LogBusinessFlow logBusinessFlow ,
			String flowActionTitle ,
			ILotEntity lotEntity ,
			FlowRelApplyTicketServiceBean paramBean ,
			IReportRelApplyTicketParamInfo actionParam ) throws Exception {

		//ログの初期処理
		logBusinessFlow.makeLogPath( paramBean.getFlowAction() , lotEntity );
		//ログの開始ラベル出力
		logBusinessFlow.writeLogWithDateBegin( flowActionTitle );
		//ログのヘッダ部出力
		this.outLogHeader( logBusinessFlow , paramBean , lotEntity , actionParam );
	}

	/**
	 * ログのヘッダ部分を出力する
	 *
	 * @param logBusinessFlow	ログ出力クラス
	 * @param paramBean
	 * @param pjtLotEntity ロット情報
	 * @param actionParam
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logBusinessFlow ,
			FlowRelApplyTicketServiceBean paramBean ,
			ILotEntity pjtLotEntity ,
			IReportRelApplyTicketParamInfo actionParam ) throws Exception {

		StringBuilder stb = new StringBuilder();
		//実行者ラベル
		String userLabel = ac.getMessage( TriLogMessage.LAM0004 , paramBean.getUserName() , paramBean.getUserId() );
		stb.append( userLabel + SEP );
		// レポート種別ラベル
		String reportIdLabel = ac.getMessage( TriLogMessage.LDCM0002 , sheet.getValue( DcmDesignBeanId.reportId, actionParam.getReportId() ) );
		stb.append( reportIdLabel + SEP );
		//レポート番号ラベル
		String reportNoLabel = ac.getMessage( TriLogMessage.LDCM0003 , actionParam.getReportNo() );
		stb.append( reportNoLabel + SEP );
		//ロット番号ラベル
		String lotIdLabel = ac.getMessage( TriLogMessage.LAM0005 , pjtLotEntity.getLotNm() , pjtLotEntity.getLotId() );
		stb.append( lotIdLabel + SEP );

		//リリース申請情報
		for( String relApplyNo : actionParam.getRelApplyNo() ) {
			String relApplyNoLabel = ac.getMessage( TriLogMessage.LDCM0006 , relApplyNo );
			stb.append( relApplyNoLabel + SEP );
		}

		logBusinessFlow.writeLog( stb.toString() );
	}


	/**
	 * レポート情報を更新する
	 *
	 * @param paramList
	 */
	private final void updateReportEntity( List<Object> paramList ) {

		// レポート情報パラメタを取得
		IReportRelApplyTicketParamInfo param = ExtractMessageAddonUtil.extractReportRelApplyTicketParamInfo( paramList );
		// レポート情報エンティティを取得
		IRepEntity reportEntity = RmExtractEntityAddonUtils.extractReport( paramList );

		reportEntity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );
		reportEntity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );
		reportEntity.setStsId				( DcmRepStatusId.ReportCreated.getStatusId() );
		reportEntity.setLatestDlUserNm		( param.getUserName() );
		reportEntity.setLatestDlUserId		( param.getUserId() );
		reportEntity.setLatestDlTimestamp	( TriDateUtils.getSystemTimestamp() );

		// レポート情報テーブルに挿入
		this.support.getRepDao().insert( reportEntity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Add.getStatusId());

			support.getRepHistDao().insert( histEntity , reportEntity);
		}
	}
}
