package jp.co.blueship.tri.dcm.domain.dl.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.beans.dto.ReportSearchBean;
import jp.co.blueship.tri.dcm.domain.dl.beans.dto.RelReportListViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * リリース管理・レポート一覧画面用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelReportDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	/**
	 * 選択ページ
	 */
	private int selectPageNo = 0;
	/**
	 * ページ制御
	 */
	private IPageNoInfo pageInfoView = null;
	/**
	 * レポート情報
	 */
	private List<RelReportListViewBean> reportListViewBeanList = new ArrayList<RelReportListViewBean>();
	/**
	 * レポート 詳細検索用Bean
	 */
	private ReportSearchBean searchBean = null ;

	private String[] selectedDelete = null ;

	public List<RelReportListViewBean> getReportAssetViewBeanList() {
		return reportListViewBeanList;
	}
	public void setReportAssetViewBeanList( List<RelReportListViewBean> reportAssetViewBeanList ) {
		this.reportListViewBeanList = reportAssetViewBeanList;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView( IPageNoInfo pageInfoView ) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo( int selectPageNo ) {
		this.selectPageNo = selectPageNo;
	}
	public ReportSearchBean getSearchBean() {
		return searchBean;
	}
	public void setSearchBean(ReportSearchBean searchBean) {
		this.searchBean = searchBean;
	}
	public String[] getSelectedDelete() {
		return selectedDelete;
	}
	public void setSelectedDelete(String[] selectedDelete) {
		this.selectedDelete = selectedDelete;
	}
}
