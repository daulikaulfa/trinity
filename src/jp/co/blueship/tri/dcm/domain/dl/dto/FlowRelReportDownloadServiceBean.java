package jp.co.blueship.tri.dcm.domain.dl.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;


/**
 * 資産管理台帳ダウンロードボタン押下のフロー
 *
 */
public class FlowRelReportDownloadServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	private String reportNo = null;
	private String downloadPath = null;
	private String downloadFile = null;
	private String downloadSize = null;
	/**
	 * @return downloadFile
	 */
	public String getDownloadFile() {
		return downloadFile;
	}
	/**
	 * @param downloadFile 設定する downloadFile
	 */
	public void setDownloadFile( String downloadFile ) {
		this.downloadFile = downloadFile;
	}

	/**
	 * @return downloadSize
	 */
	public String getDownloadSize() {
		return downloadSize;
	}
	/**
	 * @param downloadSize 設定する downloadSize
	 */
	public void setDownloadSize( String downloadSize ) {
		this.downloadSize = downloadSize;
	}

	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}

	public String getDownloadPath() {
		return downloadPath;
	}
	public void setDownloadPath( String downloadPath ) {
		this.downloadPath = downloadPath;
	}
}
