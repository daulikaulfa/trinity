package jp.co.blueship.tri.dcm.domain.dl.beans.dto;

/**
 * リリース管理・レポート一覧
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class RelReportListViewBean {

	
	
	/** 
	 * レポート番号 
	 */
	private String reportNo = null;
	/** 
	 * 帳票区分 
	 */
	private String reportId = null;
	/** 
	 * 出力依頼者 
	 */
	private String executeUser = null;
	/** 
	 * 出力依頼者ＩＤ 
	 */
	private String executeUserId = null;
	/** 
	 * 出力依頼日時 
	 */
	private String executeStartDate = null;
	/** 
	 * ステータス 
	 */
	private String status = null;
	/** 
	 * ダウンロード可能フラグ 
	 */
	private boolean isDownloadEnabled = false;
	/** 
	 * ロット番号 
	 */
	private String lotId = null;
	/** 
	 * サーバ番号 
	 */
	private String serverNo = null;
	
	/**
	 * status id for trinity V4.00.00
	 */
	private String statusId = null;
	
	/**
	 * report type for trinity V4.00.00
	 */
	private String reportTypeId = null;
	
	
	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}

	public String getReportId() {
		return reportId;
	}
	public void setReportId( String reportId ) {
		this.reportId = reportId;
	}
	
	public String getExecuteUser() {
		return executeUser;
	}
	public void setExecuteUser( String executeUser ) {
		this.executeUser = executeUser;
	}
	
	public String getExecuteStartDate() {
		return executeStartDate;
	}
	public void setExecuteStartDate(String executeStartDate) {
		this.executeStartDate = executeStartDate;
	}
	
	public String getStatus() {
		return status;
	}
	public void setStatus( String status ) {
		this.status = status;
	}
	
	public boolean isDownloadEnabled() {
		return isDownloadEnabled;
	}
	public void setDownloadEnabled( boolean isDownload ) {
		this.isDownloadEnabled = isDownload;
	}
	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}
	
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo( String lotId ) {
		this.lotId = lotId;
	}
	
	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getStatusId() {
		return statusId;
	}
	public void setStatusId(String statusId) {
		this.statusId = statusId;
	}
	
	public String getReportTypeId() {
		return reportTypeId;
	}
	public void setReportTypeId(String reportTypeId) {
		this.reportTypeId = reportTypeId;
	}
}
