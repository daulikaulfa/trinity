package jp.co.blueship.tri.dcm.domain.dl.beans;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportFileParamInfo;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * レポートファイルのダウンロード処理を行います。
 *
 */
public class ActionDownloadRelReportFile extends ActionPojoAbstract<FlowRelReportDownloadServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * <br>レポートファイルが該当のパスに存在しない場合には、処理を行わずに抜ける<br>
	 * <br>トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
	 */
	@Override
	public IServiceDto<FlowRelReportDownloadServiceBean> execute( IServiceDto<FlowRelReportDownloadServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IRepEntity repEntity = DcmExtractEntityAddonUtil.extractReport( paramList );
		if ( null == repEntity ) {
			throw new TriSystemException( DcmMessageId.DCM005003S ) ;
		}

		IReportFileParamInfo paramInfo = ExtractMessageAddonUtil.extractReportFileParamInfo( paramList );
		if ( null == paramInfo ) {
			throw new TriSystemException( DcmMessageId.DCM005000S ) ;
		}


		File reportFile = paramInfo.getReportFile();
		InputStream is	= null;

		try {
			reportFile = DcmDesignBusinessRuleUtils.getReportFile( paramList );

			is = new FileInputStream( reportFile );
			byte[] contentsArray = StreamUtils.convertInputStreamToBytes( is );

			paramInfo.setContentsArray( contentsArray );

		} catch ( FileNotFoundException fnfe ) {
			LogHandler.fatal( log , fnfe ) ;
			throw new TriSystemException( DcmMessageId.DCM004035F , fnfe , reportFile.getPath() ) ;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005022S ,e) ;
		} finally {
			if ( null != is ) {
				try {
					is.close();
				} catch ( IOException ioe ) {
					throw new TriSystemException(DcmMessageId.DCM005029S, ioe);
				}
			}

		}

		return serviceDto;
	}
}
