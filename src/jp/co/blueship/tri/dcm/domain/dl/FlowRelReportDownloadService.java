package jp.co.blueship.tri.dcm.domain.dl;

import java.io.File;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.beans.dto.IReportFileParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportFileParamInfo;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDownloadServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelReportEditSupport;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * レポート／資産管理台帳のダウンロード処理を行う。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */

public class FlowRelReportDownloadService implements IDomain<FlowRelReportDownloadServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelReportEditSupport support;
	public void setSupport( FlowRelReportEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelReportDownloadServiceBean> execute( IServiceDto<FlowRelReportDownloadServiceBean> serviceDto ) {

		FlowRelReportDownloadServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( ScreenType.next.equals( paramBean.getScreenType() )) {

				String reportNo = paramBean.getReportNo();
				checkReportNo( reportNo );

				IRepEntity entity = this.getReportEntity( reportNo );
				StatusCheckAddonUtil.checkReportAssetDownload( entity );

				ILotDto lotDto = null;
				if ( TriStringUtils.isNotEmpty( entity.getLotId() )) {
					// グループの存在チェック
					lotDto = this.support.getAmFinderSupport().findLotDto( entity.getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
				}


				List<Object> paramList = new CopyOnWriteArrayList<Object>();
				paramList.add( entity );

				File reportFile = DcmDesignBusinessRuleUtils.getReportFile( paramList );

				// エージェントで作成されて、未ダウンロードのファイル
				if ( !reportFile.exists() ) {

					if ( null != lotDto ) {

						paramList.add( paramBean );

						IReportFileParamInfo paramInfo = new ReportFileParamInfo();
						paramInfo.setReportFile( reportFile );
						paramList.add( paramInfo );

						IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
								.setServiceBean( paramBean )
								.setParamList( paramList );

						for ( IDomain<IGeneralServiceBean> action : this.actions ) {
							action.execute( innerServiceDto );
						}

						paramInfo = ExtractMessageAddonUtil.extractReportFileParamInfo( paramList );
						if ( null == paramInfo ) {
							throw new TriSystemException( DcmMessageId.DCM005000S ) ;
						}

						byte[] contentsArray = paramInfo.getContentsArray();

						TriFileUtils.saveToFileStream( reportFile, StreamUtils.convertBytesToInputStreamToBytes( contentsArray ));
					} else {
						// 本体サーバにもファイルがなく、レポートエンティティにロット番号が設定されていない
						throw new TriSystemException( DcmMessageId.DCM004035F , reportFile.getPath() ) ;
					}
				}

				paramBean.setDownloadPath( reportFile.getParent() );
				paramBean.setDownloadFile( reportFile.getName() );
				paramBean.setDownloadSize( String.valueOf( reportFile.length() ));

				updateReportEntity( entity, paramBean );

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面が表示されるclassはBaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException( DcmMessageId.DCM005022S,e ) ;
		}
	}
	/**
	 * レポート番号が選択されているかをチェックする
	 * @param reportNo レポート番号
	 */

	public static void checkReportNo( String reportNo ) throws BaseBusinessException {

		if ( TriStringUtils.isEmpty( reportNo ) ) {
			throw new ContinuableBusinessException( DcmMessageId.DCM001004E );
		}
	}

	/**
	 * レポート情報を更新する
	 * @param entity レポートエンティティ
	 * @param paramBean FlowReportAssetDownloadBtnServiceBean
	 */
	private final void updateReportEntity(
			IRepEntity entity, FlowRelReportDownloadServiceBean paramBean ) {

		entity.setLatestDlUserNm	( paramBean.getUserName() );
		entity.setLatestDlUserId	( paramBean.getUserId() );
		entity.setLatestDlTimestamp	( TriDateUtils.getSystemTimestamp() );
		entity.setUpdUserNm			( paramBean.getUserName() );
		entity.setUpdUserId			( paramBean.getUserId() );
		entity.setUpdTimestamp		( TriDateUtils.getSystemTimestamp() );

		this.support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.none.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

	/**
	 * レポート情報を取得します。
	 *
	 * @param reportNo レポート番号
	 * @return 取得したレポート情報エンティティを戻します。
	 */
	public final IRepEntity getReportEntity( String reportNo ) {

		PreConditions.assertOf(reportNo != null, "Error in method usage : ReportId was not selected.");

		RepCondition condition = new RepCondition();
		condition.setRepId(reportNo);
		IRepEntity reportEntity = this.support.getRepDao().findByPrimaryKey( condition.getCondition() );

		if ( null == reportEntity )
			throw new TriSystemException(DcmMessageId.DCM004036F , reportNo);

		return reportEntity;
	}

}
