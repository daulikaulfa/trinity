package jp.co.blueship.tri.dcm.domain.dl;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;


/**
 * ステータスチェックの共通処理Class
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class StatusCheckAddonUtil {

	/**
	 * 資産管理台帳がダウンロード可能かをチェックする
	 * @param entity レポートエンティティ
	 * @throws BaseBusinessException
	 */
	public static void checkReportAssetDownload( IRepEntity entity ) throws BaseBusinessException {

//		if ( !IReportEntity.StatusId.ReportComplete.getValue().equals( entity.getBaseStatusId() )) {
		if ( !DcmRepStatusId.ReportCreated.equals( entity.getStsId() )) {
			throw new ContinuableBusinessException( DcmMessageId.DCM001005E ,entity.getRepId());
		}
	}
}
