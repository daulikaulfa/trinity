package jp.co.blueship.tri.dcm.domain.dl;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmEntityAddonUtil;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.domain.dl.beans.dto.RelReportListViewBean;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportListServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelReportEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * レポート一覧表示処理を行う。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */

public class FlowRelReportListService implements IDomain<FlowRelReportListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelReportEditSupport support = null;
	public void setSupport( FlowRelReportEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelReportListServiceBean> execute( IServiceDto<FlowRelReportListServiceBean> serviceDto ) {

		FlowRelReportListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			paramBean.setSearchBean( DBSearchBeanAddonUtil.setReportSearchBean(
										paramBean,
										paramBean.getSearchBean(),
										DcmDesignBeanId.reportListCount,
										DcmDesignBeanId.statusId ) );

			RepCondition condition = DBSearchConditionAddonUtil.getRelReportCondition();

			int count = this.support.getRepDao().count( condition.getCondition() );
			if ( count == 0 ) {
				paramBean.setInfoMessage( DcmMessageId.DCM001003E );
			}

			this.support.setReportCondition( condition, paramBean.getSearchBean() );

			IEntityLimit<IRepEntity> limit = this.support.getRepDao().find( condition.getCondition(), null, 1, 0, 0 );

			Map<String, ILotDto> repNoLotMap =
				getAccessableReportNumbers( paramBean.getUserId(), limit.getEntities() );
			((RepCondition)condition).setRepIds( repNoLotMap.keySet().toArray( new String[0] ));

			int selectedPageNo = 1;

			if ( 0 < repNoLotMap.size() ) {

				// 再検索
				selectedPageNo =
					(( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo() );

				limit = this.getReportEntityLimit( condition, selectedPageNo, paramBean );
			} else {

				limit = new EntityLimit<IRepEntity>();
				limit.setEntities( Collections.<IRepEntity>emptyList() );
			}

			if ( 0 < count && TriStringUtils.isEmpty( limit.getEntities() )) {
				paramBean.setInfoMessage( DcmMessageId.DCM001002E );
			}


			List<RelReportListViewBean> reportListViewBeanList = paramBean.getReportAssetViewBeanList();
			setRelReportListViewBeanReportEntity(
										reportListViewBeanList, limit.getEntities(), repNoLotMap );

			paramBean.setReportAssetViewBeanList	( reportListViewBeanList );
			paramBean.setPageInfoView				(
					DcmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), limit.getLimit() ));
			paramBean.setSelectPageNo				( selectedPageNo );


			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005023S,e);
		}
	}
	/**
	 * レポート一覧情報をレポート情報エンティティから設定する
	 * @param reportListViewBeanList レポート一覧情報
	 * @param entitys レポート情報のエンティティ
	 * @param repNoLotMap レポート番号とロットエンティティのマップ
	 */
	private void setRelReportListViewBeanReportEntity(
			List<RelReportListViewBean> reportListViewBeanList,
			List<IRepEntity> entities,
			Map<String, ILotDto> repNoLotMap ) {

		for ( IRepEntity entity : entities ) {

			RelReportListViewBean viewBean = new RelReportListViewBean();

			viewBean.setReportNo		( entity.getRepId() );
			viewBean.setReportId		(
					sheet.getValue( DcmDesignBeanId.reportId, entity.getRepCtgCd() ) );
			viewBean.setExecuteUser		( entity.getExecUserNm() );
			viewBean.setExecuteUserId	( entity.getExecUserId() );
			viewBean.setExecuteStartDate(
					TriDateUtils.convertViewDateFormat( entity.getProcStTimestamp() ));
			viewBean.setStatus			(
					sheet.getValue( DcmDesignBeanId.statusId, entity.getProcStsId() ));

			if ( DcmRepStatusId.ReportCreated.equals( entity.getProcStsId() )) {
				viewBean.setDownloadEnabled( true );
			}


			String lotId = null;

			if ( null != entity.getLotId() ) {
				ILotDto lotDto = repNoLotMap.get( entity.getRepId() );

				if ( null != lotDto ) {
					ILotEntity lotEntity = lotDto.getLotEntity();

					lotId = lotEntity.getLotId();
				}
			}

			viewBean.setLotNo	( lotId );
			viewBean.setServerNo( this.support.getBmFinderSupport().findBldSrvEntityByController().getBldSrvId() );

			viewBean.setStatusId(entity.getProcStsId());
			viewBean.setReportTypeId(entity.getRepCtgCd());

			reportListViewBeanList.add( viewBean );
		}
	}

	/**
	 * ユーザIDが所属しているグループがアクセス可能なレポート番号を抽出する
	 * @param paramBean
	 * @return
	 */
	private Map<String, ILotDto> getAccessableReportNumbers( String userId, List<IRepEntity> reports ) {

		// レポートを登録したロット番号を抽出
		Set<String> lotNoSet = new HashSet<String>();

		for ( IRepEntity report : reports ) {
			if ( !TriStringUtils.isEmpty( report.getLotId() )) {
				lotNoSet.add( report.getLotId() );
			}
		}


		// ロットエンティティ取得
		IJdbcCondition condition	= DBSearchConditionAddonUtil.getLotConditionByLotNo( (String[])lotNoSet.toArray( new String[0] ));
		ISqlSort sort			= new SortBuilder();
		IEntityLimit<ILotEntity> limit = this.support.getAmFinderSupport().getLotDao().find( condition.getCondition() , sort, 1 , 0 ) ;
		List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( limit.getEntities() );

		Map<String, ILotDto> lotMap = new HashMap<String, ILotDto>();
		for ( ILotDto lot : lotDto ) {
			lotMap.put( lot.getLotEntity().getLotId(), lot );
		}


		// グループID抽出
		List<IGrpUserLnkEntity> groupUserEntitys = support.getUmFinderSupport().findGrpUserLnkByUserId( userId );
		Set<String> groupIdSet = DcmEntityAddonUtil.getGroupIdSet( groupUserEntitys );


		// 絞り込み
		Map<String, ILotDto> reportNoLotMap = new HashMap<String, ILotDto>();
		for ( IRepEntity report : reports ) {

			// ロット番号なし＝対応前のロット＝アクセス可能
			if ( TriStringUtils.isEmpty( report.getLotId() )) {

				reportNoLotMap.put( report.getRepId(), null );

			} else {

				ILotDto lot = lotMap.get( report.getLotId() );

				// 削除されたロット
				if ( null == lot ) continue;

				// グループ情報なし＝対応前のロット＝アクセス可能
				if ( TriStringUtils.isEmpty( lot.getLotGrpLnkEntities() )) {

					reportNoLotMap.put( report.getRepId(), lot );

				} else {

					for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

						// 自分の所属しているグループの一部がアクセス可能グループに一致＝アクセス可能
						if ( groupIdSet.contains( group.getGrpId() )) {
							reportNoLotMap.put( report.getRepId(), lot );
							break;
						}
					}
				}
			}
		}

		return reportNoLotMap;
	}


	/**
	 * レポート出力情報を取得する。
	 *
	 * @param condition
	 * @param selectPageNo
	 * @param paramBean
	 * @return
	 */
	private final IEntityLimit<IRepEntity> getReportEntityLimit(
									IJdbcCondition condition,
									int selectPageNo,
									FlowRelReportListServiceBean paramBean ) {

		int maxPageNumber = sheet.intValue(
								DcmDesignEntryKeyByReport.maxPageNumberByReportAssetList );
		ISqlSort sort = DBSearchSortAddonUtil.getReportSortFromDesignDefineByReportAssetList();

		this.support.setReportCondition( (RepCondition)condition, paramBean.getSearchBean() );

		int maxHitLimit = 0;

		if ( TriStringUtils.isNotEmpty( paramBean.getSearchBean().getSelectedCount() ) ) {
			maxHitLimit = Integer.parseInt( paramBean.getSearchBean().getSelectedCount() );
		}

		IEntityLimit<IRepEntity> limit =
				this.support.getRepDao().find( condition.getCondition(), sort, selectPageNo, maxPageNumber , maxHitLimit );

		return limit;
	}

}
