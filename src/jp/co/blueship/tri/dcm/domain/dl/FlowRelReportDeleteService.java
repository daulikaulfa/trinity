package jp.co.blueship.tri.dcm.domain.dl;

import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepCondition;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelReportEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.DcmRepStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * レポート削除および一覧表示処理を行う。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */

public class FlowRelReportDeleteService implements IDomain<FlowRelReportDeleteServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelReportEditSupport support = null;
	public void setSupport( FlowRelReportEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = null;
	public void setActions( List<IDomain<IGeneralServiceBean>> actions ) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelReportDeleteServiceBean> execute( IServiceDto<FlowRelReportDeleteServiceBean> serviceDto ) {

		FlowRelReportDeleteServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String[] selectedDelete = paramBean.getSelectedDelete() ;
			if( true == TriStringUtils.isEmpty( selectedDelete ) ) {
				throw new ContinuableBusinessException( DcmMessageId.DCM001007E );
			}

			List<IRepEntity> reportEntityArray = getReportEntityArray( selectedDelete ) ;

			//レコードの削除
			this.deleteReportEntity( reportEntityArray , paramBean ) ;


			// ロット番号でレポートエンティティを取りまとめ
			Map<String, List<IRepEntity>> lotNoReportMap = getLotNoReportMap( reportEntityArray );
			List<ILotEntity> lotEntities =
				this.support.getPjtLotEntityArray( (String[])lotNoReportMap.keySet().toArray( new String[0] ));
			List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto(lotEntities);

			// グループの存在チェック
			// 今回の対応前のレポートはロット番号が取れないのでチェックできない
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			for ( ILotDto lot : lotDto ) {
				RelCommonAddonUtil.checkAccessableGroup( lot, this.support.getUmFinderSupport().getGrpDao(), groupUsers );
			}

			//レポートファイルの削除
			this.deleteReportData( lotEntities, lotNoReportMap, paramBean ) ;

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(DcmMessageId.DCM005027S,e);
		}
	}

	/**
	 * レポート番号の配列をもとに、レポート情報の配列を取得する<br>
	 * @param reportNoArray
	 * @return
	 */
	private List<IRepEntity> getReportEntityArray( String[] reportNoArray ) {

		RepCondition condition = new RepCondition() ;
		condition.setRepIds( reportNoArray );

		ISqlSort sort = new SortBuilder() ;

		IEntityLimit<IRepEntity> limit = this.support.getRepDao().find( condition.getCondition() , sort, 1 , 0 ) ;
		List<IRepEntity> entityArray = limit.getEntities() ;

		return entityArray ;
	}

	/**
	 *
	 * @param reportEntityArray
	 * @param paramBean
	 */
	private void deleteReportEntity( List<IRepEntity> reportEntityArray , FlowRelReportDeleteServiceBean paramBean ) {

		for ( IRepEntity reportEntity : reportEntityArray ) {

			this.updateReportEntity( reportEntity , paramBean ) ;
		}
	}

	/**
	 * レポート情報を更新する（削除状態）
	 * @param entity レポートエンティティ
	 * @param paramBean FlowReportAssetDownloadBtnServiceBean
	 */
	private final void updateReportEntity(
			IRepEntity entity, FlowRelReportDeleteServiceBean paramBean ) {

		entity.setDelStsId			( StatusFlg.on );
		entity.setStsId				( DcmRepStatusId.ReportRemoved.getStatusId() );
		entity.setDelTimestamp		( TriDateUtils.getSystemTimestamp() );
		entity.setDelUserNm			( paramBean.getUserName() );
		entity.setDelUserId			( paramBean.getUserId() );

		this.support.getRepDao().update( entity );

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Remove.getStatusId());

			support.getRepHistDao().insert( histEntity , entity);
		}
	}

	/**
	 * レポートファイルを削除する。
	 * <br>レポートファイルが該当のパスに存在しない場合には、処理を行わずに抜ける<br>
	 * <br>トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
	 * @param lotEntities ロットエンティティの配列
	 * @param lotNoRelMap ロット番号とリリースエンティティリストのマップ
	 */
	private void deleteReportData(
			List<ILotEntity> lotEntities, Map<String, List<IRepEntity>> lotNoReportMap, FlowRelReportDeleteServiceBean paramBean ) throws RemoteException {

		try {

			// 続いて本体サーバ
			// レポートのダウンロード時に本体サーバにエージェントからコピーするため、全部消しにかかる
			List<IRepEntity> allRepList = new ArrayList<IRepEntity>();
			for ( List<IRepEntity> repList : lotNoReportMap.values() ) {
				allRepList.addAll( repList );
			}

			List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
			paramList.add( (IRepEntity[])allRepList.toArray( new IRepEntity[0] ));
			// 本体サーバIDを取得
			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();

			paramBean.setLockServerId( srvEntity.getBldSrvId() );

			paramList.add( paramBean );

			IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
					.setServiceBean( paramBean )
					.setParamList( paramList );

			for ( IDomain<IGeneralServiceBean> action : this.actions ) {
				action.execute( innerServiceDto );
			}
		} finally {
			paramBean.setLockServerId( null );
		}
	}

	/**
	 * ロット番号-レポートエンティティのリストのマップを取得する
	 * @param entities
	 * @return
	 */
	private Map<String, List<IRepEntity>> getLotNoReportMap( List<IRepEntity> entities ) {

		Map<String, List<IRepEntity>> lotNoReportMap = new HashMap<String, List<IRepEntity>>() ;
		for( IRepEntity repEntity : entities ) {

			String lotId = repEntity.getLotId();
			// 今回の対応前のデータ
			if ( TriStringUtils.isEmpty( lotId )) {
				lotId = "dummy";
			}

			if ( !lotNoReportMap.containsKey( lotId )) {
				lotNoReportMap.put( lotId, new ArrayList<IRepEntity>() ) ;
			}

			List<IRepEntity> repList = lotNoReportMap.get( lotId );
			repList.add( repEntity );
		}

		return lotNoReportMap;

	}
}
