package jp.co.blueship.tri.dcm.domain.dl.beans;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.dl.dto.FlowRelReportDeleteServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * レポートファイルの削除処理を行います。
 *
 */
public class ActionDeleteRelReportFile extends ActionPojoAbstract<FlowRelReportDeleteServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * <br>レポートファイルが該当のパスに存在しない場合には、処理を行わずに抜ける<br>
	 * <br>トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
	 */
	@Override
	public IServiceDto<FlowRelReportDeleteServiceBean> execute( IServiceDto<FlowRelReportDeleteServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IRepEntity[] repEntities = DcmExtractEntityAddonUtil.extractReportArray( paramList );
		if ( TriStringUtils.isEmpty( repEntities ) ) {
			throw new TriSystemException( DcmMessageId.DCM005002S ) ;
		}

		try {
			for( IRepEntity reportEntity : repEntities ) {
				//StatusCheckAddonUtil.checkReportAssetDownload( reportEntity );

				List<Object> childParamList = new ArrayList<Object>();
				childParamList.add( reportEntity );
				File reportFile = DcmDesignBusinessRuleUtils.getReportFile( childParamList );

				try {
					//削除処理（パスが存在する場合のみ）
					if( reportFile.exists() ) {
						reportFile.delete() ;
					}

				} catch( Exception e ) {
					//削除されたすべてのレポートの削除をトライする
					LogHandler.fatal( log , e ) ;
				}
			}
		} catch( Exception e ) {
			//トランザクション全体の影響を考慮し、当メソッド内で発生したExceptionをthrowしない。
			LogHandler.fatal( log , e ) ;
		}

		return serviceDto;

	}
}
