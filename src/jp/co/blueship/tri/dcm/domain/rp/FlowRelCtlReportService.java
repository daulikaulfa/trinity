package jp.co.blueship.tri.dcm.domain.rp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlReportParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelCtlReportParamInfo;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.rp.dto.FlowRelCtlReportServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.constants.RelCtlScreenID;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.support.FlowRelCtlReportEditSupport;


/**
 * FlowRelCtlReportServiceイベントのサービスClass
 * <br>
 * <p>
 * リリース／作成レポートのレポート出力処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelCtlReportService implements IDomain<FlowRelCtlReportServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelCtlReportEditSupport support = null;
	public void setSupport( FlowRelCtlReportEditSupport support ) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();
	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}
	IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	@Override
	public IServiceDto<FlowRelCtlReportServiceBean> execute( IServiceDto<FlowRelCtlReportServiceBean> serviceDto ) {

		FlowRelCtlReportServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String reportID	= paramBean.getReportId();

			if ( ! DcmReportType.RmRpReport.value().equals( reportID )) {
				return serviceDto;
			}

			this.executeForward( paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面が表示されるclassはBaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException( DcmMessageId.DCM005024S , e );
		}
	}

	/**
	 * レポート出力依頼画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private final void executeForward( FlowRelCtlReportServiceBean paramBean ) {

		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() )) {

			ItemCheckAddonUtils.checkRelNo( paramBean.getSelectedRelNo() );

			String[] lotId = getLotNo( paramBean );
			checkSelectedOneLotNo( lotId );
			paramBean.setSelectedLotNo	( lotId[0] );
			paramBean.setSelectedLotNo	( lotId[0] );

			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );

			// グループの存在チェック
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );


			IBldEnvEntity relEnvEntity = null;
			if ( ! TriStringUtils.isEmpty( paramBean.getSelectedRelEnvNo() ))
				relEnvEntity = this.support.getBmFinderSupport().findBldEnvEntity( paramBean.getSelectedRelEnvNo() );


			//下層のアクションを実行する
			IReportRelCtlReportParamInfo actionParam = new ReportRelCtlReportParamInfo();

			LogBusinessFlow logBusinessFlow = new LogBusinessFlow() ;
			String flowActionTitle = null ;

			try {
				List<Object> paramList = new CopyOnWriteArrayList<Object>();
				paramBean.setReportNo( this.support.getDcmFinderSupport().nextvalByRepId() );

				actionParam.setUserId	( paramBean.getUserId() );
				actionParam.setUserName	( paramBean.getUserName() );
				actionParam.setRepId	( paramBean.getReportNo() );
				actionParam.setRepCtgCd	( paramBean.getReportId() );
				actionParam.setLotNo	( paramBean.getSelectedLotNo() );
				actionParam.setRelEnvNo	( paramBean.getSelectedRelEnvNo() );
				actionParam.setRelNo	( paramBean.getSelectedRelNo() );

				paramList.add( actionParam );
				paramList.add( lotDto );

				if ( ! TriStringUtils.isEmpty(paramBean.getSelectedRelEnvNo()) )
					paramList.add( relEnvEntity );

				//ログの初期処理
				logBusinessFlow.makeLogPath( paramBean.getFlowAction() , lotDto.getLotEntity() ) ;
				//ログの開始ラベル出力
				flowActionTitle = LogBusinessFlow.makeFlowActionTitle( paramBean.getFlowAction() ) ;
				logBusinessFlow.writeLogWithDateBegin( flowActionTitle ) ;
				//ログのヘッダ部出力
				this.outLogHeader( logBusinessFlow , paramBean , lotDto.getLotEntity() , actionParam ) ;

				paramList.add( logBusinessFlow ) ;
				paramList.add( RmDesignEntryKeyByLogs.deleteLogAtSuccessRelCtlReport ) ;

				if ( null == paramBean.getLockServerId() ) {
					IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
					paramBean.setLockServerId( srvEntity.getBldSrvId() );
				}
				paramList.add( paramBean );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}

			} catch( TriRuntimeException be ) {
				logBusinessFlow.writeLog( be.getStackTraceString() ) ;

				throw be ;
			} catch ( Exception e ) {
				logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( DcmMessageId.DCM005018S , e );
			} finally {
				if ( null != flowActionTitle ) {
					logBusinessFlow.writeLogWithDateEnd( flowActionTitle ) ;
				}

			}
		}
	}

	/**
	 * ロット番号を取得する
	 * @param paramBean FlowRelReportAssetRegisterServiceBean
	 * @return ロット番号
	 */
	private String[] getLotNo( FlowRelCtlReportServiceBean paramBean ) {

		String referer	= paramBean.getReferer();
		String[] lotId	= null;

		if ( referer.equals( RelCtlScreenID.HISTORY_LIST )) {

			String[] relNo = paramBean.getSelectedRelNo();

			IRpEntity[] entities = this.support.getRelEntity( relNo );

			Set<String> lotNoSet = new HashSet<String>();
			for ( IRpEntity entity : entities ) {

				// IRelEntity#lotNo追加前に作成されたデータ対応
				if ( !TriStringUtils.isEmpty( entity.getLotId() ))
					lotNoSet.add( entity.getLotId() );
			}

			if ( 0 != lotNoSet.size() ) {
				lotId = (String[])lotNoSet.toArray( new String[0] );
			}

		} else {
			// IRelEntity#lotNo追加前に作成されたデータ対応
			if ( !TriStringUtils.isEmpty( paramBean.getSelectedLotNo() ))
				lotId = new String[]{ paramBean.getSelectedLotNo() };
		}


		return lotId;
	}

	/**
	 * ロット番号が１つだけ選択されているかどうかチェックする
	 *
	 * @param lotId ロット番号
	 */

	private static void checkSelectedOneLotNo( String[] lotId ) {

		if ( TriStringUtils.isEmpty( lotId ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001002E );
		} else if ( 1 < lotId.length ) {
			throw new ContinuableBusinessException( DcmMessageId.DCM001006E );
		}
	}

	/**
	 * /**
	 * ログのヘッダ部分を出力する
	 * @param logBusinessFlow	ログ出力クラス
	 * @param paramBean
	 * @param pjtLotEntity ロット情報
	 * @param actionParam
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logBusinessFlow ,
			FlowRelCtlReportServiceBean paramBean ,
			ILotEntity pjtLotEntity ,
			IReportRelCtlReportParamInfo actionParam ) throws Exception {

		StringBuilder stb = new StringBuilder() ;
		//実行者ラベル
		String userLabel = ac.getMessage( TriLogMessage.LAM0004 , paramBean.getUserName() , paramBean.getUserId() );
		stb.append( userLabel + SEP ) ;
		//レポート種別ラベル
		String reportIdLabel = ac.getMessage( TriLogMessage.LDCM0002 , sheet.getValue( DcmDesignBeanId.reportId, actionParam.getRepCtgCd() ) );
		stb.append( reportIdLabel + SEP ) ;
		//レポート番号ラベル
		String reportNoLabel = ac.getMessage( TriLogMessage.LDCM0003 , actionParam.getRepId() );
		stb.append( reportNoLabel + SEP ) ;

		//リリース環境番号
		String relEnvNoLabel = ac.getMessage( TriLogMessage.LDCM0004 , actionParam.getRelEnvNo() );
		stb.append( relEnvNoLabel + SEP ) ;

		//リリースパッケージ情報
		for( String relNo : actionParam.getRelNo() ) {
			String relNoLabel = ac.getMessage( TriLogMessage.LDCM0005 , relNo );
			stb.append( relNoLabel + SEP ) ;
		}

		//ロット番号ラベル
		//String lotIdLabel = 	"[ ロット番号 ]" + " " + pjtLotEntity.getLotName() + "(" + pjtLotEntity.getLotNo() + ")" ;
		//stb.append( lotIdLabel + lineSeparator ) ;

		logBusinessFlow.writeLog( stb.toString() ) ;
	}
}
