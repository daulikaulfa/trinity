package jp.co.blueship.tri.dcm.domain.rp.beans;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.TimeZone;

import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.beans.IReportRenderer;
import jp.co.blueship.tri.dcm.beans.ReportRendererExcelPOIFormat;
import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.rm.RmDesignBusinessRuleUtils;

/**
 * リリースパッケージ／作成レポートをエクセル形式(POI)でレンダリングするクラスです。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionReportRendererReleaseAssetRegisterExcelPOIFormat extends ReportRendererExcelPOIFormat implements IReportRenderer {

	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private String language = SystemProps.TriSystemLanguage.getProperty();

	/**
	 * コンストラクタ
	 *
	 */
	public ActionReportRendererReleaseAssetRegisterExcelPOIFormat() {

	}

	/**
	 * テンプレート定義ファイルのパスを取得する<br>
	 *
	 */
	private void getTemplateFilePaths() {

		/** テンプレートファイルのパス */
		this.setReportTemplateFile		(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.reportTemplateFileRelativePathRelCtlReportAssetRegister ))));
		/** 出力情報ファイルのパス */
		this.setReportOutputInfoFile	(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.reportOutputInfoFileRelativePathRelCtlReportAssetRegister ))));

	}

	private void getTemplateFilePathsV4() {

		/** テンプレートファイルのパス */
		this.setReportTemplateFile		(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.V4ReportTemplateFileRelativePathReleasePackageReportAssetRegister ))));
		/** 出力情報ファイルのパス */
		this.setReportOutputInfoFile	(
				new File(	RmDesignBusinessRuleUtils.getTemplatePath().getPath(),
						getLocaleFileName( sheet.getValue( DcmDesignEntryKeyByReport.V4ReportOutputInfoFileRelativePathReleasePackageReportAssetRegister ))));

	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.business.common.addon.report.IReportRenderer#getRendererFormat(java.util.List)
	 */
	public void render( List<Object> paramList, boolean append ) throws TriSystemException {

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList ) ;
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		try {
			logBusinessFlow.writeLogWithDateBegin( this.getClass().getCanonicalName() ) ;

			TimeZone timeZone = null;
			boolean isLegacy = this.isLegacyReport(paramList);
			if ( isLegacy ){
				this.getTemplateFilePaths() ;
			} else {
				this.getTemplateFilePathsV4();
				timeZone = getTimeZone(paramList);
			}

			File reportFile			= DcmDesignBusinessRuleUtils.getReportFile( paramList );

			//業務フローログにヘッダ情報を出力
			this.outLogHeader( logBusinessFlow , reportFile ) ;

			HSSFWorkbook workbook	= getReportWorkBook( reportFile, append );

			List<IEntity> entityList = DcmExtractEntityAddonUtil.extractEntities( paramList );
			parseEntityList( entityList, workbook, isLegacy, timeZone );

			writeReport( reportFile, workbook, true );

		} catch( TriRuntimeException be ) {

			logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			throw be ;

		} catch ( Exception e ) {

			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005017S , e );

		} finally {
			logBusinessFlow.writeLogWithDateEnd( this.getClass().getCanonicalName() ) ;
		}
	}

	public String getRendererFormat() {
		return "";
	}

	/**
	 * 与えられた資産申請情報の配列から、資産１件ごとに分割した資産申請情報の配列を作成する<br>
	 *
	 * 【前提条件】：assetApplyEntitiesの配列は、承認日時で昇順にソートされていることとする。<br>
	 * @param assetApplyEntities 資産申請情報の配列
	 * @return 資産１件ごとに分割した資産申請情報の配列
	 */
//	private IAssetApplyEntity[] modifyAssetApplyEntities( IAssetApplyEntity[] assetApplyEntities ) {
//
//		Map<String, IAssetApplyEntity> pathApplyMap = new HashMap<String, IAssetApplyEntity>();
//		Map<String, IAssetFileEntity> pathAssetMap = new HashMap<String, IAssetFileEntity>();
//
//		for ( IAssetApplyEntity assetApplyEntity : assetApplyEntities ) {
//			for ( IAssetFileEntity assetFileEntity: assetApplyEntity.getReturnApplyFilePath() ) {//資産返却
//				pathApplyMap.put(assetFileEntity.getFilePath(), assetApplyEntity);
//				pathAssetMap.put(assetFileEntity.getFilePath(), assetFileEntity);
//			}
//			for ( IAssetFileEntity assetFileEntity: assetApplyEntity.getDelApplyFilePath() ) {//資産削除
//				pathApplyMap.put(assetFileEntity.getFilePath(), assetApplyEntity);
//				pathAssetMap.put(assetFileEntity.getFilePath(), assetFileEntity);
//			}
//		}
//
//		List<IAssetApplyEntity> applyEntityList = new ArrayList<IAssetApplyEntity>() ;
//
//		for ( IAssetFileEntity assetFileEntity : pathAssetMap.values() ) {
//			IAssetApplyEntity assetApplyEntity = pathApplyMap.get(assetFileEntity.getFilePath());
//
//			IAssetApplyEntity assetApplyEntityWork = new AssetApplyEntity() ;
//			assetApplyEntityWork.setApplyId( assetApplyEntity.getApplyId() ) ;
//			assetApplyEntityWork.setApplyNo( assetApplyEntity.getApplyNo() ) ;
//			assetApplyEntityWork.setGroupName( assetApplyEntity.getGroupName() ) ;
//			assetApplyEntityWork.setLendApplyFilePath( new IAssetFileEntity[]{ assetFileEntity } ) ;//返却も削除も、共通して貸出に入れてしまう
//
//			applyEntityList.add( assetApplyEntityWork ) ;
//		}
//		return applyEntityList.toArray( new IAssetApplyEntity[ 0 ] ) ;
//	}

	/**
	 *
	 */
	private void outLogHeader( LogBusinessFlow logBusinessFlow , File outputFile ) throws IOException {

		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		StringBuilder stb = new StringBuilder() ;

//		レポート定義パス
		String reportDefPathLabel = SEP + "\t" + this.getReportTemplateFile().getCanonicalPath() + SEP +
						"\t" + this.getReportOutputInfoFile().getCanonicalPath() + SEP  ;
		stb.append( ac.getMessage( TriLogMessage.LRM0008 , reportDefPathLabel ) ) ;
		//レポート出力パス
		String reportOutputPathLabel = SEP + "\t" + outputFile.getCanonicalPath() + SEP ;
		stb.append( ac.getMessage( TriLogMessage.LRM0009 , reportOutputPathLabel ) ) ;

		logBusinessFlow.writeLog( stb.toString() ) ;
	}

	/*
	 * ファイル名をロケール対応したものにする
	 */
	private String getLocaleFileName( String srcFileName ) {
		String localeFileName = srcFileName;

		int extIndex = srcFileName.lastIndexOf( '.' );
		if( extIndex > 0 ) {
			// 拡張子有無チェック
			String fileName = srcFileName.substring( 0, extIndex );
			String fileExt = srcFileName.substring( extIndex );
			int sepIndex = fileName.lastIndexOf( '_' );
			if( sepIndex > 0 ) {
				// ファイル名にロケール文字列「_**」がある場合、使用中の言語設定に変更
					fileName = fileName.substring( 0, sepIndex ) + "_" + language;
			} else {
					fileName = fileName + "_" + language;
			}
			localeFileName = fileName + fileExt;
		}
		return localeFileName;
	}

}
