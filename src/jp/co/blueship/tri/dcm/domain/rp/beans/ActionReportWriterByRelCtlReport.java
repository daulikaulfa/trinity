package jp.co.blueship.tri.dcm.domain.rp.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmTaskAddonUtils;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowResItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResCondition;
import jp.co.blueship.tri.bm.dao.taskflow.utils.TaskMappingUtils;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmEntityAddonUtil;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.ReportAddonUtil;
import jp.co.blueship.tri.dcm.ReportConverterAddonUtil;
import jp.co.blueship.tri.dcm.ReportFileAddonUtil;
import jp.co.blueship.tri.dcm.ReportViewInfoAddonUtil;
import jp.co.blueship.tri.dcm.beans.ActionReportArchiveFileExtractor;
import jp.co.blueship.tri.dcm.beans.IReportRenderer;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportAssetApplyInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlArchiveInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlCreateLogInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelCtlReportParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelProcessInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportStringInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelCtlArchiveInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelCtlCreateLogInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelProcessInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportStringInfo;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.rp.dto.FlowRelCtlReportServiceBean;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelease;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType.Target;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.support.FlowRelCtlReportEditSupport;

/**
 * 選択されたリリースのレポートを出力します。<br>
 * <br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionReportWriterByRelCtlReport extends ActionPojoAbstract<FlowRelCtlReportServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private Map<String,String> variableMap = null;

	private IReportRenderer actionReportRenderer = null;
	public void setActionReportRenderer( IReportRenderer actionReportRenderer ) {
		this.actionReportRenderer = actionReportRenderer;
	}

	private List<ActionReportArchiveFileExtractor> extractorList = null;
	public final void setArchiveFileExtractorList( List<ActionReportArchiveFileExtractor> extractorList ) {
		this.extractorList = extractorList;
	}

	private FlowRelCtlReportEditSupport support = null;
	public void setSupport( FlowRelCtlReportEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelCtlReportServiceBean> execute( IServiceDto<FlowRelCtlReportServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReportRelCtlReportParamInfo param =
						ExtractMessageAddonUtil.extractReportRelCtlReportParamInfo( paramList );
		if ( null == param ) {
			throw new TriSystemException(DcmMessageId.DCM005005S);
		}

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList ) ;
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		IRepEntity reportEntity			= null;

		try {

			reportEntity = DcmExtractEntityAddonUtil.extractReport( paramList );

			ILotDto lotDto	 = DcmExtractEntityAddonUtil.extractLotDto( paramList );

			List<Object> entityList = new ArrayList<Object>();
			entityList.add( reportEntity );
			entityList.add( lotDto.getLotEntity() );


			List<IRpEntity> relEntities = this.support.getRelEntityByRelAssetRegister( param.getRelNo() );
			List<IRpDto> rpDtoList = this.support.findRpDto(relEntities);
			entityList.addAll( convertReportRelEntity( relEntities ));

			IBldEnvEntity relEnvEntity = getRelEnvEntity( this.support, relEntities );
			entityList.add( relEnvEntity );

			for ( IRpDto relEntity : rpDtoList ) {

				List<IBpEntity> bpEntities = this.support.getBuildEntityByRelAssetRegister( DcmEntityAddonUtil.getBuildNo( relEntity ));
				List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(bpEntities);
				Map<String, String> buildNoMergeOrderMap = buildNoMergeOrderMap( relEntity );

				entityList.addAll( ReportConverterAddonUtil.convertReportBuildEntity( bpDtoList, buildNoMergeOrderMap, support ));

				// 異なるRUの中に、同じPJTがあるとDaoでの取得時にまとめられてしまうので、
				// １つずつRUをまわして取得する
				for ( IBpDto buildEntity : bpDtoList ) {

					// 変更管理
					IPjtEntity[] pjtEntities = this.support.getPjtEntityByRelAssetRegister( buildEntity );
					entityList.addAll( ReportConverterAddonUtil.convertReportPjtEntity( pjtEntities ));

					// 申請情報
					List<IReportAssetApplyInfo> reportApplyEntityList =
										getReportAssetApplyEntity( buildEntity, this.support );
					entityList.addAll( reportApplyEntityList );
				}

				// 処理結果（概要）
				List<IReportRelProcessInfo> reportRelProcessEntityList =
												getReportRelProcessEntity( relEntity, this.support );
				entityList.addAll( reportRelProcessEntityList );


				// アーカイブ展開
				IReportRelCtlArchiveInfo[] archiveEntities =
					getReportRelCtlArchiveEntity( this.support, lotDto.getLotEntity(), relEntity );
				entityList.addAll( FluentList.from(archiveEntities).asList());

				// RelTaskRecの中身
				IReportRelCtlCreateLogInfo createLogEntity = new ReportRelCtlCreateLogInfo();
				createLogEntity.setRelNo	( relEntity.getRpEntity().getRpId() );
				createLogEntity.setLog		( getRelTaskRecContents( relEntity.getRpEntity(), this.support ));
				entityList.add( createLogEntity );
			}

			entityList.add( logBusinessFlow ) ;
			entityList.add( serviceDto.getServiceBean() );
			this.writeReport( entityList );

			param.setSuccess( true );

		} catch ( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			throw be ;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			LogHandler.fatal( log , e ) ;
			param.setSuccess( false );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005020S , e );

		}

		return serviceDto;

	}


	/**
	 * リリースエンティティをレポート用に変換する
	 * @param entities
	 * @return
	 */
	private List<IReportRelInfo> convertReportRelEntity( List<IRpEntity> entities ) {

		List<IReportRelInfo> reportEntityList = new ArrayList<IReportRelInfo>();
		BpCondition condition = new BpCondition();

		for ( IRpEntity entity : entities ) {

			IReportRelInfo reportEntity = new ReportRelInfo();
			TriPropertyUtils.copyProperties( reportEntity, entity );

			condition.setLotId( entity.getLotId() );
			List<IBpEntity> bpEntityList = this.support.getBmFinderSupport().getBpDao().find(condition.getCondition());
			reportEntity.setBuild( bpEntityList.toArray(new IBpEntity[0]) );
			reportEntityList.add( reportEntity );
		}

		return reportEntityList;

	}

	/**
	 * リリース環境エンティティを取得する。
	 * @param finder
	 * @param relEntities
	 * @return
	 * @throws TriSystemException
	 */
	private IBldEnvEntity getRelEnvEntity(
			FlowRelCtlReportEditSupport finder, List<IRpEntity> relEntities ) throws TriSystemException {

		Set<String> relEnvNoSet = new HashSet<String>();
		for ( IRpEntity relEntity : relEntities ) {
			relEnvNoSet.add( relEntity.getBldEnvId() );
		}

		// 環境を選択してからレポート出力の画面に入るので、環境は１つのはず。
		if ( 1 < relEnvNoSet.size() ) {
			throw new TriSystemException( DcmMessageId.DCM004000F );
		}

		return finder.getBmFinderSupport().findBldEnvEntity( relEntities.get(0).getBldEnvId() );

	}

	/**
	 * レポートを出力
	 *
	 * @param entity
	 * @param paramList
	 * @throws IOException
	 */
	private final void writeReport( List<Object> paramList ) {

		this.actionReportRenderer.render( paramList, false );
		String format = this.actionReportRenderer.getRendererFormat();

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "writeReport:=" + format );

	}

	/**
	 * レポート用申請情報エンティティを取得する。
	 * @param buildEntity ビルドエンティティ
	 * @param finder
	 * @return 申請番号-レポート用申請情報エンティティのマップ
	 */
	private List<IReportAssetApplyInfo> getReportAssetApplyEntity(
			IBpDto buildEntity, FlowRelCtlReportEditSupport finder ) {

		Map<String, IAssetCounterInfo> applyNoCounterMap =
			BmTaskAddonUtils.getAssetCount( buildEntity, finder.getBmFinderSupport(), null );

		IAreqEntity[] entities = finder.getAssetApplyEntityByRelAssetRegister( buildEntity );

		return ReportConverterAddonUtil.convertReportAssetApplyEntity( entities, applyNoCounterMap );
	}

	/**
	 * レポート用リリースプロセスエンティティを取得する。
	 * @param relEntity リリースエンティティ
	 * @param finder
	 * @return レポート用リリースプロセスエンティティ
	 */
	private List<IReportRelProcessInfo> getReportRelProcessEntity(
			IRpDto relEntity, FlowRelCtlReportEditSupport finder ) {

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId( relEntity.getRpEntity().getProcId() );

		ITaskFlowProcEntity[] entities = finder.getBmFinderSupport().getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);

		List<IReportRelProcessInfo> reportEntityList = new ArrayList<IReportRelProcessInfo>();

		Map<String, ITaskFlowResEntity> taskRecEntityMap = this.getTaskRecMap( relEntity.getRpEntity() );

		for ( ITaskFlowProcEntity entity : entities ) {

			IReportRelProcessInfo reportEntity = new ReportRelProcessInfo();

			TriPropertyUtils.copyProperties( reportEntity, entity );

			reportEntity.setProcessName	( BmTaskAddonUtils.getProcessName( entity, taskRecEntityMap ));
			BldSrvCondition bcondition = new BldSrvCondition();
			bcondition.setBldSrvId(entity.getBldSrvId());
			reportEntity.setServerName	(( finder.getBmFinderSupport().getBldSrvDao().find( bcondition.getCondition() )).get(0).getBldSrvNm() );

			reportEntityList.add( reportEntity );
		}

		return reportEntityList;
	}

	/**
	 *
	 */
	private Map<String, ITaskFlowResEntity> getTaskRecMap( IRpEntity relEntity ) {
		Map<String, ITaskFlowResEntity> taskRecEntityMap = new LinkedHashMap<String, ITaskFlowResEntity>();

		TaskFlowResCondition condition = new TaskFlowResCondition();
		condition.setDataId( relEntity.getRpId() );

		SortBuilder sort = new SortBuilder();
		sort.setElement(TaskFlowResItems.dataId, TriSortOrder.Asc, 1);

		IEntityLimit<ITaskFlowResEntity> limit =
			this.support.getBmFinderSupport().getTaskFlowResDao().find( condition.getCondition(), sort, 1, 0 );

		for ( ITaskFlowResEntity taskRecEntity : limit.getEntities() ) {
			taskRecEntityMap.put(taskRecEntity.getTaskFlowId(), taskRecEntity);
		}

		return taskRecEntityMap;
	}

	/**
	 * IRelTaskRecEntityの内容を文字列化する。
	 * @param entity
	 * @param finder
	 * @return
	 */
	private IReportStringInfo[] getRelTaskRecContents( IRpEntity entity, FlowRelCtlReportEditSupport finder )
		throws TriSystemException {

		List<IReportStringInfo> infoList	= new ArrayList<IReportStringInfo>();
		ITaskFlowResEntity[] taskRecEntities	= getRelTaskRecEntity( entity, finder );

		for ( ITaskFlowResEntity recEntity : taskRecEntities ) {

			TaskDocument doc	= mapDB2XMLBeans( recEntity, finder );
			Document document		= (Document)doc.getDomNode();

			ReportViewInfoAddonUtil.sortNodeBySequence( document );


			String[] logs = ReportViewInfoAddonUtil.toStrings( document );
			for ( String log : logs ) {
				IReportStringInfo info = new ReportStringInfo();
				info.setValue			( log );
				info.setIdentifierId	( "CTRL_CREATE_LOG" );

				infoList.add( info );
			}
		}

		return (IReportStringInfo[])infoList.toArray( new IReportStringInfo[0] );

	}

	/**
	 * IRelTaskRecEntityをRelTaskRecDocumentに変換する。
	 * @param entity IRelTaskRecEntity
	 * @param finder
	 * @return RelTaskRecDocument
	 */
	private TaskDocument mapDB2XMLBeans(
			ITaskFlowResEntity entity, FlowRelCtlReportEditSupport finder ) {

		TaskDocument doc = TaskDocument.Factory.newInstance();
		TaskRecType xml = doc.addNewTask();

		xml = new TaskMappingUtils().mapDB2XMLBeans(
									entity.getTask(), xml, finder.getBmFinderSupport().getTaskFlowResDao().getTaskMapping() );

		// sequenceNoで並び替え
		Target[] targets	= xml.getTargetArray();

		Arrays.sort( targets, new Comparator<Target>() {
			public int compare( Target param1, Target param2 ) {
				Integer para1 = Integer.valueOf( param1.getSequenceNo() );
				Integer para2 = Integer.valueOf( param2.getSequenceNo() );

				return para1.compareTo( para2 );
			}
		}  );
		xml.setTargetArray( targets );

		return doc;
	}

	/**
	 * IRelTaskRecEntityを取得する。
	 * @param entity
	 * @param finder
	 * @return
	 */
	private ITaskFlowResEntity[] getRelTaskRecEntity( IRpEntity entity, FlowRelCtlReportEditSupport finder ) {

		TaskFlowResCondition condition = new TaskFlowResCondition();
		condition.setDataId( entity.getRpId() );

		SortBuilder sort = new SortBuilder();
		sort.setElement(TaskFlowResItems.dataId, TriSortOrder.Asc, 1);

		IEntityLimit<ITaskFlowResEntity> limit =
			this.support.getBmFinderSupport().getTaskFlowResDao().find( condition.getCondition(), sort, 1, 0 );

		return limit.getEntities().toArray(new ITaskFlowResEntity[0]);
	}

	/**
	 * リリースファイル情報を取得する。
	 * @param
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return リリースファイル情報
	 */
	private IReportRelCtlArchiveInfo[] getReportRelCtlArchiveEntity(
			FinderSupport finder, ILotEntity lotEntity, IRpDto relEntity )
		throws TriSystemException {

		File topDir = DcmDesignBusinessRuleUtils.getHistoryRelDSLPath( lotEntity, relEntity.getRpEntity() );
		List<String> extensionList =
			sheet.getValueList( DcmDesignBeanId.rpFileExtension );
		File[] relCtlFiles	= ReportFileAddonUtil.getFiles( topDir, extensionList );
		if ( TriStringUtils.isEmpty( relCtlFiles )) return  new IReportRelCtlArchiveInfo[ 0 ];


		Map<String, String> fileMap			= sheet.getKeyMap( DcmDesignBeanId.rpArchiveFileTopPath );
		Map<String[], String> filePathMap	= getEntryPathMap( finder, lotEntity, relEntity.getRpEntity(), fileMap );

		Map<String,String> entryMap			= sheet.getKeyMap( DcmDesignBeanId.rpArchiveEntryTopPath );
		Map<String[],String> entryPathMap	= getEntryPathMap( finder, lotEntity, relEntity.getRpEntity(), entryMap );

		Charset charset =
			Charset.value( sheet.getValue( RmDesignEntryKeyByRelease.assetArchiveCharset ));


		List<IReportArchiveEntryGroupInfo> groupInfoList =
			ReportAddonUtil.getArchiveEntryGroupInfo(
					relCtlFiles, topDir, filePathMap, this.extractorList, charset, entryPathMap,
					sheet.getKeyMap( DcmDesignBeanId.rpExtractArchiveFileExtension ));

		List<IReportArchiveEntryInfo> entryInfoList =
			ReportAddonUtil.getArchiveEntryInfoWithArchiveEntry( groupInfoList );

		IReportRelCtlArchiveInfo reportEntity = new ReportRelCtlArchiveInfo();
		reportEntity.setRelNo				( relEntity.getRpEntity().getRpId() );
		reportEntity.setArchiveTopPath		( topDir.getAbsolutePath() );
		reportEntity.setArchiveEntryGroup	(
				(IReportArchiveEntryGroupInfo[])groupInfoList.toArray( new IReportArchiveEntryGroupInfo[0] ));
		reportEntity.setArchiveEntry		(
				(IReportArchiveEntryInfo[])entryInfoList.toArray( new IReportArchiveEntryInfo[0] ));

		return new IReportRelCtlArchiveInfo[]{ reportEntity };
	}

	/**
	 * リリースファイル／アーカイブファイルのパスマップを取得する
	 * @param finder
	 * @param lotEntity
	 * @param relEntity
	 * @param origMap 表示順序-パス,パス名のマップ
	 * @return
	 */
	private Map<String[],String> getEntryPathMap(
			FinderSupport finder, ILotEntity lotEntity, IRpEntity relEntity, Map<String,String> origMap ) {

		Map<String, String> orderMap = ReportAddonUtil.getMap( origMap );

		Map<String[],String> entryTopPathMap = new LinkedHashMap<String[],String>();
		for ( String key : orderMap.keySet() ) {
			String replacedKey = replacedEntryPath( finder, lotEntity, relEntity, key );
			entryTopPathMap.put( TriStringUtils.convertPath( replacedKey ).split( "/" ), orderMap.get( key ) );
		}

		return entryTopPathMap;
	}

	/**
	 * エントリパスに対して変数の置換を行う。
	 * @param finder
	 * @param lotEntity
	 * @param relEntity
	 * @param entryTopPath エントリトップパス
	 * @return 変数置換後のエントリトップパス
	 */
	private String replacedEntryPath(
			FinderSupport finder, ILotEntity lotEntity, IRpEntity relEntity, String entryTopPath ) {

		List<String> variableList = ReportAddonUtil.getVariableList( entryTopPath );
		if ( null == variableList ) return entryTopPath;


		if ( null == this.variableMap ) {
			this.variableMap = new HashMap<String,String>();

			this.variableMap.put( "@{no}",			relEntity.getRpId() );
			this.variableMap.put( "@{executeUser}",	relEntity.getExecUserNm() );
			this.variableMap.put( "@{executeUserId}",	relEntity.getExecUserId() );
			this.variableMap.put( "@{envNo}",		relEntity.getBldEnvId() );
			this.variableMap.put( "@{envName}",		finder.getBmFinderSupport().findBldEnvEntity( relEntity.getBldEnvId() ).getBldEnvNm() );
			this.variableMap.put( "@{lotNo}",		lotEntity.getLotId() );
			this.variableMap.put( "@{lotName}",		lotEntity.getLotNm() );
		}

		for ( String variable : variableList ) {
			entryTopPath = StringUtils.replace( entryTopPath, variable, this.variableMap.get( variable ));
		}

		return entryTopPath;
	}

	/**
	 * リリースエンティティからビルド番号・マージ順のマップを返します。
	 * @param relEntity リリースエンティティ
	 * @return ビルド番号・マージ順のマップ
	 */
	private Map<String, String> buildNoMergeOrderMap( IRpDto entity ) {

		Map<String, String> mergeOrderMap = new HashMap<String, String>();
		for ( IRpBpLnkEntity build : entity.getRpBpLnkEntities() ) {
			mergeOrderMap.put( build.getBpId(), support.getMergeOdr(build.getRpId(), build.getBpId()) );
		}

		return mergeOrderMap;
	}
}
