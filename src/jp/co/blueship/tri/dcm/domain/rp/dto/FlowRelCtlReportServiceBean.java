package jp.co.blueship.tri.dcm.domain.rp.dto;

import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;

public class FlowRelCtlReportServiceBean extends ReportServiceBean {

	private static final long serialVersionUID = 1L;

	/** ロット番号 */
	private String selectedLotNo = null;
	/** リリース環境番号 */
	private String selectedRelEnvNo = null;
	/** リリース番号 */
	private String[] selectedRelNo = null;
	/** レポート番号 */
	private String reportNo = null;


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedRelEnvNo() {
		return selectedRelEnvNo;
	}
	public void setSelectedRelEnvNo(String selectedRelEnvNo) {
		this.selectedRelEnvNo = selectedRelEnvNo;
	}

	public String[] getSelectedRelNo() {
		return selectedRelNo;
	}
	public void setSelectedRelNo(String[] selectedRelNo) {
		this.selectedRelNo = selectedRelNo;
	}

	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}
}
