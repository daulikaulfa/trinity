package jp.co.blueship.tri.dcm.domain.bp.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Document;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmTaskAddonUtils;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.TaskMappingUtils;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskResultEntity;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExtractEntityAddonUtil;
import jp.co.blueship.tri.dcm.ExtractMessageAddonUtil;
import jp.co.blueship.tri.dcm.ReportAddonUtil;
import jp.co.blueship.tri.dcm.ReportConverterAddonUtil;
import jp.co.blueship.tri.dcm.ReportFileAddonUtil;
import jp.co.blueship.tri.dcm.ReportViewInfoAddonUtil;
import jp.co.blueship.tri.dcm.beans.ActionReportArchiveFileExtractor;
import jp.co.blueship.tri.dcm.beans.IReportRenderer;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryGroupInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportArchiveEntryInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportAssetApplyInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportBuildProcessConcentrateDetailInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportBuildProcessInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportBuildTaskRecInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitArchiveInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitCreateLogInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitReportParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportStringInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportTaskResultConcentrateInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportTaskResultConcentrateInfo.IReportTaskDiffWrapperInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportTaskResultConcentrateInfo.IReportTaskDiffWrapperInfo.IReportTaskDiffInfo;
import jp.co.blueship.tri.dcm.beans.dto.IReportTaskResultConcentrateInfo.IReportTaskDiffWrapperInfo.IReportTaskResultInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportBuildProcessConcentrateDetailInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportBuildProcessInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportBuildTaskRecInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelUnitArchiveInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelUnitCreateLogInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportStringInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportTaskResultConcentrateInfo;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelUnitReportEditSupport;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.ExtractLoggerAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType.Target;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * 選択されたビルドパッケージのレポートを出力します。<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionReportWriterByRelUnitReport extends ActionPojoAbstract<FlowRelUnitReportServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IReportRenderer actionReportRenderer = null;
	public void setActionReportRenderer( IReportRenderer actionReportRenderer ) {
		this.actionReportRenderer = actionReportRenderer;
	}

	private List<ActionReportArchiveFileExtractor> extractorList = null;
	public final void setArchiveFileExtractorList( List<ActionReportArchiveFileExtractor> extractorList ) {
		this.extractorList = extractorList;
	}

	private FlowRelUnitReportEditSupport support = null;
	public void setSupport(FlowRelUnitReportEditSupport support) {
		this.support = support;
	}

	private Map<String,String> variableMap = null;

	@Override
	public IServiceDto<FlowRelUnitReportServiceBean> execute( IServiceDto<FlowRelUnitReportServiceBean> serviceDto ) {

		List<Object> paramList = serviceDto.getParamList();

		IReportRelUnitReportParamInfo param =
						ExtractMessageAddonUtil.extractReportRelUnitReportParamInfo( paramList );

		if ( null == param ) {
			throw new TriSystemException(DcmMessageId.DCM005005S);
		}

		LogBusinessFlow logBusinessFlow = ExtractLoggerAddonUtils.extractLogBusinessFlow( paramList ) ;
		if( null == logBusinessFlow ) {
			throw new TriSystemException(DcmMessageId.DCM005001S);
		}

		IRepEntity repEntity = null;

		try {
			repEntity = DcmExtractEntityAddonUtil.extractReport( paramList );

//			ILotEntity lotEntity = DcmExtractEntityAddonUtil.extractPjtLot( paramList );
			LotCondition condition = new LotCondition();
			condition.setLotId(repEntity.getLotId());
			ILotEntity lotEntity = support.getAmFinderSupport().getLotDao().findByPrimaryKey(condition.getCondition());
			List<Object> entityList = new ArrayList<Object>();
			entityList.add( repEntity );
			entityList.add( lotEntity );

			// ビルドパッケージ
			List<IBpEntity> bpEntities = this.support.getBuildEntityByBuildAssetRegister( param.getBuildNo() );
			List<IBpDto> bpDtoList = this.support.getBmFinderSupport().findBpDto(bpEntities);
			entityList.addAll( ReportConverterAddonUtil.convertReportBuildEntity( bpDtoList , support ));

			for ( IBpDto buildEntity : bpDtoList ) {

				// 変更管理
				IPjtEntity[] pjtEntities = this.support.getPjtEntityByBuildAssetRegister( buildEntity );
				entityList.addAll( ReportConverterAddonUtil.convertReportPjtEntity( pjtEntities ));

				// 変更管理番号-変更管理情報のマップ
				Map<String, IPjtEntity> pjtNoPjtEntityMap = getPjtNoPjtEntityMap( pjtEntities );


				// 申請情報
				Map<String, ITaskFlowResEntity> taskRecEntityMap = new LinkedHashMap<String, ITaskFlowResEntity>();
				Map<String, IReportAssetApplyInfo> reportApplyEntityMap =
									getReportAssetApplyEntity( buildEntity, this.support, taskRecEntityMap );
				entityList.addAll( reportApplyEntityMap.values() );

				// 申請番号-変更管理情報のマップ
				Map<String, IPjtEntity> applyNoPjtEntityMap =
					getApplyNoPjtEntityMap( reportApplyEntityMap.values(), pjtNoPjtEntityMap );


				// 処理結果（概要）
				List<IReportBuildProcessInfo> reportBuildProcessEntityList =
												getReportBuildProcessEntity( buildEntity, this.support, taskRecEntityMap );
				entityList.addAll( reportBuildProcessEntityList );


				// 処理結果（集約用）
				for ( IReportBuildProcessInfo reportBuildProcessEntity : reportBuildProcessEntityList ) {

					// 集約を含むか判定する
					List<IReportBuildTaskRecInfo> buildTaskRecEntityList =
						getIBuildTaskRecContainsConcentrateOnly(
								(ITaskFlowProcEntity) reportBuildProcessEntity, taskRecEntityMap,
								pjtNoPjtEntityMap, applyNoPjtEntityMap, reportApplyEntityMap );

					if ( 0 < buildTaskRecEntityList.size() ) {

						IReportBuildProcessConcentrateDetailInfo[] reportConcentrateEntities =
							getReportBuildProcessConcentrateEntity( reportBuildProcessEntity, lotEntity );

						entityList.addAll( FluentList.from(reportConcentrateEntities).asList());
						entityList.addAll( buildTaskRecEntityList );
					}
				}

				// アーカイブ展開
				IReportRelUnitArchiveInfo[] archiveEntities =
					getReportRelUnitArchiveEntity( this.support, lotEntity, buildEntity );
				entityList.addAll( FluentList.from(archiveEntities).asList());

				// BuildTaskRecの中身
				IReportRelUnitCreateLogInfo createLogEntity = new ReportRelUnitCreateLogInfo();
				createLogEntity.setBuildNo	( buildEntity.getBpEntity().getBpId() );
				createLogEntity.setLog		( getBuildTaskRecContents( this.support, taskRecEntityMap ));
				entityList.add( createLogEntity );
			}

			entityList.add( logBusinessFlow ) ;
			entityList.add( serviceDto.getServiceBean() );
			this.writeReport( entityList );

			param.setSuccess( true );
		} catch ( TriRuntimeException be ) {
			logBusinessFlow.writeLog( be.getStackTraceString() ) ;
			throw be ;
		} catch ( Exception e ) {
			logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
			LogHandler.fatal( log , e ) ;
			param.setSuccess( false );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( DcmMessageId.DCM005021S , e );

		}

		return serviceDto;

	}

	/**
	 * 変更管理エンティティをレポート用に変換する
	 * @param entities
	 * @return
	 */
	private IReportBuildTaskRecInfo convertReportBuildTaskRecEntity( ITaskFlowResEntity entity ) {

		IReportBuildTaskRecInfo reportEntity = new ReportBuildTaskRecInfo();
		reportEntity.setBpId(entity.getDataId());
		reportEntity.setTaskEntity(entity.getTask());

		return reportEntity;

	}

	/**
	 * 変更管理情報を変更管理番号-変更管理情報に分類する。
	 * @param entities 変更管理情報
	 * @return 申請番号-変更管理情報のマップ
	 */
	private Map<String, IPjtEntity> getPjtNoPjtEntityMap( IPjtEntity[] entities ) {

		Map<String, IPjtEntity> pjtNoPjtEntityMap = new LinkedHashMap<String, IPjtEntity>();

		for ( IPjtEntity entity : entities ) {
			pjtNoPjtEntityMap.put( entity.getPjtId(), entity );
		}
		return pjtNoPjtEntityMap;
	}

	/**
	 * 変更管理情報を申請番号-変更管理情報に分類する。
	 * @param applyEntitys 申請情報
	 * @param pjtNoPjtEntityMap 変更管理番号-変更管理情報のマップ
	 * @return 申請番号-変更管理情報のマップ
	 */
	private Map<String, IPjtEntity> getApplyNoPjtEntityMap(
			Collection<IReportAssetApplyInfo> applyEntities, Map<String, IPjtEntity> pjtNoPjtEntityMap ) {

		Map<String, IPjtEntity> applyNoPjtEntityMap = new LinkedHashMap<String, IPjtEntity>();

		for ( IReportAssetApplyInfo applyEntity : applyEntities ) {
			applyNoPjtEntityMap.put( applyEntity.getAreqId(), pjtNoPjtEntityMap.get( applyEntity.getPjtId() ));
		}
		return applyNoPjtEntityMap;
	}

	/**
	 * エントリパスに対して変数の置換を行う。
	 * @param finder
	 * @param lotEntity
	 * @param buildEntity
	 * @param entryTopPath エントリトップパス
	 * @return 変数置換後のエントリトップパス
	 */
	private String replacedEntryPath(
			FinderSupport finder, ILotEntity lotEntity, IBpEntity buildEntity, String entryTopPath ) {

		List<String> variableList = ReportAddonUtil.getVariableList( entryTopPath );
		if ( null == variableList ) return entryTopPath;


		if ( null == this.variableMap ) {
			this.variableMap = new HashMap<String,String>();

			this.variableMap.put( "@{no}",			buildEntity.getBpId() );
			this.variableMap.put( "@{name}",		buildEntity.getBpNm() );
			this.variableMap.put( "@{executeUser}",	buildEntity.getExecUserNm() );
			this.variableMap.put( "@{executeUserId}",	buildEntity.getExecUserId() );
			this.variableMap.put( "@{envNo}",		buildEntity.getBldEnvId() );
			this.variableMap.put( "@{envName}",		finder.getBmFinderSupport().findBldEnvEntity( buildEntity.getBldEnvId() ).getBldEnvNm() );
			this.variableMap.put( "@{lotNo}",		lotEntity.getLotId() );
			this.variableMap.put( "@{lotName}",		lotEntity.getLotNm() );
			this.variableMap.put( "@{versionTag}",	buildEntity.getBpCloseVerTag() );
			this.variableMap.put( "@{baseLineTag}",	buildEntity.getBpCloseBlTag() );
		}

		for ( String variable : variableList ) {
			entryTopPath = StringUtils.replace( entryTopPath, variable, this.variableMap.get( variable ));
		}

		return entryTopPath;
	}

	/**
	 * ビルドパッケージファイル情報を取得する。
	 * @param finder
	 * @param lotEntity ロットエンティティ
	 * @param buildEntity ビルドエンティティ
	 * @return ビルドパッケージファイル情報
	 */
	private IReportRelUnitArchiveInfo[] getReportRelUnitArchiveEntity(
			FinderSupport finder, ILotEntity lotEntity, IBpDto buildEntity )
		throws TriSystemException {

		File topDir	= DcmDesignBusinessRuleUtils.getHistoryRelUnitDSLPath( lotEntity, buildEntity.getBpEntity() );
		List<String> extensionList =
			sheet.getValueList( DcmDesignBeanId.bpFileExtension );
		File[] relUnitFiles	= ReportFileAddonUtil.getFiles( topDir, extensionList );
		if ( TriStringUtils.isEmpty( relUnitFiles )) return  new IReportRelUnitArchiveInfo[ 0 ];


		Map<String, String> fileMap			= sheet.getKeyMap( DcmDesignBeanId.bpArchiveFileTopPath );
		Map<String[], String> filePathMap	= getEntryPathMap( finder, lotEntity, buildEntity.getBpEntity(), fileMap );

		Map<String,String> entryMap			= sheet.getKeyMap( DcmDesignBeanId.bpArchiveEntryTopPath );
		Map<String[],String> entryPathMap	= getEntryPathMap( finder, lotEntity, buildEntity.getBpEntity(), entryMap );

		Charset charset =
			Charset.value( sheet.getValue( BmDesignEntryKeyByBuild.assetArchiveCharset ));


		List<IReportArchiveEntryGroupInfo> groupInfoList =
			ReportAddonUtil.getArchiveEntryGroupInfo(
					relUnitFiles, topDir, filePathMap, this.extractorList, charset, entryPathMap,
					sheet.getKeyMap( DcmDesignBeanId.bpExtractArchiveFileExtension ));

		List<IReportArchiveEntryInfo> entryInfoList =
			ReportAddonUtil.getArchiveEntryInfoWithArchiveEntry( groupInfoList );

		IReportRelUnitArchiveInfo reportEntity = new ReportRelUnitArchiveInfo();
		reportEntity.setBuildNo				( buildEntity.getBpEntity().getBpId() );
		reportEntity.setArchiveTopPath		( topDir.getAbsolutePath() );
		reportEntity.setArchiveEntryGroup	(
				(IReportArchiveEntryGroupInfo[])groupInfoList.toArray( new IReportArchiveEntryGroupInfo[0] ));
		reportEntity.setArchiveEntry		(
				(IReportArchiveEntryInfo[])entryInfoList.toArray( new IReportArchiveEntryInfo[0] ));

		return new IReportRelUnitArchiveInfo[]{ reportEntity };
	}

	/**
	 * ビルドパッケージファイル／アーカイブファイルのパスマップを取得する
	 * @param finder
	 * @param lotEntity
	 * @param buildEntity
	 * @param origMap 表示順序-パス,パス名のマップ
	 * @return
	 */
	private Map<String[],String> getEntryPathMap(
			FinderSupport finder, ILotEntity lotEntity, IBpEntity buildEntity, Map<String,String> origMap ) {

		Map<String, String> orderMap = ReportAddonUtil.getMap( origMap );

		Map<String[],String> entryTopPathMap = new LinkedHashMap<String[],String>();
		for ( String key : orderMap.keySet() ) {
			String replacedKey = replacedEntryPath( finder, lotEntity, buildEntity, key );
			entryTopPathMap.put( TriStringUtils.convertPath( replacedKey ).split( "/" ), orderMap.get( key ) );
		}

		return entryTopPathMap;
	}

	/**
	 * 集約処理のみのビルドプロセスエンティティを取得する。
	 * @param buildProcessEntity ビルドプロセスエンティティ
	 * @param lotEntity ロットエンティティ
	 * @return レポート用ビルドプロセスエンティティ
	 */
	private IReportBuildProcessConcentrateDetailInfo[] getReportBuildProcessConcentrateEntity(
			IReportBuildProcessInfo buildProcessEntity, ILotEntity lotEntity ) {

		IReportBuildProcessConcentrateDetailInfo reportEntity = new ReportBuildProcessConcentrateDetailInfo();

		TriPropertyUtils.copyProperties( reportEntity, buildProcessEntity );

		reportEntity.setBasePath( TriStringUtils.convertPath( DcmDesignBusinessRuleUtils.getMasterWorkPath( lotEntity ) ));

		return new IReportBuildProcessConcentrateDetailInfo[]{ reportEntity };
	}

	/**
	 * レポート用ビルドプロセスエンティティを取得する。
	 * @param buildEntity ビルドエンティティ
	 * @param finder
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @return レポート用ビルドプロセスエンティティ
	 */
	private List<IReportBuildProcessInfo> getReportBuildProcessEntity(
			IBpDto buildEntity, FlowRelUnitReportEditSupport finder, Map<String, ITaskFlowResEntity> taskRecEntityMap ) {

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(buildEntity.getBpEntity().getProcId());
		ITaskFlowProcEntity[] entities = finder.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);

		List<IReportBuildProcessInfo> reportEntityList = new ArrayList<IReportBuildProcessInfo>();

		for ( ITaskFlowProcEntity entity : entities ) {

			IReportBuildProcessInfo reportEntity = new ReportBuildProcessInfo();

			reportEntity.setBpId( buildEntity.getBpEntity().getBpId() );
			reportEntity.setTaskFlowId(entity.getTaskFlowId());
			reportEntity.setTargetSeqNo(entity.getTargetSeqNo());
			reportEntity.setStsId(entity.getStsId());
			reportEntity.setProcessName	( BmTaskAddonUtils.getProcessName( entity, taskRecEntityMap ));
			BldSrvCondition bcondition = new BldSrvCondition();
			bcondition.setBldSrvId(entity.getBldSrvId());
			reportEntity.setBldSrvNm	(( finder.getBldSrvDao().find( bcondition.getCondition() )).get(0).getBldSrvNm() );

			reportEntityList.add( reportEntity );
		}

		return reportEntityList;
	}

	/**
	 * レポート用申請情報エンティティを取得する。
	 * @param buildEntity ビルドエンティティ
	 * @param finder
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @return 申請番号-レポート用申請情報エンティティのマップ
	 */
	private Map<String, IReportAssetApplyInfo> getReportAssetApplyEntity(
			IBpDto buildEntity,
			FlowRelUnitReportEditSupport finder,
			Map<String, ITaskFlowResEntity> taskRecEntityMap ) {

		Map<String, IReportAssetApplyInfo> reportEntityMap = new LinkedHashMap<String, IReportAssetApplyInfo>();

		List<ITaskFlowResEntity> taskRecEntityList = new ArrayList<ITaskFlowResEntity>();
		Map<String, IAssetCounterInfo> applyNoCounterMap =
			BmTaskAddonUtils.getAssetCount( buildEntity, finder.getBmFinderSupport(), taskRecEntityList );

		for ( ITaskFlowResEntity taskRecEntity: taskRecEntityList ) {
			taskRecEntityMap.put( taskRecEntity.getTaskFlowId(), taskRecEntity ) ;
		}

		IAreqEntity[] entities = finder.getAssetApplyEntityByBuildAssetRegister( buildEntity );

		List<IReportAssetApplyInfo> reportList =
			ReportConverterAddonUtil.convertReportAssetApplyEntity( entities, applyNoCounterMap );

		for ( IReportAssetApplyInfo reportEntity : reportList ) {
			reportEntityMap.put( reportEntity.getAreqId(), reportEntity );
		}

		return reportEntityMap;
	}

	/**
	 * レポートを出力
	 *
	 * @param entity
	 * @param paramList
	 * @throws IOException
	 */
	private final void writeReport( List<Object> paramList ) {

		this.actionReportRenderer.render( paramList, false );
		String format = this.actionReportRenderer.getRendererFormat();

		if ( log.isDebugEnabled() )
			LogHandler.debug( log , "writeReport:=" + format );

	}

	/**
	 * 集約処理結果のみを含むIBuildTaskRecEntityを取得する。
	 * @param entity ITaskFlowProcEntity
	 * @param taskRecEntityMap
	 * @param applyNoPjtEntityMap 申請番号-変更管理情報のマップ
	 * @param reportApplyEntityMap 申請番号-申請情報のマップ
	 * @return 集約処理結果のみを含むITaskResultTypeEntity
	 */
	private List<IReportBuildTaskRecInfo> getIBuildTaskRecContainsConcentrateOnly(
			ITaskFlowProcEntity entity, Map<String, ITaskFlowResEntity> taskRecEntityMap,
			Map<String, IPjtEntity> pjtNoPjtEntityMap, Map<String, IPjtEntity> applyNoPjtEntityMap,
			Map<String, IReportAssetApplyInfo> reportApplyEntityMap ) {

		List<IReportBuildTaskRecInfo> buildTaskRecEntityList = new ArrayList<IReportBuildTaskRecInfo>();

		ITaskFlowResEntity taskRecEntity = taskRecEntityMap.get( entity.getTaskFlowId() );
		ITaskFlowResEntity newTaskRecEntity = new TaskFlowResEntity();
		TriPropertyUtils.copyProperties( newTaskRecEntity, taskRecEntity );

		ITaskEntity newTaskEntity = new TaskEntity();
		TriPropertyUtils.copyProperties( newTaskEntity, taskRecEntity.getTask() );
		newTaskRecEntity.setTask(newTaskEntity);

		List<ITaskTargetEntity> taskTargetEntityList = new ArrayList<ITaskTargetEntity>();
		for ( ITaskTargetEntity taskTargetEntity : newTaskEntity.getTarget() ) {

			if ( !entity.getTargetSeqNo().equals( taskTargetEntity.getSequenceNo() )) continue;

			ITaskTargetEntity newTaskTargetEntity = new TaskTargetEntity();
			TriPropertyUtils.copyProperties( newTaskTargetEntity, taskTargetEntity );

			List<ITaskResultTypeEntity> taskResultTypeEntityList = new ArrayList<ITaskResultTypeEntity>();
			for ( ITaskResultTypeEntity taskResultTypeEntity : newTaskTargetEntity.getTaskResult() ) {

				if ( taskResultTypeEntity instanceof ITaskResultConcentrateEntity ) {
					taskResultTypeEntityList.add(
							convertReportTaskResultConcentrateEntity(
									(ITaskResultConcentrateEntity)taskResultTypeEntity,
									pjtNoPjtEntityMap, applyNoPjtEntityMap, reportApplyEntityMap ));
				}
			}

			if ( 0 < taskResultTypeEntityList.size() ) {
				newTaskTargetEntity.setTaskResult(
						(ITaskResultTypeEntity[])taskResultTypeEntityList.toArray( new ITaskResultTypeEntity[0] ));
				taskTargetEntityList.add( newTaskTargetEntity );
			}
		}

		if ( 0 < taskTargetEntityList.size() ) {
			newTaskRecEntity.getTask().setTarget((
					(ITaskTargetEntity[])taskTargetEntityList.toArray( new ITaskTargetEntity[0] )));
			buildTaskRecEntityList.add( convertReportBuildTaskRecEntity( newTaskRecEntity ));
		}

		return buildTaskRecEntityList;
	}

	/**
	 * ITaskDiffEntityをIReportTaskDiffEntityに変換する。
	 * @param reportTypeEntity
	 * @param diffEntity
	 * @return
	 */
	private IReportTaskDiffInfo convertReportTaskDiffEntity(
			IReportTaskResultConcentrateInfo reportTypeEntity,
			ITaskDiffEntity diffEntity, Map<String,IReportAssetApplyInfo> reportApplyEntityMap ) {

		List<IAreqFileEntity> lendingFileEntities = this.support.getAmFinderSupport().findAreqFileEntities( diffEntity.getApplyNo(), AreqCtgCd.LendingRequest );
		List<IAreqFileEntity> returningFileEntities = this.support.getAmFinderSupport().findAreqFileEntities( diffEntity.getApplyNo(), AreqCtgCd.ReturningRequest );
		List<IAreqFileEntity> removalFileEntities = this.support.getAmFinderSupport().findAreqFileEntities( diffEntity.getApplyNo(), AreqCtgCd.RemovalRequest);


		IReportTaskDiffInfo reportDiffEntity	= reportTypeEntity.newDiffWrapper().newDiff();

		reportDiffEntity.setApplyNo	( diffEntity.getApplyNo() );
		reportDiffEntity.setCode	( diffEntity.getCode() );
		reportDiffEntity.setSequence( diffEntity.getSequence() );

		List<IReportTaskResultInfo> reportResultList = new ArrayList<IReportTaskResultInfo>();
		for ( ITaskResultEntity result : diffEntity.getResult() ) {

			IReportTaskResultInfo reportResult = reportDiffEntity.newResult();
			TriPropertyUtils.copyProperties( reportResult, result );

			if ( EqualsContentsSetSameLineDiffer
					.Status.DELETE.name().equals( reportResult.getCode() )) {

				if ( null != removalFileEntities ) {

					for ( IAreqFileEntity deleteFileEntity : removalFileEntities ) {
						copyIfPathEquals( reportResult, deleteFileEntity );
					}
				}

			} else {

				if ( EqualsContentsSetSameLineDiffer
						.Status.CHANGE.name().equals( reportResult.getCode() )) {

					for ( IAreqFileEntity lendFileEntity : lendingFileEntities ) {
						if ( pathEquals( lendFileEntity, reportResult )) {

							reportResult.setLendRevision( lendFileEntity.getFileRev() );

						}
					}
				}

				for ( IAreqFileEntity retFileEntity : returningFileEntities ) {
					copyIfPathEquals( reportResult, retFileEntity );
				}
			}

			reportResultList.add( reportResult );
		}

		reportDiffEntity.setResult(
				(IReportTaskResultInfo[])reportResultList.toArray( new IReportTaskResultInfo[0] ));

		return reportDiffEntity;

	}

	/**
	 * IAssetFileEntityのパスと、IReportTaskResultInfoのパスが等しいかを判定する。
	 * @param fileEntity
	 * @param reportResult
	 * @return 等しい場合はtrue、そうでなければfalse
	 */
	private boolean pathEquals( IAreqFileEntity fileEntity, IReportTaskResultInfo reportResult ) {

		if ( TriStringUtils.convertPath( fileEntity.getFilePath() ).equals(
				TriStringUtils.convertPath( reportResult.getRelativePath() ))) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 資産のパスが等しい場合にIAssetFileEntityの情報をIReportTaskResultEntityにコピーする
	 * @param reportResult IReportTaskResultEntity
	 * @param fileEntity IAssetFileEntity
	 * @return パスが等しい場合はtrue、
	 */
	private void copyIfPathEquals( IReportTaskResultInfo reportResult, IAreqFileEntity fileEntity ) {

		if ( pathEquals( fileEntity, reportResult )) {

			reportResult.setByteSize		( fileEntity.getFileByteSize().toString() );
			reportResult.setInsertUpdateDate( TriDateUtils.convertViewDateFormat( fileEntity.getUpdTimestamp() ) );
			reportResult.setCloseRevision	( fileEntity.getFileRev() );
		}
	}



	/**
	 * TaskResultConcentrateEntityをレポート用に変換する。
	 * @param typeEntity TaskResultConcentrateEntity
	 * @param pjtNoPjtEntityMap 変更管理番号-変更管理情報のマップ
	 * @param applyNoPjtEntityMap 申請番号-変更管理情報のマップ
	 * @param reportApplyEntityMap 申請番号-申請情報のマップ
	 * @return ReportTaskResultConcentrateEntity
	 */
	private IReportTaskResultConcentrateInfo convertReportTaskResultConcentrateEntity(
			ITaskResultConcentrateEntity typeEntity,
			Map<String, IPjtEntity> pjtNoPjtEntityMap, Map<String, IPjtEntity> applyNoPjtEntityMap,
			Map<String, IReportAssetApplyInfo> reportApplyEntityMap ) {

		Map<String, List<IReportTaskDiffInfo>> pjtNoTaskDiffListMap =
			new LinkedHashMap<String, List<IReportTaskDiffInfo>>();

		IReportTaskResultConcentrateInfo reportTypeEntity = new ReportTaskResultConcentrateInfo();

		for ( ITaskDiffEntity diffEntity : typeEntity.getDiff() ) {

			IReportTaskDiffInfo reportDiffEntity =
				convertReportTaskDiffEntity( reportTypeEntity, diffEntity, reportApplyEntityMap );

			IPjtEntity pjtEntity = applyNoPjtEntityMap.get( reportDiffEntity.getApplyNo() );

			List<IReportTaskDiffInfo> diffEntityList = pjtNoTaskDiffListMap.get( pjtEntity.getPjtId() );
			if ( null == diffEntityList ) {
				diffEntityList = new ArrayList<IReportTaskDiffInfo>();
				pjtNoTaskDiffListMap.put( pjtEntity.getPjtId(), diffEntityList );
			}

			diffEntityList.add( reportDiffEntity );
		}


		List<IReportTaskDiffWrapperInfo> wrapperEntityList = new ArrayList<IReportTaskDiffWrapperInfo>();

		for ( String pjtNo : pjtNoTaskDiffListMap.keySet() ) {

			IReportTaskDiffWrapperInfo wrapperEntity = reportTypeEntity.newDiffWrapper();

			wrapperEntity.setPjtNo			( pjtNo );
			wrapperEntity.setChangeCauseNo	( pjtNoPjtEntityMap.get( pjtNo).getChgFactorNo() );
			wrapperEntity.setDiff			(
					((IReportTaskDiffInfo[])(pjtNoTaskDiffListMap.get( pjtNo )).toArray( new IReportTaskDiffInfo[0] )));

			wrapperEntityList.add( wrapperEntity );
		}


		reportTypeEntity.setDiffWrapper(
				(IReportTaskDiffWrapperInfo[])wrapperEntityList.toArray( new IReportTaskDiffWrapperInfo[0] ));

		return reportTypeEntity;
	}

	/**
	 * IBuildTaskRecEntityの内容を文字列化する。
	 * @param finder
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @return
	 */
	private IReportStringInfo[] getBuildTaskRecContents(
			FlowRelUnitReportEditSupport finder,
			Map<String, ITaskFlowResEntity> taskRecEntityMap )
		throws TriSystemException {

		List<IReportStringInfo> entityList = new ArrayList<IReportStringInfo>();

		for ( ITaskFlowResEntity recEntity : taskRecEntityMap.values() ) {

			TaskDocument doc	= mapDB2XMLBeans( recEntity, finder );
			Document document		= (Document)doc.getDomNode();

			ReportViewInfoAddonUtil.sortNodeBySequence( document );

			String[] logs = ReportViewInfoAddonUtil.toStrings( document );
			for ( String log : logs ) {
				IReportStringInfo entity = new ReportStringInfo();
				entity.setValue			( log );
				entity.setIdentifierId	( "UNIT_CREATE_LOG" );

				entityList.add( entity );
			}
		}

		return (IReportStringInfo[])entityList.toArray( new IReportStringInfo[0] );

	}

	/**
	 * IBuildTaskRecEntityをBuildTaskRecDocumentに変換する。
	 * @param entity IBuildTaskRecEntity
	 * @param finder
	 * @return BuildTaskRecDocument
	 */
	private TaskDocument mapDB2XMLBeans(
			ITaskFlowResEntity entity, FlowRelUnitReportEditSupport finder ) {

		TaskDocument doc = TaskDocument.Factory.newInstance();
		TaskRecType xml = doc.addNewTask();

		xml = new TaskMappingUtils().mapDB2XMLBeans(
									entity.getTask(), xml, finder.getTaskFlowResDao().getTaskMapping() );


		// sequenceNoで並び替え
		Target[] targets	= xml.getTargetArray();

		Arrays.sort( targets, new Comparator<Target>() {
			public int compare( Target param1, Target param2 ) {

				String sequenceNo1 = param1.getSequenceNo();
				String sequenceNo2 = param2.getSequenceNo();

				return ReportViewInfoAddonUtil.compareToBySequence( sequenceNo1, sequenceNo2 );
			}
		}  );

		xml.setTargetArray( targets );

		return doc;
	}
}
