package jp.co.blueship.tri.dcm.domain.bp.dto;

import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;

public class FlowRelUnitReportServiceBean extends ReportServiceBean {

	private static final long serialVersionUID = 1L;

	

	/** ロット番号 */
	private String selectedLotNo = null;
	/** ビルドパッケージ番号 */
	private String[] selectedBuildNo = null;
	/** レポート番号 */
	private String reportNo = null;


	public String[] getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo(String[] selectedBuildNo) {
		this.selectedBuildNo = selectedBuildNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getReportNo() {
		return reportNo;
	}
	public void setReportNo( String reportNo ) {
		this.reportNo = reportNo;
	}
}
