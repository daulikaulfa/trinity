package jp.co.blueship.tri.dcm.domain.bp;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.dcm.beans.dto.IReportRelUnitReportParamInfo;
import jp.co.blueship.tri.dcm.beans.dto.ReportRelUnitReportParamInfo;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domain.bp.dto.FlowRelUnitReportServiceBean;
import jp.co.blueship.tri.dcm.support.FlowRelUnitReportEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.DcmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;


/**
 * FlowRelUnitReportAssetRegisterServiceイベントのサービスClass
 * <br>
 * <p>
 * ビルドパッケージ／作成レポートのレポート出力処理を行う。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class FlowRelUnitReportService implements IDomain<FlowRelUnitReportServiceBean>  {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitReportEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowRelUnitReportEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelUnitReportServiceBean> execute( IServiceDto<FlowRelUnitReportServiceBean> serviceDto ) {

		FlowRelUnitReportServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String reportID		= paramBean.getReportId();

			if ( ! DcmReportType.BmBpReport.value().equals( reportID )) {
				return serviceDto;
			}

			this.executeForward( paramBean );
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面を表示させる場合,BaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException(DcmMessageId.DCM005025S,e);
		}
	}

	/**
	 * レポート出力依頼画面への遷移
	 *
	 * @param paramBean
	 * @param paramBean
	 */

	private final void executeForward( FlowRelUnitReportServiceBean paramBean ) {

		//「次へ」で次画面に進む場合、下層のアクションを実行する
		if ( ScreenType.next.equals( paramBean.getScreenType() )) {

			ItemCheckAddonUtils.checkLotNo	( paramBean.getSelectedLotNo() );
			ItemCheckAddonUtils.checkBuildNo( paramBean.getSelectedBuildNo() );

			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );

			// グループの存在チェック
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );


			//下層のアクションを実行する
			IReportRelUnitReportParamInfo actionParam = new ReportRelUnitReportParamInfo();

			LogBusinessFlow logBusinessFlow = new LogBusinessFlow() ;
			String flowActionTitle = null ;

			try {
				List<Object> paramList = new CopyOnWriteArrayList<Object>();
				paramBean.setReportNo( this.support.getDcmFinderSupport().nextvalByRepId() );

				actionParam.setUserId	( paramBean.getUserId() );
				actionParam.setUserName	( paramBean.getUserName() );
				actionParam.setRepId	( paramBean.getReportNo() );
				actionParam.setRepCtgCd	( paramBean.getReportId() );
				actionParam.setLotNo	( paramBean.getSelectedLotNo() );
				actionParam.setBuildNo	( paramBean.getSelectedBuildNo() );

				paramList.add( actionParam );
				paramList.add( lotDto );

				//ログの初期処理
				logBusinessFlow.makeLogPath( paramBean.getFlowAction() , lotDto.getLotEntity() ) ;
				//ログの開始ラベル出力
				flowActionTitle = LogBusinessFlow.makeFlowActionTitle( paramBean.getFlowAction() ) ;
				logBusinessFlow.writeLogWithDateBegin( flowActionTitle ) ;
				//ログのヘッダ部出力
				this.outLogHeader( logBusinessFlow , paramBean , lotDto.getLotEntity() , actionParam ) ;

				paramList.add( logBusinessFlow ) ;
				paramList.add( BmDesignEntryKeyByLogs.deleteLogAtSuccessRelUnitReport ) ;

				paramList.add( paramBean );

				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}

			} catch( TriRuntimeException be ) {
				logBusinessFlow.writeLog( be.getStackTraceString() ) ;

				throw be ;
			} catch ( Exception e ) {
				logBusinessFlow.writeLog( ExceptionUtils.getStackTraceString( e ) ) ;
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( DcmMessageId.DCM005018S , e );
			} finally {
				if ( null != flowActionTitle ) {
					logBusinessFlow.writeLogWithDateEnd( flowActionTitle ) ;
				}
			}
		}

	}

	/**
	 * /**
	 * ログのヘッダ部分を出力する
	 * @param logBusinessFlow	ログ出力クラス
	 * @param paramBean
	 * @param pjtLotEntity ロット情報
	 * @param actionParam
	 * @throws Exception
	 */
	private void outLogHeader(
			LogBusinessFlow logBusinessFlow ,
			FlowRelUnitReportServiceBean paramBean ,
			ILotEntity pjtLotEntity ,
			IReportRelUnitReportParamInfo actionParam ) throws Exception {

		StringBuilder stb = new StringBuilder() ;
		//実行者ラベル
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		String userLabel = ac.getMessage( TriLogMessage.LAM0004 , paramBean.getUserName() , paramBean.getUserId() );
		stb.append( userLabel + SEP ) ;
		//レポート種別ラベル
		String reportIdLabel = ac.getMessage( TriLogMessage.LDCM0002 , sheet.getValue( DcmDesignBeanId.reportId, actionParam.getRepCtgCd() ) );
		stb.append( reportIdLabel + SEP ) ;
		//レポート番号ラベル
		String reportNoLabel = ac.getMessage( TriLogMessage.LDCM0003 , actionParam.getRepId() );
		stb.append( reportNoLabel + SEP ) ;
		//ロット番号ラベル
		String lotIdLabel = ac.getMessage( TriLogMessage.LAM0005 , pjtLotEntity.getLotNm() , pjtLotEntity.getLotId() );
		stb.append( lotIdLabel + SEP ) ;

		//ビルドパッケージ情報
		for( String buildNo : actionParam.getBuildNo() ) {
			String buildNoLabel = ac.getMessage( TriLogMessage.LDCM0007 , buildNo );
			stb.append( buildNoLabel + SEP ) ;
		}

		logBusinessFlow.writeLog( stb.toString() ) ;
	}
}
