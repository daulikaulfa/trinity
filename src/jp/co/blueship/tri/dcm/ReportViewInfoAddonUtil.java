package jp.co.blueship.tri.dcm;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;

/**
 * レポート表示情報の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ReportViewInfoAddonUtil {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * BuildTaskRec、RelTaskRecのxmlドキュメントにおいて、
	 * xmlドキュメントに含まれるNodeのうち、"sequence"を持つNodeを"sequence"の昇順に並べる
	 * @param node
	 */
	public static void sortNodeBySequence( Node node ) {

		// "sequence"をattributesに含むNodeの取得
		List<Node> sequenceNodeList = new ArrayList<Node>();
		getSequenceNode( node, sequenceNodeList );

		// "sequence"軍団の並び替えのため、親Nodeが同じものごとに分類したい
		// 親ノードの抽出
		List<Node> parentNodeList = new ArrayList<Node>();
		for ( Node sequenceNode : sequenceNodeList) {

			Node parentNode	= sequenceNode.getParentNode();
			if ( !parentNodeList.contains( parentNode )) {
				parentNodeList.add( parentNode );
			}
		}


		for ( Node parentNode : parentNodeList ) {

			// 親を同じくするNodeのリスト
			List<Node> hasSameParentNodeList = new ArrayList<Node>();
			for ( Node sequenceNode : sequenceNodeList ) {

				if ( sequenceNode.getParentNode() == parentNode ) {
					hasSameParentNodeList.add( sequenceNode );
				}
			}

			// １つしかないんだったら並べ替えもへったくれもなし
			if ( 1 == hasSameParentNodeList.size() ) continue;


			Map<String, List<Node>> sortNodeMap = new TreeMap<String, List<Node>>();
			for ( Node sequenceNode : hasSameParentNodeList ) {

				// "sequence"の値をキーとして、キーの昇順に格納
				// 同じ"sequence"が重複していることも念のため考慮してListにする
				String sequence = sequenceNode.getAttributes().getNamedItem( "sequence" ).getNodeValue();
				List<Node> nodeList = sortNodeMap.get( sequence );
				if ( null == nodeList ) {
					nodeList = new ArrayList<Node>();
					sortNodeMap.put( sequence, nodeList );
				}
				nodeList.add( sequenceNode );
			}


			// 並び替え
			String[] sortedKey = (String[])sortNodeMap.keySet().toArray( new String[0] );
			Arrays.sort( sortedKey, new Comparator<String>() {
				public int compare( String param1, String param2 ) {

					return compareToBySequence( param1, param2 );
				}
			}  );


			// 最後の１つを並べ替えの基点として残しておく
			List<Node> lastList = sortNodeMap.get( sortedKey[ sortedKey.length - 1 ] );
			Node baseNode = lastList.get( lastList.size() - 1 );

			for ( int i = 0; i < sortedKey.length; i++ ) {

				List<Node> sortNodeList = sortNodeMap.get( sortedKey[ i ] );
				for ( Node sortNode : sortNodeList ) {

					if ( baseNode  != sortNode ) {
						parentNode.removeChild( sortNode );
					}
					parentNode.insertBefore( sortNode, baseNode );
				}
			}
		}
	}

	/**
	 * "sequence"をattributeに含むNodeを取得する
	 * @param node
	 * @param sequenceNodeList
	 */
	private static void getSequenceNode( Node node, List<Node> sequenceNodeList ) {

		NodeList childrenNode = node.getChildNodes();
		if ( null == childrenNode ) return;

		for ( int i = 0; i < childrenNode.getLength(); i++ ) {

			Node child = childrenNode.item( i );
			NamedNodeMap attributes = child.getAttributes();
			if ( null == attributes ) continue;

			Node targetNode = attributes.getNamedItem( "sequence" );
			if ( null != targetNode ) {
				sequenceNodeList.add( child );
			}

			getSequenceNode( child, sequenceNodeList );
		}
	}

	/**
	 * xmlドキュメントの内容を文字列の配列にする。
	 * @param document
	 * @return
	 * @throws TriSystemException
	 */
	public static String[] toStrings( Document document ) throws TriSystemException {

//		Charset charset				=
//			Charset.getValue( DesignProjectUtil.getValue( DesignProjectReportDefineId.charset ));
		String charset				=
				sheet.getValue( DcmDesignEntryKeyByReport.charset );
		LinefeedCode linefeedCode	=
			LinefeedCode.getLabel( sheet.getValue( DcmDesignEntryKeyByReport.linefeedCode ));

		// 第２引数がnull = デフォルトエンコーディング
		OutputFormat format = new OutputFormat( document, charset, true );
		format.setLineSeparator( linefeedCode.getValue() );

		StringWriter writer			= new StringWriter();
		XMLSerializer serializer	= new XMLSerializer( writer, format );

		String[] logs = null;
		try {
			serializer.serialize( document );
			// "\r"も改行として"\n"に置換
			String log = TriStringUtils.convertLinefeedCode( writer.toString(), linefeedCode );
			logs = log.split( linefeedCode.getValue() );

		} catch ( IOException e ) {
			throw new TriSystemException( DcmMessageId.DCM004032F, e );
		}

		return logs;
	}

	/**
	 * sequenceNo、またはsequenceを比較する
	 * @param param1
	 * @param param2
	 * @return compareToの結果
	 */
	public static int compareToBySequence( String param1, String param2 ) {

		if ( NumberUtils.isNumber( param1 ) && NumberUtils.isNumber( param2 )) {

			return Integer.valueOf( param1 ).compareTo( Integer.valueOf( param2 ) );

		} else if ( !NumberUtils.isNumber( param1 ) && !NumberUtils.isNumber( param2 )) {

			return param1.compareTo( param2 );

		} else if ( NumberUtils.isNumber( param1 ) && !NumberUtils.isNumber( param2 )) {

			// 数字優先
			return -1;

		} else {

			return 1;
		}
	}
}
