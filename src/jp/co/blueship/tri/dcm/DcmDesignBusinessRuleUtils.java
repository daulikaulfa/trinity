package jp.co.blueship.tri.dcm;

import java.io.File;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */

public class DcmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ページ制御情報からページ番号情報に設定します。
	 * @param dest ページ番号情報
	 * @param src ページ制御
	 * @return ページ番号情報を戻します。
	 */
	public static final IPageNoInfo convertPageNoInfo( IPageNoInfo dest, ILimit src ) {

		if ( null == dest ){
			throw new TriSystemException( DcmMessageId.DCM005012S );
		}
		if ( null == src ) {
			dest.setMaxPageNo( new Integer(0) );
			dest.setSelectPageNo( new Integer(0) );
			return dest;
		}

		dest.setMaxPageNo( new Integer(src.getPageBar().getMaximum()) );
		dest.setSelectPageNo( new Integer(src.getPageBar().getValue()) );

		dest.setViewRows(src.getViewRows());
		dest.setMaxRows(src.getMaxRows());

		return dest;
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @return 取得した情報を戻します
	 */
	public static final File getMasterWorkPath(ILotEntity lotEntity) {

		return new File( lotEntity.getLotMwPath() );
	}

	/**
	 * 履歴フォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getHistoryPath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( DcmMessageId.DCM005013S );
		}
		if ( null == entity.getHistPath() ){
			throw new TriSystemException( DcmMessageId.DCM004028F , entity.getHistPath());
		}
		file = new File( entity.getHistPath() );
		if ( ! file.isDirectory() ) {
			throw new TriSystemException( DcmMessageId.DCM004029F , file.getPath());
		}

		return file;
	}

	/**
	 * ビルドパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param buildEntity ビルドパッケージエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelUnitDSLPath( ILotEntity lotEntity, IBpEntity buildEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryPath( lotEntity );

		if ( null == buildEntity ){
			throw new TriSystemException( DcmMessageId.DCM005014S );
		}
		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relUnitDSLRelationPath ),
				buildEntity.getBpId() )
				);

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLPath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryPath( lotEntity );

		if ( null == relEntity ){
			throw new TriSystemException( DcmMessageId.DCM005015S );
		}
		String relativePath = TriStringUtils.linkPath( relEntity.getBldEnvId(), relEntity.getRpId() );

		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relDSLRelationPath ),
				relativePath )
				);

		return file;
	}

	/**
	 * レポートのファイル名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得したファイル名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReportFile( List<Object> list )
			throws TriSystemException {

		IRepEntity entity = DcmExtractEntityAddonUtil.extractReport( list );
		if ( null == entity ){
			throw new TriSystemException( DcmMessageId.DCM005016S );
		}
		File path = new File( sheet.getValue(DcmDesignEntryKeyByReport.reportFilePath) );
		if ( ! path.isDirectory() )
			path.mkdirs();
		String extension = "";
		if ( DcmReportType.AmRsReport.value().equalsIgnoreCase(entity.getRepCtgCd())  ){
			extension = "csv";
		}else {
			extension = sheet.getValue( DcmDesignEntryKeyByReport.reportFileExtension ) ;
		}

		extension = ( '.' != extension.charAt( 0 ) ) ? "." + extension : extension ;//.が拡張子文字列の先頭になければ付加

		return new File(path, entity.getRepId() + extension );
	}

	/**
	 * Get temporary Download Folder
	 *
	 * @param String userId
	 * @return Download Temporary Folder
	 */
	public static final File getDownloadTempPath(String userId ){

		File path = new File(
				TriStringUtils.linkPathBySlash( sheet.getValue(DcmDesignEntryKeyByReport.downloadTempPath) , userId ) );

		if( ! path.isDirectory() ){
			path.mkdirs();
		}

		return path;
	}
}
