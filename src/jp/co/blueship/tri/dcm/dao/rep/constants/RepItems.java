package jp.co.blueship.tri.dcm.dao.rep.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the report entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RepItems implements ITableItem {
	repId("rep_id"),
	lotId("lot_id"),
	repCtgCd("rep_ctg_cd"),
	execUserId("exec_user_id"),
	execUserNm("exec_user_nm"),
	procStTimestamp("proc_st_timestamp"),
	procEndTimestamp("proc_end_timestamp"),
	latestDlUserId("latest_dl_user_id"),
	latestDlUserNm("latest_dl_user_nm"),
	latestDlTimestamp("latest_dl_timestamp"),
	stsId("sts_id"),
	procStsId("proc_sts_id"),
	delUserId("del_user_id"),
	delUserNm("del_user_nm"),
	delTimestamp("del_timestamp"),
	delCmt("del_cmt"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private RepItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
