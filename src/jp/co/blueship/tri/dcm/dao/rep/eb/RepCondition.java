package jp.co.blueship.tri.dcm.dao.rep.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.constants.RepItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The SQL condition of the report entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RepCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = DcmTables.DCM_REP;

	/**
	 * rep-ID
	 */
	public String repId = null;
	/**
	 * rep-ID's
	 */
	public String[] repIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * report catalog code
	 */
	public String repCtgCd = null;
	/**
	 * report catalog code's
	 */
	public String[] repCtgCds = null;
	/**
	 * execute user name
	 */
	public String execUserNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * latest download user-ID
	 */
	public String latestDlUserId = null;
	/**
	 * latest download user name
	 */
	public String latestDlUserNm = null;
	/**
	 * latest download time stamp
	 */
	public Timestamp latestDlTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(RepItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * procStatusId's
	 */
	public String[] procStsIds = null;
	
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * keyword
	 */
	public String keyword = null;

	public RepCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( RepItems.repId );
		super.setJoinCtg( true );
		super.setJoinMstone( true );

	}

	/**
	 * rep-IDを取得します。
	 * @return rep-ID
	 */
	public String getRepId() {
	    return repId;
	}

	public void setRepId(String repId , boolean isLike) {
	    this.repId = repId;
	    super.append(RepItems.repId, SqlFormatUtils.getCondition(repId, RepItems.repId.getItemName() , isLike) );
	}
	/**
	 * rep-IDを設定します。
	 * @param repId rep-ID
	 */
	public void setRepId(String repId) {
	    this.repId = repId;
	    super.append(RepItems.repId, repId );
	}

	/**
	 * rp-ID'sを取得します。
	 * @return rep-ID's
	 */
	public String[] getRepIds() {
	    return repIds;
	}

	/**
	 * rep-ID'sを設定します。
	 * @param repIds rep-ID's
	 */
	public void setRepIds(String[] repIds) {
	    this.repIds = repIds;
	    super.append( RepItems.repId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(repIds, RepItems.repId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(RepItems.lotId, lotId );
	}

	/**
	 * report catalog codeを取得します。
	 * @return report catalog code
	 */
	public String getRepCtgCd() {
	    return repCtgCd;
	}
	/**
	 * report catalog codeを設定します。
	 * @param repCtgCd report catalog code
	 */
	public void setRepCtgCd(String repCtgCd) {
	    this.repCtgCd = repCtgCd;
	    super.append(RepItems.repCtgCd, repCtgCd );
	}
	/**
	 * report catalog code'sを取得します。
	 * @return report catalog code's
	 */
	public String[] getRepCtgCds() {
	    return repCtgCds;
	}
	/**
	 * report catalog code'sを設定します。
	 * @param repCtgCd report catalog code's
	 */
	public void setRepCtgCds(String[] repCtgCds) {
	    this.repCtgCds = repCtgCds;
	    super.append( RepItems.repCtgCd.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(repCtgCds, RepItems.repCtgCd.getItemName(), false, true, false) );
	}
	/**
	 *  execute user nameを取得します。
	 * @return execUserNm execute user name
	 */
	public String getExecUserNm() {
	    return execUserNm;
	}
	/**
	 *  execute user nameを設定します。
	 * @param execUserNm execute user name
	 */
	public void setExecUserNm(String execUserNm) {
	    this.execUserNm = execUserNm;
	    super.append(RepItems.execUserNm, execUserNm );
	}
	/**
	 *  execute user nameを設定します。
	 * @param execUserNm execute user name , isLike(部分一致検索)
	 */
	public void setExecUserNm(String execUserNm , boolean isLike) {
	    this.execUserNm = execUserNm;
	    super.append(RepItems.execUserNm, SqlFormatUtils.getCondition(execUserNm, RepItems.execUserNm.getItemName() , isLike) );
	}

	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(RepItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(RepItems.procEndTimestamp, procEndTimestamp );
	}

	/**
	 * latest download user-IDを取得します。
	 * @return latest download user-ID
	 */
	public String getLatestDlUserId() {
	    return latestDlUserId;
	}
	/**
	 * latest download user-IDを設定します。
	 * @param latestDlUserId latest download user-ID
	 */
	public void setLatestDlUserId(String latestDlUserId) {
	    this.latestDlUserId = latestDlUserId;
	    super.append(RepItems.latestDlUserId, latestDlUserId );
	}

	/**
	 * latest download user nameを取得します。
	 * @return latest download user name
	 */
	public String getLatestDlUserNm() {
	    return latestDlUserNm;
	}
	/**
	 * latest download user nameを設定します。
	 * @param latestDlUserId latest download user name
	 */
	public void setLatestDlUserNm(String latestDlUserNm) {
	    this.latestDlUserNm = latestDlUserNm;
	    super.append(RepItems.latestDlUserNm, latestDlUserNm );
	}
	/**
	 * latest download time stampを取得します。
	 * @return latest download time stamp
	 */
	public Timestamp getLatestDlTimestamp() {
	    return latestDlTimestamp;
	}
	/**
	 * latest download time stampを設定します。
	 * @param latestDlimestamp latest download time stamp
	 */
	public void setLatestDlTimestamp(Timestamp latestDlTimestamp) {
	    this.latestDlTimestamp = latestDlTimestamp;
	    super.append(RepItems.latestDlTimestamp, latestDlTimestamp );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(RepItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID
	 */
	public String[] getStsIds() {
	    return stsIds;
	}
	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( RepItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, RepItems.stsId.getItemName(), false, true, false) );
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}

	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}

	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}

	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}

	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}

	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}

	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}

	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( RepItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}
	/**
	 * process-status-ID'sを取得します。
	 * @return process-status-ID's procStsIds
	 */
	public String[] getProcStsId(){
		return procStsIds;
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsId(String[] id){
		this.procStsIds =id;
	    super.appendByJoinExecData( RepItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(id, RepItems.procStsId.getItemName(), false, true, false) );
	}
	
	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}
	
	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}
	
	/**
	 * keyword を設定します。（中間一致）
	 * @param keyword
	 */
	public void setContainsByKeyword(String keyword) {
	    this.keyword = keyword;
	    super.append( RepItems.lotId.getItemName() + "Other",
	    		SqlFormatUtils.joinCondition( true,
	    			new String[] {
	    			SqlFormatUtils.getCondition( this.keyword, RepItems.repId.getItemName(), true, false, false),
	    			SqlFormatUtils.getCondition( this.keyword, RepItems.execUserNm.getItemName(), true, false, false),
	    			} ));
	}

}