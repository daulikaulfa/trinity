package jp.co.blueship.tri.dcm.dao.rep.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * report entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RepEntity implements IRepEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * rep-ID(V2のreportNo)
	 */
	public String repId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * report catalog code (V2のreportId)
	 */
	public String repCtgCd = null;
	/**
	 * execute user-ID
	 */
	public String execUserId = null;
	/**
	 * execute user name
	 */
	public String execUserNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * latest download user-ID
	 */
	public String latestDlUserId = null;
	/**
	 * latest download user name
	 */
	public String latestDlUserNm = null;
	/**
	 * latest download time stamp
	 */
	public Timestamp latestDlTimestamp = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * proc status-ID
	 */
	public String procStsId = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	/**
	 * rep-IDを取得します。
	 * @return rep-ID
	 */
	public String getRepId() {
	    return repId;
	}
	/**
	 * rep-IDを設定します。
	 * @param repId rep-ID
	 */
	public void setRepId(String repId) {
	    this.repId = repId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * report catalog codeを取得します。
	 * @return report catalog code
	 */
	public String getRepCtgCd() {
	    return repCtgCd;
	}
	/**
	 * report catalog codeを設定します。
	 * @param repCtgCd report catalog code
	 */
	public void setRepCtgCd(String repCtgCd) {
	    this.repCtgCd = repCtgCd;
	}
	/**
	 * execute user-IDを取得します。
	 * @return execute user-ID
	 */
	public String getExecUserId() {
	    return execUserId;
	}
	/**
	 * execute user-IDを設定します。
	 * @param execUserId execute user-ID
	 */
	public void setExecUserId(String execUserId) {
	    this.execUserId = execUserId;
	}
	/**
	 * execute user nameを取得します。
	 * @return execute user name
	 */
	public String getExecUserNm() {
	    return execUserNm;
	}
	/**
	 * execute user nameを設定します。
	 * @param ExecUserNm execute user name
	 */
	public void setExecUserNm(String execUserNm) {
	    this.execUserNm = execUserNm;
	}
	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}
	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	}
	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}
	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	}
	/**
	 * latest download user-IDを取得します。
	 * @return latest download user-ID
	 */
	public String getLatestDlUserId() {
	    return latestDlUserId;
	}
	/**
	 * latest download user-IDを設定します。
	 * @param latestDlUserId latest download user-ID
	 */
	public void setLatestDlUserId(String latestDlUserId) {
	    this.latestDlUserId = latestDlUserId;
	}
	/**
	 * latest download user nameを取得します。
	 * @return latest download user name
	 */
	public String getLatestDlUserNm() {
	    return latestDlUserNm;
	}
	/**
	 * latest download user nameを設定します。
	 * @param latestDlUserNm latest download user name
	 */
	public void setLatestDlUserNm(String latestDlUserNm) {
	    this.latestDlUserNm = latestDlUserNm;
	}
	/**
	 * latest download time stampを取得します。
	 * @return latest download time stamp
	 */
	public Timestamp getLatestDlTimestamp() {
	    return latestDlTimestamp;
	}
	/**
	 * latest download time stampを設定します。
	 * @param latestDlimestamp latest download time stamp
	 */
	public void setLatestDlTimestamp(Timestamp latestDlTimestamp) {
	    this.latestDlTimestamp = latestDlTimestamp;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * proc status-IDを取得します。
	 * @return proc status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * proc status-IDを設定します。
	 * @param procStsId proc status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}
	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}
	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	}
	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}
	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	}
	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}
	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	}
	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}
	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	}
	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}
	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	}
	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}
	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	}
	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}
	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	}
}
