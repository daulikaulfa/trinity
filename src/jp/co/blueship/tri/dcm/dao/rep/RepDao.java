package jp.co.blueship.tri.dcm.dao.rep;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dcm.dao.rep.constants.RepItems;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.RepEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.sm.dao.execdatasts.constants.ExecDataStsItems;

/**
 * The implements of the report DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RepDao extends JdbcBaseDao<IRepEntity> implements IRepDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return DcmTables.DCM_REP;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRepEntity entity ) {
		builder
			.append(RepItems.repId, entity.getRepId(), true)
			.append(RepItems.lotId, entity.getLotId())
			.append(RepItems.repCtgCd, entity.getRepCtgCd())
			.append(RepItems.execUserId, entity.getExecUserId())
			.append(RepItems.execUserNm, entity.getExecUserNm())
			.append(RepItems.procStTimestamp, entity.getProcStTimestamp() )
			.append(RepItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(RepItems.latestDlUserId, entity.getLatestDlUserId())
			.append(RepItems.latestDlUserNm, entity.getLatestDlUserNm())
			.append(RepItems.latestDlTimestamp, entity.getLatestDlTimestamp())
			.append(RepItems.stsId, entity.getStsId())
			.append(RepItems.delUserId, entity.getDelUserId())
			.append(RepItems.delUserNm, entity.getDelUserNm())
			.append(RepItems.delTimestamp, entity.getDelTimestamp())
			.append(RepItems.delCmt, entity.getDelCmt())
			.append(RepItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RepItems.regTimestamp, entity.getRegTimestamp())
			.append(RepItems.regUserId, entity.getRegUserId())
			.append(RepItems.regUserNm, entity.getRegUserNm())
			.append(RepItems.updTimestamp, entity.getUpdTimestamp())
			.append(RepItems.updUserId, entity.getUpdUserId())
			.append(RepItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRepEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		RepEntity entity = new RepEntity();

		entity.setRepId( rs.getString(RepItems.repId.getItemName()) );
		entity.setLotId( rs.getString(RepItems.lotId.getItemName()) );
		entity.setRepCtgCd( rs.getString(RepItems.repCtgCd.getItemName()) );
		entity.setExecUserId( rs.getString(RepItems.execUserId.getItemName()) );
		entity.setExecUserNm( rs.getString(RepItems.execUserNm.getItemName()) );
		entity.setProcStTimestamp( rs.getTimestamp(RepItems.procStTimestamp.getItemName()) );
		entity.setProcEndTimestamp( rs.getTimestamp(RepItems.procEndTimestamp.getItemName()) );
		entity.setLatestDlUserId( rs.getString(RepItems.latestDlUserId.getItemName()) );
		entity.setLatestDlUserNm( rs.getString(RepItems.latestDlUserNm.getItemName()) );
		entity.setLatestDlTimestamp( rs.getTimestamp(RepItems.latestDlTimestamp.getItemName()) );
		entity.setStsId( rs.getString(RepItems.stsId.getItemName()) );
		entity.setProcStsId( rs.getString(ExecDataStsItems.procStsId.getItemName()) );
		entity.setDelUserId( rs.getString(RepItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(RepItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(RepItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(RepItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(RepItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(RepItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(RepItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(RepItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(RepItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(RepItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(RepItems.updUserNm.getItemName()) );

		return entity;
	}

}
