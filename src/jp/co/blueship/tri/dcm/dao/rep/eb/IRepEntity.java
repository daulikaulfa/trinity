package jp.co.blueship.tri.dcm.dao.rep.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;

/**
 * The interface of the report entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRepEntity extends IEntity, IEntityExecData {

	public String getRepId();
	public void setRepId(String repId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getRepCtgCd();
	public void setRepCtgCd(String repCtgCd);

	public String getExecUserId();
	public void setExecUserId(String execUserId);

	public String getExecUserNm();
	public void setExecUserNm(String ExecUserNm);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	public String getLatestDlUserId();
	public void setLatestDlUserId(String latestDlUserId);

	public String getLatestDlUserNm();
	public void setLatestDlUserNm(String latestDlUserNm);

	public Timestamp getLatestDlTimestamp();
	public void setLatestDlTimestamp(Timestamp latestDlTimestamp);

	public String getStsId();
	public void setStsId(String stsId);

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public StatusFlg getDelStsId();
	public void setDelStsId(StatusFlg delStsId);

	public String getRegUserId();
	public void setRegUserId(String regUserId);

	public String getRegUserNm();
	public void setRegUserNm(String regUserNm);

	public Timestamp getRegTimestamp();
	public void setRegTimestamp(Timestamp regTimestamp);

	public String getUpdUserId();
	public void setUpdUserId(String updUserId);

	public String getUpdUserNm();
	public void setUpdUserNm(String updUserNm);

	public Timestamp getUpdTimestamp();
	public void setUpdTimestamp(Timestamp updTimestamp);

	public String getProcStsId();

}
