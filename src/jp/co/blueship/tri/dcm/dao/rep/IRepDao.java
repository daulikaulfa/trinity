package jp.co.blueship.tri.dcm.dao.rep;

import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the report DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRepDao extends IJdbcDao<IRepEntity> {

}
