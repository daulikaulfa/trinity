package jp.co.blueship.tri.dcm.uix.reports;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportRemovalServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/reports")
public class ReportRemovalController extends TriControllerSupport<FlowReportRemovalServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportRemovalService;
	}

	@Override
	protected FlowReportRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportRemovalServiceBean bean = new FlowReportRemovalServiceBean();
		return bean;
	}

	@RequestMapping("/remove")
	public String remove(FlowReportRemovalServiceBean bean, TriModel model) {
		String view = TriView.Messages.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			model.getRedirectAttributes().addFlashAttribute( "result", bean );
			bean.getParam().setRequestType(RequestType.init);
			view = "redirect:/reports/redirect";

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowReportRemovalServiceBean bean, TriModel model) {
		String[] repIds = model.getRequestInfo().getParameterValues("repIds[]");
		if ( repIds != null && repIds.length > 0)
		bean.getParam().setSelectedRepIds(repIds);
	}

}
