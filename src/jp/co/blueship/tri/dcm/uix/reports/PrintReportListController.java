package jp.co.blueship.tri.dcm.uix.reports;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 */
@Controller
@RequestMapping("/reports/list/print")
public class PrintReportListController extends TriControllerSupport<FlowReportListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmPrintReportListService;
	}

	@Override
	protected FlowReportListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportListServiceBean bean = new FlowReportListServiceBean();
		return bean;
	}

	@RequestMapping
	public String search(FlowReportListServiceBean bean, TriModel model) {
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintReportList.value())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReportListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			String type = requestInfo.getParameter("condType");
			if (TriStringUtils.isNotEmpty(type)) {
				searchCondition.setType(DcmReportType.value(type));
			} else {
				searchCondition.setType(DcmReportType.none);
			}
			searchCondition
				.setStsId(requestInfo.getParameter("condStatus"))
				.setCtgId(requestInfo.getParameter("condCategory"))
				.setMstoneId(requestInfo.getParameter("condMilestone"))
				.setKeyword(requestInfo.getParameter("searchKeyword"))
				.setSelectedPageNo(1)
			;
			bean.getParam().setLinesPerPage(0);
		}
	}

}
