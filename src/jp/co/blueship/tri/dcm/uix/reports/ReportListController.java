package jp.co.blueship.tri.dcm.uix.reports;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.dcm.constants.DcmReportType;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.ListSelection.ReportSelect;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.RequestOption;
import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 */
@Controller
@RequestMapping("/reports")
public class ReportListController extends TriControllerSupport<FlowReportListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportListService;
	}

	@Override
	protected FlowReportListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportListServiceBean bean = new FlowReportListServiceBean();
		return bean;
	}
	
	@RequestMapping(value = "/redirect")
	public String redirect( FlowReportListServiceBean bean, TriModel model ){
		return list( bean , model.setRedirect(true) );
	}

	@RequestMapping(value = { "/", "", "/list" })
	public String list(FlowReportListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReportList.value())
			.addAttribute("selectedMenu", "reportsMenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/search")
	public String search(FlowReportListServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReportList.value())
			.addAttribute("selectedMenu", "reportsMenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping("/select")
	public @ResponseBody String[] selectReport(FlowReportListServiceBean bean, TriModel model) {

		try {
			bean.getParam().setRequestOption(RequestOption.selectResource);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
		Set<String> selectedIdSet = bean.getParam().getListSelection().getSelectedIdSet();
		return selectedIdSet.toArray(new String[selectedIdSet.size()]);
	}

	@RequestMapping("/clear")
	public @ResponseBody String clear(FlowReportListServiceBean bean, TriModel model) {
		String message = "success";

		try {
			bean.getParam().setRequestOption(RequestOption.unselectResource);
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
		} catch (Exception e) {
			message = "fail";
			ExceptionUtils.printStackTrace(e);
		}
		return message;
	}

	@RequestMapping(value = "/filter/save" )
	public String save(FlowReportListServiceBean bean, TriModel model ){
		String view = "redirect:/searchfilter/create";


		try{
			FlowSearchFilterCreationServiceBean searchFilterBean = new FlowSearchFilterCreationServiceBean();

			ISessionInfo sesInfo = model.getSessionInfo();
			searchFilterBean.getParam().getInputInfo()
				.setLotId(this.getSessionSelectedLot(sesInfo, bean))
				.setFilterNm( model.getRequestInfo().getParameter("filterNm") )
				.setServiceId( getServiceId().value() )
				.setSearchFilter( bean.getParam().getSearchCondition() );

			model.getRedirectAttributes().addFlashAttribute( TriModelAttributes.RedirectFilter.value() , searchFilterBean );

		} catch ( Exception e ) {
			e.printStackTrace();
		}

		return view;
	}

	@RequestMapping(value = "/filter/search" )
	public String receive(FlowReportListServiceBean bean, TriModel model ){
		model.setRedirect(true);
		String json = null;
		if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectFilter) ) {
			json = model.valueOf(TriModelAttributes.RedirectFilter);
			ISessionInfo sesInfo = model.getSessionInfo();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		FlowReportListServiceBean.SearchCondition condition = gson.fromJson(json, FlowReportListServiceBean.SearchCondition.class);
		bean.getParam().setSearchCondition( condition );
		bean.getParam().setLinesPerPage(5);
		bean.getParam().getSearchCondition().setSelectedPageNo(1);


		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.ReportList.value());
		model.getModel().addAttribute("selectedMenu", "reportsMenu");
		model.getModel().addAttribute("pagination", bean.getPage());
		model.getModel().addAttribute("result", bean);

		setPrev(model);
		return view;


	}

	private void mapping(FlowReportListServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			if(RequestOption.selectResource.equals(bean.getParam().getRequestOption())) {
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				ReportSelect[] reportArr = gson.fromJson(model.getRequestInfo().getParameter("reportSelect"), ReportSelect[].class);
				bean.getParam().getListSelection().setReportSelect( new ArrayList<ReportSelect>( Arrays.asList(reportArr) ) );
				return;
			}
			String selectedPageNo = requestInfo.getParameter("selectedPageNo");
			if (TriStringUtils.isNotEmpty(selectedPageNo)) {
				searchCondition.setSelectedPageNo(Integer.parseInt(selectedPageNo));
			} else {
				searchCondition.setSelectedPageNo(1);
			}
			String type = requestInfo.getParameter("condType");
			if (TriStringUtils.isNotEmpty(type)) {
				searchCondition.setType(DcmReportType.value(type));
			} else {
				searchCondition.setType(DcmReportType.none);
			}
			searchCondition.setStsId(requestInfo.getParameter("condStatus"));
			searchCondition.setCtgId(requestInfo.getParameter("condCategory"));
			searchCondition.setMstoneId(requestInfo.getParameter("condMilestone"));
			searchCondition.setKeyword(requestInfo.getParameter("searchKeyword"));
		}
	}
}
