package jp.co.blueship.tri.dcm.uix.reports;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/reports/download")
public class ReportDownloadController extends TriControllerSupport<FlowReportDownloadServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportDownloadService;
	}

	@Override
	protected FlowReportDownloadServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportDownloadServiceBean bean = new FlowReportDownloadServiceBean();
		return bean;
	}

	@RequestMapping(value = { "/", "" })
	public void download(FlowReportDownloadServiceBean bean, TriModel model, HttpServletResponse response) {
		try {
			StreamUtils.download(response, bean.getFileResponse().getFile());
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
		return;
	}

	@RequestMapping("/validate")
	public String check(FlowReportDownloadServiceBean bean, TriModel model) {
		String view = TriView.Messages.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowReportDownloadServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String repId = requestInfo.getParameter("reportId");
		bean.getParam().setSelectedRepId(repId);
	}

}
