package jp.co.blueship.tri.dcm.uix.reports;

import java.util.Arrays;
import java.util.Set;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportBuildPackageCreationServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 */
@Controller
@RequestMapping("/reports/create")
public class ReportBuildPackageCreationController extends TriControllerSupport<FlowReportBuildPackageCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportBuildPackageCreationService;
	}

	@Override
	protected FlowReportBuildPackageCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportBuildPackageCreationServiceBean bean = new FlowReportBuildPackageCreationServiceBean();
		return bean;
	}

	@RequestMapping("/buildPackage")
	public String search(FlowReportBuildPackageCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/reports/redirect";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.ReportBuildPackageCreation.value())
			.addAttribute("selectedMenu", "reportsMenu")
			.addAttribute("pagination", bean.getPage())
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	private void mapping(FlowReportBuildPackageCreationServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();
		IRequestInfo requestInfo = model.getRequestInfo();

		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		if (RequestType.onChange.equals(bean.getParam().getRequestType())) {
			String selectedPageNo = requestInfo.getParameter("selectedPageNo");
			if (TriStringUtils.isNotEmpty(selectedPageNo)) {
				bean.getParam().setSelectedPageNo(Integer.parseInt(selectedPageNo));
			} else {
				bean.getParam().setSelectedPageNo(1);
			}

			String builtPackageIds = requestInfo.getParameter("selectedId");
			String[] bpIds = {};
			if(TriStringUtils.isNotEmpty(builtPackageIds)) {
				bpIds = builtPackageIds.split(",");
				Set<String> allSelectedIds = bean.getParam().getListSelection().getSelectedIdSet();
				allSelectedIds.addAll(Arrays.asList(bpIds));

				bean.getParam().getListSelection().setSelectedIds(bpIds);
			} else {
				bean.getParam().getListSelection().setSelectedIds(bpIds);
			}

		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			String builtPackageIds = requestInfo.getParameter("selectedId");
			String[] bpIds = {};
			if(TriStringUtils.isNotEmpty(builtPackageIds)) {
				bpIds = builtPackageIds.split(",");
				Set<String> allSelectedIds = bean.getParam().getListSelection().getSelectedIdSet();
				allSelectedIds.addAll(Arrays.asList(bpIds));

				bean.getParam().getListSelection().setSelectedIds(bpIds);
			} else {
				bean.getParam().getListSelection().setSelectedIds(bpIds);
			}
		}
	}
}
