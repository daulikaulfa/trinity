package jp.co.blueship.tri.dcm.uix.reports;

import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.dcm.domainx.reports.dto.FlowReportMultipleDownloadServiceBean;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

@Controller
@RequestMapping("/reports/multipledownload")
public class ReportMultipleDownloadController extends TriControllerSupport<FlowReportMultipleDownloadServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.DcmReportMultipleDownloadService;
	}

	@Override
	protected FlowReportMultipleDownloadServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowReportMultipleDownloadServiceBean bean = new FlowReportMultipleDownloadServiceBean();
		return bean;
	}

	@RequestMapping(value = { "/", "" })
	public void download(FlowReportMultipleDownloadServiceBean bean, TriModel model, HttpServletResponse response) {
		try {
			StreamUtils.download(response, bean.getReport());
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
		return;
	}

	@RequestMapping("/validate")
	public String check(FlowReportMultipleDownloadServiceBean bean, TriModel model, HttpServletResponse response) {
		String view = TriView.Messages.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowReportMultipleDownloadServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		String lotId = this.getSessionSelectedLot(model.getSessionInfo(), bean);
		String[] reportId = requestInfo.getParameterValues("reportId[]");

		bean.getParam().setSelectedLotId(lotId);
		if ( reportId != null && reportId.length >  0 )
		bean.getParam().setRepId(reportId);
	}

}
