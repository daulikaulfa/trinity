package jp.co.blueship.tri.dcm;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class DcmExportToFileUtils {

	/*
	 * Convert html to ExportToFileBean ExportToFileBean ( use in Controller )
	 */
	public static ExportToFileBean createExportToFileBean(String htmlTable) {
		ExportToFileBean fileBean = new ExportToFileBean();
		String parseHtmlTable = "<html><body><table>" + htmlTable + "</table></body></html>";

		Document doc = Jsoup.parse(parseHtmlTable);

		for (Element element : doc.select("th")) {
			fileBean.getTitleHeader().add(element.text());
		}

		int columnCount = fileBean.getTitleHeader().size();

		List<String> row = new ArrayList<String>();

		for (Element element : doc.select("td")) {
			if (row.size() == columnCount) {
				fileBean.addRow(row);
				row = new ArrayList<String>();
			}

			if ("attachment".equals(element.attr("data-column"))) {
				if (element.children() != null && element.children().get(0).hasClass("fa-paperclip")) {
					row.add("yes");
				} else {
					row.add("");
				}
			} else {
				row.add(element.text());
			}
		}

		fileBean.addRow(row);

		return fileBean;
	}

	/*
	 * Create download file name ( use in Controller )
	 */
	public static String getDownloadFileName(ServiceId serviceId, String extention, String locale, TimeZone zone) {
		String service = serviceId.value();

		service = StringUtils.stripStart(service, "Flow");

		if (service.endsWith("Service")) {
			service = StringUtils.stripEnd(service, "Service");
		}

		service = StringUtils.replace(service, "Service", "_");

		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormatOfExportFile(locale, zone);
		String fileName = service + "-" + format.format(TriDateUtils.getSystemTimestamp());

		return fileName + "." + extention;
	}

	/*
	 * Create exportBean to CSV file ( use in domain service )
	 */
	public static File createCsvFile(Path directory, ExportToFileBean exportBean) {
		File file = null;
		OutputStreamWriter writer = null;
		CSVPrinter csvPrinter = null;

		try  {
			file = Files.createTempFile(directory, "ServiceId-", ".csv").toFile();

			writer = new OutputStreamWriter(new FileOutputStream(file), Charset.UTF_8.value());
			csvPrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withRecordSeparator(System.lineSeparator()));

			List<String> titleHeader = exportBean.getTitleHeader();
			for (int i = 0; i < titleHeader.size(); i++) {
				csvPrinter.print(titleHeader.get(i));
			}
			csvPrinter.println();

			List<List<String>> content = exportBean.getContents();
			for (int i = 0; i < content.size(); i++) {
				List<String> row = content.get(i);
				for (int j = 0; j < row.size(); j++) {
					csvPrinter.print(row.get(j));
				}
				csvPrinter.println();
			}

			csvPrinter.flush();
			writer.flush();
		} catch (IOException e) {
			throw new TriSystemException(DcmMessageId.DCM005029S, e);
		} finally {
			try {
				if ( null != csvPrinter ) {
						csvPrinter.close();
				}

				if ( null != writer ) {
					writer.close();
				}
			} catch (IOException e) {
				throw new TriSystemException(DcmMessageId.DCM005029S, e);
			}

		}

		return file;
	}

	/*
	 * Create exportBean to Excel file ( use in domain service )
	 */
	public static File createExcelFile(Path directory, ExportToFileBean exportBean) {
		File file = null;;
		FileOutputStream fileOut = null;

		try  {
			file = Files.createTempFile(directory, "ServiceId-", ".xls").toFile();
			fileOut = new FileOutputStream(file);

			Workbook wb = new HSSFWorkbook();
			Sheet sheet1 = wb.createSheet("trinity");
			Row rowHeader = sheet1.createRow(0);

			List<String> titleHeader = exportBean.getTitleHeader();
			for (int i = 0; i < titleHeader.size(); i++) {
				rowHeader.createCell(i).setCellValue(titleHeader.get(i));
			}

			Row rowBody = sheet1.createRow(1);
			List<List<String>> content = exportBean.getContents();
			for (int i = 0; i < content.size(); i++) {
				List<String> row = content.get(i);
				for (int j = 0; j < row.size(); j++) {
					rowBody.createCell(j).setCellValue(row.get(j));
				}
				rowBody = sheet1.createRow(i + 2);
			}

			wb.write(fileOut);
			fileOut.flush();
		} catch (IOException e) {
			throw new TriSystemException(DcmMessageId.DCM005029S, e);
		} finally {
			try {
				if ( null != fileOut ) {
					fileOut.close();
				}
			} catch (IOException e) {
				throw new TriSystemException(DcmMessageId.DCM005029S, e);
			}
		}

		return file;
	}

	public static File createCSV(DomainServiceBean paramBean, ExportToFileBean fileBean) {
		Path directory = Paths.get(DcmDesignBusinessRuleUtils.getDownloadTempPath(paramBean.getUserId()).getAbsolutePath());
		return DcmExportToFileUtils.createCsvFile(directory,fileBean);
	}

	public static File createExcel(DomainServiceBean paramBean, ExportToFileBean fileBean) {
		Path directory = Paths.get(DcmDesignBusinessRuleUtils.getDownloadTempPath(paramBean.getUserId()).getAbsolutePath());
		return DcmExportToFileUtils.createExcelFile(directory,fileBean);
	}

	/*
	 * Create exportBean to pdf file ( use in domain service )
	 */
	public static File createPdfFile(Path directory, ExportToFileBean exportBean) throws IOException {

		File file = null;
		FileOutputStream fileOut = null;

		try  {
			file = Files.createTempFile(directory, "ServiceId-", ".pdf").toFile();
			fileOut = new FileOutputStream(file);
			fileOut.flush();
		} catch (IOException e) {
			throw new TriSystemException(DcmMessageId.DCM005029S, e);
		} finally {
			try {
				if ( null != fileOut ) {
					fileOut.close();
				}
			} catch (IOException e) {
				throw new TriSystemException(DcmMessageId.DCM005029S, e);
			}
		}

		return file;
	}

}
