package jp.co.blueship.tri.dcm;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class ReportStringUtils {
	
	/**
	 * Excel上の行ラベル（1～）を0～の文字列に変換する
	 * @param row Excel上の行ラベル
	 * @return 0～の文字列
	 */
	public static String ConvertExcelRowLabel( String row ) {
		if( TriStringUtils.isEmpty( row ) ) {
			return null ;
		}
		
		int rowValue = Integer.parseInt( row ) - 1 ;
		
		return String.valueOf( rowValue ) ;
	}
	
	/**
	 * Excel上の列ラベル（A～）を0～の文字列に変換する
	 * @param column Excel上の列ラベル
	 * @return 0～の文字列
	 */
	public static String ConvertExcelColumnLabel( String column ) {
		if( TriStringUtils.isEmpty( column ) ) {
			return null ;
		}
		
		if( 1 == column.length() ) {
			int columnValue = column.charAt( 0 ) - 'A' ;
			
			return String.valueOf( columnValue ) ;
		} else if( 2 == column.length() ) {
			int hi = column.charAt( 0 ) - 'A' ;
			int low = column.charAt( 1 ) - 'A' ;
			
			int columnValue = ( ( hi + 1 ) * 26 ) + low ; 
			
			return String.valueOf( columnValue ) ;
		} else {
			return null ;
		}
	}
}
