package jp.co.blueship.tri.bm;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * {@link jp.co.blueship.tri.fw.cmn.utils.collections.FluentList}のサポートUtils
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class BmFluentFunctionUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static final TriFunction<IBpEntity, String> toBpId = new TriFunction<IBpEntity, String>() {
		public String apply(IBpEntity input) {
			return input.getBpId();
		}
	};

	public static final TriFunction<IBpDto, String> toBpIdFromBpDto = new TriFunction<IBpDto, String>() {
		public String apply(IBpDto input) {
			return input.getBpEntity().getBpId();
		}
	};

	public static final TriFunction<IBpAreqLnkEntity, String> toBpIdFromBpAreqLnk = new TriFunction<IBpAreqLnkEntity, String>() {
		public String apply(IBpAreqLnkEntity input) {
			return input.getBpId();
		}
	};

	public static final TriFunction<IBpAreqLnkEntity, String> toAreqIdFromBpAreqLnk = new TriFunction<IBpAreqLnkEntity, String>() {
		public String apply(IBpAreqLnkEntity input) {
			return input.getAreqId();
		}
	};

	public static final TriFunction<IStatusId, ItemLabelsBean> toItemLabelsFromStatusId = new TriFunction<IStatusId, ItemLabelsBean>() {
		@Override
		public ItemLabelsBean apply(IStatusId input) {
			return new ItemLabelsBean( sheet.getValue(BmDesignBeanId.statusId, input.getStatusId()), input.getStatusId());
		}
	};

}
