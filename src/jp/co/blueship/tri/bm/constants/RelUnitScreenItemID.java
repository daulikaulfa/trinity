package jp.co.blueship.tri.bm.constants;

/**
 *
 * ビルドパッケージ作成画面項目ID
 *
 */
public class RelUnitScreenItemID {

	/**
	 * ビルドパッケージ基本情報入力
	 */
	public enum RelUnitInfoInput {
		// ビルドパッケージ名
		NAME("unit_name"),
		// ビルドパッケージ概要
		SUMMARY("summary");

		private String itemName;

		RelUnitInfoInput( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * ビルドパッケージクローズ受付確認
	 */
	public enum RelUnitCloseRequestCheck {
		// クローズコメント
		CLOSE_COMMENT( "closeComment" ),
		// ベースライン
		BASE_LINE_TAG( "baseLineTag" );

		private String itemName;

		RelUnitCloseRequestCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}

	/**
	 * ビルドパッケージ取消確認
	 */
	public enum RelUnitCancelCheck {
		// 取消コメント
		CANCEL_COMMENT( "cancelComment" );

		private String itemName;

		RelUnitCancelCheck( String itemName ) {
			this.itemName = itemName;
		}

		public String toString() {
			return itemName;
		}
	}
}

