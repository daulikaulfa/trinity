package jp.co.blueship.tri.bm.constants;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum BuildType {
	none,
	Build,
	FullBuild,
	;
}
