package jp.co.blueship.tri.bm.constants;


/**
 * 遷移用画面IDの定義です。
 */
public class RelUnitScreenID {

	/**
	 * ビルドパッケージ作成トップ画面
	 */
	public static final String TOP = "RelUnitTop";
	/**
	 *  パッケージ情報入力画面
	 */
	public static final String UNIT_INFO_INPUT = "RelUnitInfoInput";
	/**
	 *  ロット選択画面
	 */
	public static final String LOT_SELECT = "RelUnitLotSelect";
	/**
	 *  変更管理選択画面
	 */
	public static final String PJT_SELECT = "RelUnitPjtSelect";
	/**
	 * ビルドパッケージ作成確認画面
	 */
	public static final String UNIT_COMFIRM = "RelUnitConfirm";
	/**
	 * ビルドパッケージ作成受付完了
	 */
	public static final String COMP_REQUEST = "RelUnitCompRequest";
	/**
	 * ビルドパッケージ作成状況画面
	 */
	public static final String UNIT_GENERATE_DETAIL_VIEW = "RelUnitGenerateDetailView";
	/**
	 * 変更管理詳細画面
	 */
	public static final String PJT_DETAIL_VIEW = "RelUnitPjtDetailView";
	/**
	 * ロット履歴一覧画面
	 */
	public static final String LOT_HISTORY_LIST = "RelUnitLotHistoryList";
	/**
	 * ビルドパッケージ作成履歴一覧画面
	 */
	public static final String UNIT_HISTORY_LIST = "RelUnitHistoryList";
	/**
	 * ビルドパッケージ履歴詳細画面
	 */
	public static final String UNIT_HISTORY_DETAIL_VIEW = "RelUnitHistoryDetailView";

	/**
	 * ビルドパッケージクローズトップ画面
	 */
	public static final String UNIT_CLOSE_TOP = "RelUnitCloseTop";
	/**
	 * ビルドパッケージクローズ一覧画面
	 */
	public static final String UNIT_CLOSE_LIST = "RelUnitCloseList";
	/**
	 * ビルドパッケージクローズ受付確認画面
	 */
	public static final String UNIT_CLOSE_REQUEST_COMFIRM = "RelUnitCloseRequestConfirm";
	/**
	 * ビルドパッケージクローズ受付完了画面
	 */
	public static final String COMP_UNIT_CLOSE_REQUEST = "RelUnitCompUnitCloseRequest";
	/**
	 * ビルドパッケージクローズ履歴一覧画面
	 */
	public static final String UNIT_CLOSE_HISTORY_LIST = "RelUnitCloseHistoryList";

	/**
	 * ビルドパッケージ取消トップ画面
	 */
	public static final String UNIT_CANCEL_TOP = "RelUnitCancelTop";
	/**
	 * ビルドパッケージ取消一覧画面
	 */
	public static final String UNIT_CANCEL_LIST = "RelUnitCancelList";
	/**
	 * ビルドパッケージ取消受付確認画面
	 */
	public static final String UNIT_CANCEL_COMFIRM = "RelUnitCancelConfirm";
	/**
	 * ビルドパッケージ取消受付完了画面
	 */
	public static final String COMP_UNIT_CANCEL = "RelUnitCompUnitCancel";

}
