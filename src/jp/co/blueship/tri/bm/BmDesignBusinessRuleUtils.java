package jp.co.blueship.tri.bm;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByDirectory;
import jp.co.blueship.tri.fw.constants.DcmDesignEntryKeyByReport;
import jp.co.blueship.tri.fw.constants.DmDesignEntryKeyByDealAssetFtp;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class BmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ページ制御情報からページ番号情報に設定します。
	 * @param dest ページ番号情報
	 * @param src ページ制御
	 * @return ページ番号情報を戻します。
	 */
	public static final IPageNoInfo convertPageNoInfo( IPageNoInfo dest, ILimit src ) {

		if ( null == dest ){
			throw new TriSystemException( BmMessageId.BM005047S );
		}
		if ( null == src ) {
			dest.setMaxPageNo( new Integer(0) );
			dest.setSelectPageNo( new Integer(0) );
			return dest;
		}

		dest.setMaxPageNo( new Integer(src.getPageBar().getMaximum()) );
		dest.setSelectPageNo( new Integer(src.getPageBar().getValue()) );

		dest.setViewRows(src.getViewRows());
		dest.setMaxRows(src.getMaxRows());

		return dest;
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @return 取得した情報を戻します
	 */
	public static final File getMasterWorkPath(ILotEntity lotEntity) {

		return new File( lotEntity.getLotMwPath() );
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static File getMasterWorkPath( List<Object> list )
			throws TriSystemException {

		ILotDto lotDto = BmExtractEntityAddonUtils.extractPjtLotDto( list );

		if ( null == lotDto || null == lotDto.getLotEntity() ) {
			throw new TriSystemException( BmMessageId.BM005048S );
		} else {

			File file = new File( lotDto.getLotEntity().getLotMwPath() );

			return file;
		}
	}

	/**
	 * 原本のリポジトリを取得します。
	 *
	 * @param mdlEntity モジュールエンティティ
	 * @param ucfScmEntities ＳＣＭエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final String getMasterRepository(
			ILotMdlLnkEntity mdlEntity, List<IVcsReposDto> repositoryInfo ) throws TriSystemException {

		if ( null == mdlEntity ){
			throw new TriSystemException( BmMessageId.BM005049S );
		}
		if ( null == repositoryInfo ){
			throw new TriSystemException( BmMessageId.BM005050S );
		}

		String repository = null;

		for ( IVcsReposDto reposEntity : repositoryInfo ) {

			if ( mdlEntity.getMdlNm().equals( reposEntity.getVcsMdlEntity().getMdlNm() )) {
				repository =  reposEntity.getVcsReposEntity().getVcsReposUrl();
				break;
			}
		}

		if ( null == repository ){
			throw new TriSystemException( BmMessageId.BM004062F );
		}
		return repository;
	}

	/**
	 * ワークスペースフォルダを取得します。
	 * <br>パラメタのlistには、以下の情報が含まれていれば、パスを取得します。
	 * <li>LendsReturnInfo
	 * <br>
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException 取得元のマスタ情報にシステム運用上の誤りがある場合
	 */
/*	public static final File getWorkspacePath( List<Object> list )
			throws TriSystemException {
		ILotDto lotDto = BmExtractEntityAddonUtils.extractPjtLotDto( list );

		return getWorkspacePath( ( null == lotDto)? null: lotDto.getLotEntity() );
	}*/

	/**
	 * ワークスペースフォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getWorkspacePath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( BmMessageId.BM005051S );
		}
		if ( null == entity.getWsPath() ){
			throw new TriSystemException( BmMessageId.BM004063F );
		}
		file = new File( entity.getWsPath() );

		return file;
	}

	/**
	 * 履歴フォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryPath( ILotEntity entity ) throws TriSystemException {

		File file = null;

		if ( null == entity ){
			throw new TriSystemException( BmMessageId.BM005051S );
		}
		if ( null == entity.getHistPath() ){
			throw new TriSystemException( BmMessageId.BM004064F , entity.getHistPath() );
		}
		file = new File( entity.getHistPath() );

		if ( ! file.isDirectory() ) {
			throw new TriSystemException( BmMessageId.BM004065F , file.getPath() );
		}

		return file;
	}

	/**
	 * 公開(InOutBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static File getInOutBoxPath( ILotEntity lotEntity ) throws TriSystemException  {

		if ( null == lotEntity ) {
			throw new TriSystemException( BmMessageId.BM005048S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getInOutBox() ) ) {
			throw new TriSystemException( BmMessageId.BM005052S );
		}

		File file = new File( lotEntity.getInOutBox() );

		return file;
	}


	/**
	 * 内部(SystemInBox)パスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxPath( ILotEntity lotEntity ) throws TriSystemException  {
		if ( null == lotEntity ) {
			throw new TriSystemException( BmMessageId.BM005048S );
		}

		if ( TriStringUtils.isEmpty( lotEntity.getSysInBox() ) ) {
			throw new TriSystemException( BmMessageId.BM005053S );
		}

		File file = new File( 	lotEntity.getSysInBox()  );

		return file;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param lotEntity 対象となるロット
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxAssetBasePath( ILotEntity lotEntity ) throws TriSystemException  {
		File systemInBox = getSystemInBoxPath( lotEntity );

		return systemInBox;
	}

	/**
	 * 内部(SystemInBox)する申請の、資産格納のベースパスを取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	private static File getSystemInBoxAssetBasePath( List<Object> list ) throws TriSystemException  {

		ILotDto lotDto = BmExtractEntityAddonUtils.extractPjtLotDto( list );

		return getSystemInBoxAssetBasePath( ( null == lotDto)? null: lotDto.getLotEntity() );
	}


	/**
	 * ビルドパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param buildEntity ビルドパッケージエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelUnitDSLPath( ILotEntity lotEntity, IBpEntity buildEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryPath( lotEntity );

		if ( null == buildEntity )
			throw new TriSystemException( BmMessageId.BM005054S );

		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relUnitDSLRelationPath ),
				buildEntity.getBpId() )
				 );

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLPath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryPath( lotEntity );

		if ( null == relEntity ){
			throw new TriSystemException( BmMessageId.BM005055S );
		}
		String relativePath = TriStringUtils.linkPath( relEntity.getBldEnvId(), relEntity.getRpId() );

		file = new File( historyFile, TriStringUtils.linkPath(
				sheet.getValue( AmDesignEntryKeyByDirectory.relDSLRelationPath ),
				relativePath )
				 );

		return file;
	}

	/**
	 * リリースパッケージのＤＳＬ資産の配付資源ファイル名を取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param relEntity リリースエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getHistoryRelDSLFilePath( ILotEntity lotEntity, IRpEntity relEntity ) throws TriSystemException {
		File file = null;

		File historyFile = getHistoryRelDSLPath( lotEntity, relEntity );

		String historyFileName = TriStringUtils.linkPath(historyFile.getPath(), relEntity.getBldEnvId());
		file = new File( historyFileName +  sheet.getValue( DmDesignEntryKeyByDealAssetFtp.srcExtension ) );

		return file;
	}

	/**
	 * サービス単位のログの管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param flowAction 実行サービス名ID
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspaceLogPath( ILotEntity lotEntity, String flowActionId ) throws TriSystemException {
		File file = null;

		File workspaceFile = getWorkspacePath( lotEntity );

		if ( null == flowActionId ){
			throw new TriSystemException( BmMessageId.BM005056S );
		}
		file = new File( workspaceFile, TriStringUtils.linkPath(
												sheet.getValue( UmDesignEntryKeyByCommon.logRelationPath ),
												flowActionId )
										 );

		return file;
	}

	/**
	 * 返却資産承認の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetPath( List<Object> list )
			throws TriSystemException {

		File file = new File( 	getSystemInBoxAssetBasePath( list ).getPath());

		return file;
	}

	/**
	 * 返却資産承認の申請番号単位の格納フォルダ名を取得します。
	 *
	 * @param list 検索するリスト
	 * @param entity 対象となる申請
	 * @return 取得した格納フォルダを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final File getReturnAssetApplyNoPath( List<Object> list, IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( BmMessageId.BM005058S );
		}

		File file = new File( 	getReturnAssetPath( list ).getPath(),
								getAssetApplyPathName( entity ));

		return file;
	}

	/**
	 * 申請の格納フォルダ名を取得します。
	 *
	 * @param entity 貸出返却情報申請
	 * @return 取得した格納フォルダ名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final String getAssetApplyPathName( IAreqEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( BmMessageId.BM005059S );
		}

		return entity.getAreqId();
	}

	/**
	 * レポートのファイル名を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得したファイル名を戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */

	public static final File getReportFile( List<Object> list )
			throws TriSystemException {

		IRepEntity entity = BmExtractEntityAddonUtils.extractReport( list );
		if ( null == entity ){
			throw new TriSystemException( BmMessageId.BM005059S );
		}
		File path = new File( sheet.getValue(DcmDesignEntryKeyByReport.reportFilePath) );
		if ( ! path.isDirectory() )
			path.mkdirs();

		String extension = sheet.getValue( DcmDesignEntryKeyByReport.reportFileExtension ) ;
		extension = ( '.' != extension.charAt( 0 ) ) ? "." + extension : extension ;//.が拡張子文字列の先頭になければ付加

		return new File(path, entity.getRepId() + extension );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlEntity モジュールエンティティー
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	public static final String convertPathInModule( String relativePath, ILotMdlLnkEntity mdlEntity ) {
		return convertPathInModule( relativePath, mdlEntity.getMdlNm() );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlName モジュール名
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	private static final String convertPathInModule( String relativePath, String mdlName ) {
		String tempPath = TriStringUtils.convertPath( relativePath );

		if ( tempPath.startsWith("/") )
			tempPath = StringUtils.substringAfter(tempPath, "/");

		if( StringUtils.substringBefore( tempPath, "/" ).equals( mdlName) ) {
			return tempPath.substring( tempPath.indexOf( "/" ) + 1 ) ;//「モジュール名」部分および先頭の"/"を取り除く
		}

		return null;
	}

	/**
	 * テンプレートフォルダを取得します。
	 *
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getTemplatePath() throws TriSystemException {

		return new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
							sheet.getValue( UmDesignEntryKeyByCommon.templatesRelativePath ));
	}
}
