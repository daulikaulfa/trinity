package jp.co.blueship.tri.bm;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsReposDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.dcm.dao.rep.eb.IRepEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。
 * ExtractEntityAddonUtilの派生クラスです。依存性を解消するために作成しました。
 *
 * @author Takashi Ono
 *
 */
public class BmExtractEntityAddonUtils {

	/**
	 * パラメータリストより申請情報エンティティの配列を取得します。
	 *
	 * @param list パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final AreqDtoList extractAssetApplyArray(List<Object> list) throws TriSystemException {

		for (Object obj : list) {
			if (obj instanceof AreqDtoList) {
				return (AreqDtoList) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからロット情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotDto extractPjtLotDto(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof ILotDto) {
				return (ILotDto) obj;
			}
		}

		return null;
	}
	/**
	 * 指定されたリストからロット情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotEntity extractPjtLot(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof ILotEntity) {
				return (ILotEntity) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからビルドパッケージDTOを取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static IBpDto extractBpDto(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof IBpDto) {
				return (IBpDto) obj;
			}
		}

		return null;
	}

	/**
	 * 指定されたリストからリポジトリ情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static List<IVcsReposDto> extractUcfScmEntity(List<Object> list) {
		for (Object obj : list) {
			if (obj instanceof IVcsReposDto[]) {
				return FluentList.from((IVcsReposDto[])obj).asList();
			}
		}

		return null;
	}

	/**
	 * パラメータリストより外部入力パラメータの格納beanを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final GenericServiceBean getGenericServiceBean(List<Object> paramList) throws TriSystemException {

		for (Object obj : paramList) {
			if (obj instanceof GenericServiceBean) {
				return ((GenericServiceBean) obj);
			}
		}

		return null;
	}

	/**
	 * 指定されたリストから資産agentのステータス情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static AgentStatus[] extractAgentStatusArray(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof AgentStatus[]) {
				return (AgentStatus[]) obj;
			}
		}

		return null;
	}

	/**
	 * パラメータリストよりIBuildTaskBeanを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IBuildTaskBean extractBuildTask(List<Object> paramList) throws TriSystemException {

		for (Object obj : paramList) {
			if (obj instanceof IBuildTaskBean) {
				return ((IBuildTaskBean) obj);
			}
		}

		return null;
	}
	/**
	 * 指定されたリストからレポート情報エンティティを取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final IRepEntity extractReport(List<Object> list) throws TriSystemException {

		for (Object obj : list) {

			if (obj instanceof IRepEntity) {
				return ((IRepEntity) obj);
			}
		}

		return null;
	}

	/**
	 * ビルドパッケージDtoから申請番号を取得する。
	 * @param dto ビルドパッケージDto
	 * @return 申請番号の配列
	 */
	public static final String[] getAreqIds( List<IBpDto> dto ) {

		List<IBpAreqLnkEntity> entityList = new ArrayList<IBpAreqLnkEntity>();
		for ( IBpDto entity : dto ) {
			entityList.addAll( entity.getBpAreqLnkEntities() );
		}

		Set<String> areqIdSet = new TreeSet<String>();

		for ( IBpAreqLnkEntity entity : entityList ) {
			areqIdSet.add( entity.getAreqId() );
		}

		return (String[])areqIdSet.toArray( new String[0] );
	}

}
