package jp.co.blueship.tri.bm.ui;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;

/**
 * 業務サービスBeanとレスポンスBeanとの相互変換を行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BmCnvActionToServiceUtils {


	/**
	 * サービスメッセージへの変換
	 *
	 * @param reqInfo
	 * @param bean
	 * @return
	 */
	public static RelUnitSearchBean convertRelUnitSearchBean( IRequestInfo reqInfo, RelUnitSearchBean bean ) {

		if ( null == bean )
			bean = new RelUnitSearchBean();

		String searchBuildNo = reqInfo.getParameter( "searchBuildNo" ) ;
		bean.setSearchBuildNo( searchBuildNo ) ;

		String searchRelNo = reqInfo.getParameter( "searchRelNo" ) ;
		bean.setSearchRelNo( searchRelNo ) ;

		String searchBuildName = reqInfo.getParameter( "searchBuildName" ) ;
			bean.setSearchBuildName( searchBuildName ) ;

		String searchPjtNo = reqInfo.getParameter( "searchPjtNo" ) ;
		bean.setSearchPjtNo( searchPjtNo ) ;

		String searchChangeCauseNo = reqInfo.getParameter( "searchChangeCauseNo" ) ;
		bean.setSearchChangeCauseNo( searchChangeCauseNo ) ;

		String selectedRelUnitCount = reqInfo.getParameter( "searchRelUnitCount" ) ;
		bean.setSelectedRelUnitCount( selectedRelUnitCount ) ;

		return bean;
	}
	public static UnitInfoInputBean convertRelUnitEntryUnitInfoInputBean( IRequestInfo reqInfo ) {

		UnitInfoInputBean inputBean = new UnitInfoInputBean();

		if ( null != reqInfo.getParameterValues( "unitName" )) {
			inputBean.setUnitName(reqInfo.getParameter( "unitName" ));
		}
		if ( null != reqInfo.getParameterValues( "unitSummary" )) {
			inputBean.setUnitSummary(reqInfo.getParameter( "unitSummary" ));
		}

		return inputBean;
	}

	public static String[] convertRelUnitEntryPjtSelectBean( IRequestInfo reqInfo ) {

		String[] pjtNo = null;

		if ( null != reqInfo.getParameterValues( "selectedPjtNo[]" )) {

			pjtNo = reqInfo.getParameterValues( "selectedPjtNo[]" );

			Set<String> pjtIdSet = new TreeSet<String>();
			pjtIdSet.addAll( FluentList.from(pjtNo).asList());

			pjtNo = (String[])pjtIdSet.toArray( new String[0] );

		}

		return pjtNo;
	}

	public static String[] convertRelUnitEntryClosedPjtSelectBean( IRequestInfo reqInfo ) {

		String[] pjtNo = null;

		if ( null != reqInfo.getParameterValues( "selectedClosedPjtNo[]" )) {

			pjtNo = reqInfo.getParameterValues( "selectedClosedPjtNo[]" );

			Set<String> pjtIdSet = new TreeSet<String>();
			pjtIdSet.addAll( FluentList.from(pjtNo).asList());

			pjtNo = (String[])pjtIdSet.toArray( new String[0] );
		}

		return pjtNo;
	}

	public static String[] convertRelUnitEntryLotSelectBean( IRequestInfo reqInfo ) {

		String[] lotNoServerNo	= null;

		if ( null != reqInfo.getParameterValues( "selectedLotNo" )) {
			String values = reqInfo.getParameter( "selectedLotNo" );
			lotNoServerNo = values.split( "," );
		}

		return 	lotNoServerNo;
	}

	public static String convertRelUnitEntryTopBean( IRequestInfo reqInfo ) {

		String allAssetCompile = StatusFlg.off.value();

		if ( null != reqInfo.getParameter( "allAssetCompile" )) {

			if ( "1".equals( reqInfo.getParameter( "allAssetCompile" ))) {
				allAssetCompile = StatusFlg.on.value();
			}
		}

		return 	allAssetCompile;
	}

	/**
	 * レポート用にチェックを入れたビルドパッケージ番号を保持する
	 * @param session ISessionInfo
	 * @param reqInfo IRequestInfo
	 * @return 保持されたビルドパッケージ番号のセット
	 */
	public static Set<String> saveSelectedRelUnitNoForReport(
										ISessionInfo session, IRequestInfo reqInfo ) {

		String forward = reqInfo.getParameter( "forward" );
		String referer = reqInfo.getParameter( "referer" );

		// ビルドパッケージ履歴一覧内遷移、またはビルドパッケージ履歴詳細画面からの遷移以外はクリア
		if ( !RelUnitScreenID.UNIT_HISTORY_LIST.equals( referer ) &&
				!RelUnitScreenID.UNIT_HISTORY_DETAIL_VIEW.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		}
		if ( !"register".equals( forward ) && !RelUnitScreenID.UNIT_HISTORY_LIST.equals( referer )) {

			session.removeAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		}


		// 選択されたビルドパッケージ番号
		String[] relUnitNoArray				= reqInfo.getParameterValues( "relUnitNo[]" );
		Set<String> tmpSelectedRelUnitNo	= new HashSet<String>();
		if ( null != relUnitNoArray) {
			for ( String pjtNo : relUnitNoArray ) {
				tmpSelectedRelUnitNo.add( pjtNo );
			}
		}

		if ( "register".equals( forward ) && RelUnitScreenID.UNIT_HISTORY_DETAIL_VIEW.equals( referer )) {
			return tmpSelectedRelUnitNo;
		}


		// 画面に表示されていたビルドパッケージ番号
		@SuppressWarnings("unchecked")
		Set<String> viewRelUnitNoSet		=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.VIEW_REPORT_NO );
		// 選択済みのビルドパッケージ番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelUnitNoSet	=
			(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );
		if ( null == selectedRelUnitNoSet ) {
			selectedRelUnitNoSet = tmpSelectedRelUnitNo;
		}

		if ( null != viewRelUnitNoSet ) {

			// 今回、選択されなかったビルドパッケージ番号
			Set<String> unSelectedRelUnitNo = new HashSet<String>();
			for ( String relNo : viewRelUnitNoSet ) {

				if ( !tmpSelectedRelUnitNo.contains(relNo )) {
					unSelectedRelUnitNo.add( relNo );
				}
			}

			selectedRelUnitNoSet.addAll		( tmpSelectedRelUnitNo );
			selectedRelUnitNoSet.removeAll	( unSelectedRelUnitNo );
		}
		session.setAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO, selectedRelUnitNoSet );

		return selectedRelUnitNoSet;
	}
}
