package jp.co.blueship.tri.bm.ui;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean.BuildProcessViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCompileDetailViewServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitPjtDetailViewServiceBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitCompRequestResponseBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitGenerateDetailViewResponseBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitLotSelectResponseBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitPjtDetailViewResponseBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitPjtSelectResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;

/**
 *
 * @version V3L10.01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class BmCnvServiceToActionUtils {

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public static RelUnitLotSelectResponseBean convertRelUnitLotSelectResponseBean(
			FlowRelUnitEntryServiceBean bean ) {

		RelUnitLotSelectResponseBean lotResBean = new RelUnitLotSelectResponseBean();

		lotResBean.setBuildLotList		( bean.getLotSelectBean().getLotViewBeanList() );
		lotResBean.setAllAssetCompile	( bean.getAllAssetCompile() );

		if ( bean.getSelectedLotNo() != null ) {
			lotResBean.setSelectedLotNo( bean.getSelectedLotNo() );
		}

		if ( null != bean.getSelectedServerNo() ) {
			lotResBean.setSelectedServerNo( bean.getSelectedServerNo() );
		}

		return lotResBean;
	}

	public static RelUnitPjtSelectResponseBean convertRelUnitPjtSelectResponseBean(
			FlowRelUnitEntryServiceBean bean ) {

		RelUnitPjtSelectResponseBean pjtResbean = new RelUnitPjtSelectResponseBean();

		pjtResbean.setSelectedLotNo		( bean.getSelectedLotNo() );
		pjtResbean.setSelectedServerNo	( bean.getLockServerId() );
		pjtResbean.setPjtList			( bean.getPjtSelectBean().getPjtViewBeanList() );
		pjtResbean.setClosedPjtList		( bean.getPjtSelectBean().getClosedPjtViewBeanList() );

		// 変更管理番号の重複を許したいのでListを使う
		List<String> pjtNo = new ArrayList<String>();
		if ( bean.getSelectedPjtNo() != null ) {
			pjtNo.addAll( FluentList.from(bean.getSelectedPjtNo()).asList());
		}
		pjtResbean.setSelectedPjtNoString( (String[])pjtNo.toArray( new String[0] ));

		return pjtResbean;
	}

	public static RelUnitCompRequestResponseBean convertRelUnitCompRequestResponseBean(
			FlowRelUnitEntryServiceBean bean ) {

		RelUnitCompRequestResponseBean resBean = new RelUnitCompRequestResponseBean();

		resBean.setBuildNo			( bean.getBuildNo() );
		resBean.setAllAssetCompile	( bean.getAllAssetCompile() );

		return resBean;
	}

	public static RelUnitGenerateDetailViewResponseBean convertRelUnitCompileDetailViewResponseBean(
			FlowRelUnitCompileDetailViewServiceBean bean ) {

		RelUnitGenerateDetailViewResponseBean resBean = new RelUnitGenerateDetailViewResponseBean();

		resBean.setBuildStatusMsg	( bean.getGenerateDetailViewBean().getBuildStatusMsg() );
		resBean.setExecuteUser		( bean.getGenerateDetailViewBean().getExecuteUser() );
		resBean.setExecuteUserId	( bean.getGenerateDetailViewBean().getExecuteUserId() );
		resBean.setBuildNo			( bean.getGenerateDetailViewBean().getBuildNo() );

		List<List<String>> imgPathList = new ArrayList<List<String>>();
		List<List<String>> messageList = new ArrayList<List<String>>();
		List<String> timelineList = new ArrayList<String>() ;

		List<BuildProcessViewBean> procBeanList = bean.getGenerateDetailViewBean().getBuildProcessViewBeanList();
		for ( BuildProcessViewBean procBean : procBeanList ) {
			imgPathList.add(getImgPathList(procBean.getImg()));
			messageList.add(FluentList.from(procBean.getMsg()).asList());

			String timelineStatus = "" ;
			for( String active : procBean.getActive() ) {//各ステータスをタイムラインとして集約
				if( null != active ) {
					if( "ERROR".equals( active ) ) {//ERRORは即エラー判定
						timelineStatus = active ;
						break ;
					}
					//ACTIVEとCOMPLETEでは、ACTIVEが勝つ
					timelineStatus = ( true == "ACTIVE".equals( timelineStatus ) ) ? timelineStatus : active ;
				}
			}
			timelineList.add( timelineStatus ) ;
		}
		resBean.setImagePathList	( imgPathList );
		resBean.setImageMsgList		( messageList );
		resBean.setTimelineList		( timelineList ) ;
		resBean.setReloadInterval	( sheet.getValue( BmDesignEntryKeyByBuild.reloadInterval ));

		resBean.setSelectedServerNo	( bean.getLockServerId() );
		resBean.setSelectedLotNo	( bean.getSelectedLotNo() );
		resBean.setLotList			( bean.getLotList() );

		return resBean;
	}

	/**
	 * アイコンIDからUm-Design-Sheetに定義されているアイコンのファイル名を取得します。
	 * @param ImgArray
	 * @return
	 */
	private static List<String> getImgPathList(String[] ImgArray) {
		List<String> imgList = new ArrayList<String>();
		for (String imgPath : ImgArray ) {
			imgList.add( sheet.getValue(RmDesignBeanId.resources,imgPath) );
		}
		return imgList;
	}

	public static RelUnitPjtDetailViewResponseBean convertRelUnitPjtDetailViewResponseBean(
			FlowRelUnitPjtDetailViewServiceBean bean ) {

		RelUnitPjtDetailViewResponseBean pjtBean = new RelUnitPjtDetailViewResponseBean();

		pjtBean.setSelectedPjtNo		( bean.getSelectedPjtNo() );
		pjtBean.setApplyInfoViewBeanList( bean.getApplyInfoViewBeanList() );

		return pjtBean;
	}

}
