package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitPjtSelectResponseBean extends BaseResponseBean {

	

	/** ページ番号数 */
	private Integer pageNoNum;
	/** 選択ページ番号 */
	private Integer selectPageNo;
	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** 選択されたサーバ番号 */
	private String selectedServerNo = null;
	/** クローズ済み案件一覧用リスト */
	private List<PjtViewBean> closedPjtList;
	/** 案件一覧用リスト */
	private List<PjtViewBean> pjtList;
	/** 選択された変更管理番号 */
	private String selectedPjtNoString = null;


	public List<PjtViewBean> getClosedPjtList() {
		return closedPjtList;
	}
	public void setClosedPjtList( List<PjtViewBean> closedPjtList ) {
		this.closedPjtList = closedPjtList;
	}

	public List<PjtViewBean> getPjtList() {
		return pjtList;
	}
	public void setPjtList( List<PjtViewBean> pjtList ) {
		this.pjtList = pjtList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public String getSelectedPjtNoString() {
		return selectedPjtNoString;
	}

	public void setSelectedPjtNoString(String[] selectedPjtNo) {
		int i = 0;
		StringBuilder buf = new StringBuilder();
		buf.append("[");
		for(String pjtNo :selectedPjtNo) {
			if (i > 0) {
				buf.append(",");
			}
			buf.append(pjtNo);
			i++;
		}
		buf.append("]");
		this.selectedPjtNoString = buf.toString();
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}

	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

}
