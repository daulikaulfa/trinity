package jp.co.blueship.tri.bm.ui.bp.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitCompRequestResponseBean extends BaseResponseBean {

	

	/** ビルドパッケージ番号 */
	private String buildNo = null;
	/** 全資産ビルドモード */
	private String allAssetCompile = null;


	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo( String buildNo ) {
		this.buildNo = buildNo;
	}

	public String getAllAssetCompile() {
		return allAssetCompile;
	}
	public void setAllAssetCompile( String allAssetCompile ) {
		this.allAssetCompile = allAssetCompile;
	}
}
