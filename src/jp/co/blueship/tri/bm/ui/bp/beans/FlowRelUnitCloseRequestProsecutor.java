package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitCloseRequestResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitCloseRequestProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelUnitCloseRequestService";

	public static final String[] screenFlows = new String[] {
														RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM,
														RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST };

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelUnitScreenID.UNIT_CLOSE_LIST,
														 };

	public FlowRelUnitCloseRequestProsecutor() {
		super( null );
	}

	public FlowRelUnitCloseRequestProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelUnitCloseRequestServiceBean bean = new FlowRelUnitCloseRequestServiceBean();
		bean.setUnitInfoInputBean( new UnitInfoInputBean() );

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String lotId = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		bean.setSelectedLotNo	( lotId );
		bean.setLockLotNo		( lotId );

		String serverNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId	( serverNo );

		if ( forward.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM )) {
			bean.setSelectedUnitNo	( reqInfo.getParameterValues( "unitNo" ));
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		if ( referer.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM )) {

			UnitInfoInputBean inputBean = new UnitInfoInputBean();
			inputBean.setCloseComment	( reqInfo.getParameter( "closeComment" ));
			inputBean.setBaseLineTag	( reqInfo.getParameter( "baseLineTag" ));
			bean.setUnitInfoInputBean	( inputBean );

			bean.setSelectedUnitNo		( TriStringUtils.convertStringToArray( reqInfo.getParameter( "unitNo" )));

			//パッケージクローズエラーチェック
			WarningCheckBean warningCheckUnitCloseError = bean.getWarningCheckUnitCloseError() ;
			String confirmCheckUnitCloseError = reqInfo.getParameter( "confirmCheckUnitCloseError" ) ;
			warningCheckUnitCloseError.setConfirmCheck( ( null != confirmCheckUnitCloseError ) ? true : false );

			warningCheckUnitCloseError.setExistWarning		(
					( new Boolean( reqInfo.getParameter( "existWarningUnitCloseError" ) )).booleanValue() );

			//２重貸出チェック
			WarningCheckBean warningCheckAssetDuplicate = bean.getWarningCheckAssetDuplicate() ;
			String confirmCheckAssetDuplicate = reqInfo.getParameter( "confirmCheckAssetDuplicate" ) ;
			warningCheckAssetDuplicate.setConfirmCheck( ( null != confirmCheckAssetDuplicate) ? true : false );

			warningCheckAssetDuplicate.setExistWarning		(
					( new Boolean( reqInfo.getParameter( "existWarningAssetDuplicate" ) )).booleanValue() );

		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		RelUnitCloseRequestResponseBean resBean = new RelUnitCloseRequestResponseBean();
		FlowRelUnitCloseRequestServiceBean bean = (FlowRelUnitCloseRequestServiceBean)getServiceReturnInformation();

		resBean.setSelectedLotNo		( bean.getSelectedLotNo() );
		resBean.setLotName				( bean.getLotName() );
		resBean.setSelectedUnitNoString	( TriStringUtils.convertArrayToString( bean.getSelectedUnitNo() ));
		if ( null != bean.getUnitInfoInputBean() ) {
			resBean.setUnitInfoInputBean( bean.getUnitInfoInputBean() );

			resBean.setBaseLineTag			( bean.getUnitInfoInputBean().getBaseLineTag() );
			resBean.setVersionTag			( bean.getNumberingVal() );
		}
		resBean.setUnitCloseConfirmViewBeanList( bean.getUnitCloseConfirmViewBeanList() );

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		//パッケージクローズエラー有無チェック
		resBean.setWarningCheckUnitCloseError( bean.getWarningCheckUnitCloseError() );
		//２重貸出チェック
		resBean.setWarningCheckAssetDuplicate( bean.getWarningCheckAssetDuplicate() );


		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}
}
