package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseListServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvActionToServiceUtils;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitCloseListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitCloseListProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelUnitCloseListService";
	public static final String SEARCH_CLOSE_LIST = FLOW_ACTION_ID + "closeList";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelUnitScreenID.UNIT_CLOSE_TOP,
														RelUnitScreenID.UNIT_CLOSE_LIST,
														RelUnitScreenID.UNIT_CLOSE_HISTORY_LIST, };

	public FlowRelUnitCloseListProsecutor() {
		super( null );
	}

	public FlowRelUnitCloseListProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		FlowRelUnitCloseListServiceBean bean = new FlowRelUnitCloseListServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( selectedServerNo );


		//詳細検索の検索条件を同一ロット内で保持する
		{
			RelUnitSearchBean relUnitSearchBean = (RelUnitSearchBean)session.getAttribute( SEARCH_CLOSE_LIST );

			if ( null == relUnitSearchBean || ! bean.getSelectedLotNo().equals( relUnitSearchBean.getSessionKey() ) ) {
				session.removeAttribute( SEARCH_CLOSE_LIST );
				bean.setRelUnitSearchBean( null );
			} else {
				bean.setRelUnitSearchBean( relUnitSearchBean );
			}

			//[検索]ボタン押下時、検索情報をセッションに保持する
			if ( RelUnitScreenID.UNIT_CLOSE_LIST .equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setRelUnitSearchBean( BmCnvActionToServiceUtils.convertRelUnitSearchBean( reqInfo, bean.getRelUnitSearchBean() ) );
					bean.getRelUnitSearchBean().setSessionKey( bean.getSelectedLotNo() );

					session.setAttribute( SEARCH_CLOSE_LIST, bean.getRelUnitSearchBean() );
				}
			}
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		RelUnitCloseListResponseBean resBean = new RelUnitCloseListResponseBean();
		FlowRelUnitCloseListServiceBean bean = (FlowRelUnitCloseListServiceBean)getServiceReturnInformation();

		resBean.setUnitViewBeanList	( bean.getUnitViewBeanList() );
		resBean.setSelectedLotNo	( bean.getSelectedLotNo() );
		resBean.setLotName			( bean.getLotName() );
		resBean.setRelUnitSearchBean( bean.getRelUnitSearchBean() );

		resBean.setSelectedUnitNoString
									( TriStringUtils.convertArrayToString( reqInfo.getParameterValues( "unitNo" ) ));

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}
}
