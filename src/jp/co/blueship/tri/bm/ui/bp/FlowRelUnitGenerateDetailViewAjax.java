package jp.co.blueship.tri.bm.ui.bp;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCompileDetailViewServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.GenericService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.ReverseAjaxService;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.SecondaryPresentationController;

public class FlowRelUnitGenerateDetailViewAjax extends SecondaryPresentationController {
//	private static final Log log = LogFactory.getLog("jp.co.blueship.trinity");

	public static final String FLOW_ACTION_ID = "FlowRelUnitCompileDetailViewService";

	@Override
	protected GenericService getApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );
		return ((ReverseAjaxService)service).getApplicationService(FLOW_ACTION_ID, serviceDto);
	}

	@Override
	protected GenericServiceBean getApplicationServiceBean(IGeneralServiceBean info) throws Exception {
		FlowRelUnitCompileDetailViewServiceBean bean = (FlowRelUnitCompileDetailViewServiceBean)info;
		return bean;
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service, IGeneralServiceBean info) throws Exception {
		return null;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		ISessionInfo session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelUnitCompileDetailViewServiceBean bean = new FlowRelUnitCompileDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		// 画面遷移しないためパラメータが存在しないので、ここでセット
		String referer = reqInfo.getParameter("referer");
		String forward = reqInfo.getParameter("forward");

		if ( referer == null ) referer = RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW;
		if ( forward == null ) forward = RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW;

		bean.setReferer(referer);
		bean.setForward(forward);

		bean.setBuildNo( (String)reqInfo.getAttribute("buildNo") );

		GenerateDetailViewBean viewBean = new GenerateDetailViewBean();
		viewBean.setBuildNo(bean.getBuildNo());
		bean.setGenerateDetailViewBean(viewBean);

		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.RP_GENERATED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		bean.setLockServerId( selectedServerNo );
		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID, selectedServerNo );
		}


		return bean;
	}

	@Override
	protected String getForward(IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, IGeneralServiceBean info) {
		return null;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		BaseResponseBean resBean = null;
		FlowRelUnitCompileDetailViewServiceBean bean = (FlowRelUnitCompileDetailViewServiceBean)getServiceReturnInformation();

		resBean = BmCnvServiceToActionUtils.convertRelUnitCompileDetailViewResponseBean(bean);

		if (resBean != null) {
			resBean.new MessageUtility().reflectMessage(bean);
		}

		return resBean;
	}

	@Override
	protected IService getService() {
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalServiceDWR");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {
	}

	@Override
	protected String getCallbackJSFuncName() {

		return "analyseCompileResult";
	}

	public IBaseResponseBean start(HttpServletRequest request, HttpServletResponse response, String buildNoParam) throws Exception{
		request.setAttribute("buildNo", buildNoParam);
		IBaseResponseBean baseBean = super.execute(request, response);
		return baseBean;
	}

}
