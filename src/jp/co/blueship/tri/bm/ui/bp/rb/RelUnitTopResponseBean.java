package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitTopMenuServiceBean.RelUnitTopViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitTopResponseBean extends BaseResponseBean {

	

	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** ビルドパッケージトップ画面用 */
	private List<RelUnitTopViewBean> relUnitTopViewBeanList = null;


	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public List<RelUnitTopViewBean> getRelUnitTopViewBeanList() {
		return relUnitTopViewBeanList;
	}
	public void setRelUnitTopViewBeanList(
				List<RelUnitTopViewBean> relUnitTopViewBeanList ) {
		this.relUnitTopViewBeanList = relUnitTopViewBeanList;
	}
}
