package jp.co.blueship.tri.bm.ui.bp;

import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitTopMenuProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.sm.ui.portal.beans.FlowLoginBtnProsecutor;
import jp.co.blueship.tri.fw.ui.PresentationController;


public class FlowRelUnitTopMenu extends PresentationController {


	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowRelUnitTopMenuProsecutor());
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowLoginBtnProsecutor(bbe));
	}

	@Override
	protected String getForward(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo,
			ISessionInfo sesInfo,
			IApplicationInfo appInfo ) {

		return "RelUnitTop";
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "SystemError";
	}

}
