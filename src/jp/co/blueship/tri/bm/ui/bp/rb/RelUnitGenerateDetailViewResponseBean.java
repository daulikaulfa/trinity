package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

public class RelUnitGenerateDetailViewResponseBean extends BaseResponseBean {

	/** ログインユーザ名 */
	private String loginUserName = null ;
	/** ログインユーザＩＤ */
	private String loginUserId = null ;
	/** エラーメッセージ */
	private String errMessage;
	/** ビルドパッケージ番号 */
	private String buildNo;
	/** ロット番号 */
	private String lotId;
	/** 実行者 */
	private String executeUser;
	/** 実行者ＩＤ */
	private String executeUserId;
	/** リロードインターバル */
	private String reloadInterval = "";
	/** イメージパス */
	private List<List<String>> imagePathList;
	/** メッセージ */
	private List<List<String>> imageMsgList;
	/** タイムラインリスト */
	private List<String> timelineList;
	/** ビルドパッケージ状況メッセージ */
	public String buildStatusMsg = null;

	/** サーバ番号 */
	private String selectedServerNo = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット番号（コンボ用）*/
	private List<ItemLabelsBean> lotList = null ;



	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}
	public String getErrMessage() {
		return errMessage;
	}
	public void setErrMessage(String errMessage) {
		this.errMessage = errMessage;
	}
	public String getExecuteUser() {
		return executeUser;
	}
	public void setExecuteUser(String executeUser) {
		this.executeUser = executeUser;
	}
	public String getLoginUserName() {
		return loginUserName;
	}
	public void setLoginUserName(String loginUserName) {
		this.loginUserName = loginUserName;
	}
	public List<List<String>> getImagePathList() {
		return imagePathList;
	}
	public void setImagePathList(List<List<String>> imagePathList) {
		this.imagePathList = imagePathList;
	}
	public List<List<String>> getImageMsgList() {
		return imageMsgList;
	}
	public void setImageMsgList(List<List<String>> imageMsgList) {
		this.imageMsgList = imageMsgList;
	}
	public List<String> getTimelineList() {
		return timelineList;
	}
	public void setTimelineList(List<String> timelineList) {
		this.timelineList = timelineList;
	}
	public String getReloadInterval() {
		return reloadInterval;
	}
	public void setReloadInterval(String reloadInterval) {
		this.reloadInterval = reloadInterval;
	}
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}
	public String getBuildStatusMsg() {
		return buildStatusMsg;
	}
	public void setBuildStatusMsg(String buildStatusMsg) {
		this.buildStatusMsg = buildStatusMsg;
	}
	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}
	public String getLoginUserId() {
		return loginUserId;
	}
	public void setLoginUserId(String loginUserId) {
		this.loginUserId = loginUserId;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public List<ItemLabelsBean> getLotList() {
		return lotList;
	}
	public void setLotList( List<ItemLabelsBean> lotList ) {
		this.lotList = lotList;
	}

}
