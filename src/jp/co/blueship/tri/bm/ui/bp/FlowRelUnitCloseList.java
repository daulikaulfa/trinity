package jp.co.blueship.tri.bm.ui.bp;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitCloseHistoryListProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitCloseListProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitCloseTopProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitTopMenuProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;


public class FlowRelUnitCloseList extends PresentationController {

	
	
	protected void addPresentationProsecutores( PresentationProsecutorManager ppm ) {
		ppm.addPresentationProsecutor( new FlowRelUnitCloseListProsecutor() );
	}

	protected void addBusinessErrorPresentationProsecutores(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe ) {
		
		String referer = reqInfo.getParameter( "referer" );
		if ( null != referer ) {
			if ( referer.equals( RelUnitScreenID.UNIT_CLOSE_TOP )) {
				ppm.addBusinessErrorPresentationProsecutor( new FlowRelUnitCloseTopProsecutor( bbe ));
				return;

			}
			if ( referer.equals( RelUnitScreenID.UNIT_CLOSE_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor( new FlowRelUnitCloseListProsecutor( bbe ));
				return;

			}
			if ( referer.equals( RelUnitScreenID.UNIT_CLOSE_HISTORY_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor( new FlowRelUnitCloseHistoryListProsecutor( bbe ));
				return;

			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor( new FlowRelUnitTopMenuProsecutor( bbe ));
	}
	
	@Override
	protected String getForward(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo ) {
		
		return reqInfo.getParameter( "forward" );
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm, IRequestInfo reqInfo,
			ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe ) {
		
		String referer = reqInfo.getParameter("referer");

		if ( null != referer ) {
			for ( String screenFlow : FlowRelUnitCloseListProsecutor.screenViewFlows ) {
				if ( screenFlow.equals( referer )) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "RelUnitTop";
	}
}
