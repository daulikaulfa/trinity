package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCompileDetailViewServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvServiceToActionUtils;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitGenerateDetailViewProsecutor extends PresentationProsecutor {
	private static final ILog log = TriLogFactory.getInstance();

	public static final String FLOW_ACTION_ID = "FlowRelUnitCompileDetailViewService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW };

	public FlowRelUnitGenerateDetailViewProsecutor() {
		super(null);
	}

	public FlowRelUnitGenerateDetailViewProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelUnitCompileDetailViewServiceBean bean = new FlowRelUnitCompileDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer = session.getReferer( this );
		String forward = session.getForward( this );
		String screenType = session.getScreenType( this );

		bean.setReferer(referer);
		bean.setForward(forward);
		bean.setScreenType(screenType);

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "★★★:referer:=" + referer );
			LogHandler.debug( log , "★★★:forward:=" + forward );
		}

		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.RP_GENERATED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}

		bean.setSelectedLotNo	( selectedLotNo );
		bean.setLockLotNo		( selectedLotNo );

		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID );
		bean.setLockServerId	( selectedServerNo );


		if(referer.equals(RelUnitScreenID.TOP)){

		} else if(referer.equals(RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW)){

			GenerateDetailViewBean viewBean = new GenerateDetailViewBean();
			viewBean.setBuildNo(reqInfo.getParameter("buildNo"));
			bean.setGenerateDetailViewBean(viewBean);
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		BaseResponseBean resBean = null;
		FlowRelUnitCompileDetailViewServiceBean bean = (FlowRelUnitCompileDetailViewServiceBean)getServiceReturnInformation();

		resBean = BmCnvServiceToActionUtils.convertRelUnitCompileDetailViewResponseBean(bean);

		if (resBean != null) {
			resBean.new MessageUtility().reflectMessage(bean);
			resBean.new MessageUtility().reflectMessage(getBussinessException());
		}

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID, selectedServerNo );
		}
	}

}
