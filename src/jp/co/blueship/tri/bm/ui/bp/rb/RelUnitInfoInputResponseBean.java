package jp.co.blueship.tri.bm.ui.bp.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitInfoInputResponseBean extends BaseResponseBean {

	

	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
	/** 全資産ビルドモード */
	private String allAssetCompile = null;


	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}

	public String getAllAssetCompile() {
		return allAssetCompile;
	}
	public void setAllAssetCompile( String allAssetCompile ) {
		this.allAssetCompile = allAssetCompile;
	}
}
