package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitCancelResponseBean extends BaseResponseBean {

	

	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** 選択されたビルドパッケージ番号 */
	private String selectedUnitNoString = null;
	/** ビルドパッケージ編集入力情報 */
	private UnitInfoInputBean unitInfoInputBean = null;
	/** 申請情報 */
	private List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList = null;
	/** クローズ時 パッケージクローズエラー警告 */
	private WarningCheckBean warningCheckUnitCloseError = new WarningCheckBean() ;

	public String getSelectedUnitNoString() {
		return selectedUnitNoString;
	}
	public void setSelectedUnitNoString( String selectedUnitNoString ) {
		this.selectedUnitNoString = selectedUnitNoString;
	}

	public UnitInfoInputBean getUnitInfoInputBean() {
		return unitInfoInputBean;
	}
	public void setUnitInfoInputBean( UnitInfoInputBean unitInfoInputBean ) {
		this.unitInfoInputBean = unitInfoInputBean;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}

	public List<UnitCloseConfirmViewBean> getUnitCloseConfirmViewBeanList() {
		return unitCloseConfirmViewBeanList;
	}
	public void setUnitCloseConfirmViewBeanList( List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList ) {
		this.unitCloseConfirmViewBeanList = unitCloseConfirmViewBeanList;
	}
	public WarningCheckBean getWarningCheckUnitCloseError() {
		return warningCheckUnitCloseError;
	}
	public void setWarningCheckUnitCloseError(
			WarningCheckBean warningCheckUnitCloseError) {
		this.warningCheckUnitCloseError = warningCheckUnitCloseError;
	}

}
