package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotHistoryViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitLotHistoryListResponseBean extends BaseResponseBean {

	

	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** ロット情報 */
	private List<LotHistoryViewBean> lotHistoryViewBean = null;


	public List<LotHistoryViewBean> getLotHistoryViewBeanList() {
		return lotHistoryViewBean;
	}
	public void setLotHistoryViewBeanList( List<LotHistoryViewBean> lotHistoryViewBean ) {
		this.lotHistoryViewBean = lotHistoryViewBean;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
}
