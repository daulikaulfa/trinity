package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotViewRelUnitBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitConfirmResponseBean extends BaseResponseBean {

	

	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
	/** ロット情報 */
	private LotViewRelUnitBean lotViewBean = null;
	/** クローズ済み変更管理情報 */
	private List<PjtViewBean> closedPjtViewBeanList = null;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 全資産ビルドモード */
	private String allAssetCompile = null;

	/** クローズ時 パッケージクローズエラー警告 */
	private WarningCheckBean warningCheckUnitCloseError = new WarningCheckBean() ;


	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}

	public LotViewRelUnitBean getLotViewBean() {
		return lotViewBean;
	}
	public void setLotViewBean( LotViewRelUnitBean lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}

	public List<PjtViewBean> getClosedPjtViewBeanList() {
		return closedPjtViewBeanList;
	}
	public void setClosedPjtViewBeanList( List<PjtViewBean> closedPjtViewBeanList ) {
		this.closedPjtViewBeanList = closedPjtViewBeanList;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public String getAllAssetCompile() {
		return allAssetCompile;
	}
	public void setAllAssetCompile( String allAssetCompile ) {
		this.allAssetCompile = allAssetCompile;
	}
	public WarningCheckBean getWarningCheckUnitCloseError() {
		return warningCheckUnitCloseError;
	}
	public void setWarningCheckUnitCloseError(
			WarningCheckBean warningCheckUnitCloseError) {
		this.warningCheckUnitCloseError = warningCheckUnitCloseError;
	}
}
