package jp.co.blueship.tri.bm.ui.bp;

import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitReportProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;

public class FlowRelUnitReport extends PresentationController {

	protected void addPresentationProsecutores( PresentationProsecutorManager ppm ) {
		ppm.addPresentationProsecutor( new FlowRelUnitReportProsecutor() );
	}
	
	protected void addBusinessErrorPresentationProsecutores(
			PresentationProsecutorManager ppm, BaseBusinessException bbe ) {
		
		ppm.addBusinessErrorPresentationProsecutor( new FlowRelUnitReportProsecutor( bbe ));
	}
	
	@Override
	protected String getForward(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo ) {
		
		return "register";
	}

	@Override
	protected String getForwardForBusinessException(
			PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe ) {
		
		return "registError";
	}

}
