package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitComfirmBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvActionToServiceUtils;
import jp.co.blueship.tri.bm.ui.BmCnvServiceToActionUtils;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitConfirmResponseBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitInfoInputResponseBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitEntryProsecutor extends PresentationProsecutor {

	private static final ILog log = TriLogFactory.getInstance();

	public static final String FLOW_ACTION_ID = "FlowRelUnitEntryService";

	public static final String[] screenFlows = new String[] {
														RelUnitScreenID.UNIT_INFO_INPUT,
														RelUnitScreenID.LOT_SELECT,
														RelUnitScreenID.PJT_SELECT,
														RelUnitScreenID.UNIT_COMFIRM,
														RelUnitScreenID.COMP_REQUEST };

	public FlowRelUnitEntryProsecutor() {
		super( null );
	}

	public FlowRelUnitEntryProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelUnitEntryServiceBean bean	= null;
		FlowRelUnitEntryServiceBean seBean	= (FlowRelUnitEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );
		if ( seBean != null ) {
			bean = seBean;
			//ワーニングメッセージの初期化
			bean.setInfoMessage( null );
		} else {
			bean = new FlowRelUnitEntryServiceBean();
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer		( referer );
		bean.setForward		( forward );
		bean.setScreenType	( screenType );

		if ( log.isDebugEnabled() ) {
			LogHandler.debug( log , "★★★:referer:=" + referer );
			LogHandler.debug( log , "★★★:forward:=" + forward );
			LogHandler.debug( log , "★★★:screenType:=" + screenType );
			LogHandler.debug( log , "★★★:selectedLotNo:=" + reqInfo.getParameter( "selectedLotNo" ) );
		}

		if ( referer.equals( RelUnitScreenID.TOP )) {

			bean.setAllAssetCompile			(
					BmCnvActionToServiceUtils.convertRelUnitEntryTopBean( reqInfo ));

		} else if ( referer.equals( RelUnitScreenID.UNIT_INFO_INPUT )) {

			UnitInfoInputBean inputBean =
					BmCnvActionToServiceUtils.convertRelUnitEntryUnitInfoInputBean( reqInfo );
			UnitComfirmBean unitComfirmBean = new UnitComfirmBean();
			unitComfirmBean.setUnitName		( inputBean.getUnitName() );
			unitComfirmBean.setUnitSummary	( inputBean.getUnitSummary() );

			UnitInfoBean infoBean = new UnitInfoBean();
			infoBean.setUnitInfoInputBean	( inputBean );

			bean.setUnitInfoBean			( infoBean );
			bean.setUnitComfirmBean			( unitComfirmBean );

		} else if ( referer.equals( RelUnitScreenID.LOT_SELECT )) {

			String[] lotNoServerNo = BmCnvActionToServiceUtils.convertRelUnitEntryLotSelectBean( reqInfo );

			if ( null != lotNoServerNo ) {
				bean.setSelectedLotNo	( lotNoServerNo[0] );
				bean.setLockLotNo		( lotNoServerNo[0] );
				bean.setSelectedServerNo( lotNoServerNo[1] );
				bean.setLockServerId	( lotNoServerNo[1] );
			}

		} else if ( referer.equals( RelUnitScreenID.PJT_SELECT )) {

			bean.setSelectedPjtNo			(
					BmCnvActionToServiceUtils.convertRelUnitEntryPjtSelectBean( reqInfo ));
			bean.setSelectedClosedPjtNo		(
					BmCnvActionToServiceUtils.convertRelUnitEntryClosedPjtSelectBean( reqInfo ));

		} else if ( referer.equals( RelUnitScreenID.UNIT_COMFIRM )) {

			//パッケージクローズエラーチェック
			WarningCheckBean warningCheckUnitCloseError = bean.getWarningCheckUnitCloseError() ;
			String confirmCheckUnitCloseError = reqInfo.getParameter( "confirmCheckUnitCloseError" ) ;
			warningCheckUnitCloseError.setConfirmCheck( ( null != confirmCheckUnitCloseError ) ? true : false );

			warningCheckUnitCloseError.setExistWarning		(
					( new Boolean( reqInfo.getParameter( "existWarningUnitCloseError" ) )).booleanValue() );

		} else if ( referer.equals( RelUnitScreenID.COMP_REQUEST )) {

		}

		if ( forward.equals( RelUnitScreenID.UNIT_INFO_INPUT ) || forward.equals( RelUnitScreenID.LOT_SELECT ) ){

			bean.setLockLotNo	( null );
			bean.setLockServerId( null );
		}

		session.setAttribute( FLOW_ACTION_ID, bean );



		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		BaseResponseBean resBean			= null;
		FlowRelUnitEntryServiceBean bean	= (FlowRelUnitEntryServiceBean)getServiceReturnInformation();

		String forward = bean.getForward();

		if ( forward.equals( RelUnitScreenID.UNIT_INFO_INPUT )){

			resBean = new RelUnitInfoInputResponseBean();
			FlowRelUnitEntryServiceBean seBean = (FlowRelUnitEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );
			if ( seBean != null ) {
				if ( seBean.getUnitInfoBean() != null ) {
					((RelUnitInfoInputResponseBean)resBean).setUnitName		( seBean.getUnitComfirmBean().getUnitName() );
					((RelUnitInfoInputResponseBean)resBean).setUnitSummary	( seBean.getUnitComfirmBean().getUnitSummary() );
				}
			}

			((RelUnitInfoInputResponseBean)resBean).setAllAssetCompile		( bean.getAllAssetCompile() );

		} else if ( forward.equals( RelUnitScreenID.LOT_SELECT )){

			resBean = BmCnvServiceToActionUtils.convertRelUnitLotSelectResponseBean( bean );

		} else if ( forward.equals( RelUnitScreenID.PJT_SELECT )){

			resBean = BmCnvServiceToActionUtils.convertRelUnitPjtSelectResponseBean( bean );

		} else if ( forward.equals( RelUnitScreenID.UNIT_COMFIRM )){
			FlowRelUnitEntryServiceBean seBean = (FlowRelUnitEntryServiceBean)session.getAttribute( FLOW_ACTION_ID );
			resBean = new RelUnitConfirmResponseBean();
			RelUnitConfirmResponseBean conResBean = (RelUnitConfirmResponseBean)resBean;

			conResBean.setUnitName				( bean.getUnitComfirmBean().getUnitName() );
			conResBean.setUnitSummary			( bean.getUnitComfirmBean().getUnitSummary() );
			if ( seBean != null ) {
				if (seBean.getUnitInfoBean() != null) {
					conResBean.setUnitName		( seBean.getUnitComfirmBean().getUnitName() );
					conResBean.setUnitSummary	( seBean.getUnitComfirmBean().getUnitSummary() );
				}
			}
			conResBean.setLotViewBean			( bean.getUnitComfirmBean().getLotViewBean() );
			conResBean.setPjtViewBeanList		( bean.getUnitComfirmBean().getPjtViewBeanList() );
			conResBean.setClosedPjtViewBeanList	( bean.getUnitComfirmBean().getClosedPjtViewBeanList() );
			conResBean.setAllAssetCompile		( bean.getAllAssetCompile() );


			//パッケージクローズエラー有無チェック
			conResBean.setWarningCheckUnitCloseError( bean.getWarningCheckUnitCloseError() );

		} else if ( forward.equals( RelUnitScreenID.COMP_REQUEST )){

			resBean = BmCnvServiceToActionUtils.convertRelUnitCompRequestResponseBean( bean );

			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_LOT_NO, bean.getSelectedLotNo() );
			session.setAttribute( SessionScopeKeyConsts.RP_GENERATED_SERVER_ID, bean.getLockServerId() );

		}

		if ( resBean != null ) {
			resBean.new MessageUtility().reflectMessage( bean );
			resBean.new MessageUtility().reflectMessage( getBussinessException() );
		}

		// RMI対応
		session.setAttribute( FLOW_ACTION_ID, bean );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String forward = session.getForward( this );

		if ( session.isRemoveAttribute( forward, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		if ( null == this.getBussinessException()
			&& screenFlows[ screenFlows.length - 1 ].equals( forward ) ) {
			session.removeAttribute( FLOW_ACTION_ID );
		}

	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		String referer = session.getReferer( this );

		if ( session.isRemoveAttribute( referer, screenFlows ))
			session.removeAttribute( FLOW_ACTION_ID );

		//遷移元画面が、完了画面の場合は、常に初期化（トピックパス対応）
		if ( screenFlows[ screenFlows.length - 1 ].equals( referer ) )
			session.removeAttribute( FLOW_ACTION_ID );
	}

}
