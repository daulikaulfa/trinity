package jp.co.blueship.tri.bm.ui.bp;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitHistoryDetailViewProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitHistoryListProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitLotHistoryListProsecutor;
import jp.co.blueship.tri.bm.ui.bp.beans.FlowRelUnitTopMenuProsecutor;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;


public class FlowRelUnitHistoryList extends PresentationController {

	
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowRelUnitHistoryListProsecutor());
	}

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, BaseBusinessException bbe) {
		String referer = reqInfo.getParameter("referer");
		
		if ( null != referer ) {
			if (referer.equals( RelUnitScreenID.TOP )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitTopMenuProsecutor(bbe));
				return;
			}
			if (referer.equals( RelUnitScreenID.LOT_HISTORY_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitLotHistoryListProsecutor(bbe));
				return;
			}
			if (referer.equals( RelUnitScreenID.UNIT_HISTORY_LIST )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitHistoryListProsecutor(bbe));
				return;
			}
			if (referer.equals( RelUnitScreenID.UNIT_HISTORY_DETAIL_VIEW )) {
				ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitHistoryDetailViewProsecutor(bbe));
				return;
			}
		}

		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		ppm.addBusinessErrorPresentationProsecutor(new FlowRelUnitTopMenuProsecutor(bbe));
	}
	
	@Override
	protected String getForward(PresentationProsecutorManager ppm,
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return reqInfo.getParameter("forward");
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		String referer = reqInfo.getParameter("referer");

		if ( null != referer ) {
			for (String screenFlow : FlowRelUnitHistoryListProsecutor.screenViewFlows) {
				if ( screenFlow.equals( referer ) ) {
					return reqInfo.getParameter("referer");
				}
			}
		}
		
		//フロー外からの遷移／又は排他制御エラーであれば、以下の画面に遷移する
		return "RelUnitTop";
	}

}
