package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvActionToServiceUtils;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitHistoryDetailViewResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitHistoryDetailViewProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowRelUnitHistoryDetailViewService";
	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelUnitScreenID.TOP,
														RelUnitScreenID.LOT_HISTORY_LIST,
														RelUnitScreenID.UNIT_HISTORY_LIST,
														RelUnitScreenID.UNIT_HISTORY_DETAIL_VIEW };

	public FlowRelUnitHistoryDetailViewProsecutor() {
		super( null );
	}

	public FlowRelUnitHistoryDetailViewProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelUnitHistoryDetailViewServiceBean bean = new FlowRelUnitHistoryDetailViewServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId( userId );
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );
		bean.setSelectedBuildNo	( reqInfo.getParameter( "selectBuildNo" ));


		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}
		bean.setLockLotNo		( selectedLotNo );
		bean.setSelectedLotNo	( selectedLotNo );

		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}
		bean.setLockServerId	( selectedServerNo );

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}


		BmCnvActionToServiceUtils.saveSelectedRelUnitNoForReport( session, reqInfo );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		RelUnitHistoryDetailViewResponseBean resBean = new RelUnitHistoryDetailViewResponseBean();
		FlowRelUnitHistoryDetailViewServiceBean bean = (FlowRelUnitHistoryDetailViewServiceBean)getServiceReturnInformation();

		resBean.setStatus					( bean.getStatus() );
		resBean.setBaseLineTag				( bean.getBaseLineTag() );
		resBean.setVersionTag				( bean.getVersionTag() );
		resBean.setUnitName					( bean.getUnitName() );
		resBean.setUnitSummary				( bean.getUnitSummary() );
		resBean.setSelectedLotNo			( bean.getSelectedLotNo() );
		resBean.setSelectedBuildNo			( bean.getSelectedBuildNo() );
		resBean.setLotName					( bean.getLotName() );
		resBean.setExecuteUser				( bean.getExecuteUser() );
		resBean.setStartDate				( TriDateUtils.convertViewDateFormat( bean.getStartDate() ) );
		resBean.setEndDate					( TriDateUtils.convertViewDateFormat( bean.getEndDate() ) );

		resBean.setLendAssetSumCount		( bean.getLendAssetSumCount() );
		resBean.setChangeAssetSumCount		( bean.getChangeAssetSumCount() );
		resBean.setDeleteAssetSumCount		( bean.getDeleteAssetSumCount() );
		resBean.setDeleteBinaryAssetSumCount( bean.getDeleteBinaryAssetSumCount() );
		resBean.setAddAssetSumCount			( bean.getAddAssetSumCount() );

		resBean.setConcentrate				( bean.isConcentrate() );
		resBean.setBinaryConcentrate		( bean.isBinaryConcentrate() );

		resBean.setPjtViewBeanList			( bean.getPjtViewBeanList() );
		resBean.setUnitResultViewBeanList	( bean.getUnitResultViewBeanList() );

		resBean.new MessageUtility().reflectMessage(bean);
		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
	}

}
