package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.UnitResultViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitHistoryDetailViewResponseBean extends BaseResponseBean {

	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** ビルドパッケージ番号 */
	private String selectedBuildNo = null;
	/** ステータス */
	private String status = null;
	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** ベースラインタグ */
	private String baseLineTag = null;
	/** バージョンタグ */
	private String versionTag = null;
	/** 実行者 */
	private String executeUser = null;
	/** 実行者ＩＤ */
	private String executeUserId = null;
	/** 実行開始日時 */
	private String startDate = null;
	/** 実行終了日時 */
	private String endDate = null;

	/** 業務シーケンス上で資産集計が行われたかどうか */
	private boolean isConcentrate = false;

	/** 業務シーケンス上でバイナリ資産集計が行われたかどうか */
	private boolean isBinaryConcentrate = false;

	/** 貸出資産総数 */
	private int lendAssetSumCount = 0;
	/** 変更資産総数 */
	private int changeAssetSumCount = 0;
	/** 削除資産総数 */
	private int deleteAssetSumCount = 0;
	/** 削除バイナリ資産総数 */
	private int deleteBinaryAssetSumCount = 0;
	/** 新規資産総数 */
	private int addAssetSumCount = 0;

	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 処理結果 */
	private List<UnitResultViewBean> unitResultViewBeanList = null;


	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getExecuteUser() {
		return executeUser;
	}
	public void setExecuteUser(String executeUser) {
		this.executeUser = executeUser;
	}

	public String getStartDate() {
		return startDate;
	}
	public void setStartDate( String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}
	public void setEndDate( String endDate) {
		this.endDate = endDate;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		if ( null == pjtViewBeanList ) {
			pjtViewBeanList = new ArrayList<PjtViewBean>();
		}
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList(
			List<PjtViewBean> pjtViewBeanList) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public List<UnitResultViewBean> getUnitResultViewBeanList() {
		if ( null == unitResultViewBeanList ) {
			unitResultViewBeanList = new ArrayList<UnitResultViewBean>();
		}
		return unitResultViewBeanList;
	}
	public void setUnitResultViewBeanList(
			List<UnitResultViewBean> unitResultViewBeanList) {
		this.unitResultViewBeanList = unitResultViewBeanList;
	}
	public String getBaseLineTag() {
		return baseLineTag;
	}
	public void setBaseLineTag(String baseLineTag) {
		this.baseLineTag = baseLineTag;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}
	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}
	public int getAddAssetSumCount() {
		return addAssetSumCount;
	}
	public void setAddAssetSumCount(int addAssetSumCount) {
		this.addAssetSumCount = addAssetSumCount;
	}
	public int getChangeAssetSumCount() {
		return changeAssetSumCount;
	}
	public void setChangeAssetSumCount(int changeAssetSumCount) {
		this.changeAssetSumCount = changeAssetSumCount;
	}
	public int getDeleteAssetSumCount() {
		return deleteAssetSumCount;
	}
	public void setDeleteAssetSumCount(int deleteAssetSumCount) {
		this.deleteAssetSumCount = deleteAssetSumCount;
	}
	public int getDeleteBinaryAssetSumCount() {
		return deleteBinaryAssetSumCount;
	}
	public void setDeleteBinaryAssetSumCount(int deleteBinaryAssetSumCount) {
		this.deleteBinaryAssetSumCount = deleteBinaryAssetSumCount;
	}
	public boolean isBinaryConcentrate() {
		return isBinaryConcentrate;
	}
	public void setBinaryConcentrate(boolean isBinaryConcentrate) {
		this.isBinaryConcentrate = isBinaryConcentrate;
	}
	public boolean isConcentrate() {
		return isConcentrate;
	}
	public void setConcentrate(boolean isConcentrate) {
		this.isConcentrate = isConcentrate;
	}
	public int getLendAssetSumCount() {
		return lendAssetSumCount;
	}
	public void setLendAssetSumCount(int lendAssetSumCount) {
		this.lendAssetSumCount = lendAssetSumCount;
	}

}
