package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitCancelListResponseBean extends BaseResponseBean {

	

	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** ビルドパッケージ情報 */
	private List<UnitCloseViewBean> unitViewBeanList = null;
	/** 選択されたビルドパッケージ番号 */
	private String selectedUnitNoString = null;

	/** ビルドパッケージ 詳細検索用Bean */
	private RelUnitSearchBean relUnitSearchBean = null ;


	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}

	public List<UnitCloseViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitCloseViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}

	public String getSelectedUnitNoString() {
		return selectedUnitNoString;
	}
	public RelUnitSearchBean getRelUnitSearchBean() {
		return relUnitSearchBean;
	}
	public void setRelUnitSearchBean(RelUnitSearchBean relUnitSearchBean) {
		this.relUnitSearchBean = relUnitSearchBean;
	}
	public void setSelectedUnitNoString( String selectedUnitNoString ) {

		StringBuilder buf = new StringBuilder();

		buf.append( "[" );
		buf.append( selectedUnitNoString );
		buf.append( "]" );
		this.selectedUnitNoString = buf.toString();
	}
}
