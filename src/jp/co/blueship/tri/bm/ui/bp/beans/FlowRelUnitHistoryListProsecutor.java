package jp.co.blueship.tri.bm.ui.bp.beans;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitHistoryViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryListServiceBean;
import jp.co.blueship.tri.bm.ui.BmCnvActionToServiceUtils;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitHistoryListResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class FlowRelUnitHistoryListProsecutor extends PresentationProsecutor {


	public static final String FLOW_ACTION_ID = "FlowRelUnitHistoryListService";

	/**
	 * 参照系の遷移フロー。
	 * <br>業務エラー発生時、PresentationControllerの遷移先制御に影響するため、合わせてメンテナンスすること。
	 */
	public static final String[] screenViewFlows = new String[] {
														RelUnitScreenID.TOP,
														RelUnitScreenID.LOT_HISTORY_LIST,
														RelUnitScreenID.UNIT_HISTORY_LIST,
														RelUnitScreenID.UNIT_HISTORY_DETAIL_VIEW };
	public static final String SEARCH_HISTORY_LIST = FLOW_ACTION_ID + "historyList";

	public FlowRelUnitHistoryListProsecutor() {
		super(null);
	}

	public FlowRelUnitHistoryListProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo,reqInfo );
		session.setAttribute(SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID);

		FlowRelUnitHistoryListServiceBean bean = new FlowRelUnitHistoryListServiceBean();

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		bean.setUserId(userId);
		bean.setUserName(userName());

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );


		String selectedLotNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO );
		String lotId = reqInfo.getParameter( "lotNo" );
		if ( !TriStringUtils.isEmpty( lotId ) ) {
			selectedLotNo = lotId;
		}
		bean.setLockLotNo		( selectedLotNo );
		bean.setSelectedLotNo	( selectedLotNo );
		if ( null != selectedLotNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_LOT_NO, selectedLotNo );
		}


		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		bean.setLockServerId( selectedServerNo );


		/*if ( null != referer ) {
			if ( referer.equals(RelUnitScreenID.UNIT_HISTORY_LIST )) {
				String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
				if( ! StringAddonUtil.isNothing( savedPageNo ) ) {
					bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
				}
			}
		}*/

//		詳細検索の検索条件を同一ロット内で保持する
		{
			RelUnitSearchBean relUnitSearchBean = (RelUnitSearchBean)session.getAttribute( SEARCH_HISTORY_LIST );

			if ( null == relUnitSearchBean || ! bean.getSelectedLotNo().equals( relUnitSearchBean.getSessionKey() ) ) {
				session.removeAttribute( SEARCH_HISTORY_LIST );
				bean.setRelUnitSearchBean( null );
			} else {
				bean.setRelUnitSearchBean( relUnitSearchBean );
			}

			//[検索]ボタン押下時、検索情報をセッションに保持する
			if ( RelUnitScreenID.UNIT_HISTORY_LIST.equals( referer ) ) {

				if ( ScreenType.select.equals( screenType ) ) {
					bean.setRelUnitSearchBean( BmCnvActionToServiceUtils.convertRelUnitSearchBean( reqInfo, bean.getRelUnitSearchBean() ) );
					bean.getRelUnitSearchBean().setSessionKey( bean.getSelectedLotNo() );

					session.setAttribute( SEARCH_HISTORY_LIST, bean.getRelUnitSearchBean() );
				}
			}
		}

		BmCnvActionToServiceUtils.saveSelectedRelUnitNoForReport( session, reqInfo );

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {

		ISessionInfo session = new SessionMultiWindows( sesInfo, reqInfo );

		RelUnitHistoryListResponseBean resBean = new RelUnitHistoryListResponseBean();
		FlowRelUnitHistoryListServiceBean bean = (FlowRelUnitHistoryListServiceBean)getServiceReturnInformation();

		resBean.setUnitHistoryViewBeanList	( bean.getUnitHistoryViewBeanList() );
		//resBean.setPageNoNum				( bean.getPageInfoView().getMaxPageNo() );
		//resBean.setSelectPageNo				( bean.getPageInfoView().getSelectPageNo() );
		resBean.setSelectedLotNo			( bean.getSelectedLotNo() );
		resBean.setLotName					( bean.getLotName() );
		resBean.setRelUnitSearchBean( bean.getRelUnitSearchBean() );

		session.setAttribute(
				SessionScopeKeyConsts.VIEW_REPORT_NO, getRelUnitNoSet( bean.getUnitHistoryViewBeanList() ));

		// 選択済みのビルドパッケージ番号
		@SuppressWarnings("unchecked")
		Set<String> selectedRelUnitNoSet	=
				(Set<String>)session.getAttribute( SessionScopeKeyConsts.SELECTED_REPORT_NO );

		if ( null != selectedRelUnitNoSet ) {
			resBean.setSelectedRelUnitNoString( (String[])selectedRelUnitNoSet.toArray( new String[0] ));
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	/**
	 * ビルドパッケージ番号のセットを取得する
	 * @param unitHistoryViewBeanList UnitHistoryViewBeanのリスト
	 * @return ビルドパッケージ番号のセット
	 */
	private Set<String> getRelUnitNoSet( List<UnitHistoryViewBean> unitHistoryViewBeanList ) {

		Set<String> relUnitNoSet = new HashSet<String>();
		for ( UnitHistoryViewBean viewBean : unitHistoryViewBeanList ) {
			relUnitNoSet.add( viewBean.getRelUnitNo() );
		}

		return relUnitNoSet;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean( "generalService" );
	}

	@Override
	protected void getSessionInfo(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected void postProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		SessionMultiWindows session = new SessionMultiWindows( sesInfo, reqInfo );

		String selectedServerNo = (String)session.getAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID );
		String serverNo = reqInfo.getParameter( "serverNo" );
		if ( !TriStringUtils.isEmpty( serverNo ) ) {
			selectedServerNo = serverNo;
		}

		if ( null != selectedServerNo ) {
			session.setAttribute( SessionScopeKeyConsts.SELECTED_SERVER_ID, selectedServerNo );
		}
	}

}
