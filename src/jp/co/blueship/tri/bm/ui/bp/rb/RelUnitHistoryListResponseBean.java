package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitHistoryViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class RelUnitHistoryListResponseBean extends BaseResponseBean {

	

	/** ページ番号数 */
	private Integer pageNoNum = null;
	/** 選択ページ番号 */
	private Integer selectPageNo = null;
	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** ビルドパッケージ情報 */
	private List<UnitHistoryViewBean> unitHistoryViewBeanList = null;
	/** 選択変ビルドパッケージ番号(レポート用) */
	private String selectedRelUnitNoString = null;
	/** ビルドパッケージ 詳細検索用Bean */
	private RelUnitSearchBean relUnitSearchBean = null ;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}

	public List<UnitHistoryViewBean> getUnitHistoryViewBeanList() {
		return unitHistoryViewBeanList;
	}
	public void setUnitHistoryViewBeanList( List<UnitHistoryViewBean> unitHistoryViewBeanList ) {
		this.unitHistoryViewBeanList = unitHistoryViewBeanList;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public String getSelectedRelUnitNoString() {
		return selectedRelUnitNoString;
	}

	public RelUnitSearchBean getRelUnitSearchBean() {
		return relUnitSearchBean;
	}
	public void setRelUnitSearchBean(RelUnitSearchBean relUnitSearchBean) {
		this.relUnitSearchBean = relUnitSearchBean;
	}
	public void setSelectedRelUnitNoString( String[] relUnitNoArray ) {

		StringBuilder stb = new StringBuilder();
		stb.append("[");
		stb.append( TriStringUtils.convertArrayToString( relUnitNoArray ) ) ;
		stb.append("]");
		this.selectedRelUnitNoString = stb.toString();
	}

}
