package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotViewRelUnitBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitLotSelectResponseBean extends BaseResponseBean {

	

	/** ロットリスト */
	private List<LotViewRelUnitBean> buildLotList;
	/** ページ番号数 */
	private Integer pageNoNum;
	/** 選択ページ番号 */
	private Integer selectPageNo;
	/** 選択サーバ情報番号 */
	private String selectedServerNo;
	/** 選択ロット情報番号 */
	private String selectedLotNo;
	/** 全資産ビルドモード */
	private String allAssetCompile = null;


	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo( String selectedServerNo ) {
		this.selectedServerNo = selectedServerNo;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public Integer getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = selectPageNo;
	}

	public Integer getPageNoNum() {
		return pageNoNum;
	}
	public void setPageNoNum(Integer pageNoNum) {
		this.pageNoNum = pageNoNum;
	}

	public List<LotViewRelUnitBean> getBuildLotList() {
		return buildLotList;
	}
	public void setBuildLotList(List<LotViewRelUnitBean> buildApplyList) {
		this.buildLotList = buildApplyList;
	}

	public String getAllAssetCompile() {
		return allAssetCompile;
	}
	public void setAllAssetCompile( String allAssetCompile ) {
		this.allAssetCompile = allAssetCompile;
	}
}
