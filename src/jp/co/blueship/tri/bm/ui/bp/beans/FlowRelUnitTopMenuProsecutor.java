package jp.co.blueship.tri.bm.ui.bp.beans;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitTopMenuServiceBean;
import jp.co.blueship.tri.bm.ui.bp.rb.RelUnitTopResponseBean;
import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionMultiWindows;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

public class FlowRelUnitTopMenuProsecutor extends PresentationProsecutor {

	

	public static final String FLOW_ACTION_ID = "FlowRelUnitTopMenuService";

	public FlowRelUnitTopMenuProsecutor() {
		super( null );
	}

	public FlowRelUnitTopMenuProsecutor( BaseBusinessException bbe ) {
		super( bbe );
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {
	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		SessionMultiWindows session = new SessionMultiWindows(sesInfo,reqInfo);
		session.setAttribute	( SessionScopeKeyConsts.FLOW_ACTION_ID, FLOW_ACTION_ID );

		sesInfo.removeAttribute	( SessionScopeKeyConsts.TOP_SCREEN_NAME );

		FlowRelUnitTopMenuServiceBean bean = new FlowRelUnitTopMenuServiceBean();

		String referer		= session.getReferer( this );
		String forward		= session.getForward( this );
		String screenType	= session.getScreenType( this );

		bean.setReferer			( referer );
		bean.setForward			( forward );
		bean.setScreenType		( screenType );

		if ( null != referer ) {
			if ( referer.equals( RelUnitScreenID.TOP )) {
				String savedPageNo = reqInfo.getParameter( "savedPageNo" ) ;
				if( ! TriStringUtils.isEmpty( savedPageNo ) ) {
					bean.setSelectPageNo(Integer.parseInt( savedPageNo ) ) ;
				}
			}
		}

		String userId = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_USER_ID );
		if ( null == referer ) {
			//ログイン用処理
			bean.setUserId( reqInfo.getUserId() );
			bean.setUserName(userName());

			// FlowChangePasswordBtnProsecutorからのキラーパス
			String tempRef = (String)sesInfo.getAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
			bean.setReferer( tempRef );

		} else {
			bean.setUserId( userId );
			bean.setUserName(userName());
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

		sesInfo.setAttribute( SessionScopeKeyConsts.TOP_SCREEN_NAME, RelUnitScreenID.TOP );

		RelUnitTopResponseBean resBean = new RelUnitTopResponseBean();
		FlowRelUnitTopMenuServiceBean bean = (FlowRelUnitTopMenuServiceBean)getServiceReturnInformation();

		resBean.setRelUnitTopViewBeanList	( bean.getRelUnitTopViewBeanList() );

		// パスワード変更画面への遷移の場合はnullで入ってくる
		if ( null != bean.getPageInfoView() ) {
			resBean.setPageNoNum				( bean.getPageInfoView().getMaxPageNo() );
			resBean.setSelectPageNo				( bean.getPageInfoView().getSelectPageNo() );
		}

		resBean.new MessageUtility().reflectMessage( bean );
		resBean.new MessageUtility().reflectMessage( getBussinessException() );

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	protected void getSessionInfo(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) throws Exception {

	}


	@Override
	protected void postProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {

		ISessionInfo session = new SessionMultiWindows( sesInfo,reqInfo );
		session.removeAttribute( FLOW_ACTION_ID );
		session.removeAttribute( SessionScopeKeyConsts.FLWC00_REFERER );
	}

	@Override
	protected void preProcessor(
			IApplicationInfo appInfo,
			ISessionInfo sesInfo,
			IRequestInfo reqInfo ) {
	}

}
