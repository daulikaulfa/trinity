package jp.co.blueship.tri.bm.ui.bp.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitPjtDetailViewServiceBean.ApplyInfoViewBean;
import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class RelUnitPjtDetailViewResponseBean extends BaseResponseBean {

	/** 遷移元画面ID */
	private String referer = null;
	/** 遷移先画面ID */
	private String forward = null;
	/** 遷移タイプ */
	private String screenType = null;
	/** 変更管理番号 */
	private String selectedPjtNo = null;
	/** 申請情報 */
	private List<ApplyInfoViewBean> applyInfoViewBeanList = null;


	public String getForward() {
		return forward;
	}
	public void setForward(String forward) {
		this.forward = forward;
	}

	public String getReferer() {
		return referer;
	}
	public void setReferer(String referer) {
		this.referer = referer;
	}

	public String getScreenType() {
		return screenType;
	}
	public void setScreenType(String screenType) {
		this.screenType = screenType;
	}

	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}

	public List<ApplyInfoViewBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
			List<ApplyInfoViewBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}

}
