package jp.co.blueship.tri.bm;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowResItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResCondition;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity.ITaskResultEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskDefineId;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.utils.AssetCounterInfo;
import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;

/**
 * タスクを扱うユーティリティークラスです。
 * <br>処理名の取得など、各AddOn共通の機能が定義されています。
 * 
 * @version V3L10.02
 * @author Yukihiro Eguchi
 * 
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 */
public class BmTaskAddonUtils {

	/**
	 * ビルドパッケージ作成のプロセス名を取得する
	 * @param entity ビルドパッケージプロセスエンティティ
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @return プロセス名
	 */
	public static String getProcessName( ITaskFlowProcEntity entity, Map<String, ITaskFlowResEntity> taskRecEntityMap ) {

		if ( ! taskRecEntityMap.containsKey(entity.getTaskFlowId()) )
			return null;

		ITaskFlowResEntity recEntity = taskRecEntityMap.get( entity.getTaskFlowId() );

		for ( ITaskTargetEntity targetEntity : recEntity.getTask().getTarget() ) {

			// この条件で１つに絞り込めるはず？ by inoue
			if ( targetEntity.getSequenceNo().equals( entity.getTargetSeqNo() ) ) {
				return targetEntity.getName();
			}
		}

		return null;
	}

	/**
	 * リリース作成のプロセス名を取得する
	 * @param entity ビルドパッケージプロセスエンティティ
	 * @param taskRecEntityMap リリースタスク履歴のマップ
	 * @return プロセス名
	 */
	/*
	public static String getProcessName( IRelProcessEntity entity, Map<String, IRelTaskRecEntity> taskRecEntityMap ) {

		if ( ! taskRecEntityMap.containsKey(entity.getWorkflowNo()) )
			return null;

		IRelTaskRecEntity recEntity = taskRecEntityMap.get( entity.getWorkflowNo() );

		for ( ITaskTargetEntity targetEntity : recEntity.getTaskEntity().getTarget() ) {

			// この条件で１つに絞り込めるはず？ by inoue
			if ( targetEntity.getSequenceNo().equals( entity.getSequenceNo() )) {
				return targetEntity.getName();
			}
		}

		return null;
	}
	*/

	/**
	 * 資産の追加、変更、削除数を取得する。
	 * @param entity ビルドパッケージエンティティ
	 * @param support
	 * @param taskRecEntityList
	 * @return 申請番号と[追加数,変更数,削除数]が格納されたオブジェクトのマップ
	 */
	public static Map<String, IAssetCounterInfo> getAssetCount(
			IBpDto entity, IBmFinderSupport support, List<ITaskFlowResEntity> taskRecEntityList ) {

		Map<String,IAssetCounterInfo> applyNoCounterMap = new TreeMap<String, IAssetCounterInfo>();

		TaskFlowResCondition condition = new TaskFlowResCondition();
		condition.setDataId( entity.getBpEntity().getBpId() );

		SortBuilder sort = new SortBuilder();
		sort.setElement(TaskFlowResItems.taskFlowId, TriSortOrder.Asc, 1);

		IEntityLimit<ITaskFlowResEntity> limit =
			support.getTaskFlowResDao().find( condition.getCondition(), sort, 1, 0 );

		int deleteBinaryAssetSumCount = 0;
		boolean isBinaryConcentrate = false;

		// 集約資産数
		for ( ITaskFlowResEntity recEntity : limit.getEntities() ) {

			if ( null != taskRecEntityList ) {
				taskRecEntityList.add( recEntity );
			}

			for ( ITaskTargetEntity targetEntity : recEntity.getTask().getTarget() ) {

				for ( ITaskResultTypeEntity resultTypeEntity : targetEntity.getTaskResult() ) {

					//静的資産を集計
					if ( resultTypeEntity instanceof TaskResultConcentrateEntity ) {

						TaskResultConcentrateEntity concentrateEntity =
								(TaskResultConcentrateEntity)resultTypeEntity;

						for ( ITaskDiffEntity diffEntity : concentrateEntity.getDiff() ) {

							AreqFileCondition fileCondition = new AreqFileCondition();
							fileCondition.setAreqId( diffEntity.getApplyNo() );
							fileCondition.setAreqCtgCd(AreqCtgCd.RemovalRequest.value());

							Set<String> delFileSet = convertAssetFileEntityArrayToPathSet( support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) ) ;

							IAssetCounterInfo counter = new AssetCounterInfo();

							for ( ITaskResultConcentrateEntity.ITaskResultEntity resultEntity : diffEntity.getResult() ) {
								if ( EqualsContentsSetSameLineDiffer.Status.ADD.name().equals( resultEntity.getCode() )) {

									if ( null == counter.getAddCount() ) {
										counter.setAddCount( 0 );
									}
									counter.setAddCount( counter.getAddCount() + 1 );

								} else if ( EqualsContentsSetSameLineDiffer.Status.CHANGE.name().equals( resultEntity.getCode() )) {

									if ( null == counter.getChangeCount() ) {
										counter.setChangeCount( 0 );
									}
									counter.setChangeCount( counter.getChangeCount() + 1 );

								} else if ( EqualsContentsSetSameLineDiffer.Status.DELETE.name().equals( resultEntity.getCode() )) {

									if( delFileSet.contains( resultEntity.getRelativePath() ) ) {
										if ( null == counter.getDelCount() ) {
											counter.setDelCount( 0 );
										}
										counter.setDelCount( counter.getDelCount() + 1 );
									}
								}
							}

							applyNoCounterMap.put( diffEntity.getApplyNo(), counter );
						}
					}

					//バイナリ資産を集計
					String taskId = (StringUtils.isEmpty( resultTypeEntity.getTaskId() ))? "": resultTypeEntity.getTaskId();

					if ( -1 != taskId.indexOf( TaskDefineId.deleteBinaryAssetCount.value() )
							&& resultTypeEntity instanceof ITaskResultDiffEntity ) {
						ITaskResultDiffEntity taskEntity =
								(ITaskResultDiffEntity)resultTypeEntity;

						for ( ITaskResultEntity result : taskEntity.getResult() ) {
							if ( EqualsContentsSetSameLineDiffer.Status.DELETE.name().equals( result.getCode() ) ) {
								isBinaryConcentrate = true;
								deleteBinaryAssetSumCount++;
							}
						}

					}
				}
			}
		}

		// 貸出資産数
		List<IBpAreqLnkEntity> applyEntities = entity.getBpAreqLnkEntities();
		for ( IBpAreqLnkEntity applyEntity : applyEntities ) {
			AreqFileCondition areqCondition = new AreqFileCondition();
			areqCondition.setAreqId(applyEntity.getAreqId());
			areqCondition.setAreqCtgCd(AreqCtgCd.LendingRequest.value());

			int countFile = support.getAmFinderSupport().getAreqFileDao().count( areqCondition.getCondition() );

			IAssetCounterInfo counter = applyNoCounterMap.get( applyEntity.getAreqId() );
			if ( null == counter ) {
				counter = new AssetCounterInfo();
				applyNoCounterMap.put( applyEntity.getAreqId(), counter );
			}

			counter.setLendCount( countFile );
		}

		if ( isBinaryConcentrate ) {
			IAssetCounterInfo counter = new AssetCounterInfo();
			counter.setDelCount( deleteBinaryAssetSumCount );

			applyNoCounterMap.put( TaskDefineId.deleteBinaryAssetCount.value(), counter );
		}

		return applyNoCounterMap;
	}
	/**
	 *
	 * @param entityArray
	 * @return
	 */
	private static Set<String> convertAssetFileEntityArrayToPathSet( List<IAreqFileEntity> entityArray ) {
		Set<String> pathSet = new LinkedHashSet<String>() ;
		for( IAreqFileEntity entity : entityArray ) {
			pathSet.add( entity.getFilePath() ) ;
		}
		return pathSet ;
	}
}
