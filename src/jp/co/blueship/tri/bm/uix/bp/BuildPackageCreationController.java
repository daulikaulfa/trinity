package jp.co.blueship.tri.bm.uix.bp;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.BuildPackageCreationInputInfo;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.ChangePropertyView;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/build")
public class BuildPackageCreationController extends TriControllerSupport<FlowBuildPackageCreationServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageCreationService;
	}

	@Override
	protected FlowBuildPackageCreationServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageCreationServiceBean bean = new FlowBuildPackageCreationServiceBean();
		return bean;
	}

	@RequestMapping(value = "/create")
	public String insert(FlowBuildPackageCreationServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.insertMapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				model.getRedirectAttributes().addFlashAttribute( "result", bean );
				bean.getParam().setRequestType(RequestType.init);
				view = "redirect:/build/progress";
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.BuildPackageCreation.value());
		model.getModel().addAttribute("selectedMenu", "releaseMenu");
		model.getModel().addAttribute("selectedSubMenu", "buildpackSubmenu");
		model.getModel().addAttribute("result", bean);
		setPrev(model);
		return view;
	}

	private void insertMapping(FlowBuildPackageCreationServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();

		BuildPackageCreationInputInfo info = bean.getParam().getInputInfo();
		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.submitChanges.equals(bean.getParam().getRequestType())) {
			List<String> values = FluentList.from(requestInfo.getParameterValues("selectedPjtIds")).asList();
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			info.setSubject(requestInfo.getParameter("subject"))
				.setSummary(requestInfo.getParameter("summary"))
				.setCtgId(requestInfo.getParameter("ctgId"))
				.setMstoneId(requestInfo.getParameter("mstoneId"))
				.setSelectedPjtIds(values.toArray(new String[0]));

				{ //Change Property table
					for ( ChangePropertyView grp: bean.getChangePropertyViews() ) {
						grp.setSelected( values.contains(grp.getPjtId()) );

				}


			if (TriStringUtils.isNotEmpty(requestInfo.getParameter("build"))) {
	        	if (requestInfo.getParameter("build").equals("buildDiff")) {
	        		bean.getParam().getInputInfo().setBuildDiff(true);
	        	} else if (requestInfo.getParameter("build").equals("buildFull")) {
	        		bean.getParam().getInputInfo().setBuildFull(true);
	        	}
	        }
		}
	}


	}
}
