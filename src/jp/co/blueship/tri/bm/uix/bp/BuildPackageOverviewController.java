package jp.co.blueship.tri.bm.uix.bp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageOverviewServiceBean;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
@Controller
@RequestMapping("/build")
public class BuildPackageOverviewController extends TriControllerSupport<FlowBuildPackageOverviewServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageOverviewService;
	}

	@Override
	protected FlowBuildPackageOverviewServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageOverviewServiceBean bean = new FlowBuildPackageOverviewServiceBean();
		return bean;
	}

	@RequestMapping("/overview")
	public String overView(FlowBuildPackageOverviewServiceBean bean , TriModel model){

		String view = "common/OverviewPopup::buildPackageOverview";
		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute( "result" , bean ); setPrev(model);

		return view;
	}

	private void mapping(FlowBuildPackageOverviewServiceBean bean , TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setSelectedBpId( requestInfo.getParameter("bpId") );
	}
}
