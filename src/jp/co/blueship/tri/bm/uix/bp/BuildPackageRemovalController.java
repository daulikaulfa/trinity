package jp.co.blueship.tri.bm.uix.bp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageRemovalServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageRemovalServiceBean.RequestParam;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/build/remove")
public class BuildPackageRemovalController extends TriControllerSupport<FlowBuildPackageRemovalServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageRemovalService;
	}

	@Override
	protected FlowBuildPackageRemovalServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageRemovalServiceBean bean = new FlowBuildPackageRemovalServiceBean();
		return bean;
	}

	@RequestMapping
	public String removal(FlowBuildPackageRemovalServiceBean bean, TriModel model) {
		String view = "redirect:/build/redirect";
		String ref = model.getRequestInfo().getParameter("referer");
		String fwd = model.getRequestInfo().getParameter("forward");
		
		try {
			bean.getParam().setRequestType( RequestType.submitChanges );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);
			bean.getParam().setRequestType(RequestType.init);
			if (bean.getResult().isCompleted()) {
				if (ref.equals(fwd) && fwd.equals(TriView.BuildPackageList.value())) {
					view = "redirect:/build/refresh";
					
				} else {
					if (fwd.equals(TriView.BuildPackageList.value())) {
						view = "redirect:/build/redirect";
					} else if (fwd.equals(TriView.BuildPackageDetails.value())) {
						view = "redirect:/build/details";
					}
				}
				
			} else {
				if (ref.equals(TriView.BuildPackageList.value())) {
					view = "redirect:/build/refresh";
				} else if (fwd.equals(TriView.BuildPackageDetails.value())) {
					view = "redirect:/build/details";
				}
			}
			
		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getRedirectAttributes().addFlashAttribute( "result", bean );
		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String removalComment(FlowBuildPackageRemovalServiceBean bean, TriModel model) {
		String view = "common/Comment::buildPackageRemovalComment";

		try {
			bean.getParam().setRequestType( RequestType.validate );
			this.mapping(bean, model);
			this.execute(getServiceId(), bean , model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowBuildPackageRemovalServiceBean bean, TriModel model) {
		IRequestInfo requestInfo = model.getRequestInfo();
		RequestParam param = bean.getParam();

		if ( requestInfo.getParameter("bpId") != null ) {
			param.setSelectedBpId(requestInfo.getParameter("bpId"));
		}
	}

}
