package jp.co.blueship.tri.bm.uix.bp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
@Controller
@RequestMapping("/build/list/print")
public class PrintBuildPackageListController extends TriControllerSupport<FlowBuildPackageListServiceBean> {

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmPrintBuildPackageListService;
	}

	@Override
	protected FlowBuildPackageListServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageListServiceBean bean = new FlowBuildPackageListServiceBean();
		return bean;
	}


	@RequestMapping
	public String print(FlowBuildPackageListServiceBean bean, TriModel model){
		String view = TriTemplateView.PrintTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.PrintBuildPackageList.value())
			.addAttribute("result", bean)
		;

		return view;
	}


	private void mapping(FlowBuildPackageListServiceBean bean, TriModel model){
		IRequestInfo requestInfo = model.getRequestInfo();
		ISessionInfo sesInfo = model.getSessionInfo();
		SearchCondition searchCondition = bean.getParam().getSearchCondition();

		bean.getParam().setLinesPerPage( 0 );
		bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));

		searchCondition.setCtgId	( requestInfo.getParameter("condCategory"))
					   .setMstoneId	( requestInfo.getParameter("condMilestone"))
					   .setStsId	( requestInfo.getParameter("condStatus"))
					   .setKeyword	( requestInfo.getParameter("searchKeyword"))
					   ;

	}

}
