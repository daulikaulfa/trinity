package jp.co.blueship.tri.bm.uix.bp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationProcessingStatusServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
@Controller
@RequestMapping("/build")
public class BuildPackageCreationProcessingStatusController extends TriControllerSupport<FlowBuildPackageCreationProcessingStatusServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageCreationProcessingStatusService;
	}

	@Override
	protected FlowBuildPackageCreationProcessingStatusServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageCreationProcessingStatusServiceBean bean = new FlowBuildPackageCreationProcessingStatusServiceBean();
		return bean;
	}

	@RequestMapping(value = "/progress")
	public String refresh( FlowBuildPackageCreationProcessingStatusServiceBean bean, TriModel model ){

		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model.setRedirect(true));

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.BuildPackageCreationProcessingStatus.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu", "buildpackSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}


	private void mapping (FlowBuildPackageCreationProcessingStatusServiceBean bean, TriModel model) {
		ISessionInfo sesInfo = model.getSessionInfo();

		if (RequestType.init.equals(bean.getParam().getRequestType())) {
			bean.getParam().setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if( RequestType.onChange.equals(bean.getParam().getRequestType()) ) {
		}
	}

}
