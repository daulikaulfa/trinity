package jp.co.blueship.tri.bm.uix.bp;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.FlowBuildPackageCloseInputInfo;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.RequestParam;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
@Controller
@RequestMapping("/build/close")
public class BuildPackageCloseController extends TriControllerSupport<FlowBuildPackageCloseServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageCloseService;
	}

	@Override
	protected FlowBuildPackageCloseServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageCloseServiceBean bean = new FlowBuildPackageCloseServiceBean();
		return bean;
	}

	@RequestMapping
	public String close(FlowBuildPackageCloseServiceBean bean, TriModel model) {
		String view = TriTemplateView.MainTemplate.value();
		try {
			this.mapping(bean, model);
			this.execute(this.getServiceId(), bean, model);
			if (bean.getResult().isCompleted()) {
				view = "redirect:/build/refresh";
				model.getRedirectAttributes().addFlashAttribute("result", bean);
			}

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel()
			.addAttribute("view", TriView.BuildPackageClose.value())
			.addAttribute("selectedMenu", "releaseMenu")
			.addAttribute("selectedSubMenu", "buildpackSubmenu")
			.addAttribute("result", bean)
		;

		setPrev(model);
		return view;
	}

	@RequestMapping(value = "/validate")
	public String closeComment(FlowBuildPackageCloseServiceBean bean, TriModel model) {
		String view  = "common/Comment::buildPackageCloseComment";

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
			view = TriView.Messages.value() + "::message";
		}

		model.getModel().addAttribute("result", bean);
		return view;
	}

	private void mapping(FlowBuildPackageCloseServiceBean bean, TriModel model) {
		RequestParam param = bean.getParam();
		IRequestInfo requestInfo = model.getRequestInfo();
		FlowBuildPackageCloseInputInfo inputInfo = param.getInputInfo();

		if (RequestType.init.equals(param.getRequestType())) {
			ISessionInfo sesInfo = model.getSessionInfo();
			param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
		}

		if (RequestType.validate.equals(param.getRequestType())) {
			ISessionInfo sesInfo = model.getSessionInfo();
			param.setSelectedLotId(this.getSessionSelectedLot(sesInfo, bean));
			List<String> values = FluentList.from(requestInfo.getParameterValues("selectedBpIds")).asList();
			inputInfo.setSelectedBpIds(values.toArray(new String[0]));
			inputInfo.setBaseLineTag(requestInfo.getParameter("baseLineTag"));
		}

		if (RequestType.submitChanges.equals(param.getRequestType())) {
			List<String> values = FluentList.from(requestInfo.getParameterValues("selectedBpIds")).asList();

			inputInfo.setSelectedBpIds(values.toArray(new String[0]));
			inputInfo.setBaseLineTag(requestInfo.getParameter("baseLineTag"));
			inputInfo.setComment(requestInfo.getParameter("reason"));
			if ( requestInfo.getParameter("submitAgreement") != null ){
				param.getInputInfo().setSubmitAgreement(Boolean.parseBoolean(requestInfo.getParameter("submitAgreement")));	
			}
		}
	}
}
