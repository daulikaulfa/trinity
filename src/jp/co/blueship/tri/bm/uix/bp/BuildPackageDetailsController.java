package jp.co.blueship.tri.bm.uix.bp;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.ex.ControllerExceptionUtils;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.TriControllerSupport;
import jp.co.blueship.tri.fw.uix.TriModel;
import jp.co.blueship.tri.fw.uix.constants.TriTemplateView;
import jp.co.blueship.tri.fw.uix.constants.TriView;;
/**
 *
 * @version V4.00.00
 * @author Megumu Terasawa
 */


@Controller
@RequestMapping("/build")
public class BuildPackageDetailsController extends TriControllerSupport<FlowBuildPackageDetailsServiceBean>{

	@Override
	public ServiceId getServiceId() {
		return ServiceId.BmBuildPackageDetailsService;
	}

	@Override
	protected FlowBuildPackageDetailsServiceBean getServiceBean(ISessionInfo sesInfo) {
		FlowBuildPackageDetailsServiceBean bean = new FlowBuildPackageDetailsServiceBean();
		return bean;
	}
	@RequestMapping(value = "/details")
	public String buildPackageDetails(
			FlowBuildPackageDetailsServiceBean bean,
			TriModel model) {
		String view = TriTemplateView.MainTemplate.value();

		try {
			this.mapping(bean, model);
			this.execute(getServiceId(), bean, model);

		} catch (Exception e) {
			if ( ControllerExceptionUtils.isRedirectException(e, this, bean, model, view) ) {
				return ControllerExceptionUtils.redirectException(e, this, bean, model, view);
			}
		}

		model.getModel().addAttribute("view", TriView.BuildPackageDetails.value());
		model.getModel().addAttribute("selectedMenu", "releaseMenu");
		model.getModel().addAttribute("selectedSubMenu"  , "buildpackSubmenu");
		model.getModel().addAttribute( "result" , bean );
		setPrev(model);
		return view;
	}


	private void mapping(FlowBuildPackageDetailsServiceBean bean, TriModel model) {

		IRequestInfo requestInfo = model.getRequestInfo();
		bean.getParam().setRequestType(RequestType.init);
		FlowBuildPackageDetailsServiceBean.RequestParam param = bean.getParam();

		if ( requestInfo.getParameter("bpId") != null )
		param.setSelectedBpId( requestInfo.getParameter("bpId") );

	}
}
