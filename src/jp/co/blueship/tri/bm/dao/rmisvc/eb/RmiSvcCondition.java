package jp.co.blueship.tri.bm.dao.rmisvc.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.rmisvc.constants.RmiSvcItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the RMI service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class RmiSvcCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_RMI_SVC;

	/**
	 * build-srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * build-srv-ID's
	 */
	public String[] bldSrvIds = null;
	/**
	 * rmi-svc-ID
	 */
	public String rmiSvcId = null;
	/**
	 * rmi-svc-ID's
	 */
	public String[] rmiSvcIds = null;
	/**
	 * rmi-host-name
	 */
	public String rmiHostNm = null;
	/**
	 * rmi-service-port
	 */
	public Integer rmiSvcPort = null;
	/**
	 * rmi registration-port
	 */
	public Integer rmiRegPort = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		super.append(RmiSvcItems.delStsId, StatusFlg.off.parseBoolean());
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	public RmiSvcCondition() {
		super(attr);
	}

	/**
	 * build-srv-IDを取得します。
	 *
	 * @return build-srv-ID
	 */
	public String getBldSrvId() {
		return bldSrvId;
	}

	/**
	 * build-srv-IDを設定します。
	 *
	 * @param bldSrvId build-srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
		this.bldSrvId = bldSrvId;
		super.append(RmiSvcItems.bldSrvId, bldSrvId);
	}

	/**
	 * build-srv-ID'sを取得します。
	 *
	 * @return build-srv-ID's
	 */
	public String[] getBldSrvIds() {
		return bldSrvIds;
	}

	/**
	 * build-srv-ID'sを設定します。
	 *
	 * @param bldSrvIds build-srv-ID's
	 */
	public void setBldSrvIds(String[] bldSrvIds) {
		this.bldSrvIds = bldSrvIds;
		super.append(RmiSvcItems.bldSrvId.getItemName() + "[]",
				SqlFormatUtils.getCondition(bldSrvIds, RmiSvcItems.bldSrvId.getItemName(), false, true, false));
	}

	/**
	 * rmi-svc-IDを取得します。
	 *
	 * @return rmi-svc-ID
	 */
	public String getRmiSvcId() {
		return rmiSvcId;
	}

	/**
	 * rmi-svc-IDを設定します。
	 *
	 * @param rmiSvcId rmi-svc-ID
	 */
	public void setRmiSvcId(String rmiSvcId) {
		this.rmiSvcId = rmiSvcId;
		super.append(RmiSvcItems.rmiSvcId, rmiSvcId);
	}

	/**
	 * rmi-svc-ID'sを取得します。
	 *
	 * @return rmi-svc-ID's
	 */
	public String[] getRmiSvcIds() {
		return rmiSvcIds;
	}

	/**
	 * rmi-svc-ID'sを設定します。
	 *
	 * @param rmiSvcIds rmi-svc-ID's
	 */
	public void setRmiSvcIds(String[] rmiSvcIds) {
		this.rmiSvcIds = rmiSvcIds;
		super.append(RmiSvcItems.rmiSvcId.getItemName() + "[]",
				SqlFormatUtils.getCondition(rmiSvcIds, RmiSvcItems.rmiSvcId.getItemName(), false, true, false));
	}

	/**
	 * rmi-host-nameを取得します。
	 *
	 * @return rmi-host-name
	 */
	public String getRmiHostNm() {
		return rmiHostNm;
	}

	/**
	 * rmi-host-nameを設定します。
	 *
	 * @param rmiHostNm rmi-host-name
	 */
	public void setRmiHostNm(String rmiHostNm) {
		this.rmiHostNm = rmiHostNm;
		super.append(RmiSvcItems.rmiHostNm, rmiHostNm);
	}

	/**
	 * rmi-service-portを取得します。
	 *
	 * @return rmi-service-port
	 */
	public Integer getRmiSvcPort() {
		return rmiSvcPort;
	}

	/**
	 * rmi-service-portを設定します。
	 *
	 * @param rmiSvcPort rmi-service-port
	 */
	public void setRmiSvcPort(Integer rmiSvcPort) {
		this.rmiSvcPort = rmiSvcPort;
		super.append(RmiSvcItems.rmiSvcPort, rmiSvcPort);
	}

	/**
	 * rmi registration-portを取得します。
	 *
	 * @return rmi registration-port
	 */
	public Integer getRmiRegPort() {
		return rmiRegPort;
	}

	/**
	 * rmi registration-portを設定します。
	 *
	 * @param rmiRegPort rmi registration-port
	 */
	public void setRmiRegPort(Integer rmiRegPort) {
		this.rmiRegPort = rmiRegPort;
		super.append(RmiSvcItems.rmiRegPort, rmiRegPort);
	}

	/**
	 * delete-status-IDを取得します。
	 *
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
		return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 *
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
		this.delStsId = delStsId;
		super.append(RmiSvcItems.delStsId, (null == delStsId) ? null : delStsId.parseBoolean());
	}

	/**
	 * registration user-IDを取得します。
	 *
	 * @return registration user-ID
	 */
	public String getRegUserId() {
		return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 *
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
		this.regUserId = regUserId;
		super.append(RmiSvcItems.regUserId, regUserId);
	}

	/**
	 * registration user-ID'sを取得します。
	 *
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
		return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 *
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
		this.regUserIds = regUserIds;
		super.append(RmiSvcItems.regUserId.getItemName() + "[]",
				SqlFormatUtils.getCondition(regUserIds, RmiSvcItems.regUserId.getItemName(), false, true, false));
	}

	/**
	 * registration user nameを取得します。
	 *
	 * @return registration user name
	 */
	public String getRegUserNm() {
		return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 *
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
		this.regUserNm = regUserNm;
		super.append(RmiSvcItems.regUserNm, regUserNm);
	}

	/**
	 * registration time stampを取得します。
	 *
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
		return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 *
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
		this.regTimestamp = regTimestamp;
		super.append(RmiSvcItems.regTimestamp, regTimestamp);
	}

	/**
	 * update user-IDを取得します。
	 *
	 * @return update user-ID
	 */
	public String getUpdUserId() {
		return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 *
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
		this.updUserId = updUserId;
		super.append(RmiSvcItems.updUserId, updUserId);
	}

	/**
	 * update user-ID'sを取得します。
	 *
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
		return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 *
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
		this.updUserIds = updUserIds;
		super.append(RmiSvcItems.updUserId.getItemName() + "[]",
				SqlFormatUtils.getCondition(updUserIds, RmiSvcItems.updUserId.getItemName(), false, true, false));
	}

	/**
	 * update user nameを取得します。
	 *
	 * @return update user name
	 */
	public String getUpdUserNm() {
		return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 *
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
		this.updUserNm = updUserNm;
		super.append(RmiSvcItems.updUserNm, updUserNm);
	}

	/**
	 * update time stampを取得します。
	 *
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
		return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 *
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
		this.updTimestamp = updTimestamp;
		super.append(RmiSvcItems.updTimestamp, updTimestamp);
	}
}