package jp.co.blueship.tri.bm.dao.rmisvc;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.rmisvc.constants.RmiSvcItems;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.RmiSvcEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the RMI service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class RmiSvcDao extends JdbcBaseDao<IRmiSvcEntity> implements IRmiSvcDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_RMI_SVC;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IRmiSvcEntity entity ) {
		builder
			.append(RmiSvcItems.bldSrvId, entity.getBldSrvId(), true)
			.append(RmiSvcItems.rmiSvcId, entity.getRmiSvcId(), true)
			.append(RmiSvcItems.rmiHostNm, entity.getRmiHostNm())
			.append(RmiSvcItems.rmiSvcPort, entity.getRmiSvcPort())
			.append(RmiSvcItems.rmiRegPort, entity.getRmiRegPort())
			.append(RmiSvcItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(RmiSvcItems.regTimestamp, entity.getRegTimestamp())
			.append(RmiSvcItems.regUserId, entity.getRegUserId())
			.append(RmiSvcItems.regUserNm, entity.getRegUserNm())
			.append(RmiSvcItems.updTimestamp, entity.getUpdTimestamp())
			.append(RmiSvcItems.updUserId, entity.getUpdUserId())
			.append(RmiSvcItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IRmiSvcEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IRmiSvcEntity entity = new RmiSvcEntity();

		entity.setBldSrvId( rs.getString(RmiSvcItems.bldSrvId.getItemName()) );
		entity.setRmiSvcId( rs.getString(RmiSvcItems.rmiSvcId.getItemName()) );
		entity.setRmiHostNm( rs.getString(RmiSvcItems.rmiHostNm.getItemName()) );
		entity.setRmiSvcPort( rs.getInt(RmiSvcItems.rmiSvcPort.getItemName()) );
		entity.setRmiRegPort( rs.getInt(RmiSvcItems.rmiRegPort.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(RmiSvcItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(RmiSvcItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(RmiSvcItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(RmiSvcItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(RmiSvcItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(RmiSvcItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(RmiSvcItems.updUserNm.getItemName()) );

		return entity;
	}

}
