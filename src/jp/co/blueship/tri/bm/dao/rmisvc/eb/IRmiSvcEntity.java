package jp.co.blueship.tri.bm.dao.rmisvc.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the rmi service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRmiSvcEntity extends IEntityFooter {

	public String getBldSrvId();
	public void setBldSrvId(String bldSrvId);

	public String getRmiSvcId();
	public void setRmiSvcId(String rmiSvcId);

	public String getRmiHostNm();
	public void setRmiHostNm(String rmiHostNm);

	public Integer getRmiSvcPort();
	public void setRmiSvcPort(Integer rmiSvcPort);

	public Integer getRmiRegPort();
	public void setRmiRegPort(Integer rmiRegPort);

}
