package jp.co.blueship.tri.bm.dao.rmisvc.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * rmi service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RmiSvcEntity extends EntityFooter implements IRmiSvcEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * rmi svc-ID
	 */
	public String rmiSvcId = null;
	/**
	 * rmi host name
	 */
	public String rmiHostNm = null;
	/**
	 * rmi service port
	 */
	public Integer rmiSvcPort;
	/**
	 * rmiregistration port
	 */
	public Integer rmiRegPort;

	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	}
	/**
	 * rmi svc-IDを取得します。
	 * @return rmi svc-ID
	 */
	public String getRmiSvcId() {
	    return rmiSvcId;
	}
	/**
	 * rmi svc-IDを設定します。
	 * @param rmiSvcId rmi svc-ID
	 */
	public void setRmiSvcId(String rmiSvcId) {
	    this.rmiSvcId = rmiSvcId;
	}
	/**
	 * rmi host nameを取得します。
	 * @return rmi host name
	 */
	public String getRmiHostNm() {
	    return rmiHostNm;
	}
	/**
	 * rmi host nameを設定します。
	 * @param rmiHostNm rmi host name
	 */
	public void setRmiHostNm(String rmiHostNm) {
	    this.rmiHostNm = rmiHostNm;
	}
	/**
	 * rmi service portを取得します。
	 * @return rmi service port
	 */
	public Integer getRmiSvcPort() {
	    return rmiSvcPort;
	}
	/**
	 * rmi service portを設定します。
	 * @param rmiSvcPort rmi service port
	 */
	public void setRmiSvcPort(Integer rmiSvcPort) {
	    this.rmiSvcPort = rmiSvcPort;
	}
	/**
	 * rmiregistration portを取得します。
	 * @return rmiregistration port
	 */
	public Integer getRmiRegPort() {
	    return rmiRegPort;
	}
	/**
	 * rmiregistration portを設定します。
	 * @param rmiRegPort rmiregistration port
	 */
	public void setRmiRegPort(Integer rmiRegPort) {
	    this.rmiRegPort = rmiRegPort;
	}

}
