package jp.co.blueship.tri.bm.dao.rmisvc.eb;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;

/**
 * RMI Service DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRmiSvcDto extends Serializable {

    /**
	 * Agentの接続情報を取得します。
	 *
	 * @return Agentの接続情報Entity
	 */
	IRmiSvcEntity getRmiSvcEntity();

    /**
	 * Agentの接続情報を設定します。
	 *
	 * @param rmiSvcEntity Agentの接続情報Entity
	 */
	void setRmiSvcEntity(IRmiSvcEntity rmiSvcEntity) ;

    /**
	 * ビルド環境サーバーを取得します。
	 *
	 * @return ビルド環境サーバーEntity
	 */
	List<IBldEnvSrvEntity> getBldEnvSrvEntity();

    /**
	 * ビルド環境サーバーを設定します。
	 *
	 * @param pjtAvlEntity ビルド環境サーバーEntity
	 */
	void setBldEnvSrvEntity(List<IBldEnvSrvEntity> bldEnvSrvEntities);

}
