package jp.co.blueship.tri.bm.dao.rmisvc.eb;

import java.util.List;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;

/**
 * RMI Service DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class RmiSvcDto implements IRmiSvcDto {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Agentの接続情報
	 */
	public IRmiSvcEntity rmiSvcEntity = null;

	/**
	 * ビルド環境サーバー
	 */
	public List<IBldEnvSrvEntity> bldEnvSrvEntities = null;

	/**
	 * Agentの接続情報を取得します。
	 *
	 * @return Agentの接続情報Entity
	 */
	public IRmiSvcEntity getRmiSvcEntity() {
		return this.rmiSvcEntity;
	}
	/**
	 * Agentの接続情報を設定します。
	 *
	 * @param rmiSvcEntity Agentの接続情報Entity
	 */
	public void setRmiSvcEntity(IRmiSvcEntity rmiSvcEntity)  {
		this.rmiSvcEntity = rmiSvcEntity;
	}

	/**
	 * ビルド環境サーバーを取得します。
	 *
	 * @return ビルド環境サーバーEntity
	 */
	public List<IBldEnvSrvEntity> getBldEnvSrvEntity() {
		return this.bldEnvSrvEntities;
	}
	/**
	 * ビルド環境サーバーを設定します。
	 *
	 * @param pjtAvlEntity ビルド環境サーバーEntity
	 */
	public void setBldEnvSrvEntity(List<IBldEnvSrvEntity> bldEnvSrvEntities) {
		this.bldEnvSrvEntities = bldEnvSrvEntities;
	}
}
