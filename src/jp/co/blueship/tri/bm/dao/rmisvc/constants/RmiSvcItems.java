package jp.co.blueship.tri.bm.dao.rmisvc.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the RMI service entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum RmiSvcItems implements ITableItem {
	bldSrvId("bld_srv_id"),
	rmiSvcId("rmi_svc_id"),
	rmiHostNm("rmi_host_nm"),
	rmiSvcPort("rmi_svc_port"),
	rmiRegPort("rmi_reg_port"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private RmiSvcItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
