package jp.co.blueship.tri.bm.dao.rmisvc;

import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the RMI service DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IRmiSvcDao extends IJdbcDao<IRmiSvcEntity> {

}
