package jp.co.blueship.tri.bm.dao.constants;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;

/**
 * The enum of the tables.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum BmTables implements ITableAttribute {

	BM_BLD_SRV( "BBS" ),
	BM_RMI_SVC( "BRS" ),
	BM_BLD_ENV( "BBE" ),
	BM_BLD_ENV_SRV( "BBES" ),
	BM_TASK_FLOW( "BTF" ),
	BM_TASK_FLOW_PROC( "BTFP" ),
	BM_TASK_FLOW_RES( "BTFR" ),
	BM_BLD_TIMELINE( "BBT" ),
	BM_BLD_TIMELINE_AGENT( "BBTA" ),
	BM_BP( "BB" ),
	BM_BP_AREQ_LNK( "BBAL" ),
	BM_BP_MDL_LNK( "BBML" );

	private String alias = null;

	private BmTables( String alias) {
		this.alias = alias;
	}

	/**
	 * Alias付きのテーブル名を取得します。
	 *
	 * @return 文字列
	 */
	public String nameWithAlias() {
		return this.name() + " " + this.alias() ;
	}

	/**
	 * テーブルごとに一意となるエイリアスを取得します。
	 *
	 * @return 文字列
	 */
	public String alias() {
		return this.alias;
	}

	/**
	 * 指定された名前に対応する列挙型を取得します。
	 *
	 * @param name 名前
	 * @return 対応する列挙型
	 */
	public static BmTables name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return null;
		}

		for ( BmTables table : values() ) {
			if ( table.name().equalsIgnoreCase( name ) ) {
				return table;
			}
		}

		return null;
	}

}
