package jp.co.blueship.tri.bm.dao.bldsrv;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bldsrv.constants.BldSrvItems;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build server DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BldSrvDao extends JdbcBaseDao<IBldSrvEntity> implements IBldSrvDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BLD_SRV;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBldSrvEntity entity ) {
		builder

			.append(BldSrvItems.bldSrvId, entity.getBldSrvId(), true)
			.append(BldSrvItems.bldSrvNm, entity.getBldSrvNm())
			.append(BldSrvItems.isAgent, (null == entity.getIsAgent())? StatusFlg.off.parseBoolean(): entity.getIsAgent().parseBoolean())
			.append(BldSrvItems.sortOdr, (null == entity.getSortOdr())? 0: entity.getSortOdr())
			.append(BldSrvItems.srvStsIdOff, entity.getSrvStsIdOff())
			.append(BldSrvItems.srvStsIdActive, entity.getSrvStsIdActive())
			.append(BldSrvItems.srvStsIdErr, entity.getSrvStsIdErr())
			.append(BldSrvItems.remarks, entity.getRemarks())
			.append(BldSrvItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BldSrvItems.regTimestamp, entity.getRegTimestamp())
			.append(BldSrvItems.regUserId, entity.getRegUserId())
			.append(BldSrvItems.regUserNm, entity.getRegUserNm())
			.append(BldSrvItems.updTimestamp, entity.getUpdTimestamp())
			.append(BldSrvItems.updUserId, entity.getUpdUserId())
			.append(BldSrvItems.updUserNm, entity.getUpdUserNm())
			.append(BldSrvItems.osTyp, entity.getOsTyp())
			;

		return builder;
	}

	@Override
	protected final IBldSrvEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBldSrvEntity entity = new BldSrvEntity();

		entity.setBldSrvId( rs.getString(BldSrvItems.bldSrvId.getItemName()) );
		entity.setBldSrvNm( rs.getString(BldSrvItems.bldSrvNm.getItemName()) );
		entity.setIsAgent( StatusFlg.value(rs.getBoolean(BldSrvItems.isAgent.getItemName())) );
		entity.setSortOdr( rs.getInt(BldSrvItems.sortOdr.getItemName()) );
		entity.setSrvStsIdOff( rs.getString(BldSrvItems.srvStsIdOff.getItemName()) );
		entity.setSrvStsIdActive( rs.getString(BldSrvItems.srvStsIdActive.getItemName()) );
		entity.setSrvStsIdErr( rs.getString(BldSrvItems.srvStsIdErr.getItemName()) );
		entity.setRemarks( rs.getString(BldSrvItems.remarks.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BldSrvItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BldSrvItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BldSrvItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BldSrvItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BldSrvItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BldSrvItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BldSrvItems.updUserNm.getItemName()) );
		entity.setOsTyp( rs.getString(BldSrvItems.osTyp.getItemName()) );

		return entity;
	}

}
