package jp.co.blueship.tri.bm.dao.bldsrv.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.bldsrv.constants.BldSrvItems;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the build server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BldSrvCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_BLD_SRV;

	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * build-srv-ID's
	 */
	public String[] bldSrvIds = null;
	/**
	 * build server name
	 */
	public String bldSrvNm = null;
	/**
	 * is agent
	 */
	public StatusFlg isAgent = StatusFlg.off;
	/**
	 * sort order
	 */
	public Integer sortOdr = null;
	/**
	 * server status ID off
	 */
	public String srvStsIdOff = null;
	/**
	 * server status ID active
	 */
	public String srvStsIdActive = null;
	/**
	 * server status ID error
	 */
	public String srvStsIdErr = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(BldSrvItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;
	/**
	 * OS Type
	 */
	public String osTyp = null;

	public BldSrvCondition(){
		super(attr);
	}

	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}

	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	    super.append(BldSrvItems.bldSrvId, bldSrvId );
	}

	/**
	 * build-srv-ID'sを取得します。
	 * @return build-srv-ID's
	 */
	public String[] getBldSrvIds() {
	    return bldSrvIds;
	}

	/**
	 * build-srv-ID'sを設定します。
	 * @param bldSrvIds build-srv-ID's
	 */
	public void setBldSrvIds(String[] bldSrvIds) {
	    this.bldSrvIds = bldSrvIds;
	    super.append( BldSrvItems.bldSrvId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bldSrvIds, BldSrvItems.bldSrvId.getItemName(), false, true, false) );
	}

	/**
	 * build server nameを取得します。
	 * @return build server name
	 */
	public String getBldSrvNm() {
	    return bldSrvNm;
	}

	/**
	 * build server nameを設定します。
	 * @param bldSrvNm build server name
	 */
	public void setBldSrvNm(String bldSrvNm) {
	    this.bldSrvNm = bldSrvNm;
	    super.append(BldSrvItems.bldSrvNm, bldSrvNm );
	}

	/**
	 * is agentを取得します。
	 * @return is agent
	 */
	public StatusFlg getIsAgent() {
	    return isAgent;
	}

	/**
	 * is agentを設定します。
	 * @param isAgent is agent
	 */
	public void setIsAgent(StatusFlg isAgent) {
	    this.isAgent = isAgent;
	    super.append( BldSrvItems.isAgent, (null == isAgent)? StatusFlg.off.parseBoolean(): isAgent.parseBoolean() );
	}

	/**
	 * sort orderを取得します。
	 * @return sort order
	 */
	public Integer getSortOdr() {
	    return sortOdr;
	}

	/**
	 * sort orderを設定します。
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr) {
	    this.sortOdr = sortOdr;
	    super.append(BldSrvItems.sortOdr, sortOdr );
	}

	/**
	 * server status ID offを取得します。
	 * @return srvStsIdOff
	 */
	public String getsrvStsIdOff() {
	    return srvStsIdOff;
	}

	/**
	 * server status ID offを設定します。
	 * @param imgOff image off
	 */
	public void setsrvStsIdOff(String srvStsIdOff) {
	    this.srvStsIdOff = srvStsIdOff;
	    super.append(BldSrvItems.srvStsIdOff, srvStsIdOff );
	}

	/**
	 * server status ID activeを取得します。
	 * @return srvStsIdActive
	 */
	public String getsrvStsIdActive() {
	    return srvStsIdActive;
	}

	/**
	 * server status ID activeを設定します。
	 * @param srvStsIdActive
	 */
	public void setImgActive(String srvStsIdActive) {
	    this.srvStsIdActive = srvStsIdActive;
	    super.append(BldSrvItems.srvStsIdActive, srvStsIdActive );
	}

	/**
	 * server status ID errorを取得します。
	 * @return srvStsId
	 */
	public String getsrvStsIdErr() {
	    return srvStsIdErr;
	}

	/**
	 * server status ID errorを設定します。
	 * @param srvStsIdErr
	 */
	public void setImgErr(String srvStsIdErr) {
	    this.srvStsIdErr = srvStsIdErr;
	    super.append(BldSrvItems.srvStsIdErr, srvStsIdErr );
	}

	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}

	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	    super.append(BldSrvItems.remarks, remarks );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( BldSrvItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(BldSrvItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( BldSrvItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, BldSrvItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(BldSrvItems.regUserNm, regUserNm );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(BldSrvItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(BldSrvItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( BldSrvItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, BldSrvItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(BldSrvItems.updUserNm, updUserNm );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(BldSrvItems.updTimestamp, updTimestamp );
	}

	/**
	 * OS Typeを取得します。
	 * @return osTyp
	 */
	public String getOsTyp() {
	    return osTyp;
	}

	/**
	 * OS Typeを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setOsTyp(String osType) {
	    this.osTyp = osType;
	    super.append(BldSrvItems.osTyp, osTyp );
	}
}