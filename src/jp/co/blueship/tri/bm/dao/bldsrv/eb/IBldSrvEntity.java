package jp.co.blueship.tri.bm.dao.bldsrv.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldSrvEntity extends IEntityFooter {

	public String getBldSrvId();
	public void setBldSrvId(String bldSrvId);

	public String getBldSrvNm();
	public void setBldSrvNm(String bldSrvNm);

	public StatusFlg getIsAgent();
	public void setIsAgent(StatusFlg isAgent);

	public Integer getSortOdr();
	public void setSortOdr(Integer sortOdr);

	public String getSrvStsIdOff();
	public void setSrvStsIdOff(String SrvStsIdOff);

	public String getSrvStsIdActive();
	public void setSrvStsIdActive(String SrvStsIdActive);

	public String getSrvStsIdErr();
	public void setSrvStsIdErr(String SrvStsIdErr);

	public String getRemarks();
	public void setRemarks(String remarks);

	public String getOsTyp();
	public void setOsTyp(String ostype);

}
