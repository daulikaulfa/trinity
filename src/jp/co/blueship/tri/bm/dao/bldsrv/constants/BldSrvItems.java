package jp.co.blueship.tri.bm.dao.bldsrv.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the build server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum BldSrvItems implements ITableItem {
	bldSrvId("bld_srv_id"),
	bldSrvNm("bld_srv_nm"),
	isAgent("is_agent"),
	sortOdr("sort_odr"),
	srvStsIdOff("srv_sts_id_off"),
	srvStsIdActive("srv_sts_id_active"),
	srvStsIdErr("srv_sts_id_err"),
	remarks("remarks"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp"),
	osTyp("os_typ");

	private String element = null;

	private BldSrvItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
