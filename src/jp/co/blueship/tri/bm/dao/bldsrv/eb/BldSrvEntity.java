package jp.co.blueship.tri.bm.dao.bldsrv.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BldSrvEntity extends EntityFooter implements IBldSrvEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * build server name
	 */
	public String bldSrvNm = null;
	/**
	 * is agent
	 */
	public StatusFlg isAgent = StatusFlg.off;
	/**
	 * sort order
	 */
	public Integer sortOdr = null;
	/**
	 * server status ID off
	 */
	public String srvStsIdOff = null;
	/**
	 * server status ID active
	 */
	public String srvStsIdActive = null;
	/**
	 * server status ID error
	 */
	public String srvStsIdErr = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * OS type
	 */
	public String osTyp = null;

	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	}
	/**
	 * build server nameを取得します。
	 * @return build server name
	 */
	public String getBldSrvNm() {
	    return bldSrvNm;
	}
	/**
	 * build server nameを設定します。
	 * @param bldSrvNm build server name
	 */
	public void setBldSrvNm(String bldSrvNm) {
	    this.bldSrvNm = bldSrvNm;
	}
	/**
	 * is agentを取得します。
	 * @return is agent
	 */
	public StatusFlg getIsAgent() {
	    return isAgent;
	}
	/**
	 * is agentを設定します。
	 * @param isAgent is agent
	 */
	public void setIsAgent(StatusFlg isAgent) {
	    this.isAgent = isAgent;
	}
	/**
	 * sort orderを取得します。
	 * @return sort order
	 */
	public Integer getSortOdr() {
	    return sortOdr;
	}
	/**
	 * sort orderを設定します。
	 * @param sortOdr sort order
	 */
	public void setSortOdr(Integer sortOdr) {
	    this.sortOdr = sortOdr;
	}
	/**
	 * server status ID offを取得します。
	 * @return srvStsIdOff
	 */
	public String getSrvStsIdOff() {
	    return srvStsIdOff;
	}
	/**
	 * server status ID offを設定します。
	 * @param srvStsIdOff
	 */
	public void setSrvStsIdOff(String srvStsIdOff) {
	    this.srvStsIdOff = srvStsIdOff;
	}
	/**
	 * server status ID activeを取得します。
	 * @return srvStsIdActive
	 */
	public String getSrvStsIdActive() {
	    return srvStsIdActive;
	}
	/**
	 * server status ID activeを設定します。
	 * @param srvStsIdActive
	 */
	public void setSrvStsIdActive(String srvStsIdActive) {
	    this.srvStsIdActive = srvStsIdActive;
	}
	/**
	 * server status ID errorを取得します。
	 * @return srvStsIdErr
	 */
	public String getSrvStsIdErr() {
	    return srvStsIdErr;
	}
	/**
	 * server status ID errorを設定します。
	 * @param srvStsIdErr
	 */
	public void setSrvStsIdErr(String srvStsIdErr) {
	    this.srvStsIdErr = srvStsIdErr;
	}
	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}
	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	}
	/**
	 * OS Typeを取得します。
	 * @return osType
	 */
	public String getOsTyp() {
	    return osTyp;
	}
	/**
	 * OS Typeを設定します。
	 * @param osType
	 */
	public void setOsTyp(String osType) {
	    this.osTyp = osType;
	}

}
