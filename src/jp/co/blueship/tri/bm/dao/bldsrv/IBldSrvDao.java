package jp.co.blueship.tri.bm.dao.bldsrv;

import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build server DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldSrvDao extends IJdbcDao<IBldSrvEntity> {

}
