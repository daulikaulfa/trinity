package jp.co.blueship.tri.bm.dao.bldenv.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the build environment entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum BldEnvItems implements ITableItem {
	bldEnvId("bld_env_id"),
	svcId("svc_id"),
	bldEnvNm("bld_env_nm"),
	remarks("remarks"),
	stsId("sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private BldEnvItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
