package jp.co.blueship.tri.bm.dao.bldenv;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build environment server DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldEnvSrvDao extends IJdbcDao<IBldEnvSrvEntity> {

}
