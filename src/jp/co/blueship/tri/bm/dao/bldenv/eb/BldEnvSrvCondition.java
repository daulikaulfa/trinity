package jp.co.blueship.tri.bm.dao.bldenv.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvSrvItems;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the vcs repository entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BldEnvSrvCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_BLD_ENV_SRV;

	/**
	 * build-env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build-srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * build-srv-ID's
	 */
	public String[] bldSrvIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(BldEnvSrvItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	public BldEnvSrvCondition(){
		super(attr);
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(BldEnvSrvItems.bldEnvId, bldEnvId );
	}

	/**
	 * build-srv-IDを取得します。
	 * @return build-srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}

	/**
	 * build-srv-IDを設定します。
	 * @param bldSrvId build-srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	    super.append(BldEnvSrvItems.bldSrvId, bldSrvId );
	}

	/**
	 * build-srv-ID'sを取得します。
	 * @return build-srv-ID's
	 */
	public String[] getBldSrvIds() {
	    return bldSrvIds;
	}

	/**
	 * build-srv-ID'sを設定します。
	 * @param bldSrvIds build-srv-ID's
	 */
	public void setBldSrvIds(String[] bldSrvIds) {
	    this.bldSrvIds = bldSrvIds;
	    super.append( BldEnvSrvItems.bldSrvId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bldSrvIds, BldEnvSrvItems.bldSrvId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( BldEnvSrvItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(BldEnvSrvItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( BldEnvSrvItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, BldEnvSrvItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(BldEnvSrvItems.regUserNm, regUserNm );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(BldEnvSrvItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(BldEnvSrvItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( BldEnvSrvItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, BldEnvSrvItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(BldEnvSrvItems.updUserNm, updUserNm );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(BldEnvSrvItems.updTimestamp, updTimestamp );
	}
}