package jp.co.blueship.tri.bm.dao.bldenv;

import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build environment DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldEnvDao extends IJdbcDao<IBldEnvEntity> {

}
