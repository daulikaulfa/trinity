package jp.co.blueship.tri.bm.dao.bldenv.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build environment entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldEnvEntity extends IEntityFooter {

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getSvcId();
	public void setSvcId(String svcId);

	public String getBldEnvNm();
	public void setBldEnvNm(String bldEnvNm);

	public String getRemarks();
	public void setRemarks(String remarks);

	public String getStsId();
	public void setStsId(String stsId);

}
