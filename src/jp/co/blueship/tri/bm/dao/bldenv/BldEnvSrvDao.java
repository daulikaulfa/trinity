package jp.co.blueship.tri.bm.dao.bldenv;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvSrvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build environment DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BldEnvSrvDao extends JdbcBaseDao<IBldEnvSrvEntity> implements IBldEnvSrvDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BLD_ENV_SRV;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBldEnvSrvEntity entity ) {
		builder
			.append(BldEnvSrvItems.bldEnvId, entity.getBldEnvId(), true)
			.append(BldEnvSrvItems.bldSrvId, entity.getBldSrvId(), true)
			.append(BldEnvSrvItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BldEnvSrvItems.regTimestamp, entity.getRegTimestamp())
			.append(BldEnvSrvItems.regUserId, entity.getRegUserId())
			.append(BldEnvSrvItems.regUserNm, entity.getRegUserNm())
			.append(BldEnvSrvItems.updTimestamp, entity.getUpdTimestamp())
			.append(BldEnvSrvItems.updUserId, entity.getUpdUserId())
			.append(BldEnvSrvItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBldEnvSrvEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBldEnvSrvEntity entity = new BldEnvSrvEntity();

		entity.setBldEnvId( rs.getString(BldEnvSrvItems.bldEnvId.getItemName()) );
		entity.setBldSrvId( rs.getString(BldEnvSrvItems.bldSrvId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BldEnvSrvItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BldEnvSrvItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BldEnvSrvItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BldEnvSrvItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BldEnvSrvItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BldEnvSrvItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BldEnvSrvItems.updUserNm.getItemName()) );

		return entity;
	}

}
