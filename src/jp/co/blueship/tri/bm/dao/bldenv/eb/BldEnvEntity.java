package jp.co.blueship.tri.bm.dao.bldenv.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;


/**
 * build environment entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BldEnvEntity extends EntityFooter implements IBldEnvEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * build environment name
	 */
	public String bldEnvNm = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}
	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	}
	/**
	 * build environment nameを取得します。
	 * @return build environment name
	 */
	public String getBldEnvNm() {
	    return bldEnvNm;
	}
	/**
	 * build environment nameを設定します。
	 * @param bldEnvNm build environment name
	 */
	public void setBldEnvNm(String bldEnvNm) {
	    this.bldEnvNm = bldEnvNm;
	}
	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}
	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}

}
