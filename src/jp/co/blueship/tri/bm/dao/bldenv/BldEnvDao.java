package jp.co.blueship.tri.bm.dao.bldenv;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build environment DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BldEnvDao extends JdbcBaseDao<IBldEnvEntity> implements IBldEnvDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BLD_ENV;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBldEnvEntity entity ) {
		builder
			.append(BldEnvItems.bldEnvId, entity.getBldEnvId(), true)
			.append(BldEnvItems.svcId, entity.getSvcId())
			.append(BldEnvItems.bldEnvNm, entity.getBldEnvNm())
			.append(BldEnvItems.remarks, entity.getRemarks())
			.append(BldEnvItems.stsId, entity.getStsId())
			.append(BldEnvItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BldEnvItems.regTimestamp, entity.getRegTimestamp())
			.append(BldEnvItems.regUserId, entity.getRegUserId())
			.append(BldEnvItems.regUserNm, entity.getRegUserNm())
			.append(BldEnvItems.updTimestamp, entity.getUpdTimestamp())
			.append(BldEnvItems.updUserId, entity.getUpdUserId())
			.append(BldEnvItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBldEnvEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBldEnvEntity entity = new BldEnvEntity();

		entity.setBldEnvId( rs.getString(BldEnvItems.bldEnvId.getItemName()) );
		entity.setSvcId( rs.getString(BldEnvItems.svcId.getItemName()) );
		entity.setBldEnvNm( rs.getString(BldEnvItems.bldEnvNm.getItemName()) );
		entity.setRemarks( rs.getString(BldEnvItems.remarks.getItemName()) );
		entity.setStsId( rs.getString(BldEnvItems.stsId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BldEnvItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BldEnvItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BldEnvItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BldEnvItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BldEnvItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BldEnvItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BldEnvItems.updUserNm.getItemName()) );

		return entity;
	}

}
