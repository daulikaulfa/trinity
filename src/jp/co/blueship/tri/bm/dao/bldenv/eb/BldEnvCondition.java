package jp.co.blueship.tri.bm.dao.bldenv.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvItems;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the build environment entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BldEnvCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_BLD_ENV;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build env-ID's
	 */
	public String[] bldEnvIds = null;
	/**
	 * svc-ID
	 */
	public String svcId = null;
	/**
	 * svc-ID's
	 */
	public String[] svcIds = null;
	/**
	 * build environment name
	 */
	public String bldEnvNm = null;
	/**
	 * remarks
	 */
	public String remarks = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(BldEnvItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	public BldEnvCondition(){
		super(attr);
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(BldEnvItems.bldEnvId, bldEnvId );
	}

	/**
	 * build env-ID'sを取得します。
	 * @return build env-ID's
	 */
	public String[] getBldEnvIds() {
	    return bldEnvIds;
	}

	/**
	 * build env-ID'sを設定します。
	 * @param bldEnvIds build env-ID's
	 */
	public void setBldEnvIds(String[] bldEnvIds) {
	    this.bldEnvIds = bldEnvIds;
	    super.append( BldEnvItems.bldEnvId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bldEnvIds, BldEnvItems.bldEnvId.getItemName(), false, true, false) );
	}
	/**
	 * svc-IDを取得します。
	 * @return svc-ID
	 */
	public String getSvcId() {
	    return svcId;
	}

	/**
	 * svc-IDを設定します。
	 * @param svcId svc-ID
	 */
	public void setSvcId(String svcId) {
	    this.svcId = svcId;
	    super.append(BldEnvItems.svcId, svcId );
	}

	/**
	 * svc-ID'sを取得します。
	 * @return svc-ID's
	 */
	public String[] getSvcIds() {
	    return svcIds;
	}

	/**
	 * svc-ID'sを設定します。
	 * @param svcIds svc-ID's
	 */
	public void setSvcIds(String[] svcIds) {
	    this.svcIds = svcIds;
	    super.append( BldEnvItems.svcId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(svcIds, BldEnvItems.svcId.getItemName(), false, true, false) );
	}

	/**
	 * build environment nameを取得します。
	 * @return build environment name
	 */
	public String getBldEnvNm() {
	    return bldEnvNm;
	}

	/**
	 * build environment nameを設定します。
	 * @param bldEnvNm build environment name
	 */
	public void setBldEnvNm(String bldEnvNm) {
	    this.bldEnvNm = bldEnvNm;
	    super.append(BldEnvItems.bldEnvNm, bldEnvNm );
	}

	/**
	 * remarksを取得します。
	 * @return remarks
	 */
	public String getRemarks() {
	    return remarks;
	}

	/**
	 * remarksを設定します。
	 * @param remarks remarks
	 */
	public void setRemarks(String remarks) {
	    this.remarks = remarks;
	    super.append(BldEnvItems.remarks, remarks );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(BldEnvItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( BldEnvItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, BldEnvItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( BldEnvItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(BldEnvItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( BldEnvItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, BldEnvItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(BldEnvItems.regUserNm, regUserNm );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(BldEnvItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(BldEnvItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( BldEnvItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, BldEnvItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(BldEnvItems.updUserNm, updUserNm );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(BldEnvItems.updTimestamp, updTimestamp );
	}
}