package jp.co.blueship.tri.bm.dao.bldenv.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build environment server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldEnvSrvEntity extends IEntityFooter {

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getBldSrvId();
	public void setBldSrvId(String bldSrvId);

	public StatusFlg getAgentStsId();
	public void setAgentStsId(StatusFlg agentStsId);
	public StatusFlg getIsAgent();
	public void setIsAgent(StatusFlg isAgent);
//	public String getSummary();
//	public void setSummary(String summary);
}
