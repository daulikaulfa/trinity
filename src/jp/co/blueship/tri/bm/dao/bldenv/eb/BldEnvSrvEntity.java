package jp.co.blueship.tri.bm.dao.bldenv.eb;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build environment server entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BldEnvSrvEntity extends EntityFooter implements IBldEnvSrvEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;

	// SQL設定が必要
	public StatusFlg agentStsId = StatusFlg.off;
	public StatusFlg isAgent = StatusFlg.off;
	public String summary = null;
	public StatusFlg getAgentStsId() {
	    return agentStsId;
	}
	public void setAgentStsId(StatusFlg agentStsId) {
	    this.agentStsId = agentStsId;
	}
	public StatusFlg getIsAgent() {
	    return isAgent;
	}
	public void setIsAgent(StatusFlg isAgent) {
	    this.isAgent = isAgent;
	}
	public String getSummary() {
	    return summary;
	}
	public void setSummary(String summary) {
	    this.summary = summary;
	}

//

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	}

}
