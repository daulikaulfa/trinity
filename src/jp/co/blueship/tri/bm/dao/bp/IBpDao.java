package jp.co.blueship.tri.bm.dao.bp;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build package DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBpDao extends IJdbcDao<IBpEntity> {

}
