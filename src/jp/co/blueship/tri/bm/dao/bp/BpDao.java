package jp.co.blueship.tri.bm.dao.bp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The implements of the build package DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class BpDao extends JdbcBaseDao<IBpEntity> implements IBpDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BP;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBpEntity entity ) {
		builder
			.append(BpItems.bpId, entity.getBpId(), true)
			.append(BpItems.lotId, entity.getLotId())
			.append(BpItems.bpNm, entity.getBpNm())
			.append(BpItems.summary, entity.getSummary())
			.append(BpItems.content, entity.getContent())
			.append(BpItems.bldEnvId, entity.getBldEnvId())
			.append(BpItems.execUserId, entity.getExecUserId())
			.append(BpItems.execUserNm, entity.getExecUserNm())
			.append(BpItems.procStTimestamp, entity.getProcStTimestamp())
			.append(BpItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(BpItems.procId, entity.getProcId())
			.append(BpItems.bpCloseVerTag, entity.getBpCloseVerTag())
			.append(BpItems.bpCloseBlTag, entity.getBpCloseBlTag())
			.append(BpItems.stsId, entity.getStsId())
			.append(BpItems.closeUserId, entity.getCloseUserId())
			.append(BpItems.closeUserNm, entity.getCloseUserNm())
			.append(BpItems.closeTimestamp, entity.getCloseTimestamp())
			.append(BpItems.closeCmt, entity.getCloseCmt())
			.append(BpItems.delUserId, entity.getDelUserId())
			.append(BpItems.delUserNm, entity.getDelUserNm())
			.append(BpItems.delTimestamp, entity.getDelTimestamp())
			.append(BpItems.delCmt, entity.getDelCmt())
			.append(BpItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BpItems.regTimestamp, entity.getRegTimestamp())
			.append(BpItems.regUserId, entity.getRegUserId())
			.append(BpItems.regUserNm, entity.getRegUserNm())
			.append(BpItems.updTimestamp, entity.getUpdTimestamp())
			.append(BpItems.updUserId, entity.getUpdUserId())
			.append(BpItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBpEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		BpEntity entity = new BpEntity();

		entity.setBpId( rs.getString(BpItems.bpId.getItemName()) );
		entity.setLotId( rs.getString(BpItems.lotId.getItemName()) );
		entity.setBpNm( rs.getString(BpItems.bpNm.getItemName()) );
		entity.setSummary( rs.getString(BpItems.summary.getItemName()) );
		entity.setContent( rs.getString(BpItems.content.getItemName()) );
		entity.setBldEnvId( rs.getString(BpItems.bldEnvId.getItemName()) );
		entity.setExecUserId( rs.getString(BpItems.execUserId.getItemName()) );
		entity.setExecUserNm( rs.getString(BpItems.execUserNm.getItemName()) );
		entity.setProcStTimestamp( rs.getTimestamp(BpItems.procStTimestamp.getItemName()) );
		entity.setProcEndTimestamp( rs.getTimestamp(BpItems.procEndTimestamp.getItemName()) );
	 	entity.setProcId( rs.getString(BpItems.procId.getItemName()) );
		entity.setBpCloseVerTag( rs.getString(BpItems.bpCloseVerTag.getItemName()) );
		entity.setBpCloseBlTag( rs.getString(BpItems.bpCloseBlTag.getItemName()) );
		entity.setStsId( rs.getString(BpItems.stsId.getItemName()) );
	 	entity.setProcStsId( rs.getString(BpItems.procStsId.getItemName()) );
	 	entity.setCloseUserId( rs.getString(BpItems.closeUserId.getItemName()) );
		entity.setCloseUserNm( rs.getString(BpItems.closeUserNm.getItemName()) );
		entity.setCloseTimestamp( rs.getTimestamp(BpItems.closeTimestamp.getItemName()) );
		entity.setCloseCmt( rs.getString(BpItems.closeCmt.getItemName()) );
		entity.setDelUserId( rs.getString(BpItems.delUserId.getItemName()) );
		entity.setDelUserNm( rs.getString(BpItems.delUserNm.getItemName()) );
		entity.setDelTimestamp( rs.getTimestamp(BpItems.delTimestamp.getItemName()) );
		entity.setDelCmt( rs.getString(BpItems.delCmt.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BpItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BpItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BpItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BpItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BpItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BpItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BpItems.updUserNm.getItemName()) );

		entity.setCtgId( rs.getString(CtgItems.ctgId.getItemName()) );
		entity.setCtgNm( rs.getString(CtgItems.ctgNm.getItemName()) );
		entity.setMstoneId( rs.getString(MstoneItems.mstoneId.getItemName()) );
		entity.setMstoneNm( rs.getString(MstoneItems.mstoneNm.getItemName()) );

		return entity;
	}

}
