package jp.co.blueship.tri.bm.dao.bp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build package module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBpMdlLnkEntity extends IEntityFooter {

	public String getBpId();
	public void setBpId(String bpId);

	public String getMdlNm();
	public void setMdlNm(String mdlNm);

	public String getMdlVerTag();
	public void setMdlVerTag(String mdlVerTag);

	public String getBpCloseRev();
	public void setBpCloseRev(String bpCloseRev);

}
