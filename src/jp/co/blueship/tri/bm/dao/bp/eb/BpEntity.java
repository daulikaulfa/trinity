package jp.co.blueship.tri.bm.dao.bp.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BpEntity extends EntityFooter implements IBpEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category name
	 */
	public String ctgNm = null;
	/**
	 * category ID
	 */
	public String mstoneId = null;
	/**
	 * category ID
	 */
	public String mstoneNm = null;

	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * build package name
	 */
	public String bpNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * execute user-ID
	 */
	public String execUserId = null;
	/**
	 * execute user name
	 */
	public String execUserNm = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * process ID
	 */
	public String procId = null;
	/**
	 * close version tag
	 */
	public String bpCloseVerTag = null;
	/**
	 * close baseline tag
	 */
	public String bpCloseBlTag = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * process status-ID
	 */
	public String procStsId = null;
	/**
	 * delete user-ID
	 */
	public String delUserId = null;
	/**
	 * delete user name
	 */
	public String delUserNm = null;
	/**
	 * delete time stamp
	 */
	public Timestamp delTimestamp = null;
	/**
	 * delete comment
	 */
	public String delCmt = null;
	/**
	 * close user-ID
	 */
	public String closeUserId = null;
	/**
	 * close user name
	 */
	public String closeUserNm = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * close comment
	 */
	public String closeCmt = null;

	@Override
	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	@Override
	public String getCtgNm() {
		return ctgNm;
	}
	public void setCtgNm(String ctgNm) {
		this.ctgNm = ctgNm;
	}

	@Override
	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

	@Override
	public String getMstoneNm() {
		return mstoneNm;
	}
	public void setMstoneNm(String mstoneNm) {
		this.mstoneNm = mstoneNm;
	}

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}
	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * build package nameを取得します。
	 * @return build package name
	 */
	public String getBpNm() {
	    return bpNm;
	}
	/**
	 * build package nameを設定します。
	 * @param bpNm build package name
	 */
	public void setBpNm(String bpNm) {
	    this.bpNm = bpNm;
	}
	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}
	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	}
	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}
	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	}
	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * execute user-IDを取得します。
	 * @return execute user-ID
	 */
	public String getExecUserId() {
	    return execUserId;
	}
	/**
	 * execute user-IDを設定します。
	 * @param execUserId execute user-ID
	 */
	public void setExecUserId(String execUserId) {
	    this.execUserId = execUserId;
	}
	/**
	 * execute user nameを取得します。
	 * @return execute user name
	 */
	public String getExecUserNm() {
	    return execUserNm;
	}
	/**
	 * execute user nameを設定します。
	 * @param execUserNm execute user name
	 */
	public void setExecUserNm(String execUserNm) {
	    this.execUserNm = execUserNm;
	}
	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}
	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	}
	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}
	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	}
	/**
	 * process IDを取得します。
	 * @return process ID
	 */
	public String getProcId() {
	    return procId;
	}
	/**
	 * process IDを設定します。
	 * @param procId process ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	}
	/**
	 * close version tagを取得します。
	 * @return close version tag
	 */
	public String getBpCloseVerTag() {
	    return bpCloseVerTag;
	}
	/**
	 * close version tagを設定します。
	 * @param closeVerTag close version tag
	 */
	public void setBpCloseVerTag(String closeVerTag) {
	    this.bpCloseVerTag = closeVerTag;
	}
	/**
	 * close baseline tagを取得します。
	 * @return close baseline tag
	 */
	public String getBpCloseBlTag() {
	    return bpCloseBlTag;
	}
	/**
	 * close baseline tagを設定します。
	 * @param closeBlTag close baseline tag
	 */
	public void setBpCloseBlTag(String closeBlTag) {
	    this.bpCloseBlTag = closeBlTag;
	}
	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}
	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	}
	/**
	 * process status-IDを取得します。
	 * @return process status-ID
	 */
	public String getProcStsId() {
	    return procStsId;
	}
	/**
	 * process status-IDを設定します。
	 * @param procStsId process status-ID
	 */
	public void setProcStsId(String procStsId) {
	    this.procStsId = procStsId;
	}
	/**
	 * delete user-IDを取得します。
	 * @return delete user-ID
	 */
	public String getDelUserId() {
	    return delUserId;
	}
	/**
	 * delete user-IDを設定します。
	 * @param delUserId delete user-ID
	 */
	public void setDelUserId(String delUserId) {
	    this.delUserId = delUserId;
	}
	/**
	 * delete user nameを取得します。
	 * @return delete user name
	 */
	public String getDelUserNm() {
	    return delUserNm;
	}
	/**
	 * delete user nameを設定します。
	 * @param delUserNm delete user name
	 */
	public void setDelUserNm(String delUserNm) {
	    this.delUserNm = delUserNm;
	}
	/**
	 * delete time stampを取得します。
	 * @return delete time stamp
	 */
	public Timestamp getDelTimestamp() {
	    return delTimestamp;
	}
	/**
	 * delete time stampを設定します。
	 * @param delTimestamp delete time stamp
	 */
	public void setDelTimestamp(Timestamp delTimestamp) {
	    this.delTimestamp = delTimestamp;
	}
	/**
	 * delete commentを取得します。
	 * @return delete comment
	 */
	public String getDelCmt() {
	    return delCmt;
	}
	/**
	 * delete commentを設定します。
	 * @param delCmt delete comment
	 */
	public void setDelCmt(String delCmt) {
	    this.delCmt = delCmt;
	}
	/**
	 * close user-IDを取得します。
	 * @return close user-ID
	 */
	public String getCloseUserId() {
	    return closeUserId;
	}
	/**
	 * close user-IDを設定します。
	 * @param closeUserId close user-ID
	 */
	public void setCloseUserId(String closeUserId) {
	    this.closeUserId = closeUserId;
	}
	/**
	 * close user nameを取得します。
	 * @return close user name
	 */
	public String getCloseUserNm() {
	    return closeUserNm;
	}
	/**
	 * close user nameを設定します。
	 * @param closeUserNm close user name
	 */
	public void setCloseUserNm(String closeUserNm) {
	    this.closeUserNm = closeUserNm;
	}
	/**
	 * close time stampを取得します。
	 * @return close time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}
	/**
	 * close time stampを設定します。
	 * @param closeTimestamp close time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	}
	/**
	 * close commentを取得します。
	 * @return close comment
	 */
	public String getCloseCmt() {
	    return closeCmt;
	}
	/**
	 * close commentを設定します。
	 * @param closeCmt close comment
	 */
	public void setCloseCmt(String closeCmt) {
	    this.closeCmt = closeCmt;
	}

}
