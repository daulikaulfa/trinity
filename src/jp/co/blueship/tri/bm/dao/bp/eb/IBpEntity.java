package jp.co.blueship.tri.bm.dao.bp.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.dao.orm.IEntityExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBpEntity extends IEntityFooter, IEntityExecData {

	public String getBpId();
	public void setBpId(String bpId);

	public String getLotId();
	public void setLotId(String lotId);

	public String getBpNm();
	public void setBpNm(String bpNm);

	public String getSummary();
	public void setSummary(String summary);

	public String getContent();
	public void setContent(String content);

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public String getExecUserId();
	public void setExecUserId(String execUserId);

	public String getExecUserNm();
	public void setExecUserNm(String execUserNm);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	public String getProcId();
	public void setProcId(String procId);

	public String getBpCloseVerTag();
	public void setBpCloseVerTag(String closeVerTag);

	public String getBpCloseBlTag();
	public void setBpCloseBlTag(String closeBlTag);

	public String getStsId();
	public void setStsId(String stsId);

	public String getDelUserId();
	public void setDelUserId(String delUserId);

	public String getDelUserNm();
	public void setDelUserNm(String delUserNm);

	public Timestamp getDelTimestamp();
	public void setDelTimestamp(Timestamp delTimestamp);

	public String getDelCmt();
	public void setDelCmt(String delCmt);

	public String getCloseUserId();
	public void setCloseUserId(String closeUserId);

	public String getCloseUserNm();
	public void setCloseUserNm(String closeUserNm);

	public Timestamp getCloseTimestamp();
	public void setCloseTimestamp(Timestamp closeTimestamp);

	public String getCloseCmt();
	public void setCloseCmt(String closeCmt);

	public String getProcStsId();

	public String getCtgId();
	public String getCtgNm();
	public String getMstoneId();
	public String getMstoneNm();

}
