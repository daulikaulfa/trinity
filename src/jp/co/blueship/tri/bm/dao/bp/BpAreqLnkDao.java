package jp.co.blueship.tri.bm.dao.bp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bp.constants.BpAreqLnkItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.IDaoTemplateCallback;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.DaoTemplate;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build package asset request link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class BpAreqLnkDao extends JdbcBaseDao<IBpAreqLnkEntity> implements IBpAreqLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BP_AREQ_LNK;
	}

	@Override
	public void setDaoTemplate( IDaoTemplate daoTemplate ) {
		super.setDaoTemplate( new CustomTemplate( new CustomCallback() ) );
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBpAreqLnkEntity entity ) {
		builder
			.append(BpAreqLnkItems.bpId, entity.getBpId(), true)
			.append(BpAreqLnkItems.areqId, entity.getAreqId(), true)
			.append(BpAreqLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BpAreqLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(BpAreqLnkItems.regUserId, entity.getRegUserId())
			.append(BpAreqLnkItems.regUserNm, entity.getRegUserNm())
			.append(BpAreqLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(BpAreqLnkItems.updUserId, entity.getUpdUserId())
			.append(BpAreqLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBpAreqLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		BpAreqLnkEntity entity = new BpAreqLnkEntity();

		entity.setBpId( rs.getString(BpAreqLnkItems.bpId.getItemName()) );
		entity.setAreqId( rs.getString(BpAreqLnkItems.areqId.getItemName()) );
		entity.setPjtId( rs.getString(BpAreqLnkItems.pjtId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BpAreqLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BpAreqLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BpAreqLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BpAreqLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BpAreqLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BpAreqLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BpAreqLnkItems.updUserNm.getItemName()) );

		return entity;
	}

	private class CustomTemplate extends DaoTemplate {
		protected CustomTemplate( IDaoTemplateCallback callback ) {
			super.setDaoTemplateCallback(callback);
		}
	}

	private class CustomCallback implements IDaoTemplateCallback {
		@Override
		public String appendOfSelectClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append("  ,Q.PJT_ID")
			;

			return buf.toString();
		}

		@Override
		public String appendOfFromClause(ISqlCondition condition, boolean isCountMode) {
			StringBuffer buf = new StringBuffer()
				.append(" LEFT JOIN (SELECT PJT_ID, AREQ_ID AS Q_AREQ_ID FROM AM_AREQ) Q ON AREQ_ID = Q.Q_AREQ_ID")
			;

			return buf.toString();
		}
	}

}
