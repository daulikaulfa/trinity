package jp.co.blueship.tri.bm.dao.bp.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build package asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BpAreqLnkEntity extends EntityFooter implements IBpAreqLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * areq-ID
	 */
	public String areqId = null;
	/**
	 * pjt-ID
	 */
	public String pjtId = null;

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}
	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	}
	/**
	 * areq-IDを取得します。
	 * @return areq-ID
	 */
	public String getAreqId() {
	    return areqId;
	}
	/**
	 * areq-IDを設定します。
	 * @param areqId areq-ID
	 */
	public void setAreqId(String areqId) {
	    this.areqId = areqId;
	}
	/**
	 * pjt-IDを取得します。
	 * @return pjt-ID
	 */
	public String getPjtId() {
	    return pjtId;
	}
	/**
	 * pjt-IDを設定します。
	 * @param pjtId pjt-ID
	 */
	public void setPjtId(String pjtId) {
	    this.pjtId = pjtId;
	}

}
