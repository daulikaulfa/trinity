package jp.co.blueship.tri.bm.dao.bp.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build package asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBpAreqLnkEntity extends IEntityFooter {

	public String getBpId();
	public void setBpId(String bpId);

	public String getAreqId();
	public void setAreqId(String areqId);

	public String getPjtId();

}
