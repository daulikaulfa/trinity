package jp.co.blueship.tri.bm.dao.bp;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build package asset request link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBpAreqLnkDao extends IJdbcDao<IBpAreqLnkEntity> {

}
