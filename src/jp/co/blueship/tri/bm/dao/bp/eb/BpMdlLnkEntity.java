package jp.co.blueship.tri.bm.dao.bp.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build package module link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BpMdlLnkEntity extends EntityFooter implements IBpMdlLnkEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * module name
	 */
	public String mdlNm = null;
	/**
	 * module version tag
	 */
	public String mdlVerTag = null;
	/**
	 * bp close revision
	 */
	public String bpCloseRev = null;

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}
	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	}
	/**
	 * module nameを取得します。
	 * @return module name
	 */
	public String getMdlNm() {
	    return mdlNm;
	}
	/**
	 * module nameを設定します。
	 * @param mdlNm module name
	 */
	public void setMdlNm(String mdlNm) {
	    this.mdlNm = mdlNm;
	}
	/**
	 * module version tagを取得します。
	 * @return module version tag
	 */
	public String getMdlVerTag() {
	    return mdlVerTag;
	}
	/**
	 * module version tagを設定します。
	 * @param mdlVerTag module version tag
	 */
	public void setMdlVerTag(String mdlVerTag) {
	    this.mdlVerTag = mdlVerTag;
	}
	/**
	 * bp close revisionを取得します。
	 * @return bp close revision
	 */
	public String getBpCloseRev() {
	    return bpCloseRev;
	}
	/**
	 * bp close revisionを設定します。
	 * @param bpCloseRev bp close revision
	 */
	public void setBpCloseRev(String bpCloseRev) {
	    this.bpCloseRev = bpCloseRev;
	}

}
