package jp.co.blueship.tri.bm.dao.bp.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.am.dao.baseline.constants.HeadLotBlItems;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;

/**
 * The SQL condition of the build package entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class BpCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_BP;
	/**
	 * keyword
	 */
	public String[] keywords = null;
	/**
	 * bp-ID
	 */
	public String bpId = null;
	/**
	 * bp-ID's
	 */
	public String[] bpIds = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * lot-ID's
	 */
	public String[] lotIds = null;
	/**
	 * build-package-name
	 */
	public String bpNm = null;
	/**
	 * summary
	 */
	public String summary = null;
	/**
	 * content
	 */
	public String content = null;
	/**
	 * build-env-ID
	 */
	public String bldEnvId = null;
	/**
	 * execute-user-ID
	 */
	public String execUserId = null;
	/**
	 * execute-user-ID's
	 */
	public String[] execUserIds = null;
	/**
	 * execute-user-name
	 */
	public String execUserNm = null;
	/**
	 * process-start-time-stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process-end-time-stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * close-version-tag
	 */
	public String bpCloseVerTag = null;
	/**
	 * close-baseline-tag
	 */
	public String bpCloseBlTag = null;
	/**
	 * sts-ID
	 */
	public String stsId = null;
	/**
	 * sts-ID's
	 */
	public String[] stsIds = null;
	/**
	 * close time stamp
	 */
	public Timestamp closeTimestamp = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(BpItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;
	/**
	 * process-sts-ID's
	 */
	public String[] procStsIds = null;
	/**
	 * merge check sts-ID's
	 */
	public String[] mergechkStsIds = null;
	/**
	 * close-sts-ID's
	 */
	public String[] closeStsIds = null;
	/**
	 * category ID
	 */
	public String ctgId = null;
	/**
	 * category ID's
	 */
	private String[] ctgIds = null;
	/**
	 * milestone ID
	 */
	public String mstoneId = null;
	/**
	 * milestone ID's
	 */
	private String[] mstoneIds = null;

	public BpCondition(){
		super(attr);
		super.setJoinExecData( true );
		super.setKeyByJoinExecData( BpItems.bpId );
		super.setJoinCtg(true);
		super.setJoinMstone(true);
	}

	@Override
	public ConditionSupport setJoinAccsHist( boolean isJoinAccsHist ) {
		super.setJoinAccsHist(isJoinAccsHist);
		return this;
	}

	/**
	 * keyword
	 * @param keyword
	 */
	public void setContainsByKeyword(String... keywords) {
		if ( TriStringUtils.isEmpty(keywords) ) {
			return;
		}

		this.keywords = keywords;
		super.append( BpItems.lotId.getItemName() + "Other",
				SqlFormatUtils.joinCondition( true,
						new String[]{
						SqlFormatUtils.getCondition(this.keywords, BpItems.bpId.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, BpItems.bpNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, BpItems.regUserNm.getItemName(), true, false, false),
						SqlFormatUtils.getCondition(this.keywords, BpItems.summary.getItemName(), true, false, false),

				}));
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIds(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( BpItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, BpItems.procStsId.getItemName(), false, true, false) );
	}
	/**
	 * process-status-ID'sを設定します。
	 * @param process-status-ID's procStsIds
	 */
	public void setProcStsIdsByNotEquals(String[] procStsIds) {
	    this.procStsIds = procStsIds;
	    super.appendByJoinExecData( BpItems.procStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procStsIds, BpItems.procStsId.getItemName(), false, false, true) );
	}

	/**
	 * merge check sts-IDを取得します。
	 * @return merge check sts-ID
	 */
	public String[] getMergechkStsIds() {
	    return mergechkStsIds;
	}
	/**
	 * merge check sts-ID'sを設定します。
	 * @param mergechkStsIds merge check sts-ID's
	 */
	public void setMergechkStsIds(String[] mergechkStsIds) {
	    this.mergechkStsIds = mergechkStsIds;
	    super.append( HeadLotBlItems.mergechkStsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mergechkStsIds, HeadLotBlItems.mergechkStsId.getItemName(), false, true, false) );
	}
	public void setCloseStsIds(String[] closeStsIds) {
	    this.closeStsIds = closeStsIds;
	}

	/**
	 * マージ終了日時(from-to)を設定します。（大小比較）
	 * @param from 開始日(from)
	 * @param to 終了日(to)
	 */
	public void setMergeEndDate( String from, String to ) {
		super.append( HeadLotBlItems.mergeEndTimestamp.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo(from, to,  HeadLotBlItems.mergeEndTimestamp.getItemName()) );
	}

	/**
	 * bp-IDを取得します。
	 * @return bp-ID
	 */
	public String getBpId() {
	    return bpId;
	}

	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 */
	public void setBpId(String bpId) {
	    this.bpId = bpId;
	    super.append(BpItems.bpId, bpId );
	}

	/**
	 * bp-IDを設定します。
	 * @param bpId bp-ID
	 * @param isLike 部分検索の場合、true
	 */
	public void setBpId(String bpId, boolean isLike) {
	    this.bpId = bpId;
	    super.append(BpItems.bpId.getItemName(), SqlFormatUtils.getCondition(bpId, BpItems.bpId.getItemName(), isLike) );
	}

	/**
	 * bp-ID'sを取得します。
	 * @return bp-ID's
	 */
	public String[] getBpIds() {
	    return bpIds;
	}

	/**
	 * bp-ID'sを設定します。
	 * @param bpIds bp-ID's
	 */
	public void setBpIds(String[] bpIds) {
	    this.bpIds = bpIds;
	    super.append( BpItems.bpId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(bpIds, BpItems.bpId.getItemName(), false, true, false) );
	}

	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}

	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	    super.append(BpItems.lotId, lotId );
	}

	/**
	 * lot-ID'sを取得します。
	 * @return lot-ID's
	 */
	public String[] getLotIds() {
	    return lotIds;
	}

	/**
	 * lot-ID'sを設定します。
	 * @param lotIds lot-ID's
	 */
	public void setLotIds(String[] lotIds) {
	    this.lotIds = lotIds;
	    super.append( BpItems.lotId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(lotIds, BpItems.lotId.getItemName(), false, true, false) );
	}

	/**
	 * build-package-nameを取得します。
	 * @return build-package-name
	 */
	public String getBpNm() {
	    return bpNm;
	}

	/**
	 * build-package-nameを設定します。
	 * @param bpNm build-package-name
	 */
	public void setBpNm(String bpNm) {
	    this.bpNm = bpNm;
	    super.append(BpItems.bpNm.getItemName(), SqlFormatUtils.getCondition(bpNm, BpItems.bpNm.getItemName(), true) );
	}

	/**
	 * summaryを取得します。
	 * @return summary
	 */
	public String getSummary() {
	    return summary;
	}

	/**
	 * summaryを設定します。
	 * @param summary summary
	 */
	public void setSummary(String summary) {
	    this.summary = summary;
	    super.append(BpItems.summary.getItemName(), SqlFormatUtils.getCondition(summary, BpItems.summary.getItemName(), true) );
	}

	/**
	 * contentを取得します。
	 * @return content
	 */
	public String getContent() {
	    return content;
	}

	/**
	 * contentを設定します。
	 * @param content content
	 */
	public void setContent(String content) {
	    this.content = content;
	    super.append(BpItems.content.getItemName(), SqlFormatUtils.getCondition(content, BpItems.content.getItemName(), true) );
	}

	/**
	 * build-env-IDを取得します。
	 * @return build-env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build-env-IDを設定します。
	 * @param bldEnvId build-env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(BpItems.bldEnvId, bldEnvId );
	}

	/**
	 * execute-user-IDを取得します。
	 * @return execute-user-ID
	 */
	public String getExecUserId() {
	    return execUserId;
	}

	/**
	 * execute-user-IDを設定します。
	 * @param execUserId execute-user-ID
	 */
	public void setExecUserId(String execUserId) {
	    this.execUserId = execUserId;
	    super.append(BpItems.execUserId, execUserId );
	}

	/**
	 * execute-user-ID'sを取得します。
	 * @return execute-user-ID's
	 */
	public String[] getExecUserIds() {
	    return execUserIds;
	}

	/**
	 * execute-user-ID'sを設定します。
	 * @param execUserIds execute-user-ID's
	 */
	public void setExecUserIds(String[] execUserIds) {
	    this.execUserIds = execUserIds;
	    super.append( BpItems.execUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(execUserIds, BpItems.execUserId.getItemName(), false, true, false) );
	}

	/**
	 * execute-user-nameを取得します。
	 * @return execute-user-name
	 */
	public String getExecUserNm() {
	    return execUserNm;
	}

	/**
	 * execute-user-nameを設定します。
	 * @param execUserNm execute-user-name
	 */
	public void setExecUserNm(String execUserNm) {
	    this.execUserNm = execUserNm;
	    super.append(BpItems.execUserNm.getItemName(), SqlFormatUtils.getCondition(execUserNm, BpItems.execUserNm.getItemName(), true) );
	}

	/**
	 * process-start-time-stampを取得します。
	 * @return process-start-time-stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process-start-time-stampを設定します。
	 * @param procStTimestamp process-start-time-stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(BpItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * process-end-time-stampを取得します。
	 * @return process-end-time-stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process-end-time-stampを設定します。
	 * @param procEndTimestamp process-end-time-stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(BpItems.procEndTimestamp, procEndTimestamp );
	}

	/**
	 * close-version-tagを取得します。
	 * @return close-version-tag
	 */
	public String getBpCloseVerTag() {
	    return bpCloseVerTag;
	}

	/**
	 * close-version-tagを設定します。
	 * @param closeVerTag close-version-tag
	 */
	public void setBpCloseVerTag(String closeVerTag) {
	    this.bpCloseVerTag = closeVerTag;
	    super.append(BpItems.bpCloseVerTag, closeVerTag );
	}

	/**
	 * close-baseline-tagを取得します。
	 * @return close-baseline-tag
	 */
	public String getBpCloseBlTag() {
	    return bpCloseBlTag;
	}

	/**
	 * close-baseline-tagを設定します。
	 * @param closeBlTag close-baseline-tag
	 */
	public void setBpCloseBlTag(String closeBlTag) {
	    this.bpCloseBlTag = closeBlTag;
	    super.append(BpItems.bpCloseBlTag, closeBlTag );
	}

	/**
	 * sts-IDを取得します。
	 * @return sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * sts-IDを設定します。
	 * @param stsId sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(BpItems.stsId, stsId );
	}

	/**
	 * sts-ID'sを取得します。
	 * @return sts-ID's
	 */
	public String[] getStsIds() {
	    return stsIds;
	}

	/**
	 * sts-ID'sを設定します。
	 * @param stsIds sts-ID's
	 */
	public void setStsIds(String[] stsIds) {
	    this.stsIds = stsIds;
	    super.append( BpItems.stsId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(stsIds, BpItems.stsId.getItemName(), false, true, false) );
	}

	/**
	 * close time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getCloseTimestamp() {
	    return closeTimestamp;
	}

	/**
	 * close time stamp(from-to)を設定します。（大小比較）
	 * @param closeTimestamp registration time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestampFrom, Timestamp closeTimestampTo) {
	    super.append(BpItems.closeTimestamp.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( closeTimestampFrom, closeTimestampTo, BpItems.closeTimestamp.getItemName() ) );
	}

	/**
	 * close time stampを設定します。
	 * @param closeTimestamp registration time stamp
	 */
	public void setCloseTimestamp(Timestamp closeTimestamp) {
	    this.closeTimestamp = closeTimestamp;
	    super.append(BpItems.closeTimestamp, closeTimestamp );
	}

	/**
	 * closeTimesをFromToで設定します
	 */
	public void setCloseTimeFromTo(String from, String to) {
		super.append(BpItems.closeTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, BpItems.closeTimestamp.getItemName(), true));
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( BpItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(BpItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( BpItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, BpItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(BpItems.regUserNm.getItemName(), SqlFormatUtils.getCondition(regUserNm, BpItems.regUserNm.getItemName(), true) );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(BpItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(BpItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( BpItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, BpItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(BpItems.updUserNm.getItemName(), SqlFormatUtils.getCondition(updUserNm, BpItems.updUserNm.getItemName(), true) );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(BpItems.updTimestamp, updTimestamp );
	}

	/**
	 * PrcoStTimesをFromToで設定します
	 */
	public void setProcStTimeFromTo(String from, String to) {
		super.append(BpItems.procStTimestamp.getItemName() ,
				SqlFormatUtils.getFromTo(from, to, BpItems.procStTimestamp.getItemName(), true));
	}

	/**
	 * category IDを取得します。
	 * @return category ID
	 */
	public String getCtgId() {
	    return ctgId;
	}

	/**
	 * category IDを設定します。
	 * @param ctgId category ID
	 */
	public void setCtgId(String ctgId) {
	    this.ctgId = ctgId;
	    super.append(CtgItems.ctgId, ctgId );
	}

	/**
	 * @return
	 */
	public String[] getCtgIds() {
		return ctgIds;
	}

	/**
	 * @param ctgIds
	 */
	public void setCtgIds(String[] ctgIds) {
		this.ctgIds = ctgIds;
		super.appendByJoinExecData( CtgItems.ctgId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(ctgIds, CtgItems.ctgId.getItemName(), false, true, false) );
	}

	/**
	 * milestone IDを取得します。
	 * @return milestone ID
	 */
	public String getMstoneId() {
	    return mstoneId;
	}

	/**
	 * milestone IDを設定します。
	 * @param mstoneId milestone ID
	 */
	public void setMstoneId(String mstoneId) {
	    this.mstoneId = mstoneId;
	    super.append(MstoneItems.mstoneId, mstoneId );
	}

	/**
	 * @return
	 */
	public String[] getMstoneIds() {
		return mstoneIds;
	}

	/**
	 * @param mstoneIds
	 */
	public void setMstoneIds(String[] mstoneIds) {
		this.mstoneIds = mstoneIds;
		super.appendByJoinExecData( MstoneItems.mstoneId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(mstoneIds, MstoneItems.mstoneId.getItemName(), false, true, false) );
	}

	/**
	 * @param mstoneStartDate
	 * @param mstoneEndDate
	 */
	public void setMstoneFromTo(String mstoneStartDate, String mstoneEndDate) {
		super.append(MstoneItems.mstoneStDate.getItemName() + "[]",
	    		SqlFormatUtils.getFromTo( mstoneStartDate, mstoneEndDate, MstoneItems.mstoneStDate, MstoneItems.mstoneEndDate ) );
	}
}