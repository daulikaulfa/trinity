package jp.co.blueship.tri.bm.dao.bp.eb;

import java.util.List;

/**
 * The interface of the build package DTO.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IBpDto {
	/**
	 * ビルドパッケージを取得します。
	 *
	 * @return ビルドパッケージEntity
	 */
	public IBpEntity getBpEntity();
	/**
	 * ビルドパッケージを設定します。
	 *
	 * @param bpEntity ビルドパッケージEntity
	 */
	public void setBpEntity(IBpEntity bpEntity);
	/**
	 * ビルドパッケージ・モジュールEntityのListを取得します。
	 *
	 * @return ビルドパッケージ・モジュールEntityのList
	 */
	public List<IBpMdlLnkEntity> getBpMdlLnkEntities();

	/**
	 * ビルドパッケージ・モジュールEntityのListを設定します。
	 *
	 * @param bpMdlLnkEntities ビルドパッケージ・モジュールEntityのList
	 */
	public void setBpMdlLnkEntities(List<IBpMdlLnkEntity> bpMdlLnkEntities);
	/**
	 * ビルドパッケージ・資産申請EntityのListを取得します。
	 *
	 * @return ビルドパッケージ・資産申請EntityのList
	 */
	public List<IBpAreqLnkEntity> getBpAreqLnkEntities();

	/**
	 * ビルドパッケージ・資産申請EntityのListを設定します。
	 *
	 * @param bpAreqLnkEntities ビルドパッケージ・資産申請EntityのList
	 */
	public void setBpAreqLnkEntities(List<IBpAreqLnkEntity> bpAreqLnkEntities);

}
