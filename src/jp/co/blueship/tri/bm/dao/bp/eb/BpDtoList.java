package jp.co.blueship.tri.bm.dao.bp.eb;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Extractで安全にBpDtoのListを取り出すために作成されたクラスです。
 * @author Takashi ono
 *
 */
public class BpDtoList extends ArrayList<IBpDto>{
	/**
	 * 修正後はインクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	public BpDtoList(){

	}
	public BpDtoList(Collection<IBpDto> c ){
		super(c);
	}

}
