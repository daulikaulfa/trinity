package jp.co.blueship.tri.bm.dao.bp.eb;

import java.util.ArrayList;
import java.util.List;

/**
 * build package DTO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BpDto implements IBpDto {
	private IBpEntity bpEntity = null;
	private List<IBpMdlLnkEntity> bpMdlLnkEntities = new ArrayList<IBpMdlLnkEntity>();
	private List<IBpAreqLnkEntity> bpArepLnkEntities = new ArrayList<IBpAreqLnkEntity>();

	@Override
	public IBpEntity getBpEntity() {
		return bpEntity;
	}

	@Override
	public void setBpEntity(IBpEntity bpEntity) {
		this.bpEntity = bpEntity;
	}

	@Override
	public List<IBpMdlLnkEntity> getBpMdlLnkEntities() {
		return bpMdlLnkEntities;
	}

	@Override
	public void setBpMdlLnkEntities(List<IBpMdlLnkEntity> bpMdlLnkEntities) {
		this.bpMdlLnkEntities = bpMdlLnkEntities;
	}

	@Override
	public List<IBpAreqLnkEntity> getBpAreqLnkEntities() {
		return bpArepLnkEntities;
	}

	@Override
	public void setBpAreqLnkEntities(List<IBpAreqLnkEntity> bpAreqLnkEntities) {
		this.bpArepLnkEntities = bpAreqLnkEntities;
	}

}
