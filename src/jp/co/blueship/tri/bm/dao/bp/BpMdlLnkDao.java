package jp.co.blueship.tri.bm.dao.bp;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bp.constants.BpMdlLnkItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build package module link DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BpMdlLnkDao extends JdbcBaseDao<IBpMdlLnkEntity> implements IBpMdlLnkDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BP_MDL_LNK;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBpMdlLnkEntity entity ) {
		builder
			.append(BpMdlLnkItems.bpId, entity.getBpId(), true)
			.append(BpMdlLnkItems.mdlNm, entity.getMdlNm(), true)
			.append(BpMdlLnkItems.mdlVerTag, entity.getMdlVerTag())
			.append(BpMdlLnkItems.bpCloseRev, entity.getBpCloseRev())
			.append(BpMdlLnkItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BpMdlLnkItems.regTimestamp, entity.getRegTimestamp())
			.append(BpMdlLnkItems.regUserId, entity.getRegUserId())
			.append(BpMdlLnkItems.regUserNm, entity.getRegUserNm())
			.append(BpMdlLnkItems.updTimestamp, entity.getUpdTimestamp())
			.append(BpMdlLnkItems.updUserId, entity.getUpdUserId())
			.append(BpMdlLnkItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBpMdlLnkEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBpMdlLnkEntity entity = new BpMdlLnkEntity();

		entity.setBpId( rs.getString(BpMdlLnkItems.bpId.getItemName()) );
		entity.setMdlNm( rs.getString(BpMdlLnkItems.mdlNm.getItemName()) );
		entity.setMdlVerTag( rs.getString(BpMdlLnkItems.mdlVerTag.getItemName()) );
		entity.setBpCloseRev( rs.getString(BpMdlLnkItems.bpCloseRev.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BpMdlLnkItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BpMdlLnkItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BpMdlLnkItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BpMdlLnkItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BpMdlLnkItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BpMdlLnkItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BpMdlLnkItems.updUserNm.getItemName()) );

		return entity;
	}

}
