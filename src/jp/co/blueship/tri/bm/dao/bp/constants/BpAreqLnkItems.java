package jp.co.blueship.tri.bm.dao.bp.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the build package asset request link entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum BpAreqLnkItems implements ITableItem {
	bpId("bp_id"),
	areqId("areq_id"),
	pjtId("pjt_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private BpAreqLnkItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
