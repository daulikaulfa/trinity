package jp.co.blueship.tri.bm.dao.taskflow.eb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;


public class TaskTargetEntity implements ITaskTargetEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -8913121478805349264L;

	private String name = null ;
	private String doSimulate = null ;
	private Integer sequenceNo = null ;
	private String forceExecute = StatusFlg.off.value() ;
    private String[] img = null;

	private List<ITaskTaskTypeEntity> task = new ArrayList<ITaskTaskTypeEntity>() ;
	private List<ITaskResultTypeEntity> result = new ArrayList<ITaskResultTypeEntity>() ;


	public String getName() {
		return name ;
	}
	public void setName( String value ) {
		name = value ;
	}

	public String getDoSimulate() {
		return doSimulate ;
	}
	public void setDoSimulate( String value ) {
		doSimulate = value ;
	}

	public Integer getSequenceNo() {
		return sequenceNo ;
	}
	public void setSequenceNo( Integer value ) {
		sequenceNo = value;
	}
	public ITaskTaskTypeEntity[] getTask() {
		return task.toArray( new ITaskTaskTypeEntity[0] );
	}
	public void setTask(ITaskTaskTypeEntity[] task) {
		this.task.clear();
		this.addTask( task );
	}
	public void addTask(ITaskTaskTypeEntity[] task) {
		this.task.addAll( FluentList.from(task).asList() );
	}
	/*public void addTask(ITaskTaskTypeEntity task) {
		this.task.addAll( FluentList.from().asList() new ITaskTaskTypeEntity[] { task } ) );
	}*/

	public String[] getImg() {
		return img;
	}
	public void setImg(String[] img) {
		this.img = img;
	}

	public ITaskResultTypeEntity[] getTaskResult() {
		return result.toArray( new ITaskResultTypeEntity[ 0 ] ) ;
	}
	public void setTaskResult( ITaskResultTypeEntity[] result) {
		this.result.clear();
		this.addTaskResult( result );
	}
	public void addTaskResult(ITaskResultTypeEntity[] result) {
		this.result.addAll( FluentList.from(result).asList() );
	}
	public void addTaskResult(ITaskResultTypeEntity result) {
		this.result.add( result );
	}
	public String getForceExecute() {
		return forceExecute;
	}
	public void setForceExecute(String forceExecute) {
		this.forceExecute = forceExecute;
	}

}
