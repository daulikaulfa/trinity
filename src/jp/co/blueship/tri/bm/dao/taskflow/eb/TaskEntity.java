package jp.co.blueship.tri.bm.dao.taskflow.eb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;


/**
 *
 * @version V3L10.01
 *
 * @version V3L12.01
 * @author Yusna Marlinai
 */
public class TaskEntity implements ITaskEntity {

	private static final long serialVersionUID = 1L;

	private String description = null;
	private String taskFlowId = null;
	private List<ITaskPropertyEntity> property = new ArrayList<ITaskPropertyEntity>();
	private List<ITaskTargetEntity> target = new ArrayList<ITaskTargetEntity>();


	public String getDescription() {
		return description;
	}

	public void setDescription( String value ) {
		description = value;
	}

	public String getTaskFlowId() {
		return taskFlowId;
	}

	public void setTaskFlowId( String value ) {
		taskFlowId = value;
	}

	public ITaskPropertyEntity[] getProperty() {
		return property.toArray( new ITaskPropertyEntity[0] );
	}
	public void setProperty( ITaskPropertyEntity[] object ) {
		property = FluentList.from(object).asList();
	}

	public ITaskTargetEntity[] getTarget() {
		return target.toArray( new ITaskTargetEntity[0] );
	}
	public void setTarget( ITaskTargetEntity[] object ) {
		target = FluentList.from(object).asList();
	}

	public ITaskPropertyEntity newPropertyEntity() {
		return new TaskPropertyEntity();
	}
	public void addProperty(ITaskPropertyEntity object) {
		property.add( object );
	}
	public ITaskTargetEntity newTargetEntity() {
		return new TaskTargetEntity();
	}
	public void addTarget(ITaskTargetEntity object) {
		target.add( object );
	}

}
