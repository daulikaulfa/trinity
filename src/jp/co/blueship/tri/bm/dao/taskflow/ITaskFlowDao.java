package jp.co.blueship.tri.bm.dao.taskflow;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the task flow DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowDao extends IJdbcDao<ITaskFlowEntity> {

}
