package jp.co.blueship.tri.bm.dao.taskflow.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the task flow resource entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum TaskFlowResItems implements ITableItem {
	dataId("data_id"),
	taskFlowId("task_flow_id"),
	lotId("lot_id"),
	task("task"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private TaskFlowResItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
