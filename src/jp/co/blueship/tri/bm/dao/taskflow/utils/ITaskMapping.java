package jp.co.blueship.tri.bm.dao.taskflow.utils;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;

/**
 * 個々のタスク部品のXML定義とエンティティとのマッピングを行うインタフェースです。
 * <br>新規にタスクを追加した場合は、このインタフェースを実装したクラスを用意します。
 * <br>
 *
 */
public interface ITaskMapping extends java.io.Serializable {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param src 複写元のXMLBeans
	 * @param dest 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( TargetType src, ITaskTargetEntity dest );

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param src 複写元のDBのEntity
	 * @param dest 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans( ITaskTargetEntity src, TargetType dest );

}
