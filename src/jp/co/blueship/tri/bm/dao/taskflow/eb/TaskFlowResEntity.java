package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * task flow response entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class TaskFlowResEntity extends EntityFooter implements ITaskFlowResEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * data-ID
	 */
	public String dataId = null;
	/**
	 * task flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * lot-ID
	 */
	public String lotId = null;
	/**
	 * task
	 */
	public ITaskEntity task = null;


	/**
	 * data-IDを取得します。
	 * @return data-ID
	 */
	public String getDataId() {
	    return dataId;
	}
	/**
	 * data-IDを設定します。
	 * @param dataId data-ID
	 */
	public void setDataId(String dataId) {
	    this.dataId = dataId;
	}
	/**
	 * task flow-IDを取得します。
	 * @return task flow-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}
	/**
	 * task flow-IDを設定します。
	 * @param taskFlowId task flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	}
	/**
	 * lot-IDを取得します。
	 * @return lot-ID
	 */
	public String getLotId() {
	    return lotId;
	}
	/**
	 * lot-IDを設定します。
	 * @param lotId lot-ID
	 */
	public void setLotId(String lotId) {
	    this.lotId = lotId;
	}
	/**
	 * taskを取得します。
	 * @return task
	 */
	public ITaskEntity getTask() {
	    return task;
	}
	/**
	 * taskを設定します。
	 * @param task task
	 */
	public void setTask(ITaskEntity task) {
	    this.task = task;
	}

}
