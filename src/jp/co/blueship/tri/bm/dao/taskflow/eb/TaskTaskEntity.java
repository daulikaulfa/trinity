package jp.co.blueship.tri.bm.dao.taskflow.eb;



/**
 * タスクのメタ情報です。
 *
 */
public abstract class TaskTaskEntity implements ITaskTaskTypeEntity {
	/**
	 * 変更した場合、手でインクリメントすること
	 */
	private static final long serialVersionUID = 2L;
	
	/**
	 * タスクシーケンス
	 */
	private String sequence = null;
	/**
	 * タスクＩＤ
	 */
	private String taskId = null;
	
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskTaskTypeEntity#getSequence()
	 */
	public String getSequence() {
		return sequence;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskTaskTypeEntity#setSequence(java.lang.String)
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskTaskTypeEntity#getTaskId()
	 */
	public String getTaskId() {
		return taskId;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskTaskTypeEntity#setTaskId(java.lang.String)
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	
}
