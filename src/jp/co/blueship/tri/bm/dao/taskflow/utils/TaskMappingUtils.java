package jp.co.blueship.tri.bm.dao.taskflow.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTargetEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Resource;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType.Property;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType.Target;

/**
 * XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskMappingUtils extends TriXmlMappingUtils {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @param mappings 追加タスクマッピング
	 * @return マッピング結果を戻します。
	 */
	public final ITaskEntity mapXMLBeans2DB( TaskRecType x , ITaskEntity d, List<ITaskMapping> mappings ) {

		copyProperties( x, d );

		//プロパティ
		Property[] properties = x.getPropertyArray();
		if ( null != properties ) {
			List<TaskPropertyEntity> propertyList = new ArrayList<TaskPropertyEntity>();

			for ( Property property : x.getPropertyArray() ) {
				TaskPropertyEntity propertyEntity = new TaskPropertyEntity();
				copyProperties( property, propertyEntity );

				propertyList.add( propertyEntity );
			}
			TaskPropertyEntity[] property = propertyList.toArray( new TaskPropertyEntity[ 0 ] );
			d.setProperty( property );
		}

		//業務シーケンス
		if( null != x.getTargetArray() ) {
			List<ITaskTargetEntity> targetList = new ArrayList<ITaskTargetEntity>();
			for ( Target target : x.getTargetArray() ) {
				ITaskTargetEntity targetEntity = d.newTargetEntity();

				copyProperties( target, targetEntity );

				Resource img = target.getResource();
				if ( null != img ) {
					String[] imgs = new String[IBldTimelineAgentEntity.img.values().length];
					imgs[ IBldTimelineAgentEntity.img.off.getValue() ] = img.getOff();
					imgs[ IBldTimelineAgentEntity.img.active.getValue() ] = img.getActive();
					imgs[ IBldTimelineAgentEntity.img.error.getValue() ] = img.getError();

					targetEntity.setImg( imgs );
				} else {
					targetEntity.setImg( new String[IBldTimelineAgentEntity.img.values().length] );
				}

				//タスク
				for ( ITaskMapping mapping : mappings ) {
					mapping.mapXMLBeans2DB( target, targetEntity );
				}

				//シーケンス順にソートする
				ITaskTaskTypeEntity[] entitys = targetEntity.getTask();
				Arrays.sort( entitys, new Comparator<ITaskTaskTypeEntity>() {
								public int compare(ITaskTaskTypeEntity param1, ITaskTaskTypeEntity param2) {
									Integer para1 = Integer.valueOf(param1.getSequence());
									Integer para2 = Integer.valueOf(param2.getSequence());

									return para1.compareTo(para2);
								}
							}  );
				targetEntity.setTask( entitys );

				targetList.add( targetEntity );
			}


			//ターゲット順にソートする
			TaskTargetEntity[] target = targetList.toArray( new TaskTargetEntity[ 0 ] );
			Arrays.sort( target, new Comparator<TaskTargetEntity>() {
				public int compare(TaskTargetEntity param1, TaskTargetEntity param2) {
					Integer para1 = param1.getSequenceNo();
					Integer para2 = param2.getSequenceNo();

					return para1.compareTo(para2);
				}
			}  );

			d.setTarget( target );
		}


		return d;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @param mappings 追加タスクマッピング
	 * @return マッピング結果を戻します。
	 */
	public final TaskRecType mapDB2XMLBeans( ITaskEntity d, TaskRecType x, List<ITaskMapping> mappings ) {

		copyProperties(d, x);

		//プロパティ
		if ( null != d.getProperty() && 0 < d.getProperty().length ) {
			for( ITaskPropertyEntity propertyEntity : d.getProperty() ) {
				Property property = x.addNewProperty();
				copyProperties(propertyEntity, property);
			}
		}

		//業務シーケンス
		if ( null != d.getTarget() && 0 < d.getTarget().length ) {
			ITaskTargetEntity[] targets = d.getTarget();
			for( ITaskTargetEntity targetEntity : targets ) {
				Target target = x.addNewTarget();
				copyProperties(targetEntity, target);

				if ( null != targetEntity.getImg() &&
					 0 < targetEntity.getImg().length ) {

					Resource img = target.addNewResource();

					img.setOff( targetEntity.getImg()[IBldTimelineAgentEntity.img.off.getValue()] );
					img.setActive( targetEntity.getImg()[IBldTimelineAgentEntity.img.active.getValue()] );
					img.setError( targetEntity.getImg()[IBldTimelineAgentEntity.img.error.getValue()] );
				}

				//タスク
				for ( ITaskMapping mapping : mappings ) {
					mapping.mapDB2XMLBeans( targetEntity, target );
				}
			}
		}
		return x;
	}

}
