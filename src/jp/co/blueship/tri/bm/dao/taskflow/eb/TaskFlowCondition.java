package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;

/**
 * The SQL condition of the task flow entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class TaskFlowCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_TASK_FLOW;

	/**
	 * task-flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * task
	 */
	public String task = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(TaskFlowItems.delStsId, StatusFlg.off.parseBoolean() );
	}

	public TaskFlowCondition(){
		super(attr);
	}

	/**
	 * task-flow-IDを取得します。
	 * @return build-srv-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}

	/**
	 * task-flow-IDを設定します。
	 * @param taskFlowId task-flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	    super.append(TaskFlowItems.taskFlowId, taskFlowId );
	}

	/**
	 * taskを取得します。
	 * @return task
	 */
	public String getTask() {
	    return task;
	}

	/**
	 * taskを設定します。
	 * @param task task
	 */
	public void setTask(String task) {
	    this.task = task;
	    super.append(TaskFlowItems.task, task );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( TaskFlowItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

}