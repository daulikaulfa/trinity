package jp.co.blueship.tri.bm.dao.taskflow;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowItems;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowResItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.dao.taskflow.utils.TaskMappingUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument.Task;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType;

/**
 * The implements of the task flow resource DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class TaskFlowResDao extends JdbcBaseDao<ITaskFlowResEntity> implements ITaskFlowResDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private static final String LS_NAMESPACES_KEY = "";
    private static final String LS_NAMESPACES_VAL = "http://www.blueship.co.jp/tri/fw/schema/beans/task";

    private XmlOptions opt = new XmlOptions();
    private List<ITaskMapping> mappings = new ArrayList<ITaskMapping>();

	/**
	 * DAOを生成します。
	 */
	public TaskFlowResDao() {
		super();
		this.addNameSpace(LS_NAMESPACES_KEY, LS_NAMESPACES_VAL);
	}

	/**
	 * XML名前空間を指定します。
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	private void addNameSpace( String key, String nameSpace ) {
		Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
		xmlNamespaceMap.put(key, nameSpace);

		opt.setLoadSubstituteNamespaces( xmlNamespaceMap );
	}

	public void setTaskMapping(List<ITaskMapping> mappings) {
		this.mappings = mappings;
	}
	public List<ITaskMapping> getTaskMapping() {
		return this.mappings;
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_TASK_FLOW_RES;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ITaskFlowResEntity entity ) {
		TaskDocument doc = TaskDocument.Factory.newInstance();
		TaskRecType xml = doc.addNewTask();

		xml = new TaskMappingUtils().mapDB2XMLBeans( entity.getTask(), xml, mappings );

		builder
			.append(TaskFlowResItems.dataId, entity.getDataId(), true)
			.append(TaskFlowResItems.taskFlowId, entity.getTaskFlowId(), true)
			.append(TaskFlowResItems.lotId, entity.getLotId())
			.append(TaskFlowResItems.task, TriXmlUtils.replaceExcessString(doc))
			.append(TaskFlowResItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(TaskFlowResItems.regTimestamp, entity.getRegTimestamp())
			.append(TaskFlowResItems.regUserId, entity.getRegUserId())
			.append(TaskFlowResItems.regUserNm, entity.getRegUserNm())
			.append(TaskFlowResItems.updTimestamp, entity.getUpdTimestamp())
			.append(TaskFlowResItems.updUserId, entity.getUpdUserId())
			.append(TaskFlowResItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ITaskFlowResEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		ITaskFlowResEntity entity = new TaskFlowResEntity();

		entity.setDataId( rs.getString(TaskFlowResItems.dataId.getItemName()) );
		entity.setTaskFlowId( rs.getString(TaskFlowResItems.taskFlowId.getItemName()) );
		entity.setLotId( rs.getString(TaskFlowResItems.lotId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(TaskFlowResItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(TaskFlowResItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(TaskFlowResItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(TaskFlowResItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(TaskFlowResItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(TaskFlowResItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(TaskFlowResItems.updUserNm.getItemName()) );

		entity.setTask( this.mapping(rs.getString(TaskFlowItems.task.getItemName()), row, opt) );

		return entity;
	}

	/**
	 * XML文書を任意のオブジェクトにマッピング（Unmarshall）します。
	 *
	 * @param xmlString XML文書の文字列
	 * @param row 検索結果行
	 * @param xmlOptions 名前空間を示すパラメータ
	 * @return マッピングしたオブジェクトを戻します
	 * @throws SQLException  DAOの例外
	 */
	private final ITaskEntity mapping( String xmlString, int row, XmlOptions xmlOptions ) throws SQLException {
		try {
			TaskDocument doc = TaskDocument.Factory.parse( xmlString, xmlOptions );

			Task xml = doc.getTask();
			ITaskEntity entity = new TaskEntity();

			entity = new TaskMappingUtils().mapXMLBeans2DB( xml, entity, mappings );

			return entity;

		} catch (XmlException e) {
			throw new TriJdbcDaoException( SmMessageId.SM005017S, e, this.getTableAttribute().name() );
		}
	}

    @Override
	public int insert( ITaskFlowResEntity entity ) throws TriJdbcDaoException {
    	return super.insert(entity);
    }

    @Override
	public int[] insert( List<ITaskFlowResEntity> entities ) throws TriJdbcDaoException {
    	return super.insert(entities);
    }

    @Override
	public int update( ITaskFlowResEntity entity ) throws TriJdbcDaoException {
    	return super.update(entity);
    }

    @Override
	public int[] update( List<ITaskFlowResEntity> entities ) throws TriJdbcDaoException {
    	return super.update(entities);
    }

    @Override
	public int update( ISqlCondition condition, ITaskFlowResEntity entity ) throws TriJdbcDaoException {
    	return super.update(condition, entity);
    }

    @Override
	public int delete( ISqlCondition condition ) throws TriJdbcDaoException {
    	return super.delete(condition);
    }

}
