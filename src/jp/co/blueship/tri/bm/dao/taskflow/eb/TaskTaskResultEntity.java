package jp.co.blueship.tri.bm.dao.taskflow.eb;



/**
 * タスク実行結果のメタ情報です。
 *
 */
public abstract class TaskTaskResultEntity implements ITaskResultTypeEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * タスクシーケンス
	 */
	private String sequence = null;
	/**
	 * タスクＩＤ
	 */
	private String taskId = null;
	/**
	 * 実行結果
	 */
	private String code = null ;

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#getSequence()
	 */
	public String getSequence() {
		return sequence;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#setSequence(java.lang.String)
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#getTaskId()
	 */
	public String getTaskId() {
		return taskId;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#setTaskId(java.lang.String)
	 */
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#getCode()
	 */
	public String getCode() {
		return code;
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.task.db.entity.ITaskResultTypeEntity#setCode(java.lang.String)
	 */
	public void setCode(String code) {
		this.code = code;
	}

}
