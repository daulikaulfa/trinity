package jp.co.blueship.tri.bm.dao.taskflow;

import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the task flow resource DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowResDao extends IJdbcDao<ITaskFlowResEntity> {

	public List<ITaskMapping> getTaskMapping();
}
