package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the task flow response entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowResEntity extends IEntityFooter {

	public String getDataId();
	public void setDataId(String dataId);

	public String getTaskFlowId();
	public void setTaskFlowId(String taskFlowId);

	public String getLotId();
	public void setLotId(String lotId);

	public ITaskEntity getTask();
	public void setTask(ITaskEntity task);

}
