package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskResultTypeEntity extends IEntity {

	/**
	 * 実行順序を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getSequence();
	/**
	 * 実行順序を設定します。
	 *
	 * @param value 実行順序
	 */
	public void setSequence( String value );
	/**
	 * 実行結果を取得します。
	 * @see jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getCode();
	/**
	 * 実行結果を設定します。
	 * @see jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode
	 *
	 * @param value 実行結果。
	 */
	public void setCode( String value );

	/**
	 * タスクを一意に識別するIDを取得します。
	 *
	 * @return タスクID
	 */
	public String getTaskId();

	/**
	 * タスクを一意に識別するIDを設定します。
	 *
	 * @param taskId タスクID
	 */
	public void setTaskId( String taskId );

}
