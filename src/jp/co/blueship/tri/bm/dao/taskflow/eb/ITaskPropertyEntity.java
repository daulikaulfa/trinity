package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskPropertyEntity extends IEntity {
	
	
	public String getName();
	public void setName( String value );
	
	public String getValue() ;
	public void setValue( String value ) ;
}
