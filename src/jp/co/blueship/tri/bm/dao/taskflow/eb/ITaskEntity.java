package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskEntity extends IEntity {

	/**
	 * 記述内容を取得します。
	 *
	 * @return 取得した情報を戻します
	 */
	public String getDescription();
	/**
	 * 記述内容を設定します
	 *
	 * @param value 記述内容
	 */
	public void setDescription( String value );

	/**
	 * プロパティを取得します。
	 *
	 * @return 取得した情報を戻します
	 */
	public ITaskPropertyEntity[] getProperty() ;
	/**
	 * プロパティを設定します
	 *
	 * @param value プロパティ
	 */
	public void setProperty( ITaskPropertyEntity[] object ) ;

	/**
	 * 業務シーケンスを取得します。
	 *
	 * @return 取得した情報を戻します
	 */
	public ITaskTargetEntity[] getTarget();
	/**
	 * 業務シーケンスを設定します
	 *
	 * @param value 業務シーケンス
	 */
	public void setTarget( ITaskTargetEntity[] object );

	/**
	 * プロパティ情報の新規インスタンスを生成します。
	 *
	 * @return 生成した情報を戻します
	 */
	public ITaskPropertyEntity newPropertyEntity();
	/**
	 * このタスクにプロパティ情報を追加します。
	 *
	 * @param object プロパティ情報
	 */
	public void addProperty( ITaskPropertyEntity object );
	/**
	 * 業務シーケンスの新規インスタンスを生成します。
	 *
	 * @return 生成した情報を戻します
	 */
	public ITaskTargetEntity newTargetEntity();
	/**
	 * このタスクに業務シーケンスを追加します。
	 *
	 * @param object 業務シーケンス
	 */
	public void addTarget( ITaskTargetEntity object );


}
