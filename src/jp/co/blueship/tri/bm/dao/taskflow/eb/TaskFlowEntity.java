package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * task flow entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class TaskFlowEntity extends EntityFooter implements ITaskFlowEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * task flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * task
	 */
	public ITaskEntity task = null;

	/**
	 * task flow-IDを取得します。
	 * @return task flow-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}
	/**
	 * task flow-IDを設定します。
	 * @param taskFlowId task flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	}
	/**
	 * taskを取得します。
	 * @return task
	 */
	public ITaskEntity getTask() {
	    return task;
	}
	/**
	 * taskを設定します。
	 * @param task task
	 */
	public void setTask(ITaskEntity task) {
	    this.task = task;
	}

}
