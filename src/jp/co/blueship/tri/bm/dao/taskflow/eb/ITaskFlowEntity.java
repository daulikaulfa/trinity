package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the task flow entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowEntity extends IEntityFooter {

	public String getTaskFlowId();
	public void setTaskFlowId(String taskFlowId);

	public ITaskEntity getTask();
	public void setTask(ITaskEntity task);

}
