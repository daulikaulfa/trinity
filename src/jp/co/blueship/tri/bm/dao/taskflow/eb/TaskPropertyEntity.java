package jp.co.blueship.tri.bm.dao.taskflow.eb;


public class TaskPropertyEntity implements ITaskPropertyEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 3108731258035175671L;

	private String name = null ;
	private String value = null ;

	public String getName() {
		return name ;
	}
	public void setName( String value ) {
		name = value ;
	}

	public String getValue() {
		return value ;
	}
	public void setValue( String value ) {
		this.value = value ;
	}
}
