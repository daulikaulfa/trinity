package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスク・業務シーケンス情報エンティティのインタフェースです。
 *
 */
public interface ITaskTargetEntity extends IEntity {

	

	public enum img {
		off( 0 ),
		active( 1 ),
		error( 2 );

		private int value = 0;

		private img( int value ) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}

	public String getName();
	public void setName( String value );

	public String getDoSimulate();
	public void setDoSimulate( String value );

	public String getForceExecute();
	public void setForceExecute( String value );

	public Integer getSequenceNo() ;
	public void setSequenceNo( Integer value ) ;

	public ITaskTaskTypeEntity[] getTask() ;
	public void setTask(ITaskTaskTypeEntity[] task) ;
	public void addTask(ITaskTaskTypeEntity[] task) ;
	//public void addTask(ITaskTaskTypeEntity task) ;

	public String[] getImg() ;
	public void setImg(String[] img) ;



	public ITaskResultTypeEntity[] getTaskResult() ;
	public void setTaskResult( ITaskResultTypeEntity[] result ) ;
	public void addTaskResult( ITaskResultTypeEntity[] result ) ;
	public void addTaskResult( ITaskResultTypeEntity result ) ;
}
