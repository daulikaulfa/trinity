package jp.co.blueship.tri.bm.dao.taskflow.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskTaskTypeEntity extends IEntity {

	/**
	 * シーケンス番号を取得します。
	 *
	 * @return シーケンス番号
	 */
	public String getSequence();

	/**
	 * シーケンス番号を設定します。
	 *
	 * @param value シーケンス番号
	 */
	public void setSequence( String value );

	/**
	 * タスクを一意に識別するIDを取得します。
	 *
	 * @return タスクID
	 */
	public String getTaskId();

	/**
	 * タスクを一意に識別するIDを設定します。
	 *
	 * @param taskId タスクID
	 */
	public void setTaskId( String taskId );

}
