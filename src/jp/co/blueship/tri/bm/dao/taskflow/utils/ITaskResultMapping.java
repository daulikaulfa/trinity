package jp.co.blueship.tri.bm.dao.taskflow.utils;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * 個々のタスク部品のXML定義とエンティティとのマッピングを行うインタフェースです。
 * <br>新規にタスクを追加した場合は、このインタフェースを実装したクラスを用意します。
 * <br>
 *
 * @author Yukihiro Eguchi
 *
 */
public interface ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param src 複写元のXMLBeans
	 * @param dest 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest );

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param src 複写元のDBのEntity
	 * @param dest 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans( ITaskTargetEntity src, Result dest );

}
