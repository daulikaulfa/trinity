package jp.co.blueship.tri.bm.dao.taskflow;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.xmlbeans.XmlException;
import org.apache.xmlbeans.XmlOptions;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.dao.taskflow.utils.TaskMappingUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument;
import jp.co.blueship.tri.fw.schema.beans.task.TaskDocument.Task;
import jp.co.blueship.tri.fw.schema.beans.task.TaskRecType;

/**
 * The implements of the task flow DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class TaskFlowDao extends JdbcBaseDao<ITaskFlowEntity> implements ITaskFlowDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	private static final String LS_NAMESPACES_KEY = "";
    private static final String LS_NAMESPACES_VAL = "http://www.blueship.co.jp/tri/fw/schema/beans/task";

    private XmlOptions opt = new XmlOptions();
    public List<ITaskMapping> mappings = new ArrayList<ITaskMapping>();

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * DAOを生成します。
	 */
	public TaskFlowDao() {
		super();
		this.addNameSpace(LS_NAMESPACES_KEY, LS_NAMESPACES_VAL);
	}

	/**
	 * XML名前空間を指定します。
	 * @param key キー値
	 * @param nameSpace 名前空間
	 */
	private void addNameSpace( String key, String nameSpace ) {
		Map<String, String> xmlNamespaceMap = new HashMap<String, String>();
		xmlNamespaceMap.put(key, nameSpace);

		opt.setLoadSubstituteNamespaces( xmlNamespaceMap );
	}

	public void setTaskMapping(List<ITaskMapping> mappings) {
		this.mappings = mappings;
	}

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_TASK_FLOW;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ITaskFlowEntity entity ) {
		TaskDocument doc = TaskDocument.Factory.newInstance();
		TaskRecType xml = doc.addNewTask();

		xml = new TaskMappingUtils().mapDB2XMLBeans( entity.getTask(), xml, mappings );

		builder
			.append(TaskFlowItems.taskFlowId, entity.getTaskFlowId(), true)
			.append(TaskFlowItems.task, TriXmlUtils.replaceExcessString(doc))
			.append(TaskFlowItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(TaskFlowItems.regTimestamp, entity.getRegTimestamp())
			.append(TaskFlowItems.regUserId, entity.getRegUserId())
			.append(TaskFlowItems.regUserNm, entity.getRegUserNm())
			.append(TaskFlowItems.updTimestamp, entity.getUpdTimestamp())
			.append(TaskFlowItems.updUserId, entity.getUpdUserId())
			.append(TaskFlowItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ITaskFlowEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		ITaskFlowEntity entity = new TaskFlowEntity();

		entity.setTaskFlowId( rs.getString(TaskFlowItems.taskFlowId.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(TaskFlowItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(TaskFlowItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(TaskFlowItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(TaskFlowItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(TaskFlowItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(TaskFlowItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(TaskFlowItems.updUserNm.getItemName()) );

		entity.setTask( this.mapping(rs.getString(TaskFlowItems.task.getItemName()), row, opt) );

		return entity;
	}

	/**
	 * XML文書を任意のオブジェクトにマッピング（Unmarshall）します。
	 *
	 * @param xmlString XML文書の文字列
	 * @param row 検索結果行
	 * @param xmlOptions 名前空間を示すパラメータ
	 * @return マッピングしたオブジェクトを戻します
	 * @throws SQLException  DAOの例外
	 */
	private final ITaskEntity mapping( String xmlString, int row, XmlOptions xmlOptions ) throws SQLException {
		try {
			TaskDocument doc = TaskDocument.Factory.parse( xmlString, xmlOptions );

			Task xml = doc.getTask();
			ITaskEntity entity = new TaskEntity();

			entity = new TaskMappingUtils().mapXMLBeans2DB( xml, entity, mappings );

			return entity;

		} catch (XmlException e) {
			LogHandler.fatal( log , xmlString , e );
			throw new TriJdbcDaoException( SmMessageId.SM005017S, e, this.getTableAttribute().name() );
		}
	}

}
