package jp.co.blueship.tri.bm.dao.v2.task.shun;

/**
 * タスクオーダー情報の要素です。
 *
 */
public class TaskEntityElements {
	
	

	//スキーマ定義を文字列置換すると、作成が容易になります。
	public static final String ROOT = "/task";
	
	//属性
    public static final String workflowNo = "/@workflowNo" ;
    public static final String targetName = "/target/@name" ;
    public static final String sequenceNo = "/target/@sequenceNo" ;

    //タグ
    public static final String description = "/description" ;

}
