package jp.co.blueship.tri.bm.dao.bldtimeline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bldtimeline.constants.BldTimelineItems;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build time line DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BldTimelineDao extends JdbcBaseDao<IBldTimelineEntity> implements IBldTimelineDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BLD_TIMELINE;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBldTimelineEntity entity ) {
		builder

			.append(BldTimelineItems.bldEnvId, entity.getBldEnvId(), true)
			.append(BldTimelineItems.bldLineNo, entity.getBldLineNo(), true)
			.append(BldTimelineItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BldTimelineItems.regTimestamp, entity.getRegTimestamp())
			.append(BldTimelineItems.regUserId, entity.getRegUserId())
			.append(BldTimelineItems.regUserNm, entity.getRegUserNm())
			.append(BldTimelineItems.updTimestamp, entity.getUpdTimestamp())
			.append(BldTimelineItems.updUserId, entity.getUpdUserId())
			.append(BldTimelineItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBldTimelineEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBldTimelineEntity entity = new BldTimelineEntity();

		entity.setBldEnvId( rs.getString(BldTimelineItems.bldEnvId.getItemName()) );
		entity.setBldLineNo( rs.getInt(BldTimelineItems.bldLineNo.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BldTimelineItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BldTimelineItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BldTimelineItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BldTimelineItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BldTimelineItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BldTimelineItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BldTimelineItems.updUserNm.getItemName()) );

		return entity;
	}

}
