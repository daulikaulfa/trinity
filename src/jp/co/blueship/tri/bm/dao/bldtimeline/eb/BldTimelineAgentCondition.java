package jp.co.blueship.tri.bm.dao.bldtimeline.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.bldtimeline.constants.BldTimelineAgentItems;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the build time line agent entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BldTimelineAgentCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_BLD_TIMELINE_AGENT;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build line number
	 */
	public Integer bldLineNo = null;
	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * task flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * target sequence number
	 */
	public Integer targetSeqNo = null;
	/**
	 * target status ID off
	 */
	public String targetStsIdOff = null;
	/**
	 * target status ID active
	 */
	public String targetStsIdActive = null;
	/**
	 * target status ID error
	 */
	public String targetStsIdErr = null;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(BldTimelineAgentItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	/**
	 * registration user-ID
	 */
	public String regUserId = null;
	/**
	 * registration user-ID's
	 */
	public String[] regUserIds = null;
	/**
	 * registration user name
	 */
	public String regUserNm = null;
	/**
	 * registration time stamp
	 */
	public Timestamp regTimestamp = null;
	/**
	 * update user-ID
	 */
	public String updUserId = null;
	/**
	 * update user-ID's
	 */
	public String[] updUserIds = null;
	/**
	 * update user name
	 */
	public String updUserNm = null;
	/**
	 * update time stamp
	 */
	public Timestamp updTimestamp = null;

	public BldTimelineAgentCondition(){
		super(attr);
	}

	/**
	 * build-env-IDを取得します。
	 * @return build-env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build-env-IDを設定します。
	 * @param bldEnvId build-env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(BldTimelineAgentItems.bldEnvId, bldEnvId );
	}

	/**
	 * build-line-numberを取得します。
	 * @return build-line-number
	 */
	public Integer getBldLineNo() {
	    return bldLineNo;
	}

	/**
	 * build-line-numberを設定します。
	 * @param bldLineNo build-line-number
	 */
	public void setBldLineNo(Integer bldLineNo) {
	    this.bldLineNo = bldLineNo;
	    super.append(BldTimelineAgentItems.bldLineNo, bldLineNo );
	}

	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	    super.append(BldTimelineAgentItems.bldSrvId, bldSrvId );
	}
	/**
	 * task flow-IDを取得します。
	 * @return task flow-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}
	/**
	 * task flow-IDを設定します。
	 * @param taskFlowId task flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	    super.append(BldTimelineAgentItems.taskFlowId, taskFlowId );
	}
	/**
	 * target sequence numberを取得します。
	 * @return target sequence number
	 */
	public Integer getTargetSeqNo() {
	    return targetSeqNo;
	}
	/**
	 * target sequence numberを設定します。
	 * @param targetSeqNo target sequence number
	 */
	public void setTargetSeqNo(Integer targetSeqNo) {
	    this.targetSeqNo = targetSeqNo;
	    super.append(BldTimelineAgentItems.targetSeqNo, targetSeqNo );
	}

	/**
	 * target status ID offを取得します。
	 * @return targetStsIdOff
	 */
	public String getTargetStsIdOff() {
	    return targetStsIdOff;
	}

	/**
	 * target status ID offを設定します。
	 * @param targetStsIdOff
	 */
	public void setTargetStsIdOff(String targetStsIdOff) {
	    this.targetStsIdOff = targetStsIdOff;
	    super.append(BldTimelineAgentItems.targetStsIdOff, targetStsIdOff );
	}

	/**
	 * target status ID activeを取得します。
	 * @return targetStsId
	 */
	public String getTargetStsIdActive() {
	    return targetStsIdActive;
	}

	/**
	 * target status ID activeを設定します。
	 * @param targetStsIdActive
	 */
	public void setTargetStsIdActive(String targetStsIdActive) {
	    this.targetStsIdActive = targetStsIdActive;
	    super.append(BldTimelineAgentItems.targetStsIdActive, targetStsIdActive );
	}

	/**
	 * target status ID errorを取得します。
	 * @return targetStsIdErr
	 */
	public String getTargetStsIdErr() {
	    return targetStsIdErr;
	}

	/**
	 * target status ID errorを設定します。
	 * @param targetStsIdErr
	 */
	public void setTargetStsIdErr(String targetStsIdErr) {
	    this.targetStsIdErr = targetStsIdErr;
	    super.append(BldTimelineAgentItems.targetStsIdErr, targetStsIdErr );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( BldTimelineAgentItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

	/**
	 * registration user-IDを取得します。
	 * @return registration user-ID
	 */
	public String getRegUserId() {
	    return regUserId;
	}

	/**
	 * registration user-IDを設定します。
	 * @param regUserId registration user-ID
	 */
	public void setRegUserId(String regUserId) {
	    this.regUserId = regUserId;
	    super.append(BldTimelineAgentItems.regUserId, regUserId );
	}

	/**
	 * registration user-ID'sを取得します。
	 * @return registration user-ID's
	 */
	public String[] getRegUserIds() {
	    return regUserIds;
	}

	/**
	 * registration user-ID'sを設定します。
	 * @param regUserIds registration user-ID's
	 */
	public void setRegUserIds(String[] regUserIds) {
	    this.regUserIds = regUserIds;
	    super.append( BldTimelineAgentItems.regUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(regUserIds, BldTimelineAgentItems.regUserId.getItemName(), false, true, false) );
	}

	/**
	 * registration user nameを取得します。
	 * @return registration user name
	 */
	public String getRegUserNm() {
	    return regUserNm;
	}

	/**
	 * registration user nameを設定します。
	 * @param regUserNm registration user name
	 */
	public void setRegUserNm(String regUserNm) {
	    this.regUserNm = regUserNm;
	    super.append(BldTimelineAgentItems.regUserNm, regUserNm );
	}

	/**
	 * registration time stampを取得します。
	 * @return registration time stamp
	 */
	public Timestamp getRegTimestamp() {
	    return regTimestamp;
	}

	/**
	 * registration time stampを設定します。
	 * @param regTimestamp registration time stamp
	 */
	public void setRegTimestamp(Timestamp regTimestamp) {
	    this.regTimestamp = regTimestamp;
	    super.append(BldTimelineAgentItems.regTimestamp, regTimestamp );
	}

	/**
	 * update user-IDを取得します。
	 * @return update user-ID
	 */
	public String getUpdUserId() {
	    return updUserId;
	}

	/**
	 * update user-IDを設定します。
	 * @param updUserId update user-ID
	 */
	public void setUpdUserId(String updUserId) {
	    this.updUserId = updUserId;
	    super.append(BldTimelineAgentItems.updUserId, updUserId );
	}

	/**
	 * update user-ID'sを取得します。
	 * @return update user-ID's
	 */
	public String[] getUpdUserIds() {
	    return updUserIds;
	}

	/**
	 * update user-ID'sを設定します。
	 * @param updUserIds update user-ID's
	 */
	public void setUpdUserIds(String[] updUserIds) {
	    this.updUserIds = updUserIds;
	    super.append( BldTimelineAgentItems.updUserId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(updUserIds, BldTimelineAgentItems.updUserId.getItemName(), false, true, false) );
	}

	/**
	 * update user nameを取得します。
	 * @return update user name
	 */
	public String getUpdUserNm() {
	    return updUserNm;
	}

	/**
	 * update user nameを設定します。
	 * @param updUserNm update user name
	 */
	public void setUpdUserNm(String updUserNm) {
	    this.updUserNm = updUserNm;
	    super.append(BldTimelineAgentItems.updUserNm, updUserNm );
	}

	/**
	 * update time stampを取得します。
	 * @return update time stamp
	 */
	public Timestamp getUpdTimestamp() {
	    return updTimestamp;
	}

	/**
	 * update time stampを設定します。
	 * @param updTimestamp update time stamp
	 */
	public void setUpdTimestamp(Timestamp updTimestamp) {
	    this.updTimestamp = updTimestamp;
	    super.append(BldTimelineAgentItems.updTimestamp, updTimestamp );
	}
}