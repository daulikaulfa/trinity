package jp.co.blueship.tri.bm.dao.bldtimeline.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build time line entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BldTimelineEntity extends EntityFooter implements IBldTimelineEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build line number
	 */
	public Integer bldLineNo = null;

	// ↓SQL設定が必要
    private IBldTimelineAgentEntity[] line = null;

	public IBldTimelineAgentEntity[] getLine() {
		return line;
	}
	public void setLine(IBldTimelineAgentEntity[] value) {
		this.line = value;
	}
	public IBldTimelineAgentEntity newLineEntity() {
		return new BldTimelineAgentEntity();
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * build line numberを取得します。
	 * @return build line number
	 */
	public Integer getBldLineNo() {
	    return bldLineNo;
	}
	/**
	 * build line numberを設定します。
	 * @param bldLineNo build line number
	 */
	public void setBldLineNo(Integer bldLineNo) {
	    this.bldLineNo = bldLineNo;
	}

}
