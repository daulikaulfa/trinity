package jp.co.blueship.tri.bm.dao.bldtimeline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the build time line entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum BldTimelineItems implements ITableItem {
	bldEnvId("bld_env_id"),
	bldLineNo("bld_line_no"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private BldTimelineItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
