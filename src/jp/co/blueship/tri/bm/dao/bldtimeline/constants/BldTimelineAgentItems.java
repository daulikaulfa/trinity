package jp.co.blueship.tri.bm.dao.bldtimeline.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the build time line agent entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum BldTimelineAgentItems implements ITableItem {
	bldEnvId("bld_env_id"),
	bldLineNo("bld_line_no"),
	bldSrvId("bld_srv_id"),
	taskFlowId("task_flow_id"),
	targetSeqNo("target_seq_no"),
	targetStsIdOff("target_sts_id_off"),
	targetStsIdActive("target_sts_id_active"),
	targetStsIdErr("target_sts_id_err"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private BldTimelineAgentItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
