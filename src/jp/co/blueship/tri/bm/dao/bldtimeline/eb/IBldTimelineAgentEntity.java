package jp.co.blueship.tri.bm.dao.bldtimeline.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build time line agent entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldTimelineAgentEntity extends IEntityFooter {

	public enum img {
		off( 0 ),
		active( 1 ),
		error( 2 );

		private int value = 0;

		private img( int value ) {
			this.value = value;
		}

		public int getValue() {
			return this.value;
		}
	}

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public Integer getBldLineNo();
	public void setBldLineNo(Integer bldLineNo);

	public String getBldSrvId();
	public void setBldSrvId(String bldSrvId);

	public String getTaskFlowId();
	public void setTaskFlowId(String taskFlowId);

	public Integer getTargetSeqNo();
	public void setTargetSeqNo(Integer targetSeqNo);

	public String getTargetStsIdOff();
	public void setTargetStsIdOff(String targetStsIdOff);

	public String getTargetStsIdActive();
	public void setTargetStsIdActive(String targetStsIdActive);

	public String getTargetStsIdErr();
	public void setTargetStsIdErr(String targetStsIdErr);

}
