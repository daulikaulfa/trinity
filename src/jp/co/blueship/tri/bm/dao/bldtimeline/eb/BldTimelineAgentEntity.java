package jp.co.blueship.tri.bm.dao.bldtimeline.eb;

import jp.co.blueship.tri.fw.dao.orm.EntityFooter;

/**
 * build time line agent entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public class BldTimelineAgentEntity extends EntityFooter implements IBldTimelineAgentEntity {

	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build line number
	 */
	public Integer bldLineNo = null;
	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * task flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * target sequence number
	 */
	public Integer targetSeqNo = null;
	/**
	 * target status ID off
	 */
	public String targetStsIdOff = null;
	/**
	 * target status ID active
	 */
	public String targetStsIdActive = null;
	/**
	 * target status ID error
	 */
	public String targetStsIdErr = null;

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}
	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	}
	/**
	 * build line numberを取得します。
	 * @return build line number
	 */
	public Integer getBldLineNo() {
	    return bldLineNo;
	}
	/**
	 * build line numberを設定します。
	 * @param bldLineNo build line number
	 */
	public void setBldLineNo(Integer bldLineNo) {
	    this.bldLineNo = bldLineNo;
	}
	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}
	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	}
	/**
	 * task flow-IDを取得します。
	 * @return task flow-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}
	/**
	 * task flow-IDを設定します。
	 * @param taskFlowId task flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	}
	/**
	 * target sequence numberを取得します。
	 * @return target sequence number
	 */
	public Integer getTargetSeqNo() {
	    return targetSeqNo;
	}
	/**
	 * target sequence numberを設定します。
	 * @param targetSeqNo target sequence number
	 */
	public void setTargetSeqNo(Integer targetSeqNo) {
	    this.targetSeqNo = targetSeqNo;
	}
    /**
	 * target status ID offを取得します。
	 * @return targetStsIdOff
	 */
	public String getTargetStsIdOff() {
	    return targetStsIdOff;
	}
	/**
	 * target status ID offを設定します。
	 * @param targetStsIdOff
	 */
	public void setTargetStsIdOff(String targetStsIdOff) {
	    this.targetStsIdOff = targetStsIdOff;
	}
	/**
	 * target status ID activeを取得します。
	 * @return targetStsIdActive
	 */
	public String getTargetStsIdActive() {
	    return targetStsIdActive;
	}
	/**
	 * target status ID activeを設定します。
	 * @param targetStsIdActive
	 */
	public void setTargetStsIdActive(String targetStsIdActive) {
	    this.targetStsIdActive = targetStsIdActive;
	}
	/**
	 * target status ID errorを取得します。
	 * @return targetStsIdErr
	 */
	public String getTargetStsIdErr() {
	    return targetStsIdErr;
	}
	/**
	 * target status ID errorを設定します。
	 * @param targetStsIdErr
	 */
	public void setTargetStsIdErr(String targetStsIdErr) {
	    this.targetStsIdErr = targetStsIdErr;
	}
}
