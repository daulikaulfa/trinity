package jp.co.blueship.tri.bm.dao.bldtimeline;

import java.sql.ResultSet;
import java.sql.SQLException;

import jp.co.blueship.tri.bm.dao.bldtimeline.constants.BldTimelineAgentItems;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the build timeline agent DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class BldTimelineAgentDao extends JdbcBaseDao<IBldTimelineAgentEntity> implements IBldTimelineAgentDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 2L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_BLD_TIMELINE_AGENT;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, IBldTimelineAgentEntity entity ) {
		builder

			.append(BldTimelineAgentItems.bldEnvId, entity.getBldEnvId(), true)
			.append(BldTimelineAgentItems.bldLineNo, entity.getBldLineNo(), true)
			.append(BldTimelineAgentItems.bldSrvId, entity.getBldSrvId(), true)
			.append(BldTimelineAgentItems.taskFlowId, entity.getTaskFlowId())
			.append(BldTimelineAgentItems.targetSeqNo, entity.getTargetSeqNo())
			.append(BldTimelineAgentItems.targetStsIdOff, entity.getTargetStsIdOff())
			.append(BldTimelineAgentItems.targetStsIdActive, entity.getTargetStsIdActive())
			.append(BldTimelineAgentItems.targetStsIdErr, entity.getTargetStsIdErr())
			.append(BldTimelineAgentItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(BldTimelineAgentItems.regTimestamp, entity.getRegTimestamp())
			.append(BldTimelineAgentItems.regUserId, entity.getRegUserId())
			.append(BldTimelineAgentItems.regUserNm, entity.getRegUserNm())
			.append(BldTimelineAgentItems.updTimestamp, entity.getUpdTimestamp())
			.append(BldTimelineAgentItems.updUserId, entity.getUpdUserId())
			.append(BldTimelineAgentItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final IBldTimelineAgentEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		IBldTimelineAgentEntity entity = new BldTimelineAgentEntity();

		entity.setBldEnvId( rs.getString(BldTimelineAgentItems.bldEnvId.getItemName()) );
		entity.setBldLineNo( rs.getInt(BldTimelineAgentItems.bldLineNo.getItemName()) );
		entity.setBldSrvId( rs.getString(BldTimelineAgentItems.bldSrvId.getItemName()) );
		entity.setTaskFlowId( rs.getString(BldTimelineAgentItems.taskFlowId.getItemName()) );
		entity.setTargetSeqNo( rs.getInt(BldTimelineAgentItems.targetSeqNo.getItemName()) );
		entity.setTargetStsIdOff( rs.getString(BldTimelineAgentItems.targetStsIdOff.getItemName()) );
		entity.setTargetStsIdActive( rs.getString(BldTimelineAgentItems.targetStsIdActive.getItemName()) );
		entity.setTargetStsIdErr( rs.getString(BldTimelineAgentItems.targetStsIdErr.getItemName()) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(BldTimelineAgentItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(BldTimelineAgentItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(BldTimelineAgentItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(BldTimelineAgentItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(BldTimelineAgentItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(BldTimelineAgentItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(BldTimelineAgentItems.updUserNm.getItemName()) );

		return entity;
	}

}
