package jp.co.blueship.tri.bm.dao.bldtimeline;

import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the build time line DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldTimelineDao extends IJdbcDao<IBldTimelineEntity> {

}
