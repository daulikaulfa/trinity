package jp.co.blueship.tri.bm.dao.bldtimeline.eb;

import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the build time line entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface IBldTimelineEntity extends IEntityFooter {

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public Integer getBldLineNo();
	public void setBldLineNo(Integer bldLineNo);

	public IBldTimelineAgentEntity newLineEntity();
	public IBldTimelineAgentEntity[] getLine();
	public void setLine( IBldTimelineAgentEntity[] value );
}
