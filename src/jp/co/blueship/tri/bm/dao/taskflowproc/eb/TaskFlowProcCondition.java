package jp.co.blueship.tri.bm.dao.taskflowproc.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflowproc.constants.TaskFlowProcItems;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.psql.ConditionSupport;
import jp.co.blueship.tri.fw.dao.orm.psql.SqlFormatUtils;

/**
 * The SQL condition of the task flow entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class TaskFlowProcCondition extends ConditionSupport {
	/**
	 * The interface of the table properties.
	 */
	private static final ITableAttribute attr = BmTables.BM_TASK_FLOW_PROC;

	/**
	 * proc-ID
	 */
	public String procId = null;
	/**
	 * proc-ID's
	 */
	public String[] procIds = null;
	/**
	 * build srv-ID
	 */
	public String bldSrvId = null;
	/**
	 * build env-ID
	 */
	public String bldEnvId = null;
	/**
	 * build line number
	 */
	public Integer bldLineNo = null;
	/**
	 * task flow-ID
	 */
	public String taskFlowId = null;
	/**
	 * target sequence number
	 */
	public Integer targetSeqNo = null;
	/**
	 * process start time stamp
	 */
	public Timestamp procStTimestamp = null;
	/**
	 * process end time stamp
	 */
	public Timestamp procEndTimestamp = null;
	/**
	 * task sts-ID
	 */
	public String stsId = null;
	/**
	 * task msg-ID
	 */
	public String msgId = null;
	/**
	 * message
	 */
	public String msg = null;
	/**
	 * complete sts-ID
	 */
	public StatusFlg compStsId = StatusFlg.off;
	/**
	 * delete-status-ID
	 */
	public StatusFlg delStsId = StatusFlg.off;
	{
		   super.append(TaskFlowProcItems.delStsId, StatusFlg.off.parseBoolean() );
	}
	public TaskFlowProcCondition(){
		super(attr);
	}

	/**
	 * proc-IDを取得します。
	 * @return proc-ID
	 */
	public String getProcId() {
	    return procId;
	}

	/**
	 * proc-IDを設定します。
	 * @param procId proc-ID
	 */
	public void setProcId(String procId) {
	    this.procId = procId;
	    super.append(TaskFlowProcItems.procId, procId );
	}

	/**
	 * proc-ID'sを設定します。
	 * @param procIds proc-ID's
	 */
	public void setProcIds(String[] procIds) {
	    this.procIds = procIds;
	    super.append( TaskFlowProcItems.procId.getItemName() + "[]",
	    		SqlFormatUtils.getCondition(procIds, TaskFlowProcItems.procId.getItemName(), false, true, false) );
	}

	/**
	 * build srv-IDを取得します。
	 * @return build srv-ID
	 */
	public String getBldSrvId() {
	    return bldSrvId;
	}

	/**
	 * build srv-IDを設定します。
	 * @param bldSrvId build srv-ID
	 */
	public void setBldSrvId(String bldSrvId) {
	    this.bldSrvId = bldSrvId;
	    super.append(TaskFlowProcItems.bldSrvId, bldSrvId );
	}

	/**
	 * build env-IDを取得します。
	 * @return build env-ID
	 */
	public String getBldEnvId() {
	    return bldEnvId;
	}

	/**
	 * build env-IDを設定します。
	 * @param bldEnvId build env-ID
	 */
	public void setBldEnvId(String bldEnvId) {
	    this.bldEnvId = bldEnvId;
	    super.append(TaskFlowProcItems.bldEnvId, bldEnvId );
	}

	/**
	 * build line numberを取得します。
	 * @return build line number
	 */
	public Integer getBldLineNo() {
	    return bldLineNo;
	}

	/**
	 * build line numberを設定します。
	 * @param bldLineNo build line number
	 */
	public void setBldLineNo(Integer bldLineNo) {
	    this.bldLineNo = bldLineNo;
	    super.append(TaskFlowProcItems.bldLineNo, bldLineNo );
	}

	/**
	 * task flow-IDを取得します。
	 * @return task flow-ID
	 */
	public String getTaskFlowId() {
	    return taskFlowId;
	}

	/**
	 * task flow-IDを設定します。
	 * @param taskFlowId task flow-ID
	 */
	public void setTaskFlowId(String taskFlowId) {
	    this.taskFlowId = taskFlowId;
	    super.append(TaskFlowProcItems.taskFlowId, taskFlowId );
	}

	/**
	 * target sequence numberを取得します。
	 * @return target sequence number
	 */
	public Integer getTargetSeqNo() {
	    return targetSeqNo;
	}

	/**
	 * target sequence numberを設定します。
	 * @param targetSeqNo target sequence number
	 */
	public void setTargetSeqNo(Integer targetSeqNo) {
	    this.targetSeqNo = targetSeqNo;
	    super.append(TaskFlowProcItems.targetSeqNo, targetSeqNo );
	}

	/**
	 * process start time stampを取得します。
	 * @return process start time stamp
	 */
	public Timestamp getProcStTimestamp() {
	    return procStTimestamp;
	}

	/**
	 * process start time stampを設定します。
	 * @param procStTimestamp process start time stamp
	 */
	public void setProcStTimestamp(Timestamp procStTimestamp) {
	    this.procStTimestamp = procStTimestamp;
	    super.append(TaskFlowProcItems.procStTimestamp, procStTimestamp );
	}

	/**
	 * process end time stampを取得します。
	 * @return process end time stamp
	 */
	public Timestamp getProcEndTimestamp() {
	    return procEndTimestamp;
	}

	/**
	 * process end time stampを設定します。
	 * @param procEndTimestamp process end time stamp
	 */
	public void setProcEndTimestamp(Timestamp procEndTimestamp) {
	    this.procEndTimestamp = procEndTimestamp;
	    super.append(TaskFlowProcItems.procEndTimestamp, procEndTimestamp );
	}

	/**
	 * task sts-IDを取得します。
	 * @return task sts-ID
	 */
	public String getStsId() {
	    return stsId;
	}

	/**
	 * task sts-IDを設定します。
	 * @param stsId task sts-ID
	 */
	public void setStsId(String stsId) {
	    this.stsId = stsId;
	    super.append(TaskFlowProcItems.stsId, stsId );
	}

	/**
	 * task msg-IDを取得します。
	 * @return task msg-ID
	 */
	public String getMsgId() {
	    return msgId;
	}

	/**
	 * task msg-IDを設定します。
	 * @param msgId task msg-ID
	 */
	public void setMsgId(String msgId) {
	    this.msgId = msgId;
	    super.append(TaskFlowProcItems.msgId, msgId );
	}

	/**
	 * messageを取得します。
	 * @return message
	 */
	public String getMsg() {
	    return msg;
	}

	/**
	 * messageを設定します。
	 * @param msg message
	 */
	public void setMsg(String msg) {
	    this.msg = msg;
	    super.append(TaskFlowProcItems.msg, msg );
	}

	/**
	 * complete sts-IDを取得します。
	 * @return complete sts-ID
	 */
	public StatusFlg getCompStsId() {
	    return compStsId;
	}

	/**
	 * complete sts-IDを設定します。
	 * @param compStsId complete sts-ID
	 */
	public void setCompStsId(StatusFlg compStsId) {
	    this.compStsId = compStsId;
	    super.append(TaskFlowProcItems.compStsId, (null == compStsId)? StatusFlg.off.parseBoolean(): compStsId.parseBoolean() );
	}

	/**
	 * delete-status-IDを取得します。
	 * @return delete-status-ID
	 */
	public StatusFlg getDelStsId() {
	    return delStsId;
	}

	/**
	 * delete-status-IDを設定します。
	 * @param delStsId delete-status-ID
	 */
	public void setDelStsId(StatusFlg delStsId) {
	    this.delStsId = delStsId;
	    super.append( TaskFlowProcItems.delStsId, (null == delStsId)? null :delStsId.parseBoolean() );
	}

}