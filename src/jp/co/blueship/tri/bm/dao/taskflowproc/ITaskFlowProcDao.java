package jp.co.blueship.tri.bm.dao.taskflowproc;

import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.fw.dao.orm.IJdbcDao;


/**
 * The interface of the task flow process DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowProcDao extends IJdbcDao<ITaskFlowProcEntity> {
}
