package jp.co.blueship.tri.bm.dao.taskflowproc.constants;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableItem;


/**
 * The items of the task flow process entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum TaskFlowProcItems implements ITableItem {
	procId("proc_id"),
	bldSrvId("bld_srv_id"),
	bldEnvId("bld_env_id"),
	bldLineNo("bld_line_no"),
	taskFlowId("task_flow_id"),
	targetSeqNo("target_seq_no"),
	procStTimestamp("proc_st_timestamp"),
	procEndTimestamp("proc_end_timestamp"),
	stsId("sts_id"),
	msgId("msg_id"),
	msg("msg"),
	compStsId("comp_sts_id"),
	delStsId("del_sts_id"),
	regUserId("reg_user_id"),
	regUserNm("reg_user_nm"),
	regTimestamp("reg_timestamp"),
	updUserId("upd_user_id"),
	updUserNm("upd_user_nm"),
	updTimestamp("upd_timestamp");

	private String element = null;

	private TaskFlowProcItems( String element) {
		this.element = element;
	}

	public String getItemName() {
		return this.element;
	}

}
