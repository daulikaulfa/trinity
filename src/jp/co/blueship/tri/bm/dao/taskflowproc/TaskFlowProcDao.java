package jp.co.blueship.tri.bm.dao.taskflowproc;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflowproc.constants.TaskFlowProcItems;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.ISqlBuilder;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.ex.TriJdbcDaoException;
import jp.co.blueship.tri.fw.dao.orm.psql.JdbcBaseDao;

/**
 * The implements of the task flow process DAO.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 */
public class TaskFlowProcDao extends JdbcBaseDao<ITaskFlowProcEntity> implements ITaskFlowProcDao {
	/**
	 * default serial version
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected final ITableAttribute getTableAttribute() {
		return BmTables.BM_TASK_FLOW_PROC;
	}

	@Override
	protected final ISqlBuilder append( ISqlBuilder builder, ITaskFlowProcEntity entity ) {
		builder
			.append(TaskFlowProcItems.procId, entity.getProcId(), true)
			.append(TaskFlowProcItems.bldSrvId, entity.getBldSrvId(), true)
			.append(TaskFlowProcItems.bldEnvId, entity.getBldEnvId(), true)
			.append(TaskFlowProcItems.bldLineNo, entity.getBldLineNo(), true)
			.append(TaskFlowProcItems.taskFlowId, entity.getTaskFlowId())
			.append(TaskFlowProcItems.targetSeqNo, entity.getTargetSeqNo())
			.append(TaskFlowProcItems.procStTimestamp, entity.getProcStTimestamp())
			.append(TaskFlowProcItems.procEndTimestamp, entity.getProcEndTimestamp())
			.append(TaskFlowProcItems.stsId, entity.getStsId())
			.append(TaskFlowProcItems.msgId, entity.getMsgId())
			.append(TaskFlowProcItems.msg, entity.getMsg())
			.append(TaskFlowProcItems.compStsId, (null == entity.getCompStsId())? StatusFlg.off.parseBoolean(): entity.getCompStsId().parseBoolean())
			.append(TaskFlowProcItems.delStsId, (null == entity.getDelStsId())? null: entity.getDelStsId().parseBoolean())
			.append(TaskFlowProcItems.regTimestamp, entity.getRegTimestamp())
			.append(TaskFlowProcItems.regUserId, entity.getRegUserId())
			.append(TaskFlowProcItems.regUserNm, entity.getRegUserNm())
			.append(TaskFlowProcItems.updTimestamp, entity.getUpdTimestamp())
			.append(TaskFlowProcItems.updUserId, entity.getUpdUserId())
			.append(TaskFlowProcItems.updUserNm, entity.getUpdUserNm())
			;

		return builder;
	}

	@Override
	protected final ITaskFlowProcEntity entityMapping( ResultSet rs, int row ) throws SQLException {
		ITaskFlowProcEntity entity = new TaskFlowProcEntity();

		entity.setProcId( rs.getString(TaskFlowProcItems.procId.getItemName()) );
		entity.setBldSrvId( rs.getString(TaskFlowProcItems.bldSrvId.getItemName()) );
		entity.setBldEnvId( rs.getString(TaskFlowProcItems.bldEnvId.getItemName()) );
		entity.setBldLineNo( rs.getInt(TaskFlowProcItems.bldLineNo.getItemName()) );
		entity.setTaskFlowId( rs.getString(TaskFlowProcItems.taskFlowId.getItemName()) );
		entity.setTargetSeqNo( rs.getInt(TaskFlowProcItems.targetSeqNo.getItemName()) );
		entity.setProcStTimestamp( rs.getTimestamp(TaskFlowProcItems.procStTimestamp.getItemName()) );
		entity.setProcEndTimestamp( rs.getTimestamp(TaskFlowProcItems.procEndTimestamp.getItemName()) );
		entity.setStsId( rs.getString(TaskFlowProcItems.stsId.getItemName()) );
		entity.setMsgId( rs.getString(TaskFlowProcItems.msgId.getItemName()) );
		entity.setMsg( rs.getString(TaskFlowProcItems.msg.getItemName()) );
		entity.setCompStsId( StatusFlg.value(rs.getBoolean(TaskFlowProcItems.compStsId.getItemName())) );
		entity.setDelStsId( StatusFlg.value(rs.getBoolean(TaskFlowProcItems.delStsId.getItemName())) );
		entity.setRegTimestamp( rs.getTimestamp(TaskFlowProcItems.regTimestamp.getItemName()) );
		entity.setRegUserId( rs.getString(TaskFlowProcItems.regUserId.getItemName()) );
		entity.setRegUserNm( rs.getString(TaskFlowProcItems.regUserNm.getItemName()) );
		entity.setUpdTimestamp( rs.getTimestamp(TaskFlowProcItems.updTimestamp.getItemName()) );
		entity.setUpdUserId( rs.getString(TaskFlowProcItems.updUserId.getItemName()) );
		entity.setUpdUserNm( rs.getString(TaskFlowProcItems.updUserNm.getItemName()) );

		return entity;
	}

    @Override
	public int insert( ITaskFlowProcEntity entity ) throws TriJdbcDaoException {
    	return super.insert(entity);
    }

    @Override
	public int[] insert( List<ITaskFlowProcEntity> entities ) throws TriJdbcDaoException {
    	return super.insert(entities);
    }

    @Override
	public int update( ITaskFlowProcEntity entity ) throws TriJdbcDaoException {
    	return super.update(entity);
    }

    @Override
	public int[] update( List<ITaskFlowProcEntity> entities ) throws TriJdbcDaoException {
    	return super.update(entities);
    }

    @Override
	public int update( ISqlCondition condition, ITaskFlowProcEntity entity ) throws TriJdbcDaoException {
    	return super.update(condition, entity);
    }

    @Override
	public int delete( ISqlCondition condition ) throws TriJdbcDaoException {
    	return super.delete(condition);
    }

}
