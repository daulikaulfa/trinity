package jp.co.blueship.tri.bm.dao.taskflowproc.eb;

import java.sql.Timestamp;

import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.dao.orm.IEntityFooter;

/**
 * The interface of the task flow process entity.
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public interface ITaskFlowProcEntity extends IEntityFooter {

	public String getProcId();
	public void setProcId(String procId);

	public String getBldSrvId();
	public void setBldSrvId(String bldSrvId);

	public String getBldEnvId();
	public void setBldEnvId(String bldEnvId);

	public Integer getBldLineNo();
	public void setBldLineNo(Integer bldLineNo);

	public String getTaskFlowId();
	public void setTaskFlowId(String taskFlowId);

	public Integer getTargetSeqNo();
	public void setTargetSeqNo(Integer targetSeqNo);

	public Timestamp getProcStTimestamp();
	public void setProcStTimestamp(Timestamp procStTimestamp);

	public Timestamp getProcEndTimestamp();
	public void setProcEndTimestamp(Timestamp procEndTimestamp);

	//V2でのBaseStatusIdにあたる
	public String getStsId();
	public void setStsId(String stsId);

	public String getMsgId();
	public void setMsgId(String msgId);

	public String getMsg();
	public void setMsg(String msg);

	public StatusFlg getCompStsId();
	public void setCompStsId(StatusFlg compStsId);

//	public String getBpId();
//	public void setBpId(String bpId);
//	public String getRpId();
//	public void setRpId(String rpId);
//	public String getLotId();
//	public void setLotId(String lotId);

}
