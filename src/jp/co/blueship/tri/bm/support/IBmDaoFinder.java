package jp.co.blueship.tri.bm.support;

import jp.co.blueship.tri.bm.dao.bldenv.IBldEnvDao;
import jp.co.blueship.tri.bm.dao.bldenv.IBldEnvSrvDao;
import jp.co.blueship.tri.bm.dao.bldsrv.IBldSrvDao;
import jp.co.blueship.tri.bm.dao.bldtimeline.IBldTimelineAgentDao;
import jp.co.blueship.tri.bm.dao.bldtimeline.IBldTimelineDao;
import jp.co.blueship.tri.bm.dao.bp.IBpAreqLnkDao;
import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowDao;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowResDao;
import jp.co.blueship.tri.bm.dao.taskflowproc.ITaskFlowProcDao;

/**
 * BM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public interface IBmDaoFinder {

	/**
	 * bldSrvDaoを取得します。
	 * @return bldEnvSrvDao
	 */
	public IBldSrvDao getBldSrvDao();

	/**
	 * bldEnvDaoを取得します。
	 * @return bldEnvDao
	 */
	public IBldEnvDao getBldEnvDao();

	/**
	 * bldTimelineDaoを取得します。
	 * @return bldTimelineDao
	 */
	public IBldTimelineDao getBldTimelineDao();

	/**
	 * bldTimelineAgentDaoを取得します。
	 * @return bldTimelineAgentDao
	 */
	public IBldTimelineAgentDao getBldTimelineAgentDao();

	/**
	 * bldEnvSrvDaoを取得します。
	 * @return bldEnvSrvDao
	 */
	public IBldEnvSrvDao getBldEnvSrvDao();

	/**
	 * taskFlowDaoを取得します。
	 * @return taskFlowDao
	 */
	public ITaskFlowDao getTaskFlowDao();
	/**
	 * taskFlowProcDaoを取得します。
	 * @return taskFlowDao
	 */
	public ITaskFlowProcDao getTaskFlowProcDao();

	/**
	 * taskFlowResDaoを取得します。
	 * @return taskFlowResDao
	 */
	public ITaskFlowResDao getTaskFlowResDao();

	/**
	 * bpkDaoを取得します。
	 * @return bpDao
	 */
	public IBpDao getBpDao();

	/**
	 * bpAreqLnkDaoを取得します。
	 * @return bpAreqLnkDao
	 */
	public IBpAreqLnkDao getBpAreqLnkDao();
}
