package jp.co.blueship.tri.bm.support;

import java.util.List;

import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * BM関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public interface IBmFinderSupport extends IBmDaoFinder {
	/**
	 * smFinderSupportを取得します。
	 * @return smFinderSupport
	 */
	public ISmFinderSupport getSmFinderSupport();
	/**
	 * umFinderSupportを取得します。
	 * @return umFinderSupport
	 */
	public IUmFinderSupport getUmFinderSupport();
	/**
	 * amFinderSupportを取得します。
	 * @return amFinderSupport
	 */
	public IAmFinderSupport getAmFinderSupport();
	/**
	 * dcmFinderSupportを取得します。
	 * @return dcmFinderSupport
	 */
	public IDcmFinderSupport getDcmFinderSupport();


	/**
	 * 本体サーバ情報を取得します。
	 *
	 * @return 取得した本体サーバ情報エンティティを戻します。本体サーバを取得できない場合、システム例外がthrowされます。
	 */
	public IBldSrvEntity findBldSrvEntityByController() throws TriSystemException;

	/**
	 * サーバ情報を取得します。
	 *
	 * @param bldSrvId ビルドサーバID
	 * @return 取得したサーバ情報エンティティを戻します。サーバを取得できない場合、システム例外がthrowされます。
	 */
	public IBldSrvEntity findBldSrvEntity( String bldSrvId ) throws TriSystemException;

	/**
	 * ビルド環境情報を取得します
	 * @param bldEnvId ビルド環境ID
	 * @return 取得したビルド環境情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IBldEnvEntity findBldEnvEntity( String bldEnvId ) throws TriSystemException;

	/**
	 * ビルドパッケージ環境に関連したサーバ情報を取得します。
	 *
	 * @param envId 環境ID
	 * @param srvId サーバID
	 * @return 取得したビルド環境サーバー情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IBldEnvSrvEntity findBldEnvSrvEntity( String envId, String srvId) throws TriSystemException;

	/**
	 * タスク・フロー情報を取得します。
	 *
	 * @param taskFlowId タスクフローID
	 * @return 取得したタスク・フローエンティティを戻します。
	 */
	public ITaskFlowEntity findTaskFlowEntity( String taskFlowId );

	/**
	 * ビルドパッケージ情報を取得します
	 * @param bpId ビルドパッケージID
	 * @return 取得したビルドパッケージ情報エンティティを戻します。１件も取得できない場合、システム例外がthrowされます。
	 */
	public IBpEntity findBpEntity( String bpId ) throws TriSystemException;

	/**
	 * ビルドパッケージ情報を取得します
	 * @param bpIds ビルドパッケージID
	 * @return 取得したビルドパッケージ情報エンティティを戻します。
	 */
	public List<IBpEntity> findBpEntities( String... bpIds );

	/**
	 * ビルドパッケージ情報を取得します
	 * @param sort ソート条件
	 * @param bpIds ビルドパッケージID
	 * @return 取得したビルドパッケージ情報エンティティを戻します。
	 */
	public List<IBpEntity> findBpEntities( ISqlSort sort, String... bpIds );

	/**
	 * ビルドパッケージ・モジュール情報を取得します。
	 *
	 * @param bpId ビルドパッケージID
	 * @return 取得したビルドパッケージ・モジュールエンティティを戻します。
	 */
	public List<IBpMdlLnkEntity> findBpMdlLnkEntities( String bpId );

	/**
	 * ロットIDからビルドパッケージエンティティのリストを取得します。
	 * @param lotId ロットID
	 * @return 取得したビルドパッケージエンティティを戻します。
	 */
	public List<IBpEntity> findBpEntitiesFromLotId( String lotId );

	/**
	 * リリースIDからビルドパッケージエンティティのリストを取得します。
	 * @param rpId リリースID
	 * @return 取得したビルドパッケージエンティティを戻します。
	 */
	public List<IBpEntity> findBpEntitiesFromRpId( String rpId );

	/**
	 * ビルドパッケージDTOを取得します。
	 *
	 * @param bpId ビルドパッケージID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IBpDto findBpDto( String bpId, BmTables... tables ) throws TriSystemException;

	/**
	 * ビルドパッケージDTOのListを取得します。
	 *
	 * @param bpIds ビルドパッケージID
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOのListを戻します。
	 */
	public List<IBpDto> findBpDto( String[] bpIds, BmTables... tables );

	/**
	 * ビルドパッケージDTOのListを取得します。
	 *
	 * ビルドパッケージのリストからビルドパッケージDTOを取得します。
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOのListを戻します。
	 */
	public List<IBpDto> findBpDto( List<IBpEntity> bpEntities, BmTables... tables );

	/**
	 * ビルドパッケージDTOを取得します。
	 *
	 * @param bpEntity ビルドパッケージEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOを戻します。親が１件も取得できない場合、システム例外がthrowされます。
	 */
	public IBpDto findBpDto( IBpEntity bpEntity, BmTables... tables ) throws TriSystemException;

	/**
	 * ビルドパッケージDTOのListを取得します。
	 *
	 * @param condition SQL Conditon
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOのListを戻します。
	 */
	public List<IBpDto> findBpDto( ISqlCondition condition, BmTables... tables );

	/**
	 * ビルドパッケージのリストからビルドパッケージDTOを取得します。
	 * @param bpEntities ビルドパッケージEntityのリスト
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * 					{@link BmTables#BM_BP_MDL_LNK}、
	 * 					{@link BmTables#BM_BP_AREQ_LNK}
	 * @return 取得したビルドパッケージDTOのListを戻します。
	 * @return 取得したビルドパッケージDTOのList
	 */
	public BpDtoList findBpDtoList( List<IBpEntity> bpEntities, BmTables... tables );
	
	public List<String> findRpIdsByBpIds( String... bpIds );
}
