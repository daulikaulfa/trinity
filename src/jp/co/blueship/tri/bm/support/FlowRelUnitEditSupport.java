package jp.co.blueship.tri.bm.support;

import java.io.File;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMdlLnkCondition;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.dao.bldenv.constants.BldEnvSrvItems;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.constants.BldTimelineItems;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineAgentCondition;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineCondition;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.taskflow.constants.TaskFlowItems;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowCondition;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitHistoryViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.vcs.ScmParamFile;
import jp.co.blueship.tri.fw.vcs.ScmParamFile.ObjType;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaBpLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;


/**
 * FlowRelUnitEntry等のビルドパッケージ更新系イベントのサポートClass
 * <br>
 * <p>
 * ビルドパッケージのための業務サービス処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 *
 * @version V4.00.00
 * @author le.thixuan
 */

public class FlowRelUnitEditSupport extends BmFinderSupport {

	private IDesignSheet sheet;

	/**
	 * 変更管理エンティティから変更管理画面用ビーンを設定します。
	 * @param entities 変更管理エンティティ
	 * @param pjtNoBean PjtNoBean
	 * @param closedPjtViewBeanList クローズ済み変更管理画面用ビーンのリスト
	 * @param pjtViewBeanList 承認済み変更管理画面用ビーンのリスト
	 */
	public void setPjtViewBean(
			IPjtEntity[] entities, PjtNoBean pjtNoBean,
			List<PjtViewBean> closedPjtViewBeanList, List<PjtViewBean> pjtViewBeanList ) {

		List<PjtViewBean> tmpPjtViewBeanList = new ArrayList<PjtViewBean>();
		setPjtViewBeanPjtEntity( tmpPjtViewBeanList, entities );


		Set<String> closedPjtIdSet			= pjtNoBean.getClosedPjtNo();
		Set<String> approvedPjtIdSet		= pjtNoBean.getApprovedPjtNo();

		for ( PjtViewBean viewBean : tmpPjtViewBeanList ) {

			String pjtNo = viewBean.getPjtNo();

			if ( closedPjtIdSet.contains( pjtNo ) && !approvedPjtIdSet.contains( pjtNo ) ) {

				// クローズのみの申請情報で構成された変更管理
				viewBean.setSelectFixFlag( true );
				closedPjtViewBeanList.add( viewBean );

			} else if ( !closedPjtIdSet.contains( pjtNo ) && approvedPjtIdSet.contains( pjtNo ) ) {

				// 承認のみの申請情報で構成された変更管理
				viewBean.setSelectFixFlag( false );
				pjtViewBeanList.add( viewBean );

			} else if ( closedPjtIdSet.contains( pjtNo ) && approvedPjtIdSet.contains( pjtNo ) ) {

				PjtViewBean newViewBean = new PjtViewBean();
				TriPropertyUtils.copyProperties( newViewBean, viewBean );

				newViewBean.setSelectFixFlag( true );
				closedPjtViewBeanList.add( newViewBean );

				viewBean.setSelectFixFlag( false );
				pjtViewBeanList.add( viewBean );
			}
		}
	}

	/**
	 * ビルドパッケージ環境のビルドサーバ環境を取得する。
	 * @param envNo 環境番号
	 * @return ビルドパッケージ環境のビルドサーバ環境情報
	 */
	public IBldEnvSrvEntity[] getBldEnvSrvEntity( String envNo ) {

		BldEnvSrvCondition condition = new BldEnvSrvCondition();
		condition.setBldEnvId( envNo );

		ISqlSort sort = new SortBuilder();
		sort.setElement(BldEnvSrvItems.bldSrvId, TriSortOrder.Asc, 1);

		IEntityLimit<IBldEnvSrvEntity> entityLimit =
				this.getBldEnvSrvDao().find( condition.getCondition(), sort, 1, 0 );

		if( TriStringUtils.isEmpty(entityLimit.getEntities()) ){
			throw new TriSystemException (BmMessageId.BM004000F , envNo );
		}
		return entityLimit.getEntities().toArray(new IBldEnvSrvEntity[0]);
	}

	/**
	 * ビルドパッケージ作成環境タスクのタイムライン情報を取得する。
	 * @param envNo 環境番号
	 * @return ビルドパッケージ作成環境タスクのタイムライン情報
	 */
	public IBldTimelineEntity[] getBldTimelineEntity( String envNo ) {

		BldTimelineCondition condition = new BldTimelineCondition();
		condition.setBldEnvId(envNo);

		ISqlSort sort = new SortBuilder();
		sort.setElement(BldTimelineItems.bldLineNo, TriSortOrder.Asc, 1);

		IEntityLimit<IBldTimelineEntity> entityLimit =
				this.getBldTimelineDao().find( condition.getCondition(), sort, 1, 0 );

		if( TriStringUtils.isEmpty(entityLimit.getEntities()) ){
			throw new TriSystemException (BmMessageId.BM004001F , envNo);
		}
		for ( IBldTimelineEntity entity : entityLimit.getEntities() ) {
			BldTimelineAgentCondition agentCondition = new BldTimelineAgentCondition();
			agentCondition.setBldEnvId(entity.getBldEnvId());
			agentCondition.setBldLineNo(entity.getBldLineNo());
			List<IBldTimelineAgentEntity> agentEntities = this.getBldTimelineAgentDao().find(agentCondition.getCondition());
			if( agentEntities!=null && agentEntities.size()>0 ) {
				entity.setLine(agentEntities.toArray(new IBldTimelineAgentEntity[0]));
			}
		}
		return entityLimit.getEntities().toArray(new IBldTimelineEntity[0]);
	}
	/**
	 * タスク情報をすべて取得する。
	 * @return タスク情報
	 */
	public ITaskFlowEntity[] getTaskFlowEntity() {

		TaskFlowCondition condition = new TaskFlowCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(TaskFlowItems.taskFlowId, TriSortOrder.Asc, 1);

		IEntityLimit<ITaskFlowEntity> entityLimit =
				this.getTaskFlowDao().find( condition.getCondition(), sort, 1, 0 );

		if( 0 == entityLimit.getEntities().size() ){
			throw new TriSystemException (BmMessageId.BM004002F);
		}
		return entityLimit.getEntities().toArray( new ITaskFlowEntity[0] );
	}
	/**
	 * ビルドパッケージ情報を取得します。
	 *
	 * <code>interval</code>に指定された間隔で、<code>times</code>に指定された回数リトライします。
	 * <code>interval</code>、<code>times</code>が<code>0</code>未満で指定された場合、デフォルトとして
	 * 以下の値で実行されます。
	 * <br>
	 * <br>
	 * <code>interval = 0</code>
	 * <br>
	 * <code>times = 0</code>
	 * <br>
	 * <br>
	 * すなわち、リトライはされず1回だけ実行されます。
	 * <br>
	 * また、<code>times</code>は<strong>リトライ</strong>回数のため、
	 * <br>
	 * <br>
	 * <code>times = 3</code>
	 * <br>
	 * <br>
	 * とした場合、処理は<strong>4</strong>回実行されることに注意してください。
	 *
	 * @param buildNo ビルドパッケージ番号
	 * @param times リトライ回数
	 * @param interval リトライ間隔(ミリ秒)
	 * @return 取得したビルドパッケージ情報エンティティを戻します。
	 */
	public final IBpEntity getBpEntity( String buildNo, int times, long interval ) {

		if ( TriStringUtils.isEmpty(buildNo) ){
			throw new TriSystemException (BmMessageId.BM005000S);
		}
		BpCondition condition = new BpCondition();
		condition.setBpId( buildNo );
		IBpEntity buildEntity = this.getBpDao().findByPrimaryKey( condition.getCondition() );

		if ( null != buildEntity )
			return buildEntity;


		if( times < 0 )
			times = 0;

		if( interval < 0 )
			interval = 0;


		for (int i = 0; i < times; i++) {

			buildEntity = this.getBpDao().findByPrimaryKey( condition.getCondition() );

			if(null != buildEntity)
				return buildEntity;

			try {
				Thread.sleep(interval);
			} catch (InterruptedException e) {
				throw new TriSystemException (BmMessageId.BM005001S , e , buildNo );
			}
		}

		return throwException(buildEntity,buildNo);
	}

	/*
	 * デッドコードの警告を消すためのメソッド。
	 */
	private IBpEntity throwException(IBpEntity buildEntity, String buildNo){

		if ( null == buildEntity ){
			throw new TriSystemException ( BmMessageId.BM004003F , buildNo );
		}
		return buildEntity;
	}

	/**
	 * SCMへの反映対象のファイルリストのうち、指定されたモジュール配下のファイルリストを抽出する。
	 * （モジュール名を除去する）
	 * @param commitFileList SCMへの反映対象のファイルリスト
	 * @param mdlEntity 対象モジュール
	 * @return 指定されたモジュール配下のファイルリスト
	 */
	public static List<ScmParamFile> getFileListInModule(
			List<String> commitFileList, ILotMdlLnkEntity mdlEntity ) {

		List<ScmParamFile> commitFileModuleList = new ArrayList<ScmParamFile>();

		for( String commitPath : commitFileList ) {

			String newPath = BmDesignBusinessRuleUtils.convertPathInModule( commitPath, mdlEntity );

			if( null != newPath ) {

				ScmParamFile paramFile = new ScmParamFile();
				paramFile.setObjName( newPath );
				paramFile.setObjType( ObjType.unknown );

				commitFileModuleList.add( paramFile ) ;
			}
		}

		return commitFileModuleList;
	}
	/**
	 * 変更申請Dtoを取得する。
	 * @param pjtEntities
	 * @return 変更申請Dto
	 */
	public List<IPjtAvlDto> findPjtAvlDtoList(IPjtEntity[] pjtEntities ){

		List<IPjtAvlDto> pjtDtoList = new ArrayList<IPjtAvlDto>();
		for ( IPjtEntity entity : pjtEntities ) {
			IPjtAvlDto pjtDto = this.getAmFinderSupport().findPjtAvlDtoByLatest( entity );
			pjtDtoList.add( pjtDto );
		}
		return pjtDtoList;
	}
	/**
	 * ビルドパッケージ番号からビルドパッケージエンティティを取得する
	 * @param buildNo ビルドパッケージ番号
	 * @return ビルドパッケージエンティティリミット
	 */
	public IEntityLimit<IBpEntity> getBpEntity( String[] buildNo ) {

		IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByBuildNo( buildNo );

		return getBpEntity( condition, null, 1, 0 );
	}


	/**
	 * ビルドパッケージエンティティを取得する
	 * @return ビルドパッケージエンティティリミット
	 */
	public IEntityLimit<IBpEntity> getBpEntity(
			IJdbcCondition condition, ISqlSort sort, int selectPageNo, int maxPageNumber ) {

		IEntityLimit<IBpEntity> limit =
				this.getBpDao().find( condition.getCondition(), sort, selectPageNo, maxPageNumber );

		return limit;
	}

	/**
	 * ビルドパッケージ番号からリリース申請エンティティを取得する
	 * リリース申請せずにビルドパッケージクローズや取消するなど、
	 * ビルドパッケージが関連していない状態のリリース申請に対しても検索対象とするため
	 * 検索結果が0件のときにエラーとはしない
	 * @param buildNo ビルドパッケージ番号
	 * @return リリース申請エンティティリミット
	 */
	public IEntityLimit<IRaEntity> getRelApplyEntityByBuildNo( String[] buildNo ) {

		//BpIDからRaIdを渡す
		List<IRaBpLnkEntity> lnkEntityList = this.getRmFinderSupport().findRaBpLnkEntitiesFromBpId(buildNo);
		List<String> raIdList = new ArrayList<String>();
		for ( IRaBpLnkEntity lnkEntity : lnkEntityList ) {
			raIdList.add( lnkEntity.getRaId() );
		}
		IJdbcCondition condition = DBSearchConditionAddonUtil.getRelApplyConditionByRaId( raIdList.toArray(new String[0]) );

		IEntityLimit<IRaEntity> limit =
				this.getRmFinderSupport().getRaDao().find( condition.getCondition(), null, 1, 0 );

		return limit;
	}

	/**
	 * リリース申請エンティティを取得する
	 * @return リリース申請エンティティリミット
	 */
	public IEntityLimit<IRaEntity> getRelApplyEntity(
			IJdbcCondition condition, ISqlSort sort, int selectPageNo, int maxPageNumber ) {

		IEntityLimit<IRaEntity> limit =
				this.getRmFinderSupport().getRaDao().find( condition.getCondition(), sort, selectPageNo, maxPageNumber );

		if ( TriStringUtils.isEmpty(limit.getEntities()) ) {
			throw new TriSystemException(BmMessageId.BM004006F );
		}

		return limit;
	}

	/**
	 * 申請番号から申請情報エンティティを取得する
	 * @param applyNo 申請番号
	 * @return 申請情報エンティティ
	 */
	public IAreqEntity[] getAreqEntity( String[] applyNo ) {
		if( TriStringUtils.isEmpty(applyNo) ) {
			throw new TriSystemException(BmMessageId.BM005004S );
		}
		IJdbcCondition condition			= DBSearchConditionAddonUtil.getAreqCondition( applyNo );
		List<IAreqEntity> entities = this.getAmFinderSupport().getAreqDao().find( condition.getCondition() );

		return entities.toArray(new IAreqEntity[0]);
	}

	/**
	 * 変更管理番号から変更管理エンティティを取得する
	 * @param pjtNo 変更管理番号
	 * @return 変更管理エンティティ
	 */
	public IPjtEntity[] getPjtEntity( String[] pjtNo ) {

		String[] conditionPjtNo = pjtNo;

		if ( TriStringUtils.isEmpty( conditionPjtNo ) ) {
			//ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[]{ "" };
		}

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getPjtCondition( conditionPjtNo );
		IEntityLimit<IPjtEntity> limit	= this.getAmFinderSupport().getPjtDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IPjtEntity[0]);
	}

	/**
	 * クローズ済、承認済の変更管理番号をマージする。同じ番号は統一される。
	 * @param paramBean FlowRelUnitEntryServiceBean
	 * @param includeClosedPjt クローズ済みの変更管理番号を含めるか
	 * <pre>
	 * 		true	:	クローズ済みのものを含める
	 * 		false	:	クローズ済みのものを含めない
	 * </pre>
	 * @return マージ後の変更管理番号
	 */
	public String[] mergePjtNo( FlowRelUnitEntryServiceBean paramBean , boolean includeClosedPjt ) {

		Set<String> pjtIdSet = new TreeSet<String>();

		if ( !TriStringUtils.isEmpty( paramBean.getSelectedPjtNo() )) {
			pjtIdSet.addAll( FluentList.from(paramBean.getSelectedPjtNo()).asList());
		}

		if( true == includeClosedPjt ) {
			if ( !TriStringUtils.isEmpty( paramBean.getSelectedClosedPjtNo() ) ) {
				pjtIdSet.addAll( FluentList.from(paramBean.getSelectedClosedPjtNo()).asList());
			}
		}

		return (String[])pjtIdSet.toArray( new String[0] );
	}

	/**
	 * 変更管理番号ビーンを取得する
	 * @param paramBean FlowRelUnitEntryServiceBean
	 * @return 変更管理番号ビーン
	 */
	public PjtNoBean getPjtNoBean( FlowRelUnitEntryServiceBean paramBean ) {

		Set<String> closedPjtIdSet		= new HashSet<String>();
		Set<String> approvedPjtNoSet	= new HashSet<String>();

		if ( !TriStringUtils.isEmpty( paramBean.getSelectedClosedPjtNo() )) {
			closedPjtIdSet.addAll( FluentList.from(paramBean.getSelectedClosedPjtNo()).asList() );
		}

		if ( !TriStringUtils.isEmpty( paramBean.getSelectedPjtNo() )) {
			approvedPjtNoSet.addAll( FluentList.from(paramBean.getSelectedPjtNo()).asList() );
		}


		FlowRelUnitEditSupport.PjtNoBean bean = this.newPjtNoBean();
		bean.setClosedPjtNo		( closedPjtIdSet );
		bean.setApprovedPjtNo	( approvedPjtNoSet );

		return bean;
	}

	/**
	 * リリースエンティティから、リリース番号と環境名のマップを取得する
	 * @param buildNoRelEntitySetMap ビルドパッケージ番号とリリースエンティティのセットのマップ
	 * @return リリース番号と環境名のマップ
	 */
	private Map<String, String> getRelNoEnvNameMap(
			Map<String, Set<IRpEntity>> buildNoRelEntitySetMap ) {

		Set<IRpEntity> relEntityAllSet = new HashSet<IRpEntity>();
		for ( Set<IRpEntity> relEntitySet : buildNoRelEntitySetMap.values() ) {
			relEntityAllSet.addAll( relEntitySet );
		}

		Set<String> envNoSet = new HashSet<String>();
		for ( IRpEntity entity : relEntityAllSet ) {
			envNoSet.add( entity.getBldEnvId() );
		}

		IJdbcCondition condition		=
				DBSearchConditionAddonUtil.getRelEnvCondition( (String[])envNoSet.toArray( new String[0] ));
		IEntityLimit<IBldEnvEntity> limit	= this.getBldEnvDao().find( condition.getCondition(), null, 1, 0 );


		Map<String, String> envNoEnvNameMap = new HashMap<String, String>();
		for ( IBldEnvEntity entity : limit.getEntities() ) {
			envNoEnvNameMap.put( entity.getBldEnvId(), entity.getBldEnvNm() );
		}

		Map<String, String> relNoEnvNameMap = new TreeMap<String, String>();
		for ( IRpEntity entity : relEntityAllSet ) {
			relNoEnvNameMap.put( entity.getRpId(), envNoEnvNameMap.get( entity.getBldEnvId() ));
		}

		return relNoEnvNameMap;
	}

	/**
	 * ビルドパッケージ番号から、ビルドパッケージ番号とリリースエンティティのセットのマップを取得する
	 * @param buildNoArray ビルドパッケージ番号の配列
	 * @return ビルドパッケージ番号とリリースエンティティのセットのマップ
	 */
	private Map<String, Set<IRpEntity>> getBuildNoRelEntitySetMap( String[] buildNoArray ) {

		Map<String, Set<IRpEntity>> buildNoRelEntitySetMap =
				new HashMap<String, Set<IRpEntity>>();
		for ( String buildNo : buildNoArray ) {
			// リリースエンティティはリリース番号の昇順に並べる
			buildNoRelEntitySetMap.put( buildNo, new TreeSet<IRpEntity>( new RelEntityComparator() ));
		}

		String[] rpIds = getRpIdFromSelectedBpIds(buildNoArray);
		IJdbcCondition condition	= DBSearchConditionAddonUtil.getRpConditionByRpIds( rpIds );
		List<IRpDto> rpEntities = this.getRmFinderSupport().findRpDto( condition.getCondition() );
		for ( IRpDto entity : rpEntities ) {

			for ( IRpBpLnkEntity build : entity.getRpBpLnkEntities() ) {

				if ( buildNoRelEntitySetMap.containsKey( build.getBpId() )) {
					buildNoRelEntitySetMap.get( build.getBpId() ).add( entity.getRpEntity() );
				}
			}
		}

		return buildNoRelEntitySetMap;
	}

	private String[] getRpIdFromSelectedBpIds( String[] buildNoArray ) {

		List<String> rpIds = new ArrayList<String>();
		for ( String buildNo : buildNoArray ){
			rpIds.addAll( getRpIdFromSelectedBpId( buildNo ) );
		}
		return rpIds.toArray( new String[0]);
	}

	private List<String> getRpIdFromSelectedBpId( String buildNo ){

		List<IRpBpLnkEntity> lnkEntityList = this.getRmFinderSupport().findRpBpLnkEntitiesFromBpId( buildNo );
		List<String> rpIds = new ArrayList<String>();
		for ( IRpBpLnkEntity lnkEntity : lnkEntityList ){
			rpIds.add(lnkEntity.getRpId());
		}
		return rpIds;
	}

	/**
	 * ビルドパッケージエンティティからビルドパッケージ番号を取得する。
	 * @param entities ビルドパッケージエンティティ
	 * @return ビルドパッケージ番号の配列
	 */
	public String[] getBuildNo( List<IBpDto> bpDtoList ) {

		Set<String> buildNoSet = new HashSet<String>();

		for ( IBpDto bpDto : bpDtoList ) {
			buildNoSet.add( bpDto.getBpEntity().getBpId() );
		}

		return (String[])buildNoSet.toArray( new String[0] );
	}

	/**
	 * ビルドパッケージエンティティからビルドIDを取得する。
	 * @param entities ビルドパッケージエンティティ
	 * @return ビルドIDの配列
	 */
	public String[] getBuildNo( IBpEntity[] entities ) {

		Set<String> buildNoSet = new HashSet<String>();
		for ( IBpEntity buildEntity : entities ) {
			if ( StringUtils.isEmpty(buildEntity.getBpId()) ) {
				continue;
			}

			buildNoSet.add( buildEntity.getBpId() );
		}
		return (String[])buildNoSet.toArray( new String[0] );
	}

	/**
	 * リリース申請エンティティからリリース申請番号を取得する。
	 * @param entities リリース申請エンティティ
	 * @return リリース申請番号の配列
	 */
	public String[] getRelApplyNo( IRaEntity[] entities ) {

		Set<String> relApplyNoSet = new HashSet<String>();

		for ( IRaEntity entity : entities ) {
			relApplyNoSet.add( entity.getRaId() );
		}

		return (String[])relApplyNoSet.toArray( new String[0] );
	}

	/**
	 * パッケージ履歴画面表示ビーンを取得する。
	 * @param buildArray ビルドパッケージの配列
	 * @return パッケージ履歴画面表示ビーンのリスト
	 */
	public List<UnitHistoryViewBean> getUnitHistoryViewBeanList( IBpDto[] buildArray ) {

		List<UnitHistoryViewBean> unitHistoryViewBeanList = new ArrayList<UnitHistoryViewBean>();

		setUnitHistoryViewBeanBuildEntity(
				unitHistoryViewBeanList, buildArray);

		return unitHistoryViewBeanList;
	}

	/**
	 * ビルドパッケージ履歴一覧情報をビルドパッケージエンティティから設定する
	 * @param unitHistoryViewBeanList ビルドパッケージ履歴一覧情報
	 * @param bpDtoList ビルドパッケージのエンティティ
	 */
	private void setUnitHistoryViewBeanBuildEntity(
			List<UnitHistoryViewBean> unitHistoryViewBeanList, IBpDto[] bpDtoList ) {

		for ( IBpDto bpDto : bpDtoList ) {
			IBpEntity bpEntity = bpDto.getBpEntity();

			UnitHistoryViewBean viewBean = new UnitHistoryViewBean();

			viewBean.setRelUnitNo		( bpEntity.getBpId() );
			viewBean.setRelUnitName		( bpEntity.getBpNm() );
			viewBean.setRelUnitSummary	( bpEntity.getSummary() );
			viewBean.setExecuteDate		( TriDateUtils.convertViewDateFormat( bpEntity.getProcStTimestamp() ) );
			viewBean.setRelUnitStatus	(
					sheet().getValue( RmDesignBeanId.statusId, bpEntity.getProcStsId() ));

			unitHistoryViewBeanList.add( viewBean );
		}
	}

	/**
	 * パッケージクローズ画面表示ビーンを取得する。
	 * @param buildArray ビルドパッケージの配列
	 * @return パッケージクローズ画面表示ビーンのリスト
	 */
	public List<UnitCloseViewBean> getUnitCloseViewBeanList( List<IBpDto> buildArray ) {

		String[] buildNoArray = this.getBuildNo( buildArray );

		Map<String, Set<IRpEntity>> buildNoRelEntitySetMap = getBuildNoRelEntitySetMap( buildNoArray );
		Map<String, String> relNoEnvNameMap = getRelNoEnvNameMap( buildNoRelEntitySetMap );


		List<UnitCloseViewBean> unitViewBeanList = new ArrayList<UnitCloseViewBean>();

		setUnitViewBeanBuildEntity(
				unitViewBeanList, buildArray,
				buildNoRelEntitySetMap, relNoEnvNameMap );

		return unitViewBeanList;
	}
	/**
	 * ビルドパッケージ一覧情報をビルドパッケージエンティティから設定する
	 * @param unitViewBeanList ビルドパッケージ一覧情報
	 * @param entities ビルドパッケージエンティティ
	 * @param buildNoRelEntitySetMap ビルドパッケージ番号とリリースエンティティのセットのマップ
	 * @param relNoEnvNameMap リリース番号と環境名のマップ
	 */
	private void setUnitViewBeanBuildEntity(
			List<UnitCloseViewBean> unitViewBeanList,
			List<IBpDto> entities,
			Map<String, Set<IRpEntity>> buildNoRelEntitySetMap,
			Map<String, String> relNoEnvNameMap ) {

		for ( IBpDto entity : entities ) {

			UnitCloseViewBean viewBean = new UnitCloseViewBean();

			viewBean.setRelUnitNo		( entity.getBpEntity().getBpId() );
			viewBean.setRelUnitName		( entity.getBpEntity().getBpNm() );
			viewBean.setRelUnitStatus	(
					sheet().getValue( RmDesignBeanId.statusId, entity.getBpEntity().getProcStsId() ));
			viewBean.setCreateBy( entity.getBpEntity().getRegUserNm() );
			viewBean.setCreateById( entity.getBpEntity().getRegUserId() );
			viewBean.setCreateDate( entity.getBpEntity().getRegTimestamp() );

			for ( IRpEntity relEntity : buildNoRelEntitySetMap.get( entity.getBpEntity().getBpId() )) {

				String relNo = relEntity.getRpId();
				viewBean.getRelNoList().add		( relNo );
				viewBean.getEnvNameList().add	( relNoEnvNameMap.get( relNo ));
			}

			unitViewBeanList.add( viewBean );
		}
	}

	/**
	 * ビルドパッケージの検索条件に情報を設定する
	 * @param condition ビルドパッケージの検索条件
	 * @param unitSearchConditionBean 検索条件格納ビーン
	 */
	public void setBuildCondition(
			BpCondition condition, RelUnitSearchBean relUnitSearchBean ) {

		if ( TriStringUtils.isNotEmpty( relUnitSearchBean.getSearchBuildNo() )) {
			condition.setBpId( relUnitSearchBean.getSearchBuildNo(), true );
		}

		if ( TriStringUtils.isNotEmpty( relUnitSearchBean.getSearchBuildName() )) {
			condition.setBpNm( relUnitSearchBean.getSearchBuildName() );
		}

	}

	/**
	 * ビルドパッケージ一覧検索の結果に、検索条件を適用する。
	 * @param entities リリースエンティティ
	 * @param relUnitSearchBean 検索条件格納ビーン
	 * @param 検索条件に合致したビルドパッケージビーンのリスト
	 */
	public List<IBpDto> applySearchCondition(
			List<IBpDto> entities,
			RelUnitSearchBean relUnitSearchBean ) {


		String searchRelNo = relUnitSearchBean.getSearchRelNo();
		String searchPjtNo = relUnitSearchBean.getSearchPjtNo();
		String searchChangeCauseNo = relUnitSearchBean.getSearchChangeCauseNo();

		if ( TriStringUtils.isEmpty( searchRelNo ) &&
				TriStringUtils.isEmpty( searchChangeCauseNo ) ) {
			// 検索条件指定なし
			return entities;
		}

		List<IBpDto> bpDtoList = new ArrayList<IBpDto>();
		//BPごとに検索条件に該当するものを判定して抽出する。該当しないものはcontinueで飛ばす。
		for ( IBpDto bpDto : entities ) {

			if ( TriStringUtils.isNotEmpty( searchRelNo ) ) {
				//リリース番号での検索条件チェック
				RpBpLnkCondition condition = (RpBpLnkCondition)DBSearchConditionAddonUtil.getRpBpLnkConditionByBuildNo( new String[]{ bpDto.getBpEntity().getBpId() } );
				condition.setRpId( searchRelNo, true );

				int relCount = this.getRmFinderSupport().getRpBpLnkDao().count( condition.getCondition() );

				if ( 0 == relCount ){
					continue;
				}
			}

			if ( TriStringUtils.isNotEmpty( searchChangeCauseNo ) ) {
				Set<String> pjtList = new HashSet<String>();

				List<IBpAreqLnkEntity> bpEntities = bpDto.getBpAreqLnkEntities();
				if ( TriStringUtils.isNotEmpty(bpEntities) ) {
					for ( IBpAreqLnkEntity buildbpEntityApplyEntity : bpEntities ) {
						pjtList.add( this.getAmFinderSupport().findAreqEntity(buildbpEntityApplyEntity.getAreqId()).getPjtId() );
					}
				}

				if ( 0 == pjtList.size() ){
					continue;
				}
				PjtCondition condition = new PjtCondition();

				if ( TriStringUtils.isNotEmpty( searchPjtNo ) ) {
					condition.setPjtId( searchPjtNo, true );
				}else{
					condition.setPjtIds(pjtList.toArray(new String[0]));
				}
				condition.setChgFactorNo( searchChangeCauseNo, true );

				int pjtCount = this.getAmFinderSupport().getPjtDao().count( condition.getCondition() );
				if ( 0 == pjtCount ) {
					continue;
				}
			}
			bpDtoList.add( bpDto );
		}
		return bpDtoList;
	}

	/**
	 * ビルドパッケージ作成可能な返却申請、削除申請を持つ変更管理の変更管理番号取得
	 * @param lotId ロット番号
	 * @return 変更管理番号ビーン
	 */
	public PjtNoBean getPjtNoByGeneratableRelUnit( String lotId ) {

		// ビルドパッケージ作成可能な返却承認
		IJdbcCondition condition =
				DBSearchConditionAddonUtil.getAreqConditionByGeneratableRelUnitByLotNo( lotId );

		IEntityLimit<IAreqEntity> limit = this.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 );


		// 変更管理番号を抽出
		Set<String> closedPjtIdSet		= new HashSet<String>();
		Set<String> approvedPjtNoSet	= new HashSet<String>();

		for ( IAreqEntity entity : limit.getEntities() ) {

			if ( BmBusinessJudgUtils.isOverRelUnitClose( entity ) ) {
				closedPjtIdSet.add( entity.getPjtId() );
			} else {
				approvedPjtNoSet.add( entity.getPjtId() );
			}
		}

		FlowRelUnitEditSupport.PjtNoBean bean = this.newPjtNoBean();
		bean.setClosedPjtNo		( closedPjtIdSet );
		bean.setApprovedPjtNo	( approvedPjtNoSet );

		return bean;
	}

	/**
	 * ビルドパッケージ作成対象の返却、削除申請情報のリストを取得する。
	 * @param lotId ロット番号
	 * @param pjtNoArray 変更管理番号
	 * @param pjtNoBean 変更管理番号ビーン
	 * @return ビルドパッケージ作成対象の返却、削除申請情報のリスト
	 */
	public List<ApplyInfoViewBean> getApplyInfoViewBeanList(
			String lotId, String[] pjtNoArray, PjtNoBean pjtNoBean ) {

		return getApplyInfoViewBeanList( lotId , pjtNoArray , pjtNoBean , true ) ;

	}
	/**
	 * ビルドパッケージ作成対象の返却、削除申請情報のリストを取得する。
	 * @param lotIds ロット番号
	 * @param pjtNoArray 変更管理番号
	 * @param pjtNoBean 変更管理番号ビーン
	 * @return ビルドパッケージ作成対象の返却、削除申請情報のリスト
	 */
	public List<ApplyInfoViewBean> getApplyInfoViewBeanList(
			String lotIds, String[] pjtNoArray, PjtNoBean pjtNoBean , boolean isAssetCheck ) {

		ILotDto lotDto = this.getAmFinderSupport().findLotDto(lotIds);
		List<Object> paramList	= new ArrayList<Object>();
		paramList.add( lotDto );

		List<ApplyInfoViewBean> applyInfoViewBeanList =
				new ArrayList<ApplyInfoViewBean>();

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();

		// ビルドパッケージ作成可能な返却、削除承認
		IJdbcCondition condition =
				DBSearchConditionAddonUtil.getAreqConditionByGeneratableRelUnitByPjtNo( pjtNoArray );

		List<IAreqEntity> areqEntities =
				this.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 ).getEntities();
		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}


		Set<String> closedPjtIdSet		= pjtNoBean.getClosedPjtNo();
		Set<String> approvedPjtNoSet	= pjtNoBean.getApprovedPjtNo();

		for ( IAreqDto entity : areqDtoEntities ) {

			String pjtNo = entity.getAreqEntity().getPjtId();

			// 承認済の申請情報のみを含む変更管理番号でステータスがクローズ済のデータは廃棄
			if ( !closedPjtIdSet.contains( pjtNo ) && approvedPjtNoSet.contains( pjtNo ) ) {
				if ( BmBusinessJudgUtils.isOverRelUnitClose( entity.getAreqEntity() ) )
					continue;
			}
			// クローズ済の申請情報のみを含む変更管理番号でステータスが承認済のデータは廃棄
			if ( closedPjtIdSet.contains( pjtNo ) && !approvedPjtNoSet.contains( pjtNo ) ) {
				if ( ! BmBusinessJudgUtils.isOverRelUnitClose( entity.getAreqEntity() ) )
					continue;
			}


			// 資産が壊れていないかをチェック
			if( isAssetCheck) {
				assetCheck( paramList, entity );
			}

			ApplyInfoViewBean applyInfoViewBean = new ApplyInfoViewBean();

			applyInfoViewBean.setPjtNo		( pjtNo );
			applyInfoViewBean.setApplyNo	( entity.getAreqEntity().getAreqId() );
			applyInfoViewBean.setApplyStatus( entity.getAreqEntity().getProcStsId() );

			applyInfoViewBeanList.add( applyInfoViewBean );
		}

		return applyInfoViewBeanList;

	}

	/**
	 * 返却、削除申請の資産の妥当性チェックを行います。
	 * @param paramList コンバート間に流通させるビーン
	 * @param entity
	 * @throws BaseBusinessException
	 */
	private void assetCheck(
			List<Object> paramList, IAreqDto entity ) throws TriSystemException {

		// クローズ済みの資産はすでにSCMに反映されているのでチェックしない
		if ( BmBusinessJudgUtils.isOverRelUnitClose( entity.getAreqEntity() ) )
			return;

		if ( BmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) {

			if ( null == entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) return;

			File masterWorkPath = BmDesignBusinessRuleUtils.getMasterWorkPath( paramList );

			for ( IAreqFileEntity assetFileEntity : entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {

				String path = assetFileEntity.getFilePath();
				File file = new File( masterWorkPath, path );

				if ( ! file.isFile() && ! file.isDirectory() ) {
					throw new TriSystemException(BmMessageId.BM004007F ,entity.getAreqEntity().getAreqId() , path );
				}
			}

		} else if ( BmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() )) {

			File path =
					BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, entity.getAreqEntity() );

			if ( !path.exists() ) {
				throw new TriSystemException(BmMessageId.BM004008F , entity.getAreqEntity().getAreqId() , path.getAbsolutePath() );
			}

			if ( 0 == this.getAssetCount( path )) {
				throw new TriSystemException(BmMessageId.BM004009F , entity.getAreqEntity().getAreqId() , path.getAbsolutePath() );
			}
		}
	}

	/**
	 * 指定されたディレクトリ配下の資産数(ファイル数)をカウントする。
	 * @param targetDir 指定ディレクトリ
	 * @return 資産数
	 */
	private int getAssetCount( File targetDir ) throws BaseBusinessException {

		if ( !targetDir.exists() ) {
			throw new TriSystemException(BmMessageId.BM004010F , targetDir.getAbsolutePath() );
		}

		int count = 0;

		File[] files = targetDir.listFiles();

		for ( File file : files ) {

			if ( file.isDirectory() ) {
				count = count + getAssetCount( file );
			} else {
				count++;
			}
		}

		return count;

	}

	/**
	 * 申請情報のソート情報を取得する。
	 * @return 申請情報のソート情報
	 */
	public static ISqlSort getAssetApplySort() {

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.areqId, TriSortOrder.Desc, 1);

		return sort;
	}

	/**
	 * ビルドパッケージエンティティから変更管理エンティティを取得します。
	 * @param entities ビルドパッケージエンティティ
	 * @return 変更管理エンティティ
	 */
	public IPjtEntity[] getPjtEntity( List<IBpDto> bpDtoList ) {

		// ビルドパッケージされた変更管理情報
		String[] pjtNo = getPjtIds( bpDtoList );

		return this.getPjtEntity( pjtNo );

	}

	/**
	 * 対象ロットのビルドパッケージエンティティを取得する
	 * @param lotId 対象ロット番号
	 * @return ビルドパッケージエンティティ
	 */
	public IBpEntity[] getBpEntityByLotNo( String lotId ) {

		BpCondition condition = new BpCondition();
		condition.setLotId( lotId );

		IEntityLimit<IBpEntity> limit =
				this.getBpDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IBpEntity[0]);
	}

	/**
	 * 対象ロットに含まれるビルドパッケージクローズエラーとなったエンティティを取得する
	 * @param lotId 対象ロット番号
	 * @return ビルドパッケージエンティティ
	 */
	public IBpEntity[] getBuildEntityByRelUnitCloseError( String lotId ) {

		BpCondition condition = new BpCondition();
		condition.setLotId( lotId );
		condition.setProcStsIds( new String[]{ BmBpStatusIdForExecData.BuildPackageCloseError.getStatusId() } );

		IEntityLimit<IBpEntity> limit =
				this.getBpDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IBpEntity[0]);
	}

	/**
	 * ビルドパッケージエンティティから変更管理番号を取得する。
	 * @param entities ビルドパッケージエンティティ
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtNo( IPjtEntity[] entities ) {

		Set<String> pjtIdSet = new TreeSet<String>();

		for ( IPjtEntity entity : entities ) {
			pjtIdSet.add( entity.getPjtId() );
		}

		return (String[])pjtIdSet.toArray( new String[0] );
	}

	/**
	 * 変更管理エンティティから変更管理番号を取得する。
	 * @param entities 変更管理エンティティ
	 * @return 変更管理番号の配列
	 */
	public String[] getPjtIds( List<IBpDto> bpDtoList ) {

		List<IBpAreqLnkEntity> entityList = new ArrayList<IBpAreqLnkEntity>();
		for ( IBpDto entity : bpDtoList ) {
			entityList.addAll( entity.getBpAreqLnkEntities() );
		}


		Set<String> pjtIdSet = new TreeSet<String>();

		for ( IBpAreqLnkEntity entity : entityList ) {
			pjtIdSet.add( this.getAmFinderSupport().findAreqEntity(entity.getAreqId()).getPjtId() );
		}

		return (String[])pjtIdSet.toArray( new String[0] );
	}

	/**
	 * ビルドパッケージエンティティから申請情報エンティティを取得します。
	 * @param entities ビルドパッケージエンティティ
	 * @param selectPageNo 選択ページ
	 * @return 申請情報エンティティ
	 */
	public IAreqEntity[] getAssetApplyEntityFromDto( List<IBpDto> entities ) {

		// ビルドパッケージされた申請情報
		String[] applyNo = BmExtractEntityAddonUtils.getAreqIds( entities );


		if ( TriStringUtils.isEmpty( applyNo )) {

			// IBuildApplyEntityの登録前に死んだビルドエンティティ対応
			// 全資産コンパイルもかな？
			return new IAreqEntity[]{};

		} else {

			return this.getAreqEntity( applyNo );
		}
	}
	/**
	 * モジュールエンティティからモジュール名を抽出する。
	 * @param moduleEntities モジュールエンティティの配列
	 * @return モジュール名のセット
	 */
	public static Set<String> getModuleNameSet(  List<IBpMdlLnkEntity> moduleEntities ) {

		Set<String> moduleNameSet = new LinkedHashSet<String>();

		for ( IBpMdlLnkEntity entity : moduleEntities ) {
			moduleNameSet.add( entity.getMdlNm() );
		}

		return moduleNameSet;
	}

	/**
	 * 申請情報画面用ビーンから申請情報エンティティを取得する
	 * @param applyInfoViewBeanList 申請情報画面用ビーンのリスト
	 * @return 申請情報エンティティのリスト
	 */
	public List<IAreqEntity> getAssetApplyEntity(
			List<ApplyInfoViewBean> applyInfoViewBeanList ) {

		List<IAreqEntity> entityList = new ArrayList<IAreqEntity>();

		// 全資産コンパイル対応
		if ( null == applyInfoViewBeanList ) return entityList;


		for ( ApplyInfoViewBean applyInfoViewBean : applyInfoViewBeanList ) {

			IAreqEntity entity =
					getAmFinderSupport().findAreqEntity( applyInfoViewBean.getApplyNo() );

			entityList.add( entity );
		}

		return entityList;
	}
	/**
	 * 申請情報画面用ビーンから申請情報エンティティを取得する
	 * @param applyInfoViewBeanList 申請情報画面用ビーンのリスト
	 * @return 申請情報エンティティのリスト
	 */
	public List<IAreqDto> getAreqEntity(
			List<ApplyInfoViewBean> applyInfoViewBeanList ) {

		List<IAreqDto> entityList = new ArrayList<IAreqDto>();

		// 全資産コンパイル対応
		if ( null == applyInfoViewBeanList ) return entityList;


		for ( ApplyInfoViewBean applyInfoViewBean : applyInfoViewBeanList ) {

			IAreqEntity entity =
					getAmFinderSupport().findAreqEntity( applyInfoViewBean.getApplyNo() );

			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( entity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( entity.getAreqId() );

			areqDto.setAreqFileEntities( this.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );

			entityList.add( areqDto );
		}

		return entityList;
	}
	/**
	 * 変更管理番号のリストから変更管理エンティティを取得する。
	 * @param pjtNo 変更管理番号のリスト
	 * @param selectPageNo 選択ページ番号
	 * @param viewRows ページあたりの行数
	 * @param sort ソート条件
	 * @return 変更管理エンティティ
	 */
	public IEntityLimit<IPjtEntity> getPjtEntityLimit(
			String[] pjtNo, int selectPageNo, int viewRows, ISqlSort sort ) {

		String[] conditionPjtNo = pjtNo;

		if ( TriStringUtils.isEmpty( conditionPjtNo ) ) {
			//ダミーの変更管理を設定して、全件検索を防止する。
			conditionPjtNo = new String[]{ "" };
		}

		IJdbcCondition condition	= DBSearchConditionAddonUtil.getPjtCondition( conditionPjtNo );

		IEntityLimit<IPjtEntity> entityLimit =
				this.getAmFinderSupport().getPjtDao().find(	condition.getCondition(), sort, selectPageNo, viewRows );

		return entityLimit;
	}
	/**
	 *
	 * ビルドパッケージに紐付く変更管理番号情報を取得する
	 *
	 * @param buildEntityArray ビルドパッケージレコードの配列
	 * @param sort ソート条件
	 * @return 変更管理情報エンティティの配列
	 */
	public IPjtEntity[] getPjtEntityArray( List<IBpDto> buildEntityArray, ISqlSort sort ) {
		Set<String> pjtIdSet = new HashSet<String>() ;
		for( IBpDto buildEntity : buildEntityArray ) {
			List<IBpAreqLnkEntity> bpAreqEntities = buildEntity.getBpAreqLnkEntities() ;
			for( IBpAreqLnkEntity bpAreqEntity : bpAreqEntities ) {
				pjtIdSet.add( this.getAmFinderSupport().findAreqEntity(bpAreqEntity.getAreqId()).getPjtId() ) ;
			}
		}
		String[] pjtNoArray = pjtIdSet.toArray( new String[ 0 ] ) ;

		IEntityLimit<IPjtEntity> pjtEntityLimit = this.getPjtEntityLimit( pjtNoArray , 1 , 0, sort ) ;
		IPjtEntity[] pjtEntityArray = pjtEntityLimit.getEntities().toArray(new IPjtEntity[0]) ;
		return pjtEntityArray ;
	}
	/**
	 * 別々の返却申請情報に付属する同じパスの資産が、同じ内容であるかどうかをチェックする
	 * @param lotDto ロットエンティティ
	 * @param origEntity 比較元返却申請エンティティ
	 * @param destEntity 比較先返却申請エンティティ
	 * @param assetPath 資産パス
	 * @return 内容が同じであればtrue、そうでなければfalse
	 */
	public boolean isSameContents(
			ILotDto lotDto, IAreqEntity origEntity,
			IAreqEntity destEntity, String assetPath ) throws IOException {

		List<Object> paramList = new ArrayList<Object>();
		paramList.add( lotDto );

		File targetFile = new File(
				BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, origEntity ),
				assetPath );

		File checkFile = new File(
				BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, destEntity ),
				assetPath );

		return TriFileUtils.isSame( targetFile, checkFile );

	}

	/**
	 * 比較元エンティティが返却承認、かつ比較先エンティティがクローズ済みの場合に、
	 * 比較先のクローズ日時＜比較元の貸出日時であるかどうかをチェックする
	 * @param origEntity 比較元返却申請エンティティ
	 * @param destEntity 比較先返却申請エンティティ
	 * @return 比較元が返却承認、比較先がクローズ済み以外であればfalse<br>
	 *          比較元が返却承認、比較先がクローズ済みで、比較元の貸出日時≦比較先のクローズ日時であればfalse<br>
	 *          比較元がクローズ済み、比較先が返却承認で、比較先の貸出日時≦比較元のクローズ日時であればfalse<br>
	 *          上記以外はtrue
	 */
	public boolean isCloseDateOlderLendDate(
			IAreqEntity origEntity, IAreqEntity destEntity ) {

		String origStatus = origEntity.getStsId();
		String destStatus = destEntity.getStsId();

		if ( AmAreqStatusId.CheckinRequestApproved.equals( origStatus ) &&
				!AmAreqStatusId.BuildPackageClosed.equals( destStatus )) {
			return false;
		}

		if ( AmAreqStatusId.CheckinRequestApproved.equals( origStatus ) &&
				AmAreqStatusId.BuildPackageClosed.equals( destStatus )) {

			Timestamp targetDate	= origEntity.getLendReqTimestamp();
			// クローズ時にInsertUpdateDateが更新されて、そのままの値のはず
			Timestamp checkDate	=  destEntity.getUpdTimestamp();

			if ( !checkDate.before( targetDate )) return false;
		}

		if ( AmAreqStatusId.BuildPackageClosed.equals( origStatus ) &&
				AmAreqStatusId.CheckinRequestApproved.equals( destStatus )) {

			Timestamp targetDate	= origEntity.getUpdTimestamp();
			// クローズ時にInsertUpdateDateが更新されて、そのままの値のはず
			Timestamp checkDate	= destEntity.getLendReqTimestamp();

			if ( !targetDate.before( checkDate )) return false;
		}

		return true;

	}

	/**
	 * 資産とその資産を含む申請情報リストのマップを取得する。
	 * @param entities 申請情報配列
	 * @return 資産とその資産を含む申請情報リストのマップ
	 */
	public Map<String,List<IAreqEntity>>
	getReturnAssetPathAsseApplyEntityMap( List<IAreqDto> entities ) {

		Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap =
				new HashMap<String,List<IAreqEntity>>();

		for ( IAreqDto entity : entities ) {

			if ( !BmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() ) ) continue;

			for ( IAreqFileEntity fileEntity : entity.getAreqFileEntities(AreqCtgCd.ReturningRequest) ) {

				String path = fileEntity.getFilePath();

				if ( !assetPathAssetApplyEntityListMap.containsKey( path )) {
					assetPathAssetApplyEntityListMap.put( path, new ArrayList<IAreqEntity>() );
				}

				assetPathAssetApplyEntityListMap.get( path ).add( entity.getAreqEntity() );
			}
		}

		return assetPathAssetApplyEntityListMap;
	}

	/**
	 * 変更管理番号の配列を保持するクラス
	 */
	public PjtNoBean newPjtNoBean() {
		return new PjtNoBean();
	}

	public class PjtNoBean {

		private Set<String> closedPjtNo	= new HashSet<String>();
		private Set<String> approvedPjtNo	= new HashSet<String>();

		public Set<String> getClosedPjtNo() {
			return closedPjtNo;
		}
		public void setClosedPjtNo( Set<String> closedPjtNo ) {
			this.closedPjtNo = closedPjtNo;
		}

		public Set<String> getApprovedPjtNo() {
			return approvedPjtNo;
		}
		public void setApprovedPjtNo( Set<String> approvedPjtNo ) {
			this.approvedPjtNo = approvedPjtNo;
		}
	}

	/**
	 * リリースエンティティの比較クラス
	 */
	private class RelEntityComparator implements Comparator<IRpEntity> {

		public int compare( IRpEntity entity1, IRpEntity entity2 ) {

			String relNo1 = entity1.getRpId();
			String relNo2 = entity2.getRpId();

			return relNo1.compareTo( relNo2 );
		}
	}

	/**
	 * 変更管理一覧画面の表示項目を、変更管理ティティから取得して設定する。
	 * @param pjtViewBeanList 変更管理一覧画面の表示項目
	 * @param entities 変更管理エンティティ
	 */
	private void setPjtViewBeanPjtEntity(
			List<PjtViewBean> pjtViewBeanList, IPjtEntity[] entities ) {

		for ( IPjtEntity entity : entities ) {

			PjtViewBean viewBean = new PjtViewBean();

			viewBean.setPjtNo				( entity.getPjtId() );
			viewBean.setChangeCauseNo		( entity.getChgFactorNo() );
			viewBean.setChangeCauseClassify	(
					sheet().getValue(
							AmDesignBeanId.changeCauseClassifyId,
							entity.getChgFactorId() ));
			viewBean.setPjtSummary			( entity.getSummary() );

			pjtViewBeanList.add( viewBean );
		}
	}
	/**
	 * ロットIDとモジュール名からモジュール単位のVCSタグを取得します。
	 * @param lotId
	 * @param moduleName
	 * @return String モジュール単位のVCSタグ
	 */
	public String getMdlVerTagFromLotIdAndMdlName(String lotId , String moduleName) {

		LotMdlLnkCondition lotCondition = new LotMdlLnkCondition();
		lotCondition.setLotId( lotId );
		lotCondition.setMdlNm( moduleName );
		ILotMdlLnkEntity lotMdlLnkEntity = this.getAmFinderSupport().getLotMdlLnkDao().findByPrimaryKey(lotCondition.getCondition());
		return lotMdlLnkEntity.getMdlVerTag();
	}
	/**
	 * ビルドパッケージ・資産申請(Link)の更新を行います。
	 * 更新方法は一度deleteを行ってから、insertします。
	 * @param buildNo
	 * @param entityList
	 */
	public void updateBpAreqLnk( String buildNo , List<IAreqDto> entityList ){

		BpAreqLnkCondition condition = new BpAreqLnkCondition();
		condition.setBpId(buildNo);
		this.getBpAreqLnkDao().delete(condition.getCondition());

		List<IBpAreqLnkEntity> applyEntityList = new ArrayList<IBpAreqLnkEntity>();
		for ( IAreqDto applyEntity : entityList ) {

			IBpAreqLnkEntity buildApplyEntity = new BpAreqLnkEntity();
			buildApplyEntity.setBpId(buildNo);
			buildApplyEntity.setAreqId	( applyEntity.getAreqEntity().getAreqId() );

			applyEntityList.add( buildApplyEntity );
		}
		this.getBpAreqLnkDao().insert(applyEntityList);
	}
	/**
	 *
	 * @param buildNo
	 * @param lotId
	 * @param moduleNameSet
	 */
	public void updateBpMdlLnk( String buildNo, String lotId , Set<String> moduleNameSet ) {
		// ビルドパッケージ・モジュール(Link) メソッド化
		BpMdlLnkCondition mcondition = new BpMdlLnkCondition();
		mcondition.setBpId(buildNo);
		this.getBpMdlLnkDao().delete(mcondition.getCondition());

		List<IBpMdlLnkEntity> moduleEntityList = new ArrayList<IBpMdlLnkEntity>();

		for ( String moduleName : moduleNameSet ) {

			IBpMdlLnkEntity moduleEntity = new BpMdlLnkEntity();
			moduleEntity.setBpId( buildNo );
			moduleEntity.setMdlNm( moduleName );
			moduleEntity.setMdlVerTag(
					this.getMdlVerTagFromLotIdAndMdlName( lotId, moduleName ) );
			moduleEntityList.add( moduleEntity );
		}
		this.getBpMdlLnkDao().insert(moduleEntityList);
	}

	/**
	 * ビルドパッケージIDからビルドパッケージDTOを取得する
	 * @param condition
	 * @param sort
	 * @return ビルドパッケージDto(リミット)
	 */
	public List<IBpDto> getBpDtoLimitList( IJdbcCondition condition , ISqlSort sort ) {

		List<IBpEntity> bpEntities = this.getBpDao().find( condition.getCondition(), sort );

		if ( TriStringUtils.isEmpty(bpEntities) ) {
			throw new TriSystemException(BmMessageId.BM004005F );
		}

		return this.findBpDtoList( bpEntities );
	}

	private IDesignSheet sheet() {

		if(sheet == null) {
			sheet = DesignSheetFactory.getDesignSheet();
		}

		return sheet;
	}

}
