package jp.co.blueship.tri.bm.support;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.BpDtoList;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskFlowCondition;
import jp.co.blueship.tri.dcm.support.IDcmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.dao.orm.ISqlCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.RmFluentFunctionUtils;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpDto;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;


/**
 * BM関連情報を検索するためのサービス機能を提供するクラスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class BmFinderSupport extends BmDaoFinder implements IBmFinderSupport {

	private ISmFinderSupport smFinderSupport = null;
	private IUmFinderSupport umFinderSupport = null;
	private IAmFinderSupport amFinderSupport = null;
	private IRmFinderSupport rmFinderSupport = null;
	private IDcmFinderSupport dcmFinderSupport = null;


	@Override
	public ISmFinderSupport getSmFinderSupport() {
	    return smFinderSupport;
	}

	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
	    this.smFinderSupport = smFinderSupport;
	}

	@Override
	public IUmFinderSupport getUmFinderSupport() {
	    return umFinderSupport;
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
	    this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IAmFinderSupport getAmFinderSupport() {
	    return amFinderSupport;
	}

	public void setAmFinderSupport(IAmFinderSupport amFinderSupport) {
	    this.amFinderSupport = amFinderSupport;
	}

	@Override
	public IRmFinderSupport getRmFinderSupport() {
	    return rmFinderSupport;
	}

	public void setRmFinderSupport(IRmFinderSupport rmFinderSupport) {
	    this.rmFinderSupport = rmFinderSupport;
	}

	@Override
	public IDcmFinderSupport getDcmFinderSupport() {
	    return dcmFinderSupport;
	}

	public void setDcmFinderSupport(IDcmFinderSupport dcmFinderSupport) {
	    this.dcmFinderSupport = dcmFinderSupport;
	}


	@Override
	public final IBldSrvEntity findBldSrvEntity( String bldSrvId ) throws TriSystemException {
		BldSrvCondition condition = new BldSrvCondition();

		condition.setBldSrvId(bldSrvId);
		IBldSrvEntity bldSrvEntity = this.getBldSrvDao().findByPrimaryKey( condition.getCondition() );

		if ( TriStringUtils.isEmpty(bldSrvEntity) ){
			throw new TriSystemException(BmMessageId.BM004079F, BmTables.BM_BLD_SRV.name(), bldSrvId);
		}

		return bldSrvEntity;
	}

	@Override
	public final IBldSrvEntity findBldSrvEntityByController() throws TriSystemException {
		BldSrvCondition condition = new BldSrvCondition();

		condition.setIsAgent( StatusFlg.off );
		List<IBldSrvEntity> bldSrvEntities = this.getBldSrvDao().find( condition.getCondition() );

		if ( TriStringUtils.isEmpty(bldSrvEntities) ){
			throw new TriSystemException(BmMessageId.BM004076F, BmTables.BM_BLD_SRV.name());
		}

		if ( 1 < bldSrvEntities.size() ) {
			throw new TriSystemException(BmMessageId.BM004075F, BmTables.BM_BLD_SRV.name());
		}

		return bldSrvEntities.get(0);
	}

	@Override
	public IBldEnvEntity findBldEnvEntity( String bldEnvId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(bldEnvId) ){
			throw new TriSystemException(BmMessageId.BM004077F, BmTables.BM_BLD_ENV.name(), "empty");
		}

		BldEnvCondition condition = new BldEnvCondition();
		condition.setBldEnvId( bldEnvId );

		IBldEnvEntity bldEnvEntity = this.getBldEnvDao().findByPrimaryKey( condition.getCondition() );

		if ( null == bldEnvEntity ){
			throw new TriSystemException(BmMessageId.BM004077F, BmTables.BM_BLD_ENV.name(), bldEnvId);
		}

		return bldEnvEntity;
	}

	@Override
	public final IBldEnvSrvEntity findBldEnvSrvEntity( String envId, String srvId) throws TriSystemException {
		if ( TriStringUtils.isEmpty(envId)
			&& TriStringUtils.isEmpty(srvId) ){
			throw new TriSystemException(BmMessageId.BM004078F, BmTables.BM_BLD_ENV_SRV.name(), "empty", "empty");
		}

		if ( TriStringUtils.isEmpty(envId) ){
			throw new TriSystemException(BmMessageId.BM004078F, BmTables.BM_BLD_ENV_SRV.name(), "empty", srvId);
		}
		if ( TriStringUtils.isEmpty(srvId) ){
			throw new TriSystemException(BmMessageId.BM004078F, BmTables.BM_BLD_ENV_SRV.name(), envId, "empty");
		}

		BldEnvSrvCondition condition = new BldEnvSrvCondition();
		condition.setBldEnvId(envId);
		condition.setBldSrvId(srvId);
		IBldEnvSrvEntity bldEnvSrvEntity = this.getBldEnvSrvDao().findByPrimaryKey( condition.getCondition() );

		if ( null == bldEnvSrvEntity ){
			throw new TriSystemException(BmMessageId.BM004078F , BmTables.BM_BLD_ENV_SRV.name(), envId , srvId );
		}

		return bldEnvSrvEntity;
	}

	@Override
	public final ITaskFlowEntity findTaskFlowEntity( String taskFlowId ) {

		if ( TriStringUtils.isEmpty(taskFlowId) )
			throw new TriSystemException(BmMessageId.BM005094S, BmTables.BM_TASK_FLOW.name(), "empty");

		TaskFlowCondition condition = new TaskFlowCondition();
		condition.setTaskFlowId(taskFlowId);

		ITaskFlowEntity taskEntity =
			this.getTaskFlowDao().findByPrimaryKey( condition.getCondition() );

		if( null == taskEntity ) {
			throw new TriSystemException(BmMessageId.BM005094S, BmTables.BM_TASK_FLOW.name(), taskFlowId);
		}

		return taskEntity;
	}

	@Override
	public IBpEntity findBpEntity( String bpId ) throws TriSystemException {

		if ( TriStringUtils.isEmpty(bpId) ){
			throw new TriSystemException(BmMessageId.BM005095S, "empty");
		}

		BpCondition condition = new BpCondition();
		condition.setBpId( bpId );

		IBpEntity bpEntity = this.getBpDao().findByPrimaryKey( condition.getCondition() );

		if ( null == bpEntity ){
			throw new TriSystemException(BmMessageId.BM005095S , bpId);
		}

		return bpEntity;
	}

	@Override
	public List<IBpEntity> findBpEntities( String... bpIds ) {

		if ( TriStringUtils.isEmpty(bpIds) ){
			throw new TriSystemException(BmMessageId.BM005095S, "empty");
		}

		BpCondition condition = new BpCondition();
		if ( 1 == bpIds.length ) {
			condition.setBpId( bpIds[0] );
		} else {
			condition.setBpIds( bpIds );
		}

		List<IBpEntity> bpEntities = this.getBpDao().find( condition.getCondition() );

		return bpEntities;
	}

	@Override
	public List<IBpEntity> findBpEntities( ISqlSort sort, String... bpIds ) {

		if ( TriStringUtils.isEmpty(bpIds) ){
			throw new TriSystemException(BmMessageId.BM005095S, "empty");
		}

		BpCondition condition = new BpCondition();
		if ( 1 == bpIds.length ) {
			condition.setBpId( bpIds[0] );
		} else {
			condition.setBpIds( bpIds );
		}

		List<IBpEntity> bpEntities;
		if ( null == sort ) {
			bpEntities = this.getBpDao().find( condition.getCondition() );
		} else {
			bpEntities = this.getBpDao().find( condition.getCondition(), sort );
		}

		return bpEntities;
	}

	@Override
	public final List<IBpMdlLnkEntity> findBpMdlLnkEntities( String bpId ) {
		if ( TriStringUtils.isEmpty(bpId) ){
			throw new TriSystemException(BmMessageId.BM005095S, "empty");
		}
		BpMdlLnkCondition condition = new BpMdlLnkCondition();
		condition.setBpId( bpId );
		List<IBpMdlLnkEntity> entities = this.getBpMdlLnkDao().find( condition.getCondition() );

		return entities;
	}

	/**
	 * ビルドパッケージ・資産申請情報を取得します。
	 *
	 * @param bpId ビルドパッケージID
	 * @return 取得したビルドパッケージ・資産申請エンティティを戻します。
	 */
	private final List<IBpAreqLnkEntity> findBpAreqLnkEntities( String bpId ) {
		if ( TriStringUtils.isEmpty(bpId) ){
			throw new TriSystemException(BmMessageId.BM005095S, "empty");
		}
		BpAreqLnkCondition condition = new BpAreqLnkCondition();
		condition.setBpId( bpId );
		List<IBpAreqLnkEntity> entities = this.getBpAreqLnkDao().find( condition.getCondition() );

		return entities;
	}

	@Override
	public List<IBpEntity> findBpEntitiesFromLotId( String lotId ){
		List<IBpEntity> bpEntities = new ArrayList<IBpEntity>();

		BpCondition condition = new BpCondition();
		condition.setLotId(lotId);
		this.getBpDao().find(condition.getCondition());

		bpEntities = this.getBpDao().find(condition.getCondition());

		return bpEntities;
	}

	@Override
	public List<IBpEntity> findBpEntitiesFromRpId( String rpId ){
		List<IBpEntity> bpEntities = new ArrayList<IBpEntity>();

		IRpDto rpDto = this.getRmFinderSupport().findRpDto( rpId );

		for ( IRpBpLnkEntity lnkEntity : rpDto.getRpBpLnkEntities() ) {
			bpEntities.add( findBpEntity(lnkEntity.getBpId() ) );
		}
		return bpEntities;
	}

	/**
	 *ビルドパッケージEntityの子Entityを取得しDTOに設定します。
	 *
	 * @param bpDto ビルドパッケージDTO
	 * @param bpEntity ビルドパッケージEntity
	 * @param tables 取得対象の以下の子テーブル。指定しない場合はすべての子テーブルを取得します。
	 * @return
	 */
	private final IBpDto setBpDto( IBpDto bpDto, IBpEntity bpEntity, BmTables... tables ) {
		bpDto.setBpEntity( bpEntity );

		BmTables[] findTables = tables;
		if ( TriStringUtils.isEmpty(tables) ) {
			findTables = new BmTables[]{
					BmTables.BM_BP_MDL_LNK,
					BmTables.BM_BP_AREQ_LNK,
			};
		}

		String bpId = bpEntity.getBpId();

		for ( BmTables table: findTables ) {
			if ( table.equals( BmTables.BM_BP_MDL_LNK ) ) {
				bpDto.setBpMdlLnkEntities(this.findBpMdlLnkEntities( bpId ));
			}
			if ( table.equals( BmTables.BM_BP_AREQ_LNK ) ) {
				bpDto.setBpAreqLnkEntities(this.findBpAreqLnkEntities( bpId ));
			}
		}

		return bpDto;
	}

	@Override
	public final IBpDto findBpDto( String bpId, BmTables... tables ) throws TriSystemException {
		IBpEntity bpEntity = findBpEntity( bpId );
		return this.setBpDto(new BpDto(), bpEntity, tables);
	}

	@Override
	public final IBpDto findBpDto( IBpEntity bpEntity, BmTables... tables ) throws TriSystemException {
		return this.setBpDto(new BpDto(), bpEntity, tables);
	}

	@Override
	public final List<IBpDto> findBpDto( String[] bpIds, BmTables... tables ) {
		List<IBpDto> bpDtoList = new ArrayList<IBpDto>();

		List<IBpEntity> bpEntities = findBpEntities( bpIds );

		for ( IBpEntity bpEntity: bpEntities ) {
			bpDtoList.add( this.setBpDto( new BpDto(), bpEntity, tables) );
		}

		return bpDtoList;
	}

	@Override
	public final List<IBpDto> findBpDto( List<IBpEntity> bpEntities, BmTables... tables ) {
		List<IBpDto> bpDtoList = new ArrayList<IBpDto>();

		for ( IBpEntity bpEntity: bpEntities ) {
			bpDtoList.add( this.setBpDto( new BpDto(), bpEntity, tables) );
		}

		return bpDtoList;
	}

	@Override
	public final List<IBpDto> findBpDto( ISqlCondition condition, BmTables... tables ) {
		List<IBpDto> bpDtoList = new ArrayList<IBpDto>();

		List<IBpEntity> bpEntities = this.getBpDao().find( condition );

		for ( IBpEntity bpEntity: bpEntities ) {
			bpDtoList.add( this.setBpDto( new BpDto(), bpEntity, tables) );
		}

		return bpDtoList;
	}

	@Override
	public BpDtoList findBpDtoList( List<IBpEntity> bpEntities, BmTables... tables ) {

		BpDtoList bpDtoList = new BpDtoList();
		for ( IBpEntity bpEntity : bpEntities ) {
			bpDtoList.add( this.setBpDto( new BpDto(), bpEntity, tables ) );
		}

		return bpDtoList;
	}
	
	@Override
	public List<String> findRpIdsByBpIds( String... bpIds ) {
		
		
		List<IRpBpLnkEntity> entities = this.getRmFinderSupport().findRpBpLnkEntitiesFromBpId( bpIds );
		
		if ( null == entities ) {
			return null;
		}
		
		return FluentList.from( entities ).map( RmFluentFunctionUtils.toRpIdsFromBpId ).asList();
	}
}
