package jp.co.blueship.tri.bm.support;

import jp.co.blueship.tri.bm.dao.bldenv.IBldEnvDao;
import jp.co.blueship.tri.bm.dao.bldenv.IBldEnvSrvDao;
import jp.co.blueship.tri.bm.dao.bldsrv.IBldSrvDao;
import jp.co.blueship.tri.bm.dao.bldtimeline.IBldTimelineAgentDao;
import jp.co.blueship.tri.bm.dao.bldtimeline.IBldTimelineDao;
import jp.co.blueship.tri.bm.dao.bp.IBpAreqLnkDao;
import jp.co.blueship.tri.bm.dao.bp.IBpDao;
import jp.co.blueship.tri.bm.dao.bp.IBpMdlLnkDao;
import jp.co.blueship.tri.bm.dao.rmisvc.IRmiSvcDao;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowDao;
import jp.co.blueship.tri.bm.dao.taskflow.ITaskFlowResDao;
import jp.co.blueship.tri.bm.dao.taskflowproc.ITaskFlowProcDao;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.um.dao.hist.IBpHistDao;

/**
 * BM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public abstract class BmDaoFinder extends FinderSupport implements IBmDaoFinder {
	private IRmiSvcDao rmiSvcDao = null;
	private IBldSrvDao bldSrvDao = null;
	private IBldEnvDao bldEnvDao = null;
	private IBldEnvSrvDao bldEnvSrvDao = null;
	private IBldTimelineDao bldTimelineDao = null;
	private IBldTimelineAgentDao bldTimelineAgentDao = null;
	private ITaskFlowDao taskFlowDao = null;
	private ITaskFlowResDao taskFlowResDao = null;
	private ITaskFlowProcDao taskFlowProcDao = null;

	private IBpDao bpDao = null;
	private IBpMdlLnkDao bpMdlLnkDao = null;
	private IBpAreqLnkDao bpAreqLnkDao = null;
	private IBpHistDao bpHistDao = null;
	/**
	 * rmiSvcDaoを取得します。
	 * @return rmiSvcDao
	 */
	public IRmiSvcDao getRmiSvcDao() {
	    return rmiSvcDao;
	}

	@Override
	public IBldEnvDao getBldEnvDao() {
	    return bldEnvDao;
	}

	/**
	 * bldEnvDaoを設定します。
	 * @param bldEnvDao bldEnvDao
	 */
	public void setBldEnvDao(IBldEnvDao bldEnvDao) {
	    this.bldEnvDao = bldEnvDao;
	}

	/**
	 * rmiSvcDaoを設定します。
	 * @param rmiSvcDao rmiSvcDao
	 */
	public void setRmiSvcDao(IRmiSvcDao rmiSvcDao) {
	    this.rmiSvcDao = rmiSvcDao;
	}

	@Override
	public IBldSrvDao getBldSrvDao() {
	    return bldSrvDao;
	}

	/**
	 * bldSrvDaoを設定します。
	 * @param bldSrvDao bldSrvDao
	 */
	public void setBldSrvDao(IBldSrvDao bldSrvDao) {
	    this.bldSrvDao = bldSrvDao;
	}

	@Override
	public IBldEnvSrvDao getBldEnvSrvDao() {
	    return bldEnvSrvDao;
	}

	/**
	 * bldEnvSrvDaoを設定します。
	 * @param bldEnvSrvDao bldEnvSrvDao
	 */
	public void setBldEnvSrvDao(IBldEnvSrvDao bldEnvSrvDao) {
	    this.bldEnvSrvDao = bldEnvSrvDao;
	}

	@Override
	public IBldTimelineDao getBldTimelineDao() {
	    return bldTimelineDao;
	}

	/**
	 * bldTimelineDaoを設定します。
	 * @param bldTimelineDao bldTimelineDao
	 */
	public void setBldTimelineDao(IBldTimelineDao bldTimelineDao) {
	    this.bldTimelineDao = bldTimelineDao;
	}

	/**
	 * bldTimelineAgentDaoを取得します。
	 * @return bldTimelineAgentDao
	 */
	public IBldTimelineAgentDao getBldTimelineAgentDao() {
	    return bldTimelineAgentDao;
	}

	/**
	 * bldTimelineAgentDaoを設定します。
	 * @param bldTimelineAgentDao bldTimelineAgentDao
	 */
	public void setBldTimelineAgentDao(IBldTimelineAgentDao bldTimelineAgentDao) {
	    this.bldTimelineAgentDao = bldTimelineAgentDao;
	}

	@Override
	public ITaskFlowDao getTaskFlowDao() {
	    return taskFlowDao;
	}

	/**
	 * taskFlowDaoを設定します。
	 * @param taskFlowDao taskFlowDao
	 */
	public void setTaskFlowDao(ITaskFlowDao taskFlowDao) {
	    this.taskFlowDao = taskFlowDao;
	}

	@Override
	public ITaskFlowResDao getTaskFlowResDao() {
	    return taskFlowResDao;
	}

	/**
	 * taskFlowResDaoを設定します。
	 * @param taskFlowResDao taskFlowResDao
	 */
	public void setTaskFlowResDao(ITaskFlowResDao taskFlowResDao) {
	    this.taskFlowResDao = taskFlowResDao;
	}

	@Override
	public ITaskFlowProcDao getTaskFlowProcDao() {
	    return taskFlowProcDao;
	}

	/**
	 * taskFlowProcDaoを設定します。
	 * @param taskFlowProcDao taskFlowProcDao
	 */
	public void setTaskFlowProcDao(ITaskFlowProcDao taskFlowProcDao) {
	    this.taskFlowProcDao = taskFlowProcDao;
	}

	@Override
	public IBpDao getBpDao() {
	    return bpDao;
	}

	/**
	 * bpDaoを設定します。
	 * @param bpDao bpDao
	 */
	public void setBpDao(IBpDao bpDao) {
	    this.bpDao = bpDao;
	}

	/**
	 * bpMdlLnkDaoを取得します。
	 * @return bpMdlLnkDao
	 */
	public IBpMdlLnkDao getBpMdlLnkDao() {
	    return bpMdlLnkDao;
	}

	/**
	 * bpMdlLnkDaoを設定します。
	 * @param bpMdlLnkDao bpMdlLnkDao
	 */
	public void setBpMdlLnkDao(IBpMdlLnkDao bpMdlLnkDao) {
	    this.bpMdlLnkDao = bpMdlLnkDao;
	}

	@Override
	public IBpAreqLnkDao getBpAreqLnkDao() {
	    return bpAreqLnkDao;
	}

	/**
	 * bpAreqLnkDaoを設定します。
	 * @param bpAreqLnkDao bpAreqLnkDao
	 */
	public void setBpAreqLnkDao(IBpAreqLnkDao bpAreqLnkDao) {
	    this.bpAreqLnkDao = bpAreqLnkDao;
	}

	/**
	 * bpHistDaoを取得します。
	 * @return bpHistDao
	 */
	public IBpHistDao getBpHistDao() {
	    return bpHistDao;
	}

	/**
	 * bpHistDaoを設定します。
	 * @param bpHistDao bpHistDao
	 */
	public void setBpHistDao(IBpHistDao bpHistDao) {
	    this.bpHistDao = bpHistDao;
	}

}
