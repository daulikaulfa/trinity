package jp.co.blueship.tri.bm.task.diff.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultDiffEntity extends TaskTaskResultEntity implements ITaskResultDiffEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];


	public ITaskResultEntity[] getResult() {
		return result ;
	}
	public void setResult( ITaskResultEntity[] result ) {
		this.result = result ;
	}

	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {

		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String relativePath = null ;

		public String getRelativePath() {
			return relativePath ;
		}
		public void setRelativePath( String value ) {
			relativePath = value ;
		}
	}
}
