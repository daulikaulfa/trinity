package jp.co.blueship.tri.bm.task.diff.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskDiffEntity extends IEntity, ITaskTaskTypeEntity {

	/**
	 * パラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam() ;
	/**
	 * パラメタ情報を設定します。
	 *
	 * @param param パラメタ情報
	 */
	public void setParam(ITaskParamEntity[] param) ;

	/**
	 * 文字コード／改行の変換コピーを行うパラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskCharsetinfoEntity[] getCharsetinfo() ;
	/**
	 * 文字コード／改行の変換コピーを行うパラメタ情報を設定します。
	 *
	 * @param charsetInfo パラメタ情報
	 */
	public void setCharsetinfo(ITaskCharsetinfoEntity[] charsetInfo) ;

	/**
	 * 比較先のディレクトリ／ファイル。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getSrcPath() ;
	/**
	 * 比較先のディレクトリ／ファイル。
	 *
	 * @param value
	 */
	public void setSrcPath( String value ) ;

	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#ADD}の場合、このディレクトリにコピーされます。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDestNewFilePath() ;
	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#ADD}の場合、このディレクトリにコピーされます。
	 *
	 * @param value 追加時のコピー先パス
	 */
	public void setDestNewFilePath( String value ) ;

	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#CHANGE}の場合、このディレクトリにコピーされます。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDestModifiedFilePath() ;
	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#CHANGE}の場合、このディレクトリにコピーされます。
	 *
	 * @param value 変更時のコピー先パス
	 */
	public void setDestModifiedFilePath( String value ) ;

	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#DELETE}の場合、このディレクトリにコピーされます。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDestDeleteFilePath() ;
	/**
	 * 比較結果が{@link jp.jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status#DELETE}の場合、このディレクトリにコピーされます。
	 *
	 * @param value 削除時のコピー先パス
	 */
	public void setDestDeleteFilePath( String value ) ;

	/**
	 * 比較先のディレクトリ／ファイル。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDiffPath() ;
	/**
	 * 比較先のディレクトリ／ファイル。
	 *
	 * @param value 比較先のディレクトリ／ファイル
	 */
	public void setDiffPath( String value ) ;

	/**
	 * コピー元のタイプムスタンプを保持するどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getHoldTimestamp();
	/**
	 * コピー元のタイムスタンプを保持するかどうかを設定します。
	 *
	 * @param value タイムスタンプを保持してコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setHoldTimestamp( String value );

	/**
	 * 資産と関係ないSCM関連ファイルを含めるかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNeglect();
	/**
	 * 資産と関係ないSCM関連ファイルを含めるかどうかを設定します。
	 *
	 * @param value SCM関連ファイルを含めずにコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNeglect( String value );

	/**
	 * 比較後、差分ファイルをコピーするかどうかを取得する。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoCopy() ;
	/**
	 * 比較後、差分ファイルをコピーするかどうかを設定する。
	 *
	 * @param value 差分ファイルをコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外はコピーされない。
	 */
	public void setDoCopy( String value ) ;

	/**
	 * 削除された資産も抽出するかどうかを取得する。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoReverse() ;
	/**
	 * 削除された資産も抽出するかどうかを設定する。
	 *
	 * @param value 削除された資産を抽出する場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoReverse( String value ) ;

	/**
	 * 新規、変更資産を抽出しないかどうかを取得する。
	 * <br>削除のみ抽出したい場合に設定する。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotForward() ;
	/**
	 * 新規、変更資産を抽出しないかどうかを設定する。
	 * <br>削除のみ抽出したい場合に設定する。
	 *
	 * @param value 削除された資産を抽出する場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotForward( String value ) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );


	/**
	 *
	 * パラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * スキップしたい行を識別する文字列。
		 * <br>文字列が存在する行は、該当行を比較しない。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSkipword() ;
		/**
		 * スキップしたい行を識別する文字列。
		 * <br>文字列が存在する行は、該当行を比較しない。
		 *
		 * @param value 比較対象外となるスキップ文字列
		 */
		public void setSkipword( String value ) ;
	}

	/**
	 *
	 * 文字コード／改行の変換コピーを行うパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskCharsetinfoEntity extends IEntity {

		/**
		 * 変換対象の拡張子
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getExtensions();
		/**
		 * 変換対象の拡張子
		 *
		 * @param extensions
		 */
		public void setExtensions(String extensions);
		/**
		 * コピー元ファイルの文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getIn();
		/**
		 * コピー元ファイルの文字コード
		 *
		 * @param in {@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}の文字列。
		 */
		public void setIn(String in);
		/**
		 * コピー先ファイルの文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getOut();
		/**
		 * コピー先ファイルの文字コード
		 *
		 * @param out {@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}の文字列。
		 */
		public void setOut(String out);
		/**
		 * コピー先ファイルの改行コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getOutls();
		/**
		 * コピー先ファイルの改行コード
		 *
		 * @param outls {@link jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode}の文字列。
		 */
		public void setOutls(String outls);
		/**
		 * diff比較先ファイルの文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDiff() ;
		/**
		 * diff比較先ファイルの文字コード
		 *
		 * @param diff {@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}の文字列。
		 */
		public void setDiff(String diff) ;
	}
}
