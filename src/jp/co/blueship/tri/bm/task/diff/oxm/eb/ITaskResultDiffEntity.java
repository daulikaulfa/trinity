package jp.co.blueship.tri.bm.task.diff.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskResultDiffEntity extends IEntity, ITaskResultTypeEntity {

	/**
	 * 実行結果の配列を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult() ;
	/**
	 * 実行結果の配列を設定します。
	 *
	 * @param result 実行結果
	 */
	public void setResult( ITaskResultEntity[] result ) ;
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {

		/**
		 * 差分比較した資産の相対パスを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getRelativePath() ;
		/**
		 * 差分比較した資産の相対パスを設定します。
		 *
		 * @param value 資産の相対パス
		 */
		public void setRelativePath( String value ) ;
	}
}
