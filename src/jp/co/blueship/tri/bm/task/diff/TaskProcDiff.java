package jp.co.blueship.tri.bm.task.diff;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskDiffEntity.ITaskCharsetinfoEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.TaskResultDiffEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskDefineId;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.FileConvertCharset;
import jp.co.blueship.tri.fw.cmn.io.FileCopy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverride;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverridePreserveLastModified;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「diff」タスクの処理内容を記述します
 * <p>
 * 定義されて情報を元に、ファイルの差分比較を行い、差分のあるファイルを対象にしてコピーを行う。
 *
 */
public class TaskProcDiff implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private static final ILog log = TriLogFactory.getInstance();

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;

	public void setSupport(TaskFinderSupport support) {
		this.support = support;
	}

	//差分比較モード
	private enum DiffMode {
		forward ,	//順方向（追加・変更ファイルを検出する）
		reverse;	//逆方向（削除ファイルを検出する）
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		ITaskDiffEntity diff = (ITaskDiffEntity) task;

		diff.setSrcPath( TaskStringUtils.substitutionPath( propertyEntity, diff.getSrcPath() ) );
		diff.setDestNewFilePath( TaskStringUtils.substitutionPath( propertyEntity, diff.getDestNewFilePath() ) );
		diff.setDestModifiedFilePath( TaskStringUtils.substitutionPath( propertyEntity, diff.getDestModifiedFilePath() ) );
		diff.setDestDeleteFilePath( TaskStringUtils.substitutionPath( propertyEntity, diff.getDestDeleteFilePath() ) );
		diff.setDiffPath( TaskStringUtils.substitutionPath( propertyEntity, diff.getDiffPath() ) );
	}

	/**
	 * 「diff」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskDiffEntity) )
			return null;

		ITaskResultDiffEntity resultDiffEntity = new TaskResultDiffEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultDiffEntity.setSequence( prmTask.getSequence() );
		resultDiffEntity.setCode( TaskResultCode.UN_PROCESS );
		resultDiffEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskDiffEntity diff = (ITaskDiffEntity) prmTask;

			if ( ! StatusFlg.on.value().equals( diff.getDoNotForward() ) ) {
				resultSequence = diffProc(
						DiffMode.forward,
						diff,
						resultDiffEntity,
						resultList,
						resultSequence,
						diff.getSrcPath(),
						diff.getSrcPath(),
						diff.getDiffPath() );
			}

			if( StatusFlg.on.value().equals( diff.getDoReverse() ) ) {
				//逆方向スキャンにより、削除ファイルを検出する
				resultSequence = diffProc(
						DiffMode.reverse,
						diff,
						resultDiffEntity,
						resultList,
						resultSequence,
						diff.getDiffPath(),
						diff.getDiffPath(),
						diff.getSrcPath() );
			}

			resultDiffEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultDiffEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005017S, e );

		} finally {
			resultDiffEntity.setResult( resultList.toArray( new TaskResultDiffEntity.TaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultDiffEntity );
		}

		return resultDiffEntity;
	}
	/**
	 * 再帰的にフォルダ構造を辿り、diff処理を実行していく。<br>
	 * @param diffMode 差分比較モード。{@link DiffMode}
	 * @param diff 差分比較タスク
	 * @param resultDiffEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param resultSequence 処理結果（詳細）の実行順序
	 * @param basePath 基準パス
	 * @param srcPath 比較元パス
	 * @param diffPath 比較先パス
	 * @return 処理結果（詳細）の実行順序
	 * @throws Exception
	 */
	private Integer diffProc( DiffMode diffMode,
							ITaskDiffEntity diff,
							ITaskResultDiffEntity resultDiffEntity,
							List<ITaskResultEntity> resultList,
							Integer resultSequence,
							String basePath,
							String srcPath,
							String diffPath ) throws Exception {

		File file = new File( srcPath );

		if( true != file.exists() ) {
			throw new TriSystemException( BmMessageId.BM004034F , srcPath);
		}

		if( StatusFlg.on.value().equals( diff.getDoNeglect() ) && true == TriFileUtils.isSCM( file ) ) {//SCM管理のファイル・ディレクトリは処理しない
			return resultSequence;
		}

		if( file.isDirectory() ) {
			File[] files = file.listFiles();
			if( null == files ) {
				throw new TriSystemException( BmMessageId.BM004022F , srcPath);
			}
			for( File tmpFile : files ) {
				resultSequence = diffProc(
						diffMode,
						diff,
						resultDiffEntity,
						resultList,
						resultSequence,
						basePath,
						TriStringUtils.linkPathBySlash( srcPath, tmpFile.getName() ),
						TriStringUtils.linkPathBySlash( diffPath, tmpFile.getName() ) );
			}
		}

		if ( file.isFile() ) {
			//処理ファイルごと処理結果を記録する
			ITaskResultEntity resultEntity = resultDiffEntity.newResult();
			resultEntity.setSequence( String.valueOf(resultSequence++) );
			resultEntity.setCode( Status.EQUAL.toString() );

			try {
				String srcPathOrg = TriStringUtils.convertPath( basePath );
				String relativePath = TriStringUtils.convertRelativePath( new File( srcPathOrg ), srcPath );
				resultEntity.setRelativePath( relativePath );

				//charsetinfoのチェック
				ITaskCharsetinfoEntity charsetinfo = getCharsetinfo( diff.getCharsetinfo(), srcPath );
				//skipwordリストの取得
				List<String> skipwordList = getSkipword( diff.getParam() );

				//diff処理
				Status status = null;
				if( DiffMode.forward.equals( diffMode ) ) {
					status = this.diffFile( srcPath, diffPath, charsetinfo, skipwordList );
				} else {//逆方向diff時は「追加（削除）」を検出するだけなのでdiff処理をスキップするために追加
					status = ( true == new File( diffPath ).isFile() ) ? Status.EQUAL : Status.DELETE;
				}

				resultEntity.setCode( status.toString() );

				if( !Status.EQUAL.equals( status ) ) {

					//コピー処理
					if( StatusFlg.on.value().equals( diff.getDoCopy() ) ) {
						//「新規」「変更」「削除」で出力先フォルダを分ける
						String usePath = null;
						if( Status.ADD.equals( status ) ) {
							usePath = diff.getDestNewFilePath();
						} else if( Status.CHANGE.equals( status ) ) {
							usePath = diff.getDestModifiedFilePath();
						} else if( Status.DELETE.equals( status ) ) {
							usePath = diff.getDestDeleteFilePath();
						}

						if ( TriStringUtils.isEmpty( usePath ) ) {
							throw new TriSystemException( BmMessageId.BM004035F);
						}

						String destPath = TriStringUtils.linkPathBySlash( usePath, relativePath );
						LogHandler.debug( log , "diff差分結果出力先パス： " + destPath );
						copyFile( srcPath, destPath, diff.getHoldTimestamp(), charsetinfo );
					}
				}

			} catch ( Exception e ) {
				resultEntity.setCode( Status.ERROR.toString() );
				throw e;

			} finally {
				if ( StatusFlg.on.value().equals( diff.getDoDetailSnap() ) ) {
					resultList.add( resultEntity );
				} else {
					if ( Status.ERROR.toString().equals( resultEntity.getCode() ) ) {
						resultList.add( resultEntity );
					} else {
						if ( Status.DELETE.toString().equals( resultEntity.getCode() ) ) {
							//バイナリ資産削除の業務DIFFであれば、スナップを記録する
							String taskId = (StringUtils.isEmpty( diff.getTaskId() ))? "": diff.getTaskId();

							if ( -1 != taskId.indexOf( TaskDefineId.deleteBinaryAssetCount.value() )
								&& DiffMode.reverse.equals(diffMode) ) {
								resultList.add( resultEntity );
							}
						}
					}
				}
			}
		}

		return resultSequence;
	}
	/**
	 * コピー処理 本体
	 * @param copy
	 * @param diffMap
	 * @param srcPath
	 * @param dstPath
	 */
	private Status diffFile( String srcPath, String diffPath, ITaskCharsetinfoEntity charsetinfo, List<String> skipwordList ) throws Exception {
		File srcFile = new File( srcPath );
		File diffFile = new File( diffPath );
		if( true != srcFile.isFile() || true != srcFile.canRead() ) {
			throw new TriSystemException( BmMessageId.BM004030F , srcPath);
		}
		if( true == diffFile.isDirectory() ) {
			throw new TriSystemException( BmMessageId.BM004036F , diffPath);
		}

		//ファイル種別の判別：デフォルトはバイナリとみなす
		Map<String,Boolean> extensionMap = this.getExtensionMap();
		String extension = TriFileUtils.getExtension( srcPath );
		Boolean binary = ( null != extension ) ? extensionMap.get( extension ) : null;
		if( null == binary ) {
			binary = true;
		}
		if( null != charsetinfo ) {//UcfExtensionに登録されていなくても、charsetinfoにエントリがあれば、テキスト比較を行う
			binary = false;
		}

		//diffのチェック
		return this.chkBinaryDiff( srcPath, diffPath );
	}

	/**
	 * コピー処理本体
	 * @param srcPath
	 * @param destPath
	 * @param holdTimestamp
	 * @param charsetinfo
	 * @throws Exception
	 */
	private void copyFile( String srcPath, String destPath, String holdTimestamp, ITaskCharsetinfoEntity charsetinfo ) throws Exception {

		boolean holdTimestampSw = ( StatusFlg.on.value().toString().equals( holdTimestamp ) ) ? true : false;

		File srcFile = new File( srcPath );
		File destFile = new File( destPath );
		File parentFile = new File( destFile.getParent() );
		parentFile.mkdirs();

		if( null != charsetinfo ) {//文字コードをコンバートしてコピー
			FileConvertCharset fileConvert = new FileConvertCharset();
			fileConvert.setSrcFile( srcFile );
			fileConvert.setSrcFileEncoding( charsetinfo.getIn() );
			fileConvert.setDstFile( destFile );
			fileConvert.setDstFileEncoding( charsetinfo.getOut() );
			if( true != TriStringUtils.isEmpty( charsetinfo.getOutls() ) ) {
				if( LinefeedCode.CRLF.equals( LinefeedCode.getLabel( charsetinfo.getOutls() ) ) ) {//Antに準拠。CRLF以外はLFとみなす
					fileConvert.setDstFileLinefeedCode( LinefeedCode.CRLF.toString() );
				} else {
					fileConvert.setDstFileLinefeedCode( LinefeedCode.LF.toString() );
				}
			}
			fileConvert.setHoldTimestamp( holdTimestampSw );
			fileConvert.convertFile();

			//duplicateList.add( dstFile );
		} else {//単純コピー
			FileCopy fileCopy = null;
			if( true == holdTimestampSw ) {//タイムスタンプを保持する
				fileCopy = new FileCopyOverridePreserveLastModified();
			} else {//タイムスタンプを保持しない
				fileCopy = new FileCopyOverride();
			}
			fileCopy.copyFileToFile( srcFile, destFile, new ArrayList<File>() );//duplicateList );
		}
	}


	private TreeMap<String,ITaskCharsetinfoEntity> charsetinfoMap = null;
	/**
	 * ファイルの拡張子に該当する、「文字コードコンバート＆改行コードの設定」を取得する<br>
	 * １度目にTreeMapに格納してしまうので、２回目以降は多少高速。<br>
	 * @param charsetinfoArray 「文字コードコンバート＆改行コードの設定」リスト
	 * @param srcPath ファイル名文字列
	 * @return 該当した「文字コードコンバート＆改行コードの設定」。該当せずの場合Nullを返す
	 * @throws Exception
	 */
	private ITaskCharsetinfoEntity getCharsetinfo( ITaskCharsetinfoEntity[] charsetinfoArray, String srcPath ) throws Exception {
//		文字コードコンバート＆改行コードの設定
		if( null == charsetinfoMap ) {
			charsetinfoMap = new TreeMap<String,ITaskCharsetinfoEntity>();
			if( null != charsetinfoArray ) {
				for( ITaskCharsetinfoEntity charsetinfo : charsetinfoArray ) {
					charsetinfoMap.put( charsetinfo.getExtensions(), charsetinfo );
				}
			}
		}
		String ext = TriFileUtils.getExtension( srcPath );
		if( null == ext ) {
			return null;
		}
		ITaskCharsetinfoEntity charsetinfo = charsetinfoMap.get( ext );
		return charsetinfo;
	}


	/**
	 * スキップワードを取得し、Listに格納する
	 * インスタンス中で一度生成したら、以降はそれを使いまわす
	 * @param paramArray ITaskDiffEntity.ITaskParamEntityオブジェクトの配列
	 * @return スキップワードのList
	 */
	private List<String> skipwordList = null;
	private List<String> getSkipword( ITaskDiffEntity.ITaskParamEntity[] paramArray ) {
//		diffセット
		if( null == skipwordList ) {
			skipwordList = new ArrayList<String>();
			if( null != paramArray ) {
				for( ITaskDiffEntity.ITaskParamEntity param : paramArray ) {
					skipwordList.add( param.getSkipword() );
				}
			}
		}
		return skipwordList;
	}

	/**
	 * /**
	 * テキストDiff比較を行う<br>
	 * @param skipwordList スキップワード文字列を格納したList
	 * @param srcPath		比較元ファイルのパス
	 * @param srcCharset	比較元ファイルのCharset（自動判定時に任せる場合はNull）
	 * @param dstPath		比較先ファイルのパス
	 * @param dstCharset	比較先ファイルのCharset（自動判定時に任せる場合はNull）
	 * @return 判定結果
	 * <pre>
	 * 		true 	:	コピーする（diff対象外 or diffで差分があったもの）
	 * 		false	:	コピーしない（ファイルサイズ０ or diffで差分がなかったもの）
	 * </pre>
	 * @throws Exception
	 */
	/*使用されていないprivateメソッド
	private Status chkTxtDiff( List<String> skipwordList, String srcPath, Charset srcCharset, String dstPath, Charset dstCharset ) throws Exception {
		File srcFile = new File( srcPath );
		File dstFile = new File( dstPath );

		if( true != dstFile.exists() ) {//比較先ファイルが存在しない
			return Status.ADD;
		}
		EqualsContentsSetSameLineDiffer diff = null;
		if( null == srcCharset || null == dstCharset ) {//文字コード指定なし⇒文字コード自動判別
			diff = new EqualsContentsSetSameLineDiffer( srcFile, dstFile );
		} else {//文字コード指定
			diff = new EqualsContentsSetSameLineDiffer( srcFile, srcCharset, dstFile, dstCharset );
		}

		//スキップワードのセット
		diff.setException( skipwordList );

		DiffResult diffResult = diff.diff();
		if( null == diffResult.getDiffElementList() ) {//ファイルサイズ０
			return Status.EQUAL;//コピーしない
		}
		for( IDiffElement element : diffResult.getDiffElementList() ) {//差分スキャン
			EqualsContentsSetSameLineDiffResult result = (EqualsContentsSetSameLineDiffResult)element;
			if( true != result.getStatus().equals( EqualsContentsSetSameLineDiffer.Status.EQUAL ) ) {//不一致
				return Status.CHANGE;//コピーする
			}
		}
		return Status.EQUAL;
	}*/
	/**
	 * バイナリdiff比較を行う<br>
	 * バイナリにスキップワードの概念はない。<br>
	 * @param srcPath	比較元ファイルパス
	 * @param dstPath	比較先ファイルパス
	 * @return 判定結果
	 * <pre>
	 * 		true 	:	コピーする（diff対象外 or diffで差分があったもの）
	 * 		false	:	コピーしない（diffで差分がなかったもの）
	 * </pre>
	 * @throws Exception
	 */
	private Status chkBinaryDiff( String srcPath, String dstPath ) throws Exception {
		File srcFile = new File( srcPath );
		File dstFile = new File( dstPath );
		if( true != dstFile.exists() ) {//比較先ファイルが存在しない
			return Status.ADD;
		}

		if( true == TriFileUtils.isSame( srcFile, dstFile ) ) {//一致
			return Status.EQUAL;
		} else {//不一致
			return Status.CHANGE;//コピーする
		}
	}


	/**
	 * 登録されている拡張子情報のキャッシュ。
	 * 直接参照する場合、Nullの可能性があるので{@link #getExtension()}の使用を推奨
	 */
	private Map<String,Boolean> extensionMap = null;
	/**
	 * DBに登録されている「拡張子情報」を取得する<br>
	 * １度目に取得しにいくので、２回目以降は多少高速かつ接続エラーも回避できるはず。<br>
	 * @throws Exception
	 */
	private Map<String,Boolean> getExtensionMap() throws Exception {
//		拡張子の設定
		if( null == extensionMap ) {
			extensionMap = support.getExtDecisionMap();
		}
		return extensionMap;
	}


}
