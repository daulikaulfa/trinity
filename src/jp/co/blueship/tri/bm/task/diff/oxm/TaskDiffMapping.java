package jp.co.blueship.tri.bm.task.diff.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.TaskDiffEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.DiffType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Diffタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskDiffMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		DiffType[] tasks = src.getDiffArray();
		if( null != tasks ) {
			for( DiffType task : tasks ) {
				entitys.add( mapXMLBeans2DBSubType( src , task ) ) ;
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultDiffMapping resultMapping = new TaskResultDiffMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のXMLBeans親階層
	 * @param task 複写元のXMLBeans
	 * @return マッピング済みITaskTaskTypeEntity
	 */
	public ITaskTaskTypeEntity mapXMLBeans2DBSubType( TargetType src , DiffType task ) {
		TaskDiffEntity taskEntity = new TaskDiffEntity();
		copyProperties( task, taskEntity );

		//param
		if( null != task.getParamArray() ) {
			List<TaskDiffEntity.ITaskParamEntity> paramList = new ArrayList<TaskDiffEntity.ITaskParamEntity>() ;

			for( DiffType.Param param : task.getParamArray() ) {
				ITaskDiffEntity.ITaskParamEntity paramEntity = taskEntity.new TaskParamEntity() ;
				copyProperties( param , paramEntity ) ;
				paramList.add( paramEntity ) ;
			}
			taskEntity.setParam( paramList.toArray( new TaskDiffEntity.TaskParamEntity[ 0 ] ) ) ;
		}

		//Charsetinfo
		if( null != task.getCharsetinfoArray() ) {
			List<TaskDiffEntity.ITaskCharsetinfoEntity> charsetinfoList = new ArrayList<TaskDiffEntity.ITaskCharsetinfoEntity>() ;

			for( CharsetinfoType charsetinfo : task.getCharsetinfoArray() ) {
				TaskDiffEntity.TaskCharsetinfoEntity charsetinfoEntity = taskEntity.new TaskCharsetinfoEntity();
				copyProperties( charsetinfo, charsetinfoEntity );

				charsetinfoList.add( charsetinfoEntity );

			}

			taskEntity.setCharsetinfo( charsetinfoList.toArray( new TaskDiffEntity.ITaskCharsetinfoEntity[0] ) );
		}
		return taskEntity ;
	}
	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskDiffEntity) )
					continue;

				DiffType task = dest.addNewDiff();
				mapDB2XMLBeansSubType( src , entity , task ) ;
			}
		}

		//Result
		TaskResultDiffMapping resultMapping = new TaskResultDiffMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のDBのEntity親階層
	 * @param entity 複写元のDBのEntity
	 * @param copy マッピング結果を格納するbeans
	 */
	public void mapDB2XMLBeansSubType(ITaskTargetEntity src, ITaskTaskTypeEntity entity , DiffType diff ) {
		TaskDiffEntity taskEntity = (TaskDiffEntity)entity;
		copyProperties( taskEntity, diff );

		//Param
		TaskDiffEntity.ITaskParamEntity[] paramArray = taskEntity.getParam() ;
		if( null != paramArray ) {
			for( TaskDiffEntity.ITaskParamEntity paramEntity : paramArray ) {
				DiffType.Param param = diff.addNewParam() ;
				copyProperties( paramEntity , param ) ;
			}
		}

		//Charsetinfo
		TaskDiffEntity.ITaskCharsetinfoEntity[] charsetinfoArray = taskEntity.getCharsetinfo();
		if( null != charsetinfoArray ) {
			for( TaskDiffEntity.ITaskCharsetinfoEntity charsetinfoEntity : charsetinfoArray ) {
				CharsetinfoType charsetinfo = diff.addNewCharsetinfo();
				copyProperties( charsetinfoEntity, charsetinfo );
			}
		}
	}
}
