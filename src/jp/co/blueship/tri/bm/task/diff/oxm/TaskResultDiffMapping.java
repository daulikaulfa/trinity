package jp.co.blueship.tri.bm.task.diff.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.TaskResultDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.TaskResultDiffEntity.TaskResultEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.DiffResultDetailType;
import jp.co.blueship.tri.fw.schema.beans.task.DiffResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Deleteタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultDiffMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();
			DiffResultType[] results = src.getDiffArray();
			if( null != results ) {
				for( DiffResultType result : results ) {
					entitys.add( mapXMLBeans2DBSubType( src , result ) ) ;
				}
			}
			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[ 0 ] ) ) ;
		}
		return dest;

	}

	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のXMLBeans親階層
	 * @param task 複写元のXMLBeans
	 * @return マッピング済みITaskTaskTypeEntity
	 */
	public ITaskResultTypeEntity mapXMLBeans2DBSubType( Result src , DiffResultType result ) {

		ITaskResultDiffEntity resultDiffEntity = new TaskResultDiffEntity() ;
		copyProperties( result, resultDiffEntity ) ;

		DiffResultDetailType[] diffResultArray = result.getResultArray() ;
		if( null != diffResultArray ) {

			ArrayList<ITaskResultEntity> taskResultList = new ArrayList<ITaskResultEntity>() ;
			for( DiffResultDetailType diffResult : diffResultArray ) {
				ITaskResultEntity resultEntity = resultDiffEntity.newResult();
				copyProperties( diffResult , resultEntity ) ;
				taskResultList.add( resultEntity ) ;
			}
			resultDiffEntity.setResult( taskResultList.toArray( new TaskResultEntity [ 0 ] ) ) ;
		}

		return resultDiffEntity ;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultDiffEntity) )
					continue;

				DiffResultType result = dest.addNewDiff();

				mapDB2XMLBeansSubType( src , entity , result ) ;
			}
		}

		return dest;
	}
	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のDBのEntity親階層
	 * @param entity 複写元のDBのEntity
	 * @param copy マッピング結果を格納するbeans
	 */
	public void mapDB2XMLBeansSubType(ITaskTargetEntity src, ITaskResultTypeEntity entity , DiffResultType result ) {

		ITaskResultDiffEntity resultEntity = (ITaskResultDiffEntity)entity;
		copyProperties( resultEntity, result );

		ITaskResultEntity[] taskResultEntityArray = resultEntity.getResult() ;
		if( null != taskResultEntityArray ) {
			for( ITaskResultEntity taskResultEntity : taskResultEntityArray ) {
				DiffResultDetailType diffResultDetailType = result.addNewResult() ;

				copyProperties( taskResultEntity , diffResultDetailType ) ;
			}
		}
	}
}
