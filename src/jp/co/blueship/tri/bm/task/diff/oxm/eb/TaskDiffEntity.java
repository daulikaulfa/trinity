package jp.co.blueship.tri.bm.task.diff.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskDiffEntity extends TaskTaskEntity implements ITaskDiffEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private ITaskParamEntity[] param = null ;
	private ITaskCharsetinfoEntity[] charsetinfo = new ITaskCharsetinfoEntity[0];

	private String srcPath = null ;
	private String destNewFilePath = null ;
	private String destModifiedFilePath = null ;
	private String destDeleteFilePath = null ;
	private String diffPath = null ;
	private String holdTimestamp = StatusFlg.off.value();
	private String doNeglect = StatusFlg.on.value();
	private String doCopy = StatusFlg.off.value();
	private String doNotForward = StatusFlg.off.value();
	private String doReverse = StatusFlg.on.value();
	private String doDetailSnap = StatusFlg.on.value();


	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String skipword = null ;


		public String getSkipword() {
			return skipword ;
		}
		public void setSkipword( String value ) {
			skipword = value ;
		}
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskCharsetinfoEntity implements ITaskCharsetinfoEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String extensions = null ;
		private String in = null ;
		private String out = null ;
		private String outls = null ;
		private String diff = null ;

		public String getExtensions() {
			return extensions;
		}
		public void setExtensions(String extensions) {
			this.extensions = extensions;
		}
		public String getIn() {
			return in;
		}
		public void setIn(String in) {
			this.in = in;
		}
		public String getOut() {
			return out;
		}
		public void setOut(String out) {
			this.out = out;
		}
		public String getOutls() {
			return outls;
		}
		public void setOutls(String outls) {
			this.outls = outls;
		}
		public String getDiff() {
			return diff;
		}
		public void setDiff(String diff) {
			this.diff = diff;
		}
	}

	public ITaskCharsetinfoEntity[] getCharsetinfo() {
		return charsetinfo;
	}

	public void setCharsetinfo(ITaskCharsetinfoEntity[] charsetinfo) {
		this.charsetinfo = charsetinfo;
	}

	public String getDestDeleteFilePath() {
		return destDeleteFilePath;
	}

	public void setDestDeleteFilePath(String destDeleteFilePath) {
		this.destDeleteFilePath = destDeleteFilePath;
	}

	public String getDestModifiedFilePath() {
		return destModifiedFilePath;
	}

	public void setDestModifiedFilePath(String destModifiedFilePath) {
		this.destModifiedFilePath = destModifiedFilePath;
	}

	public String getDestNewFilePath() {
		return destNewFilePath;
	}

	public void setDestNewFilePath(String destNewFilePath) {
		this.destNewFilePath = destNewFilePath;
	}

	public String getDiffPath() {
		return diffPath;
	}

	public void setDiffPath(String diffPath) {
		this.diffPath = diffPath;
	}

	public String getDoCopy() {
		return doCopy;
	}

	public void setDoCopy(String doCopy) {
		this.doCopy = doCopy;
	}

	public String getDoNeglect() {
		return doNeglect;
	}

	public void setDoNeglect(String doNeglect) {
		this.doNeglect = doNeglect;
	}

	public String getDoReverse() {
		return doReverse;
	}

	public void setDoReverse(String doReverse) {
		this.doReverse = doReverse;
	}

	public String getHoldTimestamp() {
		return holdTimestamp;
	}

	public void setHoldTimestamp(String holdTimestamp) {
		this.holdTimestamp = holdTimestamp;
	}

	public ITaskParamEntity[] getParam() {
		return param;
	}

	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public String getDoNotForward() {
		return doNotForward;
	}

	public void setDoNotForward(String doNotForward) {
		this.doNotForward = doNotForward;
	}

}
