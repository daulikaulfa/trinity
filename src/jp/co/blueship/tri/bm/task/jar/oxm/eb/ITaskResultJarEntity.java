package jp.co.blueship.tri.bm.task.jar.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultJarEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult( ITaskResultEntity[] values );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineFilePath();
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @param value 外部定義ファイル（定義リスト）
		 */
		public void setDefineFilePath(String value);
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineDetailFilePath();
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @param value 外部定義ファイル（詳細定義リスト）
		 */
		public void setDefineDetailFilePath(String value);
		/**
		 * 出力先のファイルパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 出力先のファイルパス
		 *
		 * @param value 出力先のファイルパス
		 */
		public void setDest( String value );
	}

}
