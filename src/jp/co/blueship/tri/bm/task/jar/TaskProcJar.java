package jp.co.blueship.tri.bm.task.jar;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.jar.oxm.eb.ITaskJarEntity;
import jp.co.blueship.tri.bm.task.jar.oxm.eb.ITaskResultJarEntity;
import jp.co.blueship.tri.bm.task.jar.oxm.eb.ITaskResultJarEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.jar.oxm.eb.TaskJarEntity;
import jp.co.blueship.tri.bm.task.jar.oxm.eb.TaskResultJarEntity;
import jp.co.blueship.tri.fw.agent.core.TaskDefineFileUtils;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.EarCreator;
import jp.co.blueship.tri.fw.cmn.io.JarCreator;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.WarCreator;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「jar」タスクの処理内容を記述します
 * <p>
 * アーカイブファイルの作成を行います。
 * <br>指定されたファイルの拡張子で、いずれのファイルを作成するかを決定します。
 * 以下のアーカイブに対応しています。
 * <li>jar
 * <li>war
 * <li>ear
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class TaskProcJar implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * アーカイブ名を示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String ARCHIVE_NAME = "ARCHIVE_NAME";
	/**
	 * アーカイブ出力先ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String OUTPUT_PATH = "OUTPUT_PATH";
	/**
	 * アーカイブ作成対象パッケージ トップ(ルート)ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String SRC_BASE_PATH = "SRC_BASE_PATH";
	/**
	 * マニフェストファイルのパスを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String MANIFEST_PATH = "MANIFEST_PATH";
	/**
	 * web.xml、application.xmlファイルのパスを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String XML_PATH = "XML_PATH";
	/**
	 * アーカイブ作成判断パスを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String JUDGE_PATH = "JUDGE_PATH";

	/**
	 * いずれのファイルを作成するかを判別する拡張子
	 *
	 */
	private enum ArchiveExtensionDefine {
		/**
		 * Jarの拡張子
		 */
		JAR_EXTENSION ( "jar" ),
		/**
		 * Warの拡張子
		 */
		WAR_EXTENSION( "war" ),
		/**
		 * Earの拡張子
		 */
		EAR_EXTENSION ( "ear" );

		private String value = null;

		private ArchiveExtensionDefine( String value ) {
			this.value = value;
		}

		public String getValue() {
			return this.value;
		}
/* 呼び出しされないメソッド
		public static ArchiveExtensionDefine getValue( String value ) {
			for ( ArchiveExtensionDefine extension : values() ) {
				if ( extension.getValue().equals( value ) ) {
					return extension;
				}
			}

			return null;
		}*/

	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskJarEntity jar = (TaskJarEntity) task;

		jar.setDefineFilePath(
				TaskStringUtils.substitutionPath( this.propertyEntity , jar.getDefineFilePath() ));
	}

	/**
	 * 「jar」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskJarEntity) )
			return null;

		TaskResultJarEntity resultJarEntity = new TaskResultJarEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultJarEntity.setSequence( prmTask.getSequence() );
		resultJarEntity.setCode( TaskResultCode.UN_PROCESS );
		resultJarEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskJarEntity jar = (ITaskJarEntity) prmTask;

			List<JarDefinition> jarDefList = new ArrayList<JarDefinition>();

			// 定義リストファイルの設定あり
			File defListFile = new File( jar.getDefineFilePath() );
			if ( !defListFile.exists() ) {
				throw new TriSystemException( BmMessageId.BM004011F ,jar.getDefineFilePath() );
			}

			jarDefList.addAll( getReformDefFileList( defListFile ) );

			resultSequence = this.reform( jarDefList, jar, resultJarEntity, resultList, resultSequence );

			resultJarEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultJarEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005022S ,e);
		} finally {
			resultJarEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultJarEntity );
		}

		return resultJarEntity;
	}

	/**
	 * Jar定義情報に従い、アーカイブを作成する。
	 * @param jarDefList Jar定義情報のリスト
	 * @param jar アーカイブタスク
	 * @param resultJarEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param sequence 処理結果（詳細）の実行順序
	 * @return 処理結果（詳細）の実行順序
	 */
	private Integer reform(
							List<JarDefinition> jarDefList,
							ITaskJarEntity jar,
							ITaskResultJarEntity resultJarEntity,
							List<ITaskResultJarEntity.ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		boolean doUpdate = ( StatusFlg.on.value().toString().equals( jar.getDoUpdate() ) ) ? true : false;

		for ( JarDefinition jarDef : jarDefList ) {
			//filesetごとに処理結果を記録する
			ITaskResultJarEntity.ITaskResultEntity innerResultEntity = resultJarEntity.newResult();
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

			try {
				innerResultEntity.setSequence( String.valueOf(sequence++) );
				innerResultEntity.setDefineFilePath( jar.getDefineFilePath() );
				innerResultEntity.setDefineDetailFilePath( jarDef.getDefineDetailFilePath() );

				// 判断パスが未設定、または判断パスに構成するファイルがあれば実施
				if ( isExist( jarDef.getJarJudgePathList(), jarDef.getJarPackageList() )) {

					File srcBaseDir = null;
					if ( null != jarDef.getSrcBasePath() ) {
						srcBaseDir = new File( jarDef.getSrcBasePath() );
					}

					File jarFile = null;
					if ( null != jarDef.getOutputPath() ) {
						jarFile = new File( jarDef.getOutputPath(), jarDef.getJarName() );
					} else {
						jarFile = new File( jarDef.getJarName() );
					}

					innerResultEntity.setDest( TriStringUtils.convertPath(jarFile.getPath()) );

					File manifestFile = null;
					if ( null != jarDef.getManifestPath() ) {
						manifestFile = new File( jarDef.getManifestPath() );
					}

					File xmlFile = null;
					if ( null != jarDef.getXmlPath() ) {
						xmlFile = new File( jarDef.getXmlPath() );
					}

					JarCreator.Duplicate dup = null;
					if ( doUpdate ) {
						dup = JarCreator.Duplicate.OVERWRITE;
					} else {
						dup = JarCreator.Duplicate.SKIP;
					}


					String extension = TriFileUtils.getExtension( jarFile.getName() );

					for ( String packageList : jarDef.getJarPackageList() ) {
						if ( ! StatusFlg.on.value().equals( jar.getDoNotExistSkip() ) && ! new File( srcBaseDir, packageList ).isFile() )
							throw new TriSystemException(BmMessageId.BM004040F , TriStringUtils.linkPath(srcBaseDir.getPath(), packageList));
					}

					if ( ArchiveExtensionDefine.JAR_EXTENSION.getValue().equalsIgnoreCase( extension )) {

						JarCreator jc = new JarCreator();
						jc.execute(
								jarFile, srcBaseDir,
								(String[])jarDef.getJarPackageList().toArray( new String[0] ),
								manifestFile, dup );

					} else if ( ArchiveExtensionDefine.WAR_EXTENSION.getValue().equalsIgnoreCase( extension )) {

						WarCreator wc = new WarCreator();
						wc.execute(
								jarFile, srcBaseDir,
								(String[])jarDef.getJarPackageList().toArray( new String[0] ),
								xmlFile, manifestFile, dup );

					} else if ( ArchiveExtensionDefine.EAR_EXTENSION.getValue().equalsIgnoreCase( extension )) {

						EarCreator ec = new EarCreator();
						ec.execute(
								jarFile, srcBaseDir,
								(String[])jarDef.getJarPackageList().toArray( new String[0] ),
								xmlFile, manifestFile, dup );

					}

					// 結果の記録
					innerResultEntity.setCode( TaskResultCode.SUCCESS );
				}

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( jar.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		return sequence;
	}

	/**
	 * アーカイブ実行判断パスの配下にアーカイブを構成するパスが存在するかを確認する。
	 * @param jarJudgePathList アーカイブ実行判断パスのリスト
	 * @param srcPathList アーカイブを構成するパスのリスト
	 * @return 存在すればtrue、そうでなければfalse
	 */
	private boolean isExist( List<String> jarJudgePathList, List<String> srcPathList ) {

		// 判断パスが未設定
		if ( null == jarJudgePathList || 0 == jarJudgePathList.size() ) return true;

		for ( String jarJudgePath : jarJudgePathList ) {

			for ( String srcPath : srcPathList ) {

				File file = new File( jarJudgePath, srcPath );
				if ( file.exists() ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * アーカイブ定義リストファイルを読み込み、アーカイブ定義情報のリストを返す。
	 * @param defListFile アーカイブ定義リストファイル
	 * @return アーカイブ定義情報のリスト
	 */
	private List<JarDefinition> getReformDefFileList( File defListFile ) throws Exception {

		List<File> defList = TaskDefineFileUtils.getDefFileList( defListFile );

		List<JarDefinition> jarDefList = new ArrayList<JarDefinition>();

		for ( File defFile : defList ) {
			jarDefList.add( getJarDefinition( defFile ));
		}

		return jarDefList;
	}

	/**
	 * アーカイブ定義ファイルを読み込み、アーカイブ定義情報を返す。
	 * @param defFile アーカイブ定義ファイル
	 * @return アーカイブ定義情報
	 */
	private JarDefinition getJarDefinition( File defFile ) throws Exception {

		JarDefinition jd	= new JarDefinition();
		jd.setDefineDetailFilePath( TriStringUtils.convertPath(defFile.getPath()) );

		int lineNumber		= 0;

		try {
			Map<String,String> aliasDic = new HashMap<String,String>();

			String[] lineArray = TriFileUtils.readFileLine( defFile , false ) ;
			for( String line : lineArray ) {
				lineNumber = lineNumber + 1;

				// コメント、空白行はスキップ
				if ( !TaskDefineFileUtils.isContents( line ))		continue;
				// 変数の定義行は、変数登録をしてスキップ
				if ( TaskDefineFileUtils.isAlias( line, aliasDic ))	continue;

				// 変数を置換
				line = TaskDefineFileUtils.replaceAliasVariable( line, aliasDic );

				// アーカイブ名の定義行でがあれば、設定してスキップ
				if ( setJarName( line, jd ))		continue;
				// アーカイブ出力先の定義行でがあれば、設定してスキップ
				if ( setOutputPath( line, jd ))		continue;
				// アーカイブ作成対象パッケージ トップ(ルート)ディレクトリの定義行でがあれば、設定してスキップ
				if ( setSrcBasePath( line, jd ))	continue;
				// マニフェストファイルパスの定義行でがあれば、設定してスキップ
				if ( setManifestPath( line, jd ))	continue;
				// web.xml、application.xmlファイルパスの定義行でがあれば、設定してスキップ
				if ( setXmlPath( line, jd ))		continue;
				// アーカイブ実行判断ディレクトリの定義行でがあれば、設定してスキップ
				if ( setJarJudgePath( line, jd ))	continue;

				// 上記以外だったら、アーカイブのパッケージ定義行と判断して設定
				setJarPackageValue( line, jd );
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM004013F , e , defFile.getAbsolutePath() , String.valueOf(lineNumber));
		}
		// アーカイブ名がなければエラー
		if ( null == jd.getJarName() ) {
			throw new TriSystemException(BmMessageId.BM004041F , defFile.getAbsolutePath());
		}

		String extension = TriFileUtils.getExtension( jd.getJarName() );

		boolean isErrorExtension = true;
		for ( ArchiveExtensionDefine define : ArchiveExtensionDefine.values()  ) {
			if ( define.getValue().equalsIgnoreCase(extension) ) {
				isErrorExtension = false;
				break;
			}
		}

		if ( isErrorExtension ) {
			throw new TriSystemException(BmMessageId.BM004042F , defFile.getAbsolutePath() );
		}

		return jd;
	}

	/**
     * アーカイブ名の定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setJarName( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, ARCHIVE_NAME );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setJarName( newValue );
    		return  true;
    	}
    }

	/**
     * アーカイブ出力先ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setOutputPath( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, OUTPUT_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setOutputPath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * アーカイブ作成対象パッケージ トップ(ルート)ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setSrcBasePath( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, SRC_BASE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setSrcBasePath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * マニフェストファイルのパスの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setManifestPath( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, MANIFEST_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setManifestPath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * web.xml、application.xmlファイルのパスの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setXmlPath( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, XML_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setXmlPath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * アーカイブ作成判断パスの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setJarJudgePath( String line, JarDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, JUDGE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.getJarJudgePathList().add( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

    /**
     * アーカイブのパッケージ情報の値を取得する
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setJarPackageValue( String line, JarDefinition jd ) {

		String formatedPath = TriStringUtils.convertPath( line ).trim();
		jd.getJarPackageList().add( formatedPath );

		return true;
    }


	/**
	 * アーカイブ定義ファイルの情報を保持するクラス
	 */
	public class JarDefinition {

		/** リフォーム定義ファイルパス */
		private String defineDetailFilePath = null;
		/** アーカイブ名 */
		private String jarName = null;
		/** アーカイブ出力先ディレクトリ */
		private String outputPath = null;
		/** アーカイブ作成対象パッケージ トップ(ルート)ディレクトリ */
		private String srcBasePath = null;
		/** マニフェストファイルパス */
		private String manifestPath = null;
		/** web.xml、application.xmlファイルパス */
		private String xmlPath = null;
		/** アーカイブ作成判断パス */
		private List<String> jarJudgePathList = new ArrayList<String>();
		/** アーカイブを構成するパッケージ(ファイル)情報 */
		private List<String> jarPackageList = new ArrayList<String>();

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getJarName() {
			return jarName;
		}
		public void setJarName( String jarName ) {
			this.jarName = jarName;
		}

		public String getOutputPath() {
			return outputPath;
		}
		public void setOutputPath( String outputPath ) {
			this.outputPath = outputPath;
		}

		public String getSrcBasePath() {
			return srcBasePath;
		}
		public void setSrcBasePath( String srcBasePath ) {
			this.srcBasePath = srcBasePath;
		}

		public String getManifestPath() {
			return manifestPath;
		}
		public void setManifestPath( String manifestPath ) {
			this.manifestPath = manifestPath;
		}

		public String getXmlPath() {
			return xmlPath;
		}
		public void setXmlPath( String xmlPath ) {
			this.xmlPath = xmlPath;
		}

		public List<String> getJarJudgePathList() {
			return jarJudgePathList;
		}
		public void setJarJudgePathList( List<String> jarJudgePathList ) {
			this.jarJudgePathList = jarJudgePathList;
		}

		public List<String> getJarPackageList() {
			return jarPackageList;
		}
		public void setJarPackageList( List<String> jarPackageList ) {
			this.jarPackageList = jarPackageList;
		}
	}
}
