package jp.co.blueship.tri.bm.task.jar.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskJarEntity extends TaskTaskEntity implements ITaskJarEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String code = null ;
	private String defineFilePath = null ;
	private String doUpdate = StatusFlg.on.value();
	private String doNotExistSkip = StatusFlg.on.value();
	private String doDetailSnap = StatusFlg.off.value();

	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getDefineFilePath() {
		return defineFilePath;
	}
	public void setDefineFilePath(String defineFilePath) {
		this.defineFilePath = defineFilePath;
	}
	public String getDoDetailSnap() {
		return doDetailSnap;
	}
	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}
	public String getDoNotExistSkip() {
		return doNotExistSkip;
	}
	public void setDoNotExistSkip(String doNotExistSkip) {
		this.doNotExistSkip = doNotExistSkip;
	}
	public String getDoUpdate() {
		return doUpdate;
	}
	public void setDoUpdate(String doUpdate) {
		this.doUpdate = doUpdate;
	}


}
