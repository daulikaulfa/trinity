package jp.co.blueship.tri.bm.task.jar.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskJarEntity extends ITaskTaskTypeEntity {

	/**
	 * Jar定義リストが記述されたファイルのパスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDefineFilePath();
	/**
	 * JarFileに格納するファイルパスのリストを設定します。
	 *
	 * @param value JarFileに格納するファイルパスのリスト
	 */
	public void setDefineFilePath( String value );

	/**
	 * 既存ファイルが存在するときに上書きを行うかの設定を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoUpdate();
	/**
	 * 既存ファイルが存在するときに上書きを行うかの設定を設定します。
	 *
	 * @param value 既存ファイルが存在するかどうか
	 */
	public void setDoUpdate( String value );

	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotExistSkip() ;
	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @param doSkip 元が存在しなければ、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotExistSkip(String doSkip) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap() ;
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap(String doSkip) ;

}
