package jp.co.blueship.tri.bm.task.sftp.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.sftp.oxm.eb.ITaskResultSftpEntity;
import jp.co.blueship.tri.bm.task.sftp.oxm.eb.ITaskResultSftpEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.sftp.oxm.eb.TaskResultSftpEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.SftpResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Sftpタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultSftpMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();
			SftpResultType[] results = src.getSftpArray();

			if( null != results ) {
				for( SftpResultType result : results ) {
					ITaskResultSftpEntity resultEntity = new TaskResultSftpEntity();

					copyProperties( result , resultEntity );

					jp.co.blueship.tri.fw.schema.beans.task.SftpResultType.Result[] innerResultArray = result.getResultArray();
					if( null != innerResultArray ) {
						List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();
						for( jp.co.blueship.tri.fw.schema.beans.task.SftpResultType.Result innerResult : innerResultArray ) {
							ITaskResultEntity innerResultEntity = resultEntity.newResult();
							copyProperties( innerResult , innerResultEntity );
							resultList.add( innerResultEntity );
						}
						resultEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultSftpEntity) )
					continue;

				TaskResultSftpEntity resultEntity = (TaskResultSftpEntity)entity;

				SftpResultType result = dest.addNewSftp();

				copyProperties( resultEntity, result );

				ITaskResultEntity[] resultEntityArray = resultEntity.getResult();
				if( null != resultEntityArray ) {
					for( ITaskResultEntity innerResultEntity : resultEntityArray ) {
						jp.co.blueship.tri.fw.schema.beans.task.SftpResultType.Result innerResult = result.addNewResult();
						copyProperties( innerResultEntity , innerResult );
					}
				}
			}
		}

		return dest;
	}

}
