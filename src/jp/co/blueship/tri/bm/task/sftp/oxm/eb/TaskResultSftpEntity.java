package jp.co.blueship.tri.bm.task.sftp.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultSftpEntity extends TaskTaskResultEntity implements ITaskResultSftpEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];

	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String command = null ;
		private String src = null;
		private String dest = null;

		public String getCommand() {
			return command;
		}
		public void setCommand(String command) {
			this.command = command;
		}
		public String getDest() {
			return dest;
		}
		public void setDest(String dest) {
			this.dest = dest;
		}
		public String getSrc() {
			return src;
		}
		public void setSrc(String src) {
			this.src = src;
		}

	}

	public ITaskResultEntity[] getResult() {
		return result;
	}


	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}
}
