package jp.co.blueship.tri.bm.task.mkdir.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultMkdirEntity extends TaskTaskResultEntity implements ITaskResultMkdirEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];

	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {

		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String dest = null ;

		public String getDest() {
			return dest;
		}
		public void setDest(String dest) {
			this.dest = dest;
		}

	}

	public ITaskResultEntity[] getResult() {
		return result;
	}

	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}

}
