package jp.co.blueship.tri.bm.task.mkdir.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskMkdirEntity extends TaskTaskEntity implements ITaskMkdirEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskParamEntity[] param = new ITaskParamEntity[0];
	private String doExistSkip = StatusFlg.off.value();
	private String doDetailSnap = StatusFlg.off.value();

	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		private static final long serialVersionUID = 1L;

		private String src = null ;

		public String getSrc() {
			return src ;
		}
		public void setSrc( String value ) {
			src = value ;
		}
	}

	public String getDoExistSkip() {
		return doExistSkip;
	}

	public void setDoExistSkip(String doExistSkip) {
		this.doExistSkip = doExistSkip;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public ITaskParamEntity[] getParam() {
		return param;
	}

	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}
}
