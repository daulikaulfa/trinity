package jp.co.blueship.tri.bm.task.mkdir.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskMkdirEntity extends IEntity, ITaskTaskTypeEntity {

	/**
	 * パラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam();
	/**
	 * パラメタ情報を設定します。
	 *
	 * @param value パラメタ情報
	 */
	public void setParam( ITaskParamEntity[] value );
	/**
	 * 対象ディレクトリが既に存在する場合、スキップするかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoExistSkip();
	/**
	 * 対象ディレクトリが既に存在する場合、スキップするかどうかを設定します。
	 *
	 * @param doExistSkip ディレクトリが既に存在する場合、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoExistSkip(String doExistSkip);

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );

	/**
	 * パラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 作成対象のディレクトリパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc() ;
		/**
		 * 作成対象のディレクトリパス
		 *
		 * @param value 作成対象のディレクトリパス
		 */
		public void setSrc( String value ) ;
	}

}
