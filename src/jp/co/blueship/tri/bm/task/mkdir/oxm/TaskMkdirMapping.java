package jp.co.blueship.tri.bm.task.mkdir.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.ITaskMkdirEntity;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.TaskMkdirEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.MkdirType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Mkdirタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskMkdirMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		MkdirType[] tasks = src.getMkdirArray();

		if( null != tasks ) {
			for( MkdirType task : tasks ) {
				TaskMkdirEntity taskEntity = new TaskMkdirEntity();

				copyProperties( task, taskEntity );

				MkdirType.Param[] params = task.getParamArray();

				if( null != params && 0 < params.length ) {
					List<TaskMkdirEntity.TaskParamEntity> paramList = new ArrayList<TaskMkdirEntity.TaskParamEntity>();

					for( MkdirType.Param param : params ) {
						TaskMkdirEntity.TaskParamEntity paramEntity = taskEntity.new TaskParamEntity();
						paramEntity.setSrc( param.getSrc() );
						paramList.add( paramEntity );
					}
					TaskMkdirEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskMkdirEntity.TaskParamEntity[ 0 ] );
					taskEntity.setParam( paramArray );
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultMkdirMapping resultMapping = new TaskResultMkdirMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskMkdirEntity) )
					continue;

				TaskMkdirEntity taskEntity = (TaskMkdirEntity)entity;

				MkdirType task = dest.addNewMkdir();

				copyProperties( taskEntity, task );

				ITaskMkdirEntity.ITaskParamEntity[] paramArray = taskEntity.getParam();

				if( null != paramArray ) {
					for( ITaskMkdirEntity.ITaskParamEntity paramEntity : paramArray ) {
						MkdirType.Param param = task.addNewParam();
						param.setSrc( paramEntity.getSrc() );
					}
				}
			}
		}

		//Result
		TaskResultMkdirMapping resultMapping = new TaskResultMkdirMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
