package jp.co.blueship.tri.bm.task.mkdir;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.ITaskMkdirEntity;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.ITaskResultMkdirEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.TaskMkdirEntity;
import jp.co.blueship.tri.bm.task.mkdir.oxm.eb.TaskResultMkdirEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「mkdir」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元にディレクトリを作成する。
 *
 */
public class TaskProcMkdir implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;
	private static final ILog log = TriLogFactory.getInstance();

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskMkdirEntity mkdir = (TaskMkdirEntity) task ;

		ITaskMkdirEntity.ITaskParamEntity[] paramArray = mkdir.getParam() ;
		if( null != paramArray ) {
			List<ITaskMkdirEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskMkdirEntity.ITaskParamEntity>() ;
			for( ITaskMkdirEntity.ITaskParamEntity param : paramArray ) {
				String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , param.getSrc() ) ;
				if( null != arrays ) {
					for( String array : arrays ) {
						ITaskMkdirEntity.ITaskParamEntity entity = mkdir.new TaskParamEntity() ;
						TriPropertyUtils.copyProperties( entity, param );
						entity.setSrc( array ) ;
						arrlst.add( entity ) ;
					}
				}
			}
			paramArray = arrlst.toArray( new ITaskMkdirEntity.ITaskParamEntity[ 0 ] ) ;
			mkdir.setParam( paramArray ) ;
		}
	}

	/**
	 * 「mkdir」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskMkdirEntity) )
			return null ;

		TaskResultMkdirEntity resultMkdirEntity = new TaskResultMkdirEntity() ;
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>() ;

		resultMkdirEntity.setSequence( prmTask.getSequence() );
		resultMkdirEntity.setCode( TaskResultCode.UN_PROCESS );
		resultMkdirEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask ) ;

			ITaskMkdirEntity mkdir = (ITaskMkdirEntity) prmTask ;

			if( null != mkdir.getParam() ) {

				for( ITaskMkdirEntity.ITaskParamEntity param : mkdir.getParam() ) {
					//mkdirごとに処理結果を記録する
					ITaskResultEntity innerResultEntity = resultMkdirEntity.newResult();
					innerResultEntity.setSequence( String.valueOf(resultSequence++) );
					innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
					innerResultEntity.setDest( param.getSrc() );

					try {

						File file = new File( param.getSrc() ) ;
						if( true == file.exists() ) {

							boolean doSkip = ( StatusFlg.on.value().equals( mkdir.getDoExistSkip() ) ) ? true : false ;
							if( true == doSkip ) {
								LogHandler.info( log , TriLogMessage.LBM0009 , param.getSrc() );
								continue;
							} else {
								throw new TriSystemException( BmMessageId.BM004043F , param.getSrc());
							}
						}

						if( true == file.mkdirs() ) {
						} else {
							throw new TriSystemException( BmMessageId.BM004044F , param.getSrc() );
						}

						innerResultEntity.setCode( TaskResultCode.SUCCESS );

					} catch ( Exception e ) {
						innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
						throw e;
					} finally {
						if ( StatusFlg.on.value().equals( mkdir.getDoDetailSnap() ) ) {
							resultList.add( innerResultEntity );
						} else {
							if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
								resultList.add( innerResultEntity );
							}
						}
					}
				}
			}

			resultMkdirEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultMkdirEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005024S ,e);
		} finally {
			resultMkdirEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultMkdirEntity );
		}
		return resultMkdirEntity;
	}
}
