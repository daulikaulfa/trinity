package jp.co.blueship.tri.bm.task.unzip.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity.ITaskUnzipFileEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.TaskResultUnzipEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;
import jp.co.blueship.tri.fw.schema.beans.task.UnzipResultType;

/**
 * Unzipタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultUnzipMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			UnzipResultType[] results = src.getUnzipArray();

			if( null != results ) {
				for( UnzipResultType result : results ) {
					ITaskResultUnzipEntity resultEntity = new TaskResultUnzipEntity() ;

					copyProperties( result , resultEntity ) ;

					//Result
					UnzipResultType.Result[] resultArray = result.getResultArray();

					if( null != resultArray && 0 < resultArray.length ) {
						List<ITaskResultUnzipEntity.ITaskResultEntity> innerResultList = new ArrayList<ITaskResultUnzipEntity.ITaskResultEntity>();

						for( UnzipResultType.Result innerResult : resultArray ) {
							ITaskResultUnzipEntity.ITaskResultEntity innerResultEntity = resultEntity.newResult();

							copyProperties( innerResult , innerResultEntity ) ;

							innerResultList.add( innerResultEntity );

							// UnzipFile
							if( null != innerResult.getUnzipFileArray() ) {
								List<ITaskUnzipFileEntity> unzipFileList = new ArrayList<ITaskUnzipFileEntity>() ;

								for( jp.co.blueship.tri.fw.schema.beans.task.UnzipResultType.Result.UnzipFile innerUnzipFile : innerResult.getUnzipFileArray() ) {
									ITaskUnzipFileEntity innerUnzipFileEntity = innerResultEntity.newUnzipFile();
									copyProperties( innerUnzipFile, innerUnzipFileEntity );

									unzipFileList.add( innerUnzipFileEntity );
								}

								innerResultEntity.setUnzipFile( unzipFileList.toArray( new ITaskUnzipFileEntity[0] ) );
							}
						}
						resultEntity.setResult( innerResultList.toArray( new ITaskResultUnzipEntity.ITaskResultEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultUnzipEntity) )
					continue;

				ITaskResultUnzipEntity resultEntity = (ITaskResultUnzipEntity)entity;

				UnzipResultType result = dest.addNewUnzip();

				copyProperties( resultEntity, result );

				//Result
				ITaskResultUnzipEntity.ITaskResultEntity[] innerResultArray = resultEntity.getResult();

				if( null != innerResultArray ) {
					for( ITaskResultUnzipEntity.ITaskResultEntity innerResultEntity : innerResultArray ) {
						UnzipResultType.Result innerResult = result.addNewResult();

						copyProperties( innerResultEntity , innerResult ) ;


						// UnzipFile
						ITaskUnzipFileEntity[] unzipFileArray = innerResultEntity.getUnzipFile();
						if ( null != unzipFileArray ) {
							for ( ITaskUnzipFileEntity innerUnzipFileEntity : unzipFileArray ) {
								jp.co.blueship.tri.fw.schema.beans.task.UnzipResultType.Result.UnzipFile innerUnzipFile = innerResult.addNewUnzipFile();
								copyProperties( innerUnzipFileEntity, innerUnzipFile );
							}
						}
					}
				}

			}
		}

		return dest;
	}

}
