package jp.co.blueship.tri.bm.task.unzip.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.TaskUnzipEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Unzip;

/**
 * Unzipタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskUnzipMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskUnzipEntity> entitys = new ArrayList<ITaskUnzipEntity>();

		Unzip[] tasks = src.getUnzipArray();

		if( null != tasks ) {
			for( Unzip task : tasks ) {
				TaskUnzipEntity taskEntity = new TaskUnzipEntity();

				copyProperties( task, taskEntity );

				//Param
				Unzip.Param[] params = task.getParamArray();

				if( null != params && 0 < params.length ) {
					List<TaskUnzipEntity.TaskParamEntity> paramList = new ArrayList<TaskUnzipEntity.TaskParamEntity>();

					for( Unzip.Param param : params ) {
						TaskUnzipEntity.TaskParamEntity paramEntity = taskEntity.new TaskParamEntity();

						copyProperties( param , paramEntity ) ;

						paramList.add( paramEntity );
					}
					TaskUnzipEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskUnzipEntity.TaskParamEntity[ 0 ] );
					taskEntity.setParam( paramArray );
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultUnzipMapping resultMapping = new TaskResultUnzipMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof TaskUnzipEntity) )
					continue;

				TaskUnzipEntity unzipEntity = (TaskUnzipEntity)entity;

				Unzip task = dest.addNewUnzip();

				copyProperties( unzipEntity, task );

				//Param
				TaskUnzipEntity.ITaskParamEntity[] paramArray = unzipEntity.getParam();

				if( null != paramArray ) {
					for( TaskUnzipEntity.ITaskParamEntity paramEntity : paramArray ) {
						Unzip.Param param = task.addNewParam();

						copyProperties( paramEntity , param ) ;
					}
				}
			}
		}

		//Result
		TaskResultUnzipMapping resultMapping = new TaskResultUnzipMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
