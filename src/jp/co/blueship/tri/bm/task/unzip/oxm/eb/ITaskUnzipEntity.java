package jp.co.blueship.tri.bm.task.unzip.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskUnzipEntity extends ITaskTaskTypeEntity {

	/**
	 * パラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam();
	/**
	 * パラメタ情報を設定します。
	 *
	 * @param value
	 */
	public void setParam( ITaskParamEntity[] value );

	/**
	 * 解凍定義ファイルパスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDefineFilePath();
	/**
	 * 解凍定義ファイルパスを設定します。
	 *
	 * @param value 定義ファイルパス
	 */
	public void setDefineFilePath( String value );

	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotExistSkip() ;
	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @param doSkip 元が存在しなければ、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotExistSkip(String doSkip) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap() ;
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap(String doSkip) ;

	/**
	 * 展開したファイルツリーを記録するかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoRecordFile();
	/**
	 * 展開したファイルツリーを記録するかどうかを設定します。
	 *
	 * @param value 展開したファイルツリーを記録する場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は記録しない。
	 */
	public void setDoRecordFile( String value );


	/**
	 * （定義ファイルを使わない場合）
	 * パラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @param value 対象ディレクトリ／ファイル
		 */
		public void setSrc( String value );
		/**
		 * 展開先のディレクトリパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 展開先のディレクトリパス
		 *
		 * @param value 展開先のディレクトリパス
		 */
		public void setDest( String value );

		/**
		 * バイトコードに復号化する際の文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getEncoding();
		/**
		 * バイトコードに復号化する際の文字コード
		 *
		 * @param value 文字コード。{@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}で定義された文字列
		 */
		public void setEncoding( String value );
	}

}
