package jp.co.blueship.tri.bm.task.unzip.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultUnzipEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult( ITaskResultEntity[] values );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineFilePath();
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @param value 外部定義ファイル（定義リスト）
		 */
		public void setDefineFilePath(String value);
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineDetailFilePath();
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @param value 外部定義ファイル（詳細定義リスト）
		 */
		public void setDefineDetailFilePath(String value);
		/**
		 * 展開元のＺＩＰファイルパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * 展開元のＺＩＰファイルパス
		 *
		 * @param value 展開元のＺＩＰファイルパス
		 */
		public void setSrc( String value );
		/**
		 * 展開先のディレクトリパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 展開先のディレクトリパス
		 *
		 * @param value 展開先のディレクトリパス
		 */
		public void setDest( String value );

		/**
		 * 展開したファイルツリーを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskUnzipFileEntity[] getUnzipFile();
		/**
		 * 展開したファイルツリーを設定します。
		 *
		 * @param value 展開したファイルツリー
		 */
		public void setUnzipFile( ITaskUnzipFileEntity[] value );
		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskUnzipFileEntity newUnzipFile();
	}

	/**
	 *
	 * 展開したファイルの結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskUnzipFileEntity extends ITaskResultTypeEntity {
		public String getPath();
		public void setPath( String value );
	}
}
