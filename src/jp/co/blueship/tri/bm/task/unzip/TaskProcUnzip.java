package jp.co.blueship.tri.bm.task.unzip;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity.ITaskUnzipFileEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.TaskResultUnzipEntity;
import jp.co.blueship.tri.fw.agent.core.TaskDefineFileUtils;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.FileIoZip;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「unzip」タスクの処理内容を記述します
 * <p>
 * <p>
 * 定義された情報を元にＺＩＰ解凍を行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskProcUnzip implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * アーカイブ名をバイトシーケンスに復号化する文字コード。
	 */
	private static final String ENCODING = "ENCODING";
	/**
	 * アーカイブ名を示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String ARCHIVE_NAME = "ARCHIVE_NAME";
	/**
	 * アーカイブ出力先ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String OUTPUT_PATH = "OUTPUT_PATH";
	/**
	 * アーカイブ解凍対象パッケージ トップ(ルート)ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String SRC_BASE_PATH = "SRC_BASE_PATH";

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		ITaskUnzipEntity unzip = (ITaskUnzipEntity) task;

		ITaskUnzipEntity.ITaskParamEntity[] paramArray = unzip.getParam();
		if( null != paramArray ) {
			for ( ITaskUnzipEntity.ITaskParamEntity entity : paramArray ) {
				entity.setSrc( TaskStringUtils.substitutionPath( propertyEntity , entity.getSrc() ) );
				entity.setDest( TaskStringUtils.substitutionPath( propertyEntity , entity.getDest() ) );
			}
		}

		unzip.setDefineFilePath(
				TaskStringUtils.substitutionPath( this.propertyEntity , unzip.getDefineFilePath() ));
	}

	/**
	 * 「unzip」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskUnzipEntity) )
			return null;

		ITaskResultUnzipEntity resultUnzipEntity = new TaskResultUnzipEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultUnzipEntity.setSequence( prmTask.getSequence() );
		resultUnzipEntity.setCode( TaskResultCode.UN_PROCESS );
		resultUnzipEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskUnzipEntity unzip = (ITaskUnzipEntity) prmTask;

			List<UnzipDefinition> unzipDefList = new ArrayList<UnzipDefinition>();

			// 定義ファイルによらないzip解凍
			resultSequence = this.zip(unzip, resultUnzipEntity, resultList, resultSequence);

			// 定義リストファイルの設定あり
			if ( null != unzip.getDefineFilePath() ) {
				File defListFile = new File( unzip.getDefineFilePath() );
				if ( !defListFile.exists() ) {
					throw new TriSystemException( BmMessageId.BM004011F , unzip.getDefineFilePath() );
				}

				unzipDefList.addAll( getReformDefFileList( defListFile ) );
				resultSequence = this.reform(unzipDefList, unzip, resultUnzipEntity, resultList, resultSequence);
			}

			resultUnzipEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultUnzipEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005030S , e );
		} finally {
			resultUnzipEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultUnzipEntity );
		}

		return resultUnzipEntity;
	}

	/**
	 * 定義ファイルによらない、zipを作成する。
	 * @param unzip ＺＩＰ圧縮タスク
	 * @param resultUnzipEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 */
	private Integer zip(
							ITaskUnzipEntity unzip,
							ITaskResultUnzipEntity resultUnzipEntity,
							List<ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		if( null == unzip.getParam() || 0 == unzip.getParam().length ) {
			return sequence;
		}

		for ( ITaskUnzipEntity.ITaskParamEntity param: unzip.getParam() ) {
			//paramごとに処理結果を記録する
			ITaskResultEntity innerResultEntity = resultUnzipEntity.newResult();
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

			try {
				innerResultEntity.setSequence( String.valueOf(sequence++) );
				innerResultEntity.setSrc( TriStringUtils.convertPath( param.getSrc() ) );
				innerResultEntity.setDest( TriStringUtils.convertPath( param.getDest() ) );

				boolean doNotExistSkip = ( StatusFlg.on.value().equals( unzip.getDoNotExistSkip() ) ) ? true : false;

				File unzipFile = new File( param.getSrc() );
				if ( ! doNotExistSkip && ! unzipFile.isFile() )
					throw new TriSystemException(BmMessageId.BM004040F , TriStringUtils.convertPath(unzipFile.getPath()) );

				File unzipDest = new File( param.getDest() );
				if ( ! unzipDest.isDirectory() )
					unzipDest.mkdirs();

				if ( unzipFile.isFile() ) {

					Charset encoding = getCharset( param.getEncoding() );
					Set<String> unzipFileSet = new TreeSet<String>();

					FileIoZip.UnCompressZip( param.getSrc() , param.getDest() , encoding, unzipFileSet );

					// 結果の記録
					setUnzipFileResult( unzip, innerResultEntity, unzipFileSet );
					innerResultEntity.setCode( TaskResultCode.SUCCESS );
				}

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( unzip.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		return sequence;
	}


	/**
	 * 結果に展開したファイルツリーをセットする
	 * @param unzip
	 * @param innerResultEntity
	 * @param unzipFileSet
	 */
	private void setUnzipFileResult( ITaskUnzipEntity unzip, ITaskResultEntity innerResultEntity, Set<String> unzipFileSet ) {

		if ( !StatusFlg.on.value().equals( unzip.getDoRecordFile() )) {
			innerResultEntity.setUnzipFile( new ITaskUnzipFileEntity[0] );
			return;
		}


		List<ITaskUnzipFileEntity> unzipFileList = new ArrayList<ITaskUnzipFileEntity>();

		for ( String unzipFile : unzipFileSet ) {
			ITaskUnzipFileEntity unzipFileEntity = innerResultEntity.newUnzipFile();
			unzipFileEntity.setPath( unzipFile );

			unzipFileList.add( unzipFileEntity );
		}

		innerResultEntity.setUnzipFile( unzipFileList.toArray( new ITaskUnzipFileEntity[0] ));
		return;
	}


	/**
	 * 定義ファイルによる、zipを作成する。
	 * @param unzipDefList zip定義情報のリスト
	 * @param unzip ＺＩＰ圧縮タスク
	 * @param resultunzipEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 */
	private Integer reform(
							List<UnzipDefinition> unzipDefList,
							ITaskUnzipEntity unzip,
							ITaskResultUnzipEntity resultUnzipEntity,
							List<ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		for ( UnzipDefinition unzipDef : unzipDefList ) {
			//filesetごとに処理結果を記録する
			ITaskResultUnzipEntity.ITaskResultEntity innerResultEntity = resultUnzipEntity.newResult();
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

			try {
				innerResultEntity.setSequence( String.valueOf(sequence++) );
				innerResultEntity.setDefineFilePath( unzip.getDefineFilePath() );
				innerResultEntity.setDefineDetailFilePath( unzipDef.getDefineDetailFilePath() );

				File srcBaseDir = null;
				if ( null != unzipDef.getSrcBasePath() ) {
					srcBaseDir = new File( unzipDef.getSrcBasePath() );
				}

				File unzipFile = null;
				if ( null != unzipDef.getSrcBasePath() ) {
					unzipFile = new File( srcBaseDir, unzipDef.getUnzipName() );
				} else {
					unzipFile = new File( unzipDef.getUnzipName() );
				}
				innerResultEntity.setSrc( TriStringUtils.convertPath(unzipFile.getPath()) );
				innerResultEntity.setDest( TriStringUtils.convertPath(unzipDef.getOutputPath()) );

				boolean doNotExistSkip = ( StatusFlg.on.value().equals( unzip.getDoNotExistSkip() ) ) ? true : false;

				if ( ! doNotExistSkip && ! unzipFile.isFile() )
					throw new TriSystemException(BmMessageId.BM004040F , TriStringUtils.convertPath(unzipFile.getPath()) );

				File unzipDest = new File( unzipDef.getOutputPath() );
				if ( ! unzipDest.isDirectory() )
					unzipDest.mkdirs();

				if ( unzipFile.isFile() ) {
					Charset encoding = getCharset( unzipDef.getEncoding() );

					FileIoZip.UnCompressZip( unzipFile.getPath(), unzipDef.getOutputPath(), encoding );

					// 結果の記録
					innerResultEntity.setCode( TaskResultCode.SUCCESS );
				}

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( unzip.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		return sequence;
	}

	/**
	 * アーカイブ定義リストファイルを読み込み、アーカイブ定義情報のリストを返す。
	 * @param defListFile アーカイブ定義リストファイル
	 * @return アーカイブ定義情報のリスト
	 */
	private List<UnzipDefinition> getReformDefFileList( File defListFile ) throws Exception {

		List<File> defList = TaskDefineFileUtils.getDefFileList( defListFile );

		List<UnzipDefinition> zipDefList = new ArrayList<UnzipDefinition>();

		for ( File defFile : defList ) {
			zipDefList.add( getUnzipDefinition( defFile ));
		}

		return zipDefList;
	}

	/**
	 * アーカイブ定義ファイルを読み込み、アーカイブ定義情報を返す。
	 * @param defFile アーカイブ定義ファイル
	 * @return アーカイブ定義情報
	 */
	private UnzipDefinition getUnzipDefinition( File defFile ) throws Exception {

		UnzipDefinition jd	= new UnzipDefinition();
		jd.setDefineDetailFilePath( TriStringUtils.convertPath(defFile.getPath()) );

		int lineNumber		= 0;

		try {
			Map<String,String> aliasDic = new HashMap<String,String>();

			String[] lineArray = TriFileUtils.readFileLine( defFile , false ) ;
			for( String line : lineArray ) {
				lineNumber = lineNumber + 1;

				// コメント、空白行はスキップ
				if ( !TaskDefineFileUtils.isContents( line ))		continue;
				// 変数の定義行は、変数登録をしてスキップ
				if ( TaskDefineFileUtils.isAlias( line, aliasDic ))	continue;

				// 変数を置換
				line = TaskDefineFileUtils.replaceAliasVariable( line, aliasDic );

				// エンコーディングの定義行でがあれば、設定してスキップ
				if ( setEncoding( line, jd ))		continue;
				// アーカイブ名の定義行でがあれば、設定してスキップ
				if ( setUnzipName( line, jd ))		continue;
				// アーカイブ出力先の定義行でがあれば、設定してスキップ
				if ( setOutputPath( line, jd ))		continue;
				// アーカイブ解凍対象パッケージ トップ(ルート)ディレクトリの定義行でがあれば、設定してスキップ
				if ( setSrcBasePath( line, jd ))	continue;
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM004021F , e , defFile.getAbsolutePath() , String.valueOf(lineNumber) );
		}
		// アーカイブ名がなければエラー
		if ( null == jd.getUnzipName() ) {
			throw new TriSystemException(BmMessageId.BM004053F , defFile.getAbsolutePath() );
		}

		return jd;
	}

	/**
     * エンコーディングの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setEncoding( String line, UnzipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, ENCODING );
    	if ( null == value ) {
    		return false;
    	} else {
    		jd.setEncoding( value );
    		return  true;
    	}
    }

	/**
     * アーカイブ名の定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setUnzipName( String line, UnzipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, ARCHIVE_NAME );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setUnzipName( newValue );
    		return  true;
    	}
    }

	/**
     * アーカイブ出力先ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setOutputPath( String line, UnzipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, OUTPUT_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setOutputPath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * アーカイブ解凍対象パッケージ トップ(ルート)ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setSrcBasePath( String line, UnzipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, SRC_BASE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setSrcBasePath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }
	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする<br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004039F, e ,encoding);
		}
		return charset;
	}

 	/**
	 * ＺＩＰ解凍定義ファイルの情報を保持するクラス
	 */
	public class UnzipDefinition {

		/** エンコーディング */
		private String encoding = null;
		/** リフォーム定義ファイルパス */
		private String defineDetailFilePath = null;
		/** 展開元アーカイブ名 */
		private String unzipName = null;
		/** 展開先アーカイブ出力先ディレクトリ */
		private String outputPath = null;
		/** 展開元アーカイブ対象パッケージ トップ(ルート)ディレクトリ */
		private String srcBasePath = null;

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getUnzipName() {
			return unzipName;
		}
		public void setUnzipName( String unzipName ) {
			this.unzipName = unzipName;
		}

		public String getOutputPath() {
			return outputPath;
		}
		public void setOutputPath( String outputPath ) {
			this.outputPath = outputPath;
		}

		public String getSrcBasePath() {
			return srcBasePath;
		}
		public void setSrcBasePath( String srcBasePath ) {
			this.srcBasePath = srcBasePath;
		}

		public String getEncoding() {
			return encoding;
		}
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}
	}

}
