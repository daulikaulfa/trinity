package jp.co.blueship.tri.bm.task.cBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.ITaskResultCBuildEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.TaskResultCBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CBuildResultType;
import jp.co.blueship.tri.fw.schema.beans.task.LogType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * CBuildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultCBuildMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			CBuildResultType[] results = src.getCBuildArray();

			if( null != results ) {
				for( CBuildResultType result : results ) {
					TaskResultCBuildEntity resultEntity = new TaskResultCBuildEntity() ;

					copyProperties( result , resultEntity ) ;

					LogType log = result.getLog() ;
					if( null != log ) {
						copyProperties( log , resultEntity ) ;
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultCBuildEntity) )
					continue;

				TaskResultCBuildEntity resultEntity = (TaskResultCBuildEntity)entity;

				CBuildResultType result = dest.addNewCBuild();

				copyProperties( resultEntity, result );

				LogType log = result.addNewLog() ;
				copyProperties( resultEntity, log );
			}
		}

		return dest;
	}

}
