package jp.co.blueship.tri.bm.task.cBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskCBuildEntity extends ITaskTaskTypeEntity {

	public String getName() ;
	public void setName(String value) ;
	public String getWorkDir() ;
	public void setWorkDir(String value) ;
	public String getExecuteShell() ;
	public void setExecuteShell(String value) ;
	public String getOutLogPath() ;
	public void setOutLogPath( String value ) ;
	public String getErrLogPath() ;
	public void setErrLogPath( String value ) ;

	public ITaskBuildPathEntity getBuildPathEntity() ;
	public void setBuildPathEntity( ITaskBuildPathEntity obj ) ;

	public ICharsetInfoEntity getCharsetInfo() ;
	public void setCharsetInfo( ICharsetInfoEntity obj ) ;

	/**
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskBuildPathEntity extends IEntity {
		public String getBinOutput() ;
		public void setBinOutput(String value) ;

		public ITaskPathTypeEntity[] getSrcParam() ;
		public void setSrcParam( ITaskPathTypeEntity[] obj ) ;

		public ITaskPathTypeEntity[] getIntensiveSrcParam() ;
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getDeleteSrcParam() ;
		public void setDeleteSrcParam( ITaskPathTypeEntity[] obj ) ;
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskPathTypeEntity extends IEntity {
		public String getPath() ;
		public void setPath( String value ) ;
	}
}
