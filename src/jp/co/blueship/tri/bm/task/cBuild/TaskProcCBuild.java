package jp.co.blueship.tri.bm.task.cBuild;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskLogUtils;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.ITaskCBuildEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.ITaskCBuildEntity.ITaskPathTypeEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.TaskCBuildEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.TaskResultCBuildEntity;
import jp.co.blueship.tri.fw.agent.ShellExecute;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「cBuild」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとにcビルドタスクの実行を行う。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class TaskProcCBuild implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private static final ILog log = TriLogFactory.getInstance();

	private ITaskPropertyEntity[] propertyEntity = null;

	private static final TriFunction<ITaskPathTypeEntity, String> TO_PATH_STRING = //
	new TriFunction<ITaskCBuildEntity.ITaskPathTypeEntity, String>() {

		@Override
		public String apply(ITaskPathTypeEntity entity) {
			return entity.getPath();
		}
	};

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath(ITaskTaskTypeEntity task) throws Exception {
		TaskCBuildEntity build = (TaskCBuildEntity) task;

		build.setWorkDir(TaskStringUtils.substitutionPath(propertyEntity, build.getWorkDir()));
		build.setExecuteShell(TaskStringUtils.substitutionPath(propertyEntity, build.getExecuteShell()));
		build.setOutLogPath(TaskStringUtils.substitutionPath(propertyEntity, build.getOutLogPath()));
		build.setErrLogPath(TaskStringUtils.substitutionPath(propertyEntity, build.getErrLogPath()));

		ITaskCBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity();
		if (null != buildPathEntity) {
			// ","区切りの複数パラメータに対応
			buildPathEntity.setSrcParam(substitutePathSubPathType(build, buildPathEntity.getSrcParam()));// SrcParam
			buildPathEntity.setIntensiveSrcParam(substitutePathSubPathType(build, buildPathEntity.getIntensiveSrcParam()));// IntensiveSrcParam
			buildPathEntity.setDeleteSrcParam(substitutePathSubPathType(build, buildPathEntity.getDeleteSrcParam()));// DeleteSrcParam

			buildPathEntity.setBinOutput(TaskStringUtils.substitutionPath(propertyEntity, buildPathEntity.getBinOutput()));// BinOutput
		}
	}

	/**
	 *
	 * @param build
	 * @param pathEntityArray
	 * @return
	 * @throws Exception
	 */
	private ITaskCBuildEntity.ITaskPathTypeEntity[] substitutePathSubPathType(TaskCBuildEntity build,
			ITaskCBuildEntity.ITaskPathTypeEntity[] pathEntityArray) throws Exception {
		List<ITaskCBuildEntity.ITaskPathTypeEntity> arrlst = new ArrayList<ITaskCBuildEntity.ITaskPathTypeEntity>();

		for (ITaskCBuildEntity.ITaskPathTypeEntity pathEntity : pathEntityArray) {
			String[] arrays = TaskStringUtils.substitutionPaths(propertyEntity, pathEntity.getPath());
			if (null != arrays) {
				for (String array : arrays) {
					ITaskCBuildEntity.ITaskPathTypeEntity entity = build.new TaskPathTypeEntity();
					TriPropertyUtils.copyProperties(entity, pathEntity);
					entity.setPath(array);
					arrlst.add(entity);
				}
			}
		}
		return arrlst.toArray(new ITaskCBuildEntity.ITaskPathTypeEntity[0]);
	}

	/**
	 * 「cBuild」タスクの実際の処理を記述します<br>
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute(ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask) throws Exception {
		if (!(prmTask instanceof ITaskCBuildEntity))
			return null;

		TaskResultCBuildEntity resultCBuildEntity = new TaskResultCBuildEntity();
		String taskResultCode = TaskResultCode.SUCCESS;
		resultCBuildEntity.setSequence(prmTask.getSequence());
		resultCBuildEntity.setTaskId(prmTask.getTaskId());

		String outLog = null;
		String errLog = null;
		ITaskCBuildEntity build = (ITaskCBuildEntity) prmTask;
		ShellExecute shellExecute = null;
		try {
			substitutePath(build);

			// 当初のMakefile生成型から、V1と同様の単純キックモデルに移行したため、パラメータは単純化
			// executeshellを仮にMakefileのパスとして使う
			ITaskCBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity();

			if (null == buildPathEntity) {
				return resultCBuildEntity ;
			}
			// 資産パスは現在利用していない。2011/11/07

			boolean buildGo = false;
			/**  削除資産のチェック  **/
			// deleteSrcのパスをチェックし、すべての削除対象ファイルが原本に存在していればビルド処理を実行、１件でも存在しなければスルーする

			String[] deleteSrcArray = pathArrayFrom(buildPathEntity.getDeleteSrcParam());

			if (TriStringUtils.isEmpty(deleteSrcArray)) {// deleteSrcのパス自体が設定されていない⇒intensiveSrcのみで実行可否を判断する。
				// buildGo = true ;//パス定義が存在しない場合、intensiveSrcのみで実行可否を判断する。
			} else {
				for (String path : deleteSrcArray) {
					if ( TriFileUtils.checkExistFiles( path, false, BusinessFileUtils.getFileFilter() ) ) {
						buildGo = true;
						break;
					}
				}
			}
			/**  返却資産のチェック  **/
			// intensiveSrcのパスを再帰的に辿って、返却資産が１件でもあればビルド処理を実行、１件もなければビルド処理をスルーする
			// また、intensiveSrcのパス自体が設定されていない場合には、ビルド処理を行う。
			String[] intensiveSrcArray = pathArrayFrom(buildPathEntity.getIntensiveSrcParam());
			if (TriStringUtils.isEmpty(intensiveSrcArray)) {// intensiveSrcのパス自体が設定されていない⇒ビルド実行
				buildGo = true;
			} else {
				FileFilter filter = BusinessFileUtils.getFileFilter();
				for (String path : intensiveSrcArray) {
					if ( TriFileUtils.checkExistFiles(path, false, filter) ) {
						buildGo =true;
						break;
					}
				}
			}
			if (true != buildGo) {// コンパイル対象となる返却資産が１件もないのでビルドを行わず抜ける
				LogHandler.info( log, TriLogMessage.LBM0001 );
				taskResultCode = TaskResultCode.UN_PROCESS;
				return resultCBuildEntity;
			} else {
				//
				Charset inCharset = build.getCharsetInfo() != null ? Charset.value( build.getCharsetInfo().getIn() ) : null;

				shellExecute = new ShellExecute( inCharset );

				shellExecute.setLog(TriLogFactory.getInstance());
				shellExecute.setExecShellPath(build.getExecuteShell());
				File file = new File(build.getExecuteShell());
				if (TriStringUtils.isEmpty(build.getWorkDir())) {
					shellExecute.setWorkDir(file.getParent().toString());
				} else {
					shellExecute.setWorkDir(build.getWorkDir());
				}
				shellExecute.execute();

				if (0 != shellExecute.getExitValue()) {
					throw new TriSystemException( BmMessageId.BM005007S , String.valueOf( shellExecute.getExitValue() ) );
				}
			}
		} catch (Exception e) {
			taskResultCode = TaskResultCode.ERROR_PROCESS;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005008S,e);
		} finally {
			try {
				if (shellExecute != null) {
					outLog = shellExecute.getOutLog();
					errLog = shellExecute.getErrLog();
				}
				String outCharset = build.getCharsetInfo() != null ? build.getCharsetInfo().getOut() : null;
				resultCBuildEntity.setOutLog( TaskLogUtils.writeLog( build.getOutLogPath() , outLog , Charset.value( outCharset ) ) );
				resultCBuildEntity.setErrLog( TaskLogUtils.writeLog( build.getErrLogPath() , errLog , Charset.value( outCharset ) ) );
			} catch (Exception e) {
				taskResultCode = TaskResultCode.ERROR_PROCESS;
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException(BmMessageId.BM005008S,e);
			} finally {
				resultCBuildEntity.setCode(taskResultCode);
				prmTarget.addTaskResult(resultCBuildEntity);
			}
		}

		return resultCBuildEntity;
	}

	/**
	 * ITaskPathTypeEntityオブジェクトの配列からのsrc文字列のみを配列に格納して返す<br>
	 *
	 * @param pathEntityArrayオブジェクト
	 * @return src文字列の配列
	 */
	private String[] pathArrayFrom(ITaskPathTypeEntity[] pathEntityArray) {

		if (pathEntityArray == null) {
			return new String[0];
		}

		return FluentList.from(pathEntityArray).//
				map(TO_PATH_STRING).//
				toArray(new String[0]);
	}

}
