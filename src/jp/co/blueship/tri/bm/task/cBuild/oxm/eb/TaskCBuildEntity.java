package jp.co.blueship.tri.bm.task.cBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskCBuildEntity extends TaskTaskEntity implements ITaskCBuildEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String name = null ;
	private String workDir = null ;
	private String executeShell = null ;
	private ITaskBuildPathEntity buildPathEntity = null ;
	private ICharsetInfoEntity charsetInfoEntity = null ;

	private String outLogPath = null ;
	private String errLogPath = null ;

	public String getName() {
		return name ;
	}
	public void setName(String value) {
		name = value ;
	}
	public String getWorkDir() {
		return workDir;
	}
	public void setWorkDir(String workDir) {
		this.workDir = workDir;
	}
	public String getExecuteShell() {
		return executeShell ;
	}
	public void setExecuteShell(String value) {
		executeShell = value ;
	}

	public ITaskBuildPathEntity getBuildPathEntity() {
		return buildPathEntity ;
	}
	public void setBuildPathEntity( ITaskBuildPathEntity obj ) {
		buildPathEntity = obj ;
	}

	public ICharsetInfoEntity getCharsetInfo() {
		return charsetInfoEntity;
	}
	public void setCharsetInfo(ICharsetInfoEntity charsetInfoEntity) {
		this.charsetInfoEntity = charsetInfoEntity;
	}


	/**
	 * インナーインターフェイス
	 *
	 */
	public class TaskBuildPathEntity implements ITaskBuildPathEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String binOutput = null ;
		private ITaskPathTypeEntity[] srcParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] intensiveSrcParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] deleteSrcParam = new ITaskPathTypeEntity[0];

		public String getBinOutput() {
			return binOutput ;
		}
		public void setBinOutput(String value) {
			binOutput = value ;
		}

		public ITaskPathTypeEntity[] getSrcParam() {
			return srcParam ;
		}
		public void setSrcParam( ITaskPathTypeEntity[] obj ) {
			srcParam = obj ;
		}

		public ITaskPathTypeEntity[] getIntensiveSrcParam() {
			return intensiveSrcParam ;
		}
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] obj ) {
			intensiveSrcParam = obj ;
		}
		public ITaskPathTypeEntity[] getDeleteSrcParam() {
			return deleteSrcParam ;
		}
		public void setDeleteSrcParam( ITaskPathTypeEntity[] obj ) {
			deleteSrcParam = obj ;
		}
	}



	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public class TaskPathTypeEntity implements ITaskPathTypeEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null ;

		public String getPath() {
			return path ;
		}
		public void setPath( String value ) {
			path = value ;
		}
	}



	public String getErrLogPath() {
		return errLogPath;
	}
	public void setErrLogPath(String errLogPath) {
		this.errLogPath = errLogPath;
	}
	public String getOutLogPath() {
		return outLogPath;
	}
	public void setOutLogPath(String outLogPath) {
		this.outLogPath = outLogPath;
	}

}
