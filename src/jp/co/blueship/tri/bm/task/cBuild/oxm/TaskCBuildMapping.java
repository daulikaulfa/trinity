package jp.co.blueship.tri.bm.task.cBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.CharsetInfoEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.ITaskCBuildEntity;
import jp.co.blueship.tri.bm.task.cBuild.oxm.eb.TaskCBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CBuildType;
import jp.co.blueship.tri.fw.schema.beans.task.CBuildType.BuildPath;
import jp.co.blueship.tri.fw.schema.beans.task.ExecCharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.PathType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * CBuildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskCBuildMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		CBuildType[] tasks = src.getCBuildArray();

		if( null != tasks ) {
			for( CBuildType task : tasks ) {
				TaskCBuildEntity taskEntity = new TaskCBuildEntity();

				copyProperties( task , taskEntity ) ;

				BuildPath buildPath = task.getBuildPath() ;
				if( null != buildPath ) {
					ITaskCBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.new TaskBuildPathEntity() ;

					copyProperties( buildPath , buildPathEntity ) ;
					//src
					if( null != buildPath.getSrcArray() ) {
						List<TaskCBuildEntity.TaskPathTypeEntity> srcList = new ArrayList<TaskCBuildEntity.TaskPathTypeEntity>() ;
						for( PathType srcPath : buildPath.getSrcArray() ) {
							TaskCBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath( srcPath.getPath() ) ;
							srcList.add( pathTypeEntity ) ;
						}
						buildPathEntity.setSrcParam( srcList.toArray( new TaskCBuildEntity.TaskPathTypeEntity[0] ) ) ;
					}
					//intensiveSrc
					if( null != buildPath.getIntensiveSrcArray() ) {
						List<TaskCBuildEntity.TaskPathTypeEntity> intensiveSrcList = new ArrayList<TaskCBuildEntity.TaskPathTypeEntity>() ;
						for( PathType srcPath : buildPath.getIntensiveSrcArray() ) {
							TaskCBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath( srcPath.getPath() ) ;
							intensiveSrcList.add( pathTypeEntity ) ;
						}
						buildPathEntity.setIntensiveSrcParam( intensiveSrcList.toArray( new TaskCBuildEntity.TaskPathTypeEntity[0] ) ) ;
					}
//					deleteSrc
					if( null != buildPath.getDeleteSrcArray() ) {
						List<TaskCBuildEntity.TaskPathTypeEntity> deleteSrcList = new ArrayList<TaskCBuildEntity.TaskPathTypeEntity>() ;
						for( PathType srcPath : buildPath.getDeleteSrcArray() ) {
							TaskCBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath( srcPath.getPath() ) ;
							deleteSrcList.add( pathTypeEntity ) ;
						}
						buildPathEntity.setDeleteSrcParam( deleteSrcList.toArray( new TaskCBuildEntity.TaskPathTypeEntity[0] ) ) ;
					}
					taskEntity.setBuildPathEntity( buildPathEntity ) ;
				}

				ExecCharsetinfoType charsetInfo = task.getCharsetinfo();
				if ( null != charsetInfo ) {
					CharsetInfoEntity charsetInfoEntity = new CharsetInfoEntity();
					charsetInfoEntity.setIn(charsetInfo.getIn());
					charsetInfoEntity.setOut(charsetInfo.getOut());
					taskEntity.setCharsetInfo(charsetInfoEntity);
				}

				entitys.add( taskEntity ) ;
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultCBuildMapping resultMapping = new TaskResultCBuildMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskCBuildEntity) )
					continue;

				ITaskCBuildEntity taskEntity = (ITaskCBuildEntity)entity;
				CBuildType task = dest.addNewCBuild() ;

				copyProperties( taskEntity, task );

				ITaskCBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.getBuildPathEntity() ;
				if( null != buildPathEntity ) {
					BuildPath buildPath = task.addNewBuildPath() ;

					copyProperties( buildPathEntity , buildPath ) ;
					//src
					ITaskCBuildEntity.ITaskPathTypeEntity[] srcEntityArray = buildPathEntity.getSrcParam() ;
					if( null != srcEntityArray ) {
						for( ITaskCBuildEntity.ITaskPathTypeEntity srcEntity : srcEntityArray ) {
							PathType pathType = buildPath.addNewSrc() ;

							copyProperties( srcEntity , pathType ) ;
						}
					}
					//intensiveSrc
					ITaskCBuildEntity.ITaskPathTypeEntity[] intensiveSrcEntityArray = buildPathEntity.getIntensiveSrcParam() ;
					if( null != intensiveSrcEntityArray ) {
						for( ITaskCBuildEntity.ITaskPathTypeEntity srcEntity : intensiveSrcEntityArray ) {
							PathType pathType = buildPath.addNewIntensiveSrc() ;

							copyProperties( srcEntity , pathType ) ;
						}
					}
//					deleteSrc
					ITaskCBuildEntity.ITaskPathTypeEntity[] deleteSrcEntityArray = buildPathEntity.getDeleteSrcParam() ;
					if( null != deleteSrcEntityArray ) {
						for( ITaskCBuildEntity.ITaskPathTypeEntity srcEntity : deleteSrcEntityArray ) {
							PathType pathType = buildPath.addNewDeleteSrc() ;

							copyProperties( srcEntity , pathType ) ;
						}
					}
				}
			}
		}

//		Result
		TaskResultCBuildMapping resultMapping = new TaskResultCBuildMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
