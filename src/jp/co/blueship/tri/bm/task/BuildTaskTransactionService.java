package jp.co.blueship.tri.bm.task;

import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;


/**
 * 業務シーケンス（ビルド）でトランザクション制御を利用するための汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class BuildTaskTransactionService implements IBuildTaskService {

	private IContextAdapter context = null;
	private String nextTransactionService = null;

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	private final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return context;
	}

	/**
	 * 一度のリクエスト処理で、複数のコネクション、またはRDBを同時に
	 * トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public final void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * @return 取得したトランザクション制御処理を戻します。
	 */
	private final IBuildTaskService getBuildTaskService() {

		if ( null != this.nextTransactionService ) {
			Object obj = this.getContext().getBean( nextTransactionService );

			if ( obj instanceof IBuildTaskService ) {
				return (IBuildTaskService)obj;
			}
		}

		throw new TriSystemException( BmMessageId.BM004072F );
	}

	@Override
	public final void execute(IBldTimelineEntity request, IBuildTaskBean bean) throws Exception {
		this.getBuildTaskService().execute(request, bean);
	}

	@Override
	public final void execute(IBldTimelineEntity request, IBuildTaskBean bean, IBldTimelineAgentEntity iLineEntity) throws Exception {
		this.getBuildTaskService().execute(request, bean, iLineEntity);
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		this.getBuildTaskService().writeProcessByIrregular(timeLine, param, line, e);
	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {
		return this.getBuildTaskService().executeLinkCheck() ;
	}

	@Override
	public List<Object> executeLinkCheckByService( List<Object> paramList) throws Exception {
		return this.getBuildTaskService().executeLinkCheckByService( paramList ) ;
	}

}
