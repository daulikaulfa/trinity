package jp.co.blueship.tri.bm.task.concentrate.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;


public class TaskResultConcentrateEntity extends TaskTaskResultEntity implements ITaskResultConcentrateEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskDiffEntity[] diff = new ITaskDiffEntity[0];

	public ITaskDiffEntity newDiff() {
		return new TaskDiffEntity();
	}

	public ITaskDiffEntity[] getDiff() {
		return this.diff;
	}
	public void setDiff( ITaskDiffEntity[] diff ) {
		this.diff = diff;
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskDiffEntity extends TaskTaskResultEntity implements ITaskDiffEntity {

		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String applyNo = null;
		private ITaskResultEntity[] result = new ITaskResultEntity[0];

		public ITaskResultEntity newResult() {
			return new TaskResultEntity();
		}
		public String getApplyNo() {
			return applyNo;
		}
		public void setApplyNo( String value ) {
			applyNo = value;
		}
		public ITaskResultEntity[] getResult() {
			return this.result;
		}
		public void setResult( ITaskResultEntity[] result ) {
			this.result = result;
		}
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {

		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String relativePath = null;

		public String getRelativePath() {
			return relativePath;
		}
		public void setRelativePath( String value ) {
			relativePath = value;
		}
	}

}
