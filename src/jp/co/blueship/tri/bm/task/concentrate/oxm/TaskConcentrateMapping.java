package jp.co.blueship.tri.bm.task.concentrate.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskConcentrateExtEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateType.Param;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Concentrate;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Concentrateタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskConcentrateMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Concentrate[] tasks = src.getConcentrateArray();

		if( null != tasks ) {
			for( Concentrate task : tasks ) {
				TaskConcentrateEntity taskEntity = new TaskConcentrateEntity();
				copyProperties( task, taskEntity );

				Param[] paramArray = task.getParamArray();
				if( null != paramArray ) {
					List<TaskConcentrateEntity.ITaskParamEntity> paramList = new ArrayList<TaskConcentrateEntity.ITaskParamEntity>();
					for( Param param : paramArray ) {
						TaskConcentrateEntity.ITaskParamEntity taskParamEntity = taskEntity.new TaskParamEntity();
						copyProperties( param , taskParamEntity );
						paramList.add( taskParamEntity );
					}
					taskEntity.setParam( paramList.toArray( new TaskConcentrateEntity.ITaskParamEntity[ 0 ] ) );
				}

				entitys.add( taskEntity );
			}
		}
 		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		TaskResultConcentrateMapping resultMapping = new TaskResultConcentrateMapping();
		resultMapping.mapXMLBeans2DB( src.getResult() , dest );

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskConcentrateEntity) )
					continue;

				if( entity instanceof ITaskConcentrateExtEntity )
					continue;

				TaskConcentrateEntity taskEntity = (TaskConcentrateEntity)entity;

				Concentrate task = dest.addNewConcentrate();

				copyProperties( taskEntity, task );

				TaskConcentrateEntity.ITaskParamEntity[] paramEntityArray = taskEntity.getParam();
				if( null != paramEntityArray ) {
					for( TaskConcentrateEntity.ITaskParamEntity paramEntity : paramEntityArray ) {
						Param param = task.addNewParam();
						copyProperties( paramEntity , param );
					}
				}
			}
		}

		TaskResultConcentrateMapping resultMapping = new TaskResultConcentrateMapping();
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult();
		resultMapping.mapDB2XMLBeans( src , result );

		return dest;
	}
}
