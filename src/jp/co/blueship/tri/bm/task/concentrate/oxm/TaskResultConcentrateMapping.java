package jp.co.blueship.tri.bm.task.concentrate.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskResultConcentrateExtEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateResultType;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateResultType.Diff;
import jp.co.blueship.tri.fw.schema.beans.task.DiffResultDetailType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Concentrateタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultConcentrateMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			ConcentrateResultType[] results = src.getConcentrateArray();

			if( null != results ) {
				for( ConcentrateResultType result : results ) {
					TaskResultConcentrateEntity resultEntity = new TaskResultConcentrateEntity();

					copyProperties( result , resultEntity );

					Diff[] diffArray = result.getDiffArray();
					if( null != diffArray ) {
						ArrayList<ITaskResultConcentrateEntity.ITaskDiffEntity> diffList = new ArrayList<ITaskResultConcentrateEntity.ITaskDiffEntity>();

						for( Diff diff : diffArray ) {
							ITaskResultConcentrateEntity.ITaskDiffEntity diffEntity = resultEntity.newDiff();
							copyProperties( diff , diffEntity );

							DiffResultDetailType[] resultDetailArray = diff.getResultArray();
							if( null != resultDetailArray ) {

								ArrayList<ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultEntity>();

								for( DiffResultDetailType diffResult : resultDetailArray ) {
									ITaskResultEntity taskResultEntity =  diffEntity.newResult();
									copyProperties( diffResult , taskResultEntity );
									resultDetailList.add( taskResultEntity );
								}
								diffEntity.setResult( resultDetailList.toArray( new ITaskResultEntity[ 0 ] ) );
							}
							diffList.add( diffEntity );
						}
						resultEntity.setDiff( diffList.toArray( new ITaskResultConcentrateEntity.ITaskDiffEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultConcentrateEntity) )
					continue;
				if( entity instanceof ITaskResultConcentrateExtEntity )
					continue;

				TaskResultConcentrateEntity resultEntity = (TaskResultConcentrateEntity)entity;

				ConcentrateResultType result = dest.addNewConcentrate();

				copyProperties( resultEntity, result );

				ITaskDiffEntity[] taskDiffEntityArray = resultEntity.getDiff();
				if( null != taskDiffEntityArray ) {
					for( ITaskDiffEntity taskDiffEntity : taskDiffEntityArray ) {
						Diff diff = result.addNewDiff();
						copyProperties( taskDiffEntity , diff );

						ITaskResultEntity[] taskResultEntityArray = taskDiffEntity.getResult();
						if( null != taskResultEntityArray ) {
							for( ITaskResultEntity taskResultEntity : taskResultEntityArray ) {
								DiffResultDetailType diffResultDetailType = diff.addNewResult();
								copyProperties( taskResultEntity , diffResultDetailType );
							}
						}
					}
				}
			}
		}

		return dest;
	}

}
