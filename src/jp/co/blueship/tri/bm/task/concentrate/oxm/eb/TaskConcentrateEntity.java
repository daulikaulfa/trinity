package jp.co.blueship.tri.bm.task.concentrate.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskDeleteEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskDiffEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskConcentrateEntity extends TaskTaskEntity implements ITaskConcentrateEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String concentrateDir = null ;
	private ITaskParamEntity[] param = new ITaskParamEntity[0];
	private ITaskCopyEntity copy = null ;
	private ITaskDeleteEntity delete = null ;
	private ITaskDiffEntity diff = null ;

	public String getConcentrateDir() {
		return concentrateDir;
	}
	public void setConcentrateDir(String concentrateDir) {
		this.concentrateDir = concentrateDir;
	}

	public ITaskParamEntity[] getParam() {
		return param;
	}
	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String applyNo = null ;

		public String getApplyNo() {
			return applyNo;
		}
		public void setApplyNo(String applyNo) {
			this.applyNo = applyNo;
		}
	}
	public ITaskCopyEntity getCopyType() {
		return copy;
	}
	public void setCopyType(ITaskCopyEntity copy) {
		this.copy = copy;
	}
	public ITaskDeleteEntity getDeleteType() {
		return delete;
	}
	public void setDeleteType(ITaskDeleteEntity delete) {
		this.delete = delete;
	}
	public ITaskDiffEntity getDiffType() {
		return diff;
	}
	public void setDiffType(ITaskDiffEntity diff) {
		this.diff = diff;
	}

}
