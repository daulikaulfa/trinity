package jp.co.blueship.tri.bm.task.concentrate;

import static jp.co.blueship.tri.fw.cmn.utils.collections.FluentList.*;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTargetEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskConcentrateEntity.ITaskParamEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.copy.TaskProcCopy;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskFilesetEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskOverwriteEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskCopyEntity;
import jp.co.blueship.tri.bm.task.diff.TaskProcDiff;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.ITaskResultDiffEntity;
import jp.co.blueship.tri.bm.task.diff.oxm.eb.TaskDiffEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「concentrate」タスクの処理内容を記述します
 * <p>
 * ビルドパッケージを作成する際、返却／削除申請された資産ファイルを原本と比較し、 今回分の最新資産の集約を行う。
 *
 */
public class TaskProcConcentrate implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;
	private TaskProcCopy taskProcCopy = null;
	private TaskProcDiff taskProcDiff = null;

	private static final TriFunction<ITaskConcentrateEntity.ITaskParamEntity, String> TO_APPLY_NO = //
	new TriFunction<ITaskConcentrateEntity.ITaskParamEntity, String>() {

		@Override
		public String apply(ITaskParamEntity input) {
			return input.getApplyNo();
		}
	};

	public void setSupport(TaskFinderSupport support) {
		this.support = support;
	}

	public void setTaskProcCopy(TaskProcCopy taskProcCopy) {
		this.taskProcCopy = taskProcCopy;
	}

	public void setTaskProcDiff(TaskProcDiff taskProcDiff) {
		this.taskProcDiff = taskProcDiff;
	}

	public ITaskPropertyEntity[] getPropertyEntity() {
		return propertyEntity;
	}

	public void setPropertyEntity(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する <br>
	 * ","で区切られた複数パラメータに対応
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public final void substitutePath(ITaskTaskTypeEntity task) throws Exception {
		TaskConcentrateEntity concentrate = (TaskConcentrateEntity) task;
		concentrate.setConcentrateDir(TaskStringUtils.substitutionPath(propertyEntity, concentrate.getConcentrateDir()));

		ITaskParamEntity[] paramArray = concentrate.getParam();
		if (null != paramArray) {
			Set<ITaskParamEntity> arrset = new HashSet<ITaskParamEntity>();
			for (ITaskParamEntity param : paramArray) {
				String[] arrays = TaskStringUtils.substitutionPaths(propertyEntity, param.getApplyNo());
				if (null != arrays) {
					for (String array : arrays) {
						ITaskParamEntity entity = concentrate.new TaskParamEntity();
						TriPropertyUtils.copyProperties(entity, param);
						entity.setApplyNo(array);
						arrset.add(entity);
					}
				}
			}
			paramArray = arrset.toArray(new ITaskParamEntity[0]);
			concentrate.setParam(paramArray);
		}
	}

	/**
	 * 「concentrate」タスクの実際の処理を記述します <br>
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */


	public final ITaskResultTypeEntity execute(ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask) throws Exception {

		if (!(prmTask instanceof ITaskConcentrateEntity))
			return null;

		if (prmTask instanceof ITaskConcentrateExtEntity)
			return null;

		// 実行結果格納領域を確保
		ITaskResultConcentrateEntity resultConcentrateEntity = new TaskResultConcentrateEntity();
		List<ITaskResultConcentrateEntity.ITaskDiffEntity> diffList = new ArrayList<ITaskResultConcentrateEntity.ITaskDiffEntity>();

		resultConcentrateEntity.setCode(TaskResultCode.UN_PROCESS);
		resultConcentrateEntity.setSequence(prmTask.getSequence());
		resultConcentrateEntity.setTaskId(prmTask.getTaskId());

		Integer diffSequence = 1;

		try {
			substitutePath(prmTask);

			ITaskConcentrateEntity concentrate = (ITaskConcentrateEntity) prmTask;
			String[] applyNoArray = convertPathArray(concentrate.getParam());
			if (null == applyNoArray || 0 == applyNoArray.length) {
				throw new BusinessException( BmMessageId.BM004074F );
			}

			List<IAreqDto> applyEntityArray = this.getAssetApplyList(applyNoArray);

			String masterWorkPath = null;

			List<Object> paramList = new ArrayList<Object>();

			if (0 < applyEntityArray.size()) {
				IAreqEntity assetApplyEntity = applyEntityArray.get(0).getAreqEntity();
				IPjtEntity pjtEntity = support.getAmFinderSupport().findPjtEntity( assetApplyEntity.getPjtId() );

				ILotDto lotDto = support.getAmFinderSupport().findLotDto( pjtEntity.getLotId() );
				masterWorkPath = TriStringUtils.convertPath(BmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() ) );
				paramList.add(lotDto);

				String concentratePath = TriStringUtils.convertPath(concentrate.getConcentrateDir());
				this.innerTaskCopy(new TaskTargetEntity(), masterWorkPath, concentratePath);
			}

			boolean isExistAsset = false;

			for (IAreqDto applyEntity : applyEntityArray) {
				IAreqEntity assetApplyEntity = applyEntity.getAreqEntity();

				List<ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultEntity>();

				ITaskResultConcentrateEntity.ITaskDiffEntity diffEntity = resultConcentrateEntity.newDiff();
				diffEntity.setCode(TaskResultCode.UN_PROCESS);
				diffEntity.setSequence(String.valueOf(diffSequence++));
				diffEntity.setApplyNo(assetApplyEntity.getAreqId());

				try {
					isExistAsset |= TriStringUtils.isNotEmpty(applyEntity.getAreqFileEntities(AreqCtgCd.ReturningRequest));
					isExistAsset |= TriStringUtils.isNotEmpty(applyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest));

					this.diff(concentrate, applyEntity, diffEntity, resultDetailList, masterWorkPath, paramList);

					diffEntity.setCode(TaskResultCode.SUCCESS);

				} catch (Exception e) {
					diffEntity.setCode(TaskResultCode.ERROR_PROCESS);
					throw e;
				} finally {
					diffEntity.setResult(resultDetailList.toArray(new ITaskResultEntity[0]));
					diffList.add(diffEntity);
				}
			}

			if (true != isExistAsset) {// 返却資産なし
				throw new BusinessException( BmMessageId.BM004074F );
			}

			resultConcentrateEntity.setCode(TaskResultCode.SUCCESS);

		} catch (Exception e) {
			resultConcentrateEntity.setCode(TaskResultCode.ERROR_PROCESS);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005014S, e);

		} finally {
			resultConcentrateEntity.setDiff(diffList.toArray(new ITaskResultConcentrateEntity.ITaskDiffEntity[0]));
			prmTarget.addTaskResult(resultConcentrateEntity);
		}

		return resultConcentrateEntity;
	}

	/**
	 * 集約のための差分比較を行う。
	 *
	 * @param concentrate 集約タスク
	 * @param assetApplyEntity 対象申請情報
	 * @param diffEntity 処理結果
	 * @param resultDetailList 処理結果（詳細）リスト
	 * @param masterWorkPath 差分の比較先
	 * @param paramList Action用のPOJO
	 * @throws Exception
	 */
	private final void diff(ITaskConcentrateEntity concentrate, IAreqDto assetApplyEntity,
			ITaskResultConcentrateEntity.ITaskDiffEntity diffEntity, List<ITaskResultEntity> resultDetailList, String masterWorkPath,
			List<Object> paramList) throws Exception {

		Integer innerResultSequence = 1;

		// 返却資産
		if (BmBusinessJudgUtils.isReturnApply(assetApplyEntity.getAreqEntity().getAreqCtgCd())) {
			String returnAssetPath = BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath(paramList, assetApplyEntity.getAreqEntity()).getPath();
			String concentratePath = TriStringUtils.convertPath(concentrate.getConcentrateDir());

			ITaskTargetEntity prmTarget = new TaskTargetEntity();
			try {
				this.innerTaskDiff(prmTarget, returnAssetPath, masterWorkPath);

			} finally {
				if (!TriStringUtils.isEmpty(prmTarget.getTaskResult())) {
					ITaskResultTypeEntity[] resultArray = prmTarget.getTaskResult();
					ITaskResultDiffEntity resultDiffEntity = (ITaskResultDiffEntity) resultArray[0];

					for (ITaskResultDiffEntity.ITaskResultEntity diffResult : resultDiffEntity.getResult()) {
						ITaskResultEntity innerResult = diffEntity.newResult();
						innerResult.setSequence(String.valueOf(innerResultSequence++));
						innerResult.setCode(diffResult.getCode());
						innerResult.setRelativePath(diffResult.getRelativePath());
						resultDetailList.add(innerResult);
					}
				}
			}

			this.innerTaskCopy(new TaskTargetEntity(), returnAssetPath, concentratePath);
		}

		// 削除資産
		if (BmBusinessJudgUtils.isDeleteApply(assetApplyEntity.getAreqEntity().getAreqCtgCd())) {

			List<IAreqFileEntity> fileEntityArray = assetApplyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest);

			for (IAreqFileEntity fileEntity : fileEntityArray) {
				String path = fileEntity.getFilePath();

				ITaskResultEntity innerResult = diffEntity.newResult();
				innerResult.setSequence(String.valueOf(innerResultSequence++));
				innerResult.setCode(Status.DELETE.toString());
				innerResult.setRelativePath(path);

				try {

					File file = new File(TriStringUtils.linkPathBySlash(concentrate.getConcentrateDir(), path));
					TriFileUtils.delete(file);

				} catch (Exception e) {
					innerResult.setCode(Status.ERROR.toString());
					throw e;
				} finally {
					resultDetailList.add(innerResult);
				}
			}
		}
	}

	/**
	 * 申請番号のリストをもとに、IAreqEntityを検索する。 <br>
	 *
	 * @param applyNoArray 申請番号のリスト
	 * @return 該当レコードを返す
	 */
	private final List<IAreqDto> getAssetApplyList(String[] applyNoArray) throws Exception {

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
		AreqCondition condition = new AreqCondition();

		condition.setAreqIds(applyNoArray);

		// RUクローズされた申請は集約対象外とする。
		condition.setStsIdsNotEquals(new String[] { AmAreqStatusId.BuildPackageClosed.getStatusId() });

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.reqTimestamp, TriSortOrder.Asc, 1);

		List<IAreqEntity> areqEntities =
				this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 ).getEntities();

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}
		return areqDtoEntities;
	}

	/**
	 * ITaskParamEntityオブジェクトの配列からのapplyNo文字列のみを配列に格納して返す <br>
	 *
	 * @param ITaskParamEntityオブジェクト
	 * @return applyNo文字列の配列
	 */
	private final String[] convertPathArray(ITaskParamEntity[] paramEntityArray) throws Exception {

		if (TriStringUtils.isEmpty(paramEntityArray)) {
			return new String[0];
		}

		return from(paramEntityArray).//
				map(TO_APPLY_NO).//
				toArray(new String[0]);

	}

	/**
	 * 内部的にコピータスクを処理する。
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @param src コピー元
	 * @param dest コピー先
	 * @return 処理結果を戻す。
	 * @throws Exception
	 */
	private final ITaskResultTypeEntity innerTaskCopy(ITaskTargetEntity prmTarget, String src, String dest) throws Exception {
		TaskCopyEntity copy = new TaskCopyEntity();

		copy.setHoldTimestamp(StatusFlg.on.value());
		copy.setDoNeglect(StatusFlg.on.value());
		copy.setDoNotExistSkip(StatusFlg.on.value());

		ITaskFilesetEntity fileset = copy.new TaskFilesetEntity();
		fileset.setSrc(src);
		fileset.setDest(dest);
		copy.setFileset(new ITaskFilesetEntity[] { fileset });

		ITaskOverwriteEntity overwrite = copy.new TaskOverwriteEntity();
		overwrite.setDoAdd(StatusFlg.on.value());
		overwrite.setDoUpdateNew(StatusFlg.on.value());
		overwrite.setDoUpdateOld(StatusFlg.on.value());
		copy.setOverwrite(overwrite);

		return taskProcCopy.execute(prmTarget, copy);
	}

	/**
	 * 内部的に差分比較タスクを処理する。
	 *
	 * @param prmTarget タスク処理内容を保持したエンティティ
	 * @param srcPath 比較元
	 * @param diffPath 比較先
	 * @return 処理結果を戻す。
	 * @throws Exception
	 */
	private final ITaskResultTypeEntity innerTaskDiff(ITaskTargetEntity prmTarget, String srcPath, String diffPath) throws Exception {

		ITaskDiffEntity diff = new TaskDiffEntity();

		diff.setSrcPath(srcPath);
		diff.setDiffPath(diffPath);
		diff.setDoCopy(StatusFlg.off.value());
		diff.setDoReverse(StatusFlg.off.value());
		diff.setDoDetailSnap(StatusFlg.on.value());

		return taskProcDiff.execute(prmTarget, diff);
	}

}
