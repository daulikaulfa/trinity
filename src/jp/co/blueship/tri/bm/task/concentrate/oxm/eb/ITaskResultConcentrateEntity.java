package jp.co.blueship.tri.bm.task.concentrate.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskResultConcentrateEntity extends IEntity, ITaskResultTypeEntity {

	/**
	 * 返却資産と作業フォルダとの資産の比較結果を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskDiffEntity[] getDiff();
	/**
	 * 返却資産と作業フォルダとの資産の比較結果を設定します。
	 *
	 * @param diff 比較結果
	 */
	public void setDiff( ITaskDiffEntity[] diff );

	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskDiffEntity newDiff();

	/**
	 *
	 * 返却資産と作業フォルダとの資産の比較結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskDiffEntity extends IEntity, ITaskResultTypeEntity {

		/**
		 *
		 * 差分比較した申請番号を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getApplyNo();
		/**
		 * 差分比較した申請番号を設定します。
		 *
		 * @param value 申請番号
		 */
		public void setApplyNo( String value );

		/**
		 * 差分比較結果を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskResultEntity[] getResult();
		/**
		 * 差分比較結果を設定します。
		 *
		 * @param result 差分比較結果
		 */
		public void setResult( ITaskResultEntity[] result );

		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskResultEntity newResult();

	}

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends IEntity, ITaskResultTypeEntity {

		/**
		 * 差分比較した資産の相対パスを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getRelativePath() ;
		/**
		 * 差分比較した資産の相対パスを設定します。
		 *
		 * @param value 資産の相対パス
		 */
		public void setRelativePath( String value ) ;
	}

}
