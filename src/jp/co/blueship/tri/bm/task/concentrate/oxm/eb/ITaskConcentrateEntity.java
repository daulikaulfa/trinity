package jp.co.blueship.tri.bm.task.concentrate.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskConcentrateEntity extends IEntity, ITaskTaskTypeEntity {

	/**
	 * 今回分の資産を集約するディレクトリを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getConcentrateDir() ;
	/**
	 * 今回分の資産を集約するディレクトリを設定します。
	 *
	 * @param value 資産を集約するディレクトリ
	 */
	public void setConcentrateDir(String value) ;

	/**
	 * 資産を集約するためのパラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam() ;
	/**
	 * 資産を集約するためのパラメタ情報を設定します。
	 *
	 * @param param 資産を集約するためのパラメタ情報
	 */
	public void setParam( ITaskParamEntity[] param ) ;

	/**
	 *
	 * 資産を集約するためのパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 指定された申請番号
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getApplyNo() ;
		/**
		 * 指定された申請番号を取得します。
		 *
		 * @param value 申請番号
		 */
		public void setApplyNo( String value ) ;
	}
}
