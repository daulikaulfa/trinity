package jp.co.blueship.tri.bm.task.dotNetBuild;

import java.io.File;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskLogUtils;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.dotNetBuild.oxm.eb.ITaskDotNetBuildEntity;
import jp.co.blueship.tri.bm.task.dotNetBuild.oxm.eb.TaskDotNetBuildEntity;
import jp.co.blueship.tri.bm.task.dotNetBuild.oxm.eb.TaskResultDotNetBuildEntity;
import jp.co.blueship.tri.fw.agent.ShellExecute;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「dotNetBuild」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとに.netビルドタスクの実行を行う。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class TaskProcDotNetBuild implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 4L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskDotNetBuildEntity build = (TaskDotNetBuildEntity) task ;

		build.setExecuteShell( TaskStringUtils.substitutionPath( propertyEntity , build.getExecuteShell() ) ) ;
		build.setOutLogPath( TaskStringUtils.substitutionPath( propertyEntity , build.getOutLogPath() ) );
		build.setErrLogPath( TaskStringUtils.substitutionPath( propertyEntity , build.getErrLogPath() ) );

	}

	/**
	 * 「cBuild」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskDotNetBuildEntity) )
			return null ;

		TaskResultDotNetBuildEntity resultDotNetBuildEntity = new TaskResultDotNetBuildEntity() ;
		String taskResultCode = TaskResultCode.SUCCESS ;
		resultDotNetBuildEntity.setSequence( prmTask.getSequence() ) ;
		resultDotNetBuildEntity.setTaskId( prmTask.getTaskId() );

		String outLog = null ;
		String errLog = null ;
		ITaskDotNetBuildEntity build = (ITaskDotNetBuildEntity) prmTask;
		ShellExecute shellExecute = null;
		try {
			substitutePath( build ) ;

			//当初のMakefile生成型から、V1と同様の単純キックモデルに移行したため、パラメータは単純化
			//executeshellを仮にMakefileのパスとして使う
			shellExecute = new ShellExecute() ;
			shellExecute.setLog( TriLogFactory.getInstance() ) ;
			shellExecute.setExecShellPath( build.getExecuteShell() ) ;
			File file = new File( build.getExecuteShell() ) ;
			shellExecute.setWorkDir( file.getParent().toString() ) ;

			shellExecute.execute() ;
			if( 0 != shellExecute.getExitValue() ) {
				throw new TriSystemException( BmMessageId.BM005007S , String.valueOf(shellExecute.getExitValue()) ) ;
			}
		} catch ( Exception e ) {
			taskResultCode = TaskResultCode.ERROR_PROCESS ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005018S ,e) ;
		} finally {
			try {
				if (shellExecute != null) {
					outLog = shellExecute.getOutLog() ;
					errLog = shellExecute.getErrLog() ;
				}
				resultDotNetBuildEntity.setOutLog( TaskLogUtils.writeLog( build.getOutLogPath() , outLog ) );
				resultDotNetBuildEntity.setErrLog( TaskLogUtils.writeLog( build.getErrLogPath() , errLog ) );
			} catch ( Exception e ) {
				taskResultCode = TaskResultCode.ERROR_PROCESS ;
				throw new TriSystemException( BmMessageId.BM005018S ,e) ;
			} finally {
				resultDotNetBuildEntity.setCode( taskResultCode ) ;
				prmTarget.addTaskResult( resultDotNetBuildEntity ) ;
			}
		}

		return resultDotNetBuildEntity ;
	}

}
