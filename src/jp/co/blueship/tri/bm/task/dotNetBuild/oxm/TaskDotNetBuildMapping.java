package jp.co.blueship.tri.bm.task.dotNetBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.dotNetBuild.oxm.eb.ITaskDotNetBuildEntity;
import jp.co.blueship.tri.bm.task.dotNetBuild.oxm.eb.TaskDotNetBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.DotNetBuildType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * dotNetBuildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskDotNetBuildMapping extends TriXmlMappingUtils implements ITaskMapping {
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		DotNetBuildType[] tasks = src.getDotNetBuildArray();

		if( null != tasks ) {
			for( DotNetBuildType task : tasks ) {
				TaskDotNetBuildEntity taskEntity = new TaskDotNetBuildEntity();

				//copyProperties( task , taskEntity ) ;
				taskEntity.setSequence( task.getSequence() ) ;
				taskEntity.setName( task.getName() ) ;
				taskEntity.setExecuteShell( task.getExecuteShell() ) ;

				/*BuildPath buildPath = task.getBuildPath() ;
				if( null != buildPath ) {
					ITaskCBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.new TaskBuildPathEntity() ;

					copyProperties( buildPath , buildPathEntity ) ;

					if( null != buildPath.getSrcArray() ) {
						List<TaskCBuildEntity.TaskPathTypeEntity> srcList = new ArrayList<TaskCBuildEntity.TaskPathTypeEntity>() ;
						for( PathType srcPath : buildPath.getSrcArray() ) {
							TaskCBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath( srcPath.getPath() ) ;
							srcList.add( pathTypeEntity ) ;
						}
						buildPathEntity.setSrcParam( srcList.toArray( new TaskCBuildEntity.TaskPathTypeEntity[0] ) ) ;
					}
					taskEntity.setBuildPath( buildPathEntity ) ;
				}*/

				entitys.add( taskEntity ) ;
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

//		Result
		TaskResultDotNetBuildMapping resultMapping = new TaskResultDotNetBuildMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskDotNetBuildEntity) )
					continue;

				ITaskDotNetBuildEntity taskEntity = (ITaskDotNetBuildEntity)entity;
				DotNetBuildType task = dest.addNewDotNetBuild() ;

				//copyProperties( taskEntity, task );
				task.setSequence( taskEntity.getSequence() ) ;
				task.setName( taskEntity.getName() ) ;
				task.setExecuteShell( taskEntity.getExecuteShell() ) ;

				/*ITaskDotNetBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.getBuildPath() ;
				if( null != buildPathEntity ) {
					BuildPath buildPath = task.addNewBuildPath() ;

					copyProperties( buildPathEntity , buildPath ) ;

					ITaskCBuildEntity.ITaskPathTypeEntity[] srcEntityArray = buildPathEntity.getSrcParam() ;
					if( null != srcEntityArray ) {
						for( ITaskCBuildEntity.ITaskPathTypeEntity srcEntity : srcEntityArray ) {
							PathType pathType = buildPath.addNewSrc() ;

							copyProperties( srcEntity , pathType ) ;
						}
					}
				}*/
			}
		}

//		Result
		TaskResultDotNetBuildMapping resultMapping = new TaskResultDotNetBuildMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
