package jp.co.blueship.tri.bm.task;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * @version V4.00.00
 * @author Nguyen Van Chung
 */
public interface ICharsetInfoEntity extends IEntity{
	public String getIn() ;
	public void setIn(String value) ;

	public String getOut() ;
	public void setOut(String value) ;
}
