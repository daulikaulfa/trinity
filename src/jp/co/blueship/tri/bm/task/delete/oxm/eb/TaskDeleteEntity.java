package jp.co.blueship.tri.bm.task.delete.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;


public class TaskDeleteEntity extends TaskTaskEntity implements ITaskDeleteEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskParamEntity[] param = new ITaskParamEntity[0];
	private String doSimulate = StatusFlg.off.value();
	private String doNotExistSkip = StatusFlg.on.value();
	private String doDetailSnap = StatusFlg.off.value();

	public String getDoSimulate() {
		return doSimulate ;
	}
	public void setDoSimulate( String value ) {
		doSimulate = value ;
	}

	public String getDoNotExistSkip() {
		return doNotExistSkip;
	}

	public void setDoNotExistSkip(String doNotExistSkip) {
		this.doNotExistSkip = doNotExistSkip;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public ITaskParamEntity[] getParam() {
		return param ;
	}
	public void setParam( ITaskParamEntity[] value ) {
		param = value ;
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String src = null ;

		public String getSrc() {
			return src ;
		}
		public void setSrc( String value ) {
			src = value ;
		}
	}
}
