package jp.co.blueship.tri.bm.task.delete.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskDeleteEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.DeleteType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Deleteタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskDeleteMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		DeleteType[] tasks = src.getDeleteArray();

		if( null != tasks ) {
			for( DeleteType task : tasks ) {
				ITaskTaskTypeEntity deleteEntity = mapXMLBeans2DBSubType( src , task ) ;
				entitys.add( deleteEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultDeleteMapping resultMapping = new TaskResultDeleteMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}
	/**
	 *
	 * @param src
	 * @param delete
	 * @return
	 */
	public ITaskTaskTypeEntity mapXMLBeans2DBSubType(TargetType src, DeleteType delete ) {
		TaskDeleteEntity entity = new TaskDeleteEntity();

		copyProperties( delete, entity );

		DeleteType.Param[] params = delete.getParamArray();

		if( null != params && 0 < params.length ) {
			List<TaskDeleteEntity.TaskParamEntity> paramList = new ArrayList<TaskDeleteEntity.TaskParamEntity>();

			for( DeleteType.Param param : params ) {
				TaskDeleteEntity.TaskParamEntity paramEntity = entity.new TaskParamEntity();
				paramEntity.setSrc( param.getSrc() );
				paramList.add( paramEntity );
			}
			TaskDeleteEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskDeleteEntity.TaskParamEntity[ 0 ] );
			entity.setParam( paramArray );
		}
		return entity ;
	}


	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof TaskDeleteEntity) )
					continue;

				DeleteType delete = dest.addNewDelete();
				mapDB2XMLBeansSubType( src , entity , delete ) ;
			}
		}

		//Result
		TaskResultDeleteMapping resultMapping = new TaskResultDeleteMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のDBのEntity親階層
	 * @param entity 複写元のDBのEntity
	 * @param delete マッピング結果を格納するbeans
	 */
	public void mapDB2XMLBeansSubType(ITaskTargetEntity src, ITaskTaskTypeEntity entity , DeleteType delete ) {

		TaskDeleteEntity taskEntity = (TaskDeleteEntity)entity;
		copyProperties( taskEntity , delete );

		TaskDeleteEntity.ITaskParamEntity[] paramArray = taskEntity.getParam();

		if( null != paramArray ) {
			for( TaskDeleteEntity.ITaskParamEntity paramEntity : paramArray ) {
				DeleteType.Param param = delete.addNewParam();
				param.setSrc( paramEntity.getSrc() );
			}
		}
	}
}
