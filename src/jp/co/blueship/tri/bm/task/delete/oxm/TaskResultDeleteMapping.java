package jp.co.blueship.tri.bm.task.delete.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskResultDeleteEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.DeleteResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Deleteタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultDeleteMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();
			DeleteResultType[] results = src.getDeleteArray();

			if( null != results ) {
				for( DeleteResultType result : results ) {
					ITaskResultDeleteEntity resultEntity = new TaskResultDeleteEntity();

					copyProperties( result , resultEntity );

					jp.co.blueship.tri.fw.schema.beans.task.DeleteResultType.Result[] innerResultArray = result.getResultArray();
					if( null != innerResultArray ) {
						List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();
						for( jp.co.blueship.tri.fw.schema.beans.task.DeleteResultType.Result innerResult : innerResultArray ) {
							ITaskResultEntity innerResultEntity = resultEntity.newResult();
							copyProperties( innerResult , innerResultEntity );
							resultList.add( innerResultEntity );
						}
						resultEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultDeleteEntity) )
					continue;

				TaskResultDeleteEntity resultEntity = (TaskResultDeleteEntity)entity;

				DeleteResultType result = dest.addNewDelete();

				copyProperties( resultEntity, result );

				ITaskResultEntity[] resultEntityArray = resultEntity.getResult();
				if( null != resultEntityArray ) {
					for( ITaskResultEntity innerResultEntity : resultEntityArray ) {
						jp.co.blueship.tri.fw.schema.beans.task.DeleteResultType.Result innerResult = result.addNewResult();
						copyProperties( innerResultEntity , innerResult );
					}
				}
			}
		}

		return dest;
	}

}
