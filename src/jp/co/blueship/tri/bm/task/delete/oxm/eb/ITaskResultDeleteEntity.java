package jp.co.blueship.tri.bm.task.delete.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultDeleteEntity extends ITaskResultTypeEntity {

	/**
	 * 実行結果の配列を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 実行結果の配列を設定します。
	 *
	 * @param result 実行結果
	 */
	public void setResult( ITaskResultEntity[] result );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {

		/**
		 * 削除ディレクトリ／ファイルパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 削除ディレクトリ／ファイルパス
		 *
		 * @param value 作成ディレクトリパス
		 */
		public void setDest( String value );
	}
}
