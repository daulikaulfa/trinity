package jp.co.blueship.tri.bm.task.delete.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskDeleteEntity extends ITaskTaskTypeEntity {

	/**
	 * パラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam();
	/**
	 * パラメタ情報を設定します。
	 *
	 * @param value
	 */
	public void setParam( ITaskParamEntity[] value );
	/**
	 * 実際に削除を行わうかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoSimulate() ;
	/**
	 * 実際に削除を行わうかどうかを設定します。
	 *
	 * @param value 実際に削除を行わない場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、削除を行う。
	 */
	public void setDoSimulate( String value ) ;

	/**
	 * コピー元が存在しない場合、スキップするかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotExistSkip();
	/**
	 * コピー元が存在しない場合、スキップするかどうかを設定します。
	 *
	 * @param value コピー元が存在しなければ、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotExistSkip( String value );

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );

	/**
	 * パラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 削除対象のディレクトリパス／ファイル
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * 削除対象のディレクトリパス／ファイル
		 *
		 * @param value 削除対象のディレクトリパス／ファイル
		 */
		public void setSrc( String value );
	}

}
