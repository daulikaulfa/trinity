package jp.co.blueship.tri.bm.task.delete;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskDeleteEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskDeleteEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskResultDeleteEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「delete」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元にディレクトリ／ファイルの削除を行う。
 *
 */
public class TaskProcDelete implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskDeleteEntity delete = (TaskDeleteEntity) task ;

		ITaskDeleteEntity.ITaskParamEntity[] paramArray = delete.getParam() ;
		if( null != paramArray ) {
			List<ITaskDeleteEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskDeleteEntity.ITaskParamEntity>() ;
			for( ITaskDeleteEntity.ITaskParamEntity param : paramArray ) {
				String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , param.getSrc() ) ;
				if( null != arrays ) {
					for( String array : arrays ) {
						ITaskDeleteEntity.ITaskParamEntity entity = delete.new TaskParamEntity() ;
						TriPropertyUtils.copyProperties( entity, param );
						entity.setSrc( array ) ;
						arrlst.add( entity ) ;
					}
				}
				paramArray = arrlst.toArray( new ITaskDeleteEntity.ITaskParamEntity[ 0 ] ) ;
				delete.setParam( paramArray ) ;
			}
		}
	}

	/**
	 * 「delete」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskDeleteEntity) )
			return null ;

		TaskResultDeleteEntity resultDeleteEntity = new TaskResultDeleteEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultDeleteEntity.setSequence( prmTask.getSequence() );
		resultDeleteEntity.setCode( TaskResultCode.UN_PROCESS );
		resultDeleteEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskDeleteEntity delete = (ITaskDeleteEntity) prmTask;

			if( null != delete.getParam() ) {

				for( ITaskDeleteEntity.ITaskParamEntity param : delete.getParam() ) {
					//deleteごとに処理結果を記録する
					ITaskResultEntity innerResultEntity = resultDeleteEntity.newResult();
					innerResultEntity.setSequence( String.valueOf(resultSequence++) );
					innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
					innerResultEntity.setDest( param.getSrc() );

					try {

						File file = new File( param.getSrc() );

						if( StatusFlg.on.value().equals( delete.getDoSimulate() ) ) {
							//シミュレーションモード＝ONの場合、実削除を行わない
							continue;
						}

						if ( ! file.exists() ) {
							if ( ! StatusFlg.on.value().equals( delete.getDoNotExistSkip() ) ) {
								throw new TriSystemException(BmMessageId.BM004033F , param.getSrc());
							}

							continue;
						}

						TriFileUtils.delete( file );

						innerResultEntity.setCode( TaskResultCode.SUCCESS );

					} catch ( Exception e ) {
						innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
						throw e;
					} finally {
						if ( StatusFlg.on.value().equals( delete.getDoDetailSnap() ) ) {
							resultList.add( innerResultEntity );
						} else {
							if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
								resultList.add( innerResultEntity );
							}
						}
					}
				}
			}

			resultDeleteEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultDeleteEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005016S,e);
		} finally {
			resultDeleteEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultDeleteEntity );
		}

		return resultDeleteEntity;
	}
}
