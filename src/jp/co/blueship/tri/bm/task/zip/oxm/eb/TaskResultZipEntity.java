package jp.co.blueship.tri.bm.task.zip.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultZipEntity extends TaskTaskResultEntity implements ITaskResultZipEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];

	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String defineFilePath = null;
		private String defineDetailFilePath = null;
		private String dest = null;
		private ITaskZipFileEntity[] unzipFile = new ITaskZipFileEntity[0];


		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}
		public String getDefineFilePath() {
			return defineFilePath;
		}
		public void setDefineFilePath(String defineFilePath) {
			this.defineFilePath = defineFilePath;
		}
		public String getDest() {
			return dest;
		}
		public void setDest(String dest) {
			this.dest = dest;
		}

		public ITaskZipFileEntity newZipFile() {
			return new TaskZipFileEntity();
		}
		public ITaskZipFileEntity[] getZipFile() {
			return unzipFile;
		}
		public void setZipFile( ITaskZipFileEntity[] unzipFile ) {
			this.unzipFile = unzipFile;
		}
	}


	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskZipFileEntity extends TaskTaskResultEntity implements ITaskZipFileEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String path = null;


		public String getPath() {
			return path;
		}
		public void setPath( String path ) {
			this.path = path;
		}

	}

	public ITaskResultEntity[] getResult() {
		return result;
	}

	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}
}
