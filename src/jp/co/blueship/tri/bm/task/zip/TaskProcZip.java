package jp.co.blueship.tri.bm.task.zip;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskResultZipEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskResultZipEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskResultZipEntity.ITaskZipFileEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskZipEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.TaskResultZipEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.TaskZipEntity;
import jp.co.blueship.tri.fw.agent.core.TaskDefineFileUtils;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.FileIoZip;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「zip」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元にＺＩＰ圧縮を行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskProcZip implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * アーカイブ名をバイトシーケンスに復号化する文字コード。
	 */
	private static final String ENCODING = "ENCODING";
	/**
	 * アーカイブ名を示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String ARCHIVE_NAME = "ARCHIVE_NAME";
	/**
	 * アーカイブ出力先ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String OUTPUT_PATH = "OUTPUT_PATH";
	/**
	 * アーカイブ作成対象パッケージ トップ(ルート)ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String SRC_BASE_PATH = "SRC_BASE_PATH";
	/**
	 * アーカイブ作成判断パスを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String JUDGE_PATH = "JUDGE_PATH";

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskZipEntity zip = (TaskZipEntity) task;

		zip.setDest( TaskStringUtils.substitutionPath( propertyEntity , zip.getDest() ) );

		ITaskZipEntity.ITaskParamEntity[] paramArray = zip.getParam();
		if( null != paramArray ) {
			List<ITaskZipEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskZipEntity.ITaskParamEntity>();
			for( ITaskZipEntity.ITaskParamEntity param : paramArray ) {
				String[] arrays = TaskStringUtils.substitutionPaths( propertyEntity , param.getSrc() );
				if( null != arrays ) {
					for( String array : arrays ) {
						ITaskZipEntity.ITaskParamEntity entity = zip.new TaskParamEntity();
						TriPropertyUtils.copyProperties( entity, param );
						entity.setSrc( array );
						arrlst.add( entity );
					}
				}
			}
			paramArray = arrlst.toArray( new ITaskZipEntity.ITaskParamEntity[ 0 ] );
			zip.setParam( paramArray );
		}

		zip.setDefineFilePath(
				TaskStringUtils.substitutionPath( this.propertyEntity , zip.getDefineFilePath() ));
	}

	/**
	 * 「zip」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskZipEntity) )
			return null;

		ITaskResultZipEntity resultZipEntity = new TaskResultZipEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultZipEntity.setSequence( prmTask.getSequence() );
		resultZipEntity.setCode( TaskResultCode.UN_PROCESS );
		resultZipEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskZipEntity zip = (ITaskZipEntity) prmTask;

			List<ZipDefinition> zipDefList = new ArrayList<ZipDefinition>();

			// 定義ファイルによらないzip作成
			resultSequence = this.zip(zip, resultZipEntity, resultList, resultSequence);

			// 定義リストファイルの設定あり
			if ( null != zip.getDefineFilePath() ) {
				File defListFile = new File( zip.getDefineFilePath() );
				if ( !defListFile.exists() ) {
					throw new TriSystemException( BmMessageId.BM004011F , zip.getDefineFilePath() );
				}

				zipDefList.addAll( getReformDefFileList( defListFile ) );

				resultSequence = this.reform(zipDefList, zip, resultZipEntity, resultList, resultSequence);
			}

			resultZipEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultZipEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005031S , e );
		} finally {
			resultZipEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultZipEntity );
		}

		return resultZipEntity;
	}

	/**
	 * 定義ファイルによらない、zipを作成する。
	 * @param zip ＺＩＰ圧縮タスク
	 * @param resultZipEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 */
	private Integer zip(
							ITaskZipEntity zip,
							ITaskResultZipEntity resultZipEntity,
							List<ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		ITaskZipEntity.ITaskParamEntity[] paramArray = zip.getParam();
		if( null == paramArray || 0 == paramArray.length ) {
			return sequence;
		}

		ITaskResultEntity innerResultEntity = resultZipEntity.newResult();
		innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

		try {
			innerResultEntity.setSequence( String.valueOf(sequence++) );
			innerResultEntity.setDest( TriStringUtils.convertPath( zip.getDest() ) );

			Charset encoding = getCharset( zip.getEncoding() );
			boolean doNotExistSkip = ( StatusFlg.on.value().equals( zip.getDoNotExistSkip() ) ) ? true : false;

			List<String> pathList = new ArrayList<String>();

			for( ITaskZipEntity.ITaskParamEntity param : paramArray ) {
				File targetFile = new File( param.getSrc() );
				if ( ! doNotExistSkip && ! targetFile.exists() )
				throw new TriSystemException(BmMessageId.BM004040F , TriStringUtils.convertPath(targetFile.getPath()) );

				if ( targetFile.exists() )
					pathList.add( param.getSrc() );
			}

			Set<String> zipFileSet = new TreeSet<String>();

			FileIoZip.CompressZip( pathList , zip.getDest() , encoding , doNotExistSkip, zipFileSet );

			setZipFileResult( zip, innerResultEntity, zipFileSet );
			innerResultEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
			throw e;
		} finally {
			if ( StatusFlg.on.value().equals( zip.getDoDetailSnap() ) ) {
				resultList.add( innerResultEntity );
			} else {
				if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
					resultList.add( innerResultEntity );
				}
			}
		}

		return sequence;
	}

	/**
	 * 結果に圧縮したファイルツリーをセットする
	 * @param zip
	 * @param innerResultEntity
	 * @param zipFileSet
	 */
	private void setZipFileResult( ITaskZipEntity zip, ITaskResultEntity innerResultEntity, Set<String> zipFileSet ) {

		if ( !StatusFlg.on.value().equals( zip.getDoRecordFile() )) {
			innerResultEntity.setZipFile( new ITaskZipFileEntity[0] );
			return;
		}


		List<ITaskZipFileEntity> zipFileList = new ArrayList<ITaskZipFileEntity>();

		for ( String zipFile : zipFileSet ) {
			ITaskZipFileEntity zipFileEntity = innerResultEntity.newZipFile();
			zipFileEntity.setPath( zipFile );

			zipFileList.add( zipFileEntity );
		}

		innerResultEntity.setZipFile( zipFileList.toArray( new ITaskZipFileEntity[0] ));
		return;
	}

	/**
	 * 定義ファイルによる、zipを作成する。
	 * @param zipDefList zip定義情報のリスト
	 * @param zip ＺＩＰ圧縮タスク
	 * @param resultZipEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 */
	private Integer reform(
							List<ZipDefinition> zipDefList,
							ITaskZipEntity zip,
							ITaskResultZipEntity resultZipEntity,
							List<ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		for ( ZipDefinition zipDef : zipDefList ) {
			//filesetごとに処理結果を記録する
			ITaskResultZipEntity.ITaskResultEntity innerResultEntity = resultZipEntity.newResult();
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

			try {
				innerResultEntity.setSequence( String.valueOf(sequence++) );
				innerResultEntity.setDefineFilePath( zip.getDefineFilePath() );
				innerResultEntity.setDefineDetailFilePath( zipDef.getDefineDetailFilePath() );

				// 判断パスが未設定、または判断パスに構成するファイルがあれば実施
				if ( isExist( zipDef.getZipJudgePathList(), zipDef.getZipPackageList() )) {

					File srcBaseDir = null;
					if ( null != zipDef.getSrcBasePath() ) {
						srcBaseDir = new File( zipDef.getSrcBasePath() );
					}

					File zipFile = null;
					if ( null != zipDef.getOutputPath() ) {
						zipFile = new File( zipDef.getOutputPath(), zipDef.getZipName() );
					} else {
						zipFile = new File( zipDef.getZipName() );
					}
					innerResultEntity.setDest( TriStringUtils.convertPath(zipFile.getPath()) );

					boolean doNotExistSkip = ( StatusFlg.on.value().equals( zip.getDoNotExistSkip() ) ) ? true : false;
					List<String> pathList = new ArrayList<String>();

					for ( String packageList : zipDef.getZipPackageList() ) {
						File targetFile = new File( srcBaseDir, packageList );
						if ( ! doNotExistSkip && ! targetFile.exists() )
//							throw new SystemException(
//									"ファイルが存在しません： " + TriStringUtils.convertPath(targetFile.getPath()) );
							throw new TriSystemException(BmMessageId.BM004040F , TriStringUtils.convertPath(targetFile.getPath()) );

						if ( targetFile.exists() )
							pathList.add( TriStringUtils.linkPath(srcBaseDir.getPath(), packageList) );
					}

					Charset encoding = getCharset( zipDef.getEncoding() );

					FileIoZip.CompressZip( pathList , zipFile.getPath() , encoding , doNotExistSkip );

					// 結果の記録
					innerResultEntity.setCode( TaskResultCode.SUCCESS );
				}

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( zip.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		return sequence;
	}

	/**
	 * アーカイブ実行判断パスの配下にアーカイブを構成するパスが存在するかを確認する。
	 * @param zipJudgePathList アーカイブ実行判断パスのリスト
	 * @param srcPathList アーカイブを構成するパスのリスト
	 * @return 存在すればtrue、そうでなければfalse
	 */
	private boolean isExist( List<String> zipJudgePathList, List<String> srcPathList ) {

		// 判断パスが未設定
		if ( null == zipJudgePathList || 0 == zipJudgePathList.size() ) return true;

		for ( String zipJudgePath : zipJudgePathList ) {

			for ( String srcPath : srcPathList ) {

				File file = new File( zipJudgePath, srcPath );
				if ( file.exists() ) {
					return true;
				}
			}
		}

		return false;
	}

	/**
	 * アーカイブ定義リストファイルを読み込み、アーカイブ定義情報のリストを返す。
	 * @param defListFile アーカイブ定義リストファイル
	 * @return アーカイブ定義情報のリスト
	 */
	private List<ZipDefinition> getReformDefFileList( File defListFile ) throws Exception {

		List<File> defList = TaskDefineFileUtils.getDefFileList( defListFile );

		List<ZipDefinition> zipDefList = new ArrayList<ZipDefinition>();

		for ( File defFile : defList ) {
			zipDefList.add( getZipDefinition( defFile ));
		}

		return zipDefList;
	}

	/**
	 * アーカイブ定義ファイルを読み込み、アーカイブ定義情報を返す。
	 * @param defFile アーカイブ定義ファイル
	 * @return アーカイブ定義情報
	 */
	private ZipDefinition getZipDefinition( File defFile ) throws Exception {

		ZipDefinition jd	= new ZipDefinition();
		jd.setDefineDetailFilePath( TriStringUtils.convertPath(defFile.getPath()) );

		int lineNumber		= 0;

		try {
			Map<String,String> aliasDic = new HashMap<String,String>();

			String[] lineArray = TriFileUtils.readFileLine( defFile , false ) ;
			for( String line : lineArray ) {
				lineNumber = lineNumber + 1;

				// コメント、空白行はスキップ
				if ( !TaskDefineFileUtils.isContents( line ))		continue;
				// 変数の定義行は、変数登録をしてスキップ
				if ( TaskDefineFileUtils.isAlias( line, aliasDic ))	continue;

				// 変数を置換
				line = TaskDefineFileUtils.replaceAliasVariable( line, aliasDic );

				// エンコーディングの定義行でがあれば、設定してスキップ
				if ( setEncoding( line, jd ))		continue;
				// アーカイブ名の定義行でがあれば、設定してスキップ
				if ( setZipName( line, jd ))		continue;
				// アーカイブ出力先の定義行でがあれば、設定してスキップ
				if ( setOutputPath( line, jd ))		continue;
				// アーカイブ作成対象パッケージ トップ(ルート)ディレクトリの定義行でがあれば、設定してスキップ
				if ( setSrcBasePath( line, jd ))	continue;
				// アーカイブ実行判断ディレクトリの定義行でがあれば、設定してスキップ
				if ( setZipJudgePath( line, jd ))	continue;

				// 上記以外だったら、アーカイブのパッケージ定義行と判断して設定
				setZipPackageValue( line, jd );
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM004013F , e , defFile.getAbsolutePath() , String.valueOf(lineNumber) );
		}
		// アーカイブ名がなければエラー
		if ( null == jd.getZipName() ) {
			throw new TriSystemException(BmMessageId.BM004041F , defFile.getAbsolutePath() );
		}

		return jd;
	}

	/**
     * エンコーディングの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setEncoding( String line, ZipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, ENCODING );
    	if ( null == value ) {
    		return false;
    	} else {
    		jd.setEncoding( value );
    		return  true;
    	}
    }

	/**
     * アーカイブ名の定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setZipName( String line, ZipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, ARCHIVE_NAME );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setZipName( newValue );
    		return  true;
    	}
    }

	/**
     * アーカイブ出力先ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setOutputPath( String line, ZipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, OUTPUT_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setOutputPath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * アーカイブ作成対象パッケージ トップ(ルート)ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setSrcBasePath( String line, ZipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, SRC_BASE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setSrcBasePath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * アーカイブ作成判断パスの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setZipJudgePath( String line, ZipDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, JUDGE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.getZipJudgePathList().add( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

    /**
     * アーカイブのパッケージ情報の値を取得する
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setZipPackageValue( String line, ZipDefinition jd ) {

		String formatedPath = TriStringUtils.convertPath( line ).trim();
		jd.getZipPackageList().add( formatedPath );

		return true;
    }

	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする<br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004039F , e , encoding );
		}
		return charset;
	}

 	/**
	 * ＺＩＰ圧縮定義ファイルの情報を保持するクラス
	 */
	public class ZipDefinition {

		/** エンコーディング */
		private String encoding = null;
		/** リフォーム定義ファイルパス */
		private String defineDetailFilePath = null;
		/** アーカイブ名 */
		private String zipName = null;
		/** アーカイブ出力先ディレクトリ */
		private String outputPath = null;
		/** アーカイブ作成対象パッケージ トップ(ルート)ディレクトリ */
		private String srcBasePath = null;
		/** アーカイブ作成判断パス */
		private List<String> zipJudgePathList = new ArrayList<String>();
		/** アーカイブを構成するパッケージ(ファイル)情報 */
		private List<String> zipPackageList = new ArrayList<String>();

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getZipName() {
			return zipName;
		}
		public void setZipName( String zipName ) {
			this.zipName = zipName;
		}

		public String getOutputPath() {
			return outputPath;
		}
		public void setOutputPath( String outputPath ) {
			this.outputPath = outputPath;
		}

		public String getSrcBasePath() {
			return srcBasePath;
		}
		public void setSrcBasePath( String srcBasePath ) {
			this.srcBasePath = srcBasePath;
		}

		public List<String> getZipJudgePathList() {
			return zipJudgePathList;
		}
		public void setZipJudgePathList( List<String> zipJudgePathList ) {
			this.zipJudgePathList = zipJudgePathList;
		}

		public List<String> getZipPackageList() {
			return zipPackageList;
		}
		public void setZipPackageList( List<String> zipPackageList ) {
			this.zipPackageList = zipPackageList;
		}
		public String getEncoding() {
			return encoding;
		}
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}
	}

}
