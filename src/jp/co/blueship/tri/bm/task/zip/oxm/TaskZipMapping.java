package jp.co.blueship.tri.bm.task.zip.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.TaskZipEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Zip;

/**
 * Zipタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskZipMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Zip[] tasks = src.getZipArray();

		if( null != tasks ) {
			for( Zip task : tasks ) {
				TaskZipEntity taskEntity = new TaskZipEntity();

				copyProperties( task, taskEntity );

				//Param
				Zip.Param[] params = task.getParamArray();

				if( null != params && 0 < params.length ) {
					List<TaskZipEntity.TaskParamEntity> paramList = new ArrayList<TaskZipEntity.TaskParamEntity>();

					for( Zip.Param param : params ) {
						TaskZipEntity.TaskParamEntity paramEntity = taskEntity.new TaskParamEntity();

						copyProperties( param , paramEntity ) ;

						paramList.add( paramEntity );
					}
					TaskZipEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskZipEntity.TaskParamEntity[ 0 ] );
					taskEntity.setParam( paramArray );
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultZipMapping resultMapping = new TaskResultZipMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof TaskZipEntity) )
					continue;

				TaskZipEntity zipEntity = (TaskZipEntity)entity;

				Zip task = dest.addNewZip();

				copyProperties( zipEntity, task );

				//Param
				TaskZipEntity.ITaskParamEntity[] paramArray = zipEntity.getParam();

				if( null != paramArray ) {
					for( TaskZipEntity.ITaskParamEntity paramEntity : paramArray ) {
						Zip.Param param = task.addNewParam();

						copyProperties( paramEntity , param ) ;
					}
				}
			}
		}

//		Result
		TaskResultZipMapping resultMapping = new TaskResultZipMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
