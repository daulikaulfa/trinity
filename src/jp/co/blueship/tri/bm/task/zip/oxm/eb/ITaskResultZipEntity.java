package jp.co.blueship.tri.bm.task.zip.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultZipEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult( ITaskResultEntity[] values );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineFilePath();
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @param value 外部定義ファイル（定義リスト）
		 */
		public void setDefineFilePath(String value);
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineDetailFilePath();
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @param value 外部定義ファイル（詳細定義リスト）
		 */
		public void setDefineDetailFilePath(String value);
		/**
		 * 出力先のＺＩＰファイルパス
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 出力先のＺＩＰファイルパス
		 *
		 * @param value 出力先のＺＩＰファイルパス
		 */
		public void setDest( String value );

		/**
		 * 圧縮したファイルツリーを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskZipFileEntity[] getZipFile();
		/**
		 * 圧縮したファイルツリーを設定します。
		 *
		 * @param value 圧縮したファイルツリー
		 */
		public void setZipFile( ITaskZipFileEntity[] value );
		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskZipFileEntity newZipFile();
	}

	/**
	 *
	 * 圧縮したファイルの結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskZipFileEntity extends ITaskResultTypeEntity {
		public String getPath();
		public void setPath( String value );
	}
}
