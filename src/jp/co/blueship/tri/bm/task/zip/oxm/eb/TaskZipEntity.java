package jp.co.blueship.tri.bm.task.zip.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskZipEntity extends TaskTaskEntity implements ITaskZipEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskParamEntity[] param = new ITaskParamEntity[0];
	private String dest = null ;
	private String encoding = null ;
	private String defineFilePath = null;
	private String doNotExistSkip = StatusFlg.on.value();
	private String doDetailSnap = StatusFlg.off.value();
	private String doRecordFile = StatusFlg.off.value();


	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String src = null ;

		public String getSrc() {
			return src ;
		}
		public void setSrc( String value ) {
			src = value ;
		}
	}

	public String getDefineFilePath() {
		return defineFilePath;
	}

	public void setDefineFilePath(String defineFilePath) {
		this.defineFilePath = defineFilePath;
	}

	public String getDest() {
		return dest;
	}

	public void setDest(String dest) {
		this.dest = dest;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public String getDoNotExistSkip() {
		return doNotExistSkip;
	}

	public void setDoNotExistSkip(String doNotExistSkip) {
		this.doNotExistSkip = doNotExistSkip;
	}

	public String getEncoding() {
		return encoding;
	}

	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}

	public ITaskParamEntity[] getParam() {
		return param;
	}

	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}

	public String getDoRecordFile() {
		return doRecordFile;
	}

	public void setDoRecordFile( String doRecordFile ) {
		this.doRecordFile = doRecordFile;
	}
}
