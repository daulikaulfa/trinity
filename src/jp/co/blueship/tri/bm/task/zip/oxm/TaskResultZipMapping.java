package jp.co.blueship.tri.bm.task.zip.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskResultZipEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.ITaskResultZipEntity.ITaskZipFileEntity;
import jp.co.blueship.tri.bm.task.zip.oxm.eb.TaskResultZipEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;
import jp.co.blueship.tri.fw.schema.beans.task.ZipResultType;

/**
 * Zipタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultZipMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			ZipResultType[] results = src.getZipArray();

			if( null != results ) {
				for( ZipResultType result : results ) {
					ITaskResultZipEntity resultEntity = new TaskResultZipEntity() ;

					copyProperties( result , resultEntity ) ;

					//Result
					ZipResultType.Result[] resultArray = result.getResultArray();

					if( null != resultArray && 0 < resultArray.length ) {
						List<ITaskResultZipEntity.ITaskResultEntity> innerResultList = new ArrayList<ITaskResultZipEntity.ITaskResultEntity>();

						for( ZipResultType.Result innerResult : resultArray ) {
							ITaskResultZipEntity.ITaskResultEntity innerResultEntity = resultEntity.newResult();

							copyProperties( innerResult , innerResultEntity ) ;

							innerResultList.add( innerResultEntity );


							// ZipFile
							if( null != innerResult.getZipFileArray() ) {
								List<ITaskZipFileEntity> zipFileList = new ArrayList<ITaskZipFileEntity>() ;

								for( jp.co.blueship.tri.fw.schema.beans.task.ZipResultType.Result.ZipFile innerZipFile : innerResult.getZipFileArray() ) {
									ITaskZipFileEntity innerZipFileEntity = innerResultEntity.newZipFile();
									copyProperties( innerZipFile, innerZipFileEntity );

									zipFileList.add( innerZipFileEntity );
								}

								innerResultEntity.setZipFile( zipFileList.toArray( new ITaskZipFileEntity[0] ) );
							}
						}
						resultEntity.setResult( innerResultList.toArray( new ITaskResultZipEntity.ITaskResultEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultZipEntity) )
					continue;

				ITaskResultZipEntity resultEntity = (ITaskResultZipEntity)entity;

				ZipResultType result = dest.addNewZip();

				copyProperties( resultEntity, result );

				//Result
				ITaskResultZipEntity.ITaskResultEntity[] innerResultArray = resultEntity.getResult();

				if( null != innerResultArray ) {
					for( ITaskResultZipEntity.ITaskResultEntity innerResultEntity : innerResultArray ) {
						ZipResultType.Result innerResult = result.addNewResult();

						copyProperties( innerResultEntity , innerResult ) ;


						// UnzipFile
						ITaskZipFileEntity[] zipFileArray = innerResultEntity.getZipFile();
						if ( null != zipFileArray ) {
							for ( ITaskZipFileEntity innerZipFileEntity : zipFileArray ) {
								jp.co.blueship.tri.fw.schema.beans.task.ZipResultType.Result.ZipFile innerZipFile = innerResult.addNewZipFile();
								copyProperties( innerZipFileEntity, innerZipFile );
							}
						}
					}
				}
			}
		}

		return dest;
	}

}
