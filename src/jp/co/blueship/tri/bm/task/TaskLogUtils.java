package jp.co.blueship.tri.bm.task;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByTaskLogs;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;

/**
 * タスクの実行ログの出力を処理する。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class TaskLogUtils {

	private static final String notOutputCode = "0";

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * パスが設定されたときにLogへ書き出す。
	 * RmDesignSheetのタスクログの出力設定に応じてログを返します。
	 *
	 * @param logPath
	 * @param log
	 * @return log DBログ出力設定が有効ならlogを返す。無効なら空文字列。
	 * @throws Exception
	 */
	public static String writeLog( String logPath , String log ) throws Exception {
		return writeLog(logPath, log, null);
	}

	/**
	 * Write a log if path has been set.<br>
	 * Return log by using RmDesignSheet setting.<br>
	 * <br>
	 * パスが設定されたときにLogへ書き出す。<br>
	 * RmDesignSheetのタスクログの出力設定に応じてログを返します。<br>
	 *
	 * @param logPath
	 * @param log
	 * @param charset
	 * @return log Return log if the log setting of DB is enabled. Return empty if disenable.
	 * @throws Exception
	 */
	public static String writeLog( String logPath , String log , Charset charset ) throws Exception {

		String retLog = "";

		if ( TriStringUtils.isNotEmpty( logPath ) ){
			TriFileUtils.saveToFileStr( logPath, ( null == log ) ? "" : log , charset );
		}
		String saveValue = sheet.getValue( RmDesignEntryKeyByTaskLogs.recordingInShellCommandResult );
		if( !notOutputCode.equals( saveValue.trim() ) ) {
			retLog = ( null == log ) ? "" : log;
		}
		return retLog;
	}

}
