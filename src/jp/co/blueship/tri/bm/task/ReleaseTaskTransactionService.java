package jp.co.blueship.tri.bm.task;

import java.util.List;

import jp.co.blueship.tri.agent.rm.svc.flow.IReleaseTaskService;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;


/**
 * 業務シーケンス（リリース）でトランザクション制御を利用するための汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ReleaseTaskTransactionService implements IReleaseTaskService {

	private IContextAdapter context = null;
	private String nextTransactionService = null;

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	private final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return context;
	}

	/**
	 * 一度のリクエスト処理で、トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public final void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * @return 取得したトランザクション制御処理を戻します。
	 */
	private final IReleaseTaskService getReleaseTaskService() {

		if ( null != this.nextTransactionService ) {
			Object obj = this.getContext().getBean( nextTransactionService );

			if ( obj instanceof IReleaseTaskService ) {
				return (IReleaseTaskService)obj;
			}
		}
		throw new TriSystemException( BmMessageId.BM004071F );
	}

	@Override
	public final void execute(IBldTimelineEntity request, IReleaseTaskBean bean) throws Exception {
		this.getReleaseTaskService().execute(request, bean);
	}

	@Override
	public final void execute(IBldTimelineEntity request, IReleaseTaskBean bean, IBldTimelineAgentEntity iLineEntity) throws Exception {
		this.getReleaseTaskService().execute(request, bean, iLineEntity);
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IReleaseTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		this.getReleaseTaskService().writeProcessByIrregular(timeLine, param, line, e);
	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {
		return this.getReleaseTaskService().executeLinkCheck() ;
	}

	@Override
	public List<Object> executeLinkCheckByService( List<Object> paramList ) throws Exception {
		return this.getReleaseTaskService().executeLinkCheckByService( paramList ) ;
	}
}
