package jp.co.blueship.tri.bm.task.copy.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskCopyEntity extends ITaskTaskTypeEntity {

	/**
	 * ファイルの「追加」「更新」を行うかどうかを判定するパラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskOverwriteEntity getOverwrite();
	/**
	 * ファイルの「追加」「更新」を行うかどうかを設定します。
	 *
	 * @param value パラメタ情報
	 */
	public void setOverwrite( ITaskOverwriteEntity value );

	/**
	 * 文字コード／改行の変換コピーを行うパラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskCharsetinfoEntity[] getCharsetinfo();
	/**
	 * 文字コード／改行の変換コピーを行うパラメタ情報を設定します。
	 *
	 * @param charsetInfo パラメタ情報
	 */
	public void setCharsetinfo(ITaskCharsetinfoEntity[] charsetInfo);

	/**
	 * コピー元、コピー先のパラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskFilesetEntity[] getFileset();
	/**
	 * コピー元、コピー先のパラメタ情報を設定します。
	 *
	 * @param fileset パラメタ情報
	 */
	public void setFileset(ITaskFilesetEntity[] fileset);

	/**
	 * コピー（リフォーム）定義ファイルパスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDefineFilePath();
	/**
	 * コピー（リフォーム）定義ファイルパスを設定します。
	 *
	 * @param value 定義ファイルパス
	 */
	public void setDefineFilePath( String value );

	/**
	 * コピー元のタイプムスタンプを保持するどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getHoldTimestamp();
	/**
	 * コピー元のタイムスタンプを保持するかどうかを設定します。
	 *
	 * @param value タイムスタンプを保持してコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setHoldTimestamp( String value );

	/**
	 * 資産と関係ないSCM関連ファイルを含めるかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNeglect();
	/**
	 * 資産と関係ないSCM関連ファイルを含めるかどうかを設定します。
	 *
	 * @param value SCM関連ファイルを含めずにコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNeglect( String value );

	/**
	 * コピー元が存在しない場合、スキップするかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotExistSkip();
	/**
	 * コピー元が存在しない場合、スキップするかどうかを設定します。
	 *
	 * @param value コピー元が存在しなければ、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotExistSkip( String value );

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );

	/**
	 * コピーしたファイルツリーを記録するかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoRecordFile();
	/**
	 * コピーしたファイルツリーを記録するかどうかを設定します。
	 *
	 * @param value コピーしたファイルツリーを記録する場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は記録しない。
	 */
	public void setDoRecordFile( String value );


	/**
	 * ファイルの「追加」「更新」を行うかどうかを判定するパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskOverwriteEntity extends IEntity {

		/**
		 * コピー先にファイルを追加するかどうか
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDoAdd();
		/**
		 * コピー先にファイルを追加するかどうか
		 *
		 * @param doAdd コピー先に存在しないファイルをコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
		 * それ以外は常にコピーする。
		 */
		public void setDoAdd(String doAdd);
		/**
		 * コピー元のファイルタイムスタンプが、新しければ上書き
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDoUpdateNew();
		/**
		 * コピー元のファイルタイムスタンプが、新しければ上書き
		 *
		 * @param doNew コピー元のファイルのタイムスタンプが、コピー先のファイルより更新日時が新しいものを常に上書きする場合、
		 * {@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
		 */
		public void setDoUpdateNew(String doNew);
		/**
		 * コピー元のファイルスタンプが、古くても上書き
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDoUpdateOld();
		/**
		 * コピー元のファイルスタンプが、古くても上書き
		 *
		 * @param doOld コピー元のファイルのタイムスタンプが、コピー先のファイルより更新日時が古くても常に上書きする場合、
		 * {@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
		 */
		public void setDoUpdateOld(String doOld);
	}
	/**
	 *
	 * 文字コード／改行の変換コピーを行うパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskCharsetinfoEntity extends IEntity {

		/**
		 * 変換対象の拡張子
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getExtensions();
		/**
		 * 変換対象の拡張子
		 *
		 * @param extensions
		 */
		public void setExtensions(String extensions);
		/**
		 * コピー元ファイルの文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getIn();
		/**
		 * コピー元ファイルの文字コード
		 *
		 * @param in {@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}の文字列。
		 */
		public void setIn(String in);
		/**
		 * コピー先ファイルの文字コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getOut();
		/**
		 * コピー先ファイルの文字コード
		 *
		 * @param out {@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}の文字列。
		 */
		public void setOut(String out);
		/**
		 * コピー先ファイルの改行コード
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getOutls();
		/**
		 * コピー先ファイルの改行コード
		 *
		 * @param outls {@link jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode}の文字列。
		 */
		public void setOutls(String outls);
	}

	/**
	 *
	 * コピー元、コピー先のパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskFilesetEntity extends IEntity {
		/**
		 * コピー元のディレクトリ／ファイル。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * コピー元のディレクトリ／ファイル。
		 *
		 * @param value コピー元のディレクトリ／ファイルパス。
		 */
		public void setSrc( String value );
		/**
		 * コピー先のディレクトリ／ファイル。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * コピー先のディレクトリ／ファイル。
		 *
		 * @param value コピー先のディレクトリ／ファイルパス
		 */
		public void setDest(String value);
	}
}
