package jp.co.blueship.tri.bm.task.copy.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskCopyFileEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskDefineDetailFileEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskResultCopyEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CopyResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Copyタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultCopyMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			if( null != src ) {
				CopyResultType[] results = src.getCopyArray();

				if( null != results ) {
					for( CopyResultType result : results ) {
						ITaskResultCopyEntity resultEntity = new TaskResultCopyEntity() ;

						copyProperties( result , resultEntity ) ;

						//Result
						if( null != result.getResultArray() ) {
							List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>() ;

							for( jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result innerResult : result.getResultArray() ) {
								ITaskResultEntity innerResultEntity = resultEntity.newResult();
								copyProperties( innerResult, innerResultEntity );

								resultList.add( innerResultEntity );

								//DefineDetailFile
								if( null != innerResult.getDefineDetailFileArray() ) {
									List<ITaskDefineDetailFileEntity> defineDetailFileList = new ArrayList<ITaskDefineDetailFileEntity>() ;

									for( jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result.DefineDetailFile innerDefineDetailFile : innerResult.getDefineDetailFileArray() ) {
										ITaskDefineDetailFileEntity innerDefineDetailFileEntity = innerResultEntity.newDefineDetailFile();
										copyProperties( innerDefineDetailFile, innerDefineDetailFileEntity );

										defineDetailFileList.add( innerDefineDetailFileEntity );
									}

									innerResultEntity.setDefineDetailFile( defineDetailFileList.toArray( new ITaskDefineDetailFileEntity[0] ) );
								}


								// CopyFile
								if( null != innerResult.getCopyFileArray() ) {
									List<ITaskCopyFileEntity> copyFileList = new ArrayList<ITaskCopyFileEntity>() ;

									for( jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result.CopyFile innerCopyFile : innerResult.getCopyFileArray() ) {
										ITaskCopyFileEntity innerCopyFileEntity = innerResultEntity.newCopyFile();
										copyProperties( innerCopyFile, innerCopyFileEntity );

										copyFileList.add( innerCopyFileEntity );
									}

									innerResultEntity.setCopyFile( copyFileList.toArray( new ITaskCopyFileEntity[0] ) );
								}
							}

							resultEntity.setResult( resultList.toArray( new ITaskResultEntity[0] ) );
						}

						entitys.add( resultEntity );
					}
				}
				dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
			}
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultCopyEntity) )
					continue;

				TaskResultCopyEntity resultEntity = (TaskResultCopyEntity)entity;

				CopyResultType result = dest.addNewCopy();

				copyProperties( resultEntity, result );

				//Result
				ITaskResultEntity[] resultArray = resultEntity.getResult();
				if( null != resultArray ) {
					for( ITaskResultEntity innerResultEntity : resultArray ) {
						jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result innerResult = result.addNewResult();
						copyProperties( innerResultEntity, innerResult );

						//DefineDetailFile
						ITaskDefineDetailFileEntity[] defineDetailFileArray = innerResultEntity.getDefineDetailFile();
						if ( null != defineDetailFileArray ) {
							for ( ITaskDefineDetailFileEntity innerDefineDetailFileEntity : defineDetailFileArray ) {
								jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result.DefineDetailFile innerDefineDetailFile = innerResult.addNewDefineDetailFile();
								copyProperties( innerDefineDetailFileEntity, innerDefineDetailFile );
							}
						}


						// CopyFile
						ITaskCopyFileEntity[] copyFileArray = innerResultEntity.getCopyFile();
						if ( null != copyFileArray ) {
							for ( ITaskCopyFileEntity innerCopyFileEntity : copyFileArray ) {
								jp.co.blueship.tri.fw.schema.beans.task.CopyResultType.Result.CopyFile innerCopyFile = innerResult.addNewCopyFile();
								copyProperties( innerCopyFileEntity, innerCopyFile );
							}
						}
					}
				}
			}
		}

		return dest;
	}

}
