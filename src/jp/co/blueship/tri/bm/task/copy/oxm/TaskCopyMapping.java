package jp.co.blueship.tri.bm.task.copy.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskCopyEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.CopyType;
import jp.co.blueship.tri.fw.schema.beans.task.CopyType.Fileset;
import jp.co.blueship.tri.fw.schema.beans.task.OverwriteType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Copy;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Copyタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskCopyMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Copy[] tasks = src.getCopyArray();
		if( null != tasks ) {
			for( Copy task : tasks ) {
				entitys.add( mapXMLBeans2DBSubType( src , task ) ) ;
			}
		}

 		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

//		Result
		TaskResultCopyMapping resultMapping = new TaskResultCopyMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}
	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のXMLBeans親階層
	 * @param task 複写元のXMLBeans
	 * @return マッピング済みITaskTaskTypeEntity
	 */
	public ITaskTaskTypeEntity mapXMLBeans2DBSubType( TargetType src , CopyType task ) {

		TaskCopyEntity taskEntity = new TaskCopyEntity();
		copyProperties( task, taskEntity );

		//Overwrite
		if ( null != task.getOverwriteInfo() ) {
			OverwriteType overwrite = task.getOverwriteInfo();

			TaskCopyEntity.ITaskOverwriteEntity overwriteEntity = taskEntity.new TaskOverwriteEntity() ;
			copyProperties( overwrite, overwriteEntity );
			taskEntity.setOverwrite( overwriteEntity );
		}

		//Charsetinfo
		if( null != task.getCharsetinfoArray() ) {
			List<TaskCopyEntity.ITaskCharsetinfoEntity> charsetinfoList = new ArrayList<TaskCopyEntity.ITaskCharsetinfoEntity>() ;

			for( CharsetinfoType charsetinfo : task.getCharsetinfoArray() ) {
				TaskCopyEntity.TaskCharsetinfoEntity charsetinfoEntity = taskEntity.new TaskCharsetinfoEntity();
				copyProperties( charsetinfo, charsetinfoEntity );

				charsetinfoList.add( charsetinfoEntity );
			}

			taskEntity.setCharsetinfo( charsetinfoList.toArray( new TaskCopyEntity.ITaskCharsetinfoEntity[0] ) );
		}

		//Fileset
		if( null != task.getFilesetArray() ) {
			List<TaskCopyEntity.ITaskFilesetEntity> filesetList = new ArrayList<TaskCopyEntity.ITaskFilesetEntity>() ;

			for( Fileset fileset : task.getFilesetArray() ) {
				TaskCopyEntity.TaskFilesetEntity filesetEntity = taskEntity.new TaskFilesetEntity();
				copyProperties( fileset, filesetEntity );

				filesetList.add( filesetEntity );
			}

			taskEntity.setFileset( filesetList.toArray( new TaskCopyEntity.ITaskFilesetEntity[0] ) );
		}

		return taskEntity ;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask() ;

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskCopyEntity) )
					continue ;
				Copy copy = dest.addNewCopy() ;
				mapDB2XMLBeansSubType( src , entity , copy ) ;
			}
		}

//		Result
		TaskResultCopyMapping resultMapping = new TaskResultCopyMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 * @param src 複写元のDBのEntity親階層
	 * @param entity 複写元のDBのEntity
	 * @param copy マッピング結果を格納するbeans
	 */
	public void mapDB2XMLBeansSubType(ITaskTargetEntity src, ITaskTaskTypeEntity entity , CopyType copy ) {

		TaskCopyEntity taskEntity = (TaskCopyEntity)entity;
		copyProperties( taskEntity, copy );

		//Overwrite
		TaskCopyEntity.ITaskOverwriteEntity overwriteEntity = taskEntity.getOverwrite();
		if( null != overwriteEntity ) {
			OverwriteType overwrite = copy.addNewOverwriteInfo();
			copyProperties( overwriteEntity, overwrite );
		}

		//Charsetinfo
		TaskCopyEntity.ITaskCharsetinfoEntity[] charsetinfoArray = taskEntity.getCharsetinfo();
		if( null != charsetinfoArray ) {
			for( TaskCopyEntity.ITaskCharsetinfoEntity charsetinfoEntity : charsetinfoArray ) {
				CharsetinfoType charsetinfo = copy.addNewCharsetinfo();
				copyProperties( charsetinfoEntity, charsetinfo );
			}
		}

		//Fileset
		TaskCopyEntity.ITaskFilesetEntity[] filesetArray = taskEntity.getFileset();
		if( null != filesetArray ) {
			for( TaskCopyEntity.ITaskFilesetEntity filesetEntity : filesetArray ) {
				Fileset fileset = copy.addNewFileset();
				copyProperties( filesetEntity, fileset );
			}
		}
	}
}
