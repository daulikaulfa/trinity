package jp.co.blueship.tri.bm.task.copy.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskCopyEntity extends TaskTaskEntity implements ITaskCopyEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskOverwriteEntity overwrite = null;
	private ITaskCharsetinfoEntity[] charsetinfo = new ITaskCharsetinfoEntity[0];
	private ITaskFilesetEntity[] fileset = new ITaskFilesetEntity[0];
	private String defineFilePath = null;
	private String holdTimestamp = StatusFlg.off.value();
	private String doNeglect = StatusFlg.on.value();
	private String doNotExistSkip = StatusFlg.on.value();
	private String doDetailSnap = StatusFlg.off.value();
	private String doRecordFile = StatusFlg.off.value();


	/**
	 * インナークラス
	 *
	 */
	public class TaskOverwriteEntity implements ITaskOverwriteEntity {

		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String doAdd = StatusFlg.on.value();
		private String doUpdateOld = StatusFlg.on.value();
		private String doUpdateNew = StatusFlg.on.value();

		public String getDoAdd() {
			return doAdd;
		}
		public void setDoAdd(String doAdd) {
			this.doAdd = doAdd;
		}
		public String getDoUpdateNew() {
			return doUpdateNew;
		}
		public void setDoUpdateNew(String doNew) {
			this.doUpdateNew = doNew;
		}
		public String getDoUpdateOld() {
			return doUpdateOld;
		}
		public void setDoUpdateOld(String doOld) {
			this.doUpdateOld = doOld;
		}
	}
	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskCharsetinfoEntity implements ITaskCharsetinfoEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String extensions = null;
		private String in = null;
		private String out = null;
		private String outls = null;

		public String getExtensions() {
			return extensions;
		}
		public void setExtensions(String extensions) {
			this.extensions = extensions;
		}
		public String getIn() {
			return in;
		}
		public void setIn(String in) {
			this.in = in;
		}
		public String getOut() {
			return out;
		}
		public void setOut(String out) {
			this.out = out;
		}
		public String getOutls() {
			return outls;
		}
		public void setOutls(String outls) {
			this.outls = outls;
		}
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public class TaskFilesetEntity implements ITaskFilesetEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String src = null;
		private String dest = null;

		public String getSrc() {
			return src;
		}
		public void setSrc( String value ) {
			src = value;
		}
		public String getDest() {
			return dest;
		}
		public void setDest( String value ) {
			dest = value;
		}
	}

	public ITaskCharsetinfoEntity[] getCharsetinfo() {
		return charsetinfo;
	}

	public void setCharsetinfo(ITaskCharsetinfoEntity[] charsetinfo) {
		this.charsetinfo = charsetinfo;
	}

	public String getDefineFilePath() {
		return defineFilePath;
	}

	public void setDefineFilePath(String defineFilePath) {
		this.defineFilePath = defineFilePath;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public String getDoNeglect() {
		return doNeglect;
	}

	public void setDoNeglect(String doNeglect) {
		this.doNeglect = doNeglect;
	}

	public String getDoNotExistSkip() {
		return doNotExistSkip;
	}

	public void setDoNotExistSkip(String doNotExistSkip) {
		this.doNotExistSkip = doNotExistSkip;
	}

	public ITaskFilesetEntity[] getFileset() {
		return fileset;
	}

	public void setFileset(ITaskFilesetEntity[] fileset) {
		this.fileset = fileset;
	}

	public String getHoldTimestamp() {
		return holdTimestamp;
	}

	public void setHoldTimestamp(String holdTimestamp) {
		this.holdTimestamp = holdTimestamp;
	}

	public ITaskOverwriteEntity getOverwrite() {
		return overwrite;
	}

	public void setOverwrite(ITaskOverwriteEntity overwrite) {
		this.overwrite = overwrite;
	}

	public String getDoRecordFile() {
		return doRecordFile;
	}

	public void setDoRecordFile( String doRecordFile ) {
		this.doRecordFile = doRecordFile;
	}
}
