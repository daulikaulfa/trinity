package jp.co.blueship.tri.bm.task.copy;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.constants.TriExtensionType;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.copy.TaskProcCopy.ReformDefinition.ReformFile;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskCharsetinfoEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskFilesetEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskCopyFileEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskDefineDetailFileEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskResultCopyEntity;
import jp.co.blueship.tri.fw.agent.core.TaskDefineFileUtils;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.FileConvertCharset;
import jp.co.blueship.tri.fw.cmn.io.FileCopy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyNoOverride;
import jp.co.blueship.tri.fw.cmn.io.FileCopyNoOverridePreserveLastModified;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverride;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverridePreserveLastModified;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「copy」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元に、ファイルのコピーを行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class TaskProcCopy implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private enum Overwrite {
		insert,
		update ,
		through;
	}

	private ITaskPropertyEntity[] propertyEntity = null;
	public void setProperty( ITaskPropertyEntity[] propertyEntity ) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;
	public void setSupport( TaskFinderSupport support ) {
		this.support = support;
	}

	/**
	 * 登録されている拡張子情報のキャッシュ。
	 * 直接参照する場合、Nullの可能性があるので{@link #getExtension()}の使用を推奨
	 */
	private Map<String,Boolean> extensionMap = null;

	private final String SUPER_WILD_CARD = "$$";
	private final String WILD_CARD = "*";
	private final String DST_DIRECTORY = ".";

	/**
	 * リフォーム元トップディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String REFORM_SRC_TOP_PATH = "REFORM_SRC_TOP_PATH";
	/**
	 * リフォーム先トップディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String REFORM_DST_TOP_PATH = "REFORM_DST_TOP_PATH";
	/**
	 * リフォーム実行判断ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String REFORM_JUDGE_PATH = "REFORM_JUDGE_PATH";


	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {

		ITaskCopyEntity copy = (ITaskCopyEntity) task;

		ITaskFilesetEntity[] filesetArray = copy.getFileset();
		if( null != filesetArray ) {

			for( ITaskFilesetEntity fileset : filesetArray ) {
				fileset.setSrc	( TaskStringUtils.substitutionPath( this.propertyEntity , fileset.getSrc() ));
				fileset.setDest	( TaskStringUtils.substitutionPath( this.propertyEntity , fileset.getDest() ));
			}
		}

		String path = copy.getDefineFilePath();
		if( null != path ) {
			copy.setDefineFilePath( TaskStringUtils.substitutionPath( this.propertyEntity , path ));
		}
	}

	/**
	 * 「copy」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute(
			ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {

		if ( ! (prmTask instanceof ITaskCopyEntity) )
			return null;

		ITaskResultCopyEntity resultCopyEntity = new TaskResultCopyEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultCopyEntity.setSequence( prmTask.getSequence() );
		resultCopyEntity.setCode( TaskResultCode.UN_PROCESS );
		resultCopyEntity.setTaskId( prmTask.getTaskId() );

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskCopyEntity copy = (ITaskCopyEntity) prmTask;

			if ( null == copy.getFileset() && null == copy.getDefineFilePath() )
				return resultCopyEntity;

			if ( null != copy.getFileset() ) {
				resultSequence = this.copy	( copy, resultCopyEntity, resultList, resultSequence );
			}

			if ( null != copy.getDefineFilePath() ) {
				resultSequence = this.reform( copy, resultCopyEntity, resultList, resultSequence );
			}

			resultCopyEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultCopyEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005015S , e );

		} finally {
			resultCopyEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult	( resultCopyEntity );
		}

		return resultCopyEntity;
	}

	/**
	 * 定義ファイルによらないコピー処理を行う。
	 * @param copy コピータスク
	 * @param resultCopyEntity 処理結果エンティティ
	 * @param resultFilesetList 処理結果（詳細）リスト
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 * @throws Exception
	 */
	private Integer copy(	ITaskCopyEntity copy,
						ITaskResultCopyEntity resultCopyEntity,
						List<ITaskResultEntity> resultList,
						Integer sequence )
		throws Exception {

		for( ITaskFilesetEntity fileset : copy.getFileset() ) {
			//filesetごとに処理結果を記録する
			ITaskResultEntity innerResultEntity = resultCopyEntity.newResult();
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

			try {
				String src	= fileset.getSrc();
				String dest	= fileset.getDest();

				innerResultEntity.setSequence( String.valueOf(sequence++) );
				innerResultEntity.setSrc( src );
				innerResultEntity.setDest( dest );

				List<String> copyFileList = new ArrayList<String>();

				copyProc( copy, src, dest, copyFileList );

				setCopyFileResult( copy, innerResultEntity, copyFileList, src );

				innerResultEntity.setCode( TaskResultCode.SUCCESS );

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( copy.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		return sequence;
	}

	/**
	 * 結果にコピーしたファイルツリーを取得する
	 * @param copy
	 * @param innerResultEntity
	 * @param copyFileSet
	 */
	private void setCopyFileResult(
			ITaskCopyEntity copy, ITaskResultEntity innerResultEntity, List<String> copyFileList, String srcPath ) {

		if ( !StatusFlg.on.value().equals( copy.getDoRecordFile() )) {
			innerResultEntity.setCopyFile( new ITaskCopyFileEntity[0] );
			return;
		}


		// 相対パス化する
		File srcFile = new File( srcPath );

		Set<String> copyFileSet = new TreeSet<String>();
		for ( String compPath : copyFileList ) {

			// srcPathとcompPathのパス区切り文字フォーマットを合わせるため、あえてFile化
			String relativePath =
				TriStringUtils.convertRelativePath( srcFile, TriStringUtils.convertPath( new File( compPath )));

			if ( !TriStringUtils.isEmpty( relativePath )) {
				copyFileSet.add( relativePath );
			}
		}


		List<ITaskCopyFileEntity> copyFileEntityList = new ArrayList<ITaskCopyFileEntity>();

		for ( String copyFile : copyFileSet ) {
			ITaskCopyFileEntity copyFileEntity = innerResultEntity.newCopyFile();
			copyFileEntity.setPath( copyFile );

			copyFileEntityList.add( copyFileEntity );
		}

		innerResultEntity.setCopyFile( copyFileEntityList.toArray( new ITaskCopyFileEntity[0] ));
		return;
	}

	/**
	 * 定義ファイルによるコピー処理(リフォーム処理)を行う。
	 * @param copy
	 * @param resultCopyEntity
	 * @param resultFilesetList
	 * @param sequence
	 * @return 処理結果（詳細）の実行順序
	 * @throws Exception
	 */
	private Integer reform(	ITaskCopyEntity copy,
							ITaskResultCopyEntity resultCopyEntity,
							List<ITaskResultEntity> resultList,
							Integer sequence )
		throws Exception {

		File defListFile = new File( copy.getDefineFilePath() );
		if ( !defListFile.exists() ) {
			throw new TriSystemException(BmMessageId.BM004020F , copy.getDefineFilePath() );
		}

		List<ReformDefinition> defFileList	= getReformDefFileList( defListFile );
		for ( ReformDefinition defFile : defFileList ) {
			//ReformDefinitionごとに処理結果を記録する
			ITaskResultEntity resultEntity = resultCopyEntity.newResult();
			List<ITaskDefineDetailFileEntity> resultDetailList = new ArrayList<ITaskDefineDetailFileEntity>();

			try {
				resultEntity.setSequence( String.valueOf(sequence++) );
				resultEntity.setDefineFilePath( copy.getDefineFilePath() );
				resultEntity.setDefineDetailFilePath( defFile.getDefineDetailFilePath() );
				resultEntity.setSrc( defFile.getSrcTopPath() );
				resultEntity.setDest( defFile.getDstTopPath() );

				// リフォーム実行パスが実在しなければリフォームしないが、1個でも実在すればリフォームする
				boolean existReformJudgePath = false;
				List<String> reformJudgePathList = defFile.getReformJudgePathList();
				for (String reformJudgePath : reformJudgePathList) {

					if ( null != reformJudgePath && new File( reformJudgePath ).exists() )
						existReformJudgePath = true;
				}

				//判断パスが記述されている場合のみ、実在チェックを行う
				if( false == existReformJudgePath && 0 < reformJudgePathList.size() ) {
					resultEntity.setCode( TaskResultCode.UN_PROCESS );
					continue;
				}

				this.reformDetail( copy, resultEntity, resultDetailList, defFile );

				resultEntity.setCode( TaskResultCode.SUCCESS );

			} catch ( Exception e ) {
				resultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				resultEntity.setDefineDetailFile( resultDetailList.toArray( new ITaskDefineDetailFileEntity[ 0 ] ) );
				if ( StatusFlg.on.value().equals( copy.getDoDetailSnap() ) ) {
					resultList.add( resultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( resultEntity.getCode() ) ) {
						resultList.add( resultEntity );
					}
				}
			}
		}

		return sequence;
	}

	/**
	 * 定義ファイルによるコピー処理(リフォーム処理)を行う。
	 * @param copy
	 * @param resultEntity
	 * @param resultDetailList
	 * @param defFile
	 * @throws Exception
	 */
	private void reformDetail(	ITaskCopyEntity copy,
								ITaskResultEntity resultEntity,
								List<ITaskDefineDetailFileEntity> resultDetailList,
								ReformDefinition defFile )
		throws Exception {

		Integer resultSequence = 1;

		for ( ReformFile refFile : defFile.getReformFileList() ) {
			ITaskDefineDetailFileEntity defineDetailFileEntity = resultEntity.newDefineDetailFile();

			try {
				defineDetailFileEntity.setSequence( String.valueOf(resultSequence++) );
				defineDetailFileEntity.setRelativeSrc( refFile.getSrcPath() );
				defineDetailFileEntity.setRelativeDest( refFile.getDstPath() );

				defineDetailFileEntity.setCode( reformProc( copy, defFile, refFile ) );

			} catch ( Exception e ) {
				defineDetailFileEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				if ( StatusFlg.on.value().equals( copy.getDoDetailSnap() ) ) {
					resultDetailList.add( defineDetailFileEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( defineDetailFileEntity.getCode() ) ) {
						resultDetailList.add( defineDetailFileEntity );
					}
				}
			}
		}
	}

	/**
     * リフォーム元トップディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
	 * @throws Exception
     */
    private boolean setReformSrcTopPath( String line, ReformDefinition rd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, REFORM_SRC_TOP_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		rd.setSrcTopPath( newValue );
    		return  true;
    	}
    }

	/**
     * リフォーム先トップディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setReformDstTopPath( String line, ReformDefinition rd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, REFORM_DST_TOP_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		rd.setDstTopPath( newValue );
    		return  true;
    	}
    }

	/**
     * リフォーム実行判断ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean addReformJudgePath( String line, ReformDefinition rd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, REFORM_JUDGE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		rd.getReformJudgePathList().add( newValue );
    		return  true;
    	}
    }

	/**
	 * リフォーム定義リストファイルを読み込み、リフォーム定義情報のリストを返す。
	 * @param defListFile リフォーム定義リストファイル
	 * @return リフォーム定義情報のリスト
	 */
	private List<ReformDefinition> getReformDefFileList( File defListFile ) {

		List<File> defList = TaskDefineFileUtils.getDefFileList( defListFile );

		List<ReformDefinition> refDefList = new ArrayList<ReformDefinition>();

		for ( File defFile : defList ) {
			refDefList.add( getReformDefinition( defFile ));
		}

		return refDefList;
	}

	/**
	 * リフォーム定義ファイルを読み込み、リフォーム定義情報を返す。
	 * @param defFile リフォーム定義ファイル
	 * @return リフォーム定義情報
	 */
	private ReformDefinition getReformDefinition( File defFile ) {

		ReformDefinition reformDefinition	= new ReformDefinition();
		reformDefinition.setDefineDetailFilePath( TriStringUtils.convertPath(defFile.getPath()) );
		int lineNumber		= 0;

		try {
			Map<String,String> aliasDic = new HashMap<String,String>();

			String[] lineArray = TriFileUtils.readFileLine( defFile , false ) ;
			for( String line : lineArray ) {
				lineNumber = lineNumber + 1;

				// コメント、空白行はスキップ
				if ( !TaskDefineFileUtils.isContents( line ))		continue;
				// 変数の定義行は、変数登録をしてスキップ
				if ( TaskDefineFileUtils.isAlias( line, aliasDic ))	continue;

				// 変数を置換
				line = TaskDefineFileUtils.replaceAliasVariable( line, aliasDic );

				// リフォーム元トップの定義行でがあれば、設定してスキップ
				if ( setReformSrcTopPath( line, reformDefinition ))	continue;
				// リフォーム先トップの定義行でがあれば、設定してスキップ
				if ( setReformDstTopPath( line, reformDefinition ))	continue;
				// リフォーム実施ディレクトリの定義行でがあれば、追加してスキップ
				if ( addReformJudgePath( line, reformDefinition ))	continue;

				// 上記以外だったら、リフォームの定義行と判断して設定
				setReformDefinitionValue( line, reformDefinition );
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM004021F , e , defFile.getAbsolutePath() , String.valueOf(lineNumber) );
		}
		return reformDefinition;
	}

    /**
     * 定義ファイルの値を取得する
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setReformDefinitionValue( String line, ReformDefinition rd ) {

    	final String SPRITTER = "," ;
    	final int DEFFILE_COLUMN_COUNT = 2 ;

		String[] reformPath = line.split( SPRITTER );
		if ( DEFFILE_COLUMN_COUNT != reformPath.length ) {
			return false;
		}

		if( reformPath[0].trim().equals( "" ) || reformPath[1].trim().equals( "" ) ) {
			return false;
		}

		// ↓の処理でタスクからの変数展開ができるが、遅くなりそうだしメリットがなさそうなのでとりあえずはしない
//		String srcPath = TaskStringUtil.substitutionPath( this.propertyEntity , reformPath[0] );
//		String dstPath = TaskStringUtil.substitutionPath( this.propertyEntity , reformPath[1] );

		//String formatedSrcPath = StringAddonUtil.convertPath( reformPath[0] ).trim();
		//String formatedDstPath = StringAddonUtil.convertPath( reformPath[1] ).trim();

		String formatedSrcPath = StringUtils.replace( reformPath[0] , "\\" , "/" ).trim();
		String formatedDstPath = StringUtils.replace( reformPath[1] , "\\" , "/" ).trim();

		// リフォーム定義の書式が正しいかどうかのチェック
		checkDefFormat( formatedSrcPath, formatedDstPath );

		ReformFile rf = rd.newReformFile();

		rf.setSrcPath( formatedSrcPath );
		rf.setDstPath( formatedDstPath );

		rd.getReformFileList().add( rf );

		return true;
    }

	/**
	 * 再帰的にフォルダ構造を辿り、ファイルをコピーしていく。必要なディレクトリは作成する<br>
	 * @param copy
	 * @param srcPath
	 * @param dstPath
	 * @throws Exception
	 */
	private void copyProc( ITaskCopyEntity copy , String srcPath , String dstPath, List<String> copyFileList ) throws Exception {

		File file = new File( srcPath );
		if ( !isCopy( copy, file ) ) return;


		if( file.isDirectory() ) {//ディレクトリ
			File dir = new File( dstPath );

			dir.mkdirs();
			File[] files = file.listFiles();
			if( null == files ) {
				throw new TriSystemException( BmMessageId.BM004022F , srcPath);
			}

			if ( StatusFlg.on.value().equals( copy.getDoRecordFile() )) {
				copyFileList.add( srcPath );
			}

			for( File tmpFile : files ) {
				copyProc(
						copy ,
						TriStringUtils.linkPathBySlash( srcPath , tmpFile.getName() ) ,
						TriStringUtils.linkPathBySlash( dstPath , tmpFile.getName() ) ,
						copyFileList );
			}
		} else {//ファイル
			copyFile( copy , new File( srcPath ) , new File( dstPath ));

			if ( StatusFlg.on.value().equals( copy.getDoRecordFile() )) {
				copyFileList.add( srcPath );
			}
		}
	}

	/**
	 * 再帰的にフォルダ構造を辿り、ファイルをコピーしていく。必要なディレクトリは作成する<br>
	 * @param copy ITaskCopyEntity
	 * @param defFile ReformDefinition
	 * @param refFile ReformFile
	 * @return {@link TaskResultCode}
	 * @throws Exception
	 */
	private String reformProc(
			ITaskCopyEntity copy, ReformDefinition defFile, ReformFile refFile )
		throws Exception {

		String resultCode = TaskResultCode.UN_PROCESS;

		String srcTopPath = defFile.getSrcTopPath();
		if ( null == srcTopPath ) {
			srcTopPath = "";
		}

		String dstTopPath = defFile.getDstTopPath();
		if ( null == dstTopPath ) {
			dstTopPath = "";
		}


		File srcFile = new File( srcTopPath, refFile.getSrcPath() );

		// ワイルドカード使用時
		if ( isUseWildCard( srcFile.getName() )) {

			// リフォーム元が"/"で終わっていれば、ワイルドカードでファイルをマッチさせないためのフラグ
			boolean copyDirOnly = false;
			if ( refFile.getSrcPath().endsWith( "/" ) ) {
				copyDirOnly = true;
			}

			File srcParent = srcFile.getParentFile();
			if ( srcParent.exists() )
				resultCode = TaskResultCode.SUCCESS;

			// ワイルドカード使用時
			reform4WildCardProc( copy, srcFile, refFile.getDstPath(), dstTopPath, copyDirOnly );

		// ワイルドカード未使用時
		} else {
			//コピー元ファイル定義がファイル扱いで、実際のコピー元ファイルがディレクトリの場合⇒実行しない
			if( ! refFile.getSrcPath().endsWith( "/" ) &&
				srcFile.isDirectory() ) {
				return resultCode;
			}

			//コピー元ファイルが実在し、中にファイルが存在する場合のみ、ディレクトリの作成およびコピーを行う。
			FileFilter filter = BusinessFileUtils.getFileFilter() ;
			if ( srcFile.exists() &&
				true == TriFileUtils.checkExistFiles( srcFile.getCanonicalPath() , filter ) ) {

				resultCode = TaskResultCode.SUCCESS;

				File dstFile = createDstDir( srcFile, refFile.getDstPath(), dstTopPath );
				reformProc( copy, srcFile, dstFile );
			}
		}

		return resultCode;
	}

	/**
	 * ワイルドカード使用時のリフォーム
	 * @param copy ITaskCopyEntity
	 * @param srcFile リフォーム元のファイルオブジェクト
	 * @param dstPath リフォーム先のパス
	 * @param reformOutPath リフォーム出力先のパス
	 * @param copyDirOnly ディレクトリのみコピーするフラグ
	 */
	private void reform4WildCardProc(	ITaskCopyEntity copy,
										File srcFile, String dstPath,
										String reformOutPath,
										boolean copyDirOnly ) throws Exception {

		String reformTarget = srcFile.getName();

		// そもそもリフォーム元の親ディレクトリが存在しなければスルー
		// reformTargetにはワイルドカードが入っているため、ここでは対象ファイル、ディレクトリの
		// 存在確認はできない
		File srcParent = srcFile.getParentFile();
		if( StatusFlg.off.value().equals( copy.getDoNotExistSkip() ) && !srcParent.exists() ) {
			throw new TriSystemException( BmMessageId.BM004023F , srcParent.getAbsolutePath() );
		}

		if( StatusFlg.on.value().equals( copy.getDoNotExistSkip() ) && !srcParent.exists() ) {
			return;
		}

		// スーパーワイルドカード($$)指定
		if ( reformTarget.equals( SUPER_WILD_CARD )) {

			reform4SuperWildCardProc( copy, srcFile, dstPath, reformOutPath );

		// 通常のワイルドカード(*)指定
		} else {
			reform4NormalWildCardProc( copy, srcFile, dstPath, reformOutPath, copyDirOnly );
		}
	}

	/**
	 * 超絶ワイルドカード($$)使用時のリフォーム
	 * @param copy ITaskCopyEntity
	 * @param srcFile リフォーム元のファイルオブジェクト
	 * @param dstPath リフォーム先のパス
	 * @param reformOutPath リフォーム出力先のパス
	 */
	private void reform4SuperWildCardProc(	ITaskCopyEntity copy,
											File srcFile,
											String dstPath,
											String reformOutPath ) throws Exception {

		File dstFile = createDstDir( srcFile, dstPath, reformOutPath );

		for ( File targetFile : srcFile.getParentFile().listFiles() ) {

			// 空のディレクトリはコピーしない
			if ( targetFile.isDirectory() && 0 == targetFile.list().length ) {
				continue;
			}

			File targetDstFile = dstFile;
			if ( targetFile.isDirectory() ) {
				targetDstFile = new File( dstFile, targetFile.getName() );
			}

			reformProc( copy, targetFile, targetDstFile );
		}
	}

	/**
	 * 通常ワイルドカード(*)使用時のリフォーム
	 * @param copy ITaskCopyEntity
	 * @param srcFile リフォーム元のファイルオブジェクト
	 * @param dstPath リフォーム先のパス
	 * @param reformOutPath リフォーム出力先のパス
	 * @param copyDirOnly ディレクトリのみコピーするフラグ
	 */
	private void reform4NormalWildCardProc(	ITaskCopyEntity copy,
												File srcFile,
												String dstPath,
												String reformOutPath,
												boolean copyDirOnly ) throws Exception {

		String reformTarget	= srcFile.getName();
		File srcParent		= srcFile.getParentFile();

		Pattern pattern		= Pattern.compile( "\\*+" );
		String[] condition	= pattern.split( reformTarget );

		// *で始まっているとconditionの最初に空文字を入れてくれるクセに、
		// *で終わっていると最後に空文字を入れてくれないので自分で入れる。
		if ( reformTarget.endsWith( "*" ) ) {
			String[] tmp = new String[ condition.length + 1 ];
			for ( int i = 0; i < condition.length; i++ ) {
				tmp[ i ] = condition[ i ];
			}
			tmp[ tmp.length - 1 ] = "";
			condition = tmp;
		}


		File dstFile = createDstDir( srcFile, dstPath, reformOutPath );

		for ( String file : srcParent.list() ) {

			if ( match( file, condition ) ) {

				File targetFile = new File( srcParent, file );
				// 空のディレクトリはコピーしない
				if ( targetFile.isDirectory() && 0 == targetFile.list().length ) {
					continue;
				}

				// ワイルドカードにはマッチしたが、ディレクトリのみコピーなのでコピーしない
				if ( copyDirOnly && targetFile.isFile() ) continue;

				if ( !copyDirOnly && targetFile.isDirectory() ) continue;

				File targetDstFile = dstFile;
				if ( targetFile.isDirectory() ) {
					targetDstFile = new File( dstFile, targetFile.getName() );
				}

				reformProc( copy, targetFile, targetDstFile );
			}
		}
	}

	/**
	 * 再帰的にフォルダ構造を辿り、ファイルをコピーしていく。必要なディレクトリは作成する<br>
	 * @param copy
	 * @param srcFile
	 * @param dstFile
	 * @throws Exception
	 */
	private void reformProc( ITaskCopyEntity copy , File srcFile , File dstFile ) throws Exception {

		if ( !isCopy( copy, srcFile ) ) return;


		if( srcFile.isDirectory() ) {

			if ( dstFile.exists() && dstFile.isFile() ) {
				throw new TriSystemException( BmMessageId.BM004024F , srcFile.getAbsolutePath() + File.separator , dstFile.getAbsolutePath() );
			}


			File[] files = srcFile.listFiles();
			if( null == files ) {
				throw new TriSystemException( BmMessageId.BM004022F , srcFile.getAbsolutePath() );
			}
			for( File tmpFile : files ) {
				reformProc(	copy ,
							new File( srcFile , tmpFile.getName() ) ,
							new File( dstFile , tmpFile.getName() ));
			}

		} else {

			if ( dstFile.exists() && dstFile.isDirectory() ) {

				dstFile = new File( dstFile, srcFile.getName() );

				// ファイルをディレクトリに上書きはできない
				if ( dstFile.exists() && dstFile.isDirectory() ) {
					throw new TriSystemException( BmMessageId.BM004025F , srcFile.getAbsolutePath() , dstFile.getAbsolutePath() + File.separator);
				}
			}

			if ( !dstFile.getParentFile().exists() ) {
				dstFile.getParentFile().mkdirs();
			}
			copyFile( copy , srcFile , dstFile );
		}
	}

	/**
	 * Reform定義の書式チェックを行う。
	 * @param srcPath reform定義のreform元
	 * @param dstPath reform定義のreform先
	 */
	private void checkDefFormat( String srcPath, String dstPath ) {

		// 元がファイル指定、ファイル用ワイルドカード指定、またはスーパーワイルドカード指定の場合、
		// 先はディレクトリ指定であってはならない
		// (元が"/"で終わっていない場合、先は"/"で終わっていてはならない)
		if ( !srcPath.endsWith( "/" ) && dstPath.endsWith( "/" ) ) {
			throw new TriSystemException(BmMessageId.BM004026F);
		}

		// 元がディレクトリ指定の場合、先もディレクトリ用ディレクトリ指定でなければならない
		// (元が"/"で終わっている場合、先は"/./"でなければならない)
		if ( srcPath.endsWith( "/" ) && !dstPath.endsWith( "/" + DST_DIRECTORY + "/" ) ) {
			throw new TriSystemException(BmMessageId.BM004027F);
		}


		File srcFile = new File( srcPath );
		String srcName = srcFile.getName();

		// 元がファイル用ワイルドカード指定、またはスーパーワイルドカード指定の場合、
		// 先はファイル用ディレクトリ指定でなければならない
		// (元が"$$"、または"*"が含まれる場合、先は"/."で終わっていなければならない)
		if ( !srcPath.endsWith( "/" ) ) {

			if ( isUseWildCard( srcName ) ) {

				if ( !dstPath.endsWith( "/" + DST_DIRECTORY ) ) {
					throw new TriSystemException(BmMessageId.BM004028F);
				}
			}
		}

		// 元は固定ディレクトリ指定であってはならない
		if ( srcPath.endsWith( "/" ) ) {

			if ( srcName.indexOf( WILD_CARD ) == -1 ) {
				throw new TriSystemException(BmMessageId.BM004029F);
			}
		}
	}

	/**
	 * ワイルドカードが使用されているかどうかをチェックする
	 * @param target チェック対象文字列
	 * @return ワイルドカードが使用されていればtrue、そうでなければfalse
	 */
	private boolean isUseWildCard( String target ) {

		if ( target.equals( SUPER_WILD_CARD ) || target.indexOf( WILD_CARD ) != -1 ) {
			return true;
		}

		return false;

	}

	/**
	 * コピー先のFileオブジェクトを作成する。
	 * @param srcFile コピー元ファイル
	 * @param dst コピー先のパス
	 * @param reformOutPath リフォーム出力先パス
	 * @return 作成されたファイルオブジェクト
	 */
	private File createDstDir( File srcFile, String dst, String reformOutPath ) {

		File dstFile = null;

		if ( dst.endsWith( DST_DIRECTORY ) ) {

			dstFile = new File( reformOutPath, StringUtils.chop( dst ) );

			if ( !dstFile.exists() ) {
				dstFile.mkdirs();
			}

		} else if ( dst.endsWith( DST_DIRECTORY + "/" ) ) {

			dstFile = new File( reformOutPath, StringUtils.chomp( dst, DST_DIRECTORY + "/" ) );

			if ( !dstFile.exists() ) {
				dstFile.mkdirs();
			}

		} else {
			dstFile = new File( reformOutPath, dst );

			if ( dstFile.exists() && dstFile.isDirectory() ) {
				throw new TriSystemException(BmMessageId.BM004025F , srcFile.getAbsolutePath() , dstFile.getAbsolutePath() + File.separator );
			}
		}

		return dstFile;
	}

	/**
	 * ファイル名が文字列で与えられた条件に合うかチェックする。
	 */
	private boolean match( String fileName, String[] condition ) {

		int index = 0;
		int preIndex = 0;

		for ( int i = 0; i < condition.length; i++ ) {

			if ( condition[i].trim().equals("") ) {
				continue;
			}

			if ( i == 0 ) {
				if ( !fileName.startsWith( condition[i] )) {
					return false;
				}

			} else if ( i == ( condition.length - 1 )) {
				if ( !fileName.endsWith( condition[i] )) {
					return false;
				}

			} else {
				index = fileName.indexOf( condition[i] );
				if ( index == -1 ) {
					return false;
				}
				if ( preIndex > index ) {
					return false;
				}
				preIndex = index;
			}
		}
		return true;
	}

	/**
	 * コピー処理 本体
	 * @param copy
	 * @param srcFile
	 * @param dstFile
	 * @throws Exception
	 */
	private void copyFile( ITaskCopyEntity copy , File srcFile , File dstFile ) throws Exception {

		if( true != srcFile.isFile() || true != srcFile.canRead() ) {
			throw new TriSystemException(BmMessageId.BM004030F , srcFile.getAbsolutePath());
		}
		if( true == dstFile.isDirectory() ) {
			throw new TriSystemException(BmMessageId.BM004031F , dstFile.getAbsolutePath());
		}

		File dstParentDir = new File(dstFile.getParent());
		if(! dstParentDir.exists() ) {
			if( false == dstParentDir.mkdirs() ) {
				throw new TriSystemException(BmMessageId.BM004032F , dstFile.getParent());
			}
		}

//		ファイル種別の判別：デフォルトはバイナリとみなす
		Map<String,Boolean> extensionMap = this.getExtensionMap();
		String extension = TriFileUtils.getExtension( srcFile.getName() );
		Boolean binary = ( null != extension ) ? extensionMap.get( extension ) : null;
		if( null == binary ) {
			binary = false;
		}
		TriExtensionType extType = ( binary ) ? TriExtensionType.binary: TriExtensionType.ascii;

		//Overwriteのチェック
		Overwrite overwriteChk = this.chkOverwrite( copy.getOverwrite() , srcFile , dstFile );
		if( Overwrite.through == overwriteChk ) {
			return;//コピーをスキップ
		}
		//charsetinfoのチェック
		ITaskCharsetinfoEntity charsetinfo = getCharsetinfo( copy.getCharsetinfo() , srcFile.getName() );

		//コピー処理
		boolean holdTimestamp = ( StatusFlg.on.value().toString().equals( copy.getHoldTimestamp() ) ) ? true : false;
		if( TriExtensionType.ascii.equals( extType ) &&
			null != charsetinfo ) {//文字コードをコンバートしてコピー

			FileConvertCharset fileConvert = new FileConvertCharset();
			fileConvert.setSrcFile( srcFile );
			fileConvert.setSrcFileEncoding( charsetinfo.getIn() );
			fileConvert.setDstFile( dstFile );
			fileConvert.setDstFileEncoding( charsetinfo.getOut() );
			if( true != TriStringUtils.isEmpty( charsetinfo.getOutls() ) ) {
				if( LinefeedCode.CRLF.equals( LinefeedCode.getLabel( charsetinfo.getOutls() ) ) ) {//Antに準拠。CRLF以外はLFとみなす
					fileConvert.setDstFileLinefeedCode( LinefeedCode.CRLF.toString() );
				} else {
					fileConvert.setDstFileLinefeedCode( LinefeedCode.LF.toString() );
				}
			}
			fileConvert.setHoldTimestamp( holdTimestamp );
			if( overwriteChk == Overwrite.insert ) {//追加のみ、上書きは行わない
				if( true != dstFile.exists() ) {
					fileConvert.convertFile();
				}
			} else if( overwriteChk == Overwrite.update ) {//既存ファイルの上書き
				fileConvert.convertFile();
			}
			//duplicateList.add( dstFile );
		} else {//単純コピー
			FileCopy fileCopy = null;
			if( overwriteChk == Overwrite.insert ) {//追加のみ、上書きは行わない
				if( true == holdTimestamp ) {//タイムスタンプを保持する
					fileCopy = new FileCopyNoOverridePreserveLastModified();
				} else {//タイムスタンプを保持しない
					fileCopy = new FileCopyNoOverride();
				}
			} else if( overwriteChk == Overwrite.update ) {//既存ファイルの上書き
				if( true == holdTimestamp ) {//タイムスタンプを保持する
					fileCopy = new FileCopyOverridePreserveLastModified();
				} else {//タイムスタンプを保持しない
					fileCopy = new FileCopyOverride();
				}
			}

			PreConditions.assertOf(fileCopy != null, "FileCopy is null.");
			fileCopy.copyFileToFile( srcFile , dstFile , new ArrayList<File>() );//duplicateList );
		}
	}

	/**
	 *
	 * @param overwrite
	 * @param srcFile
	 * @param dstFile
	 * @return
	 * @throws Exception
	 */
	private Overwrite chkOverwrite( ITaskCopyEntity.ITaskOverwriteEntity overwrite , File srcFile , File dstFile ) throws Exception {
		//（※注）Overwriteパラメータは "1" or "0" を想定する。
		Overwrite copyExec = Overwrite.through;
		if( dstFile.exists() ) {//既存ファイルあり
			if( srcFile.lastModified() > dstFile.lastModified() ) {//タイムスタンプが新しい
				if( StatusFlg.on.value().toString().equals( overwrite.getDoUpdateNew() ) ) {//既存ファイルで新しいものはコピー可
					copyExec = Overwrite.update;
				}
			} else {//タイムスタンプが古いか同じ
				if( StatusFlg.on.value().toString().equals( overwrite.getDoUpdateOld() ) ) {//既存ファイルで古いものはコピー可
					copyExec = Overwrite.update;
				}
			}
		} else {//既存ファイルなし
			if( StatusFlg.on.value().toString().equals( overwrite.getDoAdd() ) ) {//新規ファイルコピー可
				copyExec = Overwrite.insert;
			}
		}
		return copyExec;
	}

	private TreeMap<String,ITaskCharsetinfoEntity> charsetinfoMap = null;
	/**
	 * ファイルの拡張子に該当する、「文字コードコンバート＆改行コードの設定」を取得する<br>
	 * １度目にTreeMapに格納してしまうので、２回目以降は多少高速。<br>
	 * @param charsetinfoArray 「文字コードコンバート＆改行コードの設定」リスト
	 * @param srcPath ファイル名文字列
	 * @return 該当した「文字コードコンバート＆改行コードの設定」。該当せずの場合Nullを返す
	 * @throws Exception
	 */
	private ITaskCharsetinfoEntity getCharsetinfo( ITaskCharsetinfoEntity[] charsetinfoArray , String srcPath ) throws Exception {
//		文字コードコンバート＆改行コードの設定
		if( null == charsetinfoMap ) {
			charsetinfoMap = new TreeMap<String,ITaskCharsetinfoEntity>();
			if( null != charsetinfoArray ) {
				for( ITaskCharsetinfoEntity charsetinfo : charsetinfoArray ) {
					charsetinfoMap.put( charsetinfo.getExtensions() , charsetinfo );
				}
			}
		}
		String ext = TriFileUtils.getExtension( srcPath );
		if( null == ext ) {
			return null;
		}
		ITaskCharsetinfoEntity charsetinfo = charsetinfoMap.get( ext );
		return charsetinfo;
	}


	/**
	 * DBに登録されている「拡張子情報」を取得する<br>
	 * １度目に取得しにいくので、２回目以降は多少高速かつ接続エラーも回避できるはず。<br>
	 * @throws Exception
	 */
	private Map<String,Boolean> getExtensionMap() throws Exception {
		//拡張子の設定
		if( null == extensionMap ) {
			extensionMap = support.getExtDecisionMap();
		}
		return extensionMap;
	}

	/**
	 * コピー元のファイルをコピーするかどうか判定する。
	 * @param copy
	 * @param srcFile コピー元ファイル
	 * @return コピーする場合はtrue、そうでなければfalse
	 */
	private boolean isCopy( ITaskCopyEntity copy, File srcFile ) {

		if( StatusFlg.off.value().equals( copy.getDoNotExistSkip() ) && !srcFile.exists() ) {
			throw new TriSystemException(BmMessageId.BM004023F , srcFile.getAbsolutePath());
		}

		if( StatusFlg.on.value().equals( copy.getDoNotExistSkip() ) && !srcFile.exists() ) {
			return false;
		}

		if( StatusFlg.on.value().equals( copy.getDoNeglect() ) && TriFileUtils.isSCM( srcFile )) {
			return false;
		}

		return true;
	}

	/**
	 * リフォーム定義ファイルの情報を保持するクラス
	 */
	public class ReformDefinition {

		/** リフォーム定義ファイルパス */
		private String defineDetailFilePath = null;
		/** リフォーム元トップディレクトリ */
		private String srcTopPath = null;
		/** リフォーム先トップディレクトリ */
		private String dstTopPath = null;
		/** リフォーム判断ディレクトリ */
		private List<String> reformJudgePathList = new ArrayList<String>();
		/** リフォームファイル */
		private List<ReformFile> reformFileList = new ArrayList<ReformFile>();

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getSrcTopPath() {
			return srcTopPath;
		}
		public void setSrcTopPath( String srcTopPath ) {
			this.srcTopPath = srcTopPath;
		}

		public String getDstTopPath() {
			return dstTopPath;
		}
		public void setDstTopPath( String dstTopPath ) {
			this.dstTopPath = dstTopPath;
		}

		public List<String> getReformJudgePathList() {
			return reformJudgePathList;
		}
		public void setReformJudgePathList( List<String> reformJudgePath ) {
			this.reformJudgePathList = reformJudgePath;
		}

		public List<ReformFile> getReformFileList() {
			return reformFileList;
		}
		public void setReformFileList( List<ReformFile> reformFileList ) {
			this.reformFileList = reformFileList;
		}

		public ReformFile newReformFile() {
			return new ReformFile();
		}

		public class ReformFile {

			/** リフォーム元 */
			private String srcPath = null;
			/** リフォーム先 */
			private String dstPath = null;

			public String getSrcPath() {
				return srcPath;
			}
			public void setSrcPath( String srcPath ) {
				this.srcPath = srcPath;
			}

			public String getDstPath() {
				return dstPath;
			}
			public void setDstPath( String dstPath ) {
				this.dstPath = dstPath;
			}
		}

	}

}
