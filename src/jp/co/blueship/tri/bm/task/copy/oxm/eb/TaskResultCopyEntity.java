package jp.co.blueship.tri.bm.task.copy.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultCopyEntity extends TaskTaskResultEntity implements ITaskResultCopyEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];

	public ITaskResultEntity[] getResult() {
		return result;
	}
	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}
	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String src = null;
		private String dest = null;
		private String defineFilePath = null;
		private String defineDetailFilePath = null;
		private ITaskDefineDetailFileEntity[] defineDetailFile = new ITaskDefineDetailFileEntity[0];
		private ITaskCopyFileEntity[] copyFile = new ITaskCopyFileEntity[0];

		public ITaskDefineDetailFileEntity newDefineDetailFile() {
			return new TaskDefineDetailFileEntity();
		}
		public ITaskDefineDetailFileEntity[] getDefineDetailFile() {
			return defineDetailFile;
		}
		public void setDefineDetailFile(ITaskDefineDetailFileEntity[] defineDetailFile) {
			this.defineDetailFile = defineDetailFile;
		}
		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}
		public String getDefineFilePath() {
			return defineFilePath;
		}
		public void setDefineFilePath(String defineFilePath) {
			this.defineFilePath = defineFilePath;
		}
		public String getDest() {
			return dest;
		}
		public void setDest(String dest) {
			this.dest = dest;
		}
		public String getSrc() {
			return src;
		}
		public void setSrc(String src) {
			this.src = src;
		}

		public ITaskCopyFileEntity newCopyFile() {
			return new TaskCopyFileEntity();
		}
		public ITaskCopyFileEntity[] getCopyFile() {
			return copyFile;
		}
		public void setCopyFile( ITaskCopyFileEntity[] copyFile ) {
			this.copyFile = copyFile;
		}
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskDefineDetailFileEntity extends TaskTaskResultEntity implements ITaskDefineDetailFileEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String relativeSrc = null;
		private String relativeDest = null;

		public String getRelativeDest() {
			return relativeDest;
		}
		public void setRelativeDest(String relativeDest) {
			this.relativeDest = relativeDest;
		}
		public String getRelativeSrc() {
			return relativeSrc;
		}
		public void setRelativeSrc(String relativeSrc) {
			this.relativeSrc = relativeSrc;
		}

	}


	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskCopyFileEntity extends TaskTaskResultEntity implements ITaskCopyFileEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null;


		public String getPath() {
			return path;
		}
		public void setPath( String path ) {
			this.path = path;
		}

	}
}
