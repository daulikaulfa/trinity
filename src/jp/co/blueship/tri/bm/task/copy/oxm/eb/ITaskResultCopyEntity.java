package jp.co.blueship.tri.bm.task.copy.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskResultCopyEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult(ITaskResultEntity[] values);
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 全体のコピー元、コピー先の結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * コピー元のディレクトリ／ファイル。
		 * 外部定義ファイルを指定した場合、定義内容のコピー元情報が設定。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * コピー元のディレクトリ／ファイル。
		 * 外部定義ファイルを指定した場合、定義内容のコピー元情報が設定。
		 *
		 * @param value コピー元のディレクトリ／ファイル
		 */
		public void setSrc( String value );
		/**
		 * コピー先のディレクトリ／ファイル。
		 * 外部定義ファイルを指定した場合、定義内容のコピー先情報が設定。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * コピー先のディレクトリ／ファイル。
		 * 外部定義ファイルを指定した場合、定義内容のコピー先情報が設定。
		 *
		 * @param value コピー先のディレクトリ／ファイル
		 */
		public void setDest(String value);
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineFilePath();
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @param value 外部定義ファイル（定義リスト）
		 */
		public void setDefineFilePath(String value);
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineDetailFilePath();
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @param value 外部定義ファイル（詳細定義リスト）
		 */
		public void setDefineDetailFilePath(String value);

		/**
		 * コピー元、コピー先の結果情報を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskDefineDetailFileEntity[] getDefineDetailFile();
		/**
		 * コピー元、コピー先の結果情報を設定します。
		 *
		 * @param values 結果情報
		 */
		public void setDefineDetailFile( ITaskDefineDetailFileEntity[] values );
		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskDefineDetailFileEntity newDefineDetailFile();

		/**
		 * コピーしたファイルツリーを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskCopyFileEntity[] getCopyFile();
		/**
		 * コピーしたファイルツリーを設定します。
		 *
		 * @param value コピーしたファイルツリー
		 */
		public void setCopyFile( ITaskCopyFileEntity[] value );
		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskCopyFileEntity newCopyFile();
	}

	/**
	 *
	 * 個々の定義ファイルごとのコピー元、コピー先の結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskDefineDetailFileEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * srcからの相対パス。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getRelativeSrc();
		/**
		 * srcからの相対パス。
		 *
		 * @param value destからの相対パス
		 */
		public void setRelativeSrc( String value );
		/**
		 * destからの相対パス。
		 *
		 * @return 取得した情報を戻します
		 */
		public String getRelativeDest();
		/**
		 * destからの相対パス。
		 *
		 * @param value destからの相対パス
		 */
		public void setRelativeDest(String value);
	}

	/**
	 *
	 * コピーしたファイルの結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskCopyFileEntity extends IEntity, ITaskResultTypeEntity {
		public String getPath();
		public void setPath( String value );
	}
}
