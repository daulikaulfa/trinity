package jp.co.blueship.tri.bm.task.ant.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.ITaskResultAntEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.TaskResultAntEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.AntResultType;
import jp.co.blueship.tri.fw.schema.beans.task.LogType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Antタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultAntMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			AntResultType[] results = src.getAntArray();

			if( null != results ) {
				for( AntResultType result : results ) {
					TaskResultAntEntity resultEntity = new TaskResultAntEntity() ;

					copyProperties( result , resultEntity ) ;

					LogType log = result.getLog() ;
					if( null != log ) {
						copyProperties( log , resultEntity ) ;
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys =src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultAntEntity) )
					continue;

				TaskResultAntEntity resultEntity = (TaskResultAntEntity)entity;

				AntResultType result = dest.addNewAnt();
				copyProperties( resultEntity, result );

				LogType log = result.addNewLog() ;
				copyProperties( resultEntity, log );
			}
		}

		return dest;
	}

}
