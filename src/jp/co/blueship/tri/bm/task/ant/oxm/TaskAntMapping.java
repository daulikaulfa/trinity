package jp.co.blueship.tri.bm.task.ant.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.CharsetInfoEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.ITaskAntEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.TaskAntEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.AntType;
import jp.co.blueship.tri.fw.schema.beans.task.AntType.Param;
import jp.co.blueship.tri.fw.schema.beans.task.ExecCharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Antタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskAntMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		AntType[] tasks = src.getAntArray();

		if( null != tasks ) {
			for( AntType task : tasks ) {
				TaskAntEntity taskEntity = new TaskAntEntity() ;

				copyProperties( task, taskEntity ) ;

				Param[] params = task.getParamArray() ;

				if( null != params && 0 < params.length ) {
					List<TaskAntEntity.TaskParamEntity> paramList = new ArrayList<TaskAntEntity.TaskParamEntity>();

					for( Param param : params ) {
						TaskAntEntity.TaskParamEntity paramEntity = taskEntity.new TaskParamEntity();
						paramEntity.setKey( param.getKey() );
						paramEntity.setValue( param.getValue() );
						paramList.add( paramEntity );
					}
					TaskAntEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskAntEntity.TaskParamEntity[ 0 ] );
					taskEntity.setParam( paramArray );
				}

				ExecCharsetinfoType charsetInfo = task.getCharsetinfo();
				if ( null != charsetInfo ) {
					CharsetInfoEntity charsetInfoEntity = new CharsetInfoEntity();
					charsetInfoEntity.setIn(charsetInfo.getIn());
					charsetInfoEntity.setOut(charsetInfo.getOut());
					taskEntity.setCharsetInfo(charsetInfoEntity);
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

//		Result
		TaskResultAntMapping resultMapping = new TaskResultAntMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskAntEntity) )
					continue;

				TaskAntEntity taskEntity = (TaskAntEntity)entity;

				AntType task = dest.addNewAnt();

				copyProperties( taskEntity, task );

				ITaskAntEntity.ITaskParamEntity[] paramArray = taskEntity.getParam();

				if( null != paramArray ) {
					for( ITaskAntEntity.ITaskParamEntity paramEntity : paramArray ) {
						Param param = task.addNewParam();
						param.setKey( paramEntity.getKey() );
						param.setValue( paramEntity.getValue() );
					}
				}
			}
		}

//		Result
		TaskResultAntMapping resultMapping = new TaskResultAntMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
