package jp.co.blueship.tri.bm.task.ant;

import java.io.File;
import java.io.IOException;
import java.util.Properties;

import org.springframework.util.StringUtils;

import jp.co.blueship.tri.fw.agent.core.AntTemplates;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;

/**
 * 外部ツール（Ant ビルド）で使用するXML定義ファイルを生成します。
 * </p>
 * [概要]
 * <br>コンパイルグループ（言語種別、環境）ごとに呼び出されます。
 * <br>Java以外の言語環境で、このクラスが呼び出されると、何もしません。
 *
 * </p>
 * @author Yukihiro Eguchi
 *
 */
public class ActionAntBuildXmlMakerGeneral {

	// ${xxx}とするとantの変数展開と重複して誤動作するため、{@xxx}と記述します
	private static final String PROPERTY_TAG_BEGIN = "{@" ;
	private static final String PROPERTY_TAG_END = "}" ;

	private String		templatePath = null ;
	private String		outputBuildXmlPath = null ;

	private Properties paramMap = null ;

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath ;
	}
	public void setOutputBuildXmlPath(String outputBuildXmlPath) {
		this.outputBuildXmlPath = outputBuildXmlPath;
	}
	public Properties getParamMap() {
		return paramMap ;
	}
	public void setParamMap(Properties paramMap) {
		this.paramMap = paramMap ;
	}
	/**
	 *
	 * @throws BaseBusinessException
	 */
	public final boolean execute() throws BaseBusinessException {

		String template = this.getTemplate( templatePath ) ;
		//パラメータの置換
		template = this.replaceMain( template , paramMap ) ;
		//パス区切り文字の統一化
		template = TriStringUtils.convertPath( template ) ;
		//Build.xmlの出力
		this.outputBuildXml( template , AntTemplates.getBuildXml() , outputBuildXmlPath ) ;

		return true ;
	}

	/**
	 * Ant ビルドのXML定義ファイルの雛形テンプレートを取得します。
	 * @param templateName テンプレートファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param list コンバート間に流通させるビーン
	 * @return 雛形テンプレートの文字列表現
	 * @throws BaseBusinessException
	 */
	private final String getTemplate( String templatePath ) throws BaseBusinessException {

		try{
			return TriFileUtils.readFileConvNewLine( new File( templatePath ) ) ;

		} catch (IOException e) {
			throw new TriSystemException( BmMessageId.BM005061S , e );
		}
	}

	/**
	 * テンプレートのメイン部を置換します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param list コンバート間に流通させるビーン
	 * @return 整形したテンプレート文字列
	 * @throws BaseBusinessException
	 */
	private final String replaceMain( String template , Properties properties ) throws BaseBusinessException {

		for (String key : properties.stringPropertyNames()) {
			template = StringUtils.replace( template , PROPERTY_TAG_BEGIN + key + PROPERTY_TAG_END , properties.getProperty(key) ) ;
		}

		return template ;

	}

	/**
	 * 整形したXML定義ファイルをファイル出力します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param buildXmlName XML定義ファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param list コンバート間に流通させるビーン
	 * @return 出力したXML定義ファイルを戻します。
	 * @throws BaseBusinessException
	 */
	private final void outputBuildXml( String template, String buildXmlName, String outputBuildXmlPath ) throws BaseBusinessException {

		try {
			File file = new File( outputBuildXmlPath , buildXmlName ) ;
			TriFileUtils.writeStringFile( file, template ) ;

		} catch( IOException e ) {
			throw new TriSystemException( BmMessageId.BM005062S , e ) ;
		}
	}
}
