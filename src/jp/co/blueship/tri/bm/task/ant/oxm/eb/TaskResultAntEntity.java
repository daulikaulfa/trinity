package jp.co.blueship.tri.bm.task.ant.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;


public class TaskResultAntEntity extends TaskTaskResultEntity implements ITaskResultAntEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String outLog = null ;
	private String errLog = null ;

	public String getOutLog() {
		return outLog ;
	}
	public void setOutLog( String value ) {
		this.outLog = value ;
	}
	public String getErrLog() {
		return errLog ;
	}
	public void setErrLog( String value ) {
		this.errLog = value ;
	}
}
