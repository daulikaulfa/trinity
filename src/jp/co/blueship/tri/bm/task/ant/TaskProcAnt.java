package jp.co.blueship.tri.bm.task.ant;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskLogUtils;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.ITaskAntEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.ITaskAntEntity.ITaskParamEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.TaskAntEntity;
import jp.co.blueship.tri.bm.task.ant.oxm.eb.TaskResultAntEntity;
import jp.co.blueship.tri.fw.agent.core.ActionAntExecutor;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「ant」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとにantタスクの実行を行う。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class TaskProcAnt implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;


	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskAntEntity ant = (TaskAntEntity) task;

		ant.setBuildXmlPath( TaskStringUtils.substitutionPath( propertyEntity , ant.getBuildXmlPath() ) );
		ant.setTemplatePath( TaskStringUtils.substitutionPath( propertyEntity , ant.getTemplatePath() ) );
		ant.setOutLogPath( TaskStringUtils.substitutionPath( propertyEntity , ant.getOutLogPath() ) );
		ant.setErrLogPath( TaskStringUtils.substitutionPath( propertyEntity , ant.getErrLogPath() ) );

		ITaskAntEntity.ITaskParamEntity[] paramArray = ant.getParam();
		if( null != paramArray ) {
			List<ITaskAntEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskAntEntity.ITaskParamEntity>();

			for( ITaskAntEntity.ITaskParamEntity param : paramArray ) {
				String key =TaskStringUtils.substitutionPath( propertyEntity , param.getKey() );
				String value =TaskStringUtils.substitutionPath( propertyEntity , param.getValue() );

				ITaskAntEntity.ITaskParamEntity entity = ant.new TaskParamEntity();
				TriPropertyUtils.copyProperties( entity, param );
				entity.setKey( key );
				entity.setValue( value );
				arrlst.add( entity );

			}
			paramArray = arrlst.toArray( new ITaskAntEntity.ITaskParamEntity[ 0 ] );
			ant.setParam( paramArray );
		}
	}

	/**
	 * 「ant」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 *
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskAntEntity) )
			return null;

		TaskResultAntEntity resultAntEntity = new TaskResultAntEntity();
		String taskResultCode = TaskResultCode.SUCCESS;
		resultAntEntity.setSequence( prmTask.getSequence() );
		resultAntEntity.setTaskId( prmTask.getTaskId() );

		String outLog = null;
		String errLog = null;
		TaskAntEntity ant = (TaskAntEntity) prmTask;
		ActionAntExecutor antExecutor = null;
		try {
			substitutePath( ant );

			//build.xmlファイルの生成
			ActionAntBuildXmlMakerGeneral xmlMaker = new ActionAntBuildXmlMakerGeneral();

			xmlMaker.setOutputBuildXmlPath( ant.getBuildXmlPath() );
			xmlMaker.setTemplatePath( ant.getTemplatePath() );

			ITaskParamEntity[] paramEntityArray = ant.getParam();
			if( null != paramEntityArray ) {
				Properties map = new Properties();
				for( ITaskParamEntity paramEntity : paramEntityArray ) {
					map.setProperty( paramEntity.getKey() , paramEntity.getValue() );
				}
				xmlMaker.setParamMap( map );
			}
			if( xmlMaker.execute() ) { //build.xmlファイル生成
				Charset inCharset = ant.getCharsetInfo() != null ? Charset.value( ant.getCharsetInfo().getIn() ) : null;

				antExecutor = new ActionAntExecutor( inCharset );

				//build.xmlを参照してビルド
				antExecutor.execute( ant.getBuildXmlPath() , TriLogFactory.getInstance() );
			}
		} catch ( Exception e ) {
			taskResultCode = TaskResultCode.ERROR_PROCESS;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005006S , e );
		} finally {
			try {
				if ( antExecutor != null ) {
					outLog = antExecutor.getOutLog();
					errLog = antExecutor.getErrLog();
				}
				String outCharset = ant.getCharsetInfo() != null ? ant.getCharsetInfo().getOut() : null;
				resultAntEntity.setOutLog( TaskLogUtils.writeLog( ant.getOutLogPath() , outLog , Charset.value(outCharset) ) );
				resultAntEntity.setErrLog( TaskLogUtils.writeLog( ant.getErrLogPath() , errLog , Charset.value(outCharset) ) );
			} catch( Exception e ) {
				taskResultCode = TaskResultCode.ERROR_PROCESS;
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( BmMessageId.BM005006S , e );
			} finally {
				resultAntEntity.setCode( taskResultCode );
				prmTarget.addTaskResult( resultAntEntity );
			}
		}

		return resultAntEntity;
	}

}
