package jp.co.blueship.tri.bm.task.ant.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;

public class TaskAntEntity extends TaskTaskEntity implements ITaskAntEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String buildXmlPath = null ;
	private String templatePath = null ;
	private String outLogPath = null ;
	private String errLogPath = null ;
	private ICharsetInfoEntity charsetInfoEntity = null ;

	private ITaskParamEntity[] param = new ITaskParamEntity[0];

	public String getBuildXmlPath() {
		return buildXmlPath;
	}
	public void setBuildXmlPath(String buildXmlPath) {
		this.buildXmlPath = buildXmlPath;
	}
	public String getTemplatePath() {
		return templatePath;
	}
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
	public String getErrLogPath() {
		return errLogPath;
	}
	public void setErrLogPath(String errLogPath) {
		this.errLogPath = errLogPath;
	}
	public String getOutLogPath() {
		return outLogPath;
	}
	public void setOutLogPath(String outLogPath) {
		this.outLogPath = outLogPath;
	}
	public ITaskParamEntity[] getParam() {
		return param ;
	}
	public void setParam( ITaskParamEntity[] value ) {
		param = value ;
	}
	public ICharsetInfoEntity getCharsetInfo() {
		return charsetInfoEntity;
	}
	public void setCharsetInfo(ICharsetInfoEntity charsetInfoEntity) {
		this.charsetInfoEntity = charsetInfoEntity;
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String key = null ;
		private String value = null ;

		public String getKey() {
			return key ;
		}
		public void setKey( String value ) {
			key = value ;
		}
		public String getValue() {
			return value;
		}
		public void setValue(String value) {
			this.value = value;
		}
	}
}
