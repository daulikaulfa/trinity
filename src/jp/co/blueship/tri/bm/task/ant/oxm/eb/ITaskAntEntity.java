package jp.co.blueship.tri.bm.task.ant.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskAntEntity extends ITaskTaskTypeEntity {

	public String getBuildXmlPath() ;
	public void setBuildXmlPath( String value ) ;
	public String getTemplatePath() ;
	public void setTemplatePath( String value ) ;
	public String getOutLogPath() ;
	public void setOutLogPath( String value ) ;
	public String getErrLogPath() ;
	public void setErrLogPath( String value ) ;
	public ITaskParamEntity[] getParam();
	public void setParam( ITaskParamEntity[] value );

	public ICharsetInfoEntity getCharsetInfo() ;
	public void setCharsetInfo( ICharsetInfoEntity obj ) ;

	/**
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		public String getKey() ;
		public void setKey( String value ) ;
		public String getValue() ;
		public void setValue( String value ) ;
	}
}
