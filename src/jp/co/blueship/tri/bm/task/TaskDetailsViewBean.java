package jp.co.blueship.tri.bm.task;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.constants.TaskFlowStatus;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskDetailsViewBean {
	private List<TimelineHeader> headers = new ArrayList<TimelineHeader>();
	private List<Timeline> timeLines = new ArrayList<Timeline>();

	public List<TimelineHeader> getHeaders() {
		return headers;
	}
	public TaskDetailsViewBean setHeaders(List<TimelineHeader> headers) {
		this.headers = headers;
		return this;
	}

	public List<Timeline> getTimeLines() {
		return timeLines;
	}
	public TaskDetailsViewBean setTimeLines(List<Timeline> timeLines) {
		this.timeLines = timeLines;
		return this;
	}

	public class TimelineHeader {
		private String serverNm;
		private String osType;

		public String getServerNm() {
			return serverNm;
		}
		public TimelineHeader setServerNm(String serverNm) {
			this.serverNm = serverNm;
			return this;
		}

		public String getOsType() {
			return osType;
		}
		public TimelineHeader setOsType(String osType) {
			this.osType = osType;
			return this;
		}
	}

	public class Timeline {
		private int lineNo;
		private List<Task> tasks = new ArrayList<Task>();

		public int getLineNo() {
			return lineNo;
		}
		public Timeline setLineNo(int lineNo) {
			this.lineNo = lineNo;
			return this;
		}

		public List<Task> getTasks() {
			return tasks;
		}
		public Timeline setTasks(List<Task> tasks) {
			this.tasks = tasks;
			return this;
		}
	}

	public class Task {
		/**
		 * In the case of 'check-in request', set the PROC_CTG_CD of SM_PROC_DETAILS table.
		 */
		private String taskNm;
		private ProcessStatus result = ProcessStatus.none;
		private IProgressBar bar;
		/**
		 * BP/RP Only
		 */
		private String remainingTime;
		private TaskFlowStatus taskStatus = TaskFlowStatus.none;

		public String getTaskNm() {
			return taskNm;
		}
		public Task setTaskNm(String taskNm) {
			this.taskNm = taskNm;
			return this;
		}

		public boolean isActive() {
			return ProcessStatus.Active.equals( result );
		}
		public boolean isCompleted() {
			return ProcessStatus.Completed.equals( result );
		}
		public boolean isError() {
			return ProcessStatus.Error.equals( result );
		}
		public Task setResult(ProcessStatus result) {
			this.result = result;
			return this;
		}

		public IProgressBar getBar() {
			return bar;
		}
		public Task setBar(IProgressBar bar) {
			this.bar = bar;
			return this;
		}

		public String getRemainingTime() {
			return remainingTime;
		}
		public Task setRemainingTime(String remainingTime) {
			this.remainingTime = remainingTime;
			return this;
		}

		public boolean isExist() {
			return TaskFlowStatus.Exist.equals( taskStatus );
		}
		public boolean isArrow() {
			return TaskFlowStatus.Arrow.equals( taskStatus );
		}
		public boolean isEmpty() {
			return TaskFlowStatus.Empty.equals( taskStatus );
		}
		public boolean isBottom() {
			return TaskFlowStatus.Bottom.equals( taskStatus );
		}
		public Task setTaskStatus(TaskFlowStatus taskStatus) {
			this.taskStatus = taskStatus;
			return this;
		}
	}

}
