package jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskResultDeleteEntity;

public class TaskResultConcentrateExtEntity extends TaskResultConcentrateEntity implements ITaskResultConcentrateExtEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;
	
	private ITaskCopyEntity[] copy = new ITaskCopyEntity[0];
	private ITaskDeleteEntity[] delete = new ITaskDeleteEntity[0];

	public ITaskResultConcentrateExtEntity.ITaskDiffEntity newDiff() {
		return new TaskDiffEntity();
	}
	
	public ITaskResultConcentrateExtEntity.ITaskDiffEntity[] getDiff() {
		return (ITaskResultConcentrateExtEntity.ITaskDiffEntity[])super.getDiff();
	}
	public void setDiff( ITaskResultConcentrateExtEntity.ITaskDiffEntity[] diff ) {
		super.setDiff( diff );
	}

	public ITaskResultConcentrateExtEntity.ITaskCopyEntity newCopy() {
		return new TaskCopyEntity();
	}
	
	public ITaskResultConcentrateExtEntity.ITaskCopyEntity[] getCopy() {
		return copy;
	}
	public void setCopy( ITaskResultConcentrateExtEntity.ITaskCopyEntity[] copy ) {
		this.copy = copy;
	}

	public ITaskResultConcentrateExtEntity.ITaskDeleteEntity newDelete() {
		return new TaskDeleteEntity();
	}
	
	public ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] getDelete() {
		return delete;
	}
	public void setDelete( ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] delete ) {
		this.delete = delete;
	}
	
	/**
	 * 
	 * インナークラス
	 *
	 */
	public class TaskDiffEntity extends TaskResultConcentrateEntity.TaskDiffEntity implements ITaskResultConcentrateExtEntity.ITaskDiffEntity {
		
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;
		
		public ITaskResultConcentrateExtEntity.ITaskResultEntity[] getResult() {
			return (ITaskResultConcentrateExtEntity.ITaskResultEntity[])super.getResult();
		}
		public void setResult( ITaskResultConcentrateExtEntity.ITaskResultEntity[] result ) {
			super.setResult(result);
		}
		
		public ITaskResultConcentrateExtEntity.ITaskResultEntity newResult() {
			return new TaskResultConcentrateExtEntity.TaskResultEntity();
		}
	}
	
	/**
	 * 
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskResultConcentrateEntity.TaskResultEntity implements ITaskResultConcentrateExtEntity.ITaskResultEntity {
		
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;
	}
	
	/**
	 * 
	 * インナークラス
	 *
	 */
	public class TaskCopyEntity extends TaskResultCopyEntity implements ITaskResultConcentrateExtEntity.ITaskCopyEntity {
		
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String applyNo = null;
		
		public String getApplyNo() {
			return applyNo; 
		}
		public void setApplyNo( String value ) {
			applyNo = value;
		}
	}
	
	/**
	 * 
	 * インナークラス
	 *
	 */
	public class TaskDeleteEntity extends TaskResultDeleteEntity implements ITaskResultConcentrateExtEntity.ITaskDeleteEntity {
		
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String applyNo = null;
		
		public String getApplyNo() {
			return applyNo; 
		}
		public void setApplyNo( String value ) {
			applyNo = value;
		}
	}

}
