package jp.co.blueship.tri.bm.task.concentrateExt.oxm;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskResultConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskResultConcentrateExtEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.TaskResultConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.shun.ISort;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateExtResultType;
import jp.co.blueship.tri.fw.schema.beans.task.CopyResultType;
import jp.co.blueship.tri.fw.schema.beans.task.DeleteResultType;
import jp.co.blueship.tri.fw.schema.beans.task.DiffResultDetailType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * ConcentrateExtタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultConcentrateExtMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			ConcentrateExtResultType[] results = src.getConcentrateExtArray();

			if( null != results ) {
				for( ConcentrateExtResultType result : results ) {
					TaskResultConcentrateExtEntity resultEntity = new TaskResultConcentrateExtEntity();

					copyProperties( result , resultEntity );

					//集約結果（複写）
					{
						ConcentrateExtResultType.Copy[] copyArray = result.getCopyArray();
						if( null != copyArray ) {
							ArrayList<ITaskResultConcentrateExtEntity.ITaskCopyEntity> copyList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskCopyEntity>();

							for( ConcentrateExtResultType.Copy copy : copyArray ) {
								ITaskResultConcentrateExtEntity.ITaskCopyEntity copyEntity = resultEntity.newCopy();
								copyProperties( copy , copyEntity );

								CopyResultType.Result[] resultDetailArray = copy.getResultArray();
								if( null != resultDetailArray ) {

									ArrayList<ITaskResultCopyEntity.ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultCopyEntity.ITaskResultEntity>();

									for( CopyResultType.Result copyResult : resultDetailArray ) {
										ITaskResultCopyEntity.ITaskResultEntity taskResultEntity = copyEntity.newResult();
										copyProperties( copyResult , taskResultEntity );
										resultDetailList.add( taskResultEntity );
									}

									{
										ITaskResultCopyEntity.ITaskResultEntity[] sortResult = resultDetailList.toArray( new ITaskResultCopyEntity.ITaskResultEntity[ 0 ] );
										copyEntity.setResult( (ITaskResultCopyEntity.ITaskResultEntity[])sortBySequence(sortResult, ISort.ASC) );
									}
								}
								copyList.add( copyEntity );
							}

							{
								ITaskResultConcentrateExtEntity.ITaskCopyEntity[] sortResult = copyList.toArray( new ITaskResultConcentrateExtEntity.ITaskCopyEntity[ 0 ] );
								resultEntity.setCopy( (ITaskResultConcentrateExtEntity.ITaskCopyEntity[])sortBySequence(sortResult, ISort.ASC) );
							}
						}
					}

					//集約結果（削除）
					{
						ConcentrateExtResultType.Delete[] deleteArray = result.getDeleteArray();
						if( null != deleteArray ) {
							ArrayList<ITaskResultConcentrateExtEntity.ITaskDeleteEntity> deleteList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskDeleteEntity>();

							for( ConcentrateExtResultType.Delete delete : deleteArray ) {
								ITaskResultConcentrateExtEntity.ITaskDeleteEntity deleteEntity = resultEntity.newDelete();
								copyProperties( delete , deleteEntity );

								DeleteResultType.Result[] resultDetailArray = delete.getResultArray();
								if( null != resultDetailArray ) {

									ArrayList<ITaskResultDeleteEntity.ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultDeleteEntity.ITaskResultEntity>();

									for( DeleteResultType.Result deleteResult : resultDetailArray ) {
										ITaskResultDeleteEntity.ITaskResultEntity taskResultEntity = deleteEntity.newResult();
										copyProperties( deleteResult , taskResultEntity );
										resultDetailList.add( taskResultEntity );
									}

									{
										ITaskResultDeleteEntity.ITaskResultEntity[] sortResult = resultDetailList.toArray( new ITaskResultDeleteEntity.ITaskResultEntity[ 0 ] );
										deleteEntity.setResult( (ITaskResultDeleteEntity.ITaskResultEntity[])sortBySequence(sortResult, ISort.ASC) );
									}
								}
								deleteList.add( deleteEntity );
							}

							{
								ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] sortResult = deleteList.toArray( new ITaskResultConcentrateExtEntity.ITaskDeleteEntity[ 0 ] );
								resultEntity.setDelete( (ITaskResultConcentrateExtEntity.ITaskDeleteEntity[])sortBySequence(sortResult, ISort.ASC) );
							}
						}
					}

					//差分結果
					{
						ConcentrateExtResultType.Diff[] diffArray = result.getDiffArray();
						if( null != diffArray ) {
							ArrayList<ITaskResultConcentrateExtEntity.ITaskDiffEntity> diffList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskDiffEntity>();

							for( ConcentrateExtResultType.Diff diff : diffArray ) {
								ITaskResultConcentrateExtEntity.ITaskDiffEntity diffEntity = resultEntity.newDiff();
								copyProperties( diff , diffEntity );

								DiffResultDetailType[] resultDetailArray = diff.getResultArray();
								if( null != resultDetailArray ) {

									ArrayList<ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultEntity>();

									for( DiffResultDetailType diffResult : resultDetailArray ) {
										ITaskResultConcentrateExtEntity.ITaskResultEntity taskResultEntity =  diffEntity.newResult();
										copyProperties( diffResult , taskResultEntity );
										resultDetailList.add( taskResultEntity );
									}

									{
										ITaskResultEntity[] sortResult = resultDetailList.toArray( new ITaskResultEntity[ 0 ] );
										diffEntity.setResult( (ITaskResultEntity[])sortBySequence(sortResult, ISort.ASC) );
									}
								}
								diffList.add( diffEntity );
							}

							{
								ITaskResultConcentrateExtEntity.ITaskDiffEntity[] sortResult = diffList.toArray( new ITaskResultConcentrateExtEntity.ITaskDiffEntity[ 0 ] );
								resultEntity.setDiff( (ITaskResultConcentrateExtEntity.ITaskDiffEntity[])sortBySequence(sortResult, ISort.ASC) );
							}
						}
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultConcentrateExtEntity) )
					continue;

				TaskResultConcentrateExtEntity resultEntity = (TaskResultConcentrateExtEntity)entity;

				ConcentrateExtResultType result = dest.addNewConcentrateExt();

				copyProperties( resultEntity, result );

				//集約結果（複写）
				{
					ITaskResultConcentrateExtEntity.ITaskCopyEntity[] taskEntityArray = resultEntity.getCopy();
					if( null != taskEntityArray ) {
						for( ITaskResultConcentrateExtEntity.ITaskCopyEntity taskEntity : taskEntityArray ) {
							ConcentrateExtResultType.Copy copy = result.addNewCopy();
							copyProperties( taskEntity , copy );

							ITaskResultCopyEntity.ITaskResultEntity[] taskResultEntityArray = taskEntity.getResult();
							if( null != taskResultEntityArray ) {
								for( ITaskResultCopyEntity.ITaskResultEntity taskResultEntity : taskResultEntityArray ) {
									CopyResultType.Result resultDetailType = copy.addNewResult();
									copyProperties( taskResultEntity , resultDetailType );
								}
							}
						}
					}
				}

				//集約結果（削除）
				{
					ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] taskEntityArray = resultEntity.getDelete();
					if( null != taskEntityArray ) {
						for( ITaskResultConcentrateExtEntity.ITaskDeleteEntity taskEntity : taskEntityArray ) {
							ConcentrateExtResultType.Delete delete = result.addNewDelete();
							copyProperties( taskEntity , delete );

							ITaskResultDeleteEntity.ITaskResultEntity[] taskResultEntityArray = taskEntity.getResult();
							if( null != taskResultEntityArray ) {
								for( ITaskResultDeleteEntity.ITaskResultEntity taskResultEntity : taskResultEntityArray ) {
									DeleteResultType.Result resultDetailType = delete.addNewResult();
									copyProperties( taskResultEntity , resultDetailType );
								}
							}
						}
					}
				}

				//差分結果
				{
					ITaskResultConcentrateExtEntity.ITaskDiffEntity[] taskDiffEntityArray = resultEntity.getDiff();
					if( null != taskDiffEntityArray ) {
						for( ITaskResultConcentrateExtEntity.ITaskDiffEntity taskDiffEntity : taskDiffEntityArray ) {
							ConcentrateExtResultType.Diff diff = result.addNewDiff();
							copyProperties( taskDiffEntity , diff );

							ITaskResultConcentrateExtEntity.ITaskResultEntity[] taskResultEntityArray = taskDiffEntity.getResult();
							if( null != taskResultEntityArray ) {
								for( ITaskResultEntity taskResultEntity : taskResultEntityArray ) {
									DiffResultDetailType diffResultDetailType = diff.addNewResult();
									copyProperties( taskResultEntity , diffResultDetailType );
								}
							}
						}
					}
				}
			}
		}

		return dest;
	}

	/**
	 * タスク実行結果をシーケンス順でソートします。
	 *
	 * @param entities エンティティ情報の配列
	 * @param orderBy {@see ISort.ASC} または{@see ISort.DESC}
	 */
	public ITaskResultTypeEntity[] sortBySequence( ITaskResultTypeEntity[] entities, final String orderBy ) {
		Arrays.sort(entities,
				new Comparator<ITaskResultTypeEntity>() {
					public int compare(ITaskResultTypeEntity param1, ITaskResultTypeEntity param2) {
						Integer paramSeq1 = (TriStringUtils.isDigits( param1.getSequence() ))? Integer.valueOf(param1.getSequence()) : 0;
						Integer paramSeq2 = (TriStringUtils.isDigits( param2.getSequence() ))? Integer.valueOf(param2.getSequence()) : 0;

						return paramSeq1.compareTo(paramSeq2);
					}
				});

		return entities;
	}

}
