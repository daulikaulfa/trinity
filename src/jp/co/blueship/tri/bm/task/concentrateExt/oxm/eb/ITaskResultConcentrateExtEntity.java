package jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskResultConcentrateEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity;

public interface ITaskResultConcentrateExtEntity extends ITaskResultConcentrateEntity {
	
	/**
	 * 返却資産と作業フォルダとの資産の比較結果を取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskDiffEntity[] getDiff();
	/**
	 * 返却資産と作業フォルダとの資産の比較結果を設定します。
	 * 
	 * @param diff 比較結果
	 */
	public void setDiff( ITaskResultConcentrateExtEntity.ITaskDiffEntity[] diff );

	/**
	 * 差分比較情報の新しいインスタンスを生成します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskDiffEntity newDiff();
	
	/**
	 * 集約結果を取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskCopyEntity[] getCopy();
	/**
	 * 集約結果を設定します。
	 * 
	 * @param copy 複写結果
	 */
	public void setCopy( ITaskResultConcentrateExtEntity.ITaskCopyEntity[] copy );

	/**
	 * 集約結果情報の新しいインスタンスを生成します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskCopyEntity newCopy();
	
	/**
	 * 集約結果を取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] getDelete();
	/**
	 * 集約結果を設定します。
	 * 
	 * @param delete 削除結果
	 */
	public void setDelete( ITaskResultConcentrateExtEntity.ITaskDeleteEntity[] delete );

	/**
	 * 集約結果情報の新しいインスタンスを生成します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultConcentrateExtEntity.ITaskDeleteEntity newDelete();
	
	/**
	 * 
	 * 返却資産と作業フォルダとの資産の比較結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskDiffEntity extends ITaskResultConcentrateEntity.ITaskDiffEntity {
		
		/**
		 * 差分比較結果を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public ITaskResultConcentrateExtEntity.ITaskResultEntity[] getResult();
		/**
		 * 差分比較結果を設定します。
		 * 
		 * @param result 差分比較結果
		 */
		public void setResult( ITaskResultConcentrateExtEntity.ITaskResultEntity[] result );

		/**
		 * 結果情報の新しいインスタンスを生成します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public ITaskResultConcentrateExtEntity.ITaskResultEntity newResult();
	}
	
	/**
	 * 
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultConcentrateEntity.ITaskResultEntity {
	}
	
	/**
	 * 
	 * 集約結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskCopyEntity extends ITaskResultCopyEntity {
		
		/**
		 * 
		 * 申請番号を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public String getApplyNo();
		/**
		 * 申請番号を設定します。
		 * 
		 * @param value 申請番号
		 */
		public void setApplyNo( String value );
		
	}
	
	/**
	 * 
	 * 集約結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskDeleteEntity extends ITaskResultDeleteEntity {
		
		/**
		 * 
		 * 申請番号を取得します。
		 * 
		 * @return 取得した情報を戻します。
		 */
		public String getApplyNo();
		/**
		 * 申請番号を設定します。
		 * 
		 * @param value 申請番号
		 */
		public void setApplyNo( String value );
		
	}

}
