package jp.co.blueship.tri.bm.task.concentrateExt;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTargetEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskConcentrateExtEntity.ITaskParamEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskResultConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskResultConcentrateExtEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.TaskConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.TaskResultConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.copy.TaskProcCopy;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskFilesetEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskOverwriteEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskCopyEntity;
import jp.co.blueship.tri.bm.task.delete.TaskProcDelete;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskDeleteEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.ITaskResultDeleteEntity;
import jp.co.blueship.tri.bm.task.delete.oxm.eb.TaskDeleteEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.FileCopy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverride;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverridePreserveLastModified;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「concentrateExt」タスクの処理内容を記述します
 * <p>
 * ビルドパッケージを作成する際、返却／削除申請された資産ファイルを原本と比較し、
 * 今回分の最新資産の集約を行う。
 *
 */
public class TaskProcConcentrateExt implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;
	private TaskProcCopy taskProcCopy = null;
	private TaskProcDelete taskProcDelete = null;

	public void setSupport(TaskFinderSupport support) {
		this.support = support;
	}
	public void setTaskProcCopy(TaskProcCopy taskProcCopy) {
		this.taskProcCopy = taskProcCopy;
	}
	public void setTaskProcDelete(TaskProcDelete taskProcDelete) {
		this.taskProcDelete = taskProcDelete;
	}
	public ITaskPropertyEntity[] getPropertyEntity() {
		return propertyEntity;
	}
	public void setPropertyEntity(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * <br>","で区切られた複数パラメータに対応

	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public final void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskConcentrateExtEntity concentrate = (TaskConcentrateExtEntity) task;

		concentrate.setConcentrateDir( TaskStringUtils.substitutionPath( propertyEntity , concentrate.getConcentrateDir() ) );
		concentrate.setDiffNewFilePath( TaskStringUtils.substitutionPath( propertyEntity, concentrate.getDiffNewFilePath() ) );
		concentrate.setDiffModifiedFilePath( TaskStringUtils.substitutionPath( propertyEntity, concentrate.getDiffModifiedFilePath() ) );
		concentrate.setDiffDeleteFilePath( TaskStringUtils.substitutionPath( propertyEntity, concentrate.getDiffDeleteFilePath() ) );

		ITaskParamEntity[] paramArray = concentrate.getParam();
		if( null != paramArray ) {
			Set<ITaskParamEntity> arrset = new HashSet<ITaskParamEntity>();
			for( ITaskParamEntity param : paramArray ) {
				String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , param.getApplyNo() );
				if( null != arrays ) {
					for( String array : arrays ) {
						ITaskParamEntity entity = concentrate.new TaskParamEntity();
						TriPropertyUtils.copyProperties( entity, param );
						entity.setApplyNo( array );
						arrset.add( entity );
					}
				}
			}
			paramArray = arrset.toArray( new ITaskParamEntity[ 0 ] );
			concentrate.setParam( paramArray );
		}
	}

	/**
	 * 集約タスクに指定されたパラメタのチェックを行う
	 *
	 * @param concentrate 集約タスク
	 * @throws Exception
	 */
	private final void checkParam( ITaskConcentrateExtEntity concentrate ) throws Exception {
		String concentrateDir = concentrate.getConcentrateDir();
		String diffNewFilePath = concentrate.getDiffNewFilePath();
		String diffModifiedFilePath = concentrate.getDiffModifiedFilePath();
		String DiffDeleteFilePath = concentrate.getDiffDeleteFilePath();

		if ( TriStringUtils.isEmpty( concentrateDir ) ) {
			throw new TriSystemException( BmMessageId.BM004016F );
		}

		if ( StatusFlg.on.value().equals( concentrate.getDoDiffCopy() ) ) {
			if ( TriStringUtils.isEmpty( diffNewFilePath ) ) {
				throw new TriSystemException( BmMessageId.BM004017F );
			}

			if ( TriStringUtils.isEmpty( diffModifiedFilePath ) ) {
				throw new TriSystemException( BmMessageId.BM004018F );
			}

			if ( TriStringUtils.isEmpty( DiffDeleteFilePath ) ) {
				throw new TriSystemException( BmMessageId.BM004019F );
			}
		}
	}

	/**
	 * 「concentrate」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */

	public final ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {

		if ( ! (prmTask instanceof ITaskConcentrateExtEntity) )
			return null;

		//実行結果格納領域を確保
		ITaskResultConcentrateExtEntity resultConcentrateEntity = new TaskResultConcentrateExtEntity();
		List<ITaskResultConcentrateExtEntity.ITaskCopyEntity> copyList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskCopyEntity>();
		List<ITaskResultConcentrateExtEntity.ITaskDeleteEntity> deleteList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskDeleteEntity>();
		List<ITaskResultConcentrateExtEntity.ITaskDiffEntity> diffList = new ArrayList<ITaskResultConcentrateExtEntity.ITaskDiffEntity>();

		resultConcentrateEntity.setCode( TaskResultCode.UN_PROCESS );
		resultConcentrateEntity.setSequence( prmTask.getSequence() );
		resultConcentrateEntity.setTaskId( prmTask.getTaskId() );

		Integer sequence = 1;

		try {
			substitutePath( prmTask );

			ITaskConcentrateExtEntity concentrate = (ITaskConcentrateExtEntity) prmTask;
			String[] applyNoArray = convertPathArray( concentrate.getParam() );
			if( null == applyNoArray || 0 == applyNoArray.length ) {
				throw new BusinessException( BmMessageId.BM004074F );
			}

			this.checkParam( concentrate );

			List<IAreqDto> applyEntityArray = this.getAssetApplyList( applyNoArray );

			String masterWorkPath = null;

			List<Object> paramList = new ArrayList<Object>();

			if ( 0 < applyEntityArray.size() ) {
				IAreqEntity assetApplyEntity = applyEntityArray.get(0).getAreqEntity();
				IPjtEntity pjtEntity = support.getAmFinderSupport().findPjtEntity( assetApplyEntity.getPjtId() );

				ILotDto lotDto = support.getAmFinderSupport().findLotDto( pjtEntity.getLotId() );
				masterWorkPath = TriStringUtils.convertPath( BmDesignBusinessRuleUtils.getMasterWorkPath( lotDto.getLotEntity() ) );
				paramList.add( lotDto );

				if ( StatusFlg.on.value().equals( concentrate.getDoConcentrateCopy() ) ) {
					String concentratePath = TriStringUtils.convertPath( concentrate.getConcentrateDir() );

					ITaskResultConcentrateExtEntity.ITaskCopyEntity copyEntity = resultConcentrateEntity.newCopy();
					copyEntity.setSequence( String.valueOf(sequence++) );
					copyList.add( copyEntity );

					this.innerTaskCopy( concentrate, copyEntity, masterWorkPath, concentratePath );
				}
			}

			boolean isExistAsset = false;

			//集約ディレクトリに申請資産を集約
			for( IAreqDto applyEntity : applyEntityArray ) {
				IAreqEntity assetApplyEntity = applyEntity.getAreqEntity();

				isExistAsset |= TriStringUtils.isNotEmpty(applyEntity.getAreqFileEntities(AreqCtgCd.ReturningRequest));
				isExistAsset |= TriStringUtils.isNotEmpty(applyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest));

				if ( StatusFlg.on.value().equals( concentrate.getDoConcentrateCopy() ) ) {
					ITaskResultConcentrateExtEntity.ITaskCopyEntity copyResultEntity = resultConcentrateEntity.newCopy();
					ITaskResultConcentrateExtEntity.ITaskDeleteEntity deleteResultEntity = resultConcentrateEntity.newDelete();

					if ( BmBusinessJudgUtils.isReturnApply( assetApplyEntity.getAreqCtgCd() ) ) {
						copyResultEntity.setSequence( String.valueOf(sequence++) );
						copyResultEntity.setApplyNo( assetApplyEntity.getAreqId() );
						copyList.add( copyResultEntity );
					}

					if ( BmBusinessJudgUtils.isDeleteApply( assetApplyEntity.getAreqCtgCd() ) ) {
						deleteResultEntity.setSequence( String.valueOf(sequence++) );
						deleteResultEntity.setApplyNo( assetApplyEntity.getAreqId() );
						deleteList.add( deleteResultEntity );
					}

					this.concentrateAssetApply(concentrate, copyResultEntity, deleteResultEntity, applyEntity, masterWorkPath, paramList);
				}
			}

			if( true != isExistAsset ) {//返却資産なし
				throw new BusinessException( BmMessageId.BM004074F );
			}

			//汎用DIFFタスクで行うとパフォーマンスが劣化するためconcentrate内でDIFF機能を肩代わりする
			boolean holdTimestamp = ( StatusFlg.on.value().toString().equals( concentrate.getHoldTimestamp() ) ) ? true : false;
			FileCopy fileCopy = null;
			if ( holdTimestamp ) {
				fileCopy = new FileCopyOverridePreserveLastModified();
			} else {
				fileCopy = new FileCopyOverride();
			}

			for( IAreqDto applyEntity : applyEntityArray ) {
				IAreqEntity assetApplyEntity = applyEntity.getAreqEntity();

				List<ITaskResultEntity> resultDetailList = new ArrayList<ITaskResultEntity>();

				ITaskResultConcentrateExtEntity.ITaskDiffEntity diffEntity = resultConcentrateEntity.newDiff();
				diffEntity.setCode( TaskResultCode.UN_PROCESS );
				diffEntity.setSequence( String.valueOf(sequence++) );
				diffEntity.setApplyNo( assetApplyEntity.getAreqId() );

				try {

					this.diff(concentrate, applyEntity, diffEntity, resultDetailList, masterWorkPath, paramList, fileCopy);

					diffEntity.setCode( TaskResultCode.SUCCESS );

				} catch ( Exception e ) {
					diffEntity.setCode( TaskResultCode.ERROR_PROCESS );
					throw e;
				} finally {
					diffEntity.setResult( resultDetailList.toArray( new ITaskResultEntity[0] ) );
					diffList.add( diffEntity );
				}
			}

			resultConcentrateEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultConcentrateEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005014S , e );

		} finally {
			resultConcentrateEntity.setCopy( copyList.toArray( new ITaskResultConcentrateExtEntity.ITaskCopyEntity[ 0 ] ) );
			resultConcentrateEntity.setDelete( deleteList.toArray( new ITaskResultConcentrateExtEntity.ITaskDeleteEntity[ 0 ] ) );
			resultConcentrateEntity.setDiff( diffList.toArray( new ITaskResultConcentrateExtEntity.ITaskDiffEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultConcentrateEntity );
		}

		return resultConcentrateEntity;
	}

	/**
	 * 集約ディレクトリに申請資産を集約する。
	 *
	 * @param concentrate 集約タスク
	 * @param copySequence 結果シーケンス
	 * @param copyResult 複写タスクの実行結果
	 * @param delResult 削除タスクの実行結果
	 * @param assetApplyEntity 対象申請情報
	 * @param masterWorkPath 差分の比較先
	 * @param paramList Action用のPOJO
	 * @throws Exception
	 */
	private final void concentrateAssetApply(
								ITaskConcentrateExtEntity concentrate,
								ITaskResultConcentrateExtEntity.ITaskCopyEntity copyResult,
								ITaskResultConcentrateExtEntity.ITaskDeleteEntity delResult,
								IAreqDto assetApplyEntity,
								String masterWorkPath,
								List<Object> paramList )
		throws Exception {

		//返却資産
		if ( BmBusinessJudgUtils.isReturnApply( assetApplyEntity.getAreqEntity().getAreqCtgCd() ) ) {
			String returnAssetPath = TriStringUtils.convertPath( BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, assetApplyEntity.getAreqEntity() ) );
			String concentratePath = TriStringUtils.convertPath( concentrate.getConcentrateDir() );

			this.innerTaskCopy( concentrate, copyResult, returnAssetPath, concentratePath );
		}

		//削除資産
		if ( BmBusinessJudgUtils.isDeleteApply( assetApplyEntity.getAreqEntity().getAreqCtgCd() ) ) {
			this.innerTaskDelete( concentrate, delResult, assetApplyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest) );
		}
	}

	/**
	 * 集約のための差分比較を行う。
	 *
	 * @param concentrate 集約タスク
	 * @param assetApplyEntity 対象申請情報
	 * @param diffEntity 処理結果
	 * @param resultDetailList 処理結果（詳細）リスト
	 * @param masterWorkPath 差分の比較先
	 * @param paramList Action用のPOJO
	 * @param fileCopy ファイルコピー部品
	 * @throws Exception
	 */
	private final void diff(	ITaskConcentrateExtEntity concentrate,
								IAreqDto assetApplyEntity,
								ITaskResultConcentrateExtEntity.ITaskDiffEntity diffEntity,
								List<ITaskResultEntity> resultDetailList,
								String masterWorkPath,
								List<Object> paramList,
								FileCopy fileCopy )
		throws Exception {

		Integer innerResultSequence = 1;

		//返却資産
		if ( BmBusinessJudgUtils.isReturnApply( assetApplyEntity.getAreqEntity().getAreqCtgCd() ) ) {

			String returnAssetPath = BmDesignBusinessRuleUtils.getReturnAssetApplyNoPath( paramList, assetApplyEntity.getAreqEntity() ).getPath();

			List<IAreqFileEntity> fileEntityArray = assetApplyEntity.getAreqFileEntities(AreqCtgCd.ReturningRequest);

			for( IAreqFileEntity fileEntity : fileEntityArray ) {
				String path = fileEntity.getFilePath();

				ITaskResultEntity innerResult = diffEntity.newResult();
				innerResult.setSequence( String.valueOf(innerResultSequence++) );
				innerResult.setRelativePath( path );

				File asset = new File( TriStringUtils.linkPathBySlash( masterWorkPath, path ) );

				//ファイル及び読み込み可能チェックは、集約ディレクトリに複写時にチェック済みのため、チェックしない
				try {
					if ( asset.exists() ) {
						innerResult.setCode( Status.CHANGE.toString() );
						this.copyFile(concentrate, returnAssetPath, concentrate.getDiffModifiedFilePath(), path, fileCopy);
					} else {
						innerResult.setCode( Status.ADD.toString() );
						this.copyFile(concentrate, returnAssetPath, concentrate.getDiffNewFilePath(), path, fileCopy);
					}

				} catch ( Exception e ) {
					innerResult.setCode( Status.ERROR.toString() );
					throw e;
				} finally {
					resultDetailList.add( innerResult );
				}
			}
		}

		//削除資産
		if ( BmBusinessJudgUtils.isDeleteApply( assetApplyEntity.getAreqEntity().getAreqCtgCd() ) ) {

			List<IAreqFileEntity> fileEntityArray = assetApplyEntity.getAreqFileEntities(AreqCtgCd.RemovalRequest);

			for( IAreqFileEntity fileEntity : fileEntityArray ) {
				String path = fileEntity.getFilePath();

				ITaskResultEntity innerResult = diffEntity.newResult();

				innerResult.setSequence( String.valueOf(innerResultSequence++) );
				innerResult.setCode( Status.DELETE.toString() );
				innerResult.setRelativePath( path );

				try {
					this.copyFile(concentrate, masterWorkPath, concentrate.getDiffDeleteFilePath(), path, fileCopy);

				} catch ( Exception e ) {
					innerResult.setCode( Status.ERROR.toString() );
					throw e;
				} finally {
					resultDetailList.add( innerResult );
				}
			}
		}
	}

	/**
	 * 指定されたファイルの複写を行う。
	 *
	 * @param concentrate 集約タスク
	 * @param srcPath 複写元のフォルダパス
	 * @param destPath 複写先のフォルダパス
	 * @param targetFilePath 複写対象のファイルパス
	 * @param fileCopy ファイルコピー部品
	 * @throws IOException
	 */
	private final void copyFile(
			ITaskConcentrateExtEntity concentrate,
			String srcPath,
			String destPath,
			String targetFilePath,
			FileCopy fileCopy ) throws Exception {

		if ( ! StatusFlg.on.value().equals( concentrate.getDoDiffCopy() ) ) {
			return;
		}

		File srcFile = new File( TriStringUtils.linkPathBySlash( srcPath, targetFilePath ) );
		File destFile = new File( TriStringUtils.linkPathBySlash( destPath, targetFilePath ) );
		File parentFile = new File( destFile.getParent() );
		parentFile.mkdirs();

		fileCopy.copyFileToFile( srcFile, destFile, new ArrayList<File>() );
	}

	/**
	 * 申請番号のリストをもとに、IAreqEntityを検索する。
	 * <br>
	 * @param applyNoArray 申請番号のリスト
	 * @return 該当レコードを返す
	 */
	private final List<IAreqDto> getAssetApplyList( String[] applyNoArray ) throws Exception {

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
		AreqCondition condition = new AreqCondition();

		condition.setAreqIds( applyNoArray );

		//RUクローズされた申請は集約対象外とする。
		condition.setStsIdsNotEquals( new String[]{ AmAreqStatusId.BuildPackageClosed.getStatusId() } );

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.reqTimestamp, TriSortOrder.Asc, 1);

		List<IAreqEntity> areqEntities =
				this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 ).getEntities();

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}

		return areqDtoEntities;
	}

	/**
	 * ITaskParamEntityオブジェクトの配列からのapplyNo文字列のみを配列に格納して返す
	 * <br>
	 * @param ITaskParamEntityオブジェクト
	 * @return applyNo文字列の配列
	 */
	private final String[] convertPathArray( ITaskParamEntity[] paramEntityArray ) throws Exception {
		List<String> list = new ArrayList<String>();
		if( null != paramEntityArray ) {
			for( ITaskParamEntity paramEntity : paramEntityArray ) {
				list.add( paramEntity.getApplyNo() );
			}
		}
		return list.toArray( new String[ 0 ] );
	}

	/**
	 * 内部的にコピータスクを処理する。
	 *
	 * @param concentrate 集約エンティティ
	 * @param copySequence 結果シーケンス
	 * @param result 処理結果
	 * @param src コピー元
	 * @param dest コピー先
	 * @throws Exception
	 */
	private final void innerTaskCopy(
			ITaskConcentrateExtEntity concentrate,
			ITaskResultConcentrateExtEntity.ITaskCopyEntity result,
			String src,
			String dest ) throws Exception {

		ITaskTargetEntity prmTarget = new TaskTargetEntity();
		TaskCopyEntity copy = new TaskCopyEntity();

		copy.setHoldTimestamp( concentrate.getHoldTimestamp() );
		copy.setDoNeglect( StatusFlg.on.value() );
		copy.setDoNotExistSkip( StatusFlg.on.value() );
		copy.setDoDetailSnap( concentrate.getDoDetailSnap() );

		ITaskFilesetEntity fileset = copy.new TaskFilesetEntity();
		fileset.setSrc( src );
		fileset.setDest( dest );
		copy.setFileset( new ITaskFilesetEntity[] { fileset } );

		ITaskOverwriteEntity overwrite = copy.new TaskOverwriteEntity();
		overwrite.setDoAdd( StatusFlg.on.value() );
		overwrite.setDoUpdateNew( StatusFlg.on.value());
		overwrite.setDoUpdateOld( StatusFlg.on.value());
		copy.setOverwrite( overwrite );

		ITaskResultTypeEntity[] taskResults = null;
		try {
			taskProcCopy.execute( prmTarget , copy );
		} finally {
			taskResults = prmTarget.getTaskResult();

			if ( 0 < taskResults.length ) {
				result.setCode( taskResults[0].getCode() );
				((ITaskResultCopyEntity)result).setResult( ((ITaskResultCopyEntity)taskResults[0]).getResult() );
			}

		}
	}

	/**
	 * 内部的に削除タスクを処理する。
	 *
	 * @param concentrate 集約エンティティ
	 * @param copySequence 結果シーケンス
	 * @param result 処理結果
	 * @param fileEntityArray 削除対象ファイル
	 * @throws Exception
	 */
	private final void innerTaskDelete(
			ITaskConcentrateExtEntity concentrate,
			ITaskResultConcentrateExtEntity.ITaskDeleteEntity result,
			List<IAreqFileEntity> fileEntityArray ) throws Exception {

		ITaskTargetEntity prmTarget = new TaskTargetEntity();
		TaskDeleteEntity delete = new TaskDeleteEntity();

		delete.setDoNotExistSkip( StatusFlg.on.value() );
		delete.setDoDetailSnap( concentrate.getDoDeleteDetailSnap());

		List<ITaskDeleteEntity.ITaskParamEntity> paramList = new ArrayList<ITaskDeleteEntity.ITaskParamEntity>();

		if ( ! TriStringUtils.isEmpty( fileEntityArray ) ) {
			for ( IAreqFileEntity fileEntity: fileEntityArray ) {
				ITaskDeleteEntity.ITaskParamEntity param = delete.new TaskParamEntity();
				param.setSrc( TriStringUtils.linkPathBySlash( concentrate.getConcentrateDir() , fileEntity.getFilePath() ) );
				paramList.add( param );
			}
		}

		delete.setParam( paramList.toArray( new ITaskDeleteEntity.ITaskParamEntity[0] ) );

		ITaskResultTypeEntity[] taskResults = null;
		try {
			taskProcDelete.execute( prmTarget , delete );
		} finally {
			taskResults = prmTarget.getTaskResult();

			if ( 0 < taskResults.length ) {
				result.setCode( taskResults[0].getCode() );
				((ITaskResultDeleteEntity)result).setResult( ((ITaskResultDeleteEntity)taskResults[0]).getResult() );
			}

		}
	}

}
