package jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.ITaskConcentrateEntity;

/**
 * タスクオーダー（拡張）情報エンティティのインタフェースです。
 *
 */
public interface ITaskConcentrateExtEntity extends ITaskConcentrateEntity {
	
	/**
	 * 原本資産を集約ディレクトリにコピーするかどうかを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDoConcentrateCopy() ;
	/**
	 * 原本資産を集約ディレクトリにコピーするかどうかを設定します。
	 * 
	 * @param value 原本からファイルをコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外はコピーされない。
	 */
	public void setDoConcentrateCopy( String value ) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 * 
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDoDeleteDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 * 
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDeleteDetailSnap( String value );
	
	/**
	 * 申請資産の中から原本に存在しない新規資産を格納するパスを指定します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDiffNewFilePath() ;
	/**
	 * 申請資産の中から原本に存在しない新規資産を格納するパスを指定します。
	 * 
	 * @param value 追加時のコピー先パス
	 */
	public void setDiffNewFilePath( String value ) ;
	
	/**
	 * 申請資産の中から原本から変更のあった資産を格納するパスを指定します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDiffModifiedFilePath() ;
	/**
	 * 申請資産の中から原本から変更のあった資産を格納するパスを指定します。
	 * 
	 * @param value 変更時のコピー先パス
	 */
	public void setDiffModifiedFilePath( String value ) ;
	
	/**
	 * 申請資産の中から原本から削除される資産を格納するパスを指定します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDiffDeleteFilePath() ;
	/**
	 * 申請資産の中から原本から削除される資産を格納するパスを指定します。
	 * 
	 * @param value 削除時のコピー先パス
	 */
	public void setDiffDeleteFilePath( String value ) ;
	
	/**
	 * 申請資産を新規／変更／削除ごとに格納先を分けて、差分ファイルをコピーするかどうかを取得する。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getDoDiffCopy() ;
	/**
	 * 申請資産を新規／変更／削除ごとに格納先を分けて、差分ファイルをコピーするかどうかを取得する。
	 * 
	 * @param value 差分ファイルをコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外はコピーされない。
	 */
	public void setDoDiffCopy( String value ) ;
	
	/**
	 * コピー元のタイプムスタンプを保持するどうかを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public String getHoldTimestamp();
	/**
	 * コピー元のタイムスタンプを保持するかどうかを設定します。
	 * 
	 * @param value タイムスタンプを保持してコピーする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setHoldTimestamp( String value );
	
	/**
	 * 資産を集約するためのパラメタ情報を取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ITaskConcentrateExtEntity.ITaskParamEntity[] getParam() ;
	/**
	 * 資産を集約するためのパラメタ情報を設定します。
	 * 
	 * @param param 資産を集約するためのパラメタ情報
	 */
	public void setParam( ITaskConcentrateExtEntity.ITaskParamEntity[] param ) ;
	
	/**
	 * 
	 * 資産を集約するためのパラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends ITaskConcentrateEntity.ITaskParamEntity {
	}
	
}
