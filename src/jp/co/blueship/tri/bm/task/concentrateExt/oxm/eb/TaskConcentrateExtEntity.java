package jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb;

import jp.co.blueship.tri.bm.task.concentrate.oxm.eb.TaskConcentrateEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;


/**
 * タスクオーダー（拡張）情報エンティティのインタフェースです。
 *
 */
public class TaskConcentrateExtEntity extends TaskConcentrateEntity implements ITaskConcentrateExtEntity {
	
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;
	
	private String doConcentrateCopy = StatusFlg.off.value();
	private String diffNewFilePath = null ;
	private String diffModifiedFilePath = null ;
	private String diffDeleteFilePath = null ;
	private String doDiffCopy = StatusFlg.off.value();
	private String holdTimestamp = StatusFlg.off.value();
	private String doDetailSnap = StatusFlg.off.value();
	private String doDeleteDetailSnap = StatusFlg.off.value();

	public ITaskConcentrateExtEntity.ITaskParamEntity[] getParam() {
		return (ITaskConcentrateExtEntity.ITaskParamEntity[])super.getParam();
	}
	public void setParam(ITaskConcentrateExtEntity.ITaskParamEntity[] param) {
		super.setParam(param);
	}
	
	public String getDiffDeleteFilePath() {
		return diffDeleteFilePath;
	}
	
	public void setDiffDeleteFilePath(String diffDeleteFilePath) {
		this.diffDeleteFilePath = diffDeleteFilePath;
	}
	
	public String getDiffModifiedFilePath() {
		return diffModifiedFilePath;
	}
	
	public void setDiffModifiedFilePath(String diffModifiedFilePath) {
		this.diffModifiedFilePath = diffModifiedFilePath;
	}
	
	public String getDiffNewFilePath() {
		return diffNewFilePath;
	}
	
	public void setDiffNewFilePath(String diffNewFilePath) {
		this.diffNewFilePath = diffNewFilePath;
	}
	
	public String getDoDiffCopy() {
		return doDiffCopy;
	}
	
	public void setDoDiffCopy(String doDiffCopy) {
		this.doDiffCopy = doDiffCopy;
	}
	
	public String getDoConcentrateCopy() {
		return doConcentrateCopy;
	}
	
	public void setDoConcentrateCopy(String doConcentrateCopy) {
		this.doConcentrateCopy = doConcentrateCopy;
	}
	
	public String getHoldTimestamp() {
		return holdTimestamp;
	}
	
	public void setHoldTimestamp(String holdTimestamp) {
		this.holdTimestamp = holdTimestamp;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public String getDoDeleteDetailSnap() {
		return doDeleteDetailSnap;
	}
	public void setDoDeleteDetailSnap(String doDeleteDetailSnap) {
		this.doDeleteDetailSnap = doDeleteDetailSnap;
	}
	
	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity extends TaskConcentrateEntity.TaskParamEntity implements ITaskConcentrateExtEntity.ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;
	}

}
