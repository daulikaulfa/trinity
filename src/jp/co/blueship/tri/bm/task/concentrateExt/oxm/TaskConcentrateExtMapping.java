package jp.co.blueship.tri.bm.task.concentrateExt.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.ITaskConcentrateExtEntity;
import jp.co.blueship.tri.bm.task.concentrateExt.oxm.eb.TaskConcentrateExtEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateExtType;
import jp.co.blueship.tri.fw.schema.beans.task.ConcentrateType.Param;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * ConcentrateExtタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskConcentrateExtMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		ConcentrateExtType[] tasks = src.getConcentrateExtArray();

		if( null != tasks ) {
			for( ConcentrateExtType task : tasks ) {
				TaskConcentrateExtEntity taskEntity = new TaskConcentrateExtEntity();
				copyProperties( task, taskEntity );

				Param[] paramArray = task.getParamArray();
				if( null != paramArray ) {
					List<ITaskConcentrateExtEntity.ITaskParamEntity> paramList = new ArrayList<ITaskConcentrateExtEntity.ITaskParamEntity>();
					for( Param param : paramArray ) {
						ITaskConcentrateExtEntity.ITaskParamEntity taskParamEntity = taskEntity.new TaskParamEntity();
						copyProperties( param , taskParamEntity );
						paramList.add( taskParamEntity );
					}
					taskEntity.setParam( paramList.toArray( new ITaskConcentrateExtEntity.ITaskParamEntity[ 0 ] ) );
				}

				entitys.add( taskEntity );
			}
		}
 		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		TaskResultConcentrateExtMapping resultMapping = new TaskResultConcentrateExtMapping();
		resultMapping.mapXMLBeans2DB( src.getResult() , dest );

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskConcentrateExtEntity) )
					continue;

				TaskConcentrateExtEntity taskEntity = (TaskConcentrateExtEntity)entity;

				ConcentrateExtType task = dest.addNewConcentrateExt();

				copyProperties( taskEntity, task );

				ITaskConcentrateExtEntity.ITaskParamEntity[] paramEntityArray = taskEntity.getParam();
				if( null != paramEntityArray ) {
					for( ITaskConcentrateExtEntity.ITaskParamEntity paramEntity : paramEntityArray ) {
						Param param = task.addNewParam();
						copyProperties( paramEntity , param );
					}
				}
			}
		}

		TaskResultConcentrateExtMapping resultMapping = new TaskResultConcentrateExtMapping();
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult();
		resultMapping.mapDB2XMLBeans( src , result );

		return dest;
	}
}
