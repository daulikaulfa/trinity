package jp.co.blueship.tri.bm.task;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskFtpPutGetTypeEntity extends IEntity {

	/**
	 * シーケンス番号を取得します<br>
	 * @return シーケンス番号
	 */
	public String getSequence() ;
	/**
	 * シーケンス番号を設定します<br>
	 * @param sequence シーケンス番号
	 */
	public void setSequence(String sequence) ;
	/**
	 * Paramデータクラスを取得します<br>
	 * @return
	 */
	public ITaskParamEntity[] getParam() ;
	/**
	 * Paramデータクラスを設定します<br>
	 * @param param
	 */
	public void setParam(ITaskParamEntity[] param) ;
	/**
	 * ファイル転送種別（テキスト/バイナリ）を取得します
	 * <br>
	 * @return ファイル転送種別（テキスト/バイナリ）
	 */
	public String getFileType() ;
	/**
	 * ファイル転送種別（テキスト/バイナリ）を設定します
	 * <br>
	 * @param fileType ファイル転送種別。{@link jp.co.blueship.tri.fw.cmn.io.constants.FileIoType}
	 */
	public void setFileType(String fileType) ;

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 転送先パスを取得します<br>
		 * @return 転送先パス
		 */
		public String getDest() ;
		/**
		 * 転送先パスを設定します<br>
		 * @param 転送先パス
		 */
		public void setDest(String dest) ;
		/**
		 * 転送元パスを取得します<br>
		 * @return 転送元パス
		 */
		public String getSrc() ;
		/**
		 * 転送元パスを設定します<br>
		 * @param 転送元パス
		 */
		public void setSrc(String src) ;
	}
}
