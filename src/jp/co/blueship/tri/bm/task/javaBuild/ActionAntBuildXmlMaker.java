package jp.co.blueship.tri.bm.task.javaBuild;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.util.StringUtils;

import jp.co.blueship.tri.fw.agent.core.AntTemplates;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.FileResult;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileResult;

/**
 * 外部ツール（Ant ビルド）で使用するXML定義ファイルを生成します。
 * </p>
 * [概要]
 * <br>コンパイルグループ（言語種別、環境）ごとに呼び出されます。
 * <br>Java以外の言語環境で、このクラスが呼び出されると、何もしません。
 *
 * </p>
 * @author Yukihiro Eguchi
 *
 */
public class ActionAntBuildXmlMaker{

	private String[]	srcPath = null;
	private String[]	classPath = null;
	private String[]	libPath = null;
	private String		classOutput = null ;

	private String 	target = null ;
	private String		compilerName = null ;
	private String		debug = null ;
	private String		debugOption = null ;
	private String		encoding = null ;
	private String		memoryInitialSize = null ;
	private String		memoryMaximumSize = null ;
	private String		executable = null ;

	private String		templatePath = null ;
	private String		outputBuildXmlPath = null ;

	//private IGathering finderReturnAsset = null;


	public final void setSrcPath( String[] value ) {
		this.srcPath = value ;
	}
	public final void setClassPath( String[] value ) {
		this.classPath = value ;
	}
	public final void setLibPath( String[] value ) {
		this.libPath = value ;
	}
	public final void setClassOutput( String value ) {
		this.classOutput = value ;
	}

	public void setCompilerName(String compilerName) {
		this.compilerName = compilerName;
	}
	public void setDebug(String debug) {
		this.debug = debug;
	}
	public void setDebugOption(String debugOption) {
		this.debugOption = debugOption;
	}
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	public void setMemoryInitialSize(String memoryInitialSize) {
		this.memoryInitialSize = memoryInitialSize;
	}
	public void setMemoryMaximumSize(String memoryMaximumSize) {
		this.memoryMaximumSize = memoryMaximumSize;
	}
	public void setTarget(String target) {
		this.target = target;
	}
	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath;
	}
	public void setOutputBuildXmlPath(String outputBuildXmlPath) {
		this.outputBuildXmlPath = outputBuildXmlPath;
	}
	public void setExecutable(String executable) {
		this.executable = executable;
	}
	/**
	 *
	 * @throws BaseBusinessException
	 */
	public final boolean execute() throws BaseBusinessException {

		String template = this.getTemplate( AntTemplates.getBuildXmlTemplate() , templatePath );
		template = this.replaceMain(template);

		// src path
		List<String> words = new ArrayList<String>();
		try {
			int files = 0;
			for ( String path : this.srcPath ) {
				File file = new File( path ) ;
				if ( file.isDirectory() ) {
					words.add( file.getPath() );
					files += TriFileUtils.getFilePaths( file, TriFileUtils.TYPE_FILE ).size() ;
				}
			}

			if ( 0 == files ) {
				return false ;
			}

		} catch ( IOException e ) {
			throw new TriSystemException( BmMessageId.BM005063S , e ) ;
		} finally {

		}

		template = replaceList(
						template,
						AntTemplates.getSrcpathTemplate() ,
						words,
						":SRC_PATH:",
						":SRC_PATH:",
						templatePath
						) ;

		// class path
		words = new ArrayList<String>() ;

		List<String> classes = new ArrayList<String>();
		classes.addAll( FluentList.from(classPath).asList() ) ;
		Iterator<String> iter = classes.iterator() ;
		while( iter.hasNext() ) {
			String path = iter.next() ;
			words.add( path );
		}

		template = replaceList(
						template,
						AntTemplates.getClasspathTemplate() ,
						words,
						":CLASS_PATH:",
						":CLASS_PATH:",
						templatePath
						);

		// class path lib
		words = new ArrayList<String>();

		List<String> lib = new ArrayList<String>();
		lib.addAll( FluentList.from(libPath).asList() ) ;

		iter = lib.iterator() ;
		while( iter.hasNext() ) {
			String path = iter.next() ;
			words.add( path );
		}

		template = replaceList(
						template,
						AntTemplates.getClasspathLibTemplate() ,
						words,
						":CLASS_PATH_LIB:",
						":CLASS_PATH_LIB:",
						templatePath
						);

		// delete
		words = new ArrayList<String>() ;
		words.add( new File( this.classOutput ).getPath() ) ;

		template = replaceList(
						template,
						AntTemplates.getDeleteTemplate() ,
						words,
						":DELETE:",
						":CLASS_DEST_PATH:",
						templatePath
						);

		template = TriStringUtils.convertPath( template ) ;

		/*list.add( */this.outputBuildXml( template, AntTemplates.getBuildXml() , outputBuildXmlPath ) /*)*/ ;
		return true ;
	}

	/**
	 * Ant ビルドのXML定義ファイルの雛形テンプレートを取得します。
	 * @param templateName テンプレートファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param list コンバート間に流通させるビーン
	 * @return 雛形テンプレートの文字列表現
	 * @throws BaseBusinessException
	 */
	private final String getTemplate(String templateName, String templatesPath ) throws BaseBusinessException {

		try{
			return TriFileUtils.readFileConvNewLine(
					new File( templatesPath , templateName ) );

		} catch (IOException e) {
			throw new TriSystemException( BmMessageId.BM005061S , e );
		}
	}

	/**
	 * テンプレートのメイン部を置換します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param list コンバート間に流通させるビーン
	 * @return 整形したテンプレート文字列
	 * @throws BaseBusinessException
	 */
	private final String replaceMain( String template ) throws BaseBusinessException {

		template = StringUtils.replace( template , ":DEBUG:" , ( StatusFlg.on.value().equals( this.debug ) ? "true" : "false" ) ) ;
		template = StringUtils.replace( template , ":DEBUG_OPTION:" , this.debugOption ) ;
		template = StringUtils.replace( template , ":ENCODING:" , this.encoding ) ;

		template = StringUtils.replace( template , ":CLASS_DEST_PATH:" , new File( this.classOutput ).getPath() ) ;

		template = StringUtils.replace( template , ":MEMORY_INITIAL_SIZE:" , this.memoryInitialSize ) ;
		template = StringUtils.replace( template , ":MEMORY_MAXIMUM_SIZE:" , this.memoryMaximumSize ) ;
		template = StringUtils.replace( template , ":TARGET:" , this.target ) ;
		template = StringUtils.replace( template , ":COMPILER_NAME:" , this.compilerName ) ;

		if( true != TriStringUtils.isEmpty( null != this.executable ? this.executable.trim() : "" ) ) {
			template = StringUtils.replace( template , ":EXECUTABLE:" ,this.executable ) ;
		} else {//パラメータが設定されなかった場合は、属性のエントリ自体を削除する
			template = StringUtils.replace( template , "executable=\":EXECUTABLE:\"" , "" ) ;
		}
		return template ;

	}

	/**
	 * テンプレートのリスト部（繰り返し）を置換します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param childTemplateName 子テンプレートファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param words 子templateを置換する文言のリスト。
	 * @param regexWord 対象となるテンプレートの置換される正規表現。
	 * @param childRegexWord 子templateの置換される正規表現。
	 * @param list コンバート間に流通させるビーン
	 * @return 整形したテンプレート文字列
	 * @throws BaseBusinessException
	 */
	private final String replaceList (
								String template,
								String childTemplateName,
								List<String> words,
								String regexWord,
								String childRegexWord,
								String templatesPath
								) throws BaseBusinessException {

		StringBuilder buf = new StringBuilder();

		String childTemplate = getTemplate( childTemplateName, templatesPath );

		for ( Iterator<String> it = words.iterator(); it.hasNext(); ) {
			String word = it.next();

			buf.append(SystemProps.LineSeparator.getProperty());

			buf.append( StringUtils.replace(childTemplate, childRegexWord, word) );
		}

		return StringUtils.replace(template, regexWord, buf.toString());

	}

	/**
	 * 整形したXML定義ファイルをファイル出力します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param buildXmlName XML定義ファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param list コンバート間に流通させるビーン
	 * @return 出力したXML定義ファイルを戻します。
	 * @throws BaseBusinessException
	 */
	private final IFileResult outputBuildXml( String template, String buildXmlName, String outputBuildXmlPath ) throws BaseBusinessException {

		try {
			File file = new File( outputBuildXmlPath , buildXmlName );
			TriFileUtils.writeStringFile( file, template );

			//IFileResult result = (IFileResult)this.getBean("fileResultBean");
			IFileResult result = (IFileResult)new FileResult() ;
			result.setFilePath( file.getPath() );

			return result;

		} catch( IOException e ) {
			throw new TriSystemException( BmMessageId.BM004068F , e );
		}
	}

}
