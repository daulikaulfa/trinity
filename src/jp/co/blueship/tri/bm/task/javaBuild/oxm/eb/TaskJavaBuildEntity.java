package jp.co.blueship.tri.bm.task.javaBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskJavaBuildEntity extends TaskTaskEntity implements ITaskJavaBuildEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String target = null ;
	private String compilerName = null ;
	private String debug = null ;
	private String debugOption = null ;
	private String encoding = null ;
	private String memoryInitialSize = null ;
	private String memoryMaximumSize = null ;
	private ITaskBuildPathEntity buildPathEntity = null ;
	private ICharsetInfoEntity charsetInfoEntity = null ;
	private String executable = null ;
	private String outLogPath = null ;
	private String errLogPath = null ;

	/**
	 * ターゲットを取得します
	 * @return ターゲット
	 */
	public String getTarget() {
		return target ;
	}
	/**
	 * ターゲットをセットします
	 * @param ターゲット
	 */
	public void setTarget(String value) {
		target = value ;
	}
	/**
	 * コンパイラ名を取得します
	 * @return コンパイラ名
	 */
	public String getCompilerName() {
		return compilerName ;
	}
	/**
	 * コンパイラ名をセットします
	 * @param コンパイラ名
	 */
	public void setCompilerName(String value) {
		compilerName = value ;
	}
	/**
	 * デバッグモードを取得します
	 * @return デバッグモード
	 */
	public String getDebug() {
		return debug ;
	}
	/**
	 * デバッグモードをセットします
	 * @param デバッグモード
	 */
	public void setDebug(String value) {
		debug = value ;
	}
	/**
	 * デバッグオプションを取得します
	 * @return デバッグオプション
	 */
	public String getDebugOption() {
		return debugOption ;
	}
	/**
	 * デバッグオプションをセットします
	 * @param デバッグオプション
	 */
	public void setDebugOption(String value) {
		debugOption = value ;
	}
	/**
	 * 文字コードエンコーディングを取得します
	 * @return 文字コードエンコーディング
	 */
	public String getEncoding() {
		return encoding ;
	}
	/**
	 * 文字コードエンコーディングをセットします
	 * @param 文字コードエンコーディング
	 */
	public void setEncoding(String value) {
		encoding = value ;
	}
	/**
	 * 初期メモリ割り当て量（ＭＢ）を取得します
	 * @return 初期メモリ割り当て量（ＭＢ）
	 */
	public String getMemoryInitialSize() {
		return memoryInitialSize ;
	}
	/**
	 * 初期メモリ割り当て量（ＭＢ）をセットします
	 * @param 初期メモリ割り当て量（ＭＢ）
	 */
	public void setMemoryInitialSize(String value) {
		memoryInitialSize = value ;
	}
	/**
	 * 最大メモリ割り当て量（ＭＢ）を取得します
	 * @return 最大メモリ割り当て量（ＭＢ）
	 */
	public String getMemoryMaximumSize() {
		return memoryMaximumSize ;
	}
	/**
	 * 最大メモリ割り当て量（ＭＢ）をセットします
	 * @param 最大メモリ割り当て量（ＭＢ）
	 */
	public void setMemoryMaximumSize(String value) {
		memoryMaximumSize = value ;
	}
	/**
	 * ビルドパスを取得します
	 * @return ビルドパス
	 */
	public ITaskBuildPathEntity getBuildPathEntity() {
		return buildPathEntity ;
	}
	/**
	 * ビルドパスをセットします
	 * @param ビルドパス
	 */
	public void setBuildPathEntity( ITaskBuildPathEntity obj ) {
		buildPathEntity = obj ;
	}
	/**
	 * コンパイラパスを取得します
	 * @return コンパイラパス
	 */
	public String getExecutable() {
		return executable ;
	}
	/**
	 * コンパイラパスをセットします
	 * @param コンパイラパス
	 */
	public void setExecutable(String value) {
		executable = value ;
	}
	/**
	 * エラーログパスを取得します
	 * @return エラーログパス
	 */
	public String getErrLogPath() {
		return errLogPath;
	}
	/**
	 * エラーログパスをセットします
	 * @param エラーログパス
	 */
	public void setErrLogPath(String errLogPath) {
		this.errLogPath = errLogPath;
	}
	/**
	 * 標準ログパスを取得します
	 * @return 標準ログパス
	 */
	public String getOutLogPath() {
		return outLogPath;
	}
	/**
	 * 標準ログパスを取得します
	 * @param 標準ログパス
	 */
	public void setOutLogPath(String outLogPath) {
		this.outLogPath = outLogPath;
	}

	public ICharsetInfoEntity getCharsetInfo() {
		return charsetInfoEntity;
	}
	public void setCharsetInfo(ICharsetInfoEntity charsetInfoEntity) {
		this.charsetInfoEntity = charsetInfoEntity;
	}

	/**
	 * インナーインターフェイス
	 *
	 */
	public class TaskBuildPathEntity implements ITaskBuildPathEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String classOutput = null ;
		private String templatePath = null ;
		private String buildXmlPath = null ;

		private ITaskPathTypeEntity[] srcParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] libParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] classesParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] intensiveSrcParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] deleteSrcParam = new ITaskPathTypeEntity[0];

		public String getClassOutput() {
			return classOutput ;
		}
		public void setClassOutput(String value) {
			classOutput = value ;
		}
		public String getTemplatePath() {
			return templatePath ;
		}
		public void setTemplatePath(String value) {
			templatePath = value ;
		}
		public String getBuildXmlPath() {
			return buildXmlPath ;
		}
		public void setBuildXmlPath(String value) {
			buildXmlPath = value ;
		}

		public ITaskPathTypeEntity[] getSrcParam() {
			return srcParam ;
		}
		public void setSrcParam( ITaskPathTypeEntity[] obj ) {
			srcParam = obj ;
		}
		public ITaskPathTypeEntity[] getLibParam() {
			return libParam ;
		}
		public void setLibParam( ITaskPathTypeEntity[] obj ) {
			libParam = obj ;
		}
		public ITaskPathTypeEntity[] getClassesParam() {
			return classesParam ;
		}
		public void setClassesParam( ITaskPathTypeEntity[] obj ) {
			classesParam = obj ;
		}
		public ITaskPathTypeEntity[] getIntensiveSrcParam() {
			return intensiveSrcParam ;
		}
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] obj ) {
			intensiveSrcParam = obj ;
		}
		public ITaskPathTypeEntity[] getDeleteSrcParam() {
			return deleteSrcParam ;
		}
		public void setDeleteSrcParam( ITaskPathTypeEntity[] obj ) {
			deleteSrcParam = obj ;
		}
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public class TaskPathTypeEntity implements ITaskPathTypeEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null ;

		public String getPath() {
			return path ;
		}
		public void setPath( String value ) {
			path = value ;
		}
	}


}
