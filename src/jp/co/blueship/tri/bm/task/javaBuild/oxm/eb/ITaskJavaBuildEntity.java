package jp.co.blueship.tri.bm.task.javaBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskJavaBuildEntity extends ITaskTaskTypeEntity {


	public String getTarget() ;
	public void setTarget(String value) ;
	public String getCompilerName() ;
	public void setCompilerName(String value) ;
	public String getDebug() ;
	public void setDebug(String value) ;
	public String getDebugOption() ;
	public void setDebugOption(String value) ;
	public String getEncoding() ;
	public void setEncoding(String value) ;
	public String getMemoryInitialSize() ;
	public void setMemoryInitialSize(String value) ;
	public String getMemoryMaximumSize() ;
	public void setMemoryMaximumSize(String value) ;
	public String getExecutable() ;
	public void setExecutable( String value ) ;

	public String getOutLogPath() ;
	public void setOutLogPath( String value ) ;
	public String getErrLogPath() ;
	public void setErrLogPath( String value ) ;

	public ITaskBuildPathEntity getBuildPathEntity() ;
	public void setBuildPathEntity( ITaskBuildPathEntity obj ) ;

	public ICharsetInfoEntity getCharsetInfo() ;
	public void setCharsetInfo( ICharsetInfoEntity obj ) ;

	/**
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskBuildPathEntity extends IEntity {
		public String getClassOutput() ;
		public void setClassOutput(String value) ;
		public String getTemplatePath() ;
		public void setTemplatePath( String value ) ;
		public String getBuildXmlPath() ;
		public void setBuildXmlPath( String value ) ;

		public ITaskPathTypeEntity[] getSrcParam() ;
		public void setSrcParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getLibParam() ;
		public void setLibParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getClassesParam() ;
		public void setClassesParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getIntensiveSrcParam() ;
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getDeleteSrcParam() ;
		public void setDeleteSrcParam( ITaskPathTypeEntity[] obj ) ;
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskPathTypeEntity extends IEntity {
		public String getPath() ;
		public void setPath( String value ) ;
	}
}
