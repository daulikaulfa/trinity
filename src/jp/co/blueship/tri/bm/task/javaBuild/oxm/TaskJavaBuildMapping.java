package jp.co.blueship.tri.bm.task.javaBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.CharsetInfoEntity;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.ITaskJavaBuildEntity;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.TaskJavaBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ExecCharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.PathType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.JavaBuild;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Buildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskJavaBuildMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		JavaBuild[] tasks = src.getJavaBuildArray();

		if( null != tasks ) {
			for( JavaBuild task : tasks ) {
				TaskJavaBuildEntity taskEntity = new TaskJavaBuildEntity();
				copyProperties( task, taskEntity );

				JavaBuild.BuildPath buildPath = task.getBuildPath() ;

				ITaskJavaBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.new TaskBuildPathEntity() ;
				copyProperties( buildPath , buildPathEntity ) ;

//				srcPath
				if( null != buildPath.getSrcArray() ) {
					List<TaskJavaBuildEntity.TaskPathTypeEntity> srcList = new ArrayList<TaskJavaBuildEntity.TaskPathTypeEntity>() ;
					for( PathType srcPath : buildPath.getSrcArray() ) {
						TaskJavaBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
						pathTypeEntity.setPath( srcPath.getPath() ) ;
						srcList.add( pathTypeEntity ) ;
					}
					buildPathEntity.setSrcParam( srcList.toArray( new TaskJavaBuildEntity.TaskPathTypeEntity[0] ) ) ;
				}
				//libPath
				if( null != buildPath.getLibArray() ) {
					List<TaskJavaBuildEntity.TaskPathTypeEntity> libList = new ArrayList<TaskJavaBuildEntity.TaskPathTypeEntity>() ;
					for( PathType libPath : buildPath.getLibArray() ) {
						TaskJavaBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
						copyProperties( libPath , pathTypeEntity ) ;
						libList.add( pathTypeEntity ) ;
					}
					buildPathEntity.setLibParam( libList.toArray( new TaskJavaBuildEntity.TaskPathTypeEntity[0] ) ) ;
				}
				//ClassesPath
				if( null != buildPath.getClassesArray() ) {
					List<TaskJavaBuildEntity.TaskPathTypeEntity> classesList = new ArrayList<TaskJavaBuildEntity.TaskPathTypeEntity>() ;
					for( PathType classesPath : buildPath.getClassesArray() ) {
						TaskJavaBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
						copyProperties( classesPath , pathTypeEntity ) ;
						classesList.add( pathTypeEntity ) ;
					}
					buildPathEntity.setClassesParam( classesList.toArray( new TaskJavaBuildEntity.TaskPathTypeEntity[0] ) ) ;
				}
//				intensiveSrc
				if( null != buildPath.getIntensiveSrcArray() ) {
					List<TaskJavaBuildEntity.TaskPathTypeEntity> intensiveSrcList = new ArrayList<TaskJavaBuildEntity.TaskPathTypeEntity>() ;
					for( PathType intensiveSrcPath : buildPath.getIntensiveSrcArray() ) {
						TaskJavaBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
						copyProperties( intensiveSrcPath , pathTypeEntity ) ;
						intensiveSrcList.add( pathTypeEntity ) ;
					}
					buildPathEntity.setIntensiveSrcParam( intensiveSrcList.toArray( new TaskJavaBuildEntity.TaskPathTypeEntity[0] ) ) ;
				}
//				originalSrc
				if( null != buildPath.getDeleteSrcArray() ) {
					List<TaskJavaBuildEntity.TaskPathTypeEntity> intensiveSrcList = new ArrayList<TaskJavaBuildEntity.TaskPathTypeEntity>() ;
					for( PathType originalSrcPath : buildPath.getDeleteSrcArray() ) {
						TaskJavaBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
						copyProperties( originalSrcPath , pathTypeEntity ) ;
						intensiveSrcList.add( pathTypeEntity ) ;
					}
					buildPathEntity.setDeleteSrcParam( intensiveSrcList.toArray( new TaskJavaBuildEntity.TaskPathTypeEntity[0] ) ) ;
				}
				taskEntity.setBuildPathEntity( buildPathEntity ) ;

				ExecCharsetinfoType charsetInfo = task.getCharsetinfo();
				if ( null != charsetInfo ) {
					CharsetInfoEntity charsetInfoEntity = new CharsetInfoEntity();
					charsetInfoEntity.setIn(charsetInfo.getIn());
					charsetInfoEntity.setOut(charsetInfo.getOut());
					taskEntity.setCharsetInfo(charsetInfoEntity);
				}

				entitys.add( taskEntity ) ;
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

//		Result
		TaskResultJavaBuildMapping resultMapping = new TaskResultJavaBuildMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskJavaBuildEntity) )
					continue;

				ITaskJavaBuildEntity taskEntity = (ITaskJavaBuildEntity)entity;

				JavaBuild task = dest.addNewJavaBuild() ;

				copyProperties( taskEntity, task );

				ITaskJavaBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.getBuildPathEntity() ;
				if( null != buildPathEntity ) {
					JavaBuild.BuildPath buildPath = task.addNewBuildPath() ;
					copyProperties( buildPathEntity , buildPath ) ;

					ITaskJavaBuildEntity.ITaskPathTypeEntity[] srcEntityArray = buildPathEntity.getSrcParam() ;
					if( null != srcEntityArray ) {
						for( ITaskJavaBuildEntity.ITaskPathTypeEntity srcEntity : srcEntityArray ) {
							PathType pathType = buildPath.addNewSrc() ;
							copyProperties( srcEntity , pathType ) ;
						}
					}
					ITaskJavaBuildEntity.ITaskPathTypeEntity[] libEntityArray = buildPathEntity.getLibParam() ;
					if( null != libEntityArray ) {
						for( ITaskJavaBuildEntity.ITaskPathTypeEntity libEntity : libEntityArray ) {
							PathType pathType = buildPath.addNewLib() ;
							copyProperties( libEntity , pathType ) ;
						}
					}
					ITaskJavaBuildEntity.ITaskPathTypeEntity[] classesEntityArray = buildPathEntity.getClassesParam() ;
					if( null != classesEntityArray ) {
						for( ITaskJavaBuildEntity.ITaskPathTypeEntity classesEntity : classesEntityArray ) {
							PathType pathType = buildPath.addNewClasses() ;
							copyProperties( classesEntity , pathType ) ;
						}
					}
					ITaskJavaBuildEntity.ITaskPathTypeEntity[] intensiveSrcEntityArray = buildPathEntity.getIntensiveSrcParam() ;
					if( null != intensiveSrcEntityArray ) {
						for( ITaskJavaBuildEntity.ITaskPathTypeEntity intensiveSrcEntity : intensiveSrcEntityArray ) {
							PathType pathType = buildPath.addNewIntensiveSrc() ;
							copyProperties( intensiveSrcEntity , pathType ) ;
						}
					}
					ITaskJavaBuildEntity.ITaskPathTypeEntity[] originalSrcEntityArray = buildPathEntity.getDeleteSrcParam() ;
					if( null != originalSrcEntityArray ) {
						for( ITaskJavaBuildEntity.ITaskPathTypeEntity originalSrcEntity : originalSrcEntityArray ) {
							PathType pathType = buildPath.addNewDeleteSrc() ;
							copyProperties( originalSrcEntity , pathType ) ;
						}
					}
				}
			}
		}

//		Result
		TaskResultJavaBuildMapping resultMapping = new TaskResultJavaBuildMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
