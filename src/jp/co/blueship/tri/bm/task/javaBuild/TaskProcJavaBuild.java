package jp.co.blueship.tri.bm.task.javaBuild;

import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskLogUtils;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.ITaskJavaBuildEntity;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.ITaskJavaBuildEntity.ITaskPathTypeEntity;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.TaskJavaBuildEntity;
import jp.co.blueship.tri.bm.task.javaBuild.oxm.eb.TaskResultJavaBuildEntity;
import jp.co.blueship.tri.fw.agent.core.ActionAntExecutor;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「javaBuild」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとにjavaビルドタスクの実行を行う。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class TaskProcJavaBuild implements ITaskProc {
	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 4L;

	private static final ILog log = TriLogFactory.getInstance();

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskJavaBuildEntity build = (TaskJavaBuildEntity) task ;
		build.setOutLogPath( TaskStringUtils.substitutionPath( propertyEntity , build.getOutLogPath() ) ) ;
		build.setErrLogPath( TaskStringUtils.substitutionPath( propertyEntity , build.getErrLogPath() ) ) ;

		ITaskJavaBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity() ;
		if( null != buildPathEntity ) {
			buildPathEntity.setClassOutput( TaskStringUtils.substitutionPath( propertyEntity , buildPathEntity.getClassOutput() ) ) ;
			buildPathEntity.setTemplatePath( TaskStringUtils.substitutionPath( propertyEntity , buildPathEntity.getTemplatePath() ) ) ;
			buildPathEntity.setBuildXmlPath( TaskStringUtils.substitutionPath( propertyEntity , buildPathEntity.getBuildXmlPath() ) ) ;

			//","区切りの複数パラメータに対応
			buildPathEntity.setSrcParam( substitutePathSubPathType( build , buildPathEntity.getSrcParam() ) ) ;//SrcParam
			buildPathEntity.setLibParam( substitutePathSubPathType( build , buildPathEntity.getLibParam() ) ) ;//LibParam
			buildPathEntity.setClassesParam( substitutePathSubPathType( build , buildPathEntity.getClassesParam() ) ) ;//ClassParam
			buildPathEntity.setIntensiveSrcParam( substitutePathSubPathType( build , buildPathEntity.getIntensiveSrcParam() ) ) ;//IntensiveSrcParam
			buildPathEntity.setDeleteSrcParam( substitutePathSubPathType( build , buildPathEntity.getDeleteSrcParam() ) ) ;//DeleteSrcParam
		}
	}

	/**
	 *
	 * @param build
	 * @param pathEntityArray
	 * @return
	 * @throws Exception
	 */
	private ITaskJavaBuildEntity.ITaskPathTypeEntity[] substitutePathSubPathType( TaskJavaBuildEntity build , ITaskJavaBuildEntity.ITaskPathTypeEntity[] pathEntityArray ) throws Exception {
		List<ITaskJavaBuildEntity.ITaskPathTypeEntity> arrlst = new ArrayList<ITaskJavaBuildEntity.ITaskPathTypeEntity>() ;

		for( ITaskJavaBuildEntity.ITaskPathTypeEntity pathEntity : pathEntityArray ) {
			String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , pathEntity.getPath() ) ;
			if( null != arrays ) {
				for( String array : arrays ) {
					ITaskJavaBuildEntity.ITaskPathTypeEntity entity = build.new TaskPathTypeEntity() ;
					TriPropertyUtils.copyProperties( entity, pathEntity );
					entity.setPath( array ) ;
					arrlst.add( entity ) ;
				}
			}
		}
		return arrlst.toArray( new ITaskJavaBuildEntity.ITaskPathTypeEntity[ 0 ] ) ;
	}

	/**
	 * 「javaBuild」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskJavaBuildEntity) )
			return null ;


		TaskResultJavaBuildEntity resultJavaBuildEntity = new TaskResultJavaBuildEntity() ;
		String taskResultCode = TaskResultCode.SUCCESS ;
		resultJavaBuildEntity.setSequence( prmTask.getSequence() ) ;
		resultJavaBuildEntity.setTaskId( prmTask.getTaskId() );

		ITaskJavaBuildEntity build = (ITaskJavaBuildEntity) prmTask ;
		try {
			substitutePath( build ) ;

//			build.xmlファイルの生成
			ActionAntBuildXmlMaker xmlMaker = new ActionAntBuildXmlMaker() ;

			xmlMaker.setEncoding( build.getEncoding() ) ;
			xmlMaker.setTarget( build.getTarget() ) ;
			xmlMaker.setCompilerName( build.getCompilerName() ) ;
			xmlMaker.setDebug( build.getDebug() ) ;
			xmlMaker.setDebugOption( build.getDebugOption() ) ;
			xmlMaker.setMemoryInitialSize( build.getMemoryInitialSize() ) ;
			xmlMaker.setMemoryMaximumSize( build.getMemoryMaximumSize() ) ;
			xmlMaker.setExecutable( build.getExecutable() ) ;

			ITaskJavaBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity() ;
			if( null != buildPathEntity ) {

				xmlMaker.setTemplatePath( buildPathEntity.getTemplatePath() ) ;
				xmlMaker.setOutputBuildXmlPath( buildPathEntity.getBuildXmlPath() ) ;
				xmlMaker.setClassOutput( buildPathEntity.getClassOutput() ) ;

				String[] srcArray = convertPathArray( buildPathEntity.getSrcParam() ) ;
				boolean chk = false ;
				for( String src : srcArray ) {
					if( true != TriStringUtils.isEmpty( src ) ) {
						chk = true ;
					}
				}
				if( false == chk ) {
					LogHandler.info( log , TriLogMessage.LBM0008 ) ;
					taskResultCode = TaskResultCode.UN_PROCESS ;
					return resultJavaBuildEntity ;
				}

				xmlMaker.setSrcPath( srcArray ) ;
				xmlMaker.setClassPath( convertPathArray( buildPathEntity.getClassesParam() ) ) ;
				xmlMaker.setLibPath( convertPathArray( buildPathEntity.getLibParam() ) ) ;

				boolean buildGo = false ;
				/** 削除資産のチェック **/
				//deleteSrcのパスをチェックし、すべての削除対象ファイルが原本に存在していればビルド処理を実行、１件でも存在しなければスルーする

				String[] deleteSrcArray = convertPathArray( buildPathEntity.getDeleteSrcParam() ) ;
				if( TriStringUtils.isEmpty( deleteSrcArray ) ) {//deleteSrcのパス自体が設定されていない⇒intensiveSrcのみで実行可否を判断する。
					//buildGo = true ;//パス定義が存在しない場合、intensiveSrcのみで実行可否を判断する。
				} else {
					for( String path : deleteSrcArray ) {
						if ( TriFileUtils.checkExistFiles( path , false, BusinessFileUtils.getFileFilter()) ) {
							buildGo = true;
							break ;
						}
					}
				}
				/** 返却資産のチェック **/
				//intensiveSrcのパスを再帰的に辿って、返却資産が１件でもあればビルド処理を実行、１件もなければビルド処理をスルーする
				//また、intensiveSrcのパス自体が設定されていない場合には、ビルド処理を行う。
				String[] intensiveSrcArray = convertPathArray( buildPathEntity.getIntensiveSrcParam() ) ;
				if( TriStringUtils.isEmpty( intensiveSrcArray ) ) {//intensiveSrcのパス自体が設定されていない⇒ビルド実行
					buildGo = true ;
				} else {
					FileFilter filter = BusinessFileUtils.getFileFilter();
					for( String path : intensiveSrcArray ) {
						if( TriFileUtils.checkExistFiles( path , false, filter ) ) {
							buildGo = true;
							break ;
						}
					}
				}
				/**
				 * ビルド処理
				 */
				if( true != buildGo ) {//コンパイル対象となる返却資産が１件もないのでビルドを行わず抜ける
					LogHandler.info( log , TriLogMessage.LBM0001 ) ;
					taskResultCode = TaskResultCode.UN_PROCESS ;
					return resultJavaBuildEntity ;
				}
				if( xmlMaker.execute() ) { //build.xmlファイル生成
					//build.xmlを参照してビルド
					Charset inCharset = build.getCharsetInfo() != null ? Charset.value( build.getCharsetInfo().getIn() ) : null;

					ActionAntExecutor antExecutor = new ActionAntExecutor( inCharset );

					try {
						antExecutor.execute( buildPathEntity.getBuildXmlPath() , TriLogFactory.getInstance() ) ;
					} catch( Exception e ) {
						throw e ;
					} finally {
						String outCharset = build.getCharsetInfo() != null ? build.getCharsetInfo().getOut() : null;
						resultJavaBuildEntity.setOutLog( TaskLogUtils.writeLog( build.getOutLogPath() , antExecutor.getOutLog() , Charset.value( outCharset ) ) );
						resultJavaBuildEntity.setErrLog( TaskLogUtils.writeLog( build.getErrLogPath() , antExecutor.getErrLog() , Charset.value( outCharset ) ) );
					}
				}
			}
		} catch ( Exception e ) {
			taskResultCode = TaskResultCode.ERROR_PROCESS ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005023S , e );
		} finally {
			resultJavaBuildEntity.setCode( taskResultCode ) ;
			prmTarget.addTaskResult( resultJavaBuildEntity ) ;
		}

		return resultJavaBuildEntity ;
	}

	/**
	 * ITaskPathTypeEntityオブジェクトの配列からのsrc文字列のみを配列に格納して返す<br>
	 * @param pathEntityArrayオブジェクト
	 * @return src文字列の配列
	 */
	private String[] convertPathArray( ITaskPathTypeEntity[] pathEntityArray ) {
		List<String> list = new ArrayList<String>() ;
		if( null != pathEntityArray ) {
			for( ITaskJavaBuildEntity.ITaskPathTypeEntity pathEntity : pathEntityArray ) {
				list.add( pathEntity.getPath() ) ;
			}
		}
		return list.toArray( new String[ 0 ] ) ;
	}

}
