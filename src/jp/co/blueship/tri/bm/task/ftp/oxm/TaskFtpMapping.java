package jp.co.blueship.tri.bm.task.ftp.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.ITaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.bm.task.TaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.TaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.ITaskFtpEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.TaskFtpEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.FtptaskType.Param;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Ftp;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Ftpタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskFtpMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Ftp[] tasks = src.getFtpArray();

		if( null != tasks ) {
			for( Ftp task : tasks ) {
				TaskFtpEntity taskEntity = new TaskFtpEntity();

				copyProperties( task, taskEntity );
				//put
				if( null != task.getPutArray() ) {
					List<TaskFtpPutGetTypeEntity> putList = new ArrayList<TaskFtpPutGetTypeEntity>();

					for( Ftp.Put put : task.getPutArray() ) {
						TaskFtpPutGetTypeEntity putEntity = new TaskFtpPutGetTypeEntity();
						copyProperties( put, putEntity );

						if( null != put.getParamArray() ) {
							List<ITaskFtpPutGetTypeEntity.ITaskParamEntity> paramList = new ArrayList<ITaskFtpPutGetTypeEntity.ITaskParamEntity>();

							for( Param param : put.getParamArray() ) {
								TaskFtpPutGetTypeEntity.TaskParamEntity paramEntity = putEntity.new TaskParamEntity( param.getSrc() , param.getDest() );
								paramList.add( paramEntity );
							}
							putEntity.setParam( paramList.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] ) );
						}
						putList.add( putEntity );
					}

					taskEntity.setPut( putList.toArray( new ITaskFtpPutGetTypeEntity[ 0 ] ) );
				}

				//get
				if( null != task.getGetArray() ) {
					List<TaskFtpPutGetTypeEntity> getList = new ArrayList<TaskFtpPutGetTypeEntity>();

					for( Ftp.Get get : task.getGetArray() ) {
						TaskFtpPutGetTypeEntity getEntity = new TaskFtpPutGetTypeEntity();
						copyProperties( get, getEntity );

						if( null != get.getParamArray() ) {
							List<TaskFtpPutGetTypeEntity.TaskParamEntity> paramList = new ArrayList<TaskFtpPutGetTypeEntity.TaskParamEntity>();

							for(Param param : get.getParamArray() ) {
								TaskFtpPutGetTypeEntity.TaskParamEntity paramEntity = getEntity.new TaskParamEntity( param.getSrc() , param.getDest() );
								paramList.add( paramEntity );
							}
							getEntity.setParam( paramList.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] ) );
						}
						getList.add( getEntity );
					}

					taskEntity.setGet( getList.toArray( new ITaskFtpPutGetTypeEntity[ 0 ] ) );
				}

				//del
				if( null != task.getDelArray() ) {
					List<TaskFtpDelTypeEntity> delList = new ArrayList<TaskFtpDelTypeEntity>();

					for( Ftp.Del del : task.getDelArray() ) {
						TaskFtpDelTypeEntity delEntity = new TaskFtpDelTypeEntity();
						copyProperties( del, delEntity );

						if( null != del.getParamArray() ) {
							List<String> paramList = new ArrayList<String>();
							for( jp.co.blueship.tri.fw.schema.beans.task.TaskDocument.Task.Target.Ftp.Del.Param param : del.getParamArray() ) {
								paramList.add( param.getSrc() );
							}
							delEntity.setSrc( paramList.toArray( new String[ 0 ] ) );
						}
						delList.add( delEntity );
					}
					taskEntity.setDel( delList.toArray( new ITaskFtpDelTypeEntity[ 0 ] ) );
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultFtpMapping resultMapping = new TaskResultFtpMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskFtpEntity) )
					continue;

				TaskFtpEntity taskEntity = (TaskFtpEntity)entity;

				Ftp task = dest.addNewFtp();

				copyProperties( taskEntity, task );

				//put
				if( null != taskEntity.getPut() ) {
					for( ITaskFtpPutGetTypeEntity putEntity : taskEntity.getPut() ) {
						Ftp.Put put = task.addNewPut();
						copyProperties( putEntity, put );

						if( null != putEntity.getParam() ) {
							for( ITaskFtpPutGetTypeEntity.ITaskParamEntity paramEntity : putEntity.getParam() ) {
								jp.co.blueship.tri.fw.schema.beans.task.FtptaskType.Param param = put.addNewParam();
								copyProperties( paramEntity, param );
							}
						}
					}
				}

				//get
				if( null != taskEntity.getGet() ) {
					for( ITaskFtpPutGetTypeEntity getEntity : taskEntity.getGet() ) {
						jp.co.blueship.tri.fw.schema.beans.task.TaskDocument.Task.Target.Ftp.Get get = task.addNewGet();
						copyProperties( getEntity, get );

						if( null != getEntity.getParam() ) {
							for( ITaskFtpPutGetTypeEntity.ITaskParamEntity paramEntity : getEntity.getParam() ) {
								jp.co.blueship.tri.fw.schema.beans.task.FtptaskType.Param param = get.addNewParam();
								copyProperties( paramEntity, param );
							}
						}
					}
				}

				//del
				if( null != taskEntity.getDel() ) {
					for( ITaskFtpDelTypeEntity delEntity : taskEntity.getDel() ) {
						Ftp.Del del = task.addNewDel();
						copyProperties( delEntity, del );

						if( null != delEntity.getSrc() ) {
							for( String delsrc : delEntity.getSrc() ) {
								jp.co.blueship.tri.fw.schema.beans.task.TaskDocument.Task.Target.Ftp.Del.Param param = del.addNewParam();
								param.setSrc( delsrc );
							}
						}
					}
				}
			}
		}

		//Result
		TaskResultFtpMapping resultMapping = new TaskResultFtpMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
