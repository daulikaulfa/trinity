package jp.co.blueship.tri.bm.task.ftp;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.ITaskFtpEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.ITaskResultFtpEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.ITaskResultFtpEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.TaskFtpEntity;
import jp.co.blueship.tri.bm.task.ftp.oxm.eb.TaskResultFtpEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.FileIoType;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.ftp.FtpIo;
import jp.co.blueship.tri.fw.ftp.FtpParamBean;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「ftp」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元にＦＴＰ転送を行う。
 * <br>セキュリティのため、パスワード管理はRDB上にて暗号化して管理を行う。
 * 「FTPホスト／IPアドレス」及び「ユーザID」で情報が紐づく。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskProcFtp implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;

	public void setSupport(TaskFinderSupport support) {
		this.support = support;
	}

	public enum FtpType {
		put ,
		get ,
		del;
	}
	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskFtpEntity ftp = (TaskFtpEntity) task;

		ITaskFtpPutGetTypeEntity[] putArray = ftp.getPut();
		if( null != putArray ) {
			for( ITaskFtpPutGetTypeEntity put : putArray ) {

				ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = put.getParam();
				if( null != paramEntity ) {
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String src = TaskStringUtils.substitutionPath( propertyEntity , param.getSrc() ) ;
						String dest = TaskStringUtils.substitutionPath( propertyEntity , param.getDest() ) ;

						param.setSrc( TriStringUtils.trimTailSeparator( src ) ) ;//ftpでは、パス後尾の"/"除去も一緒に行う。
						param.setDest( TriStringUtils.trimTailSeparator( dest ) ) ;//ftpでは、パス後尾の"/"除去も一緒に行う。
					}
				}
//				","区切りの複数パラメータに対応
				/* 下記コードだとTaskParamEntityの生成数・内容に問題ありそうなので複数パラメータ対応はいったん外す
				 ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = put.getParam();
				if( null != paramEntity ) {
					List<ITaskFtpPutGetTypeEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskFtpPutGetTypeEntity.ITaskParamEntity>();
					//src
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getSrc() );
						if( null != arrays ) {
							for( String array : arrays ) {
								TaskFtpPutGetTypeEntity.TaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)put ).new TaskParamEntity();
								entity.setSrc( array );
								entity.setDest( param.getDest() );
								arrlst.add( entity );
							}
						}
					}
					//dst
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getDest() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)put ).new TaskParamEntity();
								entity.setSrc( param.getSrc() );
								entity.setDest( array );
								arrlst.add( entity );
							}
						}
					}
					paramEntity = arrlst.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] );
					put.setParam( paramEntity );
				}
				*/
			}
		}

		ITaskFtpPutGetTypeEntity[] getArray = ftp.getGet();
		if( null != getArray ) {
			for( ITaskFtpPutGetTypeEntity get : getArray ) {

				ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = get.getParam();
				if( null != paramEntity ) {
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String src = TaskStringUtils.substitutionPath( propertyEntity , param.getSrc() );
						String dest = TaskStringUtils.substitutionPath( propertyEntity , param.getDest() );

						param.setSrc( TriStringUtils.trimTailSeparator( src ) ) ;//ftpでは、パス後尾の"/"除去も一緒に行う。
						param.setDest( TriStringUtils.trimTailSeparator( dest ) ) ;//ftpでは、パス後尾の"/"除去も一緒に行う。
					}
				}
				//","区切りの複数パラメータに対応
				/* 下記コードだとTaskParamEntityの生成数・内容に問題ありそうなので複数パラメータ対応はいったん外す
				ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = get.getParam();
				if( null != paramEntity ) {
					List<ITaskFtpPutGetTypeEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskFtpPutGetTypeEntity.ITaskParamEntity>();
					//src
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getSrc() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)get ).new TaskParamEntity();
								entity.setSrc( array );
								entity.setDest( param.getDest() );
								arrlst.add( entity );
							}
						}
					}
					//dst
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getDest() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)get ).new TaskParamEntity();
								entity.setSrc( param.getSrc() );
								entity.setDest( array );
								arrlst.add( entity );
							}
						}
					}
					paramEntity = arrlst.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] );
					get.setParam( paramEntity );
				}
				*/
			}
		}
		ITaskFtpDelTypeEntity[] delArray = ftp.getDel();
		if( null != delArray ) {

			for( ITaskFtpDelTypeEntity del : delArray ) {
				String[] srcArray = del.getSrc();
				if( null != srcArray ) {
					List<String> newSrcList = new ArrayList<String>() ;
					for( String src : srcArray ) {
						src = TaskStringUtils.substitutionPath( propertyEntity , src ) ;

						src = TriStringUtils.trimTailSeparator( src ) ;//ftpでは、パス後尾の"/"除去も一緒に行う。
						newSrcList.add( src ) ;
					}
					del.setSrc( newSrcList.toArray( new String[ 0 ] ) ) ;
				}
			}
//			","区切りの複数パラメータに対応
			/* GET/PUTに倣って複数パラメータ対応はいったん外す
			for( ITaskFtpDelTypeEntity del : delArray ) {
				List<String> arrlst = new ArrayList<String>();
				String[] srcArray = del.getSrc();
				for( String src : srcArray ) {
					String[] arrays = TaskStringUtil.substitutionPaths( propertyEntity , src );
					if( null != arrays ) {
						for( String array : arrays ) {
							arrlst.add( array );
						}
					}
					srcArray = arrlst.toArray( new String[ 0 ] );
					del.setSrc( srcArray );
				}
			}
			*/
		}
	}

	/**
	 * 「ftp」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskFtpEntity) )
			return null;

		ITaskResultFtpEntity resultFtpEntity = new TaskResultFtpEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultFtpEntity.setSequence( prmTask.getSequence() );
		resultFtpEntity.setCode( TaskResultCode.UN_PROCESS );
		resultFtpEntity.setTaskId( prmTask.getTaskId() );

		FtpIo ftpIo = null;

		ITaskFtpEntity ftp = (ITaskFtpEntity) prmTask;

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			Map<Integer,Object[]> commandParamMap = new TreeMap<Integer,Object[]>();
			FtpParamBean ftpParam = new FtpParamBean();
			this.setCommandParam(ftp, ftpParam, commandParamMap);

			ftpIo = new FtpIo( ftpParam );
			if ( ! StatusFlg.on.value().equals( ftp.getDoSimulate() ) )
				ftpIo.openConnection();

			for ( Object[] obj : commandParamMap.values() ) {
				if( FtpType.put.equals( obj[ 0 ] ) ) {
					ITaskFtpPutGetTypeEntity put = (ITaskFtpPutGetTypeEntity)obj[ 1 ];
					ftpIo.setFileType( this.getFileIoType( put.getFileType() ) );
					ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramArray = put.getParam();

					if( null != paramArray ) {
						for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramArray ) {
							//1コマンドごとに処理結果を記録する
							ITaskResultEntity innerResultEntity = resultFtpEntity.newResult();

							try {
								innerResultEntity.setSequence( String.valueOf(resultSequence++) );
								innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								innerResultEntity.setCommand( FtpType.put.toString() );
								innerResultEntity.setSrc( param.getSrc() );
								innerResultEntity.setDest( param.getDest() );

								if ( StatusFlg.on.value().equals( ftp.getDoSimulate() ) ) {
								} else {
									ftpIo.putObject( param.getSrc() , param.getDest() );
									innerResultEntity.setCode( TaskResultCode.SUCCESS );
								}
							} catch ( FileNotFoundException e ) {
								boolean doNotExistSkip = ( StatusFlg.on.value().equals( ftp.getDoNotExistSkip() ) ) ? true : false;
								if( true == doNotExistSkip ) {//送るファイルがないのでスキップ
									innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								} else {
									innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
									throw e;
								}
							} catch ( Exception e ) {
								innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
								throw e;
							} finally {
								if ( StatusFlg.on.value().equals( ftp.getDoDetailSnap() ) ) {
									resultList.add( innerResultEntity );
								} else {
									if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
										resultList.add( innerResultEntity );
									}
								}
							}
						}
					}
				}

				if( FtpType.get.equals( obj[ 0 ] ) ) {
					ITaskFtpPutGetTypeEntity get = (ITaskFtpPutGetTypeEntity)obj[ 1 ];
					ftpIo.setFileType( this.getFileIoType( get.getFileType() ) );
					ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramArray = get.getParam();


					if( null != paramArray ) {
						for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramArray ) {
							//1コマンドごとに処理結果を記録する
							ITaskResultEntity innerResultEntity = resultFtpEntity.newResult();

							try {
								innerResultEntity.setSequence( String.valueOf(resultSequence++) );
								innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								innerResultEntity.setCommand( FtpType.get.toString() );
								innerResultEntity.setSrc( param.getSrc() );
								innerResultEntity.setDest( param.getDest() );

								if ( StatusFlg.on.value().equals( ftp.getDoSimulate() ) ) {
								} else {
									ftpIo.getObject( param.getDest() , param.getSrc() );
									innerResultEntity.setCode( TaskResultCode.SUCCESS );
								}
							} catch ( FileNotFoundException e ) {
								boolean doNotExistSkip = ( StatusFlg.on.value().equals( ftp.getDoNotExistSkip() ) ) ? true : false;
								if( true == doNotExistSkip ) {//受け取るファイルがないのでスキップ
									innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								} else {
									innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
									throw e;
								}
							} catch ( Exception e ) {
								innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
								throw e;
							} finally {
								if ( StatusFlg.on.value().equals( ftp.getDoDetailSnap() ) ) {
									resultList.add( innerResultEntity );
								} else {
									if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
										resultList.add( innerResultEntity );
									}
								}
							}
						}
					}
				}

				if( FtpType.del.equals( obj[ 0 ] ) ) {
					ITaskFtpDelTypeEntity del = (ITaskFtpDelTypeEntity)obj[ 1 ];
					String[] srcArray = del.getSrc();


					if( null != srcArray ) {
						for( String src : srcArray ) {
							//1コマンドごとに処理結果を記録する
							ITaskResultEntity innerResultEntity = resultFtpEntity.newResult();

							try {
								innerResultEntity.setSequence( String.valueOf(resultSequence++) );
								innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								innerResultEntity.setCommand( FtpType.del.toString() );
								innerResultEntity.setSrc( src );

								if ( StatusFlg.on.value().equals( ftp.getDoSimulate() ) ) {
								} else {
									ftpIo.deleteObject( src );
									innerResultEntity.setCode( TaskResultCode.SUCCESS );
								}
							} catch ( FileNotFoundException e ) {
								boolean doNotExistSkip = ( StatusFlg.on.value().equals( ftp.getDoNotExistSkip() ) ) ? true : false;
								if( true == doNotExistSkip ) {//受け取るファイルがないのでスキップ
									innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								} else {
									innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
									throw e;
								}
							} catch ( Exception e ) {
								innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
								throw e;
							} finally {
								if ( StatusFlg.on.value().equals( ftp.getDoDetailSnap() ) ) {
									resultList.add( innerResultEntity );
								} else {
									if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
										resultList.add( innerResultEntity );
									}
								}
							}
						}
					}
				}
			}

			resultFtpEntity.setCode( TaskResultCode.SUCCESS );
		} catch( Exception e ) {
			resultFtpEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005021S , e );
		} finally {
			try {
				if( null != ftpIo ) {
					if ( ! StatusFlg.on.value().equals( ftp.getDoSimulate() ) )
						ftpIo.closeConnection();
				}
			} catch ( Exception e ) {
				throw e ;
			} finally {
				resultFtpEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
				prmTarget.addTaskResult( resultFtpEntity );
			}
		}

		return resultFtpEntity;
	}

	/**
	 * put/get/delのコマンドパラメタを格納する。
	 * @param ftp
	 * @param ftpParam
	 * @param commandParamMap
	 * @param defFile
	 * @throws Exception
	 */
	private void setCommandParam(	ITaskFtpEntity ftp,
									FtpParamBean ftpParam,
									Map<Integer,Object[]> commandParamMap ) throws Exception {

		ftpParam.setHost( ftp.getServer() );
		if( true != TriStringUtils.isEmpty( ftp.getPort() ) ) {
			ftpParam.setPort( Integer.parseInt( ftp.getPort() ) );
		}
		ftpParam.setUser( ftp.getUserid() );
		ftpParam.setPass( support.getSmFinderSupport().findPasswordEntity( PasswordCategory.FTP , ftp.getServer() , ftp.getUserid() ).getPassword() );
		if( null != ftp.getDoPassivemode() ) {
			ftpParam.setPasvMode( StatusFlg.on.value().toString().equals( ftp.getDoPassivemode().toLowerCase() ) ? true : false );
		} else {
			throw new TriSystemException( BmMessageId.BM005021S );
		}
		ftpParam.setControlEncoding( getCharset( ftp.getEncoding() ) );

		ITaskFtpPutGetTypeEntity[] putArray = ftp.getPut();
		if( null != putArray ) {
			for(ITaskFtpPutGetTypeEntity put : putArray ) {
				Object[] obj = { FtpType.put , put };
				commandParamMap.put( Integer.parseInt( put.getSequence() ) , obj );
			}
		}
		ITaskFtpPutGetTypeEntity[] getArray = ftp.getGet();
		if( null != getArray ) {
			for(ITaskFtpPutGetTypeEntity get : getArray ) {
				Object[] obj = { FtpType.get , get };
				commandParamMap.put( Integer.parseInt( get.getSequence() ) , obj );
			}
		}
		ITaskFtpDelTypeEntity[] delArray = ftp.getDel();
		if( null != delArray ) {
			for(ITaskFtpDelTypeEntity del : delArray ) {
				Object[] obj = { FtpType.del , del };
				commandParamMap.put( Integer.parseInt( del.getSequence() ) , obj );
			}
		}
	}

	/**
	 * 「ＦＴＰファイル種別」文字列をenum型FtpFileにコンバートする
	 * <br>
	 * @param value 「ＦＴＰファイル種別」文字列
	 * @return enum型FtpFile
	 * @throws Exception
	 */
	private FileIoType getFileIoType( String type ) throws Exception {
		FileIoType fileIoType = null;
		try {
			fileIoType = FileIoType.getValue( Integer.parseInt( type ) );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004038F , e , type);
		}
		return fileIoType;
	}
	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする
	 * <br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004039F ,e ,encoding);
		}
		return charset;
	}
}
