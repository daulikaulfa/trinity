package jp.co.blueship.tri.bm.task.ftp.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;


public class TaskFtpEntity extends TaskTaskEntity implements ITaskFtpEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String server = null;
	private String port = "21";
	private String userid = null;
	private String doPassivemode = StatusFlg.off.value();
	private String encoding = null;
	private String doSimulate = StatusFlg.off.value();
	private String doDetailSnap = StatusFlg.off.value();
	private String doNotExistSkip = StatusFlg.off.value();

	private ITaskFtpPutGetTypeEntity[] put = new ITaskFtpPutGetTypeEntity[0];
	private ITaskFtpPutGetTypeEntity[] get = new ITaskFtpPutGetTypeEntity[0];
	private ITaskFtpDelTypeEntity[] del = new ITaskFtpDelTypeEntity[0];

	public String getDoPassivemode() {
		return doPassivemode;
	}
	public void setDoPassivemode(String doPassivemode) {
		this.doPassivemode = doPassivemode;
	}
	public String getEncoding() {
		return encoding;
	}
	public void setEncoding(String encoding) {
		this.encoding = encoding;
	}
	public String getPort() {
		return port;
	}
	public void setPort(String port) {
		this.port = port;
	}
	public String getServer() {
		return server;
	}
	public void setServer(String server) {
		this.server = server;
	}
	public String getUserid() {
		return userid;
	}
	public void setUserid(String userid) {
		this.userid = userid;
	}

	public ITaskFtpPutGetTypeEntity[] getGet() {
		return get;
	}
	public void setGet(ITaskFtpPutGetTypeEntity[] get) {
		this.get = get;
	}
	public ITaskFtpPutGetTypeEntity[] getPut() {
		return put;
	}
	public void setPut(ITaskFtpPutGetTypeEntity[] put) {
		this.put = put;
	}
	public ITaskFtpDelTypeEntity[] getDel() {
		return del;
	}
	public void setDel(ITaskFtpDelTypeEntity[] del) {
		this.del = del;
	}
	public String getDoSimulate() {
		return doSimulate ;
	}
	public void setDoSimulate( String value ) {
		doSimulate = value ;
	}
	public String getDoDetailSnap() {
		return doDetailSnap;
	}
	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}
	public String getDoNotExistSkip() {
		return doNotExistSkip;
	}
	public void setDoNotExistSkip(String doNotExistSkip) {
		this.doNotExistSkip = doNotExistSkip;
	}
}
