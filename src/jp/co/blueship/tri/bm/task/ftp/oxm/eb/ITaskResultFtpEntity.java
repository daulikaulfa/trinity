package jp.co.blueship.tri.bm.task.ftp.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultFtpEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult(ITaskResultEntity[] values);
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 *
	 * 全体の転送元、転送先の結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends ITaskResultTypeEntity {
		/**
		 * 実行されたFTPコマンドを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getCommand();
		/**
		 * 実行されたFTPコマンドを設定します。
		 *
		 * @param value FTP実行コマンド。{@link jp.co.blueship.tri.bm.task.ftp.TaskProcFtp.FtpType}。
		 */
		public void setCommand( String value );
		/**
		 * 転送元のディレクトリ／ファイル。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getSrc();
		/**
		 * 転送元のディレクトリ／ファイル。
		 *
		 * @param value 転送元のディレクトリ／ファイル
		 */
		public void setSrc( String value );
		/**
		 * 転送先のディレクトリ／ファイル。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDest();
		/**
		 * 転送先のディレクトリ／ファイル。
		 *
		 * @param value 転送先のディレクトリ／ファイル
		 */
		public void setDest(String value);
	}

}
