package jp.co.blueship.tri.bm.task.changePermission;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.changePermission.TaskProcChangePermission.ChPermissionDefinition.ChPermissionEntry;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskChangePermissionEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskChangePermissionEntity.ITaskParamEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskResultChangePermissionEntity.ITaskDetailEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskResultChangePermissionEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskChangePermissionEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskResultChangePermissionEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskResultChangePermissionEntity.TaskDetailEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskResultChangePermissionEntity.TaskResultEntity;
import jp.co.blueship.tri.fw.agent.core.TaskCommandExecutor;
import jp.co.blueship.tri.fw.agent.core.TaskDefineFileUtils;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriSystemUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.LogUtil;
import jp.co.blueship.tri.fw.log.LogUtilLog4j;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「changePermission」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元に、パーミッションの変更を行う。
 */
public class TaskProcChangePermission implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private static final ILog log = TriLogFactory.getInstance();

	//private static final boolean scmNeglect = true;//true : SCM関連フォルダ・ディレクトリを無視

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private static final String COMMAND_CHMOD = "chmod" ;
	private static final String COMMAND_CHOWN = "chown" ;
	private static final String COMMAND_CHGRP = "chgrp" ;

	/**
	 * パーミッション変更対象 トップ(ルート)ディレクトリを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String BASE_DIR = "BASE_DIR";
	/**
	 * パーミッション変更判断パスを示す予約語。
	 * タスクからの変数展開の対象
	 */
	private static final String JUDGE_PATH = "JUDGE_PATH";

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskChangePermissionEntity changePermission = (TaskChangePermissionEntity) task;

		ITaskChangePermissionEntity.ITaskParamEntity[] paramArray = changePermission.getParam();
		if( null != paramArray ) {
			List<ITaskChangePermissionEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskChangePermissionEntity.ITaskParamEntity>();
			for( ITaskChangePermissionEntity.ITaskParamEntity param : paramArray ) {
				String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , param.getPath() );
				if( null != arrays ) {
					for( String array : arrays ) {
						ITaskChangePermissionEntity.ITaskParamEntity entity = changePermission.new TaskParamEntity();
						TriPropertyUtils.copyProperties( entity, param );
						entity.setPath( array );
						arrlst.add( entity );
					}
				}
				paramArray = arrlst.toArray( new ITaskChangePermissionEntity.ITaskParamEntity[ 0 ] );
				changePermission.setParam( paramArray );
			}
		}

		changePermission.setDefineFilePath(
				TaskStringUtils.substitutionPath( this.propertyEntity , changePermission.getDefineFilePath() ));
	}

	/**
	 * 「changePermission」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskChangePermissionEntity) )
			return null;

		TaskResultChangePermissionEntity resultChangePermissionEntity = new TaskResultChangePermissionEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultChangePermissionEntity.setSequence( prmTask.getSequence() );
		resultChangePermissionEntity.setCode( TaskResultCode.UN_PROCESS );
		resultChangePermissionEntity.setTaskId( prmTask.getTaskId() );

		final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			ITaskChangePermissionEntity changePermission = (ITaskChangePermissionEntity) prmTask;

			List<ChPermissionDefinition> chPermissionDefList = new ArrayList<ChPermissionDefinition>();

			// 定義ファイルによらないパーミッション変更
			String code = TaskResultCode.UN_PROCESS;
			try{
				code = this.doChangePermission(changePermission, resultChangePermissionEntity, resultList, resultSequence);
			} catch (Exception e){
				throw e;
			}

			// 定義リストファイルの設定あり
			String codeByDefFile = TaskResultCode.UN_PROCESS;
			if ( null != changePermission.getDefineFilePath() ) {
				File defListFile = new File( changePermission.getDefineFilePath() );
				if ( !defListFile.exists() ) {
					throw new TriSystemException( BmMessageId.BM004011F , changePermission.getDefineFilePath() );
				}

				chPermissionDefList.addAll( this.getChPermissionDefFileList( defListFile ) );

				try{
					codeByDefFile = this.doChangePermissionByDefFile(chPermissionDefList, changePermission, resultChangePermissionEntity, resultList, resultSequence);
				} catch (Exception e){
					throw e;
				}
			}

			if(TaskResultCode.ERROR_PROCESS.equals(code)
				|| TaskResultCode.ERROR_PROCESS.equals(codeByDefFile)) {

				throw new TriSystemException(BmMessageId.BM004012F);
			}

			if(TaskResultCode.UN_PROCESS.equals(code)
					&& TaskResultCode.UN_PROCESS.equals(codeByDefFile)) {

				resultChangePermissionEntity.setCode( TaskResultCode.UN_PROCESS );

			} else if(TaskResultCode.SUCCESS.equals(code)
					|| TaskResultCode.SUCCESS.equals(codeByDefFile)) {

				resultChangePermissionEntity.setCode( TaskResultCode.SUCCESS );
			} else {
//				throw new IllegalStateException(BmMessageId.BM005009S , code , codeByDefFile );
				throw new IllegalStateException( ac.getMessage( BmMessageId.BM005009S , code , codeByDefFile ) );
			}

		} catch ( Exception e ) {
			resultChangePermissionEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005010S , e );
		} finally {
			resultChangePermissionEntity.setResult( resultList.toArray( new TaskResultEntity[ 0 ] ) );
			prmTarget.addTaskResult( resultChangePermissionEntity );
		}

		return resultChangePermissionEntity;
	}

	/**
	 * 定義ファイルによる、パーミッション変更をする。
	 * @param chPermissionDefList パーミッション変更定義情報のリスト
	 * @param changePermission パーミッション変更タスク
	 * @param resultChangePermissionEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param resultSequence
	 * @return 指定された処理結果（詳細）
	 */
	private String doChangePermissionByDefFile(
							List<ChPermissionDefinition> chPermissionDefList,
							ITaskChangePermissionEntity changePermission,
							TaskResultChangePermissionEntity resultChangePermissionEntity,
							List<ITaskResultEntity> resultList,
							Integer resultSequence )
		throws Exception {

		if( null == chPermissionDefList || 0 == chPermissionDefList.size() ) {
			return TaskResultCode.UN_PROCESS;
		}

		for ( ChPermissionDefinition defFile : chPermissionDefList ) {

			if(null == defFile) {
				continue;
			}

			// リフォーム実行パスが実在しなければリフォームしないが、1個でも実在すればリフォームする
			boolean existChPermissionJudgePath = false;
			List<String> chPermissionJudgePathList = defFile.getChPermissionJudgePathList();
			for (String chPermissionJudgePath : chPermissionJudgePathList) {

				if ( null != chPermissionJudgePath && new File( chPermissionJudgePath ).exists() )
					existChPermissionJudgePath = true;
			}

			//判断パスが記述されている場合のみ、実在チェックを行う
			if( false == existChPermissionJudgePath && 0 < chPermissionJudgePathList.size() ) {
				continue;
			}

			for( ChPermissionEntry chPermissionEntry : defFile.getChPermissionPathList() ) {
				//pathごとに処理結果を記録する
				ITaskResultEntity innerResultEntity = resultChangePermissionEntity.newResult();

				innerResultEntity.setBaseDir( defFile.getBasePath() );
				innerResultEntity.setDefineDetailFilePath( defFile.getDefineDetailFilePath() );
				innerResultEntity.setDefineFilePath( changePermission.getDefineFilePath() );

				Integer detailSequence = 1;
				innerResultEntity.setSequence( String.valueOf(resultSequence++) );
				innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
				innerResultEntity.setPath( chPermissionEntry.getPath() );
				List<ITaskDetailEntity> detailList = new ArrayList<ITaskDetailEntity>();

				String concatPath = TriStringUtils.linkPathBySlash(defFile.getBasePath(), chPermissionEntry.getPath() );

				try {
					String resultCode = TaskResultCode.UN_PROCESS;
					PermissionInfo filePermissionInfo = chPermissionEntry.getFilePermissionInfo() ;
					PermissionInfo directoryPermissionInfo = chPermissionEntry.getDirectoryPermissionInfo() ;

					resultCode = this.changePermission(
										changePermission,
										concatPath,
										concatPath,
										chPermissionEntry.getDoRecursion(),
										filePermissionInfo ,
										directoryPermissionInfo ,
										innerResultEntity,
										detailList,
										detailSequence );

					if(TaskResultCode.UN_PROCESS.equals(resultCode)) {

						innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

					} else if(TaskResultCode.ERROR_PROCESS.equals(resultCode)) {

						innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );

					} else if(TaskResultCode.SUCCESS.equals(resultCode)) {

						innerResultEntity.setCode( TaskResultCode.SUCCESS );

					} else {
						throw new TriSystemException( BmMessageId.BM005011S , resultCode );
					}

				} catch ( Exception e ) {
					innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
					throw e;
				} finally {
					innerResultEntity.setDetail( detailList.toArray( new TaskDetailEntity[ 0 ] ) );

					if ( StatusFlg.on.value().equals( changePermission.getDoDetailSnap() ) ) {
						resultList.add( innerResultEntity );
					} else {
						if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
							resultList.add( innerResultEntity );
						}
					}
				}	// try節
			}	// 定義１行毎のfor文
		}	// 定義ファイル毎のfor文

		boolean hasErr = false;
		boolean hasSuc = false;
		for (ITaskResultEntity entity : resultList) {
			String codeTmp = entity.getCode();
			if(TaskResultCode.ERROR_PROCESS.equals(codeTmp)) {
				hasErr = true;
			}
			if(TaskResultCode.SUCCESS.equals(codeTmp)) {
				hasSuc = true;
			}
		}
		if(true == hasErr) {
			return TaskResultCode.ERROR_PROCESS;
		} else if(true == hasSuc) {
			return TaskResultCode.SUCCESS;
		} else {
			return TaskResultCode.UN_PROCESS;
		}
	}

	/**
	 * 定義ファイルによらない、パーミッション情報を付与する。
	 * @param changePermission パーミッション変更タスク
	 * @param resultZipEntity 処理結果エンティティ
	 * @param resultList 処理結果（詳細）リスト
	 * @param resultSequence
	 * @return 処理結果（詳細）の実行順序
	 */
	private String doChangePermission(
							ITaskChangePermissionEntity changePermission,
							TaskResultChangePermissionEntity resultChangePermissionEntity,
							List<ITaskResultEntity> resultList,
							Integer resultSequence)
		throws Exception {

		ITaskChangePermissionEntity.ITaskParamEntity[] paramArray = changePermission.getParam();
		if( TriStringUtils.isEmpty( paramArray ) ) {
			return TaskResultCode.UN_PROCESS;
		}

		for( ITaskParamEntity param : paramArray ) {

			if(null == param) {
				continue;
			}

			//pathごとに処理結果を記録する
			Integer detailSequence = 1;
			ITaskResultEntity innerResultEntity = resultChangePermissionEntity.newResult();
			innerResultEntity.setSequence( String.valueOf(resultSequence++) );
			innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
			innerResultEntity.setPath( param.getPath() );
			List<ITaskDetailEntity> detailList = new ArrayList<ITaskDetailEntity>();

			try {
				String resultCode = TaskResultCode.UN_PROCESS;
				//ファイルパーミッション
				PermissionInfo filePermissionInfo = new PermissionInfo(	param.getPermission() ,
																		param.getOwner() ,
																		param.getGroup() ) ;
				//ディレクトリパーミッション
				//※すべてがNullの場合には、下位互換モードとして、ディレクトリに対しても、ファイルパーミッションを用いる
				PermissionInfo directoryPermissionInfo = null ;
				if( null == param.getDirectoryPermission() &&
					null == param.getDirectoryOwner() &&
					null == param.getDirectoryGroup() ) {
					directoryPermissionInfo = filePermissionInfo ;
				} else {
					directoryPermissionInfo = new PermissionInfo( 	param.getDirectoryPermission() ,
																	param.getDirectoryOwner() ,
																	param.getDirectoryGroup() ) ;
				}

				LogHandler.debug( log , "ファイルのパーミッション設定：" + param.getPermission() + " " + param.getOwner() + " " + param.getGroup() ) ;
				LogHandler.debug( log , "ディレクトリのパーミッション設定：" + param.getDirectoryPermission() + " " + param.getDirectoryOwner() + " " + param.getDirectoryGroup() ) ;

				resultCode = this.changePermission(
									changePermission,
									new File(param.getPath()).getPath(),
									new File(param.getPath()).getPath(),
									param.getDoRecursion(),
									filePermissionInfo,
									directoryPermissionInfo ,
									innerResultEntity,
									detailList,
									detailSequence );

				if(TaskResultCode.UN_PROCESS.equals(resultCode)) {

					innerResultEntity.setCode( TaskResultCode.UN_PROCESS );

				} else if(TaskResultCode.ERROR_PROCESS.equals(resultCode)) {

					innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );

				} else if(TaskResultCode.SUCCESS.equals(resultCode)) {

					innerResultEntity.setCode( TaskResultCode.SUCCESS );

				} else {
					throw new TriSystemException( BmMessageId.BM005011S , resultCode );
				}

			} catch ( Exception e ) {
				innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
				throw e;
			} finally {
				innerResultEntity.setDetail( detailList.toArray( new TaskDetailEntity[ 0 ] ) );

				if ( StatusFlg.on.value().equals( changePermission.getDoDetailSnap() ) ) {
					resultList.add( innerResultEntity );
				} else {
					if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
						resultList.add( innerResultEntity );
					}
				}
			}
		}

		boolean hasErr = false;
		boolean hasSuc = false;
		for (ITaskResultEntity entity : resultList) {
			String codeTmp = entity.getCode();
			if(TaskResultCode.ERROR_PROCESS.equals(codeTmp)) {
				hasErr = true;
			}
			if(TaskResultCode.SUCCESS.equals(codeTmp)) {
				hasSuc = true;
			}
		}
		if(true == hasErr) {
			return TaskResultCode.ERROR_PROCESS;
		} else if(true == hasSuc) {
			return TaskResultCode.SUCCESS;
		} else {
			return TaskResultCode.UN_PROCESS;
		}
	}

	/**
	 * パーミッション変更定義リストファイルを読み込み、パーミッション変更定義情報のリストを返す。
	 * @param defListFile パーミッション変更定義リストファイル
	 * @return パーミッション変更定義情報のリスト
	 */
	private List<ChPermissionDefinition> getChPermissionDefFileList( File defListFile ) throws Exception {

		List<File> defList = TaskDefineFileUtils.getDefFileList( defListFile );

		List<ChPermissionDefinition> chPermissionDefList = new ArrayList<ChPermissionDefinition>();

		for ( File defFile : defList ) {
			chPermissionDefList.add( this.getChPermissionDefinition( defFile ));
		}

		return chPermissionDefList;
	}

	/**
	 * パーミッション変更定義ファイルを読み込み、パーミッション変更定義情報を返す。
	 * @param defFile パーミッション変更定義ファイル
	 * @return パーミッション変更定義情報
	 */
	private ChPermissionDefinition getChPermissionDefinition( File defFile ) throws Exception {

		ChPermissionDefinition jd	= new ChPermissionDefinition();
		jd.setDefineDetailFilePath( TriStringUtils.convertPath( defFile.getPath() ) );

		int lineNumber	= 0;

		try {
			Map<String,String> aliasDic = new HashMap<String,String>();

			String[] lineArray = TriFileUtils.readFileLine( defFile , false ) ;
			for( String line : lineArray ) {
				lineNumber++;

				// コメント、空白行はスキップ
				if ( !TaskDefineFileUtils.isContents( line ) ) {
					continue;
				}
				// 変数の定義行は、変数登録をしてスキップ
				if ( TaskDefineFileUtils.isAlias( line, aliasDic ) ) {
					continue;
				}

				// 変数を置換
				line = TaskDefineFileUtils.replaceAliasVariable( line, aliasDic );

				// パーミッション変更対象ファイル／ディレクトリ トップ(ルート)ディレクトリの定義行でがあれば、設定してスキップ
				if ( setBaseDir( line, jd )) {
					continue;
				}
				// パーミッション変更実行判断ディレクトリの定義行でがあれば、設定してスキップ
				if ( setChPermissionJudgePath( line, jd )) {
					continue;
				}

				// 上記以外だったら、アーカイブのパッケージ定義行と判断して設定
				setChPermissionPathValue( line, jd );
			}

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004013F , e , defFile.getAbsolutePath() , String.valueOf(lineNumber) );
		}
		return jd;
	}
	/**
	 * 定義ファイル パラメータの列挙型
	 */
	private enum PermissionDefinitionParam {

		FORMATED_PATH		( 0 ) ,	//パス							【必須】
		DO_RECURSION		( 1 ) ,	//ディレクトリ再帰の有無		【必須】
		FILE_PERMISSION		( 2 ) ,	//ファイルのパーミッション		【任意】
		FILE_OWNER			( 3 ) ,	//ファイルのオーナー			【任意】
		FILE_GROUP			( 4 ) ,	//ファイルのグループ			【任意】
		DIRECTORY_PERMISSION( 5 ) ,	//ディレクトリのパーミッション	【任意】
		DIRECTORY_OWNER		( 6 ) ,	//ディレクトリのオーナー		【任意】
		DIRECTORY_GROUP		( 7 ) ;	//ディレクトリのグループ		【任意】

		private int value ;

		PermissionDefinitionParam( int value ) {
			this.value = value;
		}
		//使用されていない
//		public int getValue() {
//			return this.value ;
//		}

		public static final int PARAM_COUNT_WITHOUT_DIRECTORY		= 5 ;//旧フォーマット（ディレクトリパーミッション指定なし）
    	public static final int PARAM_COUNT_WITH_DIRECTORY		= 8 ;//新フォーマット（ディレクトリパーミッション指定あり）
    	public static final String SEPARATOR = "," ;
	}
    /**
     * パーミッション変更対象のパス情報の値を取得する
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setChPermissionPathValue( String line, ChPermissionDefinition jd ) {

		String[] chPermissionPath = TriStringUtils.split( line , PermissionDefinitionParam.SEPARATOR , true , true ) ;
    	//String[] chPermissionPath = line.split( PermissionDefinitionParam.SEPARATOR );

		//項目数チェック（旧フォーマットor新フォーマット）それ以外の個数はエラー
		if ( PermissionDefinitionParam.PARAM_COUNT_WITHOUT_DIRECTORY != chPermissionPath.length &&
				PermissionDefinitionParam.PARAM_COUNT_WITH_DIRECTORY != chPermissionPath.length ) {
			LogHandler.warn( log , TriLogMessage.LBM0002 , line , String.valueOf(chPermissionPath.length) ) ;
			return false;
		}

		//必須項目チェック
		for( int paramColumn = 0 ; paramColumn < chPermissionPath.length ; paramColumn++ ) {
			//空白部は除去する
			chPermissionPath[ paramColumn ] = chPermissionPath[ paramColumn ].trim() ;

			if( TriStringUtils.isEmpty( chPermissionPath[ paramColumn ] ) ) {
				if( PermissionDefinitionParam.FORMATED_PATH.value == paramColumn ) {//必須項目のみチェックする:FORMATED_PATH
					LogHandler.warn( log , TriLogMessage.LBM0004 , line ) ;
					return false;
				}
				if( PermissionDefinitionParam.DO_RECURSION.value == paramColumn ) {//必須項目のみチェックする:DO_RECURSION
					LogHandler.warn( log , TriLogMessage.LBM0005 , line ) ;
					return false;
				}
			}
		}

		// ↓の処理でタスクからの変数展開ができるが、遅くなりそうだしメリットがなさそうなのでとりあえずはしない
//		String srcPath = TaskStringUtil.substitutionPath( this.propertyEntity , reformPath[0] );
		String formatedPath = this.editParam( chPermissionPath[ PermissionDefinitionParam.FORMATED_PATH.value ] );
		String doRecursion = this.editParam( chPermissionPath[ PermissionDefinitionParam.DO_RECURSION.value ] );

		//ファイルのパーミッション情報
		String filePermission = this.editParam( chPermissionPath[ PermissionDefinitionParam.FILE_PERMISSION.value ] );
		if( false == checkPermissionNumber( filePermission ) ) {
			LogHandler.warn( log , TriLogMessage.LBM0006 , line ) ;
			return false;
		}
		String fileOwner = this.editParam( chPermissionPath[ PermissionDefinitionParam.FILE_OWNER.value ] );
		String fileGroup = this.editParam( chPermissionPath[ PermissionDefinitionParam.FILE_GROUP.value ] );
		PermissionInfo filePermissionInfo = new PermissionInfo( filePermission , fileOwner , fileGroup ) ;
		//ディレクトリのパーミッション情報
		PermissionInfo directoryPermissionInfo = null ;
		if( PermissionDefinitionParam.PARAM_COUNT_WITH_DIRECTORY <= chPermissionPath.length ) {
			String directoryPermission = this.editParam( chPermissionPath[ PermissionDefinitionParam.DIRECTORY_PERMISSION.value ] );
			if( false == checkPermissionNumber( directoryPermission ) ) {
				LogHandler.warn( log , TriLogMessage.LBM0006 , line ) ;
				return false;
			}
			String directoryOwner = this.editParam( chPermissionPath[ PermissionDefinitionParam.DIRECTORY_OWNER.value ] );
			String directoryGroup = this.editParam( chPermissionPath[ PermissionDefinitionParam.DIRECTORY_GROUP.value ] );
			directoryPermissionInfo = new PermissionInfo( directoryPermission , directoryOwner , directoryGroup ) ;
		} else {//下位互換モード（ディレクトリパーミッションのエントリが存在しない場合、ファイルパーミッションを用いる）
			directoryPermissionInfo = filePermissionInfo ;
		}

		//パーミッション設定値抜けのチェック
		if( TriStringUtils.isEmpty( filePermissionInfo.group ) &&
			TriStringUtils.isEmpty( filePermissionInfo.owner ) &&
			TriStringUtils.isEmpty( filePermissionInfo.permission ) &&
			TriStringUtils.isEmpty( directoryPermissionInfo.group ) &&
			TriStringUtils.isEmpty( directoryPermissionInfo.owner ) &&
			TriStringUtils.isEmpty( directoryPermissionInfo.permission ) ) {
			LogHandler.warn( log ,TriLogMessage.LBM0007 , line ) ;
			return false;
		}

		ChPermissionEntry cpEntry = jd.newChPermissionEntry();
		cpEntry.setPath(formatedPath);
		cpEntry.setDoRecursion(doRecursion);

		cpEntry.setFilePermissionInfo( filePermissionInfo );
		cpEntry.setDirectoryPermissionInfo( directoryPermissionInfo ) ;

		jd.getChPermissionPathList().add( cpEntry );

		return true;
    }
    /**
     *
     * @param permission
     * @return
     */
    private boolean checkPermissionNumber( String permission ) {
    	final int LEN_PERMISSION = 3 ; //000 ～ 999

    	if( TriStringUtils.isEmpty( permission ) ) {
    		return true ;
    	}
    	if( LEN_PERMISSION != permission.length() ) {
    		return false ;
    	}
    	try {
    		int permissionVal = Integer.valueOf( permission )  ;
    		if( 0 > permissionVal || permissionVal > 1000 ) {//000 ～ 999
    			return false ;
    		}
    	} catch( NumberFormatException e ) {
    		return false ;
    	}
    	return true ;
    }
    /**
     *
     * @param param
     * @return
     */
    private String editParam( String param ) {
    	return TriStringUtils.convertPath( param ).trim() ;
    }
	/**
     * パーミッション変更対象ファイル／ディレクトリ トップ(ルート)ディレクトリの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setBaseDir( String line, ChPermissionDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, BASE_DIR );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.setBasePath( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
     * パーミッション変更判断パスの定義を設定する。
     * @return 設定すればtrue、そうでなければfalse
     */
    private boolean setChPermissionJudgePath( String line, ChPermissionDefinition jd ) throws Exception {

    	String value = TaskDefineFileUtils.getValue( line, JUDGE_PATH );
    	if ( null == value ) {
    		return false;
    	} else {
    		String newValue = TaskStringUtils.substitutionPath( this.propertyEntity , value );	// タスクからの変数を展開
    		jd.getChPermissionJudgePathList().add( TriStringUtils.convertPath( newValue ) );
    		return  true;
    	}
    }

	/**
	 * 再帰的にパーミッションの変更処理を行う<br>
	 * @param changePermission パーミッションタスク
	 * @param param パーミッション設定情報
	 * @param basePath 基準パス
	 * @param curPath 再帰処理時のカレントパス
	 * @param doRecursion 再帰オプション
	 * @param filePermissionInfo ファイルのパーミッション情報
	 * @param directoryPermissionInfo ディレクトリのパーミッション情報
	 * @param innerResultEntity
	 * @param detailList
	 * @param detailSequence
	 * @return 処理結果（詳細）の実行順序
	 * @throws Exception
	 */
	private String changePermission(
						ITaskChangePermissionEntity changePermission,
						String basePath,
						String curPath,
						String doRecursion,
						PermissionInfo filePermissionInfo ,
						PermissionInfo directoryPermissionInfo ,
						ITaskResultEntity innerResultEntity,
						List<ITaskDetailEntity> detailList,
						Integer detailSequence ) throws Exception {

		final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

		if( null == curPath ) {
			throw new TriSystemException( BmMessageId.BM005012S );
		}

		File file = new File( curPath );
		if( true != file.exists() ) {
			return TaskResultCode.UN_PROCESS;	//パスが存在しない場合にそのままスルーするように変更
		}

		ITaskDetailEntity innerDetailEntity = innerResultEntity.newDetail();
		innerDetailEntity.setSequence( String.valueOf(detailSequence++) );
		innerDetailEntity.setCode( TaskResultCode.UN_PROCESS );
		innerDetailEntity.setRelativePath( TriStringUtils.convertRelativePath(new File(basePath), TriStringUtils.convertPath(file.getPath())) );

		try {
			if ( StatusFlg.on.value().equals(changePermission.getDoSimulate()) ) {
				innerDetailEntity.setCode( TaskResultCode.UN_PROCESS );
			} else {
				//ファイルorディレクトリにより、パーミッション設定を切り替える。
				PermissionInfo permissionInfo = ( true == file.isFile() ) ? filePermissionInfo : directoryPermissionInfo ;
				if( null != permissionInfo ) {
					LogHandler.debug( log , "before change pemission : " + file.getAbsolutePath() + " " + permissionInfo.getPermission() + " " + permissionInfo.getGroup() + " " + permissionInfo.getOwner() ) ;
					if( true != TriStringUtils.isEmpty( permissionInfo.getPermission() ) ) {
						this.executeCommand( COMMAND_CHMOD , file.getPath() , permissionInfo.getPermission() );
						innerDetailEntity.setCode( TaskResultCode.SUCCESS );
					}//パラメータが設定されなかった場合は、実行しない
					if( true != TriStringUtils.isEmpty( permissionInfo.getGroup() ) ) {
						this.executeCommand( COMMAND_CHGRP , file.getPath() , permissionInfo.getGroup() );
						innerDetailEntity.setCode( TaskResultCode.SUCCESS );
					}//パラメータが設定されなかった場合は、実行しない
					if( true != TriStringUtils.isEmpty( permissionInfo.getOwner() ) ) {
						this.executeCommand( COMMAND_CHOWN , file.getPath() , permissionInfo.getOwner() );
						innerDetailEntity.setCode( TaskResultCode.SUCCESS );
					}//パラメータが設定されなかった場合は、実行しない
				}
			}

		} catch ( Exception e ) {
			innerDetailEntity.setCode( TaskResultCode.ERROR_PROCESS );
		} finally {
			if ( StatusFlg.on.value().equals( changePermission.getDoDetailSnap() ) ) {
				detailList.add( innerDetailEntity );
			} else {
				if ( TaskResultCode.ERROR_PROCESS.equals( innerDetailEntity.getCode() ) ) {
					detailList.add( innerDetailEntity );
				}
			}
		}

		boolean hasErr = false;
		boolean hasSuc = false;
		if( StatusFlg.on.value().equals( doRecursion ) && file.isDirectory() ) {
			//「再帰処理あり、ディレクトリ指定」の場合
			File[] files = file.listFiles();
			for( File newFile : files ) {
				String codeTmp = this.changePermission(
									changePermission,
									basePath,
									newFile.getPath(),
									doRecursion,
									filePermissionInfo,
									directoryPermissionInfo ,
									innerResultEntity,
									detailList,
									detailSequence );

				if(TaskResultCode.ERROR_PROCESS.equals(codeTmp)) {
					hasErr = true;
				}
				if(TaskResultCode.SUCCESS.equals(codeTmp)) {
					hasSuc = true;
				}
			}
		}

		if(true == hasErr) {
			return TaskResultCode.ERROR_PROCESS;
		} else if(true == hasSuc) {
			return TaskResultCode.SUCCESS;
		} else {
			String resultCode = innerDetailEntity.getCode();
			if(TaskResultCode.UN_PROCESS.equals(resultCode)) {

				return TaskResultCode.UN_PROCESS;

			} else if(TaskResultCode.ERROR_PROCESS.equals(resultCode)) {

				return TaskResultCode.ERROR_PROCESS;

			} else if(TaskResultCode.SUCCESS.equals(resultCode)) {

				return TaskResultCode.SUCCESS;

			} else {
				throw new IllegalStateException( ac.getMessage(BmMessageId.BM005011S, resultCode ) );
			}
		}
	}

	/**
	 * UNIXシェル上でコマンドを実行する<br>
	 * コマンドは、<コマンド名> <パラメータ> <ファイル名> を想定。<br>
	 * @param command コマンド
	 * @param path ファイルパス
	 * @param parameter パラメータ
	 * @throws Exception
	 */
	private void executeCommand( String command , String path , String parameter ) throws Exception {
		LogUtil logUtil = new LogUtilLog4j( TriLogFactory.getInstance() , LogUtil.LOG_TYPE_CHANGEPERMISSION );
		TaskCommandExecutor localCommandExecute = new TaskCommandExecutor( logUtil );

		boolean result = false;
		String[] val = null;

		if ( TriSystemUtils.isWindows() ) {
			// Windowsでの実行(テスト)
			throw new BusinessException( BmMessageId.BM004014F );
		} else {
			// Linuxでの実行(本番)
			val = new String[] { command , parameter , path };
		}
		LogHandler.debug( log , "executeCommand: " + command + " " + parameter + " " + path ) ;
		result = localCommandExecute.executeCommand( val );
		if ( true != result ) {
			throw new BusinessException( BmMessageId.BM004015F , command , parameter , path );
		}
	}
	/**
	 * パーミッション設定情報を保持するための内部クラス
	 *
	 */
	private class PermissionInfo {

		private String permission = null ;
		private String owner = null ;
		private String group = null ;

		/**
		 * コンストラクタ
		 * @param permission パーミッション情報
		 * @param owner オーナー情報
		 * @param group グループ情報
		 */
		public PermissionInfo( String permission , String owner , String group ) {
			this.permission = permission ;
			this.owner = owner ;
			this.group = group ;
		}

		public String getGroup() {
			return group;
		}
		//ローカルで使用されない
//		public void setGroup(String group) {
//			this.group = group;
//		}
		public String getOwner() {
			return owner;
		}
//		public void setOwner(String owner) {
//			this.owner = owner;
//		}
		public String getPermission() {
			return permission;
		}
//		public void setPermission(String permission) {
//			this.permission = permission;
//		}

	}

 	/**
	 * パーミッション変更定義ファイルの情報を保持するクラス
	 */
	public class ChPermissionDefinition {

		/** 定義ファイルパス */
		private String defineDetailFilePath = null;
		/** パーミッション変更対象 トップ(ルート)ディレクトリ */
		private String basePath = null;
		/** パーミッション変更判断パス */
		private List<String> chPermissionJudgePathList = new ArrayList<String>();
		/** パーミッション変更する対象のファイル／ディレクトリ情報 */
		private List<ChPermissionEntry> chPermissionPathList = new ArrayList<ChPermissionEntry>();

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getBasePath() {
			return basePath;
		}
		public void setBasePath( String basePath ) {
			this.basePath = basePath;
		}

		public List<String> getChPermissionJudgePathList() {
			return chPermissionJudgePathList;
		}
		public void setChPermissionJudgePathList( List<String> chPermissionJudgePathList ) {
			this.chPermissionJudgePathList = chPermissionJudgePathList;
		}

		public List<ChPermissionEntry> getChPermissionPathList() {
			return chPermissionPathList;
		}
		public void setChPermissionFileList( List<ChPermissionEntry> chPermissionPathList ) {
			this.chPermissionPathList = chPermissionPathList;
		}

		public ChPermissionEntry newChPermissionEntry() {
			return new ChPermissionEntry();
		}

		public class ChPermissionEntry {

			/** パス */
			private String path = null;
			/** 再帰オプション */
			private String doRecursion = null;
			/** ファイルのパーミッション情報 */
			PermissionInfo filePermissionInfo = null ;
			/** ディレクトリのパーミッション情報 */
			PermissionInfo directoryPermissionInfo = null ;

			public String getPath() {
				return path;
			}
			public void setPath( String path ) {
				this.path = path;
			}
			public String getDoRecursion() {
				return doRecursion;
			}
			public void setDoRecursion(String doRecursion) {
				this.doRecursion = doRecursion;
			}
			public PermissionInfo getFilePermissionInfo() {
				return filePermissionInfo;
			}
			public void setFilePermissionInfo(PermissionInfo filePermissionInfo) {
				this.filePermissionInfo = filePermissionInfo;
			}
			public PermissionInfo getDirectoryPermissionInfo() {
				return directoryPermissionInfo;
			}
			public void setDirectoryPermissionInfo(PermissionInfo directoryPermissionInfo) {
				this.directoryPermissionInfo = directoryPermissionInfo;
			}

		}

	}

}
