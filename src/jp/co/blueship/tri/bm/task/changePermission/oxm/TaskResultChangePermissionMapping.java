package jp.co.blueship.tri.bm.task.changePermission.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskResultChangePermissionEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskResultChangePermissionEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskResultChangePermissionEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * ChangePermissionタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultChangePermissionMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			ChangePermissionResultType[] results = src.getChangePermissionArray();

			if( null != results ) {
				for( ChangePermissionResultType result : results ) {
					ITaskResultChangePermissionEntity resultEntity = new TaskResultChangePermissionEntity();

					copyProperties( result , resultEntity );

					jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionResultType.Result[] innerResultArray = result.getResultArray();

					if( null != innerResultArray && 0 < innerResultArray.length ) {
						List<ITaskResultEntity> paramList = new ArrayList<ITaskResultEntity>();

						for( jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionResultType.Result innerResult : innerResultArray ) {
							ITaskResultEntity innerResultEntity = resultEntity.newResult();
							copyProperties( innerResult , innerResultEntity );

							paramList.add( innerResultEntity );
						}
						ITaskResultEntity[] paramArray = paramList.toArray( new ITaskResultEntity[ 0 ] );
						resultEntity.setResult( paramArray );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultChangePermissionEntity) )
					continue;

				ITaskResultChangePermissionEntity resultEntity = (ITaskResultChangePermissionEntity)entity;

				ChangePermissionResultType result = dest.addNewChangePermission();

				copyProperties( resultEntity, result );

				ITaskResultEntity[] innerResultEntityArray = resultEntity.getResult();
				if( null != innerResultEntityArray ) {
					for( ITaskResultEntity innerResultEntity : innerResultEntityArray ) {
						jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionResultType.Result innerResult = result.addNewResult();

						copyProperties( innerResultEntity, innerResult );
					}
				}
			}
		}

		return dest;
	}

}
