package jp.co.blueship.tri.bm.task.changePermission.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

public class TaskChangePermissionEntity extends TaskTaskEntity implements ITaskChangePermissionEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String defineFilePath = null;
	private ITaskParamEntity[] param = new ITaskParamEntity[0];
	private String doSimulate = StatusFlg.off.value();
	private String doDetailSnap = StatusFlg.off.value();

	/**
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null;
		private String doRecursion = StatusFlg.on.value();
		private String permission  = null;
		private String owner = null;
		private String group = null;
		private String directoryPermission  = null;
		private String directoryOwner = null;
		private String directoryGroup = null;

		public String getDoRecursion() {
			return doRecursion;
		}
		public void setDoRecursion(String doRecursion) {
			this.doRecursion = doRecursion;
		}
		public String getGroup() {
			return group;
		}
		public void setGroup(String group) {
			this.group = group;
		}
		public String getOwner() {
			return owner;
		}
		public void setOwner(String owner) {
			this.owner = owner;
		}
		public String getPath() {
			return path;
		}
		public void setPath(String path) {
			this.path = path;
		}
		public String getPermission() {
			return permission;
		}
		public void setPermission(String permission) {
			this.permission = permission;
		}
		public String getDirectoryGroup() {
			return directoryGroup;
		}
		public void setDirectoryGroup(String directoryGroup) {
			this.directoryGroup = directoryGroup;
		}
		public String getDirectoryOwner() {
			return directoryOwner;
		}
		public void setDirectoryOwner(String directoryOwner) {
			this.directoryOwner = directoryOwner;
		}
		public String getDirectoryPermission() {
			return directoryPermission;
		}
		public void setDirectoryPermission(String directoryPermission) {
			this.directoryPermission = directoryPermission;
		}

	}

	public String getDefineFilePath() {
		return defineFilePath;
	}

	public void setDefineFilePath(String defineFilePath) {
		this.defineFilePath = defineFilePath;
	}

	public String getDoSimulate() {
		return doSimulate ;
	}
	public void setDoSimulate( String value ) {
		doSimulate = value ;
	}

	public String getDoDetailSnap() {
		return doDetailSnap;
	}

	public void setDoDetailSnap(String doDetailSnap) {
		this.doDetailSnap = doDetailSnap;
	}

	public ITaskParamEntity[] getParam() {
		return param;
	}

	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}
}
