package jp.co.blueship.tri.bm.task.changePermission.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultChangePermissionEntity extends TaskTaskResultEntity implements ITaskResultChangePermissionEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];

	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null;
		private ITaskDetailEntity[] detail  = new ITaskDetailEntity[0];
		private String baseDir = null;
		private String defineDetailFilePath = null;
		private String defineFilePath = null;


		public ITaskDetailEntity newDetail() {
			return new TaskDetailEntity();
		}

		public ITaskDetailEntity[] getDetail() {
			return detail;
		}

		public void setDetail(ITaskDetailEntity[] detail) {
			this.detail = detail;
		}

		public String getPath() {
			return path;
		}

		public void setPath(String path) {
			this.path = path;
		}

		public String getBaseDir() {
			return baseDir;
		}

		public void setBaseDir(String baseDir) {
			this.baseDir = baseDir;
		}

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}

		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}

		public String getDefineFilePath() {
			return defineFilePath;
		}

		public void setDefineFilePath(String defineFilePath) {
			this.defineFilePath = defineFilePath;
		}

	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskDetailEntity extends TaskTaskResultEntity implements ITaskDetailEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String relativePath = null;

		public String getRelativePath() {
			return relativePath;
		}
		public void setRelativePath(String relativePath) {
			this.relativePath = relativePath;
		}
	}


	public ITaskResultEntity[] getResult() {
		return result;
	}

	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}

}
