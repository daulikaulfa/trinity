package jp.co.blueship.tri.bm.task.changePermission.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskResultChangePermissionEntity extends ITaskResultTypeEntity {

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult( ITaskResultEntity[] value );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();

	/**
	 * 結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineFilePath();
		/**
		 * 外部定義ファイル（定義リスト）
		 *
		 * @param value 外部定義ファイル（定義リスト）
		 */
		public void setDefineFilePath(String value);
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDefineDetailFilePath();
		/**
		 * 外部定義ファイル（詳細定義リスト）
		 *
		 * @param value 外部定義ファイル（詳細定義リスト）
		 */
		public void setDefineDetailFilePath(String value);
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getPath();
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @param value 対象ディレクトリ／ファイル
		 */
		public void setPath( String value );
		/**
		 * パーミッション変更対象の基点ディレクトリ。
		 * 外部定義ファイルを指定した場合、定義内容のコピー元情報が設定。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getBaseDir();
		/**
		 * コピー元のディレクトリ／ファイル。
		 * 外部定義ファイルを指定した場合、定義内容のコピー元情報が設定。
		 *
		 * @param value コピー元のディレクトリ／ファイル
		 */
		public void setBaseDir( String value );
		/**
		 * 再帰的な結果情報
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskDetailEntity[] getDetail();
		/**
		 * 再帰的な結果情報
		 *
		 * @param values 再帰的な結果情報
		 */
		public void setDetail( ITaskDetailEntity[] values );
		/**
		 * 再帰的な結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskDetailEntity newDetail();

	}

	/**
	 *
	 * 再帰的な結果情報のインナーインターフェイス
	 *
	 */
	public interface ITaskDetailEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * pathからの相対パス。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getRelativePath();
		/**
		 * pathからの相対パス。
		 *
		 * @param value destからの相対パス
		 */
		public void setRelativePath( String value );
	}

}
