package jp.co.blueship.tri.bm.task.changePermission.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskChangePermissionEntity extends IEntity, ITaskTaskTypeEntity {

	/**
	 * 圧縮定義ファイルパスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDefineFilePath();
	/**
	 * 圧縮定義ファイルパスを設定します。
	 *
	 * @param value 定義ファイルパス
	 */
	public void setDefineFilePath( String value );

	/**
	 * パラメタ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskParamEntity[] getParam();
	/**
	 * パラメタ情報を設定します。
	 *
	 * @param value
	 */
	public void setParam( ITaskParamEntity[] value );

	/**
	 * 実際にパーミション変更を行うかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoSimulate() ;
	/**
	 * 実際にパーミション変更を行うかどうかを設定します。
	 *
	 * @param value 実際にパーミション変更を行わない場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 * それ以外は、パーミション変更を行う。
	 */
	public void setDoSimulate( String value ) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );

	/**
	 * パラメタ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskParamEntity extends IEntity {
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getPath();
		/**
		 * 対象ディレクトリ／ファイル
		 *
		 * @param value 対象ディレクトリ／ファイル
		 */
		public void setPath( String value );
		/**
		 * pathがディレクトリの場合、再帰的にディレクトリ内のファイルを処理するかどうかを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDoRecursion();
		/**
		 * pathがディレクトリの場合、再帰的にディレクトリ内のファイルを処理するかどうかを設定します。
		 *
		 * @param value 再帰的に処理する場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
		 */
		public void setDoRecursion( String value );
		/**
		 * パーミッション値
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getPermission();
		/**
		 * パーミッション値
		 *
		 * @param value
		 */
		public void setPermission( String value );
		/**
		 * 所有者名
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getOwner();
		/**
		 * 所有者名
		 *
		 * @param value 所有者名
		 */
		public void setOwner( String value );
		/**
		 * グループ名
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getGroup();
		/**
		 * グループ名
		 *
		 * @param value グループ名
		 */
		public void setGroup( String value );

		/**
		 * ディレクトリパーミッション値
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDirectoryPermission();
		/**
		 * ディレクトリパーミッション値
		 *
		 * @param value
		 */
		public void setDirectoryPermission( String value );
		/**
		 * ディレクトリ所有者名
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDirectoryOwner();
		/**
		 * ディレクトリ所有者名
		 *
		 * @param value ディレクトリ所有者名
		 */
		public void setDirectoryOwner( String value );
		/**
		 * ディレクトリグループ名
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getDirectoryGroup();
		/**
		 * ディレクトリグループ名
		 *
		 * @param value ディレクトリグループ名
		 */
		public void setDirectoryGroup( String value );
	}

}
