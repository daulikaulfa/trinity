package jp.co.blueship.tri.bm.task.changePermission.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.ITaskChangePermissionEntity;
import jp.co.blueship.tri.bm.task.changePermission.oxm.eb.TaskChangePermissionEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionType;
import jp.co.blueship.tri.fw.schema.beans.task.ChangePermissionType.Param;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * ChangePermissionタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskChangePermissionMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		ChangePermissionType[] tasks = src.getChangePermissionArray();

		if( null != tasks ) {
			for( ChangePermissionType task : tasks ) {
				TaskChangePermissionEntity taskEntity = new TaskChangePermissionEntity() ;

				copyProperties( task, taskEntity ) ;

				Param[] params = task.getParamArray() ;

				if( null != params && 0 < params.length ) {
					List<TaskChangePermissionEntity.TaskParamEntity> paramList = new ArrayList<TaskChangePermissionEntity.TaskParamEntity>();

					for( Param param : params ) {
						TaskChangePermissionEntity.TaskParamEntity paramEntity = taskEntity.new TaskParamEntity();
						copyProperties( param , paramEntity ) ;

						paramList.add( paramEntity );
					}
					TaskChangePermissionEntity.TaskParamEntity[] paramArray = paramList.toArray( new TaskChangePermissionEntity.TaskParamEntity[ 0 ] );
					taskEntity.setParam( paramArray );
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultChangePermissionMapping resultMapping = new TaskResultChangePermissionMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskChangePermissionEntity) )
					continue;

				TaskChangePermissionEntity taskEntity = (TaskChangePermissionEntity)entity;

				ChangePermissionType task = dest.addNewChangePermission();

				copyProperties( taskEntity, task );

				ITaskChangePermissionEntity.ITaskParamEntity[] paramArray = taskEntity.getParam();

				if( null != paramArray ) {
					for( ITaskChangePermissionEntity.ITaskParamEntity paramEntity : paramArray ) {
						Param param = task.addNewParam();
						copyProperties( paramEntity, param );
					}
				}
			}
		}

		//Result
		TaskResultChangePermissionMapping resultMapping = new TaskResultChangePermissionMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
