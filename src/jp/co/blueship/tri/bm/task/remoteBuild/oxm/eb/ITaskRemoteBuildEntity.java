package jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskRemoteBuildEntity extends ITaskTaskTypeEntity {

	public String getName() ;
	public void setName( String name ) ;

	public String getStartTriggerPath() ;
	public void setStartTriggerPath( String startTriggerPath ) ;

	public String getSkipTriggerPath() ;
	public void setSkipTriggerPath( String skipTriggerPath ) ;

	public String getSucceededResultPath() ;
	public void setSucceededResultPath( String succeededResultPath ) ;

	public String getFailedResultPath() ;
	public void setFailedResultPath( String failedResultPath ) ;

	public String getIntervalTime() ;
	public void setIntervalTime( String intervalTime ) ;

	public ITaskBuildPathEntity getBuildPathEntity() ;
	public void setBuildPathEntity( ITaskBuildPathEntity obj ) ;

	/**
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskBuildPathEntity extends IEntity {

		public ITaskPathTypeEntity[] getIntensiveSrcParam() ;
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] obj ) ;
		public ITaskPathTypeEntity[] getDeleteSrcParam() ;
		public void setDeleteSrcParam( ITaskPathTypeEntity[] obj ) ;
	}

	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskPathTypeEntity extends IEntity {

		public String getPath() ;
		public void setPath( String value ) ;
	}
}
