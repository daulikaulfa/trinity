package jp.co.blueship.tri.bm.task.remoteBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.ITaskRemoteBuildEntity;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.TaskRemoteBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.PathType;
import jp.co.blueship.tri.fw.schema.beans.task.RemoteBuildType;
import jp.co.blueship.tri.fw.schema.beans.task.RemoteBuildType.BuildPath;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * RemoteBuildタスク用に、XMLBeansのEntityとDataBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskRemoteBuildMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( TargetType src, ITaskTargetEntity dest ) {

		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		RemoteBuildType[] tasks = src.getRemoteBuildArray();

		if ( null != tasks ) {
			for ( RemoteBuildType task : tasks ) {

				TaskRemoteBuildEntity taskEntity = new TaskRemoteBuildEntity();

				copyProperties( task , taskEntity ) ;

				BuildPath buildPath = task.getBuildPath() ;
				if ( null != buildPath ) {

					ITaskRemoteBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.new TaskBuildPathEntity() ;
					copyProperties( buildPath , buildPathEntity ) ;

					//intensiveSrc
					if ( null != buildPath.getIntensiveSrcArray() ) {

						List<TaskRemoteBuildEntity.TaskPathTypeEntity> intensiveSrcList = new ArrayList<TaskRemoteBuildEntity.TaskPathTypeEntity>() ;

						for ( PathType srcPath : buildPath.getIntensiveSrcArray() ) {

							TaskRemoteBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath	( srcPath.getPath() ) ;
							intensiveSrcList.add	( pathTypeEntity ) ;
						}
						buildPathEntity.setIntensiveSrcParam( intensiveSrcList.toArray( new TaskRemoteBuildEntity.TaskPathTypeEntity[0] )) ;
					}

					//deleteSrc
					if ( null != buildPath.getDeleteSrcArray() ) {

						List<TaskRemoteBuildEntity.TaskPathTypeEntity> deleteSrcList = new ArrayList<TaskRemoteBuildEntity.TaskPathTypeEntity>() ;

						for ( PathType srcPath : buildPath.getDeleteSrcArray() ) {

							TaskRemoteBuildEntity.TaskPathTypeEntity pathTypeEntity = taskEntity.new TaskPathTypeEntity() ;
							pathTypeEntity.setPath	( srcPath.getPath() ) ;
							deleteSrcList.add	( pathTypeEntity ) ;
						}
						buildPathEntity.setDeleteSrcParam( deleteSrcList.toArray( new TaskRemoteBuildEntity.TaskPathTypeEntity[0] )) ;
					}

					taskEntity.setBuildPathEntity( buildPathEntity ) ;
				}

				entitys.add( taskEntity ) ;
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		//Result
		TaskResultRemoteBuildMapping resultMapping = new TaskResultRemoteBuildMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans( ITaskTargetEntity src, TargetType dest ) {

		ITaskTaskTypeEntity[] entitys =src.getTask();

		if ( null != entitys ) {

			for ( ITaskTaskTypeEntity entity : entitys ) {

				if ( !( entity instanceof ITaskRemoteBuildEntity )) continue;

				ITaskRemoteBuildEntity taskEntity = (ITaskRemoteBuildEntity)entity;
				RemoteBuildType task = dest.addNewRemoteBuild() ;

				copyProperties( taskEntity, task );

				ITaskRemoteBuildEntity.ITaskBuildPathEntity buildPathEntity = taskEntity.getBuildPathEntity() ;
				if ( null != buildPathEntity ) {
					BuildPath buildPath = task.addNewBuildPath() ;

					copyProperties( buildPathEntity , buildPath ) ;

					//intensiveSrc
					ITaskRemoteBuildEntity.ITaskPathTypeEntity[] intensiveSrcEntityArray = buildPathEntity.getIntensiveSrcParam() ;
					if ( null != intensiveSrcEntityArray ) {

						for ( ITaskRemoteBuildEntity.ITaskPathTypeEntity srcEntity : intensiveSrcEntityArray ) {

							PathType pathType = buildPath.addNewIntensiveSrc() ;
							copyProperties( srcEntity , pathType ) ;
						}
					}
					//deleteSrc
					ITaskRemoteBuildEntity.ITaskPathTypeEntity[] deleteSrcEntityArray = buildPathEntity.getDeleteSrcParam() ;
					if ( null != deleteSrcEntityArray ) {

						for ( ITaskRemoteBuildEntity.ITaskPathTypeEntity srcEntity : deleteSrcEntityArray ) {

							PathType pathType = buildPath.addNewDeleteSrc() ;
							copyProperties( srcEntity , pathType ) ;
						}
					}
				}
			}
		}

//		Result
		TaskResultRemoteBuildMapping resultMapping = new TaskResultRemoteBuildMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
