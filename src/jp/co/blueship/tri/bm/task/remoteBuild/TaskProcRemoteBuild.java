package jp.co.blueship.tri.bm.task.remoteBuild;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.ITaskRemoteBuildEntity;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.ITaskRemoteBuildEntity.ITaskPathTypeEntity;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.TaskRemoteBuildEntity;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.TaskResultRemoteBuildEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「remoteBuild」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとにremoteビルドタスクの実行を行う。
 */
public class TaskProcRemoteBuild implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;
	public void setProperty( ITaskPropertyEntity[] propertyEntity ) {
		this.propertyEntity = propertyEntity;
	}

	// デフォルトの待ち時間（10秒）
	private long defaultIntervalTime = 10000L;

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {

		TaskRemoteBuildEntity build = (TaskRemoteBuildEntity) task ;

		build.setStartTriggerPath	( TaskStringUtils.substitutionPath( propertyEntity , build.getStartTriggerPath() )) ;
		build.setSkipTriggerPath	( TaskStringUtils.substitutionPath( propertyEntity , build.getSkipTriggerPath() )) ;
		build.setSucceededResultPath( TaskStringUtils.substitutionPath( propertyEntity , build.getSucceededResultPath() )) ;
		build.setFailedResultPath	( TaskStringUtils.substitutionPath( propertyEntity , build.getFailedResultPath() )) ;

		ITaskRemoteBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity() ;
		if ( null != buildPathEntity ) {
			//","区切りの複数パラメータに対応
			buildPathEntity.setIntensiveSrcParam( substitutePathSubPathType( build , buildPathEntity.getIntensiveSrcParam() )) ;//IntensiveSrcParam
			buildPathEntity.setDeleteSrcParam( substitutePathSubPathType( build , buildPathEntity.getDeleteSrcParam() ) ) ;//DeleteSrcParam
		}
	}
	/**
	 *
	 * @param build
	 * @param pathEntityArray
	 * @return
	 * @throws Exception
	 */
	private ITaskRemoteBuildEntity.ITaskPathTypeEntity[] substitutePathSubPathType(
			TaskRemoteBuildEntity build , ITaskRemoteBuildEntity.ITaskPathTypeEntity[] pathEntityArray ) throws Exception {

		List<ITaskRemoteBuildEntity.ITaskPathTypeEntity> arrlst = new ArrayList<ITaskRemoteBuildEntity.ITaskPathTypeEntity>() ;

		for ( ITaskRemoteBuildEntity.ITaskPathTypeEntity pathEntity : pathEntityArray ) {

			String[] arrays =TaskStringUtils.substitutionPaths( propertyEntity , pathEntity.getPath() ) ;
			if ( null != arrays ) {

				for ( String array : arrays ) {

					ITaskRemoteBuildEntity.ITaskPathTypeEntity entity = build.new TaskPathTypeEntity() ;
					TriPropertyUtils.copyProperties( entity, pathEntity );
					entity.setPath( array ) ;
					arrlst.add( entity ) ;
				}
			}
		}
		return arrlst.toArray( new ITaskRemoteBuildEntity.ITaskPathTypeEntity[ 0 ] ) ;
	}

	/**
	 * 「remoteBuild」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {

		if ( !( prmTask instanceof ITaskRemoteBuildEntity )) return null ;

		TaskResultRemoteBuildEntity resultRemoteBuildEntity = new TaskResultRemoteBuildEntity() ;

		String taskResultCode = TaskResultCode.UN_PROCESS ;
		resultRemoteBuildEntity.setSequence( prmTask.getSequence() ) ;
		resultRemoteBuildEntity.setTaskId( prmTask.getTaskId() );

		File triggerFile			= null;
		File succeededResultFile	= null;
		File failedResultFile		= null;

		try {
			substitutePath( prmTask ) ;

			ITaskRemoteBuildEntity build = (ITaskRemoteBuildEntity) prmTask ;
			ITaskRemoteBuildEntity.ITaskBuildPathEntity buildPathEntity = build.getBuildPathEntity() ;

			if ( null != buildPathEntity ) {

				boolean buildGo = false ;
				/** 削除資産のチェック **/
				//deleteSrcのパスをチェックし、すべての削除対象ファイルが原本に存在していればビルド処理を実行、１件でも存在しなければスルーする

				String[] deleteSrcArray = convertPathArray( buildPathEntity.getDeleteSrcParam() ) ;
				 ;
				if( TriStringUtils.isEmpty( deleteSrcArray ) ) {//deleteSrcのパス自体が設定されていない⇒intensiveSrcのみで実行可否を判断する。
					//buildGo = true ;//パス定義が存在しない場合、intensiveSrcのみで実行可否を判断する。
				} else {
					for( String path : deleteSrcArray ) {
						FileFilter filter = BusinessFileUtils.getFileFilter();

						buildGo |= TriFileUtils.checkExistFiles( path , false, filter ) ;
						if( true == buildGo ) {
							break ;
						}
					}
				}
				/** 返却資産のチェック **/
				// intensiveSrcのパスを再帰的に辿って、返却資産が１件でもあればビルド処理実行トリガーファイルを、
				// １件もなければビルド処理スルートリガーファイルを作成する。
				// また、intensiveSrcのパス自体が設定されていない場合には、ビルド処理実行トリガーファイルを作成する。
				String[] intensiveSrcArray = convertPathArray( buildPathEntity.getIntensiveSrcParam() ) ;

				// intensiveSrcのパス自体が設定されていない⇒ビルド実行
				if ( TriStringUtils.isEmpty( intensiveSrcArray ) ) {
					buildGo = true ;
				} else {
					FileFilter filter = BusinessFileUtils.getFileFilter();

					for ( String path : intensiveSrcArray ) {
						buildGo |= TriFileUtils.checkExistFiles( path , false, filter ) ;
						if ( buildGo ) {
							break ;
						}
					}
				}


				// 相手シェルの返事待ちチェック間隔
				long waitTime = getIntervalTime( build );

				// 結果ファイルの作成
				succeededResultFile	= createResultFile( build.getSucceededResultPath() );
				failedResultFile	= createResultFile( build.getFailedResultPath() );

				// トリガーファイルの作成
				triggerFile = createTriggerFile( build, buildGo );


				// ビルド実行、不実行ともに、相手の返事を待つ
				while ( true ) {
					try {
				         Thread.sleep( waitTime );
						// 結果ファイルのチェック
				        // 正常終了
						if ( succeededResultFile.exists() ) {
							if ( buildGo ) {
								taskResultCode = TaskResultCode.SUCCESS ;
							}
							break;

						// 異常終了
						} else if ( failedResultFile.exists() ) {
							if ( buildGo ) {
								taskResultCode = TaskResultCode.ERROR_PROCESS ;
								throw new TriSystemException( BmMessageId.BM005025S ) ;
							}
							break;
						}
					} catch ( InterruptedException e ) {
						throw new TriSystemException( BmMessageId.BM005026S, e );
					}
				}
			}

		} catch ( TriSystemException e ) {
			throw e ;
		} catch ( Exception e ) {
			taskResultCode = TaskResultCode.ERROR_PROCESS ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005027S,e);
		} finally {

			try {

				deleteFile( triggerFile );
				deleteFile( succeededResultFile );
				deleteFile( failedResultFile );

			} catch ( Exception e ) {

				taskResultCode = TaskResultCode.ERROR_PROCESS ;
				throw new TriSystemException(BmMessageId.BM005027S,e);

			} finally {

				resultRemoteBuildEntity.setCode( taskResultCode ) ;
				prmTarget.addTaskResult( resultRemoteBuildEntity ) ;
			}
		}

		return resultRemoteBuildEntity ;
	}

	/**
     * 結果ファイルの作成を行う
     * @param resultPath
     * @return 結果ファイルオブジェクト
     */
    private File createResultFile( String resultPath ) {

    	if ( TriStringUtils.isEmpty( resultPath )) {
    		throw new TriSystemException( BmMessageId.BM004047F );
    	}


    	File resultFile = new File( resultPath );
    	if ( resultFile.exists() ) {
			throw new TriSystemException(BmMessageId.BM004045F , resultFile.getPath() );
    	}


    	if ( !resultFile.getParentFile().exists() ) {
			if ( !resultFile.getParentFile().mkdirs() ) {
				throw new TriSystemException(BmMessageId.BM004046F , resultFile.getParentFile().getPath() );
			}
		}

		return resultFile;
    }

	/**
	 * 結果ファイルの待ち時間を取得する
	 * @param build
	 * @return
	 */
	private long getIntervalTime( ITaskRemoteBuildEntity build ) {

		long intervalTime = this.defaultIntervalTime;

		String intervalTimeString = build.getIntervalTime();
		if ( TriStringUtils.isEmpty( intervalTimeString )) return intervalTime;


		try {
			long tmpIntervalTime = Long.parseLong( intervalTimeString );
			if ( 0 < tmpIntervalTime ) {
				intervalTime = tmpIntervalTime;
			}

		} catch ( NumberFormatException e ) {
			// 変換できなければデフォルト値を使う
		}

		return intervalTime;
	}

	/**
     * トリガーファイルの作成を行う
     * @param build
     * @param goBuild true：ビルド実施、false：ビルド未実施
     * @return トリガーファイル
     */
    private File createTriggerFile( ITaskRemoteBuildEntity build, boolean buildGo ) {

    	String triggerPath = null;
    	if ( buildGo ) {
    		triggerPath = build.getStartTriggerPath();
		} else {
			triggerPath = build.getSkipTriggerPath();
		}
    	if ( TriStringUtils.isEmpty( triggerPath )) {
    		throw new TriSystemException( BmMessageId.BM004048F );
    	}


    	File triggerFile = new File( triggerPath );
    	if ( triggerFile.exists() ) {
			throw new TriSystemException(BmMessageId.BM004049F , triggerFile.getPath() );
    	}


    	try {

			if ( !triggerFile.getParentFile().exists() ) {
				if ( !triggerFile.getParentFile().mkdirs() ) {
					throw new TriSystemException(BmMessageId.BM004050F , triggerFile.getParentFile().getPath() );
				}
			}

			triggerFile.createNewFile();

		} catch ( IOException e ) {
			throw new TriSystemException( BmMessageId.BM004051F, e );
		}

		return triggerFile;
    }

    /**
     * ファイルの削除を行う
     */
    private void deleteFile( File file ) {

    	if ( null != file && file.exists() ) {
    		if ( !file.delete() ) {
    			throw new TriSystemException( BmMessageId.BM004052F , file.getPath());
    		}
    	}
    }

	/**
	 * ITaskPathTypeEntityオブジェクトの配列からのsrc文字列のみを配列に格納して返す<br>
	 * @param pathEntityArrayオブジェクト
	 * @return src文字列の配列
	 */
	private String[] convertPathArray( ITaskPathTypeEntity[] pathEntityArray ) {

		List<String> list = new ArrayList<String>() ;

		if( null != pathEntityArray ) {

			for( ITaskRemoteBuildEntity.ITaskPathTypeEntity pathEntity : pathEntityArray ) {
				list.add( pathEntity.getPath() ) ;
			}
		}

		return list.toArray( new String[ 0 ] ) ;
	}
}
