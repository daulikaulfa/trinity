package jp.co.blueship.tri.bm.task.remoteBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.ITaskResultRemoteBuildEntity;
import jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb.TaskResultRemoteBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.RemoteBuildResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * CBuildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultRemoteBuildMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if ( null != src ) {

			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			RemoteBuildResultType[] results = src.getRemoteBuildArray();

			if ( null != results ) {

				for ( RemoteBuildResultType result : results ) {

					TaskResultRemoteBuildEntity resultEntity = new TaskResultRemoteBuildEntity() ;
					copyProperties( result , resultEntity ) ;

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0] ));
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans( ITaskTargetEntity src, Result dest ) {

		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if ( null != entitys ) {

			for ( ITaskResultTypeEntity entity : entitys ) {

				if ( !( entity instanceof ITaskResultRemoteBuildEntity )) continue;

				TaskResultRemoteBuildEntity resultEntity = (TaskResultRemoteBuildEntity)entity;

				RemoteBuildResultType result = dest.addNewRemoteBuild();
				copyProperties( resultEntity, result );
			}
		}

		return dest;
	}

}
