package jp.co.blueship.tri.bm.task.remoteBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskRemoteBuildEntity extends TaskTaskEntity implements ITaskRemoteBuildEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String name							= null ;
	private String startTriggerPath				= null ;
	private String skipTriggerPath					= null ;
	private String succeededResultPath				= null ;
	private String failedResultPath				= null ;
	private String intervalTime					= null ;

	private ITaskBuildPathEntity buildPathEntity	= null ;

	public String getName() {
		return this.name ;
	}
	public void setName( String name ) {
		this.name = name ;
	}

	public String getStartTriggerPath() {
		return this.startTriggerPath ;
	}
	public void setStartTriggerPath( String startTriggerPath ) {
		this.startTriggerPath = startTriggerPath ;
	}

	public String getSkipTriggerPath() {
		return this.skipTriggerPath ;
	}
	public void setSkipTriggerPath( String skipTriggerPath ) {
		this.skipTriggerPath = skipTriggerPath ;
	}

	public String getSucceededResultPath() {
		return this.succeededResultPath ;
	}
	public void setSucceededResultPath( String succeededResultPath ) {
		this.succeededResultPath = succeededResultPath ;
	}

	public String getFailedResultPath() {
		return this.failedResultPath ;
	}
	public void setFailedResultPath( String failedResultPath ) {
		this.failedResultPath = failedResultPath ;
	}

	public String getIntervalTime() {
		return this.intervalTime ;
	}
	public void setIntervalTime( String intervalTime ) {
		this.intervalTime = intervalTime ;
	}

	public ITaskBuildPathEntity getBuildPathEntity() {
		return this.buildPathEntity ;
	}
	public void setBuildPathEntity( ITaskBuildPathEntity buildPathEntity ) {
		this.buildPathEntity = buildPathEntity ;
	}


	/**
	 * インナークラス
	 *
	 */
	public class TaskBuildPathEntity implements ITaskBuildPathEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private ITaskPathTypeEntity[] intensiveSrcParam = new ITaskPathTypeEntity[0];
		private ITaskPathTypeEntity[] deleteSrcParam = new ITaskPathTypeEntity[0];

		public ITaskPathTypeEntity[] getIntensiveSrcParam() {
			return this.intensiveSrcParam ;
		}
		public void setIntensiveSrcParam( ITaskPathTypeEntity[] intensiveSrcParam ) {
			this.intensiveSrcParam = intensiveSrcParam ;
		}
		public ITaskPathTypeEntity[] getDeleteSrcParam() {
			return this.deleteSrcParam ;
		}
		public void setDeleteSrcParam( ITaskPathTypeEntity[] deleteSrcParam ) {
			this.deleteSrcParam = deleteSrcParam ;
		}
	}


	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskPathTypeEntity implements ITaskPathTypeEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null ;

		public String getPath() {
			return this.path ;
		}
		public void setPath( String path ) {
			this.path = path ;
		}
	}
}
