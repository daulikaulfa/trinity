package jp.co.blueship.tri.bm.task;

public class TaskFtpDelTypeEntity implements ITaskFtpDelTypeEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String sequence = null ;
	public String[] src = null ;

	/**
	 * シーケンス番号を取得します<br>
	 * @return シーケンス番号
	 */
	public String getSequence() {
		return sequence;
	}
	/**
	 * シーケンス番号を設定します<br>
	 * @param sequence シーケンス番号
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	/**
	 * 削除するファイルのソースパスを取得します<br>
	 * @return 削除するファイルのソースパス
	 */
	public String[] getSrc() {
		return src;
	}
	/**
	 * 削除するファイルのソースパスを設定します<br>
	 * @param src 削除するファイルのソースパス
	 */
	public void setSrc(String[] src) {
		this.src = src;
	}
}
