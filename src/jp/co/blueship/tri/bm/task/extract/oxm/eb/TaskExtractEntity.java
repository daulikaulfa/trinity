package jp.co.blueship.tri.bm.task.extract.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskExtractEntity extends TaskTaskEntity implements ITaskExtractEntity {

	

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String doCopy		= StatusFlg.off.value();
	private String doUnzip		= StatusFlg.off.value();
	private String extractPath	= null;
	private ITaskPathTypeEntity[] srcPath	= new ITaskPathTypeEntity[0];
	private ITaskBuildEntity[] build		= new ITaskBuildEntity[0];

	public String getDoCopy() {
		return doCopy;
	}
	public void setDoCopy( String doCopy ) {
		this.doCopy = doCopy;
	}

	public String getDoUnzip() {
		return doUnzip;
	}
	public void setDoUnzip( String doUnzip ) {
		this.doUnzip = doUnzip;
	}

	public String getExtractPath() {
		return extractPath;
	}
	public void setExtractPath( String extractPath ) {
		this.extractPath = extractPath;
	}

	public ITaskPathTypeEntity[] getSrcPath() {
		return srcPath;
	}
	public void setSrcPath( ITaskPathTypeEntity[] srcPath ) {
		this.srcPath = srcPath;
	}
	public ITaskPathTypeEntity newSrcPath() {
		return new TaskPathTypeEntity();
	}

	public ITaskBuildEntity[] getBuild() {
		return build;
	}
	public void setBuild( ITaskBuildEntity[] build ) {
		this.build = build;
	}
	public ITaskBuildEntity newBuild() {
		return new TaskBuildEntity();
	}


	/**
	 * インナークラス
	 *
	 */
	public class TaskBuildEntity implements ITaskBuildEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String mergeOrder	= null ;
		private String buildNo		= null ;

		public String getMergeOrder() {
			return mergeOrder;
		}
		public void setMergeOrder( String mergeOrder ) {
			this.mergeOrder = mergeOrder;
		}

		public String getBuildNo() {
			return buildNo;
		}
		public void setBuildNo( String buildNo ) {
			this.buildNo = buildNo;
		}
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskPathTypeEntity implements ITaskPathTypeEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String path = null ;

		public String getPath() {
			return path ;
		}
		public void setPath( String value ) {
			path = value ;
		}
	}

}
