package jp.co.blueship.tri.bm.task.extract.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskResultExtractEntity extends ITaskResultTypeEntity {

	

	/**
	 * 結果情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity[] getResult();
	/**
	 * 結果情報を設定します。
	 *
	 * @param values 結果情報
	 */
	public void setResult( ITaskResultEntity[] values );
	/**
	 * 結果情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskResultEntity newResult();


	/**
	 * 展開情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskExtractResultEntity[] getExtractResult();
	/**
	 * 展開情報を設定します。
	 *
	 * @param values 展開情報
	 */
	public void setExtractResult( ITaskExtractResultEntity[] values );
	/**
	 * 展開情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskExtractResultEntity newExtractResult();


	/**
	 *
	 * 実行結果エンティティのインナーインターフェイス
	 *
	 */
	public interface ITaskResultEntity extends IEntity, ITaskResultTypeEntity {

		/**
		 * 資産展開結果のＲＰ情報を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskBuildResultEntity getBuildResult() ;
		/**
		 * 資産展開結果のＲＰ情報を設定します。
		 *
		 * @param build 資産を展開するためのＲＰ情報
		 */
		public void setBuildResult( ITaskBuildResultEntity build ) ;
		/**
		 * 結果情報の新しいインスタンスを生成します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ITaskBuildResultEntity newBuildResult();
	}


	/**
	 *
	 * 資産展開結果のインナーインタフェースです。
	 *
	 */
	public interface ITaskBuildResultEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * マージ順を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getMergeOrder() ;
		/**
		 * マージ順を設定します。
		 *
		 * @param value マージ順
		 */
		public void setMergeOrder( String value ) ;

		/**
		 * ビルドパッケージ番号を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getBuildNo() ;
		/**
		 * ビルドパッケージ番号を設定します。
		 *
		 * @param value ビルドパッケージ番号
		 */
		public void setBuildNo( String value ) ;
	}


	/**
	 *
	 * 資産展開結果のインナーインタフェースです。
	 *
	 */
	public interface ITaskExtractResultEntity extends IEntity, ITaskResultTypeEntity {
		/**
		 * 展開したファイルパスを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getFilePath() ;
		/**
		 * 展開したファイルパスを設定します。
		 *
		 * @param value 展開したファイルパス
		 */
		public void setFilePath( String value ) ;

		/**
		 * ビルドパッケージ番号を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getBuildNo() ;
		/**
		 * ビルドパッケージ番号を設定します。
		 *
		 * @param value ビルドパッケージ番号
		 */
		public void setBuildNo( String value ) ;
	}
}
