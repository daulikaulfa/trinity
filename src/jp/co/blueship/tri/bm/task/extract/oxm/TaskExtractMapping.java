package jp.co.blueship.tri.bm.task.extract.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskExtractEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.TaskExtractEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ExtractType.Build;
import jp.co.blueship.tri.fw.schema.beans.task.PathType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Extract;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;


/**
 * Extractタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskExtractMapping extends TriXmlMappingUtils implements ITaskMapping {

	

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( TargetType src, ITaskTargetEntity dest ) {

		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Extract[] tasks = src.getExtractArray();

		if ( null != tasks ) {

			for ( Extract task : tasks ) {

				ITaskExtractEntity taskEntity = new TaskExtractEntity();
				copyProperties( task, taskEntity );


				// 資産格納パス
				PathType[] pathArray = task.getSrcPathArray();
				if ( null != pathArray ) {

					List<ITaskExtractEntity.ITaskPathTypeEntity> pathList = new ArrayList<ITaskExtractEntity.ITaskPathTypeEntity>() ;

					for ( PathType path : pathArray ) {

						ITaskExtractEntity.ITaskPathTypeEntity pathTypeEntity = taskEntity.newSrcPath() ;
						pathTypeEntity.setPath( path.getPath() ) ;

						pathList.add( pathTypeEntity ) ;
					}

					taskEntity.setSrcPath( pathList.toArray( new TaskExtractEntity.TaskPathTypeEntity[0] ) ) ;
				}


				// ＲＰ情報
				Build[] buildArray = task.getBuildArray();
				if ( null != buildArray ) {

					List<TaskExtractEntity.ITaskBuildEntity> buildList = new ArrayList<TaskExtractEntity.ITaskBuildEntity>();

					for ( Build build : buildArray ) {

						ITaskExtractEntity.ITaskBuildEntity taskBuildEntity = taskEntity.newBuild();
						copyProperties( build , taskBuildEntity );

						buildList.add( taskBuildEntity );
					}

					taskEntity.setBuild( buildList.toArray( new TaskExtractEntity.ITaskBuildEntity[ 0 ] ) );
				}


				entitys.add( taskEntity );
			}
		}
 		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

		TaskResultExtractMapping resultMapping = new TaskResultExtractMapping();
		resultMapping.mapXMLBeans2DB( src.getResult() , dest );

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans( ITaskTargetEntity src, TargetType dest ) {

		ITaskTaskTypeEntity[] entitys =src.getTask();

		if ( null != entitys ) {

			for ( ITaskTaskTypeEntity entity : entitys ) {

				if( ! (entity instanceof ITaskExtractEntity ) )
					continue;

				TaskExtractEntity taskEntity = (TaskExtractEntity)entity;

				Extract task = dest.addNewExtract();

				copyProperties( taskEntity, task );


				// 資産格納パス
				ITaskExtractEntity.ITaskPathTypeEntity[] srcPathArray = taskEntity.getSrcPath();
				if ( null != srcPathArray ) {

					for ( ITaskExtractEntity.ITaskPathTypeEntity srcPath : srcPathArray ) {

						PathType pathType = task.addNewSrcPath() ;
						copyProperties( srcPath , pathType ) ;
					}
				}


				// ＲＰ情報
				TaskExtractEntity.ITaskBuildEntity[] buildEntityArray = taskEntity.getBuild();
				if ( null != buildEntityArray ) {

					for ( TaskExtractEntity.ITaskBuildEntity buildEntity : buildEntityArray ) {
						Build build = task.addNewBuild();
						copyProperties( buildEntity , build );
					}
				}
			}
		}

		TaskResultExtractMapping resultMapping = new TaskResultExtractMapping();
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult();
		resultMapping.mapDB2XMLBeans( src , result );

		return dest;
	}
}
