package jp.co.blueship.tri.bm.task.extract.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskExtractEntity extends ITaskTaskTypeEntity {

	

	/**
	 * 資産展開方法としてコピーを行うかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoCopy();
	/**
	 * 資産展開方法としてコピーを行うかを設定します。
	 *
	 * @param value 資産展開方法としてコピーを行う場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoCopy( String value );

	/**
	 * 資産展開方法としてUnzipを行うかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoUnzip();
	/**
	 * 資産展開方法としてUnzipを行うかを設定します。
	 *
	 * @param value 資産展開方法としてUnZipを行う場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoUnzip( String value );

	/**
	 * 資産展開ディレクトリを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getExtractPath() ;
	/**
	 * 資産展開ディレクトリを設定します。
	 *
	 * @param value 資産展開ディレクトリ
	 */
	public void setExtractPath( String value ) ;

	/**
	 * 資産格納ディレクトリを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskPathTypeEntity[] getSrcPath() ;
	/**
	 * 資産格納ディレクトリを設定します。
	 *
	 * @param value 資産格納ディレクトリ
	 */
	public void setSrcPath( ITaskPathTypeEntity[] value ) ;
	/**
	 * 資産展開ディレクトリの新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskPathTypeEntity newSrcPath();

	/**
	 * 資産を展開するためのＲＰ情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskBuildEntity[] getBuild() ;
	/**
	 * 資産を展開するためのＲＰ情報を設定します。
	 *
	 * @param build 資産を展開するためのＲＰ情報
	 */
	public void setBuild( ITaskBuildEntity[] build ) ;
	/**
	 * ＲＰ情報の新しいインスタンスを生成します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskBuildEntity newBuild();


	/**
	 *
	 * 資産を展開するためのＲＰ情報エンティティのインナーインタフェースです。
	 *
	 */
	public interface ITaskBuildEntity extends IEntity {
		/**
		 * マージ順を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getMergeOrder() ;
		/**
		 * マージ順を設定します。
		 *
		 * @param value マージ順
		 */
		public void setMergeOrder( String value ) ;

		/**
		 * ビルドパッケージ番号を取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public String getBuildNo() ;
		/**
		 * ビルドパッケージ番号を設定します。
		 *
		 * @param value ビルドパッケージ番号
		 */
		public void setBuildNo( String value ) ;
	}


	/**
	 *
	 * インナーインターフェイス
	 *
	 */
	public interface ITaskPathTypeEntity extends IEntity {
		public String getPath() ;
		public void setPath( String value ) ;
	}
}
