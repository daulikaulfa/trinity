package jp.co.blueship.tri.bm.task.extract.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskResultExtractEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskResultExtractEntity.ITaskBuildResultEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.TaskResultExtractEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ExtractResultType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Extractタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultExtractMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest ) {

		if ( null != src ) {

			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			ExtractResultType[] results = src.getExtractArray();

			if ( null != results ) {

				for ( ExtractResultType result : results ) {

					ITaskResultExtractEntity resultEntity = new TaskResultExtractEntity() ;

					copyProperties( result , resultEntity ) ;

					// Result
					ExtractResultType.Result[] resultArray = result.getResultArray();

					if ( null != resultArray && 0 < resultArray.length ) {

						List<ITaskResultExtractEntity.ITaskResultEntity> innerResultList = new ArrayList<ITaskResultExtractEntity.ITaskResultEntity>();

						for ( ExtractResultType.Result innerResult : resultArray ) {

							ITaskResultExtractEntity.ITaskResultEntity innerResultEntity = resultEntity.newResult();
							//copyProperties( innerResult , innerResultEntity ) ;

							innerResultList.add( innerResultEntity );

							// Build
							if ( null != innerResult.getBuildResult() ) {

								jp.co.blueship.tri.fw.schema.beans.task.ExtractResultType.Result.BuildResult innerBuild = innerResult.getBuildResult();
								ITaskBuildResultEntity innerBuildEntity = innerResultEntity.newBuildResult();
								copyProperties( innerBuild, innerBuildEntity );

								innerResultEntity.setBuildResult( innerBuildEntity );
							}
						}
						resultEntity.setResult( innerResultList.toArray( new ITaskResultExtractEntity.ITaskResultEntity[ 0 ] ) );
					}


					// ExtractResult
					ExtractResultType.ExtractResult[] extractResultArray = result.getExtractResultArray();

					if ( null != extractResultArray && 0 < extractResultArray.length ) {

						List<ITaskResultExtractEntity.ITaskExtractResultEntity> innerExtractResultList =
							new ArrayList<ITaskResultExtractEntity.ITaskExtractResultEntity>();

						for ( ExtractResultType.ExtractResult innerExtractResult : extractResultArray ) {

							ITaskResultExtractEntity.ITaskExtractResultEntity innerExtractResultEntity = resultEntity.newExtractResult();
							copyProperties( innerExtractResult , innerExtractResultEntity ) ;

							innerExtractResultList.add( innerExtractResultEntity );

						}
						resultEntity.setExtractResult( innerExtractResultList.toArray( new ITaskResultExtractEntity.ITaskExtractResultEntity[ 0 ] ) );
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans( ITaskTargetEntity src, Result dest ) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if ( null != entitys ) {
			for ( ITaskResultTypeEntity entity : entitys ) {
				if ( ! ( entity instanceof ITaskResultExtractEntity ) )
					continue;

				ITaskResultExtractEntity resultEntity = (ITaskResultExtractEntity)entity;

				ExtractResultType result = dest.addNewExtract();

				copyProperties( resultEntity, result );

				// Result
				ITaskResultExtractEntity.ITaskResultEntity[] innerResultArray = resultEntity.getResult();

				if ( null != innerResultArray ) {

					for ( ITaskResultExtractEntity.ITaskResultEntity innerResultEntity : innerResultArray ) {

						ExtractResultType.Result innerResult = result.addNewResult();
						//copyProperties( innerResultEntity , innerResult ) ;


						// Build
						ITaskBuildResultEntity innerBuildEntity = innerResultEntity.getBuildResult();

						if ( null != innerBuildEntity ) {

							jp.co.blueship.tri.fw.schema.beans.task.ExtractResultType.Result.BuildResult innerBuild = innerResult.addNewBuildResult();
							copyProperties( innerBuildEntity, innerBuild );
						}
					}
				}


				// ExtractResult
				ITaskResultExtractEntity.ITaskExtractResultEntity[] innerExtractResultArray = resultEntity.getExtractResult();

				if ( null != innerExtractResultArray ) {

					for ( ITaskResultExtractEntity.ITaskExtractResultEntity innerExtractResultEntity : innerExtractResultArray ) {

						ExtractResultType.ExtractResult innerExtractResult = result.addNewExtractResult();
						copyProperties( innerExtractResultEntity , innerExtractResult ) ;
					}
				}

			}
		}

		return dest;
	}

}
