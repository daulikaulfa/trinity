package jp.co.blueship.tri.bm.task.extract.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultExtractEntity extends TaskTaskResultEntity implements ITaskResultExtractEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskResultEntity[] result = new ITaskResultEntity[0];
	private ITaskExtractResultEntity[] extractResult = new ITaskExtractResultEntity[0];


	public ITaskResultEntity newResult() {
		return new TaskResultEntity();
	}

	public ITaskExtractResultEntity newExtractResult() {
		return new TaskExtractResultEntity();
	}

	/**
	 * インナークラス
	 *
	 */
	public class TaskResultEntity extends TaskTaskResultEntity implements ITaskResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String defineFilePath = null;
		private String defineDetailFilePath = null;
		private String src = null;
		private String dest = null;
		private ITaskBuildResultEntity buildResult = null;

		public String getDefineDetailFilePath() {
			return defineDetailFilePath;
		}
		public void setDefineDetailFilePath(String defineDetailFilePath) {
			this.defineDetailFilePath = defineDetailFilePath;
		}
		public String getDefineFilePath() {
			return defineFilePath;
		}
		public void setDefineFilePath(String defineFilePath) {
			this.defineFilePath = defineFilePath;
		}
		public String getDest() {
			return dest;
		}
		public void setDest(String dest) {
			this.dest = dest;
		}
		public String getSrc() {
			return src;
		}
		public void setSrc(String src) {
			this.src = src;
		}

		public ITaskBuildResultEntity getBuildResult() {
			return buildResult;
		}
		public void setBuildResult( ITaskBuildResultEntity buildResult ) {
			this.buildResult = buildResult;
		}
		public ITaskBuildResultEntity newBuildResult() {
			return new TaskBuildResultEntity();
		}
	}


	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskBuildResultEntity extends TaskTaskResultEntity implements ITaskBuildResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String mergeOrder	= null ;
		private String buildNo		= null ;

		public String getMergeOrder() {
			return mergeOrder;
		}
		public void setMergeOrder( String mergeOrder ) {
			this.mergeOrder = mergeOrder;
		}

		public String getBuildNo() {
			return buildNo;
		}
		public void setBuildNo( String buildNo ) {
			this.buildNo = buildNo;
		}

	}


	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskExtractResultEntity extends TaskTaskResultEntity implements ITaskExtractResultEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 2L;

		private String buildNo		= null ;
		private String filePath	= null ;

		public String getFilePath() {
			return filePath;
		}
		public void setFilePath( String filePath ) {
			this.filePath = filePath;
		}

		public String getBuildNo() {
			return buildNo;
		}
		public void setBuildNo( String buildNo ) {
			this.buildNo = buildNo;
		}

	}

	public ITaskResultEntity[] getResult() {
		return result;
	}

	public void setResult(ITaskResultEntity[] result) {
		this.result = result;
	}

	public ITaskExtractResultEntity[] getExtractResult() {
		return extractResult;
	}
	public void setExtractResult( ITaskExtractResultEntity[] extractResult ) {
		this.extractResult = extractResult;
	}
}
