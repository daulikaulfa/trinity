package jp.co.blueship.tri.bm.task.extract;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.copy.TaskProcCopy;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskFilesetEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskCopyEntity.ITaskOverwriteEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.ITaskResultCopyEntity;
import jp.co.blueship.tri.bm.task.copy.oxm.eb.TaskCopyEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskExtractEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskExtractEntity.ITaskBuildEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskExtractEntity.ITaskPathTypeEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.ITaskResultExtractEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.TaskExtractEntity;
import jp.co.blueship.tri.bm.task.extract.oxm.eb.TaskResultExtractEntity;
import jp.co.blueship.tri.bm.task.unzip.TaskProcUnzip;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskResultUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.ITaskUnzipEntity;
import jp.co.blueship.tri.bm.task.unzip.oxm.eb.TaskUnzipEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;

/**
 * 「extract」タスクの処理内容を記述します
 * <p>
 * リリース作成を行う際、複数のビルドパッケージの資産を指定された順序でマージ（上書き）します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskProcExtract implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	@SuppressWarnings("unused")
	private static final ILog log = TriLogFactory.getInstance();

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty( ITaskPropertyEntity[] propertyEntity ) {
		this.propertyEntity = propertyEntity;
	}

	private TaskProcCopy taskProcCopy		= null;
	private TaskProcUnzip taskProcUnzip	= null;

	public void setTaskProcCopy( TaskProcCopy taskProcCopy ) {
		this.taskProcCopy = taskProcCopy;
	}
	public void setTaskProcUnzip( TaskProcUnzip taskProcUnzip ) {
		this.taskProcUnzip = taskProcUnzip;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * <br>","で区切られた複数パラメータに対応
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public final void substitutePath( ITaskTaskTypeEntity task ) throws Exception {

		TaskExtractEntity extract = (TaskExtractEntity) task;

		// 資産展開パス
		extract.setExtractPath( TaskStringUtils.substitutionPath( propertyEntity , extract.getExtractPath() ));

		// 資産格納パス
		ITaskPathTypeEntity[] srcPathArray = extract.getSrcPath();
		if ( null != srcPathArray ) {

			List<ITaskPathTypeEntity> entityList = new ArrayList<ITaskPathTypeEntity>() ;

			for ( ITaskPathTypeEntity srcPath : srcPathArray ) {

				String[] paths =TaskStringUtils.substitutionPaths( propertyEntity , srcPath.getPath() ) ;

				if ( null != paths ) {

					for ( String path : paths ) {

						ITaskPathTypeEntity entity = extract.new TaskPathTypeEntity() ;

						TriPropertyUtils.copyProperties( entity, srcPath );
						entity.setPath( path ) ;

						entityList.add( entity ) ;
					}
				}
			}

			srcPathArray = entityList.toArray( new ITaskPathTypeEntity[ 0 ] );
			extract.setSrcPath( srcPathArray );
		}


		// ＲＰ情報
		ITaskBuildEntity[] buildArray = extract.getBuild();
		if ( null != buildArray ) {

			List<ITaskBuildEntity> entityList = new ArrayList<ITaskBuildEntity>();

			for ( ITaskBuildEntity build : buildArray ) {

				String[] mergeOrders	= TaskStringUtils.substitutionPaths( propertyEntity , build.getMergeOrder() );
				String[] buildNoes		= TaskStringUtils.substitutionPaths( propertyEntity , build.getBuildNo() );

				if ( null != buildNoes ) {

					for ( int i = 0; i < buildNoes.length; i++ ) {

						ITaskBuildEntity entity = extract.new TaskBuildEntity();

						TriPropertyUtils.copyProperties( entity, build );
						entity.setMergeOrder( mergeOrders[i] );
						entity.setBuildNo	( buildNoes[i] );

						entityList.add( entity );
					}
				}
			}

			buildArray = entityList.toArray( new ITaskBuildEntity[ 0 ] );
			extract.setBuild( buildArray );
		}
	}


	/**
	 * 「extract」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */

	public final ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {

		if ( ! ( prmTask instanceof ITaskExtractEntity ) )
			return null;

		//実行結果格納領域を確保
		ITaskResultExtractEntity resultExtractEntity = new TaskResultExtractEntity();

		List<ITaskResultExtractEntity.ITaskResultEntity> innerResultList =
			new ArrayList<ITaskResultExtractEntity.ITaskResultEntity>();
		// 資産パスとＲＰ番号
		Map<String, String> assetPathbuildNoMap = new LinkedHashMap<String, String>();


		resultExtractEntity.setCode		( TaskResultCode.UN_PROCESS );
		resultExtractEntity.setSequence	( prmTask.getSequence() );
		resultExtractEntity.setTaskId( prmTask.getTaskId() );

		try {
			substitutePath( prmTask );

			ITaskExtractEntity extractEntity = (ITaskExtractEntity) prmTask;

			ITaskPathTypeEntity[] srcPaths	= extractEntity.getSrcPath();
			String extractPath				= extractEntity.getExtractPath();

			ITaskBuildEntity[] builds		= extractEntity.getBuild();


			for ( int i = 0; i < srcPaths.length; i++ ) {

				ITaskResultExtractEntity.ITaskResultEntity innerResultEntity = null;

				try {
					innerResultEntity = resultExtractEntity.newResult();
					innerResultEntity.setCode		( TaskResultCode.UN_PROCESS );
					innerResultEntity.setSequence	( String.valueOf( i ));


					if ( StatusFlg.on.value().equals( extractEntity.getDoCopy() )) {

						setAssetPathByBuildNo(
								assetPathbuildNoMap,
								innerTaskCopy( prmTarget, srcPaths[i].getPath(), extractPath ),
								builds[i].getBuildNo() );
					}

					if ( StatusFlg.on.value().equals( extractEntity.getDoUnzip() )) {

						setAssetPathByBuildNo(
								assetPathbuildNoMap,
								innerTaskUnzip( prmTarget, srcPaths[i].getPath(), extractPath ),
								builds[i].getBuildNo() );
					}

				} catch ( Exception e ) {
					innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
					throw e;
				} finally {
					ITaskResultExtractEntity.ITaskBuildResultEntity innerBuildEntity = innerResultEntity.newBuildResult();
					if ( null != builds[i].getMergeOrder() ) {
						innerBuildEntity.setMergeOrder	( builds[i].getMergeOrder() );
					}
					innerBuildEntity.setBuildNo		( builds[i].getBuildNo() );
					innerResultEntity.setBuildResult( innerBuildEntity );

					innerResultList.add( innerResultEntity );
				}

			}

			resultExtractEntity.setCode( TaskResultCode.SUCCESS );

		} catch ( Exception e ) {
			resultExtractEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005020S , e );

		} finally {
			resultExtractEntity.setResult( innerResultList.toArray( new ITaskResultExtractEntity.ITaskResultEntity[ 0 ] ));
			setExtractResult( resultExtractEntity, assetPathbuildNoMap );

			prmTarget.addTaskResult( resultExtractEntity );
		}

		return resultExtractEntity;
	}

	/**
	 * ＲＰ番号ごとの資産パスを設定する
	 * @param assetPathbuildNoMap
	 * @param treeSet
	 */
	private void setAssetPathByBuildNo( Map<String, String> assetPathbuildNoMap, Set<String> treeSet, String buildNo ) {

		for ( String path : treeSet ) {
			assetPathbuildNoMap.put( path, buildNo );
		}
	}

	/**
	 * 展開結果を設定する
	 * @param resultExtractEntity
	 * @param assetPathbuildNoMap
	 */
	private void setExtractResult(
			ITaskResultExtractEntity resultExtractEntity, Map<String, String> assetPathbuildNoMap ) {

		List<ITaskResultExtractEntity.ITaskExtractResultEntity> resultList =
			new ArrayList<ITaskResultExtractEntity.ITaskExtractResultEntity>();

		for ( Map.Entry<String, String> assetPathbuildNoEntry : assetPathbuildNoMap.entrySet() ) {

			ITaskResultExtractEntity.ITaskExtractResultEntity extractResult = resultExtractEntity.newExtractResult();
			extractResult.setFilePath	( assetPathbuildNoEntry.getKey() );
			extractResult.setBuildNo	( assetPathbuildNoEntry.getValue() );

			resultList.add( extractResult );
		}

		resultExtractEntity.setExtractResult( resultList.toArray( new ITaskResultExtractEntity.ITaskExtractResultEntity[0] ));
	}


	/**
	 * 内部的にCopyタスクを処理する。
	 *
	 * @param task タスク処理内容を保持したエンティティ
	 * @param src コピー元
	 * @param dest コピー先
	 * @return コピーしたパスツリー
	 * @throws Exception
	 */
	private Set<String> innerTaskCopy( ITaskTargetEntity prmTarget, String srcPath, String destPath ) throws Exception {

		TaskCopyEntity copy = new TaskCopyEntity();

		copy.setHoldTimestamp	( StatusFlg.on.value() );
		copy.setDoNeglect		( StatusFlg.on.value() );
		copy.setDoNotExistSkip	( StatusFlg.on.value() );
		copy.setDoDetailSnap	( StatusFlg.on.value() );
		copy.setDoRecordFile	( StatusFlg.on.value() );

		ITaskFilesetEntity fileset = copy.new TaskFilesetEntity();
		fileset.setSrc	( srcPath );
		fileset.setDest	( destPath );
		copy.setFileset	( new ITaskFilesetEntity[] { fileset } );

		ITaskOverwriteEntity overwrite = copy.new TaskOverwriteEntity();
		overwrite.setDoAdd		( StatusFlg.on.value() );
		overwrite.setDoUpdateNew( StatusFlg.on.value());
		overwrite.setDoUpdateOld( StatusFlg.on.value());

		copy.setOverwrite		( overwrite );

		ITaskResultCopyEntity resultCopyEntity = (ITaskResultCopyEntity)taskProcCopy.execute( prmTarget , copy );

		// ここに来た時点でコピーは正常終了
		Set<String> treeSet = getCopyTree( resultCopyEntity );

		return treeSet;
	}

	/**
	 * 内部的にUnzipタスクを処理する。
	 *
	 * @param prmTarget タスク処理内容を保持したエンティティ
	 * @param srcPath 比較元
	 * @param diffPath 比較先
	 * @return 処理結果を戻す。
	 * @throws Exception
	 */
	private Set<String> innerTaskUnzip( ITaskTargetEntity prmTarget, String srcPath, String destPath ) throws Exception {

		TaskUnzipEntity unzip = new TaskUnzipEntity();

		unzip.setDoNotExistSkip	( StatusFlg.on.value() );
		unzip.setDoDetailSnap	( StatusFlg.on.value() );
		unzip.setDoRecordFile	( StatusFlg.on.value() );

		ITaskUnzipEntity.ITaskParamEntity param = unzip.new TaskParamEntity();
		param.setSrc	( srcPath );
		param.setDest	( destPath );
		unzip.setParam( new ITaskUnzipEntity.ITaskParamEntity[] { param } );

		ITaskResultUnzipEntity resultUnzipEntity = (ITaskResultUnzipEntity)taskProcUnzip.execute( prmTarget , unzip );

		// ここに来た時点で展開は正常終了
		Set<String> treeSet = getUnzipTree( resultUnzipEntity );

		return treeSet;
	}


	/**
	 * コピーしたファイルツリーを取得する。
	 * @param resultCopyEntity
	 * @throws Exception
	 */
	private Set<String> getCopyTree( ITaskResultCopyEntity resultCopyEntity ) {

		Set<String> treeSet = new TreeSet<String>();

		for ( ITaskResultCopyEntity.ITaskResultEntity resultEntity : resultCopyEntity.getResult() ) {

			for ( ITaskResultCopyEntity.ITaskCopyFileEntity copyFileEntity : resultEntity.getCopyFile() ) {

				treeSet.add( copyFileEntity.getPath() );
			}
		}

		return treeSet;
	}

	/**
	 * 展開したファイルツリーを取得する。
	 * @param resultCopyEntity
	 * @throws Exception
	 */
	private Set<String> getUnzipTree( ITaskResultUnzipEntity resultUnzipEntity ) {

		Set<String> treeSet = new TreeSet<String>();

		for ( ITaskResultUnzipEntity.ITaskResultEntity resultEntity : resultUnzipEntity.getResult() ) {

			for ( ITaskResultUnzipEntity.ITaskUnzipFileEntity unzipFileEntity : resultEntity.getUnzipFile() ) {

				treeSet.add( unzipFileEntity.getPath() );
			}
		}

		return treeSet;
	}
}
