package jp.co.blueship.tri.bm.task;

/**
 * @version V4.00.00
 * @author Nguyen Van Chung
 */
public class CharsetInfoEntity implements ICharsetInfoEntity {

	private static final long serialVersionUID = 1L;
	private String in;
	private String out;

	public String getIn() {
		return in;
	}
	public void setIn(String in) {
		this.in = in;
	}
	public String getOut() {
		return out;
	}
	public void setOut(String out) {
		this.out = out;
	}

}
