package jp.co.blueship.tri.bm.task;

import jp.co.blueship.tri.fw.dao.orm.IEntity;

public interface ITaskFtpDelTypeEntity extends IEntity {

	/**
	 * シーケンス番号を取得します<br>
	 * @return シーケンス番号
	 */
	public String getSequence() ;
	/**
	 * シーケンス番号を設定します<br>
	 * @param sequence シーケンス番号
	 */
	public void setSequence(String sequence) ;
	/**
	 * 削除するファイルのソースパスを取得します<br>
	 * @return 削除するファイルのソースパス
	 */
	public String[] getSrc() ;
	/**
	 * 削除するファイルのソースパスを設定します<br>
	 * @param src 削除するファイルのソースパス
	 */
	public void setSrc(String[] src) ;
}
