package jp.co.blueship.tri.bm.task;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;

public class TaskStringUtils {

	private static final String SEPARATOR_COMMA = "," ;

	private static final String PROPERTY_TAG_BEGIN = "{@" ;
	private static final String PROPERTY_TAG_END = "}" ;
	private static final int RETRY_COUNT = 999 ;

	/**
	 * 代理キーに該当する文字を、PropertyEntityの候補の中から探し、一致するものがあれば置換する<br>
	 * 以下に示すように、パラメータが単独の場合と複数の場合で動きが異なる。<br>
	 * パラメータが複数の場合は、","で区切ること。<br>
	 * <pre>
	 * ////////////////////////////////////////////////////////////////////
	 * 【パラメータが単独の場合】
	 * src="/home/trinity/user/原本管理フォルダ/lotXX/{@masterDeletePath}
	 *
	 * property.name="masterDeletePath"
	 * property.value="SwingSet/src/AA.java"
	 *
	 * 《結果》
	 * /@src="/home/trinity/user/原本管理フォルダ/lotXX/SwingSet/src/AA.java
	 *
	 * ////////////////////////////////////////////////////////////////////
	 *
	 * 【パラメータが複数の場合】
	 * src="/home/trinity/user/原本管理フォルダ/lotXX/{@masterDeletePath}
	 *
	 * property.name="masterDeletePath"
	 * property.value="SwingSet/src/AA.java,SwingSet/src/BB.java"
	 *
	 * 《結果》
	 * /@src="/home/trinity/user/原本管理フォルダ/lotXX/SwingSet/src/AA.java
	 * /@src="/home/trinity/user/原本管理フォルダ/lotXX/SwingSet/src/BB.java
	 *
	 * /////////////////////////////////////////////////////////////////////
	 * </pre>
	 * @param propertyEntity 置換候補とする、代理キーとパスのセットを格納したITaskPropertyEntity
	 * @param path 処理対象とするパス文字列
	 * @return 処理後のパス文字列の配列
	 * @throws Exception
	 */
	public static String[] substitutionPaths( ITaskPropertyEntity[] propertyEntityArray , String path ) throws Exception {

		if( null == propertyEntityArray || null == path ) {
			return new String[]{ path } ;
		}
		propertyEntityArray = substitutionPathProperty( propertyEntityArray ) ;

		List<String> pathList = new ArrayList<String>() ;
		pathList.add( path );								// 置換した結果複数になるのに合わせて事前に1個のリストにしておく

		for( ITaskPropertyEntity property : propertyEntityArray ) {
			if( null == property.getName() || null == property.getValue() ) {
				continue ;
			}
			//プロパティ名側にタグが付加されていない場合は"{@" "}"を付加する
			String name = property.getName() ;
			name = ( true != name.startsWith( PROPERTY_TAG_BEGIN ) ) ? PROPERTY_TAG_BEGIN + name : name ;
			name = ( true != name.endsWith( PROPERTY_TAG_END ) ) ? name + PROPERTY_TAG_END : name ;

			List<String> tmpPathList = new ArrayList<String>() ;	// 置換後のtemporary用格納リスト
			for (String tmpPath : pathList) {						// 置換した結果複数になったらその分まわす

				if( -1 != tmpPath.indexOf( name ) ) {//プロパティ名と一致
					String[] values = null ;
					if( -1 != property.getValue().indexOf( SEPARATOR_COMMA ) ) {
						values = property.getValue().split( SEPARATOR_COMMA ) ;
					} else {
						values = new String[]{ property.getValue() } ;
					}
					for( String value : values ) {
						String getPath = StringUtils.replace( tmpPath , name , value );
						tmpPathList.add( getPath ) ;		// 置換後のtemporary用に格納
					}
				}
			}

			if(! tmpPathList.isEmpty()) {	// 置換結果を差し替える
				pathList.clear() ;			// temporaryが変更されてないのにclearすると全部台無しになるので注意
				pathList.addAll(tmpPathList) ;
			}
		}
		return pathList.toArray( new String[ 0 ] ) ;
	}

	/**
	 * 代理キーに該当する文字を、PropertyEntityの候補の中から探し、一致するものがあれば置換する<br>
	 *
	 * @param propertyEntity 置換候補とする、代理キーとパスのセットを格納したITaskPropertyEntity
	 * @param path 処理対象とするパス文字列
	 * @return 処理後のパス文字列
	 * @throws Exception
	 */
	public static String substitutionPath( ITaskPropertyEntity[] propertyEntityArray , String path ) throws Exception {
		if( null == propertyEntityArray || null == path ) {
			return path;
		}
		propertyEntityArray = substitutionPathProperty( propertyEntityArray ) ;

		for( ITaskPropertyEntity property : propertyEntityArray ) {
			if( null == property.getName() || null == property.getValue() ) {
				continue ;
			}
			//			プロパティ名側にタグが付加されていない場合は"{@" "}"を付加する
			String name = property.getName() ;
			name = ( true != name.startsWith( PROPERTY_TAG_BEGIN ) ) ? PROPERTY_TAG_BEGIN + name : name ;
			name = ( true != name.endsWith( PROPERTY_TAG_END ) ) ? name + PROPERTY_TAG_END : name ;

			if( -1 != path.indexOf( property.getName() ) ) {//プロパティ名と一致
				path = StringUtils.replace( path , name , property.getValue() );
			}
		}

		return path ;
	}
	/**
	 * PropertyEntity自身に含まれる、代理キーに該当する文字を、PropertyEntityの候補の中から探し、一致するものがあれば置換する<br>
	 * @param propertyEntityArray 置換候補とする、代理キーとパスのセットを格納したITaskPropertyEntity
	 * @return 置換後の、代理キーとパスのセットを格納したITaskPropertyEntity
	 * @throws Exception
	 */
	private static ITaskPropertyEntity[] substitutionPathProperty( ITaskPropertyEntity[] propertyEntityArray ) throws Exception {
		if( null == propertyEntityArray ) {
			return propertyEntityArray ;
		}
		for( ITaskPropertyEntity propertyEntity : propertyEntityArray ) {
			if( null == propertyEntity.getName() || null == propertyEntity.getValue() ) {
				continue ;
			}
			int retryCount = RETRY_COUNT ;
			while( true ) {
				if( -1 != propertyEntity.getValue().indexOf( PROPERTY_TAG_BEGIN ) &&
						-1 != propertyEntity.getValue().indexOf( PROPERTY_TAG_END ) ) {//タグが存在する
					//
					for( ITaskPropertyEntity curPropertyEntity : propertyEntityArray ) {
						if( null == curPropertyEntity.getName() || null == curPropertyEntity.getValue() ) {
							continue ;
						}
						//							プロパティ名側にタグが付加されていない場合は"{@" "}"を付加する
						String name = curPropertyEntity.getName() ;
						name = ( true != name.startsWith( PROPERTY_TAG_BEGIN ) ) ? PROPERTY_TAG_BEGIN + name : name ;
						name = ( true != name.endsWith( PROPERTY_TAG_END ) ) ? name + PROPERTY_TAG_END : name ;

						if( -1 != propertyEntity.getValue().indexOf( curPropertyEntity.getName() ) ) {//プロパティ名と一致
							propertyEntity.setValue( StringUtils.replace( propertyEntity.getValue() , name , curPropertyEntity.getValue() ) ) ;
						}
					}
					//
					if( 0 >= retryCount-- ) {//リトライ回数オーバー⇒タグが残っていても抜ける（SystemExceptionにする？）
						break ;
					}
				} else {//置換するべきタグがなくなったので抜ける
					break ;
				}
			}
		}
		return propertyEntityArray ;
	}

	/**
	 * 正規表現文字をキャンセルしたリテラル文字列を生成する<br>
	 * キャンセルしたい正規表現該当文字は必要におうじて適宜、追加すること。<br>
	 * @param src 文字列
	 * @return リテラル文字列
	 */
	public static String toLiteral(String src) {
		if( -1 == src.indexOf( '\\' ) &&
				-1 == src.indexOf( '$' ) &&
				-1 == src.indexOf( '{' ) &&
				-1 == src.indexOf( '}' ) ) {
			return src;
		}

		StringBuilder stb = new StringBuilder();

		for (int i=0; i < src.length(); i++) {
			char c = src.charAt( i ) ;
			if ( '\\' == c ) {
				stb.append( '\\' ) ;
				stb.append( '\\' ) ;
			} else if ( '$' == c ) {
				stb.append( '\\' ) ;
				stb.append( '$' ) ;
			} else if ( '{' == c ) {
				stb.append( '\\' ) ;
				stb.append( '{' ) ;
			} else if ( '}' == c ) {
				stb.append( '\\' ) ;
				stb.append( '}' ) ;
			} else {
				stb.append( c ) ;
			}
		}
		return stb.toString() ;
	}
}
