package jp.co.blueship.tri.bm.task.exe.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;

public interface ITaskResultExeEntity extends ITaskResultTypeEntity {

	public String getOutLog() ;
	public void setOutLog( String value ) ;
	public String getErrLog() ;
	public void setErrLog( String value ) ;
}
