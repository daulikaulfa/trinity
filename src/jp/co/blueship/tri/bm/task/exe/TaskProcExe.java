package jp.co.blueship.tri.bm.task.exe;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.TaskLogUtils;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.exe.oxm.eb.ITaskExeEntity;
import jp.co.blueship.tri.bm.task.exe.oxm.eb.TaskResultExeEntity;
import jp.co.blueship.tri.fw.agent.ShellExecute;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「exe」タスクの処理内容を記述します
 * <p>
 * 定義された情報をもとにexeタスクの実行を行う。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 */
public class TaskProcExe implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 4L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		ITaskExeEntity exe = (ITaskExeEntity) task ;
		exe.setWorkDir( TaskStringUtils.substitutionPath( propertyEntity , exe.getWorkDir() ) ) ;
		exe.setCommand( TaskStringUtils.substitutionPath( propertyEntity , exe.getCommand() ) ) ;
		exe.setOutLogPath( TaskStringUtils.substitutionPath( propertyEntity, exe.getOutLogPath() ) );
		exe.setErrLogPath( TaskStringUtils.substitutionPath( propertyEntity, exe.getErrLogPath() ) );

		if( null != exe.getEnv() ) {
			List<String> envList = new ArrayList<String>() ;
			for( String env : exe.getEnv() ) {
				envList.add( TaskStringUtils.substitutionPath( propertyEntity , env ) ) ;
			}
			exe.setEnv( envList.toArray( new String[ 0 ] ) ) ;
		}
	}

	/**
	 * 「exe」タスクの実際の処理を記述します<br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskExeEntity) )
			return null ;

		TaskResultExeEntity resultExeEntity = new TaskResultExeEntity() ;
		String taskResultCode = TaskResultCode.SUCCESS ;
		resultExeEntity.setSequence( prmTask.getSequence() ) ;
		resultExeEntity.setTaskId( prmTask.getTaskId() );

		String outLog = null;
		String errLog = null;
		ITaskExeEntity exe = (ITaskExeEntity) prmTask;

		Charset inCharset = exe.getCharsetInfo() != null ? Charset.value( exe.getCharsetInfo().getIn() ) : null;

		ShellExecute shellExecute = new ShellExecute( inCharset ) ;

		try {
			substitutePath( exe ) ;

			shellExecute.setExecShellPath( exe.getCommand() ) ;
			shellExecute.setEnv( exe.getEnv() ) ;
			shellExecute.setWorkDir( exe.getWorkDir() ) ;
			shellExecute.setLog( TriLogFactory.getInstance() ) ;
			//shell実行
			shellExecute.execute() ;

			if( 0 != shellExecute.getExitValue() ) {
				throw new TriSystemException( BmMessageId.BM005007S , String.valueOf(shellExecute.getExitValue())) ;
			}

		} catch ( Exception e ) {
			taskResultCode = TaskResultCode.ERROR_PROCESS ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005019S , e );
		} finally {
			try {
				outLog = shellExecute.getOutLog();
				errLog = shellExecute.getErrLog();
				String outCharset = exe.getCharsetInfo() != null ? exe.getCharsetInfo().getOut() : null;
				resultExeEntity.setOutLog( TaskLogUtils.writeLog( exe.getOutLogPath() , outLog , Charset.value( outCharset ) ) );
				resultExeEntity.setErrLog( TaskLogUtils.writeLog( exe.getErrLogPath() , errLog , Charset.value( outCharset ) ) );
			}  catch ( Exception e ) {
				taskResultCode = TaskResultCode.ERROR_PROCESS ;
				throw new TriSystemException( BmMessageId.BM005019S , e );
			} finally {
				resultExeEntity.setCode( taskResultCode ) ;
				prmTarget.addTaskResult( resultExeEntity ) ;
			}
		}

		return resultExeEntity ;
	}

}
