package jp.co.blueship.tri.bm.task.exe.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;

public class TaskExeEntity extends TaskTaskEntity implements ITaskExeEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private String workDir = null ;
	private String command = null ;

	private String outLogPath = null ;
	private String errLogPath = null ;

	private ICharsetInfoEntity charsetInfoEntity = null ;

	/**
	 * 文字列の配列。 配列の各要素は、name=value という形式で環境変数設定を保持する。
	 * または、サブプロセスが現在のプロセスの環境を継承する場合は null
	 */
	private String[] env = null;

	/**
	 * 指定されたシステムコマンドを発行する作業ディレクトリを取得します。
	 *
	 * @return
	 */
	public String getWorkDir() {
		return workDir;
	}
	/**
	 * 指定されたシステムコマンドを発行する作業ディレクトリを設定します。
	 *
	 * @param value
	 */
	public void setWorkDir(String value ) {
		this.workDir = value ;
	}
	/**
	 * システムコマンドを取得します。
	 *
	 * @return 取得したシステムコマンド
	 */
	public String getCommand() {
		return command ;
	}
	/**
	 * システムコマンドを設定します。
	 *
	 * @param value システムコマンド
	 */
	public void setCommand( String value ) {
		command = value ;
	}
	/**
	 * 環境変数を取得します。
	 *
	 * @return 環境変数
	 */
	public String[] getEnv() {
		return env ;
	}
	/**
	 * 環境変数を設定します。
	 *
	 * @param value 環境変数
	 */
	public void setEnv( String[] value ) {
		env = value ;
	}

	/**
	 * エラーログファイルを出力する先のファイルパスを取得します。
	 *
	 * @return ファイルパス
	 */
	public String getErrLogPath() {
		return errLogPath;
	}
	/**
	 * エラーログファイルの出力する先のファイルパスを設定します。
	 *
	 * @param errLogPath ファイルパス
	 */
	public void setErrLogPath(String errLogPath) {
		this.errLogPath = errLogPath;
	}
	/**
	 * 実行結果ログファイルの出力する先のファイルパスを取得します。
	 *
	 * @return ファイルパス
	 */
	public String getOutLogPath() {
		return outLogPath;
	}
	/**
	 * 実行結果ログファイルの出力する先のファイルパスを取得します。
	 *
	 * @param outLogPath ファイルパス
	 */
	public void setOutLogPath(String outLogPath) {
		this.outLogPath = outLogPath;
	}

	public ICharsetInfoEntity getCharsetInfo() {
		return charsetInfoEntity;
	}
	public void setCharsetInfo(ICharsetInfoEntity charsetInfoEntity) {
		this.charsetInfoEntity = charsetInfoEntity;
	}

}
