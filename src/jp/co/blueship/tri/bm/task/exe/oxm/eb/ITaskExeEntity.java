package jp.co.blueship.tri.bm.task.exe.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ICharsetInfoEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskExeEntity extends ITaskTaskTypeEntity {

	/**
	 * 指定されたシステムコマンドを発行する作業ディレクトリを取得します。
	 *
	 * @return
	 */
	public String getWorkDir();
	/**
	 * 指定されたシステムコマンドを発行する作業ディレクトリを設定します。
	 *
	 * @param value
	 */
	public void setWorkDir( String value );

	/**
	 * システムコマンドを取得します。
	 *
	 * @return 取得したシステムコマンド
	 */
	public String getCommand();
	/**
	 * システムコマンドを設定します。
	 *
	 * @param value システムコマンド
	 */
	public void setCommand( String value );

	/**
	 * 環境変数を取得します。
	 *
	 * @return 環境変数
	 */
	public String[] getEnv();
	/**
	 * 環境変数を設定します。
	 *
	 * @param value 環境変数
	 */
	public void setEnv( String[] value );

	/**
	 * エラーログファイルを出力する先のファイルパスを取得します。
	 *
	 * @return ファイルパス
	 */
	public String getErrLogPath();
	/**
	 * エラーログファイルの出力する先のファイルパスを設定します。
	 *
	 * @param errLogPath ファイルパス
	 */
	public void setErrLogPath(String substitutionPath);
	/**
	 * 実行結果ログファイルの出力する先のファイルパスを取得します。
	 *
	 * @return ファイルパス
	 */
	public String getOutLogPath();
	/**
	 * 実行結果ログファイルの出力する先のファイルパスを取得します。
	 *
	 * @param outLogPath ファイルパス
	 */
	public void setOutLogPath(String substitutionPath);

	public ICharsetInfoEntity getCharsetInfo() ;
	public void setCharsetInfo( ICharsetInfoEntity obj ) ;

}
