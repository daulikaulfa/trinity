package jp.co.blueship.tri.bm.task.exe.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskMapping;
import jp.co.blueship.tri.bm.task.CharsetInfoEntity;
import jp.co.blueship.tri.bm.task.exe.oxm.eb.ITaskExeEntity;
import jp.co.blueship.tri.bm.task.exe.oxm.eb.TaskExeEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.ExecCharsetinfoType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Exe;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * Exeタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskExeMapping extends TriXmlMappingUtils implements ITaskMapping {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB(TargetType src, ITaskTargetEntity dest) {
		List<ITaskTaskTypeEntity> entitys = new ArrayList<ITaskTaskTypeEntity>();

		Exe[] tasks = src.getExeArray();

		if( null != tasks ) {
			for( Exe task : tasks ) {
				TaskExeEntity taskEntity = new TaskExeEntity();

				copyProperties( task, taskEntity );

				Exe.Param[] params = task.getParamArray();

				if( null != params && 0 < params.length ) {
					List<String> paramList = new ArrayList<String>();

					for( Exe.Param param : params ) {
						String paramEntity = new String();
						paramEntity = param.getEnv();
						paramList.add( paramEntity );
					}
					String[] paramArray = paramList.toArray( new String[ 0 ] );
					taskEntity.setEnv( paramArray );
				}

				ExecCharsetinfoType charsetInfo = task.getCharsetinfo();
				if ( null != charsetInfo ) {
					CharsetInfoEntity charsetInfoEntity = new CharsetInfoEntity();
					charsetInfoEntity.setIn(charsetInfo.getIn());
					charsetInfoEntity.setOut(charsetInfo.getOut());
					taskEntity.setCharsetInfo(charsetInfoEntity);
				}

				entitys.add( taskEntity );
			}
		}

		dest.addTask( entitys.toArray( new ITaskTaskTypeEntity[0]) );

//		Result
		TaskResultExeMapping resultMapping = new TaskResultExeMapping() ;
		resultMapping.mapXMLBeans2DB( src.getResult() , dest ) ;

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public TargetType mapDB2XMLBeans(ITaskTargetEntity src, TargetType dest) {
		ITaskTaskTypeEntity[] entitys =src.getTask();

		if( null != entitys ) {
			for( ITaskTaskTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskExeEntity) )
					continue;

				TaskExeEntity taskEntity = (TaskExeEntity)entity;

				Exe task = dest.addNewExe();

				copyProperties( taskEntity, task );

				String[] paramArray = taskEntity.getEnv();

				if( null != paramArray ) {
					for( String paramEntity : paramArray ) {
						Exe.Param param = task.addNewParam();
						param.setEnv( paramEntity );
					}
				}
			}
		}

//		Result
		TaskResultExeMapping resultMapping = new TaskResultExeMapping() ;
		Result result = ( null != dest.getResult() ) ? dest.getResult() : dest.addNewResult() ;
		resultMapping.mapDB2XMLBeans( src , result ) ;

		return dest;
	}

}
