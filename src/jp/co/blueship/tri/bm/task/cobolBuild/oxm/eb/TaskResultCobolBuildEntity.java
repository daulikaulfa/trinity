package jp.co.blueship.tri.bm.task.cobolBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskResultEntity;

public class TaskResultCobolBuildEntity extends TaskTaskResultEntity implements ITaskResultCobolBuildEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private String outLog = null ;
	private String errLog = null ;

	public String getOutLog() {
		return outLog ;
	}
	public void setOutLog( String value ) {
		this.outLog = value ;
	}
	public String getErrLog() {
		return errLog ;
	}
	public void setErrLog( String value ) {
		this.errLog = value ;
	}
}
