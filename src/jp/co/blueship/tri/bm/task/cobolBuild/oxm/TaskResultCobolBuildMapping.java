package jp.co.blueship.tri.bm.task.cobolBuild.oxm;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.utils.ITaskResultMapping;
import jp.co.blueship.tri.bm.task.cobolBuild.oxm.eb.ITaskResultCobolBuildEntity;
import jp.co.blueship.tri.bm.task.cobolBuild.oxm.eb.TaskResultCobolBuildEntity;
import jp.co.blueship.tri.fw.dao.oxm.utils.TriXmlMappingUtils;
import jp.co.blueship.tri.fw.schema.beans.task.CobolBuildResultType;
import jp.co.blueship.tri.fw.schema.beans.task.LogType;
import jp.co.blueship.tri.fw.schema.beans.task.TargetType.Result;

/**
 * CobolBuildタスク用に、XMLBeansのEntityとDartaBaseのEntity、双方向のマッピングを行います。
 *
 *
 */
public class TaskResultCobolBuildMapping extends TriXmlMappingUtils implements ITaskResultMapping {

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param x 複写元のXMLBeans
	 * @param d 複写先のDBのEntity
	 * @return マッピング結果を戻します。
	 */
	public ITaskTargetEntity mapXMLBeans2DB( Result src, ITaskTargetEntity dest) {

		if( null != src ) {
			List<ITaskResultTypeEntity> entitys = new ArrayList<ITaskResultTypeEntity>();

			CobolBuildResultType[] results = src.getCobolBuildArray();

			if( null != results ) {
				for( CobolBuildResultType result : results ) {
					TaskResultCobolBuildEntity resultEntity = new TaskResultCobolBuildEntity() ;

					copyProperties( result , resultEntity ) ;

					LogType log = result.getLog() ;
					if( null != log ) {
						copyProperties( log , resultEntity ) ;
					}

					entitys.add( resultEntity );
				}
			}

			dest.addTaskResult( entitys.toArray( new ITaskResultTypeEntity[0]) );
		}

		return dest;
	}

	/**
	 * 複写元から複写先へコピーします。
	 *
	 * @param d 複写元のDBのEntity
	 * @param x 複写先のXMLBeans
	 * @return マッピング結果を戻します。
	 */
	public Result mapDB2XMLBeans(ITaskTargetEntity src, Result dest) {
		ITaskResultTypeEntity[] entitys = src.getTaskResult();

		if( null != entitys ) {
			for( ITaskResultTypeEntity entity : entitys ) {
				if( ! (entity instanceof ITaskResultCobolBuildEntity) )
					continue;

				TaskResultCobolBuildEntity resultEntity = (TaskResultCobolBuildEntity)entity;

				CobolBuildResultType result = dest.addNewCobolBuild();

				copyProperties( resultEntity, result );

				LogType log = result.addNewLog() ;
				copyProperties( resultEntity, log );
			}
		}

		return dest;
	}

}
