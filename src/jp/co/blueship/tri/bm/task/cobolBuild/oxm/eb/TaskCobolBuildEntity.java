package jp.co.blueship.tri.bm.task.cobolBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskTaskEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public class TaskCobolBuildEntity extends TaskTaskEntity implements ITaskCobolBuildEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 3L;

	private String name = null ;
	private String executeShell = null ;
	private String outLogPath = null;
	private String errLogPath = null;
	//private ITaskBuildPathEntity buildPath = null ;

	public String getName() {
		return name ;
	}
	public void setName(String value) {
		name = value ;
	}
	public String getExecuteShell() {
		return executeShell ;
	}
	public void setExecuteShell(String value) {
		executeShell = value ;
	}
	public String getOutLogPath() {
		return outLogPath ;
	}
	public void setOutLogPath(String value) {
		outLogPath = value ;
	}
	public String getErrLogPath() {
		return errLogPath ;
	}
	public void setErrLogPath(String value) {
		errLogPath = value ;
	}

	/*
	public ITaskBuildPathEntity getBuildPath() {
		return buildPath ;
	}
	public void setBuildPath( ITaskBuildPathEntity obj ) {
		buildPath = obj ;
	}*/


	/**
	 * インナーインターフェイス
	 *
	 *//*
	public class TaskBuildPathEntity implements ITaskBuildPathEntity {
		private String binOutput = null ;
		private String debugBinOutput = null ;
		private ITaskPathTypeEntity[] srcParam = null ;

		public String getBinOutput() {
			return binOutput ;
		}
		public void setBinOutput(String value) {
			binOutput = value ;
		}
		public String getDebugBinOutput() {
			return debugBinOutput ;
		}
		public void setDebugBinOutput(String value) {
			debugBinOutput = value ;
		}

		public ITaskPathTypeEntity[] getSrcParam() {
			return srcParam ;
		}
		public void setSrcParam( ITaskPathTypeEntity[] obj ) {
			srcParam = obj ;
		}
	}*/



	/**
	 *
	 * インナーインターフェイス
	 *
	 *//*
	public class TaskPathTypeEntity implements ITaskPathTypeEntity {
		private String path = null ;

		public String getPath() {
			return path ;
		}
		public void setPath( String value ) {
			path = value ;
		}
	}*/
}
