package jp.co.blueship.tri.bm.task.cobolBuild.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;

/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskCobolBuildEntity extends ITaskTaskTypeEntity {

	public String getName() ;
	public void setName(String value) ;
	public String getExecuteShell() ;
	public void setExecuteShell(String value) ;
	public String getOutLogPath() ;
	public void setOutLogPath(String value) ;
	public String getErrLogPath() ;
	public void setErrLogPath(String value) ;

	//public ITaskBuildPathEntity getBuildPath() ;
	//public void setBuildPath( ITaskBuildPathEntity obj ) ;

	/**
	 * インナーインターフェイス
	 *
	 *//*
	public interface ITaskBuildPathEntity {
		public String getBinOutput() ;
		public void setBinOutput(String value) ;
		public String getDebugBinOutput() ;
		public void setDebugBinOutput(String value) ;

		public ITaskPathTypeEntity[] getSrcParam() ;
		public void setSrcParam( ITaskPathTypeEntity[] obj ) ;
	}*/

	/**
	 *
	 * インナーインターフェイス
	 *
	 *//*
	public interface ITaskPathTypeEntity {
		public String getPath() ;
		public void setPath( String value ) ;
	}*/
}
