package jp.co.blueship.tri.bm.task;

import java.rmi.ConnectException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.cmn.utils.AgentExtractEntityAddonUtil;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.BldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.agent.ex.TaskIncompleteException;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatusTask;
import jp.co.blueship.tri.fw.agent.service.rpc.RmiProxyFactoryWrapperBean;
import jp.co.blueship.tri.fw.agent.service.rpc.RmiServiceUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RemoteService;
import jp.co.blueship.tri.fw.constants.RemoteServiceType;
import jp.co.blueship.tri.fw.constants.SmDesignEntryKeyByTask;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;

/**
 * ビルドのタイムラインごとの実行を制御するプロキシクラスです。 <br>
 * ここでは、実際の処理を行いません。実処理は、別クラスにフォワードします。 <br>
 * <br>
 * 依頼先のビルド環境サーバがAgentである場合は、Agentサーバのサービスを起動します。 <br>
 * 実行サービスが非同期で操作するため、このクラス内で、ライン全体の各非同期処理が完了するまで、 <br>
 * 処理を待ちます。
 *
 * @version V3L10.02
 * @author Yukihiro Eguchi
 *
 * @version SP-20150601_V3L12R01
 * @author Norheda Zulkipeli
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class BuildTaskServiceProxy implements IBuildTaskService {

	private static final ILog log = TriLogFactory.getInstance();

	private BmFinderSupport support = null;
	private String buildTaskService = null;

	/**
	 * データアクセスを支援するファインダーが設定されます。
	 *
	 * @param support データアクセスファインダー
	 */
	public void setSupport(BmFinderSupport support) {
		this.support = support;
	}

	/**
	 * Task Serviceが設定されます。
	 *
	 * @param buildTaskService ビルドタスクサービスのbean id
	 */
	public final void setTaskService( String buildTaskService ) {
		this.buildTaskService = buildTaskService;
	}

	private final IBuildTaskService getTaskService() {
		return (IBuildTaskService)ContextAdapterFactory.getContextAdapter().getBean(buildTaskService);
	}

	@Override
	public List<AgentStatusTask> executeLinkCheck() throws Exception {

//		List<RmiProxyFactoryWrapperBean> beanList = RmiServiceUtils.getRmiProxyFactoryBeanList(null, null, RemoteServiceType.AGENT_RELEASE,
//				IBuildTaskService.class.getSimpleName());
		RmiProxyFactoryWrapperBean rmiProxyFactoryBean = RmiServiceUtils.getRmiProxyFactoryBean(null, RemoteService.getValueById(IBuildTaskService.class.getSimpleName()));

		List<AgentStatusTask> checkedList = new ArrayList<AgentStatusTask>();

//		for (RmiProxyFactoryWrapperBean rmiProxyFactoryBean : beanList) {
			for (IBldEnvSrvEntity envServer : rmiProxyFactoryBean.getEnvServer()) {
				LogHandler.debug(log, "agent " + envServer.getBldSrvId() + " " + envServer.getBldEnvId() + " との疎通チェックを行います。");

				AgentStatusTask agentStatus = new AgentStatusTask();
				agentStatus.setStatus(true);
				try {
					rmiProxyFactoryBean.afterPropertiesSet();
					IBuildTaskService agentService = (IBuildTaskService) rmiProxyFactoryBean.getObject();
					agentService.executeLinkCheck();
				} catch (Exception e) {// exceptionは一旦握っておき、あとでまとめて出力する
					LogHandler.fatal(log, e);
					agentStatus.setStatus(false);
				} finally {
					agentStatus.setServiceUrl(rmiProxyFactoryBean.getServiceUrl());
					agentStatus.setEnvNo(envServer.getBldEnvId());
					agentStatus.setServerNo(envServer.getBldSrvId());
					checkedList.add(agentStatus);
				}
			}
//		}

		return checkedList;
	}

	@Override
	public List<Object> executeLinkCheckByService(List<Object> paramList) throws Exception {

		IRmiSvcDto remoteService = AgentExtractEntityAddonUtil.extractRemoteService(paramList);

//		List<RmiProxyFactoryWrapperBean> beanList = RmiServiceUtils.getRmiProxyFactoryBeanList(remoteService.getRmiSvcEntity().getBldSrvId(), null, RemoteServiceType.AGENT_BUILD,
//				IBuildTaskService.class.getSimpleName());
		RmiProxyFactoryWrapperBean rmiProxyFactoryBean = RmiServiceUtils.getRmiProxyFactoryBean(remoteService.getRmiSvcEntity().getBldSrvId(), RemoteService.getValueById(IBuildTaskService.class.getSimpleName()));

//		for (RmiProxyFactoryWrapperBean rmiProxyFactoryBean : beanList) {
			if (remoteService.getRmiSvcEntity().getBldSrvId().equals(rmiProxyFactoryBean.getServerId())
					&& remoteService.getRmiSvcEntity().getRmiSvcId().equals(rmiProxyFactoryBean.getServiceId())) {

				LogHandler.debug(log, "agent " + remoteService.getRmiSvcEntity().getBldSrvId() + " " + remoteService.getRmiSvcEntity().getRmiSvcId()
						+ " との疎通チェックを行います。");

				AgentStatus agentStatus = new AgentStatus();
				agentStatus.setStatus(true);

				try {
					rmiProxyFactoryBean.afterPropertiesSet();
					IBuildTaskService agentService = (IBuildTaskService) rmiProxyFactoryBean.getObject();

					paramList = agentService.executeLinkCheckByService(paramList);
				} catch (Exception e) {// exceptionは一旦握っておき、あとでまとめて出力する
					LogHandler.fatal(log, e);
					agentStatus.setStatus(false);
				} finally {

				}
			}
//		}
		return paramList;
	}

	/**
	 * 業務シーケンスをタイムライン単位に実行します。 <br>
	 * 同一タイムラインのシステムに対し、順に起動指示を行います。
	 *
	 * @param request 依頼するタイムライン
	 * @param bean 業務シーケンスで使用するメッセージ
	 *
	 * @throws Exception
	 */

	public void execute(IBldTimelineEntity request, IBuildTaskBean bean) throws Exception {

		if (null == request || null == bean) {
			return;
		}

		if (log.isDebugEnabled()) {
			LogHandler.debug(log, "◆◆◆:getBuildNo:=" + bean.getBuildNo());
		}
		//
		// if ( 1 == Integer.parseInt( request.getLineNo() ) )
		// buildProcessDao.delete();

		boolean isLineWait = false;
		if (request.getLine() != null) {
			for (IBldTimelineAgentEntity line : request.getLine()) {
				ITaskTargetEntity targetEntity = this.getTaskTargetEntity(line.getTargetSeqNo(),
						this.support.findTaskFlowEntity(line.getTaskFlowId()));

				if (false == bean.isForceExecuteMode() || StatusFlg.on.value().equals(targetEntity.getForceExecute())) {
					isLineWait = true;
					execute(request, bean, line);
				}
			}
		}

		if (isLineWait) {
			this.lineWait(request, bean, Integer.parseInt(DesignSheetFactory.getDesignSheet().getValue(SmDesignEntryKeyByTask.reloadInterval)));
		}

	}

	/**
	 * 業務シーケンスをタイムライン中のシステム単位に実行します。<br>
	 * 引数で与えられた、タイムラインの単一システムに対し、起動指示を行います。
	 *
	 * @param request タイムライン全体
	 * @param bean 業務シーケンスで使用するメッセージ
	 * @param iLineEntity 依頼するライン（システム）
	 * @throws Exception
	 */

	public void execute(IBldTimelineEntity request, IBuildTaskBean bean, IBldTimelineAgentEntity iLineEntity) throws Exception {

		if (null == iLineEntity.getTargetSeqNo()) {
			return;
		}

		IBldTimelineEntity timelineEntity = new BldTimelineEntity();
		IBldTimelineAgentEntity lineEntity = timelineEntity.newLineEntity();
		timelineEntity.setLine(new IBldTimelineAgentEntity[] { lineEntity });

		TriPropertyUtils.copyProperties(timelineEntity, request);
		TriPropertyUtils.copyProperties(lineEntity, iLineEntity);

		IBldSrvEntity sventity = this.support.findBldSrvEntity(iLineEntity.getBldSrvId());

		if (this.isAgent(sventity)) {
			LogHandler.debug(log, "	:isAgent = true");

			IBldEnvSrvEntity entity = deriveBldEnvSrv(request.getBldEnvId(), iLineEntity.getBldSrvId());
			IBuildTaskService agentService = null;
			try {
				RmiProxyFactoryWrapperBean rmiProxyFactoryBean = //
				RmiServiceUtils.getRmiProxyFactoryBean(entity.getBldSrvId(), RemoteService.BUILD_TASK_SERVICE);
				rmiProxyFactoryBean.afterPropertiesSet();
				agentService = (IBuildTaskService) rmiProxyFactoryBean.getObject();
			} catch (Exception e) {

				ExceptionUtils.reThrowExceptionIf(ConnectException.class, e);
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException(BmMessageId.BM005060S, e, entity.getBldSrvId(), entity.getBldEnvId(),
						RemoteServiceType.AGENT_BUILD.getValue(), RemoteService.BUILD_TASK_SERVICE.getId());
			}

			agentService.execute(timelineEntity, bean, iLineEntity);

		} else {
			LogHandler.debug(log, "	:isAgent = false");
			getTaskService().execute(timelineEntity, bean, iLineEntity);
		}

	}

	/**
	 * 指定されたビルド環境とビルドサーバより、ビルド環境情報を導出します。
	 *
	 * @param bldEnvId ビルド環境ID
	 * @param bldSrvId ビルドサーバID
	 * @return ビルド環境情報Entity
	 */
	private IBldEnvSrvEntity deriveBldEnvSrv(String bldEnvId, String bldSrvId) {

		IBldEnvSrvEntity entity = this.support.findBldEnvSrvEntity(bldEnvId, bldSrvId);
		if (null == entity) {
			throw new TriSystemException(BmMessageId.BM004067F, bldEnvId, bldSrvId);
		}

		return entity;
	}

	/**
	 * 指定されたミリ秒間隔で、ビルド状況の確認を行います。
	 *
	 * @param request ビルドラインを指定。
	 * @param bean メッセージを指定。
	 * @param line ターゲットラインを指定。
	 * @param msec 何ミリ秒単位に状況を確認するかを指定。
	 * @throws Exception
	 */

	private synchronized void lineWait(IBldTimelineEntity request, IBuildTaskBean bean, long msec) throws Exception {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {
			boolean isWait = true;
			int waitCount = 0;
			while (isWait) {
				wait(msec);

				TaskFlowProcCondition condition = new TaskFlowProcCondition();
				condition.setProcId(bean.getProcId());
				condition.setBldLineNo(request.getBldLineNo());
				ITaskFlowProcEntity[] entitys = this.support.getTaskFlowProcDao().find(condition.getCondition()).toArray(new ITaskFlowProcEntity[0]);
				LogHandler.debug(log, "lineWait : buildProcessレコードの出力待ち : " + bean.getBuildNo() + " " + bean.getLotNo() + " "
						+ request.getBldLineNo().toString() + " " + "[" + " 要求タスク数=" + request.getLine().length + ":" + " 実行タスク数=" + entitys.length
						+ " ]" + " " + (waitCount * msec) + "ms");
				waitCount++;

				if (request.getLine().length != entitys.length)
					continue;

				isWait = false; // いったんfalseに

				for (ITaskFlowProcEntity entity : entitys) {
					if (BmBpStatusIdForExecData.CreatingBuildPackage.equals(entity.getStsId())) {
						isWait = true; // 1個でもACTIVEだったら待つ
					}

					if (BmBpStatusIdForExecData.BuildPackageError.equals(entity.getStsId())) {
						messageList.add(RmMessageId.RM005075S);
						messageArgsList.add(new String[] {});

						Throwable cbe = new ContinuableBusinessException(messageList, messageArgsList);

						throw new TaskIncompleteException(cbe);
					}

					if (BmBpStatusId.BuildPackageCreated.equals(entity.getStsId())) {
						// すべてCOMPLETEだったら待つ
						// falseのままのはずなので特になにもしない
					}
				}
			}

		} catch (InterruptedException e) {
			throw new TriSystemException(BmMessageId.BM005066S, e);
		}
	}

	/**
	 * エージェントサーバかどうかの判断を行います。
	 *
	 * @param entity
	 * @return Agentサーバの場合true、それ以外はfalseを戻します。
	 */
	private final boolean isAgent(IBldSrvEntity entity) {

		if (StatusFlg.on.value().equals(entity.getIsAgent().value())) {
			return true;
		}
		/*
		 * if ( IBldSrvEntity.AgentType.rmi.toString().equalsIgnoreCase(
		 * entity.getAgentType() ) ) return true;
		 *
		 * if ( IBldSrvEntity.AgentType.soap.toString().equalsIgnoreCase(
		 * entity.getAgentType() ) ) return true;
		 */
		return false;
	}

	/**
	 * 指定されたシーケンス番号から、該当タスクを確定して取得します。
	 *
	 * @param SequenceNo 業務シーケンス番号
	 * @param taskEntity ワークフロー
	 * @return 取得したターゲットタスクを戻します。
	 */
	private final ITaskTargetEntity getTaskTargetEntity(Integer SequenceNo, ITaskFlowEntity taskFlowEntity) throws Exception {

		ITaskTargetEntity targetEntity = null;
		ITaskTargetEntity[] targetEntityArray = taskFlowEntity.getTask().getTarget();
		if (null != targetEntityArray) {
			for (ITaskTargetEntity target : targetEntityArray) {
				if (SequenceNo.equals(target.getSequenceNo())) {
					if (null == targetEntity) {
						targetEntity = target;
					} else {
						throw new TriSystemException(BmMessageId.BM004070F, targetEntity.getSequenceNo().toString());
					}
				}
			}
		}

		return targetEntity;
	}

	@Override
	public void writeProcessByIrregular(IBldTimelineEntity timeLine,
			IBuildTaskBean param, IBldTimelineAgentEntity line, Throwable e) throws Exception {
		getTaskService().writeProcessByIrregular(timeLine, param, line, e);
	}

}
