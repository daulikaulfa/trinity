package jp.co.blueship.tri.bm.task.scp.oxm.eb;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.fw.dao.orm.IEntity;


/**
 * タスクオーダー情報エンティティのインタフェースです。
 *
 */
public interface ITaskScpEntity extends IEntity, ITaskTaskTypeEntity {

	/**
	 * SCPのgetコマンドを実行するための情報を取得します。
	 *
	 * @return  取得した情報を戻します。
	 */
	public ITaskFtpPutGetTypeEntity[] getGet() ;
	/**
	 * SCPのgetコマンドを実行するための情報を設定します。
	 *
	 * @param get SCPコマンド情報
	 */
	public void setGet(ITaskFtpPutGetTypeEntity[] get) ;

	/**
	 * SCPのputコマンドを実行するための情報を取得します。
	 *
	 * @return  取得した情報を戻します。
	 */
	public ITaskFtpPutGetTypeEntity[] getPut() ;
	/**
	 * SCPのputコマンドを実行するための情報を設定します。
	 *
	 * @param get SCPコマンド情報
	 */
	public void setPut(ITaskFtpPutGetTypeEntity[] put) ;

	/**
	 * SCPのdelコマンドを実行するための情報を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public ITaskFtpDelTypeEntity[] getDel() ;
	/**
	 * SCPのdelコマンドを実行するための情報を設定します。
	 *
	 * @param del SCPコマンド情報
	 */
	public void setDel(ITaskFtpDelTypeEntity[] del) ;

	/**
	 * SCPホスト／IPアドレスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getServer() ;
	/**
	 * SCPホスト／IPアドレスを設定します。
	 *
	 * @param server FTPホスト／IPアドレス
	 */
	public void setServer(String server) ;

	/**
	 * ポート番号を取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getPort() ;
	/**
	 * ポート番号を設定します。
	 * <br>未指定の場合、デフォルト"80"
	 *
	 * @param port ポート番号
	 */
	public void setPort(String port) ;

	/**
	 * ログインユーザを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getUserid() ;
	/**
	 * ログインユーザを設定します。
	 *
	 * @param userid ログインユーザ
	 */
	public void setUserid(String userid) ;

	/**
	 * 転送ファイル名を符号化／復号化する際の文字コード
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getEncoding();
	/**
	 * 転送ファイル名を符号化／復号化する際の文字コード
	 *
	 * @param value 文字コード。{@link jp.co.blueship.tri.fw.cmn.io.constants.Charset}で定義された文字列
	 */
	public void setEncoding(String encoding);

	/**
	 * 実際に処理を行うかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoSimulate() ;
	/**
	 * 実際に処理を行うかどうかを設定します。
	 *
	 * @param value 実際に処理を行わない場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 * それ以外は、処理を行う。
	 */
	public void setDoSimulate( String value ) ;

	/**
	 * 処理結果にすべての結果を戻すかどうかを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoDetailSnap();
	/**
	 * 処理結果にすべての結果を戻すかどうかを設定します。
	 *
	 * @param value すべての結果を戻す場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。それ以外は、エラー結果のみ戻す。
	 */
	public void setDoDetailSnap( String value );
	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @return 取得した情報を戻します。
	 */
	public String getDoNotExistSkip() ;
	/**
	 * 元が存在しない場合、スキップするかどうか
	 *
	 * @param doSkip 元が存在しなければ、スキップする場合、{@link jp.co.blueship.tri.fw.cmn.utils.StatusFlg#on}。
	 */
	public void setDoNotExistSkip(String doSkip) ;
}
