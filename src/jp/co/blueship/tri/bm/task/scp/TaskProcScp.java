package jp.co.blueship.tri.bm.task.scp;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskResultTypeEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTaskTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpDelTypeEntity;
import jp.co.blueship.tri.bm.task.ITaskFtpPutGetTypeEntity;
import jp.co.blueship.tri.bm.task.TaskStringUtils;
import jp.co.blueship.tri.bm.task.scp.oxm.eb.ITaskResultScpEntity;
import jp.co.blueship.tri.bm.task.scp.oxm.eb.ITaskResultScpEntity.ITaskResultEntity;
import jp.co.blueship.tri.bm.task.scp.oxm.eb.ITaskScpEntity;
import jp.co.blueship.tri.bm.task.scp.oxm.eb.TaskResultScpEntity;
import jp.co.blueship.tri.bm.task.scp.oxm.eb.TaskScpEntity;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskResultCode;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.ftp.ScpIo;
import jp.co.blueship.tri.fw.ftp.ScpParamBean;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.security.constants.PasswordCategory;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;
import jp.co.blueship.tri.fw.task.ITaskProc;
/**
 * 「scp」タスクの処理内容を記述します
 * <p>
 * 定義された情報を元にＦＴＰ転送を行う。
 * <br>セキュリティのため、パスワード管理はRDB上にて暗号化して管理を行う。
 * 「FTPホスト／IPアドレス」及び「ユーザID」で情報が紐づく。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TaskProcScp implements ITaskProc {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	private ITaskPropertyEntity[] propertyEntity = null;

	public void setProperty(ITaskPropertyEntity[] propertyEntity) {
		this.propertyEntity = propertyEntity;
	}

	private TaskFinderSupport support = null;

	public void setSupport(TaskFinderSupport support) {
		this.support = support;
	}
	public enum FtpType {
		put ,
		get ,
		del;
	}
	/**
	 * パス文字列中の代用キー{@hoge}を置換する
	 * @param task タスク処理内容を保持したエンティティ
	 * @throws Exception
	 */
	public void substitutePath( ITaskTaskTypeEntity task ) throws Exception {
		TaskScpEntity scp = (TaskScpEntity) task;

		ITaskFtpPutGetTypeEntity[] putArray = scp.getPut();
		if( null != putArray ) {
			for( ITaskFtpPutGetTypeEntity put : putArray ) {
				ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = put.getParam();
				if( null != paramEntity ) {
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String src = TaskStringUtils.substitutionPath( propertyEntity , param.getSrc() ) ;
						String dest = TaskStringUtils.substitutionPath( propertyEntity , param.getDest() ) ;

						param.setSrc( TriStringUtils.trimTailSeparator( src ) ) ;//scpでは、パス後尾の"/"除去も一緒に行う。
						param.setDest( TriStringUtils.trimTailSeparator( dest ) ) ;//scpでは、パス後尾の"/"除去も一緒に行う。
					}
				}
//				","区切りの複数パラメータに対応
				/* 下記コードだとTaskParamEntityの生成数・内容に問題ありそうなので複数パラメータ対応はいったん外す
				if( null != paramEntity ) {
					List<ITaskFtpPutGetTypeEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskFtpPutGetTypeEntity.ITaskParamEntity>();
					//src
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getSrc() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)put ).new TaskParamEntity();
								entity.setSrc( array );
								entity.setDest( param.getDest() );
								arrlst.add( entity );
							}
						}
					}
					//dst
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getDest() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)put ).new TaskParamEntity();
								entity.setSrc( param.getSrc() );
								entity.setDest( array );
								arrlst.add( entity );
							}
						}
					}
					paramEntity = arrlst.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] );
					put.setParam( paramEntity );
				}
				*/
			}
		}

		ITaskFtpPutGetTypeEntity[] getArray = scp.getGet();
		if( null != getArray ) {
			for( ITaskFtpPutGetTypeEntity get : getArray ) {
				ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramEntity = get.getParam();
				if( null != paramEntity ) {
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String src = TaskStringUtils.substitutionPath( propertyEntity , param.getSrc() ) ;
						String dest = TaskStringUtils.substitutionPath( propertyEntity , param.getDest() ) ;

						param.setSrc( TriStringUtils.trimTailSeparator( src ) ) ;//scpでは、パス後尾の"/"除去も一緒に行う。
						param.setDest( TriStringUtils.trimTailSeparator( dest ) ) ;//scpでは、パス後尾の"/"除去も一緒に行う。
					}
				}
//				","区切りの複数パラメータに対応
				/* 下記コードだとTaskParamEntityの生成数・内容に問題ありそうなので複数パラメータ対応はいったん外す
				if( null != paramEntity ) {
					List<ITaskFtpPutGetTypeEntity.ITaskParamEntity> arrlst = new ArrayList<ITaskFtpPutGetTypeEntity.ITaskParamEntity>();
					//src
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getSrc() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)get ).new TaskParamEntity();
								entity.setSrc( array );
								entity.setDest( param.getDest() );
								arrlst.add( entity );
							}
						}
					}
					//dst
					for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramEntity ) {
						String[] arrays =TaskStringUtil.substitutionPaths( propertyEntity , param.getDest() );
						if( null != arrays ) {
							for( String array : arrays ) {
								ITaskFtpPutGetTypeEntity.ITaskParamEntity entity = ( (TaskFtpPutGetTypeEntity)get ).new TaskParamEntity();
								entity.setSrc( param.getSrc() );
								entity.setDest( array );
								arrlst.add( entity );
							}
						}
					}
					paramEntity = arrlst.toArray( new ITaskFtpPutGetTypeEntity.ITaskParamEntity[ 0 ] );
					get.setParam( paramEntity );
				}
				*/
			}
		}
		ITaskFtpDelTypeEntity[] delArray = scp.getDel();
		if( null != delArray ) {
			for( ITaskFtpDelTypeEntity del : delArray ) {
				String[] srcArray = del.getSrc();
				if( null != srcArray ) {
					List<String> newSrcList = new ArrayList<String>() ;
					for( String src : srcArray ) {
						src = TaskStringUtils.substitutionPath( propertyEntity , src );

						src = TriStringUtils.trimTailSeparator( src ) ;//scpでは、パス後尾の"/"除去も一緒に行う。
						newSrcList.add( src ) ;
					}
					del.setSrc( newSrcList.toArray( new String[ 0 ] ) ) ;
				}
//				","区切りの複数パラメータに対応
				/* GET/PUTに倣って複数パラメータ対応はいったん外す
				List<String> arrlst = new ArrayList<String>();
				String[] srcArray = del.getSrc();
				for( String src : srcArray ) {
					String[] arrays = TaskStringUtil.substitutionPaths( propertyEntity , src );
					if( null != arrays ) {
						for( String array : arrays ) {
							arrlst.add( array );
						}
					}
					srcArray = arrlst.toArray( new String[ 0 ] );
					del.setSrc( srcArray );
				}
				*/
			}
		}
	}

	/**
	 * 「scp」タスクの実際の処理を記述します
	 * <br>
	 * @param task タスク処理内容を保持したエンティティ
	 * @return 業務シーケンスの処理結果エンティティ
	 * @throws Exception
	 */
	public ITaskResultTypeEntity execute( ITaskTargetEntity prmTarget, ITaskTaskTypeEntity prmTask ) throws Exception {
		if ( ! (prmTask instanceof ITaskScpEntity) )
			return null;

		ITaskResultScpEntity resultScpEntity = new TaskResultScpEntity();
		List<ITaskResultEntity> resultList = new ArrayList<ITaskResultEntity>();

		resultScpEntity.setSequence( prmTask.getSequence() );
		resultScpEntity.setCode( TaskResultCode.UN_PROCESS );
		resultScpEntity.setTaskId( prmTask.getTaskId() );

		ScpIo scpIo = null;

		ITaskScpEntity scp = (ITaskScpEntity) prmTask;

		try {
			Integer resultSequence = 1;

			substitutePath( prmTask );

			Map<Integer,Object[]> commandParamMap = new TreeMap<Integer,Object[]>();
			ScpParamBean scpParam = new ScpParamBean();
			this.setCommandParam(scp, scpParam, commandParamMap);

			scpIo = new ScpIo( scpParam );
			if ( ! StatusFlg.on.value().equals( scp.getDoSimulate() ) )
				scpIo.openConnection();

			for ( Object[] obj : commandParamMap.values() ) {
				if( FtpType.put.equals( obj[ 0 ] ) ) {
					ITaskFtpPutGetTypeEntity put = (ITaskFtpPutGetTypeEntity)obj[ 1 ];
					ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramArray = put.getParam();
					if( null != paramArray ) {
						for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramArray ) {
							//1コマンドごとに処理結果を記録する
							ITaskResultEntity innerResultEntity = resultScpEntity.newResult();

							try {
								innerResultEntity.setSequence( String.valueOf(resultSequence++) );
								innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								innerResultEntity.setCommand( FtpType.put.toString() );
								innerResultEntity.setSrc( param.getSrc() );
								innerResultEntity.setDest( param.getDest() );

								if ( StatusFlg.on.value().equals( scp.getDoSimulate() ) ) {
								} else {
									scpIo.putFile( param.getSrc() , param.getDest() );
									innerResultEntity.setCode( TaskResultCode.SUCCESS );
								}
							} catch ( FileNotFoundException e ) {
								boolean doNotExistSkip = ( StatusFlg.on.value().equals( scp.getDoNotExistSkip() ) ) ? true : false;
								if( true == doNotExistSkip ) {//送るファイルがないのでスキップ
									innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								} else {
									innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
									throw e;
								}
							} catch ( Exception e ) {
								innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
								throw e;
							} finally {
								if ( StatusFlg.on.value().equals( scp.getDoDetailSnap() ) ) {
									resultList.add( innerResultEntity );
								} else {
									if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
										resultList.add( innerResultEntity );
									}
								}
							}
						}
					}
				}

				if( FtpType.get.equals( obj[ 0 ] ) ) {
					ITaskFtpPutGetTypeEntity get = (ITaskFtpPutGetTypeEntity)obj[ 1 ];
					//scpIo.setFileType( this.getFileIoType( get.getFileType() ) );
					ITaskFtpPutGetTypeEntity.ITaskParamEntity[] paramArray = get.getParam();
					if( null != paramArray ) {
						for( ITaskFtpPutGetTypeEntity.ITaskParamEntity param : paramArray ) {
							//1コマンドごとに処理結果を記録する
							ITaskResultEntity innerResultEntity = resultScpEntity.newResult();

							try {
								innerResultEntity.setSequence( String.valueOf(resultSequence++) );
								innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								innerResultEntity.setCommand( FtpType.get.toString() );
								innerResultEntity.setSrc( param.getSrc() );
								innerResultEntity.setDest( param.getDest() );

								if ( StatusFlg.on.value().equals( scp.getDoSimulate() ) ) {
								} else {
									scpIo.getFile( param.getDest() , param.getSrc() );
									innerResultEntity.setCode( TaskResultCode.SUCCESS );
								}
							} catch ( FileNotFoundException e ) {
								boolean doNotExistSkip = ( StatusFlg.on.value().equals( scp.getDoNotExistSkip() ) ) ? true : false;
								if( true == doNotExistSkip ) {//受け取るファイルがないのでスキップ
									innerResultEntity.setCode( TaskResultCode.UN_PROCESS );
								} else {
									innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
									throw e;
								}
							} catch ( Exception e ) {
								innerResultEntity.setCode( TaskResultCode.ERROR_PROCESS );
								throw e;
							} finally {
								if ( StatusFlg.on.value().equals( scp.getDoDetailSnap() ) ) {
									resultList.add( innerResultEntity );
								} else {
									if ( TaskResultCode.ERROR_PROCESS.equals( innerResultEntity.getCode() ) ) {
										resultList.add( innerResultEntity );
									}
								}
							}
						}
					}
				}
			}

			resultScpEntity.setCode( TaskResultCode.SUCCESS );
		} catch ( Exception e ) {
			resultScpEntity.setCode( TaskResultCode.ERROR_PROCESS );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005028S , e );
		} finally {
			try {
				if( null != scpIo ) {
					if ( ! StatusFlg.on.value().equals( scp.getDoSimulate() ) )
						scpIo.closeConnection();
				}
			} catch ( Exception e ) {
				throw e ;
			} finally {
				resultScpEntity.setResult( resultList.toArray( new ITaskResultEntity[ 0 ] ) );
				prmTarget.addTaskResult( resultScpEntity );
			}
		}

		return resultScpEntity;
	}

	/**
	 * put/get/delのコマンドパラメタを格納する。
	 * @param scp
	 * @param scpParam
	 * @param commandParamMap
	 * @param defFile
	 * @throws Exception
	 */
	private void setCommandParam(	ITaskScpEntity scp,
									ScpParamBean scpParam,
									Map<Integer,Object[]> commandParamMap ) throws Exception {

		scpParam.setHost( scp.getServer() );
		if( true != TriStringUtils.isEmpty( scp.getPort() ) ) {
			scpParam.setPort( Integer.parseInt( scp.getPort() ) );
		}
		scpParam.setUser( scp.getUserid() );
		scpParam.setPass( support.getSmFinderSupport().findPasswordEntity( PasswordCategory.SCP , scp.getServer() , scp.getUserid() ).getPassword() );
		scpParam.setControlEncoding( getCharset( scp.getEncoding() ) );

		ITaskFtpPutGetTypeEntity[] putArray = scp.getPut();
		if( null != putArray ) {
			for(ITaskFtpPutGetTypeEntity put : putArray ) {
				Object[] obj = { FtpType.put , put };
				commandParamMap.put( Integer.parseInt( put.getSequence() ) , obj );
			}
		}
		ITaskFtpPutGetTypeEntity[] getArray = scp.getGet();
		if( null != getArray ) {
			for(ITaskFtpPutGetTypeEntity get : getArray ) {
				Object[] obj = { FtpType.get , get };
				commandParamMap.put( Integer.parseInt( get.getSequence() ) , obj );
			}
		}
		ITaskFtpDelTypeEntity[] delArray = scp.getDel();
		if( null != delArray ) {
			for(ITaskFtpDelTypeEntity del : delArray ) {
				Object[] obj = { FtpType.del , del };
				commandParamMap.put( Integer.parseInt( del.getSequence() ) , obj );
			}
		}
	}

	/**
	 * 「文字コードセット」文字列をenum型Charsetにコンバートする
	 * <br>
	 * @param encoding 「文字コードセット」文字列
	 * @return enum型Charset
	 * @throws Exception
	 */
	private Charset getCharset( String encoding ) throws Exception {
		Charset charset = null;
		try {
			charset = Charset.value( encoding );
		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM004039F , e , encoding);
		}
		return charset;
	}
}
