package jp.co.blueship.tri.bm.task;

public class TaskFtpPutGetTypeEntity implements ITaskFtpPutGetTypeEntity {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private String sequence = null ;
	private ITaskParamEntity[] param = null ;
	private String fileType = null ;

	/**
	 * シーケンス番号を取得します<br>
	 * @return シーケンス番号
	 */
	public String getSequence() {
		return sequence;
	}
	/**
	 * シーケンス番号を設定します<br>
	 * @param sequence シーケンス番号
	 */
	public void setSequence(String sequence) {
		this.sequence = sequence;
	}
	/**
	 * Paramデータクラスを取得します<br>
	 * @return
	 */
	public ITaskParamEntity[] getParam() {
		return param;
	}
	/**
	 * Paramデータクラスを設定します<br>
	 * @param param
	 */
	public void setParam(ITaskParamEntity[] param) {
		this.param = param;
	}
	/**
	 * ファイル転送種別（テキスト/バイナリ）を取得します<br>
	 * @return ファイル転送種別（テキスト/バイナリ）
	 */
	public String getFileType() {
		return fileType;
	}
	/**
	 * ファイル転送種別（テキスト/バイナリ）を設定します<br>
	 * @param fileType ファイル転送種別（テキスト/バイナリ）
	 */
	public void setFileType(String fileType) {
		this.fileType = fileType;
	}

	/**
	 *
	 * インナークラス
	 *
	 */
	public class TaskParamEntity implements ITaskParamEntity {
		/**
		 * 修正後、インクリメントすること
		 */
		private static final long serialVersionUID = 1L;

		private String src = null ;
		private String dest = null ;

		/**
		 * コンストラクタ
		 *
		 */
		public TaskParamEntity() {
		}
		/**
		 * コンストラクタ
		 * @param src
		 * @param dest
		 */
		public TaskParamEntity( String src , String dest ) {
			this.src = src ;
			this.dest = dest ;
		}
		/**
		 * 転送先パスを取得します<br>
		 * @return 転送先パス
		 */
		public String getDest() {
			return dest;
		}
		/**
		 * 転送先パスを設定します<br>
		 * @param 転送先パス
		 */
		public void setDest(String dest) {
			this.dest = dest;
		}
		/**
		 * 転送元パスを取得します<br>
		 * @return 転送元パス
		 */
		public String getSrc() {
			return src;
		}
		/**
		 * 転送元パスを設定します<br>
		 * @param 転送元パス
		 */
		public void setSrc(String src) {
			this.src = src;
		}
	}
}
