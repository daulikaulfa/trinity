package jp.co.blueship.tri.bm;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotViewRelUnitBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean.UnitClosePjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseLotViewBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.EntityAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * 画面表示情報の共通処理Class
 * ViewInfoAddonUtilのBm版
 */
public class BmViewInfoAddonUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * @param viewBean ロット一覧画面の表示項目
	 * @param entity ロットエンティティ
	 */
	public static void setLotViewBeanPjtLotEntity(
			LotViewRelUnitBean viewBean, ILotDto dto, String srvId ) {

		ILotEntity entity = dto.getLotEntity();
		viewBean.setServerNo	( srvId );
		viewBean.setLotNo		( entity.getLotId() );
		viewBean.setLotName		( entity.getLotNm() );
		viewBean.setInputUser	( entity.getRegUserNm() );
		viewBean.setInputUserId	( entity.getRegUserId() );
		viewBean.setInputDate	( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp()) );

	}

	/**
	 * ロット一覧画面の表示項目を、ロットエンティティから取得して設定する。
	 * @param viewBeanList ロット一覧画面の表示項目
	 * @param lotDto ロットのエンティティ
	 * @param groupUserEntitys グループユーザエンティティ
	 */
	public static void setLotViewRelUnitBeanPjtLotEntity(
			List<LotViewRelUnitBean> viewBeanList, List<ILotDto> lotDto, List<IGrpUserLnkEntity> groupUserEntitys, String srvId ) {

		List<ILotDto> lotList = new ArrayList<ILotDto>();

		setAccessablePjtLotEntity( lotDto, groupUserEntitys, lotList, null );

		for ( ILotDto entity : lotList ) {
			LotViewRelUnitBean viewBean = new LotViewRelUnitBean();
			setLotViewBeanPjtLotEntity( viewBean, entity, srvId );

			viewBeanList.add( viewBean );
		}

	}

	/**
	 * ロット一覧情報をロットエンティティから設定する
	 * @param lotViewBeanList ロット一覧情報
	 * @param lotDto ロットのエンティティ
	 * @param disableLinkLotNumbers 一覧表示のみのロット番号
	 */
	public static void setUnitCloseLotViewBeanPjtLotEntity(
			List<UnitCloseLotViewBean> lotViewBeanList, List<ILotDto> lotDto, List<String> disableLinkLotNumbers, String srvId ) {

		for ( ILotDto dto : lotDto ) {

			ILotEntity entity = dto.getLotEntity();
			UnitCloseLotViewBean viewBean = new UnitCloseLotViewBean();
			viewBean.setServerNo	( srvId );

			viewBean.setLotNo		( entity.getLotId() );
			viewBean.setLotName		( entity.getLotNm() );
			viewBean.setLotSummary	( entity.getSummary() );
			viewBean.setLotStatus	(
					sheet.getValue( AmDesignBeanId.statusId, entity.getProcStsId() ));

			if ( disableLinkLotNumbers.contains( viewBean.getLotNo() )) {
				viewBean.setViewLinkEnabled( false );
			} else {
				viewBean.setViewLinkEnabled( true );
			}

			lotViewBeanList.add( viewBean );
		}

	}

	/**
	 * 申請情報を、ビルドパッケージ、変更管理、申請情報の各エンティティから設定する
	 * @param unitCloseConfirmViewBeanList 申請情報
	 * @param buildEntities ビルドパッケージエンティティ
	 * @param pjtDtos 変更管理エンティティ
	 * @param applyEntities 申請情報エンティティ
	 */
	public static void setApplyInfoViewBeanAssetApplyEntity(BmFinderSupport support,
			List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList,
			List<IBpDto> bpDtoList, List<IPjtAvlDto> pjtDtos, IAreqEntity[] applyEntities ) {


		Map<String, IAreqEntity> applyNoEntityMap = new HashMap<String, IAreqEntity>();
		for ( IAreqEntity applyEntity : applyEntities ) {
			applyNoEntityMap.put( applyEntity.getAreqId(), applyEntity );
		}


		Map<String, IPjtAvlDto> pjtNoEntityMap = new HashMap<String, IPjtAvlDto>();
		for ( IPjtAvlDto pjtDto : pjtDtos ) {
			pjtNoEntityMap.put( pjtDto.getPjtEntity().getPjtId(), pjtDto );
		}


		for ( IBpDto bpDto : bpDtoList ) {

			UnitCloseConfirmViewBean viewBean = new UnitCloseConfirmViewBean();
			viewBean.setUnitNo( bpDto.getBpEntity().getBpId() );

			List<UnitClosePjtViewBean> unitClosePjtViewBeanList = viewBean.getUnitClosePjtViewBeanList();

			for ( IBpAreqLnkEntity buildApplyentity : bpDto.getBpAreqLnkEntities() ) {

				UnitClosePjtViewBean pjtViewBean = viewBean.newUnitClosePjtViewBean();

				String pjtId = support.getAmFinderSupport().findAreqEntity(buildApplyentity.getAreqId()).getPjtId();
				IPjtAvlDto pjtDto = pjtNoEntityMap.get( pjtId );

				if ( null != pjtDto ) {
					pjtViewBean.setPjtNo			( pjtDto.getPjtEntity().getPjtId() );
					pjtViewBean.setChangeCauseNo	( pjtDto.getPjtEntity().getChgFactorNo() );
					pjtViewBean.setApplyApproveUser	( pjtDto.getPjtAvlEntity().getPjtAvlUserNm());
					pjtViewBean.setApplyApproveUserId	( pjtDto.getPjtAvlEntity().getPjtAvlUserId());
				} else {
					pjtViewBean.setPjtNo			( pjtId );
				}

				IAreqEntity applyEntity = applyNoEntityMap.get( buildApplyentity.getAreqId() );

				if ( null != applyEntity ) {
					pjtViewBean.setApplyNo			( applyEntity.getAreqId() );
					pjtViewBean.setApplySummary		( applyEntity.getSummary() );
					pjtViewBean.setApplyStatus		(
							sheet.getValue( AmDesignBeanId.statusId, applyEntity.getProcStsId() ));
				} else {
					pjtViewBean.setApplyNo			( buildApplyentity.getAreqId() );
				}

				unitClosePjtViewBeanList.add( pjtViewBean );
			}

			viewBean.setUnitClosePjtViewBeanList( unitClosePjtViewBeanList );

			unitCloseConfirmViewBeanList.add( viewBean );
		}

	}
	public static void setAccessablePjtLotEntity(
			List<ILotDto> lotDto, List<IGrpUserLnkEntity> groupUserEntitys,
			List<ILotDto> lotList, List<String> disableLotNoList ) {

		Set<String> groupIdSet = EntityAddonUtils.getGroupIdSet( groupUserEntitys );


		for ( ILotDto lot : lotDto ) {

			// グループの設定なし＝全グループがアクセス可能
			if ( null == lot.getLotGrpLnkEntities() || 0 == lot.getLotGrpLnkEntities().size() ) {
				lotList.add( lot );
			} else {

				boolean listView = false;

				for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

					if ( groupIdSet.contains( group.getGrpId() )) {
						lotList.add( lot );
						listView = true;
						break;
					}
				}

				if ( !listView && null != disableLotNoList ) {
					if ( lot.getLotEntity().getIsAssetDup().parseBoolean() ) {
						lotList.add( lot );
						disableLotNoList.add( lot.getLotEntity().getLotId() );
					}
				}
			}
		}
	}

}
