package jp.co.blueship.tri.bm.domainx.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseListServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public class FlowBuildPackageCloseServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private List<ClosableBuildPackageView> closableViews = new ArrayList<ClosableBuildPackageView>();

	private BuildPackageConfirmView confirmView = new BuildPackageConfirmView();
	private RequestsCompletion result = new RequestsCompletion();


	{
		this.setInnerService( new FlowRelUnitCloseRequestServiceBean() );
		this.setInnerViewService( new FlowRelUnitCloseListServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public List<ClosableBuildPackageView> getClosableViews() {
		return closableViews;
	}

	public FlowBuildPackageCloseServiceBean setClosableViews(List<ClosableBuildPackageView> closableViews) {
		this.closableViews = closableViews;
		return this;
	}

	public BuildPackageConfirmView getConfirmView() {
		return confirmView;
	}
	public FlowBuildPackageCloseServiceBean setConfirmView(BuildPackageConfirmView confirmView) {
		this.confirmView = confirmView;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowBuildPackageCloseServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private FlowBuildPackageCloseInputInfo inputInfo = new FlowBuildPackageCloseInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public FlowBuildPackageCloseInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(FlowBuildPackageCloseInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Input Information
	 */
	public class FlowBuildPackageCloseInputInfo {
		private String baseLineTag;
		private String comment;
		private String[] selectedBpIds = new String[0];
		private boolean submitAgreement;


		public String getBaseLineTag() {
			return baseLineTag;
		}

		public FlowBuildPackageCloseInputInfo setBaseLineTag(String baseLineTag) {
			this.baseLineTag = baseLineTag;
			return this;
		}

		public String getComment() {
			return comment;
		}

		public FlowBuildPackageCloseInputInfo setComment(String comment) {
			this.comment = comment;
			return this;
		}

		public String[] getSelectedBpIds() {
			return selectedBpIds;
		}
		public FlowBuildPackageCloseInputInfo setSelectedBpIds(String... selectedBpIds) {
			this.selectedBpIds = selectedBpIds;
			return this;
		}

		/**
		 * @return the submitAgreement
		 */
		public boolean isSubmitAgreement() {
			return submitAgreement;
		}

		/**
		 * @param submitAgreement the submitAgreement to set
		 */
		public void setSubmitAgreement(boolean submitAgreement) {
			this.submitAgreement = submitAgreement;
		}

	}

	/**
	 * Build Package
	 */
	public class ClosableBuildPackageView {
		private boolean selected;
		private String bpId;
		private String subject;
		private String createdBy;
		private String createdByIconPath;
		private String createdTime;
		private String[] rpIds = new String[0];

		public boolean isSelected() {
			return selected;
		}
		public ClosableBuildPackageView setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}
		public String getBpId() {
			return bpId;
		}
		public ClosableBuildPackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}
		public String getSubject() {
			return subject;
		}
		public ClosableBuildPackageView setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		public String getCreatedBy() {
			return createdBy;
		}
		public ClosableBuildPackageView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}
		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public ClosableBuildPackageView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}
		public String getCreatedTime() {
			return createdTime;
		}
		public ClosableBuildPackageView setCreatedTime(String createdTime) {
			this.createdTime = createdTime;
			return this;
		}
		public String[] getRpIds() {
			return rpIds;
		}
		public ClosableBuildPackageView setRpIds(String... rpIds) {
			if ( null == rpIds ) {
				this.rpIds = new String[0];
			} else {
				this.rpIds = rpIds;
			}

			return this;
		}
	}

	public class BuildPackageConfirmView {
		private String agreementMessage;
		private List<String> warningMessages = new ArrayList<String>();

		public boolean isWarning() {
			if( TriStringUtils.isNotEmpty( agreementMessage ) ) {
				return true;
			}

			if( 0 != warningMessages.size() ) {
				return true;
			}
			return false;
		}

		public String getAgreementMessage() {
			return agreementMessage;
		}
		public BuildPackageConfirmView setAgreementMessage( String agreementMessage ) {
			this.agreementMessage = agreementMessage;
			return this;
		}

		public List<String> getWarningMessages() {
			return warningMessages;
		}
		public BuildPackageConfirmView setWarningMessages( List<String> warningMessages ) {
			this.warningMessages = warningMessages;
			return this;
		}

		public BuildPackageConfirmView clear() {
			this.agreementMessage = null;
			this.warningMessages.clear();

			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
