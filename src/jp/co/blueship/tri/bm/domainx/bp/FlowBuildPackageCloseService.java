package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.IHeadBlEntity;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseListServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.ClosableBuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCloseServiceBean.FlowBuildPackageCloseInputInfo;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowBuildPackageCloseService implements IDomain<FlowBuildPackageCloseServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support;

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	private IDomain<IGeneralServiceBean> closeListService = null;
	private IDomain<IGeneralServiceBean> closeConfirmationService = null;
	private IDomain<IGeneralServiceBean> closeCompleteService = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setCloseListService(IDomain<IGeneralServiceBean> closeListService) {
		this.closeListService = closeListService;
	}

	public void setCloseConfirmationService(
			IDomain<IGeneralServiceBean> closeConfirmationService) {
		this.closeConfirmationService = closeConfirmationService;
	}

	public void setCloseCompleteService(
			IDomain<IGeneralServiceBean> closeCompleteService) {
		this.closeCompleteService = closeCompleteService;
	}

	@Override
	public IServiceDto<FlowBuildPackageCloseServiceBean> execute(
			IServiceDto<FlowBuildPackageCloseServiceBean> serviceDto) {
		FlowBuildPackageCloseServiceBean paramBean = serviceDto.getServiceBean();
		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType()) ) {
				this.validate(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType()) ) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowBuildPackageCloseServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitCloseListServiceBean serviceBean = paramBean.getInnerViewService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean, null);

		// Call Release Unit Close List Service
		{
			closeListService.execute(dto);
		}

		this.afterExecution(serviceBean, null, paramBean);
	}

	private void validate(FlowBuildPackageCloseServiceBean paramBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitCloseRequestServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		// Status Matrix Check
		{
			if( TriStringUtils.isEmpty(paramBean.getParam().getInputInfo().getSelectedBpIds()) ) {
				throw new ContinuableBusinessException(BmMessageId.BM001032E);
			}
			String[] bpIds = paramBean.getParam().getInputInfo().getSelectedBpIds();

			StatusCheckDto statusDto = new StatusCheckDto()
			.setServiceBean( paramBean )
			.setFinder( support )
			.setActionList( statusMatrixAction )
			.setLotIds( paramBean.getParam().getSelectedLotId() )
			.setHeadBls( this.getHeadBlIds( paramBean.getParam().getSelectedLotId() ) )
			.setBpIds( bpIds )
			.setRpIds( this.getRpIds(bpIds) );

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, null, serviceBean);

		// Call Release Unit Close Confirmation Service to check duplicate asset/ warning/ error
		{
			serviceBean.setReferer( RelUnitScreenID.UNIT_CLOSE_LIST );
			serviceBean.setForward( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM );
			closeConfirmationService.execute(dto);
		}

		// Call Release Unit Close Confirmation Service to validate status matrix and accessible group
		{
			serviceBean.setReferer( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM );
			serviceBean.setForward( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST );

			validateBuildPackageClose( paramBean );
			closeConfirmationService.execute(dto);
		}
		{
			paramBean.getConfirmView().clear();

			if( serviceBean.getWarningCheckUnitCloseError().isExistWarning() )
			{

				IContextAdapter ca = ContextAdapterFactory.getContextAdapter() ;

				if ( serviceBean.getInfoMessageIdList() != null && serviceBean.getInfoMessageArgsList() != null )
				{
					for (int i = 0; i <  serviceBean.getInfoMessageIdList().size(); i++ ) {
						paramBean.getMessages().add(ca.getMessage( paramBean.getLanguage(),
													serviceBean.getInfoMessageIdList().get(i),
													serviceBean.getInfoMessageArgsList().get(i) ));
					}
				}

				List<String> closeErrorMessages = serviceBean.getWarningCheckUnitCloseError().getCommentList();

				if( 0 != closeErrorMessages.size() ){
					String agreementMessage = closeErrorMessages.get(0);
					closeErrorMessages.remove(0);

					paramBean.getConfirmView()
							.setAgreementMessage( agreementMessage )
							.setWarningMessages	( closeErrorMessages )
							;
				}
			}
		}
	}

	private static void validateBuildPackageClose( FlowBuildPackageCloseServiceBean paramBean )
			throws BaseBusinessException {
		FlowRelUnitCloseRequestServiceBean serviceBean = paramBean.getInnerService();
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			UnitInfoInputBean unitInfoInputBean = serviceBean.getUnitInfoInputBean();

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType() ) ) {
				if ( TriStringUtils.isEmpty( unitInfoInputBean.getCloseComment() )) {
					messageList.add		( BmMessageId.BM001011E );
					messageArgsList.add	( new String[] {} );
				}


				// パッケージクローズエラーチェック
				WarningCheckBean warningCheckAssetUnitCloseError = serviceBean.getWarningCheckUnitCloseError() ;
				if ( warningCheckAssetUnitCloseError.isExistWarning() ) {

					if ( !warningCheckAssetUnitCloseError.isConfirmCheck() ) {
						messageList.add		( BmMessageId.BM001021E );
						messageArgsList.add	( new String[] {} );
					}
				}
			}

			//２重貸出チェック
			WarningCheckBean warningCheckAssetDuplicate = serviceBean.getWarningCheckAssetDuplicate() ;
			if ( warningCheckAssetDuplicate.isExistWarning() ) {

				if ( !warningCheckAssetDuplicate.isConfirmCheck() ) {
					messageList.add		( BmMessageId.BM001021E );
					messageArgsList.add	( new String[] {} );
				}
			}


		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	private void submitChanges(FlowBuildPackageCloseServiceBean paramBean) {

			IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
			FlowRelUnitCloseRequestServiceBean serviceBean = paramBean.getInnerService();
			TriPropertyUtils.copyProperties(serviceBean, paramBean);
			dto.setServiceBean(serviceBean);

			// Status Matrix Check
			{
				if( TriStringUtils.isEmpty(paramBean.getParam().getInputInfo().getSelectedBpIds()) ) {
					throw new ContinuableBusinessException(BmMessageId.BM001032E);
				}
				String[] bpIds = paramBean.getParam().getInputInfo().getSelectedBpIds();
				List<IBpDto> bpDtoList = this.support.findBpDto(bpIds);
				IRaEntity[] raEntities	= this.support.getRelApplyEntityByBuildNo( this.support.getBuildNo( bpDtoList ) ).getEntities().toArray(new IRaEntity[0]);

				StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( paramBean.getParam().getSelectedLotId() )
				.setHeadBls( this.getHeadBlIds( paramBean.getParam().getSelectedLotId() ) )
				.setBpIds( bpIds )
				.setRaIds( this.support.getRelApplyNo(raEntities) )
				.setRpIds( this.getRpIds(bpIds) );

				StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
			}

			this.beforeExecution(paramBean, null, serviceBean);

			// Call Release Unit Close Confirmation Service to check duplicate asset/ warning/ error
			{
				serviceBean.setReferer( RelUnitScreenID.UNIT_CLOSE_LIST );
				serviceBean.setForward( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM );
				closeConfirmationService.execute(dto);
			}

			{
				serviceBean.setReferer( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM );
				serviceBean.setForward( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST );
			}

			// Call Release Unit Close Confirmation Service to validate status matrix and accessible group
			{
				validateBuildPackageClose( paramBean );
				closeConfirmationService.execute(dto);
			}

			// Call Release Unit Close Complete Service
			{
				closeCompleteService.execute(dto);
			}

			this.afterExecution(null, serviceBean, paramBean);

	}

	private void beforeExecution(FlowBuildPackageCloseServiceBean src,
			FlowRelUnitCloseListServiceBean listDest,
			FlowRelUnitCloseRequestServiceBean entryDest) {

		if (RequestType.init.equals(src.getParam().getRequestType())) {

			listDest.setSelectedLotNo(src.getParam().getSelectedLotId());
			listDest.setDisplayAll(true);
			listDest.setRelUnitSearchBean(null); // New system doesn't have search area

		} else if (RequestType.submitChanges.equals(src.getParam().getRequestType())
				|| RequestType.validate.equals(src.getParam().getRequestType())) {
			IBldSrvEntity srvEntity = this.support.getBmFinderSupport().findBldSrvEntityByController();
			String serverId = srvEntity.getBldSrvId();

			FlowBuildPackageCloseInputInfo inputInfo = src.getParam().getInputInfo();
			UnitInfoInputBean unitInfoInputBean = new UnitInfoInputBean();

			entryDest.setSelectedUnitNo(inputInfo.getSelectedBpIds());
			entryDest.setLockServerId( serverId );
			entryDest.setBuildCloseConfirmBean( false );
			entryDest.setProcId( src.getProcId() );
			unitInfoInputBean.setBaseLineTag( inputInfo.getBaseLineTag() );
			unitInfoInputBean.setCloseComment( inputInfo.getComment() );
			entryDest.setUnitInfoInputBean(unitInfoInputBean);
			if ( RequestType.submitChanges.equals( src.getParam().getRequestType() ) ) {
				entryDest.getWarningCheckUnitCloseError().setConfirmCheck(src.getParam().getInputInfo().isSubmitAgreement());
			}
		}
	}

	private void afterExecution(FlowRelUnitCloseListServiceBean listSrc,
			FlowRelUnitCloseRequestServiceBean entrySrc,
			FlowBuildPackageCloseServiceBean dest) {

		if (RequestType.init.equals(dest.getParam().getRequestType())) {

			if (TriStringUtils.isNotEmpty(listSrc.getUnitViewBeanList())) {
				List<ClosableBuildPackageView> bpViewList = new ArrayList<ClosableBuildPackageView>();
				for (UnitCloseViewBean viewBean: listSrc.getUnitViewBeanList()) {

					ClosableBuildPackageView bpView = dest.new ClosableBuildPackageView();
					SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( dest.getLanguage(), dest.getTimeZone() );

					bpView.setSelected(false)
						.setBpId( viewBean.getRelUnitNo() )
						.setSubject( viewBean.getRelUnitName() )
						.setCreatedBy( viewBean.getCreateBy() )
						.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(viewBean.getCreateById()))
						.setCreatedTime( TriDateUtils.convertViewDateFormat(viewBean.getCreateDate(),formatYMDHM) )
						.setRpIds( viewBean.getRelNoList().toArray(new String [0]) );
					bpViewList.add(bpView);
				}

				dest.setClosableViews(bpViewList);

			} else {
				List<IMessageId> messageList = new ArrayList<IMessageId>();
				List<String[]> messageArgsList = new ArrayList<String[]>();
				if (TriStringUtils.isNotEmpty(listSrc.getInfoMessageId())) {
					messageList.add( listSrc.getInfoMessageId() );
					messageArgsList.add	( new String[] {} );
				}
				if ( 0 != messageList.size() )
					throw new ContinuableBusinessException( messageList, messageArgsList );
			}


		} else if (RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(entrySrc.isLockByThread());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable(BmMessageId.BM003013I);
		}
	}

	private String[] getRpIds( String[] bpIds ){

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setBpIds( bpIds );

		List<IRpBpLnkEntity> rpBpLnkEntityList = this.support.getRmFinderSupport().getRpBpLnkDao().find( condition.getCondition() );
		List<String> rpIdList = new ArrayList<String>();

		for( IRpBpLnkEntity entity :  rpBpLnkEntityList ){
			rpIdList.add(entity.getRpId());
		}

		return rpIdList.toArray(new String[]{});
	}

	private String[] getHeadBlIds(String lotId){

		HeadBlCondition condition = new HeadBlCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement( HistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IHeadBlEntity> entityList =  this.support.getAmFinderSupport().getHeadBlDao().find(condition.getCondition(), sort);
		List<String> headBlIdList = new ArrayList<String>();

		if( entityList.size() != 0){
			headBlIdList.add( entityList.get(0).getHeadBlId() );
		}

		return headBlIdList.toArray(new String[]{});
	}
}
