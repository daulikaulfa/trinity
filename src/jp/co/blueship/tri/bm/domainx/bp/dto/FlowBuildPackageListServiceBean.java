package jp.co.blueship.tri.bm.domainx.bp.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
public class FlowBuildPackageListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<BuildPackageView> buildPackageViews = new ArrayList<BuildPackageView>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<BuildPackageView> getBuildPackageViews() {
		return buildPackageViews;
	}
	public FlowBuildPackageListServiceBean setBuildPackageViews( List<BuildPackageView> buildPackageViews ) {
		this.buildPackageViews = buildPackageViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowBuildPackageListServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private SearchServiceType searchServiceType = SearchServiceType.none;
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();
		private int linesPerPage = 20;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
		
		public SearchServiceType getSearchServiceType() {
			return searchServiceType;
		}

		public RequestParam setSearchServiceType(SearchServiceType searchServiceType) {
			this.searchServiceType = searchServiceType;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter{
		@Expose private String stsId = null;
		@Expose private String ctgId = null;
		@Expose private String mstoneId = null;
		@Expose private String keyword = null;
		private Integer selectedPageNo = 1;
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getStsId() {
			return stsId;
		}
		public SearchCondition setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getCtgId() {
			return ctgId;
		}
		public SearchCondition setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}

		public String getMstoneId() {
			return mstoneId;
		}
		public SearchCondition setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder bpId = TriSortOrder.Desc;

		public TriSortOrder getBpId() {
			return bpId;
		}
		public OrderBy setBpId(TriSortOrder bpId) {
			this.bpId = bpId;
			return this;
		}
	}

	/**
	 * Build Package
	 */
	public class BuildPackageView {
		private boolean isSuccess = false;

		private String bpId;
		private String subject;
		private String createdBy;
		private String createdByIconPath;
		private String createdDate;
		private String stsId;
		private String status;
		private String startTime;
		private String endTime;

		public String getStartTime() {
			return startTime;
		}
		public BuildPackageView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}
		
		public String getEndTime() {
			return endTime;
		}
		public BuildPackageView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}
		
		public boolean isError() {
			return !isSuccess;
		}
		public boolean isSuccess() {
			return isSuccess;
		}
		public BuildPackageView setSuccess(boolean isSuccess) {
			this.isSuccess = isSuccess;
			return this;
		}

		public String getBpId() {
			return bpId;
		}
		public BuildPackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public BuildPackageView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public BuildPackageView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public BuildPackageView setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}

		public String getCreatedDate() {
			return createdDate;
		}
		public BuildPackageView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public BuildPackageView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public BuildPackageView setStatus(String status) {
			this.status = status;
			return this;
		}
	}
	
	public enum SearchServiceType {
		none( "" ),
		SearchSite( "SearchSite" ),
		SearchLot( "SearchLot" ),
		;

		private String value = null;

		private SearchServiceType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SearchServiceType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SearchServiceType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SearchServiceType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

}
