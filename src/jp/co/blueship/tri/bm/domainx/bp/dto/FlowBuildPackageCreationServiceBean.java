package jp.co.blueship.tri.bm.domainx.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.constants.BuildType;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowBuildPackageCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<ChangePropertyView> changePropertyViews = new ArrayList<ChangePropertyView>();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowRelUnitEntryServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public List<ChangePropertyView> getChangePropertyViews() {
		return changePropertyViews;
	}
	public FlowBuildPackageCreationServiceBean setChangePropertyViews(List<ChangePropertyView> changePropertyViews) {
		this.changePropertyViews = changePropertyViews;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowBuildPackageCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private BuildPackageCreationInputInfo inputInfo = new BuildPackageCreationInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public BuildPackageCreationInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(BuildPackageCreationInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Input Information
	 */
	public class BuildPackageCreationInputInfo {
		private String subject;
		private String summary;
		private BuildType buildType = BuildType.Build;
		private String[] selectedPjtIds = new String[0];
		private String ctgId;
		private String mstoneId;
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String getSubject() {
			return subject;
		}
		public BuildPackageCreationInputInfo setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		public String getSummary() {
			return summary;
		}
		public BuildPackageCreationInputInfo setSummary(String summary) {
			this.summary = summary;
			return this;
		}
		public boolean isBuildDiff() {
			return BuildType.Build.equals( this.buildType );
		}
		public BuildPackageCreationInputInfo setBuildDiff( boolean isDiff ) {
			if ( isDiff ) {
				this.buildType = BuildType.Build;
			} else {
				setBuildFull( true );
			}
			return this;
		}
		public boolean isBuildFull() {
			return BuildType.FullBuild.equals( this.buildType );
		}
		public BuildPackageCreationInputInfo setBuildFull( boolean isFull ) {
			if ( isFull ) {
				this.buildType = BuildType.FullBuild;
			} else {
				setBuildDiff( true );
			}
			return this;
		}
		public String[] getSelectedPjtIds() {
			return selectedPjtIds;
		}
		public BuildPackageCreationInputInfo setSelectedPjtIds(String... selectedPjtIds) {
			this.selectedPjtIds = selectedPjtIds;
			return this;
		}
		public String getCtgId() {
			return ctgId;
		}
		public BuildPackageCreationInputInfo setCtgId(String ctgId) {
			this.ctgId = ctgId;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public BuildPackageCreationInputInfo setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}
		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public BuildPackageCreationInputInfo setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}
		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public BuildPackageCreationInputInfo setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}
	}

	/**
	 * Change Property
	 */
	public class ChangePropertyView {
		private boolean selected = false;
		private String pjtId;
		private String referenceId;
		private String summary;
		private String submitterNm;
		private String submitterIconPath;
		private String approvedTime;

		public boolean isSelected() {
			return selected;
		}
		public ChangePropertyView setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}
		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}
		public String getSummary() {
			return summary;
		}
		public ChangePropertyView setSummary(String summary) {
			this.summary = summary;
			return this;
		}
		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}
		public String getSubmitterNm() {
			return submitterNm;
		}
		public ChangePropertyView setSubmitterNm(String submitterNm) {
			this.submitterNm = submitterNm;
			return this;
		}

		public String getSubmitterIconPath() {
			return submitterIconPath;
		}
		public ChangePropertyView setSubmitterIconPath(String submitterIconPath) {
			this.submitterIconPath = submitterIconPath;
			return this;
		}

		public String getApprovedTime() {
			return approvedTime;
		}
		public ChangePropertyView setApprovedTime(String approvedTime) {
			this.approvedTime = approvedTime;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String bpId;
		private boolean completed = false;

		public String getBpId() {
			return bpId;
		}
		public RequestsCompletion setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

}
