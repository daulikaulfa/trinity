package jp.co.blueship.tri.bm.domainx.bp.dto;

import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelServiceBean;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	{
		this.setInnerService( new FlowRelUnitCancelServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowBuildPackageRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String bpId = null;

		public String getSelectedBpId() {
			return bpId;
		}
		public RequestParam setSelectedBpId(String selectedBpId) {
			this.bpId = selectedBpId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
