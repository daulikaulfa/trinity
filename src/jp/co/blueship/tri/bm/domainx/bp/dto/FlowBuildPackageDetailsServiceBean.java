package jp.co.blueship.tri.bm.domainx.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.constants.BuildType;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowBuildPackageDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private BuildPackageView detailsView = new BuildPackageView();

	{
		this.setInnerService( new FlowRelUnitHistoryDetailViewServiceBean() );
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	public BuildPackageView getDetailsView() {
		return detailsView;
	}
	public FlowBuildPackageDetailsServiceBean setDetailsView(BuildPackageView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String bpId = null;

		public String getSelectedBpId() {
			return bpId;
		}
		public RequestParam setSelectedBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}
	}

	public class BuildPackageView {
		private String bpId;
		private String subject;
		private String stsId;
		private String status;
		private String createdBy;
		private String startTime;
		private String endTime;
		private String summary;
		private BuildType buildType = BuildType.none;
		private String ctgNm;
		private String mstoneNm;

		private ResourcesView totalResources = new ResourcesView();
		private List<ChangePropertyView> changeProperties = new ArrayList<ChangePropertyView>();
		private List<ProcessingResultView> processingResults = new ArrayList<ProcessingResultView>();

		public String getBpId() {
			return bpId;
		}
		public BuildPackageView setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public BuildPackageView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public BuildPackageView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public BuildPackageView setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public BuildPackageView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public BuildPackageView setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public BuildPackageView setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public String getSummary() {
			return summary;
		}
		public BuildPackageView setSummary(String summary) {
			this.summary = summary;
			return this;
		}

		public boolean isBuildDiff() {
			return BuildType.Build.equals( this.buildType );
		}
		public BuildPackageView setBuildDiff( boolean isDiff ) {
			if ( isDiff ) {
				this.buildType = BuildType.Build;
			} else {
				setBuildFull( true );
			}
			return this;
		}

		public boolean isBuildFull() {
			return BuildType.FullBuild.equals( this.buildType );
		}

		public BuildPackageView setBuildFull( boolean isFull ) {
			if ( isFull ) {
				this.buildType = BuildType.FullBuild;
			} else {
				setBuildDiff( true );
			}
			return this;
		}

		public String getCtgNm() {
			return ctgNm;
		}
		public BuildPackageView setCtgNm(String ctgNm) {
			this.ctgNm = ctgNm;
			return this;
		}

		public String getMstoneNm() {
			return mstoneNm;
		}
		public BuildPackageView setMstoneNm(String mstoneNm) {
			this.mstoneNm = mstoneNm;
			return this;
		}

		public ResourcesView getTotalResources() {
			return totalResources;
		}
		public BuildPackageView setTotalResources(ResourcesView totalResources) {
			this.totalResources = totalResources;
			return this;
		}

		public List<ChangePropertyView> getChangeProperties() {
			return changeProperties;
		}
		public BuildPackageView setChangeProperties(List<ChangePropertyView> changeProperties) {
			this.changeProperties = changeProperties;
			return this;
		}

		public List<ProcessingResultView> getProcessResults() {
			return processingResults;
		}
		public BuildPackageView setProcessResults(List<ProcessingResultView> processingResults) {
			this.processingResults = processingResults;
			return this;
		}
	}

	public class ResourcesView {
		private Integer checkoutResources;
		private Integer changedResources;
		private Integer addedResources;
		private Integer deletedAsciiResources;
		private Integer deletedBinaryResources;

		public Integer getCheckoutResources() {
			return checkoutResources;
		}

		public ResourcesView setCheckoutResources(Integer checkoutResources) {
			this.checkoutResources = checkoutResources;
			return this;
		}

		public Integer getChangedResources() {
			return changedResources;
		}

		public ResourcesView setChangedResources(Integer changedResources) {
			this.changedResources = changedResources;
			return this;
		}

		public Integer getAddedResources() {
			return addedResources;
		}

		public ResourcesView setAddedResources(Integer addedResources) {
			this.addedResources = addedResources;
			return this;
		}

		public Integer getDeletedAsciiResources() {
			return deletedAsciiResources;
		}

		public ResourcesView setDeletedAsciiResources(Integer deletedAsciiResources) {
			this.deletedAsciiResources = deletedAsciiResources;
			return this;
		}

		public Integer getDeletedBinaryResources() {
			return deletedBinaryResources;
		}

		public ResourcesView setDeletedBinaryResources(Integer deletedBinaryResources) {
			this.deletedBinaryResources = deletedBinaryResources;
			return this;
		}
	}

	public class ChangePropertyView {
		private String pjtId;
		private String referenceId;
		private List<RequestResourcesView> resources = new ArrayList<RequestResourcesView>();

		public String getPjtId() {
			return pjtId;
		}
		public ChangePropertyView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public ChangePropertyView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public List<RequestResourcesView> getResources() {
			return resources;
		}
		public ChangePropertyView setResources(List<RequestResourcesView> resources) {
			this.resources = resources;
			return this;
		}
	}

	public class RequestResourcesView extends ResourcesView {
		private String areqId;
		private AreqCtgCd areqType;

		public AreqCtgCd getAreqType() {
			return areqType;
		}
		public RequestResourcesView setAreqType(AreqCtgCd areqType) {
			this.areqType = areqType;
			return this;
		}

		public boolean isCheckinRequest() {
			return BmBusinessJudgUtils.isReturnApply(this.areqType.value());
		}
		public boolean isRemovalRequest() {
			return BmBusinessJudgUtils.isDeleteApply(this.areqType.value());
		}

		public String getAreqId() {
			return areqId;
		}
		public RequestResourcesView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}
	}

	public class ProcessingResultView {
		private String sequenceNo;
		private String processNm;
		private ProcessStatus result = ProcessStatus.none;

		public String getSequenceNo() {
			return sequenceNo;
		}
		public ProcessingResultView setSequenceNo(String sequenceNo) {
			this.sequenceNo = sequenceNo;
			return this;
		}

		public String getProcessNm() {
			return processNm;
		}
		public ProcessingResultView setProcessNm(String processNm) {
			this.processNm = processNm;
			return this;
		}

		public ProcessingResultView setResult(ProcessStatus result) {
			this.result = result;
			return this;
		}

		public boolean isActive() {
			return ProcessStatus.Active.equals( result );
		}
		public boolean isCompleted() {
			return ProcessStatus.Completed.equals( result );
		}
		public boolean isError() {
			return ProcessStatus.Error.equals( result );
		}

	}

}
