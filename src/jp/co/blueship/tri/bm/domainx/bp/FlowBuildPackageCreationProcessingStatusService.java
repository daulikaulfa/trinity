package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;
import java.util.*;

import jp.co.blueship.tri.bm.dao.bldenv.eb.BldEnvSrvCondition;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.BldSrvCondition;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationProcessingStatusServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean.Task;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean.Timeline;
import jp.co.blueship.tri.bm.task.TaskDetailsViewBean.TimelineHeader;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.TaskFlowStatus;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.SmProgressNotificationUtils;
import jp.co.blueship.tri.fw.sm.dao.procmgt.constants.ProcMgtItems;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.IProcMgtEntity;
import jp.co.blueship.tri.fw.sm.dao.procmgt.eb.ProcMgtCondition;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;
import jp.co.blueship.tri.fw.svc.beans.dto.ProgressBar;

public class FlowBuildPackageCreationProcessingStatusService implements IDomain<FlowBuildPackageCreationProcessingStatusServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowBuildPackageCreationProcessingStatusServiceBean> execute(
			IServiceDto<FlowBuildPackageCreationProcessingStatusServiceBean> serviceDto) {
		FlowBuildPackageCreationProcessingStatusServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);

			}
			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowBuildPackageCreationProcessingStatusServiceBean paramBean) {
		String lotId = paramBean.getParam().getSelectedLotId();

		// Bp情報取得
		BpCondition bpCondition = new BpCondition();
		bpCondition.setBpId(this.getTaskBpId(lotId));
		ISqlSort sort = new SortBuilder();
		sort.setElement(BpItems.lotId, TriSortOrder.Desc, 0);
		List<IBpEntity> bpEntities = this.support.getBpDao().find(bpCondition.getCondition(), sort);
		if( !TriCollectionUtils.isEmpty( bpEntities ) )	{
			IBpEntity bpEntity = bpEntities.get(0);
			String bpId = bpEntity.getBpId();

			long predictionProcessTime = 0;

			ProcessStatus status = ProcessStatus.Active;
			if( BmBpStatusId.BuildPackageCreated.equals(bpEntity.getStsId())
					|| BmBpStatusId.BuildPackageClosed.equals(bpEntity.getStsId())
					|| BmBpStatusId.BuildPackageRemoved.equals(bpEntity.getStsId())) {
				status = ProcessStatus.Completed;
			} else if( BmBpStatusId.Unprocessed.equals(bpEntity.getStsId()) && bpEntity.getProcEndTimestamp() != null ) {
				status = ProcessStatus.Error;
			} else {
				predictionProcessTime = this.getPredictionProcessTime();
			}
			long elapsedTime = TriDateUtils.getSystemTimestamp().getTime() - bpEntity.getProcStTimestamp().getTime();
			if(predictionProcessTime <= elapsedTime) predictionProcessTime = elapsedTime + 1000;
			String remainingTime = "";
//			String remainingTime = String.valueOf((predictionProcessTime - elapsedTime)/1000);
			IProgressBar overviewBar;
			if( status.equals(ProcessStatus.Active) ) {
				overviewBar = this.getProgressBar((int)(predictionProcessTime/1000),(int)(elapsedTime/1000));
				remainingTime = SmProgressNotificationUtils.stringOf( paramBean.getLanguage() , predictionProcessTime - elapsedTime);
			} else {
				overviewBar = this.getProgressBar(1, 1);
				remainingTime = "";
			}

			SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());
			paramBean.getOverview()
				.setBpId( bpId )
				.setCreatedBy( bpEntity.getExecUserNm() )
				.setCreatedByIconPath(this.support.getUmFinderSupport().getIconPath( bpEntity.getExecUserId()) )
				.setStartTime( TriDateUtils.convertViewDateFormat(bpEntity.getProcStTimestamp(),formatYMDHM).toString() )
				.setEndTime( TriDateUtils.convertViewDateFormat(bpEntity.getProcEndTimestamp(),formatYMDHM).toString() )
				.setResult( status )
				.setBar( overviewBar )
				.setRemainingTime( remainingTime )
				;

			this.makeTaskView( bpEntity, paramBean );
		}

	}

	// onChange
	private void onChange(FlowBuildPackageCreationProcessingStatusServiceBean paramBean) {
		init( paramBean );
	}

	private final void makeTaskView(IBpEntity bpEntity, FlowBuildPackageCreationProcessingStatusServiceBean paramBean) {
		TaskDetailsViewBean detailsView = paramBean.getDetailsView();

		List<TimelineHeader> timelineHeaders = new ArrayList<TimelineHeader>();

		BldEnvSrvCondition bldEnvSrvCondition = new BldEnvSrvCondition();
		bldEnvSrvCondition.setBldEnvId(bpEntity.getBldEnvId());
		ISqlSort sort = new SortBuilder();
		List<IBldEnvSrvEntity> bldEnvSrvEntities = this.support.getBldEnvSrvDao().find(bldEnvSrvCondition.getCondition(), sort);
		List<IBldSrvEntity> bldSrvEntities = new ArrayList<IBldSrvEntity>();
		for( IBldEnvSrvEntity bldEnvSrvEntity : bldEnvSrvEntities ) {
			BldSrvCondition bldSrvCondition = new BldSrvCondition();
			bldSrvCondition.setBldSrvId(bldEnvSrvEntity.getBldSrvId());
			IBldSrvEntity bldSrvEntity = this.support.getBldSrvDao().findByPrimaryKey(bldSrvCondition.getCondition());
			bldSrvEntities.add(bldSrvEntity);

		}

		IBldTimelineEntity[] timeLineEntitys	= new IBldTimelineEntity[0];
		timeLineEntitys	= this.support.getBldTimelineEntity( bpEntity.getBldEnvId() );
		ITaskFlowEntity[] taskFlowEntitys = new ITaskFlowEntity[0] ;
		taskFlowEntitys = this.support.getTaskFlowEntity();
		//ラインの処理状況を取得
		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(bpEntity.getProcId());
		ITaskFlowProcEntity[] process = this.support.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);

		// タイムラインの取得情報を正規化
		Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> lineMap = this.normalizeTimeLine( timeLineEntitys,  bldSrvEntities.toArray(new IBldSrvEntity[0])  );
		//処理結果の取得情報を正規化
		Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> processMap = this.normalizeProcess( process,  bldSrvEntities.toArray(new IBldSrvEntity[0]) );
		//システムを変換
		Map<String, IBldSrvEntity> systemMap = convertSystem( bldSrvEntities.toArray(new IBldSrvEntity[0]) );//変更
		//タスクをMap化
		Map<String , ITaskEntity> taskMap = this.mapTask( taskFlowEntitys ) ;

		// タスク一覧を設定
		String activeServerNo = null;

		String activeSystemNo = null;
		boolean isBreakSystem = true;

		List<Timeline> timeLines = new ArrayList<Timeline>();
		int lineCntMax = timeLineEntitys.length;
		int srvCntMax = 1;
		for( IBldTimelineEntity bldTimelineEntity : timeLineEntitys ) {
			if( bldTimelineEntity.getLine().length > srvCntMax ) {
				srvCntMax = bldTimelineEntity.getLine().length;
			}
		}
		Task[][] tasks = new Task[lineCntMax][srvCntMax];
		int[] lineNos = new int[lineCntMax];
		int srvCnt = 0;
		for ( IBldSrvEntity serverEntity : lineMap.keySet() ) {

			if ( null == activeServerNo || ! serverEntity.getBldSrvId().equals( activeServerNo )) {
				activeServerNo = serverEntity.getBldSrvId();
				isBreakSystem = true;
			}

			String msgServer = null ;
			String osTypeId = null;

			Map<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMap = lineMap.get( serverEntity );
			Map<String, ITaskFlowProcEntity> processLineMap = processMap.get( serverEntity );
			int lineCnt = 0;
			for ( Map.Entry<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMapEntry : targetLineMap.entrySet() ) {

				IBldTimelineEntity timeLineEntity = targetLineMapEntry.getKey();
				IBldTimelineAgentEntity lineEntity = targetLineMapEntry.getValue();

				Task task = detailsView.new Task();
				long taskProcessTime = 1;
				long elapsedTime = 0;
				IProgressBar bar = new ProgressBar();

				//タイムライン上に業務処理が存在する場合
				if ( null != lineEntity ) {
					if ( null == activeSystemNo || ! lineEntity.getBldSrvId().equals( activeSystemNo )) {
						activeSystemNo = lineEntity.getBldSrvId();
						isBreakSystem = true;
					}

					if ( isBreakSystem ) {
						IBldSrvEntity bldSrvEntity = systemMap.get( lineEntity.getBldSrvId() ) ;
						if( null == bldSrvEntity ) {
							throw new TriSystemException(BmMessageId.BM005041S , lineEntity.getBldSrvId() );
						}

						osTypeId = bldSrvEntity.getOsTyp();
						isBreakSystem = false;
					}

					// プロセスステータス
					if ( null != processLineMap ) {

						ITaskFlowProcEntity processEntity = processLineMap.get( timeLineEntity.getBldLineNo().toString() );

						if ( null != processEntity ) {
							if ( BmBpStatusIdForExecData.CreatingBuildPackage.equals( processEntity.getStsId() )) {
								//imgPathList.add( IMAGE_NEXT_ARROW_ON );
								task.setResult(ProcessStatus.Active);
								taskProcessTime = this.getTaskProcessTime(timeLineEntity.getBldLineNo());
								elapsedTime = TriDateUtils.getSystemTimestamp().getTime() - processEntity.getProcStTimestamp().getTime();
								if(taskProcessTime <= elapsedTime) taskProcessTime = elapsedTime + 1000;
							}
							if ( BmBpStatusIdForExecData.BuildPackageError.equals( processEntity.getStsId() )) {
								//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
								task.setResult(ProcessStatus.Error);
								paramBean.getMessages().add(processEntity.getMsg());
							}
							if ( BmBpStatusId.BuildPackageCreated.equals( processEntity.getStsId() )) {
								//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
								task.setResult(ProcessStatus.Completed);
								elapsedTime = 1;
							}
						} else {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							task.setResult(ProcessStatus.none);
						}
					} else {
						//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
						task.setResult(ProcessStatus.none);
					}

					// タスク名設定
					ITaskEntity taskEntity = taskMap.get( lineEntity.getTaskFlowId() ) ;
					String taskNm = "" ;
					if( null != taskEntity ) {
						Map<String,ITaskTargetEntity> targetMap = this.mapTarget( taskEntity ) ;
						if( targetMap.containsKey( lineEntity.getTargetSeqNo().toString() ) ) {
							taskNm = targetMap.get( lineEntity.getTargetSeqNo().toString() ).getName() ;
						}
					}
					task.setTaskNm(taskNm);
					task.setTaskStatus(TaskFlowStatus.Exist);
				} else {
					task.setResult(ProcessStatus.none);
					task.setTaskNm("");
					task.setTaskStatus(TaskFlowStatus.Arrow);
				}
				int procValue = (int) (((double)elapsedTime/taskProcessTime)*100);
				bar.setValue( procValue );
				task.setBar(bar);
				String taskRemainingTime = SmProgressNotificationUtils.stringOf( paramBean.getLanguage() , taskProcessTime - elapsedTime);
				task.setRemainingTime( taskRemainingTime );

				tasks[lineCnt][srvCnt] = task;
				lineNos[lineCnt] = timeLineEntity.getBldLineNo();
				lineCnt++;
			}
			// header
			TimelineHeader timelineHeader = detailsView.new TimelineHeader();
			timelineHeader.setOsType(osTypeId);
			timelineHeader.setServerNm(msgServer);
			timelineHeaders.add(timelineHeader);

			srvCnt++;
		}
		detailsView.setHeaders(timelineHeaders);

		for( int i = 0; i < srvCnt; i++ ) {
			Task bottom = null;
			for(int j = 0; j < lineCntMax; j++) {
				Task task = tasks[j][i];
				if(task.isExist()) {
					bottom = task;
				}
			}
			//find out bottom
			if(bottom != null) {
				bottom.setTaskStatus(TaskFlowStatus.Bottom);
			} else {
				tasks[lineCntMax - 1][i].setTaskStatus(TaskFlowStatus.Bottom);
			}

			//set empty after bottom
			boolean bottomReach = false;
			for(int j = 0; j < lineCntMax; j++) {
				Task t = tasks[j][i];
				if(bottomReach) {
					t.setTaskStatus(TaskFlowStatus.Empty);
				}
				if(t.isBottom()) {
					bottomReach = true;
				}
			}
		}

		int taskCnt = 0;
		for( Task[] t1 : tasks ) {
			Timeline timeLine = detailsView.new Timeline();
			List<Task> addTasks = new ArrayList<Task>();
			Collections.addAll(addTasks, t1);
			timeLine.setLineNo(lineNos[taskCnt++]);
			timeLine.setTasks(addTasks);
			timeLines.add(timeLine);
		}
		detailsView.setTimeLines(timeLines);

	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param timelineEntitys ビルドパッケージ・タイムラインエンティティー
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> normalizeTimeLine(
			IBldTimelineEntity[] timelineEntitys,
			IBldSrvEntity[] bldSrvEntities) {

		IUcfServerEntityComparator comparator = new IUcfServerEntityComparator();

		Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> timeLineMap = new TreeMap<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>>( comparator );

		for ( IBldTimelineEntity timeline : timelineEntitys ) {
			for ( IBldTimelineAgentEntity line : timeline.getLine() ) {

				IBldSrvEntity server = null;
				for( IBldSrvEntity bldSrvEntity : bldSrvEntities ) {
					if( line.getBldSrvId().equals(bldSrvEntity.getBldSrvId()) )
					{
						server = bldSrvEntity;
						break;
					}
				}

				Map<IBldTimelineEntity, IBldTimelineAgentEntity> lineMap = null;

				if ( (! timeLineMap.containsKey( server ))
						|| (null == timeLineMap.get( server )) ) {

					lineMap = new TreeMap<IBldTimelineEntity, IBldTimelineAgentEntity>( new IBldTimelineEntityComparator() );
					timeLineMap.put( server, lineMap );
				}

				lineMap = timeLineMap.get( server );
				lineMap.put( timeline, line );
			}
		}

		// ↑の処理だと処理していないlineNoを詰めてしまうので、処理していない部分に明示的にnullをsetする
		for (Map<IBldTimelineEntity, IBldTimelineAgentEntity> map : timeLineMap.values()) {

			for (IBldTimelineEntity entity : timelineEntitys) {

				if( map.containsKey( entity ) ) {

				} else {
					map.put(entity, null);
				}
			}
		}

		return timeLineMap;
	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param processs ビルドパッケージ状況
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> normalizeProcess(
														ITaskFlowProcEntity[] processs,
														IBldSrvEntity[] bldSrvEntities ) {

		Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> timeLineMap = new HashMap<IBldSrvEntity, Map<String, ITaskFlowProcEntity>>();

		for ( ITaskFlowProcEntity process : processs ) {

			IBldSrvEntity server = null;
			for( IBldSrvEntity bldSrvEntity : bldSrvEntities ) {
				if( process.getBldSrvId().equals(bldSrvEntity.getBldSrvId()) )
				{
					server = bldSrvEntity;
					break;
				}
			}

			Map<String, ITaskFlowProcEntity> lineMap = null;

			if ( (! timeLineMap.containsKey( server ))
					|| (null == timeLineMap.get( server )) ) {
				lineMap = new HashMap<String, ITaskFlowProcEntity>();
				timeLineMap.put( server, lineMap );
			}

			lineMap = timeLineMap.get( server );
			lineMap.put( process.getBldLineNo().toString(), process );
		}

		return timeLineMap;
	}

	/**
	 * システム検索の効率化のため、マップに変換します。
	 *
	 * @param systems サーバ・システム情報
	 * @return システム単位にマップし直した情報を戻します。
	 */
	private final Map<String, IBldSrvEntity> convertSystem( IBldSrvEntity[] systems ) {
		Map<String, IBldSrvEntity> map = new HashMap<String, IBldSrvEntity>();

		for ( IBldSrvEntity system : systems ) {
			map.put(system.getBldSrvId(), system);
		}

		return map;
	}

	/**
	 * タスク情報を、workflowNoをキーとしたマップに格納して返す。
	 * @param taskFlowEntities タスク情報の配列
	 * @return マップ
	 * <pre>
	 * 		Key		: workflowNo
	 * 		Value	: ITaskEntity
	 * </pre>
	 */
	private final Map<String , ITaskEntity> mapTask( ITaskFlowEntity[] taskFlowEntities ) {
		Map<String , ITaskEntity> map = new HashMap<String , ITaskEntity>() ;

		for( ITaskFlowEntity taskFlowEntity : taskFlowEntities ) {
			map.put( taskFlowEntity.getTaskFlowId() , taskFlowEntity.getTask() ) ;
		}
		return map ;
	}

	/**
	 * タスクのターゲット情報（TaskTargetを、sequenceNoをキーとしたマップに格納して返す。
	 * @param taskEntity タスク情報
	 * @return マップ
	 * <pre>
	 * 		Key		: target.sequenceNo
	 * 		Value	: ITaskTargetEntity
	 * </pre>
	 */
	private final Map<String , ITaskTargetEntity> mapTarget( ITaskEntity taskEntity ) {
		Map<String , ITaskTargetEntity> map = new HashMap<String , ITaskTargetEntity>() ;

		ITaskTargetEntity[] targetEntitys = taskEntity.getTarget() ;
		for( ITaskTargetEntity targetEntity : targetEntitys ) {
			String seqNo = targetEntity.getSequenceNo().toString();
			map.put( seqNo , targetEntity ) ;
		}
		return map ;
	}

	private class IUcfServerEntityComparator implements Comparator<IBldSrvEntity> {

		public int compare(IBldSrvEntity obj1, IBldSrvEntity obj2) {

			String serverNo1 = obj1.getBldSrvId();
			String serverNo2 = obj2.getBldSrvId();

			if ( StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
					return 1;
				}
			}
			if ( StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
					return -1;
				}
			}

			return serverNo1.compareTo( serverNo2 );

		}
	}

	private class IBldTimelineEntityComparator implements Comparator<IBldTimelineEntity> {

		public int compare(IBldTimelineEntity obj1, IBldTimelineEntity obj2) {

			String lineNo1 = obj1.getBldLineNo().toString();
			String lineNo2 = obj2.getBldLineNo().toString();

			int intLineNo1 = Integer.parseInt(lineNo1);
			int intLineNo2 = Integer.parseInt(lineNo2);

			return intLineNo1 - intLineNo2;
		}
	}


	private final String getTaskBpId( String lotId )
	{
		String bpId = null;
		BpCondition bpCondition = new BpCondition();
		bpCondition.setLotId( lotId );
		ISqlSort sort = new SortBuilder();
		sort.setElement(BpItems.bpId, TriSortOrder.Desc, 1);
		List<IBpEntity> bpEntities = this.support.getBmFinderSupport().getBpDao().find(bpCondition.getCondition(), sort);
		if( bpEntities.size()>0 ) {
			for ( IBpEntity bpEntity : bpEntities ) {
				TaskFlowProcCondition condition = new TaskFlowProcCondition();
				condition.setProcId(bpEntity.getProcId());
				ITaskFlowProcEntity[] buildProcessEntities = this.support.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);
				if( buildProcessEntities!=null && buildProcessEntities.length>0 ){
					bpId = bpEntity.getBpId();
					break;
				}
			}
			if( bpId == null ) {
				bpId = bpEntities.get(0).getBpId();
			}
		}
		return bpId;
	}
	
	private IProgressBar getProgressBar(int maximum , int minimum){
		IProgressBar progressBar = new ProgressBar()
				.setMaximum(maximum)
				.setMinimum(minimum)
				;

		progressBar.setValue((int)(progressBar.getPercentComplete()*100));

		return progressBar;
	}

	private long getPredictionProcessTime(){

		ProcMgtCondition condition = new ProcMgtCondition();
		String[] serviceIds = {ServiceId.BmBuildPackageCreationService.value(),ServiceId.BmBuildPackageCreationService.valueOfV3()};
		condition.setServiceIds(serviceIds);
		condition.setStsId(SmProcMgtStatusId.Success.getStatusId());
		condition.setDelStsId( (StatusFlg)null );

		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcMgtItems.updTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IProcMgtEntity> entityLimit = this.support.getSmFinderSupport().getProcMgtDao().find(condition.getCondition() , sort , 1 , 1);

		List<IProcMgtEntity> entityList = entityLimit.getEntities();

		if(entityList.size() <= 0) return 0;

		long sumTime =0;
		for(IProcMgtEntity entity : entityList)
			sumTime += entity.getProcEndTimestamp().getTime() - entity.getProcStTimestamp().getTime();

		return sumTime / entityList.size();
	}

	private long getTaskProcessTime( int bldLineNo ){

		ProcMgtCondition condition = new ProcMgtCondition();
		String[] serviceIds = {ServiceId.BmBuildPackageCreationService.value(),ServiceId.BmBuildPackageCreationService.valueOfV3()};
		condition.setServiceIds(serviceIds);
		condition.setStsId(SmProcMgtStatusId.Success.getStatusId());
		condition.setDelStsId( StatusFlg.on );

		ISqlSort sort = new SortBuilder();
		sort.setElement(ProcMgtItems.updTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IProcMgtEntity> entityLimit = this.support.getSmFinderSupport().getProcMgtDao().find(condition.getCondition() , sort , 1 , 5);

		List<IProcMgtEntity> entityList = entityLimit.getEntities();

		if(entityList.size() <= 0) return 0;

		TaskFlowProcCondition taskCondition = new TaskFlowProcCondition();
		taskCondition.setProcId(entityList.get(0).getProcId());
		taskCondition.setBldLineNo(bldLineNo);
		ITaskFlowProcEntity[] taskFlowEntities = this.support.getTaskFlowProcDao().find( taskCondition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);
		if( taskFlowEntities.length <= 0 ) return 0;
		long sumTime = taskFlowEntities[0].getProcEndTimestamp().getTime() - taskFlowEntities[0].getProcStTimestamp().getTime();

		return sumTime;
	}

}
