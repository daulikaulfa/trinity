package jp.co.blueship.tri.bm.domainx.bp.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.BuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowBuildPackageListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private BuildPackageListFileResponse fileResponse = new BuildPackageListFileResponse();
	private List<BuildPackageView> buildPackageView = new ArrayList<BuildPackageView>();

	{
		this.setInnerService( new FlowBuildPackageListServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowBuildPackageListServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public BuildPackageListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowBuildPackageListServiceExportToFileBean setFileResponse(BuildPackageListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<BuildPackageView> getBuildPackageViews() {
		return buildPackageView;
	}

	public void setBuildPackageViews(List<BuildPackageView> buildPackageView) {
		this.buildPackageView = buildPackageView;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowBuildPackageListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class BuildPackageListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
