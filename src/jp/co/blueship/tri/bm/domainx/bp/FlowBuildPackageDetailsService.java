package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.PjtViewBean.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.BuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.ChangePropertyView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.ProcessingResultView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.RequestResourcesView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageDetailsServiceBean.ResourcesView;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.UnitResultViewBean;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;

/**
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageDetailsService implements IDomain<FlowBuildPackageDetailsServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;

	private IDomain<IGeneralServiceBean> detailsService = null;

	public void setSupport (FlowRelUnitEditSupport support) {
		this.support = support;
	}

	public void setDetailsService(IDomain<IGeneralServiceBean> detailsService) {
		this.detailsService = detailsService;
	}

	@Override
	public IServiceDto<FlowBuildPackageDetailsServiceBean> execute(
				IServiceDto<FlowBuildPackageDetailsServiceBean> serviceDto) {

		FlowBuildPackageDetailsServiceBean serviceBean = serviceDto.getServiceBean();
		FlowRelUnitHistoryDetailViewServiceBean innerSericeBean = serviceBean.getInnerService();

		try {
			PreConditions.assertOf( serviceBean != null, "ServiceBean is not specified");

			String bpId = serviceBean.getParam().getSelectedBpId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty(bpId), "SelectedBpId is not specified" );

			if ( RequestType.init.equals( serviceBean.getParam().getRequestType()) ) {
				this.init(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM004000F, e);
		} finally {
			ExceptionUtils.copyToParent(serviceBean, innerSericeBean);
		}
	}

	private void init (FlowBuildPackageDetailsServiceBean serviceBean) {

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitHistoryDetailViewServiceBean innerBean = serviceBean.getInnerService();
		TriPropertyUtils.copyProperties(innerBean, serviceBean);
		dto.setServiceBean(innerBean);

		this.beforeExecution(serviceBean, innerBean);
		detailsService.execute(dto);
		this.afterExecution(innerBean, serviceBean);
	}

	private void beforeExecution( FlowBuildPackageDetailsServiceBean src,
								  FlowRelUnitHistoryDetailViewServiceBean dest) {

		dest.setSelectedBuildNo(src.getParam().getSelectedBpId());
	}

	private void afterExecution( FlowRelUnitHistoryDetailViewServiceBean src,
								 FlowBuildPackageDetailsServiceBean dest) {

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat(dest.getLanguage(), dest.getTimeZone());


		String bpId = dest.getParam().getSelectedBpId();
		IBpEntity bpEntity = this.support.findBpEntity(bpId);
		ILotEntity lotEntity = this.support.getAmFinderSupport().findLotDto( bpEntity.getLotId() ).getLotEntity();


		BuildPackageView view = dest.new BuildPackageView()
									.setBpId		( bpEntity.getBpId() )
									.setStsId		( bpEntity.getProcStsId() )
									.setStatus		( sheet.getValue(BmDesignBeanId.statusId, bpEntity.getProcStsId()) )
									.setSubject		( bpEntity.getBpNm() )
									.setCreatedBy	( bpEntity.getRegUserNm() )
									.setStartTime	( TriDateUtils.convertViewDateFormat(bpEntity.getProcEndTimestamp(), formatYMDHM) )
									.setEndTime		( TriDateUtils.convertViewDateFormat(bpEntity.getProcEndTimestamp(), formatYMDHM) )
									.setSummary		( bpEntity.getSummary() )
									.setBuildDiff	( TriStringUtils.isEquals(bpEntity.getBldEnvId(), lotEntity.getBldEnvId()) ? true : false )
									.setCtgNm		( bpEntity.getCtgNm() )
									.setMstoneNm	( bpEntity.getMstoneNm() );

		dest.setDetailsView(view);
		this.setTotalResources(src, dest);
		this.setChangePropertyList(src, dest);
		this.setProcessResults(src, dest);
	}

	private void setTotalResources( FlowRelUnitHistoryDetailViewServiceBean src,
									FlowBuildPackageDetailsServiceBean dest) {

		ResourcesView totalResources = dest.new ResourcesView()
										   .setCheckoutResources		(src.getLendAssetSumCount())
										   .setChangedResources			(src.getChangeAssetSumCount())
										   .setDeletedAsciiResources	(src.getDeleteAssetSumCount())
										   .setDeletedBinaryResources	(src.getDeleteBinaryAssetSumCount())
										   .setAddedResources			(src.getAddAssetSumCount());

		dest.getDetailsView().setTotalResources(totalResources);
	}

	private void setChangePropertyList( FlowRelUnitHistoryDetailViewServiceBean src,
										FlowBuildPackageDetailsServiceBean dest) {
		List<ChangePropertyView> changePropertyList = new ArrayList<ChangePropertyView>();

		for (PjtViewBean pjt : src.getPjtViewBeanList()) {

			ChangePropertyView changeProperty = dest.new ChangePropertyView()
													.setPjtId( pjt.getPjtNo() )
													.setReferenceId( pjt.getChangeCauseNo() );

			List<RequestResourcesView> resourceList = new ArrayList<RequestResourcesView>();

			for (ApplyInfoViewBean applyInfoView : pjt.getApplyInfoViewBeanList()) {

				IAreqEntity areqEntity = this.support.getAmFinderSupport().findAreqEntity(applyInfoView.getApplyNo());
				RequestResourcesView resource = dest.new RequestResourcesView()
													.setAreqId(applyInfoView.getApplyNo())
													.setAreqType(AreqCtgCd.value(areqEntity.getAreqCtgCd()));

				resource.setCheckoutResources		(new Integer(applyInfoView.getLendAssetCount()))
						.setChangedResources		(new Integer(applyInfoView.getChangeAssetCount()))
						.setDeletedAsciiResources	(new Integer(applyInfoView.getDeleteAssetCount()))
						.setAddedResources			(new Integer(applyInfoView.getAddAssetCount()));

				resourceList.add(resource);
			}

			changeProperty.setResources(resourceList);
			changePropertyList.add(changeProperty);
		}

		dest.getDetailsView().setChangeProperties(changePropertyList);
	}

	private void setProcessResults( FlowRelUnitHistoryDetailViewServiceBean src,
									FlowBuildPackageDetailsServiceBean dest) {
		List<ProcessingResultView> processResultList = new ArrayList<ProcessingResultView>();

		for (UnitResultViewBean unitResult : src.getUnitResultViewBeanList()) {
			ProcessingResultView processResult = dest.new ProcessingResultView()
													 .setSequenceNo	( unitResult.getProcessOrder() )
													 .setProcessNm	( unitResult.getProcessName() )
													 .setResult		( unitResult.getProcessResult() ? ProcessStatus.Completed : ProcessStatus.Error );
			processResultList.add(processResult);
		}

		dest.getDetailsView().setProcessResults(processResultList);
	}
}
