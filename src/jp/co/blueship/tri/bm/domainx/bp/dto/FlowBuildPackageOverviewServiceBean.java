package jp.co.blueship.tri.bm.domainx.bp.dto;

import jp.co.blueship.tri.bm.constants.BuildType;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowBuildPackageOverviewServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private BuildPackageOverview detailsView = new BuildPackageOverview();

	public RequestParam getParam() {
		return param;
	}

	public BuildPackageOverview getDetailsView() {
		return detailsView;
	}
	public FlowBuildPackageOverviewServiceBean setDetailsView(BuildPackageOverview detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String bpId = null;

		public String getSelectedBpId() {
			return bpId;
		}
		public RequestParam setSelectedBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}
	}

	public class BuildPackageOverview {
		private String subject;
		private String stsId;
		private String status;
		private String createdBy;
		private String startTime;
		private String endTime;
		private BuildType buildType = BuildType.none;

		public String getSubject() {
			return subject;
		}
		public BuildPackageOverview setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getStsId() {
			return stsId;
		}
		public BuildPackageOverview setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}

		public String getStatus() {
			return status;
		}
		public BuildPackageOverview setStatus(String status) {
			this.status = status;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public BuildPackageOverview setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public BuildPackageOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public BuildPackageOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public boolean isBuildDiff() {
			return BuildType.Build.equals( this.buildType );
		}
		public BuildPackageOverview setBuildDiff( boolean isDiff ) {
			if ( isDiff ) {
				this.buildType = BuildType.Build;
			} else {
				setBuildFull( true );
			}
			return this;
		}
		public boolean isBuildFull() {
			return BuildType.FullBuild.equals( this.buildType );
		}
		public BuildPackageOverview setBuildFull( boolean isFull ) {
			if ( isFull ) {
				this.buildType = BuildType.FullBuild;
			} else {
				setBuildDiff( true );
			}
			return this;
		}
	}

}
