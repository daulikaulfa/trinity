package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.BuildPackageCreationInputInfo;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageCreationServiceBean.ChangePropertyView;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
  *
  * @version V4.00.00
  * @author Hai Thach
  */
public class FlowBuildPackageCreationService implements IDomain<FlowBuildPackageCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	private IDomain<IGeneralServiceBean> inputInfoService = null;
	private IDomain<IGeneralServiceBean> pjtSelectService = null;
	private IDomain<IGeneralServiceBean> confirmService = null;
	private IDomain<IGeneralServiceBean> createService = null;
	private ActionStatusMatrixList statusMatrixAction = null;

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	public void setInputInfoService(IDomain<IGeneralServiceBean> inputInfoService) {
		this.inputInfoService = inputInfoService;
	}

	public void setPjtSelectService(IDomain<IGeneralServiceBean> pjtSelectService) {
		this.pjtSelectService = pjtSelectService;
	}

	public void setConfirmService(IDomain<IGeneralServiceBean> confirmService) {
		this.confirmService = confirmService;
	}

	public void setCreateService(IDomain<IGeneralServiceBean> createService) {
		this.createService = createService;
	}

	@Override
	public IServiceDto<FlowBuildPackageCreationServiceBean> execute(
			IServiceDto<FlowBuildPackageCreationServiceBean> serviceDto) {

		FlowBuildPackageCreationServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelUnitEntryServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType()) &&
					paramBean.getParam().getInputInfo().isBuildDiff()) {
				this.init(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

		return serviceDto;
	}

	private void init(FlowBuildPackageCreationServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer( RelUnitScreenID.LOT_SELECT);
			serviceBean.setForward( RelUnitScreenID.PJT_SELECT);
			pjtSelectService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void submitChanges(FlowBuildPackageCreationServiceBean paramBean) {

		this.validationOnSubmitChanges( paramBean );

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitEntryServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		BuildPackageCreationInputInfo inputInfo = paramBean.getParam().getInputInfo();
		this.beforeExecution(paramBean, serviceBean);

		{
			serviceBean.setReferer( RelUnitScreenID.UNIT_INFO_INPUT );
			serviceBean.setForward( RelUnitScreenID.LOT_SELECT );
			inputInfoService.execute(dto);
		}

		if (inputInfo.isBuildDiff()) {
			serviceBean.setReferer( RelUnitScreenID.PJT_SELECT );
			serviceBean.setForward( RelUnitScreenID.UNIT_COMFIRM );
			pjtSelectService.execute(dto);
			confirmService.execute(dto);

		} else {
			serviceBean.setReferer( RelUnitScreenID.LOT_SELECT );
			serviceBean.setForward( RelUnitScreenID.UNIT_COMFIRM );
			confirmService.execute(dto);
		}

		{
			serviceBean.setReferer( RelUnitScreenID.UNIT_COMFIRM);
			serviceBean.setForward( RelUnitScreenID.COMP_REQUEST );
			confirmService.execute(dto);
			createService.execute(dto);
		}

		this.afterExecution(serviceBean, paramBean);
	}

	private void validationOnSubmitChanges( FlowBuildPackageCreationServiceBean paramBean ){

		String lotId = paramBean.getParam().getSelectedLotId();
		String[] pjtIds = paramBean.getParam().getInputInfo().getSelectedPjtIds();
		String[] areqIds = this.getAreqIdArray( pjtIds ) ;

		StatusCheckDto statusDto = new StatusCheckDto()
				.setServiceBean( paramBean )
				.setFinder( support )
				.setActionList( statusMatrixAction )
				.setLotIds( lotId )
				.setPjtIds( pjtIds )
				.setAreqIds( areqIds )
				;

		StatusMatrixCheckUtils.checkStatusMatrix( statusDto );


		if( 0 == paramBean.getChangePropertyViews().size()
				&& paramBean.getParam().getInputInfo().isBuildDiff() ){

			throw new BusinessException( BmMessageId.BM001002E );
		}
	}

	private void beforeExecution (FlowBuildPackageCreationServiceBean src, FlowRelUnitEntryServiceBean dest) {
		BuildPackageCreationInputInfo srcInfo = src.getParam().getInputInfo();
		UnitInfoBean unitInfoBean = dest.getUnitInfoBean();
		UnitInfoInputBean unitInfoInputBean = null;

		dest.setSelectedLotNo( src.getParam().getSelectedLotId() );

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {

			dest.setProcId( src.getProcId() );
			dest.setScreenType(ScreenType.next);
			dest.setLanguage( src.getLanguage() );
			dest.setFlowAction( src.getFlowAction() );
			dest.setUserId( src.getUserId() );
			dest.setProcMgtStatusId( src.getProcMgtStatusId()) ;
			dest.setProcId( src.getProcId() );

			unitInfoInputBean = unitInfoBean.getUnitInfoInputBean();
			unitInfoInputBean.setUnitName( srcInfo.getSubject() );
			unitInfoInputBean.setUnitSummary( srcInfo.getSummary() );
			unitInfoInputBean.setCtgId( srcInfo.getCtgId() );
			unitInfoInputBean.setMstoneId( srcInfo.getMstoneId() );

			if (srcInfo.isBuildFull()) {
				dest.setAllAssetCompile(StatusFlg.on.value());
			}

			if ( srcInfo.isBuildDiff() ) {
				dest.setSelectedPjtNo( srcInfo.getSelectedPjtIds() );
				dest.setAllAssetCompile(StatusFlg.off.value());
			}
		}

	}

	private void afterExecution (FlowRelUnitEntryServiceBean src, FlowBuildPackageCreationServiceBean dest) {

		if ( RequestType.init.equals(dest.getParam().getRequestType())) {
			this.generateLists(dest);

			if (dest.getParam().getInputInfo().isBuildDiff()) {
				this.setPjtList(src, dest);
			} else if (dest.getParam().getInputInfo().isBuildFull()) {
				dest.setChangePropertyViews( new ArrayList<ChangePropertyView>() );
			}

		}

		if ( RequestType.submitChanges.equals(dest.getParam().getRequestType())) {
			dest.setLockByThread(src.isLockByThread());
			dest.getResult().setBpId( src.getBuildNo());
			dest.getResult().setCompleted(true);
			dest.getMessageInfo().addFlashTranslatable( BmMessageId.BM003010I );
		}
	}

	private void generateLists(FlowBuildPackageCreationServiceBean paramBean) {

		BuildPackageCreationInputInfo inputInfo = paramBean.getParam().getInputInfo();
		String lotId = paramBean.getParam().getSelectedLotId();

		List<ICtgEntity> ctgEntityList = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		inputInfo.setCtgViews(FluentList.from( ctgEntityList ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList());

		List<IMstoneEntity> mstoneEntityList = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		inputInfo.setMstoneViews(FluentList.from( mstoneEntityList).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList());

	}

	private void setPjtList(FlowRelUnitEntryServiceBean src, FlowBuildPackageCreationServiceBean dest) {
		List<PjtViewBean> pjtViewList = src.getPjtSelectBean().getPjtViewBeanList();
		List<ChangePropertyView> views = new ArrayList<ChangePropertyView>();
		PjtCondition condition = new PjtCondition();
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat(dest.getLanguage(), dest.getTimeZone());

		for (PjtViewBean pjtView : pjtViewList) {
			condition.setPjtId( pjtView.getPjtNo() );
			IPjtEntity pjtEntity = this.support.getAmFinderSupport().getPjtDao().findByPrimaryKey(condition.getCondition());

			ChangePropertyView view = dest.new ChangePropertyView()
						.setPjtId( pjtEntity.getPjtId() )
						.setReferenceId( pjtEntity.getChgFactorNo() )
						.setSummary( pjtEntity.getSummary() )
						.setSubmitterNm( pjtEntity.getRegUserNm() )
						.setSubmitterIconPath( this.support.getUmFinderSupport().getIconPath(pjtEntity.getRegUserId()))
						.setApprovedTime(
								TriDateUtils.convertViewDateFormat(pjtEntity.getPjtAvlTimestamp(), formatYMDHM).toString() )
						;
			views.add(view);
		}

		dest.setChangePropertyViews(views);
	}

	/**
	 * 変更管理番号に紐付く申請情報の申請番号を取得する。
	 * @param pjtIdArray 変更管理番号の配列
	 * @return 申請番号の配列
	 */
	private String[] getAreqIdArray( String[] pjtIdArray ) {

		if ( TriStringUtils.isEmpty( pjtIdArray ) ) {
			//変更管理が１件も選択されていなければ、紐付く申請情報を取得しない。
			return new String[0];
		}

		AreqCondition condition = (AreqCondition)DBSearchConditionAddonUtil.getAreqConditionByGeneratableRelUnitByPjtNo( pjtIdArray ) ;
		ISqlSort iSort = new SortBuilder() ;

		IEntityLimit<IAreqEntity> areqEntityLimit = this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition() , iSort , 1 , 0 ) ;
		IAreqEntity[] areqEntityArray = areqEntityLimit.getEntities().toArray(new IAreqEntity[0]) ;

		List<String> areqIdList = new ArrayList<String>() ;

		for( IAreqEntity areqEntity : areqEntityArray ) {
			areqIdList.add( areqEntity.getAreqId() ) ;
		}

		return areqIdList.toArray( new String[ 0 ] ) ;
	}

}
