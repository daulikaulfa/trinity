package jp.co.blueship.tri.bm.domainx.bp;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.BuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceExportToFileBean;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;

public class FlowBuildPackageListServiceExportToFile implements IDomain<FlowBuildPackageListServiceExportToFileBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> buildPackageList = null;

	public void setBuildPackageList(IDomain<IGeneralServiceBean> buildPackageList) {
		this.buildPackageList = buildPackageList;
	}

	@Override
	public IServiceDto<FlowBuildPackageListServiceExportToFileBean> execute(
			IServiceDto<FlowBuildPackageListServiceExportToFileBean> serviceDto) {
		FlowBuildPackageListServiceExportToFileBean paramBean = serviceDto.getServiceBean();
		FlowBuildPackageListServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if(RequestType.onChange.equals(paramBean.getParam().getRequestType())){
				this.onChange(paramBean);
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);
		}

	}

	private void submitChanges(FlowBuildPackageListServiceExportToFileBean paramBean) {
		File file = null;
		ExportToFileSubmitOption submitOption = paramBean.getParam().getSubmitOption();

		if (ExportToFileSubmitOption.ExportToExcel.equals(submitOption)) {
			file = DcmExportToFileUtils.createExcel(paramBean, paramBean.getParam().getExportBean());
		} else if (ExportToFileSubmitOption.ExportToCsv.equals(submitOption)) {
			file = DcmExportToFileUtils.createCSV(paramBean, paramBean.getParam().getExportBean());
		}

		paramBean.getFileResponse().setFile(file);
	}

	private void onChange(FlowBuildPackageListServiceExportToFileBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowBuildPackageListServiceBean serviceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(serviceBean, paramBean);
		dto.setServiceBean(serviceBean);

		{
			serviceBean.getParam().setRequestType(RequestType.init);
			serviceBean.getParam().setSelectedLotId(paramBean.getParam().getLotId());
			buildPackageList.execute(dto);
		}

		serviceBean.getParam().setRequestType(RequestType.onChange);
		this.beforeExecution(paramBean, serviceBean);
		buildPackageList.execute(dto);
		this.afterExecution(serviceBean, paramBean);

	}

	private void beforeExecution(FlowBuildPackageListServiceExportToFileBean src, FlowBuildPackageListServiceBean dest) {
		SearchCondition srcSearchConditon = src.getParam().getSearchCondition();
		SearchCondition destSearchCondition = dest.getParam().getSearchCondition();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			dest.getParam().setRequestType(RequestType.onChange);
//			dest.getParam().setSearchServiceType(SearchServiceType.SearchSite);

			destSearchCondition.setStsId(srcSearchConditon.getStsId());
			destSearchCondition.setCtgId(srcSearchConditon.getCtgId());
			destSearchCondition.setMstoneId(srcSearchConditon.getMstoneId());
			destSearchCondition.setKeyword(srcSearchConditon.getKeyword());
			destSearchCondition.setSelectedPageNo(1);
			dest.getParam().setLinesPerPage(0);
		}
	}

	private void afterExecution(FlowBuildPackageListServiceBean src, FlowBuildPackageListServiceExportToFileBean dest) {
		List<BuildPackageView> srcSearchSiteViews = src.getBuildPackageViews();
		List<BuildPackageView> destSearchSiteViews = new ArrayList<BuildPackageView>();

		if (RequestType.onChange.equals(src.getParam().getRequestType())) {
			for (int i = 0; i < srcSearchSiteViews.size(); i++) {
				BuildPackageView view = srcSearchSiteViews.get(i);
				destSearchSiteViews.add(view);
			}
			dest.setBuildPackageViews(destSearchSiteViews);
		}
	}
}
