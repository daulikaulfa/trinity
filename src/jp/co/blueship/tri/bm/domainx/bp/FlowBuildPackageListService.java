package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmLibraryAddonUtils;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmFluentFunctionUtils;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.BuildPackageView;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.OrderBy;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.RequestParam;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageListServiceBean.SearchCondition;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageListService implements IDomain<FlowBuildPackageListServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support = null;
	public void setSupport (FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowBuildPackageListServiceBean> execute(
			IServiceDto<FlowBuildPackageListServiceBean> serviceDto) {

		FlowBuildPackageListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {
				this.onChange(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		}
	}

	/**
	 * Call when initializing by service
	 *
	 * @param serviceBean
	 */
	private void init ( FlowBuildPackageListServiceBean serviceBean ) {
		String lotId = serviceBean.getParam().getSelectedLotId();
		PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "ServiceBean is not specified");

		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId, AmTables.AM_LOT_GRP_LNK);
		AmLibraryAddonUtils.checkAccessableGroup(lotDto,
					this.support.getUmFinderSupport().getGrpDao(),
					this.support.getUmFinderSupport().findGroupByUserId(serviceBean.getUserId()));

		SearchCondition searchCondition = serviceBean.getParam().getSearchCondition();
		this.generateDropBox (lotId, searchCondition);

		this.onChange(serviceBean);
	}

	/**
	 * @param lotId Target lot-ID
	 * @param searchCondition Set the generated search conditions
	 */
	private void generateDropBox( String lotId, SearchCondition searchCondition) {
		List<IStatusId> bpStatuses = new ArrayList<IStatusId>();
		bpStatuses.add(BmBpStatusIdForExecData.CreatingBuildPackage);
		bpStatuses.add(BmBpStatusId.BuildPackageCreated);
		bpStatuses.add(BmBpStatusIdForExecData.BuildPackageError);
		bpStatuses.add(BmBpStatusId.BuildPackageRemoved);
		bpStatuses.add(BmBpStatusIdForExecData.BuildPackageClosing);
		bpStatuses.add(BmBpStatusId.BuildPackageClosed);
		bpStatuses.add(BmBpStatusIdForExecData.BuildPackageCloseError);

		searchCondition.setStatusViews(FluentList.from( bpStatuses ).map( BmFluentFunctionUtils.toItemLabelsFromStatusId).asList() );

		List<ICtgEntity> ctgEntities = this.support.getUmFinderSupport().findCtgByLotId(lotId);
		searchCondition.setCtgViews(FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntity).asList() );

		List<IMstoneEntity> mstoneEntities = this.support.getUmFinderSupport().findMstoneByLotId(lotId);
		searchCondition.setMstoneViews(FluentList.from( mstoneEntities ).map( UmFluentFunctionUtils.toItemLabelsFromMstoneEntity).asList() );

	}

	/**
	 * Call when request by service
	 *
	 * @param serviceBean
	 */
	private void onChange ( FlowBuildPackageListServiceBean serviceBean ) {
		ISqlSort sort = getSortOrder( serviceBean.getParam() );

		int linesPerPage = serviceBean.getParam().getLinesPerPage();

		int pageNo = serviceBean.getParam().getSearchCondition().getSelectedPageNo();

		IEntityLimit<IBpEntity> limit = support.getBpDao().find(
				this.getCondition(serviceBean).getCondition(),
				sort,
				pageNo,
				linesPerPage);

		IPageNoInfo page = BmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		serviceBean.setPage(page);
		serviceBean.setBuildPackageViews( getBpViewBean(serviceBean, limit.getEntities()) );
	}

	/**
	 * @param param
	 */
	private ISqlSort getSortOrder( RequestParam param ) {
		ISqlSort sort = new SortBuilder();

		if( null == param.getOrderBy() ) {
			return sort;
		}

		OrderBy orderBy = param.getOrderBy();

		if ( null != orderBy.getBpId() ) {
			sort.setElement(BpItems.bpId, orderBy.getBpId(), sort.getLatestSeq() + 1);
		}

		return sort;
	}

	/**
	 * @param serviceBean
	 * @return
	 */
	private BpCondition getCondition(FlowBuildPackageListServiceBean serviceBean) {

		RequestParam param = serviceBean.getParam();
		String lotId = serviceBean.getParam().getSelectedLotId();

		SearchCondition searchParam = param.getSearchCondition();
		BpCondition condition = new BpCondition();

		condition.setLotId(lotId);

		if ( TriStringUtils.isNotEmpty(searchParam.getStsId()) ) {
			condition.setProcStsIds( new String[]{searchParam.getStsId()} );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getCtgId()) ) {
			condition.setCtgId(searchParam.getCtgId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getMstoneId()) ) {
			condition.setMstoneId(searchParam.getMstoneId() );
		}

		if ( TriStringUtils.isNotEmpty(searchParam.getKeyword()) ) {
			condition.setContainsByKeyword(searchParam.getKeyword());
		}

		return condition;
	}

	/**
	 * @param serviceBean ServiceBean
	 * @param bpList Build Package Entities
	 * @return
	 */
	private List<BuildPackageView> getBpViewBean(
										FlowBuildPackageListServiceBean serviceBean,
										List<IBpEntity> bpList) {
		List<BuildPackageView> viewBeanList = new ArrayList<BuildPackageView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDHMDateFormat( serviceBean.getLanguage(), serviceBean.getTimeZone() );

		for (IBpEntity entity : bpList) {
			BuildPackageView view = serviceBean.new BuildPackageView()
					.setBpId( entity.getBpId() )
					.setSubject( entity.getBpNm() )
					.setCreatedBy( entity.getRegUserNm() )
					.setCreatedByIconPath( this.support.getUmFinderSupport().getIconPath(entity.getRegUserId()) )
					.setCreatedDate( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMD) )
					.setStsId( entity.getProcStsId() )
					.setStatus( sheet.getValue(BmDesignBeanId.statusId, entity.getProcStsId()) )
					.setSuccess( BmBusinessJudgUtils.isSuccess(entity) )
					.setStartTime( TriDateUtils.convertViewDateFormat(entity.getProcStTimestamp(), formatYMD) )
					.setEndTime( TriDateUtils.convertViewDateFormat(entity.getProcEndTimestamp(), formatYMD) )
					;

			viewBeanList.add(view);
		}
		return viewBeanList;
	}
}

