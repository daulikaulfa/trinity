package jp.co.blueship.tri.bm.domainx.bp;

import java.text.SimpleDateFormat;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageOverviewServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageOverviewServiceBean.BuildPackageOverview;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;


/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowBuildPackageOverviewService implements IDomain<FlowBuildPackageOverviewServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;


	public void setSupport (FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowBuildPackageOverviewServiceBean> execute(
			IServiceDto<FlowBuildPackageOverviewServiceBean> serviceDto) {

		FlowBuildPackageOverviewServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf( serviceBean != null, "ServiceBean is not specified");

			String bpId = serviceBean.getParam().getSelectedBpId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty(bpId), "SelectedBpId is not specified" );

			if ( RequestType.init.equals( serviceBean.getParam().getRequestType()) ) {
				this.init(serviceBean);
			}
			if ( RequestType.onChange.equals( serviceBean.getParam().getRequestType()) ) {
				this.init(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, serviceBean.getFlowAction());
		}
	}

	private void init (FlowBuildPackageOverviewServiceBean serviceBean) {

		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormat(serviceBean.getLanguage(), serviceBean.getTimeZone());

		String bpId = serviceBean.getParam().getSelectedBpId();
		IBpEntity bpEntity = this.support.findBpEntity(bpId);
		ILotEntity lotEntity = this.support.getAmFinderSupport().findLotDto( bpEntity.getLotId() ).getLotEntity();


		BuildPackageOverview view = serviceBean.new BuildPackageOverview()
				.setStsId		( bpEntity.getProcStsId() )
				.setStatus		( sheet.getValue(BmDesignBeanId.statusId, bpEntity.getProcStsId()) )
				.setSubject		( bpEntity.getBpNm() )
				.setCreatedBy	( bpEntity.getRegUserNm() )
				.setStartTime	( TriDateUtils.convertViewDateFormat( bpEntity.getProcStTimestamp(),format) )
				.setEndTime		( TriDateUtils.convertViewDateFormat( bpEntity.getProcEndTimestamp(), format) )
				.setBuildDiff	( TriStringUtils.isEquals(bpEntity.getBldEnvId(), lotEntity.getBldEnvId()) ? true : false )
				;

		serviceBean.setDetailsView(view);
	}
}
