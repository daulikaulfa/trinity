package jp.co.blueship.tri.bm.domainx.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelServiceBean;
import jp.co.blueship.tri.bm.domainx.bp.dto.FlowBuildPackageRemovalServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpBpLnkEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpBpLnkCondition;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowBuildPackageRemovalService implements IDomain<FlowBuildPackageRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDomain<IGeneralServiceBean> confirmService = null;
	private IDomain<IGeneralServiceBean> completeService = null;
	private IDomain<IGeneralServiceBean> mailService = null;
	private FlowRelUnitEditSupport support = null;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public void setConfirmService(IDomain<IGeneralServiceBean> confirmService) {
		this.confirmService = confirmService;
	}

	public void setCompleteService(IDomain<IGeneralServiceBean> completeService) {
		this.completeService = completeService;
	}

	public void setMailService(IDomain<IGeneralServiceBean> mailService) {
		this.mailService = mailService;
	}

	@Override
	public IServiceDto<FlowBuildPackageRemovalServiceBean> execute(
			IServiceDto<FlowBuildPackageRemovalServiceBean> serviceDto) {

		FlowBuildPackageRemovalServiceBean paramBean = serviceDto.getServiceBean();
		FlowRelUnitCancelServiceBean innerServiceBean = paramBean.getInnerService();

		try {
			PreConditions.assertOf( paramBean != null, "ServiceBean is not specified" );

			String bpId = paramBean.getParam().getSelectedBpId();
			PreConditions.assertOf( TriStringUtils.isNotEmpty(bpId), "Seleceted BpId is not specified");

			if ( RequestType.validate.equals(paramBean.getParam().getRequestType())) {
				this.validate(paramBean);
			}

			if ( RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, paramBean.getFlowAction());
		} finally {
			ExceptionUtils.copyToParent(paramBean, innerServiceBean);

		}

		return serviceDto;
	}

	private void validate(FlowBuildPackageRemovalServiceBean paramBean) {
		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitCancelServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		String bpId = paramBean.getParam().getSelectedBpId();
		IBpEntity bpEntity = this.support.findBpEntity(bpId);

		// Status Matrix Check
		{
			String[] bpIds = {bpId};
			IRaEntity[] raEntities	= this.support.getRelApplyEntityByBuildNo( bpIds ).getEntities().toArray(new IRaEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( paramBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( bpEntity.getLotId() )
					.setBpIds		( bpIds )
					.setRaIds		( this.support.getRelApplyNo(raEntities) )
					.setRpIds		( this.getRpIds(bpId) )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		this.beforeExecution(paramBean, innerServiceBean);

		{
			innerServiceBean.setReferer(RelUnitScreenID.UNIT_CANCEL_LIST);
			innerServiceBean.setForward(RelUnitScreenID.UNIT_CANCEL_COMFIRM);
			confirmService.execute(dto);
		}
	}

	private void submitChanges(FlowBuildPackageRemovalServiceBean paramBean) {
		String bpId = paramBean.getParam().getSelectedBpId();
		IBpEntity bpEntity = this.support.findBpEntity(bpId);

		{
			String[] bpIds = { bpId };
			IRaEntity[] raEntities	= this.support.getRelApplyEntityByBuildNo( bpIds ).getEntities().toArray(new IRaEntity[0]);

			StatusCheckDto statusDto = new StatusCheckDto()
					.setServiceBean	( paramBean )
					.setFinder		( support )
					.setActionList	( statusMatrixAction )
					.setLotIds		( bpEntity.getLotId() )
					.setBpIds		( bpIds )
					.setRaIds		( this.support.getRelApplyNo(raEntities) )
					.setRpIds		( this.getRpIds(bpId) )
					;

			StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
		}

		IServiceDto<IGeneralServiceBean> dto = new ServiceDto<IGeneralServiceBean>();
		FlowRelUnitCancelServiceBean innerServiceBean = paramBean.getInnerService();
		TriPropertyUtils.copyProperties(innerServiceBean, paramBean);
		dto.setServiceBean(innerServiceBean);

		this.beforeExecution(paramBean, innerServiceBean);

		{
			innerServiceBean.setReferer(RelUnitScreenID.UNIT_CANCEL_COMFIRM);
			innerServiceBean.setForward(RelUnitScreenID.COMP_UNIT_CANCEL);
			confirmService.execute(dto);
			completeService.execute(dto);
			mailService.execute(dto);
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(BmMessageId.BM003011I);
	}

	private void beforeExecution(FlowBuildPackageRemovalServiceBean src,
								 FlowRelUnitCancelServiceBean dest) {
		src.getResult().setCompleted(false);

		dest.setUserId( src.getUserId() );
		dest.setUserName( src.getUserName() );
		dest.setFlowAction( src.getFlowAction() );
		dest.setLanguage( src.getLanguage() );

		String[] selectedUnitNo = { src.getParam().getSelectedBpId() };
		dest.setSelectedUnitNo(selectedUnitNo);

		if (RequestType.submitChanges.equals(src.getParam().getRequestType())) {

			UnitInfoInputBean unitInfoInputBean = new UnitInfoInputBean();
			unitInfoInputBean.setCancelComment("");
			dest.setUnitInfoInputBean(unitInfoInputBean);

		}
	}

	private String[] getRpIds( String bpId ){

		RpBpLnkCondition condition = new RpBpLnkCondition();
		condition.setBpId( bpId );

		List<IRpBpLnkEntity> rpBpLnkEntityList = this.support.getRmFinderSupport().getRpBpLnkDao().find( condition.getCondition() );
		List<String> rpIdList = new ArrayList<String>();

		for( IRpBpLnkEntity entity :  rpBpLnkEntityList ){
			rpIdList.add(entity.getRpId());
		}

		return rpIdList.toArray(new String[]{});
	}
}

