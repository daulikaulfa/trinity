package jp.co.blueship.tri.bm.domainx.bp.dto;

import jp.co.blueship.tri.bm.task.TaskDetailsViewBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowBuildPackageCreationProcessingStatusServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private BuildPackageCreationProcessingStatusOverview overview = new BuildPackageCreationProcessingStatusOverview();
	private TaskDetailsViewBean detailsView = new TaskDetailsViewBean();

	public RequestParam getParam() {
		return param;
	}

	public BuildPackageCreationProcessingStatusOverview getOverview() {
		return overview;
	}
	public FlowBuildPackageCreationProcessingStatusServiceBean setOverview(BuildPackageCreationProcessingStatusOverview overview) {
		this.overview = overview;
		return this;
	}

	public TaskDetailsViewBean getDetailsView() {
		return detailsView;
	}
	public FlowBuildPackageCreationProcessingStatusServiceBean setDetailsView(TaskDetailsViewBean detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		refresh("refresh"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class BuildPackageCreationProcessingStatusOverview {
		private String bpId;
		private String createdBy;
		private String createdByIconPath;
		private String startTime;
		private String endTime;
		private ProcessStatus result = ProcessStatus.none;
		private IProgressBar bar;
		private String remainingTime;

		public String getBpId() {
			return bpId;
		}
		public BuildPackageCreationProcessingStatusOverview setBpId(String bpId) {
			this.bpId = bpId;
			return this;
		}

		public String getCreatedBy() {
			return createdBy;
		}
		public BuildPackageCreationProcessingStatusOverview setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByIconPath() {
			return createdByIconPath;
		}
		public BuildPackageCreationProcessingStatusOverview setCreatedByIconPath(String createdByIconPath) {
			this.createdByIconPath = createdByIconPath;
			return this;
		}

		public String getStartTime() {
			return startTime;
		}
		public BuildPackageCreationProcessingStatusOverview setStartTime(String startTime) {
			this.startTime = startTime;
			return this;
		}

		public String getEndTime() {
			return endTime;
		}
		public BuildPackageCreationProcessingStatusOverview setEndTime(String endTime) {
			this.endTime = endTime;
			return this;
		}

		public boolean isActive() {
			return ProcessStatus.Active.equals( result );
		}
		public boolean isCompleted() {
			return ProcessStatus.Completed.equals( result );
		}
		public boolean isError() {
			return ProcessStatus.Error.equals( result );
		}
		public BuildPackageCreationProcessingStatusOverview setResult(ProcessStatus result) {
			this.result = result;
			return this;
		}

		public IProgressBar getBar() {
			return bar;
		}
		public BuildPackageCreationProcessingStatusOverview setBar(IProgressBar bar) {
			this.bar = bar;
			return this;
		}

		public String getRemainingTime() {
			return remainingTime;
		}
		public BuildPackageCreationProcessingStatusOverview setRemainingTime(String remainingTime) {
			this.remainingTime = remainingTime;
			return this;
		}
	}

}
