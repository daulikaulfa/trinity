package jp.co.blueship.tri.bm.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.bm.beans.dto.NumberingRelUnitServiceBean;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.beans.NumberingServiceSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.INumberingCallback;
import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;


/**
 * ビルドパッケージのアクション実行時、採番を行う汎用クラス。
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class NumberingRelUnitGeneralService extends NumberingServiceSupport<IGeneralServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IBmFinderSupport support = null;

	public void setSupport( IBmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> prmBLBean ) {


		try {
			NumberingRelUnitServiceBean numberingServiceBean = this.getNumberingServiceBean( prmBLBean.getServiceBean() );

			this.numbering(
					numberingServiceBean,
					new INumberingCallback() {

						public Map<String, Object> getNumbering( NumberingServiceBean bean, Map<String, Object> data ) {
							return getNumberingCallBack( bean, data );
						}
					});

			TriPropertyUtils.copyProperties( prmBLBean.getServiceBean(), numberingServiceBean );

			return prmBLBean;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005005S, e );
		}

	}

	/**
	 * 採番を取得します。
	 *
	 * @param bean 採番サービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 */
	private final Map<String, Object> getNumberingCallBack( NumberingServiceBean bean, Map<String, Object> data ) {
		return data;
	}

	/**
	 * 採番取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @return 生成したインスタンス
	 */
	private NumberingRelUnitServiceBean getNumberingServiceBean( Object paramBean ) {
		NumberingRelUnitServiceBean destBean = new NumberingRelUnitServiceBean();

		TriPropertyUtils.copyProperties(destBean, paramBean);

		if ( ! TriStringUtils.isEmpty( destBean.getSelectedUnitNo() ) ) {
			List<IEntity> entityList = new ArrayList<IEntity>();

			for ( String bpId : destBean.getSelectedUnitNo() ) {
				entityList.add( this.support.findBpEntity( bpId ) );
			}

			destBean.setEntities( entityList );
		}

		return destBean;
	}

}
