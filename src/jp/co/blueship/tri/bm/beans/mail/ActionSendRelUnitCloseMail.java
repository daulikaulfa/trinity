package jp.co.blueship.tri.bm.beans.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.beans.mail.dto.RelUnitMailServiceBean;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;


/**
 * ビルドパッケージ作成・ビルドパッケージクローズ受付完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionSendRelUnitCloseMail implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelUnitEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute( IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto ) {

		FlowRelUnitCloseRequestServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( null != paramBean.getBusinessThrowable() ) {
				//現時点では、業務エラーメールは存在しないため、システムエラーに変換する
				if ( null == paramBean.getSystemThrowable() ) {
					paramBean.setSystemThrowable( (TriRuntimeException)paramBean.getBusinessThrowable() );
				}

				paramBean.setBusinessThrowable( null );
			}

			boolean sendMail = false;

			//基礎情報のコピー
			RelUnitMailServiceBean successMailBean = new RelUnitMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, paramBean );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

			//ビルド番号
			String[] buildNos = paramBean.getSelectedUnitNo();

			//ロットエンティティ
			String lotId =  this.support.findBpEntity(buildNos[0]).getLotId();
			ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity(lotId);

			//変更申請者宛メール送信用の申請番号リスト
			List<String> applyNos = new ArrayList<String>();

			//テンプレート用リスト作成
			List<Map<String,Object>> packageData = new ArrayList<Map<String,Object>>();
			for(String buildNo : buildNos){
//				IBpEntity bpEntity = this.support.getBpEntity(buildNo);
				IBpDto bpDto = this.support.findBpDto(buildNo);
				List<IBpAreqLnkEntity> bpAreqEntities = bpDto.getBpAreqLnkEntities();

				if ( BmBpStatusIdForExecData.BuildPackageCloseError.equals(bpDto.getBpEntity().getProcStsId())
					|| BmBpStatusIdForExecData.BuildPackageClosing.equals(bpDto.getBpEntity().getProcStsId())
					|| BmBpStatusId.BuildPackageClosed.equals(bpDto.getBpEntity().getProcStsId()) ) {
					sendMail = true;
				}

				//枠
 				List<Map<String,Object>> pjtData = new ArrayList<Map<String,Object>>();
				Map<String,List<String>> pjtApplyMap = new HashMap<String,List<String>>();
				//重複確認用
				Map<String,String> dupChkMap = new HashMap<String,String>();

				for ( IBpAreqLnkEntity bpAreqEntity : bpAreqEntities ) {

					String pjtNo			= this.support.getAmFinderSupport().findAreqEntity(bpAreqEntity.getAreqId()).getPjtId();
					String changeCauseNo 	= this.support.getAmFinderSupport().findPjtEntity( pjtNo ).getChgFactorNo();
					String applyNo			= bpAreqEntity.getAreqId();

					//変更申請者宛メール送信用の申請番号リストに申請番号を詰める
					applyNos.add( applyNo );

					//pjtApplyMap作成
					if ( !pjtApplyMap.containsKey( pjtNo )) {
						List<String> list = new ArrayList<String>();
						pjtApplyMap.put(pjtNo, list);
					}
					pjtApplyMap.get( pjtNo ).add( applyNo );

					//
					if( !dupChkMap.containsKey(pjtNo) ){
						dupChkMap.put(pjtNo, "");

						Map<String,Object> pjtMap = new HashMap<String,Object>();
						pjtMap.put("pjtId", pjtNo);
						pjtMap.put("chgFactorNo", changeCauseNo);
						pjtMap.put("applyList", pjtApplyMap.get(pjtNo));
						pjtData.add(pjtMap);
					}
				}

				Map<String,Object> packageMap = new HashMap<String,Object>();
				packageMap.put("bpId", buildNo);
				packageMap.put("pjtList", pjtData);
				packageData.add(packageMap);

			}

			if ( ! sendMail ) {
				return serviceDto;
			}


			//ビルドパッケージクローズごとにメール送信
			IBpEntity bpEntitiy = this.support.findBpEntity(buildNos[0]);
			bpEntitiy.setContent(paramBean.getUnitInfoInputBean().getCloseComment());
			successMailBean.setBpEntity	( bpEntitiy );
			successMailBean.setLotEntity( pjtLotEntity );
			successMailBean.setPackageList(packageData);
			successMailBean.setAssetApplyEntityList( getAssetApplyEntity( applyNos.toArray( new String[0]) ));
			successMail.execute( mailServiceDto );

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( BmMessageId.BM005068S, e , paramBean.getFlowAction() ) ) ;
		}

		return serviceDto;
	}

	/**
	 * ビルドパッケージに含まれる承認済申請情報を取得する
	 * @param applyNoList
	 * @return
	 */
	private List<IAreqEntity> getAssetApplyEntity( String[] applyNoArray ) {

		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		if ( !TriStringUtils.isEmpty( applyNoArray )) {

			IAreqEntity[] assetApplyEntities = this.support.getAreqEntity( applyNoArray );

			for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

				assetApplyEntityList.add( assetApplyEntity );
			}
		}

		return assetApplyEntityList;
	}

}
