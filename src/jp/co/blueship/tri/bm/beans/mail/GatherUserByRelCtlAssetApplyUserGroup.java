package jp.co.blueship.tri.bm.beans.mail;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriPair;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.beans.mail.dto.RelCtlMailServiceBean;

/**
 * リリースパッケージに含まれる資産申請（返却／削除）を行ったユーザが所属するグループの全ユーザ情報を取得します。 <br>
 * columnに指定可能な項目 <br>
 * ・申請情報の項目名 <br>
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherUserByRelCtlAssetApplyUserGroup implements IDomain<RelCtlMailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao = null;
	private IUmFinderSupport umFinderSupport;
	private List<String> column = null;

	public GatherUserByRelCtlAssetApplyUserGroup() {
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 *
	 * @param column 申請の該当項目名
	 */
	public final void setColumn(List<String> column) {
		this.column = column;
	}

	@Override
	public IServiceDto<RelCtlMailServiceBean> execute(IServiceDto<RelCtlMailServiceBean> serviceDto) {

		final String LABEL_ID = "Id";

		RelCtlMailServiceBean paramBean = null;
		SendMailBean targetBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			targetBean = paramBean.getTarget();
			targetBean.setUsersView(new ArrayList<IUserEntity>());

			List<IAreqEntity> entityList = paramBean.getAssetApplyEntityList();

			for (IAreqEntity entity : entityList) {

				TriPair<String, String> userIdentify = userIdentifyFrom(entity);
				PreConditions.assertOf(userIdentify.isPresent(), "UserID or UserName is not found from AreqEntity.");

				if (identifyName(userIdentify).substring(identifyName(userIdentify).length() - LABEL_ID.length()).equals(LABEL_ID)) {

					this.getUserEntityList(targetBean.getUsersView(), userIdentifyValue(userIdentify));

				} else {

					// ユーザ名からユーザエンティティ取得（同姓同名で複数のケースあり）
					UserCondition condition = new UserCondition();
					condition.setUserNm(userIdentifyValue(userIdentify));
					List<IUserEntity> users = this.userDao.find(condition.getCondition());
					for (IUserEntity userEntity : users) {
						this.getUserEntityList(targetBean.getUsersView(), userEntity.getUserId());
					}
				}

			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005069S, e, "GatherUserByRelCtlAssetApplyUserGroup");
		}

		return serviceDto;
	}

	private String userIdentifyValue(TriPair<String, String> userColumnAndValue) {
		return userColumnAndValue.getWife();
	}

	private String identifyName(TriPair<String, String> userColumnAndValue) {
		return userColumnAndValue.getHasband();
	}

	private TriPair<String, String> userIdentifyFrom(IAreqEntity entity) {

		for (String key : this.column) {
			String columnValue = (String) TriPropertyUtils.getProperty(entity, key);
			if (!TriStringUtils.isEmpty(columnValue)) {
				return new TriPair<String, String>(key, columnValue);
			}
		}

		return new TriPair<String, String>();
	}

	/**
	 * 指定したユーザが所属する全グループについて、それぞれ所属メンバーの ユーザエンティティを取得する。その際、重複は除外する。
	 *
	 * @param list
	 * @param user_id
	 */
	private void getUserEntityList(List<IUserEntity> list, String user_id) {

		for (IGrpEntity group : groupsFrom(user_id)) {
			for (IUserEntity user : usersFrom(group.getGrpId())) {
				// 重複するエンティティは追加しない
				if (!this.isDuplicate(list, user)) {
					list.add(user);
				}
			}
		}
	}

	private List<IUserEntity> usersFrom(String groupId) {
		return umFinderSupport.findUserByGroup(groupId);
	}

	private List<IGrpEntity> groupsFrom(String user_id) {
		return umFinderSupport.findGroupByUserId(user_id);
	}

	private Boolean isDuplicate(List<IUserEntity> list, IUserEntity user) {
		for (IUserEntity entity : list) {
			if (entity.getUserId().equals(user.getUserId())) {
				return true;
			}
		}

		return false;
	}

}
