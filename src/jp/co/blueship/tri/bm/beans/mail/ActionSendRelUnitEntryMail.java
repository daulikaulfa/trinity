package jp.co.blueship.tri.bm.beans.mail;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.beans.mail.dto.RelUnitMailServiceBean;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;


/**
 * ビルドパッケージ作成・ビルドパッケージ作成受付時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionSendRelUnitEntryMail implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private MailGenericService successMail = null;
	private FlowRelUnitEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@SuppressWarnings("unchecked")
	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
			//基礎情報のコピー
			RelUnitMailServiceBean successMailBean = new RelUnitMailServiceBean();
			TriPropertyUtils.copyProperties( successMailBean, paramBean );
			IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);


			String selectedBuildNo = paramBean.getBuildNo();

			ILotEntity lotEntity = this.support.getAmFinderSupport().findLotEntity( paramBean.getSelectedLotNo() );
			IBpDto bpDto = null ;
			try {
				bpDto = this.support.findBpDto( selectedBuildNo );
			} catch ( TriSystemException e ) {
				//ロック待ちなどでビルドレコード生成までたどり着かなかった場合、メールを送信しない
				LogHandler.info( log , TriLogMessage.LBM0010 , selectedBuildNo ) ;
				return serviceDto ;
			}

			// ビルドパッケージ取消に合わせてつくる
			// （取消は複数指定可能）注：現在は機能していない
			// ビルドパッケージ情報
			List<Map<String,Object>> packageData			= new ArrayList<Map<String,Object>>();
			Map<String, String[]> buildNoAssetApplyNoMap	= new HashMap<String, String[]>();
			Map<String, IBpEntity> buildNoEntityMap			= new HashMap<String, IBpEntity>();

			String buildNo = bpDto.getBpEntity().getBpId();
			List<IBpAreqLnkEntity> buildApplyEntities = bpDto.getBpAreqLnkEntities();

			//枠
			List<Map<String,Object>> pjtData = new ArrayList<Map<String,Object>>();
			Map<String,List<String>> pjtApplyMap = new HashMap<String,List<String>>();
			//重複確認用
			Map<String,String> dupChkMap = new HashMap<String,String>();

			for ( IBpAreqLnkEntity buildApplyentity : buildApplyEntities ) {

				String pjtNo			= this.support.getAmFinderSupport().findAreqEntity(buildApplyentity.getAreqId()).getPjtId();
				String changeCauseNo 	= this.support.getAmFinderSupport().findPjtEntity( pjtNo ).getChgFactorNo();
				String applyNo			= buildApplyentity.getAreqId();

				//pjtApplyMap作成
				if ( !pjtApplyMap.containsKey( pjtNo )) {
					List<String> list = new ArrayList<String>();
					pjtApplyMap.put(pjtNo, list);
				}
				pjtApplyMap.get( pjtNo ).add( applyNo );

				//
				if( !dupChkMap.containsKey(pjtNo) ){
					dupChkMap.put(pjtNo, "");

					Map<String,Object> pjtMap = new HashMap<String,Object>();
					pjtMap.put("pjtId", pjtNo);
					pjtMap.put("chgFactorNo", changeCauseNo);
					pjtMap.put("applyList", pjtApplyMap.get(pjtNo));
					pjtData.add(pjtMap);
				}
			}


			Map<String,Object> packageMap = new HashMap<String,Object>();
			packageMap.put("bpId", buildNo);
			packageMap.put("pjtList", pjtData);
			packageData.add(packageMap);

			//エンティティ用につめなおし…
			List<String> applyNos = new ArrayList<String>();
			Map<String,String> dupCheckmap = new HashMap<String,String>();
			for(Map<String,Object> pjtMap : pjtData){
				for(String applyNo : (List<String>) pjtMap.get("applyList")){
					if(!dupCheckmap.containsKey(applyNo)){
						applyNos.add(applyNo);
						dupCheckmap.put(applyNo, "");
					}
				}
			}
			buildNoAssetApplyNoMap.put( buildNo, applyNos.toArray( new String[0]) );
			bpDto.getBpEntity().setContent(paramBean.getUnitComfirmBean().getUnitSummary());
			buildNoEntityMap.put( buildNo, bpDto.getBpEntity() );


			//ビルドパッケージごとにメール送信
			for ( Map<String,Object> map : packageData ) {

				successMailBean.setBpEntity		( buildNoEntityMap.get( map.get( "bpId" ) ));
				successMailBean.setLotEntity	( lotEntity );
				successMailBean.setPackageMap	( map );
				successMailBean.setAssetApplyEntityList( getAssetApplyEntity( buildNoAssetApplyNoMap.get( map.get( "bpId" ) )));

				successMail.execute( mailServiceDto );
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( BmMessageId.BM005068S, e , paramBean.getFlowAction() ) ) ;
		}

		return serviceDto;
	}

	/**
	 * ビルドパッケージに含まれる承認済申請情報を取得する
	 * @param applyNoList
	 * @return
	 */
	private List<IAreqEntity> getAssetApplyEntity( String[] applyNoArray ) {

		List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>();

		// 全量コンパイル対応
		if ( !TriStringUtils.isEmpty( applyNoArray )) {

			IAreqEntity[] assetApplyEntities = this.support.getAreqEntity( applyNoArray );

			for ( IAreqEntity assetApplyEntity : assetApplyEntities ) {

				if ( !AmAreqStatusId.BuildPackageClosed.equals( assetApplyEntity.getStsId() ) ) {
					assetApplyEntityList.add( assetApplyEntity );
				}
			}
		}

		return assetApplyEntityList;
	}

}
