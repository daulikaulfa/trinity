package jp.co.blueship.tri.bm.beans.mail.dto;

import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;

public class RelUnitMailServiceBean extends MailServiceBean {

	/**
	 *
	 */
	private static final long serialVersionUID = 5371470310313252818L;

	private IBpEntity bpEntity;
	private ILotEntity pjtLotEntity;
	private List<IAreqEntity> assetApplyEntityList;
	private List<Map<String,Object>> packageList ;
	private Map<String,Object> packageMap;

	public IBpEntity getBpEntity() {
		return bpEntity;
	}
	public void setBpEntity(IBpEntity bpEntity) {
		this.bpEntity = bpEntity;
	}
	public ILotEntity getLotEntity() {
		return pjtLotEntity;
	}
	public void setLotEntity( ILotEntity pjtLotEntity ) {
		this.pjtLotEntity = pjtLotEntity;
	}
	public List<IAreqEntity> getAssetApplyEntityList() {
		return assetApplyEntityList;
	}
	public void setAssetApplyEntityList( List<IAreqEntity> assetApplyEntityList ) {
		this.assetApplyEntityList = assetApplyEntityList;
	}
	public List<Map<String,Object>> getPackageList() {
		return packageList;
	}
	public void setPackageList( List<Map<String,Object>> packageList ) {
		this.packageList = packageList;
	}
	public Map<String,Object> getPackageMap() {
		return packageMap;
	}
	public void setPackageMap( Map<String,Object> packageMap ) {
		this.packageMap = packageMap;
	}

}
