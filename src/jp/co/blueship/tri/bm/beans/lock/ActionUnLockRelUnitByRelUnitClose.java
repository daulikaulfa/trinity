package jp.co.blueship.tri.bm.beans.lock;

import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * ビルドパッケージクローズに関連する業務排他の解除を行います。
 * <br>予期しないエラーとなった場合、「クローズ中」になってしまうステータスを処理します。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */
public class ActionUnLockRelUnitByRelUnitClose implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private BmFinderSupport support = null;

	public void setSupport( BmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute(IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto) {

		FlowRelUnitCloseRequestServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( !refererID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST ) &&
					!forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {
				return serviceDto;
			}

			if ( forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {
				String[] bpIds = paramBean.getSelectedUnitNo();

				List<IBpDto> bpDtoList = this.support.findBpDto(bpIds);
				String userName		= paramBean.getUserName();
				String userId		= paramBean.getUserId();
				Timestamp systemDate = TriDateUtils.getSystemTimestamp();

				// ビルドパッケージ情報の更新
				updateBuildEntityToCloseUnActive			( paramBean, bpDtoList, userName, userId , systemDate );

				// 申請情報の更新
				updateAssetApplyEntityToCloseUnActive	( paramBean, bpDtoList, userName, userId , systemDate );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005029S, e);
		}
	}

	/**
	 * ビルドパッケージ情報がクローズ中のままであれば、クローズエラーに更新する
	 * @param entities ビルドパッケージ情報
	 * @param userName ユーザ名
	 * @param systemDate 更新日時
	 */
	private void updateBuildEntityToCloseUnActive(
			FlowRelUnitCloseRequestServiceBean paramBean,
			List<IBpDto> bpDtoList, String userName, String userId , Timestamp systemDate ) {

		if ( SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) )
			return;

		this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), BmTables.BM_BP, BmBpStatusIdForExecData.BuildPackageCloseError);

		if ( DesignSheetUtils.isRecord() ) {
			for ( IBpDto bpDto : bpDtoList ) {
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts(UmActStatusId.none.getStatusId());

				support.getBpHistDao().insert( histEntity , bpDto);
			}
		}

	}

	/**
	 * 申請情報はクローズ中のままであれば、クローズエラーに更新する
	 * @param bpEntities ビルドパッケージ情報
	 * @param userName ユーザ名
	 * @param systemDate 更新日時
	 * @return 更新された申請番号
	 */
	private void updateAssetApplyEntityToCloseUnActive(
			FlowRelUnitCloseRequestServiceBean paramBean,
			List<IBpDto> bpDtoList, String userName, String userId , Timestamp systemDate ) {

		String[] applyNoArray = BmExtractEntityAddonUtils.getAreqIds( bpDtoList );
		if ( TriStringUtils.isEmpty( applyNoArray )) {
			// 全資産コンパイル対応
			return;
		}

		if ( SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) )
			return;

		this.support.getSmFinderSupport().updateExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.BuildPackageCloseError);

		if ( DesignSheetUtils.isRecord() ) {
			for ( String areqId : applyNoArray ) {
				// AreqDto取得
				IAreqDto areqDto = this.support.getAmFinderSupport().findAreqDto( areqId );

				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts(UmActStatusId.none.getStatusId());

				support.getAmFinderSupport().getAreqHistDao().insert( histEntity , areqDto);
			}
		}

	}

}
