package jp.co.blueship.tri.bm.beans.lock;

import java.io.File;
import java.sql.Timestamp;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.rm.RmBusinessJudgUtils;

/**
 * ビルドパッケージ作成に関連する業務排他を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class ActionLockRelUnitByRelUnitEntry implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute(IServiceDto<FlowRelUnitEntryServiceBean> serviceDto) {

		FlowRelUnitEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			// ビルドパッケージに含まれている申請情報
			List<IAreqDto> entitiyList =
				this.support.getAreqEntity( paramBean.getApplyInfoViewBeanList() );

			// ビルドパッケージ情報の登録
			String bpId = insertBuildEntity( paramBean, entitiyList );
			paramBean.setBuildNo	( bpId );

			support.getSmFinderSupport().cleaningExecDataSts( BmTables.BM_BP, bpId );
			support.getSmFinderSupport().registerExecDataSts(
					paramBean.getProcId(), BmTables.BM_BP, BmBpStatusIdForExecData.CreatingBuildPackage, bpId);

			UnitInfoInputBean unitInfoInputBean = paramBean.getUnitInfoBean().getUnitInfoInputBean();
			support.getUmFinderSupport().registerCtgLnk(unitInfoInputBean.getCtgId(), BmTables.BM_BP, bpId);
			support.getUmFinderSupport().registerMstoneLnk(unitInfoInputBean.getMstoneId(), BmTables.BM_BP, bpId);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005030S, e);
		}
	}

	/**
	 * ビルドパッケージエンティティを登録する。
	 * @param paramBean FlowRelUnitEntryServiceBeanオブジェクト
	 * @param systemDate システム日時
	 * @param entityList 申請情報エンティティのリスト
	 * @return 採番したビルドパッケージ番号
	 */
	private final String insertBuildEntity(
			FlowRelUnitEntryServiceBean paramBean,
			List<IAreqDto> entityList ) {

		String buildNo			= paramBean.getBuildNo();
		IBpEntity buildEntity	= new BpEntity();

		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );

		this.support.updateBpAreqLnk(buildNo, entityList);

		Set<String> moduleNameSet = getChangedModuleSet( entityList, lotDto );
		this.support.updateBpMdlLnk(buildNo, lotDto.getLotEntity().getLotId(), moduleNameSet);

		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		buildEntity.setBpId				( buildNo );
		buildEntity.setExecUserNm		( paramBean.getUserName() );
		buildEntity.setExecUserId		( paramBean.getUserId() );
		buildEntity.setLotId			( paramBean.getSelectedLotNo() );

		if ( StatusFlg.on.value().equals( paramBean.getAllAssetCompile() ) ) {
			buildEntity.setBldEnvId		( lotDto.getLotEntity().getFullBldEnvId() );
		} else {
			buildEntity.setBldEnvId		( lotDto.getLotEntity().getBldEnvId() );
		}
		buildEntity.setStsId			( BmBpStatusId.Unprocessed.getStatusId() );
		buildEntity.setDelStsId			( StatusFlg.off );
		buildEntity.setProcId			( paramBean.getProcId() );
		buildEntity.setProcStTimestamp	( systemTimestamp );
		buildEntity.setUpdTimestamp		( systemTimestamp );
		buildEntity.setUpdUserNm		( paramBean.getUserName() );
		buildEntity.setUpdUserId		( paramBean.getUserId() );
		buildEntity.setBpNm				(
				paramBean.getUnitInfoBean().getUnitInfoInputBean().getUnitName() );
		buildEntity.setSummary			(
				paramBean.getUnitInfoBean().getUnitInfoInputBean().getUnitSummary() );

		this.support.getBpDao().insert( buildEntity );

		if ( DesignSheetUtils.isRecord() ) {
			// 履歴用DTO作成
			IBpDto bpDto = this.support.findBpDto(buildEntity);

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getBpHistDao().insert( histEntity , bpDto);
		}

		return buildNo;
	}

	/**
	 * 変更のあったモジュール名を取得する
	 * @param entityList 申請情報エンティティのリスト
	 * @param lotDto ロットDTO
	 * @return 変更のあったモジュール名のセット
	 */
	private Set<String> getChangedModuleSet(
			List<IAreqDto> entityList, ILotDto lotDto ) {

		Set<String> modifiedModuleSet	= new TreeSet<String>();

		if ( 0 != entityList.size() ) {

			for ( IAreqDto entity : entityList ) {

				if ( RmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() )) {

					setModuleNameByRetApply( entity, modifiedModuleSet );

				} else if ( RmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) {

					setModuleNameByDelApply( entity, modifiedModuleSet );
				}
			}

		} else {

			// 全資産コンパイル対応
			for ( ILotMdlLnkEntity moduleEntity : lotDto.getIncludeMdlEntities( true ) ) {

				modifiedModuleSet.add( moduleEntity.getMdlNm() );
			}
		}
		return modifiedModuleSet;
	}

	/**
	 * 返却申請番号からから変更のあったモジュール名を抽出する
	 * @param entity 返却申請情報エンティティ
	 * @param modifiedModuleSet 抽出したモジュール名を格納するセット
	 */
	private void setModuleNameByRetApply(
			IAreqDto entity, Set<String> modifiedModuleSet ) {

		for ( IAreqFileEntity assetFileEntity : entity.getAreqFileEntities(AreqCtgCd.ReturningRequest) ) {

			String assetRetApplyFilePath = assetFileEntity.getFilePath();
			// パス区切り文字をシステムデフォルトに変換
			assetRetApplyFilePath = ( new File( assetRetApplyFilePath ) ).getPath();

			// パスの先頭がモジュール名
			String modifiedModule =
				StringUtils.substringBefore( assetRetApplyFilePath, File.separator );
			modifiedModuleSet.add( modifiedModule );

		}
	}

	/**
	 * 削除資産のパスから変更のあったモジュール名を抽出する
	 * @param entity 削除申請情報エンティティ
	 * @param modifiedModuleSet 抽出したモジュール名を格納するセット
	 */
	private void setModuleNameByDelApply(
			IAreqDto entity, Set<String> modifiedModuleSet ) {

		if ( null == entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) return;


		for ( IAreqFileEntity assetFileEntity : entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {

			String assetDelApplyFilePath = assetFileEntity.getFilePath();
			// パス区切り文字をシステムデフォルトに変換
			assetDelApplyFilePath = ( new File( assetDelApplyFilePath ) ).getPath();

			// パスの先頭がモジュール名
			String modifiedModule =
				StringUtils.substringBefore( assetDelApplyFilePath, File.separator );
			modifiedModuleSet.add( modifiedModule );

		}
	}
}
