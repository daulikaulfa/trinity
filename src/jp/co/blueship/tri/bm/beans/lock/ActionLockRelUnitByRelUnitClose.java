package jp.co.blueship.tri.bm.beans.lock;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.BmFluentFunctionUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;


/**
 * ビルドパッケージクローズに関連する業務排他を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 *
 */

public class ActionLockRelUnitByRelUnitClose implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private BmFinderSupport support = null;
	public void setSupport( BmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute(IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto) {

		FlowRelUnitCloseRequestServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();

			if ( !refererID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST ) &&
					!forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {
				return serviceDto;
			}

			if ( forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {

				List<IBpDto> bpDtoList = this.support.findBpDto( paramBean.getSelectedUnitNo() );
				String userName		= paramBean.getUserName();
				String userId		= paramBean.getUserId();
				Timestamp systemDate = TriDateUtils.getSystemTimestamp();

				// ビルドパッケージ情報の更新
				List<IBpEntity> bpEntities = updateBuildEntityToCloseActive( paramBean, bpDtoList, userName, userId , systemDate );

				// 申請情報の更新
				String[] areqIds  =
					updateAssetApplyEntityToCloseActive	( paramBean, bpDtoList, userName, userId , systemDate );
				paramBean.setTargetApplyNo( areqIds );

				String[] bpIds = FluentList.from(bpEntities).map( BmFluentFunctionUtils.toBpId ).asList().toArray(new String[0]);

				ISmFinderSupport smFinder = support.getSmFinderSupport();
				smFinder.cleaningExecDataSts(BmTables.BM_BP, bpIds);
				smFinder.cleaningExecDataSts(AmTables.AM_AREQ, areqIds);
				smFinder.registerExecDataSts(paramBean.getProcId(), BmTables.BM_BP, BmBpStatusIdForExecData.BuildPackageClosing, bpIds);
				smFinder.registerExecDataSts(paramBean.getProcId(), AmTables.AM_AREQ, AmAreqStatusIdForExecData.BuildPackageClosing, areqIds);

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005029S, e);
		}
	}

	/**
	 * ビルドパッケージ情報をクローズ中に更新する
	 * @param entities ビルドパッケージ情報
	 * @param userName ユーザ名
	 * @param systemDate 更新日時
	 */
	private List<IBpEntity> updateBuildEntityToCloseActive(
			FlowRelUnitCloseRequestServiceBean paramBean,
			List<IBpDto> entities, String userName, String userId , Timestamp systemDate ) {

		List<IBpEntity> bpEntities = new ArrayList<IBpEntity>();

		for ( IBpDto entity : entities ) {
			IBpEntity bpEntity = new BpEntity();

			bpEntity.setBpId( entity.getBpEntity().getBpId() );
			bpEntity.setCloseUserId		( userId );
			bpEntity.setCloseUserNm		( userName );
			bpEntities.add( bpEntity );
		}

		if ( 0 != bpEntities.size() ) {
			this.support.getBpDao().update( bpEntities );
		}

		return bpEntities;
	}
	/**
	 * 申請情報をクローズ中に更新する
	 * @param entities ビルドパッケージ情報
	 * @param userName ユーザ名
	 * @param systemDate 更新日時
	 * @return 更新された申請番号
	 */
	private String[] updateAssetApplyEntityToCloseActive(
			FlowRelUnitCloseRequestServiceBean paramBean,
			List<IBpDto> dtoList, String userName, String userId , Timestamp systemDate ) {

		String[] applyNoArray = BmExtractEntityAddonUtils.getAreqIds( dtoList );
		if ( TriStringUtils.isEmpty( applyNoArray )) {
			// 全資産コンパイル対応
			return new String[]{};
		}

		IEntityLimit<IAreqEntity> entityLimit = null;
		{
			IJdbcCondition condition =
					DBSearchConditionAddonUtil.getAreqConditionByClosableRelUnit( applyNoArray );

				entityLimit =
					this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 );
		}

		//再度、リリースクローズ済の変更管理を選択して再パッケージ化した場合に対応するため、コメントアウト
//		if ( StringAddonUtil.isNothing( entityLimit.getEntity() )) {
//
//			List<String>	messageList		= new ArrayList<String>();
//			List<String[]>	messageArgsList	= new ArrayList<String[]>();
//			messageList.add		( MessageId.MESREL3011 );
//			messageArgsList.add	( new String[] { } );
//
//			throw new ContinuableBusinessException( messageList, messageArgsList );
//		}

		List<String> applyNoList = new ArrayList<String>();

		for ( IAreqEntity entity : entityLimit.getEntities() ) {
			applyNoList.add( entity.getAreqId() );
		}

		return (String[])applyNoList.toArray( new String[0] );
	}

}
