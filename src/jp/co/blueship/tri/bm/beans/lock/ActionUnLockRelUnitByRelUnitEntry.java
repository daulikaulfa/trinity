package jp.co.blueship.tri.bm.beans.lock;

import java.sql.Timestamp;

import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * ビルドパッケージ作成に関連する業務排他の解除を行います。 <br>
 * 予期しないエラーとなった場合、「作成中」になってしまうステータスを処理します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class ActionUnLockRelUnitByRelUnitEntry implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute(IServiceDto<FlowRelUnitEntryServiceBean> serviceDto) {

		FlowRelUnitEntryServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			IBpEntity buildEntity = null;
			String systemDate = null;
			boolean genSuccess = false;

			systemDate = TriDateUtils.getSystemDate();
			String bpId = paramBean.getBuildNo();
			buildEntity = this.support.findBpEntity(bpId);

			genSuccess = isGenerateComplete(paramBean.getProcId(),bpId);

			// ビルドパッケージ情報の更新
			updateBuildEntity(paramBean, buildEntity, systemDate, genSuccess);

			if (genSuccess) {
				support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
				paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );
			} else {
				support.getSmFinderSupport().updateExecDataSts(
						paramBean.getProcId(), BmTables.BM_BP, BmBpStatusIdForExecData.BuildPackageError, bpId);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(RmMessageId.RM005030S, e);
		}
	}

	/**
	 * ビルドパッケージ作成が成功したかどうかをチェックする。
	 *
	 * @param procId 処理対象プロセスID
	 * @param buildNo パッケージビルドId
	 * @return 成功していればtrue、そうでなければfalse
	 */
	private boolean isGenerateComplete(String procId, String buildNo) {

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(procId);
		ITaskFlowProcEntity[] entities = this.support.getTaskFlowProcDao().find(condition.getCondition()).toArray(new ITaskFlowProcEntity[0]);

		if (0 == entities.length) {
			// プロセスレコードが一件もない場合、なんらかのエラーが発生したとみなす
			TriSystemException e = new TriSystemException(BmMessageId.BM004004F, buildNo);
			IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
			LogHandler.fatal(log, ac.getMessage(BmMessageId.BM004004F), e);
			return false;
		}

		for (ITaskFlowProcEntity entity : entities) {
			if (BmBpStatusIdForExecData.BuildPackageError.equals(entity.getStsId()))
				return false;
		}

		return true;
	}

	/**
	 * Taskの結果を受けて、ビルドパッケージ情報を更新する。
	 *
	 * @param paramBean FlowRelUnitEntryServiceBeanオブジェクト
	 * @param entity ビルドパッケージエンティティ
	 * @param systemDate システム日時
	 * @param genSuccess ビルドパッケージが成功したかどうか
	 */
	private void updateBuildEntity(FlowRelUnitEntryServiceBean paramBean, IBpEntity entity, String systemDate, boolean genSuccess) {

		if (null == entity){
			return;
		}

		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		if (genSuccess) {
			entity.setStsId(BmBpStatusId.BuildPackageCreated.getStatusId());
		}

		entity.setProcEndTimestamp(systemTimestamp);
		entity.setUpdUserNm(paramBean.getUserName());
		entity.setUpdUserId(paramBean.getUserId());
		entity.setUpdTimestamp(systemTimestamp);

		this.support.getBpDao().update(entity);

		if (DesignSheetUtils.isRecord()) {
			// 履歴用DTO作成
			IBpDto bpDto = this.support.findBpDto(entity);

			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts( UmActStatusId.none.getStatusId() );

			support.getBpHistDao().insert( histEntity , bpDto);
		}
	}

}
