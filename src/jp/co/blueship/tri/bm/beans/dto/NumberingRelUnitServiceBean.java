package jp.co.blueship.tri.bm.beans.dto;

import jp.co.blueship.tri.fw.svc.beans.dto.NumberingServiceBean;

public class NumberingRelUnitServiceBean extends NumberingServiceBean {

	private static final long serialVersionUID = 1L;

	

	/** 選択されたビルドパッケージ番号 */
	private String[] selectedUnitNo = null;

	public String[] getSelectedUnitNo() {
		return selectedUnitNo;
	}
	public void setSelectedUnitNo( String[] selectedUnitNo ) {
		this.selectedUnitNo = selectedUnitNo;
	}

}
