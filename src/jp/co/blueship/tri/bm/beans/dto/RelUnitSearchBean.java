package jp.co.blueship.tri.bm.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * ビルドパッケージの詳細検索を行うための検索条件情報です。
 * 
 * @author blueship
 *
 */
public class RelUnitSearchBean implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** sessionに保持する場合、任意に一意とするキー値 */
	private String sessionKey = null;

	/** 詳細検索 RU番号 */
	private String searchBuildNo = null;
	
	/** 詳細検索 RU名 */
	private String searchBuildName = null;
	
	/** 詳細検索 RC番号 */
	private String searchRelNo = null;
	
	/** 詳細検索 変更管理番号 */
	private String searchPjtNo = null ;
	
	/** 詳細検索 変更要因番号 */
	private String searchChangeCauseNo = null ;
	
	/** 詳細検索 検索件数 */
	private List<ItemLabelsBean> searchRelUnitCountList = null ;
	
	/** 詳細検索 選択済み検索件数 */
	private String selectedRelUnitCount = null ;
	
	public String getSessionKey() {
		return sessionKey;
	}
	public void setSessionKey(String sessionKey) {
		this.sessionKey = sessionKey;
	}
	
	public String getSelectedRelUnitCount() {
		return selectedRelUnitCount;
	}
	public void setSelectedRelUnitCount(String selectedRelUnitCount) {
		this.selectedRelUnitCount = selectedRelUnitCount;
	}
	public List<ItemLabelsBean> getSearchRelUnitCountList() {
		return searchRelUnitCountList;
	}
	public void setSearchRelUnitCountList(List<ItemLabelsBean> searchRelUnitCountList) {
		this.searchRelUnitCountList = searchRelUnitCountList;
	}
	public String getSearchChangeCauseNo() {
		return searchChangeCauseNo;
	}
	public void setSearchChangeCauseNo(String searchChangeCauseNo) {
		this.searchChangeCauseNo = searchChangeCauseNo;
	}
	public String getSearchPjtNo() {
		return searchPjtNo;
	}
	public void setSearchPjtNo(String searchPjtNo) {
		this.searchPjtNo = searchPjtNo;
	}
	public String getSearchBuildName() {
		return searchBuildName;
	}
	public void setSearchBuildName(String searchBuildName) {
		this.searchBuildName = searchBuildName;
	}
	public String getSearchBuildNo() {
		return searchBuildNo;
	}
	public void setSearchBuildNo(String searchBuildNo) {
		this.searchBuildNo = searchBuildNo;
	}
	public String getSearchRelNo() {
		return searchRelNo;
	}
	public void setSearchRelNo(String searchRelNo) {
		this.searchRelNo = searchRelNo;
	}
	
}
