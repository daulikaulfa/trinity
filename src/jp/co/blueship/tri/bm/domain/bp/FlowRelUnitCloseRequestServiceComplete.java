package jp.co.blueship.tri.bm.domain.bp;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.sm.beans.NumberingServiceSupport;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージクローズ受付完了画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitCloseRequestServiceComplete implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private List<IDomain<IGeneralServiceBean>> pojoList;
	private FlowRelUnitEditSupport support;
	private NumberingServiceSupport<IGeneralServiceBean> numbering;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	/**
	 * 自動採番部品がインスタンス生成時に自動的に設定されます。
	 * @param numbering ロット・VCSタグの自動採番部品
	 */
	public void setNumbering( NumberingServiceSupport<IGeneralServiceBean> numbering ) {
		this.numbering = numbering;
	}

	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute( IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto) {

		FlowRelUnitCloseRequestServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST ) &&
					!forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {
				return serviceDto;
			}

			if ( refererID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {

			}

			if ( forwordID.equals( RelUnitScreenID.COMP_UNIT_CLOSE_REQUEST )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String[] buildNo = paramBean.getSelectedUnitNo();
					ItemCheckAddonUtils.checkBuildNo( buildNo );


					// グループの存在チェック
					IBpEntity buildEntity = this.support.findBpEntity( buildNo[0] ) ;
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( buildEntity.getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

					IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>();
					innerServiceDto.setServiceBean( paramBean );

					paramBean.setCacheLotDto( dto );
					//IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();

					this.numbering.execute( innerServiceDto );


					//下層のアクションを実行する
					{
						for ( IDomain<IGeneralServiceBean> pojo : this.pojoList ) {
							innerServiceDto = pojo.execute( innerServiceDto );
						}
					}
				}
			}

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005076S,e);
		}
	}

}
