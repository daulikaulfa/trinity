package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * ビルドパッケージ作成・ビルドパッケージ情報入力用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class UnitInfoInputBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
//	/** ビルドパッケージ番号 */
//	private String unitNo = null;
	/** 取消コメント */
	private String cancelComment = null;
	/** クローズコメント */
	private String closeComment = null;
	/** ベースライン */
	private String baseLineTag = null;

	private String ctgId = null;

	private String mstoneId = null;

	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}
//
//	public String getUnitNo() {
//		return unitNo;
//	}
//	public void setUnitNo( String unitNo ) {
//		this.unitNo = unitNo;
//	}

	public String getCancelComment() {
		return cancelComment;
	}
	public void setCancelComment( String cancelComment ) {
		this.cancelComment = cancelComment;
	}

	public String getCloseComment() {
		return closeComment;
	}
	public void setCloseComment( String closeComment ) {
		this.closeComment = closeComment;
	}

	public String getBaseLineTag() {
		return baseLineTag;
	}
	public void setBaseLineTag( String baseLineTag ) {
		this.baseLineTag = baseLineTag;
	}

	public String getCtgId() {
		return ctgId;
	}
	public void setCtgId(String ctgId) {
		this.ctgId = ctgId;
	}

	public String getMstoneId() {
		return mstoneId;
	}
	public void setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
	}

}
