package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * リリースからの変更管理情報の閲覧
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 変更要因番号 */
	private String changeCauseNo = null;
	/** 変更要因分類 */
	private String changeCauseClassify = null;
	/** 変更概要 */
	private String pjtSummary = null;
	/** 選択固定フラグ */
	private boolean selectFixFlag = false;
	
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}

	public String getChangeCauseNo() {
		return changeCauseNo;
	}
	public void setChangeCauseNo(String changeCauseNo) {
		this.changeCauseNo = changeCauseNo;
	}

	public String getChangeCauseClassify() {
		return changeCauseClassify;
	}
	public void setChangeCauseClassify(String changeCauseClassify) {
		this.changeCauseClassify = changeCauseClassify;
	}

	public String getPjtSummary() {
		return pjtSummary;
	}
	public void setPjtSummary(String pjtSummary) {
		this.pjtSummary = pjtSummary;
	}
	
	public boolean getSelectFixFlag() {
		return selectFixFlag;
	}
	public void setSelectFixFlag( boolean selectFixFlag ) {
		this.selectFixFlag = selectFixFlag;
	}
}
