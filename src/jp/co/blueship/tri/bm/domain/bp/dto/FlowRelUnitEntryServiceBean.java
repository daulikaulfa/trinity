package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtSelectBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitComfirmBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.dcm.beans.dto.ReportServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;

/**
 * ビルドパッケージ作成・基本情報入力～ビルドパッケージ作成確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitEntryServiceBean extends ReportServiceBean {

	private static final long serialVersionUID = 1L;

	/**
	 * ロット選択
	 */
	private LotSelectBean lotSelectBean = null;
	/**
	 * 選択されたロット番号
	 */
	private String selectedLotNo = null;
	/**
	 * 選択されたサーバ番号
	 */
	private String selectedServerNo = null;
	/**
	 * 変更管理選択
	 */
	private PjtSelectBean pjtSelectBean = null;
	/**
	 * 選択された変更管理番号(クローズ済分)
	 */
	private String[] selectedClosedPjtNo = null;
	/**
	 * 選択された変更管理番号
	 */
	private String[] selectedPjtNo = null;
	/**
	 * 申請情報
	 */
	private List<ApplyInfoViewBean> applyInfoViewBeanList = null;
	/**
	 * ビルドパッケージ情報
	 */
	private UnitInfoBean unitInfoBean = null;
	/**
	 * ビルドパッケージ作成確認
	 */
	private UnitComfirmBean unitComfirmBean = null;
	/**
	 * コンパイル状況照会
	 */
	private GenerateDetailViewBean generateDetailViewBean = null;
	/**
	 * ビルドパッケージ番号(ビルドパッケージ番号)
	 */
	private String buildNo = null;
	/**
	 * 全資産ビルドモード
	 */
	private String allAssetCompile = StatusFlg.off.value();

	/** クローズ時 パッケージクローズエラー警告 */
	private WarningCheckBean warningCheckUnitCloseError = new WarningCheckBean() ;


	public WarningCheckBean getWarningCheckUnitCloseError() {
		return warningCheckUnitCloseError;
	}
	public void setWarningCheckUnitCloseError(
			WarningCheckBean warningCheckUnitCloseError) {
		this.warningCheckUnitCloseError = warningCheckUnitCloseError;
	}
	public LotSelectBean getLotSelectBean() {
		if ( null == lotSelectBean ) {
			lotSelectBean = new LotSelectBean();
		}
		return lotSelectBean;
	}
	public void setLotSelectBean(LotSelectBean lotSelectBean) {
		this.lotSelectBean = lotSelectBean;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo(String selectedLotNo) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getSelectedServerNo() {
		return selectedServerNo;
	}
	public void setSelectedServerNo(String selectedServerNo) {
		this.selectedServerNo = selectedServerNo;
	}

	public PjtSelectBean getPjtSelectBean() {
		if ( null == pjtSelectBean ) {
			pjtSelectBean = new PjtSelectBean();
		}
		return pjtSelectBean;
	}
	public void setPjtSelectBean(PjtSelectBean pjtSelectBean) {
		this.pjtSelectBean = pjtSelectBean;
	}

	public String[] getSelectedClosedPjtNo() {
		return selectedClosedPjtNo;
	}
	public void setSelectedClosedPjtNo( String[] selectedClosedPjtNo ) {
		this.selectedClosedPjtNo = selectedClosedPjtNo;
	}

	public String[] getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String[] selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}

	public List<ApplyInfoViewBean> getApplyInfoViewBeanList() {
		if ( null == applyInfoViewBeanList ) {
			applyInfoViewBeanList = new ArrayList<ApplyInfoViewBean>();
		}
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList( List<ApplyInfoViewBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}

	public UnitInfoBean getUnitInfoBean() {
		if ( null == unitInfoBean ) {
			unitInfoBean = new UnitInfoBean();
		}
		return unitInfoBean;
	}
	public void setUnitInfoBean( UnitInfoBean unitInfoBean ) {
		this.unitInfoBean = unitInfoBean;
	}

	public UnitComfirmBean getUnitComfirmBean() {
		if ( null == unitComfirmBean ) {
			unitComfirmBean = new UnitComfirmBean();
		}
		return unitComfirmBean;
	}
	public void setUnitComfirmBean( UnitComfirmBean unitComfirmBean ) {
		this.unitComfirmBean = unitComfirmBean;
	}

	public GenerateDetailViewBean getGenerateDetailViewBean() {
		if ( null == generateDetailViewBean ) {
			generateDetailViewBean = new GenerateDetailViewBean();
		}
		return generateDetailViewBean;
	}
	public void setGenerateDetailViewBean(
			GenerateDetailViewBean generateDetailViewBean ) {
		this.generateDetailViewBean = generateDetailViewBean;
	}

	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}

	public String getAllAssetCompile() {
		return allAssetCompile;
	}
	public void setAllAssetCompile( String allAssetCompile ) {
		this.allAssetCompile = allAssetCompile;
	}

}
