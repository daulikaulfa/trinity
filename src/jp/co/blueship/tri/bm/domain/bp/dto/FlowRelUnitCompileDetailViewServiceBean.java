package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * ビルドパッケージ作成・ビルドパッケージ作成状況確認用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitCompileDetailViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択されたロット番号 */	
	private String selectedLotNo = null;
	/** ビルドパッケージ番号 */
	private String buildNo = null;
	/** コンパイル状況照会 */
	private GenerateDetailViewBean generateDetailViewBean = null;
	/** ロット番号（コンボ用）*/
	private List<ItemLabelsBean> lotList = null ;
	
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo( String buildNo ) {
		this.buildNo = buildNo;
	}
	
	public GenerateDetailViewBean getGenerateDetailViewBean() {
		if ( null == generateDetailViewBean ) {
			generateDetailViewBean = new GenerateDetailViewBean();
		}
		return generateDetailViewBean;
	}
	public void setGenerateDetailViewBean(
			GenerateDetailViewBean generateDetailViewBean) {
		this.generateDetailViewBean = generateDetailViewBean;
	}
	
	public List<ItemLabelsBean> getLotList() {
		return lotList;
	}
	public void setLotList( List<ItemLabelsBean> lotList ) {
		this.lotList = lotList;
	}
}
