package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * ビルドパッケージ作成・ビルドパッケージクローズ受付用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class FlowRelUnitCloseRequestServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** 選択されたロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** 選択されたビルドパッケージ番号 */
	private String[] selectedUnitNo = null;
	/** ビルドパッケージ編集入力情報 */
	private UnitInfoInputBean unitInfoInputBean = null;
	/** 申請情報 */
	private List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList = new ArrayList<UnitCloseConfirmViewBean>();
	/** クローズ対象の申請番号 */
	private String[] targetApplyNo = null;

	/** クローズ時 ２重貸出警告 */
	private WarningCheckBean warningCheckAssetDuplicate = new WarningCheckBean() ;
	/** クローズ時 パッケージクローズエラー警告 */
	private WarningCheckBean warningCheckUnitCloseError = new WarningCheckBean() ;

	/**
	 * CacheしたロットDTO。Domain層の上位で設定されDomain層下でのみ利用される
	 */
	private ILotDto cacheLotDto = null;
	
	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用される
	 */
	private List<ILotBlDto> registerLotBlDto = new ArrayList<ILotBlDto>();
	
	private boolean buildCloseConfirmBean = true;

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}

	public String[] getSelectedUnitNo() {
		return selectedUnitNo;
	}
	public void setSelectedUnitNo( String[] selectedUnitNo ) {
		this.selectedUnitNo = selectedUnitNo;
	}

	public UnitInfoInputBean getUnitInfoInputBean() {
		return unitInfoInputBean;
	}
	public void setUnitInfoInputBean( UnitInfoInputBean unitInfoInputBean ) {
		this.unitInfoInputBean = unitInfoInputBean;
	}

	public List<UnitCloseConfirmViewBean> getUnitCloseConfirmViewBeanList() {
		return unitCloseConfirmViewBeanList;
	}
	public void setUnitCloseConfirmViewBeanList( List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList ) {
		this.unitCloseConfirmViewBeanList = unitCloseConfirmViewBeanList;
	}

	public String[] getTargetApplyNo() {
		return targetApplyNo;
	}
	public void setTargetApplyNo( String[] targetApplyNo ) {
		this.targetApplyNo = targetApplyNo;
	}
	public WarningCheckBean getWarningCheckAssetDuplicate() {
		return warningCheckAssetDuplicate;
	}
	public void setWarningCheckAssetDuplicate(
			WarningCheckBean warningCheckAssetDuplicate) {
		this.warningCheckAssetDuplicate = warningCheckAssetDuplicate;
	}
	public WarningCheckBean getWarningCheckUnitCloseError() {
		return warningCheckUnitCloseError;
	}
	public void setWarningCheckUnitCloseError(
			WarningCheckBean warningCheckUnitCloseError) {
		this.warningCheckUnitCloseError = warningCheckUnitCloseError;
	}

	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されるを取得します。
	 *
	 * @return CacheしたロットDTO。
	 */
	public ILotDto getCacheLotDto() {
	    return cacheLotDto;
	}
	/**
	 * CacheしたロットDTO。
	 * Domain層の上位で設定されDomain層下でのみ利用されるを設定します。
	 *
	 * @param cacheLotDto CacheしたロットDTO。
	 */
	public void setCacheLotDto(ILotDto cacheLotDto) {
	    this.cacheLotDto = cacheLotDto;
	}
	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用されるを取得します。
	 * @return 登録したロット・ベースラインDTO。Domain層でのみ利用される
	 */
	public List<ILotBlDto> getRegisterLotBlDto() {
	    return registerLotBlDto;
	}
	/**
	 * 登録したロット・ベースラインDTO。Domain層でのみ利用されるを設定します。
	 * @param registerLotBlDto 登録したロット・ベースラインDTO。Domain層でのみ利用される
	 */
	public void setRegisterLotBlDto(List<ILotBlDto> registerLotBlDto) {
	    this.registerLotBlDto = registerLotBlDto;
	}
	
	public boolean isBuildCloseConfirmBean() {
		return buildCloseConfirmBean;
	}
	
	public void setBuildCloseConfirmBean(boolean buildCloseConfirmBean) {
		this.buildCloseConfirmBean = buildCloseConfirmBean;
	}

}
