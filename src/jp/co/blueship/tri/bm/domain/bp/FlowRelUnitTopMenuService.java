package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.rmisvc.eb.IRmiSvcDto;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitTopMenuServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitTopMenuServiceBean.RelUnitTopViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitTopMenuServiceBean.RelUnitTopViewBean.RelUnitCompileResultViewBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.agent.service.flow.beans.AgentStatus;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.constants.RemoteServiceType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * ビルドパッケージ作成・トップ画面の表示情報設定Class<br>
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitTopMenuService implements IDomain<FlowRelUnitTopMenuServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelUnitTopMenuServiceBean> execute( IServiceDto<FlowRelUnitTopMenuServiceBean> serviceDto ) {

		FlowRelUnitTopMenuServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			//アクション
			List<Object> paramList = new ArrayList<Object>();
			paramList.add( RemoteServiceType.AGENT_RELEASE ) ;

			try {
				IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
						.setServiceBean( paramBean )
						.setParamList( paramList );

				for ( IDomain<IGeneralServiceBean> action : this.actions ) {
					action.execute( innerServiceDto );
				}
			} finally {
				//agent疎通チェック結果によるメッセージ出力
				this.checkActiveAgents( paramList , paramBean ) ;
			}


			// 活動中の全ロットを取得
			IJdbcCondition condition = DBSearchConditionAddonUtil.getActiveLotCondition();
			List<String> disableLinkLotNumbers	= new ArrayList<String>();

			// 旧互換機能のため、当該箇所に問題はない。
			@SuppressWarnings("deprecation")
			boolean isSearch =
				this.support.getAmFinderSupport().setAccessableLotNumbers( condition, paramBean, disableLinkLotNumbers, true );


			IEntityLimit<ILotEntity> lotEntityLimit = null;
			int selectedPageNo = 1;

			// アクセス可能なロットが１件もない場合は検索に行かない
			// （ロット番号が指定できないので、全部取ってきてしまうので）
			if ( isSearch ) {
				// 再検索
				ISqlSort sort = DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelUnitTop();
				selectedPageNo	=
					(( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo() );
				int maxPageNumber = sheet.intValue( BmDesignEntryKeyByBuild.maxPageNumberByTopLot );

				lotEntityLimit = this.support.getAmFinderSupport().getLotDao().find(
						condition.getCondition(), sort, selectedPageNo, maxPageNumber );
			} else {

				lotEntityLimit = new EntityLimit<ILotEntity>();
				lotEntityLimit.setEntities( FluentList.from(new ILotEntity[0]).asList() );
			}


			List<RelUnitTopViewBean> topViewBeanList = new ArrayList<RelUnitTopViewBean>();

			List<ILotDto> lotDtoList = this.support.getAmFinderSupport().findLotDto( lotEntityLimit.getEntities() );
			for ( ILotDto lotDto : lotDtoList )
			{

				RelUnitTopViewBean topViewBean = paramBean.newRelUnitTopViewBean();
				setRelUnitTopViewBeanLotEntity( topViewBean, lotDto.getLotEntity(), lotDto.getIncludeMdlEntities( true ) );

				if ( disableLinkLotNumbers.contains( topViewBean.getLotNo() )) {
					topViewBean.setViewLinkEnabled( false );
				} else {
					topViewBean.setViewLinkEnabled( true );
				}

				topViewBeanList.add( topViewBean );
			}

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() )) {
				if ( TriStringUtils.isEmpty( paramBean.getInfoMessageId() ) &&
						TriStringUtils.isEmpty( topViewBeanList )) {
					paramBean.setInfoMessage( BmMessageId.BM001028E );
				}
			}

			paramBean.setPageInfoView				(
					BmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), lotEntityLimit.getLimit() ));
			paramBean.setSelectPageNo				( selectedPageNo );
			paramBean.setRelUnitTopViewBeanList	( topViewBeanList );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005089S,e);
		}
	}

//	※agentがtaskのみに使われていたときの疎通チェックメッセージ生成ルール。
//	/**
//	 * agentの疎通チェック結果を取得し、メッセージ作成を行う。
//	 * @param paramList
//	 * @param paramBean
//	 */
//	private void checkActiveAgents( List<Object> paramList , FlowRelUnitTopMenuServiceBean paramBean ) {
//		AgentStatusTask[] agentStatusArray = ExtractEntityAddonUtil.extractAgentStatusTask( paramList ) ;
//
//		List<String> messageInfoList = new ArrayList<String>() ;
//		List<String[]> messageInfoArgsList = new ArrayList<String[]>() ;
//
//		if( null != agentStatusArray ) {
//			for( AgentStatusTask status : agentStatusArray ) {
//				if( true != status.isStatus() ) {
//					messageInfoList.add( MessageId.MESREL0016 ) ;
//					messageInfoArgsList.add( new String[] { /*status.getConfigLocations() ,*/ status.getServiceUrl() , status.getEnvNo() , status.getServerNo() } ) ;
//				}
//				log.debug( ( status.isStatus() ? "正常" : "無応答" ) + " " + status.getServiceUrl() + " " + status.getEnvNo() + " " + status.getServerNo() ) ;
//			}
//			if( 0 != messageInfoList.size() ) {
//				paramBean.setInfoMessageList( messageInfoList ) ;
//				paramBean.setInfoMessageArgsList( messageInfoArgsList ) ;
//			}
//		}
//	}
	/**
	 * agentの疎通チェック結果を取得し、メッセージ作成を行う。
	 * @param paramList
	 * @param paramBean
	 */

	private void checkActiveAgents( List<Object> paramList , FlowRelUnitTopMenuServiceBean paramBean ) {

		AgentStatus[] agentStatusArray = BmExtractEntityAddonUtils.extractAgentStatusArray( paramList ) ;
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		List<IMessageId> messageInfoList = new ArrayList<IMessageId>() ;
		List<String[]> messageInfoArgsList = new ArrayList<String[]>() ;

		if( null != agentStatusArray ) {
			for( AgentStatus status : agentStatusArray ) {
				IRmiSvcDto remoteService = status.getRemoteServiceEntity() ;
				//画面にはエラーのもののみ出力
				if( true != status.isStatus() ) {
					messageInfoList.add( BmMessageId.BM001029E ) ;
					String[] messageArgs = new String[] {
							remoteService.getRmiSvcEntity().getBldSrvId() ,
							remoteService.getRmiSvcEntity().getRmiSvcId() ,
							remoteService.getRmiSvcEntity().getRmiHostNm() ,
							String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()) ,
							String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()) ,
					} ;
					messageInfoArgsList.add( messageArgs ) ;
				}
				//ログにはすべてのステータスを出力
				String statusStr = status.isStatus() ? ac.getMessage(TriLogMessage.LSM0014) : ac.getMessage(TriLogMessage.LSM0015) ;
				String[] messageArgs = new String[] {
						statusStr ,
						remoteService.getRmiSvcEntity().getBldSrvId() ,
						remoteService.getRmiSvcEntity().getRmiSvcId() ,
						remoteService.getRmiSvcEntity().getRmiHostNm() ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiSvcPort()) ,
						String.valueOf(remoteService.getRmiSvcEntity().getRmiRegPort()) ,
				} ;
				LogHandler.info( log , ac.getMessage( paramBean.getLanguage(), BmMessageId.BM001030E , messageArgs ) ) ;
			}

			if( 0 != messageInfoList.size() ) {
				paramBean.setInfoMessageIdList( messageInfoList ) ;
				paramBean.setInfoMessageArgsList( messageInfoArgsList ) ;
			}
		}
	}


	/**
	 * ロットエンティティから画面表示内容を設定する。
	 * @param topViewBean 画面表示内容
	 * @param lotEntity ロットエンティティ
	 */
	private void setRelUnitTopViewBeanLotEntity(
			RelUnitTopViewBean topViewBean, ILotEntity lotEntity, List<ILotMdlLnkEntity> mdlEntities ) {

		// サーバ番号
		IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();
		topViewBean.setServerNo	( srvEntity.getBldSrvId() );

		// ロット番号
		topViewBean.setLotNo	( lotEntity.getLotId() );

		// ロット名
		topViewBean.setLotName	( lotEntity.getLotNm() );

		// モジュール名
		setRelUnitTopViewBeanModuleEntity	( topViewBean, mdlEntities );


		// 指定件数分のビルドパッケージ履歴情報
		IBpEntity[] buildEntities = getBuildEntities( lotEntity.getLotId() );
		// ビルドパッケージ結果
		setRelUnitTopViewBeanBuildEntity	( topViewBean, buildEntities );

	}

	/**
	 * モジュールエンティティから画面表示内容(ロットに含まれるモジュール名)を設定する。
	 * @param topViewBean 画面表示内容
	 * @param moduleEntities モジュールエンティティ
	 */
	private void setRelUnitTopViewBeanModuleEntity(
			RelUnitTopViewBean topViewBean,
			List<ILotMdlLnkEntity> moduleEntities ) {

		List<RelUnitCompileResultViewBean> compileResultViewBeanList =
									topViewBean.getRelUnitCompileResultViewBeanList();

		for ( ILotMdlLnkEntity moduleEntity : moduleEntities ) {

			RelUnitCompileResultViewBean compileResultViewBean =
											topViewBean.newRelUnitCompileResultViewBean();

			compileResultViewBean.setModuleName( moduleEntity.getMdlNm() );

			compileResultViewBeanList.add( compileResultViewBean );
		}

	}

	/**
	 * 指定されたロットに付属する作成済み(成功、失敗両方)ビルドパッケージエンティティを取得する
	 * @param lotId ロット番号
	 * @return ビルドパッケージエンティティ
	 */
	private IBpEntity[] getBuildEntities( String lotId ) {

		// 指定された件数(＝トップ画面の列数)取得
		BpCondition condition	= (BpCondition)DBSearchConditionAddonUtil.getBuildConditionByLotNo( lotId );
		ISqlSort sort				= DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelUnitTop();

		@SuppressWarnings("deprecation")
		String[] baseStatusId = { 	BmBpStatusId.Unprocessed.getStatusId(),
									BmBpStatusId.BuildPackageCreated.getStatusId(),
									BmBpStatusId.BuildPackageClosed.getStatusId(),
									BmBpStatusId.Merged.getStatusId() };

		String[] statusIdByNotEquals = {
									BmBpStatusIdForExecData.CreatingBuildPackage.getStatusId(),
									BmBpStatusIdForExecData.PendingRecreateBuildPackage.getStatusId(),
								 			};

		condition.setStsIds( baseStatusId );
		condition.setProcStsIdsByNotEquals( statusIdByNotEquals );

		int maxPageNumber		= sheet.intValue( BmDesignEntryKeyByBuild.maxPageNumberByTopUnit );

		IEntityLimit<IBpEntity> buildEntityLimit =
								this.support.getBpDao().find( condition.getCondition(), sort, 1, maxPageNumber );

		List<IBpEntity> buildEntityList = new ArrayList<IBpEntity>();

		IBpEntity[] entitys = buildEntityLimit.getEntities().toArray(new IBpEntity[0]);
		//for ( int i = entitys.length - 1 ; i >= 0; i-- ) {
		//	buildEntityList.add( entitys[i] );
		//}
		for( IBpEntity entity : entitys ) {
			buildEntityList.add( entity ) ;
		}

		return (IBpEntity[])buildEntityList.toArray( new IBpEntity[0] );
	}

	/**
	 * ビルドパッケージエンティティから画面表示内容(ビルドパッケージ履歴)を設定する。
	 * @param topViewBean 画面表示内容
	 * @param buildEntities ビルドパッケージエンティティ
	 */
	@SuppressWarnings("deprecation")
	private void setRelUnitTopViewBeanBuildEntity(
			RelUnitTopViewBean topViewBean, IBpEntity[] buildEntities ) {

		List<RelUnitCompileResultViewBean> compileResultViewBeanList =
			topViewBean.getRelUnitCompileResultViewBeanList();

		for ( IBpEntity buildEntity : buildEntities ) {

			topViewBean.setBuildNo		(
					addArrayElement( topViewBean.getBuildNo(), buildEntity.getBpId() ));
			topViewBean.setVersionTag	(
					addArrayElement( topViewBean.getVersionTag(), buildEntity.getBpCloseVerTag() ));
			topViewBean.setBuildEndDate	(
					addArrayElement( topViewBean.getBuildEndDate(),
							TriDateUtils.convertViewDateFormat( buildEntity.getProcEndTimestamp() ) ) );

			Set<String> moduleNameSet = FlowRelUnitEditSupport.getModuleNameSet( this.support.findBpMdlLnkEntities(buildEntity.getBpId() ) );

			for ( RelUnitCompileResultViewBean viewBean : compileResultViewBeanList ) {

				Boolean result = null;

				if ( moduleNameSet.contains( viewBean.getModuleName() )) {

					if ( BmBpStatusId.BuildPackageCreated.equals( buildEntity.getStsId() ) ||
							BmBpStatusId.BuildPackageClosed.equals( buildEntity.getStsId() ) ||
							BmBpStatusId.Merged.equals( buildEntity.getStsId() ) ) {
						result = new Boolean( true );
					} else if ( BmBpStatusIdForExecData.BuildPackageError.equals( buildEntity.getProcStsId() )) {
						result = new Boolean( false );
					}
				}

				viewBean.setBuildResult( addArrayElement( viewBean.getBuildResult(), result ));
			}
		}
	}

	/**
	 * 配列に要素を加えて返す。
	 * @param array 配列
	 * @param element 加える要素
	 * @return 配列
	 */
	private String[] addArrayElement( String[] array, String element ) {

		List<String> list = FluentList.from(array).asList();
		list = new ArrayList<String>( list );
		list.add( element );

		return (String[])list.toArray( new String[0] );

	}

	/**
	 * 配列に要素を加えて返す。
	 * @param array 配列
	 * @param element 加える要素
	 * @return 配列
	 */
	private Boolean[] addArrayElement( Boolean[] array, Boolean element ) {

		List<Boolean> list = FluentList.from(array).asList();
		list = new ArrayList<Boolean>( list );
		list.add( element );

		return (Boolean[])list.toArray( new Boolean[0] );

	}

}
