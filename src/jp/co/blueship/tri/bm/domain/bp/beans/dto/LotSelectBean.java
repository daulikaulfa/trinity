package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ビルドパッケージ作成・ロット選択画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class LotSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	/** ロット情報 */
	private List<LotViewRelUnitBean> lotViewBeanList = null;

	public List<LotViewRelUnitBean> getLotViewBeanList() {
		if ( null == lotViewBeanList ) {
			lotViewBeanList = new ArrayList<LotViewRelUnitBean>();
		}
		return lotViewBeanList;
	}
	public void setLotViewBeanList( List<LotViewRelUnitBean> lotViewBeanList ) {
		this.lotViewBeanList = lotViewBeanList;
	}

//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
	
}
