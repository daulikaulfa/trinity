package jp.co.blueship.tri.bm.domain.bp.beans;

import java.util.List;

import jp.co.blueship.tri.am.domain.head.beans.dto.ActionLotCheckInServiceBean;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * ビルドパッケージクローズ処理<br>
 * 【Subversion版】<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2011<br>
 */
public class RelUnitCloseRequestServiceCompleteForSvn extends BpClosureServiceTemplete implements IDomain<FlowRelUnitCloseRequestServiceBean> {


	@Override
	public void innerExecute(
			IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto,
			ActionLotCheckInServiceBean checkinBean,
			List<IBpDto> bpDtoList ) throws Exception {

		// 申請情報の更新
		updateAreqEntityToClose(checkinBean);
		// ビルドパッケージ情報の更新
		updateBpEntityToClose(checkinBean, bpDtoList);
		// ロット情報の更新（ロットは、成功履歴のみ記録する）
		updateLotEntityToClose(checkinBean);

		return;

	}

	@Override
	protected boolean isTargetVcsCategory( String vcsCtgCd ) {
		if ( VcsCategory.SVN.equals(VcsCategory.value(vcsCtgCd) )) {
			return true;
		}

		return false;
	}

}
