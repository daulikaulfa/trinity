package jp.co.blueship.tri.bm.domain.bp.beans.dto;

/**
 * ビルドパッケージ作成・ロット履歴一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class LotHistoryViewBean {

	/** サーバ番号 */
	private String serverNo = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** 概要 */
	private String lotSummary = null;
	/** ステータス */
	private String lotStatus = null;
	/**
	 * このロットの閲覧リンクを有効にするかどうか
	 */
	private boolean isViewLinkEnabled = false;
	
	
	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getLotSummary() {
		return lotSummary;
	}
	public void setLotSummary( String lotSummary ) {
		this.lotSummary = lotSummary;
	}

	public String getLotStatus() {
		return lotStatus;
	}
	public void setLotStatus( String lotStatus ) {
		this.lotStatus = lotStatus;
	}
	
	public boolean isViewLinkEnabled() {
		return isViewLinkEnabled;
	}
	public void setViewLinkEnabled( boolean isViewLinkEnabled ) {
		this.isViewLinkEnabled = isViewLinkEnabled;
	}
}
