package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * ビルドパッケージ作成・トップ画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitTopMenuServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** ビルドパッケージトップ画面用 */
	private List<RelUnitTopViewBean> relUnitTopViewBeanList = new ArrayList<RelUnitTopViewBean>();;
	
	
	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}
	
	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
	public List<RelUnitTopViewBean> getRelUnitTopViewBeanList() {
		return relUnitTopViewBeanList;
	}
	public void setRelUnitTopViewBeanList(
				List<RelUnitTopViewBean> relUnitTopViewBeanList ) {
		this.relUnitTopViewBeanList = relUnitTopViewBeanList;
	}
	
	
	public RelUnitTopViewBean newRelUnitTopViewBean() {
		return new RelUnitTopViewBean();
	}
	
	public class RelUnitTopViewBean {
		
		/** サーバ番号 */
		private String serverNo = null;
		/** ロット番号 */
		private String lotId = null;
		/** ロット名 */
		private String lotName = null;
		/** ビルドパッケージ番号 */
		private String[] buildNo = new String[0];
		/** ビルドパッケージタグ */
		private String[] versionTag = new String[0];
		/** ビルドパッケージ完了日時 */
		private String[] buildEndDate = new String[0];
		/** ビルドパッケージ作成結果 */
		private List<RelUnitCompileResultViewBean> relUnitCompileResultViewBeanList =
											new ArrayList<RelUnitCompileResultViewBean>();
		/**
		 * このロットの閲覧リンクを有効にするかどうか
		 */
		private boolean isViewLinkEnabled = false;
		
		
		public String getServerNo() {
			return serverNo;
		}
		public void setServerNo( String serverNo ) {
			this.serverNo = serverNo;
		}
		
		public String getLotNo() {
			return lotId;
		}
		public void setLotNo( String lotId ) {
			this.lotId = lotId;
		}
		
		public String getLotName() {
			return lotName;
		}
		public void setLotName( String lotName ) {
			this.lotName = lotName;
		}
		
		public String[] getBuildNo() {
			return buildNo;
		}
		public void setBuildNo( String[] buildNo ) {
			this.buildNo = buildNo;
		}
		
		public String[] getVersionTag() {
			return versionTag;
		}
		public void setVersionTag( String[] versionTag ) {
			this.versionTag = versionTag;
		}
		
		public String[] getBuildEndDate() {
			return buildEndDate;
		}
		public void setBuildEndDate( String[] buildEndDate ) {
			this.buildEndDate = buildEndDate;
		}
		
		public List<RelUnitCompileResultViewBean> getRelUnitCompileResultViewBeanList() {
			return relUnitCompileResultViewBeanList;
		}
		public void setRelUnitCompileResultViewBeanList(
					List<RelUnitCompileResultViewBean> relUnitCompileResultViewBeanList ) {
			this.relUnitCompileResultViewBeanList = relUnitCompileResultViewBeanList;
		}
		public boolean isViewLinkEnabled() {
			return isViewLinkEnabled;
		}
		public void setViewLinkEnabled( boolean isViewLinkEnabled ) {
			this.isViewLinkEnabled = isViewLinkEnabled;
		}
		
		public RelUnitCompileResultViewBean newRelUnitCompileResultViewBean() {
			return new RelUnitCompileResultViewBean();
		}
		
		public class RelUnitCompileResultViewBean {
			
			/** モジュール名 */
			private String moduleName = null;
			/** ビルドパッケージ結果 */
			private Boolean[] buildResult = new Boolean[0];
			
			
			public String getModuleName() {
				return moduleName;
			}
			public void setModuleName( String moduleName ) {
				this.moduleName = moduleName;
			}
			
			public Boolean[] getBuildResult() { 
				return buildResult;
			}
			public void setBuildResult( Boolean[] buildResult ) {
				this.buildResult = buildResult;
			}
		}
		
	}
}
