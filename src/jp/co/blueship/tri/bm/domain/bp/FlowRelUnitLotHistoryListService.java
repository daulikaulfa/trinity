package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotHistoryViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitLotHistoryListServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;


/**
 * ビルドパッケージ作成・ロット履歴一覧画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitLotHistoryListService implements IDomain<FlowRelUnitLotHistoryListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitLotHistoryListServiceBean> execute( IServiceDto<FlowRelUnitLotHistoryListServiceBean> serviceDto ) {

		FlowRelUnitLotHistoryListServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			IJdbcCondition condition = DBSearchConditionAddonUtil.getLotConditionByRelUnitLotHistory();
			List<String> disableLinkLotNumbers	= new ArrayList<String>();

			// 旧互換機能のため、当該箇所に問題はない。
			@SuppressWarnings("deprecation")
			boolean isSearch =
				this.support.getAmFinderSupport().setAccessableLotNumbers( condition, paramBean, disableLinkLotNumbers, false );


			IEntityLimit<ILotEntity> entityLimit = null;
			int selectPageNo = 1;

			// アクセス可能なロットが１件もない場合は検索に行かない
			// （ロット番号が指定できないので、全部取ってきてしまうので）
			if ( isSearch ) {
				// 再検索
				ISqlSort sort = DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelUnitLotHistory();
				selectPageNo =
					( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

				entityLimit = this.support.getAmFinderSupport().getLotDao().find(
						condition.getCondition(), sort, selectPageNo,
									sheet.intValue( BmDesignEntryKeyByBuild.maxPageNumberByLotHistoryList ) );
			} else {

				entityLimit = new EntityLimit<ILotEntity>();
				entityLimit.setEntities( FluentList.from(new ILotEntity[0]).asList());
			}

			//ロットの履歴を確認するのに、一件もないかどうかは関係ない
			setServiceBeanSearchResult( entityLimit, selectPageNo, paramBean, disableLinkLotNumbers );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005087S,e);
		}
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param entityLimit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean  FlowRelUnitLotHistoryListServiceBeanオブジェクト
	 * @param disableLinkLotNumbers 一覧表示のみのロット番号
	 */
	private void setServiceBeanSearchResult(
			IEntityLimit<ILotEntity> entityLimit, int selectPageNo,
			FlowRelUnitLotHistoryListServiceBean paramBean, List<String> disableLinkLotNumbers ) {

		List<LotHistoryViewBean> lotHistoryViewBeanList = new ArrayList<LotHistoryViewBean>();

		IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();
		String serverId = srvEntity.getBldSrvId();

		for ( ILotEntity entity : entityLimit.getEntities() ) {

			LotHistoryViewBean viewBean = new LotHistoryViewBean();

			viewBean.setServerNo	( serverId );

			viewBean.setLotNo		( entity.getLotId() );
			viewBean.setLotName		( entity.getLotNm() );
			viewBean.setLotSummary	( entity.getSummary() );
			viewBean.setLotStatus	(
				sheet.getValue( AmDesignBeanId.statusId, entity.getProcStsId() ));

			if ( disableLinkLotNumbers.contains( viewBean.getLotNo() )) {
				viewBean.setViewLinkEnabled( false );
			} else {
				viewBean.setViewLinkEnabled( true );
			}

			lotHistoryViewBeanList.add( viewBean );
		}

		paramBean.setLotHistoryViewBeanList	( lotHistoryViewBeanList );
		paramBean.setPageInfoView				(
				BmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), entityLimit.getLimit() ));
		paramBean.setSelectPageNo				( selectPageNo );

	}
}
