package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.beans.mail.dto.RelUnitMailServiceBean;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.MailGenericService;


/**
 * ビルドパッケージ作成・ビルドパッケージ取消受付完了時のメール送信情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitCancelServiceMail implements IDomain<FlowRelUnitCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();


	private MailGenericService successMail = null;
	private FlowRelUnitEditSupport support = null;

	public void setSuccessMail( MailGenericService successMail ) {
		this.successMail = successMail;
	}
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCancelServiceBean> execute( IServiceDto<FlowRelUnitCancelServiceBean> serviceDto ) {

		FlowRelUnitCancelServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String forwordID = paramBean.getForward();

			if ( forwordID.equals( RelUnitScreenID.COMP_UNIT_CANCEL )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {


					//基礎情報のコピー
					RelUnitMailServiceBean successMailBean = new RelUnitMailServiceBean();
					TriPropertyUtils.copyProperties( successMailBean, paramBean );
					IServiceDto<IGeneralServiceBean> mailServiceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean(successMailBean);

					//選択されたビルド番号
					String[] buildNos = paramBean.getSelectedUnitNo();
					BpCondition bpCondition = new BpCondition();
					bpCondition.setBpIds( buildNos );
					bpCondition.setDelStsId( null );

					List<IBpDto> bpDtoList = this.support.findBpDto( support.getBpDao().find(bpCondition.getCondition()) );

					//ロットエンティティ
					String lotId =  bpDtoList.get(0).getBpEntity().getLotId();
					ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity(lotId);


					// ビルドパッケージ情報
					List<Map<String,Object>> packageData = new ArrayList<Map<String,Object>>();

					for ( IBpDto bpDto : bpDtoList ) {

						String buildNo = bpDto.getBpEntity().getBpId();
						List<IBpAreqLnkEntity> bpAreqEntities = bpDto.getBpAreqLnkEntities();

						//枠
		 				List<Map<String,Object>> pjtData = new ArrayList<Map<String,Object>>();
						Map<String,List<String>> pjtApplyMap = new HashMap<String,List<String>>();
						//重複確認用
						Map<String,String> dupChkMap = new HashMap<String,String>();

						for ( IBpAreqLnkEntity bpAreqEntity : bpAreqEntities ) {

							String pjtNo			= this.support.getAmFinderSupport().findAreqEntity(bpAreqEntity.getAreqId()).getPjtId();
							String changeCauseNo 	= this.support.getAmFinderSupport().findPjtEntity( pjtNo ).getChgFactorNo();
							String applyNo			= bpAreqEntity.getAreqId();

							//pjtApplyMap作成
							if ( !pjtApplyMap.containsKey( pjtNo )) {
								List<String> list = new ArrayList<String>();
								pjtApplyMap.put(pjtNo, list);
							}
							pjtApplyMap.get( pjtNo ).add( applyNo );

							//
							if( !dupChkMap.containsKey(pjtNo) ){
								dupChkMap.put(pjtNo, "");

								Map<String,Object> pjtMap = new HashMap<String,Object>();
								pjtMap.put("pjtId", pjtNo);
								pjtMap.put("chgFactorNo", changeCauseNo);
								pjtMap.put("applyList", pjtApplyMap.get(pjtNo));
								pjtData.add(pjtMap);
							}
						}

						Map<String,Object> packageMap = new HashMap<String,Object>();
						packageMap.put( "bpId", buildNo );
						packageMap.put( "pjtList", pjtData );

						packageData.add( packageMap );
					}

					bpDtoList.get(0).getBpEntity().setContent(paramBean.getUnitInfoInputBean().getCancelComment());
					successMailBean.setBpEntity		( bpDtoList.get(0).getBpEntity() );
					successMailBean.setLotEntity	( pjtLotEntity );
					successMailBean.setPackageList	( packageData );

					successMail.execute( mailServiceDto );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , new TriSystemException( BmMessageId.BM005068S, e , paramBean.getFlowAction() ) ) ;
		}

		return serviceDto;
	}
}
