package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseLotViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelTopServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByBuild;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * ビルドパッケージ作成・パッケージ取消トップ画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitCancelTopService implements IDomain<FlowRelUnitCancelTopServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCancelTopServiceBean> execute( IServiceDto<FlowRelUnitCancelTopServiceBean> serviceDto ) {

		FlowRelUnitCancelTopServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			// 取消可能なビルドパッケージを持つロットのロット番号
			List<String> disableLinkLotNumbers	= new ArrayList<String>();
			String[] lotNoArray = getLotNoWithCancelableBuildEntity( paramBean, disableLinkLotNumbers );

			IEntityLimit<ILotEntity> entityLimit = null;
			int selectPageNo = 1;

			// アクセス可能なロットが１件もない場合は検索に行かない
			// （ロット番号が指定できないので、全部取ってきてしまうので）
			if ( ! TriStringUtils.isEmpty( lotNoArray ) ) {
				// 再検索
				IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotNoArray );
				ISqlSort sort = DBSearchSortAddonUtil.getPjtLotSelectSortFromDesignDefineByRelUnitCancel();
				selectPageNo =
					( 0 == paramBean.getSelectPageNo() )? 1: paramBean.getSelectPageNo();

				entityLimit =
					this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort, selectPageNo,
									sheet.intValue( BmDesignEntryKeyByBuild.maxPageNumberByUnitCancelTop ) );
			} else {

				entityLimit = new EntityLimit<ILotEntity>();
				entityLimit.setEntities( FluentList.from(new ILotEntity[0]).asList() );
			}

			setServiceBeanSearchResult( entityLimit, selectPageNo, paramBean, disableLinkLotNumbers );

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005073S,e);
		}
	}

	/**
	 * 取消可能なビルドパッケージを持つロット情報のロット番号を取得する
	 * @param paramBean
	 * @param disableLinkLotNumbers 一覧表示のみ可能ロット番号
 * @return 取消可能なビルドパッケージを持つロット情報のロット番号の配列
	 */

	private String[] getLotNoWithCancelableBuildEntity(
			FlowRelUnitCancelTopServiceBean paramBean, List<String> disableLinkLotNumbers ) {

		// 活動中の全ロットを取得
		List<ILotEntity> accessableLot = null;
		{
			LotCondition lotCondition = (LotCondition)DBSearchConditionAddonUtil.getActiveLotCondition();

			accessableLot =
				this.support.getAmFinderSupport().getAccessableLotNumbers( lotCondition, null, paramBean, true, disableLinkLotNumbers, false );
		}

		IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByCancelableRelUnit(  this.support.getAmFinderSupport().convertToLotNo(accessableLot)  );

		IEntityLimit<IBpEntity>  entityLimit =
								this.support.getBpDao().find( condition.getCondition(), null, 1, 0 );

		if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() )) {
			if ( TriStringUtils.isEmpty( entityLimit.getEntities() )) {
				paramBean.setInfoMessage(BmMessageId.BM001017E);
			}
		}

		Set<String> lotNoSet = new TreeSet<String>();
		for ( IBpEntity entity : entityLimit.getEntities() ) {
			lotNoSet.add( entity.getLotId() );
		}

		return (String[])lotNoSet.toArray( new String[0] );
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param entityLimit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param selectPageNo 選択ページ番号
	 * @param paramBean  FlowRelUnitCancelTopServiceBean
	 * @param disableLinkLotNumbers 一覧表示のみのロット番号
	 */

	private void setServiceBeanSearchResult(
			IEntityLimit<ILotEntity> entityLimit, int selectPageNo,
			FlowRelUnitCancelTopServiceBean paramBean,
			List<String> disableLinkLotNumbers ) {

		List<UnitCloseLotViewBean> lotViewBeanList = new ArrayList<UnitCloseLotViewBean>();

		IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();
		List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( entityLimit.getEntities() );

		BmViewInfoAddonUtils.setUnitCloseLotViewBeanPjtLotEntity(
				lotViewBeanList, lotDto, disableLinkLotNumbers, srvEntity.getBldSrvId() );

		if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() )) {
			if ( !TriStringUtils.isEmpty( entityLimit.getEntities() ) && TriStringUtils.isEmpty( lotViewBeanList )) {
				paramBean.setInfoMessage( BmMessageId.BM001028E );
			}
		}

		paramBean.setLotViewBeanList	( lotViewBeanList );
		paramBean.setPageInfoView				(
				BmDesignBusinessRuleUtils.convertPageNoInfo( new PageNoInfo(), entityLimit.getLimit() ));
		paramBean.setSelectPageNo				( selectPageNo );

	}
}
