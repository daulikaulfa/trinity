package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.util.List;

/**
 * ビルドパッケージ作成・ビルドパッケージ作成確認画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitComfirmBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
	/** ロット情報 */
	private LotViewRelUnitBean lotViewBean = new LotViewRelUnitBean();
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 変更管理情報(クローズ済み) */
	private List<PjtViewBean> closedPjtViewBeanList = null;
//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	
	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}
	
	public LotViewRelUnitBean getLotViewBean() {
		return lotViewBean;
	}
	public void setLotViewBean( LotViewRelUnitBean lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}
	
	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}
	
	public List<PjtViewBean> getClosedPjtViewBeanList() {
		return closedPjtViewBeanList;
	}
	public void setClosedPjtViewBeanList( List<PjtViewBean> closedPjtViewBeanList ) {
		this.closedPjtViewBeanList = closedPjtViewBeanList;
	}
	
//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
}
