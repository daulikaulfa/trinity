package jp.co.blueship.tri.bm.domain.bp.beans.dto;

/**
 * ビルドパッケージ検索条件用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitSearchConditionBean {

	/** リリース番号 */
	private String inRelNo = null;
	/** リリース環境名 */
	private String inEnvName = null;
	/** リリース環境番号 */
	private String inEnvNo = null;
	/** ロット番号 */
	private String inLotNo = null;
	
	public String getInEnvName() {
		return inEnvName;
	}
	public void setInEnvName(String inEnvName) {
		this.inEnvName = inEnvName;
	}
	public String getInEnvNo() {
		return inEnvNo;
	}
	public void setInEnvNo(String inEnvNo) {
		this.inEnvNo = inEnvNo;
	}
	public String getInLotNo() {
		return inLotNo;
	}
	public void setInLotNo(String inLotNo) {
		this.inLotNo = inLotNo;
	}
	public String getInRelNo() {
		return inRelNo;
	}
	public void setInRelNo(String inRelNo) {
		this.inRelNo = inRelNo;
	}

	

}
