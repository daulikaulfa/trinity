package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotSelectBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotViewRelUnitBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;


/**
 * ビルドパッケージ作成・ロット選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitEntryServiceLotSelect implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if( !refererID.equals( RelUnitScreenID.LOT_SELECT ) &&
					!forwordID.equals( RelUnitScreenID.LOT_SELECT ) ){
				return serviceDto;
			}

			//初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;

			if( refererID.equals( RelUnitScreenID.LOT_SELECT )) {

				String selectedLotNo = paramBean.getSelectedLotNo();
				String selectedServerNo = paramBean.getSelectedServerNo();

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( RelUnitScreenID.LOT_SELECT )) {

					ItemCheckAddonUtils.checkLotNo( selectedLotNo );

					// グループの存在チェック
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( selectedLotNo );
//					IGroupUserView[] groupUsers	= this.support.getGroupUserDao().find( paramBean.getUserId() );
					List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				}
				//paramBean.setSelectedLotNo( selectedLotNo );
				paramBean.setSelectedServerNo( selectedServerNo );
			}

			if( forwordID.equals( RelUnitScreenID.LOT_SELECT )) {
				if ( ScreenType.next.equals( screenType ) ||
						ScreenType.select.equals( screenType ) ) {

					String[] lotId = null ;
					if( StatusFlg.off.value().equals( paramBean.getAllAssetCompile() ) ) {//差分ビルド
						// 「ビルドパッケージ作成可能な返却申請、削除申請を持つ変更管理」を持つロット
//						String[] pjtNo = this.getPjtNoByGeneratableRelUnit();
//						lotNo = this.getLotNo( pjtNo );
						lotId = null; //パフォーマンスチューニング
					} else {//全量ビルド
						lotId = null; //全ロットが対象
					}

					IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( lotId );
					ISqlSort sort			= DBSearchSortAddonUtil.getPjtLotSortFromDesignDefineByRelUnitEntityLotSelect();

					LotSelectBean lotSelectBean = paramBean.getLotSelectBean();

					int selectedPageNo	= 1;
					int maxPageNumber	= 0;

					IEntityLimit<ILotEntity> pjtLotEntityLimit =
						this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );

					if ( TriStringUtils.isEmpty( pjtLotEntityLimit.getEntities() )) {
						throw new BusinessException( BmMessageId.BM001000E );
					}
					IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();
					List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( pjtLotEntityLimit.getEntities() );

					List<IGrpUserLnkEntity> groupUserEntitys	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					List<LotViewRelUnitBean> lotViewBeanList = new ArrayList<LotViewRelUnitBean>();

					BmViewInfoAddonUtils.setLotViewRelUnitBeanPjtLotEntity(
							lotViewBeanList, lotDto, groupUserEntitys, srvEntity.getBldSrvId() );

					if ( TriStringUtils.isEmpty( lotViewBeanList )) {
						throw new BusinessException( BmMessageId.BM001028E );
					}

					lotSelectBean.setLotViewBeanList	( lotViewBeanList );
	//				lotSelectBean.setPageInfoView		(
	//						AddonUtil.convertPageNoInfo( new PageNoInfo(), pjtLotEntityLimit.getLimit() ));
	//				lotSelectBean.setSelectPageNo		( selectedPageNo );

					paramBean.setLotSelectBean( lotSelectBean );
				}

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005081S,e);
		}
	}

}
