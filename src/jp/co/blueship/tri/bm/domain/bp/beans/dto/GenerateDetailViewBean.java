package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.util.List;

/**
 * ビルドパッケージ作成・ビルドパッケージ工程用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class GenerateDetailViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** ビルドパッケージ番号 */
	private String buildNo = null;
	/** ビルドパッケージ実行者 */
	private String executeUser = null;
	/** ビルドパッケージ実行者ＩＤ */
	private String executeUserId = null;
	/** ビルドパッケージ状況メッセージ */
	private String buildStatusMsg = null;
	/** 最大カラム数 */
	private String maxColumns = null;
	/** ビルドパッケージ状況 */
	private List<BuildProcessViewBean> buildProcessViewBeanList = null;
	
	
	public String getBuildNo() {
		return buildNo;
	}
	public void setBuildNo(String buildNo) {
		this.buildNo = buildNo;
	}
	public List<BuildProcessViewBean> getBuildProcessViewBeanList() {
		return buildProcessViewBeanList;
	}
	public void setBuildProcessViewBeanList(
			List<BuildProcessViewBean> buildProcessViewBeanList) {
		this.buildProcessViewBeanList = buildProcessViewBeanList;
	}
	public String getBuildStatusMsg() {
		return buildStatusMsg;
	}
	public void setBuildStatusMsg(String buildStatusMsg) {
		this.buildStatusMsg = buildStatusMsg;
	}
	public String getExecuteUser() {
		return executeUser;
	}
	public void setExecuteUser(String executeUser) {
		this.executeUser = executeUser;
	}
	public String getMaxColumns() {
		return maxColumns;
	}
	public void setMaxColumns(String maxColumns) {
		this.maxColumns = maxColumns;
	}

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public BuildProcessViewBean newBuildProcessViewBean() {
		BuildProcessViewBean bean = new BuildProcessViewBean();
		return bean;
	}

	public class BuildProcessViewBean implements Serializable {

		private static final long serialVersionUID = 1L;
		
		
		/** リソースパス */
		private String img[] = null;
		/** メッセージ */
		private String msg[] = null;
		
		private String active[] = null ;
		
		public String[] getImg() {
			return img;
		}
		public void setImg(String[] img) {
			this.img = img;
		}
		public String[] getMsg() {
			return msg;
		}
		public void setMsg(String[] msg) {
			this.msg = msg;
		}
		public String[] getActive() {
			return active;
		}
		public void setActive(String[] active) {
			this.active = active;
		}
		
		
	}

	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}
	
}
