package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtSelectBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport.PjtNoBean;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;


/**
 * ビルドパッケージ作成・変更管理選択画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitEntryServicePjtSelect implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelUnitScreenID.PJT_SELECT ) &&
					!forwordID.equals( RelUnitScreenID.PJT_SELECT ) ){
				return serviceDto;
			}

			//初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;

			String selectedLotNo = paramBean.getSelectedLotNo();

			if( refererID.equals( RelUnitScreenID.PJT_SELECT )) {

				String[] pjtNo = this.support.mergePjtNo( paramBean , true );

				if ( ScreenType.next.equals( screenType ) ) {

					ItemCheckAddonUtils.checkPjtNo( pjtNo );

					//this.support.checkPjtGenerateRelUnit( pjtNo );

				}

				if ( ! TriStringUtils.isEmpty( pjtNo ) ) {
					PjtNoBean pjtNoBean = this.support.getPjtNoBean( paramBean );
					paramBean.setApplyInfoViewBeanList(
							this.support.getApplyInfoViewBeanList( selectedLotNo, pjtNo, pjtNoBean ));
				}

			}


			if( forwordID.equals( RelUnitScreenID.PJT_SELECT )) {
				if ( ! ScreenType.bussinessException.equals( screenType ) ) {
					ItemCheckAddonUtils.checkLotNo( selectedLotNo );
				}

				if ( ! TriStringUtils.isEmpty( selectedLotNo ) ) {
					// 指定されたロット番号を持ち、ビルドパッケージ作成可能な返却申請、削除申請を持つ変更管理
					PjtNoBean pjtNoBean = this.support.getPjtNoByGeneratableRelUnit( selectedLotNo );

					Set<String> pjtIdSet = new TreeSet<String>();
					pjtIdSet.addAll( pjtNoBean.getApprovedPjtNo() );
					pjtIdSet.addAll( pjtNoBean.getClosedPjtNo() );

					IJdbcCondition condition =
						DBSearchConditionAddonUtil.getPjtConditionByGenerateRelUnit( (String[])pjtIdSet.toArray( new String[0] ) );

					ISqlSort sort = DBSearchSortAddonUtil.getPjtSortFromDesignDefineByRelUnitEntryPjtSelect();

					PjtSelectBean pjtSelectBean = paramBean.getPjtSelectBean();

	//				int selectedPageNo	=
	//					(( 0 == pjtSelectBean.getSelectPageNo() )? 1: pjtSelectBean.getSelectPageNo() );
	//				int maxPageNumber	=
	//					DesignProjectUtil.getMaxPageNumber(
	//						DesignProjectRelDefineId.maxPageNumberByPjtList );

					int selectedPageNo	= 1;
					int maxPageNumber	= 0;

					IEntityLimit<IPjtEntity> limit =
						this.support.getAmFinderSupport().getPjtDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );

					if ( limit.getEntities().size() == 0 && paramBean.isStatusMatrixV3() ) {
						throw new BusinessException( BmMessageId.BM001002E );
					}
					
					List<PjtViewBean> closedPjtViewBeanList	= new ArrayList<PjtViewBean>();
					List<PjtViewBean> pjtViewBeanList		= new ArrayList<PjtViewBean>();

					this.support.setPjtViewBean(
							limit.getEntities().toArray(new IPjtEntity[0]), pjtNoBean, closedPjtViewBeanList, pjtViewBeanList );

					pjtSelectBean.setClosedPjtViewBeanList	( closedPjtViewBeanList );
					pjtSelectBean.setPjtViewBeanList		( pjtViewBeanList );

	//				pjtSelectBean.setPageInfoView		(
	//						AddonUtil.convertPageNoInfo( new PageNoInfo(), pjtEntityLimit.getLimit() ));
	//				pjtSelectBean.setSelectPageNo		( selectedPageNo );

					paramBean.setPjtSelectBean( pjtSelectBean );
				}
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005082S,e);
		}
	}
}
