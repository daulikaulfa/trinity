package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitHistoryViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * ビルドパッケージ作成・履歴一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitHistoryListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** 選択されたロット番号 */	
	private String selectedLotNo = null;
	/** ロット名 */	
	private String lotName = null;
	/** ビルドパッケージ情報 */
	private List<UnitHistoryViewBean> unitHistoryViewBeanList = null;
	/** 検索条件情報 */
	private RelUnitSearchBean relUnitSearchBean = null;
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}
	
	public List<UnitHistoryViewBean> getUnitHistoryViewBeanList() {
		if ( null == unitHistoryViewBeanList ) {
			unitHistoryViewBeanList = new ArrayList<UnitHistoryViewBean>();
		}
		return unitHistoryViewBeanList;
	}
	public void setUnitHistoryViewBeanList( List<UnitHistoryViewBean> unitHistoryViewBeanList ) {
		this.unitHistoryViewBeanList = unitHistoryViewBeanList;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	public RelUnitSearchBean getRelUnitSearchBean() {
		return relUnitSearchBean;
	}
	public void setRelUnitSearchBean(RelUnitSearchBean relUnitSearchBean) {
		this.relUnitSearchBean = relUnitSearchBean;
	}
	
}
