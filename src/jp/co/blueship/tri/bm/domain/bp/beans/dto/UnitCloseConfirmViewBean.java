package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * ビルドパッケージクローズ・クローズ確認用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitCloseConfirmViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ビルドパッケージ番号 */
	private String unitNo = null;
	/** 変更管理情報 */
	private List<UnitClosePjtViewBean> unitClosePjtViewBeanList = new ArrayList<UnitClosePjtViewBean>();

	public String getUnitNo() {
		return unitNo;
	}
	public void setUnitNo( String unitNo ) {
		this.unitNo = unitNo;
	}
	
	public List<UnitClosePjtViewBean> getUnitClosePjtViewBeanList() {
		return unitClosePjtViewBeanList;
	}
	public void setUnitClosePjtViewBeanList( List<UnitClosePjtViewBean> unitClosePjtViewBeanList ) {
		this.unitClosePjtViewBeanList = unitClosePjtViewBeanList;
	}
	
	
	public UnitClosePjtViewBean newUnitClosePjtViewBean() {
		return new UnitClosePjtViewBean();
	}
	
	public class UnitClosePjtViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/** 変更管理番号 */
		private String pjtNo = null;
		/** 変更要因番号 */
		private String changeCauseNo = null;
		/** 申請番号 */
		private String applyNo = null;
		/** 申請件名 */
		private String applySummary = null;
		/** 申請承認者 */
		private String applyApproveUser = null;
		/** 申請承認者ＩＤ */
		private String applyApproveUserId = null;
		/** 申請ステータス */
		private String applyStatus = null;
		
		
		public String getPjtNo() {
			return pjtNo;
		}
		public void setPjtNo( String pjtNo ) {
			this.pjtNo = pjtNo;
		}

		public String getChangeCauseNo() {
			return changeCauseNo;
		}
		public void setChangeCauseNo( String changeCauseNo ) {
			this.changeCauseNo = changeCauseNo;
		}

		public String getApplyNo() {
			return applyNo;
		}
		public void setApplyNo( String applyNo ) {
			this.applyNo = applyNo;
		}

		public String getApplySummary() {
			return applySummary;
		}
		public void setApplySummary( String applySummary ) {
			this.applySummary = applySummary;
		}
		
		public String getApplyApproveUser() {
			return applyApproveUser;
		}
		public void setApplyApproveUser( String applyApproveUser ) {
			this.applyApproveUser = applyApproveUser;
		}
		
		public String getApplyStatus() {
			return applyStatus;
		}
		public void setApplyStatus( String applyStatus ) {
			this.applyStatus = applyStatus;
		}
		public String getApplyApproveUserId() {
			return applyApproveUserId;
		}
		public void setApplyApproveUserId(String applyApproveUserId) {
			this.applyApproveUserId = applyApproveUserId;
		}
	}
}
