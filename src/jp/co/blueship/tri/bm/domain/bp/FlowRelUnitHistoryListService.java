package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitHistoryViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryListServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージ履歴一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitHistoryListService implements IDomain<FlowRelUnitHistoryListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitHistoryListServiceBean> execute( IServiceDto<FlowRelUnitHistoryListServiceBean> serviceDto ) {

		FlowRelUnitHistoryListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			ItemCheckAddonUtils.checkLotNo( selectedLotNo );

			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( selectedLotNo );
			paramBean.setLotName( lotDto.getLotEntity().getLotNm() );

			// グループの存在チェック
			List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
			RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );


			paramBean.setRelUnitSearchBean(
					DBSearchBeanAddonUtil.setRelUnitSearchBean(
							paramBean.getRelUnitSearchBean(), BmDesignBeanId.bpHistoryListCount ) );

			IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByLotNo( selectedLotNo );

			int buildCount = this.support.getBpDao().count( condition.getCondition() );

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				if ( buildCount == 0 ) {
					throw new BusinessException( BmMessageId.BM001003E );
				}
			}

			this.support.setBuildCondition( (BpCondition)condition, paramBean.getRelUnitSearchBean() );

			ISqlSort sort = (ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelUnitHistory();

			IEntityLimit<IBpEntity> entityLimit = this.support.getBpDao().find( condition.getCondition(), sort, 1, 0 ) ;
			List<IBpDto> bpDtoList = this.support.findBpDtoList(entityLimit.getEntities());

			List<IBpDto> buildEntityList = this.support.applySearchCondition( bpDtoList, paramBean.getRelUnitSearchBean() );

			List<UnitHistoryViewBean> unitHistoryViewBeanList = this.support.getUnitHistoryViewBeanList( buildEntityList.toArray( new IBpDto[0] ) );

			{
				RelUnitSearchBean relUnitSearchBean = paramBean.getRelUnitSearchBean();
				int count = 0;
				int maxCount = ( null == relUnitSearchBean.getSelectedRelUnitCount() )? 0: Integer.parseInt(relUnitSearchBean.getSelectedRelUnitCount());

				//外部結合検索のため、全体件数を予測できないため、全体件数を指定せずに、ロジックで絞込み。
				//このケースでは、ページ制御を一覧でのみ実現可能。
				List<UnitHistoryViewBean> newViewBeanList = new ArrayList<UnitHistoryViewBean>();
				for ( UnitHistoryViewBean bean : unitHistoryViewBeanList ) {
					count++;

					if ( 0 != maxCount && count > maxCount )
						break;

					newViewBeanList.add( bean );
				}

				paramBean.setUnitHistoryViewBeanList( newViewBeanList );
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005086S , e );
		}
	}
}
