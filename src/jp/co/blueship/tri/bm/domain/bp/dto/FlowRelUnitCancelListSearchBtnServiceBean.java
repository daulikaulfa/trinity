package jp.co.blueship.tri.bm.domain.bp.dto;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitSearchConditionBean;

/**
 * 一覧画面から検索を行うフロー
 *
 */
public class FlowRelUnitCancelListSearchBtnServiceBean extends FlowRelUnitCancelListServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 検索条件 */
	private UnitSearchConditionBean unitSearchConditionBean = null;
	
	
	public UnitSearchConditionBean getUnitSearchConditionBean() {
		return unitSearchConditionBean;
	}
	public void setUnitSearchConditionBean( UnitSearchConditionBean unitSearchConditionBean ) {
		this.unitSearchConditionBean = unitSearchConditionBean;
	}
}

