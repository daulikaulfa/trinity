package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.util.List;

/**
 * ビルドパッケージ作成・変更管理選択画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class PjtSelectBean implements Serializable {

	private static final long serialVersionUID = 1L;

//	/** 選択ページ */
//	private int selectPageNo = 0;
//	/** ページ制御 */
//	private IPageNoInfo pageInfoView = null;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 変更管理情報(クローズ済み) */
	private List<PjtViewBean> closedPjtViewBeanList = null;
	
	
	public List<PjtViewBean> getPjtViewBeanList() {
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList( List<PjtViewBean> pjtViewBeanList ) {
		this.pjtViewBeanList = pjtViewBeanList;
	}
	
	public List<PjtViewBean> getClosedPjtViewBeanList() {
		return closedPjtViewBeanList;
	}
	public void setClosedPjtViewBeanList( List<PjtViewBean> closedPjtViewBeanList ) {
		this.closedPjtViewBeanList = closedPjtViewBeanList;
	}
	
//	public IPageNoInfo getPageInfoView() {
//		return pageInfoView;
//	}
//	public void setPageInfoView(IPageNoInfo pageInfoView) {
//		this.pageInfoView = pageInfoView;
//	}
//
//	public int getSelectPageNo() {
//		return selectPageNo;
//	}
//	public void setSelectPageNo(int selectPageNo) {
//		this.selectPageNo = selectPageNo;
//	}
	
}
