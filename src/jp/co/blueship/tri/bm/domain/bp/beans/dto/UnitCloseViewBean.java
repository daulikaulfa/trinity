package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * ビルドパッケージクローズ・ビルドパッケージ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class UnitCloseViewBean implements Serializable {

	private static final long serialVersionUID = 1L;
	
	/** 
	 * ビルドパッケージ番号 
	 */
	private String relUnitNo = null;
	/** 
	 * ビルドパッケージ名 
	 */
	private String relUnitName = null;
	/** 
	 * リリース番号 
	 */
	private List<String> relNoList = new ArrayList<String>();
	/** 
	 * ビルド環境名 
	 */
	private List<String> envNameList = new ArrayList<String>();
	/** 
	 * ステータス 
	 */
	private String relUnitStatus = null;
	
	private String createBy = null;
	
	private String createById = null;
	
	private Timestamp createDate = null;

	public String getRelUnitNo() {
		return relUnitNo;
	}
	public void setRelUnitNo(String relUnitNo) {
		this.relUnitNo = relUnitNo;
	}
	
	public String getRelUnitName() {
		return relUnitName;
	}
	public void setRelUnitName(String relUnitName) {
		this.relUnitName = relUnitName;
	}

	public List<String> getRelNoList() {
		return relNoList;
	}
	public void setRelNoList( List<String> relNoList ) {
		this.relNoList = relNoList;
	}
	
	public List<String> getEnvNameList() {
		return envNameList;
	}
	public void setEnvNameList( List<String> envNameList ) {
		this.envNameList = envNameList;
	}
	
	public String getRelUnitStatus() {
		return relUnitStatus;
	}
	public void setRelUnitStatus(String relUnitStatus) {
		this.relUnitStatus = relUnitStatus;
	}
	
	public String getCreateBy() {
		return createBy;
	}
	
	public void setCreateBy(String createBy) {
		this.createBy = createBy;
	}
	
	public String getCreateById() {
		return createById;
	}
	
	public void setCreateById(String createById) {
		this.createById = createById;
	}
	
	public Timestamp getCreateDate() {
		return createDate;
	}
	
	public void setCreateDate(Timestamp createDate) {
		this.createDate = createDate;
	}
}
