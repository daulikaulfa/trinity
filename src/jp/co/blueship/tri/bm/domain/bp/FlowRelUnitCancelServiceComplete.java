package jp.co.blueship.tri.bm.domain.bp;

import java.sql.Timestamp;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージ取消受付完了画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitCancelServiceComplete implements IDomain<FlowRelUnitCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCancelServiceBean> execute( IServiceDto<FlowRelUnitCancelServiceBean> serviceDto ) {

		FlowRelUnitCancelServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelUnitScreenID.COMP_UNIT_CANCEL ) &&
					!forwordID.equals( RelUnitScreenID.COMP_UNIT_CANCEL )) {
				return serviceDto;
			}

			if ( refererID.equals( RelUnitScreenID.COMP_UNIT_CANCEL )) {

			}

			if ( forwordID.equals( RelUnitScreenID.COMP_UNIT_CANCEL )) {

				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {

					String[] buildNo = paramBean.getSelectedUnitNo();
					ItemCheckAddonUtils.checkBuildNo( buildNo );


					UnitInfoInputBean unitInfoInputBean = paramBean.getUnitInfoInputBean();

					List<IBpDto> bpDtoList = this.support.findBpDto( buildNo );
					String userName		= paramBean.getUserName();
					String userId		= paramBean.getUserId();

					// ビルドパッケージ情報の更新
					updateBuildEntityToCancel( bpDtoList, unitInfoInputBean, userName, userId );


					// グループの存在チェック
					ILotDto dto = this.support.getAmFinderSupport().findLotDto( bpDtoList.get(0).getBpEntity().getLotId() );
					List<IGrpUserLnkEntity> groupUsers	= this.support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( dto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005071S,e);
		}
	}

	/**
	 * ビルドパッケージ情報を取消に更新する
	 * @param bpDtoList ビルドパッケージエンティティ
	 * @param unitInfoInputBean 編集情報
	 * @param userName ユーザ名
	 * @param systemDate 更新日時
	 */
	private void updateBuildEntityToCancel(
			List<IBpDto> bpDtoList, UnitInfoInputBean unitInfoInputBean,
			String userName, String userId ) {
		Timestamp systemTimestamp = TriDateUtils.getSystemTimestamp();

		for ( IBpDto bpDto : bpDtoList ) {

			bpDto.getBpEntity().setDelCmt			( unitInfoInputBean.getCancelComment() );
			bpDto.getBpEntity().setDelTimestamp		( systemTimestamp );
			bpDto.getBpEntity().setDelUserNm		( userName );
			bpDto.getBpEntity().setDelUserId		( userId );
			bpDto.getBpEntity().setDelStsId			( StatusFlg.on );
			bpDto.getBpEntity().setStsId			( BmBpStatusId.BuildPackageRemoved.getStatusId() );
			bpDto.getBpEntity().setUpdTimestamp		( systemTimestamp );
			bpDto.getBpEntity().setUpdUserNm		( userName );
			bpDto.getBpEntity().setUpdUserId		( userId );

			this.support.getBpDao().update( bpDto.getBpEntity() );

			// bm_bp_mdl_lnk 論理削除
			BpMdlLnkCondition mdlCondition = new BpMdlLnkCondition();
			mdlCondition.setBpId( bpDto.getBpEntity().getBpId() );
			IBpMdlLnkEntity mdlEntity = new BpMdlLnkEntity();
			mdlEntity.setDelStsId( StatusFlg.on );
			this.support.getBpMdlLnkDao().update( mdlCondition.getCondition(), mdlEntity );

			// bm_bp_areq_lnk 論理削除
			BpAreqLnkCondition areqCondition = new BpAreqLnkCondition();
			areqCondition.setBpId( bpDto.getBpEntity().getBpId() );
			IBpAreqLnkEntity areqEntity = new BpAreqLnkEntity();
			areqEntity.setDelStsId( StatusFlg.on );
			this.support.getBpAreqLnkDao().update( areqCondition.getCondition(), areqEntity );

			if ( DesignSheetUtils.isRecord() ) {

				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts( UmActStatusId.Remove.getStatusId() );

				support.getBpHistDao().insert( histEntity , bpDto);
			}

			this.support.getUmFinderSupport().cleaningCtgLnk(BmTables.BM_BP, bpDto.getBpEntity().getBpId());
			this.support.getUmFinderSupport().cleaningMstoneLnk(BmTables.BM_BP, bpDto.getBpEntity().getBpId());
		}
	}
}
