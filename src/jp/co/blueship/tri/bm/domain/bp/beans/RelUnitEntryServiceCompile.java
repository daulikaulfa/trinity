package jp.co.blueship.tri.bm.domain.bp.beans;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArrayList;

import jp.co.blueship.tri.agent.bm.svc.flow.IBuildTaskService;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.BuildTaskBean;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.cmn.utils.TaskImageResourceCache;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDtoList;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.TaskFinderSupport;


/**
 * <p>
 * ビルドパッケージの作成を行う。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class RelUnitEntryServiceCompile implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;

	private FlowRelUnitEditSupport support = null;
	private TaskFinderSupport taskSupport = null;
	private IBuildTaskService buildTaskService = null;
	private List<IDomain<IGeneralServiceBean>> actions = new ArrayList<IDomain<IGeneralServiceBean>>();

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	public void setTaskSupport(TaskFinderSupport taskSupport) {
		this.taskSupport = taskSupport;
	}

	public void setBuildTaskService( IBuildTaskService buildTaskService ) {
		this.buildTaskService = buildTaskService;
	}

	public void setActions(List<IDomain<IGeneralServiceBean>> actions) {
		this.actions = actions;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean = serviceDto.getServiceBean();

		try {
//			IBpEntity bpEntity	= null;

			try {

				String buildNo = paramBean.getBuildNo();
				ItemCheckAddonUtils.checkBuildNo( buildNo );

				IBpDto bpDto = this.support.findBpDto(buildNo);
				ILotEntity pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( bpDto.getBpEntity().getLotId() );

				String envNo = bpDto.getBpEntity().getBldEnvId();

				IBldEnvEntity bldEnvEntity = this.support.findBldEnvEntity( envNo );
				IBldEnvSrvEntity[] bldEnvSrvEntitys = this.support.getBldEnvSrvEntity( envNo );

				// ビルドパッケージ対象の申請情報
				AreqDtoList areqDtoEntities = new AreqDtoList();
				IAreqEntity[] assetApplyEntities =
					setAssetApplyEntity( paramBean.getApplyInfoViewBeanList() );

				if( StatusFlg.on.value().equals( paramBean.getAllAssetCompile() ) ) {//全量
					assetApplyEntities = new IAreqEntity[ 0 ] ;
				} else {
					//2011/12/06
					//ＲＰクローズ済みの申請情報は、ここで取り除く。
					//ＲＰクローズ済みの申請情報があると、削除→復活した資産が削除されてしまう場合がある。
					//また、masterDeletePathが肥大化(RPレポートのセルにmasterDeletePathが32768文字を超える）
					//したりしてエラーの原因となるため。
					List<IAreqEntity> assetApplyEntityList = new ArrayList<IAreqEntity>() ;
					for( IAreqEntity assetApplyEntity : assetApplyEntities ) {
						if( AmAreqStatusId.CheckinRequestApproved.equals( assetApplyEntity.getStsId() ) ||
							AmAreqStatusId.RemovalRequestApproved.equals( assetApplyEntity.getStsId() ) ) {
							assetApplyEntityList.add( assetApplyEntity ) ;
						}
					}
					assetApplyEntities = assetApplyEntityList.toArray( new IAreqEntity[ 0 ] ) ;
				}

				for ( IAreqEntity areqEntity: assetApplyEntities ) {
					IAreqDto areqDto = this.support.getAmFinderSupport().findAreqDto( areqEntity );
					areqDtoEntities.add( areqDto );
				}

				//▼タイムラインごとにサービスを依頼する
				IBuildTaskBean bean =
					getBuildTaskBean( paramBean, pjtLotEntity, bpDto, areqDtoEntities );

				IBldTimelineEntity[] entitys = this.support.getBldTimelineEntity( envNo );
				ITaskFlowEntity[] taskFlowEntitys = this.support.getTaskFlowEntity();

				//キャッシュ設定
				TaskImageResourceCache.setInit( false );
				TaskImageResourceCache.setBuildResourceCache(
											pjtLotEntity, bldEnvEntity, bldEnvSrvEntitys, bpDto, entitys , taskFlowEntitys );

					List<String> exceptionList = null ;
					bean.setForceExecuteMode( false ) ;
					for ( IBldTimelineEntity request : entitys ) {
						//例外が発生してもタイムラインを継続させる。
						try {
							this.buildTaskService.execute( request, bean );
						} catch( java.rmi.ConnectException ex ) {
							this.taskSupport.writeBuildProcessByIrregular((IBldTimelineEntity)request, (IBuildTaskBean)bean, null, ex);
							throw new TriSystemException( BmMessageId.BM005036S , ex ) ;
						} catch ( TriRuntimeException e ) {
							if( null == exceptionList ) {
								exceptionList = new ArrayList<String>() ;
							}
							exceptionList.add( e.getStackTraceString() ) ;
							bean.setForceExecuteMode( true ) ;
						} catch( Exception e ) {
							if( null == exceptionList ) {
								exceptionList = new ArrayList<String>() ;
							}
							exceptionList.add( ExceptionUtils.getStackTraceString(e) ) ;
							bean.setForceExecuteMode( true ) ;
						}
					}
					//タイムラインがすべて終了後、当初の例外を発生させる ＆ buildProcessのエラーメッセージを収集する
					if( null != exceptionList && 0 != exceptionList.size() ) {
						String errorMessage = this.makeErrorMessage( buildNo, paramBean.getProcId() ) ;

						StringBuilder stb = new StringBuilder() ;
						for( String stackTrace : exceptionList ) {
							stb.append( stackTrace ) ;
						}
						Exception exception = new BusinessException(
								BmMessageId.BM005037S , SEP +
								errorMessage + SEP + SEP +
								stb.toString() ) ;

						throw exception ;
					}
			} finally {

				//レポート機能は見直し中
//
//					if ( null != buildEntity ) {
//						IReportEntity repEntity = this.insertReportEntity( paramBean, retBean, systemDate );
//
//						//下層のアクションを実行する
//						{
//							List<Object> paramList = new ArrayList<Object>();
//
//							IReportRelUnitReportParamInfo actionParam = new ReportRelUnitReportParamInfo();
//							actionParam.setUserId	( paramBean.getUserId() );
//							actionParam.setUserName	( paramBean.getUserName() );
//							actionParam.setReportId	( paramBean.getReportId() );
//
//							paramList.add( this.support );
//							paramList.add( actionParam );
//							paramList.add( buildEntity );
//							paramList.add( this.getBuildTaskRecEntity(buildEntity) );
//							paramList.add( repEntity );
//
//							for ( IActionPojo action : actions ) {
//								action.execute( paramList );
//							}
//						}
//					}
			}

			return serviceDto;

		} catch ( BusinessException be ) {
			//ビジネスエラーメール用
			paramBean.setBusinessThrowable( be ) ;
			throw be;
		} catch ( TriRuntimeException be ) {
			//システムエラーメール用
			paramBean.setSystemThrowable( be ) ;
			throw be;
		} catch ( Exception e ) {
			//システムエラーメール用
			paramBean.setSystemThrowable( e ) ;

			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005038S,e);
		}
	}

	/**
	 * ビルドパッケージ作成タスクの引数オブジェクトを取得する。
	 * @param paramBean FlowRelUnitEntryServiceBeanオブジェクト
	 * @param pjtLotEntity ロットエンティティ
	 * @param buildEntity ビルドパッケージエンティティ
	 * @param assetApplyEntities 申請情報エンティティ
	 * @return ビルドパッケージ作成タスクの引数オブジェクト
	 */
	private final IBuildTaskBean getBuildTaskBean(
			FlowRelUnitEntryServiceBean paramBean,
			ILotEntity pjtLotEntity,
			IBpDto bpDto,
			AreqDtoList assetApplyEntities ) {

		BuildTaskBean bean = new BuildTaskBean();

		List<Object> paramList = new CopyOnWriteArrayList<Object>() ;
		paramList.add( paramBean );
		paramList.add( bean );
		paramList.add( pjtLotEntity );
		paramList.add( bpDto );
		paramList.add( assetApplyEntities );

		IServiceDto<IGeneralServiceBean> innerServiceDto = new ServiceDto<IGeneralServiceBean>()
				.setServiceBean( paramBean )
				.setParamList( paramList );

		for ( IDomain<IGeneralServiceBean> action : this.actions ) {
			action.execute( innerServiceDto );
		}

		bean = (BuildTaskBean)BmExtractEntityAddonUtils.extractBuildTask( paramList );
		if ( null == bean ) {
			throw new TriSystemException(BmMessageId.BM005039S);
		}

		return bean;
	}

	/**
	 * 申請情報から申請情報エンティティを取得する。
	 * @param applyInfoViewBeanList 申請情報
	 * @return 申請情報エンティティの配列
	 */
	private IAreqEntity[] setAssetApplyEntity(
						List<ApplyInfoViewBean> applyInfoViewBeanList ) {

		// 全資産コンパイル対応
		if ( TriStringUtils.isEmpty( applyInfoViewBeanList )  ) return new IAreqEntity[]{};


		// 申請番号の抽出
		Set<String> applyNoSet = new HashSet<String>();

		for ( ApplyInfoViewBean applyInfoViewBean : applyInfoViewBeanList ) {
			applyNoSet.add( applyInfoViewBean.getApplyNo() );
		}

		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getAreqCondition(
					(String[])applyNoSet.toArray( new String[0] ));

		ISqlSort sort = FlowRelUnitEditSupport.getAssetApplySort();

		IEntityLimit<IAreqEntity> assetApplyEntityLimit =
			this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), sort, 1, 0 );

		return assetApplyEntityLimit.getEntities().toArray(new IAreqEntity[0]);
	}

//	/**
//	 * ビルドパッケージの業務タスク実行履歴情報エンティティを取得する。
//	 * @param buildEntity
//	 * @return 実行履歴情報エンティティの配列
//	 */
//	private IBuildTaskRecEntity[] getBuildTaskRecEntity( IBuildEntity buildEntity ) {
//
//		BuildTaskRecCondition condition = new BuildTaskRecCondition();
//		condition.setBuildNo( buildEntity.getBuildNo() );
//		condition.setBuildStartDate( buildEntity.getBuildStartDate(), buildEntity.getBuildStartDate() );
//
//		IBuildTaskRecEntityLimit buildTaskRecEntityLimit =
//			this.support.getBuildTaskRecDao().find( condition, null, 1, 0 );
//
//		return buildTaskRecEntityLimit.getEntity();
//	}
//
//	/**
//	 * レポートレコードの登録
//	 * <br>
//	 * <br>ステータスを「処理中」としてレポートレコードを登録する。
//	 *
//	 * @param paramBean
//	 * @param retBean
//	 * @param systemDate システム日時
//	 * @return 登録したレコード情報
//	 */
//	private final IReportEntity insertReportEntity(
//			FlowRelUnitEntryServiceBean paramBean,
//			FlowRelUnitEntryServiceBean retBean,
//			String systemDate ) {
//
//		IReportEntity entity = new ReportEntity();
//
//		String date = DateAddonUtil.getSystemDate();
//
//		entity.setReportNo			( this.support.getReportNumberingDao().nextval() );
//		entity.setReportId			( ReportEntity.ReportId.RelUnitReport.getValue() );
//		entity.setGroupName			( paramBean.getGroupName() );
//		entity.setExecuteUser		( paramBean.getUserName() );
//		entity.setExecuteUserId		( paramBean.getUserId() );
//		entity.setExecuteStartDate	( ( null == systemDate )? date : systemDate );
//		//entity.setExecuteEndDate	(  );
//		entity.setInsertUpdateUser	( paramBean.getUserName() );
//		entity.setInsertUpdateUserId( paramBean.getUserId() );
//		entity.setInsertUpdateDate	( date );
//		entity.setBaseStatusId		( IReportEntity.StatusId.UnEnter.getValue() );
//		entity.setStatusId			( IReportEntity.StatusId.ReportActive.getValue() );
//		entity.setDelStatusId		( StatusFlg.off.value() ) ;
//
//		this.support.getReportDao().insert( entity );
//
//		return entity;
//	}

	/**
	 *
	 * @param buildNo
	 * @param procId
	 * @return
	 */
	private String makeErrorMessage( String buildNo, String procId ) {
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		StringBuilder stb = new StringBuilder() ;

		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(procId);
		ITaskFlowProcEntity[] buildProcessEntityArray = this.support.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]) ;
		for( Integer i = 0 ; i < buildProcessEntityArray.length ; i++ ) {
			ITaskFlowProcEntity buildProcessEntity = buildProcessEntityArray[ i ] ;
			if( BmBpStatusIdForExecData.BuildPackageError.equals( buildProcessEntity.getStsId() ) ) {
				String[] messageArgs = new String[] {
						i.toString(),
						buildNo,
						buildProcessEntity.getProcId(),
						buildProcessEntity.getBldEnvId(),
						buildProcessEntity.getBldLineNo().toString(),
						buildProcessEntity.getBldSrvId(),
						buildProcessEntity.getTaskFlowId(),
						buildProcessEntity.getMsg()
				} ;
				stb.append( ac.getMessage( BmMessageId.BM001027E , messageArgs ) + SEP ) ;
			}
		}

		return stb.toString() ;
	}
}
