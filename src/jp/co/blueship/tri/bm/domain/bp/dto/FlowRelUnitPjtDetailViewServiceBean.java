package jp.co.blueship.tri.bm.domain.bp.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * ビルドパッケージ作成・変更管理詳細用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitPjtDetailViewServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 変更管理番号 */
	private String selectedPjtNo = null;
	/** 申請情報クローズ済フラグ */
	private Boolean assetApplyClosedFlag = null;
	/** 申請情報 */
	private List<ApplyInfoViewBean> applyInfoViewBeanList = new ArrayList<ApplyInfoViewBean>();
	
	
	public String getSelectedPjtNo() {
		return selectedPjtNo;
	}
	public void setSelectedPjtNo( String selectedPjtNo ) {
		this.selectedPjtNo = selectedPjtNo;
	}
	
	public Boolean getAssetApplyClosedFlag() {
		return assetApplyClosedFlag;
	}
	public void setAssetApplyClosedFlag( Boolean assetApplyClosedFlag ) {
		this.assetApplyClosedFlag = assetApplyClosedFlag;
	}
	
	public List<ApplyInfoViewBean> getApplyInfoViewBeanList() {
		return applyInfoViewBeanList;
	}
	public void setApplyInfoViewBeanList(
			List<ApplyInfoViewBean> applyInfoViewBeanList ) {
		this.applyInfoViewBeanList = applyInfoViewBeanList;
	}
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public ApplyInfoViewBean newApplyInfoViewBean() {
		ApplyInfoViewBean bean = new ApplyInfoViewBean();
		return bean;
	}
	
	public class ApplyInfoViewBean implements Serializable {

		private static final long serialVersionUID = 1L;
		
		/** 申請番号 */
		private String applyNo = null;
		/** ステータス */
		private String status = null;
		/** ビルドパッケージ作成対象 */
		private Boolean target = null;
		
		
		public void setApplyNo( String applyNo ) {
			this.applyNo = applyNo;
		}
		public String getApplyNo() {
			return applyNo;
		}

		public void setStatus( String status ) {
			this.status = status;
		}
		public String getStatus() {
			return status;
		}
		public Boolean isTarget() {
			return target;
		}
		public void setTarget(Boolean target) {
			this.target = target;
		}
	}
}
