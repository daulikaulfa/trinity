package jp.co.blueship.tri.bm.domain.bp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.constants.RelUnitScreenItemID;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージクローズ受付確認画面の表示情報設定Class<br>
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author le.thixuan
 * 
 */
public class FlowRelUnitCloseRequestServiceConfirm implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support;
	private ActionStatusMatrixList statusMatrixAction;

	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	/**
	 * {@inheritDoc}
	 */

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute( IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto ) {

		FlowRelUnitCloseRequestServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM ) &&
					!forwordID.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM )) {
				return serviceDto;
			}


			String[] bpIds = paramBean.getSelectedUnitNo();

			if ( refererID.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM )) {

				if ( ScreenType.next.equals( screenType )) {

					if( paramBean.isStatusMatrixV3() ) {
						
						checkRelUnitClose( paramBean );
						List<IBpDto> bpDtoList = this.support.findBpDto(bpIds);
						IRaEntity[] relApplyEntities	= this.support.getRelApplyEntityByBuildNo( this.support.getBuildNo( bpDtoList ) ).getEntities().toArray(new IRaEntity[0]);
	
						String[] pjtNo = this.support.getPjtIds( bpDtoList ) ;
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( paramBean.getSelectedLotNo() )
						.setPjtIds( pjtNo )
						.setAreqIds( BmExtractEntityAddonUtils.getAreqIds( bpDtoList ) )
						.setBpIds( bpIds )
						.setRaIds( this.support.getRelApplyNo(relApplyEntities) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( RelUnitScreenID.UNIT_CLOSE_REQUEST_COMFIRM  )) {

				ItemCheckAddonUtils.checkBuildNo( bpIds );

				IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByBuildNo( bpIds );
				ISqlSort sort = (ISqlSort)DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelUnitCloseRequestConfirm();
				List<IBpDto> bpDtoList = this.support.getBpDtoLimitList( condition, sort );
				// トップ画面でロットをしぼっているので、どのビルドパッケージエンティティでも属するロットは同じ
				ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( bpDtoList.get(0).getBpEntity().getLotId() );

				for ( IBpDto bpDto : bpDtoList ) {
					// PjtId昇順、AreqId昇順にソート
					sortBpAreqLnk( bpDto.getBpAreqLnkEntities() );
				}
				// 申請情報取得（StsId使用）
				IAreqEntity[] applyEntities = this.support.getAssetApplyEntityFromDto( bpDtoList );
				List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
				areqDtoEntities = setAreqDtoFromApplayEntities( areqDtoEntities , applyEntities );

				// 変更管理情報取得（変更管理番号、変更要因番号、申請承認者を使用）
				IPjtEntity[] pjtEntities = this.support.getPjtEntity( bpDtoList );
				List<IPjtAvlDto> pjtDtos	= new ArrayList<IPjtAvlDto>();
				pjtDtos = setPjtDtoFromPjtEntity( pjtDtos , pjtEntities );

				// グループの存在チェック
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );


				// ２重貸出チェック実施（重複貸出が許可されている場合）
				if ( lotDto.getLotEntity().getIsAssetDup().parseBoolean() ) {

					// 選択されたビルドパッケージに含まれている申請番号と、それを含むビルド番号リスト
					Map<String, List<String>> assetApplyNoBuildNoListMap =
						getAssetApplyNoBuildNoListMap( bpDtoList );

					// 資産パスと申請情報リストのマップ
					Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap =
						this.support.getReturnAssetPathAsseApplyEntityMap( areqDtoEntities );

					// 選択されたビルドパッケージ内のチェック
					// 承認の資産同士の重複はエラー
					// 承認とクローズの資産同士の重複は、時間の関係をチェックしたうえでエラー
					checkAssetDuplicateError(
							lotDto, assetPathAssetApplyEntityListMap, assetApplyNoBuildNoListMap );

					// 上記以外の警告チェック
					checkAssetDuplicateWarning(
							lotDto, assetPathAssetApplyEntityListMap, assetApplyNoBuildNoListMap, paramBean );

				}

				//クローズエラー有無チェック
				this.checkUnitCloseError( lotDto.getLotEntity() , paramBean ) ;


				// ロット情報
				setLotInfo( lotDto.getLotEntity(), paramBean );

				// 変更管理、申請情報
				// If new system calls legacy system to validate, don't need to build UnitCloseConfirmViewBean
				if (paramBean.isBuildCloseConfirmBean()) {
					setServiceBeanSearchResult( bpDtoList, pjtDtos, applyEntities, paramBean );
				}

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005077S , e );
		}
	}

	/**
	 * ビルドパッケージに含まれるの資産同士が重複していないかをチェックする
	 * @param lotDto ロットエンティティ
	 * @param assetPathAssetApplyEntityListMap 資産パスと申請情報リストのマップ
	 * @param assetApplyNoBuildNoListMap 申請番号とそれを含むビルドパッケージ番号のマップ
	 */

	private void checkAssetDuplicateError(
			ILotDto lotDto,
			Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap,
			Map<String, List<String>> assetApplyNoBuildNoListMap ) throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( String assetPath : assetPathAssetApplyEntityListMap.keySet() ) {

				List<IAreqEntity> checkEntityList = assetPathAssetApplyEntityListMap.get( assetPath );

				// 重複あり
				if ( 1 < checkEntityList.size() ) {

					// 最初の承認ステータスを比較元に
					IAreqEntity origEntity = null;
					for ( IAreqEntity entity : checkEntityList ) {

						if ( AmAreqStatusId.CheckinRequestApproved.equals( entity.getStsId() )) {
							origEntity = entity;
							break;
						}
					}

					// この重複パスには、返却承認は含まれない（全部クローズ済み）
					if ( null == origEntity ) continue;


					// 残りを比較先としてまわす
					for ( IAreqEntity destEntity : checkEntityList ) {

						// すでにクローズされている案件はチェック対象外とする
						// => クローズ時に警告しているので
						if ( !AmAreqStatusId.CheckinRequestApproved.equals( destEntity.getStsId() )) continue;

						// 比較元は除外する
						if ( !origEntity.getAreqId().equals( destEntity.getAreqId() )) {

							// 内容が同じならセーフ
							// 片や返却承認、片やクローズでクローズ日時＜貸出日時であればセーフ
							if ( !this.support.isSameContents( lotDto, origEntity, destEntity, assetPath ) &&
									!this.support.isCloseDateOlderLendDate( origEntity, destEntity )) {

								List<String> origBuildNoList = assetApplyNoBuildNoListMap.get( origEntity.getAreqId() );
								List<String> destBuildNoList = assetApplyNoBuildNoListMap.get( destEntity.getAreqId() );

								for ( String origBuildNo : origBuildNoList ) {

									for ( String destBuildNo : destBuildNoList ) {

										messageList.add		( BmMessageId.BM001014E );
										messageArgsList.add	( new String[] {
												origBuildNo,
												origEntity.getPjtId(),
												origEntity.getAreqId(),
												assetPath,
												destBuildNo,
												destEntity.getPjtId(),
												destEntity.getAreqId() } );

									}
								}
							}
						}
					}
				}
			}
		} catch ( IOException e ) {
			throw new TriSystemException ( BmMessageId.BM005065S , e );
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ビルドパッケージエンティティから、ビルドパッケージエンティティを構成する申請情報番号と
	 * ビルドパッケージ番号リストのマップを作成する。
	 * @param list ビルドパッケージエンティティ
	 * @return 申請情報番号とビルドパッケージ番号リストのマップ
	 */
	private Map<String, List<String>> getAssetApplyNoBuildNoListMap( List<IBpDto> bpDtoList ) {

		Map<String, List<String>> assetApplyNoBuildNoListMap = new HashMap<String, List<String>>();

		for ( IBpDto bpDto : bpDtoList ) {

			for ( IBpAreqLnkEntity buildApplyEntity : bpDto.getBpAreqLnkEntities() ) {

				String applyNo = buildApplyEntity.getAreqId();

				if ( !assetApplyNoBuildNoListMap.containsKey( applyNo )) {
					assetApplyNoBuildNoListMap.put( applyNo, new ArrayList<String>() );
				}

				assetApplyNoBuildNoListMap.get( applyNo ).add( bpDto.getBpEntity().getBpId() );
			}
		}

		return assetApplyNoBuildNoListMap;

	}

	/**
	 * ビルドパッケージに含まれる資産と含まれない資産の重複チェックを行う（警告チェック）。
	 * @param lotEntity ロットエンティティ
	 * @param relUnitAssetPathApplyEntityListMap ビルドパッケージに含まれる資産のパスと申請情報エンティティリストのマップ
	 * @param assetApplyNoBuildNoListMap 申請番号とそれを含むビルドパッケージ番号のマップ
	 * @param paramBean FlowRelUnitCloseRequestServiceBean
	 */

	private void checkAssetDuplicateWarning(
			ILotDto lotDto,
			Map<String,List<IAreqEntity>> relUnitAssetPathApplyEntityListMap,
			Map<String, List<String>> assetApplyNoBuildNoListMap,
			FlowRelUnitCloseRequestServiceBean paramBean ) throws IOException {

		// 警告チェック対象の全返却申請情
		IAreqEntity[] warningEntities =
			getReturnAssetApplyEntities4Warning( lotDto.getLotEntity().getLotId() );

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
		for ( IAreqEntity areqEntity: warningEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}

		// 警告チェック対象のパスとエンティティリスト
		Map<String,List<IAreqEntity>> warningAssetPathAssetApplyEntityListMap =
			this.support.getReturnAssetPathAsseApplyEntityMap( areqDtoEntities );

		Set<String> warningAssetPathSet = warningAssetPathAssetApplyEntityListMap.keySet();


		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		// ビルドパッケージ外へのチェック
		// ビルドパッケージに含まれる申請情報が承認で、その他の申請情報が承認の場合は警告
		// ビルドパッケージに含まれる申請情報が承認で、その他の申請情報がクローズの場合は、日付比較のうえ警告
		// ビルドパッケージに含まれる申請情報がクローズの場、コミットされない資産なので問題なし
		for ( String relUnitAssetPath : relUnitAssetPathApplyEntityListMap.keySet() ) {

			if ( warningAssetPathSet.contains( relUnitAssetPath )) {

				List<IAreqEntity> relUnitAssetApplyEntityList =
					relUnitAssetPathApplyEntityListMap.get( relUnitAssetPath );

				List<IAreqEntity> warningAssetApplyEntityList =
					warningAssetPathAssetApplyEntityListMap.get( relUnitAssetPath );


				for ( IAreqEntity relUnitAssetApplyEntity : relUnitAssetApplyEntityList ) {

					// ビルドパッケージに含まれる申請情報がすでにクローズされている場合、
					// パッケージクローズ時には何も起きないので外す（承認のみ対象）
					if ( AmAreqStatusId.BuildPackageClosed.equals(
							relUnitAssetApplyEntity.getStsId() )) continue;

					for ( IAreqEntity warningAssetApplyEntity : warningAssetApplyEntityList ) {

						// 内容が同じならセーフ
						// 片や返却承認、片やクローズでクローズ日時＜貸出日時であればセーフ
						if ( !this.support.isSameContents( lotDto, relUnitAssetApplyEntity, warningAssetApplyEntity, relUnitAssetPath ) &&
								!this.support.isCloseDateOlderLendDate( relUnitAssetApplyEntity, warningAssetApplyEntity )) {

							List<String> buildNoList =
								assetApplyNoBuildNoListMap.get( relUnitAssetApplyEntity.getAreqId() );

							List<String> warningBuildNoList =
								assetApplyNoBuildNoListMap.get( warningAssetApplyEntity.getAreqId() );

							for ( String buildNo : buildNoList ) {

								// 比較元の返却申請と警告対象の返却申請が同じビルドパッケージに含まれている場合は除外
								if ( null != warningBuildNoList && warningBuildNoList.contains( buildNo )) continue;

								if ( !AmAreqStatusId.BuildPackageClosed.equals(
										warningAssetApplyEntity.getStsId() )) {

									messageList.add( BmMessageId.BM001015E );
								} else {
									messageList.add( BmMessageId.BM001016E );
								}

								messageArgsList.add	( new String[] {
										buildNo,
										relUnitAssetApplyEntity.getPjtId(),
										relUnitAssetApplyEntity.getAreqId(),
										relUnitAssetPath,
										warningAssetApplyEntity.getPjtId(),
										warningAssetApplyEntity.getAreqId() } );
							}
						}
					}
				}
			}
		}

		if( null == paramBean.getInfoMessageIdList() ) {
			paramBean.setInfoMessageIdList( messageList );
		} else {
			paramBean.getInfoMessageIdList().addAll( messageList ) ;
		}
		if( null == paramBean.getInfoMessageArgsList() ) {
			paramBean.setInfoMessageArgsList( messageArgsList );
		} else {
			paramBean.getInfoMessageArgsList().addAll( messageArgsList ) ;
		}

		WarningCheckBean warningCheck = new WarningCheckBean() ;
		warningCheck.setExistWarning	( ( 0 != messageList.size() ) ? true : false ) ;

//		メッセージ生成
		MessageManager messageManager = ContextAdapterFactory.getContextAdapter().getMessageManager() ;
		List<String> commentListUnitCloseError	= warningCheck.getCommentList() ;
		//[0]
		commentListUnitCloseError.add( messageManager.getMessage( paramBean.getLanguage(), BmMessageId.BM001018E ) ) ;
		//[1]
		commentListUnitCloseError.add( messageManager.getMessage( paramBean.getLanguage(), BmMessageId.BM001019E ) ) ;

		paramBean.setWarningCheckAssetDuplicate( warningCheck ) ;

	}
	/**
	 *
	 * @param pjtLotEntity
	 * @param paramBean
	 */

	private void checkUnitCloseError( ILotEntity pjtLotEntity , FlowRelUnitCloseRequestServiceBean paramBean ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		IBpEntity[] buildEntityArray = this.support.getBuildEntityByRelUnitCloseError( pjtLotEntity.getLotId() ) ;
		for( IBpEntity buildEntity : buildEntityArray ) {
			messageList.add( BmMessageId.BM001023E ) ;
			messageArgsList.add( new String[]{ buildEntity.getBpId() } ) ;
		}

		if( null == paramBean.getInfoMessageIdList() ) {
			paramBean.setInfoMessageIdList( messageList );
		} else {
			paramBean.getInfoMessageIdList().addAll( messageList ) ;
		}
		if( null == paramBean.getInfoMessageArgsList() ) {
			paramBean.setInfoMessageArgsList( messageArgsList );
		} else {
			paramBean.getInfoMessageArgsList().addAll( messageArgsList ) ;
		}

		WarningCheckBean warningCheck = new WarningCheckBean() ;
		warningCheck.setExistWarning	( ( 0 != messageList.size() ) ? true : false ) ;
		warningCheck.setConfirmCheck	( paramBean.getWarningCheckUnitCloseError().isConfirmCheck() );
//		メッセージ生成
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter() ;
		List<String> commentListUnitCloseError	= warningCheck.getCommentList() ;
		//[0]
		commentListUnitCloseError.add( ca.getMessage( paramBean.getLanguage(), BmMessageId.BM001018E ) ) ;
		//[1]
		String logPath = TriStringUtils.convertPath( BmDesignBusinessRuleUtils.getWorkspaceLogPath( pjtLotEntity, paramBean.getFlowAction() ) ) ;
		commentListUnitCloseError.add( ca.getMessage( paramBean.getLanguage(), BmMessageId.BM001024E ,logPath ) ) ;

		paramBean.setWarningCheckUnitCloseError( warningCheck ) ;
	}

	/**
	 * ビルドパッケージがクローズ可能かをチェックする
	 * @param unitInfoInputBean ビルドパッケージ入力情報
	 */

	private static void checkRelUnitClose( FlowRelUnitCloseRequestServiceBean bean )
		throws BaseBusinessException {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			UnitInfoInputBean unitInfoInputBean = bean.getUnitInfoInputBean();

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							BmDesignBeanId.flowRelUnitCloseCheck,
							RelUnitScreenItemID.RelUnitCloseRequestCheck.BASE_LINE_TAG.toString() )) ) {

				if ( TriStringUtils.isEmpty( unitInfoInputBean.getBaseLineTag() )) {
					messageList.add		( BmMessageId.BM001013E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							BmDesignBeanId.flowRelUnitCloseCheck,
							RelUnitScreenItemID.RelUnitCloseRequestCheck.CLOSE_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( unitInfoInputBean.getCloseComment() )) {
					messageList.add		( BmMessageId.BM001011E );
					messageArgsList.add	( new String[] {} );
				}
			}
//			パッケージクローズエラーチェック
			WarningCheckBean warningCheckAssetUnitCloseError = bean.getWarningCheckUnitCloseError() ;
			if ( warningCheckAssetUnitCloseError.isExistWarning() ) {

				if ( !warningCheckAssetUnitCloseError.isConfirmCheck() ) {
					messageList.add		( BmMessageId.BM001021E );
					messageArgsList.add	( new String[] {} );
				}
			}
			//２重貸出チェック
			WarningCheckBean warningCheckAssetDuplicate = bean.getWarningCheckAssetDuplicate() ;
			if ( warningCheckAssetDuplicate.isExistWarning() ) {

				if ( !warningCheckAssetDuplicate.isConfirmCheck() ) {
					messageList.add		( BmMessageId.BM001021E );
					messageArgsList.add	( new String[] {} );
				}
			}


		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * 警告チェック対象の返却申請情報を取得する
	 * @param lotId ロット番号
	 * @return 警告チェック対象の返却申請情報のリスト
	 */
	private IAreqEntity[] getReturnAssetApplyEntities4Warning( String lotId ) {

		// 貸出申請～クローズまでの申請情報
		IJdbcCondition condition			=
			DBSearchConditionAddonUtil.getReturnAreqConditionForWarningAtRelUnitClose( lotId );
		IEntityLimit<IAreqEntity> limit	= this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IAreqEntity[0]);
	}

	/**
	 * ロット情報を設定する
	 * @param lotEntity ロットエンティティ
	 * @param paramBean FlowRelUnitCloseRequestServiceBean
	 */
	private void setLotInfo(
			ILotEntity lotEntity, FlowRelUnitCloseRequestServiceBean paramBean ) {

		paramBean.setSelectedLotNo( lotEntity.getLotId() );
		paramBean.setLotName		( lotEntity.getLotNm() );

	}

	/**
	 * Beanに検索結果を設定する。
	 * @param bpDtoList 検索結果が格納されたビルドパッケージDto(Limit)
	 * @param pjtDtos 変更管理エンティティ
	 * @param applyEntities 申請情報エンティティ
	 * @param paramBean  FlowRelUnitCloseRequestServiceBean
	 */
	private void setServiceBeanSearchResult(
			List<IBpDto> bpDtoList, List<IPjtAvlDto> pjtDtos, IAreqEntity[] applyEntities,
			FlowRelUnitCloseRequestServiceBean paramBean ) {

		List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList =
											new ArrayList<UnitCloseConfirmViewBean>();

		BmViewInfoAddonUtils.setApplyInfoViewBeanAssetApplyEntity( support,
				unitCloseConfirmViewBeanList, bpDtoList, pjtDtos, applyEntities );

		paramBean.setUnitCloseConfirmViewBeanList( unitCloseConfirmViewBeanList );

	}

	/**
	 * 変更情報エンティティから変更承認Dtoを取得します。
	 * @param pjtDtos
	 * @param pjtEntities
	 * @return
	 */
	private List<IPjtAvlDto> setPjtDtoFromPjtEntity ( List<IPjtAvlDto> pjtDtos , IPjtEntity[] pjtEntities ) {

		for ( IPjtEntity entity : pjtEntities ) {
			IPjtAvlDto pjtDto = support.getAmFinderSupport().findPjtAvlDtoByLatest( entity );
			pjtDtos.add( pjtDto );
		}
		return pjtDtos;
	}
	/**
	 * 資産申請エンティティから資産申請Dtoを取得します。
	 * @param areqDtoEntities
	 * @param applyEntities
	 * @return
	 */
	private List<IAreqDto> setAreqDtoFromApplayEntities ( List<IAreqDto> areqDtoEntities , IAreqEntity[] applyEntities ) {
		for ( IAreqEntity areqEntity: applyEntities ) {
			IAreqDto areqDto = support.getAmFinderSupport().findAreqDto( areqEntity );
			areqDtoEntities.add( areqDto );
		}
		return areqDtoEntities;
	}

	/**
	 * BpAreqLnkEntityをPjtId,AreqId順に昇順にソートします
	 * @param bpAreqLnkArray
	 */
	private static void sortBpAreqLnk( List<IBpAreqLnkEntity> bpAreqLnkArray ) {

		Collections.sort( bpAreqLnkArray,
				new Comparator<IBpAreqLnkEntity>() {
					public int compare( IBpAreqLnkEntity obj1, IBpAreqLnkEntity obj2 ) {
						int comp = compareId( obj1.getPjtId(), obj2.getPjtId() );
						if( 0!=comp ) {
							return comp;
						}
						return compareId( obj1.getAreqId(), obj2.getAreqId() );
					}
					private int compareId( String id1, String id2 ) {
						return id1.compareTo( id2 );
					}
				} );
	}
}
