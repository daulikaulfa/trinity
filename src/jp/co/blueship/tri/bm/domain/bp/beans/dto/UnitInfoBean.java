package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * ビルドパッケージ作成・ビルドパッケージ情報入力画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitInfoBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ビルドパッケージ情報 */
	private UnitInfoInputBean unitInfoInputBean = null;

	public UnitInfoInputBean getUnitInfoInputBean() {
		if ( null == unitInfoInputBean ) {
			unitInfoInputBean = new UnitInfoInputBean();
		}
		return unitInfoInputBean;
	}
	public void setUnitInfoInputBean( UnitInfoInputBean unitInfoInputBean ) {
		this.unitInfoInputBean = unitInfoInputBean;
	}
}
