package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * ビルドパッケージ作成・ビルドパッケージ一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class UnitHistoryViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** ビルドパッケージ番号 */
	private String relUnitNo = null;
	/** ビルドパッケージ名 */
	private String relUnitName = null;
	/** ビルドパッケージ概要 */
	private String relUnitSummary = null;
	/** 実行日時 */
	private String executeDate = null;
	/** ステータス */
	private String relUnitStatus = null;

	public String getRelUnitNo() {
		return relUnitNo;
	}
	public void setRelUnitNo(String relUnitNo) {
		this.relUnitNo = relUnitNo;
	}
	
	public String getRelUnitName() {
		return relUnitName;
	}
	public void setRelUnitName(String relUnitName) {
		this.relUnitName = relUnitName;
	}

	public String getRelUnitSummary() {
		return relUnitSummary;
	}
	public void setRelUnitSummary(String relUnitSummary) {
		this.relUnitSummary = relUnitSummary;
	}

	public String getExecuteDate() {
		return executeDate;
	}
	public void setExecuteDate(String executeDate) {
		this.executeDate = executeDate;
	}
	
	public String getRelUnitStatus() {
		return relUnitStatus;
	}
	public void setRelUnitStatus(String relUnitStatus) {
		this.relUnitStatus = relUnitStatus;
	}
}
