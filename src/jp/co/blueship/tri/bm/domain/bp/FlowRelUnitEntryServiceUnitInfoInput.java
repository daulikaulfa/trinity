package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.constants.RelUnitScreenItemID;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;


/**
 * ビルドパッケージ作成・リリース情報入力画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitEntryServiceUnitInfoInput implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelUnitScreenID.UNIT_INFO_INPUT ) &&
					!forwordID.equals( RelUnitScreenID.UNIT_INFO_INPUT ) ){
				return serviceDto;
			}

//			初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;

			UnitInfoBean unitInfoBean = paramBean.getUnitInfoBean();

			if( refererID.equals( RelUnitScreenID.UNIT_INFO_INPUT )) {

				if ( ScreenType.next.equals( screenType ) ) {

					checkRelUnitInput( unitInfoBean );

				}

				paramBean.setUnitInfoBean( unitInfoBean );
			}


			if( forwordID.equals( RelUnitScreenID.UNIT_INFO_INPUT )) {

			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005084S,e);
		}
	}

	/**
	 * ビルドパッケージの入力情報をチェックする
	 * @param unitInfoBean ビルドパッケージ入力情報
	 */

	private static void checkRelUnitInput( UnitInfoBean unitInfoBean )
		throws BaseBusinessException {

		UnitInfoInputBean unitInfoInputBean = unitInfoBean.getUnitInfoInputBean();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							BmDesignBeanId.flowRelUnitInfoInputCheck,
							RelUnitScreenItemID.RelUnitInfoInput.NAME.toString() )) ) {

				if ( TriStringUtils.isEmpty( unitInfoInputBean.getUnitName() )) {
					messageList.add		( BmMessageId.BM001006E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							BmDesignBeanId.flowRelUnitInfoInputCheck,
							RelUnitScreenItemID.RelUnitInfoInput.SUMMARY.toString() )) ) {

				if ( TriStringUtils.isEmpty( unitInfoInputBean.getUnitSummary() )) {
					messageList.add		( BmMessageId.BM001007E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

}
