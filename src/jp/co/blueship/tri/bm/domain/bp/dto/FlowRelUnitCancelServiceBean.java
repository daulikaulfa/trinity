package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * ビルドパッケージ作成・ビルドパッケージ取消用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitCancelServiceBean extends GenericServiceBean {
	
	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択されたロット番号 */	
	private String selectedLotNo = null;
	/** ロット名 */	
	private String lotName = null;
	/** 選択されたビルドパッケージ番号 */
	private String[] selectedUnitNo = null;
	/** ビルドパッケージ編集入力情報 */
	private UnitInfoInputBean unitInfoInputBean = null;
	/** 申請情報 */
	private List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList = new ArrayList<UnitCloseConfirmViewBean>();
	
	/** クローズ時 パッケージクローズエラー警告 */
	private WarningCheckBean warningCheckUnitCloseError = new WarningCheckBean() ;
	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}
	
	public String[] getSelectedUnitNo() {
		return selectedUnitNo;
	}
	public void setSelectedUnitNo( String[] selectedUnitNo ) {
		this.selectedUnitNo = selectedUnitNo;
	}

	public UnitInfoInputBean getUnitInfoInputBean() {
		return unitInfoInputBean;
	}
	public void setUnitInfoInputBean( UnitInfoInputBean unitInfoInputBean ) {
		this.unitInfoInputBean = unitInfoInputBean;
	}
	
	public List<UnitCloseConfirmViewBean> getUnitCloseConfirmViewBeanList() {
		return unitCloseConfirmViewBeanList;
	}
	public void setUnitCloseConfirmViewBeanList( List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList ) {
		this.unitCloseConfirmViewBeanList = unitCloseConfirmViewBeanList;
	}
	public WarningCheckBean getWarningCheckUnitCloseError() {
		return warningCheckUnitCloseError;
	}
	public void setWarningCheckUnitCloseError(
			WarningCheckBean warningCheckUnitCloseError) {
		this.warningCheckUnitCloseError = warningCheckUnitCloseError;
	}
	
}
