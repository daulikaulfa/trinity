package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * リリースからの申請情報閲覧
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ApplyInfoViewBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** 変更管理番号 */
	private String pjtNo = null;
	/** 申請番号 */
	private String applyNo = null;
	/** 申請件名 */
	private String applySubject = null;
	/** 貸出日 */
	private String lendApplyDate = null;
	/** 返却日 */
	private String returnApplyDate = null;
	/** 返却承認日 */
	private String returnApproveDate = null;
	/** 申請者 */
	private String returnApplyUser = null;
	/** 申請者ＩＤ */
	private String returnApplyUserId = null;
	/** ステータス */
	private String applyStatus = null;

	public String getReturnApplyUserId() {
		return returnApplyUserId;
	}
	public void setReturnApplyUserId(String returnApplyUserId) {
		this.returnApplyUserId = returnApplyUserId;
	}
	public String getPjtNo() {
		return pjtNo;
	}
	public void setPjtNo(String pjtNo) {
		this.pjtNo = pjtNo;
	}
	
	public String getApplyNo() {
		return applyNo;
	}
	public void setApplyNo(String applyNo) {
		this.applyNo = applyNo;
	}

	public String getApplySubject() {
		return applySubject;
	}
	public void setApplySubject(String applySubject) {
		this.applySubject = applySubject;
	}

	public String getLendApplyDate() {
		return lendApplyDate;
	}
	public void setLendApplyDate(String lendApplyDate) {
		this.lendApplyDate = lendApplyDate;
	}

	public String getReturnApplyDate() {
		return returnApplyDate;
	}
	public void setReturnApplyDate(String returnApplyDate) {
		this.returnApplyDate = returnApplyDate;
	}
	
	public String getReturnApproveDate() {
		return returnApproveDate;
	}
	public void setReturnApproveDate(String returnApproveDate) {
		this.returnApproveDate = returnApproveDate;
	}
	
	public String getReturnApplyUser() {
		return returnApplyUser;
	}
	public void setReturnApplyUser(String returnApplyUser) {
		this.returnApplyUser = returnApplyUser;
	}
	
	public String getApplyStatus() {
		return applyStatus;
	}
	public void setApplyStatus(String applyStatus) {
		this.applyStatus = applyStatus;
	}
}
