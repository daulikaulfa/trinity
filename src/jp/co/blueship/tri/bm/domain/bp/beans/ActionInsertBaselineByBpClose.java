package jp.co.blueship.tri.bm.domain.bp.beans;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.LotBlReqLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * クローズ対象のビルドパッケージ単位でベースラインを登録します。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 * 
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 */
public class ActionInsertBaselineByBpClose extends ActionPojoAbstract<FlowRelUnitCloseRequestServiceBean>  {

	private BmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( BmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute( IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto ) {

		FlowRelUnitCloseRequestServiceBean paramBean = serviceDto.getServiceBean();

		ILotDto lotDto = paramBean.getCacheLotDto();

		String[] bpIds = paramBean.getSelectedUnitNo();
		ItemCheckAddonUtils.checkBuildNo( bpIds );

		List<IBpDto> bpDtoList = this.support.findBpDto( bpIds );

		Map<String, String> bpIdSet = this.insertLotBl( paramBean, bpDtoList );
		this.insertLotBlReqLnk( bpDtoList, bpIdSet );
		this.insertLotBlMdlLnk( bpDtoList, bpIdSet, lotDto.getIncludeMdlEntities(true) );

		List<ILotBlEntity> lotBlEntities = this.support.getAmFinderSupport().findLotBlEntities( bpIdSet.values().toArray(new String[0]) );
		paramBean.setRegisterLotBlDto( this.support.getAmFinderSupport().findLotBlDto( lotBlEntities ));

		this.support.getSmFinderSupport().cleaningExecDataSts( AmTables.AM_LOT_BL, bpIdSet.values().toArray(new String[0]) );
		this.support.getSmFinderSupport().registerExecDataSts(
				paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.Committing, bpIdSet.values().toArray(new String[0]) );

		return serviceDto;
	}

	/**
	 * ロット・ベースライン登録
	 *
	 * @param param
	 * @param bpDtoList
	 * @return 登録したロット・ベースラインIDのリスト
	 */
	private Map<String, String> insertLotBl( FlowRelUnitCloseRequestServiceBean param, List<IBpDto> bpDtoList ) {
		UnitInfoInputBean unitInfoInputBean = param.getUnitInfoInputBean();

		Map<String, String> bpIdSet = new TreeMap<String, String>();
		List<ILotBlEntity> entities = new ArrayList<ILotBlEntity>();

		for ( IBpDto bpDto: bpDtoList ) {
			IBpEntity bpEntity = bpDto.getBpEntity();

			ILotBlEntity lotBlEntity = new LotBlEntity();

			lotBlEntity.setLotBlId( this.support.getAmFinderSupport().nextvalByLotBlId() );
			lotBlEntity.setLotBlNm( bpEntity.getBpNm() );
			lotBlEntity.setLotId( bpEntity.getLotId() );
			lotBlEntity.setLotVerTag( param.getNumberingVal() );
			lotBlEntity.setLotBlTag( unitInfoInputBean.getBaseLineTag() );
			lotBlEntity.setDataCtgCd( BmTables.BM_BP.name() );
			lotBlEntity.setDataId( bpEntity.getBpId() );
			lotBlEntity.setStsId( AmLotBlStatusId.Unspecified.getStatusId() );
			lotBlEntity.setLotChkinUserId( param.getUserId() );
			lotBlEntity.setLotChkinUserNm( param.getUserName() );
			lotBlEntity.setLotChkinTimestamp( TriDateUtils.getSystemTimestamp() );
			lotBlEntity.setLotChkinCmt( unitInfoInputBean.getCloseComment() );
			lotBlEntity.setDelStsId( StatusFlg.off );

			entities.add( lotBlEntity );
			bpIdSet.put( bpEntity.getBpId(), lotBlEntity.getLotBlId() );
		}

		this.support.getAmFinderSupport().getLotBlDao().insert( entities );

		return bpIdSet;
	}

	/**
	 * ロット・ベースライン・資産申請登録
	 *
	 * @param bpDtoList
	 * @param bpIdSet
	 */
	private void insertLotBlReqLnk( List<IBpDto> bpDtoList, Map<String, String> bpIdSet ) {
		List<ILotBlReqLnkEntity> lotBlReqLnkEntities = new ArrayList<ILotBlReqLnkEntity>();

		for ( IBpDto bpDto: bpDtoList ) {
			IBpEntity bpEntity = bpDto.getBpEntity();

			for ( IBpAreqLnkEntity areqLnkEntity : bpDto.getBpAreqLnkEntities() ) {
				ILotBlReqLnkEntity reqEntity = new LotBlReqLnkEntity();
				reqEntity.setLotBlId( bpIdSet.get( bpEntity.getBpId() ) );
				reqEntity.setAreqId( areqLnkEntity.getAreqId() );
				reqEntity.setDelStsId( StatusFlg.off );

				lotBlReqLnkEntities.add( reqEntity );
			}
		}

		this.support.getAmFinderSupport().getLotBlReqLnkDao().insert( lotBlReqLnkEntities );

		return;
	}

	/**
	 * ロット・ベースライン・モジュール登録
	 *
	 * @param bpDtoList
	 * @param bpIdSet
	 * @param mdlEntities
	 */
	private void insertLotBlMdlLnk( List<IBpDto> bpDtoList, Map<String, String> bpIdSet, List<ILotMdlLnkEntity> mdlEntities ) {
		List<ILotBlMdlLnkEntity> lotBlMdlLnkEntities = new ArrayList<ILotBlMdlLnkEntity>();


		for ( IBpDto bpDto: bpDtoList ) {
			Set<String> modifiedModuleSet = new TreeSet<String>();
			this.setModuleName(bpDto.getBpMdlLnkEntities(), modifiedModuleSet);

			IBpEntity bpEntity = bpDto.getBpEntity();

			for ( ILotMdlLnkEntity lotMdlEntity : mdlEntities ) {
				ILotBlMdlLnkEntity mdlEntity = new LotBlMdlLnkEntity();
				mdlEntity.setLotBlId( bpIdSet.get( bpEntity.getBpId() ) );
				mdlEntity.setMdlNm( lotMdlEntity.getMdlNm() );
				mdlEntity.setMdlVerTag( lotMdlEntity.getMdlVerTag() );

				if ( modifiedModuleSet.contains(lotMdlEntity.getMdlNm()) ) {
					mdlEntity.setChkinTargetMdl( StatusFlg.on );
				} else {
					mdlEntity.setChkinTargetMdl( StatusFlg.off );
				}

				mdlEntity.setLotChkinRev( lotMdlEntity.getLotChkinRev() );
				mdlEntity.setDelStsId( StatusFlg.off );

				lotBlMdlLnkEntities.add( mdlEntity );
			}
		}

		this.support.getAmFinderSupport().getLotBlMdlLnkDao().insert( lotBlMdlLnkEntities );
	}

	/**
	 * ビルドパッケージ・モジュールからモジュール名を抽出する
	 * @param mdlLnkEntities ビルドパッケージ・モジュールエンティティのList
	 * @param modifiedModuleSet 抽出したモジュール名を格納するセット
	 */
	private void setModuleName(
			List<IBpMdlLnkEntity> mdlLnkEntities, Set<String> modifiedModuleSet ) {

		for ( IBpMdlLnkEntity mdlEntity : mdlLnkEntities ) {
			modifiedModuleSet.add( mdlEntity.getMdlNm() );
		}
	}

}
