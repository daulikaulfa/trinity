package jp.co.blueship.tri.bm.domain.bp.beans;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqEntity;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlDto;
import jp.co.blueship.tri.am.dao.baseline.eb.ILotBlMdlLnkEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.domain.head.beans.dto.ActionLotCheckInServiceBean;
import jp.co.blueship.tri.am.domainx.IAmDomain;
import jp.co.blueship.tri.bm.dao.bp.eb.BpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.BpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignEntryKeyByLogs;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;

/**
 * ビルドパッケージクローズ処理で利用するサービステンプレートです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Siti Hajar
 */
public abstract class BpClosureServiceTemplete implements IDomain<FlowRelUnitCloseRequestServiceBean> {

	protected static final ILog log = TriLogFactory.getInstance();

	protected BmFinderSupport support;
	protected IContextAdapter ac;

	protected static final String logServiceByUpdateAmAreq = " Update " + AmTables.AM_AREQ.name() + " for Status ";
	protected static final String logServiceByUpdateBmBp = " Update " + BmTables.BM_BP.name() + " for Status ";
	protected static final String logServiceByUpdateAmLot = " Update " + AmTables.AM_LOT.name() + " for History ";

	private IAmDomain<ActionLotCheckInServiceBean> actionCheckIn;

	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport(BmFinderSupport support) {
		this.support = support;
	}

	/**
	 * インタフェースがインスタンス生成時に自動的に設定されます。
	 * @param actionCheckIn チェックイン処理
	 */
	public void setActionCheckIn(IAmDomain<ActionLotCheckInServiceBean> actionCheckIn) {
		this.actionCheckIn = actionCheckIn;
	}

	protected IContextAdapter contextAdapter() {

		if (ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return ac;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute(
			IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto) {

		FlowRelUnitCloseRequestServiceBean paramBean = serviceDto.getServiceBean();

		String flowActionTitle = null;

		boolean isSuccess = false;
		int countBySkipResource = 0;

		LogBusinessFlow logBusinessFlow = new LogBusinessFlow();

		try {
			ILotDto lotDto = paramBean.getCacheLotDto();
			ILotEntity lotEntity = lotDto.getLotEntity();

			if ( ! isTargetVcsCategory( lotEntity.getVcsCtgCd() ) ) {
				return serviceDto;
			}

			String[] bpIds = paramBean.getSelectedUnitNo();
			ItemCheckAddonUtils.checkBuildNo(bpIds);

			//ログの初期処理
			logBusinessFlow.makeLogPath( paramBean.getFlowAction() , lotDto.getLotEntity() ) ;
			//ログの開始ラベル出力
			flowActionTitle = LogBusinessFlow.makeFlowActionTitle( paramBean.getFlowAction() ) ;
			logBusinessFlow.writeLogWithDateBegin( flowActionTitle ) ;

			// ログのヘッダ部出力
			this.outLogHeader(logBusinessFlow, lotEntity, paramBean);

			List<IBpDto> bpDtoList = this.support.findBpDto(bpIds);

			List<IAreqDto> areqDtoList;
			if ( !TriStringUtils.isEmpty( paramBean.getTargetApplyNo() ) ) {
				areqDtoList = this.getAreqEntitiesSortByRequestDate( paramBean.getTargetApplyNo() );
			} else {// 全資産コンパイル対応
				areqDtoList = new ArrayList<IAreqDto>();
			}

			ActionLotCheckInServiceBean checkinBean = new ActionLotCheckInServiceBean();
			{
				IServiceDto<ActionLotCheckInServiceBean> innerServiceDto = new ServiceDto<ActionLotCheckInServiceBean>();

				TriPropertyUtils.copyProperties(checkinBean, paramBean);
				checkinBean.setLogService( logBusinessFlow );
				checkinBean.setLotVerTag( paramBean.getNumberingVal() );
				checkinBean.setLotBlTag( paramBean.getUnitInfoInputBean().getBaseLineTag() );
				checkinBean.setLotChkinTimestamp( TriDateUtils.getSystemTimestamp() );
				checkinBean.setLotChkinCmt( paramBean.getUnitInfoInputBean().getCloseComment() );
				checkinBean.setTargetLotDto( paramBean.getCacheLotDto() );
				checkinBean.setTargetAreqDtoList( areqDtoList );
				checkinBean.setRegisterLotBlDto( paramBean.getRegisterLotBlDto() );

				innerServiceDto.setServiceBean( checkinBean );

				actionCheckIn.execute( innerServiceDto );
			}
			this.innerExecute(serviceDto, checkinBean, bpDtoList);

			this.support.getSmFinderSupport().cleaningExecDataSts( paramBean.getProcId() );
			paramBean.setProcMgtStatusId( SmProcMgtStatusId.Success );

			countBySkipResource = checkinBean.getCountBySkipResource();

			isSuccess = true;

			return serviceDto;

		} catch (TriRuntimeException be) {
			logBusinessFlow.writeLog(be.getStackTraceString());
			throw be;
		} catch (Exception e) {
			logBusinessFlow.writeLog(ExceptionUtils.getStackTraceString(e));
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005032S, e);
		} finally {
			if (null != flowActionTitle) {
				if( 0 < countBySkipResource ) {
					logBusinessFlow.writeLog( BmMessageId.BM002000W , String.valueOf(countBySkipResource));
				}

				logBusinessFlow.writeLogWithDateEnd(flowActionTitle);

				if (isSuccess) {
					logBusinessFlow.deleteLog(BmDesignEntryKeyByLogs.deleteLogAtSuccessRelUnitClose);
				}
			}
		}
	}

	/**
	 * Templeteを継承したクラスで実装する必要のあるメソッドです。
	 *
	 * @param serviceDto Service呼び出し時のパラメータ
	 * @param checkinBean チェックインDTO
	 * @param bpDtoList ビルドパッケージDTOのList
	 */
	protected abstract void innerExecute(
			IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto,
			ActionLotCheckInServiceBean checkinBean,
			List<IBpDto> bpDtoList ) throws Exception;

	/**
	 * 処理対象のVCSかどうかの判定を行います。
	 *
	 * @param vcsCtgCd VCS分類コード
	 * @return 処理対象であればtrue。それ以外はfalse
	 */
	protected abstract boolean isTargetVcsCategory( String vcsCtgCd );

	/**
	 * ログのヘッダ部分を出力する
	 *
	 * @param logPath ログファイル出力パス
	 * @param pjtLotEntity ロット情報
	 * @param paramBean FlowRelUnitCloseRequestServiceBean
	 * @throws Exception
	 */
	private void outLogHeader(LogBusinessFlow logBusinessFlow, ILotEntity pjtLotEntity, FlowRelUnitCloseRequestServiceBean paramBean)
			throws Exception {

		StringBuilder stb = new StringBuilder();
		stb = logBusinessFlow.makeUserAndLotLabel(stb, paramBean, pjtLotEntity.getLotNm(), pjtLotEntity.getLotId());
		stb = logBusinessFlow.makeBaseLineLabel(stb, paramBean.getUnitInfoInputBean().getBaseLineTag(), paramBean.getNumberingVal());
		// ビルドパッケージ番号ラベル
		String[] buildNoArray = paramBean.getSelectedUnitNo();
		for (String buildNo : buildNoArray) {
			stb = logBusinessFlow.makeCheckInIdLabel(stb, buildNo);
		}
		logBusinessFlow.writeLog(stb.toString());
	}

	/**
	 * 申請番号から申請情報DTOのListを取得する
	 * @param areqId 申請番号
	 * @return 申請情報DTOのList
	 */
	private List<IAreqDto> getAreqEntitiesSortByRequestDate( String[] areqId ) {
		if( TriStringUtils.isEmpty(areqId) ) {
			throw new TriSystemException(BmMessageId.BM005004S );
		}

		List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.reqTimestamp, TriSortOrder.Asc, 1);

		IJdbcCondition condition			= DBSearchConditionAddonUtil.getAreqCondition( areqId );
		List<IAreqEntity> areqEntities = this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition() , sort);

		for ( IAreqEntity areqEntity: areqEntities ) {
			IAreqDto areqDto = new AreqDto();
			areqDto.setAreqEntity( areqEntity );

			AreqFileCondition fileCondition = new AreqFileCondition();
			fileCondition.setAreqId( areqEntity.getAreqId() );

			areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
			areqDtoEntities.add( areqDto );
		}

		return areqDtoEntities;
	}

	/**
	 * 申請情報をクローズ成功／失敗に更新する
	 *
	 * @param checkinBean チェックインDTO
	 */
	protected void updateAreqEntityToClose(ActionLotCheckInServiceBean checkinBean) {
		LogBusinessFlow lotService = checkinBean.getLogService();

		lotService.writeLogWithDateBegin(logServiceByUpdateAmAreq);

		try {
			List<IAreqEntity> areqEntities = new ArrayList<IAreqEntity>();

			for (IAreqDto areqDto : checkinBean.getTargetAreqDtoList()) {
				IAreqEntity areqEntity = new AreqEntity();
				areqEntity.setAreqId( areqDto.getAreqEntity().getAreqId() );
				areqEntity.setStsId(AmAreqStatusId.BuildPackageClosed.getStatusId());
				areqEntities.add( areqEntity );

				if ( DesignSheetUtils.isRecord() ) {
					IHistEntity histEntity = new HistEntity();
					histEntity.setActSts(UmActStatusId.none.getStatusId());

					support.getAmFinderSupport().getAreqHistDao().insert(histEntity , areqDto);
				}
			}

			this.support.getAmFinderSupport().getAreqDao().update(areqEntities);

		} finally {
			lotService.writeLogWithDateEnd(logServiceByUpdateAmAreq);
		}
	}

	/**
	 * ビルドパッケージ情報をクローズ成功／失敗に更新する
	 *
	 * @param checkinBean チェックインDTO
	 * @param unitInfoInputBean 編集情報
	 * @param bpDtoList
	 */
	protected void updateBpEntityToClose(ActionLotCheckInServiceBean checkinBean, List<IBpDto> bpDtoList) {
		LogBusinessFlow lotService = checkinBean.getLogService();

		lotService.writeLogWithDateBegin( logServiceByUpdateBmBp );

		try {
			Map<String, String> moduleNameMap = this.getModuleNameMap(checkinBean.getRegisterLotBlDto());

			Timestamp systemDate = checkinBean.getLotChkinTimestamp();
			List<IBpEntity> bpEntities = new ArrayList<IBpEntity>();
			List<IBpMdlLnkEntity> mdlEntities = new ArrayList<IBpMdlLnkEntity>();

			for (IBpDto bpDto : bpDtoList) {
				IBpEntity bpEntity = bpDto.getBpEntity();
				IBpEntity updBpEntity = new BpEntity();

				for (IBpMdlLnkEntity mdlEntity : bpDto.getBpMdlLnkEntities()) {
					if ( ! moduleNameMap.containsKey( mdlEntity.getMdlNm() ) ) {
						continue;
					}

					IBpMdlLnkEntity updMdlEntity = new BpMdlLnkEntity();
					updMdlEntity.setBpId( mdlEntity.getBpId() );
					updMdlEntity.setMdlNm( mdlEntity.getMdlNm() );
					updMdlEntity.setMdlVerTag( checkinBean.getLotVerTag() );
					updMdlEntity.setBpCloseRev( moduleNameMap.get( mdlEntity.getMdlNm() ) );

					mdlEntities.add( updMdlEntity );
				}

				updBpEntity.setBpId(bpEntity.getBpId());
				updBpEntity.setCloseCmt(checkinBean.getLotChkinCmt());
				updBpEntity.setBpCloseBlTag(checkinBean.getLotBlTag());
				updBpEntity.setBpCloseVerTag(checkinBean.getLotVerTag());
				updBpEntity.setCloseTimestamp(systemDate);
				updBpEntity.setCloseUserNm(checkinBean.getUserName());
				updBpEntity.setCloseUserId(checkinBean.getUserId());
				updBpEntity.setStsId(BmBpStatusId.BuildPackageClosed.getStatusId());

				bpEntities.add( updBpEntity );

				if (DesignSheetUtils.isRecord()) {

					IHistEntity histEntity = new HistEntity();
					histEntity.setActSts( UmActStatusId.none.getStatusId() );

					support.getBpHistDao().insert( histEntity , bpDto);
				}
			}

			this.support.getBpDao().update(bpEntities);
			this.support.getBpMdlLnkDao().update(mdlEntities);

		} finally {
			lotService.writeLogWithDateEnd( logServiceByUpdateBmBp );
		}
	}

	/**
	 * ロット情報を更新する。
	 *
	 * @param checkinBean チェックインDTO
	 */
	protected void updateLotEntityToClose( ActionLotCheckInServiceBean checkinBean ) {
		LogBusinessFlow lotService = checkinBean.getLogService();

		lotService.writeLogWithDateBegin( logServiceByUpdateAmLot );

		try {
			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( checkinBean.getTargetLotDto().getLotEntity().getLotId() );

			if (DesignSheetUtils.isRecord()) {
				IHistEntity histEntity = new HistEntity();
				histEntity.setActSts(UmActStatusId.none.getStatusId());

				this.support.getUmFinderSupport().getLotHistDao().insert( histEntity , lotDto);
			}

		} finally {
			lotService.writeLogWithDateEnd( logServiceByUpdateAmLot );
		}
	}
	/**
	 * 変更のあったモジュール名を取得する
	 *
	 * @param lotBlDtoList ロットベースラインDTOのList
	 * @return モジュール名のマップ
	 */
	protected Map<String, String> getModuleNameMap(List<ILotBlDto> lotBlDtoList) {

		Map<String, String> moduleNameMap = new LinkedHashMap<String, String>();

		for (ILotBlDto dto : lotBlDtoList) {
			for ( ILotBlMdlLnkEntity entity : dto.getCheckInMdlEntities(true) ) {
				moduleNameMap.put( entity.getMdlNm(), entity.getLotChkinRev() );
			}
		}

		return moduleNameMap;
	}

}
