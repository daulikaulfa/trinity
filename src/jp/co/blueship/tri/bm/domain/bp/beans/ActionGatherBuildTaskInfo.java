package jp.co.blueship.tri.bm.domain.bp.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.agent.bm.svc.flow.beans.constants.BuildTaskDefineId;
import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.BuildTaskBean;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqBinaryFileEntity;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqFileEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpMdlLnkEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskPropertyEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.TaskPropertyEntity;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;


/**
 * ＲＰのタスクに必要な情報収集を行います。
 *
 * All Rights Reserved, Copyright(c) Blueship  2012<br>
 *
 */
public class ActionGatherBuildTaskInfo extends ActionPojoAbstract<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		try {

			List<Object> paramList = serviceDto.getParamList();

			FlowRelUnitEntryServiceBean paramBean = serviceDto.getServiceBean();

			if ( null == paramBean ) {
				throw new TriSystemException( BmMessageId.BM005042S );
			}

			BuildTaskBean bean = (BuildTaskBean)BmExtractEntityAddonUtils.extractBuildTask( paramList );

			if ( null == bean ) {
				throw new TriSystemException( BmMessageId.BM005043S );
			}

			ILotEntity lotEntity = BmExtractEntityAddonUtils.extractPjtLot(paramList);
			if ( null == lotEntity ) {
				throw new TriSystemException( BmMessageId.BM005044S );
			}
			IBpDto bpDto = BmExtractEntityAddonUtils.extractBpDto(paramList);
			if ( null == bpDto ) {
				throw new TriSystemException( BmMessageId.BM005045S );
			}

			List<IAreqDto> assetApplyEntities = BmExtractEntityAddonUtils.extractAssetApplyArray( paramList );
			if ( null == assetApplyEntities ) {
				throw new TriSystemException( BmMessageId.BM005046S );
			}

			List<ITaskPropertyEntity> propertys = new ArrayList<ITaskPropertyEntity>();

			bean.setLotNo				( lotEntity.getLotId() ) ;
			bean.setBuildNo				( bpDto.getBpEntity().getBpId() );
			bean.setProcId				( paramBean.getProcId() );
			bean.setInsertUpdateUser	( paramBean.getUserName() );
			bean.setInsertUpdateUserId	( paramBean.getUserId() );

			// ビルドパッケージ系情報
			setBuildInfo		( propertys, bpDto , lotEntity);

			// 返却申請、削除申請系情報
			setAssetApplyInfo	( propertys, lotEntity, assetApplyEntities );

			// ホームパス
			{
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( BuildTaskDefineId.homePath.toString() );
				propertyEntity.setValue	( sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath )) ;

				propertys.add( propertyEntity );
			}

			bean.setPropertys( propertys );

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( BmMessageId.BM005091S, e );
		}

		return serviceDto;
	}


	/**
	 * タスクの引数にビルドパッケージ情報を設定する。
	 * @param propertys タスクの引数
	 * @param bpDto ビルドパッケージ情報
	 * @param lotEntity ロット情報
	 */
	private void setBuildInfo(
			List<ITaskPropertyEntity> propertys, IBpDto bpDto , ILotEntity lotEntity ) {

		// ビルドパッケージ番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.buildNo.toString() );
			propertyEntity.setValue	( bpDto.getBpEntity().getBpId() );

			propertys.add( propertyEntity );
		}

		// ビルドパッケージタグ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.buildTag.toString() );
			propertyEntity.setValue	( bpDto.getBpEntity().getBpCloseVerTag() );

			propertys.add( propertyEntity );
		}

		// ロット番号
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.lotNo.toString() );
			propertyEntity.setValue	( bpDto.getBpEntity().getLotId() );

			propertys.add( propertyEntity );
		}

		// 変更のあったモジュール名
		{
			List<String> moduleNameList = new ArrayList<String>();
			for ( IBpMdlLnkEntity moduleEntity : bpDto.getBpMdlLnkEntities() ) {
				moduleNameList.add( moduleEntity.getMdlNm() );
			}

			if( true != TriStringUtils.isEmpty( moduleNameList ) ) {
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( BuildTaskDefineId.module.toString() );
				propertyEntity.setValue	( convertValue( (String[])moduleNameList.toArray( new String[0] )));

				propertys.add( propertyEntity );
			}
		}

		// ロットの原本ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.workPath.toString() );
			propertyEntity.setValue	( lotEntity.getLotMwPath() );

			propertys.add( propertyEntity );
		}

		// ロットの作業ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.workspace.toString() );
			propertyEntity.setValue	( lotEntity.getWsPath() );

			propertys.add( propertyEntity );
		}

		// ロットの履歴格納ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.historyPath.toString() );
			propertyEntity.setValue	( lotEntity.getHistPath() );

			propertys.add( propertyEntity );
		}

		// ロットの内部返却ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.systemInBox.toString() );
			propertyEntity.setValue	( lotEntity.getSysInBox() );

			propertys.add( propertyEntity );
		}

		// ＲＰ生成物出力先ディレクトリ
		{
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.historyRelUnitDSLPath.toString() );
			propertyEntity.setValue	( BmDesignBusinessRuleUtils.getHistoryRelUnitDSLPath( lotEntity, bpDto.getBpEntity() ).getPath() );

			propertys.add( propertyEntity );
		}

	}

	/**
	 * タスクの引数に返却、削除申請情報を設定する。
	 * @param propertys タスクの引数
	 * @param lotEntity ロットエンティティ
	 * @param assetApplyEntities 申請情報エンティティ
	 */
	private void setAssetApplyInfo(
			List<ITaskPropertyEntity> propertys,
			ILotEntity lotEntity,
			List<IAreqDto> assetApplyEntities ) {

		// 返却申請関連
		setAssetRetApplyInfo( propertys, lotEntity, assetApplyEntities );

		// 削除申請関連
		setAssetDelApplyInfo( propertys, assetApplyEntities );

	}

	/**
	 * タスクの引数に返却申請情報を設定する。
	 * @param propertys タスクの引数
	 * @param lotEntity ロットエンティティ
	 * @param assetApplyEntities 申請情報エンティティの配列
	 */
	private void setAssetRetApplyInfo(	List<ITaskPropertyEntity> propertys,
										ILotEntity lotEntity,
										List<IAreqDto> assetApplyEntities ) {

		List<String> assetApplyNoList = new ArrayList<String>();
		for ( IAreqDto entity : assetApplyEntities ) {

			if ( BmBusinessJudgUtils.isReturnApply( entity.getAreqEntity() ) ) {
				assetApplyNoList.add( entity.getAreqEntity().getAreqId() );
			}
		}

		//返却申請番号
		String[] assetApplyNoArray = (String[])assetApplyNoList.toArray( new String[0] );
		if( true != TriStringUtils.isEmpty( assetApplyNoArray ) ) {

			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.applyNo.toString() );
			propertyEntity.setValue	( convertValue( assetApplyNoArray ));

			propertys.add( propertyEntity );
		}
	}

	/**
	 * タスクの引数に削除申請情報を設定する。
	 * @param propertys タスクの引数
	 * @param assetApplyEntities 申請情報エンティティの配列
	 */
	private void setAssetDelApplyInfo(
			List<ITaskPropertyEntity> propertys, List<IAreqDto> assetApplyEntities ) {

		List<String> assetDelApplyNoList = new ArrayList<String>();
		for ( IAreqDto entity : assetApplyEntities ) {

			if ( BmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) {
				assetDelApplyNoList.add( entity.getAreqEntity().getAreqId() );
			}
		}


		String[] assetDelApplyNoArray =
							(String[])assetDelApplyNoList.toArray( new String[0] );

		// 削除申請番号
		if( true != TriStringUtils.isEmpty( assetDelApplyNoArray ) ) {
			ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

			propertyEntity.setName	( BuildTaskDefineId.delApplyNo.toString() );
			propertyEntity.setValue	( convertValue( assetDelApplyNoArray ) );

			propertys.add( propertyEntity );
		}


		// 原本削除のパス
		{
			// ソース
			List<String> filePathList = new ArrayList<String>();
			// モジュール
			List<String> binaryFilePathList = new ArrayList<String>();

			for ( IAreqDto entity : assetApplyEntities ) {
				if ( !BmBusinessJudgUtils.isDeleteApply( entity.getAreqEntity() )) continue;

				if ( null != entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {
					for ( IAreqFileEntity assetFileEntity: entity.getAreqFileEntities(AreqCtgCd.RemovalRequest) ) {
						filePathList.add( assetFileEntity.getFilePath() );
					}
				}

				if ( null != entity.getAreqBinaryFileEntities(AreqCtgCd.RemovalRequest) ) {
					for ( IAreqBinaryFileEntity assetFileEntity: entity.getAreqBinaryFileEntities(AreqCtgCd.RemovalRequest) ) {
						binaryFilePathList.add( assetFileEntity.getFilePath() );
					}
				}
			}


			String[] filePathArray = filePathList.toArray( new String[0] );

			if( true != TriStringUtils.isEmpty( filePathArray ) ) {
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( BuildTaskDefineId.masterDeletePath.toString() );
				propertyEntity.setValue	( convertValue( filePathArray ));

				propertys.add( propertyEntity );
			}



			String[] binaryFilePathArray = binaryFilePathList.toArray( new String[0] );

			if( true != TriStringUtils.isEmpty( binaryFilePathArray ) ) {
				ITaskPropertyEntity propertyEntity = new TaskPropertyEntity();

				propertyEntity.setName	( BuildTaskDefineId.masterBinaryDeletePath.toString() );
				propertyEntity.setValue	( convertValue( binaryFilePathArray ));

				propertys.add( propertyEntity );
			}
		}
	}

	/**
	 * 指定された文字列配列をカンマ(,)編集した単一文字列に変換します。
	 *
	 * @param values
	 * @return 編集した単一文字列を戻します。<code>values</code>に何もない場合は、空文字を戻します。
	 */
	private final String convertValue( String[] values ) {

		if ( TriStringUtils.isEmpty( values ) ) {
			return "";
		}

		StringBuilder buf = new StringBuilder();
		buf.append( TriStringUtils.convertArrayToString( values ) ) ;

		return buf.toString();
	}
}
