package jp.co.blueship.tri.bm.domain.bp.beans;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseRequestServiceBean;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.constants.status.AmLotBlStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.SmProcMgtStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.svc.beans.ActionPojoAbstract;

/**
 * クローズ対象のビルドパッケージ単位でベースラインのステータスをエラーに更新します。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 * 
 * @version SP-20150701_V3L13R01
 * @author Takashi Ono
 */
public class ActionChangeStatusToLotCheckInErrorByBpClose extends ActionPojoAbstract<FlowRelUnitCloseRequestServiceBean> {

	private BmFinderSupport support = null;
	/**
	 * アクセスインタフェースがインスタンス生成時に自動的に設定されます。
	 * @param support アクセスインタフェース
	 */
	public void setSupport( BmFinderSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseRequestServiceBean> execute( IServiceDto<FlowRelUnitCloseRequestServiceBean> serviceDto ) {

		FlowRelUnitCloseRequestServiceBean paramBean = serviceDto.getServiceBean();

		// エラーがなければ脱出
		if ( SmProcMgtStatusId.Success.equals( paramBean.getProcMgtStatusId() ) ) {
			return serviceDto;
		}

		this.support.getSmFinderSupport().updateExecDataSts(
				paramBean.getProcId(), AmTables.AM_LOT_BL, AmLotBlStatusIdForExecData.CommitError );

		return serviceDto;
	}

}
