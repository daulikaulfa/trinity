package jp.co.blueship.tri.bm.domain.bp.dto;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * ビルドパッケージ作成・ビルドパッケージ状況確認用
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitHistoryDetailViewServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 2L;

	/** ビルドパッケージ番号 */
	private String selectedBuildNo = null;
	/** ステータス */
	private String status = null;
	/** パッケージ名 */
	private String unitName = null;
	/** パッケージ概要 */
	private String unitSummary = null;
	/** ロット番号 */
	private String selectedLotNo = null;
	/** ロット名 */
	private String lotName = null;
	/** ベースラインタグ */
	private String baseLineTag = null;
	/** バージョンタグ */
	private String versionTag = null;
	/** 実行者 */
	private String executeUser = null;
	/** 実行者ＩＤ */
	private String executeUserId = null;
	/** 実行開始日時 */
	private Timestamp startDate = null;
	/** 実行終了日時 */
	private Timestamp endDate = null;

	/** 業務シーケンス上で資産集計が行われたかどうか */
	private boolean isConcentrate = false;

	/** 業務シーケンス上でバイナリ資産集計が行われたかどうか */
	private boolean isBinaryConcentrate = false;

	/** 貸出資産総数 */
	private int lendAssetSumCount = 0;
	/** 変更資産総数 */
	private int changeAssetSumCount = 0;
	/** 削除資産総数 */
	private int deleteAssetSumCount = 0;
	/** 削除バイナリ資産総数 */
	private int deleteBinaryAssetSumCount = 0;
	/** 新規資産総数 */
	private int addAssetSumCount = 0;
	/** 変更管理情報 */
	private List<PjtViewBean> pjtViewBeanList = null;
	/** 処理結果 */
	private List<UnitResultViewBean> unitResultViewBeanList = null;


	public String getSelectedBuildNo() {
		return selectedBuildNo;
	}
	public void setSelectedBuildNo( String selectedBuildNo ) {
		this.selectedBuildNo = selectedBuildNo;
	}

	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}

	public String getUnitName() {
		return unitName;
	}
	public void setUnitName(String unitName) {
		this.unitName = unitName;
	}

	public String getUnitSummary() {
		return unitSummary;
	}
	public void setUnitSummary(String unitSummary) {
		this.unitSummary = unitSummary;
	}

	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getBaseLineTag() {
		return baseLineTag;
	}
	public void setBaseLineTag(String baseLineTag) {
		this.baseLineTag = baseLineTag;
	}
	public String getVersionTag() {
		return versionTag;
	}
	public void setVersionTag(String versionTag) {
		this.versionTag = versionTag;
	}

	public String getExecuteUser() {
		return executeUser;
	}
	public void setExecuteUser(String executeUser) {
		this.executeUser = executeUser;
	}

	public Timestamp getStartDate() {
		return startDate;
	}
	public void setStartDate( Timestamp startDate) {
		this.startDate = startDate;
	}

	public Timestamp getEndDate() {
		return endDate;
	}
	public void setEndDate( Timestamp endDate) {
		this.endDate = endDate;
	}

	public String getExecuteUserId() {
		return executeUserId;
	}
	public void setExecuteUserId(String executeUserId) {
		this.executeUserId = executeUserId;
	}

	public boolean isConcentrate() {
		return isConcentrate;
	}
	public void setConcentrate(boolean isConcentrate) {
		this.isConcentrate = isConcentrate;
	}

	public boolean isBinaryConcentrate() {
		return isBinaryConcentrate;
	}
	public void setBinaryConcentrate(boolean isBinaryConcentrate) {
		this.isBinaryConcentrate = isBinaryConcentrate;
	}

	public int getAddAssetSumCount() {
		return addAssetSumCount;
	}
	public void setAddAssetSumCount(int addAssetSumCount) {
		this.addAssetSumCount = addAssetSumCount;
	}
	public int getChangeAssetSumCount() {
		return changeAssetSumCount;
	}
	public void setChangeAssetSumCount(int changeAssetSumCount) {
		this.changeAssetSumCount = changeAssetSumCount;
	}
	public int getDeleteAssetSumCount() {
		return deleteAssetSumCount;
	}
	public void setDeleteAssetSumCount(int deleteAssetSumCount) {
		this.deleteAssetSumCount = deleteAssetSumCount;
	}
	public int getDeleteBinaryAssetSumCount() {
		return deleteBinaryAssetSumCount;
	}
	public void setDeleteBinaryAssetSumCount(int deleteBinaryAssetSumCount) {
		this.deleteBinaryAssetSumCount = deleteBinaryAssetSumCount;
	}
	public int getLendAssetSumCount() {
		return lendAssetSumCount;
	}
	public void setLendAssetSumCount(int lendAssetSumCount) {
		this.lendAssetSumCount = lendAssetSumCount;
	}

	public List<PjtViewBean> getPjtViewBeanList() {
		if ( null == pjtViewBeanList ) {
			pjtViewBeanList = new ArrayList<PjtViewBean>();
		}
		return pjtViewBeanList;
	}
	public void setPjtViewBeanList(
			List<PjtViewBean> pjtViewBeanList) {
		this.pjtViewBeanList = pjtViewBeanList;
	}

	public List<UnitResultViewBean> getUnitResultViewBeanList() {
		if ( null == unitResultViewBeanList ) {
			unitResultViewBeanList = new ArrayList<UnitResultViewBean>();
		}
		return unitResultViewBeanList;
	}
	public void setUnitResultViewBeanList(
			List<UnitResultViewBean> unitResultViewBeanList) {
		this.unitResultViewBeanList = unitResultViewBeanList;
	}

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public PjtViewBean newPjtViewBean() {
		PjtViewBean bean = new PjtViewBean();
		return bean;
	}

	public class PjtViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/** 変更管理番号 */
		private String pjtNo = null;
		/** 変更要因番号 */
		private String changeCauseNo = null;
		/** 返却申請情報 */
		private List<ApplyInfoViewBean> applyInfoViewBeanList = null;


		public void setPjtNo( String pjtNo ) {
			this.pjtNo = pjtNo;
		}
		public String getPjtNo() {
			return pjtNo;
		}

		public String getChangeCauseNo() {
			return changeCauseNo;
		}
		public void setChangeCauseNo(String changeCauseNo) {
			this.changeCauseNo = changeCauseNo;
		}

		public List<ApplyInfoViewBean> getApplyInfoViewBeanList() {
			if ( null == applyInfoViewBeanList ) {
				applyInfoViewBeanList = new ArrayList<ApplyInfoViewBean>();
			}
			return applyInfoViewBeanList;
		}
		public void setApplyInfoViewBeanList(
				List<ApplyInfoViewBean> applyInfoViewBeanList ) {
			this.applyInfoViewBeanList = applyInfoViewBeanList;
		}


		/**
		 * 新しいインスタンスを取得します。
		 *
		 * @return 取得した情報を戻します。
		 */
		public ApplyInfoViewBean newApplyInfoViewBean() {
			ApplyInfoViewBean bean = new ApplyInfoViewBean();
			return bean;
		}

		public class ApplyInfoViewBean implements Serializable {

			private static final long serialVersionUID = 1L;

			/** 業務シーケンス上で資産集計が行われたかどうか */
			private boolean isConcentrate = false;

			/** 申請番号 */
			private String applyNo = null;
			/** 貸出資産数 */
			private int lendAssetCount = 0;
			/** 返却資産数 */
			private int returnAssetCount = 0;
			/** 新規資産数 */
			private int addAssetCount = 0;
			/** 変更資産数 */
			private int changeAssetCount = 0;
			/** 削除資産数 */
			private int deleteAssetCount = 0;

			public boolean isConcentrate() {
				return isConcentrate;
			}
			public void setConcentrate(boolean isConcentrate) {
				this.isConcentrate = isConcentrate;
			}
			public void setApplyNo( String applyNo ) {
				this.applyNo = applyNo;
			}
			public String getApplyNo() {
				return applyNo;
			}

			public void setLendAssetCount( int lendAssetCount ) {
				this.lendAssetCount = lendAssetCount;
			}
			public int getLendAssetCount() {
				return lendAssetCount;
			}

			public void setReturnAssetCount( int returnAssetCount ) {
				this.returnAssetCount = returnAssetCount;
			}
			public int getReturnAssetCount() {
				return returnAssetCount;
			}

			public void setAddAssetCount( int addAssetCount ) {
				this.addAssetCount = addAssetCount;
			}
			public int getAddAssetCount() {
				return addAssetCount;
			}

			public void setChangeAssetCount( int changeAssetCount ) {
				this.changeAssetCount = changeAssetCount;
			}
			public int getChangeAssetCount() {
				return changeAssetCount;
			}

			public void setDeleteAssetCount( int deleteAssetCount ) {
				this.deleteAssetCount = deleteAssetCount;
			}
			public int getDeleteAssetCount() {
				return deleteAssetCount;
			}

		}
	}

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public UnitResultViewBean newUnitResultViewBean() {
		UnitResultViewBean bean = new UnitResultViewBean();
		return bean;
	}

	public class UnitResultViewBean implements Serializable {

		private static final long serialVersionUID = 1L;

		/** 処理順序 */
		private String processOrder = null;
		/** 処理名 */
		private String processName = null;
		/** 処理結果 */
		private boolean processResult = false;


		public void setProcessOrder( String processOrder ) {
			this.processOrder = processOrder;
		}
		public String getProcessOrder() {
			return processOrder;
		}

		public String getProcessName() {
			return processName;
		}
		public void setProcessName( String processName ) {
			this.processName = processName;
		}

		public boolean getProcessResult() {
			return processResult;
		}
		public void setProcessResult( boolean processResult ) {
			this.processResult = processResult;
		}
	}
}
