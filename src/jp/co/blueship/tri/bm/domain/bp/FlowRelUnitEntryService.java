package jp.co.blueship.tri.bm.domain.bp;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * ビルドパッケージ作成・ビルドパッケージ作成受付完了画面の表示情報設定Class<br>
 * <p>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitEntryService implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	private List<IDomain<IGeneralServiceBean>> pojoList;

	/**
	 * 実行されるビジネス処理のリストを設定します。
	 * @param pojoList 一括して実行するリスト
	 */
	public final void setActions( List<IDomain<IGeneralServiceBean>> pojoList ) {
		this.pojoList = pojoList;
	}

	@Override
	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID = paramBean.getReferer();
			String forwordID = paramBean.getForward();

			if ( !refererID.equals( RelUnitScreenID.COMP_REQUEST ) &&
					!forwordID.equals( RelUnitScreenID.COMP_REQUEST ) ){
				return serviceDto;
			}

			if ( refererID.equals( RelUnitScreenID.COMP_REQUEST )) {
			}

			if ( forwordID.equals( RelUnitScreenID.COMP_REQUEST )) {
				if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
					paramBean.setBuildNo( this.support.getBpNumberingDao().nextval() );

					// RMI対策
					paramBean.setBuildNo( paramBean.getBuildNo() );

					// グループの存在チェック
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );
					List<IGrpUserLnkEntity> groupUsers	=support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );


					//下層のアクションを実行する
					{
						IServiceDto<IGeneralServiceBean> innetDto = new ServiceDto<IGeneralServiceBean>();
						innetDto.setServiceBean( serviceDto.getServiceBean() );

						for ( IDomain<IGeneralServiceBean> pojo : this.pojoList ) {
							innetDto = pojo.execute( innetDto );
						}
					}
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005080S,e);
		}
	}

}