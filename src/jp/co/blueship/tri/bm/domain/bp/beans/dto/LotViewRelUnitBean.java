package jp.co.blueship.tri.bm.domain.bp.beans.dto;

import java.io.Serializable;

/**
 * ビルドパッケージ作成・ロット一覧用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class LotViewRelUnitBean implements Serializable {

	private static final long serialVersionUID = 1L;

	/** サーバ番号 */
	private String serverNo = null;
	/** ロット番号 */
	private String lotId = null;
	/** ロット名 */
	private String lotName = null;
	/** 作成者 */
	private String inputUser = null;
	/** 作成者ＩＤ */
	private String inputUserId = null;
	/** 作成日 */
	private String inputDate = null;

	
	public String getServerNo() {
		return serverNo;
	}
	public void setServerNo( String serverNo ) {
		this.serverNo = serverNo;
	}
	
	public String getLotNo() {
		return lotId;
	}
	public void setLotNo(String lotId) {
		this.lotId = lotId;
	}

	public String getLotName() {
		return lotName;
	}
	public void setLotName(String lotName) {
		this.lotName = lotName;
	}

	public String getInputUser() {
		return inputUser;
	}
	public void setInputUser(String inputUser) {
		this.inputUser = inputUser;
	}

	public String getInputDate() {
		return inputDate;
	}
	public void setInputDate(String inputDate) {
		this.inputDate = inputDate;
	}
	public String getInputUserId() {
		return inputUserId;
	}
	public void setInputUserId(String inputUserId) {
		this.inputUserId = inputUserId;
	}
}
