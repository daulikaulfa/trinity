package jp.co.blueship.tri.bm.domain.bp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.AreqFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqDto;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.LotViewRelUnitBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitComfirmBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitEntryServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport.PjtNoBean;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;


/**
 * ビルドパッケージ作成・ビルドパッケージ作成確認画面の表示情報設定Class<br>
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitEntryServiceUnitComfirm implements IDomain<FlowRelUnitEntryServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}
	private ActionStatusMatrixList statusMatrixAction = null;
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	public IServiceDto<FlowRelUnitEntryServiceBean> execute( IServiceDto<FlowRelUnitEntryServiceBean> serviceDto ) {

		FlowRelUnitEntryServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelUnitScreenID.UNIT_COMFIRM ) &&
					!forwordID.equals( RelUnitScreenID.UNIT_COMFIRM ) ){
				return serviceDto;
			}
			//メッセージ初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;

			String[] pjtNo = this.support.mergePjtNo( paramBean , true );
			String[] pjtNoNotIncludeClosedPjt = this.support.mergePjtNo( paramBean , false );

			String[] applyNoArray = this.getApplyNoArray( pjtNo ) ;

			if ( refererID.equals( RelUnitScreenID.UNIT_COMFIRM )) {

				if ( ScreenType.next.equals( screenType ) &&
						!forwordID.equals( RelUnitScreenID.UNIT_COMFIRM )) {

					checkRelUnitEntry( paramBean ) ;

					if( paramBean.isStatusMatrixV3() ) {
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( paramBean.getSelectedLotNo() )
						.setPjtIds( pjtNoNotIncludeClosedPjt )
						.setAreqIds( applyNoArray )
						.setBpIds( support.getBuildNo( support.getBpEntityByLotNo(paramBean.getSelectedLotNo()) ) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( RelUnitScreenID.UNIT_COMFIRM )) {
				if ( ScreenType.next.equals( screenType ) ||
						ScreenType.select.equals( screenType ) ) {

					if ( !StatusFlg.on.value().equals( paramBean.getAllAssetCompile() )) {
						ItemCheckAddonUtils.checkPjtNo( pjtNo );
					}

					String selectedLotNo = paramBean.getSelectedLotNo();
					ItemCheckAddonUtils.checkLotNo( selectedLotNo );


					// ロットエンティティ
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( selectedLotNo );
					// ２重貸出チェック実施（重複貸出が許可されている場合）
					if ( lotDto.getLotEntity().getIsAssetDup().parseBoolean() ) {

						// ビルドパッケージに含まれている申請情報
						List<IAreqEntity> entitiyList =
							this.support.getAssetApplyEntity( paramBean.getApplyInfoViewBeanList() );

						List<IAreqDto> areqDtoEntities = new ArrayList<IAreqDto>();
						for ( IAreqEntity areqEntity: entitiyList ) {
							IAreqDto areqDto = new AreqDto();
							areqDto.setAreqEntity( areqEntity );

							AreqFileCondition fileCondition = new AreqFileCondition();
							fileCondition.setAreqId( areqEntity.getAreqId() );

							areqDto.setAreqFileEntities( this.support.getAmFinderSupport().getAreqFileDao().find( fileCondition.getCondition() ) );
							areqDtoEntities.add( areqDto );
						}
						// 承認の資産同士の重複はエラー
						// 承認とクローズの資産同士の重複は、時間の関係をチェックしたうえでエラー
						checkAssetDuplicateError( lotDto, areqDtoEntities );

					}

					UnitComfirmBean unitConfirmBean = paramBean.getUnitComfirmBean();

					// パッケージ入力情報
					setUnitInfoInputUnitConfirm(
							paramBean.getUnitInfoBean().getUnitInfoInputBean(), unitConfirmBean );

					// ロット情報
					setLotUnitConfirm( selectedLotNo, unitConfirmBean );

					// 変更管理情報
					PjtNoBean pjtNoBean = null;
					if ( !StatusFlg.on.value().equals( paramBean.getAllAssetCompile() )) {

						pjtNoBean = this.support.getPjtNoBean( paramBean );

						if ( 0 == pjtNoBean.getApprovedPjtNo().size() ) {
							throw new ContinuableBusinessException( BmMessageId.BM001004E );
						}

						setPjtUnitConfirm( pjtNo, unitConfirmBean, pjtNoBean );

					}

					//クローズエラー有無チェック
					this.checkUnitCloseError( lotDto.getLotEntity() , paramBean ) ;

					paramBean.setUnitComfirmBean( unitConfirmBean );
				}
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005083S,e);
		}
	}

	/**
	 * ビルドパッケージに含まれるの資産同士が重複していないかをチェックする
	 * @param lotDto ロットdto
	 * @param entityList ビルドパッケージに含まれる申請情報エンティティリスト
	 */

	private void checkAssetDuplicateError(
			ILotDto lotDto,
			List<IAreqDto> entityList ) throws BaseBusinessException {

		// 資産パスと申請情報リストのマップ
		Map<String,List<IAreqEntity>> assetPathAssetApplyEntityListMap =
			this.support.getReturnAssetPathAsseApplyEntityMap( entityList );


		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			for ( String assetPath : assetPathAssetApplyEntityListMap.keySet() ) {

				List<IAreqEntity> checkEntityList = assetPathAssetApplyEntityListMap.get( assetPath );

				// 重複あり
				if ( 1 < checkEntityList.size() ) {

					// 最初の承認ステータスを比較元に
					IAreqEntity origEntity = null;
					for ( IAreqEntity entity : checkEntityList ) {

						if ( AmAreqStatusId.CheckinRequestApproved.equals( entity.getStsId() )) {
							origEntity = entity;
							break;
						}
					}

					// この重複パスには、返却承認は含まれない ＝ 全部クローズ済み
					// クローズされてたらどうにもならんのでスルーする
					if ( null == origEntity ) continue;


					// 残りを比較先としてまわす
					for ( IAreqEntity destEntity : checkEntityList ) {

						// すでにクローズされている案件はチェック対象外とする
						// => クローズ時に警告しているので
						if ( !AmAreqStatusId.CheckinRequestApproved.equals( destEntity.getStsId() )) continue;

						// 比較元は除外する
						if ( !origEntity.getAreqId().equals( destEntity.getAreqId() )) {

							// 内容が同じならセーフ
							// 片や返却承認、片やクローズでクローズ日時＜貸出日時であればセーフ
							if ( !this.support.isSameContents( lotDto, origEntity, destEntity, assetPath ) &&
									!this.support.isCloseDateOlderLendDate( origEntity, destEntity )) {

								messageList.add		( BmMessageId.BM001005E );
								messageArgsList.add	( new String[] {
										origEntity.getPjtId(),
										origEntity.getAreqId(),
										assetPath,
										destEntity.getPjtId(),
										destEntity.getAreqId() } );

							}
						}
					}
				}
			}
		} catch ( IOException e ) {
			throw new TriSystemException ( BmMessageId.BM005065S , e );
		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ビルドパッケージが作成可能かをチェックする
	 * @param unitInfoInputBean ビルドパッケージ入力情報
	 */

	private static void checkRelUnitEntry( FlowRelUnitEntryServiceBean bean )
			throws BaseBusinessException {

		//パッケージクローズエラーチェック
		WarningCheckBean warningCheckAssetUnitCloseError = bean.getWarningCheckUnitCloseError() ;
		if ( warningCheckAssetUnitCloseError.isExistWarning() ) {

			if ( !warningCheckAssetUnitCloseError.isConfirmCheck() ) {
				throw new ContinuableBusinessException( BmMessageId.BM001009E );
			}
		}
	}

	/**
	 * パッケージ情報入力ビーンからパッケージ情報確認ビーンに値をセットする。
	 *
	 */
	private void setUnitInfoInputUnitConfirm(
			UnitInfoInputBean unitInputBean, UnitComfirmBean unitComfirmBean ) {

		unitComfirmBean.setUnitName		( unitInputBean.getUnitName() );
		unitComfirmBean.setUnitSummary	( unitInputBean.getUnitSummary() );
	}

	/**
	 * ロットの情報をパッケージ情報確認ビーンに値をセットする。
	 *
	 */
	private void setLotUnitConfirm(
			String lotId, UnitComfirmBean unitComfirmBean ) {

		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(lotId);
		IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();

		LotViewRelUnitBean lotViewBean = new LotViewRelUnitBean();
		BmViewInfoAddonUtils.setLotViewBeanPjtLotEntity( lotViewBean, lotDto, srvEntity.getBldSrvId() );

		unitComfirmBean.setLotViewBean( lotViewBean );
	}

	/**
	 * 変更管理の情報をパッケージ情報確認ビーンに値をセットする。
	 * @param pjtNo 変更管理番号
	 * @param unitComfirmBean パッケージ情報確認ビーン
	 * @param pjtNoBean 変更管理番号ビーン
	 */
	private void setPjtUnitConfirm(
			String[] pjtNo, UnitComfirmBean unitComfirmBean,
			FlowRelUnitEditSupport.PjtNoBean pjtNoBean ) {

		IJdbcCondition condition = DBSearchConditionAddonUtil.getPjtCondition( pjtNo );
		ISqlSort sort = DBSearchSortAddonUtil.getPjtSortFromDesignDefineByRelUnitEntryConfirm();

//		int selectedPageNo	=
//			(( 0 == unitComfirmBean.getSelectPageNo() )? 1: unitComfirmBean.getSelectPageNo() );
//		int maxPageNumber	=
//			DesignProjectUtil.getMaxPageNumber(
//					DesignProjectRelDefineId.maxPageNumberByPjtList );

		int selectedPageNo	= 1;
		int maxPageNumber	= 0;

		IEntityLimit<IPjtEntity> limit =
			this.support.getAmFinderSupport().getPjtDao().find( condition.getCondition(), sort, selectedPageNo, maxPageNumber );


		List<PjtViewBean> closedPjtViewBeanList	= new ArrayList<PjtViewBean>();
		List<PjtViewBean> pjtViewBeanList		= new ArrayList<PjtViewBean>();

		this.support.setPjtViewBean(
				limit.getEntities().toArray(new IPjtEntity[0]), pjtNoBean, closedPjtViewBeanList, pjtViewBeanList );

		unitComfirmBean.setClosedPjtViewBeanList( closedPjtViewBeanList );
		unitComfirmBean.setPjtViewBeanList		( pjtViewBeanList );


//		unitComfirmBean.setPageInfoView		(
//				AddonUtil.convertPageNoInfo( new PageNoInfo(), pjtEntityLimit.getLimit() ));
//		unitComfirmBean.setSelectPageNo		( selectedPageNo );
	}

//	/**
//	 * ビルドパッケージ実行中の全ビルドパッケージエンティティを取得する。
//	 *
//	 */
//	private IBuildEntity[] getActiveBuildEntity() {
//
//		ICondition condition = DBSearchConditionAddonUtil.getActiveBuildCondition();
//
//		IBuildEntityLimit entityLimit = this.support.getBuildDao().find( condition, null, 1, 0 );
//
//		return entityLimit.getEntity();
//	}
	/**
	 * 変更管理番号に紐付く申請情報の申請番号を取得する。
	 * @param pjtNoArray 変更管理番号の配列
	 * @return 申請番号の配列
	 */
	private String[] getApplyNoArray( String[] pjtNoArray ) {

		if ( TriStringUtils.isEmpty( pjtNoArray ) ) {
			//変更管理が１件も選択されていなければ、紐付く申請情報を取得しない。
			return new String[0];
		}

		AreqCondition condition = (AreqCondition)DBSearchConditionAddonUtil.getAreqConditionByGeneratableRelUnitByPjtNo( pjtNoArray ) ;
		ISqlSort iSort = new SortBuilder() ;

		IEntityLimit<IAreqEntity> assetApplyEntityLimit = this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition() , iSort , 1 , 0 ) ;
		IAreqEntity[] assetApplyEntityArray = assetApplyEntityLimit.getEntities().toArray(new IAreqEntity[0]) ;

		List<String> applyNoList = new ArrayList<String>() ;

		for( IAreqEntity assetApplyEntity : assetApplyEntityArray ) {
			applyNoList.add( assetApplyEntity.getAreqId() ) ;
		}

		return applyNoList.toArray( new String[ 0 ] ) ;
	}

	/**
	 *
	 * @param pjtLotEntity
	 * @param paramBean
	 */

	private void checkUnitCloseError( ILotEntity pjtLotEntity , FlowRelUnitEntryServiceBean paramBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		IBpEntity[] buildEntityArray = this.support.getBuildEntityByRelUnitCloseError( pjtLotEntity.getLotId() ) ;
		for( IBpEntity buildEntity : buildEntityArray ) {
			messageList.add( BmMessageId.BM001023E ) ;
			messageArgsList.add( new String[]{ buildEntity.getBpId() } ) ;
		}

		if( null == paramBean.getInfoMessageIdList() ) {
			paramBean.setInfoMessageIdList( messageList );
		} else {
			paramBean.getInfoMessageIdList().addAll( messageList ) ;
		}
		if( null == paramBean.getInfoMessageArgsList() ) {
			paramBean.setInfoMessageArgsList( messageArgsList );
		} else {
			paramBean.getInfoMessageArgsList().addAll( messageArgsList ) ;
		}

		WarningCheckBean warningCheck = new WarningCheckBean() ;
		warningCheck.setExistWarning	( ( 0 != messageList.size() ) ? true : false ) ;

		//メッセージ生成
		IContextAdapter ca = ContextAdapterFactory.getContextAdapter() ;

		//パッケージクローズエラー有無チェック
		List<String> commentListUnitCloseError	= warningCheck.getCommentList() ;
		//[0]
		commentListUnitCloseError.add( ca.getMessage( paramBean.getLanguage(), BmMessageId.BM001008E ) ) ;
		//[1]
		String logPath = TriStringUtils.convertPath( BmDesignBusinessRuleUtils.getWorkspaceLogPath( pjtLotEntity, paramBean.getFlowAction() ) ) ;
		commentListUnitCloseError.add( ca.getMessage( paramBean.getLanguage(), BmMessageId.BM001024E, logPath ) ) ;

		paramBean.setWarningCheckUnitCloseError( warningCheck ) ;
	}
}
