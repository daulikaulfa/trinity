package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

/**
 * ビルドパッケージ作成・ビルドパッケージクローズ一覧画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitCloseHistoryListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択されたロット番号 */	
	private String selectedLotNo = null;
	/** ロット名 */	
	private String lotName = null;
	/** ビルドパッケージ情報 */
	private List<UnitCloseViewBean> unitViewBeanList = new ArrayList<UnitCloseViewBean>();
	/** 検索条件情報 */
	private RelUnitSearchBean relUnitSearchBean = null;

	
	public String getSelectedLotNo() {
		return selectedLotNo;
	}
	public void setSelectedLotNo( String selectedLotNo ) {
		this.selectedLotNo = selectedLotNo;
	}
	
	public String getLotName() {
		return lotName;
	}
	public void setLotName( String lotName ) {
		this.lotName = lotName;
	}
	
	public List<UnitCloseViewBean> getUnitViewBeanList() {
		return unitViewBeanList;
	}
	public void setUnitViewBeanList( List<UnitCloseViewBean> unitViewBeanList ) {
		this.unitViewBeanList = unitViewBeanList;
	}
	public RelUnitSearchBean getRelUnitSearchBean() {
		return relUnitSearchBean;
	}
	public void setRelUnitSearchBean(RelUnitSearchBean relUnitSearchBean) {
		this.relUnitSearchBean = relUnitSearchBean;
	}

}
