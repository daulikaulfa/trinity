package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import jp.co.blueship.tri.agent.cmn.utils.TaskImageResourceCache;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvSrvEntity;
import jp.co.blueship.tri.bm.dao.bldsrv.eb.IBldSrvEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskTargetEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.GenerateDetailViewBean.BuildProcessViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCompileDetailViewServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * ビルドパッケージ作成・ビルドパッケージ作成状況画面の表示情報設定Class<br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowRelUnitCompileDetailViewService implements IDomain<FlowRelUnitCompileDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static final String SEP = LogBusinessFlow.getSystemLineSeparator() ;
	//private static final String IMAGE_NEXT_ARROW_ON = "pci_wait_arrow2.gif";
	//private static final String IMAGE_NEXT_ARROW_OFF = "pci_wait_arrow2_off.gif";
	private static final String WAIT		= "wait" ;//"pci-wait_die.gif";

	private static final String STATUS_ACTIVE			= "ACTIVE" ;
	private static final String STATUS_COMPLETE		= "COMPLETE" ;
	private static final String STATUS_ERROR			= "ERROR" ;

	private static final String relUnitStatusMsgPrefix = "ビルドパッケージ作成";
	private static final String[] STATE_MSG = {
		relUnitStatusMsgPrefix + "処理は実行されていません。",
		relUnitStatusMsgPrefix + "処理が実行中です。",
		relUnitStatusMsgPrefix + "処理中にエラーが発生しました。",
		relUnitStatusMsgPrefix + "処理は既に終了しています。",
		"ロットを選択してください。"
	};

	private FlowRelUnitEditSupport support;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public IServiceDto<FlowRelUnitCompileDetailViewServiceBean> execute( IServiceDto<FlowRelUnitCompileDetailViewServiceBean> serviceDto ) {

		FlowRelUnitCompileDetailViewServiceBean paramBean	= null;

		StringBuilder stbMessage = new StringBuilder() ;

		try {
			paramBean	= serviceDto.getServiceBean();

			// 遷移元画面ID
			String refererID = paramBean.getReferer();
			// 遷移先画面ID
			String forwordID = paramBean.getForward();

			// 遷移元画面IDによる処理を行う
			if( refererID.equals( RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW )) {
			}

			// 遷移先画面IDによる処理を行う
			if( forwordID.equals( RelUnitScreenID.UNIT_GENERATE_DETAIL_VIEW )) {
				String selectedLotNo = paramBean.getSelectedLotNo();

				List<ILotDto> pjtLotEntityArray = null ;
				List<IGrpUserLnkEntity> groupUserEntityArray = null ;
				Map<String,Object> lotInfoMap = TaskImageResourceCache.getLotInfo( paramBean.getUserId() ) ;
				boolean needRefreshFlg = false ;
				if ( null == lotInfoMap ) {
					needRefreshFlg = true ;
				} else {
					pjtLotEntityArray = (List<ILotDto>)lotInfoMap.get( ILotDto.class.getSimpleName() ) ;
					groupUserEntityArray = (List<IGrpUserLnkEntity>)lotInfoMap.get( IGrpUserLnkEntity.class.getSimpleName() ) ;
					if( ! isContainsSelectedPjtLotEntity( pjtLotEntityArray , selectedLotNo ) ) {
						needRefreshFlg = true ;//既存キャッシュにないロットの場合、キャッシュを最新化する
					}
				}
				if( needRefreshFlg ) {
					pjtLotEntityArray = this.getActivePjtLotEntityArray() ;
					groupUserEntityArray = support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
					TaskImageResourceCache.setLotInfoResourceCache( paramBean.getUserId() , pjtLotEntityArray , groupUserEntityArray );
				}

				// ロットコンボ
				paramBean.setLotList( this.makeLotList( pjtLotEntityArray , groupUserEntityArray ) ) ;

				if ( TriStringUtils.isEmpty( selectedLotNo )) {
					setMessage( paramBean, STATE_MSG[4] );
					return serviceDto;
				} else {
					// グループの存在チェック
					ILotEntity pjtLotEntity	= this.getSelectedPjtLotEntity( pjtLotEntityArray , selectedLotNo ) ;
					if( TriStringUtils.isEmpty( pjtLotEntity ) ) {
						pjtLotEntity = this.support.getAmFinderSupport().findLotEntity( selectedLotNo ) ;
						if( TriStringUtils.isEmpty( pjtLotEntity ) ) {
							throw new TriSystemException( BmMessageId.BM004060F , selectedLotNo ) ;
						}
					}
					ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(pjtLotEntity);
					RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUserEntityArray );
				}

				// ラインが１件も存在しない場合
				if( true == checkBpTaskIsEmpty( selectedLotNo ) ) {

					setMessage( paramBean, STATE_MSG[0] );
					return serviceDto;
				}

				String buildNo	= this.getRecentBuildNo( paramBean );


				ILotEntity lotEntity = null;
				IBldEnvEntity bldEnvEntity = null;
				IBldEnvSrvEntity[] buildEnvServerEntitys = new IBldEnvSrvEntity[0];
				IBpDto buildEntity;
				IBldTimelineEntity[] timeLineEntitys	= new IBldTimelineEntity[0];
				ITaskFlowEntity[] taskFlowEntitys = new ITaskFlowEntity[0] ;

				Map<String,Object> resMap = TaskImageResourceCache.getBuildResource( buildNo ) ;
				if ( null == resMap ) {
					buildEntity = this.support.findBpDto(buildNo);
					bldEnvEntity = this.support.findBldEnvEntity( buildEntity.getBpEntity().getBldEnvId() );
					buildEnvServerEntitys = this.support.getBldEnvSrvEntity( buildEntity.getBpEntity().getBldEnvId() );
					lotEntity = this.support.getAmFinderSupport().findLotEntity( buildEntity.getBpEntity().getLotId() );
					timeLineEntitys	= this.support.getBldTimelineEntity( buildEntity.getBpEntity().getBldEnvId() );
					taskFlowEntitys = this.support.getTaskFlowEntity();

					TaskImageResourceCache.setBuildResourceCache(
							lotEntity, bldEnvEntity, buildEnvServerEntitys, buildEntity, timeLineEntitys , taskFlowEntitys );
				} else {
					buildEntity = (IBpDto)resMap.get( IBpDto.class.getSimpleName() ) ;
					bldEnvEntity = (IBldEnvEntity)resMap.get(IBldEnvEntity.class.getSimpleName() ) ;
					buildEnvServerEntitys = (IBldEnvSrvEntity[])resMap.get( IBldEnvSrvEntity.class.getSimpleName() ) ;
					lotEntity = (ILotEntity)resMap.get( ILotEntity.class.getSimpleName() ) ;
					timeLineEntitys = (IBldTimelineEntity[]) resMap.get(IBldTimelineEntity.class.getSimpleName() );
					taskFlowEntitys = (ITaskFlowEntity[])resMap.get( ITaskFlowEntity.class.getSimpleName() ) ;
				}

				paramBean.setBuildNo( buildNo );


				if ( 0 == timeLineEntitys.length ) {
					throw new TriSystemException(BmMessageId.BM004001F , ((null != buildEntity)? buildEntity.getBpEntity().getBldEnvId() : "null") );
				}


				//ラインの処理状況を取得
				TaskFlowProcCondition condition = new TaskFlowProcCondition();
				condition.setProcId(buildEntity.getBpEntity().getProcId());
				ITaskFlowProcEntity[] process = this.support.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);

				// ラインが存在しない場合
				if(false == isProcessExist(process)) {
					GenerateDetailViewBean generateDetailViewBean = new GenerateDetailViewBean();

					generateDetailViewBean.setBuildNo					( buildNo );
					generateDetailViewBean.setExecuteUser				( buildEntity.getBpEntity().getExecUserNm() );
					generateDetailViewBean.setExecuteUserId				( buildEntity.getBpEntity().getExecUserId() );
					generateDetailViewBean.setBuildProcessViewBeanList	( new ArrayList<BuildProcessViewBean>() );

					generateDetailViewBean.setBuildStatusMsg( STATE_MSG[0] );

					paramBean.setGenerateDetailViewBean( generateDetailViewBean );
					return serviceDto;
				} else {//buildProcess中にメッセージがあれば、付加情報を増やすために連結して渡す
					for( ITaskFlowProcEntity buildProcess : process ) {
						if( !TriStringUtils.isEmpty( buildProcess.getMsg() ) ) {
							stbMessage.append( buildProcess.getMsg() + SEP ) ;
						}
					}
				}

				//タイムラインの取得情報を正規化
				Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> lineMap = this.normalizeTimeLine( buildNo , timeLineEntitys , lotEntity.getBldEnvId());

				//処理結果の取得情報を正規化
				Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> processMap = this.normalizeProcess( process );

				//システムを変換
				Map<String, IBldSrvEntity> systemMap = convertSystem( TaskImageResourceCache.getBldSrvEntity() );//変更

				//タスクをMap化
				Map<String , ITaskEntity> taskMap = this.mapTask( taskFlowEntitys ) ;


				GenerateDetailViewBean generateDetailViewBean = this.makeGenerateDetailViewBean(
						buildEntity , timeLineEntitys , process ,
						lineMap , processMap , systemMap , taskMap ) ;

				paramBean.setGenerateDetailViewBean( generateDetailViewBean );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005079S,e);
		}

	}

	private void setMessage( FlowRelUnitCompileDetailViewServiceBean paramBean, String message) {

		GenerateDetailViewBean generateDetailViewBean = new GenerateDetailViewBean();

		generateDetailViewBean.setBuildProcessViewBeanList	( new ArrayList<BuildProcessViewBean>() );

		generateDetailViewBean.setBuildStatusMsg( message );

		paramBean.setGenerateDetailViewBean( generateDetailViewBean );
	}


	/**
	 *
	 * @param pjtLotEntityArray
	 * @param selectedLotNo
	 * @return
	 */
	private ILotEntity getSelectedPjtLotEntity( List<ILotDto> pjtLotEntityArray , String selectedLotNo ) {
		for( ILotDto pjtLotEntity : pjtLotEntityArray ) {
			if( pjtLotEntity.getLotEntity().getLotId().equals( selectedLotNo ) ) {
				return pjtLotEntity.getLotEntity() ;
			}
		}
		return null ;
	}

	/**
	 *
	 * @param pjtLotEntityArray
	 * @param selectedLotNo
	 * @return
	 */
	private boolean isContainsSelectedPjtLotEntity( List<ILotDto> pjtLotEntityArray , String selectedLotNo ) {
		for( ILotDto pjtLotEntity : pjtLotEntityArray ) {
			if( pjtLotEntity.getLotEntity().getLotId().equals( selectedLotNo ) ) {
				return true ;
			}
		}
		return false ;
	}

	/**
	 *
	 * @return
	 */
	private List<ILotDto> getActivePjtLotEntityArray() {
		IJdbcCondition condition	= DBSearchConditionAddonUtil.getActiveLotCondition( null );
		ISqlSort sort			= DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelUnitGenerateDetailViewLotSelect();

		IEntityLimit<ILotEntity> limit	= this.support.getAmFinderSupport().getLotDao().find( condition.getCondition(), sort, 1, 0 ) ;
		List<ILotDto> lotDto = this.support.getAmFinderSupport().findLotDto( limit.getEntities() );
		return lotDto ;
	}

	/**
	 *
	 * @param pjtLotDtoArray
	 * @param groupUserEntityArray
	 * @return
	 */
	private List<ItemLabelsBean> makeLotList( List<ILotDto> pjtLotDtoArray , List<IGrpUserLnkEntity> groupUserEntityArray ) {

		List<ILotDto> lotList = new ArrayList<ILotDto>();
		List<ItemLabelsBean> labelList = new ArrayList<ItemLabelsBean>() ;

		BmViewInfoAddonUtils.setAccessablePjtLotEntity(  pjtLotDtoArray, groupUserEntityArray, lotList, null );

		IBldSrvEntity srvEntity = this.support.findBldSrvEntityByController();
		String serverId = srvEntity.getBldSrvId();
		for( ILotDto lotDto : lotList ) {
			ILotEntity pjtLotEntity = lotDto.getLotEntity();

			labelList.add( new ItemLabelsBean( pjtLotEntity.getLotNm(), pjtLotEntity.getLotId() , serverId )) ;
		}

		return labelList ;
	}


	/**
	 * ロットIDに対応したビルドパッケージがTaskFlowProc上に存在するかチェック
	 *
	 * @param lotId ロットId
	 * @return 無ければtrue
	 */
	private final boolean checkBpTaskIsEmpty( String lotId )
	{
		boolean ret = true;
		BpCondition bpCondition = new BpCondition();
		bpCondition.setLotId(lotId);
		List<IBpEntity> bpEntities = this.support.getBmFinderSupport().getBpDao().find(bpCondition.getCondition());
		if( 0<bpEntities.size() ) {
			List<String> procIds = new ArrayList<String>();
			for ( IBpEntity bpEntity : bpEntities ) {
				procIds.add(bpEntity.getProcId());
			}
			TaskFlowProcCondition condition = new TaskFlowProcCondition();
			condition.setProcIds(procIds.toArray(new String[0]));
			int taskCount = this.support.getBmFinderSupport().getTaskFlowProcDao().count( condition.getCondition() );
			if( 0<taskCount ) {
				ret = false;
			}
		}
		return ret;
	}

	/**
	 * ロットIDから最新のBpIdを取得する
	 *
	 * @param lotId ロットId
	 * @return 最新のBpId
	 */
	private final String getTaskBpId( String lotId )
	{
		String bpId = null;
		BpCondition bpCondition = new BpCondition();
		bpCondition.setLotId( lotId );
		ISqlSort sort = new SortBuilder();
		sort.setElement(BpItems.bpId, TriSortOrder.Desc, 1);
		List<IBpEntity> bpEntities = this.support.getBmFinderSupport().getBpDao().find(bpCondition.getCondition(), sort);
		if( bpEntities.size()>0 ) {
			for ( IBpEntity bpEntity : bpEntities ) {
				TaskFlowProcCondition condition = new TaskFlowProcCondition();
				condition.setProcId(bpEntity.getProcId());
				ITaskFlowProcEntity[] buildProcessEntities = this.support.getTaskFlowProcDao().find( condition.getCondition() ).toArray(new ITaskFlowProcEntity[0]);
				if( buildProcessEntities!=null && buildProcessEntities.length>0 ){
					bpId = bpEntity.getBpId();
					break;
				}
			}
		}
		return bpId;
	}


	/**
	 * 最新のビルドパッケージ情報を取得します。
	 *
	 * @param paramBean パラメタ情報
	 * @return 最新のビルドパッケージ番号
	 */
	private final String getRecentBuildNo( FlowRelUnitCompileDetailViewServiceBean paramBean ) {

		String buildNo = paramBean.getBuildNo();
		if ( TriStringUtils.isEmpty( buildNo )) {
			buildNo = getTaskBpId( paramBean.getSelectedLotNo() );
		}
		return buildNo;
	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param timelineEntitys ビルドパッケージ・タイムラインエンティティー
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> normalizeTimeLine(
									String relNo , IBldTimelineEntity[] timelineEntitys , String envNo) {

		IUcfServerEntityComparator comparator = new IUcfServerEntityComparator();

		Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> timeLineMap = new TreeMap<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>>( comparator );

		for ( IBldTimelineEntity timeline : timelineEntitys ) {
			for ( IBldTimelineAgentEntity line : timeline.getLine() ) {

				IBldSrvEntity server = TaskImageResourceCache.findUcfServerEntityByCache(line.getBldSrvId() );

				Map<IBldTimelineEntity, IBldTimelineAgentEntity> lineMap = null;

				if ( (! timeLineMap.containsKey( server ))
						|| (null == timeLineMap.get( server )) ) {

					lineMap = new TreeMap<IBldTimelineEntity, IBldTimelineAgentEntity>( new IBldTimelineEntityComparator() );
					timeLineMap.put( server, lineMap );
				}

				lineMap = timeLineMap.get( server );
				lineMap.put( timeline, line );
			}
		}

		// ↑の処理だと処理していないlineNoを詰めてしまうので、処理していない部分に明示的にnullをsetする
		for (Map<IBldTimelineEntity, IBldTimelineAgentEntity> map : timeLineMap.values()) {

			for (IBldTimelineEntity entity : timelineEntitys) {

				if( map.containsKey( entity ) ) {

				} else {
					map.put(entity, null);
				}
			}
		}

		return timeLineMap;
	}

	/**
	 * サーバ単位に正規化します。
	 *
	 * @param processs ビルドパッケージ状況
	 * @return サーバ単位にマップし直した情報を戻します。
	 */
	private final Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> normalizeProcess(
														ITaskFlowProcEntity[] processs ) {

		Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> timeLineMap = new HashMap<IBldSrvEntity, Map<String, ITaskFlowProcEntity>>();

		for ( ITaskFlowProcEntity process : processs ) {

			IBldSrvEntity server = TaskImageResourceCache.findUcfServerEntityByCache(process.getBldSrvId());

			Map<String, ITaskFlowProcEntity> lineMap = null;

			if ( (! timeLineMap.containsKey( server ))
					|| (null == timeLineMap.get( server )) ) {
				lineMap = new HashMap<String, ITaskFlowProcEntity>();
				timeLineMap.put( server, lineMap );
			}

			lineMap = timeLineMap.get( server );
			lineMap.put( process.getBldLineNo().toString(), process );
		}

		return timeLineMap;
	}

	/**
	 * システム検索の効率化のため、マップに変換します。
	 *
	 * @param systems サーバ・システム情報
	 * @return システム単位にマップし直した情報を戻します。
	 */
	private final Map<String, IBldSrvEntity> convertSystem( IBldSrvEntity[] systems ) {
		Map<String, IBldSrvEntity> map = new HashMap<String, IBldSrvEntity>();

		for ( IBldSrvEntity system : systems ) {
			map.put(system.getBldSrvId(), system);
		}

		return map;
	}
	/**
	 * タスク情報を、workflowNoをキーとしたマップに格納して返す。
	 * @param taskEntitys タスク情報の配列
	 * @return マップ
	 * <pre>
	 * 		Key		: workflowNo
	 * 		Value	: ITaskEntity
	 * </pre>
	 */
	private final Map<String , ITaskEntity> mapTask( ITaskFlowEntity[] taskFlowEntitys ) {
		Map<String , ITaskEntity> map = new HashMap<String , ITaskEntity>() ;

		for( ITaskFlowEntity taskFlowEntity : taskFlowEntitys ) {
			map.put( taskFlowEntity.getTaskFlowId() , taskFlowEntity.getTask() ) ;
		}
		return map ;
	}

	/**
	 * タスクのターゲット情報（TaskTargetを、sequenceNoをキーとしたマップに格納して返す。
	 * @param taskEntity タスク情報
	 * @return マップ
	 * <pre>
	 * 		Key		: target.sequenceNo
	 * 		Value	: ITaskTargetEntity
	 * </pre>
	 */
	private final Map<String , ITaskTargetEntity> mapTarget( ITaskEntity taskEntity ) {
		Map<String , ITaskTargetEntity> map = new HashMap<String , ITaskTargetEntity>() ;

		ITaskTargetEntity[] targetEntitys = taskEntity.getTarget() ;
		for( ITaskTargetEntity targetEntity : targetEntitys ) {
			String seqNo = targetEntity.getSequenceNo().toString();
			map.put( seqNo , targetEntity ) ;
		}
		return map ;
	}

	private final boolean isProcessExist(ITaskFlowProcEntity[] entity) throws Exception {

		if( 0 == entity.length ) {
			return false;
		} else {
			return true;
		}
	}

	// isProcessExist() == true であること
	public enum ProcessState {
		PROCESSING,	// 実行中
		ERROR, 	 	// エラーによる停止
		COMPLETE	// 全タスク終了による停止

	}
	public final ProcessState evalProcessState( ITaskFlowProcEntity[] processEntities, IBldTimelineEntity[] timeLineEntities) {

		Set<String> processLineNoSet = new HashSet<String>();
		// プロセス中に１件でもエラーがあったらERROR
		boolean err_ = false;
		boolean active_ = false ;
		for (ITaskFlowProcEntity processEntity : processEntities) {
			String state = processEntity.getStsId();

			processLineNoSet.add(processEntity.getBldLineNo().toString());

			if( BmBpStatusIdForExecData.CreatingBuildPackage.equals(state) ) {
				active_ = true;
			}
			if( BmBpStatusIdForExecData.BuildPackageError.equals(state) ) {
				err_ = true;
			}
		}
		if( true == active_ ) {
			return ProcessState.PROCESSING;
		} else if( true == err_ ) {
			return ProcessState.ERROR;
		}

		// プロセス数がタイムライン数と一致していればCOMPLETE
		// プロセス数がタイムライン数を満たしていなければPROCESSING
		// プロセス数がタイムライン数を超えていてもCOMPLETE(debugログ対象)
		int maxLength = timeLineEntities.length;
		int processingLength = processLineNoSet.size();

		if( processingLength < maxLength ) {
			return ProcessState.PROCESSING;
		} else if(processingLength == maxLength) {
			return ProcessState.COMPLETE;
		} else {
			LogHandler.debug( log , "irregular:processingLength(" + processingLength + ") < maxLength(" + maxLength + ")");
			return ProcessState.COMPLETE;
		}
	}

	/**
	 * ビルドパッケージ中かどうかを判定する
	 * @param processMap ビルドパッケージプロセスエンティティが格納されたマップ
	 * @return ビルドパッケージ中であればtrue、そうでなければfalse
	 */
	/* 使用されていないprivateメソッド
	private final boolean isActiveEnv( Map<IUcfServerEntity, Map<String, ITaskFlowProcEntity>> processMap ) {

		if ( null == processMap )
			return false;

		boolean isActive = false;

		for ( Map<String, ITaskFlowProcEntity> map : processMap.values() ) {
			for ( ITaskFlowProcEntity entity : map.values() ) {
				if ( ! RelStatusId.BUILD_ACTIVE.equals(entity.getStatusId()) )
					continue;

				isActive = true;
				break;
			}
		}

		return isActive;
	}*/

	/**
	 * ビルドパッケージ中かどうかを判定する
	 * @param processMap ビルドパッケージプロセスエンティティが格納されたマップ
	 * @return ビルドパッケージ中であればtrue、そうでなければfalse
	 */
	private final boolean isActiveServer( Map<String, ITaskFlowProcEntity> processLineMap ) {

		if ( null == processLineMap )
			return false;

		boolean isActive = false;

		for ( ITaskFlowProcEntity entity : processLineMap.values() ) {
			if ( ! BmBpStatusIdForExecData.CreatingBuildPackage.equals(entity.getStsId() ))
				continue;

			isActive = true;
			break;
		}

		return isActive;
	}

	/**
	 * ビルドパッケージ中かどうかを判定する
	 * @param processMap ビルドパッケージプロセスエンティティが格納されたマップ
	 * @return ビルドパッケージ中であればtrue、そうでなければfalse
	 */
	@SuppressWarnings("unused")
	private final boolean isActiveSystem(
				Map<String, ITaskFlowProcEntity> processLineMap, String systemNo ) {

		if ( null == processLineMap )
			return false;

		boolean isActive = false;

		for ( ITaskFlowProcEntity entity : processLineMap.values() ) {
//			if ( ! entity.getSystemNo().equals(systemNo) )
	//			continue;

			if ( ! BmBpStatusIdForExecData.CreatingBuildPackage.equals(entity.getStsId() ))
				continue;

			isActive = true;
			break;
		}

		return isActive;
	}

	/**
	 *
	 * <p>
	 * inputの構造<br>
	 * <table>
	 * <tr><td>(list.get(0))[0]</td><td>(list.get(0))[1]</td><td>(list.get(0))[2]</td></tr>
	 * <tr><td>(list.get(1))[0]</td><td>(list.get(1))[1]</td><td>                </td></tr>
	 * <tr><td>(list.get(2))[0]</td><td>(list.get(2))[1]</td><td>(list.get(2))[2]</td></tr>
	 * <tr><td>(list.get(3))[0]</td><td>                </td><td>                </td></tr>
	 * </table>
	 * </p>
	 *
	 * <p>
	 * outputの構造<br>
	 * <table>
	 * <tr><td>(list.get(0))[0]</td><td>(list.get(1))[0]</td><td>(list.get(2))[0]</td><td>(list.get(3))[0]</td></tr>
	 * <tr><td>(list.get(0))[1]</td><td>(list.get(1))[1]</td><td>(list.get(2))[1]</td><td>                </td></tr>
	 * <tr><td>(list.get(0))[2]</td><td>                </td><td>(list.get(2))[2]</td><td>                </td></tr>
	 * </table>
	 * </p>
	 *
	 * @param generateDetailViewBean
	 * @return
	 */
	private final GenerateDetailViewBean convertArrayStructure(GenerateDetailViewBean generateDetailViewBean) {

		List<BuildProcessViewBean> prmList = generateDetailViewBean.getBuildProcessViewBeanList();
		if(log.isDebugEnabled()) {
			String strPrmArrStructure = this.showArrayStructure(prmList);
			LogHandler.debug( log , strPrmArrStructure );
		}

		// 空だったらとりあえず返す
		if(prmList.isEmpty())
			return generateDetailViewBean;

		int col = 0;
		int row = 0;
		// タテヨコ変換なのでListのサイズがヨコに相当する
		col = prmList.size();
		// 変換前のヨコ(配列の数)は違う可能性があるのでMaxを持ってくる->タテに相当
		for (BuildProcessViewBean bean : prmList) {

			String[] imgs = bean.getImg();
			if( null != imgs ) {

				int iL = imgs.length;
				if(row < iL)  row = iL;		// rowより大きければ更新
			}

			String[] msgs = bean.getMsg();
			if( null != msgs ) {

				int mL = msgs.length;
				if(row < mL) row = mL;		// rowより大きければ更新
			}

			String[] actives = bean.getActive();
			if( null != actives ) {

				int aL = actives.length;
				if(row < aL) row = aL;		// rowより大きければ更新
			}
		}

		// 始めに返すListの構造を作っておく
		List<BuildProcessViewBean> newList = new ArrayList<BuildProcessViewBean>(row);
		for (int i = 0; i < row; i++) {

			BuildProcessViewBean newBean = generateDetailViewBean.newBuildProcessViewBean();
			newBean.setImg(new String[col]);
			newBean.setMsg(new String[col]);
			newBean.setActive(new String[col]);

			newList.add(newBean);
		}


		// タテヨコを入れ替えるので、iとjを逆にする
		for (int i = 0; i < prmList.size(); i++) {
			BuildProcessViewBean bean = prmList.get(i);

			// 前段の処理でimgsのサイズとmsgsのサイズが同じの前提
			String[] imgs = bean.getImg();
			String[] msgs = bean.getMsg();
			String[] actives = bean.getActive() ;
			for (int j = 0; j < imgs.length; j++) {

				String img = null;
				if( j < imgs.length ) {
					img = imgs[j];
				}

				String msg = null;
				if( null != msgs && j < msgs.length ) {
					msg = msgs[j];
				}

				String active = null;
				if( null != actives && j < actives.length ) {
					active = actives[j];
				}

				// 添え字指定でオブジェクトを持ってくる
				// 始めに返すListの構造を作っているので、直指定が可能なはず
				String[] newImg = newList.get(j).getImg();
				String[] newMsg = newList.get(j).getMsg();
				String[] newActive = newList.get(j).getActive();

				newImg[i] = img;
				newMsg[i] = msg;
				newActive[i] = active;
			}
		}

		if(log.isDebugEnabled()) {
			String strNewArrStructure = this.showArrayStructure(newList);
			LogHandler.debug( log , strNewArrStructure );
		}

		// buildProcessViewBeanListを新しいので差し替え
		generateDetailViewBean.setBuildProcessViewBeanList(newList);
		return generateDetailViewBean;
	}

	private final String showArrayStructure(List<BuildProcessViewBean> buildProcessViewBeanList) {

		StringBuilder sb = new StringBuilder();

		if(buildProcessViewBeanList.isEmpty()) {
			return "";
		}

		int i = 0;
		for (BuildProcessViewBean bean : buildProcessViewBeanList) {

			sb.append("bean(" + i + "):");
			sb.append("\t");

			String[] imgs = bean.getImg();

			if(null != imgs) {

				int j = 0;
				for (String img : imgs) {

					sb.append("img[" + j + "]=" + img + ",");
					sb.append("\t");
					j++;
				}
			}
			String[] msgs = bean.getMsg();

			if(null != msgs) {

				int k = 0;
				for (String msg : msgs) {

					sb.append("msg[" + k + "]=" + msg + ",");
					sb.append("\t");
					k++;
				}
			}

			String[] actives = bean.getActive();

			if(null != actives) {

				int l = 0;
				for (String active : actives) {

					sb.append("active[" + l + "]=" + active + ",");
					sb.append("\t");
					l++;
				}
			}

			sb.append("[EOD]");
			sb.append("\n");

			i++;
		}

		return sb.toString();
	}

	private class IUcfServerEntityComparator implements Comparator<IBldSrvEntity> {

		public int compare(IBldSrvEntity obj1, IBldSrvEntity obj2) {

			String serverNo1 = obj1.getBldSrvId();
			String serverNo2 = obj2.getBldSrvId();

			if ( StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
					return 1;
				}
			}
			if ( StatusFlg.on.value().equals( obj2.getIsAgent().value() ) ) {
				if ( ! StatusFlg.on.value().equals( obj1.getIsAgent().value() ) ) {
					return -1;
				}
			}

			return serverNo1.compareTo( serverNo2 );

		}
	}

	private class IBldTimelineEntityComparator implements Comparator<IBldTimelineEntity> {

		public int compare(IBldTimelineEntity obj1, IBldTimelineEntity obj2) {

			String lineNo1 = obj1.getBldLineNo().toString();
			String lineNo2 = obj2.getBldLineNo().toString();

			int intLineNo1 = Integer.parseInt(lineNo1);
			int intLineNo2 = Integer.parseInt(lineNo2);

			return intLineNo1 - intLineNo2;
		}
	}

	/**
	 *
	 * @param buildNo
	 * @param buildEntity
	 * @param timeLineEntitys
	 * @param process
	 * @param lineMap
	 * @param processMap
	 * @param systemMap
	 * @param taskMap
	 * @return
	 */
	private GenerateDetailViewBean makeGenerateDetailViewBean(
			IBpDto buildEntity ,
			IBldTimelineEntity[] timeLineEntitys ,
			ITaskFlowProcEntity[] process ,
			Map<IBldSrvEntity, Map<IBldTimelineEntity, IBldTimelineAgentEntity>> lineMap ,
			Map<IBldSrvEntity, Map<String, ITaskFlowProcEntity>> processMap ,
			Map<String, IBldSrvEntity> systemMap ,
			Map<String , ITaskEntity> taskMap
			) {

		List<BuildProcessViewBean> buildProcessViewBeanList = new ArrayList<BuildProcessViewBean>();

//		boolean isActiveEnv = isActiveEnv( processMap );
//		boolean isBreakEnv = true; //現在未使用

		String activeServerNo = null;
		boolean isBreakServer = true;

		String activeSystemNo = null;
		boolean isBreakSystem = true;

		GenerateDetailViewBean generateDetailViewBean = new GenerateDetailViewBean();

		for ( IBldSrvEntity serverEntity : lineMap.keySet() ) {

			if ( null == activeServerNo || ! serverEntity.getBldSrvId().equals( activeServerNo )) {
				activeServerNo = serverEntity.getBldSrvId();
//				isBreakEnv = true;//現在未使用
				isBreakServer = true;
				isBreakSystem = true;
			}

			BuildProcessViewBean viewBean = generateDetailViewBean.newBuildProcessViewBean();

			String serverStsId = null;
			String msgServer = null ;
			String osTypeId = null;
			String msgSystem = null ;
			List<String> taskStsIdList = new ArrayList<String>();
			List<String> messageList = new ArrayList<String>();
			List<String> activeList = new ArrayList<String>() ;

			Map<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMap = lineMap.get( serverEntity );
			Map<String, ITaskFlowProcEntity> processLineMap = processMap.get( serverEntity );

//			int column = 0; //現在未使用
			for ( Map.Entry<IBldTimelineEntity, IBldTimelineAgentEntity> targetLineMapEntry : targetLineMap.entrySet() ) {

				IBldTimelineEntity timeLineEntity = targetLineMapEntry.getKey();
				IBldTimelineAgentEntity lineEntity = targetLineMapEntry.getValue();
//
//				if ( isBreakEnv ) {
//					if ( isActiveEnv ) {
//						imgPathList.add( lotEntity.getImg()[IBldTimelineEntity.img.active.getValue()] );
//					} else {
//						imgPathList.add( lotEntity.getImg()[IBldTimelineEntity.img.off.getValue()] );
//					}
//					isBreakEnv = false;
//				}

				if ( isBreakServer ) {
					if ( isActiveServer( processLineMap )) {
						serverStsId = serverEntity.getSrvStsIdActive();
					} else {
						serverStsId = serverEntity.getSrvStsIdOff();
					}
					msgServer = serverEntity.getBldSrvNm() ;
					isBreakServer = false;
				}

				//タイムライン上に業務処理が存在する場合
				if ( null !=lineEntity ) {
					if ( null == activeSystemNo || ! lineEntity.getBldSrvId().equals( activeSystemNo )) {
						activeSystemNo = lineEntity.getBldSrvId();
						isBreakSystem = true;
					}

					if ( isBreakSystem ) {
						IBldSrvEntity bldSrvEntity = systemMap.get( lineEntity.getBldSrvId() ) ;
						if( null == bldSrvEntity ) {
							throw new TriSystemException(BmMessageId.BM005041S , lineEntity.getBldSrvId() );
						}

						osTypeId = bldSrvEntity.getOsTyp();
						msgSystem = bldSrvEntity.getBldSrvNm();
						isBreakSystem = false;
					}

					//
					if ( null != processLineMap ) {
						ITaskFlowProcEntity processEntity = processLineMap.get( timeLineEntity.getBldLineNo().toString() );

						if ( null != processEntity && BmBpStatusIdForExecData.CreatingBuildPackage.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_ON );
							taskStsIdList.add( lineEntity.getTargetStsIdActive() );
							activeList.add( STATUS_ACTIVE ) ;
						}

						if ( null != processEntity && BmBpStatusIdForExecData.BuildPackageError.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdErr() );
							activeList.add( STATUS_ERROR ) ;
						}
						if ( null != processEntity && BmBpStatusId.BuildPackageCreated.equals( processEntity.getStsId() )) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdOff() );
							activeList.add( STATUS_COMPLETE ) ;
						}
						if ( null == processEntity ) {
							//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
							taskStsIdList.add( lineEntity.getTargetStsIdOff() );
							activeList.add( null ) ;
						}
					} else {
						//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
						taskStsIdList.add( lineEntity.getTargetStsIdOff() );
						activeList.add( null ) ;
					}

//					column++; //現在未使用
				} else {
					//タイムライン上に業務処理が存在しない場合
					//imgPathList.add( IMAGE_NEXT_ARROW_OFF );
					taskStsIdList.add( WAIT );
					activeList.add( null ) ;
//					column++; //現在未使用
				}
				//メッセージ
				if( null != lineEntity ) {
//					ITaskEntity taskEntity = taskMap.get( lineEntity.getWorkflowNo() ) ;
					ITaskEntity taskEntity = taskMap.get( lineEntity.getTaskFlowId() ) ;
					String message = "" ;
					if( null != taskEntity ) {
						Map<String,ITaskTargetEntity> targetMap = this.mapTarget( taskEntity ) ;
						if( true == targetMap.containsKey( lineEntity.getTargetSeqNo().toString() ) ) {
							message = targetMap.get( lineEntity.getTargetSeqNo().toString() ).getName() ;
						}
					}
					messageList.add( message ) ;
				} else {
					messageList.add( "" ) ;
				}
			}
			//画像アイコン用
			List<String> serverTaskStsIdList = new ArrayList<String>();
			serverTaskStsIdList.add( serverStsId );//server
			serverTaskStsIdList.add( osTypeId );//system
			//imgOutPathList.add( IMAGE_NEXT_ARROW_OFF );//arrow
			serverTaskStsIdList.addAll( taskStsIdList );
			//メッセージ用
			List<String> messageOutList = new ArrayList<String>();
			messageOutList.add( msgServer );//server
			messageOutList.add( msgSystem );//system
			//messageOutList.add( "" );//arrow
			messageOutList.addAll( messageList );

			//タイムライン装飾用
			List<String> activeOutList = new ArrayList<String>();
			activeOutList.add( null ) ;//server
			activeOutList.add( null ) ;//system
			//activeOutList.add( null ) ;//arrow
			activeOutList.addAll( activeList ) ;

			viewBean.setImg( serverTaskStsIdList.toArray( new String[serverTaskStsIdList.size()] ));
			viewBean.setMsg( messageOutList.toArray( new String[messageOutList.size()] ));
			viewBean.setActive( activeOutList.toArray( new String[activeOutList.size()] ));
			buildProcessViewBeanList.add( viewBean );
		}

		generateDetailViewBean.setBuildNo					( buildEntity.getBpEntity().getBpId() );
		generateDetailViewBean.setExecuteUser				( buildEntity.getBpEntity().getExecUserNm() );
		generateDetailViewBean.setExecuteUserId				( buildEntity.getBpEntity().getExecUserId() );
		generateDetailViewBean.setBuildProcessViewBeanList	( buildProcessViewBeanList );

		generateDetailViewBean = convertArrayStructure(generateDetailViewBean);

		ProcessState ps = evalProcessState(process, timeLineEntitys);
		switch (ps) {
		case PROCESSING:

			generateDetailViewBean.setBuildStatusMsg( STATE_MSG[1] );
			break;
		case ERROR:

			generateDetailViewBean.setBuildStatusMsg( STATE_MSG[2] );
			break;
		case COMPLETE:

			generateDetailViewBean.setBuildStatusMsg( STATE_MSG[3] );
			break;
		default:

			generateDetailViewBean.setBuildStatusMsg( "" );
			break;
		}

		return generateDetailViewBean ;
	}
}
