package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.bm.beans.dto.RelUnitSearchBean;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCloseHistoryListServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchBeanAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージクローズ履歴一覧画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitCloseHistoryListService implements IDomain<FlowRelUnitCloseHistoryListServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private FlowRelUnitEditSupport support = null;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitCloseHistoryListServiceBean> execute( IServiceDto<FlowRelUnitCloseHistoryListServiceBean> serviceDto ) {

		FlowRelUnitCloseHistoryListServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String selectedLotNo = paramBean.getSelectedLotNo();
			ItemCheckAddonUtils.checkLotNo( selectedLotNo );

			paramBean.setRelUnitSearchBean(
					DBSearchBeanAddonUtil.setRelUnitSearchBean(
							paramBean.getRelUnitSearchBean(), BmDesignBeanId.bpCloseHistoryListCount ) );

			IJdbcCondition condition	=
				DBSearchConditionAddonUtil.getBuildConditionByCloseRelUnit( selectedLotNo );

			int buildCount = this.support.getBpDao().count( condition.getCondition() );

			if ( ! ScreenType.bussinessException.equals( paramBean.getScreenType() ) ) {
				//履歴画面は、最新画面との水平遷移のため、infoメッセージを表示し、エラーとしない
				if ( buildCount == 0 ) {
					paramBean.setInfoMessage( BmMessageId.BM001022E );
					setParamBeanAndCheckGrp(paramBean);
					paramBean.setUnitViewBeanList( new ArrayList<UnitCloseViewBean>() );
					return serviceDto;
				}
			}

			setParamBeanAndCheckGrp(paramBean);

			this.support.setBuildCondition( (BpCondition)condition, paramBean.getRelUnitSearchBean() );
			ISqlSort sort = (ISqlSort) DBSearchSortAddonUtil.getRelUnitSortFromDesignDefineByRelUnitCloseHistoryList();
			List<IBpDto> bpDtoList = getBpDtoLimitListForClose( condition , sort );
			List<IBpDto> buildEntityList = this.support.applySearchCondition(bpDtoList, paramBean.getRelUnitSearchBean() );

			List<UnitCloseViewBean> unitViewBeanList = this.support.getUnitCloseViewBeanList( buildEntityList );

			{
				RelUnitSearchBean relUnitSearchBean = paramBean.getRelUnitSearchBean();
				int count = 0;
				int maxCount = ( null == relUnitSearchBean.getSelectedRelUnitCount() )? 0: Integer.parseInt(relUnitSearchBean.getSelectedRelUnitCount());

				//外部結合検索のため、全体件数を予測できないため、全体件数を指定せずに、ロジックで絞込み。
				//このケースでは、ページ制御を一覧でのみ実現可能。
				List<UnitCloseViewBean> newViewBeawnList = new ArrayList<UnitCloseViewBean>();
				for ( UnitCloseViewBean bean : unitViewBeanList ) {
					count++;

					if ( 0 != maxCount && count > maxCount )
						break;

					newViewBeawnList.add( bean );
				}

				paramBean.setUnitViewBeanList( newViewBeawnList );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005074S,e);
		}
	}

	/**
	 * paramBeanに情報を設定するprivateメソッド
	 * 従来の処理をメソッド化
	 * @param paramBean
	 */
	private void setParamBeanAndCheckGrp(FlowRelUnitCloseHistoryListServiceBean paramBean) {

		ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( paramBean.getSelectedLotNo() );

		paramBean.setLotName( lotDto.getLotEntity().getLotNm() );
		// グループの存在チェック
		RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), getGrpUsers(paramBean) );
		paramBean.setRelUnitSearchBean( paramBean.getRelUnitSearchBean() );
	}

	/**
	 * グループユーザエンティティを取り出すprivateメソッド
	 * @param paramBean
	 * @return
	 */
	private List<IGrpUserLnkEntity> getGrpUsers(FlowRelUnitCloseHistoryListServiceBean paramBean){
		return support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
	}

	/**
	 * 選択されたロットNoからビルドパッケージDto(リミット)を取得する(クローズ用)
	 * @param buildNo ビルドパッケージID
	 * @return ビルドパッケージDto(リミット)
	 */
	private List<IBpDto> getBpDtoLimitListForClose( IJdbcCondition condition , ISqlSort sort ) {

		IEntityLimit<IBpEntity> bpEntityLimit = this.support.getBpEntity( condition, sort, 1, 0 );
		List<IBpEntity> bpEntities = bpEntityLimit.getEntities();

		return this.support.findBpDtoList( bpEntities );
	}
}
