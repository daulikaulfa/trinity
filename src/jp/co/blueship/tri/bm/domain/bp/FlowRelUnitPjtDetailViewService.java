package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitPjtDetailViewServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitPjtDetailViewServiceBean.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;


/**
 * ビルドパッケージ作成・変更管理詳細画面(ポップアップ)の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitPjtDetailViewService implements IDomain<FlowRelUnitPjtDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support = null;
	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitPjtDetailViewServiceBean> execute( IServiceDto<FlowRelUnitPjtDetailViewServiceBean> serviceDto ) {

		FlowRelUnitPjtDetailViewServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String forward = paramBean.getForward();
			String referer = paramBean.getReferer();

			if ( !referer.equals( RelUnitScreenID.PJT_DETAIL_VIEW ) &&
					!forward.equals( RelUnitScreenID.PJT_DETAIL_VIEW )) {
				return serviceDto;
			}

			if ( forward.equals(RelUnitScreenID.PJT_DETAIL_VIEW) ) {

				String selectedPjtNo = paramBean.getSelectedPjtNo();
				checkPjtNo( selectedPjtNo );


				String[] targetApplyNos = getGeneratableRelUnitApplyNo( selectedPjtNo );
				List<String> targetApplyNoList = new ArrayList<String>( FluentList.from(targetApplyNos).asList());


				IJdbcCondition condition =
					DBSearchConditionAddonUtil.getAreqCondition( selectedPjtNo );
				ISqlSort sort = DBSearchSortAddonUtil.getAssetApplySortFromDesignDefineByRelUnitPjtDetailView();

				IEntityLimit<IAreqEntity> limit =
					this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), sort, 1, 0 );


				List<ApplyInfoViewBean> applyInfoViewBeanList =
												new ArrayList<ApplyInfoViewBean>();
				Boolean isAssetApplyClosed = paramBean.getAssetApplyClosedFlag();

				for ( IAreqEntity entity : limit.getEntities() ) {

					if ( null != isAssetApplyClosed ) {

						if ( isAssetApplyClosed ) {

							// 申請情報クローズ済フラグが立っている場合、クローズ済の申請情報のみ表示する
							if ( ! BmBusinessJudgUtils.isOverRelUnitClose( entity ) ) {
								continue;
							}
						} else {

							// 申請情報クローズ済フラグが立っていない場合、クローズ済の申請情報は表示しない
							if ( BmBusinessJudgUtils.isOverRelUnitClose( entity ) ) {
								continue;
							}
						}
					}


					ApplyInfoViewBean viewBean = paramBean.newApplyInfoViewBean();

					viewBean.setApplyNo	( entity.getAreqId() );
					viewBean.setStatus	(
							sheet.getValue(
									AmDesignBeanId.statusId, entity.getProcStsId() ));

					if( targetApplyNoList.contains( entity.getAreqId() ) ) {
						viewBean.setTarget( true );
					} else {
						viewBean.setTarget( false );
					}

					applyInfoViewBeanList.add( viewBean );
				}

				paramBean.setSelectedPjtNo		( selectedPjtNo );
				paramBean.setApplyInfoViewBeanList( applyInfoViewBeanList );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			//ダイアログ画面が表示されるclassはBaseBusinessExceptionをthrowする
			ExceptionUtils.throwBaseBusinessException(e);
			throw new BaseBusinessException(BmMessageId.BM005088S,e);
		}
	}

	/**
	 * ビルドパッケージ作成可能な返却申請、削除申請の申請番号取得
	 * @param pjtNo
	 * @return 申請番号
	 */
	private String[] getGeneratableRelUnitApplyNo( String pjtNo ) {

		// ビルドパッケージ作成可能な返却、削除承認
		IJdbcCondition condition =
			DBSearchConditionAddonUtil.getAreqConditionByGeneratableRelUnitByPjtNo( pjtNo );

		IEntityLimit<IAreqEntity> limit =
			this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 );


		// 申請番号を抽出
		HashSet<String> applyNoSet = new HashSet<String>();
		for ( IAreqEntity entity : limit.getEntities() ) {
			applyNoSet.add( entity.getAreqId() );
		}

		return applyNoSet.toArray(new String[0]);

	}

	/**
	 * 変更管理番号が選択されているかどうかチェックする
	 *
	 * @param pjtNo 変更管理番号
	 */

	public static void checkPjtNo( String pjtNo ) {

		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			throw new ContinuableBusinessException( BmMessageId.BM001001E );
		}
	}
}
