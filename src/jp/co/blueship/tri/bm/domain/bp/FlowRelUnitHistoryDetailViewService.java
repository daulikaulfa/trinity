package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.bm.BmBusinessJudgUtils;
import jp.co.blueship.tri.bm.BmTaskAddonUtils;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.dao.taskflow.eb.ITaskFlowResEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.constants.TaskFlowProcItems;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.PjtViewBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.PjtViewBean.ApplyInfoViewBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitHistoryDetailViewServiceBean.UnitResultViewBean;
import jp.co.blueship.tri.fw.agent.service.flow.constants.TaskDefineId;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * ビルドパッケージ作成・ビルドパッケージ詳細画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2009<br>
 */
public class FlowRelUnitHistoryDetailViewService implements IDomain<FlowRelUnitHistoryDetailViewServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support = null;

	public void setSupport(FlowRelUnitEditSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowRelUnitHistoryDetailViewServiceBean> execute(IServiceDto<FlowRelUnitHistoryDetailViewServiceBean> serviceDto) {

		FlowRelUnitHistoryDetailViewServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			// ビルドパッケージ情報
			String buildNo = paramBean.getSelectedBuildNo();
			ItemCheckAddonUtils.checkBuildNo(buildNo);

			IBpDto bpDto = this.support.findBpDto(buildNo);
			paramBean.setStatus(sheet.getValue(RmDesignBeanId.statusId, bpDto.getBpEntity().getProcStsId()));
			paramBean.setBaseLineTag(bpDto.getBpEntity().getBpCloseBlTag());
			paramBean.setVersionTag(bpDto.getBpEntity().getBpCloseVerTag());
			paramBean.setUnitName(bpDto.getBpEntity().getBpNm());
			paramBean.setUnitSummary(bpDto.getBpEntity().getSummary());
			paramBean.setExecuteUser(bpDto.getBpEntity().getExecUserNm());
			paramBean.setExecuteUserId(bpDto.getBpEntity().getExecUserId());
			paramBean.setStartDate(bpDto.getBpEntity().getProcStTimestamp());
			paramBean.setEndDate(bpDto.getBpEntity().getProcEndTimestamp());

			// ロット情報
			ILotDto lotDto = this.support.getAmFinderSupport().findLotDto(bpDto.getBpEntity().getLotId());
			paramBean.setLotName(lotDto.getLotEntity().getLotNm());

			// グループの存在チェック
			List<IGrpUserLnkEntity> groupUsers = support.getUmFinderSupport().findGrpUserLnkByUserId(paramBean.getUserId());
			RelCommonAddonUtil.checkAccessableGroup(lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers);

			// ビルドパッケージされた申請情報
			Map<String, List<String>> pjtNoApplyNoMap = getPjtNoApplyNoMap(bpDto.getBpAreqLnkEntities());

			Map<String, ITaskFlowResEntity> taskRecEntityMap = new LinkedHashMap<String, ITaskFlowResEntity>();
			List<PjtViewBean> pjtViewBeanList = this.getPjtViewBean(paramBean, pjtNoApplyNoMap, bpDto, taskRecEntityMap);

			paramBean.setPjtViewBeanList(pjtViewBeanList);

			// ビルドパッケージの結果
			List<UnitResultViewBean> unitResultViewBeanList = new ArrayList<UnitResultViewBean>();
			setUnitResultViewBean(paramBean, taskRecEntityMap, unitResultViewBeanList);
			paramBean.setUnitResultViewBeanList(unitResultViewBeanList);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005085S, e);
		}
	}

	/**
	 * ビルドパッケージ申請情報から、変更管理番号とそれに含まれる申請番号のマップを取得する。
	 *
	 * @param buildApplyEntities ビルドパッケージ申請情報
	 * @return 変更管理番号とそれに含まれる申請番号のマップ
	 */
	private Map<String, List<String>> getPjtNoApplyNoMap(List<IBpAreqLnkEntity> bpAreqEntities) {

		// 変更管理番号の昇順に並ぶ
		Map<String, List<String>> pjtNoApplyNoMap = new TreeMap<String, List<String>>();

		for (IBpAreqLnkEntity bpAreqEntity : bpAreqEntities) {

			String pjtNo = this.support.getAmFinderSupport().findAreqEntity(bpAreqEntity.getAreqId()).getPjtId();

			if (!pjtNoApplyNoMap.containsKey(pjtNo)) {
				pjtNoApplyNoMap.put(pjtNo, new ArrayList<String>());
			}
			pjtNoApplyNoMap.get(pjtNo).add(bpAreqEntity.getAreqId());
		}

		return pjtNoApplyNoMap;
	}

	/**
	 * 変更管理番号と申請番号から、変更管理情報に値をセットする。
	 *
	 * @param paramBean FlowRelUnitDetailViewServiceBeanオブジェクト
	 * @param pjtNoApplyNoMap 変更管理番号と申請番号のマップ
	 * @param buildEntity ビルドパッケージエンティティ
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @return
	 */
	private List<PjtViewBean> getPjtViewBean(FlowRelUnitHistoryDetailViewServiceBean paramBean, Map<String, List<String>> pjtNoApplyNoMap,
			IBpDto bpEntity, Map<String, ITaskFlowResEntity> taskRecEntityMap) {

		List<PjtViewBean> pjtViewBeanList = new ArrayList<PjtViewBean>();

		// 資産数取得
		List<ITaskFlowResEntity> taskRecEntityList = new ArrayList<ITaskFlowResEntity>();
		Map<String, IAssetCounterInfo> applyNoCounterMap = BmTaskAddonUtils.getAssetCount(bpEntity, this.support, taskRecEntityList);

		for (ITaskFlowResEntity taskRecEntity : taskRecEntityList) {
			taskRecEntityMap.put(taskRecEntity.getTaskFlowId(), taskRecEntity);
		}

		Map<String, IPjtEntity> pjtMap = new HashMap<String, IPjtEntity>();
		for (IPjtEntity pjtEntity : support.getPjtEntity(pjtNoApplyNoMap.keySet().toArray(new String[0]))) {
			pjtMap.put(pjtEntity.getPjtId(), pjtEntity);
		}

		for (Map.Entry<String, List<String>> pjtNoApplyNoEntry : pjtNoApplyNoMap.entrySet()) {

			String pjtNo = pjtNoApplyNoEntry.getKey();

			PjtViewBean pjtViewBean = paramBean.newPjtViewBean();
			pjtViewBean.setPjtNo(pjtNo);
			if (pjtMap.containsKey(pjtNo)) {
				pjtViewBean.setChangeCauseNo(pjtMap.get(pjtNo).getChgFactorNo());
			}

			List<ApplyInfoViewBean> applyInfoViewBeanList = new ArrayList<ApplyInfoViewBean>();

			List<String> applyNoList = pjtNoApplyNoEntry.getValue();
			Collections.sort(applyNoList);

			for (String applyNo : applyNoList) {

				ApplyInfoViewBean applyInfoViewBean = pjtViewBean.newApplyInfoViewBean();

				applyInfoViewBean.setApplyNo(applyNo);
				setApplyInfoViewBeanAssetCount(applyNo, applyInfoViewBean, applyNoCounterMap.get(applyNo));

				applyInfoViewBeanList.add(applyInfoViewBean);

			}

			pjtViewBean.setApplyInfoViewBeanList(applyInfoViewBeanList);

			pjtViewBeanList.add(pjtViewBean);
		}

		// 総資産数を集計
		int lendAssetSumCount = 0;
		int changeAssetSumCount = 0;
		int deleteAssetSumCount = 0;
		int addAssetSumCount = 0;
		boolean isConcentrate = false;

		for (PjtViewBean pjtBean : pjtViewBeanList) {
			for (ApplyInfoViewBean applyBean : pjtBean.getApplyInfoViewBeanList()) {
				if (applyBean.isConcentrate()) {
					isConcentrate = true;
				}

				lendAssetSumCount += applyBean.getLendAssetCount();
				changeAssetSumCount += applyBean.getChangeAssetCount();
				deleteAssetSumCount += applyBean.getDeleteAssetCount();
				addAssetSumCount += applyBean.getAddAssetCount();
			}
		}

		paramBean.setLendAssetSumCount(lendAssetSumCount);
		paramBean.setChangeAssetSumCount(changeAssetSumCount);
		paramBean.setDeleteAssetSumCount(deleteAssetSumCount);
		paramBean.setAddAssetSumCount(addAssetSumCount);

		paramBean.setConcentrate(isConcentrate);

		// 総資産数を集計する
		if (applyNoCounterMap.containsKey(TaskDefineId.deleteBinaryAssetCount.value())) {
			IAssetCounterInfo counter = applyNoCounterMap.get(TaskDefineId.deleteBinaryAssetCount.value());

			if (null != counter.getDelCount()) {
				paramBean.setDeleteBinaryAssetSumCount(counter.getDelCount());
				paramBean.setBinaryConcentrate(true);
			}
		}

		return pjtViewBeanList;
	}

	/**
	 * ApplyInfoViewBeanオブジェクトに各種資産数を設定する。
	 *
	 * @param applyNo 申請番号
	 * @param applyInfoViewBean ApplyInfoViewBeanオブジェクト
	 * @param counter 資産数オブジェクト
	 */
	private void setApplyInfoViewBeanAssetCount(String applyNo, ApplyInfoViewBean applyInfoViewBean, IAssetCounterInfo counter) {

		// TaskにapplyNoに該当するdiffの結果がなかった場合(実際にはないかも)
		if (null == counter.getAddCount() && null == counter.getChangeCount() && null == counter.getDelCount()) {
			applyInfoViewBean.setConcentrate(false);
		} else {
			applyInfoViewBean.setConcentrate(true);
		}

		// 申請情報
		IAreqEntity entity = this.support.getAmFinderSupport().findAreqEntity(applyNo);

		// ActionReportWriterByBuildAssetRegister#getReportAssetApplyEntity にも
		// 同様のロジックがある
		if (BmBusinessJudgUtils.isReturnApply(entity)) {
			// 貸出資産数
			if (null != counter.getLendCount()) {
				applyInfoViewBean.setLendAssetCount(counter.getLendCount());
			}

			// 返却資産数
			// applyInfoViewBean.setReturnAssetCount (
			// counter.getAddCount() + counter.getChangeCount() );

			// 追加資産数
			if (null != counter.getAddCount()) {
				applyInfoViewBean.setAddAssetCount(counter.getAddCount());
			}

			// 変更資産数
			if (null != counter.getChangeCount()) {
				applyInfoViewBean.setChangeAssetCount(counter.getChangeCount());
			}

		} else if (BmBusinessJudgUtils.isDeleteApply(entity)) {

			// 削除資産数
			if (null != counter.getDelCount()) {
				applyInfoViewBean.setDeleteAssetCount(counter.getDelCount());
			}
		}
	}

	/**
	 * ビルドパッケージ作成結果情報に値をセットする。
	 *
	 * @param paramBean FlowRelUnitDetailViewServiceBeanオブジェクト
	 * @param taskRecEntityMap ビルドタスク履歴のマップ
	 * @param unitResultViewBeanList ビルドパッケージ作成結果情報
	 */
	private void setUnitResultViewBean(FlowRelUnitHistoryDetailViewServiceBean paramBean, Map<String, ITaskFlowResEntity> taskRecEntityMap,
			List<UnitResultViewBean> unitResultViewBeanList) {

		BpCondition bpCondition = new BpCondition();
		bpCondition.setBpId(paramBean.getSelectedBuildNo());
		IBpEntity bpEntity = this.support.getBpDao().findByPrimaryKey(bpCondition.getCondition());
		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId(bpEntity.getProcId());
		ISqlSort sort = new SortBuilder();
		sort.setElement(TaskFlowProcItems.bldLineNo, TriSortOrder.Asc, 0);
		List<ITaskFlowProcEntity> entities = this.support.getTaskFlowProcDao().find(condition.getCondition(),sort);

		for (ITaskFlowProcEntity entity : entities) {

			UnitResultViewBean unitResultViewBean = paramBean.newUnitResultViewBean();

			unitResultViewBean.setProcessOrder(entity.getTargetSeqNo().toString());
			unitResultViewBean.setProcessName(BmTaskAddonUtils.getProcessName(entity, taskRecEntityMap));

			boolean result = false;
			if (BmBpStatusId.BuildPackageCreated.equals(entity.getStsId())) {
				result = true;
			}
			unitResultViewBean.setProcessResult(result);

			unitResultViewBeanList.add(unitResultViewBean);
		}
	}
}
