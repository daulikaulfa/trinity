package jp.co.blueship.tri.bm.domain.bp.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseLotViewBean;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;

/**
 * ビルドパッケージ作成・ビルドパッケージクローズトップ画面用
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FlowRelUnitCloseTopServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** 選択ページ */
	private int selectPageNo = 0;
	/** ページ制御 */
	private IPageNoInfo pageInfoView = null;
	/** ロット情報 */
	private List<UnitCloseLotViewBean> lotViewBean = new ArrayList<UnitCloseLotViewBean>();

	
	public List<UnitCloseLotViewBean> getLotViewBeanList() {
		return lotViewBean;
	}
	public void setLotViewBeanList( List<UnitCloseLotViewBean> lotViewBean ) {
		this.lotViewBean = lotViewBean;
	}

	public IPageNoInfo getPageInfoView() {
		return pageInfoView;
	}
	public void setPageInfoView(IPageNoInfo pageInfoView) {
		this.pageInfoView = pageInfoView;
	}

	public int getSelectPageNo() {
		return selectPageNo;
	}
	public void setSelectPageNo(int selectPageNo) {
		this.selectPageNo = selectPageNo;
	}
	
}
