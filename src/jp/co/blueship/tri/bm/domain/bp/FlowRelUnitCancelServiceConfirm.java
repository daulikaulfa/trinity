package jp.co.blueship.tri.bm.domain.bp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.dao.pjtavl.eb.IPjtAvlDto;
import jp.co.blueship.tri.bm.BmDesignBusinessRuleUtils;
import jp.co.blueship.tri.bm.BmExtractEntityAddonUtils;
import jp.co.blueship.tri.bm.BmViewInfoAddonUtils;
import jp.co.blueship.tri.bm.constants.RelUnitScreenID;
import jp.co.blueship.tri.bm.constants.RelUnitScreenItemID;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpAreqLnkEntity;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpDto;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitCloseConfirmViewBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.UnitInfoInputBean;
import jp.co.blueship.tri.bm.domain.bp.beans.dto.WarningCheckBean;
import jp.co.blueship.tri.bm.domain.bp.dto.FlowRelUnitCancelServiceBean;
import jp.co.blueship.tri.bm.support.FlowRelUnitEditSupport;
import jp.co.blueship.tri.fw.cmn.utils.ItemCheckAddonUtils;
import jp.co.blueship.tri.fw.cmn.utils.RelCommonAddonUtil;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.BmDesignBeanId;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.dao.orm.DBSearchConditionAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.DBSearchSortAddonUtil;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.sm.beans.matrix.ActionStatusMatrixList;
import jp.co.blueship.tri.fw.sm.beans.matrix.dto.StatusCheckDto;
import jp.co.blueship.tri.fw.sm.beans.matrix.utils.StatusMatrixCheckUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;


/**
 * ビルドパッケージ作成・ビルドパッケージ取消受付確認画面の表示情報設定Class<br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009<br>
 */
public class FlowRelUnitCancelServiceConfirm implements IDomain<FlowRelUnitCancelServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private FlowRelUnitEditSupport support;
	public void setSupport( FlowRelUnitEditSupport support ) {
		this.support = support;
	}

	private ActionStatusMatrixList statusMatrixAction;
	public void setStatusMatrixAction( ActionStatusMatrixList action ) {
		this.statusMatrixAction = action;
	}

	@Override
	public IServiceDto<FlowRelUnitCancelServiceBean> execute( IServiceDto<FlowRelUnitCancelServiceBean> serviceDto ) {

		FlowRelUnitCancelServiceBean paramBean	= null;

		try {
			paramBean	= serviceDto.getServiceBean();

			String refererID	= paramBean.getReferer();
			String forwordID	= paramBean.getForward();
			String screenType	= paramBean.getScreenType();

			if ( !refererID.equals( RelUnitScreenID.UNIT_CANCEL_COMFIRM ) &&
					!forwordID.equals( RelUnitScreenID.UNIT_CANCEL_COMFIRM )) {
				return serviceDto;
			}

			//初期化
			paramBean.setInfoMessageIdList( null ) ;
			paramBean.setInfoMessageArgsList( null ) ;

			String[] buildNo = paramBean.getSelectedUnitNo();

			if ( refererID.equals( RelUnitScreenID.UNIT_CANCEL_COMFIRM )) {

				if ( ScreenType.next.equals( screenType )) {

					checkRelUnitCancel( paramBean );

					if( paramBean.isStatusMatrixV3() ) {
						List<IBpDto> bpDtoList = this.support.findBpDto(buildNo);
						IRaEntity[] relApplyEntities	= this.support.getRelApplyEntityByBuildNo( buildNo ).getEntities().toArray(new IRaEntity[0]);
	
						StatusCheckDto statusDto = new StatusCheckDto()
						.setServiceBean( paramBean )
						.setFinder( support )
						.setActionList( statusMatrixAction )
						.setLotIds( paramBean.getSelectedLotNo() )
						.setPjtIds( this.support.getPjtNo( this.support.getPjtEntity( bpDtoList ) ) )
						.setBpIds( buildNo )
						.setRaIds( this.support.getRelApplyNo(relApplyEntities) );
	
						StatusMatrixCheckUtils.checkStatusMatrix( statusDto );
					}
				}
			}

			if ( forwordID.equals( RelUnitScreenID.UNIT_CANCEL_COMFIRM  )) {

				ItemCheckAddonUtils.checkBuildNo( buildNo );

//				IEntityLimit<IBpEntity> limit = getBuildEntity( buildNo );
				IJdbcCondition condition = DBSearchConditionAddonUtil.getBuildConditionByBuildNo( buildNo );
				ISqlSort sort = (ISqlSort)DBSearchSortAddonUtil.getUnitSortFromDesignDefineByRelUnitCancelConfirm();
				List<IBpDto> bpDtoList = this.support.getBpDtoLimitList( condition, sort );
				//トップ画面でロットをしぼっているので、どのビルドパッケージエンティティでも属するロットは同じ
				ILotDto lotDto = this.support.getAmFinderSupport().findLotDto( bpDtoList.get(0).getBpEntity().getLotId() );

				// グループの存在チェック
				List<IGrpUserLnkEntity> groupUsers	= support.getUmFinderSupport().findGrpUserLnkByUserId( paramBean.getUserId() );
				RelCommonAddonUtil.checkAccessableGroup( lotDto, this.support.getUmFinderSupport().getGrpDao(), groupUsers );

				paramBean.setSelectedLotNo	( lotDto.getLotEntity().getLotId() );
				paramBean.setLotName		( lotDto.getLotEntity().getLotNm() );

				// 変更管理、申請情報
				for ( IBpDto bpDto : bpDtoList ) {
					// PjtId昇順、AreqId昇順にソート
					sortBpAreqLnk( bpDto.getBpAreqLnkEntities() );
				}
				// 申請情報取得（StsId、Summary使用）
				IAreqEntity[] applyEntities = this.getAssetApplyEntity( bpDtoList );
				// 変更管理情報取得（変更管理番号、変更要因番号、申請承認者を使用）
				IPjtEntity[] pjtEntities = this.getPjtEntity( bpDtoList );
				List<IPjtAvlDto> pjtDtoList = this.support.findPjtAvlDtoList( pjtEntities );
				setServiceBeanSearchResult( bpDtoList, pjtDtoList, applyEntities, paramBean );

//				クローズエラー有無チェック
				this.checkUnitCloseError( lotDto.getLotEntity() , paramBean ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(BmMessageId.BM005072S,e);
		}
	}

	/**
	 *
	 * @param pjtLotEntity
	 * @param paramBean
	 */

	private void checkUnitCloseError( ILotEntity pjtLotEntity , FlowRelUnitCancelServiceBean paramBean ) {

		List<IMessageId>	messageList		= new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		IBpEntity[] buildEntityArray = this.support.getBuildEntityByRelUnitCloseError( pjtLotEntity.getLotId() ) ;
		for( IBpEntity buildEntity : buildEntityArray ) {
			messageList.add( BmMessageId.BM001023E ) ;
			messageArgsList.add( new String[]{ buildEntity.getBpId() } ) ;
		}

		if( null == paramBean.getInfoMessageIdList() ) {
			paramBean.setInfoMessageIdList( messageList );
		} else {
			paramBean.getInfoMessageIdList().addAll( messageList ) ;
		}
		if( null == paramBean.getInfoMessageArgsList() ) {
			paramBean.setInfoMessageArgsList( messageArgsList );
		} else {
			paramBean.getInfoMessageArgsList().addAll( messageArgsList ) ;
		}

		WarningCheckBean warningCheck = new WarningCheckBean() ;
		warningCheck.setExistWarning	( ( 0 != messageList.size() ) ? true : false ) ;

		//メッセージ生成
		MessageManager messageManager = ContextAdapterFactory.getContextAdapter().getMessageManager() ;
		List<String> commentListUnitCloseError	= warningCheck.getCommentList() ;
		//[0]
		commentListUnitCloseError.add( messageManager.getMessage( paramBean.getLanguage(), BmMessageId.BM001025E , new String[]{} ) ) ;
		//[1]
		String logPath = TriStringUtils.convertPath( BmDesignBusinessRuleUtils.getWorkspaceLogPath( pjtLotEntity, paramBean.getFlowAction() ) ) ;
		commentListUnitCloseError.add( messageManager.getMessage( paramBean.getLanguage(), BmMessageId.BM001024E , new String[]{ logPath } ) ) ;

		paramBean.setWarningCheckUnitCloseError( warningCheck ) ;
	}

	/**
	 * ビルドパッケージが取消可能かをチェックする
	 * @param unitInfoInputBean ビルドパッケージ編集入力情報
	 */

	private static void checkRelUnitCancel( FlowRelUnitCancelServiceBean bean )
		throws BaseBusinessException {

		UnitInfoInputBean unitInfoInputBean = bean.getUnitInfoInputBean() ;

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			if ( !StatusFlg.off.value().equals(
					sheet.getValue(
							BmDesignBeanId.flowRelUnitCancelCheck,
							RelUnitScreenItemID.RelUnitCancelCheck.CANCEL_COMMENT.toString() )) ) {

				if ( TriStringUtils.isEmpty( unitInfoInputBean.getCancelComment() )) {
					messageList.add		( BmMessageId.BM001012E );
					messageArgsList.add	( new String[] {} );
				}
			}
			//パッケージクローズエラーチェック
			WarningCheckBean warningCheckAssetUnitCloseError = bean.getWarningCheckUnitCloseError() ;
			if ( warningCheckAssetUnitCloseError.isExistWarning() ) {

				if ( !warningCheckAssetUnitCloseError.isConfirmCheck() ) {
					messageList.add		( BmMessageId.BM001026E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {

			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Beanに検索結果を設定する。
	 * @param limit 検索結果が格納されたIEntityLimitオブジェクト
	 * @param pjtDtos 変更管理エンティティ
	 * @param applyEntities 申請情報エンティティ
	 * @param paramBean  FlowRelUnitCancelServiceBean
	 */
	private final void setServiceBeanSearchResult(
			List<IBpDto> bpDtoList, List<IPjtAvlDto> pjtDtos, IAreqEntity[] applyEntities,
			FlowRelUnitCancelServiceBean paramBean ) {

		List<UnitCloseConfirmViewBean> unitCloseConfirmViewBeanList =
											new ArrayList<UnitCloseConfirmViewBean>();

		BmViewInfoAddonUtils.setApplyInfoViewBeanAssetApplyEntity( support,
				unitCloseConfirmViewBeanList, bpDtoList, pjtDtos, applyEntities );

		paramBean.setUnitCloseConfirmViewBeanList( unitCloseConfirmViewBeanList );

	}

	/**
	 * 変更管理番号から変更管理エンティティを取得する
	 *
	 * <br>※画面表示項目のため、削除済の変更管理情報も取得する。
	 * 更新系の処理で使用するとデータの不整合が発生する場合があるため、
	 * あくまで画面表示用のみに利用すること。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @return 変更管理エンティティ
	 */
	private final IPjtEntity[] getPjtEntity( List<IBpDto> entities ) {

		String[] pjtNo = this.support.getPjtIds( entities );

		PjtCondition condition	= (PjtCondition)DBSearchConditionAddonUtil.getPjtCondition( pjtNo );
		condition.setDelStsId( null );

		IEntityLimit<IPjtEntity> limit	= this.support.getAmFinderSupport().getPjtDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IPjtEntity[0]);
	}

	/**
	 * ビルドパッケージエンティティから申請情報エンティティを取得します。
	 *
	 * <br>※画面表示項目のため、削除済の申請も取得する。
	 * 更新系の処理で使用するとデータの不整合が発生する場合があるため、
	 * あくまで画面表示用のみに利用すること。
	 *
	 * @param entities ビルドパッケージエンティティ
	 * @param selectPageNo 選択ページ
	 * @return 申請情報エンティティ
	 */
	private final IAreqEntity[] getAssetApplyEntity( List<IBpDto> entities ) {

		// ビルドパッケージされた申請情報
		String[] applyNo = BmExtractEntityAddonUtils.getAreqIds( entities );

		if ( TriStringUtils.isEmpty( applyNo )) {
			return new IAreqEntity[]{};

		} else {
			return this.getAssetApplyEntity( applyNo );
		}
	}

	/**
	 * 申請番号から申請情報エンティティを取得する
	 *
	 * <br>※画面表示項目のため、削除済の申請も取得する。
	 * 更新系の処理で使用するとデータの不整合が発生する場合があるため、
	 * あくまで画面表示用のみに利用すること。
	 *
	 * @param applyNo 申請番号
	 * @return 申請情報エンティティ
	 */
	private final IAreqEntity[] getAssetApplyEntity( String[] applyNo ) {
		AreqCondition condition	= (AreqCondition)DBSearchConditionAddonUtil.getAreqCondition( applyNo );
		condition.setDelStsId(  null  );

		IEntityLimit<IAreqEntity> limit	= this.support.getAmFinderSupport().getAreqDao().find( condition.getCondition(), null, 1, 0 );

		return limit.getEntities().toArray(new IAreqEntity[0]);
	}

	/**
	 * BpAreqLnkEntityをPjtId,AreqId順に昇順にソートします
	 * @param bpAreqLnkArray
	 */
	private static void sortBpAreqLnk( List<IBpAreqLnkEntity> bpAreqLnkArray ) {

		Collections.sort( bpAreqLnkArray,
				new Comparator<IBpAreqLnkEntity>() {
					public int compare( IBpAreqLnkEntity obj1, IBpAreqLnkEntity obj2 ) {
						int comp = compareId( obj1.getPjtId(), obj2.getPjtId() );
						if( 0!=comp ) {
							return comp;
						}
						return compareId( obj1.getAreqId(), obj2.getAreqId() );
					}
					private int compareId( String id1, String id2 ) {
						return id1.compareTo( id2 );
					}
				} );
	}
}
