package jp.co.blueship.tri.fw.session;


public class SessionScopeKeyConsts {
	/**
	 * 添付ファイル名の列挙型です。
	 *
	 */
	public enum AppendFileEnum {
		AppendFile1			( "FILE_NAME1", "FILE_INPUT_STREAM1", "FILE_SIZE1", "0001" ),	
		AppendFile2			( "FILE_NAME2", "FILE_INPUT_STREAM2", "FILE_SIZE2", "0002" ),	
		AppendFile3			( "FILE_NAME3", "FILE_INPUT_STREAM3", "FILE_SIZE3", "0003" ),	
		AppendFile4			( "FILE_NAME4", "FILE_INPUT_STREAM4", "FILE_SIZE4", "0004" ),	
		AppendFile5			( "FILE_NAME5", "FILE_INPUT_STREAM5", "FILE_SIZE5", "0005" ),	
		AppendFile6			( "FILE_NAME6", "FILE_INPUT_STREAM6", "FILE_SIZE6", "0006" );	

		private String name = null;
		private String stream = null;
		private String size = null;
		private String id = null;
		
		private AppendFileEnum( String name, String stream, String size, String no ) {
			this.name = name;
			this.stream = stream;
			this.size = size;
			this.id = no;
		}
		
		public String getId() {
			return this.id;
		}
		
		public String getFileName() {
			return this.name;
		}
		
		public String getFileInputStream() {
			return this.stream;
		}
		
		public String getFileSize() {
			return this.size;
		}
		
		public static AppendFileEnum getValue( String id ) {
			for ( AppendFileEnum appendFile : values() ) {
				if ( appendFile.getId().equals( id ) ) {
					return appendFile;
				}
			}
		
			return null;
		}
	}

	public static final String FLWC00_USER_ID = "FLWC00_USER_ID";
	public static final String FLWC00_USER_NAME = "FLWC00_USER_NAME";
	public static final String FLWL00_GROUP = "FLWC00_GROUP";
	public static final String FLWL00_GROUP_ID = "FLWC00_GROUP_ID";
	public static final String FLWC00_PROJECT_ID = "FLWC00_PROJECT_ID";
	public static final String FLWC00_PASSWORD_LIMIT = "FLWC00_PASSWORD_LIMIT";
	public static final String FLWC00_AGENT_COUNT = "FLWC00_AGENT_COUNT";
	public static final String FLWC00_REFERER = "FLWC00_REFERER";
	public static final String FLWL06_CHECK_RESOURCE_LIST = "FLWL06_CHECK_RESOURCE_LIST";
	public static final String FLWL06_VIEW_RESOURCE_LIST = "FLWL06_VIEW_RESOURCE_LIST";
	public static final String FLWL06_SELECT_CURRENT_PATH = "FLWL06_SELECT_CURRENT_PATH";
	
	public static final String FLWC00_PROJECT_NAME = "FLWC00_PROJECT_NAME";
	public static final String FLWC00_VERSION = "FLWC00_VERSION";
	
	public static final String FLWL06_USER_NAME = "FLWL06_USER_NAME";
	public static final String FLWL06_USER_GROUP = "FLWL06_USER_GROUP";
	public static final String FLWL06_LEND_SUBJECT = "FLWL06_LEND_SUBJECT";
	public static final String FLWL06_LEND_CONTENT = "FLWL06_LEND_CONTENT";
	public static final String FLWL06_LEND_INCIDENT = "FLWL06_LEND_INCIDENT";
	public static final String FLWL06_LEND_RESOURCE_LIST = "FLWL06_LEND_RESOURCE_LIST";
	public static final String FLWL06_LEND_NO = "FLWL06_LEND_NO";
	public static final String FLWL06_LEND_DATE = "FLWL06_LEND_DATE";
	public static final String FLWL06_LEND_RESOURCE_MANAGER = "FLWL06_LEND_RESOURCE_MANAGER";
	
	public static final String FLWL02_RETURN_NO = "FLWL02_RETURN_NO";
	public static final String FLWL02_USER_NAME = "FLWL02_USER_NAME";
	public static final String FLWL02_USER_GROUP = "FLWL02_USER_GROUP";
	public static final String FLWL02_LEND_SUBJECT = "FLWL02_USER_GROUP";
	public static final String FLWL02_LEND_CONTENT = "FLWL02_LEND_CONTENT";
	public static final String FLWL02_LEND_INCIDENT = "FLWL02_LEND_INCIDENT";
	public static final String FLWL02_LEND_RESOURCE_MANAGER = "FLWL02_LEND_RESOURCE_MANAGER";

	public static final String FLWL03_SELECT_APPLY_NO = "FLWL03_SelectApplyNo";
	public static final String FLWL03_SELECT_APPLY_REASON = "FLWL03_SelectApplyReasn";
	public static final String FLWCS00_SECTION_ID = "FLWL03_SectionID";
	
	public static final String FLWL07_SELECT_RETURN_NO = "FLWL07_SELECT_RETURN_NO";
	
	public static final String LIST_SEARCH_ITEM = "LIST_SEARCH_ITEM";
	public static final String LIST_SEARCH_CONDITION = "LIST_SEARCH_CONDITION";
	public static final String CONTENTS_SEARCH_CONDITION = "CONTENTS_SEARCH_CONDITION";
	
	public static final String FLOW_ACTION_ID = "flowActionId";
		
	public static final String TOP_SCREEN_NAME = "topScreenName";

	public static final String ACTION = "action";
	public static final String ACTION_DETAIL = "actionDetail";
	public static final String ACTION_DETAIL_VIEW = "actionDetailView";
	public static final String ACTION_DOWNLOAD = "actionDownLoad";
	public static final String ACTION_SEARCH = "actionSearch";
	public static final String ACTION_ISSUANCE = "actionIssuance";
	public static final String ACTION_SEARCH_BTN = "actionSearchBtn";
		
	public static final String REL_NO = "relNo";
	public static final String REL_CONTENT = "relContent";
	public static final String REL_COPE_SECTION = "relCopeSection";
	public static final String REL_COPE_USER = "relCopeUser";
	public static final String DISTRIBUTION_FORM = "distributionForm";
	public static final String REL_SUMMARY = "relSummary";
	public static final String REL_APPROVE_USER = "relApproveUser";
	public static final String REL_APPROVE_DATE = "relApproveDate";
	public static final String REL_STATUS_VIEW = "statusView";
	public static final String REL_ENV_SELECT = "selectedEnvNo";
	public static final String REL_BLD_SELECT = "selectedBuildNo";

	public static final String PJT_NO = "pjtNo";
	public static final String PJT_SUMMARY = "pjtSummary";
	public static final String PJT_CONTENT = "pjtContent";
	public static final String PJT_RFC_NO = "pjtRfcNo";
	public static final String PJT_LOT_SELECT = "pjtLotSelect";
	public static final String PJT_MDL_SELECT = "pjtModuleSelect";
	public static final String PJT_STATUS_VIEW = "statusView";
	
	public static final String BLD_PJT_NO = "bldPjtNo";
	public static final String BLD_APPLY_NO = "bldApplyNo";
	public static final String BLD_LOT_NO = "bldLotNo";
	public static final String BLD_UNIT_NAME = "bldUnitName";
	public static final String BLD_UNIT_SUMMARY = "bldUnitSummary";

	public static final String SELECTED_LOT_NO = "selectedLotNo";
	public static final String SELECTED_SERVER_ID = "selectedServerId";
	
	public static final String RP_GENERATED_LOT_NO = "rpGeneratedLotNo";
	public static final String RP_GENERATED_SERVER_ID = "rpGeneratedServerId";
	
	public static final String RC_GENERATED_LOT_NO = "rcGeneratedLotNo";
	public static final String RC_GENERATED_SERVER_ID = "rcGeneratedServerId";
	
	public static final String SELECTED_REPORT_NO = "selectedReportNo";
	public static final String VIEW_REPORT_NO = "viewReportNo";
	
	public static final String PJT_MERGE_RESULT = "mergeResult" ;

}
