package jp.co.blueship.tri.fw.session;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * セション情報を{@link javax.servlet.http.HttpSession}オブジェクトに格納するラッパークラスです。
 *
 * @author kanda
 *
 * @see HttpSession#getAttribute(java.lang.String)
 * @see HttpSession#setAttribute(java.lang.String, java.lang.Object)
 * @see HttpSession#removeAttribute(java.lang.String)
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
class HttpSessionWrapper implements ISessionInfo {



	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionContext#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String name) {
		return this.httpSession.getAttribute(name);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionContext#setAttribute(java.lang.String, java.lang.String)
	 */
	public void setAttribute(String name, Object value) {
		this.httpSession.setAttribute(name, value);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionContext#removeAttribute(java.lang.String)
	 */
	public void removeAttribute(String name) {
		this.httpSession.removeAttribute(name);
	}

	/**
	 * Invalidates this session then unbinds any objects bound to it.
	 */
	public void invalidate() {
		this.httpSession.invalidate();
	}

	/**
	 * <i><b>このメソッドは、処理が実装されていません。</b></i>
	 * インターフェースを合わせるため、空メソッドとして作成していますが、<br>
	 * 実行しても何も処理されません。
	 *
	 * @deprecated
	 */
	public void removeAllAttribute() {
		// java EEのAPIを使用する場合、Session開放時に{@link HttpSession}が開放されるため
		// 全ての属性を削除する処理は不要
	}

	/**
	 * {@link HttpServletRequest}オブジェクトを引数にして、初期処理を行います。
	 * @param obj {@link HttpServletRequest}オブジェクト
	 */
	public void init(Object obj) {
		this.httpSession = ((HttpServletRequest)obj).getSession();
	}

	HttpSessionWrapper() {}

	private HttpSession httpSession = null;

	public Enumeration<?> getAttributeNames() {
		return this.httpSession.getAttributeNames();
	}

}
