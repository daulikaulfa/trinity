package jp.co.blueship.tri.fw.session;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * セション情報実装クラスを生成するファクトリクラスです。
 * @author kanda
 *
 */
public class SessionInfoFactory {


	/**
	 * {@link HttpSessionWrapper}オブジェクトを使用するときに
	 * {@link SessionInfoFactory#getSessionContext(String)}に渡す
	 * 指定子です。
	 */
	public static final String ASSIGN_HTTPSESSION = "httpsession";

	/**
	 * セション情報を格納するオブジェクトを生成します。
	 *
	 * @param assign 使用するオブジェクトを指定するための指定子
	 * @return セション情報を格納するオブジェクト
	 */
	public static ISessionInfo getSessionContext(String assign){

		if(assign != null) {
			if(ASSIGN_HTTPSESSION.equals(assign)) {
				return new HttpSessionWrapper();
			}
		}
		throw new TriSystemException(SmMessageId.SM005110S);
	}

}
