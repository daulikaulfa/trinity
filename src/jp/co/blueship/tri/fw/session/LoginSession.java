package jp.co.blueship.tri.fw.session;

import java.util.ArrayList;
import java.util.List;

import org.springframework.security.core.session.SessionInformation;
import org.springframework.security.core.session.SessionRegistry;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.security.spi.TriAuthUser;

/**
 * 現在ログイン中のユーザが使用するセッションを管理するクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class LoginSession {

	@VisibleForTesting
	static final String SESSION_REGISTORY_BEAN_NAME = "triSessionRegistory";

	/**
	 * 指定されたユーザがログイン中かどうかを返します。
	 *
	 * @param userID ユーザID
	 * @return ログイン中の場合はtrue
	 */
	public boolean isLoggedInUser(String userID) {

		List<Object> principals = sessionRegistory().getAllPrincipals();
		List<Object> activePrincipals = new ArrayList<>();
		if ( TriCollectionUtils.isEmpty(principals) ) {
			return false;
		}
		
		for (Object principal : principals) {
			if ( principal instanceof TriAuthUser ) {
				List<SessionInformation> activeUserSessions = sessionRegistory().getAllSessions(principal, false);
				if( !TriCollectionUtils.isEmpty(activeUserSessions) ) {
					activePrincipals.add(principal);
				}
			}
		}
		return FluentList.from(activePrincipals).contains(userIdIs(userID));
	}
	
	public void clearOldUserSession(String userID) {
		List<Object> principals = sessionRegistory().getAllPrincipals();
		for (Object principal : principals) {
			if ( principal instanceof TriAuthUser ) {
				if( TriStringUtils.isEquals(((TriAuthUser) principal).getUserID(), userID )) {
					List<SessionInformation> activeUserSessions = sessionRegistory().getAllSessions(principal, false);
					if( TriCollectionUtils.isNotEmpty(activeUserSessions) && activeUserSessions.size() > 1 ) {
						for ( int i = 0; i < activeUserSessions.size() - 1; i++ ) {
							sessionRegistory().removeSessionInformation( activeUserSessions.get(i).getSessionId() );
						}
					}
				}
			}
		}
	}

	private SessionRegistry sessionRegistory() {
		return (SessionRegistry) Contexts.getInstance().getBeanFactory().getBean(SESSION_REGISTORY_BEAN_NAME);
	}

	private static TriPredicate<Object> userIdIs(final String userID) {

		return new TriPredicate<Object>() {

			@Override
			public boolean evalute(Object principal) {

				if (!(principal instanceof TriAuthUser)) {
					return false;
				}
				
				return TriStringUtils.isEquals(((TriAuthUser) principal).getUserID(), userID);
			}
		};
	}

}
