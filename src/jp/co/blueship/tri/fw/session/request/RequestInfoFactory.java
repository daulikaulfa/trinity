package jp.co.blueship.tri.fw.session.request;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * リクエスト情報実装クラスを生成するファクトリクラスです。
 *
 * @version V3L10R01
 *
 * @version SP-201511-1_V3L14R01
 * @author Yukihiro Eguchi
 */
public class RequestInfoFactory {

	/**
	 * {@link HttpServletRequestWrapper}オブジェクトを使用するときに
	 * {@link RequestInfoFactory#getRequestContext(String)}に渡す
	 * 指定子です。
	 */
	public static final String ASSIGN_HTTPSERVLETREUEST = "httpsservletrequest";

	public static final String SERVLET_CHARSET = "charset";
	public static final String SERVLET_ENCODING = "encoding";

	/**
	 * リクエスト情報を格納するオブジェクトを生成します。
	 *
	 * @param assign 使用するオブジェクトを指定するための指定子
	 * @return リクエスト情報を格納するオブジェクト
	 */
	public static IRequestInfo getRequestContext(String assign){

		if(assign != null) {
			if(ASSIGN_HTTPSERVLETREUEST.equals(assign)) {
				return new HttpServletRequestWrapper();
			}
		}
		throw new TriSystemException(SmMessageId.SM005109S);
	}

}
