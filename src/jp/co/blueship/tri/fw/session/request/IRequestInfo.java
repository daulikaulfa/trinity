package jp.co.blueship.tri.fw.session.request;

import jp.co.blueship.tri.fw.session.SessionInfoFactory;

/**
 * ユーザを一意に識別し、そのユーザについての情報を格納する方法を提供します。
 *
 * 基本的には、J2EEにおけるHttpServletRequestクラスの一機能と似ていますが、
 * 格納できる<code>value</code>の型がStringのみなどの制約があります。
 *
 * @version V3L10R01
 *
 * @version SP-201511-1_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface IRequestInfo {

	

	/**
	 * 指定された名前でこのリクエストパラメータにバインドされた<b>オブジェクト</b>を返します。
	 * その名前でバインドされた文字列がない場合は、null を返します。
	 * @return  指定された名前に対応したオブジェクト。指定された名前と一致する文字列が存在しない場合は null
	 *
	 */
	public String getParameter(String key);

	/**
	 *
	 */
	public String[] getParameterValues(String key);

	/**
	 * 指定された名前でこのリクエストにバインドされた<b>文字列</b>を返します。
	 * その名前でバインドされた文字列がない場合は、null を返します。
	 *
	 * @param name 文字列の名前を指定する文字列
	 * @return 指定された名前に対応した文字列。指定された名前と一致する文字列が存在しない場合は null
	 */
	public Object getAttribute(String name);

	/**
	 * 指定された名前を使用して、文字列をこのリクエストにバインドします。
	 * 同じ名前の文字列がすでにリクエストにバインドされている場合は、文字列が置き換えられます。
	 *
	 * @param name 文字列がバインドされる名前。null であってはならない
	 * @param value バインドされる文字列
	 */
	public void setAttribute(String name, Object value);

	/**
	 * このHTTPリクエストを発行したユーザのIDを返す。
	 * @return ユーザID
	 */
	public String getUserId();

	/**
	 * 実装クラスに依存した、初期処理を行います。
	 * 詳細は、使用する実装クラスのドキュメントを参照してください。
	 * @param obj 実装クラスに依存した初期処理に渡すオブジェクト
	 * @see SessionInfoFactory
	 */
	public void init(Object obj);

	public void setCharacterEncoding(String env);

}
