package jp.co.blueship.tri.fw.session.request;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.springframework.security.core.context.SecurityContextHolder;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 *
 * @version V3L10R01
 *
 * @version SP-201511-1_V3L14R01
 * @author Yotei Tanaka
 */
class HttpServletRequestWrapper implements IRequestInfo{
	String encoding = null;

	private HttpServletRequest request = null;

	public String  getParameter(String key) {
		return this.getEncodingString(request.getParameter( key ));
	}

	public String[] getParameterValues(String key) {
		List<String> results = new ArrayList<String>();
		String[] vals = request.getParameterValues( key );

		if( TriStringUtils.isEmpty( vals ) ){
			return vals;
		}
		for( String val : vals ){
			results.add( this.getEncodingString( val) );
		}

		return results.toArray(new String[0]);
	}

	private String getEncodingString( String str ){
		if( !this.isEncoding() ){
			return str;
		}
		if( TriStringUtils.isEmpty(str) ){
			return str;
		}
		try {
			return ( new String( str.getBytes( request.getCharacterEncoding() ), this.encoding ) );
		} catch (UnsupportedEncodingException e) {
			throw new TriSystemException( SmMessageId.SM001019E, e);
		}
	}

	public Object getAttribute(String name) {
		Object obj = request.getAttribute(name);
		if( null == obj ){
			return obj;
		}
		if( obj instanceof String ){
			String str = (String)obj;
			return getEncodingString( str );
		}
		return obj;
	}

	public void init(Object obj) {
		this.request = (HttpServletRequest)obj;
	}

    public void setCharacterEncoding(String env) {
    	this.encoding = env;
    }

	public void setAttribute(String name, Object value) {
		this.request.setAttribute(name, value);
	}

	private boolean isEncoding() {
		if ( ServletFileUpload.isMultipartContent(request) ) {
			return false;
		}

		if ( TriStringUtils.isEmpty(this.encoding) ) {
			return false;
		}

		if ( this.encoding.equalsIgnoreCase(request.getCharacterEncoding()) ) {
			return false;
		}
		return true;
	}

	/**
	 * このHTTPリクエストを発行したユーザのIDを返す。
	 *
	 * @return ユーザID
	 */
	public String getUserId() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

}
