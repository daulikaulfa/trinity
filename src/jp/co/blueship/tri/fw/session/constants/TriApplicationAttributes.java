package jp.co.blueship.tri.fw.session.constants;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum TriApplicationAttributes {
	none			( "" ),
    ;

    private String value = null ;

    private TriApplicationAttributes( String value ) {
        this.value = value ;
    }

    public String value() {
        return this.value ;
    }

    public static TriApplicationAttributes value( String value ) {
        for ( TriApplicationAttributes attr : values() ) {
            if ( attr.value().equals( value ) ) {
                return attr;
            }
        }

        return none;
    }

}
