package jp.co.blueship.tri.fw.session.constants;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum TriSessionAttributes {
	none					( "" ),
    //--------------------------------------------------------------------
    // Normal Session
    //--------------------------------------------------------------------
    WindowsId				( "WindowsId" ),
    RedirectWindowsId 		( "RedirectWindowsId" ),
    UserLanguage			( "UserLang" ),
    UserTimeZone			( "UserTimeZone" ),
    SelectedLanguage		( "selectedLang" ),
    ProjectName				( "ProjectName" ),
    ProjectIconPath			( "ProjectIconPath" ),
    UserIconPath			( "UserIconPath" ),
    UserName				( "UserName" ),
    //--------------------------------------------------------------------
    // WindowsID Only
    //--------------------------------------------------------------------
    SelectedLotId			( "selectedLotId" ),
    SelectedLotName			( "SelectedLotName" ),
    SelectedLotThemeColor	( "SelectedLotThemeColor" ),
    SelectedLotIconPath		( "SelectedLotIconPath" ),
    SelectedLotEnabled		( "SelectedLotEnabled" ),
    ;

    private String value = null ;

    private TriSessionAttributes( String value ) {
        this.value = value ;
    }

    public String value() {
        return this.value ;
    }

}
