package jp.co.blueship.tri.fw.session;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;

/**
 * ユーザを一意に識別し、そのユーザについての情報を格納する方法を提供します。
 *
 * 基本的には、J2EEにおけるHttpSessionクラスの一機能と似ていますが、
 * 格納できる<code>value</code>の型がStringのみなどの制約があります。
 *
 * @author kanda
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public interface ISessionInfo {



	/**
	 * 指定された名前でこのセションにバインドされた<b>文字列</b>を返します。
	 * その名前でバインドされた文字列がない場合は、null を返します。
	 *
	 * @param name 文字列の名前を指定する文字列
	 * @return 指定された名前に対応した文字列。指定された名前と一致する文字列が存在しない場合は null
	 */
	public Object getAttribute(String name);

	/**
	 * 指定された名前を使用して、文字列をこのセッションにバインドします。
	 * 同じ名前の文字列がすでにセッションにバインドされている場合は、文字列が置き換えられます。
	 *
	 * @param name 文字列がバインドされる名前。null であってはならない
	 * @param value バインドされる文字列
	 */
	public void setAttribute(String name, Object value);

	/**
	 * 指定された名前でバインドされた文字列をこのセッションから削除します。
	 * セッションに指定された名前でバインドされた文字列がない場合、このメソッドは何も実行しません。
	 *
	 * @param name このセッションから削除される文字列の名前
	 */
	public void removeAttribute(String name);

	/**
	 * ユーザのセションにバインドされた全ての情報を破棄します。
	 * ただし、<b>実装クラスによって大きく異なるため、
	 * 必ず使用する実装クラスのドキュメントを参照してください。</b>
	 *
	 */
	public void removeAllAttribute();

	/**
	 * Invalidates this session then unbinds any objects bound to it.
	 */
	public void invalidate();

	/**
	 * 実装クラスに依存した、初期処理を行います。
	 * 詳細は、使用する実装クラスのドキュメントを参照してください。
	 * @param obj 実装クラスに依存した初期処理に渡すオブジェクト
	 * @see SessionInfoFactory
	 */
	public void init(Object obj);

	/**
	 * Request内に存在する全ての属性のKeyを取得します。
	 * @see HttpServletRequest
	 * @return 全ての属性のKey
	 */
	public Enumeration<?> getAttributeNames();
}
