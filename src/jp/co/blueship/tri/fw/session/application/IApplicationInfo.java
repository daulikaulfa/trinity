package jp.co.blueship.tri.fw.session.application;

import java.util.Enumeration;

/**
 * アプリケーション内で一意に保つべき情報を格納する方法を提供します。
 *
 * 基本的には、J2EEにおけるSrvletContextクラスの一機能と似ていますが、
 * 格納できる<code>value</code>の型がStringのみなどの制約があります。
 *
 * @author kanda
 *
 */
public interface IApplicationInfo {

	

	/**
	 * 指定された名前に対応する属性を返します。
	 * その名前の文字列がない場合は、null を返します。
	 *
	 * @param name 属性の名前を指定する文字列
	 * @return 属性の値を含む文字列。指定された名前と一致する属性が存在しない場合は null
	 */
	public Object getAttribute(String name);

	/**
	 * 文字列を、アプリケーション内にある属性名にバインドします。
	 * 指定された名前がすでに属性に使用されている場合は、その属性が新しい属性に置き換えられます。
	 *
	 * @param name 属性の名前を指定する String。null であってはならない
	 * @param value バインドされる文字列
	 */
	public void setAttribute(String name, Object object);

	/**
	 * 現在、属性に設定されているKeyを {@link Enumeration} クラスで取得します。
	 *
	 * @return 設定されている全てのKey
	 */
	public Enumeration<?> getAttributeNames();

	/**
	 * 指定された名前でバインドされた文字列をこのアプリケーションから削除します。
	 *
	 * @param name 削除する属性の名前
	 */
	public void removeAttribute(String name);

	/**
	 * アプリケーションにバインドされた全ての情報を破棄します。
	 * ただし、<b>実装クラスによって大きく異なるため、
	 * 必ず使用する実装クラスのドキュメントを参照してください。</b>
	 *
	 */
	public void removeAllAttribute();

	/**
	 * 実装クラスに依存した、初期処理を行います。
	 * 詳細は、使用する実装クラスのドキュメントを参照してください。
	 * @param obj 実装クラスに依存した初期処理に渡すオブジェクト
	 * @see ApplicationInfoFactory
	 */
	public void init(Object obj);
}
