package jp.co.blueship.tri.fw.session.application;

import java.util.Enumeration;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

/**
 * アプリケーション情報を{@link javax.servlet.ServletContext}オブジェクトに格納するラッパークラスです。
 *
 * @see javax.servlet.ServletContext#getAttribute(java.lang.String)
 * @see javax.servlet.ServletContext#setAttribute(java.lang.String, java.lang.Object)
 * @see javax.servlet.ServletContext#removeAttribute(java.lang.String)
 *
 */
class ServletContextWrapper implements IApplicationInfo {

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.application.IApplicationContext#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String name) {
		return (String)this.servletContext.getAttribute(name);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.application.IApplicationContext#setAttribute(java.lang.String, java.lang.String)
	 */
	public void setAttribute(String name, Object object) {
		this.servletContext.setAttribute(name, object);
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.application.IApplicationContext#removeAttribute(java.lang.String)
	 */
	public void removeAttribute(String name) {
		this.servletContext.removeAttribute(name);
	}

	/*
	 * (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.application.IApplicationContext#getAttributeNames()
	 */
	public Enumeration<?> getAttributeNames(){
		return servletContext.getAttributeNames();
	}

	/**
	 * <i><b>このメソッドは、処理が実装されていません。</b></i>
	 * インターフェースを合わせるため、空メソッドとして作成していますが、<br>
	 * 実行しても何も処理されません。
	 *
	 * @deprecated
	 */
	public void removeAllAttribute() {
		// java EEのAPIを使用する場合、Servlet停止時に{@link ServletContext}が開放されるため
		// 全ての属性を削除する本メソッドは不要
	}

	/**
	 * {@link HttpServletRequest}オブジェクトを引数にして、初期処理を行います。
	 * @param obj {@link HttpServletRequest}オブジェクト
	 */
	public void init(Object obj) {
		this.servletContext = ((HttpServletRequest)obj).getSession()
										.getServletContext();
	}

	ServletContextWrapper() {}

	private ServletContext servletContext = null;

}
