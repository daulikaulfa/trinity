package jp.co.blueship.tri.fw.session.application;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * アプリケーション情報実装クラスを生成するファクトリクラスです。
 * @author kanda
 *
 */
public class ApplicationInfoFactory {

	/**
	 * {@link ServletContextWrapper}オブジェクトを使用するときに
	 * {@link ApplicationInfoFactory#getApplicationContext(String)}に渡す
	 * 指定子です。
	 */
	public static final String ASSIGN_SERVLETCONTEXT = "servletcontext";

	/**
	 * セション情報を格納するオブジェクトを生成します。
	 *
	 * @param assign 使用するオブジェクトを指定するための指定子
	 * @return セション情報を格納するオブジェクト
	 */
	public static IApplicationInfo getApplicationContext(String assign){

		if(assign != null) {
			if(ASSIGN_SERVLETCONTEXT.equals(assign)) {
				return new ServletContextWrapper();
			}
		}
		throw new TriSystemException( SmMessageId.SM005108S );
	}

}
