package jp.co.blueship.tri.fw.session;

import java.util.Enumeration;

import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;

/**
 * ISessionInfoのセッション変数にアクセスするためのファインダーです。
 * <br>
 * <br>同一セッション内でマルチウインドウ画面を操作する場合、ウインドウごとに
 * セッション変数を管理したい場合、このファインダーを通してセッション変数の設定／取得を行います。
 * <br>
 * <br>BaseResponseBean内で管理されるwindowsIdをプレフィックスとしてセッション変数を管理します。
 * これにより、セッション＋windwosId単位に一意なセッション変数を管理出来ます。
 * <br>
 * <br>このファインダーを通して、セッションにアクセスする場合、セッションの初期化を適切なタイミングで
 * 行って下さい。
 * たとえば、'dialog'というwindowsIdを呼び出した場合、'dialog'に関連する
 * セッションを初期化して'+dialog'というウインドウＩＤを戻します。
 * これによりマルチ画面から同じウインドウを共有した場合であっても、最初の呼び出しで
 * 必ず初期化されますので、以前にセッションに残った変数で動作が不良になる事はありません。
 *
 */
public class SessionMultiWindows implements ISessionInfo {
	private static final ILog log = TriLogFactory.getInstance();

	private static final String WINDOWS_ID = "windowsId";

	private ISessionInfo session = null;
	private IRequestInfo reqInfo = null;

	/**
	 * インスタンスを生成します。
	 *
	 * @param session セッション情報
	 * @param reqInfo リクエスト情報
	 */
	public SessionMultiWindows( ISessionInfo session, IRequestInfo reqInfo ) {
		this.session = session;
		this.reqInfo = reqInfo;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionInfo#getAttribute(java.lang.String)
	 */
	public Object getAttribute(String name) {

		return session.getAttribute( getAttributeKey(name) );
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionInfo#getAttributeNames()
	 */
	public Enumeration<?> getAttributeNames() {
		return session.getAttributeNames();
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionInfo#init(java.lang.Object)
	 */
	public void init(Object obj) {
		session.init(obj);
	}

	/**
	 * ユーザのセションにバインドされた同一のWindowsIdの情報を破棄します。
	 * windowsIdに関連しないセッション情報は、初期化の対象外となります。
	 * このメソッドで初期化されるのは、あくまでもwindwosIdに紐づくセッション情報だけです。
	 * <br>
	 * ただし、<b>実装クラスによって大きく異なるため、
	 * 必ず使用する実装クラスのドキュメントを参照してください。</b>
	 *
	 */
	public void removeAllAttribute() {
		String id = this.getAttributeKey("");

		Enumeration<?> enu = session.getAttributeNames();
		for ( ;enu.hasMoreElements(); ) {
			Object name = (Object)enu.nextElement();
			if ( ! (name instanceof String) )
				continue;

			if ( ((String)name).startsWith(id) ) {
				LogHandler.debug( log , "SessionMultiWindows.removeAllAttribute:key:= " + (String)name);
				session.removeAttribute( (String)name );
			}
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionInfo#removeAttribute(java.lang.String)
	 */
	public void removeAttribute(String name) {
		session.removeAttribute( getAttributeKey(name) );
	}

	@Override
	public void invalidate() {
		session.invalidate();
	}

	/**
	 * 指定されたキーが初期化対象かどうかを判定します。
	 * 「保持するキー」に含まれる場合は、初期化対象では無いと見なします。
	 *
	 * @param name 初期化対象のキー値
	 * @param keepNames 保持するキー値
	 * @return 保持するキーに含まれる場合、falseを戻します。それ以外はtrueを戻します。
	 */
	public boolean isRemoveAttribute(String name, String[] keepNames) {
		boolean isKeep = true;

		for ( String keepName : keepNames ) {
			if ( keepName.equals( name ) ) {
				isKeep = false;
				break;
			}
		}

		return isKeep;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.scope.session.ISessionInfo#setAttribute(java.lang.String, java.lang.Object)
	 */
	public void setAttribute(String name, Object value) {
		LogHandler.debug( log , "SessionMultiWindows.setAttribute:key:= " + getAttributeKey((String)name) + " value:= " + value);
		session.setAttribute( getAttributeKey(name), value);
	}

	/**
	 * バインドするユニークなキーを取得します。
	 * 指定値が同じ名前であっても、windwosIdごとに別々に管理されます。
	 *
	 * @param name 文字列がバインドされる名前。null であってはならない
	 * @return 取得した文字列を戻します。
	 */
	private String getAttributeKey( String name ) {
		String prefix = getWindwosId();
		prefix = (null == prefix)? "windowsId:none:": "windowsId:" + prefix + ":";

		return prefix + name;
	}

	/**
	 * WindowsIdを取得します。
	 *
	 * @return 取得したWindowsId。
	 */
	public final String getWindwosId() {
		String prefix = "";

		if ( isWindwosIdInit() ) {
		} else {
			prefix = "+";
		}

		return prefix + reqInfo.getParameter( WINDOWS_ID );
	}

	/**
	 * WindowsIdのセッションが初期化済かどうかを判定します。
	 *
	 * @return 初期化済の場合、true。それ以外はfalseを戻します。
	 */
	 public final boolean isWindwosIdInit() {
		String windowsId = reqInfo.getParameter( WINDOWS_ID );
		if ( null == windowsId || ! windowsId.startsWith("+") ) {
			return false;
		}

		return true;
	}

	/**
	 * 遷移先画面の画面IDを取得します。
	 *
	 * @param p 対象となるPresentationProsecutor
	 * @return 取得した画面ID
	 */
	public final String getForward( PresentationProsecutor p ) {
		String referer		= reqInfo.getParameter( "referer" );
		String forward		= reqInfo.getParameter( "forward" );

		if ( p.getBussinessException() != null ) {
			forward = referer;
		}

		return forward;
	 }

	/**
	 * 遷移元画面の画面IDを取得します。
	 *
	 * @param p 対象となるPresentationProsecutor
	 * @return 取得した画面ID
	 */
	public final String getReferer( PresentationProsecutor p ) {
		String referer		= reqInfo.getParameter( "referer" );

		return referer;
	 }

	/**
	 * 遷移画面のタイプを取得します。
	 *
	 * @param p 対象となるPresentationProsecutor
	 * @return {@link jp.co.blueship.tri.fw.constants.ScreenType }
	 */
	public final String getScreenType( PresentationProsecutor p ) {
		String screenType	= reqInfo.getParameter( "screenType" );

		if ( p.getBussinessException() != null ) {
			screenType = ScreenType.bussinessException;
		}

		return screenType;
	 }

}
