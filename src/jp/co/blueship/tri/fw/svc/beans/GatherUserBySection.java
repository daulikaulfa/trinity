package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;

/**
 * 任意の部署から、部署に所属するユーザを取得します。 <br>
 * columnに指定可能な項目 <br>
 * ・部署名称 <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherUserBySection implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao;
	private IDeptDao deptDao;
	private String column;

	public GatherUserBySection() {
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	public void setDeptDao(IDeptDao deptDao) {
		this.deptDao = deptDao;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 *
	 * @param column 該当項目名
	 */
	public final void setColumn(String column) {
		this.column = column;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {

		SendMailBean targetBean = null;

		try {
			targetBean = serviceDto.getServiceBean().getTarget();
			if (null == targetBean.getUsersView()) {
				targetBean.setUsersView(new ArrayList<IUserEntity>());
			}

			for (IDeptEntity entity : deptsFrom(this.column)) {

				if (TriStringUtils.isEmpty(targetBean.getUserView())) {
					targetBean.setUserView(entity.getDeptNm());
				}

				targetBean.getUsersView().addAll(usersFrom(entity.getDeptId()));
			}

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005064S, e);
		}

		return serviceDto;
	}

	private List<IUserEntity> usersFrom(String deptId) {

		UserCondition condition = new UserCondition();
		condition.setDeptId(deptId);
		return userDao.find(condition.getCondition());
	}

	private List<IDeptEntity> deptsFrom(String deptName) {

		DeptCondition condition = new DeptCondition();
		condition.setDeptNm(deptName);
		return deptDao.find(condition.getCondition());
	}

}
