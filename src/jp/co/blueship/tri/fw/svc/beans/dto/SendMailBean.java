package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;
import java.util.List;

import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;

public class SendMailBean
					implements	Serializable {

	/**
	 * デフォルトシリアル
	 */
	private static final long serialVersionUID = 1L;


	private String userView = null;
	private List<IUserEntity> users = null;

	/**
	 * メール本文に表示するユーザ名をを戻します
	 *
	 * @return 表示名称
	 */
	public String getUserView() {
		return userView;
	}

	/**
	 * メール本文に表示するユーザ名を設定します
	 *
	 * @param value 表示名称
	 */
	public void setUserView(String value) {
		this.userView = value;
	}

	/**
	 * 取得したユーザ情報を戻します
	 *
	 * @return ユーザ情報エンティティ
	 */
	public List<IUserEntity> getUsersView() {
		return users;
	}

	/**
	 * 条件に該当するユーザ情報を設定します
	 *
	 * @param users ユーザ情報エンティティ
	 */
	public void setUsersView(List<IUserEntity> users) {
		this.users = users;
	}
}
