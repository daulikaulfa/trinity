package jp.co.blueship.tri.fw.svc.beans;

import java.util.Map;
import java.util.TreeMap;

import jp.co.blueship.tri.agent.bm.svc.flow.beans.dto.IBuildTaskBean;
import jp.co.blueship.tri.agent.rm.svc.flow.beans.dto.IReleaseTaskBean;
import jp.co.blueship.tri.am.dao.constants.TriExtensionType;
import jp.co.blueship.tri.am.dao.ext.eb.IExtEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineAgentEntity;
import jp.co.blueship.tri.bm.dao.bldtimeline.eb.IBldTimelineEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.ITaskFlowProcDao;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.ITaskFlowProcEntity;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcCondition;
import jp.co.blueship.tri.bm.dao.taskflowproc.eb.TaskFlowProcEntity;
import jp.co.blueship.tri.bm.support.BmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;



/**
 * タスク処理のＤＢアクセスのサポートClass
 * <br>
 * <p>
 * タスクからＤＢにアクセスする処理を支援するサポートクラスです。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class TaskFinderSupport extends BmFinderSupport {

	private static final String SEP = SystemProps.LineSeparator.getProperty();

	/**
	 * 拡張子情報からすべてのレコードを取得する<br>
	 * @return
	 */
	public final Map<String,Boolean> getExtDecisionMap() {

		Map<String, IExtEntity> extMap = this.getAmFinderSupport().getExtMap();

		Map<String,Boolean> map = new TreeMap<String,Boolean>() ;
		for( IExtEntity entity : extMap.values() ) {
			Boolean bynaryExtension = ( TriExtensionType.binary.equals( entity.getExtTyp() ) ) ? true : false ;
			map.put( entity.getExt() , bynaryExtension ) ;
		}

		return map ;
	}

	public void writeBuildProcessByIrregular(
			IBldTimelineEntity request,
			IBuildTaskBean bean,
			IBldTimelineAgentEntity line,
			Exception e) {

		ITaskFlowProcEntity buildProcessEntity = new TaskFlowProcEntity();

		String serverNo = ( null != line ) ? line.getBldEnvId() : "" ;		//lineがnullだったときのために
		String workflowNo = ( null != line ) ? line.getTaskFlowId() : "" ;	//同上
		String sequenceNo = ( null != line ) ? line.getTargetSeqNo().toString() : "" ;	//同上

		buildProcessEntity.setProcId		( bean.getProcId() );
//		buildProcessEntity.setLotId			( bean.getLotNo() );
		buildProcessEntity.setBldEnvId		( request.getBldEnvId() );
		buildProcessEntity.setBldLineNo		( request.getBldLineNo() );
		buildProcessEntity.setBldSrvId		( serverNo );
		buildProcessEntity.setTaskFlowId	( workflowNo );
		buildProcessEntity.setTargetSeqNo	( Integer.parseInt(sequenceNo));
		buildProcessEntity.setMsg			( e.getMessage() + SEP + ExceptionUtils.getStackTraceString( e ) );	// Exceptionのメッセージを拾う
		buildProcessEntity.setMsgId			( "" );
		buildProcessEntity.setUpdUserNm		( bean.getInsertUpdateUser() );
		buildProcessEntity.setStsId			( BmBpStatusIdForExecData.BuildPackageError.getStatusId());	// イレギュラーなので当然エラー

		buildProcessEntity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
		buildProcessEntity.setProcEndTimestamp	( TriDateUtils.getSystemTimestamp() );

		ITaskFlowProcDao dao = this.getTaskFlowProcDao() ;
		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId( bean.getProcId() );
		condition.setBldLineNo(request.getBldLineNo());
		condition.setTaskFlowId(workflowNo);
		condition.setTargetSeqNo(Integer.parseInt(sequenceNo));
		ITaskFlowProcEntity prvEntity = dao.find( condition.getCondition() ).get(0) ;
		if( null == prvEntity ) {
			dao.insert( buildProcessEntity );
		} else {
			dao.update( buildProcessEntity );
		}
	}

	public void writeReleaseProcessByIrregular(
			IBldTimelineEntity request,
			IReleaseTaskBean bean,
			IBldTimelineAgentEntity line,
			Exception e) {

		ITaskFlowProcEntity relProcessEntity = new TaskFlowProcEntity();

		String serverNo = ( null != line ) ? line.getBldSrvId() : "" ;		//lineがnullだったときのために
		String workflowNo = ( null != line ) ? line.getTaskFlowId() : "" ;	//同上
		String sequenceNo = ( null != line ) ? line.getTargetSeqNo().toString() : "" ;	//同上

		relProcessEntity.setProcId			( bean.getProcId() );
//		relProcessEntity.setLotId			( bean.getLotNo() );
		relProcessEntity.setBldLineNo		( request.getBldLineNo() );
		relProcessEntity.setBldSrvId		( serverNo );
		relProcessEntity.setTaskFlowId		( workflowNo );
		relProcessEntity.setTargetSeqNo		( Integer.parseInt(sequenceNo));
		relProcessEntity.setMsg				( e.getMessage() + SEP + ExceptionUtils.getStackTraceString( e ) );	// Exceptionのメッセージを拾う
		relProcessEntity.setMsgId			( "" );
		relProcessEntity.setUpdUserNm		( bean.getInsertUpdateUser() );
		relProcessEntity.setStsId			( RmRpStatusIdForExecData.ReleasePackageError.getStatusId() );	// イレギュラーなので当然エラー

		relProcessEntity.setProcStTimestamp	( TriDateUtils.getSystemTimestamp() );
		relProcessEntity.setProcEndTimestamp( TriDateUtils.getSystemTimestamp() );

		ITaskFlowProcDao dao = this.getTaskFlowProcDao() ;
		TaskFlowProcCondition condition = new TaskFlowProcCondition();
		condition.setProcId( bean.getProcId() );
		condition.setBldLineNo(request.getBldLineNo());
		condition.setTaskFlowId(workflowNo);
		condition.setTargetSeqNo(Integer.parseInt(sequenceNo));
		ITaskFlowProcEntity prvEntity = dao.find( condition.getCondition() ).get(0) ;
		if( null == prvEntity ) {
			dao.insert( relProcessEntity );
		} else {
			dao.update( relProcessEntity );
		}


	}

}
