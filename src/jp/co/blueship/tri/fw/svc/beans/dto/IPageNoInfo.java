package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;

/**
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IPageNoInfo extends Serializable {

	/**
	 * Set count of rows on one screen
	 *
	 * @param viewRows
	 * @return
	 */
	public IPageNoInfo setViewRows(Integer viewRows);

	/**
	 * Get count of rows on displaying screen
	 *
	 * @return
	 */
	public Integer getViewRows();

	/**
	 * Set count of all rows
	 *
	 * @param maxRows
	 * @return
	 */
	public IPageNoInfo setMaxRows(Integer maxRows);

	/**
	 * Get count of all rows
	 *
	 * @return
	 */
	public Integer getMaxRows();

	/**
	 * Set max pages
	 *
	 * @param maxPageNo
	 * @return
	 */
	public IPageNoInfo setMaxPageNo(Integer maxPageNo);

	/**
	 * Get max pages
	 *
	 * @return
	 */
	public Integer getMaxPageNo();

	/**
	 * Set selected page
	 *
	 * @param selectPageNo
	 * @return
	 */
	public IPageNoInfo setSelectPageNo(Integer selectPageNo);

	/**
	 * Get selected page
	 *
	 * @return
	 */
	public Integer getSelectPageNo();

	/**
	 *  Get start row in displaying screen
	 *
	 * @return
	 */
	public Integer getViewRangeFrom();

	/**
	 *  Get end row in displaying screen
	 *
	 * @return
	 */
	public Integer getViewRangeTo();

	/**
	 * Get if it need previous page or not
	 *
	 * @return
	 */
	public boolean isPrev();

	/**
	 * Get if it needs next page or not
	 *
	 * @return
	 */
	public boolean isNext();
}
