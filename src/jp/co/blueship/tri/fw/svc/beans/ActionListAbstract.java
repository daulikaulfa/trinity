package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * ビジネス処理を一括して実行する抽象クラスです。 <br>
 * このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 * @param <E>
 *
 */
public abstract class ActionListAbstract<E> implements IDomain<E> {

	private IContextAdapter context = null;

	private IDomain<E> gathering = null;

	private List<IDomain<E>> beforeAdvices = null;
	private List<IDomain<E>> actions = null;
	private List<IDomain<E>> throwAdvices = null;
	private List<IDomain<E>> afterAdvices = null;

	public void setGathering(IDomain<E> gathering) {
		this.gathering = gathering;
	}

	public final IDomain<E> getGathering() {
		return gathering;
	}

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 * @param context コンテキスト
	 */
	public final void setContext( IContextAdapter context ) {
		this.context = context;
	}

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	protected final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return context;
	}

	/**
	 * 処理前に必ず実行されるビジネス処理のリストを設定します。
	 *
	 * @param list 一括して実行するリスト
	 */
	public final void setBeforeAdvices(List<IDomain<E>> list) {
		beforeAdvices = list;
	}

	/**
	 * 処理前に必ず実行されるビジネス処理のリストを取得します。
	 *
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<E>> getBeforeAdvices() {
		if (null == beforeAdvices)
			beforeAdvices = new ArrayList<IDomain<E>>();

		return beforeAdvices;
	}

	/**
	 * 一括してビジネス処理を実行するリストを設定します。
	 *
	 * @param list 一括して実行するリスト
	 */
	public final void setActions(List<IDomain<E>> list) {
		actions = list;
	}

	/**
	 * 一括してビジネス処理を実行するリストを取得します。
	 *
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<E>> getActions() {
		if (null == actions)
			actions = new ArrayList<IDomain<E>>();

		return actions;
	}

	/**
	 * 例外発生時に実行されるビジネス処理のリストを設定します。
	 *
	 * @param list 一括して実行するリスト
	 */
	public final void setThrowAdvices(List<IDomain<E>> list) {
		throwAdvices = list;
	}

	/**
	 * 例外発生時に実行されるビジネス処理のリストを取得します。
	 *
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<E>> getThrowAdvices() {
		if (null == throwAdvices)
			throwAdvices = new ArrayList<IDomain<E>>();

		return throwAdvices;
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理のリストを設定します。
	 *
	 * @param list 一括して実行するリスト
	 */
	public final void setAfterAdvices(List<IDomain<E>> list) {
		afterAdvices = list;
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理のリストを取得します。
	 *
	 * @return 一括して実行するリスト
	 */
	public final List<IDomain<E>> getAfterAdvices() {
		if (null == afterAdvices)
			afterAdvices = new ArrayList<IDomain<E>>();

		return afterAdvices;
	}

	/**
	 * 処理前に必ず実行されるビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 */
	protected final void beforeAdvices(IServiceDto<E> serviceDto) {

		for (IDomain<E> action : this.getBeforeAdvices()) {
			action.execute(serviceDto);
		}

	}

	/**
	 * 一括してビジネス処理を実行するビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 */
	protected final void actions(IServiceDto<E> serviceDto) {
		try {
			for (IDomain<E> action : this.getActions()) {
				action.execute(serviceDto);
			}

		} catch (BaseBusinessException e) {

			throw e;
		}
	}

	/**
	 *  一括してビジネス処理を実行するビジネス処理です。 <br>
	 * このメソッドは、継続可能な業務例外であれば、ビジネス処理を中断せずに、 すべてのビジネス処理の実行後に例外を発生させます。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @param continuableBusinessException 継続可能な業務例外でビジネス処理を中断しない場合true。
	 */
	protected final void actions(IServiceDto<E> serviceDto, boolean continuableBusinessException) {

		if (!continuableBusinessException) {
			this.actions(serviceDto);
			return;
		}

		List<IMessageId> messageList = new LinkedList<IMessageId>();
		List<String[]> messageArgsList = new LinkedList<String[]>();

		for (IDomain<E> action : this.getActions()) {

			try {
				action.execute(serviceDto);

			} catch (BusinessException e) {
				messageList.add(e.getMessageID());
				messageArgsList.add(e.getMessageArgs());

				continue;
			} catch (ContinuableBusinessException e) {
				messageList.addAll(e.getMessageIdList());
				messageArgsList.addAll(e.getMessageArgsList());

				continue;
			}
		}

		if (TriCollectionUtils.isNotEmpty(messageList)) {
			throw new ContinuableBusinessException(messageList, messageArgsList);
		}
	}

	/**
	 * 処理終了後に必ず実行されるビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 */
	protected final void afterAdvices(IServiceDto<E> serviceDto) {

		for (IDomain<E> action : this.getAfterAdvices()) {
			action.execute(serviceDto);
		}
	}

	/**
	 * 例外発生時に実行するビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 */
	protected final void throwAdvices(IServiceDto<E> serviceDto) {

		for (IDomain<E> action : this.getThrowAdvices()) {
			action.execute(serviceDto);
		}
	}

}
