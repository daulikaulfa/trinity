package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * ビジネス処理を一括して実行するクラスです。
 * <br>このクラスは非同期で実行されます。
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ThreadActionList extends ActionListAbstract<IGeneralServiceBean> implements Runnable {

	private static final ILog log = TriLogFactory.getInstance();

	private IServiceDto<IGeneralServiceBean> serviceDto;

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		this.serviceDto = serviceDto;

		if ( null != this.getGathering()) {
			this.getGathering().execute( serviceDto );
		}

		boolean isSuccess = false;

		try {
			this.beforeAdvices( this.serviceDto );
			isSuccess = true;

		} catch (BaseBusinessException e) {
			LogHandler.error( log , e ) ;

			IGeneralServiceBean info = serviceDto.getServiceBean();
			if ( null != info ){
				info.setBusinessThrowable( e );
			}
			this.throwAdvices( serviceDto );

			throw e;
		} catch (Exception e) {
			LogHandler.fatal( log , e ) ;

			IGeneralServiceBean info = serviceDto.getServiceBean();
			if ( null != info ){
				info.setSystemThrowable( e );
			}
			this.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005054S , e );

		} finally {
			if ( ! isSuccess ) {
				try {
					this.afterAdvices( serviceDto );
				}  catch ( Exception e ) {
					this.throwAdvices( serviceDto );
					ExceptionUtils.reThrowIfTrinityException(e);
					throw new TriSystemException(SmMessageId.SM005055S , e );

				}
			}
		}

		new Thread( this ).start();
		return serviceDto;
	}

	public void run() {
		try {
			this.executeThread( this.serviceDto );

		} catch (Throwable e) {
			e.printStackTrace();

			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005054S , e );
		}
	}

	/**
	 * ビジネス処理を非同期で実行します。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 */
	private final void executeThread( IServiceDto<IGeneralServiceBean> serviceDto ) throws BaseBusinessException {

		try {
			this.actions( serviceDto );

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;

			IGeneralServiceBean info = serviceDto.getServiceBean();
			if ( null != info ){
				info.setBusinessThrowable( e );
			}
			this.throwAdvices( serviceDto );

			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;

			IGeneralServiceBean info = serviceDto.getServiceBean();
			if ( null != info ){
				info.setSystemThrowable( e );
			}
			this.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005054S , e );

		} finally {
			try {
				this.afterAdvices( serviceDto );
			}  catch ( Exception e ) {
				this.throwAdvices( serviceDto );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException(SmMessageId.SM005055S , e );

			}
		}
	}


}
