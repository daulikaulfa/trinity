package jp.co.blueship.tri.fw.svc.beans;

import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.DesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.CreateMailContentsException;
import jp.co.blueship.tri.fw.mail.beans.IMailCallback;
import jp.co.blueship.tri.fw.mail.beans.MailServiceSupport;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.mail.beans.dto.SystemErrorMailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * アクション実行時、システムエラーメール送信を行う汎用クラス。
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class MailFlowSystemErrorService extends MailServiceSupport {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public MailFlowSystemErrorService() {
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute(
			IServiceDto<IGeneralServiceBean> serviceDto ) {


		try {
			this.sendMail(
					this.getMailServiceBean( serviceDto ),
					new IMailCallback() {

						public Map<String, Object> getContents( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getContentsCallBack( bean, data );
						}

						public Map<String, Object> getSubject( MailServiceBean bean, Map<String, Object> data ) throws CreateMailContentsException {
							return getSubjectCallBack( bean, data );
						}
					});

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005065S, e );
		}

	}

	/**
	 * メール送信の件名を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getSubjectCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {
		return data;
	}

	/**
	 * メール送信の内容を取得します。
	 *
	 * @param bean メールサービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws CreateMailContentsException
	 */
	private final Map<String, Object> getContentsCallBack( MailServiceBean bean, Map<String, Object> data )  throws CreateMailContentsException {

		data.put("_sendFrom", bean.getFrom().getUserView());
		data.put("_actionName", sheet.getValue( UmDesignBeanId.actionId, bean.getFlowAction()) );
		data.put("_date", TriDateUtils.convertViewDateFormat(bean.getSystemDate(), TriDateUtils.getYMDDateFormat() ) );
		data.put("_time", TriDateUtils.convertViewDateFormat(bean.getSystemDate(), TriDateUtils.getHMSDateFormat() ) );

		Throwable th = bean.getSystemThrowable();
		data.put("_message", ( null != th)? th.getMessage() : "" );
		data.put("_class", ( null != th)? th.getClass().toString() : "" );
		data.put("_stack", ( null != th)? ExceptionUtils.getStackTrace( th ) : "" );

		return data;
	}

	/**
	 * メール送信先取得のサービスビーンのインスタンスを生成します。
	 *
	 * @param paramBean
	 * @return 生成したインスタンス
	 */
	private IServiceDto<MailServiceBean> getMailServiceBean( IServiceDto<IGeneralServiceBean> serviceDto ) {
		SystemErrorMailServiceBean destBean = new SystemErrorMailServiceBean();

		IServiceDto<MailServiceBean> mailServiceDto = new ServiceDto<MailServiceBean>();
		mailServiceDto.setServiceBean( destBean );

		TriPropertyUtils.copyProperties(destBean, serviceDto.getServiceBean());

		destBean.setVmResourceLoaderPath( DesignBusinessRuleUtils.getTemplatePath().getPath() );

		return mailServiceDto;
	}

}
