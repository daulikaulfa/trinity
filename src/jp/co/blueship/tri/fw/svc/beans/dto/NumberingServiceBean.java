package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;


public class NumberingServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	private List<IEntity> entityList = new ArrayList<IEntity>();

	/**
	 * 採番の対象エンティティを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public List<IEntity> getEntities() {
		return entityList;
	}
	/**
	 * 採番の対象エンティティを設定します。
	 *
	 * @param entities 採番の対象エンティティ
	 */
	public void setEntities(List<IEntity> entities) {
		this.entityList = entities;
	}

}
