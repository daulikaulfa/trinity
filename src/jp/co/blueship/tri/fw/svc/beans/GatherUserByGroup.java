package jp.co.blueship.tri.fw.svc.beans;

import static jp.co.blueship.tri.fw.cmn.utils.comparator.Comparators.*;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunctions;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 任意のグループから、ユーザの所属グループの全ユーザ情報を取得します。 <br>
 * columnに指定可能な項目 <br>
 * ・グループＩＤ <br>
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherUserByGroup implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	private String column = null;

	public GatherUserByGroup() {
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 *
	 * @param column 該当項目名
	 */
	public final void setColumn(String column) {
		this.column = column;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {

		try {
			MailServiceBean paramBean = serviceDto.getServiceBean();
			SendMailBean targetBean = paramBean.getTarget();
			List<IUserEntity> userList = userListFrom(targetBean);

			GrpCondition condition = new GrpCondition();
			condition.setGrpNm(this.column);
			List<IGrpEntity> groups = umFinderSupport.getGrpDao().find(condition.getCondition());
			TriCollectionUtils.forEach(groups, addUsersToResultList(userList));

			if (TriCollectionUtils.isNotEmpty(groups) && TriStringUtils.isEmpty(targetBean.getUserView())) {
				targetBean.setUserView(groups.get(0).getGrpNm());
			}

			// ユーザリストの重複を除外
			userList = this.delDuplicateUserEntity(userList);
			targetBean.setUsersView(userList);

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005063S, e);
		}

	}

	private TriClojure<IGrpEntity> addUsersToResultList(final List<IUserEntity> resultList) {

		return new TriClojure<IGrpEntity>() {

			@Override
			public boolean apply(IGrpEntity group) {
				List<IUserEntity> users = usersFrom(group.getGrpId());
				resultList.addAll(users);
				return true;
			}
		};
	}

	private List<IUserEntity> usersFrom(String groupId) {
		return umFinderSupport.findUserByGroup(groupId);
	}

	private List<IUserEntity> userListFrom(SendMailBean targetBean) {

		if (null != targetBean.getUsersView()) {
			return targetBean.getUsersView();
		}

		return new ArrayList<IUserEntity>();
	}

	private List<IUserEntity> delDuplicateUserEntity(List<IUserEntity> userList) {
		ComparableSet<IUserEntity> newSet = new ComparableSet<IUserEntity>(IS_EQUAL_USER_ID);
		TriCollectionUtils.collect(userList, TriFunctions.<IUserEntity> nopFunction(), newSet);

		return FluentList.from(newSet).asList();
	}

}
