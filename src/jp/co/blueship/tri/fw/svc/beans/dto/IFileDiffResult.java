package jp.co.blueship.tri.fw.svc.beans.dto;



/**
 * アプリケーション層で、比較元と比較先ファイルとの差分情報を格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IFileDiffResult extends IFileResult {

	

	/**
	 * このファイルが比較先フォルダに存在するかを判定します。
	 * @return 存在する場合はtrueを戻します。それ以外はfalseを戻します。
	 */
	public boolean isMasterExists();
	/**
	 * このファイルが比較先フォルダに存在するかを設定します。
	 * @param exists 比較先ファイルが存在する場合はtrue。それ以外はfalseを戻します。
	 */
	public void setMasterExists( boolean exists );

	/**
	 * このファイルと比較先ファイルとの差分を判定します。
	 * @return 差分がある場合はtrueを戻します。それ以外はfalseを戻します。
	 */
	public boolean isDiff();
	/**
	 * このファイルと比較先ファイルとの差分を設定します。
	 * @param exists 差分がある場合はtrue。それ以外はfalseを戻します。
	 */
	public void setDiff( boolean diff );

}
