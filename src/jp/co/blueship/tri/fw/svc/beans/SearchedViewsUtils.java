package jp.co.blueship.tri.fw.svc.beans;

import java.util.Arrays;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriAnnotationHelper;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriSelectedId;

/**
 * @version V4.00.00
 * @author Akahoshi
 */
public class SearchedViewsUtils {

	
	/**
	 * 前ページで選択されていたIdと前ページのViewsから
	 * cacheIdSetを更新し、それを返します<br>
	 * なお、viewのIDを取得する関数にアノテーション(@TriSelectedId)
	 * を指定する必要があります
	 * 
	 * @param cacheIdSet
	 * @param selecedIds
	 * @param views
	 * @return cacheIdSet
	 */
	public static Set<String> getCaheIdSet(Set<String> cacheIdSet , String[] selectedIds , List<?> views){
		
		for(Object view : views){
			cacheIdSet.remove(TriAnnotationHelper.valueOf(view, TriSelectedId.class));
		}
		
		cacheIdSet.addAll( Arrays.asList(selectedIds) );
		
		return cacheIdSet;
	}
}
