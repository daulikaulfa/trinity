package jp.co.blueship.tri.fw.svc.beans;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.CallbackExpansion;
import jp.co.blueship.tri.fw.cmn.io.FileRecursive;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.FileDiffResult;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * ２つのファイルパスのファイル差分チェックを行います。
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class FilesDiffer implements IFilesDiffer {

	private DiffDo callBack = null;
	private List<IFileDiffResult> fileDiffResult = null;


	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.business.library.addon.prediffer.IFilesDiffer#execute(java.io.File, java.io.File)
	 */
	public final IFileDiffResult[] execute( File src, File dest )
			throws BaseBusinessException {

		this.fileDiffResult = new ArrayList<IFileDiffResult>();
		this.callBack = new DiffDo();

		FileRecursive fr = new FileRecursive( src, dest, this.callBack, BusinessFileUtils.getFileFilter() );
		fr.doRecursive();

		return (IFileDiffResult[])this.fileDiffResult.toArray( new IFileDiffResult[0] );
	}

	private class DiffDo implements CallbackExpansion {

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doFileFile(java.io.File, java.io.File, java.lang.String)
		 */
		public final void doFileFile(File src, File dst, String relatePath) {
			try{
				if ( TriFileUtils.isSCM( src ) )
					return;

				IFileDiffResult result = new FileDiffResult();
				result.setFilePath(relatePath);
				result.setDiff( ! TriFileUtils.isSame(src, dst) );
				result.setMasterExists( true );

				fileDiffResult.add( result );
			} catch( IOException e ) {
				throw new TriSystemException( SmMessageId.SM005053S , e );
			}
		}

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doFileNothing(java.io.File, java.io.File, java.lang.String)
		 */
		public final void doFileNothing( File src, File dst, String relatePath ) {

			if ( TriFileUtils.isSCM( src ) )
				return;

			IFileDiffResult result = new FileDiffResult();
			result.setFilePath(relatePath);
			result.setDiff( true );
			result.setMasterExists( false );

			fileDiffResult.add( result );
		}

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doDirectoryDirectory(java.io.File, java.io.File, java.lang.String)
		 */
		public final boolean doDirectoryDirectory(File src, File dst, String relatePath) {
			return true;
		}

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doDirectoryFile(java.io.File, java.io.File, java.lang.String)
		 */
		public final boolean doDirectoryFile(File src, File dst, String relatePath) {
			return false;
		}

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doDirectoryNothing(java.io.File, java.io.File, java.lang.String)
		 */
		public final boolean doDirectoryNothing(File src, File dst, String relatePath) {
			return true;
		}

		/* (non-Javadoc)
		 * @see jp.co.blueship.trinity.common.CallbackExpansion#doFileDirectory(java.io.File, java.io.File, java.lang.String)
		 */
		public final void doFileDirectory(File src, File dst, String relatePath) {

		}

	}
}
