package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.bm.dao.bldenv.eb.IBldEnvEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.cal.eb.ICalEntity;

/**
 * エンティティからセレクトボックスなどに使用するItemLabelsBeanへ変換するユーティリティークラスです。
 *
 * @author kanda
 *
 */
public class ItemLabelsUtils {

	/**
	 * IGroupEntityからlabelが"groupName"、valueが"groupId"となる
	 * ItemLabelsBeanのリストを返します。
	 *
	 * @param array
	 * @return
	 */
	public static List<ItemLabelsBean> conv( IGrpEntity[] array ) {

		List<ItemLabelsBean> newList = new ArrayList<ItemLabelsBean>() ;

		if( array == null ){
			return newList;
		}

		for( IGrpEntity entity : array ) {
			newList.add(new ItemLabelsBean(entity.getGrpNm(), entity.getGrpId()));
		}

		return newList ;
	}

	/**
	 * IGroupEntityからlabelが"groupName"、valueが"groupId"となる
	 * ItemLabelsBeanのリストを返します。
	 *
	 * @param array
	 * @return
	 */
	public static List<ItemLabelsBean> conv(IUmFinderSupport umFinderSuppot ,List<IGrpUserLnkEntity> array ) {

		List<ItemLabelsBean> newList = new ArrayList<ItemLabelsBean>() ;

		if( array == null ){
			return newList;
		}
		for( IGrpUserLnkEntity entity : array ) {
			IGrpEntity grpEntity = umFinderSuppot.findGroupById( entity.getGrpId() );
			newList.add(new ItemLabelsBean( grpEntity.getGrpNm(), entity.getGrpId() ) );
		}

		return newList ;
	}

	/**
	 * ILotEntityからlabelが"lotName(lotId)"、valueが"lotNo,serverId"となる
	 * ItemLabelsBeanのリストを返します。
	 * 実際の成形はmayaaにて実装されていることに注意してください。
	 *
	 * @param array
	 * @return
	 */
	public static List<ItemLabelsBean> conv( List<ILotEntity> array, String srvId ) {

		List<ItemLabelsBean> newList = new ArrayList<ItemLabelsBean>() ;

		if( array == null ){
			return newList;
		}
		for( ILotEntity entity : array ) {
			newList.add(new ItemLabelsBean(entity.getLotNm(), entity
					.getLotId(), srvId ));
		}

		return newList ;
	}

	/**
	 * IRelEnvEntityからlabelが"envName"、valueが"envNo"となる
	 * ItemLabelsBeanのリストを返します。
	 *
	 * @param array
	 * @return
	 */
	public static List<ItemLabelsBean> conv( IBldEnvEntity[] array ) {

		List<ItemLabelsBean> newList = new ArrayList<ItemLabelsBean>() ;

		if( array == null ){
			return newList;
		}

		for( IBldEnvEntity entity : array ) {
			newList.add(new ItemLabelsBean(entity.getBldEnvNm(), entity
					.getBldEnvId()));
		}

		return newList ;
	}

	/**
	 * IRelCalendarEntityからlabelが"relDate"、valueが"スラッシュなしのrelDate"となる
	 * ItemLabelsBeanのリストを返します。
	 *
	 * relDateは"YYYY/MM/DD"の書式となっていることを期待します。
	 * relDateが"YYYY/MM/DD"となっていない場合、あるいは変換に失敗した場合は
	 * そのレコードは無視され、リストには含まれません。
	 *
	 * @param array
	 * @return
	 */
	public static List<ItemLabelsBean> conv( ICalEntity[] array ) {

		List<ItemLabelsBean> newList = new ArrayList<ItemLabelsBean>() ;

		if( array == null ){
			return newList;
		}

		for( ICalEntity entity : array ) {

			String relDate = entity.getPreferredRelDate();

			if( false == TriDateUtils.checkYMD(relDate) ) {
				continue;
			}

			newList.add(new ItemLabelsBean(relDate, relDate ));
		}

		return newList ;
	}

}
