package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * <p>
 * 動作テストを行うため、プログラム内で擬似的に業務エクセプションを発生させます。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ActionThrowBusinessExceptionService  implements IDomain<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {

		try{
			throw new BusinessException(SmMessageId.SM005049S);

		} finally {
		}
	}

}
