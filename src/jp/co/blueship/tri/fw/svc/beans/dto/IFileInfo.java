package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;


/**
 * アプリケーション層で、ファイル情報及びファイルステータスを格納するインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface IFileInfo extends Serializable {

	/**
	 * ファイルステータスを取得します。
	 * @return ファイルステータスを戻します。
	 */
	public String getFileStatus();
	/**
	 * このファイルが比較先フォルダに存在するかを設定します。
	 * @param exists 比較先ファイルが存在する場合はtrue。それ以外はfalseを戻します。
	 */
	public void setMasterExists( boolean exists );
	/**
	 * このファイルと比較先ファイルとの差分を設定します。
	 * @param exists 差分がある場合はtrue。それ以外はfalseを戻します。
	 */
	public void setDiff( boolean diff );


}
