package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;


/**
 * トランザクション制御を行うビジネス処理を一括して処理する汎用クラスです。
 * <br>この実装クラスは、必ずsingleton = falseで実行します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionTransactionService extends ActionPojoAbstract<IGeneralServiceBean> {
	private String nextTransactionService = null;

	/**
	 * 一度のリクエスト処理で、トランザクション制御を行う場合、指定されます。
	 *
	 * @param transactionService 次のトランザクション制御処理があれば、指定する
	 */
	public void setNextTargetService( String transactionService ) {
		this.nextTransactionService = transactionService;
	}

	/**
	 * このメソッド内は、SpringAOPにより暗黙的なトランザクション制御が行われます。
	 *
	 * @param paramList
	 * @throws TriSystemException
	 */
	@SuppressWarnings("unchecked")
	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		if ( null != this.nextTransactionService ) {
			Object obj = this.getContext().getBean( nextTransactionService );
			((IDomain<IGeneralServiceBean>)obj).execute( serviceDto );
		}

		return serviceDto;
	}

}
