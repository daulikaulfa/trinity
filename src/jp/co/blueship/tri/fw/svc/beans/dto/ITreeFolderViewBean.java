package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.List;

import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean.CheckedState;

/**
 * How to convert Json for Pojo.
 * ex.)
 * Gson gson = new GsonBuilder().excludeFieldsWithoutExposeAnnotation().create();
 * String json = gson.toJson(bean);
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public interface ITreeFolderViewBean {
	public ITreeFolderViewBean getFolder( String path );
	public boolean isOwner();
	public ITreeFolderViewBean setCount( int count );
	public boolean isOpenFolder();
	public ITreeFolderViewBean getOwner();
	public ITreeFolderViewBean getParent();
	public String getName();
	public ITreeFolderViewBean setName(String name);
	public String[] getAllPath();
	public String getPath();
	public int getCount();
	public CheckedState getCheckedState();
	public ITreeFolderViewBean setCheckedState( CheckedState state );
	public List<ITreeFolderViewBean> getFolders();
	public ITreeFolderViewBean addFolders(List<ITreeFolderViewBean> folders);
	public ITreeFolderViewBean addFolder(ITreeFolderViewBean folder);
	public ITreeFolderViewBean setFolders(List<ITreeFolderViewBean> folders);

}
