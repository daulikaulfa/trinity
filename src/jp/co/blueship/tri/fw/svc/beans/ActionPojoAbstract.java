package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;



/**
 * ビジネス処理を実行するインタフェースの抽象クラスです。
 * @param <E>
 *
 */
public abstract class ActionPojoAbstract<E> implements IDomain<E> {

	private static final ILog log = TriLogFactory.getInstance();

	private IContextAdapter context = null;

	/**
	 * ビーンを生成するコンテキストが設定されます。
	 * @param context コンテキスト
	 */
	public final void setContext( IContextAdapter context ) {
		this.context = context;
	}

	/**
	 * ビーンを生成するコンテキストを取得します。
	 * @return 取得したインスタンスを戻します。
	 */
	protected final IContextAdapter getContext() {
		if ( null == this.context )
			this.context = ContextAdapterFactory.getContextAdapter();

		return context;
	}

	/**
	 * ビーン名に対応するオブジェクトのインスタンスを取得します。
	 * @param beanName 取得するビーン名
	 * @return 取得したインスタンスを戻します。
	 */
	protected final Object getBean( String beanName ) {
		if ( null == this.context ){
			throw new TriSystemException(SmMessageId.SM005048S);
		}
		return context.getBean(beanName);
	}

	protected final void outputBLStartLog(String className){
		LogHandler.info( log , TriLogMessage.LSM0030 , String.valueOf(Thread.currentThread().getId()) , className );
	}

	protected final void outputBLEndLog(String className){
		LogHandler.info( log , TriLogMessage.LSM0031 , String.valueOf(Thread.currentThread().getId()) , className );
	}
}
