package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;

/**
 * アプリケーション層で、ファイルパスを格納するインタフェースです。
 *
 */
public interface IFileResult extends Serializable {

	/**
	 * ファイルパスを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getFilePath();
	/**
	 * ファイルパスを設定します。
	 * @param path 資産ファイルパス
	 */
	public void setFilePath( String path );

}
