package jp.co.blueship.tri.fw.svc.beans.dto;

public class LockParamInfo implements ILockParamInfo {

	/**
	 * 修正したらインクリメントすること
	 */
	private static final long serialVersionUID = 2L;

	

	private String actionId = null;
	private String userId = null;
	private String lotId = null;
	private String procId = null;


	public final String getActionId() {
		return actionId;
	}
	public final void setActionId(String id) {
		this.actionId = id;
	}
	public final String getUserId() {
		return userId;
	}
	public final void setUserId(String id) {
		this.userId = id;
	}

	public final String getLotNo() {
		if ( null == this.lotId ) {
			this.lotId = "NONE";
		}
		return this.lotId;
	}
	public final void setLotNo( String lotId ) {
		this.lotId = lotId;
	}

	public final String getProcId() {
		return this.procId;
	}
	public final void setProcId( String procId ) {
		this.procId = procId;
	}
}
