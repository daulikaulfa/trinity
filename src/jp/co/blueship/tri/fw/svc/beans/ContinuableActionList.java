package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ビジネス処理を一括して実行するクラスです。
 * <br>継続可能な業務例外であれば、ビジネス処理を中断せずに、 すべてのビジネス処理の実行後に例外を発生させます。
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ContinuableActionList extends ActionListAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {

		try {

			if ( null != this.getGathering()) {
				this.getGathering().execute( serviceDto );
			}

			this.beforeAdvices( serviceDto );
			this.actions( serviceDto, true );

		} catch ( BaseBusinessException e ) {
			this.throwAdvices( serviceDto );
			throw e;

		} catch ( Exception e ) {
			this.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		} finally {
			try {
				this.afterAdvices( serviceDto );
			}  catch ( Exception e ) {
				this.throwAdvices( serviceDto );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( SmMessageId.SM005052S , e );

			}
		}

		return serviceDto;
	}

}
