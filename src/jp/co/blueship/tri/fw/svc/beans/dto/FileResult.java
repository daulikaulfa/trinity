package jp.co.blueship.tri.fw.svc.beans.dto;

/**
 * アプリケーション層で、ファイルパスを格納するクラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class FileResult implements	IFileResult {

	private static final long serialVersionUID = 1L;

	

	private String filePath = null;

	public final String getFilePath() {
		return filePath;
	}
	public final void setFilePath(String filePath) {
		this.filePath = filePath;
	}

}
