package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;


/**
 * アプリケーション層で排他に使用するパラメタインタフェースです。
 *
 * @author Yukihiro Eguchi
 *
 */
public interface ILockParamInfo extends Serializable {

	/**
	 * アクションＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getActionId();
	/**
	 * アクションＩＤを設定します。
	 * @param id アクションＩＤ
	 */
	public void setActionId( String id );

	/**
	 * ユーザＩＤを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getUserId();
	/**
	 * ユーザＩＤを設定します。
	 * @param id ユーザＩＤ
	 */
	public void setUserId( String id );

	/**
	 * ロット番号を取得します。
	 * @return 取得した値を戻します。
	 */
	public String getLotNo();
	/**
	 * ロット番号を設定します。
	 * @param lotId ロット番号
	 */
	public void setLotNo( String lotId );

	/**
	 * ProcIdを取得します。
	 * @return 取得した値を戻します。
	 */
	public String getProcId();
	/**
	 * ProcIdを設定します。
	 * @param procId ProcID
	 */
	public void setProcId( String procId );
}
