package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ビジネス処理を一括して実行するクラスです。
 * <br>synchronizedによって、同期されます。
 * <br>
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class SynchronizedActionList extends ActionListAbstract<IGeneralServiceBean> {

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		executeSynchronized( serviceDto, this );
		return serviceDto;
	}

	/**
	 * このメソッドは、synchronizedによって、同期されます。
	 * @param list コンバート間に流通させるビーン
	 * @param actions この実行アクション
	 * @throws BaseBusinessException
	 */
	private static final synchronized void executeSynchronized(
												IServiceDto<IGeneralServiceBean> serviceDto,
												SynchronizedActionList actions )
			throws BaseBusinessException {

		try {

			actions.beforeAdvices( serviceDto );
			actions.actions( serviceDto );

		} catch ( BaseBusinessException e ) {
			actions.throwAdvices( serviceDto );
			throw e;

		} catch ( Exception e ) {
			actions.throwAdvices( serviceDto );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		} finally {
			try {
				actions.afterAdvices( serviceDto );

			}  catch ( Exception e ) {
				actions.throwAdvices( serviceDto );
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new TriSystemException( SmMessageId.SM005052S , e );

			}
		}
	}

}
