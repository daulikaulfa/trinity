package jp.co.blueship.tri.fw.svc.beans.dto;

import jp.co.blueship.tri.fw.constants.FileStatus;

/**
 * アプリケーション層で、資産と原本作業ファイルとの差分情報を格納するクラスです。
 *
 * @version V3.10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class FileDiffResult extends FileResult implements IFileDiffResult, IFileInfo {

	private static final long serialVersionUID = 1L;

	private boolean exists = false;
	private boolean diff = false;

	public final boolean isMasterExists() {
		return exists;
	}
	public final void setMasterExists(boolean exists) {
		this.exists = exists;
	}

	public final boolean isDiff() {
		return diff;
	}
	public final void setDiff(boolean diff) {
		this.diff = diff;
	}

	public final String getFileStatusId() {
		return this.getFileStatusEnum().getStatusId();
	}
	@SuppressWarnings("deprecation")
	public final String getFileStatus() {
		return this.getFileStatusEnum().getStatus();
	}

	private final FileStatus getFileStatusEnum() {
		if ( ! isMasterExists() )
			return FileStatus.Add;

		if ( isDiff() )
			return FileStatus.Edit;

		return FileStatus.Same;
	}

}
