package jp.co.blueship.tri.fw.svc.beans;

import java.io.File;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.svc.beans.dto.IFileDiffResult;

/**
 * ２つのファイルパスのファイル差分チェックを行うインタフェースです。
 * 
 * @author Yukihiro Eguchi
 *
 */
public interface IFilesDiffer {
	
	

	/**
	 * 指定された２つのファイルパスを元に差分チェックを行い、差分結果を取得します。
	 * <br>比較元を基準とした情報を戻します。
	 * 
	 * @param src 比較元のディレクトリパス
	 * @param dest 比較先のディレクトリパス
	 * @return 差分ファイル情報を戻します。
	 * @throws BaseBusinessException
	 */
	public IFileDiffResult[] execute( File src, File dest ) throws BaseBusinessException;

}
