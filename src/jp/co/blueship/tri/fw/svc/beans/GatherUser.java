package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;

/**
 * 任意のユーザを取得します。
 * <br>columnに指定可能な項目
 * <br>・ユーザＩＤ
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class GatherUser implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao = null;
	private String column = null;

	public GatherUser() {
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	/**
	 * インスタンス生成時に自動的に設定されます。
	 * @param column 該当項目名
	 */
	public final void setColumn( String column ) {
		this.column = column;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {

		MailServiceBean paramBean = null;
		SendMailBean targetBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			targetBean = paramBean.getTarget();
			targetBean.setUsersView( new ArrayList<IUserEntity>() );


			UserCondition condition = new UserCondition();
			condition.setUserId(this.column);
			IUserEntity user = userDao.findByPrimaryKey(condition.getCondition());
			if ( user != null )
				targetBean.setUserView(user.getUserNm());
			targetBean.getUsersView().addAll( FluentList.from(new IUserEntity[]{ user }).asList());

		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005062S, e );
		}

		return serviceDto;
	}

}
