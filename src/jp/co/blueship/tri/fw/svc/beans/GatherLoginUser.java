package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;

import jp.co.blueship.tri.fw.dao.orm.IJdbcCondition;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;

/**
 * ログイン中のユーザを取得します。 <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherLoginUser implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao = null;

	public GatherLoginUser() {
	}

	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {

		MailServiceBean paramBean = null;
		SendMailBean targetBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			targetBean = paramBean.getTarget();
			targetBean.setUsersView(new ArrayList<IUserEntity>());

			IUserEntity user = userDao.findByPrimaryKey(conditionFrom(paramBean.getUserId()).getCondition());
			targetBean.setUserView(user.getUserNm());
			targetBean.getUsersView().add(user);

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005060S, e);
		}

		return serviceDto;
	}

	private IJdbcCondition conditionFrom(String userId) {

		UserCondition condition = new UserCondition();
		condition.setUserId(userId);

		return condition;
	}

}
