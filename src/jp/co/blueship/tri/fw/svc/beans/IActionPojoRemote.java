package jp.co.blueship.tri.fw.svc.beans;

import java.rmi.RemoteException;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * RMI呼び出しのためのインタフェースです。
 *
 *
 */
public interface IActionPojoRemote<E> extends java.rmi.Remote {

	/**
	 * ドメイン層での処理を実行します。
	 *
	 * @param serviceDto Service呼び出し時のパラメータ
	 * @return 戻り値を返します。
	 */
	public IServiceDto<E> execute( IServiceDto<E> serviceDto ) throws TriSystemException, RemoteException;

}
