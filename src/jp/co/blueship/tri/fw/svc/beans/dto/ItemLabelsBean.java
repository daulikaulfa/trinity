package jp.co.blueship.tri.fw.svc.beans.dto;

import java.io.Serializable;

/**
 * 汎用 labelとvalueをセットで管理するBean
 * セレクトボックスなどに使用する。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ItemLabelsBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String label = null ;
	private String value = null ;
	private String subValue = null ;

	public ItemLabelsBean() {

	}

	public ItemLabelsBean( String label , String value ) {
		this.label = label ;
		this.value = value ;
	}

	public ItemLabelsBean( String label , String value, String subValue ) {
		this.label = label ;
		this.value = value ;
		this.subValue = subValue ;
	}

	public String getLabel() {
		return label;
	}
	public ItemLabelsBean setLabel(String label) {
		this.label = label;
		return this;
	}

	public String getValue() {
		return value;
	}
	public ItemLabelsBean setValue(String value) {
		this.value = value;
		return this;
	}

	public String getSubValue() {
		return subValue;
	}
	public ItemLabelsBean setSubValue( String subValue ) {
		this.subValue = subValue;
		return this;
	}
}
