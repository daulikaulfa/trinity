package jp.co.blueship.tri.fw.svc.beans.dto;

import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;

/**
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class PageNoInfo implements IPageNoInfo {

	private static final long serialVersionUID = 1L;

	private Integer viewRows = 0;
	private Integer maxRows = 0;
	private Integer maxPageNo = 0;
	private Integer selectPageNo = 0;

	public Integer getViewRows() {
		if ( selectPageNo.equals(maxPageNo) ) {
			if ( 0 == maxRows.intValue() )
				return maxRows;
			if ( 0 != maxRows.intValue() && 0 != viewRows.intValue() ) {
				if ( 1 < selectPageNo ) {
					if ( maxRows % viewRows != 0 ) {
						return maxRows % viewRows;
					}
				} else {
					return maxRows;
				}
			}
		}

		return viewRows;
	}
	public PageNoInfo setViewRows(Integer viewRows) {
		this.viewRows = TriObjectUtils.defaultIfNull(viewRows, 0);
		return this;
	}
	public Integer getMaxRows() {
		return maxRows;
	}
	public PageNoInfo setMaxRows(Integer maxRows) {
		this.maxRows = TriObjectUtils.defaultIfNull(maxRows, 0);
		return this;
	}
	public Integer getMaxPageNo() {
		return this.maxPageNo;
	}
	public PageNoInfo setMaxPageNo(Integer maxPageNo) {
		this.maxPageNo = TriObjectUtils.defaultIfNull(maxPageNo, 0);
		return this;
	}
	public Integer getSelectPageNo() {
		return this.selectPageNo;
	}
	public PageNoInfo setSelectPageNo(Integer selectPageNo) {
		this.selectPageNo = TriObjectUtils.defaultIfNull(selectPageNo, 0);
		return this;
	}
	public Integer getViewRangeFrom() {
		if ( 0 == maxRows.intValue() ){
			return 0;
		}
		Integer viewRangeFrom = (selectPageNo - 1) * viewRows + 1;
		return viewRangeFrom;
	}

	public Integer getViewRangeTo() {
		if ( 0 == maxRows.intValue() ){
			return 0;
		}
		Integer viewRangeTo = (getViewRangeFrom() + getViewRows() ) - 1;
		return viewRangeTo;
	}

	public boolean isNext() {
		if( selectPageNo.equals(this.maxPageNo) ){
			return false;
		}
		return true;
	}

	public boolean isPrev() {
		if( 0 == selectPageNo.intValue() || 1 == selectPageNo.intValue() ){
			return false;
		}
		return true;
	}

}
