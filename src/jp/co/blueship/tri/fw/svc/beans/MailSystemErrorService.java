package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * システムエラーのみメール送信を行う汎用の上位クラスです。 <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class MailSystemErrorService implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	public static final String FLOW_SYSTEM_ERROR_ID = "FlowSystemErrorService";

	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto) {

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if (null == paramBean.getSystemThrowable()) {
				return serviceDto;
			}

			if (null != paramBean.getSystemThrowable()) {
				if (!DesignSheetUtils.isMail(UmDesignBeanId.mail, FLOW_SYSTEM_ERROR_ID))
					return serviceDto;
			}

			return mailSystemErrorService().execute(serviceDto);

		} catch (Exception e) {
			// メール送信が失敗しても処理を続行する
			LogHandler.fatal(log, e);
		}

		return serviceDto;

	}

	@SuppressWarnings("unchecked")
	private IDomain<IGeneralServiceBean> mailSystemErrorService() {

		return (IDomain<IGeneralServiceBean>) ContextAdapterFactory.getContextAdapter().getBean("mailSystemErrorService");
	}

}
