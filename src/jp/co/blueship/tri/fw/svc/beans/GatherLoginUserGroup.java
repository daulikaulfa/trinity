package jp.co.blueship.tri.fw.svc.beans;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;
import static jp.co.blueship.tri.fw.cmn.utils.comparator.Comparators.*;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * ログイン中のユーザが所属しているグループの全ユーザを取得します。 <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherLoginUserGroup implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;

	public GatherLoginUserGroup() {
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {


		try {
			MailServiceBean paramBean = serviceDto.getServiceBean();

			ComparableSet<IUserEntity> userSet = new ComparableSet<IUserEntity>(IS_EQUAL_USER_ID);
			forEach(groupsFrom(paramBean.getUserId()), retrieveUsers(userSet));

			SendMailBean targetBean = paramBean.getTarget();
			targetBean.setUserView(orgUserFrom(userSet, paramBean.getUserId()).getUserNm());
			targetBean.setUsersView(FluentList.from(userSet).asList());

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005061S, e);
		}

	}

	private List<IUserEntity> usersFrom(String groupId) {
		return umFinderSupport.findUserByGroup(groupId);
	}

	private List<IGrpEntity> groupsFrom(String userId) {
		return umFinderSupport.findGroupByUserId(userId);
	}

	private TriClojure<IGrpEntity> retrieveUsers(final ComparableSet<IUserEntity> userSet) {

		return new TriClojure<IGrpEntity>() {

			@Override
			public boolean apply(IGrpEntity group) {
				List<IUserEntity> groupUsers = usersFrom(group.getGrpId());
				userSet.addAll(groupUsers);
				return true;
			}
		};
	}

	private IUserEntity orgUserFrom(Set<IUserEntity> userEntitiesSet, final String targetUserId) {

		Collection<IUserEntity> origUsers = TriCollectionUtils.select(userEntitiesSet, new TriPredicate<IUserEntity>() {

			@Override
			public boolean evalute(IUserEntity entity) {
				return entity.getUserId().equals(targetUserId);
			}
		});

		PreConditions.assertOf(TriCollectionUtils.isNotEmpty(origUsers), "Orig user is not found.");
		PreConditions.assertOf(origUsers.size() == 1, "Orig user is more than one.");

		return origUsers.iterator().next();
	}

}
