package jp.co.blueship.tri.fw.svc.beans;

import java.util.HashMap;
import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Run collectively the business processing.
 * Be sure to run in singleton = false.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public abstract class ActionMapAbstract<E> implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private Map<String, E> actions = null;

	/**
	 * 一括してビジネス処理を実行するマップを設定します。
	 *
	 * @param map 一括して実行するマップ
	 */
	public final void setActions(Map<String, E> map) {
		actions = map;
	}

	/**
	 * 一括してビジネス処理を実行するマップを取得します。
	 *
	 * @return 一括して実行するマップ
	 */
	public final Map<String, E> getActions() {
		if (null == actions)
			actions = new HashMap<String, E>();

		return actions;
	}

	@Override
	public final IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		try {
			for ( String actionKey : this.getActions().keySet() ) {
				for ( String key: this.getKeys(actionKey) ) {
					this.actions( serviceDto, key );
				}
			}

		} catch ( BaseBusinessException e ) {
			LogHandler.error( log , e ) ;

			throw e;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e ) ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005051S , e );

		}

		return serviceDto;

	}

	private String[] getKeys( String key ) {
		if (TriStringUtils.isEmpty(key))
			return new String[0];

		return TriStringUtils.split(key, ",", false, true);
	}

	/**
	 * 一括してビジネス処理を実行するビジネス処理です。
	 *
	 * @param serviceDto コンバート間に流通させるビーン
	 * @param key 対象キー
	 */
	protected abstract void actions(IServiceDto<IGeneralServiceBean> serviceDto, String key);

}
