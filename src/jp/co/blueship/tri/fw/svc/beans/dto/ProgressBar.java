package jp.co.blueship.tri.fw.svc.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
public class ProgressBar implements IProgressBar {

	private int value;
	private int minimum;
	private int maximum;


	public IProgressBar setValue(int value){
		this.value = value;
		return this;
	}
	public int getValue(){
		return value;
	}

	public IProgressBar setMinimum(int minimum) {
		this.minimum = minimum;
		return this;
	}
	public int getMinimum() {
		return minimum;
	}

	public IProgressBar setMaximum(int maximum) {
		this.maximum = maximum;
		return this;
	}
	public int getMaximum() {
		return maximum;
	}

	public IProgressBar setPercentComplete(double maximum) {
		return this;
	}
	public double getPercentComplete() {
		if(maximum <= 0 ) return 0;

		double percentComplete = (double)minimum / (double)maximum;
		if(percentComplete > 1) percentComplete = 1;

		return percentComplete;
	}

	public String getString() {
		return (int)(this.getPercentComplete() * 100) + "%";
	}


}
