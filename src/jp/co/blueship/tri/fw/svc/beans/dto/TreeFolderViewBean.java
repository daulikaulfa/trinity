package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class TreeFolderViewBean implements ITreeFolderViewBean {
	private TreeFolderViewBean owner;
	private TreeFolderViewBean parent;
	@Expose	private String name;
	@Expose	private String path;
	@Expose private int count;
	@Expose private String checkedState = CheckedState.UnChecked.value();
	@Expose private boolean openFolder = false;;
	@Expose private List<ITreeFolderViewBean> folders = new ArrayList<ITreeFolderViewBean>();
	protected Map<String, TreeFolderViewBean> pathMap;

	protected Map<String, TreeFolderViewBean> getMap() {
		if ( null == ((TreeFolderViewBean)getOwner()).pathMap ) {
			((TreeFolderViewBean)getOwner()).pathMap = new HashMap<String, TreeFolderViewBean>();
		}

		return ((TreeFolderViewBean)getOwner()).pathMap;
	}

	@Override
	public ITreeFolderViewBean getFolder( String path ) {
		String innerPath = (StringUtils.startsWith( path, "/" ))? path: "/" + path;
		if ( ! this.getOwner().getPath().equals(innerPath) )
			innerPath = TriStringUtils.trimTailSeparator( innerPath );

		if ( null == path || ! getMap().containsKey(innerPath) )
			return null;

		return getMap().get(innerPath);
	}

	@Override
	public boolean isOwner() {
		if (null == this.getOwner() )
			return false;

		return this.getOwner().equals(this);
	}

	@Override
	public ITreeFolderViewBean getOwner() {
		return owner;
	}
	public TreeFolderViewBean setOwner() {
		this.owner = this;
		this.openFolder = true;
		return this;
	}
	public TreeFolderViewBean setOwner(TreeFolderViewBean owner) {
		this.owner = owner;
		return this;
	}

	@Override
	public ITreeFolderViewBean getParent() {
		return parent;
	}
	public void setParent(TreeFolderViewBean parent) {
		this.parent = parent;
	}

	@Override
	public String getName() {
		return name;
	}
	@Override
	public TreeFolderViewBean setName(String name) {
		this.name = name;
		if ( this.isOwner() ) {
			this.setPath();
		}

		return this;
	}

	@Override
	public String[] getAllPath() {
		return getMap().keySet().toArray( new String[0] );
	}

	@Override
	public String getPath() {
		return path;
	}
	private TreeFolderViewBean setPath() {
		if ( null != owner && TriStringUtils.isNotEmpty(this.path) ) {
			if ( getMap().containsKey(this.path) ) {
				getMap().remove(this.path);
			}
		}
		this.path = (null == this.getParent() || "/".equals(this.getParent().getPath()) )?
				"/" + name: this.getParent().getPath() + "/" + name;
		getMap().put(this.path, this);
		return this;
	}
	@Override
	public int getCount() {
		return count;
	}
	@Override
	public ITreeFolderViewBean setCount(int count) {
		this.count = count;
		return this;
	}

	@Override
	public CheckedState getCheckedState() {
		return CheckedState.value( this.checkedState );
	}
	@Override
	public TreeFolderViewBean setCheckedState(CheckedState checkedState) {
		this.checkedState = checkedState.value();

		this.setCheckedState(checkedState, true);
		this.setCheckedState(checkedState, false);

		return this;
	}

	private TreeFolderViewBean setCheckedState(CheckedState checkedState, boolean goingDown) {
		this.checkedState = checkedState.value();

		if ( CheckedState.Checked.equals(checkedState) ) {
			if ( goingDown ) {
				for ( ITreeFolderViewBean folder: this.getFolders() ) {
					((TreeFolderViewBean)folder).setCheckedState(checkedState, true);
				}
			}
		} else if ( CheckedState.UnChecked.equals(checkedState) ) {
			if ( goingDown ) {
				for ( ITreeFolderViewBean folder: this.getFolders() ) {
					((TreeFolderViewBean)folder).setCheckedState(checkedState, true);
				}
			}
		} else if ( CheckedState.Mixed.equals(checkedState) ) {
		} else {
			return this;
		}

		if ( ! goingDown ) {
			if ( ! this.isOwner() ) {
				((TreeFolderViewBean)this.getParent()).setCheckedState(this.judgeState(this.getParent()), false);
			}
		}

		return this;
	}

	private CheckedState judgeState( ITreeFolderViewBean targetFolder ) {
		int count = 0;

		for ( ITreeFolderViewBean folder: targetFolder.getFolders() ) {
			if ( CheckedState.Mixed.equals(folder.getCheckedState()) ) {
				return CheckedState.Mixed;
			}

			if ( CheckedState.Checked.equals(folder.getCheckedState()) ) {
				count++;
			}
		}

		if ( 0 == count ) {
			return CheckedState.UnChecked;
		}

		if ( targetFolder.getFolders().size() == count ) {
			return CheckedState.Checked;
		}

		return CheckedState.Mixed;
	}

	@Override
	public boolean isOpenFolder() {
		return openFolder;
	}

	public TreeFolderViewBean setOpenFolder(boolean openFolder) {
		if ( ! this.isOwner() ) {
			this.openFolder = openFolder;
		}

		return this;
	}

	@Override
	public List<ITreeFolderViewBean> getFolders() {
		return folders;
	}
	@Override
	public TreeFolderViewBean addFolders(List<ITreeFolderViewBean> folders) {
		for ( ITreeFolderViewBean folder: folders ) {
			addFolder( folder );
		}

		return this;
	}
	@Override
	public TreeFolderViewBean addFolder(ITreeFolderViewBean folder) {
		this.folders.add(folder);

		((TreeFolderViewBean)folder).setOwner(this.owner);
		((TreeFolderViewBean)folder).setParent(this);
		((TreeFolderViewBean)folder).setPath();

		return this;
	}

	@Override
	public TreeFolderViewBean setFolders(List<ITreeFolderViewBean> folders) {
		this.folders = new ArrayList<ITreeFolderViewBean>();
		return this.addFolders( folders );
	}

	public static ITreeFolderViewBean toBean( List<String> paths ) {
		return toBean(FluentList.from(paths).toArray(new String[0]));
	}

	public static ITreeFolderViewBean toBean( String... paths ) {
		ITreeFolderViewBean root = new TreeFolderViewBean().setOwner().setName("");

		if ( null == paths )
			return root;

		for ( String path: paths ) {
			String[] split = TriStringUtils.split( path, "/" );
			String linkPath = "";
			ITreeFolderViewBean correntTree = root;

			for ( String folderPath: split ) {
				linkPath += (linkPath.equals("/")? "": "/") + folderPath;

				ITreeFolderViewBean tree = correntTree.getFolder(linkPath);
				if ( null == tree ) {
					tree = new TreeFolderViewBean().setName(folderPath);
					correntTree.addFolder(tree);
				}
				correntTree = tree;
			}
		}

		return root;
	}

	/**
	 * Enumeration type of checked state.
	 */
	public enum CheckedState {
		UnChecked(),
		Checked(),
		Mixed(),
		;

		private String value = null;

		private CheckedState( String value) {
			this.value = value;
		}

		private CheckedState() {
			this.value = this.name();
		}

		public boolean equals( String value ) {
			CheckedState state = value( value );

			if ( null == state ) return false;
			if ( ! this.equals(state) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static CheckedState value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return UnChecked;
			}

			for ( CheckedState state : values() ) {
				if ( state.value().equals( value ) ) {
					return state;
				}
			}

			return UnChecked;
		}
	}
}
