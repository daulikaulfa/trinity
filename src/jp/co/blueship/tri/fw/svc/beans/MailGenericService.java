package jp.co.blueship.tri.fw.svc.beans;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.RmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * メール送信を行う汎用の上位クラスです。
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class MailGenericService implements IDomain<IGeneralServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	public static final String FLOW_SYSTEM_ERROR_ID = "FlowSystemErrorService";
	public static final String FLOW_BUSINESS_ERROR_PREFIX = "Error";

	@SuppressWarnings("unchecked")
	@Override
	public IServiceDto<IGeneralServiceBean> execute(IServiceDto<IGeneralServiceBean> serviceDto ) {

		IGeneralServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( null != paramBean.getBusinessThrowable() ) {
				if (! DesignSheetUtils.isMail( AmDesignBeanId.mail, FLOW_BUSINESS_ERROR_PREFIX + paramBean.getFlowAction() ) &&
						! DesignSheetUtils.isMail( RmDesignBeanId.mail, FLOW_BUSINESS_ERROR_PREFIX + paramBean.getFlowAction() ) ) {
						return serviceDto;
					}
			}

			if ( null != paramBean.getSystemThrowable() ) {
				if ( ! DesignSheetUtils.isMail( UmDesignBeanId.mail, FLOW_SYSTEM_ERROR_ID) )
				return serviceDto;
			}

			if ( null == paramBean.getBusinessThrowable() && null == paramBean.getSystemThrowable() ) {
				if (! DesignSheetUtils.isMail( AmDesignBeanId.mail, paramBean.getFlowAction() ) &&
					! DesignSheetUtils.isMail( RmDesignBeanId.mail, paramBean.getFlowAction() ) ) {
					return serviceDto;
				}
			}

			IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

			IDomain<IGeneralServiceBean> pojo = null;

			if ( null != paramBean.getSystemThrowable() ) {
				pojo = (IDomain<IGeneralServiceBean>)ac.getBean( "mailSystemErrorService" );
			}

			if ( null != paramBean.getBusinessThrowable() ) {
				pojo = (IDomain<IGeneralServiceBean>)ac.getBean( "mailError" + paramBean.getFlowAction() );
			}

			if ( null == paramBean.getBusinessThrowable() && null == paramBean.getSystemThrowable() ) {
				pojo = (IDomain<IGeneralServiceBean>)ac.getBean( "mail" + paramBean.getFlowAction() );
			}

			return pojo.execute(serviceDto);
		} catch ( Exception e ) {
			//メール送信が失敗しても処理を続行する
			LogHandler.fatal( log , e ) ;
		}

		return serviceDto;

	}

}
