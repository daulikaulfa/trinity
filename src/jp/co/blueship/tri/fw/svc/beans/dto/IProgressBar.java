package jp.co.blueship.tri.fw.svc.beans.dto;

/**
 * A component that visually displays the progress of some task. As the task progresses towards completion, the progress bar displays the task's percentage of completion.
 * <br>
 * <br>一部のタスクの進捗状況を視覚的に表示するコンポーネントです。タスクの処理の進行に合わせて、進捗バーに、処理の完了のパーセンテージが表示されます。
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IProgressBar {
	/**
	 * Sets the progress bar's current value to value.
	 * <br>
	 * <br>進捗バーの現在の値を value に設定します。
	 *
	 * @param value
	 * @return
	 */
	public IProgressBar setValue(int value);

	/**
	 * Returns the progress bar's current value. The value is always between the minimum and maximum values, inclusive.
	 * <br>
	 * <br>進捗バーの現在の値を返します。この値は常に、最小値以上、最大値以下の範囲に収まります。
	 *
	 * @return the current value of the progress bar. 進捗バーの現在の値
	 */
	public int getValue();

	/**
	 * Sets the progress bar's minimum value to n.
	 * <br>
	 * <br>進捗バーの最小値をminimumに設定します。
	 *
	 * @param minimum the new minimum 新しい最小値
	 * @return
	 */
	public IProgressBar setMinimum(int minimum);

	/**
	 * Returns the progress bar's minimum value.
	 * <br>
	 * <br>進捗バーの現在の最小値を返します。
	 *
	 * @return the progress bar's minimum value
	 */
	public int getMinimum();

	/**
	 * Sets the progress bar's maximum value to n.
	 * <br>
	 * <br>進捗バーの最大値をmaximumに設定します。
	 *
	 * @param maximum the new maximum 新しい最大値
	 * @return
	 */
	public IProgressBar setMaximum(int maximum);

	/**
	 * Returns the progress bar's maximum value.
	 * <br>
	 * <br>進捗バーの現在の最大値を返します。
	 *
	 * @return the progress bar's maximum value
	 */
	public int getMaximum();

	/**
	 * Sets the percent complete for the progress bar. Note that this number is between 0.0 and 1.0.
	 * <br>
	 * <br>進捗バーの完了したパーセントを設定します。値は 0.0 〜 1.0 の範囲であることに注意してください。
	 *
	 * @param
	 * @return
	 */
	public IProgressBar setPercentComplete(double maximum);

	/**
	 * Returns the percent complete for the progress bar. Note that this number is between 0.0 and 1.0.
	 * <br>
	 * <br>進捗バーの完了したパーセントを返します。値は 0.0 〜 1.0 の範囲であることに注意してください。
	 *
	 * @return the percent complete for this progress bar 進捗バーの完了したパーセント
	 */
	public double getPercentComplete();

	/**
	 * Returns a String representation of the current progress.
	 * By default, this returns a simple percentage String based on the value returned from getPercentComplete.
	 * An example would be the "42%". You can change this by calling setString.
	 * <br>
	 * <br>現在の進捗状況を表す String 表現を返します。
	 * デフォルトでは、getPercentComplete の戻り値に基づいて、単純なパーセンテージを表す String が返されます。
	 * たとえば、「42%」のような文字列になります。この設定は、setString を呼び出すことで変更できます。
	 *
	 * @return the value of the progress string, or a simple percentage string if the progress string is null 進捗文字列の値。進捗文字列が null の場合、単純なパーセンテージを示す文字列
	 */
	public String getString();

}
