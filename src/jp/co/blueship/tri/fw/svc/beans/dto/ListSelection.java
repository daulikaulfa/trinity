package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ListSelection {
	private String[] selectedIds;
	private Set<String> selectedIdSet = new LinkedHashSet<String>();

	public String[] getSelectedIds() {
		if ( null == selectedIds )
			return new String[0];

		return selectedIds;
	}
	public ListSelection setSelectedIds(String... selectedIds) {
		this.selectedIds = selectedIds;
		return this;
	}
	public Set<String> getSelectedIdSet() {
		return selectedIdSet;
	}
	public void setSelectedIdSet(Set<String> selectedIdSet) {
		this.selectedIdSet = selectedIdSet;
	}

}
