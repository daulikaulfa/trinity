package jp.co.blueship.tri.fw.svc.beans;

import static jp.co.blueship.tri.fw.cmn.utils.comparator.Comparators.*;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunctions;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.dao.orm.IEntity;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.mail.beans.dto.MailServiceBean;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.SendMailBean;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * ロット情報から特定グループを取得し、グループの全ユーザ情報を取得します。 <br>
 * <br>
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class GatherUserByGroupFromLot implements IDomain<MailServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static final TriPredicate<IEntity> IS_LOT_ENTITY = new TriPredicate<IEntity>() {

		@Override
		public boolean evalute(IEntity entity) {
			return ILotEntity.class.isInstance(entity);
		}
	};

	private IUmFinderSupport umFinderSupport;

	public GatherUserByGroupFromLot() {
	}

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<MailServiceBean> execute(IServiceDto<MailServiceBean> serviceDto) {

		try {
			MailServiceBean paramBean = serviceDto.getServiceBean();
			SendMailBean targetBean = paramBean.getTarget();
			List<IUserEntity> userList = userListFrom(targetBean);
			String lotId = lotIdFrom(paramBean);

			if (TriStringUtils.isNotEmpty(lotId)) {

				List<String> mailGroupIds = umFinderSupport.findMailGroupIdsByLotId(lotId);

				if (TriCollectionUtils.isNotEmpty(mailGroupIds)) {
					TriCollectionUtils.forEach(mailGroupIds, retrieveUsers(userList));

					if (TriCollectionUtils.isNotEmpty(mailGroupIds) && TriStringUtils.isEmpty(targetBean.getUserView())) {
						targetBean.setUserView(groupFrom(mailGroupIds.get(0)).getGrpNm());
					}
				}
			}

			// ユーザリストの重複を除外
			userList = this.delDuplicateUserEntity(userList);

			targetBean.setUsersView(userList);

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005063S, e);
		}

		return serviceDto;
	}

	private List<IUserEntity> userListFrom(SendMailBean targetBean) {

		if (null != targetBean.getUsersView()) {
			return targetBean.getUsersView();
		}

		return new ArrayList<IUserEntity>();
	}

	private List<IUserEntity> usersFrom(String groupId) {
		return umFinderSupport.findUserByGroup(groupId);
	}

	private String lotIdFrom(MailServiceBean paramBean) {

		IEntity entity = FluentList.from(paramBean.getEntity()).//
				atFirstOf(IS_LOT_ENTITY);

		return ILotEntity.class.cast(entity).getLotId();
	}

	private IGrpEntity groupFrom(String groupId) {
		return umFinderSupport.findGroupById(groupId);
	}

	private List<IUserEntity> delDuplicateUserEntity(List<IUserEntity> userList) {

		ComparableSet<IUserEntity> newSet = new ComparableSet<IUserEntity>(IS_EQUAL_USER_ID);
		TriCollectionUtils.collect(userList, TriFunctions.<IUserEntity> nopFunction(), newSet);

		return FluentList.from(newSet).asList();
	}

	private TriClojure<String> retrieveUsers(final List<IUserEntity> userList) {

		return new TriClojure<String>() {

			@Override
			public boolean apply(String groupId) {

				userList.addAll(usersFrom(groupId));
				return true;
			}
		};
	}

}
