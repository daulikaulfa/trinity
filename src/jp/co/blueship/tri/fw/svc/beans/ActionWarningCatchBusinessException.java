package jp.co.blueship.tri.fw.svc.beans;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 業務処理で発生した例外を警告レベルに変換します。
 * <br>
 * <br>ラップされた処理内で例外が発生すると、例外を共通リスト内に
 * 設定したうえで、例外をthrowせずに処理を続行させます。
 * <br>
 * <br>注意する点は、この処理内以外で、一つの例外も発生しない場合に、
 * この例外を上位呼び出し元でcatch出来なくなり、ロールバックされるはずのＤＢ更新値が
 * コミットされてしまう場合がある事です。
 * <br>コミットされても問題のない業務チェックを運用上緩和させたい場合など、設計上の考慮に
 * 基づいて使用するようにして下さい。
 * <br>
 * <br>このクラスは、必ずsingleton = falseで実行します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ActionWarningCatchBusinessException implements IDomain<IGeneralServiceBean> {

	private IDomain<IGeneralServiceBean> action = null;

	/**
	 * ビジネス処理がインスタンス生成時に自動的に設定されます。
	 * @param action ビジネス処理
	 */
	public final void setAction( IDomain<IGeneralServiceBean> action ) {
		this.action = action;
	}

	@Override
	public IServiceDto<IGeneralServiceBean> execute( IServiceDto<IGeneralServiceBean> serviceDto ) {
		List<Object> paramList = serviceDto.getParamList();

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		try {

			if ( null != action )
				action.execute( serviceDto );

		} catch ( BaseBusinessException e ) {
			e.printStackTrace();

			BaseBusinessException ex = extractBaseBusinessException(paramList);
			paramList.remove( extractBaseBusinessException(paramList) );

			if ( null == ex ) {
				paramList.add( e );
			} else {
				this.getMessageList(messageList, messageArgsList, ex);
				this.getMessageList(messageList, messageArgsList, e);

				paramList.add( new ContinuableBusinessException( messageList, messageArgsList ) );
			}

		} finally {
		}

		return serviceDto;

	}

	private final void getMessageList(List<IMessageId> destMessageList,
										List<String[]> destMessageArgsList,
										BaseBusinessException e ) {
		if ( e instanceof ContinuableBusinessException )
			this.getMessageList(destMessageList, destMessageArgsList, (ContinuableBusinessException)e);

		if ( e instanceof BusinessException )
			this.getMessageList(destMessageList, destMessageArgsList, (BusinessException)e);

	}

	private final void getMessageList(List<IMessageId> destMessageList,
										List<String[]> destMessageArgsList,
										ContinuableBusinessException e ) {
		destMessageList.addAll( ((ContinuableBusinessException)e).getMessageIdList() );
		destMessageArgsList.addAll( ((ContinuableBusinessException)e).getMessageArgsList() );

	}

	private final void getMessageList(List<IMessageId> destMessageList,
										List<String[]> destMessageArgsList,
										BusinessException e ) {
		destMessageList.add( e.getMessageID() );
		destMessageArgsList.add( e.getMessageArgs() );

	}
	/**
	 * 指定されたリストから業務ロジック例外情報を取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。例外が設定されていない場合、nullを戻します。
	 * @throws TriSystemException このメソッドの使い方に誤りがある場合
	 */
	public static final BaseBusinessException extractBaseBusinessException( List<Object> list )
		throws TriSystemException {

		BaseBusinessException e = null;
		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();
			if ( !(obj instanceof BaseBusinessException) )
				continue;

			e = (BaseBusinessException)obj;
			break;
		}

		return e;
	}

}
