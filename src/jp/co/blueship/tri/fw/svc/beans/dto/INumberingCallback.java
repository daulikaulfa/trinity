package jp.co.blueship.tri.fw.svc.beans.dto;

import java.util.Map;

import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * コールバックインタフェースです。
 *
 */
public interface INumberingCallback {

	

	/**
	 * 採番情報を戻します。
	 *
	 * @param bean 採番サービス情報
	 * @param data テンプレートへマージするデータ
	 * @return テンプレートへマージするデータ
	 * @throws TriSystemException
	 */
	public Map<String, Object> getNumbering( NumberingServiceBean bean, Map<String, Object> data )  throws TriSystemException;

}
