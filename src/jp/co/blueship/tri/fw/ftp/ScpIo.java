package jp.co.blueship.tri.fw.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Hashtable;
import java.util.List;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

import jp.co.blueship.tri.fw.cmn.io.GenericIo;
import jp.co.blueship.tri.fw.cmn.io.ObjInfo;
import jp.co.blueship.tri.fw.cmn.io.constants.FileCopyMode;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 「ＳＣＰ接続」関連操作クラスに関する実装クラスです。<br>
 * <br>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ScpIo implements GenericIo {

	private static final ILog log = TriLogFactory.getInstance() ;

//	public static final String FILENAME_DIR_CURRENT	=	"." ;
//	public static final String FILENAME_DIR_PARENT	=	".." ;

	public static final String CHANNEL_EXEC = "exec" ;

	private JSch jsch = null ;		//

	@SuppressWarnings("unused")
	private FileCopyMode copyMode = FileCopyMode.OVERWRITE_ABORT ;			//ファイルコピーモード種別

	private ScpParamBean scpParam = null ; 	//ＳＣＰ接続パラメータ

	private Session session = null ;
	private Channel channel = null ;

	private String latestEndStatus = null ;

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	/**
	 * コンストラクタ
	 * @param scpParam ＳＣＰ接続に用いる設定パラメータ群
	 */
	public ScpIo( ScpParamBean scpParam ) {
		this.scpParam = scpParam ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#setCopyMode(jp.co.blueship.trinity.common.io.define.FileCopyMode)
	 */
	public void setCopyMode(FileCopyMode copyMode) {
		this.copyMode = copyMode;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getLatestEndStatus()
	 */
	public String getLatestEndStatus() {
		return this.latestEndStatus ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#openConnection()
	 */
	public boolean openConnection() throws ScpIoException {
		if( null == jsch ) {
			try {
				log.debug( "ログイン前: " ) ;
				jsch = new JSch() ;

//				HostKeyチェック
				boolean hostKeyChk = false ;
				if( null != scpParam.getKnownHost() ) {
					File file = new File( scpParam.getKnownHost() ) ;
					if( file.isFile() && file.canRead() ) {
						jsch.setKnownHosts( scpParam.getKnownHost() ) ;
						hostKeyChk = true ;
					}
				}
				if( false == hostKeyChk ) {//厳格なHostKeyチェックを行わない
					Hashtable<String, String> config = new Hashtable<String, String>();
					config.put( "StrictHostKeyChecking" , "no" ) ;
					JSch.setConfig( config ) ;
				}
				//鍵認証 「鍵ファイルのパス」、「パスフレーズ」をセット
				//使うならそのときに実装を考える
				//jsch.addIdentity( arg0 , arg1 ) ;

				//接続要求
				this.session = jsch.getSession( this.scpParam.getUser() , this.scpParam.getHost() , this.scpParam.getPort() ) ;
				this.session.setPassword( this.scpParam.getPass() ) ;
				this.session.connect() ;
				if( true != this.session.isConnected() ) {
					return false ;
				}

				log.debug( "ログイン完了: " ) ;
			} catch ( Exception e ) {
				throw new ScpIoException ( SmMessageId.SM005165S , e ) ;
			}
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#closeConnection()
	 */
	public void closeConnection() throws Exception {
		if( null != this.channel ) {
			if( true == channel.isConnected() ) {
				channel.disconnect() ;
			}
		}
		if( null != this.session ) {
			if( true == session.isConnected() ) {
				session.disconnect() ;
			}
		}
		log.debug( "ログアウト完了: " ) ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean putObject( String localPath , String srvPath ) throws Exception {
		/*boolean ret = true ;
		FilePathDelimiter filePath = new FilePathDelimiter( localPath ) ;

		File file = new File( localPath ) ;
		if( true != file.exists() ) {
			throw new ScpIoException( "送信するファイルまたはディレクトリが存在しません： " + localPath ) ;
		}
		if( file.isDirectory() ) {//ディレクトリ
			if(DBG)System.out.println( "ディレクトリ：" + localPath ) ;
			String newDir = StringAddonUtil.linkPathBySlash( srvPath , file.getName() ) ;
			this.makeDirectory( newDir ) ;

			File[] files = file.listFiles() ;
			for( File tmpFile : files ) {
				String path = StringAddonUtil.linkPathBySlash( localPath , tmpFile.getName() ) ;
				ret = putObject( path , newDir ) ;
				if( false == ret ) {
					return false ;
				}
			}
		} else {//ファイル
			if(DBG)System.out.println( "ファイル  ：" + file.getName() ) ;
			String newPath = StringAddonUtil.linkPathBySlash( srvPath , filePath.getObjName() ) ;
			boolean sts = this.putFile( localPath , newPath ) ;
			if( COPYMODE_OVERWRITE_ABORT == this.copyMode && true != sts	) {
				return false ;
			}
		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
		//return ret ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean getObject( String localPath , String srvPath ) throws Exception {
		/*boolean ret = true ;
		FilePathDelimiter filePath = new FilePathDelimiter( srvPath ) ;

		SftpATTRS sftpAttrs = this.getAttribute( srvPath ) ;
		if( null == sftpAttrs ) {
			throw new ScpIoException( "受信するファイルまたはディレクトリが存在しません： " + srvPath ) ;
		}
		if( sftpAttrs.isDir() ) {//ディレクトリ
			if(DBG)System.out.println( "ディレクトリ：" + srvPath ) ;
			String newDir = StringAddonUtil.linkPathBySlash( localPath , filePath.getObjName() ) ;
			File file = new File( newDir ) ;
			file.mkdirs() ;

			Vector<LsEntry> list = this.getList( srvPath ) ;
			for( LsEntry lsEntry : list ) {
				ret = getObject( newDir , StringAddonUtil.linkPathBySlash( srvPath , lsEntry.getFilename() ) ) ;
				if( false == ret ) {
					return false ;
				}
			}
		} else {//ファイル
			if(DBG)System.out.println( "ファイル  ：" + srvPath ) ;
			String newPath = StringAddonUtil.linkPathBySlash( localPath , filePath.getObjName() ) ;
			boolean sts = this.getFile( newPath , srvPath ) ;
			if( COPYMODE_OVERWRITE_ABORT == this.copyMode && true != sts ) {
				return false ;
			}
		}
		*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
		//return ret ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public void deleteObject( String srvPath ) throws Exception {
		/*
		SftpATTRS sftpAttrs = this.getAttribute( srvPath ) ;
		if( null == sftpAttrs ) {
			throw new ScpIoException( "削除するファイルまたはディレクトリが存在しません： " + srvPath ) ;
		}
		if( sftpAttrs.isDir() ) {//ディレクトリ
			if(DBG)System.out.println( "ディレクトリ：" + srvPath ) ;
			Vector list = this.getList( srvPath ) ;
			for( Object tmp : list ) {
				LsEntry lsEntry = (LsEntry)tmp ;
				deleteObject( StringAddonUtil.linkPathBySlash( srvPath , lsEntry.getFilename() ) ) ;
			}
			this.removeDirectory( srvPath ) ;
		} else {//ファイル
			if(DBG)System.out.println( "ファイル  ：" + srvPath ) ;
			this.removeFile( srvPath ) ;
		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public void renameObject( String path , String newPath ) throws Exception {
		/*
		if( null == this.channelSftp ||
			true != this.channelSftp.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
			channelSftp.rename( path , newPath ) ;
		} catch ( Exception e ) {
			this.channelSftp.disconnect() ;
			this.latestEndStatus = this.channelSftp.getExitStatus() ;
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean makeDirectory( String path ) throws Exception {
		/*
		if( null == this.channelSftp ||
			true != this.channelSftp.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
			//作成対象のディレクトリ有無をチェック
			SftpATTRS sftpAttrs = this.getAttribute( path ) ;
			if( null != sftpAttrs ) {
				if( true != sftpAttrs.isDir() ) {
					throw new ScpIoException( "同名のファイルが既に存在します： " + path ) ;
				} else {
					return false ;
				}
			}
			//ディレクトリ作成
			channelSftp.mkdir( path ) ;
			if(DBG)System.out.println("ディレクトリの作成完了: " + getCurrentDateTime() ) ;
		} catch ( Exception e ) {
			this.channelSftp.disconnect() ;
			this.latestEndStatus = this.channelSftp.getExitStatus() ;
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
		//return true ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean removeDirectory( String path ) throws Exception {
		/*
		if( null == this.channelSftp ||
			true != this.channelSftp.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
			//削除対象のディレクトリ有無をチェック
			SftpATTRS sftpAttrs = this.getAttribute( path ) ;
			if( null == sftpAttrs ) {
				throw new ScpIoException( "削除対象のディレクトリが存在しません： " + path ) ;
			}
			//ディレクトリ削除
			channelSftp.rmdir( path ) ;
			if(DBG)System.out.println("ディレクトリの削除完了: " + getCurrentDateTime() ) ;
		} catch ( Exception e ) {
			this.channelSftp.disconnect() ;
			this.latestEndStatus = this.channelSftp.getExitStatus() ;
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
		//return true ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public List<ObjInfo> getList( String path ) throws Exception {
		/*
		if( null == this.channelSftp ||
			true != this.channelSftp.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
			if(DBG)System.out.println("ファイル一覧取得開始: " + getCurrentDateTime() ) ;
//			ファイル一覧を取得
			Vector<LsEntry> list = channelSftp.ls( path ) ;
			Vector<LsEntry> newList = new Vector<LsEntry>() ;
			for( LsEntry lsEntry : list ) {//リストに".."と"."が含まれるため、除外する
				if( true != FILENAME_DIR_CURRENT.equals( lsEntry.getFilename() ) &&
					true != FILENAME_DIR_PARENT.equals( lsEntry.getFilename() ) ) {
					newList.add( lsEntry ) ;
					System.out.println( lsEntry.toString() ) ;
				}
			}
			if(DBG)System.out.println("ファイル一覧取得完了: " + getCurrentDateTime() ) ;
			return newList ;
		} catch( Exception e ) {
			this.channelSftp.disconnect() ;
			this.latestEndStatus = this.channelSftp.getExitStatus() ;
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public ObjInfo getAttribute( String path ) throws Exception {
		/*
		FilePathDelimiter filePath = new FilePathDelimiter( path ) ;
		if( true != filePath.isEnabled() ) {
			throw new ScpIoException( "パスが正しくありません" ) ;
		}

		try {
			SftpATTRS sftpAttrs = channelSftp.lstat( path ) ;
			if(DBG)System.out.println( "lstat " + sftpAttrs ) ;
			return sftpAttrs ;
		} catch( Exception e ) {
			return null ;//ファイルがない場合、Exceptionが発生するので止めて、Nullを返す
		} finally {

		}
		return null ;*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean isExist( String path ) throws Exception {
		/*
		SftpATTRS sftpAttrs = this.getAttribute( path ) ;
		if( null != sftpAttrs ) {
			return true ;
		}
		*/
		return false ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean isFile( String path ) throws Exception {
		/*
		SftpATTRS sftpAttrs = this.getAttribute( path ) ;
		if( null != sftpAttrs ) {
			if( true != sftpAttrs.isDir() ) {
				return true ;
			}
		}*/
		return false ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean isDirectory( String path ) throws Exception {
		/*
		SftpATTRS sftpAttrs = this.getAttribute( path ) ;
		if( null != sftpAttrs ) {
			if( true == sftpAttrs.isDir() ) {
				return true ;
			}
		}*/
		return false ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getFile(java.lang.String, java.lang.String)
	 */
	public boolean getFile( String localPath , String srvPath ) throws Exception {
		if( null == this.session ||
			true != this.session.isConnected() ) {
			throw new ScpIoException( SmMessageId.SM005167S ) ;
		}/*
//		ローカルの既存ファイル有無をチェック
		File file = new File( localPath ) ;
		if( true == file.isDirectory() ) {
			throw new ScpIoException( "ファイルの受信先に同名のディレクトリが存在します： " + localPath ) ;
		}
		if( true == file.isFile() ) {
			if( FileCopyMode.OVERWRITE_NONE == this.copyMode ||
					FileCopyMode.OVERWRITE_ABORT == this.copyMode ) {
				return false ;
			}
		}*/

		FileOutputStream fos = null ;
		try {
			String prefix = null ;
			if( new File( localPath ).isDirectory() ) {
				prefix = localPath + File.separator ;
			}
			//チャンネルをオープン
			channel = (ChannelExec)session.openChannel( CHANNEL_EXEC ) ;
			//ファイル取得コマンドを発行
			String command = "scp -f " + srvPath ;

			byte[] cmdByte = command.getBytes( this.scpParam.getControlEncoding().toString() ) ;
			( (ChannelExec)channel ).setCommand( cmdByte ) ;

			//コマンド／応答のストリームを生成
			OutputStream os = channel.getOutputStream();
			InputStream is = channel.getInputStream();
			//接続
			channel.connect() ;
			if( true != channel.isConnected() ) {
				throw new ScpIoException( SmMessageId.SM005168S ) ;
			}
			//データ受け渡し
			byte[] buf = new byte[ 1024 ] ;
			// send '\0'
			buf[ 0 ] = 0 ;
			os.write( buf , 0 , 1 ) ;
			os.flush() ;

			while( true ) {
				int c = checkAck( is ) ;
				if( c != 'C' ) {
					break ;
				}
				// read '0644 '
				is.read( buf , 0 , 5 ) ;

				long filesize = 0L ;
				while( true ) {
					if( 0 > is.read( buf , 0 , 1 ) ) {
						break ; //error
					}
					if( ' ' == buf[0] ) {
						break ;
					}
					filesize = filesize * 10L + (long)( buf[ 0 ] - '0' ) ;
				}

				String fileRecv = null ;
				for( int i=0 ; ; i++ ) {
					is.read( buf , i , 1 ) ;
					if( buf[ i ] == (byte)0x0a ) {
						fileRecv = new String( buf , 0 , i ) ;
						break ;
					}
				}
				//System.out.println("filesize="+filesize+", file="+file);

				// send '\0'
				buf[ 0 ] = 0 ;
				os.write( buf , 0 , 1 ) ;
				os.flush() ;

				// read a content of lfile
				fos = new FileOutputStream( null == prefix ? localPath : prefix + fileRecv ) ;
				int foo ;
				while( true ) {
					if( filesize > buf.length ) {
						foo = buf.length ;
					} else {
						foo = (int)filesize ;
					}
					foo = is.read( buf , 0 , foo ) ;
					if( 0 > foo ) {
						break ;// error
					}
					fos.write( buf , 0 , foo ) ;
					filesize -= foo ;
					if( 0L == filesize ) {
						break ;
					}
				}
				fos.flush() ;
				fos.close() ;
				fos = null ;

				if( 0 != checkAck( is ) ) {
					System.exit( 0 ) ;
				}
				// send '\0'
				buf[ 0 ] = 0 ;
				os.write( buf , 0 , 1 ) ;
				os.flush() ;
			}


			log.debug( "ファイル受信完了: " ) ;
		} catch ( Exception e ) {
			if( null != channel ) {
				this.latestEndStatus = String.valueOf( this.channel.getExitStatus() ) ;
				channel.disconnect() ;
			}
			throw new ScpIoException ( SmMessageId.SM005169S , e ) ;
		} finally {
			if( null != channel ) {
				channel.disconnect() ;
			}
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#putFile(java.lang.String, java.lang.String)
	 */
	public boolean putFile( String localPath , String srvPath ) throws Exception {
		if( null == this.session ||
			true != this.session.isConnected() ) {
			throw new ScpIoException( SmMessageId.SM005167S ) ;
		}

		if( true != new File( localPath ).exists() ) {
			throw new FileNotFoundException( ac.getMessage( SmMessageId.SM004198F , srvPath ) ) ;
		}

	    FileInputStream fis = null ;
		try {

			// exec 'scp -t rfile' remotely
			String command="scp -p -t "+ srvPath ;
			byte[] cmdByte = command.getBytes( this.scpParam.getControlEncoding().toString() ) ;

			channel = (ChannelExec)session.openChannel( CHANNEL_EXEC ) ;
			((ChannelExec)channel).setCommand( cmdByte ) ;

			// get I/O streams for remote scp
			OutputStream os = channel.getOutputStream() ;
			InputStream is = channel.getInputStream() ;

			channel.connect() ;

			if( 0 != checkAck( is ) ) {
				System.exit( 0 ) ;
			}

			// send "C0644 filesize filename", where filename should not include '/'
			long filesize = ( new File( localPath ) ).length() ;
			command = "C0644 " + filesize + " " ;
			if( 0 < localPath.lastIndexOf( '/' ) ) {
				command += localPath.substring( localPath.lastIndexOf( '/' ) + 1 ) ;
			} else{
		       	command += localPath ;
			}
			command += "\n" ;
			os.write( command.getBytes() ) ;
			os.flush() ;
			if( 0 != checkAck( is ) ) {
				System.exit( 0 ) ;
			}

			// send a content of lfile
			fis = new FileInputStream( localPath ) ;
			byte[] buf = new byte[ 1024 ] ;
			while( true ) {
				int len = fis.read( buf , 0 , buf.length ) ;
				if( 0 >= len ) {
					break ;
				}
				os.write(buf, 0, len);
				//out.flush();
			}
			fis.close() ;
			fis = null ;
			// send '\0'
			buf[ 0 ] = 0 ;
			os.write( buf , 0 , 1 ) ;
			os.flush() ;
			if( 0 != checkAck( is ) ) {
				System.exit(0);
			}
			os.close();


			log.debug( "ファイル転送完了: " ) ;
		} catch ( Exception e ) {
			if( null != channel ) {
				this.latestEndStatus = String.valueOf( this.channel.getExitStatus() ) ;
				channel.disconnect() ;
			}
			throw new ScpIoException( SmMessageId.SM005170S , e ) ;
		} finally {
			if( null != channel ) {
				channel.disconnect() ;
			}
		}
		return true ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public boolean removeFile( String path ) throws Exception {
		/*
		if( null == this.channel ||
			true != this.channel.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
			//削除対象のファイル有無をチェック
			SftpATTRS sftpAttrs = this.getAttribute( path ) ;
			if( null == sftpAttrs ) {
				throw new ScpIoException( "取得対象のファイルがサーバに存在しません： " + path ) ;
			}
			if( true == sftpAttrs.isDir() ) {
				throw new ScpIoException( "取得対象はディレクトリです： " + path ) ;
			}
			//ローカルに取得
			channel.rm( path ) ;
			if(DBG)System.out.println("ファイル削除完了: " + getCurrentDateTime() ) ;
		} catch ( Exception e ) {
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
		//return true ;
	}

	/**
	 * @deprecated 未対応の機能です。
	 */
	public void changeDirectory( String path ) throws Exception {
		/*
		if( null == this.session ||
			true != this.session.isConnected() ) {
			throw new ScpIoException( "ＳＣＰサーバに未接続です" ) ;
		}

		try {
//			ディレクトリ移動
			channelSftp.cd( path ) ;
		} catch( Exception e ) {
			this.channelSftp.disconnect() ;
			this.latestEndStatus = this.channelSftp.getExitStatus() ;
			throw new ScpIoException( "エラーが発生しました" , e ) ;
		} finally {

		}*/
//		throw new ScpIoException( "機能が未実装です" ) ;
		throw new ScpIoException( SmMessageId.SM005166S ) ;
	}

	/**
	 * ACK戻り値の判定を行う<br>
	 * 戻り値がエラーの場合、例外を発生する<br>
	 * @param in 	InputStream
	 * @return ACK戻り値
	 */
	@SuppressWarnings("deprecation")
	private static int checkAck( InputStream in ) throws Exception {
		int b = in.read() ;
		// 0	:	success
		// 1	:	error
		// 2	:	fatal error
		// -1	:	?
		if( 0 == b ) {
			return b ;
		} else if( -1 == b ) {
			return b ;
		} else if( 1 == b || 2 == b ) {
			StringBuilder stb = new StringBuilder() ;
			int c ;
			do {
				c = in.read() ;
				stb.append( (char) c ) ;
			} while( '\n' != c ) ;

			if( 1 == b ) { // error
				throw new ScpIoException( stb.toString() ) ;
			}
			if( 2 == b ) { // fatal error
				throw new ScpIoException( stb.toString() ) ;
			}
		}
		return b ;
	}
}
