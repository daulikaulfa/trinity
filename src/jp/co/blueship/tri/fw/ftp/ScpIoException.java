package jp.co.blueship.tri.fw.ftp;

import jp.co.blueship.tri.fw.ex.TriException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 「Ｓｃｐ接続」関連操作クラスに関する例外クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class ScpIoException extends TriException {

	/**
	 *
	 */
	private static final long serialVersionUID = -8745957795940881451L;

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public ScpIoException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public ScpIoException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public ScpIoException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public ScpIoException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, messageArgs, cause);
	}

	/**
	 * 詳細メッセージを指定して本クラスを構築します。
	 *
	 * @param message 詳細メッセージ
	 */
	@Deprecated
	// メッセージIDを使用してください。
	public ScpIoException( String message ) {
		super( message ) ;
	}

}
