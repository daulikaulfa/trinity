package jp.co.blueship.tri.fw.ftp;

import jp.co.blueship.tri.fw.ex.TriException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 「Ｓｆｔｐ接続」関連操作クラスに関する例外クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class SftpIoException extends TriException {

	/**
	 *
	 */
	private static final long serialVersionUID = 5503002558632622124L;

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public SftpIoException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public SftpIoException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public SftpIoException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public SftpIoException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, messageArgs, cause);
	}

}
