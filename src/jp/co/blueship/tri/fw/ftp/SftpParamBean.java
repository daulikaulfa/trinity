package jp.co.blueship.tri.fw.ftp;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;

/**
 * 「Ｓｃｐ接続」関連操作クラスに関する接続パラメータ設定クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class SftpParamBean {

	

	private String host = null ;	//ホスト名(DNSによる名前解決が行われる場合) or IP-address
	private String user = null ;	//ｆｔｐ接続に用いるユーザ名
	private String pass = null ;	//ｆｔｐ接続に用いるユーザのパスワード

	private Integer port = 22 ;	//PORT番号

	private Charset controlEncoding = Charset.WINDOWS_31J ;//文字コードセット

	private String knownHost = null ;//"/home/hogehoge/.ssh/known_hosts" ;// known_hosts ファイルのフルパス

	public Charset getControlEncoding() {
		return controlEncoding;
	}
	public void setControlEncoding(Charset controlEncoding) {
		this.controlEncoding = controlEncoding;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getKnownHost() {
		return knownHost;
	}
	public void setKnownHost(String knownHost) {
		this.knownHost = knownHost;
	}

}
