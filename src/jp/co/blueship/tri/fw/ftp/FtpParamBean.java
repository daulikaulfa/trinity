package jp.co.blueship.tri.fw.ftp;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;

/**
 * 「Ｆｔｐ接続」関連操作クラスに関する接続パラメータ設定クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FtpParamBean {

	

	private String host = null ;	//ホスト名(DNSによる名前解決が行われる場合) or IP-address
	private String user = null ;	//ｆｔｐ接続に用いるユーザ名
	private String pass = null ;	//ｆｔｐ接続に用いるユーザのパスワード

	private Integer port = 21 ;	//PORT番号
	private Integer defaultTimeout	=	60 * 1000 ;//接続要求のタイムアウト[ms]
	private Integer soTimeout		=	180 * 1000 ;//接続後各種処理のタイムアウト[ms]

	private boolean pasvMode = false ;//PASVモード true:ON false:OFF

	private Charset controlEncoding = Charset.WINDOWS_31J ;//文字コードセット


	public Charset getControlEncoding() {
		return controlEncoding;
	}
	public void setControlEncoding(Charset controlEncoding) {
		this.controlEncoding = controlEncoding;
	}
	public Integer getDefaultTimeout() {
		return defaultTimeout;
	}
	public void setDefaultTimeout(Integer defaultTimeout) {
		this.defaultTimeout = defaultTimeout;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public Integer getSoTimeout() {
		return soTimeout;
	}
	public void setSoTimeout(Integer soTimeout) {
		this.soTimeout = soTimeout;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public boolean isPassiveMode() {
		return pasvMode;
	}
	public void setPasvMode(boolean pasvMode) {
		this.pasvMode = pasvMode;
	}

}
