package jp.co.blueship.tri.fw.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Vector;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;

import jp.co.blueship.tri.fw.cmn.io.FilePathDelimiter;
import jp.co.blueship.tri.fw.cmn.io.GenericIo;
import jp.co.blueship.tri.fw.cmn.io.ObjInfo;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.FileCopyMode;
import jp.co.blueship.tri.fw.cmn.io.constants.ObjType;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 「ＳＦＴＰ接続」関連操作クラスに関する実装クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class SftpIo implements GenericIo {

	private static final ILog log = TriLogFactory.getInstance();

	public static final String FILENAME_DIR_CURRENT = ".";
	public static final String FILENAME_DIR_PARENT = "..";

	public static final String CHANNEL_SFTP = "sftp";

	private JSch jsch = null; //

	private FileCopyMode copyMode = FileCopyMode.OVERWRITE_ABORT; // ファイルコピーモード種別
	private SftpParamBean sftpParam = null; // ＳＦＴＰ接続パラメータ

	private Session session = null;
	private ChannelSftp channelSftp = null;

	private String latestEndStatus = null;

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	/**
	 * コンストラクタ
	 *
	 * @param sftpParam ＳＦＴＰ接続に用いる設定パラメータ群
	 */
	public SftpIo(SftpParamBean sftpParam) {
		this.sftpParam = sftpParam;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#setCopyMode(jp.co.blueship
	 * .trinity.common.io.define.FileCopyMode)
	 */
	public void setCopyMode(FileCopyMode copyMode) {
		this.copyMode = copyMode;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getLatestEndStatus()
	 */
	public String getLatestEndStatus() {
		return this.latestEndStatus;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#openConnection()
	 */
	public boolean openConnection() throws SftpIoException {
		if (null == jsch) {
			try {
				log.debug("ログイン前: ");
				jsch = new JSch();

				// HostKeyチェック
				boolean hostKeyChk = false;
				if (null != sftpParam.getKnownHost()) {
					File file = new File(sftpParam.getKnownHost());
					if (file.isFile() && file.canRead()) {
						jsch.setKnownHosts(sftpParam.getKnownHost());
						hostKeyChk = true;
					}
				}
				// 厳格なHostKeyチェックを行わない
				// 鍵認証 「鍵ファイルのパス」、「パスフレーズ」をセット
				// 使うならそのときに実装を考える
				// jsch.addIdentity( arg0 , arg1 ) ;

				// 接続要求
				this.session = jsch.getSession(this.sftpParam.getUser(), this.sftpParam.getHost(), this.sftpParam.getPort());
				this.session.setPassword(this.sftpParam.getPass());

				// パラメータ設定
				Hashtable<Object, Object> config = new Hashtable<Object, Object>();
				if (false == hostKeyChk) {
					config.put("StrictHostKeyChecking", "no");
				}
				config.put("cipher.s2c", "aes128-cbc, zlib"); // clientへのstreamをAES
																// 128bit-cbcで暗号化,圧縮
				config.put("cipher.c2s", "aes128-cbc, zlib"); // serverへのstreamをAES
																// 128bit-cbcで暗号化,圧縮
				config.put("compression_level", "6"); // zlibの圧縮レベルを設定(level=6)
				config.put("CheckCiphers", "aes128-cbc"); // AES 128bit-cbc
															// のチェック
				session.setConfig(config);

				// セッション接続
				this.session.connect();
				if (true != this.session.isConnected()) {
					return false;
				}
				// チャネルを開く
				channelSftp = (ChannelSftp) session.openChannel(CHANNEL_SFTP);
				channelSftp.connect();
				// サーバ側文字コード系のセット
				if (null != this.sftpParam.getControlEncoding()) {// 文字コード
					channelSftp.setFilenameEncoding(this.sftpParam.getControlEncoding().toString());
				}

				if (true != channelSftp.isConnected()) {
					throw new SftpIoException(SmMessageId.SM005171S);
				}

				log.debug("ログイン完了: ");
			} catch (Exception e) {
				ExceptionUtils.reThrowIfTrinityException(e);
				throw new SftpIoException(SmMessageId.SM005172S, e);
			}
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#closeConnection()
	 */
	public void closeConnection() throws Exception {
		if (null != this.channelSftp) {
			if (true == channelSftp.isConnected()) {
				channelSftp.disconnect();
			}
		}
		if (null != this.session) {
			if (true == session.isConnected()) {
				session.disconnect();
			}
		}
		log.debug("ログアウト完了: ");
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#putObject(java.lang.String,
	 * java.lang.String)
	 */
	public boolean putObject(String localPath, String srvPath) throws Exception {

		File file = new File(localPath);
		if (true != file.exists()) {
			throw new FileNotFoundException(ac.getMessage(SmMessageId.SM004177F, localPath));
		}

		return this.putObjectReentrant(localPath, srvPath);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#putObject(java.lang.String,
	 * java.lang.String)
	 */
	private boolean putObjectReentrant(String localPath, String srvPath) throws Exception {
		boolean ret = true;
		FilePathDelimiter filePath = new FilePathDelimiter(localPath);

		File file = new File(localPath);
		if (true != file.exists()) {
			throw new SftpIoException(SmMessageId.SM004177F, localPath);
		}
		if (file.isDirectory()) {// ディレクトリ
			log.debug("ディレクトリ：" + localPath);
			String newDir = TriStringUtils.linkPathBySlash(srvPath, file.getName());
			this.makeDirectory(newDir);

			File[] files = file.listFiles();
			if (null == files) {
				throw new SftpIoException(SmMessageId.SM004044F, localPath);
			}
			for (File tmpFile : files) {
				ret = putObjectReentrant(TriStringUtils.linkPathBySlash(localPath, tmpFile.getName()), newDir);
				if (false == ret) {
					return false;
				}
			}
		} else {// ファイル
			log.debug("ファイル  ：" + file.getName());
			String newPath = TriStringUtils.linkPathBySlash(srvPath, filePath.getObjName());
			if (true != this.isDirectory(srvPath)) {
				throw new SftpIoException(SmMessageId.SM004178F, srvPath);
			}

			boolean sts = this.putFile(localPath, newPath);
			if (FileCopyMode.OVERWRITE_ABORT == this.copyMode && true != sts) {
				return false;
			}
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#getObject(java.lang.String,
	 * java.lang.String)
	 */
	public boolean getObject(String localPath, String srvPath) throws Exception {

		ObjInfo objInfo = this.getAttribute(srvPath);
		if (objInfo.isAbsent()) {
			throw new FileNotFoundException(ac.getMessage(SmMessageId.SM004179F, srvPath));
		}

		return this.getObjectReentrant(localPath, srvPath);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#getObject(java.lang.String,
	 * java.lang.String)
	 */
	private boolean getObjectReentrant(String localPath, String srvPath) throws Exception {
		boolean ret = true;
		FilePathDelimiter filePath = new FilePathDelimiter(srvPath);

		ObjInfo objInfo = this.getAttribute(srvPath);
		if (objInfo.isAbsent()) {
			throw new SftpIoException(SmMessageId.SM004179F, srvPath);
		}
		if (objInfo.isDirectory()) {// ディレクトリ
			log.debug("ディレクトリ：" + srvPath);
			String newDir = TriStringUtils.linkPathBySlash(localPath, filePath.getObjName());
			File file = new File(newDir);
			file.mkdirs();

			List<ObjInfo> list = this.getList(srvPath);
			if (null == list) {
				throw new SftpIoException(SmMessageId.SM004044F, srvPath);
			}
			for (ObjInfo info : list) {
				ret = getObjectReentrant(newDir, TriStringUtils.linkPathBySlash(srvPath, info.getName()));
				if (false == ret) {
					return false;
				}
			}
		} else {// ファイル
			log.debug("ファイル  ：" + srvPath);
			String newPath = TriStringUtils.linkPathBySlash(localPath, filePath.getObjName());
			if (true != new File(localPath).isDirectory()) {
				throw new SftpIoException(SmMessageId.SM004180F, localPath);
			}

			boolean sts = this.getFile(newPath, srvPath);
			if (FileCopyMode.OVERWRITE_ABORT == this.copyMode && true != sts) {
				return false;
			}
		}
		return ret;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#deleteObject(java.lang.String)
	 */
	public void deleteObject(String srvPath) throws Exception {

		ObjInfo objInfo = this.getAttribute(srvPath);
		if (objInfo.isAbsent()) {
			throw new FileNotFoundException(ac.getMessage(SmMessageId.SM004181F, srvPath));
		}

		this.deleteObjectReentrant(srvPath);
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#deleteObject(java.lang.String)
	 */
	private void deleteObjectReentrant(String srvPath) throws Exception {
		ObjInfo objInfo = this.getAttribute(srvPath);
		if (objInfo.isAbsent()) {
			throw new SftpIoException(SmMessageId.SM004181F, srvPath);
		}
		if (objInfo.isDirectory()) {// ディレクトリ
			log.debug("ディレクトリ：" + srvPath);
			List<ObjInfo> list = this.getList(srvPath);
			if (null == list) {
				throw new SftpIoException(SmMessageId.SM004044F, srvPath);
			}
			for (ObjInfo info : list) {
				deleteObjectReentrant(TriStringUtils.linkPathBySlash(srvPath, info.getName()));
			}
			this.removeDirectory(srvPath);
		} else {// ファイル
			log.debug("ファイル  ：" + srvPath);
			this.removeFile(srvPath);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#renameObject(java.lang.String,
	 * java.lang.String)
	 */
	public void renameObject(String path, String newPath) throws Exception {

		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			channelSftp.rename(path, newPath);
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005174S, e);
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#makeDirectory(java.lang.String
	 * )
	 */
	public boolean makeDirectory(String path) throws Exception {
		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			// 作成対象のディレクトリ有無をチェック
			ObjInfo objInfo = this.getAttribute(path);
			if (objInfo.isPresent()) {
				if (true != objInfo.isDirectory()) {
					throw new SftpIoException(SmMessageId.SM004183F, path);
				} else {
					return false;
				}
			}
			// ディレクトリ作成
			channelSftp.mkdir(path);
			log.debug("ディレクトリの作成完了: ");
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005181S, e);
		} finally {

		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#removeDirectory(java.lang.
	 * String)
	 */
	public boolean removeDirectory(String path) throws Exception {
		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			// 削除対象のディレクトリ有無をチェック
			ObjInfo objInfo = this.getAttribute(path);
			if (objInfo.isAbsent()) {
				throw new SftpIoException(SmMessageId.SM004185F, path);
			}
			// ディレクトリ削除
			channelSftp.rmdir(path);
			log.debug("ディレクトリの削除完了: ");
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005182S, e);
		} finally {

		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getList(java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	public List<ObjInfo> getList(String path) throws Exception {

		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			log.debug("ファイル一覧取得開始: ");
			// ファイル一覧を取得
			Vector<LsEntry> list = channelSftp.ls(path);
			if (null == list) {
				throw new SftpIoException(SmMessageId.SM004044F, path);
			}

			List<ObjInfo> newList = new ArrayList<ObjInfo>();
			for (LsEntry lsEntry : list) {// リストに".."と"."が含まれるため、除外する
				if (true != FILENAME_DIR_CURRENT.equals(lsEntry.getFilename()) && true != FILENAME_DIR_PARENT.equals(lsEntry.getFilename())) {

					ObjInfo objInfo = this.convSftpATTRS2ObjInfo(lsEntry.getFilename(), lsEntry.getAttrs());
					newList.add(objInfo);
					log.debug(lsEntry.toString());
				}
			}
			log.debug("ファイル一覧取得完了: ");
			return newList;
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005160S, e);
		} finally {

		}
	}

	/**
	 * pathファイルまたはディレクトリが存在するかチェックする<br>
	 *
	 * @param path ＦＴＰサーバ側のパス
	 * @return ObjInfoオブジェクト。ファイルが無い場合、ObjInfoのisAbsentプロパティにtrueを設定して返す。
	 * @throws Exception 指定されたパスが有効でない。
	 * @see jp.co.blueship.tri.fw.cmn.io.GenericIo#getAttribute(java.lang.String)
	 */
	@Override
	public ObjInfo getAttribute(String path) throws Exception {

		FilePathDelimiter filePath = new FilePathDelimiter(path);
		if (true != filePath.isEnabled()) {
			throw new SftpIoException(SmMessageId.SM004187F);
		}

		try {
			SftpATTRS sftpAttrs = channelSftp.lstat(path);
			if (null == sftpAttrs) {
				throw new SftpIoException(SmMessageId.SM004199F, path);
			}
			ObjInfo objInfo = this.convSftpATTRS2ObjInfo(filePath.getObjName(), sftpAttrs);
			log.debug("lstat " + sftpAttrs);
			return objInfo;
		} catch (Exception e) {
			ObjInfo objInfo = new ObjInfo();
			objInfo.setAbsent(true);
			return objInfo;// ファイルがない場合、Exceptionが発生するので止めて、absentをtrueにしてObjInfoを返す
		} finally {

		}
	}

	/**
	 * SftpATTRSオブジェクトの項目をObjInfoに移す<br>
	 *
	 * @param name ファイル名
	 * @param attrs SftpATTRSオブジェクト
	 * @return ObjInfoオブジェクト
	 * @throws Exception
	 */
	private ObjInfo convSftpATTRS2ObjInfo(String name, SftpATTRS attrs) {

		ObjInfo objInfo = new ObjInfo();
		objInfo.setName(name);
		objInfo.setSize(attrs.getSize());

		if (attrs.isLink()) {
			objInfo.setType(ObjType.TYPE_SYMBOLIC_LINK);
		} else if (true == attrs.isDir()) {
			objInfo.setType(ObjType.TYPE_DIRECTORY);
		} else if (true != attrs.isDir()) {
			objInfo.setType(ObjType.TYPE_FILE);
		} else {
			objInfo.setType(ObjType.TYPE_UNKNOWN);
		}

		java.util.Date updateDate = new java.util.Date(((long) attrs.getATime()) * 1000);// msec→sec単位変更
		objInfo.setUpdateDate(updateDate);
		java.util.Date createDate = new java.util.Date(((long) attrs.getMTime()) * 1000);// msec→sec単位変更
		objInfo.setCreateDate(createDate);
		// objInfo.setGroup( attrs.getGId() ) ;
		// objInfo.setUser( attrs.getUId() ) ;
		return objInfo;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#isExist(java.lang.String)
	 */
	public boolean isExist(String path) throws Exception {
		ObjInfo objInfo = this.getAttribute(path);
		if (objInfo.isPresent()) {
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#isFile(java.lang.String)
	 */
	public boolean isFile(String path) throws Exception {
		ObjInfo objInfo = this.getAttribute(path);
		if (objInfo.isPresent()) {
			if (true != objInfo.isDirectory()) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#isDirectory(java.lang.String)
	 */
	public boolean isDirectory(String path) throws Exception {
		ObjInfo objInfo = this.getAttribute(path);
		if (objInfo.isPresent()) {
			if (true == objInfo.isDirectory()) {
				return true;
			}
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#putFile(java.lang.String,
	 * java.lang.String)
	 */
	public boolean putFile(String localPath, String srvPath) throws Exception {
		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}
		InputStream is = null;
		try {
			is = new FileInputStream(localPath);
			// 既存ファイルの存在有無をチェック
			ObjInfo objInfo = this.getAttribute(srvPath);
			if (objInfo.isPresent()) {// ファイルまたはディレクトリが存在する
				if (true == objInfo.isDirectory()) {// ディレクトリが存在する
					TriFileUtils.close(is);
					throw new SftpIoException(SmMessageId.SM004188F, srvPath);
				} else {// ファイルが存在する
					if (FileCopyMode.OVERWRITE_NONE == this.copyMode || FileCopyMode.OVERWRITE_ABORT == this.copyMode) {
						TriFileUtils.close(is);
						return false;
					}
				}
			}
			channelSftp.put(is, srvPath);
			log.debug("ファイル送信完了: ");
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005162S, e);
		} finally {

		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getFile(java.lang.String,
	 * java.lang.String)
	 */
	public boolean getFile(String localPath, String srvPath) throws Exception {
		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}
		// ローカルの既存ファイル有無をチェック
		File file = new File(localPath);
		if (true == file.isDirectory()) {
			throw new SftpIoException(SmMessageId.SM004190F, localPath);
		}
		if (true == file.isFile()) {
			if (FileCopyMode.OVERWRITE_NONE == this.copyMode || FileCopyMode.OVERWRITE_ABORT == this.copyMode) {
				return false;
			}
		}

		OutputStream os = null;
		try {
			// 取得対象のファイル有無をチェック
			ObjInfo objInfo = this.getAttribute(srvPath);
			if (objInfo.isAbsent()) {
				throw new SftpIoException(SmMessageId.SM004191F, srvPath);
			}
			if (true == objInfo.isDirectory()) {
				throw new SftpIoException(SmMessageId.SM004192F, srvPath);
			}

			// ローカルに取得
			os = new FileOutputStream(localPath);
			channelSftp.get(srvPath, os);
			log.debug("ファイル転送完了: ");
		} catch (Exception e) {
			file.delete();
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005163S, e);
		} finally {

		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#removeFile(java.lang.String)
	 */
	public boolean removeFile(String path) throws Exception {
		if (null == this.channelSftp || true != this.channelSftp.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			// 削除対象のファイル有無をチェック
			ObjInfo objInfo = this.getAttribute(path);
			if (objInfo.isAbsent()) {
				throw new SftpIoException(SmMessageId.SM004191F, path);
			}
			if (true == objInfo.isDirectory()) {
				throw new SftpIoException(SmMessageId.SM004194F, path);
			}
			// ローカルに取得
			channelSftp.rm(path);
			log.debug("ファイル削除完了: ");
		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new SftpIoException(SmMessageId.SM005164S, e);
		}
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * jp.co.blueship.trinity.common.io.GenericIo#changeDirectory(java.lang.
	 * String)
	 */
	public void changeDirectory(String path) throws Exception {
		if (null == this.session || true != this.session.isConnected()) {
			throw new SftpIoException(SmMessageId.SM005173S);
		}

		try {
			// ディレクトリ移動
			channelSftp.cd(path);
		} catch (Exception e) {
			this.channelSftp.disconnect();
			this.latestEndStatus = String.valueOf(this.channelSftp.getExitStatus());
			throw new SftpIoException(SmMessageId.SM005175S, e);
		} finally {

		}
	}
}
