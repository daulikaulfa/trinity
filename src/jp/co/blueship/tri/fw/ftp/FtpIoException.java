package jp.co.blueship.tri.fw.ftp;

import jp.co.blueship.tri.fw.ex.TriException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 「Ｆｔｐ接続」関連操作クラスに関する例外クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FtpIoException extends TriException {

	/**
	 *
	 */
	private static final long serialVersionUID = 4614744147939543614L;

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public FtpIoException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public FtpIoException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public FtpIoException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public FtpIoException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, messageArgs, cause);
	}

}
