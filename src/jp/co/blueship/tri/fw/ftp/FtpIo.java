package jp.co.blueship.tri.fw.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Vector;

import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.parser.DefaultFTPFileEntryParserFactory;

import jp.co.blueship.tri.fw.cmn.io.FilePathDelimiter;
import jp.co.blueship.tri.fw.cmn.io.GenericIo;
import jp.co.blueship.tri.fw.cmn.io.ObjInfo;
import jp.co.blueship.tri.fw.cmn.io.constants.FileCopyMode;
import jp.co.blueship.tri.fw.cmn.io.constants.FileIoType;
import jp.co.blueship.tri.fw.cmn.io.constants.ObjType;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
/**
 * 「ＦＴＰ接続」関連操作クラスに関する実装クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FtpIo implements GenericIo {

	private static final ILog log = TriLogFactory.getInstance() ;

	private FTPClient ftpClient = null ;		//
	private Integer fileType = null ;			//ファイル転送形式（テキスト／バイナリ）
	private Integer fileTypeLatest = null ;	//ファイル転送形式（テキスト／バイナリ）

	private FileCopyMode copyMode = FileCopyMode.OVERWRITE_ABORT ;		//ファイルコピーモード種別
	private FtpParamBean ftpParam = null ; 	//ＦＴＰ接続パラメータ

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();

	/**
	 * コンストラクタ
	 * @param ftpParam ｆｔｐ接続に用いる設定パラメータ群
	 */
	public FtpIo( FtpParamBean ftpParam ) {
		this.ftpParam = ftpParam ;
	}

	/**
	 * デフォルトのParserFactory
	 */
	static class PrivateFTPFileEntryParserFactory extends DefaultFTPFileEntryParserFactory {

	}

	/**
	 * ＦＴＰ転送時のファイル転送モードをセットする
	 *
	 * @param type ファイル転送モード
	 */
	public void setFileType( FileIoType type ) {
		this.fileType = type.intValue();
	}
	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#setCopyMode(jp.co.blueship.trinity.common.io.define.FileCopyMode)
	 */
	public void setCopyMode(FileCopyMode copyMode) {
		this.copyMode = copyMode;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getLatestEndStatus()
	 */
	public String getLatestEndStatus() {
		if( null == ftpClient ) {
			return null ;
		}
		return ftpClient.getReplyCode() + " , " + ftpClient.getReplyString() ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#openConnection()
	 */
	public boolean openConnection() throws FtpIoException {
		if( null == ftpClient ) {
			try {
				log.debug( "ログイン前: " ) ;

				ftpClient = new FTPClient() ;
				//接続前パラメータのセット
				if( null != this.ftpParam.getPort() ) {//ポート番号
					ftpClient.setDefaultPort( this.ftpParam.getPort() ) ;
				}
				if( null != this.ftpParam.getDefaultTimeout() ) {//接続要求タイムアウト
					ftpClient.setDefaultTimeout( this.ftpParam.getDefaultTimeout() ) ;
				}
				if( null != this.ftpParam.getControlEncoding() ) {//文字コード
					ftpClient.setControlEncoding( this.ftpParam.getControlEncoding().toString() ) ;
				}
				//接続要求
				ftpClient.connect( this.ftpParam.getHost() ) ;
				if( true != ftpClient.login( this.ftpParam.getUser() , this.ftpParam.getPass() ) ) {
					return false ;
				}
				//接続後パラメータのセット
				if( null != this.ftpParam.getSoTimeout() ) {//接続後各種処理のタイムアウト
					ftpClient.setSoTimeout( this.ftpParam.getSoTimeout() ) ;
				}
				if( true == this.ftpParam.isPassiveMode() ) {//PASVモード
					ftpClient.enterLocalPassiveMode() ;
				}
				ftpClient.setParserFactory( new PrivateFTPFileEntryParserFactory() ) ;

				log.debug( "ログイン完了: " ) ;
			} catch ( Exception e ) {
				throw new FtpIoException ( SmMessageId.SM005156S , e ) ;
			}
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#closeConnection()
	 */
	public void closeConnection() throws Exception {
		if( null != ftpClient ) {
			if( true == ftpClient.isConnected() ) {
				ftpClient.disconnect() ;
			}
		}
		log.debug( "ログアウト完了: " ) ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#putObject(java.lang.String, java.lang.String)
	 */
	public boolean putObject( String localPath , String srvPath ) throws Exception {

		File file = new File( localPath ) ;
		if( true != file.exists() ) {
			throw new FileNotFoundException( ac.getMessage(SmMessageId.SM004177F , localPath) ) ;
		}
		return this.putObjectReentrant( localPath , srvPath ) ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#putObject(java.lang.String, java.lang.String)
	 */
	private boolean putObjectReentrant( String localPath , String srvPath ) throws Exception {
		boolean ret = true ;
		FilePathDelimiter filePath = new FilePathDelimiter( localPath ) ;

		File file = new File( localPath ) ;
		if( true != file.exists() ) {
			throw new FtpIoException( SmMessageId.SM004177F , localPath) ;
		}

		if( file.isDirectory() ) {//ディレクトリ
			log.debug( "ディレクトリ：" + localPath ) ;
			String newDir =  TriStringUtils.linkPathBySlash( srvPath , file.getName() ) ;
			this.makeDirectory( newDir ) ;

			File[] files = file.listFiles() ;
			if( null == files ) {
				throw new FtpIoException( SmMessageId.SM004044F , localPath) ;
			}
			for( File tmpFile : files ) {
				String path =  TriStringUtils.linkPathBySlash( localPath , tmpFile.getName() ) ;
				ret = putObjectReentrant( path , newDir ) ;
				if( false == ret ) {
					return false ;
				}
			}
		} else {//ファイル
			log.debug( "ファイル  ：" + file.getName() ) ;
			String newPath = TriStringUtils.linkPathBySlash( srvPath , filePath.getObjName() ) ;
			if( true != this.isDirectory( srvPath ) ) {
				throw new FtpIoException( SmMessageId.SM004178F , srvPath) ;
			}

			boolean sts = this.putFile( localPath , newPath ) ;
			if( FileCopyMode.OVERWRITE_ABORT == this.copyMode && true != sts	) {
				return false ;
			}
		}
		return ret ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getObject(java.lang.String, java.lang.String)
	 */
	public boolean getObject( String localPath , String srvPath ) throws Exception {

		ObjInfo objInfo = this.getAttribute( srvPath ) ;
		if( null == objInfo ) {
			throw new FileNotFoundException( ac.getMessage(SmMessageId.SM004179F , srvPath) ) ;
		}
		return this.getObjectReentrant( localPath , srvPath ) ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getObject(java.lang.String, java.lang.String)
	 */
	private boolean getObjectReentrant( String localPath , String srvPath ) throws Exception {
		boolean ret = true ;
		FilePathDelimiter filePath = new FilePathDelimiter( srvPath ) ;

		ObjInfo objInfo = this.getAttribute( srvPath ) ;
		if( null == objInfo ) {
			throw new FtpIoException( SmMessageId.SM004179F , srvPath) ;
		}

		if( objInfo.isDirectory() ) {//ディレクトリ
			log.debug( "ディレクトリ：" + srvPath ) ;
			String newDir = TriStringUtils.linkPathBySlash( localPath , objInfo.getName() ) ;
			File file = new File( newDir ) ;
			file.mkdirs() ;

			List<ObjInfo> list = this.getList( srvPath ) ;
			if( null == list ) {
				throw new FtpIoException( SmMessageId.SM004044F , srvPath) ;
			}
			for( ObjInfo info : list ) {
				String path = TriStringUtils.linkPathBySlash( srvPath , info.getName() ) ;
				ret = getObjectReentrant( newDir , path ) ;
				if( false == ret ) {
					return false ;
				}
			}
		} else {//ファイル
			log.debug( "ファイル  ：" + objInfo.getName() ) ;
			String newPath = TriStringUtils.linkPathBySlash( localPath , filePath.getObjName() ) ;
			if( true != new File( localPath ).isDirectory() ) {
				throw new FtpIoException( SmMessageId.SM004180F , localPath) ;
			}

			boolean sts = this.getFile( newPath , srvPath ) ;
			if( FileCopyMode.OVERWRITE_ABORT == this.copyMode && true != sts ) {
				return false ;
			}
		}
		return ret ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#deleteObject(java.lang.String)
	 */
	public void deleteObject( String srvPath ) throws Exception {

		ObjInfo objInfo = this.getAttribute( srvPath ) ;
		if( null == objInfo ) {
			throw new FileNotFoundException( ac.getMessage( SmMessageId.SM004181F , srvPath ) ) ;
		}
		this.deleteObjectReentrant( srvPath ) ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#deleteObject(java.lang.String)
	 */
	public void deleteObjectReentrant( String srvPath ) throws Exception {
		ObjInfo objInfo = this.getAttribute( srvPath ) ;
		if( null == objInfo ) {
			throw new FtpIoException( SmMessageId.SM004181F , srvPath) ;
		}

		if( objInfo.isDirectory() ) {//ディレクトリ
			log.debug( "ディレクトリ：" + srvPath ) ;
			List<ObjInfo> list = this.getList( srvPath ) ;
			if( null == list ) {
				throw new FtpIoException( SmMessageId.SM004044F , srvPath ) ;
			}
			for( ObjInfo info : list ) {
				deleteObjectReentrant( TriStringUtils.linkPathBySlash( srvPath , info.getName() ) ) ;
			}
			this.removeDirectory( srvPath ) ;
		} else {//ファイル
			log.debug( "ファイル  ：" + objInfo.getName() ) ;
			this.removeFile( srvPath ) ;
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#renameObject(java.lang.String, java.lang.String)
	 */
	public void renameObject( String path , String newPath ) throws Exception {
		if( null == ftpClient || true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		if( true != ftpClient.rename( path , newPath ) ) {
			throw new FtpIoException( SmMessageId.SM004182F , path , newPath ) ;
		}
	}


	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#makeDirectory(java.lang.String)
	 */
	public boolean makeDirectory( String path ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		try {
			//作成対象のディレクトリ有無をチェック
			ObjInfo objInfo = this.getAttribute( path ) ;
			if( null != objInfo ) {
				if( true == objInfo.isFile() ) {
					throw new FtpIoException( SmMessageId.SM004183F , path ) ;
				} else {
					return false ;
				}
			}
			//ディレクトリ作成
			boolean ret = ftpClient.makeDirectory( path ) ;
			if( true != ret ) {
				throw new FtpIoException( SmMessageId.SM004184F , path ) ;
			}
			log.debug( "ディレクトリの作成完了: " ) ;
		} catch ( Exception e ) {
			throw new FtpIoException( SmMessageId.SM005158S , e ) ;
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#removeDirectory(java.lang.String)
	 */
	public boolean removeDirectory( String path ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}

		try {
			//削除対象のディレクトリ有無をチェック
			ObjInfo objInfo = this.getAttribute( path ) ;
			if( null == objInfo ) {
				throw new FtpIoException( SmMessageId.SM004185F , path ) ;
			}
			//ディレクトリ削除
			boolean ret = ftpClient.removeDirectory( path ) ;
			if( true != ret ) {
				throw new FtpIoException( SmMessageId.SM004186F , path ) ;
			}
			log.debug( "ディレクトリの削除完了: " ) ;
		} catch ( Exception e ) {
			throw new FtpIoException( SmMessageId.SM005159S, e ) ;
		} finally {

		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getList(java.lang.String)
	 */
	public List<ObjInfo> getList( String path ) throws Exception {

		if( null == ftpClient || true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		try {
			log.debug( "ファイル一覧取得開始: " ) ;
//			ファイル一覧を取得
			List<ObjInfo> list = new Vector<ObjInfo>() ;
			FTPFile[] ftpfiles = ftpClient.listFiles( path ) ;
			if( null != ftpfiles ) {
				for( FTPFile ftpFile : ftpfiles ) {
					ObjInfo objInfo = convFTPFile2ObjInfo( ftpFile ) ;
					list.add( objInfo ) ;
				}
			}

			log.debug( "ファイル一覧取得完了: " ) ;
			return list ;
		} catch( Exception e ) {
			throw new FtpIoException( SmMessageId.SM005160S , e) ;
		} finally {

		}
	}
	/**
	 * FTPFileオブジェクトの項目をObjInfoに移す<br>
	 * @param ftpFile FTPFileオブジェクト
	 * @return ObjInfoオブジェクト
	 * @throws Exception
	 */
	private ObjInfo convFTPFile2ObjInfo( FTPFile ftpFile ) throws Exception {
		if( null == ftpFile ) {
			return null ;
		}
		ObjInfo objInfo = new ObjInfo() ;
		objInfo.setName( ftpFile.getName() ) ;
		if( FTPFile.DIRECTORY_TYPE == ftpFile.getType() ) {

 			objInfo.setType( ObjType.TYPE_DIRECTORY ) ;
		} else if( FTPFile.FILE_TYPE == ftpFile.getType() ) {
			objInfo.setType( ObjType.TYPE_FILE ) ;
		} else if( FTPFile.SYMBOLIC_LINK_TYPE == ftpFile.getType() ) {
			objInfo.setType( ObjType.TYPE_SYMBOLIC_LINK ) ;
		} else {
			objInfo.setType( ObjType.TYPE_UNKNOWN ) ;
		}
		objInfo.setUpdateDate( ftpFile.getTimestamp().getTime() ) ;
		objInfo.setSize( ftpFile.getSize() ) ;
		//objInfo.setUser( ftpFile.getUser() ) ;
		//objInfo.setGroup( ftpFile.getGroup() ) ;
		return objInfo ;
	}


	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getAttribute(java.lang.String)
	 */
	public ObjInfo getAttribute( String path ) throws Exception {
		FilePathDelimiter filePath = new FilePathDelimiter( path ) ;
		if( true != filePath.isEnabled() ) {
			throw new FtpIoException( SmMessageId.SM004187F ) ;
		}
		if( null == ftpClient || true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		try {
//			ファイル一覧を取得
			List<ObjInfo> list = this.getList( filePath.getParentDir() ) ;
			if( null == list ) {
				return null ;
			}
			for( ObjInfo info : list ) {
				if( filePath.getObjName().equals( info.getName() ) ) {
					return info ;
				} else if( ( path ).equals( info.getName() ) ) {//ファイル名がフルパスの場合
					return info ;
				}
			}
			return null ;//該当なし
		} catch( Exception e ) {
			throw new FtpIoException( SmMessageId.SM005161S , e ) ;
		}
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#isExist(java.lang.String)
	 */
	public boolean isExist( String path ) throws Exception {
		ObjInfo objInfo = this.getAttribute( path ) ;
		if( null != objInfo ) {
			return true ;
		}
		return false ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#isFile(java.lang.String)
	 */
	public boolean isFile( String path ) throws Exception {
		ObjInfo objInfo = this.getAttribute( path ) ;
		if( null != objInfo ) {
			if( ObjType.TYPE_FILE == objInfo.getType() ) {
				return true ;
			}
		}
		return false ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#isDirectory(java.lang.String)
	 */
	public boolean isDirectory( String path ) throws Exception {
		ObjInfo objInfo = this.getAttribute( path ) ;
		if( null != objInfo ) {
			if( ObjType.TYPE_DIRECTORY == objInfo.getType() ) {
				return true ;
			}
		}
		return false ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#putFile(java.lang.String, java.lang.String)
	 */
	public boolean putFile( String localPath , String SrvPath ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		InputStream is = null ;
		boolean ret ;
		try {
			is = new FileInputStream( localPath ) ;
			//既存ファイルの存在有無をチェック
			ObjInfo objInfo = this.getAttribute( SrvPath ) ;
			if( null != objInfo ) {//ファイルまたはディレクトリが存在する
				if( ObjType.TYPE_DIRECTORY == objInfo.getType() ) {//ディレクトリが存在する
					throw new FtpIoException( SmMessageId.SM004188F , SrvPath ) ;
				} else {//ファイルが存在する
					if( FileCopyMode.OVERWRITE_NONE == this.copyMode ||
							FileCopyMode.OVERWRITE_ABORT == this.copyMode ) {
						return false ;
					}
				}
			}
			//転送モードの設定
			this.setFileType() ;
			//ファイルの転送
			ret = ftpClient.storeFile( SrvPath , is ) ;
			if( true != ret ) {
				throw new FtpIoException( SmMessageId.SM004189F , localPath ) ;
			}
			log.debug( "ファイル送信完了: " ) ;
		} catch ( Exception e ) {
			throw new FtpIoException ( SmMessageId.SM005162S , e ) ;
		} finally {
			if( null != is ) {
				is.close() ;
			}
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#getFile(java.lang.String, java.lang.String)
	 */
	public boolean getFile( String localPath , String srvPath ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
//		ローカルの既存ファイル有無をチェック
		File file = new File( localPath ) ;
		if( true == file.isDirectory() ) {
			throw new FtpIoException ( SmMessageId.SM004190F , localPath ) ;
		}
		if( true == file.isFile() ) {
			if( FileCopyMode.OVERWRITE_NONE == this.copyMode ||
					FileCopyMode.OVERWRITE_ABORT == this.copyMode ) {
				return false ;
			}
		}

		OutputStream os = null ;
		try {
			//取得対象のファイル有無をチェック
			ObjInfo objInfo = this.getAttribute( srvPath ) ;
			if( null == objInfo ) {
				throw new FtpIoException ( SmMessageId.SM004191F , srvPath ) ;
			}
			if( true != objInfo.isFile() ) {
				throw new FtpIoException ( SmMessageId.SM004192F , srvPath ) ;
			}
			//転送モードの設定
			this.setFileType() ;
			//ローカルに取得
			os = new FileOutputStream( localPath ) ;
			boolean ret = ftpClient.retrieveFile( srvPath , os ) ;
			if( true != ret ) {
				throw new FtpIoException ( SmMessageId.SM004193F , srvPath ) ;
			}
			log.debug( "ファイル転送完了: " ) ;
		} catch ( Exception e ) {
			throw new FtpIoException ( SmMessageId.SM005163S , e) ;
		} finally {
			if( null != os ) {
				os.close() ;
			}
		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#removeFile(java.lang.String)
	 */
	public boolean removeFile( String path ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}

		try {
			//削除対象のファイル有無をチェック
			ObjInfo objInfo = this.getAttribute( path ) ;
			if( null == objInfo ) {
				throw new FtpIoException ( SmMessageId.SM004191F , path ) ;
			}
			if( true != objInfo.isFile() ) {
				throw new FtpIoException ( SmMessageId.SM004194F , path ) ;
			}

			//ローカルに取得
			boolean ret = ftpClient.deleteFile( path ) ;
			if( true != ret ) {
				throw new FtpIoException ( SmMessageId.SM004195F , path ) ;
			}
			log.debug( "ファイル削除完了: " ) ;
		} catch ( Exception e ) {
			throw new FtpIoException( SmMessageId.SM005164S , e ) ;
		} finally {

		}
		return true ;
	}

	/* (non-Javadoc)
	 * @see jp.co.blueship.trinity.common.io.GenericIo#changeDirectory(java.lang.String)
	 */
	public void changeDirectory( String path ) throws Exception {
		if( null == ftpClient ||
			true != ftpClient.isConnected() ) {
			throw new FtpIoException( SmMessageId.SM005157S ) ;
		}
		if( true != ftpClient.changeWorkingDirectory( path ) ) {//ディレクトリ移動
			throw new FtpIoException( SmMessageId.SM004196F , path ) ;
		}
	}

	/**
	 * 「バイナリ／テキスト」転送モードを変更する<br>
	 * 頻繁な変更を避けるため、前回とモードが同じ場合には変更を行わない<br>
	 * @throws Exception
	 */
	private final void setFileType() throws Exception {
		if( null != this.fileType ) {
			if( null == this.fileTypeLatest ||
				this.fileTypeLatest.intValue() != this.fileType.intValue() ) {
//				転送モードの設定
				if( true != ftpClient.setFileType( this.fileType ) ) {
					throw new FtpIoException( SmMessageId.SM004197F ) ;
				}
				log.debug( "転送モードを変更" + fileTypeLatest + " -> " + fileType ) ;
			}
			this.fileTypeLatest = new Integer( this.fileType.intValue() ) ;
		}
	}
}
