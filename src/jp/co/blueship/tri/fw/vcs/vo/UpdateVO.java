package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import org.hibernate.validator.constraints.NotBlank;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;

/**
 * Update時のValueObjectクラスです<br>
 * 対応メモ:
 * update
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 *	pathDest		ローカル資産のフルパス
 *	moduleName		モジュール名
 * </pre>
 */
public class UpdateVO {

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME})
	private String moduleName;

	public String getPathDest(){
		return pathDest;
	}
	public String getModuleName(){
		return moduleName;
	}

	public void setPathDest(String path){
		pathDest = path;
	}
	public void setModuleName(String module){
		moduleName = module;
	}

}