package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import org.hibernate.validator.constraints.NotBlank;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;



/**
 * Subディレクトリ取得時のValuObjectクラスです。<br>
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 *	repository				リポジトリのＵＲＬ
 *	versionTag				バージョンタグ
 * </pre>
 */
public class ListSubDirVO {

	public interface ListChecks{}
	public interface ChkTagChecks{}

	@NotBlank(groups = {ListChecks.class,ChkTagChecks.class})
	@TriMessage(smMessageID=SM004096F, args={REPOSITORY})
	private String repository;

	@NotBlank(groups = ChkTagChecks.class)
	@TriMessage(smMessageID=SM004096F, args={VERSION_TAG})
	private String versionTag;

	public String getRepository(){
		return repository;
	}
	public String getVersionTag(){
		return versionTag;
	}

	public void setRepository(String param){
		repository = param;
	}
	public void setVersionTag(String tag){
		versionTag = tag;
	}

}