package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import org.hibernate.validator.constraints.NotBlank;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;


/**
 * ステータス取得時のValueObjectクラスです<br>
 * 対応メモ:
 * statusModule(未参照)
 * statusAll
 * statusSingle
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 *	pathDest				ローカル資産のフルパス
 *	moduleName				モジュール名
 *	objectPath				モジュール名を含み、それ以下のパス
 * </pre>
 */

public class StatusVO {

	public interface SingleChecks{}
	public interface AllChecks{}
	public interface StatusAll{}

	@NotBlank(groups = {SingleChecks.class, AllChecks.class, StatusAll.class})
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	@NotBlank(groups = StatusAll.class)
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME})
	private String moduleName;

	@NotBlank(groups = {SingleChecks.class, AllChecks.class})
	@TriMessage(smMessageID=SM004096F, args={OBJECT_PATH})
	private String objectPath;

	public String getPathDest(){
		return pathDest;
	}
	public String getModuleName(){
		return moduleName;
	}
	public String getObjectPath(){
		return objectPath;
	}

	public void setPathDest(String path){
		pathDest = path;
	}
	public void setModuleName(String module){
		moduleName = module;
	}
	public void setObjectPath(String object){
		objectPath = object;
	}

}