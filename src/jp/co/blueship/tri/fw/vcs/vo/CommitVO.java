package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;
import jp.co.blueship.tri.fw.msg.MessageParameter;

/**
 * コミット時のValueObjectクラスです<br>
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 * 	repository		リポジトリのＵＲＬ
 *	labelBranch		ブランチ名
 *	commitComment	コミットコメント
 *	labelTag		タグ名
 *	pathDest		ローカル資産のパス
 *	moduleNameArray	モジュール名の配列
 * </pre>
 */

public class CommitVO{

	public interface BranchChecks{}
	public interface TagToBChecks{}
	public interface TagToTChecks{}
	public interface CommitChecks{}

	@NotBlank(groups = {BranchChecks.class,TagToBChecks.class,TagToTChecks.class})
	@TriMessage(smMessageID=SM004096F, args={REPOSITORY})
	private String repository;

	@NotBlank(groups = {BranchChecks.class,TagToBChecks.class})
	@TriMessage(smMessageID=SM004096F, args={LABEL_BRANCH})
	private String labelBranch;

	private String commitComment;

	@NotBlank(groups = {TagToBChecks.class,TagToTChecks.class})
	@TriMessage(smMessageID=SM004096F, args={MessageParameter.LABEL_TAG})
	private String labelTag;

	@NotBlank(groups = CommitChecks.class)
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	@NotEmpty(groups = CommitChecks.class)
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME_ARRAY})
	private String[] moduleNameArray;

	public boolean isAllModuleNameNotEmpty(){
		if (TriStringUtils.isEmpty(moduleNameArray)){
			return true;
		}
		for ( String module : moduleNameArray ){
			if (TriStringUtils.isEmpty(module)){
				return false;
			}
		}
		return true;
	}
	public String getRepository(){
		return repository;
	}
	public String getLabelBranch(){
		return labelBranch;
	}
	public String getCommitComment(){
		return commitComment;
	}
	public String getLabelTag(){
		return labelTag;
	}
	public String getPathDest(){
		return pathDest;
	}
	public String[] getModuleNameArray(){
		return moduleNameArray;
	}

	public void setRepository(String param){
		repository = param;
	}
	public void setLabelBranch(String branch){
		labelBranch = branch;
	}
	public void setCommitComment(String comment){
		commitComment = TriStringUtils.defaultIfEmpty(comment, "");
	}
	public void setLabelTag(String tag){
		labelTag = tag;
	}
	public void setPathDest(String path){
		pathDest = path;
	}
	public void setModuleNameArray(String[] moduleName){
		moduleNameArray = moduleName;
	}
}