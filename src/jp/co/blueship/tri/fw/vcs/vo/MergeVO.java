package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;
import org.tmatesoft.svn.core.wc.SVNRevision;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;




/**
 * マージ時のValueObjectクラスです<br>
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 *	repository		リポジトリのパス情報
 *	labelBranch		ブランチ名
 *	moduleName		モジュール名
 *	mergeFrom
 *	mergeTo
 *	pathDest		取得先パス
 * </pre>
 */
public class MergeVO {

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={REPOSITORY})
	private String repository;

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={LABEL_BRANCH})
	private String labelBranch;

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME})
	private String moduleName;

	@NotNull
	@TriMessage(smMessageID=SM004096F, args={MERGE_FROM})
	private SVNRevision mergeFrom;

	@NotNull
	@TriMessage(smMessageID=SM004096F, args={MERGE_TO})
	private SVNRevision mergeTo;

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	public String getRepository(){
		return repository;
	}
	public String getLabelBranch(){
		return labelBranch;
	}
	public String getModuleName(){
		return moduleName;
	}
	public SVNRevision getMergeFrom(){
		return mergeFrom;
	}
	public SVNRevision getMergeTo(){
		return mergeTo;
	}
	public String getPathDest(){
		return pathDest;
	}

	public void setRepository(String param){
		repository = param;
	}
	public void setLabelBranch(String branch){
		labelBranch = branch;
	}
	public void setModuleName(String module){
		moduleName = module;
	}
	public void setMergeFrom(SVNRevision from){
		mergeFrom = from;
	}
	public void setMergeTo(SVNRevision to){
		mergeTo = to;
	}
	public void setPathDest(String path){
		pathDest = path;
	}

}