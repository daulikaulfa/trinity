package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;
import jp.co.blueship.tri.fw.msg.MessageParameter;


/**
 * チェックアウト時のValueObjectクラスです。<br>
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 * 	repository		リポジトリのＵＲＬ
 *	moduleName		モジュール名
 *	pathDest		チェックアウト先フルパス
 *	labelBranch		ブランチ名
 *	revisionNo		取得するリビジョン番号(Long型)
 * </pre>
 */

public class CheckOutVO {

	public interface ChkHeadChecks{}
	public interface ChkBranchChecks{}
	public interface RevisionChecks{}

	@NotBlank(groups = {ChkHeadChecks.class,ChkBranchChecks.class,RevisionChecks.class})
	@TriMessage(smMessageID=SM004096F, args={REPOSITORY})
	private String repository;

	@NotBlank(groups = {ChkHeadChecks.class,ChkBranchChecks.class,RevisionChecks.class})
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME})
	private String moduleName;

	@NotBlank(groups = {ChkHeadChecks.class,ChkBranchChecks.class,RevisionChecks.class})
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	@NotBlank(groups = {ChkBranchChecks.class,RevisionChecks.class})
	@TriMessage(smMessageID=SM004096F, args={LABEL_BRANCH})
	private String labelBranch;

	@NotNull(groups = RevisionChecks.class)
	@Min(value = 1L, groups = RevisionChecks.class)
	@TriMessage(smMessageID=SM004097F, args={MessageParameter.REVISION_NO})
	private Long revisionNo;

	public String getRepository(){
		return repository;
	}
	public String getModuleName(){
		return moduleName;
	}
	public String getPathDest(){
		return pathDest;
	}
	public String getLabelBranch(){
		return labelBranch;
	}
	public Long getRevisionNo(){
		return revisionNo;
	}

	public void setRepository(String param){
		repository = param;
	}
	public void setModuleName(String module){
		moduleName = module;
	}
	public void setPathDest(String path){
		pathDest = path;
	}
	public void setLabelBranch(String branch){
		labelBranch = branch;
	}
	public void setRevisionNo(Long rev){
		revisionNo = rev;
	}

}