package jp.co.blueship.tri.fw.vcs.vo;

import static jp.co.blueship.tri.fw.msg.MessageParameter.*;
import static jp.co.blueship.tri.fw.msg.SmMessageId.*;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;

/**
 * delete時のValueObjectです<br>
 * @version:V3L10
 * @author:Takashi Ono
 * @param
 * <pre>
 *	pathDest		ワーキングコピーのパス
 *	moduleName		モジュール名
 *	filePathArray	追加する資産パスの配列（フルパス）
 * </pre>
 */
public class DeleteVO {

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={PATH_DEST})
	private String pathDest;

	@NotBlank
	@TriMessage(smMessageID=SM004096F, args={MODULE_NAME})
	private String moduleName;

	@NotEmpty
	@TriMessage(smMessageID=SM004096F, args={PATH_DELETE_ASSETS})
	private String[] filePathArray;

	public boolean isAllFilePathNotEmpty(){
		if (TriStringUtils.isEmpty(filePathArray)){
			return true;
		}
		for ( String module : filePathArray ){
			if (TriStringUtils.isEmpty(module)){
				return false;
			}
		}
		return true;
	}

	public String getPathDest(){
		return pathDest;
	}
	public String getModuleName(){
		return moduleName;
	}
	public String[] getFilePathArray(){
		return filePathArray;
	}

	public void setPathDest(String path){
		pathDest = path;
	}
	public void setModuleName(String module){
		moduleName = module;
	}
	public void setFilePathArray(String... pathArray){
		filePathArray = pathArray;
	}
}