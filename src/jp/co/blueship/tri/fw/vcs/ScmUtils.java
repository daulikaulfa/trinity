package jp.co.blueship.tri.fw.vcs;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.fw.cmn.utils.DesignBusinessRuleUtils;

/**
 * 変更管理(SCM)を扱うユーティリティークラスです。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ScmUtils {

    /**
     *
     * @param absFile
     * @param checkInFile
     * @param list
     */
	public static void getNotExistParentFile( File absFile, File checkInFile, List<String> list ) {

		File parentFile = absFile.getParentFile();

		if ( !parentFile.exists() ) {
			list.add( 0, checkInFile.getParent() );
			getNotExistParentFile( parentFile, checkInFile.getParentFile(), list );
		}
	}

	/**
	 * SCMへの反映対象のファイルリストのうち、指定されたモジュール配下のファイルリストを抽出する。
	 * @param commitFileList SCMへの反映対象のファイルリスト
	 * @param mdlEntity 対象モジュール
	 * @return 指定されたモジュール配下のファイルリスト
	 */
	public static List<String> getFileListInModule(
			List<String> commitFileList, ILotMdlLnkEntity mdlEntity ) {

		List<String> extractFileModuleList = new ArrayList<String>();

		for( String commitPath : commitFileList ) {
			String newPath = DesignBusinessRuleUtils.convertPathInModule( commitPath, mdlEntity );

			if( null != newPath ) {
				extractFileModuleList.add( newPath ) ;
			}
		}

		return extractFileModuleList;
	}

	/**
	 * SCMへの反映対象のファイルリストのうち、指定されたモジュール配下のファイルリストを抽出する。
	 * @param commitFileList SCMへの反映対象のファイルリスト
	 * @param mdlEntity 対象モジュール
	 * @return 指定されたモジュール配下のファイルリスト
	 */
	public static List<String> getFileListInModule(
			List<String> commitFileList, IVcsMdlEntity mdlEntity ) {

		List<String> extractFileModuleList = new ArrayList<String>();

		for( String commitPath : commitFileList ) {
			String newPath = DesignBusinessRuleUtils.convertPathInModule( commitPath, mdlEntity );

			if( null != newPath ) {
				extractFileModuleList.add( newPath ) ;
			}
		}

		return extractFileModuleList;
	}

}
