package jp.co.blueship.tri.fw.vcs;

public class ScmParamFile {

	public static enum ObjType {
		binaryFile ,//バイナリファイル	：ADD時にはいずれかのセットが必須
		textFile ,	//テキストファイル	：ADD時にはいずれかのセットが必須
		directory ,	//ディレクトリ		：ADD時にはいずれかのセットが必須
		unknown ;	//不明 			：REMOVE時には種別を参照していないので、これをセットすればよい。他をセットしても構わないが
	}

	private String objName = null ;
	private ObjType objType = null ;

	public String getObjName() {
		return objName;
	}
	public void setObjName(String objName) {
		this.objName = objName;
	}
	public ObjType getObjType() {
		return objType;
	}
	public void setObjType(ObjType objType) {
		this.objType = objType;
	}



}

