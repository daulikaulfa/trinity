package jp.co.blueship.tri.fw.vcs;

import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.vcs.constants.VcsCategory;

/**
 * VCSアクセスサービスクラスのファクトリクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class VcsServiceFactory {

	public static final String VCS_SERVICE_FACTORY_NAME = "vcsServiceFactory";

	public IVcsService createService(VcsCategory type, String userName, String password) {

		if (VcsCategory.SVN.equals(type)) {

			return (IVcsService) Contexts.getInstance().getBeanFactory().getBean("svnService", userName, password);
		}

		throw new UnsupportedOperationException("Unsuppoted Scm is specified.");
	}

}
