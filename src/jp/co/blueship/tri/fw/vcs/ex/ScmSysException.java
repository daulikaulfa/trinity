/*
 * ScmSysException.java
 *
 * Created on 2006/10/11, 11:04
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jp.co.blueship.tri.fw.vcs.ex;

import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * システム例外をラッピングします。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ScmSysException extends TriRuntimeException {

    /**
	 *
	 */
	private static final long serialVersionUID = -4191532557412817994L;

	public ScmSysException( IMessageId id ){
		super(id);
	}

	public ScmSysException( IMessageId id , Throwable e ){
		super( id , e );
	}
	/**
     * 指定された詳細メッセージおよび原因を使用して新規例外を構築します。
     *
     * @param message メッセージ
     * @param cause 原因
     */
    @SuppressWarnings("deprecation")
	public ScmSysException( String message, Throwable cause ) {
        super(message, cause);
    }

}
