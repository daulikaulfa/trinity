/*
 * ScmException.java
 *
 * Created on 2006/10/11, 11:04
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

package jp.co.blueship.tri.fw.vcs.ex;

import jp.co.blueship.tri.fw.ex.TriException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * SCMアクセス処理におけるアプリケーション例外です。
 */
public class ScmException extends TriException {

	private static final long serialVersionUID = -4441236707333488030L;

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 */
	public ScmException(IMessageId message){
		super(message);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public ScmException(IMessageId message, String... messageArgs){
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param cause the root cause
	 */
	public ScmException(IMessageId message, Throwable cause){
		super(message, cause);
	}

	/**
	 * Constructor for Exception
	 * <br>
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public ScmException(IMessageId message, Throwable cause, String... messageArgs){
		super(message, messageArgs, cause);
	}

	/**
     * 指定された詳細メッセージおよび原因を使用して新規例外を構築します。
     *
     * @param message メッセージ
     * @param cause 原因
     */
	@Deprecated
	// メッセージIDを使用してください。
    public ScmException( String message, Throwable cause ) {
        super(message, cause);
    }

}
