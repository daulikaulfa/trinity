package jp.co.blueship.tri.fw.vcs;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.MergeVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;
import jp.co.blueship.tri.fw.vcs.vo.UpdateVO;


/**
 * VCSアクセスサービスを提供するクラスの共通インタフェースです。
 *
 *
 * @author Takayuki Kubo
 *
 */
public interface IVcsService {
	/**
	 * ＨＥＡＤの資産をチェックアウトします<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ
	 * moduleName モジュール名
	 * pathDest 作業ディレクトリパス
	 *
	 * <pre>
	 * @param vo チェックアウトVO
	 * @throws ScmException
	 */
	public abstract void checkoutHead(CheckOutVO vo) throws ScmException;

	/**
	 * チェックアウト処理を行います<br>
	 * 【対象はロット（ブランチ）】<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ
	 * pathDest 作業ディレクトリ
	 * moduleName モジュール名
	 * labelBranch ブランチタグ
	 *
	 * <pre>
	 * @param vo チェックアウトVO
	 * @throws ScmException
	 */
	public abstract void checkoutBranch(CheckOutVO vo) throws ScmException;

	/**
	 * ロットの資産をチェックアウトします<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ
	 * pathDest 作業ディレクトリパス
	 * moduleName モジュール名
	 *
	 * <pre>
	 * @param vo チェックアウトVO
	 * @throws ScmException
	 */
	public abstract void checkoutBranchRevision(CheckOutVO vo)
			throws ScmException;

	/**
	 * マージ処理を行います<br>
	 * branchの資産から特定リビジョン間の範囲での変更を抽出し、ワーキングコピー(pathDest)に反映します。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ
	 * pathDest 作業ディレクトリパス
	 * labelBranch ブランチタグ
	 * moduleName モジュール名
	 * mergeFrom マージ差分基点のRevisionNo
	 * mergeTo バージョンタグ
	 * </pre>
	 *
	 * @param vo マージVO
	 * @throws ScmException
	 */
	public abstract void merge(MergeVO vo) throws ScmException;

	/**
	 * ブランチ作成処理を行います<br>
	 * 【対象はロット（ブランチ）】<br>
	 * ブランチ作成時のブランチタグを打つ。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ
	 * labelBranch ブランチタグ
	 * commitComment コミットコメント
	 * </pre>
	 *
	 * @param vo コミットVO
	 * @return VcsCommitInfo 実行時ログ
	 * @throws ScmException
	 */
	public abstract VcsCommitInfo branch(CommitVO vo) throws ScmException;

	/**
	 * ブランチに対してバージョンタグを打ちます
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ情報
	 * labelBranch ブランチタグ
	 * labelTag バージョンタグ
	 * commitComment コミットコメント
	 * </pre>
	 *
	 * @param vo コミットVO
	 * @return VcsCommitInfo コミット情報
	 * @throws ScmException
	 *
	 */
	public abstract VcsCommitInfo tagToBranch(CommitVO vo) throws ScmException;

	/**
	 * トランクに対してタグを打ちます
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリのパス
	 * labelTag バージョンタグ
	 * commitComment コミットコメント
	 * </pre>
	 *
	 * @param vo コミットVO
	 * @return VcsCommitInfo コミット情報
	 * @throws ScmException
	 */
	public abstract VcsCommitInfo tagToTrunk(CommitVO vo) throws ScmException;

	// ここからscmUtils
	/**
	 * 特定の資産１件のステータス取得リクエストを行います。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * pathDest ワーキングコピーのパス
	 * objectPath ステータスを取得する資産のパス
	 * </pre>
	 *
	 * @param vo ステータスVO
	 * @return SVNStatus SvnStatusListがnullもしくはsizeが0の場合にnullを返す
	 * @throws ScmException
	 */
	public abstract SVNStatus statusSingle(StatusVO vo) throws ScmException;

	/**
	 * モジュール単位での一括ステータス取得リクエストを行います。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * basePath ワーキングコピーのベースパス
	 * moduleName モジュール名
	 * </pre>
	 *
	 * @param vo ステータスVO
	 * @return SVNStatus (Map形式)
	 * @throws ScmException,IOException
	 *
	 */
	public abstract Map<String, SVNStatus> statusAll(StatusVO vo)
			throws ScmException;

	/**
	 * 新規資産の「追加宣言」を行います。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * pathDest ワーキングコピーのパス
	 * moduleName モジュール名
	 * filePathArray 追加資産ファイルパスの配列
	 * </pre>
	 *
	 * @param vo アッドVO
	 * @throws ScmException
	 */
	public abstract void add(AddVO vo) throws ScmException;

	/**
	 * 削除資産の「削除宣言」を行います。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * pathDest ワーキングコピーのパス
	 * moduleName モジュール名
	 * fileArray 削除資産ファイルパスの配列
	 * </pre>
	 *
	 * @param vo デリートVO
	 * @throws ScmException
	 */
	public abstract void remove(DeleteVO vo) throws ScmException;

	/**
	 * 資産の「コミット」を行います。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * pathDest ワーキングコピーのパス
	 * moduleName モジュール名
	 * commitComment コミットコメント
	 * </pre>
	 *
	 * @param vo コミットVO
	 * @return VcsCommitInfo コミット結果オブジェクト
	 * @throws ScmException
	 */
	public abstract VcsCommitInfo commit(CommitVO vo) throws ScmException;

	/**
	 * 任意のワーキングコピーへの「アップデート」を行います。
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * pathDest ワーキングコピーのパス
	 * moduleName モジュール名
	 * </pre>
	 *
	 * @param vo アップデートVO
	 * @return long updateされたリビジョン番号 update失敗時は-1を返す。
	 * @throws ScmException
	 */
	public abstract long update(UpdateVO vo) throws ScmException;

	/**
	 * リポジトリのtrunk以下をチェックし、チェックアウトしようとしているモジュールがすべて存在するか判定して返す。<br>
	 * リポジトリのtrunk以下のモジュール名をセットで返します<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ情報
	 * </pre>
	 *
	 * @param vo Subディレクトリ取得VO
	 * @return Set<String> モジュール名
	 * @throws ScmException
	 */
	public abstract Set<String> chkExistsModuleInTrunk(ListSubDirVO vo)
			throws ScmException;

	/**
	 * リポジトリを参照し、trunkのリビジョンを取得します。
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリ情報
	 * </pre>
	 *
	 * @param vo Subディレクトリ取得VO
	 * @return trunkのリビジョン番号
	 * @throws ScmException
	 */
	public abstract long retriveTrunkRevision(ListSubDirVO vo)
			throws ScmException;

	/**
	 * リポジトリのtags以下をチェックし、varsionTagと同名の資産が存在しないか判定して返します。<br>
	 * 既に存在するタグと同名でタグ打ちすると失敗するので、事前に当該メソッドでチェックします。<br>
	 *
	 * <pre>
	 * voは以下の情報を必須とします
	 * repository リポジトリURL
	 * versionTag バージョンタグ名
	 * </pre>
	 *
	 * @param vo Subディレクトリ取得VO
	 * @return チェック結果
	 *
	 *         <pre>
	 * 		true	:	既存タグあり
	 * 		false	:	既存タグなし
	 * </pre>
	 * @throws ScmException
	 */
	public abstract boolean chkExistsTag(ListSubDirVO vo) throws ScmException;

	public abstract String makeSvnStatusStr(SVNStatus svnStatus);

	public abstract String makeSvnStatusStr(List<SVNStatus> svnStatus);

	public abstract List<SVNStatus> mergeStatusAll(StatusVO vo) throws ScmException;


}
