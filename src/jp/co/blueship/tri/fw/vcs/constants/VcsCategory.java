package jp.co.blueship.tri.fw.vcs.constants;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public enum VcsCategory {
	SVN( "SVN" );

	private String value = null;

	private VcsCategory( String value ) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static VcsCategory value( String value ) {

		if ( null == value ) {
			return SVN;
		}

		for ( VcsCategory scmType : values() ) {
			if ( scmType.value().equals( value ) ) {
				return scmType;
			}
		}

		return null;
	}
}
