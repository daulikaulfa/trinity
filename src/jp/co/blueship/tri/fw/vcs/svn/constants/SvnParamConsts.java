package jp.co.blueship.tri.fw.vcs.svn.constants;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.wc.SVNRevision;

import jp.co.blueship.tri.fw.vcs.svn.DirEntryHandler;
import jp.co.blueship.tri.fw.vcs.svn.LogEntryHandler;
import jp.co.blueship.tri.fw.vcs.svn.StatusHandler;


/**
 * SvnUtilで引数として渡すParamListに使用します。
 * 値はパラメータに渡すデータ型になっています。
 *
 */
public enum SvnParamConsts {

	REPOSITORY				( "リポジトリのパス情報" , new String() ) ,
	SVNSUBDIR				( "SVNサブディレクトリ[trunk/branches/tags]" , SvnSubDir.trunk ) ,
	MODULE_NAME				( "モジュール名" , new String() ) ,
	MODULE_NAME_ARRAY		( "モジュール名の配列" , new String[]{} ) ,
	PATH_DEST				( "取得先パス" , new String() ) ,
	OBJECT_PATH				( "モジュール名以下の資産パス" , new String() ) ,
	LABEL_TAG				( "タグ名" , new String() ) ,
	LABEL_BRANCH			( "ブランチ名" , new String() ) ,
	USER					( "ユーザ名" , new String() ) ,
	PASSWORD				( "パスワード" , new String() ) ,
	MERGE_FROM				( "" , SVNRevision.HEAD ) ,
	MERGE_TO				( "" , SVNRevision.HEAD ) ,
	//REVISION_LABEL		( "取得するリビジョンのラベル" , new String() ) ,
	REVISION_NO				( "取得するリビジョン番号(Long型)" , new Long( 0 ) ) ,
	PATH_ADD_ASSETS			( "追加する資産パスの配列（フルパス）" , new String[]{} ) ,
	PATH_DELETE_ASSET		( "削除する資産パス（フルパス）" , new String() ) ,
	COMMIT_COMMENT			( "コミットコメント" , new String() ),
	STATUS_HANDLER			( "ステータスハンドラ" , new StatusHandler( true ) ) ,
	LOG_HANDLER				( "ログハンドラ" , new LogEntryHandler() ) ,
	DIR_HANDLER				( "ディレクトリハンドラ" , new DirEntryHandler() ) ,
	SVN_DEPTH				( "探索範囲の深さ" , SVNDepth.EMPTY )
	;

	

	private String description = null ;
	private Object dataType = null ;

	/**
	 * コンストラクタ
	 * @param description
	 * @param dataType
	 */
	private SvnParamConsts( String description , Object dataType ) {
		this.description = description ;
		this.dataType = dataType ;
	}

	public Object getLabel() {
		return this.toString() ;
	}
	public String getDescription() {
		return this.description ;
	}
	public Object getDataType() {
		return this.dataType ;
	}


	/*
	public static SvnParamConsts getValue( String value ) {
		for ( SvnParamConsts userSearchMode : values() ) {
			if ( userSearchMode.getValue().equals( value ) ) {
				return userSearchMode ;
			}
		}

		return null;
	}
	*/

	/**
	 * 指定されたパラメタ名(キー)をもつパラメタ値が、指定されたコマンドパラメタマップ内に<BR>
     * 指定されているかどうかを検査します。
     *
     * @param params パラメタ名の配列
     * @param paramMap コマンドパラメタ値を格納したMap
     *
     * @return 一致した場合はNull。指定されたパラメタ値が存在しないか、型が一致しない場合は一致しないパラメータの配列を返す。
     */
	public static SvnParamConsts[] validate( SvnParamConsts[] params , Map<SvnParamConsts,Object> paramMap ) {

		List<SvnParamConsts> errorList = new ArrayList<SvnParamConsts>() ;

		for( SvnParamConsts param : params ) {
			//値がセットされているかのチェック
			Object obj = paramMap.get( param ) ;
			if( null == obj ) {
				errorList.add( param ) ;
				continue ;
			}
			//引数の型が一致しているかのチェック
			if( obj.getClass() != param.getDataType().getClass() ) {
				errorList.add( param ) ;
				continue ;
			}
		}

		return ( 0 == errorList.size() ) ? null : errorList.toArray( new SvnParamConsts[ 0 ] ) ;
	}
}
