package jp.co.blueship.tri.fw.vcs.svn.constants;


/**
 * SvnUtilで引数として渡すParamListに使用します。
 * 値はパラメータに渡すデータ型になっています。
 *
 */
public enum SvnSubDir {

	trunk			( "trunk" ) ,
	tags			( "tags" ) ,
	branches		( "branches" ) ;


	

	private String svnSubDir = null ;

	/**
	 * コンストラクタ
	 * @param description
	 * @param dataType
	 */
	private SvnSubDir( String svnSubDir ) {
		this.svnSubDir = svnSubDir ;
	}

	public String getLabel() {
		return this.toString() ;
	}

	public String getValue() {
		return this.svnSubDir ;
	}

	public static SvnSubDir getValue( String value ) {
		for ( SvnSubDir svnSubDir : values() ) {
			if ( svnSubDir.getValue().equals( value ) ) {
				return svnSubDir ;
			}
		}

		return null;
	}

}
