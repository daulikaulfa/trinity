package jp.co.blueship.tri.fw.vcs.svn;

import java.util.ArrayList;
import java.util.List;

import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;

/**
 *
 *
 */
public class DirEntryHandler implements ISVNDirEntryHandler {

	private List<SVNDirEntry> svnDirEntryList = new ArrayList<SVNDirEntry>() ;

	public List<SVNDirEntry> getSvnDirEntryList() {
		return svnDirEntryList;
	}

	public void setSvnDirEntryList(List<SVNDirEntry> svnDirEntryList) {
		this.svnDirEntryList = svnDirEntryList;
	}

	/**
	 * コンストラクタ
	 * @param isRemote
	 */
	public DirEntryHandler() {

	}

	public void handleDirEntry(SVNDirEntry svnDirEntry ) throws SVNException {
		this.svnDirEntryList.add( svnDirEntry ) ;
	}

}