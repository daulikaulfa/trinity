package jp.co.blueship.tri.fw.vcs.svn;

import java.util.ArrayList;
import java.util.List;

import org.tmatesoft.svn.core.ISVNLogEntryHandler;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLogEntry;

/**
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class LogEntryHandler implements ISVNLogEntryHandler {

	private List<SVNLogEntry> svnLogEntryList = new ArrayList<SVNLogEntry>() ;

	public List<SVNLogEntry> getSvnLogEntryList() {
		return svnLogEntryList;
	}

	public void setSvnLogEntryList(List<SVNLogEntry> svnLogEntryList) {
		this.svnLogEntryList = svnLogEntryList;
	}

	/**
	 * コンストラクタ
	 * @param isRemote
	 */
	public LogEntryHandler() {

	}

	/**
	 * @deprecated
	 */
	public void handleLogEntry( SVNLogEntry svnLogEntry ) throws SVNException {
//		this.svnLogEntryList.add( svnLogEntry ) ;
//
//
//		Map changedPaths = svnLogEntry.getChangedPaths() ;
//		Iterator<Set<Map.Entry>> iter = changedPaths.entrySet().iterator() ;
//		while( iter.hasNext() ) {
//			Map.Entry entry = (Map.Entry)iter.next() ;
//			System.out.println( entry.getKey() + " : " + entry.getValue() ) ;
//		}
//		//SVNProperties svnProperties = svnLogEntry.getRevisionProperties() ;
//
//		System.out.println( svnLogEntry.getAuthor() + " " +
//				svnLogEntry.getMessage() + " " +
//				svnLogEntry.getRevision() + " " +
//				svnLogEntry.getDate() ) ;
//
	}



}