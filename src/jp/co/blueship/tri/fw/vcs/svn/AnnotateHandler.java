package jp.co.blueship.tri.fw.vcs.svn;

import java.io.File;
import java.util.Date;

import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.ISVNAnnotateHandler;
/**
 *
 *
 */
public class AnnotateHandler implements ISVNAnnotateHandler {

	/**
	 * コンストラクタ
	 * @param isRemote
	 */
	public AnnotateHandler() {

	}

	public void handleEOF() {
	}

	public void handleLine(Date arg0, long arg1, String arg2, String arg3) throws SVNException {
		System.out.println( "@0  " + arg0 + " " + arg1 + " " + arg2 + " " + arg3 ) ;
	}

	public void handleLine(Date arg0, long arg1, String arg2, String arg3, Date arg4, long arg5, String arg6, String arg7, int arg8) throws SVNException {
		System.out.println( "@1  " + arg0 + " " + arg1 + " " + arg2 + " " + arg3 + " " + arg4 + " " + arg5 + " " + arg6 + " " + arg7 + " " + arg8 ) ;
	}

	public boolean handleRevision(Date arg0, long arg1, String arg2, File arg3) throws SVNException {
		System.out.println( "@2  " + arg0 + " " +  arg1 + " " + arg2 + " " + arg3 ) ;

		return false;
	}

}