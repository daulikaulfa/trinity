package jp.co.blueship.tri.fw.vcs.svn;

import java.util.ArrayList;
import java.util.List;

import org.tmatesoft.svn.core.SVNCancelException;
import org.tmatesoft.svn.core.wc.ISVNEventHandler;
import org.tmatesoft.svn.core.wc.ISVNStatusHandler;
import org.tmatesoft.svn.core.wc.SVNEvent;
import org.tmatesoft.svn.core.wc.SVNEventAction;
import org.tmatesoft.svn.core.wc.SVNStatus;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class StatusHandler implements ISVNStatusHandler , ISVNEventHandler {

	@SuppressWarnings("unused")
	private boolean isRemote ;

	private List<SVNStatus> svnStatusList = new ArrayList<SVNStatus>() ;

	public List<SVNStatus> getSvnStatusList() {
		return svnStatusList;
	}

	public void setSvnStatusList(List<SVNStatus> svnStatusList) {
		this.svnStatusList = svnStatusList;
	}

	/**
	 * コンストラクタ
	 * @param isRemote
	 */
	public StatusHandler( boolean isRemote ) {
		this.isRemote = isRemote ;
	}

	@Override
	public void handleStatus( SVNStatus status ) {

		this.svnStatusList.add( status ) ;

		//出力確認用
		/*
		SVNStatusType contentsStatus = status.getContentsStatus() ;

		String pathChangeType = " " ;

		boolean isAddedWithHistory = status.isCopied() ;
		if ( contentsStatus == SVNStatusType.STATUS_MODIFIED ) {//変更のあった資産
			pathChangeType = "M" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_CONFLICTED ) {//コンフリクトの発生している資産
			pathChangeType = "C" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_DELETED ) {//削除された資産
			pathChangeType = "D" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_ADDED ) {//新規追加された資産
			pathChangeType = "A" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_UNVERSIONED ) {//管理外の資産
			pathChangeType = "?" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_EXTERNAL ) {
			pathChangeType = "X" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_IGNORED ) {//バージョン管理外（無視）資産
			pathChangeType = "I" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_MISSING
				|| contentsStatus == SVNStatusType.STATUS_INCOMPLETE ) {
			pathChangeType = "!" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_OBSTRUCTED ) {
			pathChangeType = "~" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_REPLACED ) {
			pathChangeType = "R" ;
		} else if ( contentsStatus == SVNStatusType.STATUS_NONE
				|| contentsStatus == SVNStatusType.STATUS_NORMAL ) {
			pathChangeType = " " ;
		}

		String remoteChangeType = " ";

		if( status.getRemotePropertiesStatus() != SVNStatusType.STATUS_NONE ||
				status.getRemoteContentsStatus() != SVNStatusType.STATUS_NONE ) {
			remoteChangeType = "*" ;
		}

		SVNStatusType propertiesStatus = status.getPropertiesStatus() ;

		String propertiesChangeType = " " ;
		if ( propertiesStatus == SVNStatusType.STATUS_MODIFIED ) {
			propertiesChangeType = "M" ;
		} else if ( propertiesStatus == SVNStatusType.STATUS_CONFLICTED ) {
			propertiesChangeType = "C" ;
		}

		boolean isLocked = status.isLocked() ;
		boolean isSwitched = status.isSwitched() ;
		SVNLock localLock = status.getLocalLock() ;
		SVNLock remoteLock = status.getRemoteLock() ;
		String lockLabel = " " ;

		if ( localLock != null ) {
			lockLabel = "K" ;
			if ( remoteLock != null ) {
				if ( !remoteLock.getID().equals(localLock.getID() ) ) {
					lockLabel = "T" ;
				}
			} else {
				if( isRemote ) {
					lockLabel = "B" ;
				}
			}
		} else if ( remoteLock != null ) {
			lockLabel = "O" ;
		}

		long workingRevision = status.getRevision().getNumber() ;
		long lastChangedRevision = status.getCommittedRevision().getNumber() ;
		String offset = "                                " ;
		String[] offsets = new String[ 3 ] ;
		offsets[ 0 ] = offset.substring( 0 , 6 - String.valueOf(workingRevision).length() ) ;
		offsets[ 1 ] = offset.substring( 0 , 6 - String.valueOf(lastChangedRevision).length() ) ;
		//status
		offsets[ 2 ] = offset.substring( 0 , offset.length() - ( status.getAuthor() != null ? status.getAuthor().length() : 1 ) ) ;

		System.out.println( pathChangeType
				+ propertiesChangeType
				+ (isLocked ? "L" : " ")
				+ (isAddedWithHistory ? "+" : " ")
				+ (isSwitched ? "S" : " ")
				+ lockLabel
				+ "  "
				+ remoteChangeType
				+ "  "
				+ workingRevision
				+ offsets[0]
				+ (lastChangedRevision >= 0 ? String.valueOf(lastChangedRevision) : "?") + offsets[1]
				+ (status.getAuthor() != null ? status.getAuthor() : "?")
				+ offsets[2] + status.getFile().getPath());
		*/
	}

	/**
	 *
	 */
	public void handleEvent( SVNEvent event , double progress ) {
		SVNEventAction action = event.getAction() ;
		if( action == SVNEventAction.STATUS_COMPLETED ) {
			System.out.println( "Status against revision:  " + event.getRevision() ) ;
		}
	}

	public void checkCancelled() throws SVNCancelException {

	}

}