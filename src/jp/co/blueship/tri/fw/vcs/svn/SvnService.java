package jp.co.blueship.tri.fw.vcs.svn;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.validation.ConstraintViolation;

import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.wc.SVNStatus;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.validator.TriValidationHelper;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.vcs.IVcsService;
import jp.co.blueship.tri.fw.vcs.VcsCommitInfo;
import jp.co.blueship.tri.fw.vcs.ex.ScmException;
import jp.co.blueship.tri.fw.vcs.svn.constants.SvnSubDir;
import jp.co.blueship.tri.fw.vcs.vo.AddVO;
import jp.co.blueship.tri.fw.vcs.vo.CheckOutVO;
import jp.co.blueship.tri.fw.vcs.vo.CommitVO;
import jp.co.blueship.tri.fw.vcs.vo.DeleteVO;
import jp.co.blueship.tri.fw.vcs.vo.ListSubDirVO;
import jp.co.blueship.tri.fw.vcs.vo.MergeVO;
import jp.co.blueship.tri.fw.vcs.vo.StatusVO;
import jp.co.blueship.tri.fw.vcs.vo.UpdateVO;

/**
 * Subversionアクセスサービス機能を提供するサービス実装クラスです。
 *
 * @author Takayuki Kubo
 */
public class SvnService implements IVcsService {

	private SubversionCore subversionCore;

	/**
	 * Subversionへのログインアカウント情報を指定するコンストラクタです。
	 *
	 * @param userName Subversionアクセスアカウントのユーザ名(ID)
	 * @param password Subversionアクセスアカウントのパスワード
	 */
	public SvnService(String userName, String password) {

		subversionCore = new SubversionCore();
		subversionCore.setSvnKitProxy(new SvnKitProxy(userName, password));

	}

	/**
	 * デフォルトコンストラクタ
	 */
	@VisibleForTesting
	SvnService() {
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#checkoutHead(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.CheckOutVO)
	 */
	@Override
	public void checkoutHead(CheckOutVO vo) throws ScmException {

		validateBean(vo, CheckOutVO.ChkHeadChecks.class);

		try {
			subversionCore.checkoutTrunkHead(vo.getRepository(), vo.getModuleName(), vo.getPathDest());
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005092S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#checkoutBranch(jp.co.
	 * blueship.tri.bl.cm.am.vcs.vo.CheckOutVO)
	 */
	@Override
	public void checkoutBranch(CheckOutVO vo) throws ScmException {

		validateBean(vo, CheckOutVO.ChkBranchChecks.class);

		try {
			subversionCore.checkoutBranchHead(vo.getRepository(), vo.getModuleName(), vo.getPathDest(), vo.getLabelBranch());
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005093S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#checkoutBranchRevision
	 * (jp.co.blueship.tri.bl.cm.am.vcs.vo.CheckOutVO)
	 */
	@Override
	public void checkoutBranchRevision(CheckOutVO vo) throws ScmException {

		validateBean(vo, CheckOutVO.RevisionChecks.class);

		try {
			subversionCore.checkoutBranchRevision(vo.getRepository(), vo.getModuleName(), vo.getPathDest(), vo.getLabelBranch(), vo.getRevisionNo());
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005094S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#merge(jp.co.blueship.
	 * tri.bl.cm.am.vcs.vo.MergeVO)
	 */
	@Override
	public void merge(MergeVO vo) throws ScmException {

		validateBean(vo);
		try {
			subversionCore.mergeFromBranch(vo.getRepository(), vo.getLabelBranch(), vo.getModuleName(), vo.getMergeFrom(), vo.getMergeTo(),
					vo.getPathDest());
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005095S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#branch(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.CommitVO)
	 */
	@Override
	public VcsCommitInfo branch(CommitVO vo) throws ScmException {

		validateBean(vo, CommitVO.BranchChecks.class);

		try {
			SVNCommitInfo info = subversionCore.branch(vo.getRepository(), vo.getLabelBranch(), vo.getCommitComment());
			return settingFromSvnInfoToVcsInfo(info);
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005096S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#tagToBranch(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.CommitVO)
	 */
	@Override
	public VcsCommitInfo tagToBranch(CommitVO vo) throws ScmException {

		validateBean(vo, CommitVO.TagToBChecks.class);

		try {
			SVNCommitInfo info = subversionCore.tagToBranch(vo.getRepository(), vo.getLabelBranch(), vo.getLabelTag(), vo.getCommitComment());
			return settingFromSvnInfoToVcsInfo(info);
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005097S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#tagToTrunk(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.CommitVO)
	 */
	@Override
	public VcsCommitInfo tagToTrunk(CommitVO vo) throws ScmException {

		validateBean(vo, CommitVO.TagToTChecks.class);

		try {
			SVNCommitInfo info = subversionCore.tagToTrunk(vo.getRepository(), vo.getLabelTag(), vo.getCommitComment());
			return settingFromSvnInfoToVcsInfo(info);
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005098S, e);
		}
	}

	// ここからscmUtils
	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#statusSingle(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.StatusVO)
	 */
	@Override
	public SVNStatus statusSingle(StatusVO vo) throws ScmException {

		validateBean(vo, StatusVO.SingleChecks.class);

		StatusHandler handler = new StatusHandler(true);
		try {
			subversionCore.statusSingle(vo.getPathDest(), vo.getObjectPath(), handler);// 該当資産の１件ずつのステータス取得
																						// :
																						// 大量資産削除時は一括より遅くなるかも
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005099S, e);
		}
		List<SVNStatus> svnStatusList = handler.getSvnStatusList();
		if (null == svnStatusList || 0 == svnStatusList.size()) {
			return null;
		} else {
			return svnStatusList.get(0);// 取得できるのは１件のみ
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#statusAll(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.StatusVO)
	 */
	@Override
	public Map<String, SVNStatus> statusAll(StatusVO vo) throws ScmException {

		validateBean(vo, StatusVO.StatusAll.class);

		List<SVNStatus> svnStatusList = statusAllCommonPart(vo);
		if (TriStringUtils.isEmpty(svnStatusList)) {
			return null;
		} else {
			return makeSvnStatusMap(svnStatusList);
		}
	}

	/**
	 * merge部分のStatusAllのみ、V2のロジックが少し異なっていたため、別で作成しました。
	 *
	 * @param vo
	 * @return svnStatusList
	 */
	public List<SVNStatus> mergeStatusAll(StatusVO vo) throws ScmException {

		validateBean(vo, StatusVO.StatusAll.class);

		List<SVNStatus> svnStatusList = statusAllCommonPart(vo);
		if (TriStringUtils.isEmpty(svnStatusList)) {
			return null;
		} else {
			return svnStatusList;
		}
	}

	/**
	 * statusAllメソッドの共通部分の処理を行うprivateメソッドです。
	 *
	 * @param vo
	 * @return handler.getSvnStatusList()
	 */
	private List<SVNStatus> statusAllCommonPart(StatusVO vo) throws ScmException {

		StatusHandler handler = new StatusHandler(true);
		try {
			subversionCore.statusAll(vo.getPathDest(), vo.getModuleName(), handler);// 該当資産のモジュール全体一括のステータス取得
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005099S, e);
		}
		return handler.getSvnStatusList();
	}

	private Map<String, SVNStatus> makeSvnStatusMap(List<SVNStatus> svnStatusList) throws TriSystemException {

		Map<String, SVNStatus> svnStatusMap = new LinkedHashMap<String, SVNStatus>();
		try {
			for (SVNStatus svnStatus : svnStatusList) {
				String path = TriStringUtils.convertPath(svnStatus.getFile().getCanonicalPath());
				svnStatusMap.put(path, svnStatus);// キーはワーキングコピーのフルパス
			}
		} catch (IOException e) {
			throw new TriSystemException(SmMessageId.SM005105S, e);// getCanonicalPathのIOExceptionはレアケースと考えられるため、SystemExceptionに渡す
		}
		return svnStatusMap;
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#add(jp.co.blueship.tri
	 * .bl.cm.am.vcs.vo.AddVO)
	 */
	@Override
	public void add(AddVO vo) throws ScmException {

		validateAddVo(vo);

		List<String> pathList = new ArrayList<String>();
		String basePath = TriStringUtils.linkPathBySlash(vo.getPathDest(), vo.getModuleName());

		for (String filePath : vo.getFilePathArray()) {
			pathList.add(TriStringUtils.linkPathBySlash(basePath, filePath));
		}
		try {
			subversionCore.add(pathList.toArray(new String[0]));
		} catch (SVNException e) {
			// SVNErrorCode.ENTRY_EXISTS
			// SVNErrorCode.CLIENT_NO_VERSIONED_PARENT
			throw new ScmException(SmMessageId.SM005100S, e);
		}
	}

	private void validateAddVo(AddVO vo) throws ScmException {

		validateBean(vo);

		ExceptionUtils.throwIf(//
				hasEmptyFilePath(vo), //
				new ScmException(//
						SmMessageId.SM004096F, //
						validationHelper().getMessageParamValue(MessageParameter.PATH_ADD_ASSETS)));
	}

	private static boolean hasEmptyFilePath(AddVO vo) {
		return !vo.isAllFilePathNotEmpty();
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#remove(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.DeleteVO)
	 */
	@Override
	public void remove(DeleteVO vo) throws ScmException {

		validateRemoveVo(vo);

		String basePath = TriStringUtils.linkPathBySlash(vo.getPathDest(), vo.getModuleName());
		try {
			for (String filePath : vo.getFilePathArray()) {
				subversionCore.delete(getDeletePath(basePath, filePath));
			}
		} catch (SVNException e) {
			// path is not under version control
			// can not delete path without forcing
			throw new ScmException(SmMessageId.SM005101S, e);
		}
	}
	
	private String getDeletePath(String basePath, String filePath) {
		File file = new File(TriStringUtils.linkPathBySlash(basePath, filePath));
		File baseFolder = new File(basePath);
		
		if (!file.exists()) return file.getAbsolutePath();
		
		// while parent folder has only one child AND parent folder is descendant of base folder
		while (file.getParentFile().list().length == 1 && file.getParentFile().getAbsolutePath().length() > baseFolder.getAbsolutePath().length()) {
			file = file.getParentFile();
		}
		
		return file.getAbsolutePath();
	}

	private void validateRemoveVo(DeleteVO vo) throws ScmException {

		validateBean(vo);

		ExceptionUtils.throwIf(//
				hasEmptyFilePath(vo), //
				new ScmException(//
						SmMessageId.SM004096F, //
						validationHelper().getMessageParamValue(MessageParameter.PATH_DELETE_ASSETS)));
	}

	private static boolean hasEmptyFilePath(DeleteVO vo) {
		return !vo.isAllFilePathNotEmpty();
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#commit(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.CommitVO)
	 */
	@Override
	public VcsCommitInfo commit(CommitVO vo) throws ScmException {

		validateCommitVo(vo);

		// 複数モジュールを渡すと上位階層で一括commitしようとしてコケる。仕様かバグか不明。V2LXX
		try {
			SVNCommitInfo info = subversionCore.commit(vo.getPathDest(), vo.getModuleNameArray(), vo.getCommitComment());
			return settingFromSvnInfoToVcsInfo(info);
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005102S, e);
		}
	}

	private void validateCommitVo(CommitVO vo) throws ScmException {

		validateBean(vo, CommitVO.CommitChecks.class);

		ExceptionUtils.throwIf(//
				hasEmptyFilePath(vo), //
				new ScmException(//
						SmMessageId.SM004096F, //
						validationHelper().getMessageParamValue(MessageParameter.MODULE_NAME_ARRAY)));
	}

	private static boolean hasEmptyFilePath(CommitVO vo) {
		return !vo.isAllModuleNameNotEmpty();
	}

	/**
	 * SVNCommitInfo型からVcsCommitInfo型へ置き換えるprivateメソッドです。
	 * 業務ロジックからSVNkitを切り離すために、置き換えます。
	 *
	 * @param info
	 * @return vcsCommitInfo
	 */
	private VcsCommitInfo settingFromSvnInfoToVcsInfo(SVNCommitInfo info) {

		VcsCommitInfo vcsCommitInfo = new VcsCommitInfo();
		vcsCommitInfo.setNewRevision(info.getNewRevision());
		vcsCommitInfo.setAuthor(info.getAuthor());
		vcsCommitInfo.setDate(info.getDate());
		if (null != info.getErrorMessage()) {
			vcsCommitInfo.setErrorMessage(info.getErrorMessage().toString());
		}
		return vcsCommitInfo;
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#update(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.UpdateVO)
	 */
	@Override
	public long update(UpdateVO vo) throws ScmException {

		validateBean(vo);

		try {
			return subversionCore.update(vo.getPathDest(), vo.getModuleName());
		} catch (SVNException e) {
			throw new ScmException(SmMessageId.SM005103S, e);
		}
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#chkExistsModuleInTrunk
	 * (jp.co.blueship.tri.bl.cm.am.vcs.vo.ListSubDirVO)
	 */
	@Override
	public Set<String> chkExistsModuleInTrunk(ListSubDirVO vo) throws ScmException {

		validateBean(vo, ListSubDirVO.ListChecks.class);
		DirEntryHandler handler = new DirEntryHandler();
		try {
			subversionCore.listSubDir(vo.getRepository(), SvnSubDir.trunk, SVNDepth.IMMEDIATES, handler);
		} catch (SVNException e) {
			throw new ScmException( SmMessageId.SM005104S, e );
		}
		return subversionCore.moduleNameSet(handler);
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#retriveTrunkRevision(
	 * jp.co.blueship.tri.bl.cm.am.vcs.vo.ListSubDirVO)
	 */
	@Override
	public long retriveTrunkRevision(ListSubDirVO vo) throws ScmException {

		validateBean(vo, ListSubDirVO.ListChecks.class);

		DirEntryHandler handler = new DirEntryHandler();
		try {
			subversionCore.listSubDir(vo.getRepository(), SvnSubDir.trunk, SVNDepth.EMPTY, handler);
		} catch (SVNException e) {
			throw new ScmException( SmMessageId.SM005104S , e );
		}
		return subversionCore.revisionFromHandler(handler);// trunkへのマージコミット直後のリビジョン
	}

	/*
	 * (非 Javadoc)
	 *
	 * @see
	 * jp.co.blueship.tri.bl.cm.am.vcs.svn.IVcsService#chkExistsTag(jp.co.blueship
	 * .tri.bl.cm.am.vcs.vo.ListSubDirVO)
	 */
	@Override
	public boolean chkExistsTag(ListSubDirVO vo) throws ScmException {

		validateBean(vo, ListSubDirVO.ChkTagChecks.class);

		DirEntryHandler handler = new DirEntryHandler();
		try {
			subversionCore.listSubDir(vo.getRepository(), SvnSubDir.tags, SVNDepth.IMMEDIATES, handler);
		} catch (SVNException e) {
			throw new ScmException( SmMessageId.SM005104S , e );
		}
		return subversionCore.checkTag(vo.getVersionTag(), handler);
	}

	/*
	 * subversionCoreから直接呼び出させないために作成
	 */
	public String makeSvnStatusStr(SVNStatus status) {

		return subversionCore.makeSvnStatusStr(status);
	}

	/*
	 * subversionCoreから直接呼び出させないために作成
	 */
	public String makeSvnStatusStr(List<SVNStatus> status) {

		return subversionCore.makeSvnStatusStr(status);
	}

	private <T> void validateBean(T bean, Class<?>... groups) throws ScmException {

		Set<ConstraintViolation<T>> violations = validationHelper().validate(bean, groups);
		if (TriCollectionUtils.isEmpty(violations)) {
			return;
		}

		ConstraintViolation<T> violation = violations.iterator().next();
		IMessageId messageID = validationHelper().getMessageIDFrom(violation);
		String[] args = validationHelper().getMessageParametersAsStringFrom(violation);

		throw new ScmException(messageID, args);
	}

	private TriValidationHelper validationHelper() {
		return (TriValidationHelper) Contexts.getInstance().getBeanFactory().getBean(TriValidationHelper.BEAN_NAME);
	}

}
