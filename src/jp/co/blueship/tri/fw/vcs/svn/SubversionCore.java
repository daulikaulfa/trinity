package jp.co.blueship.tri.fw.vcs.svn;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNLock;
import org.tmatesoft.svn.core.SVNNodeKind;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNCopySource;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatus;
import org.tmatesoft.svn.core.wc.SVNStatusType;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PathBuilder;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.vcs.svn.constants.SvnSubDir;

/**
 *
 * Subversionアクセス機能を提供するクラスです。 Subversionアクセスに関する低レベルAPIを提供します。
 *
 * @author Takayuki Kubo
 *
 */
public class SubversionCore {

	private static final String SEP = SystemProps.LineSeparator.getProperty();
	private static final TriFunction<SVNDirEntry, String> TO_DIR_ENTRY_NAME = new TriFunction<SVNDirEntry, String>() {

		@Override
		public String apply(SVNDirEntry entry) {
			return entry.getName();
		}
	};

	private SvnKitProxy proxy;

	/**
	 * デフォルトコンストラクタ
	 */
	public SubversionCore() {
	}

	/**
	 * SvnKitプロキシクラスのインスタンスを注入します。
	 *
	 * @param proxy
	 */
	public void setSvnKitProxy(SvnKitProxy proxy) {
		this.proxy = proxy;
	}

	/**
	 * チェックアウトを行います。<br>
	 * trunkから最新資産を取得する際に用います。<br>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param moduleName モジュール名
	 * @param pathDest チェックアウト先フルパス
	 *
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void checkoutTrunkHead(String repository, String moduleName, String pathDest) throws SVNException {

		String url = PathBuilder.from(repository).//
				with(SvnSubDir.trunk.getValue()).//
				with(moduleName).//
				buildBySlash();

		String destFullPath = PathBuilder.from(pathDest).//
				with(moduleName).//
				buildBySlash();

		// 処理の実行
		proxy.checkout(SVNURL.parseURIEncoded(url), new File(destFullPath), SVNRevision.HEAD, SVNRevision.HEAD);
	}

	/**
	 * チェックアウトを行う。<br>
	 * branchesから特定のブランチでの最新資産を取得する際に用います。<br>
	 *
	 * <pre>
	 * 	[取得元]    ：branches
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param moduleName モジュール名
	 * @param pathDest チェックアウト先フルパス
	 * @param labelBranch ブランチ
	 *
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void checkoutBranchHead(String repository, String moduleName, String pathDest, String labelBranch) throws SVNException {

		// branchはトップディレクトリがブランチ名になるため、１階層掘り下げ
		String url = PathBuilder.from(repository).//
				with(SvnSubDir.branches.getValue()).//
				with(labelBranch).//
				with(moduleName).//
				buildBySlash();

		String destFullPath = PathBuilder.from(pathDest).//
				with(moduleName).//
				buildBySlash();

		// 処理の実行
		proxy.checkout(SVNURL.parseURIEncoded(url), new File(destFullPath), SVNRevision.parse(labelBranch), SVNRevision.parse(labelBranch));

	}

	/**
	 * チェックアウトを行う。<br>
	 * branchesから特定のブランチ・特定のリビジョンでの資産を取得する際に用います。<br>
	 *
	 * <pre>
	 * 	[取得元]    ：branches
	 * 	[取得バージョン]：任意のリビジョン
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param moduleName モジュール名
	 * @param pathDest チェックアウト先フルパス
	 * @param labelBranch ブランチタグ名
	 * @param revisionNo 取得するリビジョン番号
	 *
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void checkoutBranchRevision(String repository, String moduleName, String pathDest, String labelBranch, Long revisionNo)
			throws SVNException {

		String url = PathBuilder.from(repository).//
				with(SvnSubDir.branches.getValue()).//
				with(labelBranch).//
				with(moduleName).//
				buildBySlash();

		String destFullPath = PathBuilder.from(pathDest).//
				with(moduleName).//
				buildBySlash();

		if (true != proxy.isValidRevisionNumber(revisionNo)) {
			throw new TriSystemException(SmMessageId.SM004092F , revisionNo.toString() );
		}

		// 処理の実行
		proxy.checkout(SVNURL.parseURIEncoded(url), new File(destFullPath), SVNRevision.create(revisionNo), SVNRevision.create(revisionNo));
	}

	/**
	 * ブランチを生成します。 trunkの最新資産からブランチを作成する際に用います。<br>
	 *
	 * <pre>
	 * 	[取得元]    ：trunk
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param moduleName モジュール名
	 * @param pathDest チェックアウト先フルパス
	 * @param labelBranch ブランチタグ名
	 * @param commitComment コミットコメント
	 * @return コミット情報
	 * @throws SVNException SVNKitで例外が発生した。
	 *
	 */
	public SVNCommitInfo branch(String repository, String labelBranch, String commitComment) throws SVNException {

		// ブランチ元となる情報
		List<SVNCopySource> svnCopySourceList = new ArrayList<SVNCopySource>();

		String srcUrl = TriStringUtils.linkPathBySlash(repository, SvnSubDir.trunk.getValue());
		SVNCopySource svnCopySource = new SVNCopySource(SVNRevision.HEAD, SVNRevision.HEAD, SVNURL.parseURIEncoded(srcUrl));
		svnCopySourceList.add(svnCopySource);

		// ブランチ格納先となる情報
		String destUrl = PathBuilder.from(repository).//
				with(SvnSubDir.branches.getValue()).//
				with(labelBranch).//
				buildBySlash();

		return proxy.copyUrlToUrl(svnCopySourceList.toArray(new SVNCopySource[0]), destUrl, commitComment);

	}

	/**
	 * タグを生成します。 trunkの最新資産からタグを作成する際に用います。<br>
	 *
	 * <pre>
	 * 	[取得元]    ：trunk
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param labelTag タグ名
	 * @param commitComment コミットコメント
	 * @return コミット情報
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public SVNCommitInfo tagToTrunk(String repository, String labelTag, String commitComment) throws SVNException {

		// タグ元となる情報
		List<SVNCopySource> svnCopySourceList = new ArrayList<SVNCopySource>();

		String srcUrl = TriStringUtils.linkPathBySlash(repository, SvnSubDir.trunk.getValue());
		SVNCopySource svnCopySource = new SVNCopySource(SVNRevision.HEAD, SVNRevision.HEAD, SVNURL.parseURIEncoded(srcUrl));
		svnCopySourceList.add(svnCopySource);

		// タグ格納先となる情報
		String destUrl = PathBuilder.from(repository).//
				with(SvnSubDir.tags.getValue()).//
				with(labelTag).//
				buildBySlash();

		return proxy.copyUrlToUrl(svnCopySourceList.toArray(new SVNCopySource[0]), destUrl, commitComment);
	}

	/**
	 * ブランチからタグを生成します。ブランチの最新資産からタグを作成する際に用います。<br>
	 *
	 * <pre>
	 * 	[取得元]    ：branchesの特定のブランチ名
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param labelBranch ブランチタグ名
	 * @param labelTag 生成するタグ名
	 * @param commitComment コミットコメント
	 * @return コミット情報
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public SVNCommitInfo tagToBranch(String repository, String labelBranch, String labelTag, String commitComment) throws SVNException {

		// タグ元となる情報
		List<SVNCopySource> svnCopySourceList = new ArrayList<SVNCopySource>();

		String srcUrl = TriStringUtils.linkPathBySlash(repository, SvnSubDir.branches.getValue());
		srcUrl = TriStringUtils.linkPathBySlash(srcUrl, labelBranch);
		SVNCopySource svnCopySource = new SVNCopySource(SVNRevision.HEAD, SVNRevision.HEAD, SVNURL.parseURIEncoded(srcUrl));
		svnCopySourceList.add(svnCopySource);

		// タグ格納先となる情報
		String destUrl = PathBuilder.from(repository).//
				with(SvnSubDir.tags.getValue()).//
				with(labelTag).//
				buildBySlash();

		return proxy.copyUrlToUrl(svnCopySourceList.toArray(new SVNCopySource[0]), destUrl, commitComment);
	}

	/**
	 * リポジトリに存在する資産の一覧を取得します。
	 *
	 * <pre>
	 * 	[取得元         ：任意
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param svnSubDir サブディレクトリ （trunk/tags/branches）
	 * @param dirHandler ディレクトリハンドラ
	 * @param svnDepth 探索範囲の深さ
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void listSubDir(String repository, SvnSubDir svnSubDir, SVNDepth svnDepth, DirEntryHandler dirHandler) throws SVNException {

		String url = TriStringUtils.linkPathBySlash(repository, svnSubDir.getValue());
		proxy.listRemote(SVNURL.parseURIEncoded(url), SVNRevision.HEAD, SVNRevision.HEAD, svnDepth, dirHandler);
	}

	/**
	 * 資産のステータスを取得します。モジュール内の全資産のステータス（ｎ件）を取得します。<br>
	 *
	 * <pre>
	 * 	[取得元]        ：ローカルのワーキングコピー
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param pathDest ローカル資産のフルパス
	 * @param moduleName モジュール名
	 * @param statusHandler 取得したステータスを格納するためのハンドラインスタンス
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void statusAll(String pathDest, String moduleName, StatusHandler statusHandler) throws SVNException {

		String localPath = TriStringUtils.linkPathBySlash(pathDest, moduleName);
		proxy.status(new File(localPath), statusHandler, SVNDepth.INFINITY);
	}

	/**
	 * 資産のステータスを取得します。特定のファイル・フォルダの資産ステータス（１件）を取得します。<br>
	 *
	 * <pre>
	 * 	[取得元]        ：ローカルのワーキングコピー
	 * 	[取得バージョン]：HEAD
	 * </pre>
	 *
	 * @param pathDest ローカル資産のフルパス
	 * @param objectPath モジュール名を含み、それ以下のパス
	 * @param statusHandler 取得したステータスを格納するためのハンドラインスタンス
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void statusSingle(String pathDest, String objectPath, StatusHandler statusHandler) throws SVNException {

		String localPath = TriStringUtils.linkPathBySlash(pathDest, objectPath);
		
		if (TriFileUtils.isParentExist(localPath)) {
			proxy.status(new File(localPath), statusHandler, SVNDepth.EMPTY);
		}
	}

	/**
	 * アップデートを行います。 ローカルのワーキングコピーに対し、最新資産からの同期を行う際に用います。<br>
	 *
	 * @param pathDest ローカル資産のフルパス
	 * @param moduleName モジュール名
	 * @return updateされたリビジョン番号 update失敗時は-1を返す。
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public long update(String pathDest, String moduleName) throws SVNException {

		String localPath = TriStringUtils.linkPathBySlash(pathDest, moduleName);
		return proxy.update(localPath, SVNRevision.HEAD);
	}

	/**
	 * コミットを行います。 ローカルのワーキングコピーでの変更を、リポジトリに反映する際に用います。<br>
	 *
	 * @param pathDest ローカル資産のパス
	 * @param moduleNameArray モジュール名の配列
	 * @param commitComment コミットコメント
	 * @return SVNCommitInfo コミット情報
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public SVNCommitInfo commit(final String pathDest, String[] moduleNameArray, String commitComment) throws SVNException {

		List<File> pathList = collect(moduleNameArray, new TriFunction<String, File>() {

			@Override
			public File apply(String moduleName) {
				String path = TriStringUtils.linkPathBySlash(pathDest, moduleName);
				return new File(path);
			}
		});

		String[] changeListArray = null; // 未使用 changelist機能は利用していない
		SVNProperties svnProperties = null; // SVNのコミットに対する付加情報を使用しないため null設定
		return proxy.commit(pathList.toArray(new File[0]), commitComment, changeListArray, svnProperties);

	}

	/**
	 * 資産の追加宣言(add)を行います。<br>
	 * ローカルのワーキングコピーでの変更を、リポジトリに反映する際に用います。<br>
	 * commitを行うまでは、実際には状態が確定されません。<br>
	 *
	 * @param pathAddAssets 追加資産フルパスの配列
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void add(String[] pathAddAssets) throws SVNException {

		List<File> pathList = collect(pathAddAssets, new TriFunction<String, File>() {

			@Override
			public File apply(String path) {
				return new File(path);
			}
		});

		proxy.add(pathList.toArray(new File[0]));
	}

	/**
	 * 資産の削除宣言(delete)を行います。<br>
	 * ローカルのワーキングコピーでの変更を、リポジトリに反映する際に用います。<br>
	 * commitを行うまでは、実際には状態が確定されません。<br>
	 *
	 * @param pathDeleteAsset 削除資産フルパス
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void delete(String pathDeleteAsset) throws SVNException {

		proxy.delete(new File(pathDeleteAsset));
	}

	/**
	 * マージ処理を行います。<br>
	 * branchの資産から特定リビジョン間の範囲での変更を抽出し、ワーキングコピー(workDir)に反映する。<br>
	 *
	 * @param repository リポジトリのＵＲＬ
	 * @param labelBranch ブランチタグ名
	 * @param moduleName モジュール名
	 * @param mergeFrom マージ元リビジョン
	 * @param mergeTo マージ先リビジョン
	 * @param pathDest ローカル資産のパス
	 *
	 * @throws SVNException SVNKitで例外が発生した。
	 */
	public void mergeFromBranch(String repository, String labelBranch, String moduleName, SVNRevision mergeFrom, SVNRevision mergeTo, String pathDest)
			throws SVNException {

		String urlPath = PathBuilder.from(repository).//
				with(SvnSubDir.branches.getValue()).//
				with(labelBranch).//
				with(moduleName).//
				buildBySlash();

		String destPath = TriStringUtils.linkPathBySlash(pathDest, moduleName);
		proxy.merge(SVNURL.parseURIEncoded(urlPath), mergeFrom, SVNURL.parseURIEncoded(urlPath), mergeTo, new File(destPath));
	}

	/**
	 * tags以下をチェックし、varsionTagと同名の資産が存在しないか判定して返します。<br>
	 *
	 * @param versionTag バージョンタグ名
	 * @param handler ハンドラ
	 * @return チェック結果
	 *
	 *         <pre>
	 * 		true	:	既存タグあり
	 * 		false	:	既存タグなし
	 * </pre>
	 * @throws TriSystemException
	 */
	public boolean checkTag(String versionTag, DirEntryHandler handler) {

		List<SVNDirEntry> entryList = handler.getSvnDirEntryList();
		for (SVNDirEntry svnDirEntry : entryList) {
			if (versionTag.equals(svnDirEntry.getName())) {// 打とうとしているタグと同名のオブジェクトが既に存在
				if (SVNNodeKind.DIR.equals(svnDirEntry.getKind())) {// 既にタグが打たれている
					return true;
				} else {
					throw new TriSystemException(SmMessageId.SM004093F , svnDirEntry.getName() );
				}
			}
		}
		return false;
	}

	/**
	 * ハンドラからリビジョンを取得します。
	 *
	 * @param handler
	 * @return trunkのリビジョン番号
	 */
	public long revisionFromHandler(DirEntryHandler handler) {

		List<SVNDirEntry> svnDirEntryList = handler.getSvnDirEntryList();
		if (null == svnDirEntryList || 1 != svnDirEntryList.size()) {
			throw new TriSystemException(SmMessageId.SM005091S);
		}
		SVNDirEntry svnDirEntry = svnDirEntryList.get(0);
		return svnDirEntry.getRevision();// trunkへのマージコミット直後のリビジョン
	}

	/**
	 * ハンドラからモジュール名を取得します。
	 *
	 * @param handler
	 * @return String型のモジュール名のset
	 */
	public Set<String> moduleNameSet(DirEntryHandler handler) {

		List<SVNDirEntry> entryList = handler.getSvnDirEntryList();
		Set<String> moduleNameSet = new LinkedHashSet<String>();
		TriCollectionUtils.collect(entryList, TO_DIR_ENTRY_NAME, moduleNameSet);

		return moduleNameSet;
	}

	/**
	 * SvnKitで取得したSubversionのステータス情報(SVNStatus)を文字列に加工する。
	 *
	 * @param status
	 * @return ステータス情報文字列
	 */
	public String makeSvnStatusStr(SVNStatus status) {

		StringBuilder stb = new StringBuilder();

		SVNStatusType contentsStatus = status.getContentsStatus();

		String pathChangeType = " ";

		boolean isAddedWithHistory = status.isCopied();
		if (contentsStatus == SVNStatusType.STATUS_MODIFIED) {// 変更のあった資産
			pathChangeType = "M";
		} else if (contentsStatus == SVNStatusType.STATUS_CONFLICTED) {// コンフリクトの発生している資産
			pathChangeType = "C";
		} else if (contentsStatus == SVNStatusType.STATUS_DELETED) {// 削除された資産
			pathChangeType = "D";
		} else if (contentsStatus == SVNStatusType.STATUS_ADDED) {// 新規追加された資産
			pathChangeType = "A";
		} else if (contentsStatus == SVNStatusType.STATUS_UNVERSIONED) {// 管理外の資産
			pathChangeType = "?";
		} else if (contentsStatus == SVNStatusType.STATUS_EXTERNAL) {
			pathChangeType = "X";
		} else if (contentsStatus == SVNStatusType.STATUS_IGNORED) {// バージョン管理外（無視）資産
			pathChangeType = "I";
		} else if (contentsStatus == SVNStatusType.STATUS_MISSING || contentsStatus == SVNStatusType.STATUS_INCOMPLETE) {
			pathChangeType = "!";
		} else if (contentsStatus == SVNStatusType.STATUS_OBSTRUCTED) {
			pathChangeType = "~";
		} else if (contentsStatus == SVNStatusType.STATUS_REPLACED) {
			pathChangeType = "R";
		} else if (contentsStatus == SVNStatusType.STATUS_NONE || contentsStatus == SVNStatusType.STATUS_NORMAL) {
			pathChangeType = " ";
		}

		String remoteChangeType = " ";

		if (status.getRemotePropertiesStatus() != SVNStatusType.STATUS_NONE || status.getRemoteContentsStatus() != SVNStatusType.STATUS_NONE) {
			remoteChangeType = "*";
		}

		SVNStatusType propertiesStatus = status.getPropertiesStatus();

		String propertiesChangeType = " ";
		if (propertiesStatus == SVNStatusType.STATUS_MODIFIED) {
			propertiesChangeType = "M";
		} else if (propertiesStatus == SVNStatusType.STATUS_CONFLICTED) {
			propertiesChangeType = "C";
		}

		boolean isLocked = status.isLocked();
		boolean isSwitched = status.isSwitched();
		SVNLock localLock = status.getLocalLock();
		SVNLock remoteLock = status.getRemoteLock();
		String lockLabel = " ";

		if (localLock != null) {
			lockLabel = "K";
			if (remoteLock != null) {
				if (!remoteLock.getID().equals(localLock.getID())) {
					lockLabel = "T";
				}
			} else {
				if (true) {
					lockLabel = "B";
				}
			}
		} else if (remoteLock != null) {
			lockLabel = "O";
		}

		long workingRevision = status.getRevision().getNumber();
		long lastChangedRevision = status.getCommittedRevision().getNumber();
		String offset = "                                ";
		String[] offsets = new String[3];
		offsets[0] = offset.substring(0, 6 - String.valueOf(workingRevision).length());
		offsets[1] = offset.substring(0, 6 - String.valueOf(lastChangedRevision).length());
		// status
		offsets[2] = offset.substring(0, offset.length() - (status.getAuthor() != null ? status.getAuthor().length() : 1));

		String statusLabel = pathChangeType + propertiesChangeType + (isLocked ? "L" : " ") + (isAddedWithHistory ? "+" : " ")
				+ (isSwitched ? "S" : " ") + lockLabel + "  " + remoteChangeType + "  " + workingRevision + offsets[0]
				+ (lastChangedRevision >= 0 ? String.valueOf(lastChangedRevision) : "?") + offsets[1]
				+ (status.getAuthor() != null ? status.getAuthor() : "?") + offsets[2] + status.getFile().getPath();

		stb.append(statusLabel + SEP);

		return stb.toString();
	}

	/**
	 * SvnKitで取得したSubversionのステータス情報(SVNStatus)を文字列に加工する。
	 *
	 * @param moduleName
	 * @param svnStatusList
	 * @return
	 */
	public String makeSvnStatusStr(List<SVNStatus> svnStatusList) {
		StringBuilder stb = new StringBuilder();

		for (SVNStatus status : svnStatusList) {
			stb.append(makeSvnStatusStr(status));
		}

		return stb.toString();
	}
}
