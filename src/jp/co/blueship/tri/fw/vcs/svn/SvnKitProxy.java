
package jp.co.blueship.tri.fw.vcs.svn;

import static jp.co.blueship.tri.fw.cmn.utils.TriStringUtils.*;
import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;
import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates.*;

import java.io.File;
import java.util.List;

import org.tmatesoft.svn.core.ISVNDirEntryHandler;
import org.tmatesoft.svn.core.SVNCommitInfo;
import org.tmatesoft.svn.core.SVNDepth;
import org.tmatesoft.svn.core.SVNDirEntry;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNProperties;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.internal.io.dav.DAVRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.fs.FSRepositoryFactory;
import org.tmatesoft.svn.core.internal.io.svn.SVNRepositoryFactoryImpl;
import org.tmatesoft.svn.core.internal.wc.DefaultSVNOptions;
import org.tmatesoft.svn.core.wc.ISVNStatusHandler;
import org.tmatesoft.svn.core.wc.SVNClientManager;
import org.tmatesoft.svn.core.wc.SVNCommitClient;
import org.tmatesoft.svn.core.wc.SVNCopyClient;
import org.tmatesoft.svn.core.wc.SVNCopySource;
import org.tmatesoft.svn.core.wc.SVNDiffClient;
import org.tmatesoft.svn.core.wc.SVNLogClient;
import org.tmatesoft.svn.core.wc.SVNRevision;
import org.tmatesoft.svn.core.wc.SVNStatusClient;
import org.tmatesoft.svn.core.wc.SVNUpdateClient;
import org.tmatesoft.svn.core.wc.SVNWCClient;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignEntryKeyByScm;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 *
 * TMate SvnKitを使用したSubversionアクセス処理を隠蔽するためのプロキシクラス
 *
 * @author Takayuki kubo
 *
 */
public class SvnKitProxy {

	protected static final ILog log = TriLogFactory.getInstance();
	protected static final String SEP = SystemProps.LineSeparator.getProperty();

	protected SVNClientManager svnClientManager = null;


	/**
	 * デフォルトコンストラクタです。
	 */
	SvnKitProxy() {
	}

	/**
	 * コンストラクタ
	 *
	 * @param userName ユーザ名
	 * @param password パスワード
	 */
	public SvnKitProxy(String userName, String password) {

		setupLibrary();

		this.svnClientManager = createSVNClientManager(userName, password);
	}

	/**
	 * SVNClientManagerのインスタンスを新規生成します。
	 *
	 * @param userName  ユーザ名
	 * @param password  パスワード
	 * @return SVNClientManagerのインスタンス
	 */
	private SVNClientManager createSVNClientManager(String userName, String password) {

		DefaultSVNOptions svnOptions = SVNWCUtil.createDefaultOptions(true);

		String globalIgnores = DesignSheetFactory.getDesignSheet().getValue(AmDesignEntryKeyByScm.ignorePatterns);
		svnOptions.setIgnorePatterns(deriveIgnorePatterns(globalIgnores));

		return SVNClientManager.newInstance(svnOptions, userName, password);
	}

	private String[] deriveIgnorePatterns(String globalIgnores) {

		if (TriStringUtils.isEmpty(globalIgnores)) {
			return new String[0];
		}

		List<String> ignorePatterns = select(split(globalIgnores, " "), IS_NOT_EMPTY_STRING);

		return ignorePatterns.toArray(new String[ignorePatterns.size()]);
	}

	/**
	 * ライブラリの初期化
	 *
	 */
	private void setupLibrary() {
		// For using over http:// and https://
		DAVRepositoryFactory.setup();

		// For using over svn:// and svn+xxx://
		SVNRepositoryFactoryImpl.setup();

		// For using over file:///
		FSRepositoryFactory.setup();
	}

	/**
	 * アップデートを行う。<br>
	 * 管理情報(.svn)を含めた状態で資産を取得します。<br>
	 *
	 * @param svnUrl  取得元URL
	 * @param dist 取得先パス
	 * @param pegRevision  基点となるリビジョン
	 * @param revision 取得リビジョン
	 * @throws SVNException
	 */
	long update(String localPath, SVNRevision revision) throws SVNException {

		SVNUpdateClient client = this.svnClientManager.getUpdateClient();

		long ret = client.doUpdate(new File(localPath), revision, SVNDepth.INFINITY, true, false);

		return ret;
	}

	/**
	 * ローカル資産の、リポジトリへの追加宣言を行う。<br>
	 *
	 * @param pathArray 追加資産フルパスの配列
	 * @throws SVNException
	 */
	void add(File[] pathArray) throws SVNException {

		SVNWCClient client = this.svnClientManager.getWCClient();

		client.doAdd(pathArray, false, // 既にバージョン管理済みの資産の扱い true:追加指定されても無視する
										// false:エラーとする
				false, // ディレクトリの作成
				false, // 使われていないらしい
				SVNDepth.EMPTY, // 指定された資産以外の自動追加を許可しない
				true, false, // 除外資産を含めない
				true);
	}

	/**
	 * ローカル資産の、リポジトリへの削除宣言を行う。<br>
	 *
	 * @param path 削除資産のフルパス
	 * @throws SVNException
	 */
	void delete(File path) throws SVNException {

		SVNWCClient client = this.svnClientManager.getWCClient();

		client.doDelete(path, false, true, // 実資産を削除する
				false);
	}

	/**
	 * ローカル資産の、リポジトリへのコミットを行う。<br>
	 *
	 * @param pathArray ローカル資産フルパスの配列
	 * @param commitComment コミットコメント
	 * @param changeListArray 未使用 changeList機能は利用していない
	 * @param svnProperties 未使用
	 * @return SVNCommitInfo
	 * @throws SVNException
	 */
	SVNCommitInfo commit(File[] pathArray, String commitComment, String[] changeListArray, SVNProperties svnProperties) throws SVNException {

		SVNCommitClient client = this.svnClientManager.getCommitClient();

		SVNCommitInfo svnCommitInfo = client
				.doCommit(pathArray, false, commitComment, svnProperties, changeListArray, true, false, SVNDepth.INFINITY);

		return svnCommitInfo;
	}

	/**
	 * チェックアウトを行う。<br>
	 * 管理情報(.svn)を含めた状態で資産を取得します。<br>
	 *
	 * @param svnUrl  取得元URL
	 * @param dist 取得先パス
	 * @param pegRevision  基点となるリビジョン
	 * @param revision 取得リビジョン
	 * @throws SVNException
	 */
	void checkout(SVNURL svnUrl, File dist, SVNRevision pegRevision, SVNRevision revision) throws SVNException {

		SVNUpdateClient client = this.svnClientManager.getUpdateClient();

		client.doCheckout(svnUrl, dist, pegRevision, revision, SVNDepth.INFINITY, true);
	}

	/**
	 *
	 * @param path
	 * @param handler
	 * @throws SVNException
	 */
	void status(File path, ISVNStatusHandler handler, SVNDepth svnDepth) throws SVNException {

		SVNStatusClient client = this.svnClientManager.getStatusClient();

		boolean remote = true;
		boolean reportAll = true;
		boolean includeIgnored = false;
		boolean collectParentExternals = false;

		client.doStatus(path, SVNRevision.HEAD, svnDepth, remote, reportAll, includeIgnored, collectParentExternals, //  使われていない
				handler, null);
	}

	/**
	 *
	 * @param path
	 * @param handler
	 * @throws SVNException
	 */
	void listRemote(SVNURL svnUrl, SVNRevision pegRevision, SVNRevision revision, SVNDepth svnDepth, ISVNDirEntryHandler handler) throws SVNException {

		SVNLogClient client = this.svnClientManager.getLogClient();

		boolean fetchLocks = true;
		int entryFields = SVNDirEntry.DIRENT_ALL;

		client.doList(svnUrl, pegRevision, revision, fetchLocks, svnDepth, entryFields, handler);
	}

	/**
	 * Url to Url の資産コピーを行う。タグ/ブランチの作成に使用します。 Subversionのコマンドとしては、svn copy
	 * に相当します。
	 *
	 * @param svnCopySourceArray  コピー元資産の情報
	 * @param destUrl コピー先のURL
	 * @param commitMessage  コピー実行時のコミットコメント
	 * @return コピー実行時のコミット実行結果
	 * @throws SVNException
	 */
	SVNCommitInfo copyUrlToUrl(SVNCopySource[] svnCopySourceArray, String destUrl, String commitMessage) throws SVNException {

		SVNCopyClient client = this.svnClientManager.getCopyClient();

		boolean isMove = false; // true: 元資産を移動する＝削除する
		boolean makeParents = true; // true: dstへのコピー時に必要な親ディレクトリを作成する
		boolean failWhenDstExists = true; // true: dstに既存資産があったときにエラーとする

		SVNProperties svnProperties = null;// new SVNProperties() ;//用途がよくわからない
		SVNURL destSVNURL = SVNURL.parseURIEncoded(destUrl);

		System.out.println("URLtoURL " + " src: " + svnCopySourceArray[0].getURL().toString() + " dst: " + destUrl);

		return client.doCopy(svnCopySourceArray, destSVNURL, isMove, makeParents, failWhenDstExists, commitMessage, svnProperties);
	}

	/**
	 *
	 * @param url1
	 * @param revision1
	 * @param url2
	 * @param revision2
	 * @param destPath
	 * @throws SVNException
	 */
	void merge(SVNURL svnUrl1, SVNRevision revision1, SVNURL svnUrl2, SVNRevision revision2, File destPath) throws SVNException {

		SVNDiffClient client = this.svnClientManager.getDiffClient();

		boolean useAncestry = true;
		boolean force = true;
		boolean dryRun = false;
		boolean recordOnly = false;

		client.doMerge(svnUrl1, revision1, svnUrl2, revision2, destPath, SVNDepth.INFINITY, useAncestry, force, dryRun, recordOnly);

	}

	/**
	 * リビジョン番号の妥当性チュエックを行います。
	 *
	 * @param revisionNo リビジョン番号
	 * @return 有効な場合はtrueを返す。
	 */
	boolean isValidRevisionNumber(Long revisionNo) {
		return SVNRevision.isValidRevisionNumber(revisionNo);
	}

	/**
	 * 与えられた引数パラメータリストの正当性チェックを行う。
	 *
	 * @param paramArray  期待する引数パラメータの配列
	 * @param paramMap  与えられた引数パラメータのMap
	 */
	/* validator導入により使用されなくなったメソッド t.ono
	protected void validate(SvnParamConsts[] paramArray, Map<SvnParamConsts, Object> paramMap) {

		StringBuilder stb = new StringBuilder();
		boolean isErr = false;

		if (paramArray.length != paramMap.size()) {
			stb.append("与えられた引数パラメータリストの個数(" + paramMap.size() + ")と、期待するパラメータの個数(" + paramArray.length + ")が一致しません。" + SEP);
			isErr = true;
		}

		SvnParamConsts[] resultArray = SvnParamConsts.validate(paramArray, paramMap);
		if (null != resultArray) {
			for (SvnParamConsts result : resultArray) {
				stb.append("「" + result.getDescription() + "」 がセットされていないか、データ型が一致しません。" + SEP);
			}
			isErr = true;
		}

		if (isErr) {
			throw new SystemException("パラメータエラーです。" + SEP + stb.toString());
		}
	}*/

}
