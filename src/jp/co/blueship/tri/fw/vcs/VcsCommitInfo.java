package jp.co.blueship.tri.fw.vcs;

import java.util.Date;

/**
 * 業務ロジックからSvnkitを切り離すための、型クラスです。
 * SVNCommitInfoから情報を取り出す際には、このクラスで受け渡しを行います。
 * @author Takashi Ono
 *
 */
public class VcsCommitInfo {

	private long myNewRevision;
	private Date myDate;
	private String myAuthor;
	private String myErrorMessage;

	public long getNewRevision(){
		return myNewRevision;
	}
	public String getAuthor(){
		return myAuthor;
	}
	public Date getDate(){
		return myDate;
	}
	public String getErrorMessage(){
		return myErrorMessage;
	}
	public void setAll(long revision, String Author ,Date date, String ErrorMessage){
		myNewRevision = revision;
		myAuthor = Author;
		myDate = date;
		myErrorMessage = ErrorMessage;
	}
	public void setNewRevision(long revision){
		myNewRevision = revision;
	}
	public void setAuthor(String Author){
		myAuthor = Author;
	}
	public void setDate(Date date){
		myDate = date;
	}
	public void setErrorMessage(String ErrorMessage){
		myErrorMessage = ErrorMessage;
	}

}