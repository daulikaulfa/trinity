package jp.co.blueship.tri.fw.cmn.io;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 指定されたファイルを読み込み、文字コードセットを変更して別のファイルへ出力する<br>
 * <pre>
 * [1]読込元ファイルの設定情報をセット
 * 		「パスまたはファイルハンドラ」	：【いずれか必須】
 * 		「文字コードセット」			：【任意：Null時は"JISAutoDetect"で判別】
 * [2]出力先ファイルの設定情報をセット
 * 		「パスまたはファイルハンドラ」	：【いずれか必須】
 * 		「文字コードセット」			：【必須】
 * [3]出力先ファイルの改行コードをセット【任意：Null時はJavaが自動セット】
 * [4]convertFile()を実行する
 * </pre>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class BinaryDiffer {

	//読み込み元ファイル設定
	private String srcFileEncoding = null ;
	private String srcPath = null ;
	private File srcFile = null ;
	//読み込み先ファイル設定
	private String dstFileEncoding = null ;
	private String dstPath = null ;
	private File dstFile = null ;

	private boolean holdTimestamp = false ;	//コピー元⇒先のタイムスタンプ保持 true : 保持する false : 保持しない
	private String dstFileLinefeedCode = null ;//出力先ファイルに指定する改行コード（Null時は文字コードセット別のデフォルトを用いる）

	/**
	 * ファイルの文字コード変換処理
	 * @throws Exception
	 */
	public void convertFile() throws FileConvertCharsetException {

		//入力元ファイルの実在をチェック
		if( null == this.srcFile ) {
			if( null == this.srcPath ) {
				throw new FileConvertCharsetException( SmMessageId.SM004033F , this.srcPath) ;
			}
			this.srcFile = new File( this.srcPath ) ;
		}
		if( true != this.srcFile.exists() ) {
			throw new FileConvertCharsetException( SmMessageId.SM004034F , this.srcPath ) ;
		}
		if( true != this.srcFile.isFile() ) {
			throw new FileConvertCharsetException( SmMessageId.SM004035F , this.srcPath) ;
		}
		if( true != this.srcFile.canRead() ) {
			throw new FileConvertCharsetException( SmMessageId.SM004036F , this.srcPath ) ;
		}

		//出力先ファイルパスの状態をチェック
		if( null == this.dstFile ) {
			if( null == this.dstPath ) {
				throw new FileConvertCharsetException( SmMessageId.SM004037F , this.dstPath) ;
			}
			this.dstFile = new File( this.dstPath ) ;
		}
		if( true == this.dstFile.isDirectory() ) {
			throw new FileConvertCharsetException( SmMessageId.SM004038F , this.dstPath );
		}

		FileInputStream fis = null ;
		InputStreamReader isr = null ;

		FileOutputStream fos = null ;
		OutputStreamWriter osw = null ;

		BufferedReader br = null ;
		BufferedWriter bw = null ;
		try {
			//読み込み側生成
			String encode = ( null == this.srcFileEncoding ) ? "JISAutoDetect" : this.srcFileEncoding ;
			fis = new FileInputStream( this.srcFile ) ;
			isr = new InputStreamReader( fis , encode ) ;
			br = new BufferedReader( isr ) ;

			//書き込み側生成
			fos = new FileOutputStream( this.dstFile ) ;
			osw = new OutputStreamWriter( fos , this.dstFileEncoding ) ;
			bw = new BufferedWriter( osw ) ;

			//１行ずつ読み出し、コンバートしつつ書き込む
			while( true ) {
				String str = br.readLine() ;
				if( null == str ) {
					break ;
				}
				bw.write( str ) ;
				//改行
				if( null == this.dstFileLinefeedCode ) {//Null時は文字コードセット別のデフォルトを用いる
					bw.newLine() ;//改行
				} else {//指定された改行コードを用いる
					bw.write( this.dstFileLinefeedCode ) ;
				}
			}

			if( true == this.holdTimestamp ) {
				//ファイルの最終更新日時をコピー元から複写
				long srcModified = srcFile.lastModified() ;
				if( false == dstFile.setLastModified( srcModified ) ) {
					throw new FileConvertCharsetException( SmMessageId.SM004039F , this.dstPath) ;
				}
			}
		} catch ( Exception e ) {
			this.dstFile.delete() ;
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new FileConvertCharsetException( SmMessageId.SM005070S , e ) ;
		} finally {
			TriFileUtils.closeWithFlush(bw);
			TriFileUtils.close(br);
			TriFileUtils.close(osw);
			TriFileUtils.close(isr);
			TriFileUtils.close(fos);
			TriFileUtils.close(fis);
		}
	}

	/**
	 * 出力先ファイルに指定する改行コードを取得する<br>
	 * @return 改行コード
	 */
	public String getDstFileLinefeedCode() {
		return dstFileLinefeedCode;
	}

	/**
	 * 出力先ファイルに指定する改行コードをセットする<br>
	 * （Null時は文字コードセット別のデフォルトを用いる）<br>
	 * @param dstFileLinefeedCode 改行コード
	 */
	public void setDstFileLinefeedCode(String toFileLinefeedCode) {
		this.dstFileLinefeedCode = toFileLinefeedCode;
	}

	public File getDstFile() {
		return dstFile;
	}

	public void setDstFile(File dstFile) {
		this.dstFile = dstFile;
	}

	public String getDstFileEncoding() {
		return dstFileEncoding;
	}

	public void setDstFileEncoding(String dstFileEncoding) {
		this.dstFileEncoding = dstFileEncoding;
	}

	public String getDstPath() {
		return dstPath;
	}

	public void setDstPath(String dstPath) {
		this.dstPath = dstPath;
	}

	public File getSrcFile() {
		return srcFile;
	}

	public void setSrcFile(File srcFile) {
		this.srcFile = srcFile;
	}

	public String getSrcFileEncoding() {
		return srcFileEncoding;
	}

	public void setSrcFileEncoding(String srcFileEncoding) {
		this.srcFileEncoding = srcFileEncoding;
	}

	public String getSrcPath() {
		return srcPath;
	}

	public void setSrcPath(String srcPath) {
		this.srcPath = srcPath;
	}

	public boolean isHoldTimestamp() {
		return holdTimestamp;
	}

	public void setHoldTimestamp(boolean holdTimestamp) {
		this.holdTimestamp = holdTimestamp;
	}
}
