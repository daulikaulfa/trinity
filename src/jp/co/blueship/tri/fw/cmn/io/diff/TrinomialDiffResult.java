package jp.co.blueship.tri.fw.cmn.io.diff;

import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;

/**
 * ３点Diffの結果を格納するクラスです。<br>
 * 「比較元」、「比較先」、「マージ状態」の３状態を想定。<br>
 * <br>
 * 比較元、比較先の内容の等しい部分が、同じ行に含まれるように格納します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TrinomialDiffResult implements IDiffElement {

	private static final long serialVersionUID = 1L;



	private long srcLineNum;		//比較元の行数
	private String srcContents;	//比較元の内容

	private long dstLineNum;		//マージ状態の行数
	private String dstContents;	//マージ状態の内容

	private long mergedLineNum;	//比較先の行数
	private String mergedContents;	//比較先の内容

	private Status statusSrcMerge;	//比較元＆マージ状態の変更の状態
	private Status statusMergeDst;//マージ状態＆比較先の変更の状態

	/**
	 * デフォルトコンストラクタ
	 * 何もしません。
	 */
	@SuppressWarnings("unused")
	private TrinomialDiffResult(){}

	/**
	 * コンストラクタ
	 * @param srcLineNum		比較元の行数
	 * @param srcContents		比較元の内容
	 * @param mergedLineNum		マージ状態の行数
	 * @param mergedContents	マージ状態の内容
	 * @param dstLineNum		比較先の行数
	 * @param dstContents		比較先の内容
	 * @param statusSrcMerge	比較元＆マージ状態の変更の状態
	 * @param statusMergeDst	マージ状態＆比較先の変更の状態
	 */
	public TrinomialDiffResult(
			long srcLineNum, String srcContents,
			long mergedLineNum , String mergedContents ,
			long dstLineNum, String dstContents ,
			Status statusSrcMerge , Status statusMergeDst ) {

		this.srcLineNum			= srcLineNum ;
		this.srcContents		= srcContents ;
		this.mergedLineNum		= mergedLineNum ;
		this.mergedContents		= mergedContents ;
		this.dstLineNum			= dstLineNum ;
		this.dstContents		= dstContents ;
		this.statusSrcMerge		= statusSrcMerge ;
		this.statusMergeDst		= statusMergeDst ;
	}

	/**
	 * 比較元の行数を取得します。
	 */
	public long getSrcLineNumber() {
		return this.srcLineNum;
	}
	/**
	 * 比較元の行数を設定します。
	 */
	public void setSrcLineNumber( long srcLineNum ) {
		this.srcLineNum = srcLineNum;
	}

	/**
	 * 比較元の内容を取得します。
	 */
	public String getSrcContents() {
		return this.srcContents;
	}
	/**
	 * 比較元の内容を設定します。
	 */
	public void setSrcContents( String srcContents ) {
		this.srcContents = srcContents;
	}

	/**
	 * 比較先の行数を取得します。
	 */
	public long getDstLineNumber() {
		return this.dstLineNum;
	}
	/**
	 * 比較先の行数を設定します。
	 */
	public void setDstLineNumber( long dstLineNum ) {
		this.dstLineNum = dstLineNum;
	}

	/**
	 * 比較先の内容を取得します。
	 */
	public String getDstContents() {
		return this.dstContents;
	}
	/**
	 * 比較先の内容を設定します。
	 */
	public void setDstContents( String dstContents ) {
		this.dstContents = dstContents;
	}
	/**
	 * マージ状態の行数を取得します。
	 */
	public long getMergedLineNumber() {
		return this.mergedLineNum;
	}
	/**
	 * マージ状態の行数を設定します。
	 */
	public void setMergedLineNumber( long mergedLineNum ) {
		this.mergedLineNum = mergedLineNum;
	}
	/**
	 * マージ状態の内容を取得します。
	 */
	public String getMergedContents() {
		return this.mergedContents;
	}
	/**
	 * マージ状態の内容を設定します。
	 */
	public void setMergedContents( String mergedContents ) {
		this.mergedContents = mergedContents;
	}

	/**
	 * 変更の状態を取得します。
	 * @deprecated
	 */
	public Status getStatus() {
		return null ;
	}
	/**
	 * 変更の状態を設定します。
	 * @deprecated
	 */
	public void setStatus( Status status ) {
		;
	}

	/**
	 * 比較元＆マージ状態の変更の状態を取得します。
	 */
	public Status getStatusSrcMerge() {
		return this.statusSrcMerge;
	}
	/**
	 * 比較元＆マージ状態の変更の状態を設定します。
	 */
	public void setStatusSrcMerge( Status status ) {
		this.statusSrcMerge = status;
	}
	/**
	 * マージ状態＆比較先の変更の状態を取得します。
	 */
	public Status getStatusMergeDst() {
		return this.statusMergeDst;
	}
	/**
	 * マージ状態＆比較先の変更の状態を設定します。
	 */
	public void setStatusMergeDst( Status status ) {
		this.statusMergeDst = status;
	}
}