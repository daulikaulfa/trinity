package jp.co.blueship.tri.fw.cmn.io;

import java.io.FileFilter;

public interface IFilterCopy extends ICopy {
	
	public void setFileFilter(FileFilter fileFilter);
	public FileFilter getFileFilter();
	
}
