package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.FileFilter;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;


public class CopyFilter implements IFilterCopy {

	private FileCopy fileCopy = null;
	private boolean synchronizedList = false;

	private IContextAdapter ac;

	private FileFilter fileFilter = null ;

	private List<File> constructList(){

		List<File> l = new ArrayList<File>();
		try {
			if(synchronizedList){
				l = Collections.synchronizedList(l);
			}

		} catch (Exception e) {
			throw new TriSystemException(SmMessageId.SM005071S, e);
		}

		return l;

	}

	public List<File> copy(File src, File dst) throws IOException {
		return copy( src, dst, true );
	}

	public List<File> copy(File src, File dst, boolean isCopyRootDir) throws IOException {

		List<File> duplicateList = constructList();

		if(src == null || dst == null){
			throw new NullPointerException();
		}

		if(! src.exists()){
			throw new FileNotFoundException( contextAdapter().getMessage( SmMessageId.SM004040F , src.toString() ) );
		}


		if(src.isFile()){
			// file copy

			// fileから、存在しないfile指定
			if(! dst.exists()){
			    File p = dst.getParentFile();
			    if(! p.exists()){
			        dst.getParentFile().mkdirs();
			    }
				fileCopy.copyFileToFile(src, dst, duplicateList) ;

			}else if(dst.exists()){
				if(dst.isFile()){
					// fileから、存在するfile指定。
					fileCopy.copyFileToFile(src, dst, duplicateList);
				}else if(dst.isDirectory()){
					// fileから、存在するdirectory指定。
					File out = new File(dst.getPath() + File.separator + src.getName());
					fileCopy.copyFileToFile(src, out, duplicateList);
				}else {
					throw new IllegalArgumentException( contextAdapter().getMessage( SmMessageId.SM004041F , dst.toString() ) );
				}
			}
		}else if(src.isDirectory()){

			// directory copy
			if(dst.exists()){
				// copy先が存在する場合
				if(dst.isFile()){
					// copy先がfileの場合はexception。
					throw new IllegalArgumentException( contextAdapter().getMessage( SmMessageId.SM004042F , dst.toString() ) );
				}else if(dst.isDirectory()){
					File d = null;
					if ( isCopyRootDir ) {
						// copy先がdirectoryの場合は配下をcopy。
						d = new File(dst.getPath() + File.separator + src.getName());
						d.mkdir();
					} else {
						d = new File(dst.getPath());
					}

					File files[] = src.listFiles( fileFilter );
					int i = 0;
					for(i = 0; i < files.length; i++){
						List<File> tmpList = copy(files[i], d);
						duplicateList.addAll(tmpList);
					}
				}
			}else{
				// copy先が存在しない場合。
				dst.mkdirs();
				File files[] = src.listFiles( fileFilter );

				int i = 0;
				for(i = 0; i < files.length; i++){
					copy(files[i], dst);
				}
			}
		}

		return duplicateList;
	}

	public void setFileCopy(FileCopy fileCopy) {
		this.fileCopy = fileCopy;
	}

	public FileCopy getFileCopy() {
		return fileCopy;
	}

	public void setSynchronizedList(boolean synchronizedList) {
		this.synchronizedList = synchronizedList;
	}

	public boolean isSynchronizedList() {
		return synchronizedList;
	}

	public FileFilter getFileFilter() {
		return fileFilter;
	}

	public void setFileFilter(FileFilter fileFilter) {
		this.fileFilter = fileFilter;
	}

	private IContextAdapter contextAdapter() {

		if (ac == null) {
			ac = ContextAdapterFactory.getContextAdapter();
		}

		return ac;
	}
}
