package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

public interface ITouch {


	/**
	 * 日付を指定した日付更新処理。
	 * <code>target</code>がディレクトリの場合日付を更新せず、中のファイルのみ処理する。
	 * この処理は、{@link this#touch(File, -1L, null)}と等価です。
	 * @param target 日付更新対象のファイル
	 * @return 1個でも更新できなかった場合false
	 * @throws IOException
	 */	public boolean touch(File target) throws IOException;

	/**
	 * 日付を指定した日付更新処理。
	 * <code>target</code>がディレクトリの場合日付を更新せず、中のファイルのみ処理する。
	 * 現在のシステム日付を設定する場合は、{@link System#currentTimeMillis()}を設定する。
	 * @param target 日付更新対象のファイル
	 * @param time 更新する時刻
	 * @param patternList 処理対象とするfileの正規表現パターンリスト
	 * @return 1個でも更新できなかった場合false
	 * @throws IOException
	 */
	public boolean touch(File target, long time, List<Pattern> patternList) throws IOException;

}
