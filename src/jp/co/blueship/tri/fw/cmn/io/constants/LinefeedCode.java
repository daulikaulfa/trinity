package jp.co.blueship.tri.fw.cmn.io.constants;
/**
 * 改行コードを列挙する<br>
 * 適宜追加のこと。
 * <br> 
 * <p>
 * </p>
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public enum LinefeedCode {
		
	CR		( "\r" ) ,
	LF		( "\n" ) ,
	CRLF	( "\r\n" ) ;
	
	private String value = null ;
		
	private LinefeedCode( String value ) {
		this.value = value ;
	}
		
	public String toString() {
		return this.value ;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public static LinefeedCode getLabel( String value ) {
		for ( LinefeedCode linefeedCode : values() ) {
 			if ( linefeedCode.name().equals( value ) ) {
				return linefeedCode ;
			}
		}
		return null;
	}
	
	public static LinefeedCode getValue( String value ) {
		for ( LinefeedCode linefeedCode : values() ) {
 			if ( linefeedCode.toString().equals( value ) ) {
				return linefeedCode ;
			}
		}
		return null;
	}
}
