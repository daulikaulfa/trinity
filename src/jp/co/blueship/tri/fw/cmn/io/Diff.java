package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * バイナリDiffを行う<br>
 * ディレクトリ同士が与えられた場合は、再帰的に動作する。<br>
 *
 */
public class Diff implements IDiff {

	private boolean scmNeglect = true ;//default=true : SCM関連フォルダ・ディレクトリを無視
	private boolean doReverse = true ;

	/**
	 * SCM関連フォルダ・ディレクトリを無視するフラグを取得する<br>
	 * @return SCM関連フォルダ・ディレクトリを無視するフラグ
	 */
	public boolean isScmNeglect() {
		return scmNeglect;
	}
	/**
	 * SCM関連フォルダ・ディレクトリを無視するフラグをセットする<br>
	 * @param scmNeglect SCM関連フォルダ・ディレクトリを無視するフラグ
	 * <pre>
	 * 		true	:	SCM関連フォルダ・ディレクトリを無視する
	 * 		false	:	SCM関連フォルダ・ディレクトリを無視しない
	 * </pre>
	 */
	public void setScmNeglect(boolean scmNeglect) {
		this.scmNeglect = scmNeglect;
	}
	/**
	 * 逆方向比較を行うフラグを取得する<br>
	 * @return 逆方向比較を行うフラグ
	 */
	public boolean isDoReverse() {
		return doReverse;
	}
	/**
	 * 逆方向比較を行うフラグをセットする<br>
	 * @param doReverse 逆方向比較を行うフラグ
	 * <pre>
	 * 		true	:	逆方向比較を行う
	 * 		false	:	逆方向比較を行わない
	 * </pre>
	 */
	public void setDoReverse(boolean doReverse) {
		this.doReverse = doReverse;
	}


	/**
	 * 再帰的にフォルダ構造を辿り、diff処理を実行してリストを出力する<br>
	 * 順方向（追加・変更を検出）および逆方向（削除を検出）を行う。<br>
	 * @param srcPath 比較元パス
	 * @param diffPath 比較先パス
	 * @return 比較結果リスト
	 * @throws Exception
	 */
	public List<DiffResult> execute( String srcPath , String diffPath ) throws Exception {
		List<DiffResult> diffList = new ArrayList<DiffResult>() ;

		diffProc( DiffMode.forward , srcPath , srcPath , diffPath , diffList ) ;
		if( true == doReverse ) {//逆方向スキャンにより、削除ファイルを検出する
			diffProc( DiffMode.reverse , diffPath , diffPath , srcPath , diffList ) ;
		}
		return diffList ;
	}

	/**
	 * 再帰的にフォルダ構造を辿り、diff処理を実行していく。<br>
	 * @param copy
	 * @param diffMap
	 * @param srcPath
	 * @param dstPath
	 * @return
	 * @throws Exception
	 */
	public void diffProc(	DiffMode diffMode ,
							String basePath ,
							String srcPath ,
							String diffPath ,
							List<DiffResult> diffList ) throws Exception {

		File file = new File( srcPath ) ;
		if( true != file.exists() ) {
			throw new TriSystemException( SmMessageId.SM004043F , srcPath) ;
		}
		if( true == scmNeglect && true == TriFileUtils.isSCM( file ) ) {//SCM管理のファイル・ディレクトリは処理しない
			return ;
		}

		if( file.isDirectory() ) {//ディレクトリ
			//log.debug( "ディレクトリ：" + srcPath ) ;
			File[] files = file.listFiles() ;
			if( null == files ) {
				throw new TriSystemException( SmMessageId.SM004044F , srcPath) ;
			}
			for( File tmpFile : files ) {
				diffProc( diffMode , basePath ,
						TriStringUtils.linkPathBySlash( srcPath , tmpFile.getName() ) ,
						TriStringUtils.linkPathBySlash( diffPath , tmpFile.getName() ) ,
						diffList ) ;
			}
		} else {//ファイル
			//log.debug( "ファイル  ：" + file.getName() ) ;
			//String newPath = FileUtil.linkPathBySlash( dstPath , filePath.getObjName() ) ;

			//diff処理
			Status status = null ;
			if( DiffMode.forward.equals( diffMode ) ) {
				status = this.diffFile( srcPath , diffPath ) ;
			} else {//逆方向diff時は「追加（削除）」を検出するだけなのでdiff処理をスキップする
				status = ( true == new File( diffPath ).isFile() ) ? Status.EQUAL : Status.DELETE ;
			}

			if( !Status.EQUAL.equals( status ) ) {//diff不一致
				String srcPathOrg = TriStringUtils.convertPath( basePath ) ;
				String relativePath = TriStringUtils.convertRelativePath( new File( srcPathOrg ) , srcPath ) ;
				relativePath = ( '/' == relativePath.charAt( 0 ) ) ? relativePath.substring( 1 ) : relativePath ;//先頭が"/"ならば除去
				//resultを保存
				DiffResult diffResult = new DiffResult() ;
				diffResult.setStatus( status ) ;
				diffResult.setRelativePath( relativePath ) ;
				diffList.add( diffResult ) ;
			}
		}
	}

	/**
	 * ファイル同士のDiff比較を行う<br>
	 * @param srcPath	比較元ファイルパス
	 * @param diffPath	比較先ファイルパス
	 * @return 判定結果
	 * <pre>
	 * 		true 	:	コピーする（diff対象外 or diffで差分があったもの）
	 * 		false	:	コピーしない（diffで差分がなかったもの）
	 * </pre>
	 * @throws Exception
	 */
	public Status diffFile( String srcPath , String diffPath ) throws Exception {
		File srcFile = new File( srcPath ) ;
		File diffFile = new File( diffPath ) ;
		if( true != srcFile.isFile() || true != srcFile.canRead() ) {
			throw new TriSystemException( SmMessageId.SM004045F , srcPath) ;
		}
		if( true == diffFile.isDirectory() ) {
			throw new TriSystemException( SmMessageId.SM004046F , diffPath) ;
		}
//		log.debug( "パスの相対部分は " + relativePath + " @ " + destPath + " @ " + srcPathOrg ) ;

		//diffのチェック
		if( true != diffFile.exists() ) {//比較先ファイルが存在しない
			return Status.ADD ;
		}
		if( true == TriFileUtils.isSame( srcFile, diffFile ) ) {//一致
			return Status.EQUAL ;
		} else {//不一致
			return Status.CHANGE ;
		}
	}
}
