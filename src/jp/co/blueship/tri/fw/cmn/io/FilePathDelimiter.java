package jp.co.blueship.tri.fw.cmn.io;

public class FilePathDelimiter {

	

	//private static final char DELIMITER_CHAR = '/' ;
	public static final String DELIMITER = String.valueOf( "/" ) ;//DELIMITER_CHAR ) ;

	private boolean isEnabled = false ;
	private String parentDir = null ;
	private String objName = null ;

	/**
	 *
	 * @param fullPath
	 * @throws Exception
	 */
	public FilePathDelimiter( String fullPath ) throws Exception {
		if( null == fullPath ) {
			return ;
		}
		try {
			int pos = fullPath.lastIndexOf( DELIMITER ) ;
			/*if( -1 == pos ) {// "C:/" を "C:"と指定した場合など
			fullPath += DELIMITER ;
			}*/
			if( 0 == pos ) {
				return ;
			}
			parentDir = fullPath.substring( 0 , fullPath.lastIndexOf( DELIMITER ) ) ;
			objName = fullPath.substring( fullPath.lastIndexOf( DELIMITER ) + 1 ) ;
			isEnabled = true ;
		} catch ( Exception e ) {
			isEnabled = false ;
		}
	}

	public String getObjName() {
		return objName;
	}

	public void setObjName(String fileName) {
		this.objName = fileName;
	}

	public String getParentDir() {
		return parentDir;
	}

	public void setParentDir(String parentDir) {
		this.parentDir = parentDir;
	}

	public boolean isEnabled() {
		return isEnabled;
	}
}
