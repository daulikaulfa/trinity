package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class StreamUtils {

	public static byte[] convertInputStreamToBytes( InputStream is ) {

		if ( null == is ) return null;

		ByteArrayOutputStream b = new ByteArrayOutputStream();
		OutputStream os = new BufferedOutputStream( b );
		int c;

		try {
			while ( ( c = is.read() ) != -1 ) {
				os.write( c );
			}
		} catch ( IOException e ) {
			throw new TriSystemException( SmMessageId.SM005087S , e );
		} finally {
			TriFileUtils.closeOfOutputStream(os);
		}

		return b.toByteArray();
	}

	public static InputStream convertBytesToInputStreamToBytes( byte[] b ) {

		if ( null == b ) return null;

		return new ByteArrayInputStream( b );
	}

	public static void download( HttpServletResponse response ,File file ) throws IOException {
		download( response, file, file.getName() );
	}

	public static void download( HttpServletResponse response ,File file, String fileName ) throws IOException {

		FileInputStream in = null;

		try {
			response.reset();
			response.setContentType("application/octet-stream");
			String encodeFileName = URLEncoder.encode(fileName, Charset.UTF_8.value());
			response.setHeader("Content-Disposition", String.format("attachment; filename=\"%s\"", encodeFileName));
			response.setHeader("Content-Length", String.valueOf( TriFileUtils.sizeOf(file) ));

			in = new FileInputStream(file);
			IOUtils.copy(in,response.getOutputStream());

		} catch (FileNotFoundException e) {
			PreConditions.assertOf(file.exists(), file + " does not exist");
		} finally {
			if ( null != in ) {
				in.close();
			}
		}
	}
}
