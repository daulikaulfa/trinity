package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ファイルの行数取得処理クラス
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class FileCounter {

	private File sourceFile = null;
	private List<String> commentSymbol = null;

	/**
	 * コンストラクタです。
	 *
	 * @param sourceFile 行数取得の対象ファイル
	 */
	public FileCounter(File sourceFile) throws TriSystemException {
		this.sourceFile = sourceFile;
	}

	/**
	 * コンストラクタです。
	 *
	 * @param sourceFile 行数取得の対象ファイル
	 * @param commentSymbol コメントとして扱う文字列のリスト
	 */
	public FileCounter(File sourceFile, List<String> commentSymbol) throws TriSystemException {

		this.sourceFile = sourceFile;
		this.commentSymbol = commentSymbol;
	}

	private BufferedReader openFile(File sourceFile) throws TriSystemException {

		FileInputStream fis = null;
		InputStreamReader isr = null;
		BufferedReader br = null;

		try {
			fis = new FileInputStream(sourceFile.getAbsolutePath());
			isr = new InputStreamReader(fis);
			br = new BufferedReader(isr);

			return br;

		} catch (FileNotFoundException ex) {
			throw new TriSystemException(SmMessageId.SM004052F, ex);
		} finally {
			TriFileUtils.close(br);
			TriFileUtils.close(isr);
			TriFileUtils.close(fis);
		}
	}

	/**
	 * ソースの行数(コメントで始まる行数以外)を取得します。
	 *
	 * @return
	 */
	public long getSourceLineCount() throws TriSystemException {

		long sum = 0;
		String line = null;

		BufferedReader br = openFile(this.sourceFile);

		try {

			while ((line = br.readLine()) != null) {

				if (!startWithComment(line)) {
					sum++;
				}
			}
			return sum;

		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM005077S, ioe);
		} finally {
			TriFileUtils.close(br);
		}

	}

	/**
	 * コメントで始まる行数以外を取得します。
	 *
	 * @return
	 */
	public long getCommentLineCount() throws TriSystemException {

		long sum = 0;
		String line = null;

		BufferedReader br = openFile(this.sourceFile);

		try {

			while ((line = br.readLine()) != null) {

				if (startWithComment(line)) {
					sum++;
				}
			}

			return sum;

		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM005077S, ioe);
		} finally {
			TriFileUtils.close(br);
		}

	}

	/**
	 * 対象文字列がコメントから始まっているかをチェックします。
	 *
	 * @param line 対象文字列
	 * @return コメントから始まっていればtrue、そうでなければfalse
	 */
	private boolean startWithComment(String line) {

		if (this.commentSymbol != null) {

			for (String comment : this.commentSymbol) {

				if (line.trim().startsWith(comment)) {
					return true;
				}
			}
		}

		return false;
	}
}