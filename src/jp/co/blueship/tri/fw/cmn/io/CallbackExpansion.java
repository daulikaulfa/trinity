package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

public interface CallbackExpansion {

	

	/**
	 * src/dstがfileだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 */
	public void doFileFile(File src, File dst, String relatePath);

	/**
	 * srcがdirectoryで存在し、dstが存在しない場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>
	 */
	public boolean doDirectoryNothing(File src, File dst, String relatePath);

	/**
	 * src/dstがfileだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 */
	public void doFileNothing(File src, File dst, String relatePath);

	/**
	 * src/dst共にdirectoryの場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>
	 */
	public boolean doDirectoryDirectory(File src, File dst, String relatePath);


	/**
	 * srcがdirectory、dstがfileの場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>
	 */
	public boolean doDirectoryFile(File src, File dst, String relatePath);

	/**
	 * srcがfile,dstがdirectoryだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 * @param relatePath 相対パス
	 */
	public void doFileDirectory(File src, File dst, String relatePath);

}
