package jp.co.blueship.tri.fw.cmn.io.constants;


/**
 * ＦＴＰ転送時のファイル転送モードを列挙する<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public enum FileIoType {

	ASCII_FILE_TYPE				( 0 ),	//used to indicate the file(s) being transfered should be treated as ASCII.
	EBCDIC_FILE_TYPE			( 1 ),	//used to indicate the file(s) being transfered should be treated as EBCDIC.
	BINARY_FILE_TYPE			( 2 ),	//used to indicate the file(s) being transfered should be treated as a binary image, i.e., no translations should be performed.
	LOCAL_FILE_TYPE				( 3 ),	//used to indicate the file(s) being transfered should be treated as a local type.
	NON_PRINT_TEXT_FORMAT		( 4 ),	//used for text files to indicate a non-print text format.
	TELNET_TEXT_FORMAT			( 5 ),	//used to indicate a text file contains format vertical format control characters.
	CARRIAGE_CONTROL_TEXT_FORMAT( 6 ),	//used to indicate a text file contains ASA vertical format control characters.
	FILE_STRUCTURE				( 7 ),	//used to indicate a file is to be treated as a continuous sequence of bytes
	RECORD_STRUCTURE			( 8 ),	//used to indicate a file is to be treated as a sequence of records.
	PAGE_STRUCTURE				( 9 ),	//used to indicate a file is to be treated as a set of independent indexed pages.
	STREAM_TRANSFER_MODE		( 10 ),	//used to indicate a file is to be transfered as a stream of bytes.
	BLOCK_TRANSFER_MODE			( 11 ),	//used to indicate a file is to be transfered as a series of blocks.
	COMPRESSED_TRANSFER_MODE	( 12 );	//used to indicate a file is to be transfered as FTP compressed data.

	

	private Integer value = 0 ;

	private FileIoType( int value ) {
		this.value = value ;
	}

	public int intValue() {
		return this.value ;
	}

	public String getValue() {
		return value.toString();
	}

	public static FileIoType getValue( int value ) {
		for ( FileIoType define : values() ) {
			if ( define.intValue() == value )
				return define;
		}

		return null;
	}

}
