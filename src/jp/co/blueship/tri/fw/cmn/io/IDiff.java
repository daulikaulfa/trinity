package jp.co.blueship.tri.fw.cmn.io;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;

public interface IDiff {
	
//	diffの処理モード
	public enum DiffMode {
		forward ,	//順方向（追加・変更ファイルを検出する）
		reverse ;	//逆方向（削除ファイルを検出する）
	}
	
	/**
	 * 再帰的にフォルダ構造を辿り、diff処理を実行してリストを出力する<br>
	 * 順方向（追加・変更を検出）および逆方向（削除を検出）を行う。<br>
	 * @param srcPath 比較元パス
	 * @param diffPath 比較先パス
	 * @return 比較結果リスト
	 * @throws Exception
	 */
	public List<DiffResult> execute( String srcPath , String diffPath ) throws Exception ;
	
	/**
	 * 再帰的にフォルダ構造を辿り、diff処理を実行していく。<br>
	 * @param diffMode 処理モード
	 * @param basePath 基底パス
	 * @param srcPath 比較元パス
	 * @param diffPath 比較先パス
	 * @param diffList 比較結果リスト
	 * @throws Exception
	 */
	void diffProc( DiffMode diffMode ,
								String basePath , String srcPath , String diffPath , 
								List<DiffResult> diffList ) throws Exception ;
	
	/**
	 * ファイル同士のDiff比較を行う<br>
	 * @param srcPath	比較元ファイルパス
	 * @param diffPath	比較先ファイルパス
	 * @return 判定結果
	 * <pre>
	 * 		true 	:	コピーする（diff対象外 or diffで差分があったもの）
	 * 		false	:	コピーしない（diffで差分がなかったもの）
	 * </pre>
	 * @throws Exception
	 */
	public Status diffFile( String srcPath , String diffPath ) throws Exception ;
	
		
	/**
	 * Diff結果を格納するための内部クラス
	 *
	 */
	public class DiffResult {
		private Status status = null ;			//Status
		private String relativePath = null ;	//相対パス
		
		
		public Status getStatus() {
			return status;
		}
		public void setStatus(Status status) {
			this.status = status;
		}
		public String getRelativePath() {
			return relativePath;
		}
		public void setRelativePath(String relativePath) {
			this.relativePath = relativePath;
		}
		
	}
}
