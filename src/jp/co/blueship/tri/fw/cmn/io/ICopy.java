package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

public interface ICopy {


	/**
	 * srcからdstへfileをcopyする。<br>
	 * copyするpatternは下記の通り。
	 *
	 * <table border=1>
	 * <tr>
	 * 	<th>src</th><th>dst</th><th>動作</th>
	 * </tr>
	 * <tr>
	 * 	<td>非存在</td><td>*</td><td>Exceptionがthrowされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>非存在</td><td>新規fileがdstに作られ、srcの内容がcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>file(存在)</td><td>{@link #getFileCopy()}で取得される{@link FileCopy}に依存する。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>directory(存在)</td><td>dstの下へcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>非存在</td><td>srcがdstという名前でcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>directory(存在)</td><td>srcの内容がrecorsiveでdst下へcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>file(存在)</td><td>Exceptionがthrowされる。</td>
	 * </tr>
	 *
	 *
	 * </table>
	 *
	 * @param src copy元file or directory.
	 * @param dst copy先file or directory.
	 * @throws IOException
	 */	public List<File> copy(File src, File dst) throws IOException;

	/**
	 * srcからdstへfileをcopyする。<br>
	 * copyするpatternは下記の通り。
	 *
	 * <table border=1>
	 * <tr>
	 * 	<th>src</th><th>dst</th><th>動作</th>
	 * </tr>
	 * <tr>
	 * 	<td>非存在</td><td>*</td><td>Exceptionがthrowされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>非存在</td><td>新規fileがdstに作られ、srcの内容がcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>file(存在)</td><td>{@link #getFileCopy()}で取得される{@link FileCopy}に依存する。</td>
	 * </tr>
	 * <tr>
	 * 	<td>file</td><td>directory(存在)</td><td>dstの下へcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>非存在</td><td>srcがdstという名前でcopyされる。</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>directory(存在)</td><td>srcの内容がrecorsiveでdst下へcopyされる。isCopyRootDir = falseの場合、srcの最初のdirectoryは複写されない</td>
	 * </tr>
	 * <tr>
	 * 	<td>directory</td><td>file(存在)</td><td>Exceptionがthrowされる。</td>
	 * </tr>
	 *
	 *
	 * </table>
	 *
	 * @param src copy元file or directory.
	 * @param dst copy先file or directory.
	 * @param isCopyRootDir copy元の最初のdirectoryをコピーするか
	 * @throws IOException
	 */
	public List<File> copy(File src, File dst, boolean isCopyRootDir) throws IOException;

	public void setFileCopy(FileCopy fileCopy);
	public FileCopy getFileCopy();

	public void setSynchronizedList(boolean synchronizedList);
	public boolean isSynchronizedList();

}
