package jp.co.blueship.tri.fw.cmn.io.diff;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;


/**
 * ３点Ｄｉｆｆを行う。<br>
 * 「比較元」、「比較先」、「マージ状態」の３状態を想定。<br>
 * 「マージ状態」のファイルを軸として、「比較元」、「比較先」を参照する形。
 */
public class TrinomialDiffer {
	/**
	 * 文字コードの比較モード
	 *
	 */
	public enum CharsetMode {

		SAME( "SAME" ),				//３点Diffの両者比較が必ず共通の文字コードを使用する
		SEPARATE( "SEPARATE" ) ;	//３点Diffの両者比較がそれぞれ別の文字コードを使用することを許容する

		String mode = null ;

		CharsetMode( String mode ) {
			this.mode = mode ;
		}

		public String toString() {
			return this.mode ;
		}

		public String getValue() {
			return this.mode;
		}

		public static CharsetMode getValue( String value ) {
			for ( CharsetMode charset : values() ) {
				if ( charset.toString().equals( value ) ) {
					return charset ;
				}
			}
			return null;
		}
	}

	private static final ILog log = TriLogFactory.getInstance() ;

	private File srcFile = null ;			//比較元ファイル
	private File destFile = null ;			//比較先ファイル
	private File mergedFile = null ;		//マージ済ファイル
	private Charset srcCharset = null ;	//比較元ファイルのCharset
	private Charset destCharset = null ;	//比較先ファイルのCharset
	private Charset mergedCharset = null ;	//マージ済みファイルのCharset
	List<String> skipwordList = null ;		//スキップワードのリスト
	private CharsetMode charsetMode = CharsetMode.SAME ;//文字コードの比較モード デフォルトは共通とする

	public CharsetMode getCharsetMode() {
		return charsetMode;
	}
	public void setCharsetMode(CharsetMode charsetMode) {
		this.charsetMode = charsetMode;
	}
	/**
	 * 比較元ファイルを取得する
	 * @return 比較元ファイル
	 */
	public File getDestFile() {
		return destFile;
	}
	/**
	 * 比較元ファイルをセットする
	 * @param destFile 比較元ファイル
	 */
	public void setDestFile(File destFile) {
		this.destFile = destFile;
	}
	/**
	 * 比較先ファイルを取得する
	 * @return 比較先ファイル
	 */
	public File getMergedFile() {
		return mergedFile;
	}
	/**
	 * 比較先ファイルをセットする
	 * @param mergedFile 比較先ファイル
	 */
	public void setMergedFile(File mergedFile) {
		this.mergedFile = mergedFile;
	}
	/**
	 * マージ済ファイルを取得する
	 * @return マージ済ファイル
	 */
	public File getSrcFile() {
		return srcFile;
	}
	/**
	 * マージ済ファイルをセットする
	 * @param srcFile マージ済ファイル
	 */
	public void setSrcFile(File srcFile) {
		this.srcFile = srcFile;
	}
	/**
	 * 比較元ファイルのCharsetを取得する
	 * @return 比較元ファイルのCharset
	 */
	public Charset getDestCharset() {
		return destCharset;
	}
	/**
	 * 比較元ファイルのCharsetをセットする
	 * @param destCharset 比較元ファイルのCharset
	 */
	public void setDestCharset(Charset destCharset) {
		this.destCharset = destCharset;
	}
	/**
	 * 比較先ファイルのCharsetを取得する
	 * @return 比較先ファイルのCharset
	 */
	public Charset getMergedCharset() {
		return mergedCharset;
	}
	/**
	 * 比較先ファイルのCharsetをセットする
	 * @param mergedCharset 比較先ファイルのCharset
	 */
	public void setMergedCharset(Charset mergedCharset) {
		this.mergedCharset = mergedCharset;
	}
	/**
	 * マージ済みファイルのCharsetを取得する
	 * @return マージ済みファイルのCharset
	 */
	public Charset getSrcCharset() {
		return srcCharset;
	}
	/**
	 * マージ済みファイルのCharsetをセットする
	 * @param srcCharset マージ済みファイルのCharset
	 */
	public void setSrcCharset(Charset srcCharset) {
		this.srcCharset = srcCharset;
	}
	/**
	 * スキップワードのリストを取得する
	 * @return スキップワードのリスト
	 */
	public List<String> getSkipwordList() {
		return skipwordList;
	}
	/**
	 * スキップワードのリストをセットする
	 * @param skipwordList スキップワードのリスト
	 */
	public void setSkipwordList(List<String> skipwordList) {
		this.skipwordList = skipwordList;
	}


	/**
	 * ３点Ｄｉｆｆを行い、結果を３点Ｄｉｆｆ結果リストで返す。<br>
	 * @return ３点Ｄｉｆｆ結果リスト
	 * @throws Exception
	 */
	public DiffResult execute() throws Exception {

		DiffResult diffResultSrcMerge = null ;
		DiffResult diffResultMergeDst = null ;

		if( CharsetMode.SAME.equals( this.charsetMode ) ) {
			Charset commonCharset = this.mergedCharset ;
			//比較元＆マージ状態
			diffResultSrcMerge = diffProc( this.srcFile , this.mergedFile , commonCharset , this.skipwordList ) ;
			//マージ状態＆比較先
			diffResultMergeDst = diffProc( this.mergedFile , this.destFile , commonCharset , this.skipwordList ) ;
		} else if( CharsetMode.SEPARATE.equals( this.charsetMode ) ) {
			//比較元＆マージ状態
			diffResultSrcMerge = diffProc( this.srcFile , this.srcCharset , this.mergedFile , this.mergedCharset , this.skipwordList ) ;
			//マージ状態＆比較先
			diffResultMergeDst = diffProc( this.mergedFile , this.mergedCharset , this.destFile , this.destCharset , this.skipwordList ) ;
		} else {
			throw new TriSystemException( SmMessageId.SM004032F , this.charsetMode.toString() ) ;
		}

		log.debug( "###  diff結果リストの長さ " + diffResultSrcMerge.getDiffElementList().size() + " " +  diffResultMergeDst.getDiffElementList().size() ) ;

		List<IDiffElement> diffElementListTrinomial = new ArrayList<IDiffElement>() ;
		//diffリストの全長が長い方を軸にループ
		if( diffResultSrcMerge.getDiffElementList().size() >= diffResultMergeDst.getDiffElementList().size() ) {
			int offset = 0 ;
			for( int i = 0 ; i < diffResultSrcMerge.getDiffElementList().size() ; i++ ) {
				IDiffElement srcMerge = diffResultSrcMerge.getDiffElementList().get( i ) ;
				IDiffElement mergeDst = null ;
				if( diffResultMergeDst.getDiffElementList().size() > i + offset ) {
					mergeDst = diffResultMergeDst.getDiffElementList().get( i + offset ) ;
				} else {
					mergeDst = new EqualsContentsSetSameLineDiffResult( Status.EQUAL , 0 , "" , 0 , "" ) ;
				}

				IDiffElement diffElement = null ;
				if( srcMerge.getDstContents().equals( mergeDst.getSrcContents() ) ) {
					diffElement = new TrinomialDiffResult(
							srcMerge.getSrcLineNumber() , srcMerge.getSrcContents() ,
							srcMerge.getDstLineNumber() , srcMerge.getDstContents() ,
							mergeDst.getDstLineNumber() , mergeDst.getDstContents() ,
							srcMerge.getStatus() , mergeDst.getStatus() ) ;
				} else {//「マージ済み」同士の行位置にズレが生じたので足踏みさせる
					offset-- ;
					diffElement = new TrinomialDiffResult(
							srcMerge.getSrcLineNumber() , srcMerge.getSrcContents() ,
							srcMerge.getDstLineNumber() , srcMerge.getDstContents() ,
							0 , "" ,
							srcMerge.getStatus() , Status.EQUAL ) ;
				}
				diffElementListTrinomial.add( diffElement ) ;
			}
		} else {
			int offset = 0 ;
			for( int i = 0 ; i < diffResultMergeDst.getDiffElementList().size() ; i++ ) {
				IDiffElement srcMerge = null ;
				if( diffResultSrcMerge.getDiffElementList().size() > i + offset ) {
					srcMerge = diffResultSrcMerge.getDiffElementList().get( i + offset ) ;
				} else {
					srcMerge = new EqualsContentsSetSameLineDiffResult( Status.EQUAL , 0 , "" , 0 , "" ) ;
				}

				IDiffElement mergeDst = diffResultMergeDst.getDiffElementList().get( i ) ;

				IDiffElement diffElement = null ;
				if( srcMerge.getDstContents().equals( mergeDst.getSrcContents() ) ) {
					diffElement = new TrinomialDiffResult(
							srcMerge.getSrcLineNumber() , srcMerge.getSrcContents() ,
							mergeDst.getSrcLineNumber() , mergeDst.getSrcContents() ,
							mergeDst.getDstLineNumber() , mergeDst.getDstContents() ,
							srcMerge.getStatus() , mergeDst.getStatus() ) ;
				} else {//「マージ済み」同士の行位置にズレが生じたので足踏みさせる
					offset-- ;
					diffElement = new TrinomialDiffResult(
							0 , "" ,
							mergeDst.getSrcLineNumber() , mergeDst.getSrcContents() ,
							mergeDst.getDstLineNumber() , mergeDst.getDstContents() ,
							Status.EQUAL , mergeDst.getStatus() ) ;
				}
				diffElementListTrinomial.add( diffElement ) ;
			}
		}

		return new DiffResult( diffElementListTrinomial ,
				diffResultSrcMerge.getCharsetSrc() ,
				diffResultSrcMerge.getCharsetDst() , 	//マージの文字コードは両側とも同一のはずなのでどちらをとってもよい
				diffResultMergeDst.getCharsetDst() ) ;
	}

	/**
	 * ２件のファイルに対してDiff比較を行う。<br>
	 * @param srcFile 		比較元File
	 * @param srcCharset	比較元Charset
	 * @param destFile		比較先File
	 * @param destCharset 比較先Charset
	 * @param skipwordList	スキップワードのリスト
	 * @return	Diff比較結果リスト
	 * @throws Exception
	 */
	private DiffResult diffProc( File srcFile , Charset srcCharset , File destFile , Charset destCharset , List<String> skipwordList ) throws Exception {
		EqualsContentsSetSameLineDiffer diff = null ;

		if( null == srcCharset || null == destCharset ) {//文字コード指定なし⇒文字コード自動判別
			diff = new EqualsContentsSetSameLineDiffer( srcFile , destFile ) ;
		} else {//文字コード指定
			diff = new EqualsContentsSetSameLineDiffer( srcFile , srcCharset , destFile , destCharset ) ;
		}
		//スキップワードのセット
		diff.setException( skipwordList ) ;
		//差分比較処理を実行
		DiffResult diffResult = diff.diff() ;

		return diffResult ;
	}

	/**
	 * ２件のファイルに対してDiff比較を行う。<br>
	 * 文字コードは両者で共通の１つのみとする。
	 * @param srcFile 		比較元File
	 * @param destFile		比較先File
	 * @param charset Charset
	 * @param skipwordList	スキップワードのリスト
	 * @return	Diff比較結果リスト
	 * @throws Exception
	 */
	private DiffResult diffProc( File srcFile , File destFile , Charset charset , List<String> skipwordList ) throws Exception {

		EqualsContentsSetSameLineDiffer diff = null ;

		if( null == charset ) {//文字コード指定なし⇒文字コード自動判別
			diff = new EqualsContentsSetSameLineDiffer( srcFile , destFile ) ;
		} else {//文字コード指定
			diff = new EqualsContentsSetSameLineDiffer( srcFile , destFile , charset ) ;
		}
		//スキップワードのセット
		diff.setException( skipwordList ) ;
		//差分比較処理を実行
		DiffResult diffResult = diff.diff() ;

		return diffResult ;
	}
}
