package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

/**
 * {@link jp.co.blueship.tri.fw.cmn.io.FileRecursive}で、実際にfile同士に対して行なう
 * 処理を表現するinterface。
 */
public interface IRecursiveCallback {

	

	/**
	 * src/dstがfileだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 */
	public void doFileFile(File src, File dst);

	/**
	 * srcがdirectoryで存在し、dstが存在しない場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>
	 */
	public boolean doDirectoryNothing(File src, File dst);

	/**
	 * src/dstがfileだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 */
	public void doFileNothing(File src, File dst);

	/**
	 * src/dst共にdirectoryの場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>	 */
	public boolean doDirectoryDirectory(File src, File dst);


	/**
	 * srcがdirectory、dstがfileの場合にcallbackされる。
	 * <p>
	 * なお、戻り値がtrueのsrcのdirectoryに入り来んで再帰処理を続行するが、
	 * falseの場合はsrcのdirectoryに入らず、次の同levelのfileに対して処理を
	 * 行なう。
	 * </p>
	 * @param src 元
	 * @param dst 先
	 * @return <dl>
	 * 		<dt>true</dt><dd>srcに潜り込む形で再帰処理続行。</dd>
	 * 		<dt>false</dt><dd>srcに潜り込まずに同levelの次fileを処理対象にする形で再帰処理続行。</dd>
	 * </dl>	 */
	public boolean doDirectoryFile(File src, File dst);

	/**
	 * srcがfile,dstがdirectoryだった場合にcallbackされる。
	 * @param src 元
	 * @param dst 先
	 */
	public void doFileDirectory(File src, File dst);

}

