package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.Closeable;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.io.Writer;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.tools.zip.ZipFile;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * <br>
 * <p>
 *
 * </p>
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class TriFileUtils {

	/** fileを読み込む時のdefault buffer size */
	public static final int BUF_SIZE = 1024 * 10;

	/** ディレクトリ配下の検索時の定数(ALL) */
	public static final int TYPE_ALL = 0;
	/** ディレクトリ配下の検索時の定数(Directory) */
	public static final int TYPE_DIR = 1;
	/** ディレクトリ配下の検索時の定数(File) */
	public static final int TYPE_FILE = 2;

	/**
	 * comment行判別用{@link Pattern}
	 */
	private static final Pattern commentPattern = Pattern.compile("^\\p{Space}*(#.*)?$");

	private static final StringBuilder EMPTY_STRING_BUILDER = new StringBuilder("");

	private static IContextAdapter contextAdapter() {
		return ContextAdapterFactory.getContextAdapter();
	}

	/**
	 * 環境に合った改行コードでfileを読み込みStringとして返却する。
	 *
	 * @param fileName 対象file名
	 * @return file内容
	 * @throws IOException 読み込み中に問題発生。
	 */
	public static String readFileConvNewLine(String fileName) throws IOException {

		File f = new File(fileName);
		return readFileConvNewLine(f);

	}

	/**
	 * 環境に合った改行コードでfileを読み込みStringとして返却する。
	 *
	 * @param file 対象file
	 * @return file内容
	 * @throws IOException 読み込み中に問題発生。
	 */
	public static String readFileConvNewLine(File file) throws IOException {

		FileReader fr = new FileReader(file);

		try {
			BufferedReader br = new BufferedReader(fr);

			StringBuilder sb = null;
			String readLine = null;
			while ((readLine = br.readLine()) != null) {
				if (sb == null) {
					sb = new StringBuilder();
				} else {
					sb.append(SystemProps.LineSeparator.getProperty());
				}
				sb.append(readLine);
			}

			return TriObjectUtils.defaultIfNull(sb, EMPTY_STRING_BUILDER).toString();
		} finally {
			close(fr);
		}
	}

	/**
	 * テキストを読みこみ、行単位の文字列配列を取得します。
	 *
	 * @param file 対象file
	 * @param isAll falseを指定すると、commentや空行を含めません
	 * @return 取得した文字列を戻します。
	 * @throws IOException 入出力エラーが発生した場合
	 * @see {@link #readFileLine(Reader, boolean)}
	 */
	public static String[] readFileLine(File file, boolean isAll) throws IOException {

		FileInputStream fis = null;
		InputStreamReader isr = null;

		try {
			fis = new FileInputStream(file);
			isr = new InputStreamReader(fis);
			return TriFileUtils.readFileLine(isr, isAll);

		} catch (IOException e) {
			throw e;
		} finally {
			close(isr);
			close(fis);
		}
	}

	/**
	 * テキストを読みこみ、行単位の文字列配列を取得します。
	 *
	 * @param reader 文字ストリームを読み込むクラス
	 * @return 取得した文字列を戻します。
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static String[] readFileLine(Reader reader) throws IOException {
		return readFileLine(reader, true);
	}

	/**
	 * テキストを読みこみ、行単位の文字列配列を取得します。
	 *
	 * @param reader 文字ストリームを読み込むクラス
	 * @param isAll falseを指定すると、commentや空行を含めません
	 * @return 取得した文字列を戻します。
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static String[] readFileLine(Reader reader, boolean isAll) throws IOException {
		List<String> outList = new ArrayList<String>();

		BufferedReader br = null;
		try {

			br = new BufferedReader(reader);

			String readLine = null;
			while ((readLine = br.readLine()) != null) {
				if (!isAll) {
					if (!isContents(readLine))
						continue;
				}

				outList.add(readLine.trim());
			}

			return outList.toArray(new String[] {});
		} catch (IOException e) {
			throw e;
		} finally {
			close(br);
		}
	}

	/**
	 * 文字列がcommentや空行では無く、実contentsの場合はtrueを返却する。
	 *
	 * @param line check対象文字列
	 * @return 判定結果
	 */
	public static final boolean isContents(String line) {
		Matcher m = commentPattern.matcher(line);
		return !m.matches();
	}

	/**
	 * 文字列をfileに書き込みする。
	 *
	 * @param fileName 書き込み先file
	 * @param contents 書き込み内容
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(String fileName, String contents) throws IOException {

		writeStringFile(new File(fileName), contents);

	}

	/**
	 * 文字列をfileに書き込みする。
	 *
	 * @param fileName 書き込み先file
	 * @param contents 書き込み内容
	 * @param append 追記する場合、true
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(String fileName, String contents, boolean append) throws IOException {

		writeStringFile(new File(fileName), contents, append);

	}

	/**
	 * 文字列を指定されたファイルに出力します。
	 *
	 * @param file 書き込み先のファイル
	 * @param contents 書き込み内容
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(File file, String contents) throws IOException {

		writeStringFile(file, contents, false);
	}

	/**
	 * 文字列を指定されたファイルに出力します。
	 *
	 * @param file 書き込み先のファイル
	 * @param contents 書き込み内容
	 * @param append 追記する場合、true
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(File file, String contents, boolean append) throws IOException {

		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(file, append);
			bw = new BufferedWriter(fw);
			bw.write(contents);

		} catch (IOException e) {
			throw e;
		} finally {
			closeWithFlush(bw);
		}

	}

	/**
	 * 文字列配列を指定されたファイルに出力します。 <br>
	 * 行単位に行区切り文字が自動的に付与されます。
	 *
	 * @param file 書き込み先のファイル
	 * @param contents 書き込み内容
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(File file, String[] contents) throws IOException {

		BufferedWriter bw = null;
		try {
			bw = new BufferedWriter(new FileWriter(file));

			for (int i = 0; i < contents.length; i++) {
				bw.write(contents[i]);
				bw.newLine();
			}

		} catch (IOException e) {
			throw e;
		} finally {
			closeWithFlush(bw);
		}
	}

	/**
	 * 文字列配列を指定されたファイルに出力します。 <br>
	 * 行単位に行区切り文字が自動的に付与されます。
	 *
	 * @param file 書き込み先のファイル
	 * @param contents 書き込み内容
	 * @param append 追記する場合、true
	 * @throws IOException 入出力エラーが発生した場合
	 */
	public static void writeStringFile(File file, String[] contents, boolean append) throws IOException {

		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			fw = new FileWriter(file, append);
			bw = new BufferedWriter(fw);

			for (int i = 0; i < contents.length; i++) {
				bw.write(contents[i]);
				bw.newLine();
			}

		} catch (IOException e) {
			throw e;
		} finally {
			closeWithFlush(bw);
		}

	}

	/**
	 * fileをcopyする。
	 *
	 * @param src file。 directoryは不可。
	 * @param dst file。 directoryは不可。
	 * @throws IOException コピー中に問題発生。
	 * @throws IllegalArgumentException 引数がfileでは無い。
	 */
	public static void copyFileToFile(File src, File dst) throws IOException {

		if (!src.isFile()) {
			throw new IllegalArgumentException();
		}

		if (dst.isDirectory()) {
			throw new IllegalArgumentException();
		}

		FileInputStream fis = null;
		FileOutputStream fos = null;
		InputStream in = null;
		OutputStream out = null;

		try {
			fis = new FileInputStream(src);
			in = new BufferedInputStream(fis);
			fos = new FileOutputStream(dst);
			out = new BufferedOutputStream(fos);

			int i = 0;
			while ((i = in.read()) != -1) {
				out.write(i);
			}

		} finally {
			close(in);
			close(fis);
			closeOfOutputStream(out);
			close(fos);
		}

	}

	/**
	 * fileかdirectoryを再帰的に削除する。
	 *
	 * @param target 対象file or directory.
	 * @throws IOException 削除中に問題発生。
	 */
	public static void delete(File target) throws IOException {

		if (!target.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004074F, target.toString()));
		}

		if (target.isFile()) {
			if (!target.delete()) {
				throw new IOException(contextAdapter().getMessage(SmMessageId.SM005086S, target.toString()));
			}
		} else if (target.isDirectory()) {
			File subFiles[] = target.listFiles();
			if (null == subFiles) {
				throw new IOException(contextAdapter().getMessage(SmMessageId.SM004075F, target.toString()));
			}
			for (int i = 0; i < subFiles.length; i++) {
				delete(subFiles[i]);
			}
			if (!target.delete()) {
				throw new IOException(contextAdapter().getMessage(SmMessageId.SM004076F, target.toString()));
			}
		} else {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004074F, target.toString()));
		}

	}

	/**
	 * directoryを再帰的にチェック、参照権限があるかチェックする<br>
	 *
	 * @param target 対象file or directory.
	 * @mode 探索モード
	 *
	 *       <pre>
	 * 		true	:	target以下のディレクトリを再帰的にチェックする
	 * 		false	:	targetディレクトリのみをチェックする
	 * </pre>
	 * @throws IOException チェック中に問題発生。
	 */
	public static void checkAccessible(File target, boolean mode) throws IOException {

		if (true != target.exists() || true == target.isFile()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004077F, target.getAbsolutePath()));
		}

		if (target.isDirectory()) {// ディレクトリ

			File subFiles[] = target.listFiles();
			if (null == subFiles) {// ディレクトリ内容が参照できない⇒参照権限がない
				throw new IOException(contextAdapter().getMessage(SmMessageId.SM004078F, target.getAbsolutePath()));
			}
			if (true == mode) {// 再帰処理
				for (File subFile : subFiles) {
					if (subFile.isDirectory()) {// ディレクトリのみ再帰処理対象
						checkAccessible(subFile, mode);
					}
				}
			}
		}
	}

	/**
	 * file1とfile2を比較する。
	 *
	 * @param file1 比較fileその1
	 * @param file2 比較fileその2
	 * @param bufSize file io時のbuffer size
	 * @return true: 一致 false:不一致
	 * @throws Exception
	 */
	public static boolean isSame(File file1, File file2, int bufSize) throws IllegalArgumentException, IOException {

		if (!(file1.exists() && file2.exists())) {
			throw new IllegalArgumentException();
		}

		FileInputStream fis1 = null;
		FileInputStream fis2 = null;
		BufferedInputStream stream1 = null;
		BufferedInputStream stream2 = null;

		try {
			fis1 = new FileInputStream(file1);
			fis2 = new FileInputStream(file2);
			stream1 = new BufferedInputStream(fis1);
			stream2 = new BufferedInputStream(fis2);

			while (true) {

				byte b1[] = new byte[bufSize];
				byte b2[] = new byte[bufSize];

				int readSize1 = stream1.read(b1, 0, bufSize);
				int readSize2 = stream2.read(b2, 0, bufSize);

				if (readSize1 != readSize2) {
					return false;
				}
				if (-1 == readSize1 && -1 == readSize2) {// 両方とも同時に終端
					return true;
				}

				for (int i = 0; i < readSize1; i++) {
					if (b1[i] != b2[i]) {
						return false;
					}
				}
			}

		} catch (IOException e) {
			throw e;
		} finally {
			close(stream1);
			close(stream2);
			close(fis1);
			close(fis2);
		}
	}

	/**
	 * file1とfile2を比較する。
	 *
	 * @param file1 比較fileその1
	 * @param file2 比較fileその2
	 * @return true: 一致 false:不一致
	 * @throws Exception
	 */
	public static boolean isSame(File file1, File file2) throws IllegalArgumentException, IOException {
		return isSame(file1, file2, BUF_SIZE);
	}

	public static List<String> getFileList(File f, int type, Map<?, ?> cancelWords) {

		List<String> flist = new ArrayList<String>();

		File[] subDirs = f.listFiles();
		for (int i = 0; i < subDirs.length; i++) {

			String fName = subDirs[i].getName();

			if (cancelWords != null) {
				if (cancelWords.containsKey(fName)) {
					continue;
				}
			}

			if (type == TYPE_FILE & subDirs[i].isFile()) {
				flist.add(fName);
			}
			if (type == TYPE_DIR & subDirs[i].isDirectory()) {
				flist.add(fName);
			}
			if (type == TYPE_ALL) {
				flist.add(fName);
			}
		}

		return flist;
	}

	/**
	 * 指定されたディレクトリ配下のファイル、ディレクトリリストを返す
	 *
	 * @param f 対象となるディレクトリのファイルオブジェクト
	 * @param type {@link TriFileUtils#TYPE_ALL},{@link TriFileUtils#TYPE_DIR},
	 *            {@link TriFileUtils#TYPE_FILE}
	 * @return List 指定したtypeのFileの文字列によるList
	 */
	public static List<String> getFileList(File f, int type) {
		return getFileList(f, type, null);
	}

	/**
	 *
	 * 指定されたディレクトリ配下のファイル、ディレクトリリストを返す
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param type type {@link TriFileUtils#TYPE_ALL},
	 *            {@link TriFileUtils#TYPE_DIR},{@link TriFileUtils#TYPE_FILE}
	 * @param filter
	 * @return List 取得したList
	 * @throws IOException
	 */
	public static List<File> getFiles(File file, int type, FileFilter filter) throws IOException {

		if (!file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004079F, file.getAbsolutePath()));
		}
		if (!file.isDirectory()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004089F, file.getAbsolutePath()));
		}
		List<File> flist = new ArrayList<File>();

		File[] subDirs = file.listFiles(filter);
		for (int i = 0; i < subDirs.length; i++) {

			if (type == TYPE_FILE & subDirs[i].isFile())
				flist.add(subDirs[i]);

			if (type == TYPE_DIR & subDirs[i].isDirectory())
				flist.add(subDirs[i]);

			if (type == TYPE_ALL)
				flist.add(subDirs[i]);

			if (subDirs[i].isDirectory())
				flist.addAll(getFiles(subDirs[i], type, filter));
		}

		return flist;
	}

	/**
	 * 指定されたディレクトリ配下のファイル、ディレクトリリストを返す
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param type {@link TriFileUtils#TYPE_ALL},{@link TriFileUtils#TYPE_DIR},
	 *            {@link TriFileUtils#TYPE_FILE}
	 * @return List 取得したList
	 * @throws IOException
	 */
	public static List<?> getFiles(File file, int type) throws IOException {

		if (!file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004079F, file.getAbsolutePath()));
		}
		if (!file.isDirectory()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004089F, file.getAbsolutePath()));
		}
		return getFiles(file, type, new FileFilter() {
			public boolean accept(File pathname) {
				return true;
			}
		});
	}

	/**
	 *
	 * 指定されたディレクトリ配下のファイル、ディレクトリリストを相対パスで返す
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param type type {@link TriFileUtils#TYPE_ALL},
	 *            {@link TriFileUtils#TYPE_DIR},{@link TriFileUtils#TYPE_FILE}
	 * @param filter
	 * @param buf
	 * @return List 取得したList
	 * @throws IOException
	 */
	public static List<String> getFiles(File file, int type, FileFilter filter, StringBuilder buf) throws IOException {

		if (!file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004079F, file.getAbsolutePath()));
		}
		if (!file.isDirectory()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004089F, file.getAbsolutePath()));
		}
		List<String> flist = new ArrayList<String>();

		File[] subDirs = file.listFiles(filter);
		for (int i = 0; i < subDirs.length; i++) {

			if (type == TYPE_FILE & subDirs[i].isFile())
				flist.add(buf.toString() + subDirs[i].getName());

			if (type == TYPE_DIR & subDirs[i].isDirectory())
				flist.add(buf.toString() + subDirs[i].getName());

			if (type == TYPE_ALL)
				flist.add(buf.toString() + subDirs[i].getName());

			if (subDirs[i].isDirectory()) {
				StringBuilder newBuf = new StringBuilder(buf.toString());
				newBuf.append(subDirs[i].getName());
				newBuf.append("/");
				flist.addAll(getFiles(subDirs[i], type, filter, newBuf));
			}
		}

		return flist;
	}

	/**
	 * 指定されたディレクトリのファイル、ディレクトリリストを相対パスで返す
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param type {@link TriFileUtils#TYPE_ALL},{@link TriFileUtils#TYPE_DIR},
	 *            {@link TriFileUtils#TYPE_FILE}
	 * @param filter
	 * @return List 取得したList
	 * @throws IOException
	 */
	public static List<String> getFilePaths(File file, int type, FileFilter filter) throws IOException {

		StringBuilder buf = new StringBuilder();

		if (!file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004079F, file.getAbsolutePath()));
		}
		if (!file.isDirectory()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004089F, file.getAbsolutePath()));
		}
		return getFiles(file, type, filter, buf);
	}

	/**
	 * 指定されたディレクトリのファイル、ディレクトリリストを相対パスで返す
	 *
	 * @param file 対象となるディレクトリのファイル
	 * @param type {@link TriFileUtils#TYPE_ALL},{@link TriFileUtils#TYPE_DIR},
	 *            {@link TriFileUtils#TYPE_FILE}
	 * @return List 取得したList
	 * @throws IOException
	 */
	public static List<String> getFilePaths(File file, int type) throws IOException {

		StringBuilder buf = new StringBuilder();

		if (!file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004079F, file.getAbsolutePath()));
		}
		if (!file.isDirectory()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004089F, file.getAbsolutePath()));
		}
		return getFiles(file, type, new FileFilter() {
			public boolean accept(File pathname) {
				return true;
			}
		}, buf);
	}

	/**
	 * ファイル文字列から、拡張子部分を取得して返す<br>
	 * 複数の'.'が存在する場合、最後の'.'以下を拡張子とする。
	 *
	 * @param value ファイル文字列
	 * @return 拡張子文字列
	 * @throws Exception
	 */
	public static String getExtension(String value) throws Exception {
		int periodPos = value.lastIndexOf('.');
		if (-1 == periodPos || value.length() - 1 == periodPos) {
			return null;
		}
		return value.substring(periodPos + 1);
	}

	/**
	 * ファイルがＳＣＭ固有のファイルかどうかの判定を行います。
	 *
	 * @param file 判定するファイル
	 * @return ＳＣＭ固有の場合trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isSCM(File file) {
		if (null == file) {
			return false;
		}

		if (!file.exists()) {// 存在しない場合

			if (true == includeSCM(file.getAbsolutePath())) {

				return true;
			}
		}

		if (file.isFile()) {// ファイル
			// /parent/fileでparentの位置に.svnがある場合を想定
			File parent = file.getParentFile();

			if (null != parent) {
				if (true == equalsSCM(parent.getName())) {

					return true;
				}
				// /parent2/parent/fileでparent2の位置に.svnがある場合を想定
				File parent2 = parent.getParentFile();

				if (null != parent2) {
					if (true == equalsSCM(parent2.getName())) {

						return true;
					}
					// /parent3/parent2/parent/fileでparent2の位置に.svnがある場合を想定
					File parent3 = parent2.getParentFile();

					if (null != parent3) {
						if (true == equalsSCM(parent3.getName())) {

							return true;
						}
						// ファイルに対して４階層上のフォルダが管理フォルダになることを確認できないため
						// 以降は考慮しない
					}
				}
			}
		} else {// ディレクトリ
			// /dirでdirの位置に.svnがある場合を想定
			if (equalsSCM(file.getName())) {

				return true;
			}
			// /parent/dirでparentの位置に.svnがある場合を想定
			File parent = file.getParentFile();

			if (null != parent) {
				if (true == equalsSCM(parent.getName())) {

					return true;
				}
				// /parent2/parent/dirでparent2の位置に.svnがある場合を想定
				File parent2 = parent.getParentFile();

				if (null != parent2) {
					if (true == equalsSCM(parent2.getName())) {

						return true;
					}
				}
				// ディレクトリに対して３階層上のフォルダが管理フォルダになることを確認できないため
				// 以降は考慮しない
			}
		}

		return false;
	}

	private static final String[] scmDirStrings = new String[] { "CVS", ".svn", "_svn" // tortoiseSVNのオプション仕様[".svn"ディレクトリの代わりに"_svn"を使用する]に対応
	};

	/**
	 * 文字列が各ＳＣＭの管理フォルダ名かどうかの判定を行います。
	 *
	 * @param str 判定する文字列
	 * @return 一致する場合trueを戻します。それ以外はfalseを戻します。
	 */
	private static final boolean equalsSCM(String str) {
		if (null == str) {
			return false;
		}

		if (null != str) {

			for (String scmDirStr : scmDirStrings) {
				if (scmDirStr.equals(str)) {
					// 文字列が一致する場合はtrue
					return true;
				}
			}
		}
		// 以外はfalse
		return false;
	}

	/**
	 * 文字列が各ＳＣＭの管理フォルダ名を含むかどうかの判定を行います。
	 *
	 * @param str 判定する文字列
	 * @return 含む場合trueを戻します。それ以外はfalseを戻します。
	 */
	private static final boolean includeSCM(String str) {
		if (null == str) {
			return false;
		}

		String strBySlash = TriStringUtils.convertPath(str);

		if (null != str) {

			for (String scmDirStr : scmDirStrings) {

				// 途中にある場合
				Pattern pattern = Pattern.compile("^.*/" + scmDirStr + "/.*$");
				Matcher m = pattern.matcher(strBySlash);
				if (m.matches()) {
					// 文字列が一致する場合はtrue
					return true;
				}

				// 末尾にある場合
				pattern = Pattern.compile("^.*/" + scmDirStr + "$");
				m = pattern.matcher(strBySlash);
				if (m.matches()) {
					// 文字列が一致する場合はtrue
					return true;
				}

				// 先頭にある場合は考慮しない
			}
		}
		// 以外はfalse
		return false;
	}

	/**
	 * 与えられたパスを再帰的に辿って、ファイルが１件でもあるかチェックする<bt>
	 *
	 * @param path チェックするパス
	 * @param filter SCM関連ファイルなどの除外
	 * @return 判定結果
	 *
	 *         <pre>
	 * 		true	:	ファイルあり
	 * 		false	:	ファイルなし
	 * </pre>
	 * @throws Exception
	 */
	public static boolean checkExistFiles(String path, FileFilter filter) throws Exception {
		boolean ret = false;

		File file = new File(path);
		if (true != file.exists()) {
			throw new IOException(contextAdapter().getMessage(SmMessageId.SM004090F, path));
		}

		if (file.isDirectory()) {// ディレクトリ
			File[] files = file.listFiles(filter);
			if (null == files) {
				throw new IOException(contextAdapter().getMessage(SmMessageId.SM004091F, file.getPath()));
			}
			for (File tmpFile : files) {
				ret = checkExistFiles(TriFileUtils.linkPathBySlash(path, tmpFile.getName()), filter);
				if (true == ret) {
					return true;
				}
			}
		} else {// ファイル
			return true;// ファイルあり
		}
		return ret;
	}

	/**
	 * 与えられたパスを再帰的に辿って、ファイルが１件でもあるかチェックする<bt>
	 *
	 * @param path チェックするパス
	 * @param filter ファイルのフィルタ
	 * @param failOnNoExistPath <code>path</code>で与えられたパスが存在しないパスの場合に例外とするか
	 *
	 *            <pre>
	 * 		true	:	<code>path</code>で与えられたパスが存在しないパスの場合に例外とする
	 * 		    	 	{@link #checkExistFiles(String, boolean)}の実行結果と同じ
	 * 		false	:	<code>path</code>で与えられたパスが存在しないパスの場合に<code>false</code>を返す
	 * </pre>
	 * @return 判定結果
	 *
	 *         <pre>
	 * 		true	:	ファイルあり
	 * 		false	:	ファイルなし
	 * </pre>
	 * @throws Exception
	 */
	public static boolean checkExistFiles(String path, boolean failOnNoExistPath, FileFilter filter) throws Exception {

		if (!failOnNoExistPath) {
			File file = new File(path);
			if (true != file.exists())
				return false;
		}

		return checkExistFiles(path, filter);
	}

	/**
	 * fileの更新日付を変更する。
	 *
	 * @param file file。 directoryは不可。
	 * @param time time。 負の数の場合は現在のシステム時間を指定する。
	 * @return true 処理が正常に行えた場合あるいは処理しなかった場合
	 * @return false 処理しようとしたが正常に行えなかった場合
	 * @throws IllegalArgumentException 引数<code>file</code>がfileでは無い。
	 */
	public static boolean touch(File file, long time) {

		if (!file.isFile()) {
			throw new IllegalArgumentException();
		}

		return file.setLastModified(currentTimeIfMinus(time));
	}

	private static long currentTimeIfMinus(long time) {

		if (time < 0) {
			return System.currentTimeMillis();
		}

		return time;
	}

	/**
	 * 指定された正規表現にマッチするfileのみ更新日付を変更する。
	 *
	 * @param file file。 directoryは不可。
	 * @param time time。 負の数の場合は現在のシステム時間を指定する。
	 * @param pList コンパイル済みの正規表現のリスト。 nullの場合は正規表現は機能せず{@link this#touch(File,
	 *            long)}の機能のみ動作する。
	 * @return true 処理が正常に行えた場合あるいは処理しなかった場合
	 * @return false 処理しようとしたが正常に行えなかった場合
	 * @throws IllegalArgumentException 引数<code>file</code>がfileでは無い。
	 * @throws IllegalArgumentException 引数<code>pList</code>が<code>null</code>。
	 */
	public static boolean touch(File file, long time, List<Pattern> pList) {

		if (!file.isFile()) {
			throw new IllegalArgumentException();
		}

		if (null == pList) {
			throw new IllegalArgumentException();
		}

		for ( Pattern p: pList ) {
			// 指定された正規表現パターンに一致しないものは処理しない
			Matcher m = p.matcher(file.getAbsolutePath());
			if (m.matches()) {
				return touch(file, time);
			}
		}

		return true;
	}

	/**
	 * ファイルのパスを連結する。<br>
	 * パス区切り文字はすべて'/'に置換。<br>
	 *
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String linkPathBySlash(String pathRoot, String path) {
		return TriStringUtils.linkPathBySlash(pathRoot, path);
	}

	/**
	 * ファイルのパスを連結する。<br>
	 * パス区切り文字はすべて'\\'に置換。<br>
	 *
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String linkPathByBackSlash(String pathRoot, String path) {
		return TriStringUtils.linkPathByBackSlash(pathRoot, path);
	}

	/**
	 * ファイルのパスを連結する。<br>
	 * 連結時のパス区切り文字は環境依存の区切り文字('\\' or '/')とし、すべて置換する<br>
	 *
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String linkPath(String pathRoot, String path) {
		return TriStringUtils.linkPath(pathRoot, path);
	}

	/**
	 * パス先頭にパス区切り文字('\' or '/')が存在する場合は除去する<br>
	 *
	 * @param path パス
	 * @return 編集後のパス
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String trimHeadSeparator(String path) {
		return TriStringUtils.trimHeadSeparator(path);
	}

	/**
	 * パス末尾にパス区切り文字('\' or '/')が存在する場合は除去する<br>
	 *
	 * @param path パス
	 * @return 編集後のパス
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String trimTailSeparator(String path) {
		return TriStringUtils.trimTailSeparator(path);
	}

	/**
	 * 指定されたファイルパスにテキストデータを書き込む。<br>
	 * 既存ファイルが存在しない場合は新規作成、存在する場合は追記する。<br>
	 *
	 * @param path ファイルパス
	 * @param data テキストデータ
	 * @throws Exception
	 */
	public static void saveToFileStr(String path, String data) throws Exception {
		if (TriStringUtils.isEmpty(path) || TriStringUtils.isEmpty(data)) {
			return;
		}

		FileWriter fw = null;
		BufferedWriter bw = null;
		try {
			File logFile = new File(path);
			logFile.getCanonicalPath();

			if (!logFile.getParentFile().exists()) {
				logFile.getParentFile().mkdirs();
			}

			fw = new FileWriter(path, true);
			bw = new BufferedWriter(fw);
			bw.write(data);

		} catch (NullPointerException ie) {
			throw new TriSystemException(SmMessageId.SM004152F, ie, path);
		} catch (IOException ie) {
			throw new TriSystemException(SmMessageId.SM005144S, ie);
		} finally {
			closeWithFlush(bw);
		}
	}

	/**
	 * Write a text file to selected file path.<br>
	 * Create new file if the file does not exist. Append text if the file exists.<br>
	 * <br>
	 *
	 * 指定されたファイルパスにテキストデータを書き込む。<br>
	 * 既存ファイルが存在しない場合は新規作成、存在する場合は追記する。<br>
	 *
	 * @param path File Path
	 * @param data Text Data
	 * @param charset Character code
	 * @throws Exception
	 */
	public static void saveToFileStr(String path, String data, Charset charset) throws Exception {

		if (TriStringUtils.isEmpty(path) || TriStringUtils.isEmpty(data)) {
			return;
		}

		File file = new File(path);
		saveToFileStr(file, data, charset);
	}

	/**
	 * Write a text file to selected file path.<br>
	 * Create new file if the file does not exist. Append text if the file exists.<br>
	 * <br>
	 *
	 * 指定されたファイルパスにテキストデータを書き込む。<br>
	 * 既存ファイルが存在しない場合は新規作成、存在する場合は追記する。<br>
	 *
	 * @param path File Path
	 * @param data Text Data
	 * @param charset Character code
	 * @throws Exception
	 */
	public static void saveToFileStr(File file, String data, Charset charset) throws Exception {
		if (file == null || TriStringUtils.isEmpty(data)) {
			return;
		}

		OutputStreamWriter osw = null;
		try {
			file.getCanonicalPath();

			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}

			if (TriStringUtils.isEmpty(charset)) {
				osw = new OutputStreamWriter(new FileOutputStream(file));
			} else {
				osw = new OutputStreamWriter(new FileOutputStream(file), charset.toString());
			}

			osw.write(data);

		} catch (NullPointerException ie) {
			throw new TriSystemException(SmMessageId.SM004152F, ie, file.getAbsolutePath());
		} catch (IOException ie) {
			throw new TriSystemException(SmMessageId.SM005144S, ie);
		} finally {
			closeWithFlush(osw);
		}
	}

	/**
	 * ファイルを上書き保存する。
	 *
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param is ファイルの内容のストリーム
	 * @param fileName 添付ファイル名
	 */
	public static void saveToFileStream(File file, InputStream is) {

		if (TriStringUtils.isEmpty(file) || TriStringUtils.isEmpty(is)) {
			return;
		}

		BufferedInputStream inBuffer = null;
		BufferedOutputStream outBuffer = null;

		try {

			file.getCanonicalPath();

			if (!file.getParentFile().exists()) {
				file.getParentFile().mkdirs();
			}

			inBuffer = new BufferedInputStream(is);

			FileOutputStream fos = new FileOutputStream(file);
			outBuffer = new BufferedOutputStream(fos);

			int contents = 0;
			while ((contents = inBuffer.read()) != -1) {
				outBuffer.write(contents);
			}
		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM004153F, ioe);
		} finally {
			close(inBuffer);
			closeOfOutputStream(outBuffer);
		}
	}

	/**
	 * 改行コードを変換する<br>
	 * 対象とする改行コードは[CRLF],[CR],[LF]のみ。<br>
	 * <br>
	 *
	 * @param data 変換前の文字列
	 * @param linefeedCode 変換後の改行コード
	 * @return 変換後の文字列
	 * @deprecated {@see jp.co.blueship.trinity.common.util.StringAddonUtil}を参照
	 */
	public static String convertLinefeedCode(String data, LinefeedCode linefeedCode) {
		return TriStringUtils.convertLinefeedCode(data, linefeedCode);
	}

	/**
	 * Serializableに設定されたObjectをファイルに書き出す。
	 *
	 * @param outFile 出力先ファイルのオブジェクト
	 * @param obj 出力するオブジェクト
	 */
	public static void writeObjectToFile(File outFile, Object obj) throws IOException {
		FileOutputStream fos = null;
		ObjectOutputStream oos = null;

		try {
			fos = new FileOutputStream(outFile);
			oos = new ObjectOutputStream(fos);
			oos.writeObject(obj);
		} catch (IOException e) {
			throw e;
		} finally {
			closeOfOutputStream(oos);
			close(fos);
		}
	}

	/**
	 * Serializableに設定されたObjectをファイルから読み出す。
	 *
	 * @param inFile 入力元ファイルのオブジェクト
	 * @return 取得したオブジェクト
	 */
	public static Object readObjectFromFile(File inFile) throws ClassNotFoundException, IOException {
		FileInputStream fis = null;
		ObjectInputStream ois = null;

		try {
			fis = new FileInputStream(inFile);
			ois = new ObjectInputStream(fis);
			Object obj = ois.readObject();

			return obj;
		} catch (IOException e) {
			throw e;
		} finally {
			close(ois);
			close(fis);
		}
	}

	public static byte[] readInputStreamBytes(String filePath) {
		File downloadFile = new File(filePath);
		byte[] data = {};

		try {
			data = Files.readAllBytes(downloadFile.toPath());
		} catch (IOException e) {
			e.printStackTrace();
		}

		return data;
	}

	/**
	 * 対象ファイルを開放します。 close中のExceptionはStackTraceに格納します
	 *
	 */
	public static void close(Closeable closeable) {

		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * 対象ファイルを開放します。 close中のExceptionはStackTraceに格納します
	 *
	 */
	public static void close(ZipFile closeable) {

		try {
			if (closeable != null) {
				closeable.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * 対象ファイルを開放します。close前にflushを行います close中のExceptionはStackTraceに格納します
	 *
	 */
	public static void closeWithFlush(Writer writer) {

		try {
			if (writer != null) {
				writer.flush();
				writer.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * 対象ファイルを開放します。close前にflushを行います。OutputStream型です
	 * close中のExceptionはStackTraceに格納します
	 *
	 */
	public static void closeOfOutputStream(OutputStream stream) {

		try {
			if (stream != null) {
				stream.flush();
				stream.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * 対象ファイルを開放します。ZipFile型です
	 * close中のExceptionはStackTraceに格納します
	 *
	 */
	public static void closeOfZipFile(ZipFile stream) {

		try {
			if (stream != null) {
				stream.close();
			}
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}
	}

	/**
	 * Returns the string representation of the file size.
	 *
	 * <br>1KB = 1,024B
	 * <br>1MB = 1,024KB
	 * <br>1GB = 1,024MB
	 * <br>1TB＝ 1,024GB
	 * <br>1PB＝ 1,024TB
	 * <br>1EB＝ 1,024PB
	 *
	 * @param bytes file size
	 * @return a string representation of the file size.
	 */
	public static String fileSizeOf(long bytes) {
		int unit = 1024;
		if (bytes < unit) {
			return bytes + "B";
		}
		int exp = (int) (Math.log(bytes) / Math.log(unit));
		String pre = "KMGTPE".charAt(exp - 1) + "";
		return String.format("%.1f%sB", bytes / Math.pow(unit, exp), pre);
	}

	/**
	 * Returns the size of the specified file or directory.
	 * If the provided java.io.File is a regular file, then the file's length is returned.
	 * If the argument is a directory, then the size of the directory is calculated recursively.
	 * If a directory or subdirectory is security restricted, its size will not be included.
	 *
	 * @param file the regular file or directory to return the size of (must not be null).
	 * @return the length of the file, or recursive size of the directory, provided (in bytes).
	 */
	public static long sizeOf(File file) {

		PreConditions.assertOf(file.exists(), file + " does not exist");

		if (file.isDirectory()) {
			return sizeOfDirectory(file);
		} else {
			return file.length();
		}

	}

	/**
	 * Counts the size of a directory recursively (sum of the length of all files).
	 *
	 * @param directory directory to inspect, must not be null
	 * @return size of directory in bytes, 0 if directory is security restricted
	 */
	public static long sizeOfDirectory(File directory) {

		PreConditions.assertOf(directory.exists(), directory + " does not exist");
		PreConditions.assertOf(directory.isDirectory(), directory + " is not a directory");

		long size = 0;

		File[] files = directory.listFiles();
		if (files == null) {
			return 0L;
		}

		for (File file : files) {
			size += sizeOf(file);
		}

		return size;
	}
	
	/**
	 * Check if a file's parent folder exist
	 *
	 * @param filePath the file path need to check
	 * @return true if the parent folder exist, otherwise return false
	 */
	public static boolean isParentExist(String filePath) {
		File file = new File(filePath);
		File parent = file.getParentFile();
		if (parent != null && parent.exists()) {
			return true;
		}
		return false;
	}
	
	public static long getBytesFromFormattedSize(final String size) throws NumberFormatException {
	    final String[] arr = size.toUpperCase().split(" ");
	    if (arr.length != 2) {
	        if (arr.length == 1 && Character.isDigit(size.charAt(0))) {
	            // Treat this as a formatted size missing the space.
	            return getBytesFromSize(size);
	        }
	        throw new IllegalArgumentException("Expected '<size> <unit>', got '" + size + "'");
	    }
	    final char unit = arr[1].charAt(0);
	    if (unit == 'B') {
	        return Integer.parseInt(arr[0]);
	    }
	    final int bytes = arr[1].length() == 3 && arr[1].charAt(1) == 'I' ? 1000 : 1024;
	    final char[] units = {
	            'K', 'M', 'G', 'T', 'P', 'E'
	    };
	    int exp = 1;
	    for (final char ch : units) {
	        if (unit == ch) {
	            break;
	        }
	        exp++;
	    }
	    return (long) (Double.parseDouble(arr[0]) * Math.pow(bytes, exp));
	}
	
	private static long getBytesFromSize(final String size) {
	    String fixedSize = "";
	    for (final char c : size.toCharArray()) {
	        if (Character.isLetter(c) && !fixedSize.contains(" ")) {
	            fixedSize += ' ';
	        }
	        fixedSize += c;
	    }
	    return getBytesFromFormattedSize(fixedSize);
	}

}