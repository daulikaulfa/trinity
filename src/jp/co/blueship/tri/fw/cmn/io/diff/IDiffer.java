package jp.co.blueship.tri.fw.cmn.io.diff;

import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * Diffの結果を表示する機能を提供することを示すインターフェースです。
 *
 */
public interface IDiffer {

	

	/**
	 * 改行コードのセッター<br>
	 * 指定せずにdiffを実行した場合、システムのデフォルト改行コードが採用されます。
	 * @param separator 改行コード
	 */
	public void setSeparator( String separator );

	/**
	 * 例外文字列リストのセッター<br>
	 * 例外文字列が含まれる行において、例外文字列からその行末まではdiffの判断を行いません。
	 * @param exceptionList 例外文字列のリスト
	 */
	public void setException( List<String> exceptionList );

	/**
     * Diffの結果を取得します。
	 * @return Diffの結果
	 * @throws TriSystemException
	 */
	public DiffResult diff() throws TriSystemException;

}
