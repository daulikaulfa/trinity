package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Jar;
import org.apache.tools.ant.taskdefs.Zip;
import org.apache.tools.ant.types.FileSet;

/**
 * Jar、war、ear作成のユーティリティクラス
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public abstract class ArchiveCreator {

	

	// 作成するJarがすでに存在する場合の動作
	public enum Duplicate {
		// 作成しない
		SKIP,
		// 上書きする
		OVERWRITE,
		// 更新する
		UPDATE;
	}

	/**
	 * Projectクラスのオブジェクトを作成する
	 */
	protected Project getProject( File jarFile ) {

		Project project = new Project();
		project.setName( jarFile.getName() );

		return project;
	}

	/**
	 * 前準備を行う
	 * @param archiveFile 作成対象アーカイブファイル
	 * @param dup 作成するJarファイルが既存の場合の動作
	 * @return 処理続行：true、処理終了：false
	 */
	protected boolean prepare( File archiveFile, Duplicate dup ) throws IOException {

		// 既存ファイルありかつ、スキップの場合は抜ける
		if ( archiveFile.isFile() && Duplicate.SKIP.equals( dup ) ) {
			return false;
		}
		// 既存ファイルありかつ、上書きの場合は削除する
		if ( archiveFile.isFile() && Duplicate.OVERWRITE.equals( dup ) ) {
			TriFileUtils.delete( archiveFile );
		}

		// 親ディレクトリが取得でき、存在しない場合は作成する
		if ( null != archiveFile.getParentFile() && !archiveFile.getParentFile().exists() ) {
			archiveFile.getParentFile().mkdirs();
		}

		return true;
	}

	/**
	 * アーカイブオブジェクトにファイルセットを設定します。
	 * @param baseDir アーカイブ対象ファイルのベース(ルート)ディレクトリ
	 * @param srcList アーカイブ対象のパッケージ名、またはファイル名のリスト
	 * @param project Project
	 * @param archive アーカイブオブジェクト
	 */
	protected void setFileSet(
			File baseDir, String[] srcList, Project project, Zip archive ) {

		// baseDirが未指定の場合はカレントディレクトリ
		if ( null == baseDir ) {
			baseDir = new File( "./" );
		}

		for ( String src : srcList ) {

			File absFile = new File( baseDir, src );

			if ( absFile.exists() ) {

				FileSet fileSet = new FileSet();
				fileSet.setProject	( project );
				fileSet.setDir		( baseDir );

				if ( absFile.isFile() ) {
					fileSet.setIncludes	( src );
				} else {
					fileSet.setIncludes	( src + "/**" );
				}

				archive.addFileset( fileSet );
			}
		}
	}

	/**
	 * アーカイブオブジェクトにファイルセットを設定します。
	 * @param fileSetList アーカイブ対象ファイルセット
	 * @param project Project
	 * @param archive アーカイブオブジェクト
	 */
	protected void setFileSet(
			FileSetParamBean[] fileSetList, Project project, Zip archive ) {

		for ( FileSetParamBean fileSetParam : fileSetList ) {

			File absFile = new File( fileSetParam.getDir(), fileSetParam.getSrc() );

			if ( absFile.exists() ) {

				FileSet fileSet = new FileSet();
				fileSet.setProject	( project );
				fileSet.setDir		( fileSetParam.getDir() );

				if ( absFile.isFile() ) {
					fileSet.setIncludes	( fileSetParam.getSrc() );
				} else {
					fileSet.setIncludes	( fileSetParam.getSrc() + "/**" );
				}

				archive.addFileset( fileSet );
			}
		}
	}

	/**
	 * アーカイブオブジェクトに値を設定する
	 * @param archive アーカイブオブジェクト
	 * @param archiveFile 作成対象アーカイブファイル
	 * @param project Project
	 * @param manifestFile マニフェストファイル
	 * @param dup Duplicate
	 */
	public void setInfo( Jar archive, File archiveFile, Project project, File manifestFile, Duplicate dup ) {

		archive.setDestFile	( archiveFile );
		archive.setProject	( project );

		// 既存ファイルありかつ、更新の場合
		if ( archiveFile.isFile() && Duplicate.UPDATE.equals( dup ) ) {
			archive.setUpdate( true );
		}

		if ( null != manifestFile ) {
			archive.setManifest( manifestFile );
		}
	}

}
