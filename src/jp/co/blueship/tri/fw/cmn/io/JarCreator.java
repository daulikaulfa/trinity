package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Jar;

import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Jar作成を行うクラス
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class JarCreator extends ArchiveCreator {

	/**
	 * Jarファイルを作成する
	 * @param jarFile 作成するJarファイル
	 * @param baseDir Jar化対象ファイルのベース(ルート)ディレクトリ
	 * @param srcList Jar化対象のパッケージ名、またはファイル名のリスト
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するJarファイルが既存の場合の動作
	 */
	public void execute(
			File jarFile , File baseDir, String[] srcList ,  File manifestFile , Duplicate dup )
		throws TriRuntimeException {

		try {

			checkArgument( srcList , jarFile , manifestFile );
			if ( !super.prepare( jarFile, dup ) ) {
				return;
			}


			Project project = super.getProject( jarFile );
			Jar jar			= getJar( jarFile, project, manifestFile, dup );

			super.setFileSet( baseDir, srcList, project, jar );

			jar.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( SmMessageId.SM005085S , e , jarFile.getAbsolutePath() ) ;
		}

	}

	/**
	 * Jarファイルを作成する
	 * @param jarFile 作成するJarファイル
	 * @param jarFileSetList Jar化対象のファイルリスト
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するJarファイルが既存の場合の動作
	 */
	public void execute(
			File jarFile , FileSetParamBean[] jarFileSetList , File manifestFile , Duplicate dup )
		throws TriRuntimeException {

		try {

			checkArgument( jarFileSetList , jarFile , manifestFile );
			if ( !super.prepare( jarFile, dup ) ) {
				return;
			}

			Project project = super.getProject( jarFile );
			Jar jar			= getJar( jarFile, project, manifestFile, dup );

			super.setFileSet( jarFileSetList, project, jar );

			jar.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005085S , e , jarFile.getAbsolutePath() ) ;
		}

	}

	/**
	 * Jarクラスのオブジェクトを作成する
	 */
	private Jar getJar( File jarFile, Project project, File manifestFile, Duplicate dup ) {

		Jar jar = new Jar();
		super.setInfo( jar, jarFile, project, manifestFile, dup );

		return jar;
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			FileSetParamBean[] jarFileSetList , File jarFile , File manifestFile )
		throws TriRuntimeException {

		// Jar化対象ファイルのチェック
		if ( null == jarFileSetList ) {
			throw new TriSystemException( SmMessageId.SM004205F );
		}

		checkArgument( jarFile, manifestFile );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			String[] srcList , File jarFile , File manifestFile ) throws TriRuntimeException {

		// Jar化対象ファイルのチェック
		if ( null == srcList ) {
			throw new TriSystemException( SmMessageId.SM004206F );
		}

		checkArgument( jarFile, manifestFile );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument( File jarFile, File manifestFile ) throws TriRuntimeException {

		// Jarファイルのチェック
		if ( null == jarFile ) {
			throw new TriSystemException( SmMessageId.SM004207F );
		}
		if ( jarFile.isDirectory() ) {
			throw new TriSystemException(SmMessageId.SM004208F , jarFile.getAbsolutePath() ) ;
		}

		// マニフェストファイルのチェック
		if ( null != manifestFile ) {

			if( !manifestFile.exists() || !manifestFile.isFile() ) {
				throw new TriSystemException(SmMessageId.SM005075S , manifestFile.getAbsolutePath() ) ;
			}
		}
	}
}
