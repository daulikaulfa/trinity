package jp.co.blueship.tri.fw.cmn.io.constants;

/**
 * ファイルのコピーモード種別を列挙する<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public enum FileCopyMode {

	OVERWRITE_ALL	( 2 ),	//同名の既存ファイル／ディレクトリが存在すれば上書き
	OVERWRITE_NONE	( 1 ),	//同名の既存ファイル／ディレクトリが存在すれば、そのファイルは上書きしない。
	OVERWRITE_ABORT	( 0 );	//同名の既存ファイル／ディレクトリが存在すれば、即座に処理を中断してfalseを返す

	

	private Integer value = 0 ;

	private FileCopyMode( int value ) {
		this.value = value ;
	}

	public int intValue() {
		return this.value ;
	}

	public String getValue() {
		return value.toString();
	}

	public static FileCopyMode getValue( int value ) {
		for ( FileCopyMode define : values() ) {
			if ( define.intValue() == value )
				return define;
		}

		return null;
	}

}
