package jp.co.blueship.tri.fw.cmn.io.constants;

/**
 * 文字コードセット定義を列挙する<br>
 * 適宜追加のこと。
 * <br>
 * <p>
 * </p>
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public enum Charset {

	WINDOWS_31J		( "Windows-31J" ) ,
	MS932			( "MS932" ) ,
	SHIFT_JIS		( "SJIS" ) ,
	EUC_JP			( "EUC_JP" ) ,
	UTF_8			( "UTF-8" ) ,
	UTF_16			( "UTF-16" ) ,
	UTF_16LE		( "UTF-16LE" ) ,
	UTF_16BE		( "UTF-16BE" ) ,
	ISO_8859_1		( "ISO-8859-1" ) ,
	ISO_2022JP		( "ISO2022JP" ) ,
	JIS0201			( "JIS0201" ) ,
	JIS0208			( "JIS0208" ) ,
	JIS0212			( "JIS0212" ) ,
	CP930			( "Cp930" ) ,
	CP939			( "Cp939" ) ,
	CP942			( "Cp942" ) ,
	CP943			( "Cp943" ) ,
	CP33722			( "Cp33722" ) ;


	private String value = null ;

	private Charset( String value ) {
		this.value = value ;
	}

	public String toString() {
		return this.value ;
	}

	public String value() {
		return this.value;
	}

	public static Charset getDefaultCharset() {
		return WINDOWS_31J;
	}

	public static Charset value( String value ) {
		for ( Charset charset : values() ) {
 			if ( charset.value().equals( value ) ) {
				return charset;
			}
		}

		return null;
	}
}
