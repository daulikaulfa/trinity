package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

/**
 * Jar、War、Ear作成のファイルセットを保持するクラス
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class FileSetParamBean {

	

	private File dir	= null;
	private String src	= null;

	public FileSetParamBean( File dir, String src ) {

		this.dir = dir;
		this.src = src;
	}

	public File getDir() {
		return dir;
	}
	public void setDir( File dir ) {
		this.dir = dir;
	}

	public String getSrc() {
		return src;
	}
	public void setSrc( String src ) {
		this.src = src;
	}
}
