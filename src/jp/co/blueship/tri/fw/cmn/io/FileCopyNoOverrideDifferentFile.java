package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * 重複時、同じ内容であれば上書きcopyを行うcopy logic.
 */
public class FileCopyNoOverrideDifferentFile implements FileCopy {

	/**
	 * @inheritDoc
	 * <p>
	 * 	dstが既に存在し、内容が同じであれば、上書きcopyを行う。
	 *  内容が異なれば、重複とみなしてcopyを行わない。
	 * </p>
	 */
	public boolean copyFileToFile(File src, File dst, List<File> duplicateList)
			throws IOException {

		// コピー先がファイルで内容が違う場合は重複扱い
		if (dst.exists() && dst.isFile() && !TriFileUtils.isSame(src, dst)){
			duplicateList.add(dst);
			return false;

		// コピー先がディレクトリは重複扱い
		} else if (dst.exists() && dst.isDirectory()) {
			duplicateList.add(dst);
			return false;

		// コピー先がない
		// コピー先がファイルで内容が同じ
		}else{
			TriFileUtils.copyFileToFile(src, dst);
			return true;
		}
	}

}
