package jp.co.blueship.tri.fw.cmn.io;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.constants.FileCopyMode;

/**
 * ネットワークでファイルの転送を行うためのインタフェースです。
 * <br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public interface GenericIo {

	/**
	 * ファイルのコピーモード種別をセットする
	 *
	 * @param copyMode ファイルのコピーモード種別
	 */
	public void setCopyMode(FileCopyMode copyMode);

	/**
	 * 直近過去の応答コードを取得する
	 *
	 * @return 応答コード
	 */
	public String getLatestEndStatus();

	/**
	 * コネクション接続を確立する。すでに接続済みの場合はそのまま保持する
	 *
	 * @return 接続結果
	 * <pre>
	 * 	true	: 接続成功
	 *  false	: 接続失敗（ユーザ／パスワード不一致によりログイン出来ず）
	 * </pre>
	 * @throws Exception
	 */
	public boolean openConnection() throws Exception;

	/**
	 * Ftpコネクション接続を切断する。すでに切断されている場合はなにもせずに抜ける
	 *
	 * @throws Exception
	 */
	public void closeConnection() throws Exception ;

	/**
	 * フォルダまたはファイル単位でＦＴＰサーバへ転送を行う<br>
	 *
	 * @param localPath ローカル側の送信するフォルダまたはファイルのフルパス
	 * @param srvPath ＦＴＰサーバ側の受信先論理パス
	 * @return
	 * @throws Exception
	 */
	public boolean putObject( String localPath , String srvPath ) throws Exception ;

	/**
	 * フォルダまたはファイル単位でサーバから転送を行う<br>
	 * @param srcPath	ＦＴＰサーバ側の送信するフォルダまたはファイルの論理フルパス
	 * @param dstPath	ローカル側の受信先フルパス
	 * @throws Exception
	 */
	public boolean getObject( String localPath , String srvPath ) throws Exception ;

	/**
	 * フォルダまたはファイル単位でサーバから削除を行う<br>
	 * @param dstPath	ＦＴＰサーバ側の送信するフォルダまたはファイルの論理フルパス
	 * @throws Exception
	 */
	public void deleteObject( String srvPath ) throws Exception ;

	/**
	 * フォルダまたはファイルの名前変更を行う<br>
	 * フルパス指定のため、結果的にフォルダ階層を移動することもある<br>
	 * @param path 名前を変更するパス
	 * @param newPath 新しい名前のパス
	 * @throws Exception
	 */
	public void renameObject( String path , String newPath ) throws Exception ;

	/**
	 * path階層に存在するファイルおよびディレクトリ構成をリストに格納して返す<br>
	 * @param path サーバ側のパス
	 * @return ObjInfo型レコードオブジェクトを格納したリスト
	 * @throws Exception
	 */
	public List<ObjInfo> getList( String path ) throws Exception ;

	/**
	 * pathファイルまたはディレクトリが存在するかチェックする<br>
	 * @param pat ＦＴＰサーバ側のパス
	 * @return ObjInfoオブジェクト
	 * @throws Exception
	 */
	public ObjInfo getAttribute( String path ) throws Exception ;

	/**
	 * 引数で指定されたサーバ側dirに、ディレクトリdirectoryを作成する<br>
	 * 未接続時は、Ftp接続を試みる<br>
	 * @param path	ＦＴＰサーバ側のパス
	 * @return 処理結果
	 * <pre>
	 *  true	: ディレクトリを作成した
	 *  false	: ディレクトリを作成しなかった
	 * </pre>
	 */
	public boolean makeDirectory( String path ) throws Exception ;

	/**
	 * 引数で指定されたサーバ側dirの、ディレクトリdirectoryを削除する<br>
	 * 未接続時は、Ftp接続を試みる<br>
	 * @param path		ＦＴＰサーバ側のパス
	 * @return 処理結果
	 * <pre>
	 *  true	: 処理成功
	 *  false	: 処理失敗（ディレクトリが空でない、など）
	 * </pre>
	 */
	public boolean removeDirectory( String path ) throws Exception ;

	/**
	 * dir階層に、objName名のファイルまたはディレクトリが存在するかチェックする<br>
	 * @param path ＦＴＰサーバ側のパス
	 * @return 処理結果
	 * <pre>
 	 *  true	: 同名のディレクトリまたはファイルが存在した
	 *  false	: 同名のディレクトリまたはファイルは存在しない
	 * </pre>
	 * @throws Exception
	 */
	public boolean isExist( String path ) throws Exception ;

	/**
	 * dir階層に、fileName名のファイルが存在するかチェックする<br>
	 * @param path サーバ側のパス
	 * @return 処理結果
	 * <pre>
 	 *  true	: 同名のファイルが存在した
	 *  false	: 同名のファイルは存在しない
	 * </pre>
	 * @throws Exception
	 */
	public boolean isFile( String path ) throws Exception ;

	/**
	 * dir階層に、directory名のディレクトリが存在するかチェックする<br>
	 * @param path ＦＴＰサーバ側のパス
	 * @return 処理結果
	 * <pre>
 	 *  true	: 同名のディレクトリが存在した
	 *  false	: 同名のディレクトリは存在しない
	 * </pre>
	 * @throws Exception
	 */
	public boolean isDirectory( String path ) throws Exception ;

	/**
	 * 引数で指定されたクライアント側ファイルlocalPathを、サーバ側dirへ、<br>
	 * ファイル名fileNameを指定して転送（送信）する<br>
	 * @param localPath クライアント側ファイル（フルパス）
	 * @param path	ＦＴＰサーバ側のパス
	 * @param mode 処理モード
	 * <pre>
	 * 	true  : 同名の既存ファイルが存在すれば上書き
	 *  false : 同名の既存ファイルを存在すれば処理を中断してfalseを返す
	 * </pre>
	 * @return 処理結果
	 * <pre>
	 *  true	: 処理成功
	 *  false	: 同名の既存ファイルが存在したため処理を中断
	 * </pre>
	 */
	public boolean putFile( String localPath , String SrvPath ) throws Exception ;

	/**
	 * 引数で指定されたクライアント側ファイルlocalPathに、サーバ側dirへ、<br>
	 * サーバ側ファイル名fileNameを指定して転送（受信）する<br>
	 * @param localPath クライアント側ファイル（フルパス）
	 * @param path ＦＴＰサーバ側のパス
	 * @param mode 処理モード
	 * <pre>
	 * 	true  : 同名の既存ファイルが存在すれば上書き
	 *  false : 同名の既存ファイルを存在すれば処理を中断してfalseを返す
	 * </pre>
	 * @return 処理結果
	 * <pre>
	 *  true	: 処理成功
	 *  false	: 同名の既存ファイルが存在したため処理を中断
	 * </pre>
	 */
	public boolean getFile( String localPath , String srvPath ) throws Exception ;

	/**
	 * 引数で指定されたサーバ側dirの、サーバ側ファイル名fileNameを削除する<br>
	 * @param path  ＦＴＰサーバ側のパス
	 * @return 処理結果
	 * <pre>
	 *  true	: 処理成功
	 *  false	: 処理失敗
	 * </pre>
	 */
	public boolean removeFile( String path ) throws Exception ;

	/**
	 * path階層にＦＴＰサーバ側のカレントディレクトリを移動する
	 * @param path 移動するディレクトリ階層のパス
	 * @throws Exception
	 */
	public void changeDirectory( String path ) throws Exception ;
}
