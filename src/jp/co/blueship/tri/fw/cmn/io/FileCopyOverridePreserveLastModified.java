package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 重複時、上書きcopyを行ない、copy元の最終更新時刻を維持するcopy logic.
 */
public class FileCopyOverridePreserveLastModified extends FileCopyOverride {

	

	/**
	 * @inheritDoc
	 * <p>
	 * 	コピーされたファイル<code>dst</code>に、
	 *  オリジナルソースファイル<code>src</code>と同じ最終更新時刻を設定します。
	 * </p>
	 */
	public boolean copyFileToFile(File src, File dst, List<File> duplicateList)
			throws IOException {

		boolean copyResult = super.copyFileToFile(src, dst, duplicateList);
		long srcModified = src.lastModified();

		if(dst.setLastModified(srcModified) == false){
//			throw new SystemException("コピー元の最終更新日時をコピー先に設定できませんでした。");
			throw new TriSystemException(SmMessageId.SM004039F);
		}
		return copyResult;
	}
}
