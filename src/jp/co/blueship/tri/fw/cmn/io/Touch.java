package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.regex.Pattern;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

public class Touch implements ITouch {

	public boolean touch(File target) throws IOException {
		return touch(target, -1L, (List<Pattern>)null);
	}

	@Override
	public boolean touch(File target, long time, List<Pattern> patternList) throws IOException {

		if (target == null) {
			throw new NullPointerException();
		}

		if (!target.exists()) {
			throw new FileNotFoundException(
					ContextAdapterFactory.getContextAdapter().getMessage(SmMessageId.SM004073F, target.toString()));
		}

		boolean result = true;

		if (target.isFile()) {
			// touch

			result = TriFileUtils.touch(target, time, patternList);

		} else if (target.isDirectory()) {
			// directory recursion
			File files[] = target.listFiles();
			int i = 0;
			for (i = 0; i < files.length; i++) {
				result = touch(files[i], time, patternList);
			}
		}

		return result;
	}
}
