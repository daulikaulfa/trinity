package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.List;

import org.apache.tools.tar.TarEntry;
import org.apache.tools.tar.TarInputStream;
import org.apache.tools.tar.TarOutputStream;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ファイルの「【TAR形式】圧縮・展開」関連操作クラスに関する実装クラスです。<br>
 *
 * <pre>
 * 【現時点での問題点】
 * ・日本語ファイル名が文字化けする。
 * ・致命的に挙動不審。
 * </pre>
 *
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class FileIoTar {

	private static final ILog log = TriLogFactory.getInstance();
	private static final int EOF = -1;

	/**
	 * Tar形式に圧縮を行う<br>
	 * ファイルおよびディレクトリは複数指定可。<BR>
	 *
	 * <pre>
	 * 【現時点での問題点】
	 * ・日本語ファイル名が文字化けする。
	 * ・致命的に挙動不審。
	 * </pre>
	 *
	 * @param lst ファイルまたはディレクトリのフルパスを格納したList
	 * @param dstFilePath 圧縮済みファイルの出力先フルパス
	 * @param encoding ファイル名の文字エンコード【任意：Null可】
	 * @param doSkip 読込元のファイルが存在しなかった場合の処理モード
	 *
	 *            <pre>
	 * 		true	:	当該ファイルを無視してZip作成を継続する
	 * 		false	:	エラーとしてZip作成を中断
	 * </pre>
	 * @throws Exception
	 */
	public static void CompressTar(List<String> lst, String dstPath, boolean doSkip) throws Exception {

		if (null == lst || 0 == lst.size()) {
			throw new FileIoArchiveException(SmMessageId.SM004058F);
		}
		if (null == dstPath) {
			throw new FileIoArchiveException(SmMessageId.SM004059F , dstPath );
		}

		TarOutputStream tos = null;
		File fileTar = null;
		try {
			// 出力先フォルダの実在チェック
			fileTar = new File(dstPath);
			// TarOutputStreamを生成
			tos = new TarOutputStream(new BufferedOutputStream(new FileOutputStream(fileTar)));

			int skipCount = 0;
			for (String basePath : lst) {
				log.debug("圧縮対象のパス ：" + basePath);

				File file = new File(basePath);
				if (true == file.exists()) {
					CompressTarReentrant(tos, basePath, file.getName());
				} else {
					if (true == doSkip) {
						skipCount++;
					} else {
						throw new FileIoArchiveException( SmMessageId.SM004054F , basePath );
					}
				}
			}
			if (lst.size() == skipCount) {// 全てのファイルがスキップされ、Zipに何も含まれなかった
				fileTar.delete();
			}
		} catch (Exception e) {
			if (fileTar != null) {
				fileTar.delete();
			}
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new FileIoArchiveException(SmMessageId.SM005079S, e);
		} finally {
			TriFileUtils.closeOfOutputStream(tos);
		}
	}

	/**
	 * Tar圧縮サブ。再帰的にディレクトリ階層を探索して圧縮指示を行う<br>
	 *
	 * @param tos Tar出力用ストリーム
	 * @param basePath 圧縮指定された基底パス
	 * @param curPath 再帰的に動作する際のカレントパス
	 * @throws Exception
	 */
	private static void CompressTarReentrant(TarOutputStream tos, String basePath, String curPath) throws Exception {
		File baseFile = new File(basePath);
		File file = new File(baseFile.getParent() + File.separator + curPath);
		if (true != file.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004060F , file.getPath() );
		}
		if (file.isDirectory()) {// ディレクトリ
			log.debug("ディレクトリ： " + curPath + " " + basePath);
			CompressTarProc(tos, file, curPath);// 空ディレクトリを作成
			// ディレクトリの中身を再帰的に捜索
			String[] list = file.list();
			if (null == list) {
				throw new FileIoArchiveException(SmMessageId.SM004061F , curPath );
			}
			for (String listData : list) {
				String path = TriStringUtils.linkPathBySlash(curPath, listData);
				CompressTarReentrant(tos, basePath, path);
			}
		} else {// ファイル
			log.debug("ファイル ： " + curPath + " " + basePath);
			CompressTarProc(tos, file, curPath);
		}
	}

	/**
	 * Tar圧縮サブ。実際に圧縮処理を行う<br>
	 *
	 * @param tos Tar出力用ストリーム
	 * @param file 再帰的に動作する際のFileハンドラ
	 * @param pathOffset 再帰的に動作する際の相対パス
	 * @throws Exception
	 */
	private static void CompressTarProc(TarOutputStream tos, File file, String pathOffset) throws Exception {

		BufferedInputStream bis = null;
		try {
			String entry = null;
			if (file.isDirectory()) {// ディレクトリは空ディレクトリをTarEntryに登録
				entry = pathOffset + FilePathDelimiter.DELIMITER;
				// TarEntryを生成
				TarEntry te = new TarEntry(entry);
				tos.putNextEntry(te);
				te.setModTime(file.lastModified());
				log.debug("空ディレクトリを作成した： " + entry);
			} else {// ファイルはそのまま登録
				entry = pathOffset;
				long fileSize = (int) file.length();
				bis = new BufferedInputStream(new FileInputStream(file));
				// TarEntryを生成
				TarEntry te = new TarEntry(entry);
				te.setSize(fileSize);
				te.setModTime(file.lastModified());// タイムスタンプの保存
				tos.putNextEntry(te);

				if (0 == fileSize) {// サイズ０のファイルを圧縮しようとした場合
					tos.write(new byte[0], 0, 0);
				} else {// 通常のファイル
					int iData;
					while (true) {
						if (EOF == (iData = bis.read())) {
							break;
						}
						tos.write((byte) iData);
					}
				}
			}
		} catch (Exception e) {
			throw new FileIoArchiveException(SmMessageId.SM005080S, e);
		} finally {
			tos.flush();
			tos.closeEntry();
			TriFileUtils.close(bis);
		}
	}

	/**
	 * Tar形式のファイルを展開する<br>
	 *
	 * <pre>
	 * 【現時点での問題点】
	 * ・日本語ファイル名が文字化けする。
	 * ・致命的に挙動不審。
	 * </pre>
	 *
	 * @param srcPath 圧縮済みTarファイルのフルパス
	 * @param dstPath 展開先ディレクトリのフルパス
	 * @throws Exception
	 */
	public static void UnCompressTar(String srcPath, String dstPath) throws Exception {

		// 読込元Tarファイルの実在チェック
		if (null == srcPath) {
			throw new FileIoArchiveException(SmMessageId.SM004062F , srcPath );
		}
		File fileTar = new File(srcPath);
		if (true != fileTar.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004063F , srcPath );
		}
		if (true != fileTar.isFile()) {
			throw new FileIoArchiveException(SmMessageId.SM004064F , srcPath );
		}
		if (true != fileTar.canRead()) {
			throw new FileIoArchiveException(SmMessageId.SM004065F , srcPath );
		}
		// 出力先パスの実在チェック
		if (null == dstPath) {
			throw new FileIoArchiveException(SmMessageId.SM004072F , dstPath );
		}
		File fileOut = new File(dstPath);
		if (true != fileOut.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004066F , dstPath );
		}
		if (true != fileOut.isDirectory()) {
			throw new FileIoArchiveException(SmMessageId.SM004067F , dstPath );
		}
		// 展開ロジックメイン
		TarInputStream tis = null;
		try {
			// TarInputStreamを生成
			tis = new TarInputStream(new BufferedInputStream(new FileInputStream(fileTar)));

			while (true) {
				TarEntry entry = tis.getNextEntry();
				if (null == entry) {
					break;
				}
				File file = new File(dstPath + File.separator + entry.getName());
				if (entry.isDirectory()) {
					log.debug(" ディレクトリ : " + entry.getName());
					file.mkdirs();// ディレクトリ作成
				} else {
					log.debug(" ファイル : " + entry.getName());
					// ファイルデータの読み込み
					BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(file));
					while (true) {
						int iData = tis.read();
						if (EOF == iData) {
							break;
						}
						bos.write(iData);
					}
					bos.flush();
					bos.close();
				}
				// タイムスタンプの書き戻し
				file.setLastModified(entry.getModTime().getTime());
			}
		} catch (Exception e) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new FileIoArchiveException(SmMessageId.SM005081S, e);
		} finally {
			TriFileUtils.close(tis);
		}
	}
}
