package jp.co.blueship.tri.fw.cmn.io;

import jp.co.blueship.tri.fw.cmn.io.constants.ObjType;

/**
 * ファイル／ディレクトリ操作時に用いる、オブジェクト情報ハンドラクラス<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class ObjInfo {

	

	private String name = null;
	private ObjType type;
	private java.util.Date createDate;
	private java.util.Date updateDate;
	private long size;
	private boolean absent;

	// private String user = null ;
	// private String group = null ;

	/*
	 * public String getGroup() { return group; }
	 */
	/*
	 * public void setGroup(String group) { this.group = group; }
	 */
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getSize() {
		return size;
	}

	public void setSize(long size) {
		this.size = size;
	}

	public java.util.Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(java.util.Date createDate) {
		this.createDate = createDate;
	}

	public java.util.Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(java.util.Date updateDate) {
		this.updateDate = updateDate;
	}

	public ObjType getType() {
		return type;
	}

	public void setType(ObjType type) {
		this.type = type;
	}

	/*
	 * public String getUser() { return user; }
	 */
	/*
	 * public void setUser(String user) { this.user = user; }
	 */

	public boolean isFile() {
		return (ObjType.TYPE_FILE == this.type) ? true : false;
	}

	public boolean isDirectory() {
		return (ObjType.TYPE_DIRECTORY == this.type) ? true : false;
	}

	public void setAbsent(boolean absent) {
		this.absent = absent;
	}

	public boolean isAbsent() {
		return absent;
	}

	public boolean isPresent() {
		return !isAbsent();
	}

}
