package jp.co.blueship.tri.fw.cmn.io.diff;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.lang.StringUtils;
import org.mozilla.universalchardet.UniversalDetector;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.SystemProps;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import name.fraser.neil.plaintext.diff_match_patch;
import name.fraser.neil.plaintext.diff_match_patch.Diff;

/**
 * diff_match_patch(http://code.google.com/p/google-diff-match-patch/)を使用して、
 * Diffの機能を提供するクラスです。
 * また、文字コードの判定はjuniversalchardet(http://code.google.com/p/juniversalchardet/)
 * を使用しています。
 * <br>
 * 比較元、比較先の内容の等しいと判断された行が、同じ行のDiffの結果に含まれます。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EqualsContentsSetSameLineDiffer implements IDiffer {



	private String fileContentsSrc = null;
	private String fileContentsDst = null;

	private String encodedSrc = null ;
	private String encodedDst = null ;

	private String separator = null;
	private List<String> exceptionList = null;

	/**
	 * 変更の状態
	 */
	public static enum Status {
		ADD,
		DELETE,
		CHANGE,
		EQUAL,
		ERROR,
	}

	/**
	 * デフォルトコンストラクタ
	 * 何もしません。
	 */
	@SuppressWarnings("unused")
	private EqualsContentsSetSameLineDiffer(){}

	/**
	 * コンストラクタ<br>
	 * 比較内容を直接Stringで受け取ります
	 * @param srcContents		比較元内容
	 * @param dstContents		比較先内容
	 * @param encodedSrc		比較元の文字コード
	 * @paramp encodedDst		比較先の文字コード
	 */
	public EqualsContentsSetSameLineDiffer( String srcContents, String dstContents, String encodedSrc, String encodedDst ){
		this.separator = SystemProps.LineSeparator.getProperty();

		this.fileContentsSrc = srcContents;
		this.fileContentsDst = dstContents;

		this.encodedSrc = encodedSrc;
		this.encodedDst = encodedDst;
	}


	/**
	 * コンストラクタ
	 * @param srcFile		比較元ファイル
	 * @param dstFile		比較先ファイル
	 * @throws TriSystemException
	 */
	public EqualsContentsSetSameLineDiffer( File srcFile, File dstFile ) throws TriSystemException {
		/*
		if ( !srcFile.exists() ) {
			throw new SystemException(
				"比較元ファイルが存在しません。比較元ファイル = " + srcFile.toString() );
		}

		if ( !srcFile.isFile() ) {
			throw new SystemException(
				"比較元がファイルではありません。比較元 = " + srcFile.toString() );
		}

		if ( !dstFile.exists() ) {
			throw new SystemException(
				"比較先ファイルが存在しません。比較先ファイル = " + dstFile.toString() );
		}

		if ( !dstFile.isFile() ) {
			throw new SystemException(
				"比較先がファイルではありません。比較先 = " + dstFile.toString() );
		}
		*/

		this.separator = SystemProps.LineSeparator.getProperty();

		String encodingStr = null ;
		if( srcFile.exists() ) {
			encodingStr = this.getEncording( srcFile.getPath() ) ;
		} else if( dstFile.exists() ) {
			encodingStr = this.getEncording( dstFile.getPath() ) ;
		} else {
			;
		}

		if( srcFile.exists() && srcFile.isFile() ) {
			ConvertedData convertedSrc = this.convertString( srcFile , encodingStr ) ;
			this.fileContentsSrc = convertedSrc.getData() ;
			this.encodedSrc = convertedSrc.getEncoding() ;
		} else {
			this.fileContentsSrc = "" ;
			this.encodedSrc = null ;
		}
		if( dstFile.exists() && dstFile.isFile() ) {
			ConvertedData convertedDst = this.convertString( dstFile , encodingStr );
			this.fileContentsDst = convertedDst.getData() ;
			this.encodedDst = convertedDst.getEncoding() ;
		} else {
			this.fileContentsDst = "" ;
			this.encodedDst = null ;
		}
	}

	/**
	 * コンストラクタ
	 * 【文字コードセットを受け取る：文字コードセットは共通の１つのみとする】
	 * @param srcFile		比較元ファイル
	 * @param dstFile		比較先ファイル
	 * @param Charset	Charset
	 * @throws TriSystemException
	 */
	public EqualsContentsSetSameLineDiffer( File srcFile, File dstFile , Charset charset ) throws TriSystemException {

		this.separator = SystemProps.LineSeparator.getProperty();

		if( srcFile.exists() && srcFile.isFile() ) {
			ConvertedData convertedSrc = this.convertString( srcFile , charset ) ;
			this.fileContentsSrc = convertedSrc.getData() ;
			this.encodedSrc = convertedSrc.getEncoding() ;
		} else {
			this.fileContentsSrc = "" ;
			this.encodedSrc = null ;
		}
		if( dstFile.exists() && dstFile.isFile() ) {
			ConvertedData convertedDst = this.convertString( dstFile , charset );
			this.fileContentsDst = convertedDst.getData() ;
			this.encodedDst = convertedDst.getEncoding() ;
		} else {
			this.fileContentsDst = "" ;
			this.encodedDst = null ;
		}

	}

	/**
	 * コンストラクタ
	 * 【文字コードセットを受け取る】
	 * @param srcFile		比較元ファイル
	 * @param srcCharset	比較元Charset
	 * @param dstFile		比較先ファイル
	 * @param dstCharset	比較先Charset
	 * @throws TriSystemException
	 */
	public EqualsContentsSetSameLineDiffer( File srcFile, Charset srcCharset , File dstFile , Charset dstCharset ) throws TriSystemException {

		/*if ( !srcFile.exists() ) {
			throw new SystemException(
				"比較元ファイルが存在しません。比較元ファイル = " + srcFile.toString() );
		}

		if ( !srcFile.isFile() ) {
			throw new SystemException(
				"比較元がファイルではありません。比較元 = " + srcFile.toString() );
		}

		if ( !dstFile.exists() ) {
			throw new SystemException(
				"比較先ファイルが存在しません。比較先ファイル = " + dstFile.toString() );
		}

		if ( !dstFile.isFile() ) {
			throw new SystemException(
				"比較先がファイルではありません。比較先 = " + dstFile.toString() );
		}*/

		this.separator = SystemProps.LineSeparator.getProperty();

		if( srcFile.exists() && srcFile.isFile() ) {
			ConvertedData convertedSrc = this.convertString( srcFile , srcCharset ) ;
			this.fileContentsSrc = convertedSrc.getData() ;
			this.encodedSrc = convertedSrc.getEncoding() ;
		} else {
			this.fileContentsSrc = "" ;
			this.encodedSrc = null ;
		}
		if( dstFile.exists() && dstFile.isFile() ) {
			ConvertedData convertedDst = this.convertString( dstFile , dstCharset );
			this.fileContentsDst = convertedDst.getData() ;
			this.encodedDst = convertedDst.getEncoding() ;

		} else {
			this.fileContentsDst = "" ;
			this.encodedDst = null ;
		}

	}

	/**
	 * 改行コードのセッター<br>
	 * 指定せずにdiffを実行した場合、システムのデフォルト改行コードが採用されます。
	 * @param separator 改行コード
	 */
	public void setSeparator( String separator ) {
		this.separator = separator;
	}

	/**
	 * 例外文字列リストのセッター<br>
	 * 例外文字列が含まれる行において、例外文字列からその行末まではdiffの判断を行いません。
	 * @param exceptionList 例外文字列のリスト
	 */
	public void setException( List<String> exceptionList ) {
		this.exceptionList = exceptionList;
	}


	/**
	 * ファイルの差分を取得します。
	 */
	public DiffResult diff() {

		// 例外文字列を含む行のから、例外文字列以降をtrim
		Map<Long, String> srcTrimedInfo = new TreeMap<Long, String>();
		String trimedSrcFileContents =
			trimExceptionStrings( this.fileContentsSrc, srcTrimedInfo );

		Map<Long, String> dstTrimedInfo = new TreeMap<Long, String>();
		String trimedDstFileContents =
			trimExceptionStrings( this.fileContentsDst, dstTrimedInfo );


		// diffの実行
		diff_match_patch dmp = new diff_match_patch();
		LinkedList<Diff> diffs =
			dmp.diff_main(	trimedSrcFileContents, trimedDstFileContents , false ) ;

		// IDiffElementに変換
		List<IDiffElement> resultList = convertDiffResult( diffs );

		// trimした例外文字列以降の内容を戻す
		if ( srcTrimedInfo.size() > 0 || dstTrimedInfo.size() > 0 ) {

			for ( IDiffElement element : resultList ) {

				Long srcLineNum = new Long( element.getSrcLineNumber() );
				Long dstLineNum = new Long( element.getDstLineNumber() );

				if ( srcTrimedInfo.containsKey( srcLineNum ) ) {
					element.setSrcContents( srcTrimedInfo.get( srcLineNum ) );
				}
				if ( dstTrimedInfo.containsKey( dstLineNum ) ) {
					element.setDstContents( dstTrimedInfo.get( dstLineNum ) );
				}
			}
		}

		DiffResult diffResult = new DiffResult( resultList , this.encodedSrc , this.encodedDst ) ;
		return  diffResult;

	}

	/**
	 * 文字列から例外文字列を検索し、含まれていた場合は例外文字列から改行コード
	 * までをtrimして返します。
	 * @param strings		検索対象文字列
	 * @param trimInfoMap	検索対象文字列の何行目をtrimしたかと、その内容を格納する
	 * @return trim後の文字列
	 */
	private String trimExceptionStrings( String strings, Map<Long, String> trimInfoMap ) {

		if ( null == this.exceptionList || 0 == this.exceptionList.size() ) {
			return strings;
		}


		String wkString	= strings;


		for ( String exception : this.exceptionList ) {

			StringBuilder trimString	= new StringBuilder();
			long line = 1;

			while ( true ) {

				// 例外文字列が見つかった場合
				if ( StringUtils.indexOf( wkString, exception ) > -1 ) {

					// 例外文字列より前の内容を抽出
					String beforeString = StringUtils.substringBefore( wkString, exception );

					// trim前の情報保持：何行目なのかを算出
					line = StringUtils.countMatches( beforeString, this.separator ) + line;

					// trim前の情報保持：例外文字列が含まれていた行のうち、例外文字列より前を保持
					StringBuilder lineContents = new StringBuilder();
					if ( StringUtils.indexOf( beforeString, this.separator ) > -1 ) {
						lineContents.append( StringUtils.substringAfterLast( beforeString, this.separator ));
					} else {
						lineContents.append( beforeString );
					}
					lineContents.append( exception );


					// trim処理：例外文字列より前＋例外文字列を先ず格納
					trimString.append( beforeString );
					trimString.append( exception );


					// 例外文字列より後を抽出
					wkString = StringUtils.substringAfter( wkString, exception );

					// 例外文字列より後に改行コードがあれば
					if ( StringUtils.indexOf( wkString, this.separator ) > -1 ) {

						// trim前の情報保持：改行コードの前までも追加
						lineContents.append( StringUtils.substringBefore( wkString, this.separator ));

						// trim前の情報保持
						// すでに他の例外文字列でその行が処理されていることがあるので、
						// 処理済みでない場合のみ保持する
						Long lineNum = new Long( line );
						if ( !trimInfoMap.containsKey( lineNum ) ) {
							trimInfoMap.put( lineNum, lineContents.toString() );
						}


						// trim処理：例外文字列より後に改行コードがあれば、改行コードだけを続いて格納
						// => 例外文字列～改行コードを空文字で置換したイメージ(trim)
						trimString.append( this.separator );

						// 改行コードより後を抽出
						wkString = StringUtils.substringAfter( wkString, this.separator );
						line = line + 1;

					} else {
						// 例外文字列より後に改行がない場合は終了

						// trim前の情報保持
						lineContents.append( wkString );
						trimInfoMap.put( new Long( line ), lineContents.toString() );

						break;
					}

				} else {
					// 例外文字列がなければ終了
					trimString.append( wkString );
					break;
				}
			}

			wkString = trimString.toString();

		}

		return wkString;

	}

//	/**
//	 * Fileオブジェクトの内容を文字列に変換します。
//	 * 変換の際、文字コードを判定して取り込みます。
//	 * @param file 変換対象のファイルオブジェクト
//	 * @return 変換後の文字列
//	 */
//	private ConvertedData convertString( File file ) throws SystemException {
//
//		BufferedReader br = null;
//		StringBuilder contents = new StringBuilder();
//
//		try {
//			String encoding = getEncording( file.getAbsolutePath() ) ;
//			br = new BufferedReader(
//					new InputStreamReader(
//							new FileInputStream( file.getAbsolutePath() ), encoding));
//
//			String line = null;
//			while( ( line = br.readLine() ) != null) {
//				contents.append( line );
//				if ( line != null ) {
//					contents.append( this.separator );
//				}
//			}
//
//			return new ConvertedData( contents.toString() , encoding ) ;
//
//		} catch ( IOException ioe ) {
//			throw new SystemException( "比較対象ファイルの読み込み中にエラーが発生しました。", ioe );
//		} finally {
//			try {
//				if ( null != br ) br.close();
//			} catch ( IOException ioe ) {}
//		}
//	}

	/**
	 * Fileオブジェクトの内容を文字列に変換します。
	 * 文字コードセットは受け取ります
	 * @param file 変換対象のファイルオブジェクト
	 * @param charset 変換対象のファイルのCharset文字列
	 * @return 変換後の文字列
	 */
	private ConvertedData convertString( File file , String charset ) throws TriSystemException {

		FileInputStream fis = null ;
		InputStreamReader isr = null ;
		BufferedReader br = null;
		StringBuilder contents = new StringBuilder();

		try {
			fis = new FileInputStream( file.getAbsolutePath() ) ;
			isr = new InputStreamReader( fis , charset ) ;
			br = new BufferedReader( isr );

			String line = null;
			while( ( line = br.readLine() ) != null) {
				contents.append( line );
				if ( line != null ) {
					contents.append( this.separator );
				}
			}

			return new ConvertedData( contents.toString() , charset ) ;

		} catch ( IOException ioe ) {
			throw new TriSystemException( SmMessageId.SM005068S, ioe );
		} finally {
			TriFileUtils.close(br);
			TriFileUtils.close(isr);
			TriFileUtils.close(fis);
		}
	}

	/**
	 * Fileオブジェクトの内容を文字列に変換します。
	 * 文字コードセットは受け取ります
	 * @param file 変換対象のファイルオブジェクト
	 * @param charset 変換対象のファイルのCharset
	 * @return 変換後の文字列
	 */
	private ConvertedData convertString( File file , Charset charset ) throws TriSystemException {

		FileInputStream fis = null ;
		InputStreamReader isr = null ;
		BufferedReader br = null;
		StringBuilder contents = new StringBuilder();

		try {
			fis = new FileInputStream( file.getAbsolutePath() ) ;
			isr = new InputStreamReader( fis , charset.toString() ) ;
			br = new BufferedReader( isr );

			String line = null;
			while( ( line = br.readLine() ) != null) {
				contents.append( line );
				if ( line != null ) {
					contents.append( this.separator );
				}
			}

			return new ConvertedData( contents.toString() , charset.toString() ) ;

		} catch ( IOException ioe ) {
			throw new TriSystemException( SmMessageId.SM005068S, ioe );
		} finally {
			TriFileUtils.close(br);
			TriFileUtils.close(isr);
			TriFileUtils.close(fis);
		}
	}
	/**
	 * 比較元と比較先の内容から、IDiffElementのリストを作成します。
	 * @param srcList 比較元の内容
	 * @param dstList 比較先の内容
	 * @param srcLineNum 比較元の行数
	 * @param dstLineNum 比較先の行数
	 * @return
	 */
	private List<IDiffElement> createDiffResult(
			List<String> srcList, List<String> dstList, long[] lineNum ) {

		List<IDiffElement> resultList = new ArrayList<IDiffElement>();

		for ( int i = 0; i <
			( ( srcList.size() >= dstList.size() )? srcList.size() : dstList.size()); i++ ) {

			// 比較元と比較先のリストの大きい方に合わせてループさせているので、
			// 足りなくなった方は空文字で補完する(行が追加・削除されているイメージ)。
			String srcContents = "";
			String dstContents = "";
			long tmpSrcLineNum = 0;
			long tmpDstLineNum = 0;

			if ( i < srcList.size() ) {

				lineNum[0] = lineNum[0] + 1;
				tmpSrcLineNum = lineNum[0];

				srcContents = srcList.get( i );
			}
			if ( i < dstList.size() ) {

				lineNum[1] = lineNum[1] + 1;
				tmpDstLineNum = lineNum[1];

				dstContents = dstList.get( i );
			}


			Status status = Status.CHANGE;

			// 比較元と比較先が等しい場合
			if ( srcContents.equals( dstContents ) ) {

				if ( srcContents.length() > 0 && dstContents.length() > 0 ) {
					// 内容が空文字でない場合
					status = Status.EQUAL;
				} else if ( i < srcList.size() && i < dstList.size() ) {
					// 本当に空文字同士の場合
					status = Status.EQUAL;
				} else if ( i < srcList.size() && i >= dstList.size() ) {
					// 比較先が補完されている場合
					status = Status.DELETE;
				} else if ( i >= srcList.size() && i < dstList.size() ) {
					// 比較元が補完されている場合
					status = Status.ADD;
				}

			} else {

				if ( srcContents.length() == 0 ) {
					// 比較元が補完されている場合
					status = Status.ADD;
				} else if ( dstContents.length() == 0 ) {
					// 比較先が補完されている場合
					status = Status.DELETE;
				}

			}

			IDiffElement element = new EqualsContentsSetSameLineDiffResult(
								status, tmpSrcLineNum, srcContents, tmpDstLineNum, dstContents );
			resultList.add( element );

		}

		return resultList;
	}

	/**
	 * diff_match_patchの結果を、IDiffElementのリストに変換します。
	 * @param diffs Diffのリスト
	 * @param dstString 比較先文字列
	 * @return IDiffElementオブジェクトのリスト
	 */
	private List<IDiffElement> convertDiffResult( LinkedList<Diff> diffs ) {

		List<IDiffElement> resultList = new ArrayList<IDiffElement>();

		List<String> srcList	= new ArrayList<String>();
		List<String> dstList	= new ArrayList<String>();

		long[] lineNum = { 0, 0};

		boolean appendFlag		= false;
		boolean srcAppendFlag	= false;
		boolean dstAppendFlag	= false;


		for ( Diff diff : diffs ) {

			diff_match_patch.Operation op = diff.operation;
			String diffContents = diff.text;
			String[] arrContents = splitNewLine( diffContents );

			// diff_match_patchの結果がEQUALで、その内容の中に１行まるまる等しい行があれば、
			// その行を結果の左右の同じ行に並べる。
			// そのため、それ以前の編集結果をひとつのブロックとして処理する。
			if ( op.equals( diff_match_patch.Operation.EQUAL ) &&
					isSameLine( arrContents, srcAppendFlag, dstAppendFlag ) &&
					( srcList.size() > 0 || dstList.size() > 0 )) {

				// 編集開始

				// 行の途中からEQUALの場合、最初の改行までの内容を、前回の結果に加える。
				if ( srcAppendFlag || dstAppendFlag ) {
					if ( srcAppendFlag ) {
						appendContents( srcList, arrContents[0] );
					} else {
						srcList.add( arrContents[0] );
					}
					if ( dstAppendFlag ) {
						appendContents( dstList, arrContents[0] );
					} else {
						dstList.add( arrContents[0] );
					}
				}


				// 編集
				resultList.addAll( createDiffResult( srcList, dstList, lineNum ) );

				// 行の途中からEQUALの場合、最初の改行までの内容を、前回の結果に加えてしまうので、
				// 最初の改行までの内容は取り除く。
				if ( srcAppendFlag || dstAppendFlag ) {
					String[] tmpContents = new String[ arrContents.length - 1 ];
					for ( int i = 0; i < arrContents.length - 1; i++ ) {
						tmpContents[ i ] = arrContents[ i + 1 ];
					}
					arrContents = tmpContents;
				}

				srcList			= new ArrayList<String>();
				dstList			= new ArrayList<String>();
				appendFlag		= false;
				srcAppendFlag	= false;
				dstAppendFlag	= false;

			}


			// 結果が改行コードだけでない場合
			if ( arrContents.length > 0 ) {

				// 改行コードで分割した配列でループ
				for ( int i = 0; i < arrContents.length; i++ ) {

					String contents = arrContents[ i ];

					// 最後が改行コードで終わっていない場合、次の差分を追加する
					// 必要があるためマーキング
					if ( i == arrContents.length - 1 && !diffContents.endsWith( this.separator )) {
						appendFlag = true;
					} else {
						appendFlag = false;
					}

					if ( op.equals( diff_match_patch.Operation.EQUAL )) {

						// 前回分の差分に追加する必要がある場合
						if ( srcAppendFlag ) {
							appendContents( srcList, contents );
							srcAppendFlag = false;
						} else {
							srcList.add( contents );
						}

						if ( dstAppendFlag ) {
							appendContents( dstList, contents );
							dstAppendFlag = false;
						} else {
							dstList.add( contents );
						}

						// 次回の処理で追加する必要がある場合
						if ( appendFlag ) {
							srcAppendFlag	= true;
							dstAppendFlag	= true;
						}

					} else if ( op.equals( diff_match_patch.Operation.DELETE )) {

						if ( srcAppendFlag ) {
							appendContents( srcList, contents );
							srcAppendFlag = false;
						} else {
							srcList.add( contents );
						}

						if ( appendFlag ) {
							srcAppendFlag	= true;
						}

					// INSERT
					} else {

						if ( dstAppendFlag ) {
							appendContents( dstList, contents );
							dstAppendFlag = false;
						} else {
							dstList.add( contents );
						}

						if ( appendFlag ) {
							dstAppendFlag	= true;
						}
					}
				}

			// arrContents.length == 0 => 改行コードのみがDiffの結果として取得された場合
			} else {

				if ( op.equals( diff_match_patch.Operation.EQUAL )) {

					// 前回の差分に改行コードが入っていた場合、改行のみの行を追加する。
					if ( !srcAppendFlag ) {
						srcList.add( "" );
					}
					if ( !dstAppendFlag ) {
						dstList.add( "" );
					}

					// 改行コードが差分で入ってきたので、次の差分を追加する
					// 必要があるためのマーキングをリセット
					srcAppendFlag	= false;
					dstAppendFlag	= false;

				} else if ( op.equals( diff_match_patch.Operation.DELETE )) {

					if ( !srcAppendFlag ) {
						srcList.add( "" );
					}

					srcAppendFlag	= false;

				// INSERT
				} else {

					if ( !dstAppendFlag ) {
						dstList.add( "" );
					}

					dstAppendFlag	= false;
				}
			}
		}

		// 最後にEQUALが現れないパターン
		if ( srcList.size() > 0 || dstList.size() > 0 ) {
			resultList.addAll( createDiffResult( srcList, dstList, lineNum ) );
		}

		return resultList;
	}

	/**
	 * 指定されたリストの最後の内容に、指定された内容を追加します
	 * @param contentsList リスト
	 * @param contents 追加する内容
	 */
	private void appendContents( List<String> contentsList, String contents ) {

		int lastIndex = contentsList.size() - 1;
		String newContents = contentsList.get( lastIndex ) + contents;

		contentsList.set( lastIndex, newContents );

	}

	/**
	 * diff_match_patchの結果がEQUALの場合に、１行すべて等しいかを検証します
	 * @param arrContents diff_match_patchの結果を改行コードで分割した配列
	 * @param srcAppendFlag 前回の比較元の差分が改行で終わっていないことを示すフラグ
	 * @param dstAppendFlag 前回の比較先の差分が改行で終わっていないことを示すフラグ
	 * @return １行すべて等しければtrue、そうでなければfalse
	 */
	private boolean isSameLine( String[] arrContents, boolean srcAppendFlag, boolean dstAppendFlag ) {

		boolean ret = false;

		// 比較元、比較先ともに改行で終わった後のEQUAL
		if ( !srcAppendFlag && !dstAppendFlag ) {
			ret = true;

		} else {
			// ...等しい文字列\n	| ...等しい文字列\n
			// 等しい行\n			| 等しい行\n
			// ...					| ...

			// ↑であれば、配列の2つめが等しい行ということになる
			if ( arrContents.length > 2 ) {
				ret = true;
			}
		}

		return ret;

	}

	/**
	 * 改行コードで分割します。
	 * @param contents 分割対象の文字列
	 * @return 分割後の配列
	 */
	private String[] splitNewLine( String contents ) {

		String[] spl = contents.split( this.separator );

		// 最後にセパレータが入っていると捨てられてしまうので、
		// その分、""を足しこむ
		if ( contents.endsWith( this.separator ) ) {

			// 文字列の最後にいくつ改行コードがあるかを調べる
			int num = 0;
			String before = contents;
			String after = null;
			while ( true ) {
				after = StringUtils.chomp( before, this.separator );
				if ( before.equals( after )) {
					break;
				} else {
					num = num + 1;
					before = after;
				}
			}

			String[] tmp = new String[ spl.length + num - 1 ];
			for ( int i = 0; i < spl.length ; i++ ) {
				tmp[ i ] = spl[ i ];
			}
			for ( int i = 0; i < num - 1; i++ ) {
				tmp[ spl.length + i ] = "";
			}
			spl = tmp;
		}

		return spl;
	}

	/**
	 * 文字コードを判定します。
	 * @param absolutePath 文字コード判定対象のファイルパス
	 * @return 判定された文字コード
	 * @throws TriSystemException
	 */
	private String getEncording( String absolutePath ) throws TriSystemException {

		final int SIZE_BYTE = 65536 ;

		UniversalDetector detector	= null;
		FileInputStream fis			= null;

		try {
			detector	= new UniversalDetector(null);
			fis			= new FileInputStream( absolutePath );

			byte[] buf = new byte[ SIZE_BYTE ];
			//int nread;
			//
			//while ( (nread = fis.read(buf) ) > 0 && ! detector.isDone() ) {
			//	detector.handleData(buf, 0, nread);
			//}

			List<Byte> byteList = new ArrayList<Byte>() ;
			while( 0 < fis.read(buf) ) {
				for( int i = 0 ; i < SIZE_BYTE ; i++ ) {
					byteList.add( buf[ i ] ) ;
				}
			}
			byte[] readByte = new byte[ byteList.size() ] ;
			for( int i = 0 ; i < byteList.size() ; i++ ) {
				readByte[ i ] = byteList.get( i ) ;
			}
			detector.handleData( readByte , 0 , byteList.size() ) ;
			detector.dataEnd();


			String encoding = detector.getDetectedCharset();
			if ( encoding == null) {
				encoding = "JISAutoDetect";
			}

			return encoding;

		} catch ( IOException ioe ) {
			throw new TriSystemException( SmMessageId.SM005069S, ioe );
		} finally {
			TriFileUtils.close(fis);
			if ( detector != null ) detector.reset();
		}

	}

	/**
	 * 内部クラス
	 */
	private class ConvertedData {
		private String data = null ;
		private String encoding = null ;

		/**
		 * コンストラクタ
		 * @param encoding
		 * @param data
		 */
		public ConvertedData( String data , String encoding ) {
			this.data = data ;
			this.encoding = encoding ;
		}

		/**
		 * データ
		 * @return
		 */
		public String getData() {
			return data;
		}
		/**
		 * データ
		 * @param data
		 */
		/* 使用されないメソッド
		public void setData(String data) {
			this.data = data;
		}*/
		/**
		 * 文字コード
		 * @return
		 */
		public String getEncoding() {
			return encoding;
		}
		/**
		 * 文字コード
		 * @param encoding
		 */
		/* 使用されないメソッド
		public void setEncoding(String encoding) {
			this.encoding = encoding;
		}*/

	}
}