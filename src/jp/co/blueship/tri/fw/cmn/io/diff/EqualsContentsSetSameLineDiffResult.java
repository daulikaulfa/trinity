package jp.co.blueship.tri.fw.cmn.io.diff;

import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;

/**
 * Diffの結果を格納するクラスです。
 * <br>
 * 比較元、比較先の内容の等しい部分が、同じ行に含まれるように格納します。
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class EqualsContentsSetSameLineDiffResult implements IDiffElement {

	private static final long serialVersionUID = 1L;



	private Status status;
	private long srcLineNum;
	private String srcContents;
	private long dstLineNum;
	private String dstContents;

	/**
	 * デフォルトコンストラクタ
	 * 何もしません。
	 */
	@SuppressWarnings("unused")
	private EqualsContentsSetSameLineDiffResult(){}

	/**
	 * コンストラクタ
	 * @param status		変更の状態
	 * @param srcLineNum	比較元の行数
	 * @param srcContents	比較元の内容
	 * @param dstLineNum	比較先の行数
	 * @param dstContents	比較先の内容
	 */
	public EqualsContentsSetSameLineDiffResult(
			Status status, long srcLineNum, String srcContents, long dstLineNum, String dstContents ) {

		this.status			= status;
		this.srcLineNum		= srcLineNum;
		this.srcContents	= srcContents;
		this.dstLineNum		= dstLineNum;
		this.dstContents	= dstContents;

	}

	/**
	 * 変更の状態を取得します。
	 */
	public Status getStatus() {
		return this.status;
	}
	/**
	 * 変更の状態を設定します。
	 */
	public void setStatus( Status status ) {
		this.status = status;
	}

	/**
	 * 比較元の行数を取得します。
	 */
	public long getSrcLineNumber() {
		return this.srcLineNum;
	}
	/**
	 * 比較元の行数を設定します。
	 */
	public void setSrcLineNumber( long srcLineNum ) {
		this.srcLineNum = srcLineNum;
	}

	/**
	 * 比較元の内容を取得します。
	 */
	public String getSrcContents() {
		return this.srcContents;
	}
	/**
	 * 比較元の内容を設定します。
	 */
	public void setSrcContents( String srcContents ) {
		this.srcContents = srcContents;
	}

	/**
	 * 比較先の行数を取得します。
	 */
	public long getDstLineNumber() {
		return this.dstLineNum;
	}
	/**
	 * 比較先の行数を設定します。
	 */
	public void setDstLineNumber( long dstLineNum ) {
		this.dstLineNum = dstLineNum;
	}

	/**
	 * 比較先の内容を取得します。
	 */
	public String getDstContents() {
		return this.dstContents;
	}
	/**
	 * 比較先の内容を設定します。
	 */
	public void setDstContents( String dstContents ) {
		this.dstContents = dstContents;
	}
}