package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.War;
import org.apache.tools.ant.types.ZipFileSet;

import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * War作成を行うクラス
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class WarCreator extends ArchiveCreator {

	/**
	 * Warファイルを作成する
	 * @param warFile 作成するWarファイル
	 * @param baseDir War化対象ファイルのベース(ルート)ディレクトリ
	 * @param srcList War化対象のパッケージ名、またはファイル名のリスト
	 * @param webXmlFile 取り込むweb.xmlファイル
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するWarファイルが既存の場合の動作
	 */
	public void execute(
			File warFile , File baseDir, String[] srcList ,
			File webXmlFile , File manifestFile, Duplicate dup ) throws TriRuntimeException {

		try {

			checkArgument( srcList , warFile , webXmlFile, manifestFile, dup );
			if ( !super.prepare( warFile, dup ) ) {
				return;
			}


			Project project = super.getProject( warFile );
			War war			= getWar( warFile, project, webXmlFile, manifestFile, dup );

			super.setFileSet( baseDir, srcList, project, war );

			war.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005146S, e , warFile.getAbsolutePath() );
		}

	}

	/**
	 * Warファイルを作成する
	 * @param warFile 作成するWarファイル
	 * @param warFileSetList War化対象のファイルリスト
	 * @param webInfoFileSetList WEB-INF配下に取り込む設定ファイルのファイルリスト
	 * @param libFileSetList WEB-INF/lib配下に取り込むライブラリファイルのファイルリスト
	 * @param classFileSetList WEB-INF/classes配下に取り込むクラスファイルのファイルリスト
	 * @param webXmlFile 取り込むweb.xmlファイル
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するWarファイルが既存の場合の動作
	 */
	public void execute(
			File warFile ,
			FileSetParamBean[] warFileSetList ,
			FileSetParamBean[] webInfoFileSetList,
			FileSetParamBean[] libFileSetList,
			FileSetParamBean[] classFileSetList,
			File webXmlFile , File manifestFile, Duplicate dup ) throws TriRuntimeException {

		try {

			checkArgument( warFileSetList , warFile , webXmlFile, manifestFile, dup );
			if ( !super.prepare( warFile, dup ) ) {
				return;
			}


			Project project = super.getProject( warFile );
			War war			= getWar( warFile, project, webXmlFile, manifestFile, dup );

			// FileSet
			super.setFileSet( warFileSetList, project, war );

			// Web-Inf配下
			if ( null != webInfoFileSetList ) {

				for ( FileSetParamBean fileSetParam : webInfoFileSetList ) {

					File absFile = new File( fileSetParam.getDir(), fileSetParam.getSrc() );

					if ( absFile.exists() ) {

						ZipFileSet zipFileSet = getZipFileSet( absFile, project, fileSetParam );
						zipFileSet.setExcludes( "**/web.xml" );

						war.addWebinf( zipFileSet );
					}
				}
			}


			// Web-Inf/lib配下
			if ( null != libFileSetList ) {

				for ( FileSetParamBean fileSetParam : libFileSetList ) {

					File absFile = new File( fileSetParam.getDir(), fileSetParam.getSrc() );

					if ( absFile.exists() ) {

						ZipFileSet zipFileSet = getZipFileSet( absFile, project, fileSetParam );
						war.addLib( zipFileSet );
					}
				}
			}


			// Web-Inf/classes配下
			if ( null != classFileSetList ) {

				for ( FileSetParamBean fileSetParam : classFileSetList ) {

					File absFile = new File( fileSetParam.getDir(), fileSetParam.getSrc() );

					if ( absFile.exists() ) {

						ZipFileSet zipFileSet = getZipFileSet( absFile, project, fileSetParam );
						war.addClasses( zipFileSet );
					}
				}
			}

			war.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005146S, e , warFile.getAbsolutePath() );
		}

	}

	/**
	 * ZipFileSetオブジェクトを取得する
	 * @param absFile ZipFileSet化対象のファイル
	 * @param project Project
	 * @param fileSetParam FileSetParamBean
	 * @return ZipFileSet
	 */
	private ZipFileSet getZipFileSet( File absFile, Project project, FileSetParamBean fileSetParam ) {

		ZipFileSet zipFileSet = new ZipFileSet();
		zipFileSet.setProject	( project );
		zipFileSet.setDir		( fileSetParam.getDir() );

		if ( absFile.isFile() ) {
			zipFileSet.setIncludes	( fileSetParam.getSrc() );
		} else {
			zipFileSet.setIncludes	( fileSetParam.getSrc() + "/**" );
		}

		return zipFileSet;
	}

	/**
	 * Warクラスのオブジェクトを作成する
	 */
	private War getWar(
			File warFile, Project project, File webXmlFile, File manifestFile, Duplicate dup ) {

		War war = new War();
		super.setInfo( war, warFile, project, manifestFile, dup );

		if ( null != webXmlFile ) {
			war.setWebxml( webXmlFile );
		}

		return war;
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			FileSetParamBean[] warFileSetList , File warFile ,
			File webXmlFile, File manifestFile, Duplicate dup ) throws TriRuntimeException {

		// War化対象ファイルのチェック
		if ( null == warFileSetList ) {
			throw new TriSystemException( SmMessageId.SM004154F );
		}

		checkArgument( warFile, webXmlFile, manifestFile, dup );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			String[] srcList , File warFile , File webXmlFile, File manifestFile, Duplicate dup )
		throws TriRuntimeException {

		// War化対象ファイルのチェック
		if ( null == srcList ) {
			throw new TriSystemException( SmMessageId.SM004155F );
		}

		checkArgument( warFile, webXmlFile, manifestFile, dup );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			File warFile , File webXmlFile, File manifestFile, Duplicate dup ) throws TriRuntimeException {

		// Warファイルのチェック
		if ( null == warFile ) {
			throw new TriSystemException( SmMessageId.SM004156F );
		}
		if ( warFile.isDirectory() ) {
			throw new TriSystemException( SmMessageId.SM004157F , warFile.getAbsolutePath() );
		}

		// web.xmlファイルのチェック
		if ( null == webXmlFile && !Duplicate.UPDATE.equals( dup ) ) {
			throw new TriSystemException( SmMessageId.SM004158F );
		}
		if ( null != webXmlFile ) {

			if( !webXmlFile.exists() || !webXmlFile.isFile() ) {
				throw new TriSystemException( SmMessageId.SM004159F , webXmlFile.getAbsolutePath() );
			}
		}

		// マニフェストファイルのチェック
		if ( null != manifestFile ) {

			if( !manifestFile.exists() || !manifestFile.isFile() ) {
				throw new TriSystemException( SmMessageId.SM005075S , manifestFile.getAbsolutePath() );
			}
		}
	}
}
