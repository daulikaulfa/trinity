package jp.co.blueship.tri.fw.cmn.io.diff;

import java.util.List;

public class DiffResult {
	
	private List<IDiffElement> diffElementList = null ;
	private String charsetSrc = null ;
	private String charsetMerge = null ;
	private String charsetDst = null ;
	
	/**
	 * コンストラクタ
	 * 文字コードが共通の場合に用いる。
	 * @param diffElementList
	 * @param charset
	 */
	public DiffResult( List<IDiffElement> diffElementList , String charset ) {
		this.diffElementList = diffElementList ;
		this.charsetSrc = charset ;
		this.charsetDst = charset ;
	}
	
	/**
	 * コンストラクタ
	 * @param diffElementList
	 * @param charsetSrc
	 * @param sharsetDst
	 */
	public DiffResult( List<IDiffElement> diffElementList , String charsetSrc , String charsetDst ) {
		this.diffElementList = diffElementList ;
		this.charsetSrc = charsetSrc ;
		this.charsetDst = charsetDst ;
	}
	
	/**
	 * コンストラクタ
	 * @param diffElementList
	 * @param charsetSrc
	 * @param sharsetDst
	 */
	public DiffResult( List<IDiffElement> diffElementList , String charsetSrc , String charsetMerge , String charsetDst ) {
		this.diffElementList = diffElementList ;
		this.charsetSrc = charsetSrc ;
		this.charsetMerge = charsetMerge ;
		this.charsetDst = charsetDst ;
	}
	
	public String getCharsetDst() {
		return charsetDst;
	}
	public void setCharsetDst(String charsetDst) {
		this.charsetDst = charsetDst;
	}
	public String getCharsetSrc() {
		return charsetSrc;
	}
	public void setCharsetSrc(String charsetSrc) {
		this.charsetSrc = charsetSrc;
	}
	public List<IDiffElement> getDiffElementList() {
		return diffElementList;
	}
	public void setDiffElementList(List<IDiffElement> diffElementList) {
		this.diffElementList = diffElementList;
	}
	public String getCharsetMerge() {
		return charsetMerge;
	}
	public void setCharsetMerge( String charsetMerge ) {
		this.charsetMerge = charsetMerge;
	}
	
}
