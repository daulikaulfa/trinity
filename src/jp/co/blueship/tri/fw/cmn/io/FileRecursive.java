package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.FileFilter;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 元と先の2箇所をrootに取って、元をbaseに再帰探査を行ないながら、
 * {@link jp.co.blueship.tri.fw.cmn.io.IRecursiveCallback}をimplementsした
 * 処理classの処理を行なう。
 */
public class FileRecursive {

	

	private File srcRoot = null;
	private File dstRoot = null;
	private IRecursiveCallback callback = null;
	private FileFilter filter = null;
	private CallbackExpansion callbackEx = null;

	/**
	 * constructor.
	 * @param src 比較元root
	 * @param dst 比較先root
	 * @param callback callback処理
	 * @param filter {@link File#listFiles(FileFilter)()}に使用されるフィルタの基準
	 */
	public FileRecursive(File src, File dst, IRecursiveCallback callback, FileFilter filter){
		this.srcRoot = src;
		this.dstRoot = dst;
		this.callback = callback;
		this.filter = filter;
	}

	/**
	 * constructor.
	 * @param src 比較元root
	 * @param dst 比較先root
	 * @param callback callback処理
	 * @param filter {@link File#listFiles(FileFilter)()}に使用されるフィルタの基準
	 */
	public FileRecursive(File src, File dst, CallbackExpansion callback, FileFilter filter){
		this.srcRoot = src;
		this.dstRoot = dst;
		this.callbackEx = callback;
		this.filter = filter;
	}

	/**
	 * 処理実行method。
	 */
	public void doRecursive(){

		if(! srcRoot.exists()){
			return;
		}

		if(srcRoot.isFile()){
			if(dstRoot.exists()){
				if(dstRoot.isFile()){
					doFileFile(srcRoot, dstRoot, srcRoot.getName());
				}else{
					doFileDirectory(srcRoot, dstRoot, srcRoot.getName());
				}
			}else{
				doFileNothing(srcRoot, dstRoot, srcRoot.getName());
			}
		}else{
			fileSearch(srcRoot, null, filter);
		}
	}

	/**
	 * 再帰の主処理。
	 * @param src 比較元directory
	 * @param parentPath 比較元のrootから、今処理しようとしているpathまでの差。
	 * @param filter {@link File#listFiles(FileFilter)()}に使用されます。
	 * 	初回callの場合はnullを設定する事。
	 */
	private void fileSearch(File src, String parentPath, FileFilter filter){

		File srcSubs[] = null;
		if( null == filter ) {
			srcSubs = src.listFiles();
		} else {
			srcSubs = src.listFiles( filter );
		}
		String myPath = parentPath;

		int i = 0;
		for(i = 0; i < srcSubs.length; i++){

			File srcSub = srcSubs[i];

			if(parentPath == null){
				myPath = srcSub.getName();
			}else{
				myPath = TriStringUtils.linkPathBySlash( parentPath , srcSub.getName() );
			}

			File nextDst = new File(dstRoot, myPath);
			if(srcSub.isDirectory()){
				boolean isContinue = false;
				if(nextDst.exists()){
					if(nextDst.isFile()){
						isContinue = doDirectoryFile(srcSub, nextDst, myPath);
					}else{
						isContinue = doDirectoryDirectory(srcSub, nextDst, myPath);
					}
				}else{
					isContinue = doDirectoryNothing(srcSub, nextDst, myPath);
				}
				if(isContinue){
					fileSearch(srcSub, myPath, filter);
				}
			}else{
				if(nextDst.exists()){
					if(nextDst.isFile()){
						doFileFile(srcSub, nextDst, myPath);
					} else {
						doFileDirectory(srcSub, nextDst, myPath);
					}
				}else{
					doFileNothing(srcSub, nextDst, myPath);
				}
			}
		}
	}

	private final void doFileFile(File src, File dst, String relatePath) {
		if ( null != callback )
			callback.doFileFile(src, dst);

		if ( null != callbackEx )
			callbackEx.doFileFile(src, dst, relatePath);
	}

	private final void doFileNothing(File src, File dst, String relatePath) {
		if ( null != callback )
			callback.doFileNothing(src, dst);

		if ( null != callbackEx )
			callbackEx.doFileNothing(src, dst, relatePath);
	}

	private final boolean doDirectoryDirectory(File src, File dst, String relatePath) {
		if ( null != callback )
			return callback.doDirectoryDirectory(src, dst);

		if ( null != callbackEx )
			callbackEx.doDirectoryDirectory(src, dst, relatePath);

		return true;
	}

	private final boolean doDirectoryFile(File src, File dst, String relatePath) {
		if ( null != callback )
			return callback.doDirectoryFile(src, dst);

		if ( null != callbackEx )
			callbackEx.doDirectoryFile(src, dst, relatePath);

		return false;
	}

	private final boolean doDirectoryNothing(File src, File dst, String relatePath) {
		if ( null != callback )
			return callback.doDirectoryNothing(src, dst);

		if ( null != callbackEx )
			callbackEx.doDirectoryNothing(src, dst, relatePath);

		return true;
	}

	private final void doFileDirectory(File src, File dst, String relatePath) {
		if ( null != callback )
			callback.doFileDirectory(src, dst);

		if ( null != callbackEx )
			callbackEx.doFileDirectory(src, dst, relatePath);
	}
}

