package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;


/**
 * 重複時、上書きcopyを行なうcopy logic.
 */public class FileCopyOverride implements FileCopy {

	/**
	 * @inheritDoc
	 * <p>
	 * 	dstが既に存在している場合は、上書きcopyを行なう。
	 * </p>
	 */	public boolean copyFileToFile(File src, File dst, List<File> duplicateList)
			throws IOException {

		if(dst.exists()){
			duplicateList.add(dst);
		}

		TriFileUtils.copyFileToFile(src, dst);

		return true;
	}

}
