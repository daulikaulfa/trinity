package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import jp.co.blueship.tri.fw.msg.SmMessageId;

//import jp.co.blueship.trinity.common.log.ILog;
//import jp.co.blueship.trinity.common.log.TriLogFactory;

/**
 * ファイルの「【GZIP形式】圧縮・展開」関連操作クラスに関する実装クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FileIoGZip {

	//private static final ILog log = TriLogFactory.getInstance() ;
	private static final int EOF = -1 ;

	/**
	 * GZip形式に圧縮を行う<br>
	 * @param srcPath 圧縮するファイルのフルパス
	 * @param dstPath 圧縮済みファイルの出力先フルパス
	 * @param doSkip 読込元のファイルが存在しなかった場合の処理モード
	 * <pre>
	 * 		true	:	当該ファイルを無視、正常終了
	 * 		false	:	エラーとしてGZip作成を中断
	 * </pre>
	 * @throws Exception
	 */
	public static void CompressGZip( String srcPath , String dstPath , boolean doSkip ) throws Exception {
//		読込元ファイルの実在チェック
		if( null == srcPath ) {
			throw new FileIoArchiveException( SmMessageId.SM004053F , srcPath) ;
		}
		File file = new File( srcPath ) ;
		if( true == file.exists() ) {
			CompressGZip( file , dstPath ) ;
		} else {
			if( true != doSkip ) {
				throw new FileIoArchiveException( SmMessageId.SM004054F ,srcPath) ;
			}
		}
	}

	/**
	 * GZip形式に圧縮を行う<br>
	 * @param file Fileハンドラ
	 * @param dstPath 圧縮済みファイルの出力先フルパス
	 * @throws Exception
	 */
	public static void CompressGZip( File file , String dstPath ) throws Exception {
		//読込元ファイルの実在チェック
		if( true != file.exists() ) {
			throw new FileIoArchiveException( SmMessageId.SM004054F , file.getPath() ) ;
		}
		if( true != file.isFile() ) {
			throw new FileIoArchiveException( SmMessageId.SM004055F , file.getPath() ) ;
		}
		if( true != file.canRead() ) {
			throw new FileIoArchiveException( SmMessageId.SM004056F , file.getPath() ) ;
		}

		BufferedInputStream bis = null ;
		GZIPOutputStream gzos = null ;
		try {
			bis = new BufferedInputStream( new FileInputStream( file ) ) ;
			gzos = new GZIPOutputStream( new BufferedOutputStream( new FileOutputStream( dstPath ) ) ) ;
			while( true ) {
				int iData = bis.read() ;
				if( EOF == iData ) {
					break ;
				}
				gzos.write( (byte)iData ) ;
			}
		} catch( Exception e ) {
			throw e ;
		} finally {
			TriFileUtils.close(gzos);
			TriFileUtils.close(bis);
		}
	}

	/**
	 * GZip形式のファイルを展開する<br>
	 * @param srcPath 展開するファイルの入力元フルパス
	 * @param dstPath 展開済みファイルの出力先フルパス
	 * @throws Exception
	 */
	public static void UnCompressGZip( String srcPath , String dstPath ) throws Exception {
//		読込元Zipファイルの実在チェック
		if( null == srcPath ) {
			throw new FileIoArchiveException( SmMessageId.SM004053F , srcPath) ;
		}
		File file = new File( srcPath ) ;
		UnCompressGZip( file , dstPath ) ;
	}

	/**
	 * GZip形式のファイルを展開する<br>
	 * @param file 展開するファイルの入力元Fileハンドラ
	 * @param dstPath 展開済みファイルの出力先フルパス
	 * @throws Exception
	 */
	public static void UnCompressGZip( File file , String dstPath ) throws Exception {

//		読込元ファイルの実在チェック
		if( true != file.exists() ) {
			throw new FileIoArchiveException( SmMessageId.SM004054F , file.getPath() ) ;
		}
		if( true != file.isFile() ) {
			throw new FileIoArchiveException( SmMessageId.SM004055F , file.getPath() ) ;
		}
		if( true != file.canRead() ) {
			throw new FileIoArchiveException( SmMessageId.SM004056F , file.getPath() ) ;
		}
		//出力先ファイルのチェック
		if( null == dstPath ) {
			throw new FileIoArchiveException( SmMessageId.SM004072F , dstPath) ;
		}
		File fileOut = new File( dstPath ) ;
		if( true == fileOut.exists() ) {
			if( true == fileOut.isDirectory() ) {
				throw new FileIoArchiveException( SmMessageId.SM004057F , dstPath) ;
			}
		}

		BufferedOutputStream bos = null ;
		GZIPInputStream gzis = null ;
		try {
			bos = new BufferedOutputStream( new FileOutputStream( fileOut ) ) ;
			gzis = new GZIPInputStream( new BufferedInputStream( new FileInputStream( file ) ) ) ;
			while( true ) {
				int iData = gzis.read() ;
				if( EOF == iData ) {
					break ;
				}
				bos.write( (byte)iData ) ;
			}
		} catch( Exception e ) {
			fileOut.delete() ;
			throw new FileIoArchiveException( SmMessageId.SM005078S , e ) ;
		} finally {
			TriFileUtils.close(gzis);
			TriFileUtils.close(bos);
		}
	}
}
