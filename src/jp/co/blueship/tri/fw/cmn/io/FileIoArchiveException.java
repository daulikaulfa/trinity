package jp.co.blueship.tri.fw.cmn.io;

import jp.co.blueship.tri.fw.ex.TriException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * ファイルの「圧縮・展開」関連操作クラスに関する例外クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class FileIoArchiveException extends TriException {

	/**
	 *
	 */
	private static final long serialVersionUID = -5620515086637128747L;

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 */
	public FileIoArchiveException(IMessageId message) {
		super(message);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param messageArgs message parameter
	 */
	public FileIoArchiveException(IMessageId message, String... messageArgs) {
		super(message, messageArgs);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param cause the root cause
	 */
	public FileIoArchiveException(IMessageId message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * Constructor for Exception <br>
	 *
	 * @param message messageID
	 * @param messageArgs message parameter
	 * @param cause the root cause
	 */
	public FileIoArchiveException(IMessageId message, Throwable cause, String... messageArgs) {
		super(message, messageArgs, cause);
	}

}
