package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Ear;

import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * Ear作成を行うクラス
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class EarCreator extends ArchiveCreator {

	/**
	 * Earファイルを作成する
	 * @param earFile 作成するEarファイル
	 * @param baseDir Ear化対象ファイルのベース(ルート)ディレクトリ
	 * @param srcList Ear化対象のjar、warファイル名のリスト
	 * @param appXmlFile 取り込むapplication.xmlファイル
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するEarファイルが既存の場合の動作
	 */
	public void execute(
			File earFile , File baseDir, String[] srcList ,
			File appXmlFile , File manifestFile, Duplicate dup ) throws TriRuntimeException {

		try {

			checkArgument( srcList , earFile , appXmlFile, manifestFile, dup );
			if ( !super.prepare( earFile, dup ) ) {
				return;
			}


			Project project = super.getProject( earFile );
			Ear ear			= getEar( earFile, project, appXmlFile, manifestFile, dup );

			super.setFileSet( baseDir, srcList, project, ear );

			ear.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005072S, e , earFile.getAbsolutePath());
		}

	}

	/**
	 * Earファイルを作成する
	 * @param earFile 作成するEarファイル
	 * @param earFileSetList Ear化対象のファイルリスト
	 * @param appXmlFile 取り込むapplication.xmlファイル
	 * @param manifestFile 取り込むマニフェストファイル
	 * @param dup 作成するEarファイルが既存の場合の動作
	 */
	public void execute(
			File earFile , FileSetParamBean[] earFileSetList ,
			File appXmlFile , File manifestFile, Duplicate dup ) throws TriRuntimeException {

		try {

			checkArgument( earFileSetList , earFile , appXmlFile, manifestFile, dup );
			if ( !super.prepare( earFile, dup ) ) {
				return;
			}


			Project project = super.getProject( earFile );
			Ear ear			= getEar( earFile, project, appXmlFile, manifestFile, dup );

			// FileSet
			super.setFileSet( earFileSetList, project, ear );

			ear.execute();

		} catch ( Exception e ) {
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005072S, e , earFile.getAbsolutePath() );
		}

	}

	/**
	 * Earクラスのオブジェクトを作成する
	 */
	private Ear getEar(
			File earFile, Project project, File appXmlFile, File manifestFile, Duplicate dup ) {

		Ear ear = new Ear();
		super.setInfo( ear, earFile, project, manifestFile, dup );

		if ( null != appXmlFile ) {
			ear.setAppxml( appXmlFile );
		}

		return ear;
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			FileSetParamBean[] earFileSetList , File earFile ,
			File appXmlFile, File manifestFile, Duplicate dup ) throws TriRuntimeException {

		// Ear化対象ファイルのチェック
		if ( null == earFileSetList ) {
			throw new TriSystemException( SmMessageId.SM004047F );
		}

		checkArgument( earFile, appXmlFile, manifestFile, dup );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			String[] srcList , File earFile , File appXmlFile, File manifestFile, Duplicate dup )
		throws TriRuntimeException {

		// Ear化対象ファイルのチェック
		if ( null == srcList ) {
			throw new TriSystemException( SmMessageId.SM004048F );
		}

		checkArgument( earFile, appXmlFile, manifestFile, dup );
	}

	/**
	 * 引数のチェックを行う。
	 * @throws TriRuntimeException
	 */
	private void checkArgument(
			File earFile , File appXmlFile, File manifestFile, Duplicate dup ) throws TriRuntimeException {

		// Earファイルのチェック
		if ( null == earFile ) {
			throw new TriSystemException( SmMessageId.SM004049F );
		}
		if ( earFile.isDirectory() ) {
			throw new TriSystemException( SmMessageId.SM004050F , earFile.getAbsolutePath() );
		}

		// application.xmlファイルのチェック
		if ( null == appXmlFile && !Duplicate.UPDATE.equals( dup ) ) {
			throw new TriSystemException( SmMessageId.SM005073S );
		}
		if ( null != appXmlFile ) {

			if( !appXmlFile.exists() || !appXmlFile.isFile() ) {
				throw new TriSystemException( SmMessageId.SM005074S , appXmlFile.getAbsolutePath() );
			}
		}

		// マニフェストファイルのチェック
		if ( null != manifestFile ) {

			if( !manifestFile.exists() || !manifestFile.isFile() ) {
				throw new TriSystemException( SmMessageId.SM005075S , manifestFile.getAbsolutePath() );
			}
		}
	}
}
