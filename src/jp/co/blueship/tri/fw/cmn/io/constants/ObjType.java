package jp.co.blueship.tri.fw.cmn.io.constants;


/**
 * オブジェクトの定義を列挙する<br>
 * 適宜追加のこと。
 * <br> 
 * <p>
 * </p>
 * 
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public enum ObjType {
	
	TYPE_UNKNOWN			( 0 ) ,
	TYPE_FILE				( 1 ) ,
	TYPE_DIRECTORY			( 2 ) ,
	TYPE_SYMBOLIC_LINK		( 3 ) ;

	private Integer value = 0 ;
	
	private ObjType( int value ) {
		this.value = value ;
	}
	
	public int value() {
		return this.value ;
	}
	public int intValue() {
		return this.value ;
	}
	
	public String getValue() {
		return value.toString();
	}
	
	public static ObjType getValue( int value ) {
		for ( ObjType define : values() ) {
			if ( define.intValue() == value )
				return define;
		}
		
		return null;
	}
}
