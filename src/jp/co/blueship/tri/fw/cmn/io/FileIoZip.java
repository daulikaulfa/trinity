package jp.co.blueship.tri.fw.cmn.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Enumeration;
import java.util.List;
import java.util.Set;

import org.apache.tools.zip.ZipEntry;
import org.apache.tools.zip.ZipFile;
import org.apache.tools.zip.ZipOutputStream;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ファイルの「【ZIP形式】圧縮・展開」関連操作クラスに関する実装クラスです。<br>
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship 2008
 */
public class FileIoZip {

	private static final ILog log = TriLogFactory.getInstance();
	private static final int EOF = -1;

	/**
	 * Zip形式に圧縮を行う<br>
	 * ファイルおよびディレクトリは複数指定可。<BR>
	 *
	 * @param lst ファイルまたはディレクトリのフルパスを格納したList
	 * @param dstFilePath 圧縮済みファイルの出力先フルパス
	 * @param encoding ファイル名の文字エンコード【任意：Null可】
	 * @param doSkip 読込元のファイルが存在しなかった場合の処理モード
	 *
	 *            <pre>
	 * 		true	:	当該ファイルを無視してZip作成を継続する
	 * 		false	:	エラーとしてZip作成を中断
	 * </pre>
	 * @throws Exception
	 */
	public static void CompressZip(List<String> lst, String dstPath, Charset encoding, boolean doSkip) throws Exception {

		CompressZip(lst, dstPath, encoding, doSkip, null);
	}

	/**
	 * Zip形式に圧縮を行う<br>
	 * ファイルおよびディレクトリは複数指定可。<BR>
	 *
	 * @param lst ファイルまたはディレクトリのフルパスを格納したList
	 * @param dstFilePath 圧縮済みファイルの出力先フルパス
	 * @param encoding ファイル名の文字エンコード【任意：Null可】
	 * @param doSkip 読込元のファイルが存在しなかった場合の処理モード
	 *
	 *            <pre>
	 * 		true	:	当該ファイルを無視してZip作成を継続する
	 * 		false	:	エラーとしてZip作成を中断
	 * </pre>
	 * @param zipFileSet 圧縮したファイルのパス
	 * @throws Exception
	 */
	public static void CompressZip(List<String> lst, String dstPath, Charset encoding, boolean doSkip, Set<String> zipFileSet) throws Exception {

		if (null == lst || 0 == lst.size()) {
			throw new FileIoArchiveException(SmMessageId.SM004058F);
		}
		if (null == dstPath) {
			throw new FileIoArchiveException(SmMessageId.SM004059F , dstPath );
		}

		ZipOutputStream zos = null;
		File fileZip = null;
		try {
			// 出力先フォルダの実在チェック
			fileZip = new File(dstPath);

			// ZipOutputStreamを生成
			zos = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(fileZip)));
			if (null != encoding) {// ファイル名のエンコードを明示的にセット
				zos.setEncoding(encoding.toString());
			}

			int skipCount = 0;
			for (String basePath : lst) {
				log.debug("圧縮対象のパス ：" + basePath);

				File file = new File(basePath);
				if (true == file.exists()) {
					CompressZipReentrant(zos, basePath, file.getName(), zipFileSet);
				} else {
					if (true == doSkip) {
						skipCount++;
					} else {
						throw new FileIoArchiveException(SmMessageId.SM004054F , basePath );
					}
				}
			}
			if (lst.size() == skipCount) {// 全てのファイルがスキップされ、Zipに何も含まれなかった
				fileZip.delete();
			}
		} catch (Exception e) {
			if (fileZip != null) {
				fileZip.delete();
			}
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new FileIoArchiveException(SmMessageId.SM005082S, e);
		} finally {
			if (null != zos) {
				zos.closeEntry();
				zos.close();
			}
		}
	}

	/**
	 * Zip圧縮サブ。再帰的にディレクトリ階層を探索して圧縮指示を行う<br>
	 *
	 * @param zos Zip出力用ストリーム
	 * @param basePath 圧縮指定された基底パス
	 * @param curPath 再帰的に動作する際のカレントパス
	 * @param zipFileSet 圧縮したファイルのパス
	 * @throws Exception
	 */
	private static void CompressZipReentrant(ZipOutputStream zos, String basePath, String curPath, Set<String> zipFileSet) throws Exception {

		File baseFile = new File(basePath);
		File file = new File(baseFile.getParent() + File.separator + curPath);
		if (true != file.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004054F , curPath );
		}
		log.debug("【chk】" + curPath + " " + basePath);
		if (file.isDirectory()) {// ディレクトリ
			log.debug("ディレクトリ： " + curPath);
			if (null != zipFileSet) {
				zipFileSet.add(curPath);
			}

			CompressZipProc(zos, file, curPath);// 空ディレクトリを作成
			// ディレクトリの中身を再帰的に捜索
			String[] list = file.list();
			for (String listData : list) {
				String path = TriStringUtils.linkPathBySlash(curPath, listData);
				CompressZipReentrant(zos, basePath, path, zipFileSet);
			}
		} else {// ファイル
			log.debug("ファイル ： " + curPath + " " + basePath);
			if (null != zipFileSet) {
				zipFileSet.add(curPath);
			}

			CompressZipProc(zos, file, curPath);
		}
	}

	/**
	 * Zip圧縮サブ。実際に圧縮処理を行う<br>
	 *
	 * @param zos Zip出力用ストリーム
	 * @param file 再帰的に動作する際のFileハンドラ
	 * @param pathOffset 再帰的に動作する際の相対パス
	 * @throws Exception
	 */
	private static void CompressZipProc(ZipOutputStream zos, File file, String pathOffset) throws Exception {

		BufferedInputStream bis = null;

		try {
			String entry = null;
			if (file.isDirectory()) {// ディレクトリは空ディレクトリをZipEntryに登録
				entry = pathOffset + FilePathDelimiter.DELIMITER;
				// ZipEntryを生成
				ZipEntry ze = new ZipEntry(entry);
				ze.setTime(file.lastModified());
				zos.putNextEntry(ze);
				log.debug("空ディレクトリを作成した： " + entry);
			} else {// ファイルはそのまま登録
				entry = pathOffset;
				long fileSize = (int) file.length();
				bis = new BufferedInputStream(new FileInputStream(file));
				// ZipEntryを生成
				ZipEntry ze = new ZipEntry(entry);
				ze.setSize(fileSize);
				ze.setTime(file.lastModified());// タイムスタンプの保存
				zos.putNextEntry(ze);

				if (0 == fileSize) {// サイズ０のファイルを圧縮しようとした場合
					zos.write(new byte[0], 0, 0);
				} else {// 通常のファイル
					int iData;
					while (true) {
						if (EOF == (iData = bis.read())) {
							break;
						}
						zos.write((byte) iData);
					}
				}
			}
		} catch (Exception e) {
			throw new FileIoArchiveException(SmMessageId.SM005083S, e);
		} finally {
			TriFileUtils.close(bis);
		}
	}

	/**
	 * Zip形式のファイルを展開する<br>
	 *
	 * @param srcPath 圧縮済みZipファイルのフルパス
	 * @param dstPath 展開先ディレクトリのフルパス
	 * @param encoding エンコーディング
	 * @throws Exception
	 */
	public static void UnCompressZip(String srcPath, String dstPath, Charset encoding) throws Exception {

		// XXX 無限再帰呼出し。t-kubo
		UnCompressZip(srcPath, dstPath, encoding);
	}

	/**
	 * // * Zip形式のファイルを展開する<br>
	 *
	 * @param srcPath 圧縮済みZipファイルのフルパス
	 * @param dstPath 展開先ディレクトリのフルパス
	 * @param encoding エンコーディング
	 * @param unzipFileSet 展開したファイルのパス
	 * @throws Exception
	 */
	public static void UnCompressZip(String srcPath, String dstPath, Charset encoding, Set<String> unzipFileSet) throws Exception {

		// 読込元Zipファイルの実在チェック
		if (null == srcPath) {
			throw new FileIoArchiveException(SmMessageId.SM004068F , srcPath );
		}
		File fileZip = new File(srcPath);
		if (true != fileZip.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004069F , srcPath );
		}
		if (true != fileZip.isFile()) {
			throw new FileIoArchiveException(SmMessageId.SM004070F , srcPath );
		}
		if (true != fileZip.canRead()) {
			throw new FileIoArchiveException(SmMessageId.SM004071F , srcPath );
		}
		// 出力先パスの実在チェック
		if (null == dstPath) {
			throw new FileIoArchiveException(SmMessageId.SM004072F , dstPath );
		}
		File fileOut = new File(dstPath);
		if (true != fileOut.exists()) {
			throw new FileIoArchiveException(SmMessageId.SM004066F , dstPath );
		}
		if (true != fileOut.isDirectory()) {
			throw new FileIoArchiveException(SmMessageId.SM004067F , dstPath );
		}
		// 展開ロジックメイン
		ZipFile zipFile = null;
		if (null != encoding) {// 明示的に文字コードをセット
			zipFile = new ZipFile(srcPath, encoding.toString());
		} else {
			zipFile = new ZipFile(srcPath);
		}
		Enumeration<?> entries = zipFile.getEntries();

		while (entries.hasMoreElements()) {
			ZipEntry entry = (ZipEntry) entries.nextElement();
			if (null == entry) {
				break;
			}

			File file = new File(dstPath + File.separator + entry.getName());
			if (entry.isDirectory()) {
				log.debug(" ディレクトリ : " + entry.getName());
				if (null != unzipFileSet) {
					unzipFileSet.add(entry.getName());
				}

				file.mkdirs();// ディレクトリ作成
			} else {
				BufferedInputStream bis = null;
				BufferedOutputStream bos = null;
				try {
					// 先に親ディレクトリを作成（ディレクトリがないとファイル作成に失敗するので）
					File parentDir = new File(file.getParent());
					parentDir.mkdirs();

					log.debug(" ファイル : " + entry.getName());
					if (null != unzipFileSet) {
						unzipFileSet.add(entry.getName());
					}

					// ファイルデータの読み込み
					bos = new BufferedOutputStream(new FileOutputStream(file));
					bis = new BufferedInputStream(zipFile.getInputStream(entry));
					int iData;
					while (true) {
						if (EOF == (iData = bis.read())) {
							break;
						}
						bos.write((byte) iData);
					}
				} catch (Exception e) {
					throw new FileIoArchiveException(SmMessageId.SM005084S, e);
				} finally {
					TriFileUtils.close(bis);
					TriFileUtils.closeOfOutputStream(bos);
				}
			}
			// タイムスタンプの書き戻し
			file.setLastModified(entry.getTime());
		}
		// zipFileのクローズ
		TriFileUtils.close( zipFile );
	}
}
