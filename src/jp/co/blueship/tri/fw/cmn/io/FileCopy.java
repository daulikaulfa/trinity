package jp.co.blueship.tri.fw.cmn.io;

import java.io.File;
import java.io.IOException;
import java.util.List;

/**
 * fileのcopy処理を抽象化したinterface.
 *
 */
public interface FileCopy {

	/**
	 * Fileをcopyする。copyが行なわれた場合に、trueを戻り値とする。
	 * <p>
	 * dstが既に存在する場合の挙動(copyするかしないか)は、subclass側の実装による。
	 * 但し、重複していた場合には、引数のduplicateListにdstを追加する。
	 * </p>
	 *
	 * @param src copy元file.
	 * @param dst copy先file.
	 * @param duplicateList 重複していたfileのリスト(要instance)
	 * @return copyを行なった場合はtrue。
	 * @throws IOException
	 */
	public boolean copyFileToFile(File src, File dst, List<File> duplicateList) throws IOException;

}
