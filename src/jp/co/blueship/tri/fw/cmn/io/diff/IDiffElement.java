package jp.co.blueship.tri.fw.cmn.io.diff;

import java.io.Serializable;

import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;

/**
 * Diffの結果を格納することを示すインターフェースです。
 *
 */
public interface IDiffElement extends Serializable {

	

	/**
	 * 変更の状態を取得します。
	 */
	public Status getStatus();
	/**
	 * 変更の状態を設定します。
	 */
	public void setStatus( Status status );

	/**
	 * 比較元の行数を取得します。
	 */
	public long getSrcLineNumber();
	/**
	 * 比較元の行数を設定します。
	 */
	public void setSrcLineNumber( long srcLineNum );

	/**
	 * 比較元の内容を取得します。
	 */
	public String getSrcContents();
	/**
	 * 比較元の内容を設定します。
	 */
	public void setSrcContents( String srcContents );

	/**
	 * 比較先の行数を取得します。
	 */
	public long getDstLineNumber();
	/**
	 * 比較先の行数を設定します。
	 */
	public void setDstLineNumber( long dstLineNum );

	/**
	 * 比較先の内容を取得します。
	 */
	public String getDstContents();
	/**
	 * 比較先の内容を設定します。
	 */
	public void setDstContents( String dstContents );
}
