package jp.co.blueship.tri.fw.cmn.utils.spec;

import java.util.List;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * 制約に対するチェックを行うクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class Constraint {

	/**
	 * 指定した値が制約違反かどうかチェックし、違反している場合は例外を送出します。
	 * TriSystemExceptionの仕様が変更(newをするとlogに書かれてしまう)になったため、ここでTriSystemExceptionをnewします。
	 *
	 * @param input 制約をチェックする値
	 * @param spec 制約を表現する仕様オブジェクト
	 * @param e 制約違反の場合に送出する例外
	 */
	@SuppressWarnings("rawtypes")
	public static final <T extends List> void validate(T input, Specification<T> spec, IMessageId message, String srvId, String serviceId) {
		if (!spec.isSpecified(input)) {
			throw new TriSystemException(message, srvId, serviceId);
		}

	}

}
