package jp.co.blueship.tri.fw.cmn.utils.validator;

import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

/**
 * BeanValidatorのインスタンスを生成するクラスです。
 *
 *
 * @author Takayuki Kubo
 *
 */
public class TriValidatorFactory {

	public static final String BEAN_NAME = "triValidatorFactory";

	private ValidatorFactory factory;

	/**
	 * 初期化処理。javax.validator.ValidatorFactoryのインスタンスを生成します。<br/>
	 * ValidatorFactoryから生成されるValidatorはSingletonパターンで実装されています。<br/>
	 * したがって、ValidatorFactoryも毎回生成するのではなく、Singletonパターンで使用されることが推奨されます。
	 */
	public void init() {
		factory = Validation.buildDefaultValidatorFactory();
	}

	/**
	 * Validatorインスタンスを生成します。
	 *
	 * @return Validator
	 */
	public Validator createValidator() {

		return factory.getValidator();
	}

}
