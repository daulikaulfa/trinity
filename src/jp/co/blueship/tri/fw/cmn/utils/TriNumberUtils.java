package jp.co.blueship.tri.fw.cmn.utils;

public class TriNumberUtils {

	public static long defaultIfMinus(long value, long defaultValue) {

		if (value < 0) {
			return defaultValue;
		}

		return value;
	}

}
