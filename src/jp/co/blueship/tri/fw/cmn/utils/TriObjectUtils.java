package jp.co.blueship.tri.fw.cmn.utils;

public class TriObjectUtils {

	/**
	 * 指定された値がNULLかどうか判断し、NULL以外の場合は指定された値をそのまま返します。<br/>
	 * NULLである場合は、指定されたデフォルト値を返します。
	 *
	 * @param t NULLかどうか検査する値
	 * @param defaultValue tがNULLである場合に返すデフォルト値
	 * @return tがNULL以外の場合はtを返す。NULLである場合はdefaultValueを返す。
	 */
	public static <T> T defaultIfNull(T t, T defaultValue) {

		if (t == null) {
			return defaultValue;
		}

		return t;
	}

}
