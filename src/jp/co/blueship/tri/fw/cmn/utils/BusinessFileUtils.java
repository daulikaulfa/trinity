package jp.co.blueship.tri.fw.cmn.utils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.TreeSet;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.io.Copy;
import jp.co.blueship.tri.fw.cmn.io.FileCopyOverride;
import jp.co.blueship.tri.fw.cmn.io.ICopy;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * ファイルの入出力の共通処理
 *
 * @version V4.00.00
 * @author Satoshi Sasaki
 */
public class BusinessFileUtils {

	/**
	 * ＳＣＭ固有のファイルを除外するフィルターを取得します。
	 *
	 * @return 生成したフィルターを戻します。
	 */
	public static FileFilter getFileFilter() {

		return new FileFilter() {
			public boolean accept(File pathname) {
				if ( TriFileUtils.isSCM( pathname ) )
					return false;

				return true;
			}
		};
	}

	/**
	 * 指定したパス配下のファイル・ディレクトリを取得する。
	 *
	 * @param path
	 * @return
	 */
	public static final List<File> getFiles(File path) {
		return getFiles( path, getFileFilter() );
	}

	/**
	 * 指定したパス配下のファイル・ディレクトリを取得する。
	 *
	 * @param path
	 * @param filter
	 * @return
	 */
	public static final List<File> getFiles(File path, FileFilter filter) {

		if ( ! path.isDirectory() ){
			throw new TriSystemException( SmMessageId.SM004160F , path.getPath());
		}
		try {
			List<File> fileList;
			fileList = TriFileUtils.getFiles(path, TriFileUtils.TYPE_ALL, (null == filter)? getFileFilter(): filter );

			File[] subDirs = fileList.toArray( new File[fileList.size()] );

			sortDefaultFileList(subDirs);

			return FluentList.from(subDirs).asList();

		} catch (IOException e) {
			throw new TriSystemException( SmMessageId.SM005148S, e);
		}
	}

	/**
	 * 指定したパス直下のみのファイル・ディレクトリを取得する。
	 *
	 * @param path
	 * @return
	 */
	public static final List<File> getListFiles(File path) {
		return getListFiles( path, getFileFilter() );
	}

	/**
	 * 指定したパス直下のみのファイル・ディレクトリを取得する。
	 * <br>
	 *
	 * @param masterPath
	 * @param filter
	 * @return
	 */
	public static final List<File> getListFiles(File path, FileFilter filter ) {

		if ( ! path.isDirectory() ){
			throw new TriSystemException( SmMessageId.SM004160F , path.getPath());
		}
		File[] subDirs = path.listFiles( (null == filter)? getFileFilter(): filter );
		sortDefaultFileList(subDirs);

		return FluentList.from(subDirs).asList();
	}

	/**
	 *  ファイル一覧をソートする。
	 *  ディレクトリがトップに来るようにし、さらにアルファベット順で昇順とする。
	 */
	public static final void sortDefaultFileList(File[] sortFile){

		Comparator<File> comp = new Comparator<File>(){
			public int compare(File o1, File o2) {
				// 第1引数がディレクトリで且つ第2引数がファイルの場合は、ディレクトリがトップのため、-1を返す。
				if (o1.isDirectory() && o2.isFile()) {
					return -1;
					// 第2引数がディレクトリで且つ第1引数がファイルの場合は、ディレクトリがトップのため、1を返す。
				}	else if (o2.isDirectory() && o1.isFile()) {
					return 1;
				}
				return o1.getPath().compareToIgnoreCase(o2.getPath());
			}
		};

		Arrays.sort(sortFile, comp);
	}

	/**
	 * 添付ファイルを保存する。
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param no インシデント番号／変更管理番号
	 * @param is ファイルの内容のストリーム
	 * @param fileName 添付ファイル名
	 */
	public static void saveAppendFile( String saveDirPath, String no, InputStream is, String fileName  ) {
		if ( TriStringUtils.isEmpty(fileName) )
			return;

		BufferedInputStream inBuffer	= null;
		BufferedOutputStream outBuffer	= null;

		try {

			File saveDir = new File( saveDirPath, no );
			if ( !saveDir.exists() ) {
				saveDir.mkdirs();
			}

			if ( null == is )
				return;

			inBuffer = new BufferedInputStream( is );

			FileOutputStream fos = new FileOutputStream( new File( saveDir, fileName ) );
			outBuffer = new BufferedOutputStream( fos );

			int contents = 0;
			while (( contents = inBuffer.read() ) != -1 ) {
				outBuffer.write( contents );
			}
		} catch ( IOException ioe ) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe );
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}
	}


	/**
	 * 添付ファイルを保存する。
	 * 半永久保存とｎ添付ファイルに対応します。
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param no インシデント番号／変更管理番号／資産申請番号
	 * @param uniqueKey 一意となるキー
	 * @param id 添付ファイルID
	 * @param is ファイルの内容のストリーム
	 * @param fileName 添付ファイル名
	 */
	public static void saveAppendFile( String saveDirPath, String no, String uniqueKey, String id, InputStream is, String fileName  ) {
		if ( TriStringUtils.isEmpty(fileName) )
			return;

		BufferedInputStream inBuffer	= null;
		BufferedOutputStream outBuffer	= null;

		try {

			File noDir = new File( saveDirPath, no );
			File uDir = new File( noDir.getAbsolutePath(), uniqueKey );
			File saveDir = new File( uDir, id );
			if ( !saveDir.exists() ) {
				saveDir.mkdirs();
			}

			if ( null == is )
				return;

			inBuffer = new BufferedInputStream( is );

			FileOutputStream fos = new FileOutputStream( new File( saveDir, fileName ) );
			outBuffer = new BufferedOutputStream( fos );

			int contents = 0;
			while (( contents = inBuffer.read() ) != -1 ) {
				outBuffer.write( contents );
			}
		} catch ( IOException ioe ) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe );
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}
	}


	/**
	 * 添付ファイル保存先URLを作成する。
	 * @param saveDirURL 保存先ディレクトリのURL
	 * @param no インシデント番号／変更管理番号
	 * @param fileName 添付ファイル名
	 */
	public static String getAppendFileURL( String saveDirURL, String no, String fileName  ) {

		if ( TriStringUtils.isEmpty( no ) )
			return "";
		if ( TriStringUtils.isEmpty( fileName ) )
			return "";

		if ( !saveDirURL.endsWith( "/") ) {
			saveDirURL = saveDirURL + "/";
		}


		return saveDirURL + no + "/" + fileName;

	}

	/**
	 * 添付ファイル保存先URLを作成する。
	 * @param saveDirURL 保存先ディレクトリのURL
	 * @param no インシデント番号／変更管理番号／資産申請番号
	 */
	public static String getAppendFileURL( String saveDirURL, String no, String uniqueKey, String id, String fileName  ) {

		if ( TriStringUtils.isEmpty( no ) )
			return "";
		if ( TriStringUtils.isEmpty( id ) )
			return "";
		if ( TriStringUtils.isEmpty( fileName ) )
			return "";

		if ( !saveDirURL.endsWith( "/") ) {
			saveDirURL = saveDirURL + "/";
		}

		return saveDirURL + no + "/" + id + "/" + fileName;

	}

	/**
	 * 添付ファイルの保存先から最大値のユニークキーを取得する。
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param no インシデント番号／変更管理番号／資産申請番号
	 * @return 格納先にディレクトリが存在しない場合（格納先を移動したなど）<strong>空文字</strong>
	 */
	public static String getLastUniqueKey( String saveDirPath, String no  ) {

		File saveDir = new File( saveDirPath, no );
		if ( !saveDir.exists() ) {
			saveDir.mkdirs();
		}

		File[] dirs = saveDir.listFiles( new FileFilter() {

			public boolean accept(File pathname) {
				return pathname.isDirectory();
			}
		} );

		TreeSet<String> dirSet = new TreeSet<String>();
		for (File dir : dirs) {

			dirSet.add(dir.getName());
		}

		if(dirSet.isEmpty()) {
			return "";
		}

		return dirSet.last();
	}

	/**
	 * 添付ファイルを移動する。
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param oldNo インシデント番号／変更管理番号(仮採番)
	 * @param newNo インシデント番号／変更管理番号
	 */
	public static void moveAppendFile( String saveDirPath, String oldNo, String newNo  ) {

		try {

			File oldDir = new File( saveDirPath, oldNo );
			if ( !oldDir.exists() ) {
				return;
			}

			File newDir = new File( saveDirPath, newNo );
			if ( !newDir.exists() ) {
				newDir.mkdirs();
			} else {
				TriFileUtils.delete( newDir );
				newDir.mkdirs();
			}

			ICopy copy = new Copy();
			FileCopyOverride fileCopy = new FileCopyOverride() ;
			copy.setFileCopy( fileCopy ) ;

			copy.copy(oldDir, newDir, false);

			TriFileUtils.delete( oldDir );

		} catch ( IOException ioe ) {
			throw new TriSystemException(SmMessageId.SM005151S, ioe );
		}
	}

	/**
	 * 添付ファイルを保存する。
	 * 半永久保存とｎ添付ファイルに対応します。
	 * @param saveDirPath 保存先ディレクトリのパス
	 * @param no インシデント番号／変更管理番号／資産申請番号
	 * @param uniqueKeyLast 一意となるキー
	 * @param uniqueKeyCurrent 一意となるキー
	 * @param id 添付ファイルID
	 * @param fileName 添付ファイル名
	 */
	public static void copyAppendFile( String saveDirPath, String no, String uniqueKeyLast, String uniqueKeyCurrent, String id, String fileName  ) {
		if ( TriStringUtils.isEmpty(fileName) )
			return;

		try {

			File noDir = new File( saveDirPath, no );
			File uLstDir = new File( noDir.getAbsolutePath(), uniqueKeyLast );
			File uCurDir = new File( noDir.getAbsolutePath(), uniqueKeyCurrent );
			File srcDir = new File( uLstDir, id );
			File saveDir = new File( uCurDir, id );
			File srcFile = new File( srcDir, fileName );
			File saveFile = new File( saveDir, fileName );

			if ( !srcFile.exists() ) {
				return;
			}
			if ( !saveDir.exists() ) {
				saveDir.mkdirs();
			}

			ICopy copy = new Copy();
			FileCopyOverride fileCopy = new FileCopyOverride() ;
			copy.setFileCopy( fileCopy ) ;

			copy.copy(srcFile, saveFile, false);

		} catch ( IOException ioe ) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe );
		} finally {
		}
	}

	/**
	 * 指定されたパスがディレクトリ作成可能かどうかを判定します。
	 * <br>
	 * <br>このメソッドが呼び出されると、指定されたパスでディレクトリが実際に作成されます。
	 * 作成されたディレクトリは最下層のフォルダのみ自動的に削除されますが、
	 * それ以外に作成された上位のフォルダは、そのまま残されます。
	 *
	 * @return 必要なすべての親ディレクトリを含めてディレクトリが生成された場合は true、そうでない場合は false。既に存在する場合はtrue。
	 */
	public final static boolean isMkdirs( File file ) {

		boolean isMkdirs = false;

		try {
			if ( null == file )
				return false;

			if ( file.isFile() )
				return false;

			if ( file.isDirectory() )
				return true;

			file.getCanonicalPath() ;

			isMkdirs = file.mkdirs() ;
			if( true == file.isDirectory() ) {
				file.delete() ;//ディレクトリが空の場合のみ消す
			} else {
				throw new IOException() ;
			}

		} catch( IOException e ) {
			isMkdirs = false;

			//ディレクトリ作成に失敗した場合は、falseを戻す。
			e.printStackTrace();
		}

		return isMkdirs;
	}
	/**
	 * プラットフォームがWindowsの場合に、引数で渡されたドライブレターが小文字であったとき、<br>
	 * ドライブレターを大文字に変換してパス全体を返す。<br>
	 * プラットフォームが非Windowsの場合は、与えられたパスをそのまま返す。<br>
	 * また、パス区切り文字が"\"の場合は、必ず"/"に置換する。<br>
	 * @param path パス文字列
	 * @return 変換後パス文字列
	 */
	public final static String toUpperPathDriveLetter( String path ) {
		final String SEPARATOR_BACKSLASH = "\\" ;
		final String SEPARATOR_SLASH = "/" ;
		final String SEPARATOR_DRIVE = ":" ;

		if( -1 != path.indexOf( SEPARATOR_BACKSLASH ) ) {
			path = StringUtils.replace( path , SEPARATOR_BACKSLASH , SEPARATOR_SLASH ) ;
		}
		if ( TriSystemUtils.isWindows() ) {

			int pos = path.indexOf( SEPARATOR_DRIVE ) ;
			if( -1 != pos && 0 < pos ) {
				String driveLetter = path.substring( 0 , pos ) ;
				String tempPath = path.substring( pos + 1 ) ;

				String retPath = driveLetter.toUpperCase() + SEPARATOR_DRIVE + tempPath ;
				return retPath ;
			} else {//:が抜けている
				return null ;
			}
		} else {
			return path ;
		}
	}

	/**
	 * 与えられたパス文字列を、上位階層から実パスと部分的に照合していき、完全一致しているか<br>
	 * 判定する。<br>
	 * チェックするのは、実パスが存在している範囲まで。実パスがない場所はチェックしない<br>
	 * また、実パスが存在している範囲で大文字/小文字の違いがあった場合には、実パスと同じに置換する。<br>
	 *
	 * @param path パス文字列
	 * @return チェック後置換文字列
	 */
	public static String chkPartialPathIsCanonical( String reqPath ) throws Exception {

		if ( TriSystemUtils.isWindows() ) {
			String path = TriStringUtils.convertPath( reqPath ) ;

			int driveLetterPos = path.indexOf( ":" ) ;
			if( 1 != driveLetterPos ) {//ドライブレターがない or 不正な箇所に存在
				return null ;
			}
			if( 2 < path.length() ) {
				char chkChar = path.charAt( driveLetterPos + 1 ) ;//ドライブレター直後の \ or / がない
				if( '\\' != chkChar && '/' != chkChar ) {
					return null ;
				}
			}
			if( -1 != path.indexOf( "//" ) ) {//パス区切り文字が複数個連続して存在
				return null ;
			}

			String[] pathArray = path.split( "/" ) ;

			StringBuilder stb = new StringBuilder() ;
			stb.append( pathArray[ 0 ] ) ;//[ドライブレター + : ]まで

			for( int i = 1 ; i < pathArray.length ; i++ ) {
				stb.append( "/" ) ;
				stb.append( pathArray[ i ] ) ;

				String partPath = stb.toString() ;

				File file = new File( partPath ) ;
				if( file.exists() ) {
					String canonicalPathPart = file.getCanonicalPath() ;
					String replacePath = TriStringUtils.convertPath( canonicalPathPart ) ;
					if( replacePath.equals( partPath ) ) {
						;
					} else {//実パスが存在した部分だけを実パスと同じ大文字/小文字の構成に置換
						stb = new StringBuilder( TriStringUtils.convertPath( canonicalPathPart ) ) ;
					}
				} else {//途中まで完全一致、以降は元のパス
					for( ++i ; i < pathArray.length ; i++ ) {
						stb.append( "/" ) ;
						stb.append( pathArray[ i ] ) ;
					}
					return stb.toString() ;
				}
			}
			return stb.toString() ;
		} else {
			return reqPath ;
		}
	}

}
