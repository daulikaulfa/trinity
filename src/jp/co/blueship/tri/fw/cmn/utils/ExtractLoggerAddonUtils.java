package jp.co.blueship.tri.fw.cmn.utils;

import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicates.*;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.log.LogBusinessFlow;

/**
 * 業務ログを扱うユーティリティークラスです。
 * <br>業務サービス内でキャッシュしているログを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ExtractLoggerAddonUtils {

	/**
	 * 指定されたリストから業務フロー ログ処理クラスを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static LogBusinessFlow extractLogBusinessFlow( List<Object> list ) {
		return (LogBusinessFlow)FluentList.from(list).atFirstOf(isInstanceOf(LogBusinessFlow.class));
	}
}
