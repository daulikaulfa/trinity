package jp.co.blueship.tri.fw.cmn.utils.monoid;

public class Monoids {

	private Monoids() {
	}

	public static final Monoid<Long> LONG_MONOID = new Monoid<Long>() {

		public Long empty() {
			return 0L;
		}

		public Long append(Long input1, Long input2) {
			return input1 + input2;
		}
	};

}
