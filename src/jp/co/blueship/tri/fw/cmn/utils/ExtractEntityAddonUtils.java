package jp.co.blueship.tri.fw.cmn.utils;

import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.ex.TriSystemException;

/**
 * キャッシュエンティティを扱うユーティリティークラスです。 <br>
 * キャッシュエンティティを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ExtractEntityAddonUtils {

	/**
	 * 指定されたリストからロット情報を取得します。
	 *
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static ILotEntity extractPjtLot(List<Object> list) {

		for (Object obj : list) {
			if (obj instanceof ILotEntity) {
				return (ILotEntity) obj;
			}
		}

		return null;
	}

	/**
	 * パラメータリストより業務ログの削除判定を取得します。
	 *
	 * @param paramList パラメータリスト
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException
	 */
	public static final StatusFlg getLogBusinessDeleteFlg(List<Object> paramList) throws TriSystemException {

		for (Object obj : paramList) {
			if (obj instanceof StatusFlg) {
				return ((StatusFlg) obj);
			}
		}

		return StatusFlg.off;
	}

}
