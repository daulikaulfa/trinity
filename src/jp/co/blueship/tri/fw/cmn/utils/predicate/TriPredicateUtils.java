package jp.co.blueship.tri.fw.cmn.utils.predicate;

public class TriPredicateUtils {

	public static <T> TriPredicate<T> not(final TriPredicate<T> predicate) {

		return new TriPredicate<T>() {

			public boolean evalute(T input) {
				return !predicate.evalute(input);
			}
		};
	}

	static <T> TriPredicate<T> or(final TriPredicate<T> predicate1, final TriPredicate<T> predicate2) {

		return new TriPredicate<T>() {

			public boolean evalute(T input) {
				return predicate1.evalute(input) || predicate2.evalute(input);
			}
		};
	}

	static <T> TriPredicate<T> and(final TriPredicate<T> predicate1, final TriPredicate<T> predicate2) {

		return new TriPredicate<T>() {

			public boolean evalute(T input) {
				return predicate1.evalute(input) && predicate2.evalute(input);
			}
		};
	}

}
