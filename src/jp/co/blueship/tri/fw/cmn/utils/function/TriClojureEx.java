package jp.co.blueship.tri.fw.cmn.utils.function;

/**
 * クロージャクラス。
 *
 * @author Takayuki Kubo
 *
 * @param I 入力値の型
 * @param EX クロージャ適用中に発生しうる例外の型
 */
public interface TriClojureEx<I, EX extends Exception> {

    /**
     * クロージャを適用する。
     *
     * @param input 入力オブジェクト
     * @return 処理結果。ループ処理中の場合、falseを返却すればループを止めることができる。
     */
    boolean apply(I input) throws EX ;

}
