package jp.co.blueship.tri.fw.cmn.utils.function;

public final class TriFunctions {

	/**
	 * 何も処理を行わなず、入力値をそのまま返す関数を生成して返します。
	 *
	 * @return 何も処理を行わない関数
	 */
	public static final <T> TriFunction<T, T> nopFunction() {
		return new TriFunction<T, T>() {

			@Override
			public T apply(T input) {
				return input;
			}
		};
	}

}
