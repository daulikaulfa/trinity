package jp.co.blueship.tri.fw.cmn.utils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;


/**
 * 定義リソースから「詳細検索」を行うためのユーティリティークラスです。
 *
 */
public class DesignDefineSearchUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * ソースとなる定義名から、任意の値のものを抽出し、
	 * 抽出した定義名と同一のキー値をターゲットとつき合わせて、
	 * 対応するターゲット定義名の設定値を抽出します。
	 *
	 * @param targetIdMap
	 * @param srcIdMap
	 * @param srcValue 任意の値。nullの場合、任意の値での判定を行わない。
	 * @return
	 */
	public static final List<String> extractValue( Map<String,String> targetIdMap, Map<String,String> srcIdMap, String srcValue ) {
		List<String> extractValueList = new ArrayList<String>();

		Set<String> srcKeySet = srcIdMap.keySet();

		for( String id : srcKeySet ) {
			if( true == targetIdMap.containsKey( id ) ) {
				if ( null != srcValue && !srcValue.equals( srcIdMap.get( id ) ) )
					continue;

				extractValueList.add( targetIdMap.get( id ) );
			}
		}

		return 	extractValueList;
	}

	/**
	 * ソースとなる定義名から、任意の値のものを抽出し、
	 * 該当する定義名のキー値を抽出します。
	 *
	 * @param srcIdMap
	 * @param srcValue 任意の値。nullの場合、任意の値での判定を行わない。
	 * @return
	 */
	public static final List<String> extractKey( Map<String,String> srcIdMap, String srcValue ) {
		List<String> extractKeyList = new ArrayList<String>();

		Set<String> keySet = srcIdMap.keySet();

		for( String id : keySet ) {
			if( true == srcIdMap.containsKey( id ) ) {
				if ( null != srcValue && !srcValue.equals( srcIdMap.get( id ) ) )
					continue;

				extractKeyList.add( id );
			}
		}

		return 	extractKeyList;
	}

	/**
	 * ソースとなる定義名から、キーと値を抽出し、別のビーンに再マッピングします。
	 *
	 * @param srcIdMap
	 * @return
	 */
	public static final List<ItemLabelsBean> mappingItemLabelsBean( Map<String,String> srcIdMap ) {
		List<ItemLabelsBean> mappingList = new ArrayList<ItemLabelsBean>();

		Set<String> keySet = srcIdMap.keySet();

		for( String id : keySet ) {
			ItemLabelsBean itemLabels = new ItemLabelsBean();

			itemLabels.setLabel( id );
			itemLabels.setValue( srcIdMap.get( id ) );

			mappingList.add( itemLabels );
		}

		return 	mappingList;
	}

	/**
	 * ソースとなる定義名から、キーと値を反転してマップに再設定を行います。
	 *
	 * @param srcIdMap
	 * @param srcValue 任意の値。nullの場合、任意の値での判定を行わない。
	 * @return
	 */
	public static final Map<String,String> reverseMap( Map<String,String> srcIdMap ) {
		Map<String,String> reverseMap = new LinkedHashMap<String,String>() ;

		Set<String> keySet = srcIdMap.keySet();

		for( String id : keySet ) {
			reverseMap.put( srcIdMap.get( id ), id );
		}

		return 	reverseMap;
	}

	/**
	 * ソースとなる定義名から、キーを抽出し、別のビーンに再マッピングします。
	 * <br>その際、キー値が数値に変換可能な文字列であることを前提として、
	 * キー値に数値変換した文字列、値に無変換のキー値を設定します。
	 *
	 * @param srcIdMap
	 * @return
	 */
	public static final List<ItemLabelsBean> mappingItemLabelsBeanByKeyParseInt( Map<String,String> srcIdMap ) {
		List<ItemLabelsBean> mappingList = new ArrayList<ItemLabelsBean>();

		for( ItemLabelsBean item : mappingItemLabelsBean( srcIdMap ) ) {
			ItemLabelsBean itemLabels = new ItemLabelsBean();

			itemLabels.setLabel( String.valueOf( Integer.parseInt( item.getLabel() ) ) );
			itemLabels.setValue( item.getLabel() );

			mappingList.add( itemLabels );
		}

		return 	mappingList;
	}

	/**
	 * ソースとなる定義名から、キーと値を抽出し、別のビーンに再マッピングします。
	 *
	 * @param beanId
	 * @return
	 */
	public static final List<ItemLabelsBean> mappingItemLabelsBean( IDesignBeanId beanId ) {
		Map<String,String> srcIdMap = sheet.getKeyMap( beanId );

		return 	mappingItemLabelsBean( srcIdMap );
	}

	/**
	 * ソースとなる定義名から、任意の値のものを抽出し、
	 * 該当する定義名のキー値を抽出します。
	 *
	 * @param beanId
	 * @param key 任意の値。nullの場合、任意の値での判定を行わない。
	 * @return
	 */
	public static final List<String> extractKey( IDesignBeanId beanId, String key ) {
		Map<String,String> srcIdMap = sheet.getKeyMap( beanId );

		return extractKey( srcIdMap, key );
	}

	/**
	 * ターゲットとなる定義名とソースの定義名同士をつき合わせて、
	 * ソース定義と同一のキー値を拾い出して、対応するターゲット定義名の設定値を抽出します。
	 *
	 * @param targetBeanId
	 * @param srcBeanId
	 * @return
	 */
	public static final List<String> extractValue( IDesignBeanId targetBeanId, IDesignBeanId srcBeanId ) {
		return 	extractValue( targetBeanId, srcBeanId, null );
	}
	/**
	 * ソースとなる定義名から、任意の値のものを抽出し、
	 * 抽出した定義名と同一のキー値をターゲットとつき合わせて、
	 * 対応するターゲット定義名の設定値を抽出します。
	 *
	 * @param targetBeanId
	 * @param srcBeanId
	 * @param key 任意の値。nullの場合、任意の値での判定を行わない。
	 * @return
	 */
	public static final List<String> extractValue( IDesignBeanId targetBeanId, IDesignBeanId srcBeanId, String key ) {
		Map<String,String> targetIdMap = sheet.getKeyMap( targetBeanId );
		Map<String,String> srcIdMap = sheet.getKeyMap( srcBeanId );

		return extractValue( targetIdMap, srcIdMap, key );
	}

	/**
	 * ソースとなる定義名から、キーを抽出し、別のビーンに再マッピングします。
	 * <br>その際、キー値が数値に変換可能な文字列であることを前提として、
	 * キー値に数値変換した文字列、値に無変換のキー値を設定します。
	 *
	 * @param beanId
	 * @return
	 */
	public static final List<ItemLabelsBean> mappingItemLabelsBeanByKeyParseInt( IDesignBeanId beanId ) {
		Map<String,String> srcIdMap = sheet.getKeyMap( beanId );

		return 	mappingItemLabelsBeanByKeyParseInt( srcIdMap );
	}

}
