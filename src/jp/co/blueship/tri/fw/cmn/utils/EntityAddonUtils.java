package jp.co.blueship.tri.fw.cmn.utils;


import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * エンティティへの入出力の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class EntityAddonUtils {

	/**
	 * グループユーザエンティティからグループIDのセットを取得します。
	 * @param groupUserEntitys グループユーザエンティティ
	 * @return グループIDのセット
	 */
	public static Set<String> getGroupIdSet( List<IGrpUserLnkEntity> groupUserEntitys ) {

		Set<String> groupIdSet = new HashSet<String>();
		for ( IGrpUserLnkEntity entity : groupUserEntitys ) {
			groupIdSet.add( entity.getGrpId() );
		}

		return groupIdSet;
	}

}
