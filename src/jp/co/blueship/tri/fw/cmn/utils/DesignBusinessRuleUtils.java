package jp.co.blueship.tri.fw.cmn.utils;

import java.io.File;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotMdlLnkEntity;
import jp.co.blueship.tri.am.dao.vcsrepos.eb.IVcsMdlEntity;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 業務を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @author Yukihiro Eguchi
 *
 */
public class DesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param list コンバート間に流通させるビーン
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static File getMasterWorkPath( List<Object> list )
			throws TriSystemException {

		ILotEntity entity = ExtractEntityAddonUtils.extractPjtLot( list );

		if ( null == entity ) {
			throw new TriSystemException( SmMessageId.SM005152S );
		} else {
			File file = new File( entity.getLotMwPath() );
			return file;
		}
	}

	/**
	 * 原本作業格納フォルダを取得します。
	 *
	 * @param entity ロットエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static File getMasterWorkPath( ILotEntity entity )
			throws TriSystemException {

		if ( null == entity ) {
			throw new TriSystemException( SmMessageId.SM005152S );
		} else {
			File file = new File( entity.getLotMwPath() );
			return file;
		}
	}

	/**
	 * ワークスペースフォルダを取得します。
	 *
	 * @param entity プロジェクトエンティティ
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	private static final File getWorkspacePath( ILotEntity entity ) throws TriSystemException {
		File file = null;

		if ( null == entity ){
			throw new TriSystemException( SmMessageId.SM005153S );
		}
		if ( null == entity.getWsPath() ){
			throw new TriSystemException( SmMessageId.SM004161F );
		}
		file = new File( entity.getWsPath() );

		return file;
	}

	/**
	 * サービス単位のログの管理フォルダを取得します。
	 *
	 * @param lotEntity ロットエンティティ
	 * @param flowAction 実行サービス名ID
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getWorkspaceLogPath( ILotEntity lotEntity, String flowActionId ) throws TriSystemException {

		File file = null;
		File workspaceFile = getWorkspacePath( lotEntity );

		if ( null == flowActionId ){
			throw new TriSystemException( SmMessageId.SM005154S );
		}
		file = new File( workspaceFile, TriStringUtils.linkPath(
												sheet.getValue( UmDesignEntryKeyByCommon.logRelationPath ),
												flowActionId )
										 );

		return file;
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlEntity モジュールエンティティー
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	public static final String convertPathInModule( String relativePath, ILotMdlLnkEntity mdlEntity ) {
		return convertPathInModule( relativePath, mdlEntity.getMdlNm() );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlEntity モジュールエンティティー
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	public static final String convertPathInModule( String relativePath, IVcsMdlEntity mdlEntity ) {
		return convertPathInModule( relativePath, mdlEntity.getMdlNm() );
	}

	/**
	 * モジュールから始まる相対パスから、モジュール名と"/"を除去してそれ以降のパスに変換する。
	 * 指定されたモジュールに該当しない場合、nullを戻します。
	 *
	 * @param path モジュールから始まる相対パス
	 * @param mdlName モジュール名
	 * @return 変換したパスを戻します。モジュールに該当しない場合、null。
	 */
	private static final String convertPathInModule( String relativePath, String mdlName ) {
		String tempPath = TriStringUtils.convertPath( relativePath );

		if ( tempPath.startsWith("/") )
			tempPath = StringUtils.substringAfter(tempPath, "/");

		if( StringUtils.substringBefore( tempPath, "/" ).equals( mdlName) ) {
			return tempPath.substring( tempPath.indexOf( "/" ) + 1 ) ;//「モジュール名」部分および先頭の"/"を取り除く
		}

		return null;
	}

	/**
	 * テンプレートフォルダを取得します。
	 *
	 * @return 取得した情報を戻します。
	 * @throws TriSystemException マスタ情報にシステム運用上の誤りがある場合
	 */
	public static final File getTemplatePath() throws TriSystemException {

		return new File(	sheet.getValue( UmDesignEntryKeyByCommon.homeTopPath ),
							sheet.getValue( UmDesignEntryKeyByCommon.templatesRelativePath ));
	}
}
