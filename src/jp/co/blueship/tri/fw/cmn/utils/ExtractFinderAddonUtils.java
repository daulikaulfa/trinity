package jp.co.blueship.tri.fw.cmn.utils;

import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.FinderSupport;

/**
 * データアクセスを扱うユーティリティークラスです。
 * <br>データアクセスファインダーを抽出します。
 *
 * @author Yukihiro Eguchi
 *
 */
public class ExtractFinderAddonUtils {

	/**
	 * 指定されたリストからデータアクセスファインダーを取得します。
	 * @param list 検索するリスト
	 * @return 取得した値を戻します。取得できない場合はnullを戻します。
	 */
	public static FinderSupport extractFinderSupport( List<Object> list ) {

		for ( Iterator<Object> it = list.iterator(); it.hasNext(); ) {
			Object obj = it.next();

			if ( obj instanceof FinderSupport ) {
				FinderSupport finder = (FinderSupport)obj;

				return finder;
			}
		}

		return null;
	}

}
