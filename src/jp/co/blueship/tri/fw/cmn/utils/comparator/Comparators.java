package jp.co.blueship.tri.fw.cmn.utils.comparator;

import java.util.Comparator;

import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;

/**
 * コンパレータを定義するクラスです。
 *
 *
 * @author Takayuki Kubo
 *
 */
public final class Comparators {

	/** ユーザID*/
	public static final Comparator<IUserEntity> IS_EQUAL_USER_ID = new Comparator<IUserEntity>() {

		@Override
		public int compare(IUserEntity entity1, IUserEntity entity2) {

			return entity1.getUserId().compareTo(entity2.getUserId());
		}
	};
}
