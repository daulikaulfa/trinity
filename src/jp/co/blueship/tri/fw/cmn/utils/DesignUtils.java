package jp.co.blueship.tri.fw.cmn.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Locale;
import java.util.ResourceBundle;

import jp.co.blueship.tri.fw.constants.EditionType;

/**
 * 定義リソースから値を取得するClass
 *
 *
 * @version V3L10R01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class DesignUtils {

	/** trinityInit */
	private static ResourceBundle initRb = ResourceBundle.getBundle("trinityInit");

	private static ResourceBundle editionRb = ResourceBundle.getBundle("trinityRom");

	private static ResourceBundle itemRb = ResourceBundle.getBundle("trinityItem", new Locale( SystemProps.TriSystemLanguage.getProperty() ) );

	private static ResourceBundle msgParam = ResourceBundle.getBundle("messageParameters",new Locale( SystemProps.TriSystemLanguage.getProperty() ) );
	private static HashMap<String, String> itemMap = null;

	/**
	 * 定義ファイル優先パスの取得
	 *
	 * @return
	 */
	public static String getPriorityDefinePath() {

		return initRb.getString("priority.define.path");
	}

	/**
	 * 定義ファイル優先パス（デザインシート）の取得
	 *
	 * @return
	 */
	public static String getPriorityDefineDesignPath() {

		return initRb.getString("priority.define.design.path");
	}

	/**
	 * エディション情報の取得
	 *
	 * @return
	 */
	public static EditionType getEdition() {

		String hashRom = editionRb.getString("info.rom");
		EditionType editionType = EditionType.getEncryptionValue(hashRom);
		return editionType;
	}

	/**
	 * 項目用リソースバンドルオブジェクトの取得
	 *
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public static HashMap<String, String> getItemMap() {

		if (null == DesignUtils.itemMap) {

			HashMap<String, String> itemMap = new HashMap<String, String>();
			Enumeration<String> keyEnum = itemRb.getKeys();

			while (keyEnum.hasMoreElements()) {

				String key = keyEnum.nextElement();
				String value = itemRb.getString(key);
				itemMap.put(key, value);
			}
			DesignUtils.itemMap = itemMap;
		}

		return (HashMap<String, String>) DesignUtils.itemMap.clone();
	}

	/**
	 * 指定されたメッセージパラメータキーに対応するパラメータ値をメッセージパラメータリソースより検索して返します。
	 *
	 * @param key メッセージパラメータキー
	 * @return パラメータ値
	 */
	public static String getMessageParameter(String key) {
		PreConditions.assertOf(msgParam != null, "Message parameter ResourceBundle is null.");
		PreConditions.assertOf(msgParam.containsKey(key), "Message parameter [" + key + "] is not defined in Message Parameter Resource.");

		return msgParam.getString(key);
	}

	/**
	 * 定義ファイルの{@link InputStream}を取得する
	 *
	 * @param clazz クラスパスの起点となるクラス
	 * @param fileName 定義ファイル名
	 * @param priorityDefinePath 参照を優先するパス
	 * @return 定義ファイルの{@link InputStream}
	 */
	@SuppressWarnings("resource")
	public <T> InputStream getDefineFileStream(Class<T> clazz, String fileName, String priorityPath) {

		File defFile = new File(priorityPath, fileName);
		InputStream is = null;

		try {
			// 先ずファイルパスで取得を試みる
			is = new FileInputStream(defFile);
//			LogHandler.info(log, TriLogMessage.LSM0023 , defFile.toString() );
		} catch (FileNotFoundException fnfe) {
			// ファイルパスで見つからない場合、クラスパスで取得する
			// getResourceAsStream()でも取得できない場合はisがnullになる
			is = clazz.getClassLoader().getResourceAsStream(fileName);
//			LogHandler.info(log, TriLogMessage.LSM0024 , fileName);
		}

		return is;

	}
}
