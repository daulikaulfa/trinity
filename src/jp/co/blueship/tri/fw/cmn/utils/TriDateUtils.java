package jp.co.blueship.tri.fw.cmn.utils;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import jp.co.blueship.tri.fw.constants.LanguageType;
import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 日付(文字列)操作の共通処理Class
 *
 * @version V3L10.01
 * @author trintyV3
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 * @version V3L11.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TriDateUtils {

	private static final String TIMESTAMP = "yyyy-mm-dd hh:mm:ss.SSS";
	private static final String DEFAULT_DATE_PATTERN = "yyyy/MM/dd HH:mm:ss.SSS";
	private static final String DEFAULT_PATTERN_YYYYMMDD_HHMMSS = "yyyy/MM/dd HH:mm:ss";
	private static final String DEFAULT_PATTERN_YYYYMMDD_HHMMSS_en = "dd/MM/yyyy HH:mm:ss";
	private static final String DEFAULT_PATTERN_YYYYMMDD_HHMM = "yyyy/MM/dd HH:mm";
	private static final String DEFAULT_PATTERN_YYYYMMDD_HHMM_en = "dd/MM/yyyy HH:mm";
	private static final String EXPORT_TO_FILE_PATTERN_YYYYMMDD_HHMM = "yyyyMMdd-HHmm";
	private static final String EXPORT_TO_FILE_PATTERN_YYYYMMDD_HHMM_en = "ddMMyyyy-HHmm";
	private static final String DATE_PATTERN_YYYYMMDD = "yyyy/MM/dd";
	private static final String DATE_PATTERN_YYYYMMDD_en = "dd/MM/yyyy";
	private static final String DATE_PATTERN_YYYYMM = "yyyy/MM";
	private static final String DATE_PATTERN_YYYYMM_en = "MM/yyyy";
	private static final String DATE_PATTERN_HHMMSS = "HH:mm:ss";
	private static final String DATE_PATTERN_HHMM = "HH:mm";
	private static final String DATE_PATTERN_YMDHMSS_FLAT = "yyMMddHHmmssSSS";
	private static final String DATE_PATTERN_YMD_FLAT = "yyMMdd";
	private static final String DATE_PATTERN_YM_FLAT = "yyMM";

	/*
	 * SimpleDateFormatに対するスレッドセーフ対応
	 */
	private static final ThreadLocal<SimpleDateFormat> dflocalTimestampFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(TIMESTAMP);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalDefaultDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_DATE_PATTERN);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDHMSDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_PATTERN_YYYYMMDD_HHMMSS);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDHMSDateFormat_en = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_PATTERN_YYYYMMDD_HHMMSS_en);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDHMDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_PATTERN_YYYYMMDD_HHMM);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDHMDateFormat_en = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DEFAULT_PATTERN_YYYYMMDD_HHMM_en);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalExportFileYMDHMDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(EXPORT_TO_FILE_PATTERN_YYYYMMDD_HHMM);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalExportFileYMDHMDateFormat_en = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(EXPORT_TO_FILE_PATTERN_YYYYMMDD_HHMM_en);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YYYYMMDD);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDDateFormat_en = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YYYYMMDD_en);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YYYYMM);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalYMDateFormat_en = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YYYYMM_en);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalHMSDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_HHMMSS);
		}
	};
	private static final ThreadLocal<SimpleDateFormat> dflocalHMDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_HHMM);
		}
	};

	private static final ThreadLocal<SimpleDateFormat> dflocalYMDHMSS_FLATDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YMDHMSS_FLAT);
		}
	};

	private static final ThreadLocal<SimpleDateFormat> dflocalYMD_FLATDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YMD_FLAT);
		}
	};

	private static final ThreadLocal<SimpleDateFormat> dflocalYM_FLATDateFormat = new ThreadLocal<SimpleDateFormat>() {
		@Override
		protected SimpleDateFormat initialValue() {
			return new SimpleDateFormat(DATE_PATTERN_YM_FLAT);
		}
	};

	/**
	 * Gets the default TimeZone for this host. The source of the default TimeZone may vary with implementation.
	 *
	 * @return a default TimeZone.
	 */
	public static TimeZone getDefaultTimeZone() {
		return TimeZone.getDefault();
	}

	/**
	 * Gets the TimeZone for the given ID.
	 *
	 * @param ID  the ID for a TimeZone, either an abbreviation such as "PST", a full name such as "America/Los_Angeles", or a custom ID such as "GMT-8:00". Note that the support of abbreviations is for JDK 1.1.x compatibility only and full names should be used.
	 * @return the specified TimeZone, or the GMT if the given ID cannot be understood.
	 */
	public static TimeZone getTimeZone( String Id ) {
		return TimeZone.getTimeZone( Id );
	}

	/**
	 * @param format data format.
	 * @param zone the given new time zone.
	 * @return the data format.
	 */
	private static SimpleDateFormat setTimeZone( SimpleDateFormat format, TimeZone zone ) {
		if ( null != zone ) {
			format.setTimeZone(zone);
		}

		return format;
	}

	/**
	 * 標準の日付フォーマット文字列をDateオブジェクトに変換する。
	 *
	 * @param value
	 * @return
	 */
//	public static synchronized Date convertStringToDate( String value ) {
//
//        try {
//            return getDefaultDateFormat().parse( value );
//        } catch (ParseException e) {
//            return null;
//        }
//    }

	/**
	 * 文字列が標準の日付フォーマットであるかをチェックする
	 * @param value
	 * @return
	 */
	public static boolean isDefaultDateFormat( String value ) {

        try {
            getDefaultDateFormat().parse( value );
        } catch ( ParseException e ) {
            return false;
        }
        return true;
    }

	/**
	 * Timestampフォーマットを取得する。
	 * @return 標準の日時フォーマット
	 */
	public static SimpleDateFormat getTimestampFormat() {
		return dflocalTimestampFormat.get();
	}

	/**
	 * 標準の日時フォーマットを取得する。
	 * @return 標準の日時フォーマット
	 */
	public static SimpleDateFormat getDefaultDateFormat() {
		return dflocalDefaultDateFormat.get();
	}

	/**
	 * 標準の日時フォーマットを取得する。
	 * @param zone the time zone.
	 * @return 標準の日時フォーマット
	 */
	public static SimpleDateFormat getDefaultDateFormat( TimeZone zone ) {
		return setTimeZone( getDefaultDateFormat(), zone );
	}

	/**
	 * yyMMddHHmmssSSS形式の日時フォーマットを取得する。
	 * @return yyMMddHHmmssSSS形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDHMSS_FLATDateFormat() {
		return dflocalYMDHMSS_FLATDateFormat.get();
	}

	/**
	 * yyMMdd形式の日時フォーマットを取得する。
	 * @return yyMMdd形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMD_FLATDateFormat() {
		return dflocalYMD_FLATDateFormat.get();
	}

	/**
	 * yyMM形式の日時フォーマットを取得する。
	 * @return yyMM形式の日時フォーマット
	 */
	public static SimpleDateFormat getYM_FLATDateFormat() {
		return dflocalYM_FLATDateFormat.get();
	}

	/**
	 * yyyy/MM/dd HH:mm:ss形式の日時フォーマットを取得する。
	 * @param locale
	 * @return yyyy/MM/dd形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDHMSDateFormat( String locale ) {
		return getYMDHMSDateFormat( locale, (TimeZone)null );
	}

	/**
	 * yyyy/MM/dd HH:mm:ss形式の日時フォーマットを取得する。
	 * @param locale
	 * @param zone the time zone.
	 * @return yyyy/MM/dd形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDHMSDateFormat( String locale, TimeZone zone ) {
		if ( TriStringUtils.isEmpty( locale ) ) {
			return setTimeZone(dflocalYMDHMSDateFormat.get(), zone);
		}

		if ( LanguageType.EN.getValue().equals( locale ) ) {
			return setTimeZone(dflocalYMDHMSDateFormat_en.get(), zone);
		} else {
			return setTimeZone(dflocalYMDHMSDateFormat.get(), zone);
		}
	}

	/**
	 * yyyy/MM/dd HH:mm形式の日時フォーマットを取得する。
	 *
	 * @param locale
	 * @param zone the time zone.
	 * @return
	 */
	public static SimpleDateFormat getYMDHMDateFormat( String locale, TimeZone zone ) {
		if ( TriStringUtils.isEmpty( locale ) ) {
			return setTimeZone(dflocalYMDHMDateFormat.get(), zone);
		}

		if ( LanguageType.EN.getValue().equals( locale ) ) {
			return setTimeZone(dflocalYMDHMDateFormat_en.get(), zone);
		} else {
			return setTimeZone(dflocalYMDHMDateFormat.get(), zone);
		}
	}

	/**
	 * yyyyMMdd HHmm形式の日時フォーマットを取得する。
	 *
	 * @param locale
	 * @param zone the time zone.
	 * @return
	 */
	public static SimpleDateFormat getYMDHMDateFormatOfExportFile( String locale, TimeZone zone ) {
		if ( TriStringUtils.isEmpty( locale ) ) {
			return setTimeZone(dflocalExportFileYMDHMDateFormat.get(), zone);
		}

		if ( LanguageType.EN.getValue().equals( locale ) ) {
			return setTimeZone(dflocalExportFileYMDHMDateFormat_en.get(), zone);
		} else {
			return setTimeZone(dflocalExportFileYMDHMDateFormat.get(), zone);
		}
	}

	/**
	 * yyyy/MM/dd形式の日時フォーマットを取得する。
	 * @return yyyy/MM/dd形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDDateFormat() {
		return dflocalYMDDateFormat.get();
	}

	/**
	 * yyyy/MM/dd形式の日時フォーマットを取得する。
	 * @param locale
	 * @param zone the time zone.
	 * @return yyyy/MM/dd形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDDateFormat( String locale, TimeZone zone ) {
		if ( TriStringUtils.isEmpty( locale ) ) {
			return setTimeZone(dflocalYMDDateFormat.get(), zone);
		}

		if ( LanguageType.EN.getValue().equals( locale ) ) {
			return setTimeZone(dflocalYMDDateFormat_en.get(), zone);
		} else {
			return setTimeZone(dflocalYMDDateFormat.get(), zone);
		}
	}

	/**
	 * yyyy/MM/dd形式の日時フォーマットを取得する。
	 * @param locale
	 * @param zone the time zone.
	 * @return yyyy/MM形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDateFormat( String locale, TimeZone zone ) {
		if ( TriStringUtils.isEmpty( locale ) ) {
			return setTimeZone(dflocalYMDateFormat.get(), zone);
		}

		if ( LanguageType.EN.getValue().equals( locale ) ) {
			return setTimeZone(dflocalYMDateFormat_en.get(), zone);
		} else {
			return setTimeZone(dflocalYMDateFormat.get(), zone);
		}
	}

	/**
	 * HH:mm:ss形式の日時フォーマットを取得する。
	 * @param locale
	 * @param zone the time zone.
	 * @return HH:MM:SS形式の日時フォーマット
	 */
	public static SimpleDateFormat getHMSDateFormat( TimeZone zone ) {
		return setTimeZone(dflocalHMSDateFormat.get(), zone);
	}

	/**
	 * HH:mm形式の日時フォーマットを取得する。
	 * @param locale
	 * @param zone the time zone.
	 * @return HH:MM形式の日時フォーマット
	 */
	public static SimpleDateFormat getHMDateFormat( TimeZone zone ) {
		return setTimeZone(dflocalHMDateFormat.get(), zone);
	}

	/**
	 * yyyy/MM形式の日時フォーマットを取得する。
	 * @return yyyy/MM形式の日時フォーマット
	 */
	public static SimpleDateFormat getYMDateFormat() {
		return dflocalYMDateFormat.get();
	}
	/**
	 * HH:mm:ss形式の日時フォーマットを取得する。
	 * @return HH:mm:ss形式の日時フォーマット
	 */
	public static SimpleDateFormat getHMSDateFormat() {
		return dflocalHMSDateFormat.get();
	}

	/**
	 * HH:mm形式の日時フォーマットを取得する。
	 * @return HH:mm形式の日時フォーマット
	 */
	public static SimpleDateFormat getHMDateFormat() {
		return dflocalHMDateFormat.get();
	}

	/**
	 * 標準の日時パターンを取得する。
	 * @return 標準の日時パターン
	 */
	public static String getDefaultDatePattern() {
		return DEFAULT_DATE_PATTERN;
	}

	/**
	 * システム日時を取得する。
	 * <br>フォーマットは、システム固定(DateAddonUtil.DEFAULT_DATE_PATTERN)
	 *
	 * @return システム日時
	 */
	public static synchronized String getSystemDate() {
		return getDefaultDateFormat().format( new Date() );
	}

	/**
	 * システム日時を指定されたフォーマットで取得する。
	 *
	 * @param format 日付フォーマット
	 * @return システム日時
	 */
	public static synchronized String getSystemDate( SimpleDateFormat format ) {
		return format.format( new Date() );
	}

	/**
	 * システム日時を取得する。
	 *
	 * @return システム日時
	 */
	public static synchronized Timestamp getSystemTimestamp() {
		return new Timestamp(System.currentTimeMillis());
	}

	/**
	 * 日時を指定されたフォーマットで取得する。
	 * <br>フォーマットは、システム固定(DateAddonUtil.DEFAULT_DATE_PATTERN)
	 *
	 * @param date 日付
	 * @return 編集した日時
	 */
	public static synchronized String getDate( Date date ) {
		return getDefaultDateFormat().format( date );
	}

	/**
	 * 日時を指定されたフォーマットで取得する。
	 *
	 * @param format 日付フォーマット
	 * @param date 日付
	 * @return 編集した日時
	 */
	public static synchronized String getDate( SimpleDateFormat format, Date date ) {
		String formatDate = new String( "" );
		if( null!=date ) {
			formatDate = format.format( date.getTime() );
		}
		return formatDate;
	}

	/**
	 * Converts the date of the timezone.
	 * <br>
	 * <br>文字列の日付を指定したタイムゾーンに従って変換する。
	 *
	 * @param date Target Date
	 * @param srcZone timezone of target
	 * @param descZone timezone of after convertion
	 * @return
	 */
	public static String convertTimezone( String date, TimeZone srcZone, TimeZone descZone ) {
		Date srcDate;
		SimpleDateFormat srcFormat = getYMDDateFormat();

		try {
			srcDate = setTimeZone( srcFormat, srcZone ).parse(date);

		} catch ( ParseException pe ) {
			pe.printStackTrace();
			throw new TriSystemException(SmMessageId.SM004174F, pe , srcFormat.toPattern() , date );
		}

		return convertTimezone( srcDate, descZone);
	}

	/**
	 * Converts the date of the timezone.
	 * <br>
	 * <br>文字列の日付を指定したタイムゾーンに従って変換する。
	 *
	 * @param date Target Date
	 * @param descZone timezone of after convertion
	 * @return
	 */
	public static String convertTimezone( Timestamp srcDate, TimeZone descZone ) {
		return convertTimezone( convertTimestampToDate( srcDate ), descZone );
	}

	/**
	 * Converts the date of the timezone.
	 * <br>
	 * <br>文字列の日付を指定したタイムゾーンに従って変換する。
	 *
	 * @param date Target Date
	 * @param descZone timezone of after convertion
	 * @return
	 */
	public static String convertTimezone( Date srcDate, TimeZone descZone ) {
		return setTimeZone( getYMDDateFormat(), descZone ).format(srcDate);
	}

	/**
	 * 日付を示す文字列(標準の日時フォーマット)を、表示形式に従った
	 * フォーマットに変換する。
	 * @param strDate 日付を示す文字列(標準の日時フォーマット)
	 * @param format 日付書式
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( String strDate, SimpleDateFormat format ) {

		if ( TriStringUtils.isEmpty( strDate ) ) {
			return "";
		}

		Date date = null;

		try {
			date = getDefaultDateFormat().parse( strDate );

			return format.format( date );
		} catch ( ParseException pe ) {
			pe.printStackTrace();
			throw new TriSystemException(SmMessageId.SM004174F, pe , DEFAULT_DATE_PATTERN , strDate );
		}
	}

	/**
	 * 日付を示す文字列(標準の日時フォーマット)を、表示形式に従った
	 * フォーマットに変換する。
	 * @param strDate 日付を示す文字列(標準の日時フォーマット)
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( String strDate ) {
		return convertViewDateFormat( strDate, (TimeZone)null );
	}

	/**
	 * 日付を示す文字列(標準の日時フォーマット)を、表示形式に従った
	 * フォーマットに変換する。
	 * @param strDate 日付を示す文字列(標準の日時フォーマット)
	 * @param date 日付
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( String strDate, TimeZone zone ) {
		SimpleDateFormat viewDate = getViewDateFormat(zone);

		return convertViewDateFormat( strDate, viewDate );
	}

	/**
	 * 日付を、表示形式に従った
	 * フォーマットに変換する。
	 * @param date 日付
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( Timestamp date ) {
		return convertViewDateFormat( date , (TimeZone)null );
	}

	/**
	 * 日付を、表示形式に従った
	 * フォーマットに変換する。
	 * @param date 日付
	 * @param zone the time zone.
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( Timestamp date, TimeZone zone ) {
		return convertViewDateFormat( date , getViewDateFormat(zone) );
	}

	/**
	 * 日付を、表示形式に従った
	 * フォーマットに変換する。
	 * @param date 日付
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( Timestamp date , SimpleDateFormat format ) {
		return TriDateUtils.getDate( format , date );
	}

	/**
	 * 日付を、表示形式に従った
	 * フォーマットに変換する。
	 * @param date 日付
	 * @param srcFormat
	 * @param destFormat
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertViewDateFormat( String date , SimpleDateFormat srcFormat, SimpleDateFormat destFormat ) {
		if ( TriStringUtils.isEmpty(date) )
			return "";

		try {
			return destFormat.format( srcFormat.parse(date) );

		} catch ( ParseException pe ) {
			pe.printStackTrace();
			throw new TriSystemException(SmMessageId.SM004174F, pe , srcFormat.toPattern() , date );
		}
	}

	/**
	 * 日付を、表示形式に従った
	 * フォーマットに変換する。
	 * @param date 日付
	 * @param srcFormat
	 * @param destFormat
	 * @return 日付を示す文字列(表示形式に従ったフォーマット)
	 */
	public static String convertExpirationViewDateFormat( String date , SimpleDateFormat srcFormat, SimpleDateFormat destFormat ) {
		if ( TriStringUtils.isEmpty(date) || date.length() != 6) {
			return "";
		}

		Calendar calendar = Calendar.getInstance();
		int month = Integer.parseInt(date.substring(4)) - 1;
		int year = Integer.parseInt(date.substring(0, 4));
		calendar.set(Calendar.YEAR, year);
		calendar.set(Calendar.MONTH, month);
		String maxDay = Integer.toString(calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		if(maxDay.length() < 2) {
			date += "0";
		}
		date += maxDay;

		return convertViewDateFormat(date, srcFormat, destFormat);
	}

	/**
	 * Timestamp型dateをDate型に変換する。
	 * @param date 日付(Timestamp型)
	 * @return Date型 の date
	 */
	public static Date convertTimestampToDate( Timestamp date ) {
		return new Date( date.getTime() );
		//return convertStringToDate(TriDateUtils.getDate( getViewDateFormat() , date) );
	}

	/**
	 * 文字列形式の日付時刻をタイムスタンプに変換します。日付時刻フォーマットはデザインシートに指定された<br/>
	 * ものと同一であることを前提とします。
	 *
	 * @param date 文字列形式の日付時刻
	 * @return タイムスタンプ
	 * @throws ParseException 日付時刻フォーマットがデザインシートに指定されたものとは異なる。
	 */
	public static Timestamp convertStringToTimestampWithDateTime( String dateTimeString ) {

		return convertStringToTimestamp(dateTimeString, (TimeZone)null);
	}

	/**
	 * 文字列形式の日付時刻をタイムスタンプに変換します。日付時刻フォーマットはデザインシートに指定された<br/>
	 * ものと同一であることを前提とします。
	 *
	 * @param date 文字列形式の日付時刻
	 * @param zone the time zone.
	 * @return タイムスタンプ
	 * @throws ParseException 日付時刻フォーマットがデザインシートに指定されたものとは異なる。
	 */
	public static Timestamp convertStringToTimestamp( String dateTimeString, TimeZone zone ) {

		return convertStringToTimestamp( dateTimeString, getViewDateFormat(zone) );
	}

	/**
	 * 文字列形式の日付時刻をタイムスタンプに変換します。日付時刻フォーマットはデザインシートに指定された<br/>
	 * ものと同一であることを前提とします。
	 *
	 * @param date 文字列形式の日付時刻
	 * @param srcFormat
	 * @return タイムスタンプ
	 * @throws ParseException 日付時刻フォーマットがデザインシートに指定されたものとは異なる。
	 */
	public static Timestamp convertStringToTimestamp( String dateTimeString, SimpleDateFormat srcFormat ) {

		try {
			return new Timestamp(srcFormat.parse(dateTimeString).getTime());

		} catch (ParseException pe) {
			pe.printStackTrace();
			throw new TriSystemException(SmMessageId.SM004174F, pe , srcFormat.toPattern() , dateTimeString );
		}
	}

	/**
	 * 日時項目の表示形式を取得する。
	 * @return 日時項目の表示形式
	 */
	public static SimpleDateFormat getViewDateFormat() {
		return getViewDateFormat(null);
	}

	/**
	 * 日時項目の表示形式を取得する。
	 * @param zone the time zone.
	 * @return 日時項目の表示形式
	 */
	public static SimpleDateFormat getViewDateFormat( TimeZone zone ) {
		SimpleDateFormat viewDate = null;

		String strValue = DesignSheetFactory.getDesignSheet().getValue( UmDesignEntryKeyByCommon.viewDateFormat );

		try {
			if ( TriStringUtils.isEmpty( strValue ) ) {
				// 定義がなかったらとりあえず標準形式で表示する。
				viewDate = TriDateUtils.getDefaultDateFormat();
			} else {
				viewDate = new SimpleDateFormat( strValue );
			}
		} catch ( IllegalArgumentException iae ) {
			throw new TriSystemException(SmMessageId.SM004165F , iae , strValue);
		}

		return setTimeZone( viewDate, zone );
	}

	/**
	 * 日付(年～ミリ秒)を示す部分文字列(標準の日時フォーマット)を、日付を示す文字列
	 * (標準の日時フォーマット)のFROM形式に変換する。
	 * 日付を示す部分文字列は日付を示す文字列に、左側から一致しているも
	 * のとする(例："2008"、"2008/08/"、"2008/08/04 14:23" など)。
	 * <br>
	 * 変換例："2008/8"→"2008/08/01 00:00:00.000" など
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @return 日付を示す文字列(標準の日時フォーマット)
	 */
	public static String convertFromTime( String strDate ) {

		//                         年     月    日    時    分    秒   ミリ秒
		String[] defaultDate = { "0000", "01", "01", "00", "00", "00", "000" };

		String fromDate = mapDefaultTime( strDate, defaultDate );

		return fromDate;

	}

	/**
	 * 日付(年～ミリ秒)を示す部分文字列(標準の日時フォーマット)を、日付を示す文字列
	 * (標準の日時フォーマット)のTO形式に変換する。
	 * 日付を示す部分文字列は日付を示す文字列に、左側から一致しているも
	 * のとする(例："2008"、"2008/08/"、"2008/08/04 14:23" など)。
	 * <br>
	 * 変換例："2008/8"→"2008/08/31 23:59:59.999" など
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @return 日付を示す文字列(標準の日時フォーマット)
	 */
	public static String convertToTime( String strDate ) {

		//                         年     月    日    時    分    秒   ミリ秒
		String[] defaultDate = { "9999", "12", "31", "23", "59", "59", "999" };

		String fromDate = mapDefaultTime( strDate, defaultDate );

		return fromDate;

	}

	/**
	 * 日付(年～日)を示す部分文字列(標準の日時フォーマット)を、日付を示す文字列
	 * (標準の日時フォーマット)のFROM形式に変換する。
	 * 日付を示す部分文字列は日付を示す文字列に、左側から一致しているも
	 * のとする(例："2008"、"2008/08/"、"2008/08/04" など)。
	 * <br>
	 * 変換例："2008/8"→"2008/08/01" など
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @return 日付を示す文字列(標準の日時フォーマット)
	 */
	public static String convertFromDate( String strDate ) {

		//                         年     月    日
		String[] defaultDate = { "0000", "01", "01" };

		String fromDate = mapDefaultDate( strDate, defaultDate );

		return fromDate;

	}

	/**
	 * 日付(年～日)を示す部分文字列(標準の日時フォーマット)を、日付を示す文字列
	 * (標準の日時フォーマット)のTO形式に変換する。
	 * 日付を示す部分文字列は日付を示す文字列に、左側から一致しているも
	 * のとする(例："2008"、"2008/08/"、"2008/08/04" など)。
	 * <br>
	 * 変換例："2008/8"→"2008/08/31" など
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @return 日付を示す文字列(標準の日時フォーマット)
	 */
	public static String convertToDate( String strDate ) {

		//                         年     月    日
		String[] defaultDate = { "9999", "12", "31" };

		String fromDate = mapDefaultDate( strDate, defaultDate );

		return fromDate;

	}

	/**
	 * 日付(年～日)を示す部分文字列(標準の日時フォーマット)をデフォルトの
	 * 日付フォーマットにマッピングする。
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @param defaultDate デフォルトの日付フォーマット
	 * @return マッピング後の日付文字列
	 */
	private static String mapDefaultDate( String strDate, String[] defaultDate ) {

		strDate = strDate.trim();

		defaultDate = mapDefaultYMD( strDate, defaultDate );

		return createDateString( defaultDate, false );
	}

	/**
	 * 日付(年～ミリ秒)を示す部分文字列(標準の日時フォーマット)をデフォルトの
	 * 日付フォーマットにマッピングする。
	 * @param strDate 日付を示す部分文字列(標準の日時フォーマット)
	 * @param defaultDate デフォルトの日付フォーマット
	 * @return マッピング後の日付文字列
	 */
	private static String mapDefaultTime( String strDate, String[] defaultDate ) {

		strDate = strDate.trim();
		String[] splitDate = StringUtils.split( strDate );

		// 日付までの指定と判定("2008/08"、"2008/08/04"など)
		if ( splitDate.length == 1 ) {

			defaultDate = mapDefaultYMD( splitDate[0], defaultDate );

		// 日付以降も指定されていると判定("2008/08/04 12:00"など)
		} else if ( splitDate.length == 2 ) {

			defaultDate = mapDefaultYMD( splitDate[0], defaultDate );

			String[] splitHMS = StringUtils.split( splitDate[1], ":" );

			// 分までの指定と判定("2008/08/04 12:00"など)
			if ( splitHMS.length < 3 ) {

				defaultDate = mapDefaultHMS( splitHMS, defaultDate );

			// 秒以下まで指定されていると判定("2008/08/04 12:00:00"、"2008/08/04 12:00:00.000"など)
			} else if ( splitHMS.length == 3 ) {

				String[] splitS = StringUtils.split( splitHMS[2], "." );

				// 秒までの指定と判定("2008/08/04 12:00:00"など)
				if ( splitS.length < 2 ) {

					defaultDate = mapDefaultHMS( splitHMS, defaultDate );

				// ミリ秒までの指定と判定("2008/08/04 12:00:00.000"など)
				} else if ( splitS.length == 2 ) {

					defaultDate[ 3 ] = splitHMS[0];	// 時
					defaultDate[ 4 ] = splitHMS[1];	// 分
					defaultDate[ 5 ] = splitS[0];	// 秒
					defaultDate[ 6 ] = splitS[1];	// ミリ秒
				}
			}
		}

		return createDateString( defaultDate, true );
	}

	/**
	 * 日付(年月日)を示す部分文字列(標準の日時フォーマット)をデフォルトの日付フォーマットに
	 * マッピングする。
	 * @param strYMD 日付を示す部分文字列(年月日・標準の日時フォーマット)
	 * @param defaultDate デフォルトの日付フォーマット
	 * @return マッピング後の日付文字列
	 */
	private static String[] mapDefaultYMD( String strYMD, String[] defaultDate ) {

		String[] splitYMD = StringUtils.split( strYMD, "/" );

		for ( int index = 0; index < splitYMD.length; index++ ) {
			defaultDate[ index ] = splitYMD[ index ];
		}

		return defaultDate;
	}

	/**
	 * 日付(時分秒)を示す部分文字列をデフォルトの日付フォーマットに
	 * マッピングする。
	 * @param splitHMS 日付を示す部分文字列(時分秒・標準の日時フォーマット)
	 * @param defaultDate デフォルトの日付フォーマット
	 * @return マッピング後の日付文字列
	 */
	private static String[] mapDefaultHMS( String[] splitHMS, String[] defaultDate ) {

		for ( int index = 0; index < splitHMS.length; index++ ) {
			defaultDate[ index + 3 ] = splitHMS[ index ];
		}

		return defaultDate;
	}

	/**
	 * 日付文字列形式(標準の日時フォーマット)を作成する。
	 * @param splitDate 年、月、日、時、分、秒、ミリ秒が格納された配列
	 * @param fullMode false:年月日まで、true:年月日時分秒ミリ秒すべて
	 * @return 日付文字列形式(標準の日時フォーマット)
	 */
	private static String createDateString( String[] splitDate, boolean fullMode ) {

		StringBuilder dateString = new StringBuilder();

		dateString.append( StringUtils.right( "0000" + splitDate[0], 4) );	// 年
		dateString.append( "/" );
		dateString.append( StringUtils.right( "00" + splitDate[1], 2) );	// 月
		dateString.append( "/" );
		dateString.append( StringUtils.right( "00" + splitDate[2], 2) );	// 日

		if ( fullMode ) {
			dateString.append( " " );
			dateString.append( StringUtils.right( "00" + splitDate[3], 2) );	// 時
			dateString.append( ":" );
			dateString.append( StringUtils.right( "00" + splitDate[4], 2) );	// 分
			dateString.append( ":" );
			dateString.append( StringUtils.right( "00" + splitDate[5], 2) );	// 秒
			dateString.append( "." );
			dateString.append( StringUtils.right( "000" + splitDate[6], 3) );	// ミリ秒
		}

		return dateString.toString();
	}

	/**
	 * 時分文字列形式(標準の日時フォーマット)を作成する。
	 * @param splitHM 時、分が格納された配列
	 * @return 時分文字列形式(標準の時分フォーマット)
	 */
	private static String createHMString( String[] splitDate ) {

		StringBuilder dateString = new StringBuilder();

		dateString.append( StringUtils.right( "00" + splitDate[0], 2) );	// 時
		dateString.append( ":" );
		dateString.append( StringUtils.right( "00" + splitDate[1], 2) );	// 分

		return dateString.toString();
	}

	/**
	 * 指定された年月日文字列が正しい入力形式(yyyy/MM/dd)か、および正しい年月日かをチェックします。
	 * @param strYMD 年月日文字列
	 * @return 正しい入力形式、かつ正しい年月日であればtrue、そうでなければfalse
	 */
	public static boolean checkYMD( String strYMD ) {

		strYMD = strYMD.trim();
		String[] splitYMD = StringUtils.split( strYMD, "/" );

		// "/"で区切って3分割できなければ("/"が2つなければ)不正
		if ( splitYMD.length != 3 ) {
			return false;
		}

		int yyyy = 0;
		int mm = 0;
		int dd = 0;

		try {
			yyyy	= Integer.parseInt( splitYMD[0] );
			mm		= Integer.parseInt( splitYMD[1] );
			dd		= Integer.parseInt( splitYMD[2] );

		} catch ( NumberFormatException nfe ) {
			// 数字ではないので不正
			return false;
		}

		Calendar cal = Calendar.getInstance();
		cal.setLenient( false );
		cal.set( yyyy, mm - 1, dd );

		try {
			cal.getTime();
		} catch ( IllegalArgumentException iae ) {
			// 正しい年月日ではないので不正
			return false;
		}

		return true;
	}

	/**
	 * 指定された年月日文字列(yyyy/MM/dd)の年、月、日の部分をゼロ補完する。
	 * (例："2008/8/6" -> "2008/08/06")
	 * フォーマットが不正な場合は入力された年月日文字列をそのまま返す。
	 * @param strYMD 年月日文字列
	 * @return 補完された年月日文字列
	 */
	public static String fillYMD( String strYMD ) {

		if ( TriStringUtils.isEmpty( strYMD ) ) {
			return strYMD;
		}

		if ( !checkYMD( strYMD ) ) {
			return strYMD;
		}

		strYMD = strYMD.trim();
		String[] splitYMD = StringUtils.split( strYMD, "/" );

		return createDateString( splitYMD, false );
	}

	/**
	 * 文字列で表現される日付の比較 引数1と引数2を比較して1が後であればtrueを返却。
	 * @param beforeDate 前に設定されるべき日付
	 * @param afterDate  後に設定されるべき日付
	 * @return true:以前か同じ日 false:以後
	 */
	public static boolean before(String beforeDate, String afterDate) {
		boolean ret = false;
		try {
//			SimpleDateFormat sdf = getDefaultDateFormat();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
			Date before = sdf.parse(beforeDate);
			Date after = sdf.parse(afterDate);

			return before.before(after);
		} catch ( Exception e ) {
			return ret;
		}

	}

	/**
	 * 文字列で表現される日付の比較
	 * 引数1と引数2を比較して1が後であればtrueを返却。
	 * @param beforeDate 前に設定されるべき日付
	 * @param afterDate  後に設定されるべき日付
	 * @return true:以前か同じ日 false:以後,同じ日時分(イコール)はfalse
	 */
	public static boolean checkDateOrderToMinute(String beforeDate, String afterDate) {
		boolean ret = false;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm");
			Date before = sdf.parse(beforeDate);
			Date after = sdf.parse(afterDate);
			if (after.before(before))
				ret = true;
			return ret;
		} catch ( Exception e ) {
			return ret;
		}

	}

	/**
	 * 日付[YYYY/MM/DD] と時分[HH:MM:SS]を連結し、"YYYY/MM/DD HH:MM:SS.000" 形式に編集する
	 * @param date		日付[YYYY/MM/DD]文字列
	 * @param time		時分[HH:MM:SS]又は[HH:MM]文字列
	 * @return "YYYY/MM/DD HH:MM:SS.000" 形式文字列
	 */
	public static String convertDateFormat( String date , String time ) {

		final int TIME_LENGTH_HHMM_ONLY = 5 ;
		final String DATETIME_LABEL_SEC = ":00" ;
		final String DATETIME_LABEL_MSEC = ".000" ;

		if ( TriStringUtils.isEmpty(time) ) {
			time = "00:00";
		}

		String dateTime = null ;
		if( ! TriStringUtils.isEmpty( date ) ) {
			if( TIME_LENGTH_HHMM_ONLY >= time.length() ) {//時分が"HH:MM"表記
				dateTime = date + " " + time + DATETIME_LABEL_SEC + DATETIME_LABEL_MSEC ;
			} else {//時分が"HH:MM:SS"表記
				dateTime = date + " " + time + DATETIME_LABEL_MSEC ;
			}
		}
		return dateTime ;
	}

	/**
	 * 指定された時分文字列が正しい入力形式(hh:mm)か、および正しい時分かをチェックします。
	 * @param strHS 時分文字列
	 * @return 正しい入力形式、かつ正しい時分であればtrue、そうでなければfalse
	 */
	public static boolean checkHM( String strHM ) {

		strHM = strHM.trim();
		String[] splitHM = StringUtils.split( strHM, ":" );

		// ":"で区切って2分割できなければ(":"が1つなければ)不正
		if ( splitHM.length != 2 ) {
			return false;
		}

		int hh = 0;
		int mm = 0;

		try {
			hh		= Integer.parseInt( splitHM[0] );
			mm		= Integer.parseInt( splitHM[1] );

		} catch ( NumberFormatException nfe ) {
			// 数字ではないので不正
			return false;
		}

		if (hh < 0 || hh > 23)
			return false;
		if ( mm < 0 || mm > 59)
			return false;

		return true;
	}

	/**
	 * 指定された時分文字列(hh:mm)の時、分の部分をゼロ補完する。
	 * (例："3:4" -> "03:04")
	 * フォーマットが不正な場合は入力された時分文字列をそのまま返す。
	 * @param strHM 時分文字列
	 * @return 補完された時分文字列
	 */
	public static String fillHM( String strHM ) {

		if ( TriStringUtils.isEmpty( strHM ) ) {
			return strHM;
		}

		if ( !checkHM( strHM ) ) {
			return strHM;
		}

		strHM = strHM.trim();
		String[] splitHM = StringUtils.split( strHM, ":" );

		return createHMString( splitHM );
	}
	
	public static String[] mapDefaultDateTimeForTimer( String strDate ) {
		strDate = strDate.trim();
		String[] splitDate = StringUtils.split( strDate );
		String[] defaultDate = { "0000", "01", "01", "00", "00", "00", "000" };

		// 日付までの指定と判定("2008/08"、"2008/08/04"など)
		if ( splitDate.length == 1 ) {

			defaultDate = mapDefaultYMD( splitDate[0], defaultDate );

		// 日付以降も指定されていると判定("2008/08/04 12:00"など)
		} else if ( splitDate.length == 2 ) {

			defaultDate = mapDefaultYMD( splitDate[0], defaultDate );

			String[] splitHMS = StringUtils.split( splitDate[1], ":" );

			// 分までの指定と判定("2008/08/04 12:00"など)
			if ( splitHMS.length < 3 ) {

				defaultDate = mapDefaultHMS( splitHMS, defaultDate );

			// 秒以下まで指定されていると判定("2008/08/04 12:00:00"、"2008/08/04 12:00:00.000"など)
			} else if ( splitHMS.length == 3 ) {

				String[] splitS = StringUtils.split( splitHMS[2], "." );

				// 秒までの指定と判定("2008/08/04 12:00:00"など)
				if ( splitS.length < 2 ) {

					defaultDate = mapDefaultHMS( splitHMS, defaultDate );

				// ミリ秒までの指定と判定("2008/08/04 12:00:00.000"など)
				} else if ( splitS.length == 2 ) {

					defaultDate[ 3 ] = splitHMS[0];	// 時
					defaultDate[ 4 ] = splitHMS[1];	// 分
					defaultDate[ 5 ] = splitS[0];	// 秒
					defaultDate[ 6 ] = splitS[1];	// ミリ秒
				}
			}
		}
		
		return defaultDate;
	}

}
