package jp.co.blueship.tri.fw.cmn.utils.function;

/**
 * ある２つの入力に対して、任意の出力結果を返すFunctionインタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface MultiInputFunction<I, J, O> {

    O apply(I input1, J input2);

}
