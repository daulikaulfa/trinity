package jp.co.blueship.tri.fw.cmn.utils.expression;

import org.apache.commons.beanutils.expression.DefaultResolver;

import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;

/**
 * Trinity Property Name Expression Resolver Implementation.
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public class TriResolver extends DefaultResolver {

	private static final ILog log = TriLogFactory.getInstance();

	private static final char INDEXED_START = '[';
    private static final char INDEXED_END   = ']';

	@Override
	public String next(String expression) {
		String property = super.next(expression);

		if ( log.isDebugEnabled() ) {
			log.debug( "expression:= " + expression + " property:=" + property );
		}

		if ("class".equalsIgnoreCase(property)) {
			LogHandler.warn( log, TriLogMessage.LSM0040, expression, property );

			return "";
		}
		return property;
	}

	@Override
    public int getIndex(String expression) {
		try {
			int index = super.getIndex(expression);
			return index;
		} catch( IllegalArgumentException e ) {
			/*
			 * V2ではservletのrequest parameterの名前に"group[]"のような"[]"がついた名前を使用しているため
			 * BeanUtilsが内部で誤認識してIllegalArgumentExceptionを発生させてしまう。
			 *
			 */
	        if (expression == null || expression.length() == 0) {
	        } else {
	        	int start = expression.indexOf(INDEXED_START);
	        	if ( 0 <= start ) {
	                int end = expression.indexOf(INDEXED_END, start);
	                if ( 0 <= end ) {
	                	String value = expression.substring(start + 1, end);
		                if (value.length() == 0) {
		                    return -1;
		                }
	                }
	        	}
	        }

			throw e;
		}

	}

	@Override
    public boolean isIndexed(String expression) {
    	boolean isIndexed = super.isIndexed(expression);

    	if ( isIndexed ) {
			/*
			 * V2ではservletのrequest parameterの名前に"group[]"のような"[]"がついた名前を使用しているため
			 * BeanUtilsが内部で誤認識してIllegalArgumentExceptionを発生させてしまう。
			 * BeanUtilsでは"[]"がついた名前はerrorとなるため、"[]"指定はIndexと判断しないようにする。
			 *
			 */
    		int start = expression.indexOf(INDEXED_START);

    		if ( 0 <= start ) {
                int end = expression.indexOf(INDEXED_END, start);
                if ( 0 <= end ) {
                	String value = expression.substring(start + 1, end);
	                if (value.length() == 0) {
	                    return false;
	                }
                }
        	}
    	}

    	return isIndexed;
    }
}
