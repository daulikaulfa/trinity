package jp.co.blueship.tri.fw.cmn.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;


/**
 * 暗号化／復号の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008-2009
 */
public class EncryptionUtils {

	/**
	 * trinityレルム名
	 */
	private static final String REALM = "trinityRealm";

	/**
	 * SHA-512で暗号化したハッシュ文字列を返します。
	 * @param word 暗号化対象文字列
	 * @return
	 */
	public static String getHashStringEncodedSha512( String word ) {
		return getHash( word, "SHA-512" );
	}


	/**
	 * 指定されたログインユーザID、レルム名、パスワードを連結後(ログインユーザID:レルム名:パスワード)、MD5で暗号化して返します。
	 *
	 * @param userID ログインユーザID
	 * @param password パスワード
	 * @return 「ログインユーザID:レルム名:パスワード」形式をMD5で暗号化された文字列。userIDまたはpasswordが未指定の場合は空文字を返す。
	 */
	public static String encodePasswordInA1Format(String userID, String password) {

		if(TriStringUtils.isEmpty(userID) || TriStringUtils.isEmpty(password)) {
			return "";
		}

		String word = userID + ":" + REALM + ":" + password;
		return getHash( word, "MD5" );
    }

	private static String getHash( String word, String algorithm ) {

		MessageDigest md= null;
        try {
            md= MessageDigest.getInstance( algorithm );
        }
        catch( NoSuchAlgorithmException e ){
            return null;
        }

        md.reset();
        md.update( word.getBytes() );
        byte[] hash= md.digest();


        StringBuilder sb = new StringBuilder();
		for ( byte b : hash ) {
			String hex = String.format("%02x", b);
			sb.append(hex);
		}
		return sb.toString();
	}
}
