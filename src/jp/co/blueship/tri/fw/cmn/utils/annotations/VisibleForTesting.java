/**
 *
 */
package jp.co.blueship.tri.fw.cmn.utils.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * テストをしやすくするためにアクセス修飾子のレベルを緩くしている(テストのためにprivateをパッケージプライベートに変更など)ということを<br/>
 * 明示するために付与するためのアノテーションです。<br/>
 * メソッドやフィールドに付与することができます。
 *
 * @author Takayuki Kubo
 *
 */
@Retention(RetentionPolicy.CLASS)
@Target({ ElementType.TYPE, ElementType.METHOD, ElementType.CONSTRUCTOR, ElementType.FIELD })
@Documented
public @interface VisibleForTesting {

}
