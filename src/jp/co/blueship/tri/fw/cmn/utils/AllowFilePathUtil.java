package jp.co.blueship.tri.fw.cmn.utils;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import jp.co.blueship.tri.fw.dao.oxm.AllowFilePathSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * 定義リソースから値を取得するClass
 *
 */
public class AllowFilePathUtil {

	private static final ILog log = TriLogFactory.getInstance();

	private static final IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
	private static AllowFilePathSheet allowFilePathSheet = null;

	private static boolean isInit = false;

	/**
	 * 許容ファイルパス設定シートを取得します。
	 *
	 * @return allowFilePathSheet 許容ファイルパス設定シート
	 */
	protected static AllowFilePathSheet getAllowFilePathSheet() {
		if ( ! isInit )
			init();

			return allowFilePathSheet ;
	}

	/**
	 * 初期化を行います。
	 */
	public static void init() {
		if ( isInit )
			return;

		isInit = true;

		allowFilePathSheet = (AllowFilePathSheet)ac.getBean("allowFilePath");

	}

	/**
	 *
	 * @param input
	 * @return
	 */
	private static final boolean evalDenyRegexp( String input ) {
		List<String> denyRegexpList = getDenyRegexp();

		// 空の場合は処理しない
		if(! denyRegexpList.isEmpty() ) {
			List<String> result = matches(input, denyRegexpList);
			if(null != result) {
				LogHandler.warn( log , TriLogMessage.LSM0018 , input , result.toString() ) ;
				return true;
			}
		}
		return false ;
	}

	/**
	 *
	 * @param input
	 * @return
	 */
	private static final boolean evalDenyRange( String input ) {
		List<Character> denyRangeList = getDenyRange();

		// 空の場合は処理しない
		if(! denyRangeList.isEmpty() ) {
			List<Character> result = contains(input, denyRangeList);
			if(null != result) {
				LogHandler.warn( log , TriLogMessage.LSM0019 , input , result.toString() );
				return true;
			}
		}
		return false ;
	}

	/**
	 *
	 * @param input
	 * @return
	 */
	private static final boolean evalAllowRegexp( String input ) {
		List<String> allowRegexpList = getAllowRegexp();

		// 空の場合は処理しない
		if(! allowRegexpList.isEmpty() ) {
			List<String> result = matches(input, allowRegexpList);
			if(null == result) {
				LogHandler.warn( log , TriLogMessage.LSM0020 , input , allowRegexpList.toString() );
				return true;
			}
		}
		return false ;
	}

	/**
	 *
	 * @param input
	 * @return
	 */
	private static final boolean evalAllowRange( String input ) {
		List<Character> allowRangeList = getAllowRange();

		// 空の場合は処理しない
		if(! allowRangeList.isEmpty() ) {
			List<Character> result = contains(input, allowRangeList);
			if(null != result) {
				LogHandler.warn( log , TriLogMessage.LSM0021 , input , result.toString() );
				return true;
			}
		}
		return false ;
	}

	/**
	 *
	 *
	 * @param input
	 * @return
	 */
	public static final boolean evalSequencial(String input) {

		boolean deny = false ;

		deny |= evalDenyRegexp( input ) ;
		deny |= evalDenyRange( input ) ;
		deny |= evalAllowRegexp( input ) ;
		deny |= evalAllowRange( input ) ;

		boolean allow = !deny;

		return allow;
	}


	/**
	 * 型名に該当するファイルパス許可情報のリストを取得します。
	 *
	 * @return ファイルパス許可情報のリスト
	 */
	public static final List<Character> getAllowRange() {

		List<Character> list = getAllowFilePathSheet().getAllowRangeList();

		if ( null == list ) {
			throw new TriSystemException( SmMessageId.SM005147S );
		}

		return list;
	}

	/**
	 * 型名に該当するファイルパス許可情報のリストを取得します。
	 *
	 * @return ファイルパス許可情報のリスト
	 */
	public static final List<String> getAllowRegexp() {

		List<String> list = getAllowFilePathSheet().getAllowRegexpList();

		if ( null == list ) {
			throw new TriSystemException( SmMessageId.SM005147S );
		}

		return list;
	}

	/**
	 * 型名に該当するファイルパス禁止情報のリストを取得します。
	 *
	 * @return ファイルパス禁止情報のリスト
	 */
	public static final List<Character> getDenyRange() {

		List<Character> list = getAllowFilePathSheet().getDenyRangeList();

		if ( null == list ) {
			throw new TriSystemException( SmMessageId.SM005147S );
		}

		return list;
	}

	/**
	 * 型名に該当するファイルパス禁止情報のリストを取得します。
	 *
	 * @return ファイルパス禁止情報のリスト
	 */
	public static final List<String> getDenyRegexp() {

		List<String> list = getAllowFilePathSheet().getDenyRegexpList();

		if ( null == list ) {
			throw new TriSystemException( SmMessageId.SM005147S );
		}

		return list;
	}

	/**
	 * <code>input</code>が指定された文字のみで構成されているか。
	 *
	 * @param input 検索対象の文字列
	 * @param keyList 検索キーとなる文字のリスト
	 * @return 指定文字以外の文字が含まれている場合は当該文字のリストを返します。指定文字のみで構成されている場合はnullを返します。
	 */
	public static final List<Character> consists(String input, List<Character> keyList) {

		List<Character> tmp = AllowFilePathUtil.asList(input.toCharArray());
		List<Character> result = new ArrayList<Character>(tmp);

		for (Character ch : input.toCharArray()) {
			boolean contains = keyList.contains(ch);
			if( contains ) result.remove(ch);
		}

		return result.isEmpty() ? null : result;
	}

	//	 List adapter for primitive char array
	private static List<Character> asList(final char[] a) {
	    return new AbstractList<Character>() {
	        public Character get(int i) { return a[i]; }
	        // Throws NullPointerException if val == null
	        public Character set(int i, Character val) {
	        	Character oldVal = a[i];
	            a[i] = val;
	            return oldVal;
	        }
	        public int size() { return a.length; }
	    };
	}

	/**
	 * <code>input</code>が指定された文字を含むか。
	 *
	 * @param input 検索対象の文字列
	 * @param keyList 検索キーとなる文字のリスト
	 * @return 含まれる場合は該当した文字のリストを返します。１件も含まれなかった場合はnullを返します。
	 */
	public static final List<Character> contains(String input, List<Character> keyList) {

		List<Character> result = new ArrayList<Character>();

		for (Character key : keyList) {
			boolean contains = contains(input, key);
			if( contains ) result.add(key);
		}

		return result.isEmpty() ? null : result;
	}

	/**
	 * <code>input</code>が指定された正規表現パターンと一致するか。
	 *
	 * @param input 検索対象の文字列
	 * @param patternList 正規表現パターンのリスト
	 * @return 一致する場合は一致したパターンのリストを返します。１件も一致しなかった場合はnullを返します。
	 */
	public static final List<String> matches(String input, List<String> patternList) {

		List<String> result = new ArrayList<String>();

		for (String pattern : patternList) {
			Pattern p = Pattern.compile(pattern);

			boolean matches = matches(input, p);
			if( matches ) result.add(pattern);
		}

		return result.isEmpty() ? null : result;
	}

	/**
	 * <code>input</code>が指定された文字<code>key</code>を含むか。
	 *
	 * @param input 検索対象の文字列
	 * @param key 検索キーとなる文字
	 * @return 含む場合true
	 */
	public static final boolean contains(String input, Character key) {

		char[] sotedInput = input.toCharArray();
		Arrays.sort(sotedInput);

		int i = Arrays.binarySearch(sotedInput, key);
		return i >= 0;
	}

	/**
	 * <code>input</code>がコンパイルされたPattern<code>p</code>と一致するか。
	 *
	 * @param input 検索対象の文字列
	 * @param p コンパイルされた正規表現パターンオブジェクト
	 * @return 一致する場合true
	 */
	public static final boolean matches(String input, Pattern p) {

		return p.matcher(input).matches();
	}

}
