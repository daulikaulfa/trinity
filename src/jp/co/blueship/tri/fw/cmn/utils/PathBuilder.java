package jp.co.blueship.tri.fw.cmn.utils;

import org.apache.commons.lang.StringUtils;

/**
 * 文字列形式のファイルパスを構築するビルダ
 *
 * @author Takayuki Kubo
 *
 */
public class PathBuilder {


	private String linkedPath;

	private PathBuilder(String sourcePath) {
		this.linkedPath = sourcePath;
	}

	/**
	 * ファイルパスを指定してPathビルダを取得します。
	 *
	 * @param filePath ファイルパス
	 * @return Pathビルダ
	 */
	public static PathBuilder from(String filePath) {
		return new PathBuilder(filePath);
	}

	/**
	 * 現在のパスに指定されたパスを後ろに連結します。
	 *
	 * @param path 連結するパス
	 * @return Pathビルダ
	 */
	public PathBuilder with(String path) {
		linkedPath = TriStringUtils.linkPath( linkedPath , path );
		return this;
	}

	/**
	 * ファイルパスを文字列形式で返します。
	 *
	 * @return ファイルパス文字列
	 */
	public String buildBySlash() {
		return StringUtils.replace( linkedPath , "\\" , "/" );
	}

}
