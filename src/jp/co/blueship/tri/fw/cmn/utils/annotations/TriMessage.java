/**
 *
 */
package jp.co.blueship.tri.fw.cmn.utils.annotations;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.DcmMessageId;
import jp.co.blueship.tri.fw.msg.DmMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * BeanValidation実行時に発生するバリデーションエラーに対するエラーメッセージを定義するためのアノテーションクラスです。<br/>
 * BeanValidationを実行するクラスのフィールドに付与して使用します。<br/>
 * 通常、BeanValidationの制約アノテーション("@NotNull"など)と一緒に使用されます。
 *
 * @author Takayuki Kubo
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD, ElementType.METHOD })
@Documented
public @interface TriMessage {

	AmMessageId amMessageID() default AmMessageId.DEFAULT;

	BmMessageId bmMessageID() default BmMessageId.DEFAULT;;

	RmMessageId rmMessageID() default RmMessageId.DEFAULT;

	SmMessageId smMessageID() default SmMessageId.DEFAULT;

	DmMessageId dmMessageID() default DmMessageId.DEFAULT;

	DcmMessageId dcmMessageID() default DcmMessageId.DEFAULT;

	MessageParameter[] args() default {};
}
