package jp.co.blueship.tri.fw.cmn.utils.spec;

/**
 * 仕様を表現する抽象クラスです。
 *
 * @author Takayuki Kubo
 *
 */
public abstract class BaseSpecification<T> implements Specification<T> {

	@Override
	public boolean evalute(T input) {
		return isSpecified(input);
	}

	public Specification<T> or(Specification<T> specification) {
		return SpecificationUtils.or(this, specification);
	}

	public Specification<T> and(Specification<T> specification) {
		return SpecificationUtils.and(this, specification);
	}
}
