package jp.co.blueship.tri.fw.cmn.utils.spec;

import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;

/**
 * 仕様を表現するためのインタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface Specification<T> extends TriPredicate<T> {

	/**
	 * 指定された入力値が仕様を満たしているかどうかを返します。
	 *
	 * @param input 入力値
	 * @return 仕様を満たしている場合はtrue
	 */
	public boolean isSpecified(T input);

}
