package jp.co.blueship.tri.fw.cmn.utils;

public class PreConditions {

	/**
	 * 指定された条件がfalseの場合、AssertionErrorを送出します。<br/>
	 * メソッドが正しく動作するための前提条件をチェックするために、メソッドの先頭で使用します。
	 *
	 * @param condition 条件
	 * @param errDetail エラー発生時の詳細メッセージ
	 */
	public static void assertOf(boolean condition, String errDetail) {

		if(!condition) {
			throw new AssertionError(errDetail);
		}
	}

	/**
	 * 指定された条件がtrueの場合、AssertionErrorを送出します。<br/>
	 * メソッドが正しく動作するための前提条件をチェックするために、メソッドの先頭で使用します。
	 *
	 * @param condition 条件
	 * @param errDetail エラー発生時の詳細メッセージ
	 */
	public static void assertFalse(boolean condition, String errDetail) {

		if(condition) {
			throw new AssertionError(errDetail);
		}
	}

}
