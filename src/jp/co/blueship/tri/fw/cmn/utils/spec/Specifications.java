package jp.co.blueship.tri.fw.cmn.utils.spec;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;

/**
 * 汎用的な仕様を表現した仕様クラスの集まりです。
 *
 * @author Takayuki Kubo
 *
 */
public class Specifications {

	public static final BaseSpecification<List<?>> NOT_EMPTY_LIST = new BaseSpecification<List<?>>() {

		@Override
		public boolean isSpecified(List<?> input) {
			return TriCollectionUtils.isNotEmpty(input);
		}
	};

	public static final BaseSpecification<List<?>> HAS_SINGLE_ELEMENT = new BaseSpecification<List<?>>() {

		@Override
		public boolean isSpecified(List<?> input) {
			return TriCollectionUtils.isNotEmpty(input) && input.size() == 1;
		}
	};

}
