package jp.co.blueship.tri.fw.cmn.utils;

import jp.co.blueship.tri.fw.dao.orm.IAssetCounterInfo;

public class AssetCounterInfo implements IAssetCounterInfo {

	/**
	 * 修正後、インクリメントすること
	 */
	private static final long serialVersionUID = 1L;

	private Integer lendCount		= null;
	private Integer addCount		= null;
	private Integer changeCount	= null;
	private Integer delCount		= null;

	public Integer getLendCount() {
		return this.lendCount;
	}
	public void setLendCount( Integer lendCount ) {
		this.lendCount = lendCount;
	}

	public Integer getAddCount() {
		return this.addCount;
	}
	public void setAddCount( Integer addCount ) {
		this.addCount = addCount;
	}

	public Integer getChangeCount() {
		return this.changeCount;
	}
	public void setChangeCount( Integer changeCount ) {
		this.changeCount = changeCount;
	}

	public Integer getDelCount() {
		return this.delCount;
	}
	public void setDelCount( Integer delCount ) {
		this.delCount = delCount;
	}

}
