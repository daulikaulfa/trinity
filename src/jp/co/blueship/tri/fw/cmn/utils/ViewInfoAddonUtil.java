package jp.co.blueship.tri.fw.cmn.utils;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * 画面表示情報の共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */

public class ViewInfoAddonUtil {

	/**
	 * ロット情報エンティティからロット番号の配列を取得する
	 *
	 * @param lotList
	 * @return
	 */
	public static Map<String, ILotDto> convertToMap( List<ILotDto> pjtLotEntityArray ) {
		Map<String, ILotDto> lotMap = new LinkedHashMap<String, ILotDto>();

		for ( ILotDto lot : pjtLotEntityArray ) {
			lotMap.put(lot.getLotEntity().getLotId(), lot);
		}

		return lotMap;
	}

	/**
	 * アクセス可能なロットを取得する
	 * @param lotDto ロットエンティティ
	 * @param groupUserEntitys グループエンティティ
	 * @param lotList アクセス可能なロットエンティティ
	 * @param disableLotNoList アクセス不可能だが名前だけ見えるロット番号
	 */
	public static void setAccessablePjtLotEntity(
			List<ILotDto> lotDto, List<IGrpUserLnkEntity> groupUserEntitys,
			List<ILotEntity> lotList, List<String> disableLotNoList ) {

		Set<String> groupIdSet = EntityAddonUtils.getGroupIdSet( groupUserEntitys );


		for ( ILotDto lot : lotDto ) {

			// グループの設定なし＝全グループがアクセス可能
			if ( null == lot.getLotGrpLnkEntities() || 0 == lot.getLotGrpLnkEntities().size() ) {
				lotList.add( lot.getLotEntity() );
			} else {

				boolean listView = false;

				for ( ILotGrpLnkEntity group : lot.getLotGrpLnkEntities() ) {

					if ( groupIdSet.contains( group.getGrpId() )) {
						lotList.add( lot.getLotEntity() );
						listView = true;
						break;
					}
				}

				if ( !listView && null != disableLotNoList ) {
					if ( lot.getLotEntity().getIsAssetDup().parseBoolean() ) {
						lotList.add( lot.getLotEntity() );
						disableLotNoList.add( lot.getLotEntity().getLotId() );
					}
				}
			}
		}
	}
}
