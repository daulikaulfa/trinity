package jp.co.blueship.tri.fw.cmn.utils;

import org.apache.commons.lang.StringUtils;

/**
 * システムプロパティを取得するための列挙型です。
 *
 * @author Yukihiro Eguchi
 */
public enum SystemProps {
	/**
	 * Java Runtime Environment のバージョン
	 */
	JavaVersion("java.version"),
	/**
	 * Java Runtime Environment のベンダー
	 */
	JavaVendor("java.vendor"),
	/**
	 * Java ベンダーの URL
	 */
	JavaVendorUrl("java.vendor.url"),
	/**
	 * Java のインストール先ディレクトリ
	 */
	JavaHome("java.home"),
	/**
	 * Java Virtual Machine の仕様バージョン
	 */
	JavaVmSpecificationVersion("java.vm.specification.version"),
	/**
	 *
	 */
	JavaVmSpecificationVendor("java.vm.specification.vendor"),
	/**
	 * Java Virtual Machine の仕様名
	 */
	JavaVmSpecificationName("java.vm.specification.name"),
	/**
	 * Java Virtual Machine の実装バージョン
	 */
	JavaVmVersion("java.vm.version"),
	/**
	 * Java Virtual Machine の実装のベンダー
	 */
	JavaVmVendor("java.vm.vendor"),
	/**
	 * Java Virtual Machine の実装名
	 */
	JavaVmName("java.vm.name"),
	/**
	 * Java Runtime Environment の仕様バージョン
	 */
	JavaSpecificationVersion("java.specification.version"),
	/**
	 * Java Runtime Environment の仕様のベンダー
	 */
	JavaSpecificationVendor("java.specification.vendor"),
	/**
	 * Java Runtime Environment の仕様名
	 */
	JavaSpecificationName("java.specification.name"),
	/**
	 * Java クラスの形式のバージョン番号
	 */
	JavaClassVersion("java.class.version"),
	/**
	 * Java クラスパス
	 */
	JavaClassPath("java.class.path"),
	/**
	 * ライブラリのロード時に検索するパスのリスト
	 */
	JavaLibraryPath("java.library.path"),
	/**
	 * デフォルト一時ファイルのパス
	 */
	JavaIoTmpdir("java.io.tmpdir"),
	/**
	 * 使用する JIT コンパイラの名前
	 */
	JavaCompiler("java.compiler"),
	/**
	 * 拡張ディレクトリのパス
	 */
	JavaExtDirs("java.ext.dirs"),
	/**
	 * オペレーティングシステム名
	 */
	OsName("os.name"),
	/**
	 * オペレーティングシステムのアーキテクチャ
	 */
	OsArch("os.arch"),
	/**
	 * オペレーティングシステムのバージョン
	 */
	OsVersion("os.version"),
	/**
	 * ファイル区切り文字 (UNIX では "/")
	 */
	FileSeparator("file.separator"),
	/**
	 * パス区切り文字 (UNIX では ":")
	 */
	PathSeparator("path.separator"),
	/**
	 * 行区切り文字 (UNIX では "\n")
	 */
	LineSeparator("line.separator"),
	/**
	 * ユーザのアカウント名
	 */
	UserName("user.name"),
	/**
	 * ユーザのホームディレクトリ
	 */
	UserHome("user.home"),
	/**
	 * ユーザの現在の作業ディレクトリ
	 */
	UserDir("user.dir"),
	/**
	 * ロケール切り替えのための言語設定
	 */
	TriSystemLanguage("tri.system.language", "ja");

	private static SystemProps defaultEnum = null;
	private String value = null;
	private String prop = null;

	private SystemProps(String value) {
		this.value = value;
		this.prop = System.getProperty(value);
	}

	private SystemProps(String value, String defaultValue) {
		this.value = value;
		this.prop = TriObjectUtils.defaultIfNull(System.getProperty(value), defaultValue);
	}

	/**
	 * キーによって示されるシステムプロパティを取得します。
	 *
	 * @param isCache VM起動時のキャッシュ値の場合、true。毎回システムプロパティから取得する場合はFalse
	 * @return システムプロパティの文字列値。そのキーにプロパティがない場合はデフォルト値
	 */
	public String getProperty(boolean isCache) {
		return (isCache) ? this.prop : System.getProperty(value);
	}

	/**
	 * キーによって示されるシステムプロパティを取得します。
	 *
	 * @return システムプロパティの文字列値。そのキーにプロパティがない場合はデフォルト値
	 */
	public String getProperty() {
		return getProperty(true);
	}

	/**
	 * システムプロパティのキー値を取得します。
	 *
	 * @return システムプロパティのキー値
	 */
	public String value() {
		return this.value;
	}

	/**
	 * 指定されたシステムプロパティのキー値に対応する列挙型を取得します。
	 *
	 * @param value システムプロパティのキー値
	 * @return 対応する列挙型
	 */
	public static SystemProps value(String value) {
		if (StringUtils.isBlank(value)) {
			return defaultEnum;
		}

		for (SystemProps prop : values()) {
			if (prop.value().equals(value)) {
				return prop;
			}
		}

		return defaultEnum;
	}

}
