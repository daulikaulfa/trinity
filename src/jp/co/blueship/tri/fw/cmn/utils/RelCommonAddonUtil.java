package jp.co.blueship.tri.fw.cmn.utils;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;

/**
 * 変更管理を扱うユーティリティークラスです。
 * <br>返却申請の資産格納パスを取得など、各AddOn共通の機能が定義されています。
 *
 * @author Yukihiro Eguchi
 *
 */
public class RelCommonAddonUtil {

	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>アクセス許可グループが、グループテーブルに存在するかのチェック</p>
	 * <p>自分が所属するグループが、アクセス許可グループであるかのチェック</p>
	 * @param lotDto チェック対象のロット
	 * @param grpDao IGroupDao
	 * @param groupUsers 自分が所属するグループリスト
	 * @throws TriSystemException
	 */
	public static final void checkAccessableGroup(
			ILotDto lotDto, IGrpDao grpDao, List<IGrpUserLnkEntity> groupUsers ) throws TriSystemException {

		List<ILotGrpLnkEntity> grpEntityArray = lotDto.getLotGrpLnkEntities() ;
		if ( TriStringUtils.isEmpty( grpEntityArray )) {
			return;
		}


		Set<String> myGroupIdSet = new HashSet<String>();
		for ( IGrpUserLnkEntity group : groupUsers ) {
			myGroupIdSet.add( group.getGrpId() );
		}


		boolean access = false;

		// グループの存在チェック
		for( ILotGrpLnkEntity grpEntity : grpEntityArray ) {

			// 自分の所属グループのみ存在チェック
			if ( myGroupIdSet.contains( grpEntity.getGrpId() )) {

				GrpCondition condition = new GrpCondition();
				condition.setGrpId( grpEntity.getGrpId() );
				IGrpEntity gEntity = grpDao.findByPrimaryKey( condition.getCondition() ) ;
				if( null == gEntity ) {
					throw new TriSystemException(SmMessageId.SM004169F , grpEntity.getGrpId() , lotDto.getGrpEntity( grpEntity.getGrpId() ).getGrpNm() );
				}

				access = true;
			}
		}

		if ( !access ) {
			throw new TriSystemException(SmMessageId.SM004170F);
		}
	}

	/**
	 * ロットに対するアクセス許可グループに対して、以下のチェックを行う。
	 * <p>アクセス許可グループが、グループテーブルに存在するかのチェック</p>
	 * <p>自分が所属するグループが、アクセス許可グループであるかのチェック</p>
	 * @param pjtLotEntities チェック対象のロットの配列
	 * @param grpDao IGrpDao
	 * @param groupUsers 自分が所属するグループリスト
	 * @throws TriSystemException
	 */
	public static final void checkAccessableGroup(
			List<ILotDto> pjtLotEntities, IGrpDao grpDao, List<IGrpUserLnkEntity> groupUsers ) throws TriSystemException {
		if ( TriStringUtils.isEmpty(pjtLotEntities) ) {
			return;
		}

		for ( ILotDto pjtLotEntity : pjtLotEntities ) {
			checkAccessableGroup( pjtLotEntity, grpDao, groupUsers );
		}
	}
}
