package jp.co.blueship.tri.fw.cmn.utils;

/**
 * ある一組のペアとなるデータ構造を表現するクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public class TriPair<T, V> {

	private T husband;
	private V wife;

	public TriPair() {
	}

	/**
	 * 一組の値を指定してペアオブジェクトを生成します。
	 *
	 * @param husband ペア値
	 * @param wife もう一方のペア値
	 */
	public TriPair(T husband, V wife) {
		this.husband = husband;
		this.wife = wife;
	}

	public T getHasband() {
		return husband;
	}

	public V getWife() {
		return wife;
	}

	/**
	 * このオブジェクトのペア値が設定されているかどうかを返す。
	 *
	 * @return 設定されている場合はtrueを返す。
	 */
	public boolean isPresent() {
		return husband != null && wife != null;
	}

}
