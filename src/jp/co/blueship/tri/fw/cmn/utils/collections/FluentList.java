package jp.co.blueship.tri.fw.cmn.utils.collections;

import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.monoid.Monoid;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;

/**
 * @version V3L10.02
 *
 * @version SP-20150622_V3L13R01
 * @author Yusna Marlina
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class FluentList<T> {

	private LinkedList<T> coreList = new LinkedList<T>();

	/**
	 * コンストラクタ。
	 *
	 * @param source 生成元リスト
	 */
	private FluentList(Collection<T> source) {
		if ( null != source ) {
			coreList.addAll(source);
		}
	}

	/**
	 * 生成元リストを指定してFluentListを生成する。
	 *
	 * @param source 生成元リスト
	 * @return FluentList
	 */
	public static <T> FluentList<T> from(List<T> source) {
		return new FluentList<T>(source);
	}

	/**
	 * 生成元コレクションを指定してFluentListを生成する。
	 *
	 * @param source 生成元コレクション
	 * @return FluentList
	 */
	public static <T> FluentList<T> from(Collection<T> source) {
		return new FluentList<T>(source);
	}

	/**
	 * 生成元リストを指定してFluentListを生成する。
	 *
	 * @param source 生成元リスト
	 * @return FluentList
	 */
	public static <T> FluentList<T> from(Iterable<T> source) {

		List<T> list = new LinkedList<T>();
		if ( null != source ) {
			for (T t : source) {
				list.add(t);
			}
		}

		return from(list);
	}

	/**
	 * 生成元の配列を指定してFluentListを生成する。
	 *
	 * @param source 生成元配列
	 * @return FluentList
	 */
	public static <T> FluentList<T> from(T[] source) {

		List<T> list = new LinkedList<T>();
		if ( null != source ) {
			for (T t : source) {
				list.add(t);
			}
		}

		return from(list);
	}

	/**
	 * リストに変換して返す。
	 *
	 * @return 変換後リスト
	 */
	public List<T> asList() {
		return coreList;
	}

	/**
	 * 配列に変換して返す。
	 *
	 * @return 変換後リスト
	 */
	public T[] toArray(T[] a) {
		return (T[])coreList.toArray(a);
    }

	/**
	 * 述語にマッチした要素のみを抽出する。
	 *
	 * @param predicate 述語を表すPrdicateオブジェクト
	 * @return 抽出後のリスト
	 */
	public FluentList<T> select(TriPredicate<T> predicate) {
		return from(TriCollectionUtils.select(coreList, predicate));
	}

	/**
	 * 述語にマッチした要素を除外する。
	 *
	 * @param predicate 述語を表すPrdicateオブジェクト
	 * @return 除外後のリスト
	 */
	public FluentList<T> reject(TriPredicate<T> predicate) {
		return from(TriCollectionUtils.selectRejected(coreList, predicate));
	}

	/**
	 * 指定された関数に従って、各要素の写像を生成する。
	 *
	 * @param function 写像生成関数
	 * @return 写像リスト
	 */
	public <O> FluentList<O> map(TriFunction<T, O> function) {
		return from(TriCollectionUtils.collect(coreList, function));
	}

	/**
	 * 指定されたコンパレータに従ってソートする。
	 *
	 * @param comparator コンパレータ
	 * @return ソート後のリスト
	 */
	public FluentList<T> sort(Comparator<T> comparator) {
		Collections.sort(coreList, comparator);
		return from(coreList);
	}

	/**
	 * 先頭の要素を除外し、新しいリストを生成する。
	 *
	 * @return 先頭要素除外後のリスト
	 */
	public FluentList<T> rejectFirst() {

		LinkedList<T> list = new LinkedList<T>(coreList);
		list.removeFirst();

		return from(list);
	}

	/**
	 * 指定されたモノイドに従ってリストの内容を集計する。
	 *
	 * @param monoid モノイド
	 * @return 集計結果
	 */
	public T sum(Monoid<T> monoid) {

		T result = monoid.empty();

		for (T elm : coreList) {
			result = monoid.append(result, defaultIfNull(monoid, elm));
		}

		return result;
	}

	/**
	 * 各要素に対して、指定したクロージャを適用する。
	 *
	 * @param clojure クロージャ
	 */
	public void forEach(TriClojure<T> clojure) {
		TriCollectionUtils.forEach(coreList, clojure);
	}

	/**
	 * 先頭の要素を返す。
	 *
	 * @return 先頭の要素。このリストが空の場合はnull。
	 */
	public T atFirst() {

		if (TriCollectionUtils.isEmpty(coreList)) {
			return null;
		}

		return coreList.get(0);
	}

	/**
	 * 指定された述語にマッチする要素のうちに最初の1件目を返す。
	 *
	 * @param predicate 述語オブジェクト
	 * @return 述語にマッチする1件目の要素。ない場合はnull。
	 */
	public T atFirstOf(TriPredicate<T> predicate) {

		if (TriCollectionUtils.isEmpty(coreList)) {
			return null;
		}

		for (T element : coreList) {
			if (predicate.evalute(element)) {
				return element;
			}
		}

		return null;
	}

	/**
	 * 指定された述語にマッチする要素を1件でも含むかどうかを返す。
	 *
	 * @param predicate 述語オブジェクト
	 * @return 含む場合はtrue
	 */
	public boolean contains(TriPredicate<T> predicate) {

		if (atFirstOf(predicate) != null) {
			return true;
		}

		return false;
	}

	/**
	 * このリストが空かどうかを返す。
	 *
	 * @return 空である場合はtrue。
	 */
	public boolean isEmpty() {
		return TriCollectionUtils.isEmpty(coreList);
	}

	/**
	 * 先頭から数えて指定された数の要素リストを返す。
	 *
	 * @param limit 要素数
	 * @return 指定された数の要素を保持した新しいリスト
	 */
	public FluentList<T> limit(int limit) {
		return from(coreList.subList(0, coreList.size() < limit ? coreList.size() : limit));
	}



	private static <T> T defaultIfNull(Monoid<T> monoid, T input) {

		if (input == null) {
			return monoid.empty();
		}

		return input;
	}

}
