package jp.co.blueship.tri.fw.cmn.utils.function;

/**
 * ある入力に対して、任意の出力結果を返すFunctionインタフェースです。
 *
 * @author Takayuki Kubo
 *
 */
public interface TriFunction<I, O> {

    O apply(I input);

}
