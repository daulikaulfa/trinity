package jp.co.blueship.tri.fw.cmn.utils.predicate;

public abstract class BasePredicate<T> implements TriPredicate<T> {

	public TriPredicate<T> or(TriPredicate<T> predicate) {
		return TriPredicateUtils.or(this, predicate);
	}

	public TriPredicate<T> and(TriPredicate<T> predicate) {
		return TriPredicateUtils.and(this, predicate);
	}
}
