package jp.co.blueship.tri.fw.cmn.utils;

import org.apache.commons.lang.SystemUtils;

/**
 * org.apache.commons.lang.SystemUtilsのラッパークラスです。
 *
 */
public class TriSystemUtils {
	

	/**
	 * オペレーティングシステムがWindowsかどうかを判定します。
	 */
	public static final boolean isWindows() {
		return SystemUtils.IS_OS_WINDOWS;
	}

}
