package jp.co.blueship.tri.fw.cmn.utils.collections;

import java.util.AbstractSet;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;

/**
 * 重複要素の無いコレクションです。追加する要素がすでにこのコレクションに存在するかどうかを判断するComparatorを指定することが可能です。
 *
 *
 * @author Takayuki Kubo
 *
 */
public class ComparableSet<E> extends AbstractSet<E> {

	private List<E> delegatedList = new ArrayList<E>();
	private Comparator<E> comparator;

	/**
	 * コンパレータを指定して空のSetを生成するコンストラクタです。
	 *
	 * @param comparator 重複要素を判断するためのコンパレータ
	 */
	public ComparableSet(Comparator<E> comparator) {
		PreConditions.assertOf(comparator != null, "Comparator is not specified.");
		this.comparator = comparator;
	}

	/**
	 * このSetのイテレータオブジェクトを返します。
	 *
	 * @return イテレータオブジェクト
	 *
	 * @see java.util.AbstractCollection#iterator()
	 */
	@Override
	public Iterator<E> iterator() {
		return delegatedList.iterator();
	}

	/**
	 * このSetの要素数を返します。
	 *
	 * @return 要素数
	 *
	 * @see java.util.AbstractCollection#size()
	 */
	@Override
	public int size() {
		return delegatedList.size();
	}

	/**
	 * コンパレータを使用して、指定された要素がすでに存在するかどうかを判断し、<br/>
	 * 存在しない場合は、要素をこのSetに追加します。
	 *
	 * @param e 追加する要素
	 *
	 * @return 追加された場合はtrue。追加されなかった場合はfalseを返す。
	 */
	@Override
	public boolean add(E e) {

		for (E elm : delegatedList) {
			if (0 == comparator.compare(elm, e)) {
				return false;
			}
		}

		return delegatedList.add(e);
	}

}
