package jp.co.blueship.tri.fw.cmn.utils.predicate;

/**
 * 述語インタフェースです。
 *
 * @author Takayuki Kubo
 */
public interface TriPredicate<T> {

    boolean evalute(T obj);
}
