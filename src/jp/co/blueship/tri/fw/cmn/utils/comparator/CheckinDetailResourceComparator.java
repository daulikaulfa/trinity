package jp.co.blueship.tri.fw.cmn.utils.comparator;

import java.util.Comparator;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceDetailsViewBean;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;

public class CheckinDetailResourceComparator implements Comparator<ICheckinResourceDetailsViewBean> {
	private String columnName;
	private String sortOrder;

	public CheckinDetailResourceComparator(String columnName, String sortOrder) {
		this.columnName = columnName;
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(ICheckinResourceDetailsViewBean o1, ICheckinResourceDetailsViewBean o2) {
		if (o1 == o2) {
		    return 0;
		} else if (o1 == null) {
		    return -1;
		} else if (o2 == null) {
		    return 1;
		}
		
		switch (columnName) {
		case "size":
			return (int) (TriFileUtils.getBytesFromFormattedSize(o1.getSize()) - TriFileUtils.getBytesFromFormattedSize(o2.getSize()));
		case "name":
			return o1.getName().compareTo(o2.getName());	
	
		default:
			break;
		}
		return 0;
	}

}
