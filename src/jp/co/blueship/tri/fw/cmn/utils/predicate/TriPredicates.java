package jp.co.blueship.tri.fw.cmn.utils.predicate;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class TriPredicates {

	/**
	 * 指定された文字列がnullでないか、もしくは空文字ではないかについて判断するPredicate
	 */
	public static final TriPredicate<String> IS_NOT_EMPTY_STRING = new BasePredicate<String>() {

		@Override
		public boolean evalute(String str) {

			return !TriStringUtils.isEmpty(str);
		}
	};

	/**
	 * 指定されたクラス型であることを判断するPredicateを返す。
	 *
	 * @param type クラス型
	 * @return Predicate
	 */
	public static <T> TriPredicate<Object> isInstanceOf(final Class<T> type) {

		return new BasePredicate<Object>() {

			@Override
			public boolean evalute(Object obj) {
				return type.isInstance(obj);
			}
		};
	}


}
