package jp.co.blueship.tri.fw.cmn.utils.collections;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.function.MultiInputFunction;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojureEx;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;

/**
 * コレクションユーティリティクラス.
 *
 * @author Takayuki Kubo
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public final class TriCollectionUtils {

	/**
	 * コンストラクタ.
	 */
	private TriCollectionUtils() {
	}

	/**
	 * 指定された述語に従ってリストの内容を抽出する.
	 *
	 * @param input 入力リスト
	 * @param predicate 述語オブジェクト
	 * @param <T> リスト要素の型
	 * @return 抽出後リスト
	 */
	public static <T> List<T> select(final List<? extends T> input, final TriPredicate<T> predicate) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(predicate != null, "Predicate is null. ");

		List<T> output = new ArrayList<T>();

		for (T t : input) {
			if (predicate.evalute(t)) {
				output.add(t);
			}
		}

		return output;
	}

	/**
	 * 指定された述語に従ってリストの内容を抽出する.
	 *
	 * @param input 入力リスト
	 * @param predicate 述語オブジェクト
	 * @param <T> リスト要素の型
	 * @return 抽出後リスト
	 */
	public static <T> List<T> select(final T[] input, final TriPredicate<T> predicate) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(predicate != null, "Predicate is null. ");

		List<T> output = new ArrayList<T>();

		for (T t : input) {
			if (predicate.evalute(t)) {
				output.add(t);
			}
		}

		return output;
	}

	/**
	 * 指定された述語に従ってコレクションの内容を抽出する.
	 *
	 * @param input 入力コレクション
	 * @param predicate 述語オブジェクト
	 * @param <T> コレクション要素の型
	 * @return 抽出後コレクション
	 */
	public static <T> Collection<T> select(final Collection<? extends T> input, final TriPredicate<T> predicate) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(predicate != null, "Predicate is null. ");

		Collection<T> output = new ArrayList<T>();

		for (T t : input) {
			if (predicate.evalute(t)) {
				output.add(t);
			}
		}

		return output;
	}

	/**
	 * 指定された述語にマッチするリスト要素を除外する.
	 *
	 * @param input 入力リスト
	 * @param predicate 述語オブジェクト
	 * @param <T> リスト要素の型
	 * @return 除外後リスト
	 */
	public static <T> List<T> selectRejected(final List<? extends T> input, final TriPredicate<T> predicate) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(predicate != null, "Predicate is null. ");

		List<T> output = new ArrayList<T>();

		for (T t : input) {
			if (!predicate.evalute(t)) {
				output.add(t);
			}
		}

		return output;
	}

	/**
	 * 指定された述語にマッチするコレクション要素を除外する.
	 *
	 * @param input 入力コレクション
	 * @param predicate 述語オブジェクト
	 * @param <T> コレクション要素の型
	 * @return 除外後コレクション
	 */
	public static <T> Collection<T> selectRejected(final Collection<? extends T> input, final TriPredicate<T> predicate) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(predicate != null, "Predicate is null. ");

		Collection<T> output = new ArrayList<T>();

		for (T t : input) {
			if (!predicate.evalute(t)) {
				output.add(t);
			}
		}

		return output;
	}

	/**
	 * 指定された配列の各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納した配列
	 * @param function 関数
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 * @return 関数適用後の出力結果リスト
	 */
	public static <I, O> List<O> collect(I[] input, TriFunction<I, O> function) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");

		List<O> output = new ArrayList<O>();

		for (I i : input) {
			output.add(function.apply(i));
		}

		return output;
	}

	/**
	 * 指定されたリストの各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納した配列
	 * @param function 関数
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 * @return 関数適用後の出力結果リスト
	 */
	public static <I, O> List<O> collect(List<? extends I> input, TriFunction<I, O> function) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");

		List<O> output = new ArrayList<O>();

		for (I i : input) {
			output.add(function.apply(i));
		}

		return output;
	}

	/**
	 * 指定されたコレクションの各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納したコレクション
	 * @param function 関数
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 * @return 関数適用後の出力結果コレクション
	 */
	public static <I, O> Collection<O> collect(Collection<? extends I> input, TriFunction<I, O> function) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");

		Collection<O> output = new ArrayList<O>();

		for (I i : input) {
			output.add(function.apply(i));
		}

		return output;
	}

	/**
	 * 指定されたリストの各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納したリスト
	 * @param function 関数
	 * @param output 出力結果を格納するリスト
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 */
	public static <I, O> void collect(List<? extends I> input, TriFunction<I, O> function, List<O> output) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");

		for (I i : input) {
			output.add(function.apply(i));
		}

	}

	/**
	 * 指定されたコレクションの各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納したコレクション
	 * @param function 関数
	 * @param output 出力結果を格納するコレクション
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 */
	public static <I, O> void collect(Collection<? extends I> input, TriFunction<I, O> function, Collection<O> output) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");

		for (I i : input) {
			output.add(function.apply(i));
		}

	}

	/**
	 * 指定された配列の各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納した配列
	 * @param function 関数
	 * @param output 出力結果を格納するSet
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 */
	public static <I, O> void collect(I[] input, TriFunction<I, O> function, Set<O> output) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");

		for (I i : input) {
			output.add(function.apply(i));
		}

	}

	/**
	 * 指定されたリストの各要素に対して関数を実行する。
	 *
	 * @param input 関数を適用する要素を格納したリスト
	 * @param function 関数
	 * @param output 出力結果を格納するSet
	 * @param <I> 入力要素の型
	 * @param <O> 出力要素の型
	 */
	public static <I, O> void collect(List<? extends I> input, TriFunction<I, O> function, Set<O> output) {

		PreConditions.assertOf(input != null, "Input is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");

		for (I i : input) {
			output.add(function.apply(i));
		}

	}

	/**
	 * 指定されたリストの各要素にクロージャを適用する。<br/>
	 * クロージャからfalseが返却された場合はその時点で処理を中断する。
	 *
	 * @param inputList クロージャを適用する要素を格納したリスト
	 * @param clojure クロージャ
	 * @param <I> 入力要素の型
	 */
	public static <I> void forEach(List<? extends I> inputList, TriClojure<I> clojure) {

		PreConditions.assertOf(inputList != null, "InputList is null. ");
		PreConditions.assertOf(clojure != null, "Clojure is null. ");

		for (I input : inputList) {
			if (!clojure.apply(input)) {
				return;
			}
		}

	}

	/**
	 * 指定されたリストの各要素に例外送出の可能性があるクロージャを適用する。<br/>
	 * クロージャからfalseが返却されるか、例外が送出された場合はその時点で処理を中断する。
	 *
	 * @param inputList クロージャを適用する要素を格納したリスト
	 * @param clojure クロージャ
	 * @param <I> 入力要素の型
	 * @param <EX> 送出する可能性のある例外の型
	 * @throws EX クロージャから送出された例外
	 */
	public static <I, EX extends Exception> void forEachEx(List<? extends I> inputList, TriClojureEx<I, EX> clojure) throws EX {

		PreConditions.assertOf(inputList != null, "InputList is null. ");
		PreConditions.assertOf(clojure != null, "Clojure is null. ");

		for (I input : inputList) {
			if (!clojure.apply(input)) {
				return;
			}
		}

	}

	/**
	 * 指定された配列の各要素に例外送出の可能性があるクロージャを適用する。<br/>
	 * クロージャからfalseが返却されるか、例外が送出された場合はその時点で処理を中断する。
	 *
	 * @param inputArray クロージャを適用する要素を格納した配列
	 * @param clojure クロージャ
	 * @param <I> 入力要素の型
	 * @param <EX> 送出する可能性のある例外の型
	 * @throws EX クロージャから送出された例外
	 */
	public static <I, EX extends Exception> void forEachEx(I[] inputArray, TriClojureEx<I, EX> clojure) throws EX {

		PreConditions.assertOf(inputArray != null, "InputList is null. ");
		PreConditions.assertOf(clojure != null, "Clojure is null. ");

		for (I input : inputArray) {
			if (!clojure.apply(input)) {
				return;
			}
		}

	}

	/**
	 * 指定された２つのリストの各要素に対して関数を実行する。
	 *
	 * @param inputList1 関数を適用する要素を格納したリスト1
	 * @param inputList2 関数を適用する要素を格納したリスト2
	 * @param function ２つの入力パラメタを持つ関数
	 * @return 関数の実行結果を格納したリスト
	 * @param <I> 入力要素1の型
	 * @param <J> 入力要素2の型
	 * @param <O> 出力要素の型
	 */
	public static <I, J, O> List<O> composite(List<I> inputList1, List<J> inputList2, MultiInputFunction<I, J, O> function) {

		PreConditions.assertOf(inputList1 != null, "InputList1 is null. ");
		PreConditions.assertOf(inputList2 != null, "InputList2 is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(inputList1.size() == inputList2.size(), "InputList1 and InputList2 are not same size. ");

		List<O> output = new ArrayList<O>();
		composite(inputList1, inputList2, function, output);

		return output;
	}

	/**
	 * 指定された２つのリストの各要素に対して関数を実行する。
	 *
	 * @param inputList1 関数を適用する要素を格納したリスト1
	 * @param inputList2 関数を適用する要素を格納したリスト2
	 * @param function ２つの入力パラメタを持つ関数
	 * @param output 出力結果を格納するリスト
	 * @param <I> 入力要素1の型
	 * @param <J> 入力要素2の型
	 * @param <O> 出力要素の型
	 */
	public static <I, J, O> void composite(List<I> inputList1, List<J> inputList2, MultiInputFunction<I, J, O> function, List<O> output) {

		PreConditions.assertOf(inputList1 != null, "InputList1 is null. ");
		PreConditions.assertOf(inputList2 != null, "InputList2 is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");
		PreConditions.assertOf(inputList1.size() == inputList2.size(), "InputList1 and InputList2 are not same size. ");

		for (int i = 0; i < inputList1.size(); i++) {
			output.add(function.apply(inputList1.get(i), inputList2.get(i)));
		}
	}

	/**
	 * 指定された２つのコレクションの各要素に対して関数を実行する。
	 *
	 * @param input1 関数を適用する要素を格納したリスト1
	 * @param input2 関数を適用する要素を格納したリスト2
	 * @param function ２つの入力パラメタを持つ関数
	 * @param output 出力結果を格納するリスト
	 * @param <I> 入力要素1の型
	 * @param <J> 入力要素2の型
	 * @param <O> 出力要素の型
	 */
	public static <I, J, O> void composite(Collection<I> input1, Collection<J> input2, MultiInputFunction<I, J, O> function, Collection<O> output) {

		PreConditions.assertOf(input1 != null, "Input1 is null. ");
		PreConditions.assertOf(input2 != null, "Input2 is null. ");
		PreConditions.assertOf(function != null, "Function is null. ");
		PreConditions.assertOf(output != null, "Output is null. ");
		PreConditions.assertOf(input1.size() == input2.size(), "InputList1 and InputList2 are not same size. ");

		Iterator<I> it1 = input1.iterator();
		Iterator<J> it2 = input2.iterator();

		while (it1.hasNext()) {
			output.add(function.apply(it1.next(), it2.next()));
		}
	}

	/**
	 * 指定されたリストがNULLもしくは空かどうかを返す。<br/>
	 * <code>
	 * TruCollectionUtils.isEmpty(null) ⇒ true
	 * TruCollectionUtils.isEmpty(new ArrayList()) ⇒ true
	 * </code>
	 *
	 *
	 * @param list リスト
	 * @return NULLもしくは空の場合はtrue。
	 */
	public static <T> boolean isEmpty(List<T> list) {

		if (list == null) {
			return true;
		}

		return list.isEmpty();
	}

	/**
	 * 指定されたセットがNULLもしくは空かどうかを返す。<br/>
	 * <code>
	 * TruCollectionUtils.isEmpty(null) ⇒ true
	 * TruCollectionUtils.isEmpty(new HashSet()) ⇒ true
	 * </code>
	 *
	 *
	 * @param set セット
	 * @return NULLもしくは空の場合はtrue。
	 */
	public static <T> boolean isEmpty(Set<T> set) {

		if (set == null) {
			return true;
		}

		return set.isEmpty();
	}

	/**
	 * 指定されたコレクションがNULLもしくは空かどうかを返す。<br/>
	 * <code>
	 * TruCollectionUtils.isEmpty(null) ⇒ true
	 * TruCollectionUtils.isEmpty(new ArrayListt()) ⇒ true
	 * </code>
	 *
	 *
	 * @param c コレクション
	 * @return NULLもしくは空の場合はtrue。
	 */
	public static <T> boolean isEmpty(Collection<T> c) {

		if (c == null) {
			return true;
		}

		return c.isEmpty();
	}

	/**
	 * 指定されたコレクションが少なくとも1件以上の要素を保持するかどうかを返す。<br/>
	 * <code>
	 * TruCollectionUtils.isEmpty(null) ⇒ false
	 * TruCollectionUtils.isEmpty(new ArrayList()) ⇒ false
	 * </code>
	 *
	 * @param c コレクション
	 * @return 1件以上の要素を持つ場合はtrue。
	 */
	public static <T> boolean isNotEmpty(Collection<T> c) {

		return !isEmpty(c);
	}

}
