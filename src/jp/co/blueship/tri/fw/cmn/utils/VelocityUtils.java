package jp.co.blueship.tri.fw.cmn.utils;

import java.util.HashSet;
import java.util.Properties;
import java.util.Set;

import org.apache.velocity.runtime.RuntimeSingleton;

/**
 * Velocityのユーティリティです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class VelocityUtils {

	

	/**
	 * Velocityの様式にあわせて、プロパティに値を設定します。
	 * <br>
	 * <br>Velocityでは、","で区切られた値の設定が可能なため、このメソッドを使用して
	 * 設定された値は、すでに設定済みの値は尊重され、","で編集してからプロパティに設定します。
	 *
	 * @param key キー値
	 * @param value 設定値
	 * @param p 対象のプロパティ
	 */
	public static final synchronized Properties addProperties( String key, String value, Properties p ) {
        Object obj = RuntimeSingleton.getProperty( key );
        if ( TriStringUtils.isEmpty( obj ) ) {
        	p.put(key, value);
        	return p;
        }

        if ( obj instanceof String ) {
            String[] loader = TriStringUtils.convertStringToArray( (String)obj );

            if ( TriStringUtils.isEmpty( loader ) ) {
                p.put(key, value);
            } else {
            	Set<String> set = new HashSet<String>();
            	for ( String item : loader ) {
            		set.add( item );
            	}
            	set.add( value );
            	p.put(key, TriStringUtils.convertArrayToString( set.toArray( new String[0] ) ) );
            }
        }

        return p;
	}

}
