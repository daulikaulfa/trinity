package jp.co.blueship.tri.fw.cmn.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Set;
import java.util.regex.Pattern;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;

import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;



/**
 * 文字列操作を行うためのAddonユーティリティークラスです。
 *
 * @version V3L10R02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version SP-201602-1_V3L15R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TriStringUtils {

	/**
	 * Compares two Strings, returning true if they are equal.
	 * <br>
	 * <br>nulls are handled without exceptions. Two null references are considered to be equal. The comparison is case sensitive.
	 *
	 *
	 * <li>StringUtils.equals(null, null)   = true
	 * <li>StringUtils.equals(null, "abc")  = false
	 * <li>StringUtils.equals("abc", null)  = false
	 * <li>StringUtils.equals("abc", "abc") = true
	 * <li>StringUtils.equals("abc", "ABC") = false
	 *
	 * @param str1 the first String, may be null
	 * @param str2 the second String, may be null
	 * @return true if the Strings are equal, case sensitive, or both null
	 */
	public static final boolean equals( String str1, String str2 ) {
		return StringUtils.equals(str1, str2);
	}

	/**
	 * Compares two Strings, returning true if they are equal ignoring the case.
	 * <br>
	 * <br>nulls are handled without exceptions. Two null references are considered equal. Comparison is case insensitive.
	 *
	 *
	 * <li>StringUtils.equalsIgnoreCase(null, null)   = true
	 * <li>StringUtils.equalsIgnoreCase(null, "abc")  = false
	 * <li>StringUtils.equalsIgnoreCase("abc", null)  = false
	 * <li>StringUtils.equalsIgnoreCase("abc", "abc") = true
	 * <li>StringUtils.equalsIgnoreCase("abc", "ABC") = false
	 *
	 * @param str1 the first String, may be null
	 * @param str2 the second String, may be null
	 * @return true if the Strings are equal, case insensitive, or both null
	 */
	public static final boolean equalsIgnoreCase( String str1, String str2 ) {
		return StringUtils.equalsIgnoreCase(str1, str2);
	}

	/**
	 * 指定された文字列に値が設定されているかどうかを判定します。
	 * <br>nullまたは、""（空文字）が未指定となります。
	 *
	 * @param string 判定する文字列
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( String string ) {
		if ( null == string )
			return true;

		if ( "".equals(string.trim()) )
			return true;

		return false;
	}

	public static final boolean isNotEmpty( String string ) {
		return !isEmpty( string );
	}
	/**
	 * 指定された文字列配列に値が設定されているかどうかを判定します。
	 * <br>nullまたは、0 == length が未指定となります。
	 *
	 * @param string 判定する文字列配列
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( String[] string ) {
		if ( null == string )
			return true;

		if ( 0 == string.length )
			return true;

		if(string.length == 1) {
			if(isEmpty(string[0])) {
				return true;
			}
		}

		return false;
	}

	public static final boolean isNotEmpty( String[] string ) {
		return !isEmpty( string );
	}
	/**
	 * 指定されたStringBuilderに値が設定されているかどうかを判定します。
	 * <br>nullまたは、""（空文字）が未指定となります。
	 *
	 * @param stb 判定する文字列を格納したStringBuilder
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( StringBuilder stb ) {
		if ( null == stb )
			return true;

		if ( 0 == stb.toString().trim().length() )
			return true;

		return false;
	}

	/**
	 * 指定されたオブジェクトに値が設定されているかどうかを判定します。
	 * <br>nullまたは、""（空文字）が未指定となります。
	 *
	 * @param obj 判定するオブジェクト
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	@SuppressWarnings("unchecked")
	public static final boolean isEmpty( Object obj ) {
		if ( null == obj )
			return true;

		if ( obj instanceof String  )
			return isEmpty( (String)obj );

		if ( obj instanceof List  )
			return isEmpty( (List<Object>)obj );

		if ( obj instanceof Set  )
			return isEmpty( (Set<Object>)obj );

		return false;
	}

	/**
	 * 指定されたオブジェクトに値が設定されているかどうかを判定します。
	 * <br>nullまたは、""（空文字）が未指定となります。
	 *
	 * @param obj 判定するオブジェクト
	 * @return 未指定の値であれば、falseを戻します。それ以外はtrueを戻します。
	 */
	public static final boolean isNotEmpty ( Object obj ) {
		return !isEmpty( obj );
	}

	/**
	 * 指定されたオブジェクト配列に値が設定されているかどうかを判定します。
	 * <br>nullまたは、0 == length が未指定となります。
	 *
	 * @param obj 判定するオブジェクト配列
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( Object[] obj ) {
		if ( null == obj )
			return true;

		if ( 0 == obj.length )
			return true;

		return false;
	}

	/**
	 * 指定されたリストに値が設定されているかどうかを判定します。
	 * <br>nullまたは、0 == size が未指定となります。
	 *
	 * @param obj 判定するオブジェクト配列
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( List<Object> obj ) {
		if ( null == obj )
			return true;

		if ( 0 == obj.size() )
			return true;

		return false;
	}

	/**
	 * 指定されたセットに値が設定されているかどうかを判定します。
	 * <br>nullまたは、0 == size が未指定となります。
	 *
	 * @param obj 判定するオブジェクト配列
	 * @return 未指定の値であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEmpty( Set<Object> obj ) {
		if ( null == obj )
			return true;

		if ( 0 == obj.size() )
			return true;

		return false;
	}

	/**
	 * 指定された文字列に数値(自然数)が設定されているかどうかを判定します。
	 * 負の整数が指定された場合、"-"(マイナス記号)によりfalseとなります。
	 *
	 * @param string 判定する文字列
	 * @return 数値(自然数)であれば、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isDigits( String string ) {
	    return NumberUtils.isDigits( string );
	}

	/**
	 * 指定された文字列が等しいかどうかを判定します。
	 *
	 * @param string 判定する文字列１
	 * @param string 判定する文字列２
	 * @return 等しい場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEquals( String string1, String string2 ) {

		// 両方とも値が入っていない場合
		if ( isEmpty(string1) && isEmpty(string2) ) {
			return true;
		// 両方とも値が入っている場合
		} else if ( !isEmpty(string1) && !isEmpty(string2) ) {
			if ( string1.equals(string2) ) {
				return true;
			} else {
				return false;
			}
		// 一方のみに値が入っている場合
		} else {
			return false;
		}
	}

	/**
	 * 指定された文字列のリストが等しいかどうかを判定します。
	 *
	 * @param List<string> 判定する文字列リスト１
	 * @param List<string> 判定する文字列リスト２
	 * @return 等しい場合、trueを戻します。それ以外はfalseを戻します。
	 */
	public static final boolean isEquals( List<String> list1, List<String> list2 ) {

		// 両方とも値が入っていない場合
		if ( isEmpty(list1) && isEmpty(list2) ) {
			return true;
		// 両方とも値が入っている場合
		} else if ( !isEmpty(list1) && !isEmpty(list2) ) {
			// サイズが違う場合
			if ( list1.size() != list2.size()) {
				return false;
			}
			// サイズが同じ場合
			int cnt = 0;
			int match = 0;
			for (String value1 : list1) {
				cnt++;
				for(String value2 : list2) {
					if (value1.equals(value2)) {
						match++;
						break;
					}
				}
				if (cnt != match) return false;
			}
			return true;
		// 一方のみに値が入っている場合
		} else {
			return false;
		}
	}

	/**
	 * ファイルのパスを連結する。<br>
	 * パス区切り文字はすべて'/'に置換。<br>
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 */
	public static String linkPathBySlash( String pathRoot , String path ) {

		String ret = TriStringUtils.linkPath( pathRoot , path );
		ret = StringUtils.replace( ret , "\\" , "/" );
		return ret;
	}
	/**
	 * ファイルのパスを連結する。<br>
	 * パス区切り文字はすべて'\\'に置換。<br>
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 */
	public static String linkPathByBackSlash( String pathRoot , String path ) {

		String ret = TriStringUtils.linkPath( pathRoot , path );
		ret = StringUtils.replace( ret , "/" , "\\" );
		return ret;
	}
	/**
	 * ファイルのパスを連結する。<br>
	 * 連結時のパス区切り文字は環境依存の区切り文字('\\' or '/')とし、すべて置換する<br>
	 * @param pathRoot パス文字列（連結時上位）
	 * @param path パス文字列（連結時下位）
	 * @return 連結後のパス文字列
	 */
	public static String linkPath( String pathRoot , String path ) {

		String ret = trimTailSeparator( pathRoot );
		path = trimHeadSeparator( path );
		ret += File.separatorChar + path;

		ret = trimTailSeparator( ret );

		//パス区切り文字をすべて置換
		String separatorOrg = ( "\\".equals( File.separator ) ) ? "/" : "\\";
		ret = StringUtils.replace( ret , separatorOrg , File.separator );

		return ret;
	}

	/**
	 * パス先頭からパス区切り文字('\' or '/')までを取得する
	 *
	 * @param relativePath 相対パス
	 * @return 取得した文字列
	 */
	public static String substringBefore( String relativePath ) {
		return StringUtils.substringBefore(
				TriObjectUtils.defaultIfNull(convertPath( relativePath ) , ""), "/" );
	}

	/**
	 * パス先頭にパス区切り文字('\' or '/')が存在する場合は除去する<br>
	 * @param path パス
	 * @return 編集後のパス
	 */
	public static String trimHeadSeparator( String path ) {

		if ( null == path || 0 == path.length() )
			return "";

		String ret = null;

		path = path.trim();
		if( '\\' == path.charAt( 0 ) ||
			'/' == path.charAt( 0 ) ) {
			ret = path.substring( 1 );
		} else {
			ret = path;
		}
		return ret;
	}
	/**
	 * パス末尾にパス区切り文字('\' or '/')が存在する場合は除去する<br>
	 * @param path パス
	 * @return 編集後のパス
	 */
	public static String trimTailSeparator( String path ) {

		if ( null == path || 0 == path.length() )
			return "";

		String ret = null;

		path = path.trim();
		int lastPos = path.length() - 1;
		if( '\\' == path.charAt( lastPos ) ||
			'/' == path.charAt( lastPos ) ) {
			ret = path.substring( 0 , lastPos );
		} else {
			ret = path;
		}
		return ret;
	}

	/**
	 * ファイルのパス名を取得します。
	 * パス名は、"/"区切りに変換されます。
	 *
	 * @param filePath ファイル
	 * @return "/"変換されたパスを戻します。
	 */
	public static final String convertPath( String filePath ) {
		if ( null == filePath )
			return null;

		String path = filePath;

//		path = path.replace(File.separator, "/");
		path = StringUtils.replace( path , "\\" , "/" );
//
//		if ( ! path.startsWith("/") )
//			path = "/" + path;

		return TriStringUtils.trimTailSeparator( path );
	}

	/**
	 * ファイルのパス名を取得します。
	 * パス名は、"/"区切りに変換されます。
	 *
	 * @param file ファイル
	 * @return "/"変換されたパスを戻します。
	 */
	public static final String convertPath( File file ) {
		if ( null == file )
			return null;

		try {
			String path = file.getCanonicalPath();

			return convertPath( path );

		} catch (IOException e) {
			throw new TriSystemException( SmMessageId.SM004175F, e );
		}
	}

	/**
	 * Converts the path "/".
	 *
	 * <br>パスを"/"区切りに変換します。
	 *
	 * @param filePath target path. 対象パス
	 * @param withTopSlash If you want to a slash at the top of path, Set true. "/"から始まるパスの場合、true
	 */
	public static final String convertPath( String filePath, boolean withTopSlash ) {
		String path = convertPath( filePath );

		if ( ! path.startsWith("/") ) {
			path = "/" + path;
		}

		return ("/".equals(path))? path: TriStringUtils.trimTailSeparator( path );
	}

	/**
	 * Convert from an absolute path to a relative path.
	 *
	 * <br>ファイルを絶対パスから相対パスに変換する。
	 *
	 * @param root this parent path to be excluded. 除去される親パス
	 * @param path absolute path. 絶対パス
	 */
	public static final String convertRelativePath(File root, String path ) {
		String rootPath = convertPath( root );

		return convertRelativePath( rootPath, path );
	}

	/**
	 * Convert from an absolute path to a relative path.
	 *
	 * <br>ファイルを絶対パスから相対パスに変換する。
	 *
	 * @param rootPath this parent path to be excluded. 除去される親パス
	 * @param path absolute path. 絶対パス
	 */
	public static final String convertRelativePath(String rootPath, String path ) {
		rootPath = convertPath( rootPath );
		String relativePath = convertPath( path );

		relativePath = relativePath.replace(rootPath, "");

		if ( relativePath.startsWith("/") )
			relativePath = StringUtils.substringAfter(relativePath, "/");

		return relativePath;
	}

	/**
	 * Convert from an absolute path to a relative path.
	 *
	 * <br>ファイルを絶対パスから相対パスに変換する。
	 *
	 * @param root this parent path to be excluded. 除去される親パス
	 * @param path absolute path. 絶対パス
	 * @param withTopSlash If you want to a slash at the top of path, Set true. "/"から始まるパスの場合、true
	 */
	public static final String convertRelativePath(File root, String path, boolean withTopSlash ) {
		String rootPath = convertPath( root );

		return convertRelativePath( rootPath, path, withTopSlash );
	}

	/**
	 * Convert from an absolute path to a relative path.
	 *
	 * <br>ファイルを絶対パスから相対パスに変換する。
	 *
	 * @param rootPath this parent path to be excluded. 除去される親パス
	 * @param path absolute path. 絶対パス
	 * @param withTopSlash If you want to a slash at the top of path, Set true. "/"から始まるパスの場合、true
	 */
	public static final String convertRelativePath(String rootPath, String path, boolean withTopSlash ) {
		String relativePath = convertRelativePath( rootPath, path );

		if ( withTopSlash ) {
			if ( ! relativePath.startsWith("/") ) {
				relativePath = "/" + relativePath;
			}
		}

		return relativePath;
	}

	/**
	 * 相対パスをパスとファイル名に分割します。
	 * <br>(ex. "module/src/a.java" ⇒ "moduke/src/", "a.java"
	 *
	 * @param relativePath
	 * @return
	 */
	public static final String[] splitPath( String relativePath ) {
		String[] outPath = new String[2];

		String tempPath = TriStringUtils.convertPath( relativePath );

		String[] paths = StringUtils.split(tempPath, "/");

		if ( 2 > paths.length ) {
			outPath[0] = "";
			outPath[1] = (0 == paths.length)? "/": paths[0];

		} else {
			StringBuilder buffer = new StringBuilder();
			for ( int i = 0; i < paths.length - 1; i++ ) {
				buffer.append( paths[i] );
				buffer.append( "/" );
			}

			outPath[0] = buffer.toString();
			outPath[1] = paths[paths.length - 1];

		}

		return outPath;
	}

	/**
	 * 指定された文字列をセパレート文字列で分割します。
	 *
	 * @param str 対象文字列
	 * @param separatorChars セパレート文字列
	 * @return 分割した文字列を戻します。
	 */
	public static final String[] split( String str, String separatorChars ) {

		return split( str, separatorChars, false, false );
	}

	/**
	 * 指定された文字列をセパレート文字列で分割します。
	 *
	 * @param str 対象文字列
	 * @param separatorChars セパレート文字列
	 * @param isLeave セパレート後に前詰めを行わない場合true。Java標準仕様に準拠する場合はfalse
	 * @return 分割した文字列を戻します。
	 */
	public static final String[] split( String str, String separatorChars, boolean isLeave ) {

		return split( str, separatorChars, isLeave, false );
	}

	/**
	 * 指定された文字列をセパレート文字列で分割します。
	 *
	 * @param str 対象文字列
	 * @param separatorChars セパレート文字列
	 * @param isLeave セパレート後に前詰めを行わない場合true。Java標準仕様に準拠する場合はfalse
	 * @param isTrim 分割文字列をtrimする場合はtrue。それ以外はfalse
	 * @return 分割した文字列を戻します。
	 */
	public static final String[] split( String str, String separatorChars, boolean isLeave, boolean isTrim ) {
		if ( isEmpty(str) )
			return new String[0];

		int count = StringUtils.countMatches(str, separatorChars);

		String[] values = null;
		String [] splits = str.split( separatorChars );

		if ( isLeave ) {
			values = new String[ (0 != count)? count + 1 : 1 ];
			for ( int i = 0; i < values.length; i++ ) {
				values[i] = "";
			}
		} else {
			values = splits;
		}

		for ( int i = 0; i < splits.length; i++ ) {
			values[i] = ( isTrim )? splits[i].trim(): splits[i];
		}

		return values;
	}

	/**
	 * ファイルパスから拡張子を取り出します。
	 * @param filePath 拡張子を取り出すファイルパス
	 * @return 取得した拡張子を戻します。拡張子無しの場合、nullを戻します。
	 */
	public static final String getExtension( String filePath ) {

		String[] s = filePath.split("\\.");
		if ( 2 > s.length )
			return null;

		return s[ s.length - 1 ];
	}

	/**
	 * ファイル名から拡張子を除去します。
	 * @param fileName 拡張子を除去するファイル名
	 * @return 取得した拡張子を除去したファイル名を戻します。
	 */
	public static final String getBaseName( String fileName ) {
		return FilenameUtils.getBaseName( fileName );
	}

	/**
	 * 改行コードを変換する<br>
	 * 対象とする改行コードは[CRLF],[CR],[LF]のみ。<br>
	 * <br>
	 * @param data 変換前の文字列
	 * @param linefeedCode 変換後の改行コード
	 * @return 変換後の文字列
	 */
	public static String convertLinefeedCode( String data , LinefeedCode linefeedCode ) {

		//CRに統一
		data = StringUtils.replace( data , "\r\n" , "\r" );//CRLF->CR
		data = StringUtils.replace( data , "\n" , "\r" );	//LR->CR
		//指定の文字コードに変換
		data = StringUtils.replace( data , "\r" , linefeedCode.getValue() );//CR->任意

		return data;
	}

	/**
	 * 文字列の配列をもとに、カンマ(,)区切りの文字列を生成する。<br>
	 * @param array 文字列の配列
	 * @return カンマ(,)区切りの文字列
	 */
	public static String convertArrayToString( String[] array ) {

		if( TriStringUtils.isEmpty( array ) ) {
			return null;
		}
		StringBuilder sd = new StringBuilder();

		for ( String string : array ) {

			if ( 0 < sd.length() ) {
				sd.append( "," );
			}
			sd.append( string );
		}

		return sd.toString();
	}
	/**
	 * カンマ(,)区切りの文字列をもとに、文字列の配列を生成する。
	 * <br>
	 * @param string カンマ(,)区切りの文字列
	 * @return 文字列の配列
	 */
	public static String[] convertStringToArray( String string ) {
		if( TriStringUtils.isEmpty( string ) ) {
			return new String[0];
		}
		return string.split( "," );
	}
	/**
	 * カンマ(,)区切りの文字列をもとに、文字列の配列を生成する。
	 * <br>
	 * @param string カンマ(,)区切りの文字列
	 * @param isEscape ダブルクォーテーションで囲まれた文字列の場合、ダブルクォーテーションを除去する場合true。
	 * @return 文字列の配列
	 */
	public static String[] convertStringToArray( String string, boolean isEscape ) {
		if ( ! isEscape )
			return convertStringToArray( string );

		final String DOUBLE_QUOTE = "\"";

		String[] array = convertStringToArray( string );

		List<String> list = new ArrayList<String>();
		for ( String value : array ) {
			value = value.trim();

			if ( StringUtils.startsWith(value, DOUBLE_QUOTE)
				&& StringUtils.endsWith(value, DOUBLE_QUOTE) ) {
				value = StringUtils.strip(value, DOUBLE_QUOTE);
			}

			list.add( value );
		}

		return list.toArray( new String[0] );
	}

	/**
	 * 文字列のリストをもとに、カンマ(,)区切りの文字列を生成する。<br>
	 * @param array 文字列の配列
	 * @return カンマ(,)区切りの文字列
	 */
	public static String convertListToString( List<String> list ) {
		if( TriStringUtils.isEmpty( list ) ) {
			return null;
		}
		return TriStringUtils.convertArrayToString( list.toArray( new String[ 0 ] ) );
	}
	/**
	 * カンマ(,)区切りの文字列をもとに、文字列の配列を生成する。<br>
	 * @param string カンマ(,)区切りの文字列
	 * @return 文字列の配列
	 */
	public static List<String> convertStringToList( String string ) {
		String[] array = TriStringUtils.convertStringToArray( string );
		return FluentList.from(array).asList();
	}


	/**
	 * 文字列が全て全角かをチェックします。
	 * @param str
	 * @return
	 */
	public static boolean isAllZenkaku( String str ) {

		if (str == null) {
			return false;
		}

		return str.matches("^[^ -~｡-ﾟ]+$");
	}


	/**
	 * 文字列が全て半角かをチェックします。
	 * @param str
	 * @return
	 */
	public static boolean isAllHankaku( String str ) {

		if (str == null) {
			return false;
		}

		return str.matches("^[ -~｡-ﾟ]+$");
	}

    /**
     * <p>Checks if String contains a search String, handling <code>null</code>.
     * This method uses {@link String#indexOf(String)}.</p>
     *
     * <p>A <code>null</code> String will return <code>false</code>.</p>
     *
     * <pre>
     * StringUtils.contains(null, *)     = false
     * StringUtils.contains(*, null)     = false
     * StringUtils.contains("", "")      = true
     * StringUtils.contains("abc", "")   = true
     * StringUtils.contains("abc", "a")  = true
     * StringUtils.contains("abc", "z")  = false
     * </pre>
     *
     * @param str  the String to check, may be null
     * @param searchStr  the String to find, may be null
     * @return true if the String contains the search String,
     *  false if not or <code>null</code> string input
     */
	public static boolean contains(String str, String searchStr) {
		return StringUtils.contains(searchStr, searchStr);
	}

   /**
     * <p>Joins the elements of the provided array into a single String
     * containing the provided list of elements.</p>
     *
     * <p>No delimiter is added before or after the list.
     * A <code>null</code> separator is the same as an empty String ("").
     * Null objects or empty strings within the array are represented by
     * empty strings.</p>
     *
     * <pre>
     * StringUtils.join(null, *)                = null
     * StringUtils.join([], *)                  = ""
     * StringUtils.join([null], *)              = ""
     * StringUtils.join(["a", "b", "c"], "--")  = "a--b--c"
     * StringUtils.join(["a", "b", "c"], null)  = "abc"
     * StringUtils.join(["a", "b", "c"], "")    = "abc"
     * StringUtils.join([null, "", "a"], ',')   = ",,a"
     * </pre>
     *
     * @param array  the array of values to join together, may be null
     * @param separator  the separator character to use, null treated as ""
     * @return the joined String, <code>null</code> if null array input
     */
    public static String join(Object[] array, String separator) {
		return StringUtils.join(array, separator);
    }

    /**
     * <p>Joins the elements of the provided <code>Collection</code> into
     * a single String containing the provided elements.</p>
     *
     * <p>No delimiter is added before or after the list.
     * A <code>null</code> separator is the same as an empty String ("").</p>
     *
     * <p>See the examples here: {@link #join(Object[],String)}. </p>
     *
     * @param collection  the <code>Collection</code> of values to join together, may be null
     * @param separator  the separator character to use, null treated as ""
     * @return the joined String, <code>null</code> if null iterator input
     */
    public static String join(Collection<String> collection, String separator) {
		return StringUtils.join(collection, separator);
    }

	/**
	 * 文字列に半角カナが含まれるかをチェックします。
	 * @param str
	 * @return
	 */
	public static boolean containsHanKana( String str ) {

		if( null == str ) {
			return false ;
		}

		StringBuilder stb = new StringBuilder( str ) ;
		for( int i = 0 ; i < stb.length() ; i++ ) {
			/**
			 * 半角カナ範囲(Unicode 0xFF61 ～ 0xFF9F)確認
			 */
			if( 0xFF61 <= stb.charAt( i ) && stb.charAt( i ) <= 0xFF9F ) {
				return true ;
			}
		}
		return false ;
	}

	/**
	 * <code>input</code>が指定された正規表現パターンと一致するかどうかを判定します。
	 *
	 * @param input 検索対象の文字列
	 * @param pattern 正規表現パターン。未指定の場合、trueを戻します。
	 * @return 一致する場合は一致したパターンのリストを返します。
	 */
	public static boolean matches(String input, String pattern) {
		if ( TriStringUtils.isEmpty(pattern) ) {
			return true;
		}

		Pattern p = Pattern.compile(pattern);

		return p.matcher(input).matches();
	}

	/**
	 * 指定された文字列がNULLか空文字か判断し、そうでない場合は指定された値をそのまま返します。<br/>
	 * NULLか空文字である場合は、指定されたデフォルト値を返します。
	 * @param str 文字列
	 * @param defaultValue NULLまたは空文字である場合に返却する値
	 *
	 * @return NULLか空文字でない場合はstrを返す。NULLか空文字である場合はdefaultValueを返す。
	 */
	public static String defaultIfEmpty(String str, String defaultValue) {

		if (isEmpty(str)) {
			return defaultValue;
		}

		return str;
	}

	/**
	 * 指定された値がNULLまたは空文字かどうか判断し、NULLまたは空文字以外の場合はotherValueを返します。<br/>
	 * NULLまたは空文字である場合は、指定されたデフォルト値を返します。
	 * @param str 文字列
	 * @param defaultValue NULLまたは空文字である場合に返却する値
	 * @param otherValue tがNULLまたは空文字以外である場合に返す値
	 * @return NULLか空文字でない場合はotherValueを返す。NULLか空文字である場合はdefaultValueを返す。
	 */
	public static String defaultIfEmpty(String str, String defaultValue, String otherValue) {
		if (isEmpty(str)) {
			return defaultValue;
		}

		return otherValue;
	}

	/**
	 * Parse the given localeString into a Locale.
	 * This is the inverse operation of Locale's toString.
	 *
	 * @param localeString the locale string, following Locale's toString() format ("en", "en_UK", etc); also accepts spaces as separators, as an alternative to underscores
	 * @return a corresponding Locale instance
	 */
	public static Locale parseLocaleString( String localeString ) {
		return org.springframework.util.StringUtils.parseLocaleString( localeString );
	}

	/**
	 * Returns true if contains value of numerals.
	 *
	 * @param value The key whose presence in string is to be tested.
	 * @return
	 */
	public static boolean containsNumerals( String value ) {
		if ( TriStringUtils.isEmpty(value) )
			return false;

		return value.matches(".*\\d.*");
	}

	/**
	 * Return the time zone for the screen display.
	 *
	 * @param key timezone
	 * @return Time zone for the screen display
	 */
	public static String timezoneOf( String key ) {
		if ( TriStringUtils.isEmpty(key) ) {
			return "";
		}

		String value = DesignSheetFactory.getDesignSheet().getValue(UmDesignBeanId.timezones, key);
		String[] values = TriStringUtils.split(value, " ");

		StringBuilder timezone = new StringBuilder();

		if ( 1 >= values.length ) {
			return values[0];
		}

		for ( int i = 1; i < values.length; i++ ) {
			timezone.append(values[i]);
		}

		return timezone.toString();
	}
	
	public static int compareString(String c1, String c2){
		if (c1 == c2) {
            return 0;
        } else if (c1 == null) {
            return  -1;
        } else if (c2 == null) {
            return  1;
        }
        return c1.compareTo(c2);
	}

}
