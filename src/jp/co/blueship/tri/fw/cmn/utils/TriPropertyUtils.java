package jp.co.blueship.tri.fw.cmn.utils;

import java.lang.reflect.InvocationTargetException;

import org.apache.commons.beanutils.PropertyUtils;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * PropertyUtilsのラッパークラスです。
 *
 * @author Yukihiro Eguchi
 *
 */
public class TriPropertyUtils {

	/**
	 * メッセージのコンバートを行います。
	 * @param dest 変換後のメッセージ
	 * @param src 変換元のメッセージ
	 */
	public static final void copyProperties( Object dest, Object src ) {
		try {
			PropertyUtils.copyProperties(dest, src);

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005184S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005184S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005184S , e , "NoSuchMethodError" );
		}
	}

	/**
	 * メッセージからgetterの値を取得します。
	 * @param obj 対象となるオブジェクト
	 * @param prop getterメソッド名
	 */
	public static final Object getProperty( Object obj, String prop ) {
		try {
			return PropertyUtils.getProperty(obj, prop);

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005185S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005185S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005185S , e , "NoSuchMethodError" );
		} finally {
		}
	}

	/**
	 * メッセージからsetterの値を設定します。
	 * @param obj 対象となるオブジェクト
	 * @param name setterメソッド名
	 * @param value 設定値
	 */
	public static final void setProperty( Object obj, String name, String value ) {
		try {
			PropertyUtils.setProperty(obj, name, value);

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "NoSuchMethodError" );
		} finally {
		}
	}

	/**
	 * メッセージからsetterの値を設定します。
	 * @param obj 対象となるオブジェクト
	 * @param name setterメソッド名
	 * @param value 設定値
	 */
	public static final void setProperty( Object obj, String name, Object value ) {
		try {
			PropertyUtils.setProperty(obj, name, value);

		} catch (IllegalAccessException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "IllegalAccessError" );
		} catch (InvocationTargetException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "InvocationTargetError" );
		} catch (NoSuchMethodException e) {
			throw new TriSystemException( SmMessageId.SM005186S , e , "NoSuchMethodError" );
		} finally {
		}
	}

	/**
	 * メッセージからgetterの値を取得可能かどうかを判定します。
	 * 原則として、getPropertyメソッドを呼ぶ前に実施されます。
	 * @param obj 対象となるオブジェクト
	 * @param prop getterメソッド名
	 */
	public static final boolean isReadable( Object obj, String prop ) {
		try {
			return PropertyUtils.isReadable(obj, prop);

		} finally {
		}
	}

	/**
	 * メッセージからsetterの値を設定可能かどうかを判定します。
	 * 原則として、setPropertyメソッドを呼ぶ前に実施されます。
	 * @param obj 対象となるオブジェクト
	 * @param name setterメソッド名
	 */
	public static final boolean isWriteable( Object obj, String name ) {
		try {
			return PropertyUtils.isWriteable(obj, name);

		} finally {
		}
	}

}
