package jp.co.blueship.tri.fw.cmn.utils.function;

/**
 * クロージャクラス。
 *
 * @author Takayuki Kubo
 */
public interface TriClojure<I> {

    /**
     * クロージャを適用する。
     *
     * @param input 入力オブジェクト
     * @return 処理結果。ループ処理中の場合、falseを返却すればループを止めることができる。
     */
    boolean apply(I input);

}
