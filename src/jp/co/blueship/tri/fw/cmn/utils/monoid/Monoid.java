package jp.co.blueship.tri.fw.cmn.utils.monoid;

/**
 * 単位元クラス。
 *
 * @author Takayuki Kubo
 */
public interface Monoid<T> {

    /**
     * 指定した２つの演算項で演算を行う。
     *
     * @param input1 演算項1
     * @param input2 演算項2
     * @return 演算結果
     */
    T append(T input1, T input2);

    /**
     * 演算項が指定されていない場合のデフォルト値を返す。
     *
     * @return デフォルト値
     */
    T empty();
}
