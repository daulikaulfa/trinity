package jp.co.blueship.tri.fw.cmn.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Properties;

import org.springframework.util.StringUtils;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.constants.LinefeedCode;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * シェルスクリプトやバッチファイル、ＸＭＬファイルなどの、テキスト形式ファイルを生成します。<br>
 * 与えられたテンプレートファイルを元に、変数の置換処理を行って、ファイルを整形します。<br>
 *
 */
public class TextMakerFromTemplate {

	// ${xxx}とするとantの変数展開と重複して誤動作するため、{@xxx}と記述します
	private static final String PROPERTY_TAG_BEGIN = "{@" ;
	private static final String PROPERTY_TAG_END = "}" ;

	private String		templatePath = null ;
	private String		outputFilePath = null ;
	private Charset	charset = null ;
	private LinefeedCode linefeedCode = null ;

	private Properties paramMap = null ;

	public void setTemplatePath(String templatePath) {
		this.templatePath = templatePath ;
	}
	public void setOutputFilePath(String outputFilePath) {
		this.outputFilePath = outputFilePath;
	}
	public Properties getParamMap() {
		return paramMap ;
	}
	public void setParamMap(Properties paramMap) {
		this.paramMap = paramMap ;
	}
	public void setCharset(Charset charset) {
		this.charset = charset;
	}
	public void setLinefeedCode(LinefeedCode linefeedCode) {
		this.linefeedCode = linefeedCode;
	}
	/**
	 *
	 * @throws BaseBusinessException
	 */
	public final boolean execute() throws BaseBusinessException {

		String template = this.getTemplate( templatePath ) ;
		//パラメータの置換
		template = this.replaceMain( template , paramMap ) ;
		//パス区切り文字の統一化
		template = TriStringUtils.convertPath( template ) ;
		//ファイルの出力
		this.outputFile( template , outputFilePath , charset , linefeedCode ) ;

		return true ;
	}

	/**
	 * Ant ビルドのXML定義ファイルの雛形テンプレートを取得します。
	 * @param templateName テンプレートファイル名。これは、パス名の名前シーケンスの最後の名前です。
	 * @param list コンバート間に流通させるビーン
	 * @return 雛形テンプレートの文字列表現
	 * @throws BaseBusinessException
	 */
	private final String getTemplate( String templatePath ) throws BaseBusinessException {

		try{
			return TriFileUtils.readFileConvNewLine( new File( templatePath ) ) ;

		} catch (IOException e) {
			throw new TriSystemException(SmMessageId.SM005089S , e );
		}
	}

	/**
	 * テンプレートのメイン部を置換します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param list コンバート間に流通させるビーン
	 * @return 整形したテンプレート文字列
	 * @throws BaseBusinessException
	 */
	private final String replaceMain( String template , Properties properties ) throws BaseBusinessException {

		for (String key : properties.stringPropertyNames()) {
			template = StringUtils.replace( template , PROPERTY_TAG_BEGIN + key + PROPERTY_TAG_END , properties.getProperty(key) ) ;
		}

		return template ;

	}

	/**
	 * 整形したファイルをファイル出力します。
	 *
	 * @param template 対象となるテンプレート文字列
	 * @param outputFilePath 出力ファイルパス名
	 * @return 出力したXML定義ファイルを戻します。
	 * @throws BaseBusinessException
	 */
	private final void outputFile( String template, String outputFilePath , Charset charset , LinefeedCode linefeedCode ) throws BaseBusinessException {

		if( null == charset ) {
			throw new TriSystemException( SmMessageId.SM004171F ) ;
		}
		if( null == linefeedCode ) {
			throw new TriSystemException( SmMessageId.SM004172F ) ;
		}

		BufferedWriter bw = null ;
		OutputStreamWriter osw = null ;
		try {
			File file = new File( outputFilePath ) ;
			if( file.isDirectory() ) {
				throw new TriSystemException( SmMessageId.SM004173F , outputFilePath) ;
			}
			//  fileの実体がない状態(新規作成ファイルなど)の場合、canWrite==falseになるためチェック無効化

			//	改行コードを変更
			template = TriStringUtils.convertLinefeedCode( template , linefeedCode ) ;

			//与えられた文字コードでファイル出力
			osw = new OutputStreamWriter( new FileOutputStream( file ) , charset.toString() ) ;
			bw = new BufferedWriter( osw );
			bw.write( template );
			bw.flush();

		} catch( IOException e ) {
			throw new TriSystemException( SmMessageId.SM005088S , e ) ;
		} finally {
			TriFileUtils.closeWithFlush(bw);
			TriFileUtils.close(osw);
		}
	}
}
