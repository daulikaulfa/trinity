package jp.co.blueship.tri.fw.cmn.utils;

import java.util.Map;

import jp.co.blueship.tri.fw.constants.RmDesignEntryKeyByRelApply;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByCommon;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByPassword;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;

/**
 * プロジェクトリソースから値を取得するClass
 *
 */
public class DesignSheetUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	/**
	 * リリース申請を有効とするか否かを返す。
	 *
	 * @return 有効とする場合はtrue。そうでない場合はfalse。
	 */
	public static final boolean isRelApplyEnable() {

		String strValue = sheet.getValue( RmDesignEntryKeyByRelApply.enable );

		if ( strValue.trim().equals( "0") ) {
			return false;
		} else {
			return true;
		}

	}

	/**
	 * 記録を取るか取らないかを判定する。
	 * @return true:取る／false;取らない
	 */
	public static final boolean isRecord() {

		String strValue = sheet.getValue( UmDesignEntryKeyByCommon.record );

		if ( strValue.trim().equals( "0") ) {
			return false;
		} else {
			return true;
		}
	}


	/**
	 * パスワードのデフォルト有効期限を取得する
	 *
	 * @return 取得した情報
	 */
	public static final String getPasswordDefaultEffectiveTerm() {

		Map<String, String> prop = sheet.getKeyMap( UmDesignBeanId.password );
		if ( null == prop ) return null;

		String strValue = (String)prop.get( UmDesignEntryKeyByPassword.passwordDefaultEffectiveTerm.toString() );

		return strValue;
	}

	/**
	 * パスワードの最低長を取得する
	 *
	 * @return 取得した情報
	 */
	public static final String getPasswordMinimumLength() {

		Map<String, String> prop = sheet.getKeyMap( UmDesignBeanId.password );
		if ( null == prop ) return null;

		String strValue = (String)prop.get( UmDesignEntryKeyByPassword.passwordMinimumLength.toString() );

		return strValue;
	}

	/**
	 * パスワードの最大長を取得する
	 *
	 * @return 取得した情報
	 */
	public static final String getPasswordMaxLength() {

		Map<String, String> prop = sheet.getKeyMap( UmDesignBeanId.password );
		if ( null == prop ) return null;

		String strValue = (String)prop.get( UmDesignEntryKeyByPassword.passwordMaxLength.toString() );

		return strValue;
	}

	/**
	 * パスワードの履歴数を取得する
	 *
	 * @return 取得した情報
	 */
	public static final String getPasswordHistoryNumber() {

		Map<String, String> prop = sheet.getKeyMap( UmDesignBeanId.password );
		if ( null == prop ) return null;

		String strValue = (String)prop.get( UmDesignEntryKeyByPassword.passwordHistoryNumber.toString() );

		return strValue;
	}

	/**
	 * パスワードの許可書式を取得する
	 *
	 * @return 取得した情報
	 */
	public static final String getPasswordAllowPattern() {

		Map<String, String> prop = sheet.getKeyMap( UmDesignBeanId.password );
		if ( null == prop ) return null;

		String strValue = (String)prop.get( UmDesignEntryKeyByPassword.passwordAllowPattern.toString() );

		return strValue;
	}

	/**
	 * メールを送るか送らないかを判定する。
	 * @param beanId 定義BeanId
	 * @param flowId 画面遷移フローＩＤ
	 * @return true:送る／false:送らない
	 */
	public static final boolean isMail( IDesignBeanId beanId, String flowId ) {

		Map<String, String> prop = sheet.getKeyMap( beanId );
		if ( null == prop )
			return false;

		String value = (String)prop.get( flowId );

		return isMail( value );
	}
	/**
	 * メールを送るか送らないかを判定する。
	 * @param value 画面遷移フローＩＤの値
	 * @return true:送る／false:送らない
	 */
	private static final boolean isMail( String value ) {

		if ( null == value )
			return false;

		if ( value.trim().equals( "0") ) {
			return false;
		} else {
			return true;
		}
	}

}
