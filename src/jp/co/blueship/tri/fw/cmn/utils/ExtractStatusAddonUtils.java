package jp.co.blueship.tri.fw.cmn.utils;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;

/**
 * エンティティから該当ステータスを取得するユーティリティです。
 * <br>
 * <br>すべての組み合わせをこのクラスで管理しません。
 * たとえば、ビルドパッケージ作成可能なステータス等のように、ステータスの
 * 特定範囲をサポートする場合は、DBSearchConditionAddonUtilのような業務固有クラスで管理します。
 * <br>
 * <br>このクラスでは、～以上(Over)、以下(Under)、未満(Less)のような単純な組み合わせパターンをサポートします。
 *
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author yotei tanaka
 *
 */
public class ExtractStatusAddonUtils {



	/**
	 * ロット情報の進行中の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getLotBaseStatusByLotProgress() {

		String[] baseStatusId	= { AmLotStatusId.LotInProgress.getStatusId() };

		return baseStatusId;
	}

	/**
	 * ロット情報のすべての基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getLotBaseStatusByAll() {

		String[] baseStatusId	= {
				AmLotStatusId.LotInProgress.getStatusId(),
				AmLotStatusId.LotClosed.getStatusId() };

		return baseStatusId;
	}

	/**
	 * ロット情報の進行中以下の全体ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getLotStatusByUnderLotProgress() {

		String[] statusId	= {
				AmLotStatusId.LotInProgress.getStatusId() ,
				AmLotStatusIdForExecData.EditingLot.getStatusId(),
				AmLotStatusIdForExecData.LotEditError.getStatusId() };

		return statusId;
	}

	/**
	 * 変更管理情報の進行中の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByPjtProgress() {

		String[] baseStatusId = { AmPjtStatusId.ChangePropertyInProgress.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報のマージの基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByPjtMerge() {

		String[] baseStatusId = { AmPjtStatusId.Merged.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報の変更クローズの基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByPjtClose() {

		String[] baseStatusId = { AmPjtStatusId.ChangePropertyClosed.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報のマージ以上の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByOverPjtMerge() {

		String[] baseStatusId = {
				AmPjtStatusId.Merged.getStatusId(),
				AmPjtStatusId.ChangePropertyClosed.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報のマージ以下の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByUnderPjtMerge() {

		String[] baseStatusId = {
				AmPjtStatusId.ChangePropertyInProgress.getStatusId(),
				AmPjtStatusId.ChangePropertyTestCompleted.getStatusId(),
				AmPjtStatusId.Merged.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報のすべての基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtBaseStatusByAll() {

		String[] baseStatusId = {
				AmPjtStatusId.ChangePropertyInProgress.getStatusId(),
				AmPjtStatusId.ChangePropertyTestCompleted.getStatusId(),
				AmPjtStatusId.Merged.getStatusId(),
				AmPjtStatusId.ChangePropertyClosed.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 変更管理情報（承認履歴）のすべての基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getPjtApproveBaseStatusByAll() {

		String[] baseStatusId = {
				AmPjtStatusId.ChangePropertyInProgress.getStatusId(),
				AmPjtStatusId.ChangePropertyApproved.getStatusId(),
				AmPjtStatusId.ChangePropertyRemoved.getStatusId(),
				AmPjtStatusId.ChangePropertyApprovalCancelled.getStatusId(),
				AmPjtStatusId.ChangePropertyTestCompleted.getStatusId(),
				AmPjtStatusId.Merged.getStatusId() };

		return baseStatusId;
	}

	/**
	 * （貸出／返却／削除）申請情報の変更承認未満の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByLessPjtApprove() {

		String[] baseStatusId = {
				AmAreqStatusId.CheckoutRequested.getStatusId(),
				AmAreqStatusId.CheckinRequested.getStatusId(),
				AmAreqStatusId.RemovalRequested.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 申請情報の変更承認の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByPjtApprove() {

		String[] baseStatusId = {
				AmAreqStatusId.CheckinRequestApproved.getStatusId() ,
				AmAreqStatusId.RemovalRequestApproved.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 削除申請情報の変更承認以下の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetDelApplyBaseStatusByUnderPjtApprove() {

		String[] baseStatusId = {
				AmAreqStatusId.RemovalRequested.getStatusId(),
				AmAreqStatusId.RemovalRequestApproved.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 返却申請情報の変更承認以下の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetRtnApplyBaseStatusByUnderPjtApprove() {

		String[] baseStatusId = {
				AmAreqStatusId.CheckinRequested.getStatusId(),
				AmAreqStatusId.CheckinRequestApproved.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 申請情報の承認以上の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByOverPjtApprove() {
		List<String> baseStatusList = new ArrayList<String>();

		baseStatusList.addAll( FluentList.from(getAssetApplyBaseStatusByPjtApprove()).asList());
		baseStatusList.addAll( FluentList.from(getAssetApplyBaseStatusByOverRelUnitClose()).asList());

		return baseStatusList.toArray( new String[0] );
	}

	/**
	 * 申請情報のビルドパッケージクローズ以上の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByOverRelUnitClose() {
		String[] baseStatusId = {
				AmAreqStatusId.BuildPackageClosed.getStatusId() ,
				AmAreqStatusId.Merged.getStatusId() };

		return baseStatusId;
	}

	/**
	 * 申請情報のビルドパッケージクローズの基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByRelUnitClose() {
		String[] baseStatusId = {
				AmAreqStatusId.BuildPackageClosed.getStatusId() };

		return baseStatusId;
	}

	/**
	 * ビルドパッケージのすべての基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getRelUnitBaseStatusByAll() {

		@SuppressWarnings("deprecation")
		String[] baseStatusId = {
				BmBpStatusId.BuildPackageCreated.getStatusId(),
				BmBpStatusId.BuildPackageClosed.getStatusId(),
				BmBpStatusId.Merged.getStatusId() };

		return baseStatusId;
	}

	/**
	 * ビルドパッケージのすべての基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getRelUnitBaseStatusByRelUnitClose() {

		String[] baseStatusId = {
				BmBpStatusId.BuildPackageClosed.getStatusId(),
				 };

		return baseStatusId;
	}

	/**
	 * 申請取消の基本ステータスを取得します。
	 *
	 * @return ステータスの文字列配列を戻します。
	 */
	public static final String[] getAssetApplyBaseStatusByApplyCancel() {

		String[] baseStatusId = {
				AmAreqStatusId.CheckoutRequestRemoved.getStatusId(),
				AmAreqStatusId.RemovalRequestRemoved.getStatusId() };

		return baseStatusId;
	}

}
