package jp.co.blueship.tri.fw.cmn.utils.spec;


public class SpecificationUtils {

	public static <T> Specification<T> not(final Specification<T> specification) {

		return new BaseSpecification<T>() {

			@Override
			public boolean isSpecified(T input) {
				return !specification.isSpecified(input);
			}
		};
	}

	static <T> Specification<T> or(final Specification<T> specification1, final Specification<T> specification2) {

		return new BaseSpecification<T>() {

			@Override
			public boolean isSpecified(T input) {
				return specification1.isSpecified(input) || specification2.isSpecified(input);
			}
		};
	}

	static <T> Specification<T> and(final Specification<T> specification1, final Specification<T> specification2) {

		return new BaseSpecification<T>() {

			@Override
			public boolean isSpecified(T input) {
				return specification1.isSpecified(input) && specification2.isSpecified(input);
			}
		};
	}

}
