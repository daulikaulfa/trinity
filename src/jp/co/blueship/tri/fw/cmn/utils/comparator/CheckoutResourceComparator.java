package jp.co.blueship.tri.fw.cmn.utils.comparator;

import java.util.Comparator;
import java.util.Objects;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class CheckoutResourceComparator implements Comparator<ICheckoutResourceViewBean> {
	private String columnName;
	private String sortOrder;

	public CheckoutResourceComparator(String columnName, String sortOrder) {
		this.columnName = columnName;
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(ICheckoutResourceViewBean o1, ICheckoutResourceViewBean o2) {
		if (o1 == o2) {
		    return 0;
		} else if (o1 == null) {
		    return -1;
		} else if (o2 == null) {
		    return 1;
		}
		
		switch (columnName) {
		case "size":
			return (int) (TriFileUtils.getBytesFromFormattedSize(o1.getSize()) - TriFileUtils.getBytesFromFormattedSize(o2.getSize()));
		case "name":
			return o1.getName().compareTo(o2.getName());	
		case "date":
			if (o1.getCheckoutDate() == o2.getCheckoutDate()) {
			    return 0;
			} else if (o1.getCheckoutDate() == null) {
			    return -1;
			} else if (o2.getCheckoutDate() == null) {
			    return 1;
			}		
			if (o1.getCheckoutDate().equals(o2.getCheckoutDate())){
				return 0;
			}
				
			return TriDateUtils.before(o1.getCheckoutDate(), o2.getCheckoutDate()) ? -1 : 1;
			
		case "status":
			if (o1.getAreqType() == null || o2.getAreqType() == null) return Integer.MIN_VALUE;
			int i1 = o1.isLocked() ? Integer.MIN_VALUE : Integer.parseInt(o1.getAreqType().value());
			int i2 = o2.isLocked() ? Integer.MIN_VALUE : Integer.parseInt(o2.getAreqType().value());
			return i1-i2;	
			
			
		case "user":
			return TriStringUtils.compareString(o1.getSubmitterNm(), o2.getSubmitterNm());
 			
		default:
			break;
		}
		return 0;
	}

}
