package jp.co.blueship.tri.fw.cmn.utils;

import org.apache.commons.lang.StringUtils;

/**
 * フラグを取得するための列挙型です。
 *
 * @author Yukihiro Eguchi
 */
public enum StatusFlg {
	/**
	 * ステータス：ＯＮ
	 */
	on( "1", true ),
	/**
	 * ステータス：ＯＦＦ
	 */
	off( "0", false );

	private static StatusFlg defaultEnum = off;
	private String value = null;
	private Boolean parseBoolean = false;
	private Integer parseInteger = null;

	

	private StatusFlg( String value, Boolean isOn ) {
		this.value = value;
		this.parseBoolean = isOn;
		this.parseInteger = Integer.parseInt( this.value );
	}

	/**
	 * フラグの文字列を取得します。
	 *
	 * @return フラグの文字列
	 */
	public String value() {
		return this.value;
	}

	/**
	 * フラグのBooleanを取得します。
	 *
	 * @return フラグの文字列
	 */
	public Boolean parseBoolean() {
		return this.parseBoolean;
	}

	/**
	 * フラグの数値を取得します。
	 *
	 * @return フラグの数値
	 */
	public Integer parseInteger() {
		return this.parseInteger;
	}

	/**
	 * 指定されたフラグの名前に対応する列挙型を取得します。
	 *
	 * @param name フラグの名前
	 * @return 対応する列挙型
	 */
	public static StatusFlg name( String name ) {
		if ( StringUtils.isBlank( name ) ) {
			return defaultEnum;
		}

		for ( StatusFlg flg : values() ) {
			if ( flg.name().equals( name ) ) {
				return flg;
			}
		}

		return defaultEnum;
	}

	/**
	 * 指定されたフラグの文字列に対応する列挙型を取得します。
	 *
	 * @param value フラグの文字列
	 * @return 対応する列挙型
	 */
	public static StatusFlg value( String value ) {
		if ( StringUtils.isBlank( value ) ) {
			return defaultEnum;
		}

		for ( StatusFlg flg : values() ) {
			if ( flg.value().equals( value ) ) {
				return flg;
			}
		}

		return defaultEnum;
	}

	/**
	 * 指定されたフラグのBooleanに対応する列挙型を取得します。
	 *
	 * @param value フラグのboolean
	 * @return 対応する列挙型
	 */
	public static StatusFlg value( boolean value ) {
		for ( StatusFlg flg : values() ) {
			if ( flg.parseBoolean().equals( value ) ) {
				return flg;
			}
		}

		return defaultEnum;
	}

	/**
	 * 指定されたフラグのBooleanに対応する列挙型を取得します。
	 *
	 * @param value フラグのint
	 * @return 対応する列挙型
	 */
	public static StatusFlg value( int value ) {
		for ( StatusFlg flg : values() ) {
			if ( flg.parseInteger().equals( value ) ) {
				return flg;
			}
		}

		return defaultEnum;
	}

}
