package jp.co.blueship.tri.fw.cmn.utils;

import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.BmMessageId;
import jp.co.blueship.tri.fw.msg.RmMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;



/**
 * 画面入力項目の必須チェック、形式チェックの共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2009
 */
public class ItemCheckAddonUtils {

	/**
	 * ロット番号が選択されているかどうかチェックする
	 *
	 * @param lotId ロット番号
	 */

	public static void checkLotNo( String lotId ) {
		if ( TriStringUtils.isEmpty( lotId ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001002E );
		}
	}

	/**
	 * 変更管理番号が選択されているかどうかチェックする
	 *
	 * @param pjtNo 変更管理番号
	 */

	public static void checkPjtNo( String[] pjtNo ) {

		if ( TriStringUtils.isEmpty( pjtNo ) ) {
			throw new ContinuableBusinessException( BmMessageId.BM001001E );
		}
	}

	/**
	 * 環境番号が選択されているかどうかチェックする
	 *
	 * @param confNo 環境番号
	 */

	public static void checkConfNo( String confNo ) {

		if ( TriStringUtils.isEmpty( confNo ) ) {
			throw new ContinuableBusinessException( RmMessageId.RM001021E );
		}
	}

	/**
	 * リリース番号が選択されているかどうかチェックする
	 *
	 * @param relNo リリース番号
	 */

	public static void checkRelNo( String relNo ) {

		if ( TriStringUtils.isEmpty( relNo ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001004E );
		}
	}

	/**
	 * リリース番号が選択されているかどうかチェックする
	 *
	 * @param relNo リリース番号
	 */

	public static void checkRelNo( String[] relNo ) {

		if ( TriStringUtils.isEmpty( relNo ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001004E  );
		}
	}

	/**
	 * リリース申請番号が選択されているかどうかチェックする
	 *
	 * @param relApplyNo リリース番号
	 */

	public static void checkRelApplyNo( String relApplyNo ) {

		if ( TriStringUtils.isEmpty( relApplyNo ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001003E );
		}
	}

	/**
	 * リリース申請番号が選択されているかどうかチェックする
	 *
	 * @param relApplyNo リリース番号
	 */

	public static void checkRelApplyNo( String[] relApplyNo ) {

		if ( TriStringUtils.isEmpty( relApplyNo ) ) {
			throw new ContinuableBusinessException( SmMessageId.SM001003E );
		}
	}

	/**
	 * ビルドパッケージ番号が選択されているかどうかチェックする
	 *
	 * @param buildNo ビルドパッケージ番号
	 */

	public static void checkBuildNo( String buildNo ) {

		if ( TriStringUtils.isEmpty( buildNo ) ) {
			throw new ContinuableBusinessException( RmMessageId.RM001022E  );
		}
	}

	/**
	 * ビルドパッケージ番号が選択されているかどうかチェックする
	 *
	 * @param buildNo ビルドパッケージ番号
	 */

	public static void checkBuildNo( String[] buildNo ) {

		if ( TriStringUtils.isEmpty( buildNo ) ) {
			throw new ContinuableBusinessException( RmMessageId.RM001022E, new String[] {} );
		}
	}
}
