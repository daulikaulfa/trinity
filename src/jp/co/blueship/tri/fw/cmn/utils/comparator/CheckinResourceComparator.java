package jp.co.blueship.tri.fw.cmn.utils.comparator;

import java.util.Comparator;
import java.util.Objects;

import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckinResourceViewBean;
import jp.co.blueship.tri.am.domainx.checkinout.beans.dto.ICheckoutResourceViewBean;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;

public class CheckinResourceComparator implements Comparator<ICheckinResourceViewBean> {
	private String columnName;
	private String sortOrder;

	public CheckinResourceComparator(String columnName, String sortOrder) {
		this.columnName = columnName;
		this.sortOrder = sortOrder;
	}

	@Override
	public int compare(ICheckinResourceViewBean o1, ICheckinResourceViewBean o2) {
		if (o1 == o2) {
		    return 0;
		} else if (o1 == null) {
		    return -1;
		} else if (o2 == null) {
		    return 1;
		}
		
		switch (columnName) {
		case "size":
			return (int) (TriFileUtils.getBytesFromFormattedSize(o1.getSize()) - TriFileUtils.getBytesFromFormattedSize(o2.getSize()));
		case "name":
			return o1.getName().compareTo(o2.getName());	
		default:
			break;
		}
		return 0;
	}

}
