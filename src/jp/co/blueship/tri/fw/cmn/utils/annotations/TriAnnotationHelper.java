package jp.co.blueship.tri.fw.cmn.utils.annotations;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TriAnnotationHelper {

	/**
	 * Returns an object holding the method-value from the Annotation Parameter.
	 * @param src target object.
	 * @param clazz annotation class.
	 */
	public static <T> String valueOf( Object src, final Class<T> clazz ) {
		Exception invokeError = null;

		try {
			for ( Method method: src.getClass().getDeclaredMethods() ) {
				for ( Annotation annotation: method.getAnnotations() ) {
					if ( !(clazz.isInstance(annotation)) ) {
						continue;
					}

					return (String)method.invoke(src);
				}
			}
		} catch (IllegalArgumentException e) {
			invokeError = e;
		} catch (IllegalAccessException e) {
			invokeError = e;
		} catch (InvocationTargetException e) {
			invokeError = e;
		} finally {
			if ( null != invokeError ) {
				PreConditions.assertOf(false, "Definition of Annotation Error -> causedBy:" + invokeError.getMessage());
			}
		}

		return null;
	}
}
