package jp.co.blueship.tri.fw.cmn.utils.validator;

import static jp.co.blueship.tri.fw.cmn.utils.collections.FluentList.*;
import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;
import static jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicateUtils.*;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriObjectUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.annotations.TriMessage;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.IMessageParameter;
import jp.co.blueship.tri.fw.msg.MessageParameter;

/**
 * Validationヘルパクラスです。
 *
 *
 * @author Takayuki Kubo
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class TriValidationHelper {

	public static final String BEAN_NAME = "triValidationHelper";
	private static final TriPredicate<IMessageId> DEFAULT_MESSAGE_ID = new TriPredicate<IMessageId>() {

		@Override
		public boolean evalute(IMessageId id) {
			return "DEFAULT".equals(id.getMessageId());
		}
	};

	private static final TriFunction<MessageParameter, String> TO_STRING_MESSAGE_ARG = new TriFunction<MessageParameter, String>() {

		@Override
		public String apply(MessageParameter parameter) {
			return DesignUtils.getMessageParameter(parameter.getKey());
		}
	};

	/**
	 * Baen Validationを実行します。
	 *
	 * @param bean Validationチェック対象
	 * @param groups Validationチェックグループ
	 *
	 * @return Validationエラーが存在する場合はエラー内容を記述したオブジェクトのSet。エラーがない場合は空のSet。
	 */
	public <T> Set<ConstraintViolation<T>> validate(T bean, Class<?>... groups) {

		Set<ConstraintViolation<T>> ret = validator().validate(bean, groups);
		return TriObjectUtils.defaultIfNull(ret, Collections.<ConstraintViolation<T>> emptySet());
	}

	/**
	 * 指定されたValidation結果の違反情報から、エラーが発生したBeanのフィールド定義にTriMessageアノテーションとして付加された<br/>
	 * メッセージパラメタを取得します。
	 *
	 * @param violation Validation結果の違反情報
	 * @return メッセージパラメタ
	 */
	public <T> String[] getMessageParametersAsStringFrom(ConstraintViolation<T> violation) {

		MessageParameter[] messageParams = messageAnnotationOf(invalidProperty(violation), violation).args();

		return collect(messageParams, TO_STRING_MESSAGE_ARG).toArray(new String[] {});
	}

	/**
	 * 指定されたValidation結果の違反情報から、エラーが発生したBeanのフィールド定義にTriMessageアノテーションとして付加された<br/>
	 * メッセージIDを取得します。
	 *
	 * @param violation Validation結果の違反情報
	 * @return メッセージID
	 */
	public <T> IMessageId getMessageIDFrom(ConstraintViolation<T> violation) {

		return messageIDIn(messageAnnotationOf(invalidProperty(violation), violation));
	}

	private static <T> TriMessage messageAnnotationOf(String invalidPropertyName, ConstraintViolation<T> violation) {

		TriMessage annotation = null;
		try {
			annotation = violation.getLeafBean().getClass().getDeclaredField(invalidPropertyName).getAnnotation(TriMessage.class);
		} catch (Exception e) {
			PreConditions.assertOf(false, "Definition of Annotation Error ⇒ causedBy:" + e.getMessage());
		}

		return annotation;
	}

	private static <T> String invalidProperty(ConstraintViolation<T> violation) {

		String[] inValidPropertyPathTokens = TriStringUtils.split(violation.getPropertyPath().toString(), "\\.");

		PreConditions.assertOf(inValidPropertyPathTokens != null, "InValidPropertyPathTokens is null.");

		int length = inValidPropertyPathTokens.length;
		PreConditions.assertOf(length > 0, "InValidPropertyPathTokens is empty.");

		return inValidPropertyPathTokens[length - 1];
	}

	private static IMessageId messageIDIn(TriMessage annotation) {

		return from(allMessageIDsOn(annotation)).atFirstOf(not(DEFAULT_MESSAGE_ID));
	}

	private static List<IMessageId> allMessageIDsOn(TriMessage annotation) {

		return Arrays.asList(//
				(IMessageId) annotation.amMessageID(),//
				(IMessageId) annotation.bmMessageID(),//
				(IMessageId) annotation.rmMessageID(),//
				(IMessageId) annotation.dmMessageID(),//
				(IMessageId) annotation.dcmMessageID(),//
				(IMessageId) annotation.smMessageID());
	}

	/**
	 * 指定されたメッセージパラメタIDに対応するメッセージパラメタ値をリソースファイルから取得します。
	 *
	 * @param messageParameter メッセージパラメタID
	 * @return メッセージパラメタ値
	 */
	public String getMessageParamValue(IMessageParameter messageParameter) {

		return DesignUtils.getMessageParameter(messageParameter.getKey());
	}

	private static Validator validator() {

		TriValidatorFactory factory = (TriValidatorFactory) Contexts.//
				getInstance().//
				getBeanFactory().//
				getBean(TriValidatorFactory.BEAN_NAME);

		return factory.createValidator();
	}

}
