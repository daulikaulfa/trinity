package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * Enumeration type of wiki.
 * <br>
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 */
public enum UmWikiStatusId {
	none(""),
	/**
	 * Pulbic
	 * <br>公開
	 */
	Public("1000"),
	/**
	 * Removal
	 * <br>削除
	 */
	Removal("1400"),
	;

	private String statusId = null;

	private UmWikiStatusId( String statusId) {
		this.statusId = statusId;
	}

	public boolean equals( String statusId ) {
		UmWikiStatusId status = value( statusId );

		if ( ! this.equals(status) ) return false;

		return true;
	}

	public String getStatusId() {
		return this.statusId;
	}

	public static UmWikiStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return none;
		}

		for ( UmWikiStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return none;
	}

}
