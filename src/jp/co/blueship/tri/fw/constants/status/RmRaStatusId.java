package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * リリース申請ステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum RmRaStatusId implements IStatusId {

	/**
	 * リリース申請下書き
	 */
	DraftReleaseRequest( "4000", "Constants.rmStatus.4000.text" ),
	/**
	 * リリース申請済
	 */
	ReleaseRequested( "4100", "Constants.rmStatus.4100.text" ),
	/**
	 * リリース申請待ち
	 */
	PendingReleaseRequest( "4120", "Constants.rmStatus.4120.text" ),
	/**
	 * リリース承認済
	 */
	ReleaseRequestApproved( "4200", "Constants.rmStatus.4200.text" ),
	/**
	 * リリース申請取消済
	 */
	ReleaseRequestRemoved( "4400", "Constants.rmStatus.4400.text" ),
	/**
	 * 下書き取消済
	 */
	DraftReleaseRequestRemoved( "4410", "Constants.rmStatus.4410.text" ),
	/**
	 * 申請待ち取消済
	 */
	PendingReleaseRequestRemoved( "4420", "Constants.rmStatus.4420.text" ),
	/**
	 * リリース申請クローズ済
	 */
	ReleaseRequestClosed( "4600", "Constants.rmStatus.4600.text" ),
	/**
	 * リリース完了
	 */
	ReleasePackageCreated( "3100", "Constants.rmStatus.3100.text" ),
	/**
	 * リリース－取消済
	 */
	ReleasePackageRemoved( "3500", "Constants.rmStatus.3500.text" ),
	;

	private String statusId = null;
	private String key = null;

	private RmRaStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		RmRaStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static RmRaStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( RmRaStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
