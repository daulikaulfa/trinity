package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * ビルドパッケージステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum BmBpStatusId implements IStatusId {

	/**
	 * ビルドパッケージ未処理
	 */
	Unprocessed( "2000", "Constants.rmStatus.2000.text" ),
	/**
	 * ビルドパッケージ作成完了
	 */
	BuildPackageCreated( "2100", "Constants.rmStatus.2100.text" ),
	/**
	 * ビルドパッケージ－取消済
	 */
	BuildPackageRemoved( "2500", "Constants.rmStatus.2500.text" ),
	/**
	 * ビルドパッケージ－クローズ完了
	 */
	BuildPackageClosed( "2600", "Constants.rmStatus.2600.text" ),
	/**
	 * 変更管理・マージ済
	 * @deprecated V3 Only
	 */
	Merged( "9100", "Constants.rmStatus.9100.text" ),
	;

	private String statusId = null;
	private String key = null;

	private BmBpStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		BmBpStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static BmBpStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( BmBpStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}
}
