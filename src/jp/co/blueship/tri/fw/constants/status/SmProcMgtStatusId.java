package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * プロセス管理ステータスIDの列挙型です。
 * <br>
 * <br>0000～0999:処理ステータス
 * <br>1000～エラーステータス
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.44
 * @author Yukihiro Eguchi
 */
public enum SmProcMgtStatusId implements IStatusId {
	/**
	 * プロセス正常終了
	 */
	Success( "0000", "" ),
	/**
	 * プロセス実行中
	 */
	Processing( "0001", "" ),
	/**
	 * プロセス終了(警告あり)
	 */
	Warning( "0002", "" ),
	/**
	 * プロセスエラ
	 */
	Error( "0003", "" ),
	;

	private String statusId = null;
	private String key = null;

	private SmProcMgtStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		SmProcMgtStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static SmProcMgtStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( SmProcMgtStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
