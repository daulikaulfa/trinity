package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IExecDataStatusId;

/**
 * 資産申請ステータスIDの列挙型です。
 * <br>実行中データのステータスで使用される処理中および処理結果ステータスです。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 */
public enum AmAreqStatusIdForExecData implements IExecDataStatusId {
	/**
	 * 返却申請－処理中
	 */
	RequestingCheckin("5101", "Constants.amStatus.5101.text"),
	/**
	 * 返却申請－エラー
	 */
	CheckinRequestError("5103", "Constants.amStatus.5103.text"),
	/**
	 * 返却承認－処理中
	 */
	ApprovingCheckinRequest("5201", "Constants.amStatus.5201.text"),
	/**
	 * 返却承認－エラー
	 */
	CheckinRequestApprovalError("5203", "Constants.amStatus.5203.text"),
	/**
	 * 返却承認キャンセル
	 */
	CheckinApprovalCancelled("5210", "Constants.amStatus.5210.text"),
	/**
	 * 返却申請の差し戻し
	 */
	ReturnToCheckoutRequest("5300", "Constants.amStatus.5300.text"),
	/**
	 * 返却申請の取り下げ
	 */
	CheckinRequestCancelled("5310", "Constants.amStatus.5310.text"),
	/**
	 * 削除承認－処理中
	 */
	ApprovingRemovalRequest("6201", "Constants.amStatus.6201.text"),
	/**
	 * 削除承認－エラー
	 */
	RemovalRequestError("6203", "Constants.amStatus.6203.text"),
	/**
	 * 削除承認キャンセル
	 */
	RemovalApprovalCancelled("6210", "Constants.amStatus.6210.text"),
	/**
	 * 削除承認の差し戻し
	 */
	ReturnToPendingRemovalRequest("6300", "Constants.amStatus.6300.text"),
	/**
	 * 削除申請の取下げ
	 */
	RemovalRequestCancelled("6310", "Constants.amStatus.6310.text"),
	/**
	 * ビルドパッケージ－クローズ中
	 */
	BuildPackageClosing("7601", "Constants.amStatus.7601.text"),
	/**
	 * ビルドパッケージ－クローズエラー
	 */
	BuildPackageCloseError("7603", "Constants.amStatus.7603.text"),
	/**
	 * 変更管理・マージ－処理中
	 */
	Merging("9101", "Constants.amStatus.9101.text"),
	/**
	 * 変更管理・マージ－エラー
	 */
	MergeError("9103", "Constants.amStatus.9103.text"),
	;

	private String statusId = null;
	private String key = null;

	private AmAreqStatusIdForExecData( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmAreqStatusIdForExecData status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmAreqStatusIdForExecData value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmAreqStatusIdForExecData value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
