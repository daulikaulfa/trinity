package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * 変更情報ステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum AmPjtStatusId implements IStatusId {
	/**
	 * 変更管理・進行中
	 */
	ChangePropertyInProgress( "3100", "Constants.amStatus.3100.text" ),
	/**
	 * 変更管理・承認済 ※変更管理履歴のみで使用
	 */
	ChangePropertyApproved( "3200", "Constants.amStatus.3200.text" ),
	/**
	 * 変更管理・取消済
	 */
	ChangePropertyRemoved( "3400", "Constants.amStatus.3400.text" ),
	/**
	 * 変更管理・承認取消済 ※変更管理履歴のみで使用
	 */
	ChangePropertyApprovalCancelled( "3500", "Constants.amStatus.3500.text" ),
	/**
	 * 変更管理・クローズ済
	 */
	ChangePropertyClosed( "3600", "Constants.amStatus.3600.text" ),
	/**
	 * 変更管理・クローズ可能（検索条件のみ)
	 */
	ChangePropertyClosable( "3600P", "Constants.amStatus.3600P.text" ),
	/**
	 * 変更管理・クローズ済み以外（検索条件のみ)
	 */
	ChangePropertyNotClosed( "3600N", "Constants.amStatus.3600N.text"),
	/**
	 * 変更管理・テスト完了
	 */
	ChangePropertyTestCompleted( "3700", "Constants.amStatus.3700.text" ),
	/**
	 * 変更管理・マージ済
	 */
	Merged("9100" , "Constants.amStatus.9100.text" ),
	;

	private String statusId = null;
	private String key = null;

	private AmPjtStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmPjtStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmPjtStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmPjtStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
