package jp.co.blueship.tri.fw.constants.status;


import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
;


/**
 * HEAD・ベースライン マージステータスコードの列挙型です。
 * <br>
 *
 * @version V3L10.03
 * @author Yusna Marlina
 *
 * @version V4.00.00
 * @author le.thixuan
 */

public enum AmHeadBlMergeStatusCode {
	/**
	 * 初期値、またはコンフリクトチェック開始時の設定値
	 */
	LOT_UN_MERGE_CHECK("0000I"),
	/**
	 * コンフリクトチェックが正常に終了した場合
	 */
	LOT_MERGE_CHECK_SUCCESS("0100I"),
	/**
	 * 正常（警告あり）に終了したが、コンフリクトした資産がある場合
	 */
	LOT_MERGE_CHECK_RESOURCE_WARNING("0101W"),
	/**
	 * マージが正常に終了した場合
	 */
	LOT_MERGE_SUCCESS("0200I"),
	/**
	 * 正常に終了（警告あり）したが、処理されずにスキップした資産がある場合
	 */
	LOT_MERGE_RESOURCE_WARNING("0201W"),

	/**
	 * コンフリクトチェックがエラーとなった場合
	 */
	LOT_MERGE_CHECK_ERROR("0400E"),
	/**
	 * マージがエラーとなった場合
	 */
	LOT_MERGE_ERROR("0500E"),
	;

	private String statusCode = null ;

	private AmHeadBlMergeStatusCode( String statusCode) {
		 this.statusCode = statusCode;
	}

	public boolean equals( String statusCode ) {
		AmHeadBlMergeStatusCode status = value( statusCode );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	public String getStatusCode() {
		return this.statusCode;
	}

	/**
	 * 指定されたマージステータスコードに対応する列挙型を取得します。
	 *
	 * @param statusCode マージステータスコード
	 * @return 対応する列挙型
	 */
	public static AmHeadBlMergeStatusCode value( String statusCode ) {
		if ( TriStringUtils.isEmpty( statusCode ) ) {
			return null;
		}

		for ( AmHeadBlMergeStatusCode value : values() ) {
			if ( value.getStatusCode().equals( statusCode ) ) {
				return value;
			}
		}

		return null;
	}



}
