package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * ビルド環境ステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.44
 * @author Yukihiro Eguchi
 */
public enum BmBldEnvStatusId implements IStatusId {

	/**
	 * 設定中
	 */
	Configuring( "1000", "Constants.bmStatus.1000.text" ),
	/**
	 * 運用中
	 */
	Running( "1100", "Constants.bmStatus.1100.text" ),
	/**
	 * 取消済
	 */
	Cancelled( "1400", "Constants.bmStatus.1400.text" ),
	/**
	 * クローズ済
	 */
	Closed( "1600", "Constants.bmStatus.1600.text" ),
	;

	private String statusId = null;
	private String key = null;

	private BmBldEnvStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		BmBldEnvStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static BmBldEnvStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( BmBldEnvStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
