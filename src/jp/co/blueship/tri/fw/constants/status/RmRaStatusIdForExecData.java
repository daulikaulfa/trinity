package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IExecDataStatusId;

/**
 * リリース申請ステータスIDの列挙型です。
 * <br>実行中データのステータスで使用される処理中および処理結果ステータスです。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum RmRaStatusIdForExecData implements IExecDataStatusId {

	/**
	 * リリース申請差し戻し
	 */
	ReturnToPendingReleaseRequest( "4300", "Constants.rmStatus.4300.text" ),
	/**
	 * リリース申請取り下げ
	 */
	ReleaseRequestCancelled( "4310", "Constants.rmStatus.4310.text" ),
	/**
	 * リリース申請却下
	 */
	ReleaseRequestRejected( "4320", "Constants.rmStatus.4320.text" ),
	/**
	 * リリース承認取消済
	 */
	ReleaseRequestApprovalCancelled( "4500", "Constants.rmStatus.4500.text" ),
	/**
	 * リリース－処理中
	 */
	CreatingReleasePackage( "3101", "Constants.rmStatus.3101.text" ),
	/**
	 * リリース－エラー
	 */
	ReleasePackageError( "3103", "Constants.rmStatus.3103.text" ),
	;

	private String statusId = null;
	private String key = null;

	private RmRaStatusIdForExecData( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		RmRaStatusIdForExecData status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static RmRaStatusIdForExecData value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( RmRaStatusIdForExecData value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
