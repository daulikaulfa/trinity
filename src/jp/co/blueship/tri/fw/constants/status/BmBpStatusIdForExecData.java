package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IExecDataStatusId;

/**
 * ビルドパッケージステータスIDの列挙型です。
 * <br>実行中データのステータスで使用される処理中および処理結果ステータスです。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum BmBpStatusIdForExecData implements IExecDataStatusId {

	/**
	 * ビルドパッケージ－作成中
	 */
	CreatingBuildPackage( "2101", "Constants.rmStatus.2101.text" ),
	/**
	 * ビルドパッケージ－再開待ち
	 */
	PendingRecreateBuildPackage( "2102", "Constants.rmStatus.2102.text" ),
	/**
	 * ビルドパッケージ－エラー
	 */
	BuildPackageError( "2103", "Constants.rmStatus.2103.text" ),
	/**
	 * ビルドパッケージ－クローズ中
	 */
	BuildPackageClosing( "2601", "Constants.rmStatus.2601.text" ),
	/**
	 * ビルドパッケージ－クローズエラー
	 */
	BuildPackageCloseError( "2603", "Constants.rmStatus.2603.text"),
	/**
	 * 変更管理・マージ－処理中
	 */
	//PJT_MERGE_ACTIVE( "9101", "Constants.rmStatus.9101.text" ),
	/**
	 * 変更管理・マージ－エラー
	 */
	//PJT_MERGE__ERROR( "9103", "Constants.rmStatus.9103.text" ),
	;

	private String statusId = null;
	private String key = null;

	private BmBpStatusIdForExecData( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		BmBpStatusIdForExecData status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static BmBpStatusIdForExecData value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( BmBpStatusIdForExecData value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}
}
