package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;


/**
 * レポートステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum DcmRepStatusId implements IStatusId {
	/**
	 * 未記入
	 */
	Unprocessed( "0000", "Constants.dcmStatus.0000.text" ),
	/**
	 * 作成済（基本／全体）
	 */
	ReportCreated( "1000", "Constants.dcmStatus.1000.text" ),
	/**
	 * 取消済
	 */
	ReportRemoved( "1400", "Constants.dcmStatus.1400.text" );

	private String statusId = null;
	private String key = null;

	private DcmRepStatusId( String statusId, String key ) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		DcmRepStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static DcmRepStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( DcmRepStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
