package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * Enumeration type of recent updates.
 * <br>
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 */
public enum UmActStatusId {
	none(""),
	/**
	 * 追加
	 */
	Add("1000"),
	/**
	 * 提出
	 */
	Request("1100"),
	/**
	 * 編集
	 */
	Edit("2000"),
	/**
	 *
	 * 編集(提出)
	 */
	EditRequest("2100"),
	/**
	 * 削除
	 */
	Remove("3000"),
	/**
	 * 却下
	 */
	Reject("3100"),
	/**
	 * 一括却下
	 */
	MultipleReject("3110"),
	/**
	 * 承認
	 */
	Approve("4000"),
	/**
	 * 一括承認
	 */
	MultipleApprove("4010"),
	/**
	 * 取下げ
	 */
	Cancel("4100"),
	/**
	 * 一括取下げ
	 */
	MultipleCancel("4110"),
	/**
	 * 差戻し
	 */
	ReturnTo("4200"),
	/**
	 * 一括差戻し
	 */
	MultipleReturnTo("4210"),
	/**
	 * クローズ
	 */
	Close("8000"),
	/**
	 * 一括クローズ
	 */
	MultipleClose("8010"),
	;

	private String statusId = null;

	private UmActStatusId( String statusId) {
		this.statusId = statusId;
	}

	public boolean equals( String statusId ) {
		UmActStatusId status = value( statusId );

		if ( ! this.equals(status) ) return false;

		return true;
	}

	public String getStatusId() {
		return this.statusId;
	}

	public static UmActStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return none;
		}

		for ( UmActStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return none;
	}

}
