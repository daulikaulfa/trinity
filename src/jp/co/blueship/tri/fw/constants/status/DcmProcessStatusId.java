package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;


/**
 * <br>

 * @version V4.02.00
 * @author anh.Nguyen Duc
 */
public enum DcmProcessStatusId implements IStatusId {

	Unprocessed( "2000", "Constants.dcmProcessStatus.2000.text" ),

	ProcessingComplete( "2100", "Constants.dcmProcessStatus.2100.text" ),

	Processing( "2101", "Constants.dcmProcessStatus.2101.text" ),

	ProcessingError( "2103", "Constants.dcmProcessStatus.2103.text" ),

	UnprocessedRp( "3000", "Constants.dcmProcessStatus.3000.text" ),

	ProcessingRpComplete( "3100", "Constants.dcmProcessStatus.3100.text" ),

	ProcessingRp( "3101", "Constants.dcmProcessStatus.3101.text" ),

	ProcessingRpError( "3103", "Constants.dcmProcessStatus.3103.text" ),
	;


	private String statusId = null;
	private String key = null;

	private DcmProcessStatusId( String statusId, String key ) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		DcmProcessStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}


	public static DcmProcessStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( DcmProcessStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
