package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * デプロイメント連携ステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum DmDoStatusId implements IStatusId {

	/**
	 * 資源登録済
	 */
	JobRegistered( "3000", "Constants.dmStatus.3000.text" ),
	/**
	 * タイマー設定済
	 */
	TimerConfigured( "3100", "Constants.dmStatus.3100.text" ),
	/**
	 * 資源登録キャンセル
	 */
	JobCancelled( "3500", "Constants.dmStatus.3500.text" ),
	;

	private String statusId = null;
	private String key = null;

	private DmDoStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		DmDoStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static DmDoStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( DmDoStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}
}
