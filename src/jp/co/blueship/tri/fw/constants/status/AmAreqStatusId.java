package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * 資産申請ステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Tanaka Yohei
 */
public enum AmAreqStatusId implements IStatusId {

	/**
	 * 貸出下書き
	 */
	DraftCheckoutRequest("4000", "Constants.amStatus.4000.text"),
	/**
	 * 貸出申請済
	 */
	CheckoutRequested("4100", "Constants.amStatus.4100.text"),
	/**
	 * 貸出申請取消済
	 */
	CheckoutRequestRemoved("4400", "Constants.amStatus.4400.text"),
	/**
	 * 貸出下書き取消済
	 */
	DraftCheckoutRequestRemoved("4410", "Constants.amStatus.4410.text"),
	/**
	 * 返却下書き
	 */
	DraftCheckinRequest("5000", "Constants.amStatus.5000.text"),
	/**
	 * 返却受付済
	 */
	CheckinRequested("5100", "Constants.amStatus.5100.text"),
	/**
	 * 返却承認済
	 */
	CheckinRequestApproved("5200", "Constants.amStatus.5200.text"),
	/**
	 * 削除下書き
	 */
	DraftRemovalRequest("6000", "Constants.amStatus.6000.text"),
	/**
	 * 削除申請済
	 */
	RemovalRequested("6100", "Constants.amStatus.6100.text"),
	/**
	 * 削除申請待ち
	 */
	PendingRemovalRequest("6120", "Constants.amStatus.6120.text"),
	/**
	 * 削除承認済
	 */
	RemovalRequestApproved("6200", "Constants.amStatus.6200.text"),
	/**
	 * 削除申請取消済
	 */
	RemovalRequestRemoved("6400", "Constants.amStatus.6400.text"),
	/**
	 * 削除下書き取消済
	 */
	DraftRemovalRequestRemoved("6410", "Constants.amStatus.6410.text"),
	/**
	 * 削除申請待ち取消済
	 */
	PendingRemovalRequestRemoved("6420", "Constants.amStatus.6420.text"),
	/**
	 * ビルドパッケージ－クローズ完了
	 */
	BuildPackageClosed("7600", "Constants.amStatus.7600.text"),
	/**
	 * 変更管理・マージ済
	 */
	Merged("9100", "Constants.amStatus.9100.text"),
	;

	private String statusId = null;
	private String key = null;

	private AmAreqStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmAreqStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmAreqStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmAreqStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
