package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;


/**
 * レポートステータスIDの列挙型です。
 * <br>実行中データのステータスで使用される処理中および処理結果ステータスです。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum DcmRepStatusIdForExecData implements IStatusId {
	/**
	 * 作成中（全体）
	 */
	CreatingReport( "1001", "Constants.dcmStatus.1001.text" ),
	/**
	 * 作成エラー（全体）
	 */
	ReportError( "1003", "Constants.dcmStatus.1003.text" );

	private String statusId = null;
	private String key = null;

	private DcmRepStatusIdForExecData( String statusId, String key ) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		DcmRepStatusIdForExecData status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static DcmRepStatusIdForExecData value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( DcmRepStatusIdForExecData value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
