package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * HEAD・ベースラインステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum AmLotStatusId implements IStatusId {
	/**
	 * ロット管理・未作成
	 */
	Unspecified( "2000", "Constants.amStatus.2000.text" ),
	/**
	 * ロット管理・進行中
	 */
	LotInProgress( "2100", "Constants.amStatus.2100.text" ),
	/**
	 * ロット管理・取消済
	 */
	LotRemoved( "2400", "Constants.amStatus.2400.text" ),
	/**
	 * ロット管理・クローズ済
	 */
	LotClosed( "2600", "Constants.amStatus.2600.text" ),
	;

	private String statusId = null;
	private String key = null;

	private AmLotStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmLotStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmLotStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmLotStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}


}
