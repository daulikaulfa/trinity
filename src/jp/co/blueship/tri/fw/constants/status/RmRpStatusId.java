package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * RPステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum RmRpStatusId implements IStatusId {

	/**
	 * リリース未処理
	 */
	Unprocessed( "3000", "Constants.rmStatus.3000.text" ),
	/**
	 * リリース完了
	 */
	ReleasePackageCreated( "3100", "Constants.rmStatus.3100.text" ),
	/**
	 * リリース－取消済
	 */
	ReleasePackageRemoved( "3500", "Constants.rmStatus.3500.text" ),
	/**
	 * リリース－クローズ
	 */
	ReleasePackageClosed( "3600", "Constants.rmStatus.3600.text" ),
	;

	private String statusId = null;
	private String key = null;

	private RmRpStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		RmRpStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static RmRpStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( RmRpStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}
}
