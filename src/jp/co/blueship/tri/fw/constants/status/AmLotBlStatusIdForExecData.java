package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IExecDataStatusId;

/**
 * ロット・ベースラインステータスIDの列挙型です。
 * <br>実行中データのステータスで使用される処理中ステータスです。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum AmLotBlStatusIdForExecData implements IExecDataStatusId {
	/**
	 * チェックイン処理中
	 */
	Committing( "9001", "Constants.amStatus.9001.text" ),
	/**
	 * チェックインエラー
	 */
	CommitError( "9003", "Constants.amStatus.9003.text" ),
	/**
	 * マージ処理中
	 */
	Merging( "9101", "Constants.amStatus.9101.text" ),
	/**
	 * マージエラー
	 */
	MergeError( "9103", "Constants.amStatus.9103.text" ),
	/**
	 * コンフリクト処理中
	 */
	ConflictChecking( "9201", "Constants.amStatus.9201.text" ),
	/**
	 * コンフリクトエラー
	 */
	ConflictCheckError( "9203", "Constants.amStatus.9203.text" ),
	;

	private String statusId = null;
	private String key = null;

	private AmLotBlStatusIdForExecData( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmLotBlStatusIdForExecData status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmLotBlStatusIdForExecData value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmLotBlStatusIdForExecData value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
