package jp.co.blueship.tri.fw.constants.status;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;

/**
 * HEAD・ベースラインステータスIDの列挙型です。
 * <br>
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum AmHeadBlStatusId implements IStatusId {
	/**
	 * 未作成
	 */
	Unspecified("2000", "Constants.amStatus.2000.text"),
	/**
	 * マージ済
	 */
	Merged( "9100", "Constants.amStatus.9100.text"),
	/**
	 * コンフリクトチェック済
	 */
	ConflictChecked( "9200", "Constants.amStatus.9200.text"),
	;

	private String statusId = null;
	private String key = null;

	private AmHeadBlStatusId( String statusId, String key) {
		this.statusId = statusId;
		this.key = key;
	}

	public boolean equals( String statusId ) {
		AmHeadBlStatusId status = value( statusId );

		if ( null == status ) return false;
		if ( ! this.equals(status) ) return false;

		return true;
	}

	@Override
	public String getStatusId() {
		return this.statusId;
	}

	@Override
	public String getMessageKey() {
		return this.key;
	}

	/**
	 * 指定されたステータスIDに対応する列挙型を取得します。
	 *
	 * @param statusId ステータスID
	 * @return 対応する列挙型
	 */
	public static AmHeadBlStatusId value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return null;
		}

		for ( AmHeadBlStatusId value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return null;
	}

}
