package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.02
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public enum RmDesignBeanId implements IDesignBeanId {
	statusId,
	release,
	relApply,
	logs,
	taskLogs,
	mail,
	rpTopOrder,
	rpTopOrderBy,
	rpHistoryListLotSelectOrder,
	rpHistoryListLotSelectOrderBy,
	rpHistoryListOrder,
	rpHistoryListOrderBy,
	rpHistoryDetailViewRelUnitListOrder,
	rpHistoryDetailViewRelUnitListOrderBy,
	rpEntryListOrder,
	rpEntryListOrderBy,
	rpEntryRelWishDateSelectOrder,
	rpEntryRelWishDateSelectOrderBy,
	rpEntryConfigurationSelectOrder,
	rpEntryConfigurationSelectOrderBy,
	rpEntryLotSelectOrder,
	rpEntryLotSelectOrderBy,
	rpEntryReleaseUnitSelectOrder,
	rpEntryReleaseUnitSelectOrderBy,
	rpEntryConfirmRelUnitListOrder,
	rpEntryConfirmRelUnitListOrderBy,
	rpGenerateDetailViewLotSelectOrder,
	rpGenerateDetailViewLotSelectOrderBy,
	rpRelUnitDetailViewRelListOrder,
	rpRelUnitDetailViewRelListOrderBy,
	rpCancelTopOrder,
	rpCancelTopOrderBy,
	rpCancelListLotSelectOrder,
	rpCancelListLotSelectOrderBy,
	rpCancelListOrder,
	rpCancelListOrderBy,
	rpCancelConfirmOrder,
	rpCancelConfirmOrderBy,
	raTopListOrder,
	raTopListOrderBy,
	raTopLotSelectOrder,
	raTopLotSelectOrderBy,
	raTopConfigurationSelectOrder,
	raTopConfigurationSelectOrderBy,
	raTopRelWishDateSelectOrder,
	raTopRelWishDateSelectOrderBy,
	raPackageListSelectListOrder,
	raPackageListSelectListOrderBy,
	raCloseListViewListOrder,
	raCloseListViewListOrderBy,
	raCloseHistoryListViewListOrder,
	raCloseHistoryListViewListOrderBy,
	raTopListCount,
	raPackageListSelectListCount,
	raCloseListViewListCount,
	raCloseHistoryListViewListCount,
	rpEntryListCount,
	rpHistoryListCount,
	rpCancelListCount,
	flowRelCtlCancelCheck,
	flowRelApplySaveInputCheck,
	flowRelApplyInputCheck,
	flowRelApplyCloseCheck,
	flowRelCtlBaseInfoInputCheck,
	relDownload,
	multiSelectable,
	resources,
	;

	private RmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.rmDesignSheet.value();
	}

}
