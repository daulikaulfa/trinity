package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum DmDesignEntryKeyByCommercialShell implements IDesignEntryKey {

	/**
	 * 商用シェルの作成に用いる、テンプレートファイルの保管パス（テンプレートパスからの相対パス）
	 */
	templateBaseRelativePath( ),
	/**
	 * 商用シェルの出力先ディレクトリパス（ホームトップパスからの相対パス）
	 */
	shellOutputBaseRelativePath( ),
	/**
	 * 商用シェルの文字コード
	 */
	charset( ),
	/**
	 * 商用シェルの改行コード
	 */
	linefeedCode( ),
	/**
	 * 商用シェルのファイル拡張子
	 */
	fileExtension( ),
	/**
	 * 「配付資源登録」商用シェル
	 */
	shellAssetReg( ),
	/**
	 * 「配付資源削除」商用シェル
	 */
	shellAssetDelete( ),
	/**
	 * 「配付資源登録履歴確認」商用シェル
	 */
	shellRegHistory( ),
	/**
	 * 「配付資源登録履歴確認」商用シェル
	 */
	shellAssetDealTimerSet( ),
	/**
	 * 「タイマー取消」商用シェル
	 */
	shellAssetDealTimerCancel( ),
	/**
	 * 「配布履歴確認」商用シェル
	 */
	shellAssetDealStatusChk( );


	

	public static DmDesignEntryKeyByCommercialShell getValue( String name ) {
		for ( DmDesignEntryKeyByCommercialShell define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return DmDesignBeanId.commercialShell;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
