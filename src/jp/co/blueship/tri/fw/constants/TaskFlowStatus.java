package jp.co.blueship.tri.fw.constants;

/**
 * Enumeration type of task flow status.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum TaskFlowStatus {
	none,
	Exist,
	Arrow,
	Empty,
	Bottom,
	;
}
