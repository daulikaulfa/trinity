package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 * 
 * @version V4.00.00
 * @author Yohei Tanaka
 * 
 */
public enum DcmDesignBeanId implements IDesignBeanId {
	statusId,
	report,
	mail,
	reportId,
	changeStatusId,
	processStatusId,
	pjtApplyRegisterPjtOrder,
	pjtApplyRegisterPjtOrderBy,
	pjtApplyRegisterAssetApplyOrder,
	pjtApplyRegisterAssetApplyOrderBy,
	reportApplyListOrder,
	reportApplyListOrderBy,
	buildAssetRegisterRelUnitOrder,
	buildAssetRegisterRelUnitOrderBy,
	buildAssetRegisterPjtOrder,
	buildAssetRegisterPjtOrderBy,
	relAssetRegisterRelCtlOrder,
	relAssetRegisterRelCtlOrderBy,
	relAssetRegisterRelUnitOrder,
	relAssetRegisterRelUnitOrderBy,
	relAssetRegisterPjtOrder,
	relAssetRegisterPjtOrderBy,
	reportAssetListOrder,
	reportAssetListOrderBy,
	relApplyAssetRegisterRelApplyOrder,
	relApplyAssetRegisterRelApplyOrderBy,
	relApplyAssetRegisterRelUnitOrder,
	relApplyAssetRegisterRelUnitOrderBy,
	reportListStatusIdSearch,
	reportListCount,
	reportConfigReportOutputInfo,
	reportConfigCategoryInfo,
	reportConfigItemInfo,
	reportConfigListViewInfo,
	bpFileExtension,
	rpFileExtension,
	bpExtractArchiveFileExtension,
	rpExtractArchiveFileExtension,
	bpArchiveFileTopPath,
	rpArchiveFileTopPath,
	bpArchiveEntryTopPath,
	rpArchiveEntryTopPath,
	download;

	private DcmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.dcmDesignSheet.value();
	}

}
