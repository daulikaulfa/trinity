package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum BmDesignEntryKeyByLogs implements IDesignEntryKey {

	/**
	 * 【リリース管理】 パッケージクローズ 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessRelUnitClose(),
	/**
	 * 【リリース管理】 ビルドパッケージレポート 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessRelUnitReport(),
	;

	

	public static BmDesignEntryKeyByLogs getValue( String name ) {
		for ( BmDesignEntryKeyByLogs define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return BmDesignBeanId.logs;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
