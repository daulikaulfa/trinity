package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum BmDesignEntryKeyByBuild implements IDesignEntryKey {
	/**
	 * トップ画面のロット数
	 */
	maxPageNumberByTopLot( ),
	/**
	 * トップ画面の列数(ビルドパッケージ作成の履歴数)
	 */
	maxPageNumberByTopUnit( ),
	/**
	 * ロット履歴一覧画面の行数
	 */
	maxPageNumberByLotHistoryList( ),
	/**
	 * ビルドパッケージ履歴一覧画面の行数
	 */
	maxPageNumberByHistoryList(),
	/**
	 * ビルドパッケージクローズトップ画面の行数(ロット)
	 */
	maxPageNumberByUnitCloseTop( ),
	/**
	 * ビルドパッケージ取消トップ画面の行数(ロット)
	 */
	maxPageNumberByUnitCancelTop( ),
	/**
	 * 申請番号のプレフィックス
	 */
	bpNumberingPrefix(),
	/**
	 * 一覧 CSVダウンロードのプレフィックス
	 */
	listFilePrefix(),
	/**
	 * 詳細 CSVダウンロードのプレフィックス
	 */
	detailFilePrefix(),
	/**
	 * 履歴 CSVダウンロードのプレフィックス
	 */
	historyFilePrefix(),
	/**
	 * CSVダウンロードファイルの格納パス
	 */
	downloadPath(),
	/**
	 * ビルド状況照会画面の再描画間隔（ミリ秒）
	 */
	reloadInterval(),
	/**
	 * ビルド状況照会画面のタスクイメージの１行分
	 */
	taskMaxColumns(),
	/**
	 * ビルドパッケージアーカイブファイルの文字コード
	 */
	assetArchiveCharset();


	

	public static BmDesignEntryKeyByBuild getValue( String name ) {
		for ( BmDesignEntryKeyByBuild define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return BmDesignBeanId.build;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
