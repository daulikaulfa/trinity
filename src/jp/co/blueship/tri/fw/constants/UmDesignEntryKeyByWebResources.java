package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）web resourcesのEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum UmDesignEntryKeyByWebResources implements IDesignEntryKey {

	defaultParentMapping(),
	customParentMapping(),
	customParentLocation(),
	images(),
	projectImages(),
	lotImages(),
	;

	public static UmDesignEntryKeyByWebResources getValue( String name ) {
		for ( UmDesignEntryKeyByWebResources define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.webResources;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
