package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum RmDesignEntryKeyByLogs implements IDesignEntryKey {

	/**
	 * 【リリース管理】 リリースパッケージレポート 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessRelCtlReport(),
	/**
	 * 【リリース管理】 リリース申請票 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessRelApplyTicket(),
	;

	

	public static RmDesignEntryKeyByLogs getValue( String name ) {
		for ( RmDesignEntryKeyByLogs define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return RmDesignBeanId.logs;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
