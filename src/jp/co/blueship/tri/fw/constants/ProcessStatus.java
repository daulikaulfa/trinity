package jp.co.blueship.tri.fw.constants;

/**
 * Enumeration type of process status.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum ProcessStatus {
	none,
	Active,
	Completed,
	Error,
	Warning,
	;
}
