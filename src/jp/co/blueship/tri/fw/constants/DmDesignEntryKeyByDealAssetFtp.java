package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum DmDesignEntryKeyByDealAssetFtp implements IDesignEntryKey {
	/**
	 * ＦＴＰサーバが有効かどうか
	 */
	ftpServer(),
	/**
	 * ＦＴＰホスト
	 */
	host(),
	/**
	 * ＦＴＰサービスのポート番号
	 */
	port(),
	/**
	 * ＦＴＰサービスのユーザアカウント
	 */
	user(),
	/**
	 * ＦＴＰサービスのＰＡＳＶモード
	 */
	pasv(),
	/**
	 * 文字コードエンコーディング
	 */
	controlEncoding(),
	/**
	 * 転送先ベースパス
	 */
	destBasePath(),
	/**
	 * 転送元ファイルの拡張子（ファイル名は業務ルール）
	 */
	srcExtension();

	

	public static DmDesignEntryKeyByDealAssetFtp getValue( String name ) {
		for ( DmDesignEntryKeyByDealAssetFtp define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return DmDesignBeanId.dealAssetFtp;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
