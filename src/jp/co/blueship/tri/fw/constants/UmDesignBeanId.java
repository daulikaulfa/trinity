package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum UmDesignBeanId implements IDesignBeanId {
	project,
	productId,
	common,
	actionId,
	mail,
	password,
	passwordEffectiveTerm,
	history,
	webResources,
	resources,
	langs,
	timezones,
	userIcons,
	lotIcons,
	projectIcons,
	wiki,
	userUpload,
	;

	private UmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.umDesignSheet.value();
	}

}
