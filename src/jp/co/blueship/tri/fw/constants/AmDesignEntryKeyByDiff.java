package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByDiff implements IDesignEntryKey {
	/**
	 * DiffでADDと判定された箇所の背景色
	 */
	addBgColor( ),
	/**
	 * DiffでCHANGEと判定された箇所の背景色
	 */
	changeBgColor( ),
	/**
	 * DiffでDELETEと判定された箇所の背景色
	 */
	deleteBgColor( ),
	/**
	 * DiffでADDと判定された箇所の文字色
	 */
	addColor(),
	/**
	 * DiffでCHANGEと判定された箇所の文字色
	 */
	changeColor(),
	/**
	 * DiffでDELETEと判定された箇所の文字色
	 */
	deleteColor();

	public static AmDesignEntryKeyByDiff getValue( String name ) {
		for ( AmDesignEntryKeyByDiff define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.diff;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
