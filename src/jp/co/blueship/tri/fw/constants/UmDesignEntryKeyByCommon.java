package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 */
public enum UmDesignEntryKeyByCommon implements IDesignEntryKey {

	/**
	 * プロジェクト名
	 */
	projectName( ),
	/**
	 * バージョン
	 */
	version(),
	/**
	 * 時間項目の表示形式
	 */
	viewDateFormat(),
	/**
	 * 記録を取る／取らないのフラグ
	 */
	record(),
	/**
	 * CSVファイルの改行置換文字列
	 */
	lineSeparator(),
	/**
	 * 履歴明細で最新と異なる項目の背景色
	 */
	bgColor(),
	/**
	 * ホームパス
	 */
	homeTopPath(),
	/**
	 * テンプレートパス（ホームパスからの相対パス）
	 */
	templatesRelativePath(),
	/**
	 * 採番テンプレートパス（テンプレートパスからの相対パス）
	 */
	numberingTemplatesRelativePath(),
	/**
	 * メールテンプレートパス（テンプレートパスからの相対パス）
	 */
	mailTemplatesRelativePath(),
	/**
	 * ログの保管パス
	 */
	logRelationPath(),
	/**
	 * Default Public Path
	 */
	defaultPublicPath(),
	/**
	 * Share Public Path
	 */
	sharePublicPath(),
	/**
	 * Default Private Path
	 */
	defaultPrivatePath(),
	;

	public static UmDesignEntryKeyByCommon getValue( String name ) {
		for ( UmDesignEntryKeyByCommon define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.common;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
