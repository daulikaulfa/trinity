package jp.co.blueship.tri.fw.constants;

/**
 * Enumeration type of Request.
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public enum RequestType {
	/**
	 * session variable is initialized.
	 */
	init					( "init" ),
	/**
	 * session variable will be retained.
	 */
	onChange				( "onChange" ),
	/**
	 * To return to the referer, and then use the session of the referer.
	 */
	returnToReferer			( "returnToReferer" ),
	validate				( "validate" ),
	submitChanges			( "submitChanges" ),
	;

	private String value = null ;

	private RequestType( String value ) {
		this.value = value ;
	}

	public String value() {
		return this.value ;
	}

	public static RequestType value( String value ) {
		for ( RequestType charset : values() ) {
			if ( charset.value().equals( value ) ) {
				return charset;
			}
		}

		return init;
	}
}
