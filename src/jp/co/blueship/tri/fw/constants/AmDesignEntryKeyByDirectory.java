package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByDirectory implements IDesignEntryKey {

	/**
	 * 貸出申請の貸出資産格納パス（相対パス・絶対パスは業務ルール）
	 */
	lendAssetRelationPath(),
	/**
	 * 返却申請のユーザ返却資産の格納パス（相対パス・絶対パスは業務ルール）
	 */
	returnInfoRelationPath(),
	/**
	 * 取消された申請の資産のバックアップパス（相対パス・絶対パスは業務ルール）
	 */
	returnAssetBackupRelationPath(),
	/**
	 * 承認された申請の取消時の差戻しパス（相対パス・絶対パスは業務ルール）
	 */
	//sendBackRelationPath(),
	/**
	 * ビルドパッケージの資産管理パス（相対パス・絶対パスは業務ルール）
	 */
	relUnitDSLRelationPath(),
	/**
	 * リリースパッケージの資産管理パス（相対パス・絶対パスは業務ルール）
	 */
	relDSLRelationPath();

	

	public static AmDesignEntryKeyByDirectory getValue( String name ) {
		for ( AmDesignEntryKeyByDirectory define : values() ) {
			if ( define.name().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.directory;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
