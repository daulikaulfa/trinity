package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public enum UmDesignEntryKeyByWikiAttachment implements IDesignEntryKey {

	appendFilePath(),
	;

	public static UmDesignEntryKeyByWikiAttachment getValue( String name ) {
		for ( UmDesignEntryKeyByWikiAttachment define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.wiki;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
