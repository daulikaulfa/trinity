package jp.co.blueship.tri.fw.constants;

/**
 * 権限アクションＩＤのプレフィックスの定義名の列挙型です。
 *
 * @author Yukihiro Eguchi
 *
 */
public enum FlowActionPrefix {
	
	/**
	 * ベースキット
	 */
	FlowUcf(),
	/**
	 * サービスデスク
	 */
	FlowSd(),
	/**
	 * インシデント管理
	 */
	FlowInc(),
	/**
	 * 問題管理
	 */
	FlowPro(),
	/**
	 * 変更管理
	 */
	FlowChaLib(),
	/**
	 * リリース管理・ビルドパッケージ
	 */
	FlowRelUnit(),
	/**
	 * リリース管理・リリースパッケージ
	 */
	FlowRelCtl(),
	/**
	 * リリース管理・資源配付
	 */
	FlowRelDistribute(),
	/**
	 * リリース管理・レポート
	 */
	FlowRelReport();

	
	
	private FlowActionPrefix() {
	}
	
	public static FlowActionPrefix getValue( String name ) {
		for ( FlowActionPrefix define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}
		
		return null;
	}

}
