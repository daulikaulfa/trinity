package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * 画面遷移のタイプの定数クラスです。
 * <br>複数画面にまたがって、１アクションフローとなる遷移体系で
 * <br>トピックパスによる遷移なのか、通常遷移なのかを識別するために使用します。
 *
 * @version V3L10.01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 */
public class ScreenType {

	/**
	 * 「次へ」等の通常遷移
	 */
	public static final String next = Type.next.value();
	/**
	 * 「選択」等による同画面遷移
	 */
	public static final String select = Type.select.value();
	/**
	 * 「それ以外」
	 */
	public static final String other = Type.other.value();
	/**
	 * 業務エラーによる画面遷移
	 */
	public static final String bussinessException = Type.bussinessException.value();

	public enum Type {
		none( null ),
		next( "next" ),
		select( "select" ),
		other( "" ),
		bussinessException( "exception" );

		private String value = null;

		private Type( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			Type type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static Type value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( Type type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
