package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum AmDesignBeanId implements IDesignBeanId {
	statusId,
	directory,
	changec,
	scm,
	merge,
	diff,
	logs,
	mail,
	applyId,
	changeCauseClassifyId,
	allowAssetDuplication,
	entryPjtSelectOrder,
	entryPjtSelectOrderBy,
	topListOrder,
	topListOrderBy,
	lotHistoryListOrder,
	lotHistoryListOrderBy,
	lotDetailViewPjtListOrder,
	lotDetailViewPjtListOrderBy,
	lotHistoryDetailViewPjtListOrder,
	lotHistoryDetailViewPjtListOrderBy,
	lotCloseConfirmPjtListOrder,
	lotCloseConfirmPjtListOrderBy,
	lotCancelConfirmPjtListOrder,
	lotCancelConfirmPjtListOrderBy,
	pjtDetailViewAssetApplyListOrder,
	pjtDetailViewAssetApplyListOrderBy,
	pjtHistoryDetailViewAssetApplyListOrder,
	pjtHistoryDetailViewAssetApplyListOrderBy,
	pjtTestCompleteConfirmAssetApplyListOrder,
	pjtTestCompleteConfirmAssetApplyListOrderBy,
	pjtCloseConfirmAssetApplyListOrder,
	pjtCloseConfirmAssetApplyListOrderBy,
	pjtCancelConfirmAssetApplyListOrder,
	pjtCancelConfirmAssetApplyListOrderBy,
	lendListOrder,
	lendListOrderBy,
	rtnListOrder,
	rtnListOrderBy,
	pjtApproveConfirmAssetApplyListOrder,
	pjtApproveConfirmAssetApplyListOrderBy,
	pjtApproveListOrder,
	pjtApproveListOrderBy,
	pjtApproveDetailViewAssetApplyListOrder,
	pjtApproveDetailViewAssetApplyListOrderBy,
	pjtApproveDetailViewPjtApproveListOrder,
	pjtApproveDetailViewPjtApproveListOrderBy,
	pjtApproveCancelConfirmAssetApplyListOrder,
	pjtApproveCancelConfirmAssetApplyListOrderBy,
	pjtApproveCancelConfirmAssetApplyRelatedListOrder,
	pjtApproveCancelConfirmAssetApplyRelatedListOrderBy,
	pjtApproveCancelConfirmPjtApproveListOrder,
	pjtApproveCancelConfirmPjtApproveListOrderBy,
	masterDelListOrder,
	masterDelListOrderBy,
	masterDelHistoryListOrder,
	masterDelHistoryListOrderBy,
	conflictCheckLotListOrder,
	conflictCheckLotListOrderBy,
	conflictCheckBaselineDetailViewPjtListOrder,
	conflictCheckBaselineDetailViewPjtListOrderBy,
	margeLotListOrder,
	margeLotListOrderBy,
	margeBaselineDetailViewPjtListOrder,
	margeBaselineDetailViewPjtListOrderBy,
	margeHistoryLotListOrder,
	margeHistoryLotListOrderBy,
	margeHistoryBaselineDetailViewPjtListOrder,
	margeHistoryBaselineDetailViewPjtListOrderBy,
	lotEntryBuildEnvListOrder,
	lotEntryBuildEnvListOrderBy,
	lotEntryRelEnvListOrder,
	lotEntryRelEnvListOrderBy,
	lotEntryUcfScmListOrder,
	lotEntryUcfScmListOrderBy,
	flowChaLibLotEntryCheck,
	flowChaLibLotCloseCheck,
	flowChaLibLotCancelCheck,
	flowChaLibPjtEntryCheck,
	flowChaLibPjtCancelCheck,
	flowChaLibPjtCloseCheck,
	flowChaLibPjtTestCompleteCheck,
	flowChaLibPjtApproveCheck,
	flowChaLibPjtApproveCancelCheck,
	flowChaLibPjtApproveRejectCheck,
	flowChaLibLendEntryCheck,
	flowChaLibMasterDelEntryCheck,
	flowChaLibConflictCheckCheck,
	flowChaLibMergeCommitCheck,
	lotDetailViewPjtListStatusIdSearch,
	lotDetailViewPjtListStatusIdSearchForTestComplete,
	lotDetailViewPjtListCount,
	callScriptAtReturnApply,
	callScriptAtReturnApplyParam,
	callScriptAtReturnApplyErrCd,
	neglectPath,
	diffNeglectKeyword;

	private AmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.amDesignSheet.value();
	}

}
