package jp.co.blueship.tri.fw.constants;

/**
 * Defines the types of joins.
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public enum JoinType {
    /**
     * Left Join.
     */

    LEFT,

    /**
     * Right Join.
     */
    RIGHT
}
