package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）履歴情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum UmDesignEntryKeyByHist implements IDesignEntryKey {

	histNumberingPrefix(),
	;

	public static UmDesignEntryKeyByHist getValue( String name ) {
		for ( UmDesignEntryKeyByHist define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.history;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
