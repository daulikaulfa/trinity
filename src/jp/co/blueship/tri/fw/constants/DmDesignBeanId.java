package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.01.00
 * @author Akahoshi
 */
public enum DmDesignBeanId implements IDesignBeanId {
	distributeStatusId,
	deploymentJob,
	dealAssetFtp,
	relFtp,
	commercialShell,
	distributeListOrder,
	distributeListOrderBy,
	flowRelDistInfoInputCheck,
	flowRelDistTimerSetCheck;

	private DmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.dmDesignSheet.value();
	}

}
