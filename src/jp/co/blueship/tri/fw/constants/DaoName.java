package jp.co.blueship.tri.fw.constants;

/**
 * DAOを取得するbean名の列挙型です。
 * <br>原則として、DAOは、DIコンテナ内でインジェクションします。
 * <br>
 * <br>出来る限りDIの定義上でPOJOのインジェクションを行い、この列挙型を安易に使用しないようにして下さい。
 *
 * @author Yukihiro Eguchi
 *
 */
public enum DaoName {

	/**
	 * DAO
	 */
	BLD_SRV_DAO( "bmBldSrvDao" ),
	USER_DAO( "userBasicDao" ),
	GROUP_USER_DAO( "groupUserDao" );




	private String value = null;

	private DaoName( String value ) {
		this.value = value;
	}

	public String toString() {
		return this.value;
	}

}
