package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum UmDesignEntryKeyByProductId implements IDesignEntryKey {
	/**
	 * ソフトウェア資産管理
	 */
	am(),
	/**
	 * リリース管理
	 */
	rm(),
	/**
	 * リリース連携
	 */
	dm(),
	;

	public static UmDesignEntryKeyByProductId getValue( String name ) {
		for ( UmDesignEntryKeyByProductId define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.productId;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
