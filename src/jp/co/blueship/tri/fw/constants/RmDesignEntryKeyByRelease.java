package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum RmDesignEntryKeyByRelease implements IDesignEntryKey {

	/**
	 * リリース履歴一覧画面の1ページの行数
	 */
	maxPageNumberByHistoryList(),
	/**
	 * リリース取消トップ画面の1ページの行数
	 */
	maxPageNumberByCancelTop(),
	/**
	 * ダウンロードファイルパス
	 */
	downloadPath(),
	/**
	 * ダウンロードファイルプレフィックス
	 */
	relDownLoadFilePrefix(),
	/**
	 * リリース状況照会画面の再描画間隔（ミリ秒）
	 */
	reloadInterval(),
	/**
	 * リリース状況照会画面のタスクイメージの１行分
	 */
	taskMaxColumns(),
	/**
	 * リリース資産ファイルの文字コード
	 */
	assetArchiveCharset(),
	/**
	 * マージ順とRP番号の区切り文字
	 */
	mergeOrderSeparatorChar(),
	/**
	 * マージ順で未選択を示す文字
	 */
	mergeOrderUnSelectedChar(),
	/**
	 * 申請番号のプレフィックス
	 */
	numberingPrefix(),
	;

	public static RmDesignEntryKeyByRelease getValue( String name ) {
		for ( RmDesignEntryKeyByRelease define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return RmDesignBeanId.release;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
