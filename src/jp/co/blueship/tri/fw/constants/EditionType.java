package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils;

public enum EditionType {
	
	/**
	 * 開発者バージョン
	 */
	Developer( "trinity+Developer_Edition" ) ,
	/**
	 * スタンダード
	 */
	Standard( "trinity+Standard_Edition" ) ;

	private String value = null;

	private EditionType( String value ) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public static EditionType getValue( String name ) {
		for ( EditionType define : values() ) {
			if ( define.getValue().equals( name ) )
				return define;
		}
		
		return null;
	}
	
	public static EditionType getEncryptionValue( String name ) {
		
		for ( EditionType define : values() ) {
			String defineHash = EncryptionUtils.getHashStringEncodedSha512( define.getValue() ) ;
			if ( defineHash.equals( name ) )
				return define;
		}
		
		return null;
	}
	
}
