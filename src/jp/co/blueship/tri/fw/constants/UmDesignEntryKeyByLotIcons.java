package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）web resourcesのEntry-Keyの列挙型です。
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public enum UmDesignEntryKeyByLotIcons implements IDesignEntryKey {

	Default("icon_Lot_Theme-01.png"),
	;

	private String key = null ;

	private UmDesignEntryKeyByLotIcons( String key ) {
		this.key = key;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.lotIcons;
	}

	@Override
	public String getKey() {
		return this.key;
	}

}
