package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum SmDesignEntryKeyByTask implements IDesignEntryKey {

	/**
	 * 業務シーケンスの終了判定の間隔（ミリ秒）
	 */
	reloadInterval(),
	/**
	 * 業務シーケンスの最大同時実行数
	 */
	maxConcurrentBusinessSequenceNum();


	public static SmDesignEntryKeyByTask getValue( String name ) {
		for ( SmDesignEntryKeyByTask define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return SmDesignBeanId.task;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
