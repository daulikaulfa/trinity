package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 * @version V4.02.00
 * @author anh.Nguyen Duc
 *
 */
public enum DcmDesignEntryKeyByReport implements IDesignEntryKey {

	/**
	 * レポート番号のプレフィックス
	 */
	reportNumberingPrefix(),
	/**
	 * レポートファイルの拡張子
	 */
	reportFileExtension(),
	/**
	 * レポートファイルの出力先パス
	 */
	reportFilePath(),
	/**
	 * レポートファイルのダウンロードＵＲＬ
	 */
	reportFileURL(),
	/**
	 * レポートファイルのプレフィックス
	 */
	reportFilePrefix(),
	/**
	 * 変更管理・申請台帳一覧画面の1ページの行数
	 */
	maxPageNumberByReportApplyList(),
	/**
	 * レポート・資産管理台帳一覧画面の1ページの行数
	 */
	maxPageNumberByReportAssetList(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） 変更管理・申請台帳
	 */
	reportTemplateFileRelativePathChaLibReportApplyRegister(),
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） 変更管理・申請台帳
	 */
	reportOutputInfoFileRelativePathChaLibReportApplyRegister(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） ビルドパッケージ／資産管理台帳
	 */
	reportTemplateFileRelativePathRelBuildReportAssetRegister(),
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） ビルドパッケージ／資産管理台帳
	 */
	reportOutputInfoFileRelativePathRelBuildReportAssetRegister(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） リリース／資産管理台帳
	 */
	reportTemplateFileRelativePathRelCtlReportAssetRegister(),
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） リリース／資産管理台帳
	 */
	reportOutputInfoFileRelativePathRelCtlReportAssetRegister(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） リリース申請票
	 */
	reportTemplateFileRelativePathRelApplyTicketAssetRegister(),
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） リリース申請票
	 */
	reportOutputInfoFileRelativePathRelApplyTicketAssetRegister(),
	/**
	 * V4レポート用テンプレートファイルパス（テンプレートパスからの相対パス） ビルドパッケージ／資産管理台帳
	 */
	V4ReportTemplateFileRelativePathBuildPackageReportAssetRegister(),
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） ビルドパッケージ／資産管理台帳
	 */
	V4ReportOutputInfoFileRelativePathBuildPackageReportAssetRegister(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） リリースパッケージ／資産管理台帳
	 */
	V4ReportTemplateFileRelativePathReleaseRequestReportAssetRegister(),
	/**
	 * レポート用テンプレートファイルパス（テンプレートパスからの相対パス） リリース申請票
	 */
	V4ReportTemplateFileRelativePathReleasePackageReportAssetRegister(),
	
	/**
	 * レポート用出力情報ファイルパス（テンプレートパスからの相対パス） リリース／資産管理台帳
	 */
	V4ReportOutputInfoFileRelativePathReleasePackageReportAssetRegister(),
	/**
	 * レポートの最大出力行数
	 */
	reportMaxRows(),
	/**
	 * レポートの文字コード
	 */
	charset( ),
	/**
	 * レポートの改行コード
	 */
	linefeedCode( ),
	/*
	 * Temporary Download Folder
	 */
	downloadTempPath( );

	public static DcmDesignEntryKeyByReport getValue( String name ) {
		for ( DcmDesignEntryKeyByReport define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return DcmDesignBeanId.report;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
