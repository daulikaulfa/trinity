package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByChangec implements IDesignEntryKey {

	/**
	 * 変更管理番号のプレフィックス
	 */
	pjtNumberingPrefix( ),
	/**
	 * ロット番号のプレフィックス
	 */
	lotNumberingPrefix(),
	/**
	 * HEAD・ベースラインIDのプレフィックス
	 */
	headBaseLineNumberingPrefix(),
	/**
	 * ロット・ベースラインIDのプレフィックス
	 */
	lotBaseLineNumberingPrefix(),
	/**
	 * 貸出返却申請番号のプレフィックス
	 */
	lendReturnNumberingPrefix(),
	/**
	 * 原本削除申請番号のプレフィックス
	 */
	masterDeleteNumberingPrefix(),
	/**
	 * generate for request saves as draft
	 */
	draftNumberingPrefix(),
	/**
	 * 一覧画面の行数
	 */
	maxPageNumberByList( ),
	/**
	 *  履歴一覧の行数
	 */
	maxPageNumberByHistory(),
	/**
	 * 変更管理トップ画面の1ページの行数
	 */
	maxPageNumberByTopMenu(),
	/**
	 * ロット履歴一覧画面の1ページの行数
	 */
	maxPageNumberByLotHistoryList(),
	/**
	 * ロット詳細画面の1ページの行数
	 */
	maxPageNumberByLotDetailView(),
	/**
	 * ロット履歴詳細画面の1ページの行数
	 */
	maxPageNumberByLotHistoryDetailView(),
	/**
	 * ロットクローズ確認画面の1ページの行数
	 */
	maxPageNumberByLotCloseConfirm(),
	/**
	 * ロット取消確認画面の1ページの行数
	 */
	maxPageNumberByLotCancelConfirm(),
	/**
	 * 変更管理詳細画面の1ページの行数
	 */
	maxPageNumberByPjtDetailView(),
	/**
	 * 変更管理履歴詳細画面の1ページの行数
	 */
	maxPageNumberByPjtHistoryDetailView(),
	/**
	 * 変更管理クローズ確認画面の1ページの行数
	 */
	maxPageNumberByPjtCloseConfirm(),
	/**
	 * 変更管理取消確認画面の1ページの行数
	 */
	maxPageNumberByPjtCancelConfirm(),
	/**
	 * 変更管理テスト完了確認画面の1ページの行数
	 */
	maxPageNumberByPjtTestCompleteConfirm(),
	/**
	 * 変更管理承認一覧画面の1ページの行数
	 */
	maxPageNumberByPjtApproveList(),
	/**
	 * 貸出申請一覧画面の1ページの行数
	 */
	maxPageNumberByLendList(),
	/**
	 * 返却申請一覧画面の1ページの行数
	 */
	maxPageNumberByRtnList(),
	/**
	 * 原本削除申請一覧画面の1ページの行数
	 */
	maxPageNumberByMasterDelList(),
	/**
	 * 原本削除申請履歴一覧画面の1ページの行数
	 */
	maxPageNumberByMasterDelHistoryList(),
	/**
	 * Defsのパス（ホームトップパスからの相対パス）
	 */
	defsRelativePath(),
	/**
	 * 拡張子のタイプ(ascii)
	 */
	extensionTypeAscii(),
	/**
	 * 拡張子のタイプ(binary)
	 */
	extensionTypeBinary(),
	/**
	 * 削除申請アップロードファイルの格納パス（ホームトップパスからの相対パス）
	 */
	delApplyDefUploadRelativePath,
	/**
	 * 削除申請アップロードファイル名
	 */
	delApplyDefUploadFile(),
	/**
	 * DelApplyFile名
	 */
	delApplyDefFile(),
	/**
	 * DelApplyBinaryFile名
	 */
	delApplyBinaryDefFile(),
	/**
	 * DelApplyTempFile名
	 */
	delApplyDefTempFile(),
	/**
	 * DelApplyBinaryTempFile名
	 */
	delApplyBinaryDefTempFile(),
	/**
	 * 貸出申請アップロードファイルの格納パス（ホームトップパスからの相対パス）
	 */
	lendApplyDefUploadRelativePath,
	/**
	 * LendApplyDefFile名
	 */
	lendApplyDefFile(),
	/**
	 * LendApplyTempFile名
	 */
	lendApplyDefTempFile(),
	/**
	 * 返却申請時の添付ファイル格納パス
	 */
	returnAssetAppendFilePath(),
	/**
	 * 返却申請時の添付ファイルのURLリンク
	 */
	returnAssetAppendFileURL(),
	/**
	 * ロット作成時に、ブランチタグ発行直前でＨＥＡＤに付与するバージョンタグ
	 */
	lotBaseVersionTagPrefix(),
	/**
	 * ロット作成時のブランチタグ
	 */
	lotBranchTagPrefix(),
	/**
	 * ロット作成時に、ブランチ発行直後のブランチに付与するバージョンタグ
	 */
	lotVersionTagPrefix(),
	/**
	 * タグの日時形式
	 */
	tagDateFormat(),
	/**
	 * ロット相対パス
	 */
	inOutBox(),
	/**
	 * ロット相対パス
	 */
	workPath(),
	/**
	 * ロット相対パス
	 */
	historyPath(),
	/**
	 * ロット相対パス
	 */
	workspace(),
	/**
	 * ロット相対パス
	 */
	systemInBox(),
	/**
	 * 貸出時、返却フォルダに自動的に空フォルダを作成するか
	 */
	makeEmptyFolderForReturnInfo(),
	/**
	 * 資産の二重貸出許可を選択できる／選択できない
	 */
	selectableAllowAssetDuplication(),
	/**
	 * 資産の二重貸出許可の表示デフォルト
	 */
	defaultAllowAssetDuplication(),
	/**
	 * 資産貸出・削除申請時に、厳密に大文字/小文字でのチェックを行うか
	 */
	checkStrictlyFilePath(),
	/**
	 * 返却申請時に、許可されるファイルパスの最大長
	 */
	checkMaxLengthByFilePath(),
	/**
	 * 資産の返却時に添付ファイルを必須とする／必須としない
	 */
	needRtnAttachFile();

	public static AmDesignEntryKeyByChangec getValue( String name ) {
		for ( AmDesignEntryKeyByChangec define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.changec;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
