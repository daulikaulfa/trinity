package jp.co.blueship.tri.fw.constants;

/**
 *
 * 添付ファイル用定数項目ID
 *
 */
public class AppendFileConstID {

	

	public enum File {

		// 添付ファイル1
		append1("0001"),
		// 添付ファイル2
		append2("2"),
		// 添付ファイル3
		append3("3"),
		// 添付ファイル4
		append4("4"),
		// 添付ファイル5
		append5("5");

		private String value;

		File( String value ) {
			this.value = value;
		}

		public String value() {
			return value;
		}
	}

}

