package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByLogs implements IDesignEntryKey {

	/**
	 * 【変更情報】コンフリクトチェック 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessConflictCheck(),
	/**
	 * 【変更情報】マージコミット 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessMergeCommit(),
	/**
	 * 【変更情報】ロット作成 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessLotEntry(),
	/**
	 * 【変更情報】ロット編集 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessLotModify(),
	/**
	 * 【変更情報】変更承認 成功時にログを削除するか ： "1"以外はログを保存する
	 */
	deleteLogAtSuccessPjtApprove(),
	;

	public static AmDesignEntryKeyByLogs getValue( String name ) {
		for ( AmDesignEntryKeyByLogs define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.logs;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
