package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.02
 * @author Satoshi Sasaki
 *
 */
public enum RmDesignEntryKeyByTaskLogs implements IDesignEntryKey {

	/**
	 * 【タスクフロー】 <!-- タスクフロー定義でシェル実行する場合にログ内容をタスク結果レコードに保存するか ： "0"以外はログを保存する
	 */
	recordingInShellCommandResult(),
;

	

	public static RmDesignEntryKeyByTaskLogs getValue( String name ) {
		for ( RmDesignEntryKeyByTaskLogs define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return RmDesignBeanId.taskLogs;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
