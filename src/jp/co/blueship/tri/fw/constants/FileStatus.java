package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 * Enumeration type of file status.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum FileStatus {
	none("", "", ""),
	Same("EQUAL", "未変更", "FileStatus.equal.text"),
	Add("ADD", "新規", "FileStatus.add.text"),
	Edit("EDIT", "変更", "FileStatus.edit.text"),
	Delete("DELETE", "削除", "FileStatus.delete.text"),
	;

	private String statusId = null;
	private String status = null;
	private String statusV4 = null;

	private FileStatus( String statusId, String status, String statusV4) {
		this.statusId = statusId;
		this.status = status;
		this.statusV4 = statusV4;
	}

	public boolean equals( String value ) {
		FileStatus type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	/**
	 * V3 compatibility.
	 * @deprecated
	 */
	public String getStatus() {
		return this.status;
	}
	
	public String getStatusV4() {
		return this.statusV4;
	}

	public String getStatusId() {
		return this.statusId;
	}

	public static FileStatus value( String statusId ) {
		if ( TriStringUtils.isEmpty( statusId ) ) {
			return none;
		}

		for ( FileStatus value : values() ) {
			if ( value.getStatusId().equals( statusId ) ) {
				return value;
			}
		}

		return none;
	}
}
