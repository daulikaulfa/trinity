package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）web resourcesのEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Satoshi Sasaki
 *
 */
public enum UmDesignEntryKeyByUserIcons implements IDesignEntryKey {

	Default("icon_user_default-01.png"),
	Empy("icon_user_default-removed.png"),
	;

	private String key = null ;

	private UmDesignEntryKeyByUserIcons( String key ) {
		this.key = key;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.userIcons;
	}

	@Override
	public String getKey() {
		return this.key;
	}

}
