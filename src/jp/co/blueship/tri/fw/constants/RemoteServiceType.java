package jp.co.blueship.tri.fw.constants;

public enum RemoteServiceType {

	AGENT_BUILD		( "agentBuild" ) ,
	AGENT_RELEASE	( "agentRelease" ) ;

	private String value = null ;

	private RemoteServiceType( String value ) {
		this.value = value ;
	}

	public String getValue() {
		return this.value ;
	}

	public static RemoteServiceType getValue( String value ) {

		for ( RemoteServiceType type : values() ) {
			if ( type.getValue().equals( value ) ) {
				return type ;
			}
		}

		return null;
	}
}
