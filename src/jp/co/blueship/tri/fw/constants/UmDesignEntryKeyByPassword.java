package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum UmDesignEntryKeyByPassword implements IDesignEntryKey {
	/**
	 * パスワード有効期限
	 */
	passwordDefaultEffectiveTerm(),
	/**
	 * パスワード最低長
	 */
	passwordMinimumLength(),
	/**
	 * パスワード最大長
	 */
	passwordMaxLength(),
	/**
	 * パスワード履歴数
	 */
	passwordHistoryNumber(),
	/**
	 * パスワード許可書式
	 */
	passwordAllowPattern(),
	;

	public static UmDesignEntryKeyByPassword getValue( String name ) {
		for ( UmDesignEntryKeyByPassword define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.password;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
