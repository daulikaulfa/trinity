package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum SmDesignEntryKeyByIntervalTime implements IDesignEntryKey {

	/**
	 * Interval time for progress notification
	 */
	progressNotification(),
	;

	public static SmDesignEntryKeyByIntervalTime getValue( String name ) {
		for ( SmDesignEntryKeyByIntervalTime define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return SmDesignBeanId.intervalTime;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
