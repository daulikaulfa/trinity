
package jp.co.blueship.tri.fw.constants.svc;

/**
 * Enumeration type of service ID.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum ServiceId {
	/**
	 * value method returns none in the case of V3.
	 */
	none										( false, false, false, "" ),
	//----------------------------------------------------------------------------------------------------------
	// SM
	//----------------------------------------------------------------------------------------------------------
	SmLogInService								( false, false, false, "FlowLogInService" ),
	SmLogInServiceChangePassword				( false, false, false, "FlowLogInServiceChangePassword" ),
	SmLogInServiceProductLicenseEdit			( false, false, false, "FlowLogInServiceProductLicenseEdit" ),
	SmProductLicenseEditService					( true, false, true,   "FlowProductLicenseEditService" ),
	SmServerStorageInformationService			( true, false, true,   "FlowServerStorageInformationService" ),
	SmFunctionConductService					( false, false, false, "FlowFunctionConductService" ),
	SmFlowDispatcherService						( false, false, false, "FlowDispatcherService" ),
	//----------------------------------------------------------------------------------------------------------
	// UM
	//----------------------------------------------------------------------------------------------------------
	UmRecentlyViewedLotsService					( false, false, false, "FlowRecentlyViewedLotsService" ),
	UmRecentlyViewedRequestsService				( false, false, false, "FlowRecentlyViewedRequestsService" ),
	UmRecentlyViewedWikiService					( false, false, false, "FlowRecentlyViewedWikiService" ),
	UmSearchFilterService						( false, false, false, "FlowSearchFilterService" ),
	UmSearchFilterCreationService				( false, false, false, "FlowSearchFilterCreationService" ),
	UmSearchFilterEditService					( false, false, false, "FlowSearchFilterEditService" ),
	UmSearchFilterRemovalService				( false, false, false, "FlowSearchFilterRemovalService" ),
	UmGlobalSearchService						( false, false, false, "FlowGlobalSearchService" ),
	UmGlobalSearchServiceExportToFile			( false, false, false, "FlowGlobalSearchServiceExportToFile" ),
	UmSearchThisSiteService						( false, false, false, "FlowSearchThisSiteService" ),
	UmSearchThisSiteServiceExportToFile			( false, false, false, "FlowSearchThisSiteServiceExportToFile" ),
	UmPrintSearchThisSiteService				( false, false, false, "FlowPrintSearchThisSiteService" ),
	UmSearchThisLotService						( false, false, false, "FlowSearchThisLotService" ),
	UmSearchThisLotServiceExportToFile			( false, false, false, "FlowSearchThisLotServiceExportToFile" ),
	UmPrintSearchThisLotService					( false, false, false, "FlowPrintSearchThisLotService" ),
	UmDashBoardService							( false, true, false, "FlowDashboardService" ),
	UmV3DashBoardService							( false, false, false, "FlowV3DashboardService" ),

	UmThemeSettingsService						( true, false, true, "FlowThemeSettingsService" ),
	UmCategoryListService						( true, true, true, "FlowCategoryListService" ),
	UmCategoryRemovalService					( true, false, false, "FlowCategoryRemovalService" ),
	UmCategoryCreationService					( true, false, false, "FlowCategoryCreationService" ),
	UmCategoryEditService						( true, false, false, "FlowCategoryEditService" ),
	UmMilestoneListService						( true, true, true, "FlowMilestoneListService" ),
	UmMilestoneRemovalService					( true, false, false, "FlowMilestoneRemovalService" ),
	UmMilestoneCreationService					( true, false, false, "FlowMilestoneCreationService" ),
	UmMilestoneEditService						( true, false, false, "FlowMilestoneEditService" ),
	UmGanttChartService							( true, true, true, "FlowGanttChartService" ),
	UmGanttChartServiceExportToFile				( true, false, false, "FlowGanttChartServiceExportToFile" ),
	UmProjectSettingsEditService				( true, false, true, "FlowProjectSettingsEditService" ),
	UmUserListService							( true, true, true, "FlowUserListService" ),
	UmUserListServiceExportToFile				( true, false, false, "FlowUserListServiceExportToFile" ),
	UmUserRemovalService						( true, false, false, "FlowUserRemovalService" ),
	UmUserCreationService						( true, false, true, "FlowUserCreationService" ),
	UmUserEditService							( true, false, true, "FlowUserEditService" ),
	UmUserBatchUpdateService					( true, false, true, "FlowUserBatchUpdateService" ),
	UmUserTemplateDownloadService				( true, false, false, "FlowUserTemplateDownloadService" ),
	UmDepartmentListService						( true, true, true, "FlowDepartmentListService" ),
	UmDepartmentRemovalService					( true, false, false, "FlowDepartmentRemovalService" ),
	UmDepartmentCreationService					( true, false, true, "FlowDepartmentCreationService" ),
	UmDepartmentEditService						( true, false, true, "FlowDepartmentEditService" ),
	UmGroupListService							( true, true, true, "FlowGroupListService" ),
	UmGroupRemovalService						( true, false, false, "FlowGroupRemovalService" ),
	UmGroupCreationService						( true, false, true, "FlowGroupCreationService" ),
	UmGroupEditService							( true, false, true, "FlowGroupEditService" ),
	UmRoleListService							( true, true, true, "FlowRoleListService" ),
	UmRoleRemovalService						( true, false, false, "FlowRoleRemovalService" ),
	UmRoleCreationService						( true, false, true, "FlowRoleCreationService" ),
	UmRoleEditService							( true, false, true, "FlowRoleEditService" ),
	UmPersonalSettingsEditService				( false, false, true, "FlowPersonalSettingsEditService" ),
	UmProfileDetailsService						( false, true, true, "FlowProfileDetailsService" ),
	UmProfileEditService						( false, false, true, "FlowProfileEditService" ),
	UmMemberListService							( false, false, true, "FlowMemberListService" ),
	UmProgressNotificationService				( false, false, false, "FlowProgressNotificationService"),
	UmWikiDetailsService						( true, false, true, "FlowWikiDetailsService"),
	UmPrintWikiDetailsService					( false, false, false, "FlowPrintWikiDetailsService"),
	UmWikiListService							( true, false, true, "FlowWikiListService"),
	UmWikiCreationService						( true, false, true, "FlowWikiCreationService"),
	UmWikiEditService							( true, false, true, "FlowWikiEditService"),
	UmWikiRemovalService						( true, false, false, "FlowWikiRemovalService"),
	UmWikiHistoryListService					( true, false, true, "FlowWikiHistoryListService"),
	UmWikiVersionDetailsService					( true, false, true, "FlowWikiVersionDetailsService"),
	UmPrintWikiVersionComparisonService			( false, false, false, "FlowPrintWikiVersionComparisonService"),
	UmWikiVersionComparisonService				( true, false, true, "FlowWikiVersionComparisonService"),
	UmWikiAttachmentUploadService				( true, false, true, "FlowWikiAttachmentUploadService"),
	UmWikiAttachmentDownloadService				( true, false, true, "FlowWikiAttachmentDownloadService"),
	//----------------------------------------------------------------------------------------------------------
	// AM
	//----------------------------------------------------------------------------------------------------------
	AmLotDetailsService							( true, false, true, "FlowLotDetailsService" ),
	AmLotClosedDetailsService					( true, false, true, "FlowLotClosedDetailsService" ),
	AmPrintLotDetailsService					( false, false, false, "FlowPrintLotDetailsService" ),
	AmLotCreationService						( true, false, true, "FlowLotCreationService" ),
	AmLotEditService							( true, false, true, "FlowLotEditService" ),
	AmLotRemovalService							( true, false, false, "FlowLotRemovalService" ),
	AmLotCloseService							( true, false, false, "FlowLotCloseService" ),
	AmLotResourceListService					( true, false, true, "FlowLotResourceListService" ),
	AmPrintLotResourceListService				( false,false, false, "FlowPrintLotResourceListService" ),
	AmLotResourceFileHistoryDetailsService		( true, false, true, "FlowLotResourceFileHistoryDetailsService" ),
	AmPrintLotResourceFileHistoryDetailsService	( false,false, false, "FlowPrintLotResourceFileHistoryDetailsService" ),
	AmChangePropertyListService					( true, true, true, "FlowChangePropertyListService" ),
	AmChangePropertyListServiceExportToFile		( true, false, false, "FlowChangePropertyListServiceExportToFile" ),
	AmPrintChangePropertyListService			( false, false, false, "FlowPrintChangePropertyListService" ),
	AmChangePropertyDetailsService				( true, false, true, "FlowChangePropertyDetailsService" ),
	AmPrintChangePropertyDetailsService			( false, false, false, "FlowPrintChangePropertyDetailsService" ),
	AmChangePropertyCreationService				( true, false, true, "FlowChangePropertyCreationService" ),
	AmChangePropertyEditService					( true, false, true, "FlowChangePropertyEditService" ),
	AmChangePropertyRemovalService				( true, false, false, "FlowChangePropertyRemovalService" ),
	AmChangePropertyTestCompletionService   	( true, false, false, "FlowChangePropertyTestCompletionService" ),
	AmChangePropertyCloseService				( true, false, false, "FlowChangePropertyCloseService" ),
	AmChangePropertyCloseServiceMultipleClose	( true, false, false, "FlowChangePropertyCloseServiceMultipleClose" ),
	AmCheckinOutRequestListService				( true, true, true, "FlowCheckinOutRequestListService" ),
	AmCheckinOutRequestListServiceExportToFile	( true, false, false, "FlowCheckinOutRequestListServiceExportToFile" ),
	AmPrintCheckinOutRequestListService			( false, false, false, "FlowPrintCheckinOutRequestListService" ),
	AmCheckoutRequestService					( true, false, true, "FlowCheckoutRequestService" ),
	AmCheckoutRequestEditService				( true, false, true, "FlowCheckoutRequestEditService" ),
	AmCheckoutRequestDetailsService				( true, false, true, "FlowCheckoutRequestDetailsService" ),
	AmPrintCheckoutRequestDetailsService		( false, false, false, "FlowPrintCheckoutRequestDetailsService" ),
	AmCheckoutRequestRemovalService				( true, false, false, "FlowCheckoutRequestRemovalService" ),
	AmCheckinRequestService						( true, false, true, "FlowCheckinRequestService" ),
	AmCheckinRequestRemovalService				( true, false, false, "FlowCheckinRequestRemovalService" ),
	AmCheckinRequestDetailsService				( true, false, true, "FlowCheckinRequestDetailsService" ),
	AmPrintCheckinRequestDetailsService			( false, false, false, "FlowPrintCheckinRequestDetailsService" ),
	AmCheckinRequestAttachmentDownloadService	( true, false, false, "FlowCheckinRequestAttachmentDownloadService" ),
	AmCheckinRequestApprovalPendingCancellationService
												( true, false, false, "FlowCheckinRequestApprovalPendingCancellationService" ),
	AmCheckinRequestProcessingStatusService		( true, false, false, "FlowCheckinRequestProcessingStatusService" ),
	AmRequestResourceFileComparisonService		( true, false, false, "FlowRequestResourceFileComparisonService" ),
	AmPrintRequestResourceFileComparisonService
												( false, false, false, "FlowPrintRequestResourceFileComparisonService" ),
	AmRemovalRequestListService					( true, true, true, "FlowRemovalRequestListService" ),
	AmPrintRemovalRequestListService			( false, false, false, "FlowPrintRemovalRequestListService" ),
	AmRemovalRequestListServiceExportToFile		( true, false, false, "FlowRemovalRequestListServiceExportToFile" ),
	AmRemovalRequestRemovalService				( true, false, false, "FlowRemovalRequestRemovalService" ),
	AmRemovalRequestService						( true, false, true, "FlowRemovalRequestService" ),
	AmRemovalRequestEditService                 ( true, false, true, "FlowRemovalRequestEditService" ),
	AmRemovalRequestDetailsService				( true, false, true, "FlowRemovalRequestDetailsService" ),
	AmPrintRemovalRequestDetailsService			( false, false, false, "FlowPrintRemovalRequestDetailsService" ),
	AmRemovalRequestApprovalPendingCancellationService
												( true, false, false, "FlowRemovalRequestApprovalPendingCancellationService" ),
	AmChangeApprovalPendingListService			( true, true, true, "FlowChangeApprovalPendingListService" ),
	AmPrintChangeApprovalPendingListService		( false, false, false, "FlowPrintChangeApprovalPendingListService" ),
	AmChangeApprovalPendingListServiceExportToFile
												( true, false, false, "FlowChangeApprovalPendingListServiceExportToFile" ),
	AmChangeApprovalServiceMultipleApproval		( true, false, false, "FlowChangeApprovalServiceMultipleApproval" ),
	AmChangeApprovalPendingDetailsService		( true,  false, true, "FlowChangeApprovalPendingDetailsService" ),
	AmChangeApprovalService						( true, false, false, "FlowChangeApprovalService" ),
	AmPrintChangeApprovalPendingDetailsService
												( false, false, false, "FlowPrintChangeApprovalPendingDetailsService" ),
	AmChangeApprovedListService					( true, true, true, "FlowChangeApprovedListService" ),
	AmPrintChangeApprovedListService			( false, false, false, "FlowPrintChangeApprovedListService" ),
	AmChangeApprovedListServiceExportToFile		( true, false, false, "FlowChangeApprovedListServiceExportToFile" ),
	AmChangeApprovedDetailsService				( true, false, true, "FlowChangeApprovedDetailsService" ),
	AmChangeApprovalReturnToSubmitterService	( true, false, false, "FlowChangeApprovalPendingReturnToSubmitterService" ),
	AmChangeApprovedReturnToPendingService		( true, false, false, "FlowChangeApprovedReturnToPendingService" ),
	AmPrintChangeApprovedDetailsService			( false, false, false, "FlowPrintChangeApprovedDetailsService" ),
	AmMergeLotListService						( true, true, true, "FlowMergeLotListService" ),
	AmMergeLotListServiceExportToFile			( true, false, false, "FlowMergeLotListServiceExportToFile" ),
	AmPrintMergeLotListService					( false, false, false, "FlowPrintMergeLotListService" ),
	AmConflictCheckService						( true, false, true, "FlowConflictCheckService" , "FlowChaLibConflictCheckService"),
	AmConflictCheckResourceFileResultsService	( true, false, true, "FlowConflictCheckResourceFileResultsService" ),
	AmPrintConflictCheckResourceFileResultsService
												( false, false, false, "FlowPrintConflictCheckResourceFileResultsService" ),
	AmMergeResourceFileComparisonService		( true, false, false, "FlowMergeResourceFileComparisonService" ),
	AmPrintMergeResourceFileComparisonService	( false, false, false, "FlowPrintMergeResourceFileComparisonService" ),
	AmMergeService								( true, false, true, "FlowMergeService" , "FlowChaLibMergeCommitService"),
	AmMergeHistoryDetailsService				( true, false, true, "FlowMergeHistoryDetailsService" ),
	AmPrintMergeHistoryDetailsService			( false, false, false, "FlowPrintMergeHistoryDetailsService" ),
	AmChangePropertyOverviewService				( false, false, false, "FlowChangePropertyOverviewService" ),
	AmCheckinOutRequestOverviewService			( false, false, false, "FlowCheckinOutRequestOverviewService" ),
	AmRemovalRequestOverviewService				( false, false, false, "FlowRemovalRequestOverviewService"),

	//----------------------------------------------------------------------------------------------------------
	// BM
	//----------------------------------------------------------------------------------------------------------
	BmBuildPackageListService					( true, true, true, "FlowBuildPackageListService" ),
	BmBuildPackageListServiceExportToFile		( true, false, false, "FlowBuildPackageListServiceExportToFile" ),
	BmPrintBuildPackageListService				( false, false, false, "FlowPrintBuildPackageListService" ),
	BmBuildPackageRemovalService				( true, false, false, "FlowBuildPackageRemovalService" ),
	BmBuildPackageCreationService				( true, false, true, "FlowBuildPackageCreationService", "FlowRelUnitEntryService" ),
	BmBuildPackageDetailsService				( true, false, true, "FlowBuildPackageDetailsService" ),
	BmPrintBuildPackageDetailsService			( false, false, false, "FlowPrintBuildPackageDetailsService" ),
	BmBuildPackageCloseService   				( true, false, false, "FlowBuildPackageCloseService" , "FlowRelUnitCloseRequestService"),
	BmBuildPackageCreationProcessingStatusService
												( true, false, false, "FlowBuildPackageCreationProcessingStatusService" ),
	BmBuildPackageOverviewService				( false, false, false, "FlowBuildPackageOverviewService" ),
	//----------------------------------------------------------------------------------------------------------
	// RM
	//----------------------------------------------------------------------------------------------------------
	RmReleaseRequestListService					( true, true, true, "FlowReleaseRequestListService" ),
	RmPrintReleaseRequestListService			( false, false, false, "FlowPrintReleaseRequestListService" ),
	RmReleaseRequestListServiceExportToFile		( true, false, false, "FlowReleaseRequestListServiceExportToFile" ),
	RmReleaseRequestRemovalService				( true, false, false, "FlowReleaseRequestRemovalService" ),
	RmReleaseRequestService						( true, false, true, "FlowReleaseRequestService" ),
	RmReleaseRequestEditService					( true, false, true, "FlowReleaseRequestEditService" ),
	RmReleaseRequestApprovalPendingListService	( true, true, true, "FlowReleaseRequestApprovalPendingListService" ),
	RmPrintReleaseRequestApprovalPendingListService
												( false, false, false, "FlowPrintReleaseRequestApprovalPendingListService" ),
	RmReleaseRequestApprovalPendingListServiceExportToFile
												( true, false, false, "FlowReleaseRequestApprovalPendingListServiceExportToFile" ),
	RmReleaseRequestApprovalPendingDetailsService
												( true, false, true, "FlowReleaseRequestApprovalPendingDetailsService" ),
	RmPrintReleaseRequestApprovalPendingDetailsService
												( false, false, false, "FlowPrintReleaseRequestApprovalPendingDetailsService" ),
	RmReleaseRequestApprovalPendingReturnToSubmitterService
												( true, false, false, "FlowReleaseRequestApprovalPendingReturnToSubmitterService" ),
	RmReleaseRequestAttachmentDownloadService	( true, false, false, "FlowReleaseRequestAttachmentDownloadService" ),
	RmReleaseRequestApprovedReturnToSubmitterService
												( true, false, false, "FlowReleaseRequestApprovedReturnToSubmitterService" ),
	RmReleaseRequestApprovalRejectionService	( true, false, false, "FlowReleaseRequestApprovalRejectionService" ),
	RmReleaseRequestApprovalService				( true, false, false, "FlowReleaseRequestApprovalService" ),
	RmReleaseRequestApprovedReturnToPendingService
												( true, false, false, "FlowReleaseRequestApprovedReturnToPendingService" ),
	RmReleaseRequestCloseService				( true, false, false, "FlowReleaseRequestCloseService" ),
	RmReleaseRequestApprovalPendingCancellationService
												( true, false, false, "FlowReleaseRequestApprovalPendingCancellationService" ),
	RmReleaseRequestApprovedListService
												( true, true, true, "FlowReleaseRequestApprovedListService" ),
	RmPrintReleaseRequestApprovedListService
												( false, false, false, "FlowPrintReleaseRequestApprovedListService" ),
	RmReleaseRequestApprovedListServiceExportToFile
												( true, false, false, "FlowReleaseRequestApprovedListServiceExportToFile" ),
	RmReleaseRequestApprovedDetailsService
												( true, false, true, "FlowReleaseRequestApprovedDetailsService" ),
	RmPrintReleaseRequestApprovedDetailsService
												( false, false, false, "FlowPrintReleaseRequestApprovedDetailsService" ),
	RmReleasePackageListService					( true, true, true, "FlowReleasePackageListService" ),
	RmReleasePackageListServiceExportToFile		( true, false, false, "FlowReleasePackageListServiceExportToFile" ),
	RmPrintReleasePackageListService			( false, false, false, "FlowPrintReleasePackageListService" ),
	RmReleasePackageRemovalService				( true, false, false, "FlowReleasePackageRemovalService" ),
	RmReleasePackageCreationService				( true, false, true, "FlowReleasePackageCreationService", "FlowRelCtlEntryService" ),
	RmFlowReleasePackageDetailsService			( true, false, true, "FlowReleasePackageDetailsService" ),
	RmPrintFlowReleasePackageDetailsService		( false, false, false, "FlowPrintReleasePackageDetailsService" ),
	RmReleasePackageCreationProcessingStatusService
												( true, false, false, "FlowReleasePackageCreationProcessingStatusService" ),
	RmReleaseRequestOverviewService				( false, false, false, "FlowReleaseRequestOverviewService" ),
	RmReleasePackageOverviewService				( false, false, false, "FlowReleasePackageOverviewService" ),
	//----------------------------------------------------------------------------------------------------------
	// DM
	//----------------------------------------------------------------------------------------------------------
	DmFlowDeploymentJobListService				( true, true, true, "FlowDeploymentJobListService" ),
	DmFlowDeploymentJobListServiceOutputShell	( false, false, false, "FlowDeploymentJobListServiceOutputShell" ),
	DmFlowDeploymentJobListServiceExportToFile	( true, false, false, "FlowDeploymentJobListServiceExportToFile" ),
	DmFlowDeploymentJobDetailsService			( true, false, true, "FlowDeploymentJobDetailsService" ),
	DmFlowDeploymentJobLatestUpdatesService		( true, false, true, "FlowDeploymentJobLatestUpdatesService" ),
	DmFlowDeploymentJobRemovalService			( true, false, false, "FlowDeploymentJobRemovalService" ),
	DmFlowDeploymentJobCancellationService		( true, false, false, "FlowDeploymentJobCancellationService" ),
	DmFlowDeploymentJobCreationService			( true, false, true, "FlowDeploymentJobCreationService" ),
	DmFlowDeploymentJobEditService				( true, false, true, "FlowDeploymentJobEditService" ),
	DmFlowPrintDeploymentJobListService			( false, false, false, "FlowPrintDeploymentJobListService" ),
	DmFlowPrintDeploymentJobDetailsService		( false, false, false, "FlowPrintDeploymentJobDetailsService" ),
	DmFlowPrintDeploymentJobLatestUpdatesService
												( false, false, false, "FlowPrintDeploymentJobLatestUpdatesService" ),
	//----------------------------------------------------------------------------------------------------------
	// DCM
	//----------------------------------------------------------------------------------------------------------
	DcmReportListService						( true, false, false, "FlowReportListService" ),
	DcmPrintReportListService					( false, false, false, "FlowPrintReportListService" ),
	DcmReportListServiceExportToFile			( true, false, false, "FlowReportListServiceExportToFile" ),
	DcmReportDownloadService					( true, false, false, "FlowReportDownloadService" ),
	DcmReportMultipleDownloadService			( true, false, false, "FlowReportMultipleDownloadService" ),
	DcmReportRemovalService						( true, false, false, "FlowReportRemovalService" ),
	DcmReportMultipleRemovalService				( true, false, false, "FlowReportMultipleRemovalService" ),
	DcmReportAssetRegisterCreationService		( true, false, false, "FlowReportAssetRegisterCreationService" ),
	DcmReportBuildPackageCreationService		( true, false, false, "FlowReportBuildPackageCreationService" ),
	DcmReportReleaseRequestCreationService		( true, false, false, "FlowReportReleaseRequestCreationService" ),
	DcmReportReleaseCreationService				( true, false, false, "FlowReportReleaseCreationService" ),
	;

	private boolean authEnabled = true;
	private boolean statefulSession = false;
	private boolean sessionGC = false;
	private String value = null;
	private String ValueOfV3 = null;

	/**
	 * Constructor
	 *
	 * @param authEnabled If True, Enable the authorization function of the Service.
	 * @param statefulSession If True, Allways Session do not initialization.
	 * @param sessionGC If the initial call of this service (init), clears the Session of stateles service.
	 * @param value Service ID
	 */
	private ServiceId( boolean authEnabled, boolean statefulSession, boolean sessionGC, String... value ) {
		this.authEnabled = authEnabled;
		this.statefulSession = statefulSession;
		this.sessionGC = sessionGC;

		this.value = value[0];
		if ( 1 < value.length ) {
			this.ValueOfV3 = value[1];
		}
	}

	/**
	 * If True, Enable the authorization function of the Service
	 * <br>
	 * <br>Trueの場合、このサービスの認可機能が有効になります。
	 *
	 * @return
	 */
	public boolean authEnabled() {
		return this.authEnabled;
	}

	/**
	 * If True, Allways Session do not initialization.
	 * <br>
	 * <br>Trueの場合、Sessionはstatefulとなり、Sessionが保持される間、前回の値が保持されます。
	 *
	 * @return
	 */
	public boolean statefulSession() {
		return this.statefulSession;
	}

	/**
	 * If the initial call of this service (init), clears the Session of stateles service.
	 * <br>
	 * <br>Trueの場合、このサービスが初期化されるタイミングで他のstatelessなSessionをクリアします。
	 *
	 * @return
	 */
	public boolean sessionGC() {
		return this.sessionGC;
	}

	public String value() {
		return this.value;
	}

	public String valueOfV3() {
		return this.ValueOfV3;
	}

	public static ServiceId value( String value ) {
		for ( ServiceId service : values() ) {
			if ( service.value().equals( value ) ) {
				return service;
			}
		}

		return none;
	}
}
