package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByMerge implements IDesignEntryKey {

	/**
	 * マージ・コンフリクトチェック実行の前提として、「テスト完了」ステータスを必要とするか
	 */
	checkStatusTestComplete() ,
	/**
	 * マージ時のコミットコメントに用いるメッセージファイル名
	 */
	scmComment() ,
	/**
	 * コンフリクトチェック時のワーク領域に用いるフォルダ名
	 */
	mergeTemp() ,
	/**
	 * コンフリクトチェックのワーク領域（ブランチ資産保存場所）に用いるフォルダ名
	 */
	mergeTempBranchCheckOut() ,
	/**
	 * マージコミット時のワーク領域に用いるフォルダ名
	 */
	commitTemp() ,
	/**
	 * コンフリクトチェック時のマージ結果出力に用いるファイル名
	 */
	mergeResultFileName() ,
	/**
	 * コンフリクトチェック時のログファイルに用いる拡張子
	 */
	conflictCheckLogFileExtension() ;

	public static AmDesignEntryKeyByMerge getValue( String name ) {
		for ( AmDesignEntryKeyByMerge define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.merge;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
