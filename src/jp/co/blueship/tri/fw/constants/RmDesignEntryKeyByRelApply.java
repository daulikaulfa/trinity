package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum RmDesignEntryKeyByRelApply implements IDesignEntryKey {

	/**
	 * リリース申請機能を使用するか "0":OFF, "0"以外:ON
	 */
	enable(),
	/**
	 * 申請番号のプレフィックス
	 */
	numberingPrefix(),
	/**
	 * 発行前の仮番号のプレフィックス
	 */
	tempNumberingPrefix(),
	/**
	 * リリース申請時の添付ファイル格納パス
	 */
	relApplyAppendFilePath(),
	/**
	 * リリース申請時の添付ファイルのURLリンク
	 */
	relApplyAppendFileURL();

	public static RmDesignEntryKeyByRelApply getValue( String name ) {
		for ( RmDesignEntryKeyByRelApply define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return RmDesignBeanId.relApply;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
