package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum AmDesignEntryKeyByScm implements IDesignEntryKey {

	/**
	 * バージョン管理に利用するＳＣＭアプリケーションの種別 "SVN"
	 */
	useApplication(),
	/**
	 * SVNのステータス取得モード ステータスを資産１件ずつ取得するか、モジュール単位で一括取得するかの閾値
	 * Ex:"100"の場合、処理する資産数が100件を超えたら一括取得
	 */
	waterMarkStatusMget(),
	/**
	 * SVN利用時に管理対象外となるファイル
	 */
	ignorePatterns();

	public static AmDesignEntryKeyByScm getValue( String name ) {
		for ( AmDesignEntryKeyByScm define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return AmDesignBeanId.scm;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
