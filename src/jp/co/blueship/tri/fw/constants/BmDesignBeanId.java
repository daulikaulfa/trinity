package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;

/**
 * デザインシート（ユーザ設計項目）文書情報を識別するためのBeanIdの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 */
public enum BmDesignBeanId implements IDesignBeanId {
	statusId,
	build,
	logs,
	mail,
	bpTopLotRowOrder,
	bpTopLotRowOrderBy,
	bpTopUnitLineOrder,
	bpTopUnitLineOrderBy,
	bpLotHistoryListOrder,
	bpLotHistoryListOrderBy,
	bpHistoryListOrder,
	bpHistoryListOrderBy,
	bpEntryLotSelectOrder,
	bpEntryLotSelectOrderBy,
	bpEntryPjtSelectOrder,
	bpEntryPjtSelectOrderBy,
	bpEntryConfirmPjtListOrder,
	bpEntryConfirmPjtListOrderBy,
	bpGenerateDetailViewLotSelectOrder,
	bpGenerateDetailViewLotSelectOrderBy,
	bpPjtDetailViewAssetApplyListOrder,
	bpPjtDetailViewAssetApplyListOrderBy,
	bpCloseTopOrder,
	bpCloseTopOrderBy,
	bpCloseListOrder,
	bpCloseListOrderBy,
	bpCloseHistoryListOrder,
	bpCloseHistoryListOrderBy,
	bpCloseRequestConfirmOrder,
	bpCloseRequestConfirmOrderBy,
	bpCancelTopOrder,
	bpCancelTopOrderBy,
	bpCancelListOrder,
	bpCancelListOrderBy,
	bpCancelConfirmOrder,
	bpCancelConfirmOrderBy,
	bpHistoryListCount,
	bpCloseListCount,
	bpCloseHistoryListCount,
	bpCancelListCount,
	flowRelUnitInfoInputCheck,
	flowRelUnitCloseCheck,
	flowRelUnitCancelCheck,
	;

	private BmDesignBeanId() {
	}

	@Override
	public String getBeanId() {
		return this.name();
	}

	@Override
	public String getSheetId() {
		return Sheet.rmDesignSheet.value();
	}

}
