package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public enum UmDesignEntryKeyByUserUpload implements IDesignEntryKey {
    encoding(),
    maxLines(),
    ;

    public static UmDesignEntryKeyByUserUpload getValue( String name ) {
        for ( UmDesignEntryKeyByUserUpload define : values() ) {
            if ( define.toString().equals(name) )
                return valueOf( name );
        }

        return null;
    }

    @Override
    public IDesignBeanId getDesignBeanId() {
        return UmDesignBeanId.userUpload;
    }

    @Override
    public String getKey() {
        return this.name();
    }
}
