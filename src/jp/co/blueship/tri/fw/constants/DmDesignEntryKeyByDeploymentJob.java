package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 *
 * @version V4.01.00
 * @author Akahoshi
 *
 */
public enum DmDesignEntryKeyByDeploymentJob implements IDesignEntryKey {
	/**
	 * Deployment ID Prefix
	 */
	numberingPrefix(),
	;

	public static DmDesignEntryKeyByDeploymentJob getValue( String name ) {
		for ( DmDesignEntryKeyByDeploymentJob define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return DmDesignBeanId.deploymentJob;
	}

	@Override
	public String getKey() {
		return this.name();
	}
}
