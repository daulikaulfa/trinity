package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum UmDesignEntryKeyByMail implements IDesignEntryKey {

	/**
	 *  メール ＳＭＴＰサーバのホスト名
	 */
	mailSmtpHost(),
	/**
	 *  メール ＳＭＴＰサーバのポート番号
	 */
	mailSmtpPort(),
	/**
	 *  メール セキュリティの種類
	 */
	mailSecurityType(),
	/**
	 *  メール 認証ユーザＩＤ
	 */
	mailAuthUserID(),
	/**
	 *  メール送信者アドレス
	 */
	mailOwnerAddress(),
	/**
	 *  メール送信者名
	 */
	mailOwnerName(),
	;

	public static UmDesignEntryKeyByMail getValue( String name ) {
		for ( UmDesignEntryKeyByMail define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return UmDesignBeanId.mail;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
