package jp.co.blueship.tri.fw.constants;

/**
  * @version V4.03.00
  * @author Cuong Nguyen
 */
public enum  LanguageType {
	EN("en"),
	JA("ja");

	private String value;
	LanguageType(String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
