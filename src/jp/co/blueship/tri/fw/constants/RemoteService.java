package jp.co.blueship.tri.fw.constants;

public enum RemoteService {

	BUILD_TASK_SERVICE("IBuildTaskService", "BuildTaskService", "buildRemoteTaskServiceExporter"),
	RELEASE_TASK_SERVICE("IReleaseTaskService", "ReleaseTaskService", "releaseRemoteTaskServiceExporter");

	private String id = null;
	private String name = null;
	private String execSvc = null;

	private RemoteService(String id, String name, String execSvc) {
		this.id = id;
		this.name = name;
		this.execSvc = execSvc;
	}

	public String getId() {
		return this.id;
	}

	public String getServiceName() {
		return this.name;
	}

	public String getExecSvc() {
		return this.execSvc;
	}

	public static RemoteService getValueById(String id) {

		for (RemoteService type : values()) {
			if (type.getId().equals(id)) {
				return type;
			}
		}

		return null;
	}
}
