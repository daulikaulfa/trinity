package jp.co.blueship.tri.fw.constants;

public enum ServerType {
	
	/**
	 * 本体サーバ
	 */
	CORE("trinity"),
	/**
	 * エージェント
	 */
	AGENT("agent");

	private String value = null;

	private ServerType( String value ) {
		this.value = value;
	}
	
	public String getValue() {
		return this.value;
	}
	
	public static ServerType getValue( String name ) {
		for ( ServerType define : values() ) {
			if ( define.getValue().equals( name ) )
				return define;
		}
		
		return null;
	}
}
