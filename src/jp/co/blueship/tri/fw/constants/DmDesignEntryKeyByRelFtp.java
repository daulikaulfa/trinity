package jp.co.blueship.tri.fw.constants;

import jp.co.blueship.tri.fw.dao.oxm.IDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.IDesignEntryKey;

/**
 * デザインシート（ユーザ設計項目）文書情報のEntry-Keyの列挙型です。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 */
public enum DmDesignEntryKeyByRelFtp implements IDesignEntryKey {
	/**
	 * ＦＴＰサーバが有効かどうか
	 */
	ftpServer(),
	/**
	 * ＦＴＰホスト
	 */
	host(),
	/**
	 * ＦＴＰサービスのポート番号
	 */
	port(),
	/**
	 * ＦＴＰサービスのユーザアカウント
	 */
	user(),
	/**
	 * ＦＴＰサービスのＰＡＳＶモード
	 */
	pasv(),
	/**
	 * 文字コードエンコーディング
	 */
	controlEncoding(),
	/**
	 * 転送元FTPパス
	 */
	srcFtpPath(),
	/**
	 * 転送元ローカルパス
	 */
	srcLocalPath(),
	/**
	 * 転送先ローカルパス
	 */
	destLocalPath(),
	/**
	 * 転送元ファイルの拡張子（ファイル名は業務ルール）
	 */
	srcExtension();

	

	public static DmDesignEntryKeyByRelFtp getValue( String name ) {
		for ( DmDesignEntryKeyByRelFtp define : values() ) {
			if ( define.toString().equals(name) )
				return valueOf( name );
		}

		return null;
	}

	@Override
	public IDesignBeanId getDesignBeanId() {
		return DmDesignBeanId.relFtp;
	}

	@Override
	public String getKey() {
		return this.name();
	}

}
