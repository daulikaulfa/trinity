package jp.co.blueship.tri.fw.security;

public class InvalidUserException extends SecurityException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private String actionId = "";
	private String actionName = "";

	InvalidUserException(String actionId, String actionName) {
		super("actionId :" + actionId + ", actionName:" + actionName);
		this.actionId = actionId;
		this.actionName = actionName;
	}

	/**
	 * @return actionId
	 */
	public String getActionId() {
		return actionId;
	}

	/**
	 * @return actionName
	 */
	public String getActionName() {
		return actionName;
	}

}
