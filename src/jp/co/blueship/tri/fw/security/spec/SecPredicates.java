package jp.co.blueship.tri.fw.security.spec;

import jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.predicate.BasePredicate;

public final class SecPredicates {

	public static BasePredicate<String> isMatchedInSHAWith(String password) {

		final String encShaPassword = EncryptionUtils.getHashStringEncodedSha512(password);

		return new BasePredicate<String>() {

			@Override
			public boolean evalute(String input) {
				return TriStringUtils.isEquals(input, encShaPassword);
			}
		};
	}

	public static BasePredicate<String> isMatchedInMD5A1FormatWith(String password, String userID) {

		final String encMD5Password = EncryptionUtils.encodePasswordInA1Format(userID, password);

		return new BasePredicate<String>() {

			@Override
			public boolean evalute(String input) {
				return TriStringUtils.isEquals(input, encMD5Password);
			}
		};
	}

}
