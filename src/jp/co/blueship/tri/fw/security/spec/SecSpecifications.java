package jp.co.blueship.tri.fw.security.spec;

import static jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils.*;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.spec.BaseSpecification;
import jp.co.blueship.tri.fw.cmn.utils.spec.Specification;

/**
 * 認証・認可に関する仕様を定義するクラスです。
 *
 * @author Takayuki Kubo
 *
 */
public final class SecSpecifications {

	/**
	 * 指定されたSHAで暗号化済みのパスワードと一致することを表す仕様を返します。
	 *
	 * @param baseSHAPassword SHAで暗号化されたパスワード
	 *
	 * @return 一致する場合はtrueを、一致しないまたは、検査対象パスワードがnullまたは空文字の場合はfalseを返す仕様オブジェクト。
	 */
	public static Specification<String> specOfMatchingPasswordInSHAWith(final String baseSHAPassword) {

		return new BaseSpecification<String>() {

			@Override
			public boolean isSpecified(String inputPassword) {

				if(TriStringUtils.isEmpty(inputPassword)) {
					return false;
				}

				return baseSHAPassword.equals(getHashStringEncodedSha512(inputPassword));
			}
		};
	}

	/**
	 * 指定されたMD5(A1フォーマット ⇒ ユーザID:レルム:パスワード)で暗号化済みのパスワードと一致することを表す仕様を返します。
	 * @param baseMD5Password MD5で暗号化されたパスワード
	 * @param userID ログインユーザID
	 *
	 * @return 一致する場合はtrueを、一致しないまたは、検査対象パスワードがnullまたは空文字の場合はfalseを返す仕様オブジェクト。
	 */
	public static Specification<String> specOfMatchingPasswordInMD5A1FormatWith(final String baseMD5Password, final String userID) {

		return new BaseSpecification<String>() {

			@Override
			public boolean isSpecified(String inputPassword) {

				if(TriStringUtils.isEmpty(inputPassword)) {
					return false;
				}

				return baseMD5Password.equals(encodePasswordInA1Format(userID, inputPassword));
			}
		};
	}

}
