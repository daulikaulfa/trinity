package jp.co.blueship.tri.fw.security.spec;

import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;

public final class SecFunctions {

	public static final TriFunction<IUserPassHistEntity, String> PASS_HIST_TO_PASSWORD = new TriFunction<IUserPassHistEntity, String>() {

		@Override
		public String apply(IUserPassHistEntity entity) {
			return entity.getUserPass();
		}
	};

}
