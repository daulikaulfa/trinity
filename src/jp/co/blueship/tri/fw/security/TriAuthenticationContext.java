package jp.co.blueship.tri.fw.security;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.security.spi.TriAuthUser;

/**
 * 認証済みユーザへアクセスするためのコンテキストクラスです。 )
 *
 * @author Takayuki Kubo
 *
 */
public class TriAuthenticationContext {

	public static final String BEAN_NAME = "triAuthenticationContext";

	/**
	 * デフォルトコンストラクタ
	 */
	public TriAuthenticationContext() {
		SecurityContextHolder.setStrategyName(SecurityContextHolder.MODE_INHERITABLETHREADLOCAL);
	}

	/**
	 * 認証済みユーザ(ログイン中のユーザ)のユーザIDを返します。
	 *
	 * @return 認証済みユーザID
	 */
	public String getAuthUserID() {

		return authentication().getName();
	}

	/**
	 * 認証済みユーザ(ログイン中のユーザ)のユーザ名を返します。
	 *
	 * @return 認証済みユーザID
	 */
	public String getAuthUserName() {

		Object principal = authentication().getPrincipal();
		PreConditions.assertOf(TriAuthUser.class.isInstance(principal), "Authentication is not instance of TriAuthUser.");

		return ((TriAuthUser)principal).getUserNm();
	}

	private static Authentication authentication() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

		if (authentication == null) {
			throw new IllegalStateException("Current user is not Authenticated.");
		}
		return authentication;
	}

	/**
	 * 認証済みユーザ(ログイン中のユーザ)情報を設定します。
	 *
	 * @param userID ユーザID
	 * @param userName ユーザ名
	 */
	public void setAuthUser(String userID, String userName) {

		UsernamePasswordAuthenticationToken authentication = //
		new UsernamePasswordAuthenticationToken(new TriAuthUser(userID, userName), null);
		SecurityContext context = SecurityContextHolder.createEmptyContext();
		context.setAuthentication(authentication);
		SecurityContextHolder.setContext(context);

	}

	/**
	 * ユーザ認証済みかどうかを返します。
	 *
	 * @return 認証済みの場合はtrueを返す。
	 */
	public boolean isAuthenticated() {

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			return false;
		}

		if (TriStringUtils.isEmpty(authentication.getName())) {
			return false;
		}

		return true;
	}

	/**
	 * 認証済みユーザ(ログイン中のユーザ)情報を消去します。
	 */
	public void clearAuthenticationContext() {
		SecurityContextHolder.clearContext();
	}

}
