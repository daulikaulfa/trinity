package jp.co.blueship.tri.fw.security;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.predicate.TriPredicate;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 機能サービスのチェック関連を扱うクラスです。
 * このクラスが提供する機能は、指定された機能サービスが登録されているかのチェックと
 * ユーザが指定された機能サービスを利用可能かをチェックする機能です。
 *
 * @version V3L10R02
 * @author Takashi Ono
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class SecurityServiceImpl implements SecurityService {

	private IUmFinderSupport umFinderSupport;

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	private ISmFinderSupport smFinderSupport;

	public void setSmFinderSupport(ISmFinderSupport smFinderSupport) {
		this.smFinderSupport = smFinderSupport;
	}

	/**
	 * ＤＡＯのサンプル実装クラスの生成を行います。 <br>
	 * このクラスは、DIコンテナを通してインスタンスを 生成します。 <br>
	 * 直接インスタンスの生成を行わないで下さい。
	 */
	public SecurityServiceImpl() {

	}

	@Override
	public boolean isExpired( String userId, final String functionID ) {
		if ( null != functionID
				&& functionID.startsWith("FlowLogin") ) {
			return false;
		}

		ServiceId svcId = ServiceId.value(functionID);
		if ( ! ServiceId.none.equals(svcId) && ! svcId.authEnabled() ) {
			return false;
		}

		IUserEntity entity = umFinderSupport.findUserByUserId( userId );

		Timestamp now = new Timestamp(System.currentTimeMillis());
		Timestamp passwordLimit = entity.getPassTimeLimit();
		Boolean expired = false;

		if (null != passwordLimit && passwordLimit.getTime() <= now.getTime()) {
			expired = true;
		}

		return expired;
	}

	@Override
	public boolean isValidAccess(String userId, final String functionID) {
		if (this.notExistsFunctionID(functionID)) {
			return false;
		}

		IFuncSvcEntity funcSvcEntity = this.smFinderSupport.findFuncSvcEntityByPrimaryKey(functionID);

		if ( this.validAccess(userId, funcSvcEntity) ) {
			return true;
		}

		return false;
	}

	@Override
	public void validAccess(String userId, final String functionID)
			throws InvalidActionException, InvalidUserException {

		ServiceId svcId = ServiceId.value(functionID);
		if ( ! ServiceId.none.equals(svcId) && ! svcId.authEnabled() ) {
			return;
		}

		/* 機能サービスを取得 */
		IFuncSvcEntity funcSvcEntity = this.smFinderSupport.findFuncSvcEntityByPrimaryKey(functionID);
		if (funcSvcEntity == null) {
			throw new InvalidActionException();
		}

		if ( this.validAccess(userId, funcSvcEntity) ) {
			return;
		}

		throw new InvalidUserException(funcSvcEntity.getSvcId(),
				funcSvcEntity.getSvcNm());
	}

	private boolean validAccess( String userId, IFuncSvcEntity funcSvcEntity ) {

		String[] uRoles = this.getUserRolesList(userId, funcSvcEntity);

		if (FluentList.from(svcIdFrom(svcCtgFrom(uRoles))).//
				contains(functionIdIsMatch(funcSvcEntity.getSvcId()))) {
			return true;
		}

		return false;
	}

	@Override
	public void validAccess(String functionID) throws InvalidActionException {
		if (this.notExistsFunctionID(functionID)) {
			throw new InvalidActionException();
		}
	}

	/**
	 * 指定された配列にサービスIDが含まれているか確認します。含まれている場合はtrueを返します.
	 *
	 * @param functionID サービスID
	 * @return サービスIDがある:true ,サービスIDがない:false
	 */
	private TriPredicate<String> functionIdIsMatch(final String functionID) {

		return new TriPredicate<String>() {

			@Override
			public boolean evalute(String id) {
				return id.equals(functionID);
			}
		};
	}

	/**
	 * ユーザIDからロールIDを取得します。
	 * @param userID
	 * @param funcSvcEntity
	 * @return
	 */
	private String[] getUserRolesList(final String userID,
			IFuncSvcEntity funcSvcEntity) {

		// ユーザIDからロールIDを取得
		List<String> roleIdList = TriCollectionUtils.collect(
				userRoleLnkEntityFrom(userID), toRoleId(userID));

		if (TriCollectionUtils.isEmpty(roleIdList)) {
			throw new InvalidUserException(funcSvcEntity.getSvcId(),
					funcSvcEntity.getSvcNm());
		}

		return roleIdList.toArray(new String[0]);
	}

	/**
	 * ユーザIDからユーザ・ロール(Lnk)エンティティを取得します。
	 * @param userID
	 * @return
	 */
	private List<IUserRoleLnkEntity> userRoleLnkEntityFrom(final String userID) {
		return umFinderSupport.findUserRoleLnkByUserId(userID);
	}

	/**
	 * UserRoleLnkEntityのリストからロールIDを取得するfunctionメソッドです。
	 * @param userID
	 * @throws RuntimeException
	 * @return
	 */
	private TriFunction<IUserRoleLnkEntity, String> toRoleId(final String userID) {

		return new TriFunction<IUserRoleLnkEntity, String>() {

			@Override
			public String apply(IUserRoleLnkEntity entity) {
				if (!entity.getUserId().equals(userID)) {
					throw new RuntimeException(contextAdapter().//
							getMessage(SmMessageId.SM004098F));
				}
				return entity.getRoleId();
			}
		};
	}

	/**
	 * ロールIDからサービスカテゴリIDを取得します。
	 * @param roleIds ロールID
	 * @return サービスカテゴリID
	 */
	private String[] svcCtgFrom(String[] roleIds) {

		List<String> svcCtgList = new ArrayList<String>();
		svcCtgList.addAll(TriCollectionUtils.collect(roleSvcCtgLnkEntityFrom(roleIds), toSvcCtgId() ));
		return svcCtgList.toArray(new String[0]);
	}

	/**
	 * ロールIDからロール・サービスカテゴリ(Lnk)エンティティのリストを取得します。
	 * @param roleIds ロールID
	 * @return RoleSvcCtgLnkEntityのリスト
	 */
	private List<IRoleSvcCtgLnkEntity> roleSvcCtgLnkEntityFrom( String[] roleIds ) {
		return umFinderSupport.findSvcCtgLnkByRoleId(roleIds);
	}

	/**
	 * RoleSvcCtgLnkEntityのリストからサービスカテゴリIDを取得するfunctionメソッドです。
	 * @return
	 */
	private TriFunction<IRoleSvcCtgLnkEntity, String> toSvcCtgId() {

		return new TriFunction<IRoleSvcCtgLnkEntity, String>() {

		@Override
		public String apply(IRoleSvcCtgLnkEntity entity) {
			if (entity.getSvcCtgId().isEmpty()) {
				throw new RuntimeException(contextAdapter().//
						getMessage(SmMessageId.SM005194S));
			}

			return entity.getSvcCtgId();
		}
		};
	}

	/**
	 * サービスカテゴリIDからサービスIDを取得します。
	 * @param svcCtgIds
	 * @return
	 */
	private String[] svcIdFrom(String[] svcCtgIds) {

		List<String> svcCtgList = TriCollectionUtils.collect(svcCtgSvcLnkEntityList( svcCtgIds ), toSvcId() );
		return svcCtgList.toArray(new String[0]);
	}

	/**
	 * サービスカテゴリIDからSvcCtgSvcLnkEntityのリストを取得します。
	 * @param svcCtgArray
	 * @return
	 */
	private List<ISvcCtgSvcLnkEntity> svcCtgSvcLnkEntityList(String[] svcCtgArray) {
		return umFinderSupport.findSvcCtgSvcLnkBySvcCtgId(svcCtgArray);
	}

	/**
	 * SvcCtgSvcLnkEntityのリストからサービスIDを取得するfunctionメソッドです。
	 * @return
	 */
	private TriFunction<ISvcCtgSvcLnkEntity, String> toSvcId() {

		return new TriFunction<ISvcCtgSvcLnkEntity, String>() {

			@Override
			public String apply(ISvcCtgSvcLnkEntity entity) {
				if (entity.getSvcId().isEmpty()) {
					throw new RuntimeException(contextAdapter().//
							getMessage(SmMessageId.SM005195S));
					}
				return entity.getSvcId();
			}
		};
	}

	/**
	 * 指定した機能IDが存在するかどうかを返します。
	 *
	 * @param functionID
	 *            機能ID
	 *
	 * @return 機能IDが存在しない場合はtrue
	 */
	private boolean notExistsFunctionID(String functionID) {

		ServiceId svcId = ServiceId.value(functionID);
		if ( ! ServiceId.none.equals(svcId) && ! svcId.authEnabled() ) {
			return false;
		}

		IFuncSvcEntity entity = this.smFinderSupport.findFuncSvcEntityByPrimaryKey(functionID);
		if (null == entity) {
			return true;
		}
		if (functionID.equals(entity.getSvcId())) {
			return false;
		}
		throw new RuntimeException(contextAdapter().getMessage(
				SmMessageId.SM004100F));
	}

	private static IContextAdapter contextAdapter() {
		return ContextAdapterFactory.getContextAdapter();
	}

}
