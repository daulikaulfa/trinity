package jp.co.blueship.tri.fw.security;

public class SecurityException extends RuntimeException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SecurityException() {
	}
	
	public SecurityException(String str){
		super(str);
	}

	public SecurityException(Throwable th){
		super(th);
	}

	public SecurityException(String str, Throwable th){
		super(str, th);
	}
}
