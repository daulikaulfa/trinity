package jp.co.blueship.tri.fw.security;

public class DoubleLoginSecurtyException extends RuntimeException {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public DoubleLoginSecurtyException() {
	}

	public DoubleLoginSecurtyException(String str){
		super(str);
	}

	public DoubleLoginSecurtyException(Throwable th){
		super(th);
	}

	public DoubleLoginSecurtyException(String str, Throwable th){
		super(str, th);
	}
}
