package jp.co.blueship.tri.fw.security;


/**
 * 認証認可処理に必要な情報を取得するクラスです。。
 *
 * @author kanda
 *
 */
public interface SecurityService {
	/**
	 * @param userId
	 * @param functionID
	 * @return
	 */
	public boolean isExpired( String userId, final String functionID );

	/**
     * To the specified user,
     * to check whether there is an execution authority of the function corresponding to the function ID.
     *
     * @param userId user ID
     * @param functionID function ID
     */
    public boolean isValidAccess( String userId, String functionID );

	/**
     * 指定したユーザに、機能IDに対応する機能の実行権限があるかどうかを返します。
     *
     * @param userId ユーザID
     * @param functionID 機能ID
     *
     * @throws InvalidActionException 実行を許可されていない場合throwされる
     * @throws InvalidUserException 実行権限がない場合throwされる
     */
    public void validAccess( String userId, String functionID )
    throws InvalidActionException, InvalidUserException;

    /**
     * 機能IDに対応する機能の実行を許可されているかどうかを返します。
     *
     * @param functionID 機能ID
     *
     * @throws InvalidActionException 実行を許可されていない場合throwされる
     */
    public void validAccess( String functionID )
    throws InvalidActionException;

}
