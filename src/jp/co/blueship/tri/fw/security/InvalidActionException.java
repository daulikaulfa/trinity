package jp.co.blueship.tri.fw.security;

public class InvalidActionException extends SecurityException {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * 
	 */

	private String actionId = "";
	private String actionName = "";
	private String actionContent = "";

	public InvalidActionException(){
		super();
		
	}

	public InvalidActionException(String actionId, String actionName, String actionContent){
		super("actionId :" + actionId + ", actionName:" + ", actionContent:" + actionContent);
		
		this.actionId = actionId;
		this.actionName = actionName;
		this.actionContent = actionContent;
		
	}

	/**
	 * @return actionContent
	 */
	public String getActionContent() {
		return actionContent;
	}

	/**
	 * @return actionId
	 */
	public String getActionId() {
		return actionId;
	}

	/**
	 * @return actionName
	 */
	public String getActionName() {
		return actionName;
	}

}
