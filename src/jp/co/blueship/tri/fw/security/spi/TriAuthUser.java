package jp.co.blueship.tri.fw.security.spi;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.User;

/**
 * Spring Securityで認証済みユーザ情報を格納するためのユーザクラスです。<br/>
 * 認証済みユーザ情報にデフォルトでは格納されないユーザ情報(例：ユーザ名など)を付加するために拡張します。
 *
 * @author Takayuki Kubo
 *
 */
public class TriAuthUser extends User {

	private static final long serialVersionUID = 8335149195659182946L;

	private String userName;

    /**
	 * コンストラクタ
	 *
	 * @param userID ユーザID
	 * @param userName ユーザ名
	 */
    public TriAuthUser(String userID, String userName) {
        this(userID, userName, "pass", true, true, true, true, AuthorityUtils.NO_AUTHORITIES);
    }

    /**
	 * コンストラクタ
	 *
	 * @param userID ユーザID
	 * @param userName ユーザ名
	 */
    public TriAuthUser(String userID, String userName, String password, boolean enabled ) {
        this(userID, userName, password, enabled, true, true, true, AuthorityUtils.NO_AUTHORITIES);
    }

    /**
	 * コンストラクタ
	 *
	 * @param userID ユーザID
	 * @param userName ユーザ名
	 * @param password パスワード
	 * @param enabled このユーザが有効である場合はtrue
	 * @param accountNonExpired ユーザアカウントが有効である場合はtrue
	 * @param credentialsNonExpired パスワードが有効である場合はtrue
	 * @param accountNonLocked ユーザアカウントをロックしない場合はtrue
	 * @param authorities ユーザロールの一覧(省略不可)
	 */
	public TriAuthUser(String userID, String userName, String password, boolean enabled, boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
		super(userID, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);

		this.userName = userName;
	}

	/**
	 * ユーザ名を返します。
	 *
	 * @return ユーザ名
	 */
	public String getUserNm() {
		return userName;
	}

	/**
	 * ユーザ名を返します。
	 *
	 * @return ユーザ名
	 */
	public String getUserID() {
		return super.getUsername();
	}

	/**
	 * これは親クラスの同名のメソッドをオーバライドしています。<br/>
	 * 親クラスではユーザIDを返すためのメソッドになっていますが、紛らわしいため直接呼ばないでください。
	 *
	 * @see org.springframework.security.core.userdetails.User#getUsername()
	 */
	@Override
	@Deprecated
	public String getUsername() {
		return super.getUsername();
	}

}
