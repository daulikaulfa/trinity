package jp.co.blueship.tri.fw.security.spi;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;

import jp.co.blueship.tri.fw.security.spec.SecSpecifications;

/**
 * Spring-Securityの認証プロバイダ拡張実装クラスです。<br/>
 * ユーザアカウント情報テーブルにパスワードがMD5で暗号化されていることを前提に<br/>
 * 認証処理を行います。
 *
 * @author Takayuki Kubo
 *
 */
public class TrinityDaoAuthenticationProvider extends DaoAuthenticationProvider {

	public TrinityDaoAuthenticationProvider() {
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication)
			throws AuthenticationException {

		if (authentication.getCredentials() == null) {
			throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
					userDetails);
		}

		checkPassword(userDetails, authentication);
	}

	@SuppressWarnings("deprecation")
	private void checkPassword(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {

		if (!isPasswordValid(userDetails, authentication)) {
			throw new BadCredentialsException(messages.getMessage("AbstractUserDetailsAuthenticationProvider.badCredentials", "Bad credentials"),
					userDetails);
		}

	}

	/**
	 * ユーザが入力したパスワードが正しいかどうかを返します。<br>
	 * ユーザが入力したパスワードは「ID:レルム名:パスワード」をMD5で暗号化後に検証されます。<br/>
	 *
	 * @param userDetails ユーザアカウント情報テーブルから抽出したアカウント情報
	 * @param authentication ユーザが入力したID、パスワードを格納したオブジェクト
	 * @return パスワードが正しい場合はtrueを返す。
	 */
	private boolean isPasswordValid(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) {

		// まずはそのまま比較してみる。WebサービスのDigest認証ではUsernamePasswordAuthenticationTokenの中にはすでにMD5 A1フォーマット化されたパスワードが入ってくるため。
		if (userDetails.getPassword().equals(authentication.getCredentials())) {
			return true;
		}

		return SecSpecifications.specOfMatchingPasswordInMD5A1FormatWith(userDetails.getPassword(), userId(authentication)).//
				isSpecified(authentication.getCredentials().toString());

	}

	private String userId(UsernamePasswordAuthenticationToken authentication) {

		Object principal = authentication.getPrincipal();
		if (TriAuthUser.class.isInstance(principal)) {
			return ((TriAuthUser) principal).getUserID();
		}

		return (String) principal;
	}
}
