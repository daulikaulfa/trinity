package jp.co.blueship.tri.fw.security.spi;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl;

import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;

/**
 * Spring Securityで認証ユーザ情報をRDBMSより取得するするためのDao実装をTrinity仕様へ拡張するためのクラスです。<br/>
 *
 * @see org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl
 *
 * @author Takayuki Kubo
 *
 */
public class TriUserDetailJdbcDaoImpl extends JdbcDaoImpl {

	/**
	 * 指定されたユーザIDを元にユーザ情報を取得します。
	 *
	 * @param userID ユーザID
	 *
	 * @return ユーザ情報
	 * @see org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl#loadUsersByUsername(java.lang.String)
	 */
	@Override
	protected List<UserDetails> loadUsersByUsername(String userID) {
		return getJdbcTemplate().query(getUsersByUsernameQuery(), new String[] { userID }, new RowMapper<UserDetails>() {
			public UserDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
				String userID = rs.getString(UserItems.userId.getItemName());
				String userName = rs.getString(UserItems.userNm.getItemName());
				String password = rs.getString(UserItems.userPass.getItemName());
				boolean isDeleted = rs.getBoolean(UserItems.delStsId.getItemName());
				return new TriAuthUser(userID, userName, password, !isDeleted);
			}

		});
	}

	/**
	 * Spring Security独自のユーザ情報にtrinity独自の情報を付加して生成し直します。
	 *
	 * @param userID ユーザID
	 * @param userFromUserQuery DBMSからロードされたユーザ情報
	 * @param combinedAuthorities ユーザのロール情報
	 *
	 * @see org.springframework.security.core.userdetails.jdbc.JdbcDaoImpl#
	 *      createUserDetails(java.lang.String,
	 *      org.springframework.security.core.userdetails.UserDetails,
	 *      java.util.List<org.springframework.security.core.GrantedAuthority>)
	 */
	@Override
	protected UserDetails createUserDetails(String userID, UserDetails userFromUserQuery, List<GrantedAuthority> combinedAuthorities) {

		UserDetails user = super.createUserDetails(userID, userFromUserQuery, combinedAuthorities);
		if (!TriAuthUser.class.isInstance(userFromUserQuery)) {
			return user;
		}

		TriAuthUser triAuthUser = (TriAuthUser) userFromUserQuery;

		return new TriAuthUser(user.getUsername(), triAuthUser.getUserNm(), user.getPassword(), user.isEnabled(), user.isAccountNonExpired(),
				user.isCredentialsNonExpired(), user.isAccountNonLocked(), user.getAuthorities());
	}

}
