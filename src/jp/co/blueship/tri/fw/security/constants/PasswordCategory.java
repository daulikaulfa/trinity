package jp.co.blueship.tri.fw.security.constants;

public enum PasswordCategory {

	FTP					( "ftp" ),
	SFTP				( "sftp" ),
	SCP					( "scp" ),
	SVN					( "svn" ),
	MAIL_SECURITY		( "mailSecurity" );

	

	private String value = null ;

	private PasswordCategory( String value ) {
		this.value = value ;
	}

	public String getValue() {
		return this.value ;
	}

	public static PasswordCategory getValue( String value ) {
		for ( PasswordCategory charset : values() ) {
			if ( charset.getValue().equals( value ) ) {
				return charset ;
			}
		}

		return null;
	}
}
