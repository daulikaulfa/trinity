package jp.co.blueship.tri.fw.act;

public class NotEndProcessException extends Exception {

	private static final long serialVersionUID = -3645056937816688029L;

	public NotEndProcessException() {
		super();
	}

	public NotEndProcessException(String message) {
		super(message);
	}

	public NotEndProcessException(Throwable th) {
		super(th);
	}

	public NotEndProcessException(String message, Throwable th) {
		super(message, th);
	}
}
