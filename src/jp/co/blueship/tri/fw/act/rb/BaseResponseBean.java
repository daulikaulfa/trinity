package jp.co.blueship.tri.fw.act.rb;

import java.util.List;

import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.ITranslatable;

/**
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class BaseResponseBean implements IBaseResponseBean {



	private List<String> messageList;
	private List<String> commentList;
	private String windowsId;
	private String flowActionId;
	private String savedPageNo;

	// 暫定▼
	private String action;
	private String actionDetail;
	private String actionDetailView;
	private String actionDownLoad;
	private String actionSearch;
	private String actionIssuance;
	private String actionSearchBtn;

	// 暫定▲

	public List<String> getMessageList() {
		return messageList;
	}

	public void setMessageList(List<String> messageList) {
		this.messageList = messageList;
	}

	public List<String> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<String> commentList) {
		this.commentList = commentList;
	}

	public class MessageUtility {

		/**
		 * メッセージIDを保持する例外からメッセージへの変換を行い、同メッセージをBaseResponceBeanへ設定します。
		 * <br>ロケール対応していないため、V4以降は原則として利用しません。
		 *
		 * @param exception メッセージIDを保持する例外
		 */
		public void reflectMessage(ITranslatable exception) {

			if (exception == null) {
				return;
			}

			setMessageList(ContextAdapterFactory.getContextAdapter().getMessage(exception));
		}

		/**
		 * メッセージIDを保持する例外からメッセージへの変換を行い、同メッセージをBaseResponceBeanへ設定します。
		 *
		 * @param lang Language
		 * @param bbe Exception Interface
		 */
		public void reflectMessage(String lang, ITranslatable bbe) {

			if (bbe == null) {
				return;
			}
			// メッセージの取得
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			setMessageList(ca.getMessage(lang, bbe));
		}

		public void reflectMessage(IGeneralServiceBean info) {

			if (info == null) {
				return;
			}
			// メッセージの取得
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			setMessageList(ca.getMessage(info));
		}

		public void reflectComment(IGeneralServiceBean info) {
			reflectComment(info, true);
		}

		public void reflectComment(IGeneralServiceBean info, boolean isDecorate) {

			if (info == null) {
				return;
			}
			// メッセージの取得
			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			setCommentList(ca.getComment(info, isDecorate));
		}
	}

	public String getWindowsId() {
		return windowsId;
	}

	public void setWindowsId(String windowsId) {
		this.windowsId = windowsId;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getActionDetail() {
		return actionDetail;
	}

	public void setActionDetail(String actionDetail) {
		this.actionDetail = actionDetail;
	}

	public String getActionDetailView() {
		return actionDetailView;
	}

	public void setActionDetailView(String actionDetailView) {
		this.actionDetailView = actionDetailView;
	}

	public String getActionDownLoad() {
		return actionDownLoad;
	}

	public void setActionDownLoad(String actionDownLoad) {
		this.actionDownLoad = actionDownLoad;
	}

	public String getActionIssuance() {
		return actionIssuance;
	}

	public void setActionIssuance(String actionIssuance) {
		this.actionIssuance = actionIssuance;
	}

	public String getActionSearch() {
		return actionSearch;
	}

	public void setActionSearch(String actionSearch) {
		this.actionSearch = actionSearch;
	}

	public String getActionSearchBtn() {
		return actionSearchBtn;
	}

	public void setActionSearchBtn(String actionSearchBtn) {
		this.actionSearchBtn = actionSearchBtn;
	}

	public String getSavedPageNo() {
		return savedPageNo;
	}

	public void setSavedPageNo(String savedPageNo) {
		this.savedPageNo = savedPageNo;
	}

	public String getFlowActionId() {
		return flowActionId;
	}

	public void setFlowActionId(String flowActionId) {
		this.flowActionId = flowActionId;
	}
}
