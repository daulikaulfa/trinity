package jp.co.blueship.tri.fw.act;

/**
 * {@link IParameterCashe#start()}を実行していない場合に発生する例外です。
 * また、{@link IParameterCashe#commit()}または、{@link IParameterCashe#rollback()}を実行した後、再使用する際に{@link IParameterCashe#start()}が実行されていないと発生します。
 * @author kawakami
 *
 */
public class NotInitilizeException extends Exception {

	private static final long serialVersionUID = 6674422065941395901L;

	public NotInitilizeException() {
		super();
	}

	public NotInitilizeException(String message) {
		super(message);
	}

	public NotInitilizeException(Throwable th) {
		super(th);
	}

	public NotInitilizeException(String message, Throwable th) {
		super(message, th);
	}
}
