/**
 * 
 */
package jp.co.blueship.tri.fw.act;

/**
 * 一時的にキャッシュを行い、あるタイミングで全てのパラメータを実体へ設定、または、キャッシュのクリアを行うInterfaceを提供します。
 * 
 * @author kawakami
 *
 */
public interface IParameterCashe {
	/**
	 * キャッシュを行いたいときには必ずこのメソッドを実行してください。
	 * @throws NotEndProcessException 発生契機は、{@link #start()}を呼び出した後に {@link #commit()}/{@link #rollback()}を呼び出さずに {@link #start()}を呼び出した時に発生します。
	 */
	public void start() throws NotEndProcessException;
	/**
	 * キャッシュしたパラメータを実体へ設定したいタイミングで本処理を実行してください。
	 * 再使用したい場合は、{@link #start()}を実行してから使用してください。
	 * @throws NotInitilizeException 発生契機は、{@link #start()}が実行されていない時です。
	 */
	public void commit() throws NotInitilizeException;
	/**
	 * キャッシュしたパラメータをクリアしたい場合、本処理を実行してください。
	 * 再使用したい場合は、{@link #start()}を実行してから使用してください。
	 * @throws NotInitilizeException 発生契機は、{@link #start()}が実行されていない時です。
	 */
	public void rollback() throws NotInitilizeException;
}
