package jp.co.blueship.tri.fw.di;

import java.util.Locale;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class MessageManager implements ApplicationContextAware {

	public static final String BEAN_NAME = "trinityMessageManager";
	private static final String MESSAGE_ID_PREFIX = "TRI-";

	private String language = "";
	private ApplicationContext ac = null;

	public String getLanguage() {
		return this.language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}

	public void setApplicationContext(ApplicationContext context) throws BeansException {
		this.ac = context;
	}

	public String getValue( String code, String[] args, String lang ) {
		return getContentsFromResource( lang, code, args );
	}

	public String getMessage(TriLogMessage messageId, String[] args) {
		return getContentsFromResource(messageId.getMessageId(), args);
	}

	public String getMessage(String lang, IMessageId messageId, String... args) {
		return getMessage(lang, messageId, args, true);
	}

	public String getMessage(String lang, IMessageId messageId, String[] args, boolean isDecorate) {
		String contents = getContentsFromResource(lang, MESSAGE_ID_PREFIX + messageId, args);
		return withDecorate(contents, messageId.getMessageId(), isDecorate);
	}

	public String getLogMessage(TriLogMessage messageId, String[] args) {
		return getContentsFromResource(messageId.getMessageId(), args);
	}

	private String getContentsFromResource(String messageId, String[] args) {
		return getContentsFromResource(this.language, messageId, args);
	}

	private String getContentsFromResource(String lang, String messageId, String[] args) {
		return ac.getMessage(messageId, args, (null == lang)? new Locale(this.language): new Locale(lang));
	}

	private static String withDecorate(String contents, String messageId, boolean isDecorate) {

		if (!isDecorate) {
			return contents;
		}

		return String.format("[%s] : %s", messageId, contents);
	}
}
