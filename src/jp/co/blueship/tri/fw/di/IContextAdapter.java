package jp.co.blueship.tri.fw.di;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.IMessageId;

/**
 * インスタンス生成時、呼び出す側のクラスが
 * ＤＩコンテナに依存しないための機能を提供するアダプターインタフェースです。
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public interface IContextAdapter {

	/**
	 * 使用しているＤＩコンテナより、インスタンスを取得します。
	 * @param name 取得するbean名
	 * @return beanのインスタンスを戻します。
	 * @throws TriSystemException
	 */
	public Object getBean(String name) throws TriSystemException;

	/**
	 * 使用しているＤＩコンテナより、メッセージマネージャインスタンスを取得します。
	 * @return メッセージマネージャのインスタンスを戻します。
	 * @throws TriSystemException
	 */
	public MessageManager getMessageManager() throws TriSystemException;

	/**
	 * Get the message from the code.
	 *
	 * @param code the code to lookup up, such as 'xxx.xxx'.
	 * @return the message corresponding to the code.
	 */
	public String getValue( String code );

	/**
	 * Get the message from the code.
	 *
	 * @param code the code to lookup up, such as 'xxx.xxx'.
	 * @param args array of arguments that will be filled in for params within the message.
	 * @return the message corresponding to the code.
	 */
	public String getValue( String code, String[] args );

	/**
	 * Get the message from the code.
	 *
	 * @param code the code to lookup up, such as 'xxx.xxx'.
	 * @param lang the Language in which to do the lookup
	 * @return the message corresponding to the code.
	 */
	public String getValue( String code, String lang );

	/**
	 * Get the message from the code.
	 *
	 * @param code the code to lookup up, such as 'xxx.xxx'.
	 * @param args array of arguments that will be filled in for params within the message.
	 * @param lang the Language in which to do the lookup
	 * @return the message corresponding to the code.
	 */
	public String getValue( String code, String[] args, String lang );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。
	 * @param id メッセージID
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( IMessageId id );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。
	 * @param id メッセージID
	 * @param args メッセージパラメタ
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( IMessageId id, String... args );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。
	 * @param lang Locale Language
	 * @param id メッセージID
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( String lang, IMessageId id );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。
	 * @param lang Locale Language
	 * @param id メッセージID
	 * @param args メッセージパラメタ
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( String lang, IMessageId id, String... args );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。
	 * @param lang Locale Language
	 * @param isDecorate decorate messageId
	 * @param id メッセージID
	 * @param args メッセージパラメタ
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( String lang, boolean isDecorate , IMessageId id, String... args );


	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。(Log用)
	 * @param id メッセージID(Log用)
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( TriLogMessage logId );

	/**
	 * 指定されたメッセージＩＤをメッセージ文言へ変換します。(Log用)
	 * @param id メッセージID(Log用)
	 * @param args メッセージパラメタ
	 * @return メッセージ文言
	 * @throws TriSystemException
	 */
	public String getMessage( TriLogMessage logId, String... args );

	/**
	 * trinityで扱うサービスビーンの内容から、メッセージＩＤを取り出してメッセージ文言へ変換します。
	 * 変換できない場合、サービスビーンのメッセージをそのまま戻します。
	 * @param info trinityで扱うサービスビーン
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	public List<String> getMessage( IGeneralServiceBean info ) throws TriSystemException;

	/**
	 * trinityで扱うExceptionの内容から、メッセージＩＤを取り出してメッセージ文言へ変換します。
	 * 変換できない場合、exeptionのメッセージをそのまま戻します。
	 * @param exception trinityで扱うException
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	public List<String> getMessage( ITranslatable exception ) throws TriSystemException;

	/**
	 * trinityで扱うExceptionの内容から、メッセージＩＤを取り出してメッセージ文言へ変換します。
	 * 変換できない場合、exeptionのメッセージをそのまま戻します。
	 * @param lang Locale Language
	 * @param exception trinityで扱うException
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	public List<String> getMessage( String lang, ITranslatable exception ) throws TriSystemException;

	/**
	 * trinityで扱うExceptionの内容から、メッセージＩＤを取り出してメッセージ文言へ変換します。
	 * 変換できない場合、exeptionのメッセージをそのまま戻します。
	 * @param exception trinityで扱うException
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	@Deprecated // V2互換用 メッセージIDよりメッセージを導出するようにしてください。
	public List<String> getMessage( TriRuntimeException exception ) throws TriSystemException;

	/**
	 * trinityで扱うExceptionの内容から、メッセージＩＤを取り出してメッセージ文言へ変換します。
	 * 変換できない場合、exeptionのメッセージをそのまま戻します。
	 * @param lang Locale Language
	 * @param exception trinityで扱うException
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	@Deprecated // V2互換用 メッセージIDよりメッセージを導出するようにしてください。
	public List<String> getMessage( String lang, TriRuntimeException exception ) throws TriSystemException;

	/**
	 * trinityで扱うサービスビーンの内容から、メッセージＩＤを取り出してコメント文言へ変換します。
	 * 変換できない場合、サービスビーンのメッセージをそのまま戻します。
	 * @param info trinityで扱うサービスビーン
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	public List<String> getComment( IGeneralServiceBean info ) throws TriSystemException;

	/**
	 * trinityで扱うサービスビーンの内容から、メッセージＩＤを取り出してコメント文言へ変換します。
	 * 変換できない場合、サービスビーンのメッセージをそのまま戻します。
	 * @param info trinityで扱うサービスビーン
	 * @param isDecorate メッセージを装飾する場合はtrueを設定します
	 * @return メッセージを戻します。
	 * @throws TriSystemException
	 */
	public List<String> getComment( IGeneralServiceBean info, boolean isDecorate ) throws TriSystemException;
}
