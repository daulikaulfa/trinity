package jp.co.blueship.tri.fw.di;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;

import jp.co.blueship.tri.fw.di.spring.ContextAdapter;
import jp.co.blueship.tri.fw.di.spring.Contexts;

/**
 * ＤＩアプリケーションのコンテキストを生成するファクトリクラスです。
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class ContextAdapterFactory {

	/**
	 * コンテキストを取得します。 <br>
	 * コンテキストはsingletonで管理されます。
	 *
	 * @return 取得したコンテキストを戻します。
	 */
	public static IContextAdapter getContextAdapter() {
		ApplicationContext ac = (ApplicationContext) Contexts.getInstance().getBeanFactory();
		ContextAdapter context = new ContextAdapter();
		context.setApplicationContext(ac);

		return context;
	}

	/**
	 * メッセージコンテキストを取得します。 <br>
	 * コンテキストはsingletonで管理されます。
	 *
	 * @return 取得したコンテキストを戻します。
	 */
	@Deprecated public static IContextAdapter getMessageContextAdapter() {

		ApplicationContext ac = (ApplicationContext) Contexts.getInstance().getBeanFactory();
		ContextAdapter context = new ContextAdapter();
		context.setApplicationContext(ac);

		return context;
	}

	/**
	 * 指定されたパスのコンテキストを取得します。 <br>
	 * 指定されたファイル名から、新規のコンテキストを生成します。 <br>
	 * Remote通信を行う場合等、コンテキストをシングルトンで管理しない場合に使用して下さい。
	 *
	 * @param configLocations - ApplictionContextXmlに対する、パッケージとファイル名を含んだクラスパスの配列
	 * @return 取得したコンテキストを戻します。
	 */
	public static IContextAdapter getContextAdapter(String[] configLocations) {
		ApplicationContext ac = (ApplicationContext) initBeanFactory(configLocations);
		ContextAdapter context = new ContextAdapter();
		context.setApplicationContext(ac);

		return context;
	}

	/**
	 * ApplictionContextXmlに対するクラスパスを元に、そのApplictionContextを生成する.
	 *
	 * @param configLocations - ApplictionContextXmlに対する、パッケージとファイル名を含んだクラスパスの配列
	 * @return 生成したファクトリーを戻します。
	 */
	private static final BeanFactory initBeanFactory(String[] configLocations) {

		return Contexts.getInstance().getApplicationContext(configLocations);

	}

}
