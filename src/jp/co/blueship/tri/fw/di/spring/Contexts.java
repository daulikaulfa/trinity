package jp.co.blueship.tri.fw.di.spring;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;

/**
 * application内で生成された、Service(SpringのApplicationContext群)を管理するクラス.<br>
 * <p>
 * 要求された文字列からBeanFactoryを生成あるいは保持する。
 * 生成するBeanFactoryのインスタンスを一元化するため、Singletonパターンを使用する。<br>
 * また、BeanFactoryインスタンスの整合性を保証するため、生成時は同期処理を行う。<br>
 *
 * @author kanda
 *
 */
public class Contexts {
	//起動時にシステムエラー発生時に正しくコンテキストがロードできない場合に緊急で使用する
	private static final String[] EMERGENCY_DI_CONTEXT_FILES = { "Fw-MessageContents-Context.xml" };

	/** application内で唯一生成される<code>Contexts</code> オブジェクト. */
	private static Contexts me = new Contexts();
	/** ApplicationContextXml名に依存しない<code>BeanFactory</code> オブジェクト. */
	private BeanFactory beanFactory = null;

	private static final ILog log = TriLogFactory.getInstance();

	/**
	 * デフォルトコンストラクタ
	 */
	private Contexts() {
	}

	/**
	 * 自分のオブジェクトを生成して返却する。
	 *
	 * @return application内で唯一生成される、<code>Contexts</code> オブジェクト.
	 */
	public static Contexts getInstance() {
		return me;
	}

	/**
	 * ApplicationContextを設定する.
	 *
	 * Springの設定ファイルロード処理の変更に伴い, サービス層,業務ロジック層,Dao層からはServletContextへアクセスできないため,
	 * ContextsクラスからApplicationContextを取得できるようにする.
	 *
	 * @param appContext - ServletContextに格納されているApplicationContext
	 */
	public void setApplicationContext(ApplicationContext appContext) {
		this.beanFactory = appContext;
	}

	/**
	 * ApplictionContextXmlに対するクラスパスを元に、そのApplictionContextを生成する.
	 *
	 * @param configLocations - ApplictionContextXmlに対する、パッケージとファイル名を含んだクラスパスの配列
	 */
	synchronized public void initBeanFactory(String[] configLocations) {

		if (null == this.beanFactory) {
			this.beanFactory = getApplicationContext(configLocations);
		}
	}

	/**
	 * ApplictionContextXmlに対するクラスパスを記述したXMLファイルを元に、定義されているApplictionContextを生成し
	 * 、 {@link BeanFactory}として返却する.
	 *
	 * @param fileName -
	 *            ApplictionContextXmlに対する、パッケージとファイル名を含んだクラスパスが記述されたXMLのクラスパス
	 */
	synchronized public void initBeanFactory(String fileName) {

		List<String> list = new ArrayList<String>();
		list.add(fileName);

		this.initBeanFactory(list.toArray(new String[0]));
	}

	/**
	 * 予め生成されているApplictionContextを{@link BeanFactory}として返却する.
	 *
	 * @return 生成した{@link BeanFactory}オブジェクト。
	 * @throws TriSystemException {@link BeanFactory}オブジェクトが予め生成されていない場合にスローされる。
	 */
	synchronized public BeanFactory getBeanFactory() {

		if (this.beanFactory == null) {
			//起動時にError発生時、MessageIDを表示させるために一時的にコンテキストを返す
			return getApplicationContext( EMERGENCY_DI_CONTEXT_FILES );

			//PreConditions.assertOf(null != this.beanFactory, "BeanFactory is null.");
			//無限LoopするためここではMessageIDを発行しない
			//throw new TriSystemException(SmMessageId.SM005155S);
		}

		return this.beanFactory;
	}

	/**
	 * クラスパス上に存在するSpring定義ファイルをもとに、ApplicationContextを生成する。<br/>
	 * このメソッドで生成されたApplicationContextはこのクラスでは保持しません。
	 *
	 * @param configLocations クラスパス上に存在するSpring定義ファイル名の配列
	 * @return ApplicationContext
	 */
	public ApplicationContext getApplicationContext(String[] configLocations) {

		BeanFactory beanFactory = null;
		try {

			beanFactory = new ClassPathXmlApplicationContext(configLocations);
		} catch (BeansException be) {
			LogHandler.fatal(log, be.getMessage() , be);
			throw be;
		}

		return (ApplicationContext) beanFactory;
	}

}
