package jp.co.blueship.tri.fw.di.spring;

/**
 * DIコンテナのContextをサービス毎にその都度利用するための属性値です。
 */
public class ContextServiceAttributes {

	

	private String configLocations = null;
	private String serviceName = null;
	private String dataSource = null;

	/**
	 * DIコンテナで使用するコンテキストのパスを取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getConfigLocations() {
		return configLocations;
	}
	/**
	 * DIコンテナで使用するコンテキストのパスを設定します。
	 *
	 * @param configLocations コンテキストのパス
	 */
	public void setConfigLocations(String configLocations) {
		this.configLocations = configLocations;
	}
	/**
	 * 接続先のリモートサービスデータソース名を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getDataSource() {
		return dataSource;
	}
	/**
	 * 接続先のリモートサービスデータソース名を設定します。
	 *
	 * @param dataSource データソース名
	 */
	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}
	/**
	 * 接続先のリモートサービスクライアント名を取得します。
	 *
	 * @return 取得した値を戻します。
	 */
	public String getServiceName() {
		return serviceName;
	}
	/**
	 * 接続先のリモートサービスクライアント名を設定します。
	 *
	 * @param serviceName リモートサービスクライアント名
	 */
	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

}
