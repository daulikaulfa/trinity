package jp.co.blueship.tri.fw.di.spring;

import static jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.NoSuchMessageException;

import jp.co.blueship.tri.fw.cmn.utils.function.MultiInputFunction;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.di.MessageManager;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;

/**
 * インスタンス生成時、呼び出す側のクラスが ＤＩコンテナに依存しないための機能を提供するアダプタークラスです。
 *
 * @version V3L10.01
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ContextAdapter implements ApplicationContextAware, IContextAdapter {

	private ApplicationContext context = null;

	@Override
	public final void setApplicationContext(ApplicationContext context) {
		this.context = context;
	}

	@Override
	public final Object getBean(String name) throws TriSystemException {
		if (null == name || null == this.context) {
			throw new TriSystemException(SmMessageId.SM004176F);
		}
		return this.context.getBean(name);
	}

	@Override
	public final MessageManager getMessageManager() throws TriSystemException {
		return (MessageManager) ContextAdapterFactory.getContextAdapter().getBean(MessageManager.BEAN_NAME);
	}

	@Override
	public String getValue( String code ) {
		return getValue( code, (String[])null );
	};

	@Override
	public String getValue( String code, String[] args ) {
		return getValue( code, (String[])null, null );
	};

	@Override
	public String getValue( String code, String lang ) {
		return getValue( code, (String[])null, lang );
	};

	@Override
	public String getValue( String code, String[] args, String lang ) {
		return getMessageManager().getValue( code, args, lang );
	};

	@Override
	public String getMessage(IMessageId id) {
		return getMessage((String)null, id);
	}

	@Override
	public String getMessage(String lang, IMessageId id) {
		return getMessage(lang, id, (String) null);
	}

	@Override
	public String getMessage(IMessageId id, String... args) {
		return getMessage((String)null, id, args);
	}

	@Override
	public String getMessage(String lang, IMessageId id, String... args) {
		return getMessageManager().getMessage(lang, id, args);
	}

	@Override
	public String getMessage(String lang, boolean isDecorate , IMessageId id, String... args) {
		return getMessageManager().getMessage(lang, id, args , isDecorate);
	}

	@Override
	public String getMessage(TriLogMessage logId){
		return getMessage(logId, (String)null);
	}

	@Override
	public String getMessage(TriLogMessage logId, String... args) {
		return getMessageManager().getLogMessage(logId, args);
	}
	@Override
	public final List<String> getMessage(ITranslatable exception) {
		return getMessage((String)null, exception);
	}
	@Override
	public final List<String> getMessage(String lang, ITranslatable exception) {

		if (exception == null) {
			return Collections.emptyList();
		}

		if (ContinuableBusinessException.class.isInstance(exception)) {
			return interpolatorMessages(//
					lang,
					((ContinuableBusinessException) exception).getMessageIdList(),//
					((ContinuableBusinessException) exception).getMessageArgsList());
		}

		List<String> messageList = new ArrayList<String>();
		messageList.add(interpolatorMessage(lang, exception.getMessageID(), exception.getMessageArgs()));

		return messageList;
	}

	private List<String> interpolatorMessages(String lang, List<IMessageId> messageIDList, List<String[]> argsList) {
		final String language = lang;

		return composite(messageIDList,//
				argsList,//
				new MultiInputFunction<IMessageId, String[], String>() {

					@Override
					public String apply(IMessageId messageId, String[] args) {
						return interpolatorMessage(language, messageId, args);
					}
				});
	}

	private String interpolatorMessage(String lang, IMessageId messageID, String[] args) {

		try {
			return decorateMessageLabel(messageID, getMessage(lang, messageID, args));
		} catch (NoSuchMessageException e1) {
			// 変換できない場合、メッセージをそのまま戻す
			return messageID.getMessageId();
		}
	}

	@Deprecated
	// V2互換用 メッセージIDよりメッセージを導出するようにしてください。
	@Override
	public final List<String> getMessage(TriRuntimeException exception) {
		return getMessage( (String)null, exception );
	}

	@Deprecated
	// V2互換用 メッセージIDよりメッセージを導出するようにしてください。
	@Override
	public final List<String> getMessage(String lang, TriRuntimeException exception) {
		List<String> messageList = new ArrayList<String>();

		if (exception == null) {
			return messageList;
		}

		MessageManager mes = this.getMessageManager();

		if (exception instanceof ContinuableBusinessException) {
			ContinuableBusinessException cbe = (ContinuableBusinessException) exception;
			List<IMessageId> beMessageList = cbe.getMessageIdList();
			List<String[]> argsList = cbe.getMessageArgsList();
			for (int x = 0; x < beMessageList.size(); x++) {
				IMessageId message = beMessageList.get(x);
				String[] mesArgs = argsList.get(x);

				String outMes = "";
				try {
					outMes = this.decorateMessageLabel(message, mes.getMessage(lang, message, mesArgs));
				} catch (NoSuchMessageException e1) {
					// 変換できない場合、メッセージIDをそのまま戻す
					outMes = message.getMessageId();
				} finally {
					messageList.add(outMes);
				}
			}
		} else if (exception instanceof BaseBusinessException) {
			BaseBusinessException e = (BaseBusinessException) exception;
			IMessageId message = e.getMessageID();
			String[] mesArgs = e.getMessageArgs();

			String outMes = "";
			try {
				outMes = this.decorateMessageLabel(message, mes.getMessage(lang, message, mesArgs));
			} catch (NoSuchMessageException e1) {
				// 変換できない場合、メッセージIDをそのまま戻す
				outMes = message.getMessageId();
			} finally {
				messageList.add(outMes);
			}
		} else {
			String message = exception.getMessage();
			messageList.add(message);
		}

		return messageList;
	}

	@Override
	public List<String> getMessage(IGeneralServiceBean info) throws TriSystemException {
		List<String> messageList = new ArrayList<String>();

		if (info == null) {
			return messageList;
		}

		MessageManager mes = this.getMessageManager();

		try {
			IMessageId infoMessage = info.getInfoMessageId();
			String[] infoMesArgs = info.getInfoMessageArgs();

			if ((null != infoMessage && !"".equals(infoMessage.getMessageId())) && (null != infoMesArgs && 0 != infoMesArgs.length)) {

				String outMes = this.decorateMessageLabel(infoMessage, mes.getMessage(info.getLanguage(), infoMessage, infoMesArgs));
				messageList.add(outMes);

			}
		} finally {
			;
		}

		List<IMessageId> infoMessageList = info.getInfoMessageIdList();
		List<String[]> infoMesArgsList = info.getInfoMessageArgsList();

		if ((null != infoMessageList && 0 != infoMessageList.size()) && (null != infoMesArgsList && 0 != infoMesArgsList.size())) {

			for (int index = 0; index < infoMessageList.size(); index++) {
				IMessageId infoMessage = infoMessageList.get(index);
				String[] infoMesArgs = infoMesArgsList.get(index);

				String outMes = this.decorateMessageLabel(infoMessage, mes.getMessage(info.getLanguage(), infoMessage, infoMesArgs));
				messageList.add(outMes);

			}
		}

		return messageList;
	}

	@Override
	public List<String> getComment(IGeneralServiceBean info) throws TriSystemException {
		return getComment(info, true);
	}

	@Override
	public List<String> getComment(IGeneralServiceBean info, boolean isDecorate) throws TriSystemException {

		List<String> commentList = new ArrayList<String>();

		if (info == null) {
			return commentList;
		}

		MessageManager mes = this.getMessageManager();

		List<IMessageId> infoCommentList = info.getInfoCommentIdList();
		List<String[]> infoCommentArgsList = info.getInfoCommentArgsList();

		if ((null != infoCommentList && 0 != infoCommentList.size()) && (null != infoCommentArgsList && 0 != infoCommentArgsList.size())) {

			for (int index = 0; index < infoCommentList.size(); index++) {

				IMessageId infoComment = infoCommentList.get(index);
				String[] infoCommentArgs = infoCommentArgsList.get(index);

				String outMes = mes.getMessage(info.getLanguage(), infoComment, infoCommentArgs, isDecorate);
				commentList.add(outMes);

			}
		}

		return commentList;
	}

	/**
	 * 画面に表示するエラーメッセージの体裁を整える
	 *
	 * @param id メッセージＩＤ
	 * @param strArgs メッセージ文言
	 * @return 整形後メッセージ文字列
	 */
	private String decorateMessageLabel(IMessageId id, String strArgs) {
		final boolean isDispMsgId = false;
		// XXX この処理では現状、メッセージIDは画面に表示されない。
		if (isDispMsgId) {
			return " [ " + id.getMessageId() + " ] : " + strArgs;
		} else {
			return strArgs;
		}
	}
}
