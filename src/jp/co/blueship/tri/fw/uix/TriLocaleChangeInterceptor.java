package jp.co.blueship.tri.fw.uix;

import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;
import org.springframework.web.servlet.support.RequestContextUtils;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TriLocaleChangeInterceptor extends LocaleChangeInterceptor {

	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws ServletException {
		super.preHandle(request, response, handler);

		String lang = (String)request.getSession().getAttribute(TriSessionAttributes.UserLanguage.value());
		if ( null != lang ) {
			request.getSession().setAttribute(TriSessionAttributes.SelectedLanguage.value(), lang);

			LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
			if ( null != localeResolver ) {
				Locale locale = TriStringUtils.parseLocaleString(lang);
				localeResolver.setLocale(request, response, locale);
			}
		} else {
			LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
			if ( null != localeResolver ) {
				Locale locale = localeResolver.resolveLocale(request);
				request.getSession().setAttribute(TriSessionAttributes.SelectedLanguage.value(), (null == locale)? null: locale.getLanguage());
			}
		}

		return true;
	}

}
