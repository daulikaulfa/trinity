package jp.co.blueship.tri.fw.uix;

import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public class TriModel {
	private Model model;
	private RedirectAttributes redirectAttributes;
	private ModelMap redirectMap;
	private boolean isRedirect = false;
	private IApplicationInfo applicationInfo;
	private ISessionInfo sessionInfo;
	private IRequestInfo requestInfo;

	@SuppressWarnings("unchecked")
	public <SB> SB valueOf( TriModelAttributes key ) {
		if ( TriModelAttributes.Result.equals(key) ) {
			if ( null != this.getModel() ) {
				return (SB)this.getModel().asMap().get(key.value());
			}
		} else if ( TriModelAttributes.RedirectResult.equals(key) ||
					TriModelAttributes.RedirectFilter.equals(key) ) {
			if ( null != this.getRedirectMap() ) {
				return (SB)this.getRedirectMap().get(key.value());
			}
		}

		return null;
	}

	/**
	 * Returns true if this model contains a mapping for the specified key.
	 *
	 * @param key key whose presence in this model is to be tested.
	 * @return true if this model contains a mapping for the specified key
	 */
	public boolean containsKey( TriModelAttributes key ) {
		if ( TriModelAttributes.Result.equals(key) ) {
			if ( null != this.getModel() ) {
				return this.getModel().asMap().containsKey(key.value());
			}
		} else if ( TriModelAttributes.RedirectResult.equals(key) ||
					TriModelAttributes.RedirectFilter.equals(key) ) {
			if ( null != this.getRedirectMap() ) {
				return this.getRedirectMap().containsKey(key.value());
			}
		}

		return false;
	}

	public Model getModel() {
		return model;
	}
	public TriModel setModel(Model model) {
		this.model = model;
		return this;
	}
	public RedirectAttributes getRedirectAttributes() {
		return redirectAttributes;
	}
	public TriModel setRedirectAttributes(RedirectAttributes redirectAttributes) {
		this.redirectAttributes = redirectAttributes;
		return this;
	}
	public ModelMap getRedirectMap() {
		return this.redirectMap;
	}

	public TriModel setRedirectMap(ModelMap redirectMap){
		this.redirectMap = redirectMap;
		return this;
	}

	public boolean isRedirect() {
		return isRedirect;
	}
	public TriModel setRedirect(boolean isRedirect) {
		this.isRedirect = isRedirect;
		return this;
	}
	public IApplicationInfo getApplicationInfo() {
		return applicationInfo;
	}
	public TriModel setApplicationInfo(IApplicationInfo applicationInfo) {
		this.applicationInfo = applicationInfo;
		return this;
	}
	public ISessionInfo getSessionInfo() {
		return sessionInfo;
	}
	public TriModel setSessionInfo(ISessionInfo sessionInfo) {
		this.sessionInfo = sessionInfo;
		return this;
	}
	public IRequestInfo getRequestInfo() {
		return requestInfo;
	}
	public TriModel setRequestInfo(IRequestInfo requestInfo) {
		this.requestInfo = requestInfo;
		return this;
	}

}
