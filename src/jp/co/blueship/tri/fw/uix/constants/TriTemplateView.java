package jp.co.blueship.tri.fw.uix.constants;

/**
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public enum TriTemplateView {
	LotSettings			( "template/LotSettings" ),
	MainTemplate		( "template/MainTemplate" ),
	AdminSettings		( "template/AdminSettings"),
	AdminSettingsV3		( "template/AdminSettingsV3"),
	PrintTemplate		( "template/PrintTemplate"),
	PersonalSettings	( "template/PersonalSettings" ),
	PersonalSettingsV3	( "template/PersonalSettingsV3" ),
	ProjectTemplate		( "template/ProjectTemplate" ),
	ProjectTemplateV3	( "template/ProjectTemplateV3" ),
	;

	private String value = null;

	private TriTemplateView(String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		TriTemplateView type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public static TriTemplateView value(String value) {
		for (TriTemplateView view : values()) {
			if (view.value().equals(value)) {
				return view;
			}
		}

		return LotSettings;
	}
}
