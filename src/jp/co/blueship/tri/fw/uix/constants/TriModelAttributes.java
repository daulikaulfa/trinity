package jp.co.blueship.tri.fw.uix.constants;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum TriModelAttributes {
	none					( "" ),
	Result					( "result" ),
	RedirectResult			( "result" ),
	RedirectFilter			( "filter" )
	;

	private String value = null;

	private TriModelAttributes(String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		TriModelAttributes type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public static TriModelAttributes value(String value) {
		for (TriModelAttributes view : values()) {
			if (view.value().equals(value)) {
				return view;
			}
		}

		return none;
	}
}
