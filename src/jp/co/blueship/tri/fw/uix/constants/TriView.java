package jp.co.blueship.tri.fw.uix.constants;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 */
public enum TriView {
	LogIn					( "common/LogIn" ),
	Messages				( "common/Messages" ),
	BandPopup				( "common/BandPopup" ),
	ForgetPassword			( "common/ForgetPassword" ),
	GlobalHeaderSearchFilter( "common/GlobalHeaderSearchFilter" ),
	GlobalHeaderViewLot		( "common/GlobalHeaderViewLot" ),
	GlobalHeaderViewRequest	( "common/GlobalHeaderViewRequest" ),
	GlobalHeaderViewWiki	( "common/GlobalHeaderViewWiki" ),
	ProgressNotification	( "common/ProgressNotification" ),
	ErrorPage				( "common/SystemError" ),
	DoubleLoginError		( "common/DoubleLoginError" ),
	ChangePassword			( "PasswordExpiry" ),
	DashBoard				( "DashBoard" ),
	DashboardForV3			( "DashboardForV3" ),
	LotCreation				( "LotCreation" ),
	LotEdit					( "LotEdit" ),
	LotDetails				( "LotDetails" ),
	LotResourceViewList		( "LotResourceViewList" ),
	LotResourceFileHistoryDetail
							( "LotResourceViewDetails" ),
	LotClosedDetails		( "LotClosedDetails" ),
	LotSearch				( "SearchThisLot" ),
	LotSettingsThemes   	( "LotSettingsThemes" ),
	CategoryCreation    	( "LotSettingsCategoryCreation" ),
	CategoryList			( "LotSettingsCategoryList" ),
	CategoryEdit			( "LotSettingsCategoryEdit" ),
	MilestoneCreation   	( "LotSettingsMilestoneCreation" ),
	MilestoneEditing    	( "LotSettingsMilestoneEdit" ),
	MilestoneList       	( "LotSettingsMilestoneList" ),
	ChangePropertyList  	( "ChangePropertyList" ),
	ChangePropertyCreate 	( "ChangePropertyCreate" ),
	ChangePropertyDetails 	( "ChangePropertyDetails" ),
	ChangePropertyEdit 		( "ChangePropertyEdit" ),
	ReleasePackageList    	( "ReleasePackageList"),
	BuildPackageList		( "BuildPackageList" ),
	BuildPackageDetails		( "BuildPackageDetails" ),
	BuildPackageCreation	( "BuildPackageCreation" ),
	BuildPackageClose		( "BuildPackageClose" ),
	BuildPackageCreationProcessingStatus
							("BuildPackageCreationProcessingStatus"),
	ReleaseRequestApprovalPendingDetails
							( "ReleaseRequestApprovalPendingDetails" ),
	ReleaseRequestApprovedDetails
							( "ReleaseRequestApprovedDetails" ),
	PrintReleaseRequestApprovalPendingDetails
							( "PrintReleaseRequestApprovalPendingDetails" ),
	PrintReleaseRequestApprovedDetails
							( "PrintReleaseRequestApprovedDetails" ),
	ReleasePackageCreation	( "ReleasePackageCreation" ),
	ReleaseRequestList 		( "ReleaseRequestList"),
	ReleaseRequestCreation	( "ReleaseRequestCreation"),
	ReleaseRequestEdit		( "ReleaseRequestEdit"),
	ReleasePackageDetails	( "ReleasePackageDetails"),
	ReleasePackageCreationProcessingStatus
							("ReleasePackageCreationProcessingStatus"),
	ReportList				( "ReportsList" ),
	ReportCreation			( "ReportsCreation" ),
	ReportBuildPackageCreation
							( "ReportsBuildPackageCreation" ),
	ReportReleaseCreation	( "ReportsReleaseCreation" ),
	ReportReleaseRequestCreation
							( "ReportsReleaseRequestCreation" ),
	ChangeRequestApprovalList
							( "ChangeRequestApprovalList" ),
	PrintChangeApprovalPendingList
							( "PrintChangeApprovalPendingList" ),
	ChangeRequestApprovalPendingApprovalDetails
							( "ChangeRequestApprovalPendingApprovalDetails" ),
	PrintChangeApprovalPendingDetails
							( "PrintChangeApprovalPendingDetails" ),
	ChangeApprovedList		( "ChangeApprovedList"),
	PrintChangeApprovedList ( "PrintChangeApprovedList" ),
	ChangeRequestApprovalApprovedDetails
							( "ChangeRequestApprovalApprovedDetails" ),
	PrintChangeApprovedDetails
							( "PrintChangeApprovedDetails" ),
	RemovalRequestCreation 	( "RemovalRequestCreation" ),
	RemovalRequestList		( "RemovalRequestList"),
	RemovalRequestEdit		( "RemovalRequestEdit"),
	RemovalRequestDetails	( "RemovalRequestDetails"),
	RemovalRequestApprovalPendingCancellation
							( "RemovalRequestApprovalPendingCancellation" ),
	CheckinRequestApprovalPendingCancellation
							("CheckinRequestApprovalPendingCancellation"),
	CheckinOutRequestList 	( "CheckInOutRequestList"),
	CheckOutRequestCreation ( "CheckOutRequestCreation"),
	CheckOutRequestEdit 	( "CheckOutRequestEdit"),
	CheckOutRequestDetails 	( "CheckOutRequestDetails"),
	CheckinRequestCreation 	( "CheckInRequestCreation"),
	CheckinRequestDetails 	( "CheckInRequestDetails"),
	CheckinFileDifferences	( "CheckInFileDifferences" ),
	CheckinRequestProcessingStatus
							("CheckInRequestProcessingStatus"),
	DeploymentJobCreation 	( "DeploymentJobCreation" ),
	DeploymentJobDetails 	( "DeploymentJobDetails" ),
	DeploymentJobEdit 		( "DeploymentJobEdit" ),
	DeploymentJobLatestUpdates
							( "DeploymentJobLatestUpdates" ),
	DeploymentJobList 		( "DeploymentJobList" ),
	DepartmentCreation		( "AdminSettingsDepartmentCreation"),
	DepartmentList			( "AdminSettingsDepartmentList"),
	DepartmentEdit			( "AdminSettingsDepartmentEdit"),
	GroupCreation			( "AdminSettingsGroupCreation"),
	GroupList				( "AdminSettingsGroupList"),
	GroupEdit				( "AdminSettingsGroupEdit"),
	RoleCreation 			( "AdminSettingsRoleCreation" ),
	RoleEdit 				( "AdminSettingsRoleEdit" ),
	RoleList 				( "AdminSettingsRoleList" ),
	UserCreation			( "AdminSettingsUserCreation" ),
	UserMultipleCreation	( "AdminSettingsMultipleUsersCreation" ),
	UserEdit				( "AdminSettingsUserEdit" ),
	UserList				( "AdminSettingsUserList" ),
	ProjectEdit				( "AdminSettingsProjectEdit" ),
	AdminSettingsLanguageTimezone
							( "AdminSettingsLanguageTimezone" ),
	AdminSettingsUICompatibility
							( "AdminSettingsUICompatibility" ),
	AdminSettingsUIcompatibilityforV3
							( "AdminSettingsUIcompatibilityforV3" ),
	ProjectNotification		( "AdminSettingsProjectNotification" ),
	PersonalSettingsUserProfile
							( "PersonalSettingsUserProfile" ),
	PersonalSettingsLanguageTimezone
							( "PersonalSettingsLanguageTimezone" ),
	PersonalsettingsPassword( "PersonalsettingsPassword" ),
	PersonalSettingsUICompatibility
							( "PersonalSettingsUICompatibility" ),
	PersonalSettingsUIcompatibilityforV3
							( "PersonalSettingsUIcompatibilityforV3" ),
	MergeLotList			( "MergeLotList" ),
	Merge					( "Merge" ),
	MergeHistory			( "MergeHistory" ),
	ConflictCheck			( "ConflictCheck" ),
	ConflictCheckResourceFileResults
							( "ConflictCheckResults" ),
	ConflictCheckDifferences( "ConflictCheckDifferences" ),
	PrintConflictCheckDifferences
							( "PrintConflictCheckDifferences" ),
	GlobalSearchResult		( "FragmentGlobalSearchResult" ),
	SearchThisSite			( "SearchThisProject" ),
	ProductLicenseEdit		( "AdminSettingsProductLicense" ),
	ServerStorageInformation( "AdminSettingsServerStorageInformation" ),
	LicenseRegistration		( "ProductLicenseRegistration" ),
	Member 					( "Member" ),
	Profile 				( "Profile" ),
	PasswordRecovery 		("PasswordRecovery"),
	GanttChart				("GanttChart"),
	WikiDetails				("WikiDetails"),
	WikiPageCreation		("WikiPageCreation"),
	WikiPageEdit			("WikiPageEdit"),
	WikiSearch				("WikiSearch"),
	WikiHistory				("WikiHistory"),
	WikiVersionDetails		("WikiVersionDetails"),
	WikiPageDifferences		("WikiPageDifferences"),
	PrintLotDetails			("PrintLotDetails"),
	PrintLotResourceViewList("PrintLotResourceViewList"),
	PrintLotResourceFileHistoryDetail
							("PrintLotResourceViewDetails"),
	PrintChangePropertyDetails
							("PrintChangePropertyDetails"),
	PrintChangePropertyList	("PrintChangePropertyList"),
	PrintCheckInOutRequestList
							("PrintCheckInOutRequestList"),
	PrintCheckOutRequestDetails
							("PrintCheckOutRequestDetails"),
	PrintCheckInRequestDetails
							("PrintCheckInRequestDetails"),
	PrintRemovalRequestDetails
							("PrintRemovalRequestDetails"),
	PrintCheckInFileDifferences
							("PrintCheckInFileDifferences"),
	PrintBuildPackageList	("PrintBuildPackageList"),
	PrintBuildPackageDetails
							("PrintBuildPackageDetails"),
	PrintReleasePackageList ("PrintReleasePackageList"),
	PrintReleasePackageDetails
							("PrintReleasePackageDetails"),
	PrintReleaseRequestList	("PrintReleaseRequestList"),
	PrintReleaseRequestApprovalPendingList
							("PrintReleaseRequestApprovalPendingList"),
	PrintReleaseRequestApprovedList
							("PrintReleaseRequestApprovedList"),
	PrintMergeLotList		("PrintMergeLotList"),
	PrintMergeHistory		("PrintMergeHistory"),
	PrintConflictCheckResults
							("PrintConflictCheckResults"),
	PrintRemovalRequestList
							("PrintRemovalRequestList"),
	PrintSearchThisProject	("PrintSearchThisProject"),
	PrintSearchThisLot		("PrintSearchThisLot"),
	PrintReportList			("PrintReportsList"),
	PrintWikiPageDifferences
							("PrintWikiPageDifferences"),
	PrintWikiPage
							("PrintWikiPage"),
	PrintDeploymentJobList 	("PrintDeploymentJobList"),
	PrintDeploymentJobDetails
							("PrintDeploymentJobDetails"),
	PrintDeploymentJobLatestUpdates
							("PrintDeploymentJobLatestUpdates"),
	;

	private String value = null;

	private TriView(String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		TriView type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public static TriView value(String value) {
		for (TriView view : values()) {
			if (view.value().equals(value)) {
				return view;
			}
		}

		return LogIn;
	}
}
