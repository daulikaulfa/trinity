package jp.co.blueship.tri.fw.uix;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.ScreenType;
import jp.co.blueship.tri.fw.constants.SmDesignEntryKeyByIntervalTime;
import jp.co.blueship.tri.fw.constants.SmDesignEntryKeyByTask;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.ITranslatable;
import jp.co.blueship.tri.fw.security.TriAuthenticationContext;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionInfoFactory;
import jp.co.blueship.tri.fw.session.application.ApplicationInfoFactory;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.constants.TriSessionAttributes;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.session.request.RequestInfoFactory;
import jp.co.blueship.tri.fw.sm.domainx.account.dto.FlowLogInServiceBean;
import jp.co.blueship.tri.fw.uix.constants.TriModelAttributes;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public abstract class TriControllerSupport<T extends DomainServiceBean > {



	protected String getUserId() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}

	protected String getUserName() {
		TriAuthenticationContext authenticationContext = //
		(TriAuthenticationContext) ContextAdapterFactory.getContextAdapter().getBean(TriAuthenticationContext.BEAN_NAME);

		return authenticationContext.getAuthUserName();
	}

	protected T initParam( T param, TriModel triModel ) {
		param.setUserId( this.getUserId() );

		String userName = (String)triModel.getSessionInfo().getAttribute( TriSessionAttributes.UserName.value() );
		if (TriStringUtils.isEmpty(userName)) {
			param.setUserName(this.getUserName());
		} else {
			param.setUserName(userName);
		}

		return param;
	}

	private IApplicationInfo getApplicationInfo(HttpServletRequest request) {
		IApplicationInfo aplInfo = ApplicationInfoFactory.getApplicationContext(ApplicationInfoFactory.ASSIGN_SERVLETCONTEXT);
		aplInfo.init(request);
		return aplInfo;
	}

	private ISessionInfo getSessionInfo(HttpServletRequest request) {
		ISessionInfo sessionInfo = SessionInfoFactory.getSessionContext(SessionInfoFactory.ASSIGN_HTTPSESSION);
		sessionInfo.init(request);
		return sessionInfo;
	}

	private IRequestInfo getRequestInfo( HttpServletRequest request ) {
		IRequestInfo requestInfo = RequestInfoFactory.getRequestContext(RequestInfoFactory.ASSIGN_HTTPSERVLETREUEST);
		requestInfo.init(request);
		return requestInfo;
	}

	@ModelAttribute
	public T getParam(
			HttpServletRequest request,
			Model model,
			RedirectAttributes redirectAttributes,
			ModelMap modelMap,
			TriModel triModel,
			@RequestParam(value = "WindowsId", required = false) String windowsId,
			@RequestParam(value = "RequestType", required = false) String requestType,
			@RequestParam(value = "forward", required = false) String forward,
			@RequestParam(value = "referer", required = false) String referer) {


		triModel
			.setModel(model)
			.setRedirectAttributes(redirectAttributes)
			.setRedirectMap(modelMap)
			.setApplicationInfo(getApplicationInfo(request))
			.setSessionInfo(getSessionInfo(request))
			.setRequestInfo(getRequestInfo(request))
			;

		if ( triModel.containsKey(TriModelAttributes.RedirectResult) ) {
			DomainServiceBean prevBean = triModel.valueOf(TriModelAttributes.RedirectResult);
				 windowsId = String.valueOf(prevBean.getHeader().getWindowsId());

			return getParam(
					triModel,
					windowsId,
					prevBean.getParam().getRequestType().value(),
					prevBean.getParam().getForward(),
					prevBean.getParam().getReferer() );
		}

		if ( model.asMap().containsKey("prevResult") ) {
			ISessionInfo sesInfo = triModel.getSessionInfo();
			triModel.setRedirect(true);
			@SuppressWarnings("unchecked")
			T prevBean = (T) model.asMap().get("prevResult");
			int id = prevBean.getHeader().getWindowsId();
			String sessionKey = this.getSessionKey(id, this.getServiceId().value());
			sesInfo.setAttribute( sessionKey, prevBean);
			return prevBean;
		}


		return getParam(triModel, windowsId, requestType, forward, referer);
	}

	@SuppressWarnings({ "unchecked" })
	public T getParam(
			TriModel model,
			String windowsId,
			String requestType,
			String forward,
			String referer) {

		ISessionInfo sesInfo = model.getSessionInfo();
		T bean;

		Integer id = 1;
		if ( TriStringUtils.isEmpty(windowsId) || Integer.valueOf(windowsId) == 0) {
			Integer redirectWindowsId = (Integer)sesInfo.getAttribute(TriSessionAttributes.RedirectWindowsId.value());
			if (TriStringUtils.isEmpty(redirectWindowsId) || Integer.valueOf(redirectWindowsId) == 0) {
				if ( null == sesInfo.getAttribute(TriSessionAttributes.WindowsId.value()) ) {
					id = 1;
				} else {
					id = (Integer)sesInfo.getAttribute(TriSessionAttributes.WindowsId.value()) + 1;
				}

				sesInfo.setAttribute(TriSessionAttributes.WindowsId.value(), id);
			} else {
				id = redirectWindowsId;
				sesInfo.removeAttribute(TriSessionAttributes.RedirectWindowsId.value());
			}

		} else {
				id = Integer.parseInt(windowsId);
		}

		String sessionKey = this.getSessionKey(id, this.getServiceId().value());

		if ( this.getServiceId().statefulSession() && null != sesInfo.getAttribute( sessionKey ) ) {
			//continue
		} else {
			if ( RequestType.init.equals(RequestType.value(requestType)) || null == sesInfo.getAttribute( sessionKey ) ) {
				bean = getServiceBean( sesInfo );
				if ( null != bean ) {
					sesInfo.setAttribute( sessionKey, bean);
				}
			}
		}

		this.removeServiceAttributes( model, id );

		bean = (T)sesInfo.getAttribute( sessionKey );

		if ( null != bean ) {
			if ( ! (bean instanceof FlowLogInServiceBean) ) {
				this.initParam( bean, model );
			}

			bean.getHeader().setWindowsId(id);

			this.setHeader(bean, sesInfo, id);

			DomainServiceBean.RequestParam param = bean.getParam();
			if ( null != param ) {
				param
					.setRequestType(RequestType.value(requestType))
					.setReferer(referer)
					.setForward(forward);
			}
			String timeZone = (String)sesInfo.getAttribute(TriSessionAttributes.UserTimeZone.value());
			if ( null != timeZone ) {
				bean.setTimeZone(TriDateUtils.getTimeZone(timeZone));
			}
			String lang = (String)sesInfo.getAttribute(TriSessionAttributes.SelectedLanguage.value());
			if ( null != lang ) {
				bean.setLanguage(lang);
			}
		}

		return bean;
	}

	public abstract ServiceId getServiceId();
	protected abstract T getServiceBean( ISessionInfo sesInfo );

	/**
	 * @param serviceId Service ID
	 * @param serviceBean Domain Service bean
	 * @param model
	 */
	public void execute( ServiceId serviceId, DomainServiceBean serviceBean, TriModel model ) {
		PreConditions.assertOf(serviceId != null, "ServiceId is not specified");
		PreConditions.assertOf(serviceBean != null, "IGeneralServiceBean is not specified");
		PreConditions.assertOf(model != null, "TriModel is not specified");

		List<String> message = serviceBean.getMessages();
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>().setServiceBean( serviceBean );
		DomainServiceBean domainBean = (DomainServiceBean)serviceDto.getServiceBean();

		try {
			DomainServiceBean prevBean = null;

			if ( model.isRedirect() && model.containsKey(TriModelAttributes.RedirectResult) ) {
				prevBean = model.valueOf(TriModelAttributes.RedirectResult);
			}

			IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
			IGenericTransactionService service = (IGenericTransactionService)ca.getBean( "generalService" );
			domainBean.getMessageInfo().clear();

			if ( null == domainBean.getScreenType() ) {
				domainBean.setScreenType( ScreenType.Type.next.value() );
			}

			ISessionInfo sesInfo = model.getSessionInfo();
//			domainBean.setLockLotNo( (String)sesInfo.getAttribute( TriSessionAttributes.SelectedLotId.value()) );
			domainBean.setLockLotNo(this.getSessionSelectedLot(sesInfo, serviceBean));
			domainBean.setStatusMatrixV3(false);

			service.execute(serviceId.value(), serviceDto);

			if ( ! domainBean.getMessageInfo().isEmptyFlashTranslatable() ) {
				for ( ITranslatable translatable: domainBean.getMessageInfo().getFlashTranslatable() ) {
					domainBean.getMessageInfo().getFlashMessages().add(
						ca.getMessage(domainBean.getLanguage(),
										false, // decorate messageId
										translatable.getMessageID(),
										translatable.getMessageArgs()
										)
					);
				}
			}

			if ( null != prevBean ) {
				domainBean.getMessageInfo().addFlashMessages(prevBean.getMessageInfo().getFlashMessages());
				domainBean.setMessages(prevBean.getMessages());
			}

			if(message != null && !message.isEmpty()){
				if ( model.getModel().asMap().containsKey("prevResult") ) {
					domainBean.setMessages(message);
				}
			}

		} catch (Exception e) {
			if ( e instanceof ITranslatable ) {
				IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
				domainBean.setMessages( ca.getMessage(domainBean.getLanguage(), (ITranslatable)e) );
			}

			ExceptionUtils.reThrowIfTrinityException(e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Removes the object bound with the specified name from this session.
	 * If the session does not have an object bound with the specified name, this method does nothing.
	 * <br>
	 * <br>指定された名前でバインドされた文字列をこのセッションから削除します。
	 * セッションに指定された名前でバインドされた文字列がない場合、このメソッドは何も実行しません。
	 *
	 * @param model
	 * @param id
	 */
	protected void removeAttribute( TriModel model, int id ) {
		model.getSessionInfo().removeAttribute( this.getServiceId().value() );
	}

	/**
	 * Removes the object session of stateles service from this session.
	 * <br>
	 * <br>他のstatelessなSessionをクリアします。。
	 *
	 * @param model
	 * @param id
	 */
	private void removeServiceAttributes( TriModel model, int id ) {
		if ( ! this.getServiceId().sessionGC() )
			return;

		for ( ServiceId serviceId: ServiceId.values() ) {
			if ( ServiceId.none.equals(serviceId) )
				continue;

			if ( this.getServiceId().equals(serviceId) ) {
				continue;
			}

			ISessionInfo sesInfo = model.getSessionInfo();
			String sessionKey = this.getSessionKey(id, serviceId.value());

			if ( serviceId.statefulSession() ) {
				if ( sesInfo.getAttribute( sessionKey ) != null &&
						sesInfo.getAttribute( sessionKey ) instanceof DomainServiceBean) {
					DomainServiceBean serviceBean = (DomainServiceBean) sesInfo.getAttribute( sessionKey );
					serviceBean.getMessageInfo().clear();
				}

				continue;
			}

			if ( null == sesInfo.getAttribute( sessionKey ) )
				continue;

			sesInfo.removeAttribute( sessionKey );
		}
	}

	protected String getSessionKey( int windowsId, String value ) {
		return String.valueOf(windowsId) + ":" + value;
	}

	protected String getSessionValue(ISessionInfo sesInfo, DomainServiceBean bean, String value) {
		Object obj = null;
		if (bean.getHeader().getWindowsId() > 0) {
			obj = sesInfo.getAttribute(this.getSessionKey(bean.getHeader().getWindowsId(), value));
		}
//		if (obj == null) {
//			obj = sesInfo.getAttribute(value);
//		}
		if (obj != null) {
			return (String)obj;
		}
		return null;
	}

	protected String getSessionSelectedLot(ISessionInfo sesInfo, DomainServiceBean bean) {
		return getSessionValue(sesInfo, bean, TriSessionAttributes.SelectedLotId.value());
	}

	protected void setHeader(DomainServiceBean bean, ISessionInfo sesInfo, Integer id) {
		IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
		{
			String projectName = (String)sesInfo.getAttribute( TriSessionAttributes.ProjectName.value() );
			String projectIconPath = (String)sesInfo.getAttribute( TriSessionAttributes.ProjectIconPath.value() );

			String userIconPath = (String)sesInfo.getAttribute( TriSessionAttributes.UserIconPath.value() );

			String lotName = (String)sesInfo.getAttribute(this.getSessionKey(id, TriSessionAttributes.SelectedLotName.value()));
			String lotIconPath = (String)sesInfo.getAttribute(this.getSessionKey(id, TriSessionAttributes.SelectedLotIconPath.value()));
			String themeColor = (String)sesInfo.getAttribute(this.getSessionKey(id, TriSessionAttributes.SelectedLotThemeColor.value()));
			Boolean isLotEnabled = (Boolean)sesInfo.getAttribute(this.getSessionKey(id, TriSessionAttributes.SelectedLotEnabled.value()));

			String lotId = null;
			Object obj = sesInfo.getAttribute(this.getSessionKey(id, TriSessionAttributes.SelectedLotId.value()));
			if (obj == null) {
				lotId = (String)sesInfo.getAttribute(TriSessionAttributes.SelectedLotId.value());
				lotName = (String)sesInfo.getAttribute(TriSessionAttributes.SelectedLotName.value());
				lotIconPath = (String)sesInfo.getAttribute(TriSessionAttributes.SelectedLotIconPath.value());
				themeColor = (String)sesInfo.getAttribute(TriSessionAttributes.SelectedLotThemeColor.value());
				isLotEnabled = (Boolean)sesInfo.getAttribute(TriSessionAttributes.SelectedLotEnabled.value());

				saveLotInfoToSession(sesInfo, id, lotId, lotName, lotIconPath, themeColor, isLotEnabled);
			} else {
				lotId = (String)obj;
			}

			boolean isLotSelected = TriStringUtils.isEmpty(lotId);
			String headerName = lotName + " (" + lotId + ")";

			bean.getHeader()
				.setProjectName( projectName )
				.setProjectIconPath( projectIconPath )
				.setUserIconPath( userIconPath )
				.setHeaderName( headerName )
				.setHeaderIconPath( lotIconPath )
				.setReloadInterval( sheet.intValue(SmDesignEntryKeyByTask.reloadInterval) )
				.setProgressNotification( sheet.intValue(SmDesignEntryKeyByIntervalTime.progressNotification) )
			;
			bean.getHeader().getSelectedLot().setThemeColor(themeColor)
											 .setSelected(isLotSelected)
											 .setEnabled(null != isLotEnabled ? isLotEnabled : true);
		}
	}

	public void saveLotInfoToSession(ISessionInfo sesInfo, Integer windowsId, String lotId, String lotName, String lotIconPath, String themeColor, Boolean isLotEnabled) {
		if (windowsId > 0) {
			sesInfo.setAttribute(this.getSessionKey(windowsId, TriSessionAttributes.SelectedLotId.value()), lotId);
			sesInfo.setAttribute(this.getSessionKey(windowsId, TriSessionAttributes.SelectedLotThemeColor.value()), themeColor);
			sesInfo.setAttribute(this.getSessionKey(windowsId, TriSessionAttributes.SelectedLotIconPath.value()), lotIconPath);
			sesInfo.setAttribute(this.getSessionKey(windowsId, TriSessionAttributes.SelectedLotName.value()), lotName);
			sesInfo.setAttribute(this.getSessionKey(windowsId, TriSessionAttributes.SelectedLotEnabled.value()), isLotEnabled);
		} else {
			sesInfo.setAttribute(TriSessionAttributes.SelectedLotId.value(), lotId);
			sesInfo.setAttribute(TriSessionAttributes.SelectedLotThemeColor.value(), themeColor);
			sesInfo.setAttribute(TriSessionAttributes.SelectedLotIconPath.value(), lotIconPath);
			sesInfo.setAttribute(TriSessionAttributes.SelectedLotName.value(), lotName);
			sesInfo.setAttribute(TriSessionAttributes.SelectedLotEnabled.value(), isLotEnabled);
		}
	}

	public  DomainServiceBean getPrevBean(TriModel model,int windowId) {
		ISessionInfo sessionInfo = model.getSessionInfo();
		String key = windowId + "prevBean";
		return (DomainServiceBean) sessionInfo.getAttribute(key);
	}
	protected  void setPrevModel(TriModel model) {
		DomainServiceBean serviceBean = (DomainServiceBean) model.getModel().asMap().get("result");
		ISessionInfo sessionInfo = model.getSessionInfo();
		String key = serviceBean.getHeader().getWindowsId() + "prevBean";
		sessionInfo.setAttribute(key, serviceBean);

	}

	protected  void setPrevPath(String path, TriModel model){
		DomainServiceBean serviceBean = (DomainServiceBean) model.getModel().asMap().get("result");
		ISessionInfo sessionInfo = model.getSessionInfo();
		String key = serviceBean.getHeader().getWindowsId() + "prevPath";
		sessionInfo.setAttribute(key, path);


	}
	public  String getPrevPath(TriModel model,int windowId){
		ISessionInfo sessionInfo = model.getSessionInfo();
		String key = windowId + "prevPath";
		if(sessionInfo.getAttribute(key) != null)
		return  sessionInfo.getAttribute(key).toString();

		return "";
	}

	protected  void setPrev(TriModel model){

		if( null == model.getModel().asMap().get("result") ){
			return;
		}

		DomainServiceBean serviceBean = (DomainServiceBean) model.getModel().asMap().get("result");
			if (RequestType.init.equals(RequestType.value(serviceBean.getParam().getRequestType().value()))) {
				setPrevModel(model);
				if(model.getRequestInfo().getAttribute("org.springframework.web.servlet.HandlerMapping.pathWithinHandlerMapping") != null){
					String path = String.valueOf(model.getRequestInfo().getAttribute("org.springframework.web.servlet.HandlerMapping.pathWithinHandlerMapping"));

					setPrevPath(path, model);
				}
			}
	}
}
