package jp.co.blueship.tri.fw.uix;

import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

import jp.co.blueship.tri.fw.agent.concurrent.TriTaskExecutor;
import jp.co.blueship.tri.fw.cmn.utils.annotations.VisibleForTesting;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.spring.Contexts;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;

/**
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class TriDispatcherServletSupport extends DispatcherServlet {
	@Override
	protected void initBeanWrapper(BeanWrapper bw) throws BeansException {
		super.initBeanWrapper(bw);
	}

	@Override
	protected void onRefresh(ApplicationContext context) {
		super.onRefresh( context );
		Contexts.getInstance().setApplicationContext(context.getParent());

		{
			//AllowFilePathUtil.init();
			ProductActivate.init();
		}

	}

	/**
	 * このServlertが破棄されるタイミングでServletContainerから呼ばれます。<br/>
	 * ここでは、スレッドプール機構のシャットダウン処理を行います。
	 *
	 * @see org.apache.struts.action.ActionServlet#destroy()
	 */
	@Override
	public void destroy() {
		shutDownTriTaskExecutor();
		super.destroy();
	}

	/**
	 * スレッドプール機構のシャットダウン処理を行います。
	 */
	@VisibleForTesting
	protected final void shutDownTriTaskExecutor() {

		TriTaskExecutor executor = (TriTaskExecutor) ContextAdapterFactory.//
				getContextAdapter().//
				getBean(TriTaskExecutor.BEAN_NAME);
		if (executor == null || !executor.isActive()) {
			return;
		}

		executor.shutDown();
	}

}
