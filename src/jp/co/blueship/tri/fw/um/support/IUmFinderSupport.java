package jp.co.blueship.tri.fw.um.support;

import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;

/**
 * ユーザ関連情報を検索するためのサービス機能を提供するインタフェースです。
 *
 * @author Takayuki Kubo
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public interface IUmFinderSupport extends IUmDaoFinder {


	/**
	 * 指定したユーザが所属するグループを検索します。
	 *
	 * @param userId ユーザID
	 * @return グループのリスト。見つからない場合は空のリスト。
	 */
	public List<IGrpEntity> findGroupByUserId(String userId);

	/**
	 * 指定したグループに所属するユーザを検索します。
	 *
	 * @param groupId グループID
	 * @return ユーザのリスト。見つからない場合は空のリスト。
	 */
	public List<IUserEntity> findUserByGroup(String groupId);

	/**
	 * 指定したグループに所属するユーザを検索します。
	 *
	 * @param groupIds
	 *            グループID
	 * @return ユーザのリスト。見つからない場合は空のリスト。
	 */
	public List<IUserEntity> findUserByGroups(String... groupIds);

	/**
	 * Find all users in the same group with current user
	 *
	 * @param userId current user id
	 * @return List of users
	 */
	public List<IUserEntity> findUsersInSameGroupWithCurrentUser(String userId);

	/**
	 * 指定した部署に所属するユーザを検索します。
	 *
	 * @param deptId 部署ID
	 * @return ユーザのリスト。見つからない場合は空のリスト。
	 */
	public List<IUserEntity> findUserByDept(String deptId);

	/**
	 * ユーザIDからユーザ情報を検索します。
	 *
	 * @param userId ユーザID
	 * @return ユーザ情報。見つからない場合はsystemException。
	 */
	public IUserEntity findUserByUserId(String userId);

	/**
	 * ユーザIDからユーザ情報が重複しているか確認します。
	 *
	 * @param userId ユーザID
	 * @return 重複している true 重複していない false
	 */
	public boolean isExistByUserId(String userId);

	/**
	 * ユーザ名からユーザ情報を検索します。
	 *
	 * @param userNm ユーザ名
	 * @return ユーザ情報のリスト。見つからない場合は空のリスト。
	 */
	public List<IUserEntity> findUserByUserNm(String userNm);

		/**
	 * ユーザ情報を全件取得します。
	 *
	 * @return ユーザのリスト。見つからない場合は空のリスト。
	 */
	public List<IUserEntity> findAllUser();

	/**
	 * 指定されたロットに紐付くメールグループIDを検索します。
	 *
	 * @param lotId ロットID
	 * @return メールグループIDのリスト。無い場合は空のリスト。
	 */
	public List<String> findMailGroupIdsByLotId(String lotId);

	/**
	 * 指定されたグループIDに紐付くグループ情報を検索します。
	 *
	 * @param groupId グループID
	 * @return グループ情報。無い場合はシステム例外をスローする。
	 */
	public IGrpEntity findGroupById(String groupId);

	/**
	 * 採番されたグループIDの重複を検索します。採番時の検索のみ使用してください。
	 * @param gruopId
	 * @return 重複がない場合true ,重複する場合false
	 */
	public boolean checkDuplicationByNumberingGroupId(String groupId);

	/**
	 * 指定されたグループ名に紐付くグループ情報を検索します。
	 * TODO V2互換。正しく画面からIDを受け取るよう是正する必要がある
	 *
	 * @param groupId グループ名
	 * @return グループ情報。無い場合はシステム例外をスローする。
	 */
	public IGrpEntity findGroupByName(String groupName);

	/**
	 * グループ情報を全件取得します。
	 *
	 * @return グループ情報。無い場合はシステム例外をスローする。
	 */

	public List<IGrpEntity> findAllGroup();

	/**
	 * 指定されたユーザのロールを検索します。
	 * @param userId ユーザID
	 * @return ロールのリスト
	 */
	public List<IUserRoleLnkEntity> findUserRoleLnkByUserId(String userId);

	/**
	 * ユーザ・ロール(Lnk)情報を全件取得します。
	 * @return ユーザ・ロール(Lnk)エンティティのリスト。ない場合は空のリスト。
	 */
	public List<IUserRoleLnkEntity> findAllUserRoleLnk();

	/**
	 * 指定されたロール情報に紐付くサービスカテゴリを検索します。
	 * @param roleArray ロール情報
	 * @return サービスカテゴリ情報。 ない場合は空のリスト
	 */
	public List<IRoleSvcCtgLnkEntity> findSvcCtgLnkByRoleId( String... roleIs );

	/**
	 * 指定されたサービスカテゴリ情報
	 * @param svcCtgIds
	 * @return
	 */
	public List<ISvcCtgSvcLnkEntity> findSvcCtgSvcLnkBySvcCtgId( String... svcCtgIds );

	/**
	 * 指定されたグループIDに紐付くグループ・ユーザ(Lnk)情報を検索します。
	 * @param grpId グループID
	 * @return グループ・ユーザ(Lnk)エンティティのリスト ない場合は空のリスト
	 */
	public List<IGrpUserLnkEntity> findGrpUserLnkByGrpId( String... grpId );

	/**
	 * 指定されたユーザIDに紐付くグループ・ユーザ(Lnk)情報を検索します。
	 * @param userId ユーザID
	 * @return グループ・ユーザ(Lnk)エンティティのリスト ない場合は空のリスト
	 */
	public List<IGrpUserLnkEntity> findGrpUserLnkByUserId( String... userId );

	/**
	 * 指定されたグループIDに紐付くグループ・ロール(Lnk)情報を検索します。
	 * @param grpId グループID
	 * @return グループ・ロール(Lnk)エンティティのリスト ない場合は空のリスト
	 */
	public List<IGrpRoleLnkEntity> findGrpRoleLnkByGrpId( String... grpId  );

	/**
	 * 指定されたロールIDに紐付くグループ・ロール(Lnk)情報を検索します。
	 * @param roleId ロールID
	 * @return グループ・ロール(Lnk)エンティティのリスト ない場合は空のリスト
	 */
	public List<IGrpRoleLnkEntity> findGrpRoleLnkByRoleId( String... roleId  );

	/**
	 * 指定された部署IDに紐付く部署情報を検索します。
	 * @param deptId
	 * @return 部署エンティティ ない場合はSystemExceptionを返します。
	 */
	public IDeptEntity findDeptByDeptId( String deptId );

	/**
	 * Find category by primary
	 *
	 * @param ctgId Category ID
	 * @return Category Entity
	 */
	public ICtgEntity findCtgByPrimaryKey(String ctgId);

	/**
	 * 採番された部署IDの重複を検索します。採番時の検索のみ使用してください。
	 * @param deptId
	 * @return 重複がない場合true ,重複する場合false
	 */
	public boolean checkDuplicationByNumberingDeptId( String deptId );

	/**
	 * 部署情報を全件取得します。
	 * @return 部署エンティティのリスト ない場合はSystemExceptionを返します。
	 */
	public List<IDeptEntity> findAllDept();

	/**
	 * ロール情報を全件取得します。
	 * @return ロールエンティティのリスト 無い場合はシステム例外をスローする。
	 */
	public List<IRoleEntity> findAllRole();

	/**
	 * 指定されたロールIDに紐付くロール情報を検索します。
	 * @return ロールエンティティ 無い場合はシステム例外をスローする。
	 */
	public IRoleEntity findRoleByRoleId( String roleId );

	/**
	 * 採番されたロールIDの重複を検索します。採番時の検索のみ使用してください。
	 * @param roleId
	 * @return 重複がない場合true ,重複する場合false
	 */
	public boolean checkDuplicationByNumberingRoleId( String roleId );

	/**
	 * 指定されたユーザIDに紐付くユーザ・パスワード履歴情報を検索します。
	 * @param userId ユーザID
	 * @return ユーザ・パスワード履歴エンティティのリスト ない場合は空のリスト
	 */
	public List<IUserPassHistEntity> findUserPassHistByUserId( String... userId );

	/**
	 * 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 *
	 * @param userId
	 * @param dataCtgCd
	 * @param dataId
	 * @return 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 */
	public IAccsHistEntity findAccsHistByPrimaryKey(String userId, String dataCtgCd, String dataId);


	/**
	 * Update a record into access history table
	 *
	 * @param serviceBean serviceBean
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 */
	public void updateAccsHist( GenericServiceBean serviceBean, ITableAttribute attr, String dataId );

	/**
	 * Update a record into access history table
	 *
	 * @param serviceBean serviceBean
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 * @param table items to be updated
	 */
	public void updateAccsHist( GenericServiceBean serviceBean, ITableAttribute attr, String dataId, AccsHistItems... updateItems );

	/**
	 * 指定されたユーザIDから通知するプロセスを返します
	 * @param userId
	 * @return 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 */
	public List<IUserProcNoticeEntity>  findUserProcNoticeByUserId ( String userId) ;

	/**
	 * 指定されたプロセスIDから通知プロセスを返します
	 * @param procId
	 * @return 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 */
	public IUserProcNoticeEntity  findUserProcNoticeByProcId ( String procId) ;

	/**
	 * Find all categories by using lot Id
	 * @param lot Id
	 * @return List of Categories
	 */
	public List<ICtgEntity> findCtgByLotId(String lotId);

	/**
	 * Find all category links by using category Id
	 * @param category Id
	 * @return List of category link
	 */
	public List<ICtgLnkEntity> findCtgLnkByCategoryId(String categoryId);

	/**
	 * Find all milestones by using lot id
	 * @param lotId
	 * @return List of Milestones
	 */
	public List<IMstoneEntity> findMstoneByLotId(String lotId);

	/**
	 * Find all milestone links by using milestone Id
	 * @param mstone Id
	 * @return List of milestone link
	 */
	public List<IMstoneLnkEntity> findMstoneLnkByMstoneId(String mstoneId);

	/**
	 * Find milestone by primary
	 *
	 * @param mstoneId Milestone ID
	 * @return Milestone Entity
	 */
	public IMstoneEntity findMstoneByPrimaryKey(String mstoneId);

	/**
	 * Find Category Link by data Id
	 *
	 * @param dataId Data ID
	 * @param dataCtgCd Data Category CD
	 * @return CtgLnkEntity Entity
	 */
	public ICtgLnkEntity findCtgLnkByPrimaryKey(String dataId, String dataCtgCd);

	/**
	 * Find Milestone Link by data Id
	 *
	 * @param dataId Data ID
	 * @param dataCtgCd Data Category CD
	 * @return MstoneLnkEntity Entity
	 */
	public IMstoneLnkEntity findMstoneLnkByPrimaryKey(String dataId, String dataCtgCd);

	/**
	 * Find Group User by Group Names
	 * @param grpNms
	 * @return List<IGrpEntity>
	 */
	public List<IGrpEntity> findGroupByGroupIds(String... grpIds);

	/**
	 * Find Grp_user_lnk by primary key
	 * @param groupId
	 * @param userId
	 * @return
	 */
	public IGrpUserLnkEntity findGrpUserLnkByPrimaryKey(String groupId, String userId);

	/**
	 * 指定されたHistIdから該当する履歴データを返します
	 * @param histId
	 * @return 検索結果を戻します。条件に該当する情報が1件もない場合はNull。
	 */
	public IHistEntity findHistByPrimaryKey(String histId);

	/**
	 * Insert a record into category link table
	 *
	 * @param ctgId category ID
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 */
	public void registerCtgLnk( String ctgId, ITableAttribute attr, String dataId );
	/**
	 * Insert a record into category link table
	 *
	 * @param mstoneId milestone ID
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 */
	public void registerMstoneLnk( String mstoneId, ITableAttribute attr, String dataId );

	/**
	 * Update a record into category link table
	 *
	 * @param ctgId category ID
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 */
	public void updateCtgLnk( String ctgId, ITableAttribute attr, String dataId );
	/**
	 * Update a record into category link table
	 *
	 * @param mstoneId milestone ID
	 * @param attr the table attribute
	 * @param dataId primary key of the table
	 */
	public void updateMstoneLnk( String mstoneId, ITableAttribute attr, String dataId );

	/**
	 * Cleaning a record of category link table
	 *
	 * @param attr table attribute
	 * @param dataId primary key of the table
	 */
	public void cleaningCtgLnk( ITableAttribute attr, String dataId );

	/**
	 * Cleaning a record of milestone link table
	 *
	 * @param attr table attribute
	 * @param dataId primary key of the table
	 */
	public void cleaningMstoneLnk( ITableAttribute attr, String dataId );

	/**
	 * Selected Role by grp_id
	 * @param groupId
	 * @return
	 */
	public List<IRoleEntity> findRoleByGroup(String groupId);

	/**
	 * Get all user_ids belong to this group
	 * @param groupId
	 * @return
	 */
	public List<String> getUserIdsByGroup(String groupId);

	public void cleaningGrpRoleLnk(String grpId, String roleId);

	public void cleaningGrpUserLnk(String grpId, String userId);

	public void cleaningUserRoleLnk(String userId, String roleId);

	public void cleaningAndUpdateUserRoleLnk(String[] userList);

	public void registerGrpRoleLnk(String[] grpIds, String[] roleIds);

	public void registerGrpUserLnk(String[] grpIds, String[] userIds);

	public void registerUserRoleLnk(String[] userIds, String[] roleIds);

	/**
	 * Get all Service catalog records
	 * @return List<ISvcCtgEntity>
	 */
	public List<ISvcCtgEntity> findAllSvcCtg();

	public List<ISvcCtgSvcLnkEntity> findSvcCtgSvcLnkBySvcIds(String... svcIds);

	public List<ICtgEntity> findCtgByLotIds(String... lotIds);

	public List<IMstoneEntity> findMstoneByLotIds(String... lotIds);

	public IProjectEntity findProjectByPrimaryKey(String productId);

	public IWikiEntity findWikiByPrimaryKey(String wikiId);

	public IFilterEntity findFilterByPrimaryKey(String filterId);

	public String getIconPath (String userId);

	public LookAndFeel getLookAndFeel( String userId );

	public LookAndFeel getAdminLookAndFeel();

	public List<String> getSvcCtgIdByRoleId (String roleId);
}