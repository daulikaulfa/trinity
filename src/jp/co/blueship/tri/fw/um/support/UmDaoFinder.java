package jp.co.blueship.tri.fw.um.support;

import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.fw.dao.orm.FinderSupport;
import jp.co.blueship.tri.fw.um.dao.accshist.IAccsHistDao;
import jp.co.blueship.tri.fw.um.dao.ctg.CtgNumberingDao;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgDao;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.filter.FilterNumberingDao;
import jp.co.blueship.tri.fw.um.dao.filter.IFilterDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.hist.IAreqHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.ICtgHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.ILotHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IMstoneHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IViewNotificationToUserDao;
import jp.co.blueship.tri.fw.um.dao.locksvc.ILockSvcDao;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneDao;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneLnkDao;
import jp.co.blueship.tri.fw.um.dao.mstone.MstoneNumberingDao;
import jp.co.blueship.tri.fw.um.dao.project.IProjectDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgSvcLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserPassHistDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserProcNoticeDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserUrlDao;
import jp.co.blueship.tri.fw.um.dao.user.IViewUserAccsHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IViewWikiTagsDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiAttachedFileDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiTagDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiUpdateHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.WikiNumberingDao;

/**
 * UM管理のDAOのGetterを提供するクラスです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Xuan Le
 */
public abstract class UmDaoFinder extends FinderSupport implements IUmDaoFinder {

	private CtgNumberingDao ctgNumberingDao;
	private MstoneNumberingDao mstoneNumberingDao;
	private FilterNumberingDao filterNumberingDao;
	private WikiNumberingDao wikiNumberingDao;

	private ILockSvcDao lockSvcDao;
	private IAccsHistDao accsHistDao;
	private IHistDao histDao;
	private ILotHistDao lotHistDao;
	private IUserDao userDao;
	private IGrpDao grpDao;
	private IGrpUserLnkDao grpUserLnkDao;
	private IGrpRoleLnkDao grpRoleLnkDao;
	private ILotMailGrpLnkDao lotMailGrpLnkDao;
	private IUserRoleLnkDao userRoleLnkDao;
	private IRoleSvcCtgLnkDao roleSvcCtgLnkDao;
	private ISvcCtgSvcLnkDao svcCtgSvcLnkDao;
	private IDeptDao deptDao;
	private IRoleDao roleDao;
	private IUserPassHistDao userPassHistDao;
	private IUserProcNoticeDao userProcNoticeDao;
	private ICtgDao ctgDao;
	private ICtgLnkDao ctgLnkDao;
	private ICtgHistDao ctgHistDao;
	private IMstoneDao mstoneDao;
	private IMstoneLnkDao mstoneLnkDao;
	private IMstoneHistDao mstoneHistDao;
	private ISvcCtgDao svcCtgDao;
	private IUserUrlDao userUrlDao;
	private IFilterDao filterDao;
	private IViewUserAccsHistDao viewUserAccsHistDao;
	private IViewNotificationToUserDao viewNotificationToUserDao;
	private IViewWikiTagsDao viewWikiTagsDao;
	private IProjectDao projectDao;
	private IWikiDao wikiDao;
	private IWikiTagDao wikiTagDao;
	private IWikiHistDao wikiHistDao;
	private IWikiAttachedFileDao wikiAttachedFileDao;
	private IAreqHistDao areqHistDao;
	private IWikiUpdateHistDao wikiUpdateHistDao;


	@Override
	public CtgNumberingDao getCtgNumberingDao() {
		return ctgNumberingDao;
	}
	public void setCtgNumberingDao(CtgNumberingDao ctgNumberingDao) {
		this.ctgNumberingDao = ctgNumberingDao;
	}

	@Override
	public MstoneNumberingDao getMstoneNumberingDao() {
		return mstoneNumberingDao;
	}
	public void setMstoneNumberingDao(MstoneNumberingDao mstoneNumberingDao) {
		this.mstoneNumberingDao = mstoneNumberingDao;
	}

	@Override
	public FilterNumberingDao getFilterNumberingDao() {
		return filterNumberingDao;
	}
	public void setFilterNumberingDao(FilterNumberingDao filterNumberingDao) {
		this.filterNumberingDao = filterNumberingDao;
	}

	@Override
	public WikiNumberingDao getWikiNumberingDao() {
		return wikiNumberingDao;
	}
	public void setWikiNumberingDao(WikiNumberingDao wikiNumberingDao) {
		this.wikiNumberingDao = wikiNumberingDao;
	}

	@Override
	public ILockSvcDao getLockSvcDao() {
		return lockSvcDao;
	}
	public void setLockSvcDao(ILockSvcDao lockSvcDao) {
		this.lockSvcDao = lockSvcDao;
	}
	@Override
	public IHistDao getHistDao() {
		return histDao;
	}
	public void setHistDao(IHistDao histDao) {
		this.histDao = histDao;
	}

	@Override
	public ILotHistDao getLotHistDao() {
		return lotHistDao;
	}
	public void setLotHistDao(ILotHistDao lotHistDao){
		this.lotHistDao = lotHistDao;
	}

	@Override
	public IAccsHistDao getAccsHistDao() {
		return accsHistDao;
	}
	public void setAccsHistDao(IAccsHistDao accsHistDao) {
		this.accsHistDao = accsHistDao;
	}
	@Override
	public IUserDao getUserDao() {
		return userDao;
	}
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}
	@Override
	public IGrpDao getGrpDao() {
		return grpDao;
	}
	public void setGrpDao(IGrpDao grpDao) {
		this.grpDao = grpDao;
	}
	@Override
	public IGrpUserLnkDao getGrpUserLnkDao() {
		return grpUserLnkDao;
	}
	public void setGrpUserLnkDao(IGrpUserLnkDao grpUserLnkDao) {
		this.grpUserLnkDao = grpUserLnkDao;
	}
	@Override
	public IGrpRoleLnkDao getGrpRoleLnkDao() {
		return grpRoleLnkDao;
	}
	public void setGrpRoleLnkDao(IGrpRoleLnkDao grpRoleLnkDao) {
		this.grpRoleLnkDao = grpRoleLnkDao;
	}
	@Override
	public ILotMailGrpLnkDao getLotMailGrpLnkDao() {
		return lotMailGrpLnkDao;
	}
	public void setLotMailGrpLnkDao(ILotMailGrpLnkDao lotMailGrpLnkDao) {
		this.lotMailGrpLnkDao = lotMailGrpLnkDao;
	}
	@Override
	public IUserRoleLnkDao getUserRoleLnkDao() {
		return userRoleLnkDao;
	}
	public void setUserRoleLnkDao(IUserRoleLnkDao userRoleLnkDao) {
		this.userRoleLnkDao = userRoleLnkDao;
	}
	@Override
	public IRoleSvcCtgLnkDao getRoleSvcCtgLnkDao() {
		return roleSvcCtgLnkDao;
	}
	public void setRoleSvcCtgLnkDao(IRoleSvcCtgLnkDao roleSvcCtgLnkDao) {
		this.roleSvcCtgLnkDao = roleSvcCtgLnkDao;
	}
	@Override
	public ISvcCtgSvcLnkDao getSvcCtgSvcLnkDao() {
		return svcCtgSvcLnkDao;
	}
	public void setSvcCtgSvcLnkDao(ISvcCtgSvcLnkDao svcCtgSvcLnkDao) {
		this.svcCtgSvcLnkDao = svcCtgSvcLnkDao;
	}
	@Override
	public IDeptDao getDeptDao() {
		return deptDao;
	}
	public void setDeptDao(IDeptDao deptDao) {
		this.deptDao = deptDao;
	}
	@Override
	public IRoleDao getRoleDao() {
		return roleDao;
	}
	public void setRoleDao(IRoleDao roleDao) {
		this.roleDao = roleDao;
	}
	@Override
	public IUserPassHistDao getUserPassHistDao() {
		return userPassHistDao;
	}
	public void setUserPassHistDao(IUserPassHistDao userPassHistDao) {
		this.userPassHistDao = userPassHistDao;
	}
	@Override
	public IUserProcNoticeDao getUserProcNoticeDao() {
		return userProcNoticeDao;
	}
	public void setUserProcNoticeDao(IUserProcNoticeDao userProcNoticeDao) {
		this.userProcNoticeDao = userProcNoticeDao;
	}
	@Override
	public ICtgDao getCtgDao() {
		return ctgDao;
	}
	public void setCtgDao(ICtgDao ctgDao) {
		this.ctgDao = ctgDao;
	}
	@Override
	public ICtgLnkDao getCtgLnkDao() {
		return ctgLnkDao;
	}
	public void setCtgLnkDao(ICtgLnkDao ctgLnkDao) {
		this.ctgLnkDao = ctgLnkDao;
	}
	@Override
	public ICtgHistDao getCtgHistDao() {
		return ctgHistDao;
	}
	public void setCtgHistDao(ICtgHistDao ctgHistDao) {
		this.ctgHistDao = ctgHistDao;
	}
	@Override
	public IMstoneDao getMstoneDao() {
		return mstoneDao;
	}
	public void setMstoneDao(IMstoneDao mstoneDao) {
		this.mstoneDao = mstoneDao;
	}
	@Override
	public IMstoneLnkDao getMstoneLnkDao() {
		return mstoneLnkDao;
	}
	public void setMstoneLnkDao(IMstoneLnkDao mstoneLnkDao) {
		this.mstoneLnkDao = mstoneLnkDao;
	}
	@Override
	public IMstoneHistDao getMstoneHistDao() {
		return mstoneHistDao;
	}
	public void setMstoneHistDao(IMstoneHistDao mstoneHistDao) {
		this.mstoneHistDao = mstoneHistDao;
	}

	@Override
	public ISvcCtgDao getSvcCtgDao() {
		return svcCtgDao;
	}

	public void setSvcCtgDao(ISvcCtgDao svcCtgDao) {
		this.svcCtgDao = svcCtgDao;
	}

	@Override
	public IUserUrlDao getUserUrlDao() {
		return userUrlDao;
	}

	public void setUserUrlDao(IUserUrlDao userUrlDao) {
		this.userUrlDao = userUrlDao;
	}

	@Override
	public IFilterDao getFilterDao() {
		return filterDao;
	}
	public void setFilterDao(IFilterDao filterDao) {
		this.filterDao = filterDao;
	}

	@Override
	public IViewUserAccsHistDao getViewUserAccsHistDao() {
		return viewUserAccsHistDao;
	}
	public void setViewUserAccsHistDao(IViewUserAccsHistDao viewUserAccsHistDao) {
		this.viewUserAccsHistDao = viewUserAccsHistDao;
	}

	@Override
	public IViewNotificationToUserDao getViewNotificationToUserDao(){
		return this.viewNotificationToUserDao;
	}
	public void setViewNotificationToUserDao(IViewNotificationToUserDao viewNotificationToUserDao) {
		this.viewNotificationToUserDao = viewNotificationToUserDao;
	}

	@Override
	public IViewWikiTagsDao getViewWikiTagsDao(){
		return this.viewWikiTagsDao;
	}
	public void setViewWikiTagsDao(IViewWikiTagsDao viewWikiTagsDao) {
		this.viewWikiTagsDao = viewWikiTagsDao;
	}

	@Override
	public IProjectDao getProjectDao() {
		return projectDao;
	}
	public void setProjectDao(IProjectDao projectDao) {
		this.projectDao = projectDao;
	}

	@Override
	public IWikiDao getWikiDao() {
		return wikiDao;
	}
	public void setWikiDao(IWikiDao wikiDao) {
		this.wikiDao = wikiDao;
	}

	@Override
	public IWikiHistDao getWikiHistDao() {
		return wikiHistDao;
	}
	public void setWikiHistDao(IWikiHistDao wikiHistDao) {
		this.wikiHistDao = wikiHistDao;
	}

	@Override
	public IWikiTagDao getWikiTagDao() {
		return wikiTagDao;
	}
	public void setWikiTagDao(IWikiTagDao wikiTagDao) {
		this.wikiTagDao = wikiTagDao;
	}

	@Override
	public IWikiAttachedFileDao getWikiAttachedFileDao() {
		return wikiAttachedFileDao;
	}
	public void setWikiAttachedFileDao(IWikiAttachedFileDao wikiAttachedFileDao) {
		this.wikiAttachedFileDao = wikiAttachedFileDao;
	}
	
	@Override
	public IWikiUpdateHistDao getWikiUpdateHistDao() {
		return wikiUpdateHistDao;
	}
	public void setWikiUpdateHistDao(IWikiUpdateHistDao wikiUpdateHistDao) {
		this.wikiUpdateHistDao = wikiUpdateHistDao;
	}
	@Override
	public IAreqHistDao getAreqHistDao() {
		return areqHistDao;
	}
	public void setAreqHistDao(IAreqHistDao areqHistDao) {
		this.areqHistDao = areqHistDao;
	}
	
	
	
}
