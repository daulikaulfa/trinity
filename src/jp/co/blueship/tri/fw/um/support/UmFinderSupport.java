package jp.co.blueship.tri.fw.um.support;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.ILotMailGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotMailGrpLnkCondition;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.ComparableSet;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.comparator.Comparators;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunctions;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByUserIcons;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistCondition;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.dept.constants.DeptItems;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterCondition;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistCondition;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.ProjectCondition;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgSvcLnkItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgSvcLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.constants.UserItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserProcNoticeEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserProcNoticeCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;

/**
 * ユーザ関連情報を検索するためのサービス機能を提供するクラスです。
 *
 *
 * @author Takayuki Kubo
 * @version V3L10R01
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class UmFinderSupport extends UmDaoFinder implements IUmFinderSupport {

	private static final TriFunction<IGrpUserLnkEntity, String> FROM_GRP_USER_LNK_TO_GROUP_ID = new TriFunction<IGrpUserLnkEntity, String>() {

		@Override
		public String apply(IGrpUserLnkEntity entity) {
			return entity.getGrpId();
		}
	};

	private static final TriFunction<IGrpUserLnkEntity, String> FROM_GRP_USER_LNK_TO_USER_ID = new TriFunction<IGrpUserLnkEntity, String>() {

		@Override
		public String apply(IGrpUserLnkEntity entity) {
			return entity.getUserId();
		}
	};

	private static final TriFunction<IGrpRoleLnkEntity, String> FROM_GRP_ROLE_LNK_TO_ROLE_ID = new TriFunction<IGrpRoleLnkEntity, String>() {

		@Override
		public String apply(IGrpRoleLnkEntity entity) {
			return entity.getRoleId();
		}
	};

	private static final TriFunction<ILotMailGrpLnkEntity, String> FROM_LOT_MAIL_GRP_LNK_TO_GROUP_ID = new TriFunction<ILotMailGrpLnkEntity, String>() {

		@Override
		public String apply(ILotMailGrpLnkEntity entity) {
			return entity.getGrpId();
		}
	};

	private TriFunction<ISvcCtgSvcLnkEntity, String> FROM_SVC_CTG_SVC_LNK_TO_SVC_ID() {
		return new TriFunction<ISvcCtgSvcLnkEntity, String>() {

			@Override
			public String apply(ISvcCtgSvcLnkEntity entity) {
				return entity.getSvcId();
			}
		};
	}


	private List<IUserEntity> sortByUserNm( List<IUserEntity> users ) {
		Collections.sort( users,
			new Comparator<IUserEntity>() {
				public int compare(IUserEntity param1, IUserEntity param2) {
					return param1.getUserNm().compareTo(param2.getUserNm());
				}
		} );

		return users;
	}

	private TriFunction<String, IGrpEntity> toGroupEntity = new TriFunction<String, IGrpEntity>() {

		@Override
		public IGrpEntity apply(String grpId) {
			GrpCondition condition = new GrpCondition();
			condition.setGrpId(grpId);

			return getGrpDao().findByPrimaryKey(condition.getCondition());
		}
	};

	private TriFunction<String, IUserEntity> toUserEntity = new TriFunction<String, IUserEntity>() {

		@Override
		public IUserEntity apply(String userId) {
			UserCondition condition = new UserCondition();
			condition.setUserId(userId);

			return getUserDao().findByPrimaryKey(condition.getCondition());
		}
	};

	private TriFunction<String, IRoleEntity> toRoleEntity = new TriFunction<String, IRoleEntity>() {

		@Override
		public IRoleEntity apply(String roleId) {
			RoleCondition condition = new RoleCondition();
			condition.setRoleId(roleId);

			return getRoleDao().findByPrimaryKey(condition.getCondition());
		}
	};

	@Override
	public List<IGrpEntity> findGroupByUserId(String userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setUserId(userId);
		List<IGrpUserLnkEntity> links = getGrpUserLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return FluentList.from(links).//
				map(FROM_GRP_USER_LNK_TO_GROUP_ID).//
				map(toGroupEntity).//
				asList();
	}

	@Override
	public List<IUserEntity> findUserByGroup(String groupId) {

		PreConditions.assertOf(groupId != null, "GroupId is not specified.");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(groupId);
		List<IGrpUserLnkEntity> links = getGrpUserLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return this.sortByUserNm(
					FluentList.from(links).//
					map(FROM_GRP_USER_LNK_TO_USER_ID).//
					map(toUserEntity).//
					asList() );
	}

	@Override
	public List<IUserEntity> findUserByGroups(String... groupIds) {

		PreConditions.assertOf(groupIds != null, "GroupId is not specified.");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpIds(groupIds);
		List<IGrpUserLnkEntity> links = getGrpUserLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		List<IUserEntity> userEntities = FluentList.from(links).//
				map(FROM_GRP_USER_LNK_TO_USER_ID).//
				map(toUserEntity).//
				asList();

		ComparableSet<IUserEntity> newSet = new ComparableSet<IUserEntity>(Comparators.IS_EQUAL_USER_ID);
		TriCollectionUtils.collect(userEntities, TriFunctions.<IUserEntity> nopFunction(), newSet);

		return this.sortByUserNm( FluentList.from(newSet).asList() );
	}

	@Override
	public List<IUserEntity> findUsersInSameGroupWithCurrentUser(String userId) {
		GrpUserLnkCondition grpUserLnkCondition = new GrpUserLnkCondition();
		grpUserLnkCondition.setUserId( userId );
		List<IGrpUserLnkEntity> grpUserLnkEntities = getGrpUserLnkDao().find( grpUserLnkCondition.getCondition() );

		List<String> groupIds = new ArrayList<String>();
		for (IGrpUserLnkEntity grpUserLnkEntity : grpUserLnkEntities) {
			groupIds.add( grpUserLnkEntity.getGrpId() );
		}

		return findUserByGroups(groupIds.toArray(new String[0]));
	}

	@Override
	public List<IUserEntity> findUserByDept(String deptId) {

		PreConditions.assertOf(deptId != null, "DepartmentId is not specified.");

		UserCondition condition = new UserCondition();
		condition.setDeptId(deptId);
		List<IUserEntity> users = getUserDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(users)) {
			return Collections.emptyList();
		}

		return users;
	}

	@Override
	public IUserEntity findUserByUserId(String userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		UserCondition condition = new UserCondition();
		condition.setUserId(userId);
		IUserEntity entity = getUserDao().findByPrimaryKey(condition.getCondition());

		if (entity == null) {
			throw new TriSystemException(UmMessageId.UM004001F, userId);
		}

		return entity;
	}

	@Override
	public boolean isExistByUserId(String userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		UserCondition condition = new UserCondition();
		condition.setUserId(userId);

		if ( 0 == getUserDao().count(condition.getCondition()) ) {
			return false;
		}

		return true;
	}

	@Override
	public List<IUserEntity> findUserByUserNm(String userNm) {

		PreConditions.assertOf(userNm != null, "UserName is not specified.");

		UserCondition condition = new UserCondition();
		condition.setUserId(userNm);
		List<IUserEntity> links = getUserDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}
		return links;
	}

	@Override
	public List<IUserEntity> findAllUser() {

		UserCondition condition = new UserCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(UserItems.userNm, TriSortOrder.Asc, 1);

		List<IUserEntity> links = getUserDao().find(condition.getCondition(), sort);

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return links;
	}

	@Override
	public List<String> findMailGroupIdsByLotId(String lotId) {

		PreConditions.assertOf(lotId != null, "LotId is not specified.");

		LotMailGrpLnkCondition condition = new LotMailGrpLnkCondition();
		condition.setLotId(lotId);
		List<ILotMailGrpLnkEntity> links = getLotMailGrpLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return FluentList.from(links).//
				map(FROM_LOT_MAIL_GRP_LNK_TO_GROUP_ID).//
				asList();
	}

	@Override
	public IGrpEntity findGroupById(String groupId) {

		PreConditions.assertOf(groupId != null, "GroupId is not specified.");

		GrpCondition condition = new GrpCondition();
		condition.setGrpId(groupId);
		IGrpEntity entity = getGrpDao().findByPrimaryKey(condition.getCondition());

		if (entity == null) {
			throw new TriSystemException(UmMessageId.UM004000F, groupId);
		}

		return entity;
	}

	@Override
	public boolean checkDuplicationByNumberingGroupId(String groupId) {

		PreConditions.assertOf(groupId != null, "GroupId is not specified.");

		GrpCondition condition = new GrpCondition();
		condition.setGrpId(groupId);
		IGrpEntity entity = getGrpDao().findByPrimaryKey(condition.getCondition());

		if (entity == null) {
			return true;
		}

		return false;
	}

	@Override
	public IGrpEntity findGroupByName(String groupName) {

		PreConditions.assertOf(groupName != null, "groupName is not specified.");

		GrpCondition conditon = new GrpCondition();
		conditon.setGrpNm(groupName);
		List<IGrpEntity> entities = getGrpDao().find(conditon.getCondition());

		if (TriCollectionUtils.isEmpty(entities)) {
			throw new TriSystemException(UmMessageId.UM004007F, groupName);
		}

		return entities.get(0);

	}

	@Override
	public List<IGrpEntity> findAllGroup() {

		GrpCondition condition = new GrpCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(GrpItems.sortOdr, TriSortOrder.Asc, 1);

		List<IGrpEntity> entityList = getGrpDao().find(condition.getCondition(), sort);

		if (TriCollectionUtils.isEmpty(entityList)) {
			throw new TriSystemException(UmMessageId.UM004002F);
		}

		return entityList;

	}

	@Override
	public List<IUserRoleLnkEntity> findUserRoleLnkByUserId(String userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);
		List<IUserRoleLnkEntity> entityList = getUserRoleLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IUserRoleLnkEntity> findAllUserRoleLnk() {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		List<IUserRoleLnkEntity> entityList = getUserRoleLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IRoleSvcCtgLnkEntity> findSvcCtgLnkByRoleId(String... roleIds) {

		PreConditions.assertOf(roleIds != null, "RoleArray is not specified ");

		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		if (TriStringUtils.isNotEmpty(roleIds)) {
			if (roleIds.length == 1) {
				condition.setRoleId(roleIds[0]);
			} else {
				condition.setRoleIds(roleIds);
			}
		}
		List<IRoleSvcCtgLnkEntity> entityList = getRoleSvcCtgLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<ISvcCtgSvcLnkEntity> findSvcCtgSvcLnkBySvcCtgId(String... svcCtgIds) {

		PreConditions.assertOf(svcCtgIds != null, "SvcCtgArray is not specified");

		SvcCtgSvcLnkCondition condition = new SvcCtgSvcLnkCondition();
		if (TriStringUtils.isNotEmpty(svcCtgIds)) {
			if (svcCtgIds.length == 1) {
				condition.setSvcCtgId(svcCtgIds[0]);
			} else {
				condition.setSvcCtgIds(svcCtgIds);
			}
		}
		List<ISvcCtgSvcLnkEntity> entityList = getSvcCtgSvcLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IGrpUserLnkEntity> findGrpUserLnkByGrpId(String... grpId) {

		PreConditions.assertOf(grpId != null, "GroupId is not specified");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		if (TriStringUtils.isNotEmpty(grpId)) {
			if (grpId.length == 1) {
				condition.setGrpId(grpId[0]);
			} else {
				condition.setGrpIds(grpId);
			}
		}
		List<IGrpUserLnkEntity> entityList = getGrpUserLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IGrpUserLnkEntity> findGrpUserLnkByUserId(String... userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		if (TriStringUtils.isNotEmpty(userId)) {
			if (userId.length == 1) {
				condition.setUserId(userId[0]);
			} else {
				condition.setUserIds(userId);
			}
		}
		List<IGrpUserLnkEntity> entityList = getGrpUserLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IGrpRoleLnkEntity> findGrpRoleLnkByGrpId(String... grpId) {

		PreConditions.assertOf(grpId != null, "GruopId is not specified");

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		if (TriStringUtils.isNotEmpty(grpId)) {
			if (grpId.length == 1) {
				condition.setGrpId(grpId[0]);
			} else {
				condition.setGrpIds(grpId);
			}
		}
		List<IGrpRoleLnkEntity> entityList = getGrpRoleLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public List<IGrpRoleLnkEntity> findGrpRoleLnkByRoleId(String... roleId) {

		PreConditions.assertOf(roleId != null, "RoleId is not specified");

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		if (TriStringUtils.isNotEmpty(roleId)) {
			if (roleId.length == 1) {
				condition.setRoleId(roleId[0]);
			} else {
				condition.setRoleIds(roleId);
			}
		}
		List<IGrpRoleLnkEntity> entityList = getGrpRoleLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public IDeptEntity findDeptByDeptId(String deptId) {

		PreConditions.assertOf(deptId != null, "DepartmentId is not specified");

		DeptCondition condition = new DeptCondition();
		condition.setDeptId(deptId);
		IDeptEntity entity = getDeptDao().findByPrimaryKey(condition.getCondition());
		if (entity == null) {
			throw new TriSystemException(UmMessageId.UM004003F, deptId);
		}
		return entity;
	}

	@Override
	public boolean checkDuplicationByNumberingDeptId(String deptId) {

		PreConditions.assertOf(deptId != null, "DepartmentId is not specified");

		DeptCondition condition = new DeptCondition();
		condition.setDeptId(deptId);
		IDeptEntity entity = getDeptDao().findByPrimaryKey(condition.getCondition());
		if (entity == null) {
			return true;
		}
		return false;
	}

	@Override
	public List<IDeptEntity> findAllDept() {

		DeptCondition condition = new DeptCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(DeptItems.sortOdr, TriSortOrder.Asc, 1);

		List<IDeptEntity> entity = getDeptDao().find(condition.getCondition(), sort);
		if (entity == null) {
			throw new TriSystemException(UmMessageId.UM004004F);
		}
		return entity;
	}

	@Override
	public List<IRoleEntity> findAllRole() {

		RoleCondition condition = new RoleCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(RoleItems.sortOdr, TriSortOrder.Asc, 1);

		List<IRoleEntity> entityList = getRoleDao().find(condition.getCondition(), sort);

		if (TriCollectionUtils.isEmpty(entityList)) {
			throw new TriSystemException(UmMessageId.UM004005F);
		}

		return entityList;

	}

	@Override
	public IRoleEntity findRoleByRoleId(String roleId) {

		PreConditions.assertOf(roleId != null, "RoleId is not specified.");

		RoleCondition condition = new RoleCondition();
		condition.setRoleId(roleId);
		IRoleEntity entity = getRoleDao().findByPrimaryKey(condition.getCondition());

		if (entity == null) {
			throw new TriSystemException(UmMessageId.UM004006F, roleId);
		}
		return entity;

	}

	@Override
	public boolean checkDuplicationByNumberingRoleId(String roleId) {

		PreConditions.assertOf(roleId != null, "RoleId is not specified.");

		RoleCondition condition = new RoleCondition();
		condition.setRoleId(roleId);
		IRoleEntity entity = getRoleDao().findByPrimaryKey(condition.getCondition());
		if (entity == null) {
			return true;
		}
		return false;
	}

	@Override
	public List<IUserPassHistEntity> findUserPassHistByUserId(String... userId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		UserPassHistCondition condition = new UserPassHistCondition();
		if (TriStringUtils.isNotEmpty(userId)) {
			if (userId.length == 1) {
				condition.setUserId(userId[0]);
			} else {
				condition.setUserIds(userId);
			}
		}
		List<IUserPassHistEntity> entityList = getUserPassHistDao().find(condition.getCondition());
		if (TriCollectionUtils.isEmpty(entityList)) {
			return Collections.emptyList();
		}
		return entityList;
	}

	@Override
	public IAccsHistEntity findAccsHistByPrimaryKey(String userId, String dataCtgCd, String dataId) {

		PreConditions.assertOf(userId != null, "UserId is not specified.");
		PreConditions.assertOf(dataCtgCd != null, "DataCtgCd is not specified.");
		PreConditions.assertOf(dataId != null, "DataId is not specified.");

		AccsHistCondition condition = new AccsHistCondition();
		condition.setUserId(userId);
		condition.setDataCtgCd(dataCtgCd);
		condition.setDataId(dataId);
		IAccsHistEntity entity = this.getAccsHistDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public void updateAccsHist(GenericServiceBean serviceBean, ITableAttribute attr, String dataId) {
		this.updateAccsHist( serviceBean, attr, dataId, AccsHistItems.accsTimestamp );
	}

	@Override
	public void updateAccsHist(GenericServiceBean serviceBean, ITableAttribute attr, String dataId, AccsHistItems... updateItems) {
		PreConditions.assertOf(serviceBean != null, "serviceBean is not specified");
		PreConditions.assertOf(attr != null, "attr is not specified");
		PreConditions.assertOf(TriStringUtils.isNotEmpty(dataId), "dataId is not specified");
		PreConditions.assertOf(updateItems != null && updateItems.length > 0, "updateItems is not specified" );

		IAccsHistEntity updateEntity = new AccsHistEntity();
		updateEntity.setUserId(serviceBean.getUserId());
		updateEntity.setDataCtgCd(attr.name());
		updateEntity.setDataId(dataId);
		updateEntity.setUserNm(serviceBean.getUserName());
		updateEntity.setDelStsId( StatusFlg.off );

		for(AccsHistItems updateItem : updateItems){
			if( AccsHistItems.accsTimestamp.equals( updateItem ) ){
				updateEntity.setAccsTimestamp( TriDateUtils.getSystemTimestamp() );
			}
			if( AccsHistItems.noticeViewTimestamp.equals( updateItem ) ){
				updateEntity.setNoticeViewTimestamp( TriDateUtils.getSystemTimestamp() );
			}
		}

		AccsHistCondition condition = new AccsHistCondition();
		condition.setDataId( dataId );
		condition.setDataCtgCd( attr.name() );
		condition.setUserId( serviceBean.getUserId() );
		condition.setDelStsId( (StatusFlg)null );

		int count = this.getAccsHistDao().count( condition.getCondition() );

		if( count == 0 ){
			this.getAccsHistDao().insert( updateEntity );

		}else{
			this.getAccsHistDao().update( updateEntity );
		}
	}

	@Override
	public List<IUserProcNoticeEntity>  findUserProcNoticeByUserId ( String userId){

		PreConditions.assertOf(userId != null, "UserId is not specified.");

		UserProcNoticeCondition condition = new UserProcNoticeCondition();

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(TriDateUtils.getSystemTimestamp().getTime()));
		cal.add(Calendar.YEAR, -1);
		condition.setFromToRegTimestamp(new Timestamp(cal.getTime().getTime()), (Timestamp)null );

		if (TriStringUtils.isNotEmpty(userId)) {
			condition.setUserId(userId);
		}

		List<IUserProcNoticeEntity> entities = getUserProcNoticeDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(entities))
			return Collections.emptyList();

		return entities;
	}

	@Override
	public IUserProcNoticeEntity findUserProcNoticeByProcId ( String procId){
		PreConditions.assertOf(procId != null, "ProcId is not specified.");

		UserProcNoticeCondition condition = new UserProcNoticeCondition();

		if (TriStringUtils.isNotEmpty(procId))
			condition.setProcId(procId);

		IUserProcNoticeEntity entity = getUserProcNoticeDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public ICtgEntity findCtgByPrimaryKey(String ctgId) {
		PreConditions.assertOf(ctgId != null, "ctgId is not specified.");

		CtgCondition condition = new CtgCondition();
		condition.setCtgId(ctgId);
		ICtgEntity entity = this.getCtgDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public List<ICtgEntity> findCtgByLotId(String lotId) {
		CtgCondition condition = new CtgCondition();
		condition.setLotId(lotId);

		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.sortOdr, TriSortOrder.Asc, 1);

		List<ICtgEntity> entityList = getCtgDao().find(condition.getCondition(), sort);

		return entityList;
	}

	@Override
	public List<ICtgLnkEntity> findCtgLnkByCategoryId(String categoryId) {
		CtgLnkCondition condition = new CtgLnkCondition();
		condition.setCtgId(categoryId);
		List<ICtgLnkEntity> entityList = getCtgLnkDao().find(condition.getCondition());
		return entityList;
	}

	@Override
	public IMstoneEntity findMstoneByPrimaryKey(String mstoneId) {
		PreConditions.assertOf(mstoneId != null, "mstoneId is not specified.");

		MstoneCondition condition = new MstoneCondition();
		condition.setMstoneId(mstoneId);
		IMstoneEntity entity = this.getMstoneDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public List<IMstoneEntity> findMstoneByLotId(String lotId) {
		MstoneCondition condition = new MstoneCondition();
		condition.setLotId(lotId);
		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.sortOdr, TriSortOrder.Asc, 1);
		List<IMstoneEntity> entityList = this.getMstoneDao().find(condition.getCondition(), sort);

		return entityList;
	}

	@Override
	public List<IMstoneLnkEntity> findMstoneLnkByMstoneId(String mstoneId) {
		MstoneLnkCondition condition = new MstoneLnkCondition();
		condition.setMstoneId(mstoneId);
		List<IMstoneLnkEntity> entityList = this.getMstoneLnkDao().find(condition.getCondition());
		return entityList;
	}

	@Override
	public ICtgLnkEntity findCtgLnkByPrimaryKey(String dataId, String dataCtgCd) {
		CtgLnkCondition condition = new CtgLnkCondition();
		condition.setDataId(dataId);
		condition.setDataCtgCd(dataCtgCd);
		ICtgLnkEntity entity = this.getCtgLnkDao().findByPrimaryKey(condition.getCondition());
		return entity;
	}

	@Override
	public IMstoneLnkEntity findMstoneLnkByPrimaryKey(String dataId, String dataCtgCd) {
		MstoneLnkCondition condition = new MstoneLnkCondition();
		condition.setDataId(dataId);
		condition.setDataCtgCd(dataCtgCd);
		IMstoneLnkEntity entity = this.getMstoneLnkDao().findByPrimaryKey(condition.getCondition());
		return entity;
	}

	@Override
	public List<IGrpEntity> findGroupByGroupIds(String... grpIds) {
		GrpCondition condition = new GrpCondition();
		condition.setGrpIds(grpIds);
		List<IGrpEntity> grpEntities = this.getGrpDao().find(condition.getCondition());

		return grpEntities;
	}

	@Override
	public IGrpUserLnkEntity findGrpUserLnkByPrimaryKey(String groupId, String userId) {
		PreConditions.assertOf(groupId != null, "groupId is not specified.");
		PreConditions.assertOf(userId != null, "userId is not specified.");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(groupId);
		condition.setUserId(userId);

		IGrpUserLnkEntity entity = this.getGrpUserLnkDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public IHistEntity findHistByPrimaryKey(String histId){
		PreConditions.assertOf(histId != null, "histId is not specified.");

		HistCondition condition = new HistCondition();
		condition.setHistId(histId);

		IHistEntity entity = this.getHistDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public void registerCtgLnk(String ctgId, ITableAttribute attr, String dataId) {
		PreConditions.assertOf(attr != null, "attr is not specified");
		PreConditions.assertOf(TriStringUtils.isNotEmpty(dataId), "dataId is not specified");

		if ( TriStringUtils.isNotEmpty(ctgId) ) {
			ICtgLnkEntity ctgLnkEntity = new CtgLnkEntity();
			ctgLnkEntity.setCtgId(ctgId);
			ctgLnkEntity.setDataCtgCd(attr.name());
			ctgLnkEntity.setDataId(dataId);

			this.getCtgLnkDao().insert(ctgLnkEntity);
		}
	}

	@Override
	public void registerMstoneLnk(String mstoneId, ITableAttribute attr, String dataId) {
		PreConditions.assertOf(attr != null, "attr is not specified");
		PreConditions.assertOf(TriStringUtils.isNotEmpty(dataId), "dataId is not specified");

		if ( TriStringUtils.isNotEmpty(mstoneId) ) {
			IMstoneLnkEntity mstoneLnkEntity = new MstoneLnkEntity();
			mstoneLnkEntity.setMstoneId(mstoneId);
			mstoneLnkEntity.setDataCtgCd(attr.name());
			mstoneLnkEntity.setDataId(dataId);

			this.getMstoneLnkDao().insert(mstoneLnkEntity);
		}
	}

	@Override
	public void updateCtgLnk(String ctgId, ITableAttribute attr, String dataId) {
		PreConditions.assertOf(attr != null, "attr is not specified");
		PreConditions.assertOf(TriStringUtils.isNotEmpty(dataId), "dataId is not specified");

		ICtgLnkEntity ctgLnkEntity = this.findCtgLnkByPrimaryKey(dataId, attr.name());

		if (null == ctgLnkEntity) {
			if (TriStringUtils.isNotEmpty(ctgId)) {
				ctgLnkEntity = new CtgLnkEntity();
				ctgLnkEntity.setDataCtgCd(attr.name());
				ctgLnkEntity.setDataId(dataId);
				ctgLnkEntity.setCtgId(ctgId);
				this.getCtgLnkDao().insert(ctgLnkEntity);
			}
		} else {
			if (TriStringUtils.isNotEmpty(ctgId)) {
				ctgLnkEntity = new CtgLnkEntity();
				ctgLnkEntity.setDataCtgCd(attr.name());
				ctgLnkEntity.setDataId(dataId);
				ctgLnkEntity.setCtgId(ctgId);
				this.getCtgLnkDao().update(ctgLnkEntity);
			} else {
				CtgLnkCondition condition = new CtgLnkCondition();
				condition.setCtgId( ctgLnkEntity.getCtgId() );
				condition.setDataCtgCd( ctgLnkEntity.getDataCtgCd() );
				this.getCtgLnkDao().delete( condition.getCondition() );

			}
		}
	}

	@Override
	public void updateMstoneLnk(String mstoneId, ITableAttribute attr, String dataId) {
		PreConditions.assertOf(attr != null, "attr is not specified");
		PreConditions.assertOf(TriStringUtils.isNotEmpty(dataId), "dataId is not specified");

		IMstoneLnkEntity mstoneLnkEntity = this.findMstoneLnkByPrimaryKey(dataId, attr.name());

		if (null == mstoneLnkEntity) {
			if (TriStringUtils.isNotEmpty(mstoneId)) {
				mstoneLnkEntity = new MstoneLnkEntity();
				mstoneLnkEntity.setDataId(dataId);
				mstoneLnkEntity.setDataCtgCd(attr.name());
				mstoneLnkEntity.setMstoneId(mstoneId);
				this.getMstoneLnkDao().insert(mstoneLnkEntity);
			}
		} else {
			if (TriStringUtils.isNotEmpty(mstoneId)) {
				mstoneLnkEntity = new MstoneLnkEntity();
				mstoneLnkEntity.setDataId(dataId);
				mstoneLnkEntity.setDataCtgCd(attr.name());
				mstoneLnkEntity.setMstoneId(mstoneId);
				this.getMstoneLnkDao().update(mstoneLnkEntity);
			} else {
				MstoneLnkCondition condition = new MstoneLnkCondition();
				condition.setDataId( mstoneLnkEntity.getDataId() );
				condition.setDataCtgCd( mstoneLnkEntity.getDataCtgCd() );
				this.getMstoneDao().delete( condition.getCondition() );
			}
		}
	}

	@Override
	public void cleaningCtgLnk( ITableAttribute attr, String dataId ) {
		CtgLnkCondition condition = new CtgLnkCondition();
		condition.setDataCtgCd( attr.name() );
		condition.setDataId( dataId );

		ICtgLnkEntity ctgLnkEntity = new CtgLnkEntity();
		ctgLnkEntity.setDataCtgCd(attr.name());
		ctgLnkEntity.setDataId(dataId);
		ctgLnkEntity.setDelStsId(StatusFlg.on);

		this.getCtgLnkDao().update( condition.getCondition(), ctgLnkEntity );
	}

	@Override
	public void cleaningMstoneLnk( ITableAttribute attr, String dataId ) {
		MstoneLnkCondition condition = new MstoneLnkCondition();
		condition.setDataCtgCd( attr.name() );
		condition.setDataId( dataId );

		IMstoneLnkEntity mstoneLnkEntity = new MstoneLnkEntity();
		mstoneLnkEntity.setDataCtgCd(attr.name());
		mstoneLnkEntity.setDataId(dataId);
		mstoneLnkEntity.setDelStsId(StatusFlg.on);

		this.getMstoneLnkDao().update( condition.getCondition(), mstoneLnkEntity );
	}

	@Override
	public List<IRoleEntity> findRoleByGroup(String groupId) {

		PreConditions.assertOf(groupId != null, "GroupId is not specified.");

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		condition.setGrpId(groupId);
		List<IGrpRoleLnkEntity> links = getGrpRoleLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return FluentList.from(links).//
					map(FROM_GRP_ROLE_LNK_TO_ROLE_ID).//
					map(toRoleEntity).//
					asList();
	}

	public List<String> getUserIdsByGroup(String groupId) {
		PreConditions.assertOf(groupId != null, "GroupId is not specified.");

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(groupId);
		List<IGrpUserLnkEntity> links = getGrpUserLnkDao().find(condition.getCondition());

		if (TriCollectionUtils.isEmpty(links)) {
			return Collections.emptyList();
		}

		return FluentList.from(links).
				map(FROM_GRP_USER_LNK_TO_USER_ID).asList();
	}

	public void cleaningGrpRoleLnk(String grpId, String roleId) {
		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();

		if (TriStringUtils.isNotEmpty(grpId)) {
			condition.setGrpId(grpId);
		}
		if (TriStringUtils.isNotEmpty(roleId)) {
			condition.setRoleId(roleId);
		}

		getGrpRoleLnkDao().delete(condition.getCondition());
	}

	public void cleaningGrpUserLnk(String grpId, String userId) {
		GrpUserLnkCondition grpUserCondition = new GrpUserLnkCondition();
		if (TriStringUtils.isNotEmpty(userId)) {
			grpUserCondition.setUserId(userId);
		}
		if (TriStringUtils.isNotEmpty(grpId)) {
			grpUserCondition.setGrpId(grpId);
		}

		getGrpUserLnkDao().delete(grpUserCondition.getCondition());
	}

	public void cleaningUserRoleLnk(String userId, String roleId) {
		if (TriStringUtils.isEmpty(userId)
				&& TriStringUtils.isEmpty(roleId)) {
			return;
		}

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		if (TriStringUtils.isNotEmpty(userId)) {
			condition.setUserId(userId);
		}
		if (TriStringUtils.isNotEmpty(roleId)) {
			condition.setRoleId(roleId);
		}

		getUserRoleLnkDao().delete(condition.getCondition());
	}

	public void cleaningAndUpdateUserRoleLnk(String[] userIds) {
		if (TriStringUtils.isNotEmpty(userIds)) {
			for (String userId : userIds) {

				cleaningUserRoleLnk(userId, null);

				List<IGrpUserLnkEntity> grpUserLnkEntities = this.findGrpUserLnkByUserId(userId);

				if (TriCollectionUtils.isNotEmpty(grpUserLnkEntities)) {
					List<String> groupList = FluentList.from(grpUserLnkEntities).map(UmFluentFunctionUtils.toGrpIdFromGrpUserLnkEntity).asList();

					for (String groupId : groupList) {
						List<IGrpRoleLnkEntity> grpRoleLnkEntities = this.findGrpRoleLnkByGrpId(groupId);

						List<String> roleList = FluentList.from(grpRoleLnkEntities).map(UmFluentFunctionUtils.toRoleFromGrpRoleLnkEntity).asList();

						for (String roleId : roleList) {
							cleaningUserRoleLnk(userId, roleId);
							registerUserRoleLnk(new String[]{userId}, new String[]{roleId});
						}
					}
				}
			}
		}
	}

	public void registerGrpRoleLnk(String[] grpList, String[] roleList) {
		if (null == grpList
				|| null == roleList) {
			return;
		}
		for (String grpId : grpList) {
			for (String roleId : roleList) {
				IGrpRoleLnkEntity entity = new GrpRoleLnkEntity();
				entity.setGrpId(grpId);
				entity.setRoleId(roleId);

				getGrpRoleLnkDao().insert(entity);
			}
		}
	}

	public void registerGrpUserLnk(String[] grpIds, String[] userIds) {
		if (null == grpIds
				|| null == userIds) {
			return;
		}
		for (String grpId : grpIds) {
			for (String userId : userIds) {
				IGrpUserLnkEntity entity = new GrpUserLnkEntity();
				entity.setGrpId(grpId);
				entity.setUserId(userId);

				getGrpUserLnkDao().insert(entity);
			}
		}
	}

	public void registerUserRoleLnk(String[] userIds, String[] roleIds) {
		if (null == userIds
				|| null == roleIds) {
			return;
		}
		for (String userId : userIds) {
			for (String roleId : roleIds) {
				IUserRoleLnkEntity entity = new UserRoleLnkEntity();
				entity.setRoleId(roleId);
				entity.setUserId(userId);

				getUserRoleLnkDao().insert(entity);
			}
		}
	}

	@Override
	public List<ISvcCtgEntity> findAllSvcCtg() {
		SvcCtgCondition condition = new SvcCtgCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(SvcCtgItems.svcCtgPath, TriSortOrder.Asc, 1);

		List<ISvcCtgEntity> entityList = getSvcCtgDao().find(condition.getCondition(), sort);

		return entityList;
	}

	@Override
	public List<ISvcCtgSvcLnkEntity> findSvcCtgSvcLnkBySvcIds(String... svcIds) {
		SvcCtgSvcLnkCondition condition = new SvcCtgSvcLnkCondition();
		condition.setSvcIds(svcIds);

		ISqlSort sort = new SortBuilder();
		sort.setElement(SvcCtgSvcLnkItems.svcId, TriSortOrder.Asc, 1);

		List<ISvcCtgSvcLnkEntity> entityList = getSvcCtgSvcLnkDao().find(condition.getCondition(), sort);

		return entityList;
	}

	public List<ICtgEntity> findCtgByLotIds(String... lotIds) {
		CtgCondition condition = new CtgCondition();
		condition.setLotIds(lotIds);

		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.sortOdr, TriSortOrder.Asc, 1);

		List<ICtgEntity> entityList = getCtgDao().find(condition.getCondition(), sort);

		return entityList;
	}

	public List<IMstoneEntity> findMstoneByLotIds(String... lotIds) {
		MstoneCondition condition = new MstoneCondition();
		condition.setLotIds(lotIds);

		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.sortOdr, TriSortOrder.Asc, 1);
		List<IMstoneEntity> entityList = this.getMstoneDao().find(condition.getCondition(), sort);

		return entityList;
	}

	@Override
	public IProjectEntity findProjectByPrimaryKey(String productId){

		ProjectCondition condition = new ProjectCondition();
		condition.setProductId(productId);
		IProjectEntity entity = getProjectDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public IWikiEntity findWikiByPrimaryKey(String wikiId){

		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);
		IWikiEntity entity = getWikiDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public IFilterEntity findFilterByPrimaryKey(String filterId){

		FilterCondition condition = new FilterCondition();
		condition.setFilterId(filterId);
		IFilterEntity entity = this.getFilterDao().findByPrimaryKey(condition.getCondition());

		return entity;
	}

	@Override
	public String getIconPath(String userId) {
		String iconPath = "";
		if (TriStringUtils.isNotEmpty(userId)) {
			if (!this.isExistByUserId(userId)) {
				return UmDesignBusinessRuleUtils.getDeletedUserIconParentSharePath();
			}
			IUserEntity userEntity = this.findUserByUserId(userId);
			iconPath = UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity);
		}

		return iconPath;
	}

	@Override
	public LookAndFeel getLookAndFeel( String userId ){

		IUserEntity userEntity = this.findUserByUserId( userId );

		LookAndFeel lookAndFeel = LookAndFeel.dbValue( userEntity.getLookAndFeel() );

		if (lookAndFeel == LookAndFeel.none){
			return getAdminLookAndFeel();
		}

		return lookAndFeel;
	}

	@Override
	public LookAndFeel getAdminLookAndFeel() {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.findProjectByPrimaryKey(productId);

		LookAndFeel lookAndFeel = LookAndFeel.dbValue(projectEntity.getLookAndFeel());

		if (lookAndFeel == LookAndFeel.none) {
			lookAndFeel = LookAndFeel.TrinityClassic;
		}

		return lookAndFeel;
	}

	public List<String> getSvcCtgIdByRoleId(String roleId) {

		List<IRoleSvcCtgLnkEntity> roleSvcCtgLnkEntityList = this.findSvcCtgLnkByRoleId( roleId );
		List<String> roleActionList = new ArrayList<String>();
		for ( IRoleSvcCtgLnkEntity entity : roleSvcCtgLnkEntityList ) {
			List<ISvcCtgSvcLnkEntity> svcCtgSvcLnkEntity = this.findSvcCtgSvcLnkBySvcCtgId( entity.getSvcCtgId() ) ;
			roleActionList.addAll(TriCollectionUtils.collect(svcCtgSvcLnkEntity, FROM_SVC_CTG_SVC_LNK_TO_SVC_ID()));
		}
		return roleActionList;
	}
}
