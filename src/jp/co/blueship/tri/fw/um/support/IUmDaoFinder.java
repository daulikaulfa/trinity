package jp.co.blueship.tri.fw.um.support;

import jp.co.blueship.tri.am.dao.lot.ILotMailGrpLnkDao;
import jp.co.blueship.tri.fw.um.dao.accshist.IAccsHistDao;
import jp.co.blueship.tri.fw.um.dao.ctg.CtgNumberingDao;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgDao;
import jp.co.blueship.tri.fw.um.dao.ctg.ICtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.filter.FilterNumberingDao;
import jp.co.blueship.tri.fw.um.dao.filter.IFilterDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.hist.IAreqHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.ICtgHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.ILotHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IMstoneHistDao;
import jp.co.blueship.tri.fw.um.dao.hist.IViewNotificationToUserDao;
import jp.co.blueship.tri.fw.um.dao.locksvc.ILockSvcDao;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneDao;
import jp.co.blueship.tri.fw.um.dao.mstone.IMstoneLnkDao;
import jp.co.blueship.tri.fw.um.dao.mstone.MstoneNumberingDao;
import jp.co.blueship.tri.fw.um.dao.project.IProjectDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgDao;
import jp.co.blueship.tri.fw.um.dao.svcctg.ISvcCtgSvcLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserPassHistDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserProcNoticeDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserUrlDao;
import jp.co.blueship.tri.fw.um.dao.user.IViewUserAccsHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IViewWikiTagsDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiAttachedFileDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiTagDao;
import jp.co.blueship.tri.fw.um.dao.wiki.IWikiUpdateHistDao;
import jp.co.blueship.tri.fw.um.dao.wiki.WikiNumberingDao;



/**
 * UM管理のDAOのGetterを提供するインタフェースです。
 * <br>ここでは、DAOのGetter Method以外の機能は提供されません。
 *
 * @version V3L10.01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public interface IUmDaoFinder {
	public CtgNumberingDao getCtgNumberingDao();
	public MstoneNumberingDao getMstoneNumberingDao();
	public FilterNumberingDao getFilterNumberingDao();
	public WikiNumberingDao getWikiNumberingDao();

	public IAccsHistDao getAccsHistDao();
	public IUserDao getUserDao();
	public IGrpUserLnkDao getGrpUserLnkDao();
	public IGrpRoleLnkDao getGrpRoleLnkDao();
	public ILotMailGrpLnkDao getLotMailGrpLnkDao();
	public IUserRoleLnkDao getUserRoleLnkDao();
	public IRoleSvcCtgLnkDao getRoleSvcCtgLnkDao();
	public ISvcCtgSvcLnkDao getSvcCtgSvcLnkDao();
	public IRoleDao getRoleDao();
	public IUserPassHistDao getUserPassHistDao();
	public IUserProcNoticeDao getUserProcNoticeDao();
	public ICtgDao getCtgDao();
	public ICtgLnkDao getCtgLnkDao();
	public ICtgHistDao getCtgHistDao();
	public IMstoneDao getMstoneDao();
	public IMstoneLnkDao getMstoneLnkDao();
	public IMstoneHistDao getMstoneHistDao();
	public IFilterDao getFilterDao();
	public IViewUserAccsHistDao getViewUserAccsHistDao();
	public IViewNotificationToUserDao getViewNotificationToUserDao();
	public IViewWikiTagsDao getViewWikiTagsDao();
	public IProjectDao getProjectDao();
	public IWikiDao getWikiDao();
	public IWikiTagDao getWikiTagDao();
	public IWikiHistDao getWikiHistDao();
	public IWikiAttachedFileDao getWikiAttachedFileDao();
	public IAreqHistDao getAreqHistDao();
	public IWikiUpdateHistDao getWikiUpdateHistDao();

	/**
	 * 同時実行を禁止するサービスDAOを取得します。
	 * @return 同時実行を禁止するサービスDAO
	 */
	public ILockSvcDao getLockSvcDao();

	/**
	 * 履歴DAOを取得します。
	 * @return 履歴DAO
	 */
	public IHistDao getHistDao();

	/**
	 * Get Lot History DAO
	 * @return Lot Listory DAO
	 */
	public ILotHistDao getLotHistDao();

	/**
	 * グループDAOを取得します。
	 * @return グループDAO
	 */
	public IGrpDao getGrpDao();

	/**
	 * 部署DAOを取得します。
	 * @return 部署DAO
	 */
	public IDeptDao getDeptDao();

	/**
	 *
	 * @return system define DAO
	 */
	public ISvcCtgDao getSvcCtgDao();

	public IUserUrlDao getUserUrlDao();

}
