package jp.co.blueship.tri.fw.um.constants;

/**
 * This is the class of enumeration types for 'submitChanges request'.
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public enum ExportToFileSubmitOption {
	ExportToCsv("csv"),
	ExportToExcel("xls"),
	;

	private String extention = null;

	private ExportToFileSubmitOption( String extention) {
		this.extention = extention;
	}

	public String extention() {
		return this.extention;
	}

}
