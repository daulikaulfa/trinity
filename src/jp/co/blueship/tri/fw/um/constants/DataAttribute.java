package jp.co.blueship.tri.fw.um.constants;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum DataAttribute {
	none			( "", null ),
	Lot				( "Lot", AmTables.AM_LOT ),
	ChangeProperty	( "ChangeProperty", AmTables.AM_PJT ),
	CheckOutRequest	( "CheckOutRequest", AmTables.AM_AREQ ),
	CheckInRequest	( "CheckInRequest", AmTables.AM_AREQ ),
	RemovalRequest	( "RemovalRequest", AmTables.AM_AREQ ),
	BuildPackage	( "BuildPackage", BmTables.BM_BP ),
	ReleaseRequest	( "ReleaseRequest", RmTables.RM_RA ),
	ReleasePackage	( "ReleasePackage", RmTables.RM_RP ),
	DeploymentJob	( "DeploymentJob", DmTables.DM_DO ),
	Report			( "Report" , DcmTables.DCM_REP ),
	Category		( "Category", UmTables.UM_CTG ),
	Milestone		( "Milestone", UmTables.UM_MSTONE ),
	Wiki			( "Wiki", UmTables.UM_WIKI),
	;

	private String value = null;
	private ITableAttribute attr = null;

	private DataAttribute( String value, ITableAttribute attr) {
		this.value = value;
		this.attr = attr;
	}

	public boolean equals( String value ) {
		DataAttribute type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public ITableAttribute getTableAttribute() {
		return this.attr;
	}

	public static DataAttribute value( String name ) {
		if ( TriStringUtils.isEmpty( name ) ) {
			return null;
		}

		for ( DataAttribute type : values() ) {
			if ( type.value().equals( name ) ) {
				return type;
			}
		}

		return null;
	}
}
