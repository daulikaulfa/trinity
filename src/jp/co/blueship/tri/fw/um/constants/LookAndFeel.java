package jp.co.blueship.tri.fw.um.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @version V4.02.00
 * @author Chung
 *
 */
public enum LookAndFeel {
	none( "", "" ),
	TrinityClassic( "TrinityClassic", "TrinityClassic" ),
	Trinity( "Trinity", "Trinity" ),
	;

	private String value = null;
	private String dbValue = null;

	private LookAndFeel( String value, String dbValue) {
		this.value = value;
		this.dbValue = dbValue;
	}

	public String value() {
		return this.value;
	}
	public String dbValue() {
		return this.dbValue;
	}

	public static LookAndFeel value( String value ) {
		if ( TriStringUtils.isEmpty( value ) ) {
			return LookAndFeel.none;
		}

		for ( LookAndFeel type : values() ) {
			if ( type.dbValue().equals( value ) ) {
				return type;
			}
		}

		return LookAndFeel.none;
	}

	public static LookAndFeel dbValue( String dbValue ) {
		if ( TriStringUtils.isEmpty( dbValue ) ) {
			return LookAndFeel.none;
		}

		for ( LookAndFeel type : values() ) {
			if ( type.dbValue().equals( dbValue ) ) {
				return type;
			}
		}

		return LookAndFeel.none;
	}

	public boolean isTrinityClassic() {
		return LookAndFeel.TrinityClassic.equals(this);
	}
}
