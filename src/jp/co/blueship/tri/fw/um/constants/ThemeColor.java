package jp.co.blueship.tri.fw.um.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum ThemeColor {
	none	( "", "" ),
	Red		( "Red", "skin-red" ),
	Orange	( "Orange", "skin-orange" ),
	Purple	( "Purple", "skin-purple" ),
	Pink	( "Pink", "skin-pink" ),
	DarkBlue( "DarkBlue", "skin-darkblue" ),
	Dark	( "Dark", "skin-dark" ),
	Green	( "Green", "skin-green" ),
	;

	private String id = null;
	private String value = null;

	private ThemeColor( String id, String value) {
		this.id = id;
		this.value = value;
	}

	public boolean equals( String value ) {
		ThemeColor type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String id() {
		return this.id;
	}

	public String value() {
		return this.value;
	}

	public static ThemeColor id( String id ) {
		if ( TriStringUtils.isEmpty( id ) ) {
			return none;
		}

		for ( ThemeColor type : values() ) {
			if ( type.id().equals( id ) ) {
				return type;
			}
		}

		return none;
	}

	public static ThemeColor value( String value ) {
		if ( TriStringUtils.isEmpty( value ) ) {
			return none;
		}

		for ( ThemeColor type : values() ) {
			if ( type.value().equals( value ) ) {
				return type;
			}
		}

		return none;
	}

}
