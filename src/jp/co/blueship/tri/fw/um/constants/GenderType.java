package jp.co.blueship.tri.fw.um.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum GenderType {
	none( "" ),
	Male( "MALE" ),
	Female( "FEMALE" ),
	;

	private String value = null;

	private GenderType( String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		GenderType type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public static GenderType value( String value ) {
		if ( TriStringUtils.isEmpty( value ) ) {
			return null;
		}

		for ( GenderType type : values() ) {
			if ( type.value().equals( value ) ) {
				return type;
			}
		}

		return none;
	}
}
