package jp.co.blueship.tri.fw.um.constants;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public enum IconSelectionOption {
	DefaultImage( "DEFAULT" ),
	CustomImage( "CUSTOM" ),
	;

	private String value = null;

	private IconSelectionOption( String value) {
		this.value = value;
	}

	public boolean equals( String value ) {
		IconSelectionOption type = value( value );

		if ( null == type ) return false;
		if ( ! this.equals(type) ) return false;

		return true;
	}

	public String value() {
		return this.value;
	}

	public static IconSelectionOption value( String value ) {
		if ( TriStringUtils.isEmpty( value ) ) {
			return null;
		}

		for ( IconSelectionOption type : values() ) {
			if ( type.value().equals( value ) ) {
				return type;
			}
		}

		return DefaultImage;
	}

}
