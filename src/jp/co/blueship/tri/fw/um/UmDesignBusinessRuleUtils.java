package jp.co.blueship.tri.fw.um;

import java.io.File;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByLotIcons;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByUserIcons;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByWebResources;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByWikiAttachment;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;

/**
 * It is a utility for business rules for use in back end layer.
 * <br>
 * <br>業務を扱うユーティリティークラスです。
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class UmDesignBusinessRuleUtils {
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	
	/**
	 * Return the user's icon type. Return default icon type if user icon type is empty.
	 *
	 * @param user
	 * @return
	 */
	public static String getUserIconType(IUserEntity user) {
		if (TriStringUtils.isEmpty(user.getIconTyp())) {
			return IconSelectionOption.DefaultImage.value();
		}
		return user.getIconTyp();
	}

	/**
	 * Returns the share path of the user's icon.
	 * <br>
	 * <br>ユーザアイコンのパス（共有）を取得します。
	 *
	 * @param user
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserIconSharePath( IUserEntity user ) throws TriSystemException  {
		return getUserIconSharePath( user.getUserId(), user.getIconTyp(), user.getDefaultIconPath(), user.getCustomIconPath() );
	}

	/**
	 * Returns the share path of the user's icon.
	 * <br>
	 * <br>ユーザアイコンのパス（共有）を取得します。
	 *
	 * @param userId
	 * @param iconType
	 * @param defaultIconPath
	 * @param customIconPath
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserIconSharePath(
			String userId,
			String iconType,
			String defaultIconPath,
			String customIconPath ) throws TriSystemException  {

		String type = iconType;

		if ( TriStringUtils.isEmpty( userId ) ) {
			return null;
		}

		if ( TriStringUtils.isEmpty( type ) ) {
			type = IconSelectionOption.DefaultImage.value();
		}

		if ( IconSelectionOption.DefaultImage.equals( type ) ) {
			defaultIconPath = (TriStringUtils.isEmpty(defaultIconPath))?
					getUserIconSharePath( userId, UmDesignEntryKeyByUserIcons.Default.getKey(), IconSelectionOption.DefaultImage ):
					defaultIconPath;

			return TriStringUtils.convertPath(defaultIconPath, true );

		} else if ( IconSelectionOption.CustomImage.equals( type ) ) {
			return TriStringUtils.convertPath( customIconPath, true );
		}

		return null;
	}

	/**
	 * Returns the share path of the user's icon.
	 * <br>
	 * <br>ユーザアイコンのパス（共有）を取得します。
	 *
	 * @param userId
	 * @param fileNm
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserIconSharePath(
			String userId,
			String fileNm,
			IconSelectionOption iconType ) throws TriSystemException  {

		String path = getUserIconParentSharePath( userId, iconType );

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}

	/**
	 * Returns the parent share path of the user's icon.
	 * <br>
	 * <br>ユーザアイコンの親パス（共有）を取得します。
	 *
	 * @param userId
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserIconParentSharePath(
			String userId,
			IconSelectionOption iconType ) throws TriSystemException  {

		if ( TriStringUtils.isEmpty( userId ) ) {
			return null;
		}

		String type = iconType.value();

		if ( TriStringUtils.isEmpty( type ) ) {
			type = IconSelectionOption.DefaultImage.value();
		}

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.images);

		if ( IconSelectionOption.DefaultImage.equals( type ) ) {
			String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.defaultParentMapping);
			String path = TriStringUtils.linkPath( mapping, images );

			return TriStringUtils.convertPath(path, true );

		} else if ( IconSelectionOption.CustomImage.equals( type ) ) {
			String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.customParentMapping);
			String path = TriStringUtils.linkPath( mapping, "/users/" + userId );
			path = TriStringUtils.linkPath( path, images );

			return TriStringUtils.convertPath( path, true );
		}

		return null;
	}
	
	public static String getDeletedUserIconParentSharePath(){
		String images = sheet.getValue(UmDesignEntryKeyByWebResources.images);
		String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.defaultParentMapping);
		String path = TriStringUtils.linkPath( mapping, images );
		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}
		path = TriStringUtils.linkPath( path, UmDesignEntryKeyByUserIcons.Empy.getKey() );
		
		return TriStringUtils.convertPath(path, true );
	}

	/**
	 * Returns the share path of the user's custom icon.
	 * <br>
	 * <br>ユーザアイコンのパス（共有）を取得します。
	 *
	 * @param userId
	 * @param fileNm
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserCustomIconSharePath( String userId, String fileNm) throws TriSystemException  {
		return getUserIconSharePath( userId, fileNm, IconSelectionOption.CustomImage );
	}

	/**
	 * Returns the local path of the user's custom icon.
	 * <br>
	 * <br>ユーザアイコンのパスを取得します。
	 *
	 * @param userId
	 * @param fileNm
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserCustomIconLocalPath(
			String userId,
			String fileNm ) throws TriSystemException  {

		String path = getUserCustomIconParentLocalPath( userId );

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}

	/**
	 * Returns the parent path of the user's custom icon.
	 * <br>
	 * <br>ユーザアイコンの親パスを取得します。
	 *
	 * @param userId
	 * @return
	 * @throws TriSystemException
	 */
	public static String getUserCustomIconParentLocalPath( String userId ) throws TriSystemException  {

		if ( TriStringUtils.isEmpty( userId ) ) {
			return null;
		}

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.images);

		String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.customParentLocation);
		String path = TriStringUtils.linkPath( mapping, "/users/" + userId );
		path = TriStringUtils.linkPath( path, images );

		return TriStringUtils.convertPath( path );
	}

	/**
	 * Returns the share path of the project icon.
	 * <br>
	 * <br>プロジェクトアイコンのパス（共有）を取得します。
	 *
	 * @param fileNm
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getProjectIconSharePath(
			String fileNm,
			IconSelectionOption iconType ) throws TriSystemException  {
		String path = getProjectIconParentSharePath( iconType );

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}

	/**
	 * Returns the parent share path of the project icon.
	 * <br>
	 * <br>プロジェクトアイコンの親パス（共有）を取得します。
	 *
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getProjectIconParentSharePath( IconSelectionOption iconType ) throws TriSystemException  {

		String type = iconType.value();

		if ( TriStringUtils.isEmpty( type ) ) {
			type = IconSelectionOption.DefaultImage.value();
		}

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.projectImages);

		if ( IconSelectionOption.DefaultImage.equals( type ) ) {
			String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.defaultParentMapping);
			String path = TriStringUtils.linkPath( mapping, images );

			return TriStringUtils.convertPath(path, true );

		} else if ( IconSelectionOption.CustomImage.equals( type ) ) {
			String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.customParentMapping);
			String path = TriStringUtils.linkPath( mapping,  "/project/"  );
			path = TriStringUtils.linkPath( path, images );

			return TriStringUtils.convertPath( path, true );
		}

		return null;
	}
	
	/**
	 * Returns the local path of the project's custom icon.
	 * <br>
	 * <br>プロジェクトアイコンのパスを取得します。
	 *
	 * @param lotId
	 * @param fileNm
	 * @return
	 * @throws TriSystemException
	 */
	public static String getProjectCustomIconLocalPath(
			String fileNm ) throws TriSystemException  {

		String path = getProjectCustomIconParentLocalPath();

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}
	
	/**
	 * Returns the parent path of the lot's custom icon.
	 * <br>
	 * <br>ロットアイコンの親パスを取得します。
	 *
	 * @param lotId
	 * @return
	 * @throws TriSystemException
	 */
	public static String getProjectCustomIconParentLocalPath() throws TriSystemException  {

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.projectImages);

		String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.customParentLocation);
		String path = TriStringUtils.linkPath( mapping, "/project/" );
		path = TriStringUtils.linkPath( path, images );

		return TriStringUtils.convertPath( path );
	}

	
	/**
	 * Returns the share path of the lot icon.
	 * <br>
	 * <br>ロットアイコンのパス（共有）を取得します。
	 *
	 * @param lot
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotIconSharePath( ILotEntity lotEntity ) throws TriSystemException  {
		return getLotIconSharePath( lotEntity.getLotId(), lotEntity.getIconTyp(), lotEntity.getDefaultIconPath(), lotEntity.getCustomIconPath() );
	}
	
	/**
	 * Returns the share path of the lot icon.
	 * <br>
	 * <br>ロットアイコンのパス（共有）を取得します。
	 *
	 * @param lotId
	 * @param fileNm
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotIconSharePath(
			String lotId,
			String fileNm,
			IconSelectionOption iconType ) throws TriSystemException  {
		String path = getLotIconParentSharePath( lotId, iconType );

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}
	
	/**
	 * Returns the share path of the lot icon.
	 * <br>
	 * <br>ロットのパス（共有）を取得します。
	 *
	 * @param lotId
	 * @param iconType
	 * @param defaultIconPath
	 * @param customIconPath
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotIconSharePath(
			String lotId,
			String iconType,
			String defaultIconPath,
			String customIconPath ) throws TriSystemException  {

		String type = iconType;

		if ( TriStringUtils.isEmpty( lotId ) ) {
			return null;
		}

		if ( TriStringUtils.isEmpty( type ) ) {
			type = IconSelectionOption.DefaultImage.value();
		}

		if ( IconSelectionOption.DefaultImage.equals( type ) ) {
			defaultIconPath = (TriStringUtils.isEmpty(defaultIconPath))?
					getLotIconSharePath( lotId, UmDesignEntryKeyByLotIcons.Default.getKey(), IconSelectionOption.DefaultImage ):
					defaultIconPath;

			return TriStringUtils.convertPath(defaultIconPath, true );

		} else if ( IconSelectionOption.CustomImage.equals( type ) ) {
			return TriStringUtils.convertPath( customIconPath, true );
		}

		return null;
	}	
	
	

	/**
	 * Returns the parent share path of the lot icon.
	 * <br>
	 * <br>ロットアイコンの親パス（共有）を取得します。
	 *
	 * @param lotId
	 * @param iconType
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotIconParentSharePath(
			String lotId,
			IconSelectionOption iconType ) throws TriSystemException  {

		if ( TriStringUtils.isEmpty( lotId ) ) {
			return null;
		}

		String type = iconType.value();

		if ( TriStringUtils.isEmpty( type ) ) {
			type = IconSelectionOption.DefaultImage.value();
		}

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.lotImages);

		if ( IconSelectionOption.DefaultImage.equals( type ) ) {
			String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.defaultParentMapping);
			String path = TriStringUtils.linkPath( mapping, images );

			return TriStringUtils.convertPath(path, true );

		} else if ( IconSelectionOption.CustomImage.equals( type ) ) {
			String parent = sheet.getValue(UmDesignEntryKeyByWebResources.customParentMapping);
			String path = TriStringUtils.linkPath( parent, "/lots/" + lotId );
			path = TriStringUtils.linkPath( path, images );

			return TriStringUtils.convertPath( path, true );
		}

		return null;
	}
	
	/**
	 * Returns the local path of the lot's custom icon.
	 * <br>
	 * <br>ロットアイコンのパスを取得します。
	 *
	 * @param lotId
	 * @param fileNm
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotCustomIconLocalPath(
			String lotId,
			String fileNm ) throws TriSystemException  {

		String path = getLotCustomIconParentLocalPath( lotId );

		if ( TriStringUtils.isEmpty( path ) ) {
			return path;
		}

		path = TriStringUtils.linkPath( path, fileNm );
		return TriStringUtils.convertPath( path );
	}
	
	/**
	 * Returns the parent path of the lot's custom icon.
	 * <br>
	 * <br>ロットアイコンの親パスを取得します。
	 *
	 * @param lotId
	 * @return
	 * @throws TriSystemException
	 */
	public static String getLotCustomIconParentLocalPath( String lotId ) throws TriSystemException  {

		if ( TriStringUtils.isEmpty( lotId ) ) {
			return null;
		}

		String images = sheet.getValue(UmDesignEntryKeyByWebResources.lotImages);

		String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.customParentLocation);
		String path = TriStringUtils.linkPath( mapping, "/lots/" + lotId );
		path = TriStringUtils.linkPath( path, images );

		return TriStringUtils.convertPath( path );
	}
	
	public static File getAttachmentFile(String wikiId, String appendFileSeqNo, String fileName) {
		String basePath = sheet.getValue(UmDesignEntryKeyByWikiAttachment.appendFilePath);

		String wikiIdPath = TriStringUtils.linkPathBySlash(basePath, wikiId);
		String appendFileSeqNoPath = TriStringUtils.linkPathBySlash(wikiIdPath, appendFileSeqNo);
		
		return new File(appendFileSeqNoPath , fileName);
	}
	
	/**
	 * Returns Gender from user.
	 * <br>
	 * <br>性別を取得します。
	 *
	 * @param userEntity
	 * @return
	 * @throws TriSystemException
	 */
	
	public static GenderType getGender( IUserEntity user )
	{
		GenderType gender = GenderType.value(user.getGenderTyp());
		if( TriStringUtils.isEmpty(gender)){
			gender = GenderType.Male;
		}
		return gender;
	}
}
