package jp.co.blueship.tri.fw.um;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class UmProjectEditServiceUtils {

	public static List<ItemLabelsBean> getTimezoneViews() {
		List<ItemLabelsBean> timezoneList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.timezones);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean timezoneBean = new ItemLabelsBean();
			timezoneBean.setValue(entry.getKey());
			timezoneBean.setLabel(entry.getValue());

			timezoneList.add(timezoneBean);
		}

		return timezoneList;
	}

	public static List<ItemLabelsBean> getLanguageViews() {
		List<ItemLabelsBean> languageList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.langs);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean languageBean = new ItemLabelsBean();
			languageBean.setValue(entry.getKey());
			languageBean.setLabel(entry.getValue());

			languageList.add(languageBean);
		}

		return languageList;
	}

	public static List<ItemLabelsBean> getProjectIconViews(String userId) {
		List<ItemLabelsBean> iconList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.projectIcons);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean userIcon = new ItemLabelsBean();
			userIcon.setLabel(entry.getValue());
			String defaultProjectIconPath = UmDesignBusinessRuleUtils.getProjectIconParentSharePath(IconSelectionOption.DefaultImage);
			String iconPath = TriStringUtils.linkPathBySlash(defaultProjectIconPath, entry.getKey());
			userIcon.setValue(iconPath);

			iconList.add(userIcon);
		}

		return iconList;
	}

	public static List<ItemLabelsBean> getLookAndFeelViews() {
		LookAndFeel lookAndFeelOld = LookAndFeel.TrinityClassic;
		LookAndFeel lookAndFeelNew = LookAndFeel.Trinity;

		List<ItemLabelsBean> list = new ArrayList<ItemLabelsBean>();
		list.add(new ItemLabelsBean(lookAndFeelNew.name(), lookAndFeelNew.value()));
		list.add(new ItemLabelsBean(lookAndFeelOld.name(), lookAndFeelOld.value()));

		return list;
	}
}
