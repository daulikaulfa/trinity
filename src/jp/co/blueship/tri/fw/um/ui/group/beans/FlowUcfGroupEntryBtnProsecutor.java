package jp.co.blueship.tri.fw.um.ui.group.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryBtnServiceBean;
import jp.co.blueship.tri.fw.um.ui.group.rb.UcfGroupListResponseBean;

public class FlowUcfGroupEntryBtnProsecutor extends PresentationProsecutor{

	public static final String FLOW_ACTION_ID = "FlowUcfGroupEntryBtnService";

	public FlowUcfGroupEntryBtnProsecutor() {
		super(null);
	}

	public FlowUcfGroupEntryBtnProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★groupId:" + reqInfo.getParameter("groupId"));
			this.getLog().debug("★★★groupName:" + reqInfo.getParameter("groupName"));
		}

		FlowUcfGroupEntryBtnServiceBean bean = new FlowUcfGroupEntryBtnServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setInGroupId(reqInfo.getParameter("groupId"));
		bean.setInGroupName(reqInfo.getParameter("groupName"));

		if ( null != reqInfo.getParameterValues("role[]") ) {

			List<String> inRoleList = new ArrayList<String>();

			for ( String value : reqInfo.getParameterValues("role[]") ){
				inRoleList.add(value);
			}

			bean.setInRoleList(inRoleList);
		}

		return bean;
	}


	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

		FlowUcfGroupEntryBtnServiceBean bean = (FlowUcfGroupEntryBtnServiceBean)getServiceReturnInformation();
		UcfGroupListResponseBean resBean = new UcfGroupListResponseBean();
		resBean.setGroupViewBeanList(bean.getViewBeanList());
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
