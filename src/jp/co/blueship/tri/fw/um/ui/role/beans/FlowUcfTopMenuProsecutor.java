package jp.co.blueship.tri.fw.um.ui.role.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.ui.role.rb.UcfTopMenuResponseBean;

public class FlowUcfTopMenuProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "";

	public FlowUcfTopMenuProsecutor() {
		super(null);
	}

	public FlowUcfTopMenuProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService arg0, IGeneralServiceBean arg1) throws Exception {
		return null;
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {
		return null;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {
		UcfTopMenuResponseBean resBean = new  UcfTopMenuResponseBean();
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {
		return null;
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
