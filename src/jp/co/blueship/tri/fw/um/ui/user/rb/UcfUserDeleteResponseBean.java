package jp.co.blueship.tri.fw.um.ui.user.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class UcfUserDeleteResponseBean extends BaseResponseBean{
	
	/** ユーザID **/
	private String userId   		  = null;
	/** ユーザ名 **/
	private String userName 		  = null;
	/** パスワード **/
	private String password 		  = null;
	/** 確認用パスワード **/
	private String repassword 		  = null;
	/** メールアドレス **/
	private String mailAddress 	  = null;
	/** 部署リスト **/
	private List<String> sectionViewList = new ArrayList<String>();
	/** 部署Id **/
	private String sectionId		  = null;
	/** 部署名 **/
	private String sectionName		  = null;
	/** グループID **/
	private String groupId         = null;
	/** グループ名 **/
	private String groupName         = null;
	/** グループリスト **/
	private List<String> groupSelected = new ArrayList<String>();
	
	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRepassword() {
		return repassword;
	}
	public void setRepassword(String repassword) {
		this.repassword = repassword;
	}
	public List<String> getSectionViewList() {
		return sectionViewList;
	}
	public void setSectionViewList(List<String> sectionList) {
		this.sectionViewList = sectionList;
	}
	public String getSectionId() {
		return sectionId;
	}
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}	
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}	
		
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public List<String> getGroupSelected() {
		return groupSelected;
	}
	
	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	
	}
	
}
