package jp.co.blueship.tri.fw.um.ui.group;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.um.ui.group.beans.FlowUcfGroupModifyBtnProsecutor;
import jp.co.blueship.tri.fw.um.ui.group.beans.FlowUcfGroupModifyProsecutor;

public class FlowUcfGroupModifyBtn extends PresentationController{

	@Override
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfGroupModifyBtnProsecutor());
	
	}	

	@Override
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfGroupModifyProsecutor(bbe));
	}	
	
	@Override
	protected String getForward(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return "UcfGroupList";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "UcfGroupModify";
	}

}
