package jp.co.blueship.tri.fw.um.ui.group.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;

public class UcfGroupListResponseBean extends BaseResponseBean{

	private List<GroupListViewBean> groupViewBeanList;
	private List<RoleListViewBean> roleList;
	private List<String> roleSelected = new ArrayList<String>();

	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	public List<RoleListViewBean> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<RoleListViewBean> roleList) {
		this.roleList = roleList;
	}
	public List<String> getRoleSelected() {
		return roleSelected;
	}
	public void setRoleSelected(List<String> roleSelected) {
		this.roleSelected = roleSelected;
	}
}
