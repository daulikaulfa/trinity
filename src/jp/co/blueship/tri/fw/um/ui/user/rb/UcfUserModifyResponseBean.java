package jp.co.blueship.tri.fw.um.ui.user.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.PasswordEffectiveTermViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.SectionViewBean;


public class UcfUserModifyResponseBean extends BaseResponseBean{

	/** ユーザID **/
	private String userId		= null;
	/** ユーザ名 **/
	private String userName	= null;
	/** パスワード **/
	private String password	= null;
	/** 再確認パスワード **/
	private String rePassword	= null;
	/** パスワード有効期間 **/
	private String termId		= null;
	/** パスワード有効期限 **/
	private String termLimit	= null;
	/** メールアドレス **/
	private String mailAddress	= null;
	/** 部署Id **/
	private String sectionId	= null;
	/** グループリスト **/
	private List<GroupListViewBean> groupList		= null;
	/** 選択グループリスト **/
	private List<String> groupSelected = null;
	/** パスワード有効期限 */
	private List<PasswordEffectiveTermViewBean> termViewBeanList = null;

	public String getMailAddress() {
		return mailAddress;
	}
	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getRePassword() {
		return rePassword;
	}
	public void setRePassword(String rePassword) {
		this.rePassword = rePassword;
	}
	public String getTermId() {
		return termId;
	}
	public void setTermId(String termId) {
		this.termId = termId;
	}
	public String getTermLimit() {
		return termLimit;
	}
	public void setTermLimit(String termLimit) {
		this.termLimit = termLimit;
	}

	public String getSectionId() {
		return sectionId;
	}
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	public List<GroupListViewBean> getGroupList() {
		return groupList;
	}
	public void setGroupList(List<GroupListViewBean> groupList) {
		this.groupList = groupList;
	}
	public List<String> getGroupSelected() {
		return groupSelected;
	}
	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	}

	private List<SectionViewBean> sectionViewBeanList;

	public List<SectionViewBean> getSectionViewBeanList() {
		return sectionViewBeanList;
	}

	public void setSectionViewBeanList( List<SectionViewBean> sectionViewBeanList ) {
		this.sectionViewBeanList = sectionViewBeanList;
	}

	public List<PasswordEffectiveTermViewBean> getTermViewBeanList() {
		return termViewBeanList;
	}

	public void setTermViewBeanList( List<PasswordEffectiveTermViewBean> termViewBeanList ) {
		this.termViewBeanList = termViewBeanList;
	}
}
