package jp.co.blueship.tri.fw.um.ui.user.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.ui.user.rb.UcfUserListResponseBean;

/**
 *
 * @version V3L10.02
 *
 * @version SP-20150622_V3L13R01
 * @author Norheda Zulkipeli
 */
public class FlowUcfUserModifyBtnProsecutor extends PresentationProsecutor{

	public static final String FLOW_ACTION_ID = "FlowUcfUserModifyBtnService";


	public FlowUcfUserModifyBtnProsecutor() {
		super(null);
	}

	public FlowUcfUserModifyBtnProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★userId:" + reqInfo.getParameter("userId"));
			this.getLog().debug("★★★userName:" + reqInfo.getParameter("userName"));
			this.getLog().debug("★★★password:" + reqInfo.getParameter("password"));
			this.getLog().debug("★★★rePassword:" + reqInfo.getParameter("rePassword"));
			this.getLog().debug("★★★termId:" + reqInfo.getParameter("termId"));
			this.getLog().debug("★★★mailAddress:" + reqInfo.getParameter("mailAddress"));
			this.getLog().debug("★★★sectionId:" + reqInfo.getParameter("sectionId"));
			if ( null != reqInfo.getParameterValues("group[]") )
				for ( String value: reqInfo.getParameterValues("group[]") ) this.getLog().debug("☆☆☆group:" + value);
		}

		FlowUcfUserModifyBtnServiceBean bean = new FlowUcfUserModifyBtnServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setInUserId		( reqInfo.getParameter("userId") );
		bean.setInUserName		( reqInfo.getParameter("userName") );
		bean.setInPassword		( reqInfo.getParameter("password") );
		bean.setInRepassword	( reqInfo.getParameter("rePassword") );
		bean.setInPasswordEffectiveTerm( reqInfo.getParameter("termId") );
		bean.setInMailAddress	( reqInfo.getParameter("mailAddress") );
		bean.setInSectionId		( reqInfo.getParameter("sectionId") );

		if ( null != reqInfo.getParameterValues("group[]") ) {

			List<String> inGroupList = new ArrayList<String>();

			for ( String value : reqInfo.getParameterValues("group[]") ){
				inGroupList.add(value);
			}

			bean.setInGroupList(inGroupList);
		}

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

		FlowUcfUserModifyBtnServiceBean bean = (FlowUcfUserModifyBtnServiceBean)getServiceReturnInformation();
		UcfUserListResponseBean resBean = new UcfUserListResponseBean();

		resBean.setUserViewBeanList(bean.getViewBeanList());
		resBean.setGroupList(bean.getGroupViewBeanList());
		resBean.setGroupSelected(bean.getInGroupList());

		resBean.new MessageUtility().reflectComment( bean, false );
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
