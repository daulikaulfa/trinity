package jp.co.blueship.tri.fw.um.ui.user;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.um.ui.user.beans.FlowUcfUserDeleteBtnProsecutor;
import jp.co.blueship.tri.fw.um.ui.user.beans.FlowUcfUserDeleteProsecutor;

public class FlowUcfUserDeleteBtn extends PresentationController {

	@Override
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfUserDeleteBtnProsecutor());
	}		

	@Override
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfUserDeleteProsecutor(bbe));
	}	
	
	@Override
	protected String getForward(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3) {
		return "UcfUserList";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3, TriRuntimeException arg4) {
		return "UcfUserDelete";
	}

}
