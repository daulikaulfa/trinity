package jp.co.blueship.tri.fw.um.ui.role.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;

public class UcfRoleModifyResponseBean extends BaseResponseBean{

	private String roleId;
	private String roleName;
	private List<ActionListViewBean> actionList;
	private List<String> actionSelected = new ArrayList<String>();

	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<ActionListViewBean> getActionList() {
		return actionList;
	}
	public void setActionList(List<ActionListViewBean> actionList) {
		this.actionList = actionList;
	}
	public List<String> getActionSelected() {
		return actionSelected;
	}
	public void setActionSelected(List<String> actionSelected) {
		this.actionSelected = actionSelected;
	}
}
