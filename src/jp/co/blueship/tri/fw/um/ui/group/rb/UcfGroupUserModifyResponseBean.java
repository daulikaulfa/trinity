package jp.co.blueship.tri.fw.um.ui.group.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean.UserListViewBean;

public class UcfGroupUserModifyResponseBean extends BaseResponseBean{

	/** グループ名 **/
	private String groupName = null;
	/** グループID **/
	private String groupId = null;
	/** グループリスト **/
	private List<?> groupList = new ArrayList<Object>();
	/** ユーザリスト **/
	private List<UserListViewBean> userList;
	/** 選択ユーザリスト **/
	private List<String> userSelected = new ArrayList<String>();

	public List<?> getGroupList() {
		return groupList;
	}
	public void setGroupList(List<?> groupList) {
		this.groupList = groupList;
	}
	public List<UserListViewBean> getUserList() {
		return userList;
	}
	public void setUserList(List<UserListViewBean> userList) {
		this.userList = userList;
	}
	public List<String> getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(List<String> userSelected) {
		this.userSelected = userSelected;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

}
