package jp.co.blueship.tri.fw.um.ui.dept.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;

public class UcfSectionListResponseBean extends BaseResponseBean{

	private List<SectionListViewBean> sectionViewBeanList;

	public List<SectionListViewBean> getSectionViewBeanList() {
		return sectionViewBeanList;
	}

	public void setSectionViewBeanList(List<SectionListViewBean> sectionViewBeanList) {
		this.sectionViewBeanList = sectionViewBeanList;
	}

}
