package jp.co.blueship.tri.fw.um.ui.dept.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.log.TriLogMessage;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionModifyServiceBean;
import jp.co.blueship.tri.fw.um.ui.dept.rb.UcfSectionModifyResponseBean;

public class FlowUcfSectionModifyProsecutor extends PresentationProsecutor{

	public static final String FLOW_ACTION_ID = "FlowUcfSectionModifyService";

	public FlowUcfSectionModifyProsecutor(){
		super(null);
	}

	public FlowUcfSectionModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);

	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {
		IContextAdapter ac = ContextAdapterFactory.getContextAdapter();
		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug( ac.getMessage(TriLogMessage.LSM0033 , (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID) ) );
			this.getLog().debug( ac.getMessage(TriLogMessage.LSM0034 , reqInfo.getParameter("sectionId") ) );
		}

		FlowUcfSectionModifyServiceBean bean = new FlowUcfSectionModifyServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setInSectionId(reqInfo.getParameter("sectionId"));
		bean.setSectionId(reqInfo.getParameter("sectionId"));
		bean.setSectionName(reqInfo.getParameter("sectionName"));

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		FlowUcfSectionModifyServiceBean bean = (FlowUcfSectionModifyServiceBean)getServiceReturnInformation();
		UcfSectionModifyResponseBean resBean = new UcfSectionModifyResponseBean();

		resBean.setSectionId(bean.getSectionId());
		resBean.setSectionName(bean.getSectionName());
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {


	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {


	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {


	}

}
