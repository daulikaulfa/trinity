package jp.co.blueship.tri.fw.um.ui.group;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.um.ui.group.beans.FlowUcfGroupListProsecutor;
import jp.co.blueship.tri.fw.um.ui.group.beans.FlowUcfGroupUserModifyProsecutor;

public class FlowUcfGroupUserModify extends PresentationController {

	@Override
	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfGroupUserModifyProsecutor());
	}	
	
	@Override
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfGroupListProsecutor(bbe));
	}
	
	@Override
	protected String getForward(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3) {
		return "UcfGroupUserModify";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager arg0, 
			IRequestInfo arg1, ISessionInfo arg2, IApplicationInfo arg3, TriRuntimeException arg4) {
		return "UcfGroupList";
	}

}
