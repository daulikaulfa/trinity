package jp.co.blueship.tri.fw.um.ui.dept.rb;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class UcfSectionDeleteResponseBean extends BaseResponseBean{
	
	/** 部署ID **/
	private String sectionId   = null;
	/** 部署名 **/
	private String sectionName = null;
	
	public String getSectionId() {
		return sectionId;
	}
	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}
	public String getSectionName() {
		return sectionName;
	}
	public void setSectionName(String sectionName) {
		this.sectionName = sectionName;
	}

}
