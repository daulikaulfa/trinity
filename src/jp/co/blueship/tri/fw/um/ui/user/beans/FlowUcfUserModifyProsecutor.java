package jp.co.blueship.tri.fw.um.ui.user.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserModifyServiceBean;
import jp.co.blueship.tri.fw.um.ui.user.rb.UcfUserModifyResponseBean;

public class FlowUcfUserModifyProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowUcfUserModifyService";


	public FlowUcfUserModifyProsecutor() {
		super(null);
	}

	public FlowUcfUserModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★userId:" + reqInfo.getParameter("userId"));
		}

		FlowUcfUserModifyServiceBean bean = new FlowUcfUserModifyServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setInUserId	( reqInfo.getParameter("userId") );
		bean.setInUserName	( reqInfo.getParameter("userName") );
		bean.setPassword	( reqInfo.getParameter("password") );
		bean.setRepassword	( reqInfo.getParameter("rePassword") );
		bean.setMailAddress	( reqInfo.getParameter("mailAddress") );
		bean.setSectionId	( reqInfo.getParameter("sectionId") );
		bean.setTermId		( reqInfo.getParameter("termId") );

		if ( null != reqInfo.getParameterValues("group[]") ) {
			List<String> inGroupList = new ArrayList<String>();
			for ( String value : reqInfo.getParameterValues("group[]") ){
				inGroupList.add(value);
			}
			bean.setGroupSelected( inGroupList);
		}


		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		FlowUcfUserModifyServiceBean bean = (FlowUcfUserModifyServiceBean)getServiceReturnInformation();
		UcfUserModifyResponseBean resBean = new UcfUserModifyResponseBean();

		resBean.setUserId		( bean.getInUserId() );
		resBean.setUserName		( bean.getUserName() );
		resBean.setPassword		( bean.getPassword() );
		resBean.setRePassword	( bean.getRepassword() );
		resBean.setMailAddress	( bean.getMailAddress() );
		resBean.setSectionId	( bean.getSectionId() );
		resBean.setTermId		( bean.getTermId() );
		resBean.setTermLimit	( bean.getTermLimit() );

		resBean.setSectionViewBeanList	( bean.getSectionViewBeanList() );
		resBean.setGroupList			( bean.getGroupViewBeanList() );
		resBean.setGroupSelected		( bean.getGroupSelected() );
		resBean.setTermViewBeanList		( bean.getTermViewBeanList() );

		resBean.new MessageUtility().reflectComment( bean, false );
		resBean.new MessageUtility().reflectMessage(getBussinessException());

		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {

	}

	@Override
	protected void preProcessor(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) {

	}

}
