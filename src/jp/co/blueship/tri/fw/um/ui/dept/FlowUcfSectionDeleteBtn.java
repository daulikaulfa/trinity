package jp.co.blueship.tri.fw.um.ui.dept;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.um.ui.dept.beans.FlowUcfSectionDeleteBtnProsecutor;
import jp.co.blueship.tri.fw.um.ui.dept.beans.FlowUcfSectionDeleteProsecutor;

public class FlowUcfSectionDeleteBtn extends PresentationController{

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfSectionDeleteBtnProsecutor());
	}		

	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfSectionDeleteProsecutor(bbe));
	}		

	@Override
	protected String getForward(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return "UcfSectionList";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "UcfSectionDelete";
	}

}
