package jp.co.blueship.tri.fw.um.ui.role.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class UcfRoleDeleteResponseBean extends BaseResponseBean{
	
	private String roleId;
	private String roleName;
	private String groupId;
	private String groupName;
	private List<String> groupSelected = new ArrayList<String>();
	
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public String getGroupId() {
		return groupId;
	}
	
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}	
		
	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public List<String> getGroupSelected() {
		return groupSelected;
	}
	
	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	
	}

}
