package jp.co.blueship.tri.fw.um.ui.group.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;

public class UcfGroupDeleteResponseBean extends BaseResponseBean{
	
	private String groupId;
	private String groupName;
	private String userId;
	private String userName;
	private List<String> userSelected = new ArrayList<String>();

	public List<String> getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(List<String> userSelected) {
		this.userSelected = userSelected;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;	
	}

}
