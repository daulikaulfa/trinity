package jp.co.blueship.tri.fw.um.ui.role.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;

public class UcfRoleListResponseBean extends BaseResponseBean{

	private List<RoleListViewBean> roleViewBeanList;

	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}

	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}

}
