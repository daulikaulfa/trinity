package jp.co.blueship.tri.fw.um.ui.user.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;

public class UcfUserListResponseBean extends BaseResponseBean{

	private List<UserListViewBean> userViewBeanList;
	private List<GroupListViewBean> groupList;
	private List<String> groupSelected = new ArrayList<String>();

	public List<UserListViewBean> getUserViewBeanList() {
		return userViewBeanList;
	}

	public void setUserViewBeanList(List<UserListViewBean> userViewBeanList) {
		this.userViewBeanList = userViewBeanList;
	}

	public List<GroupListViewBean> getGroupList() {
		return groupList;
	}
	public void setGroupList(List<GroupListViewBean> groupList) {
		this.groupList = groupList;
	}
	public List<String> getGroupSelected() {
		return groupSelected;
	}
	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	}

}
