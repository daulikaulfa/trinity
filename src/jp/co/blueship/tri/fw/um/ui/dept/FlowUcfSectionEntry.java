package jp.co.blueship.tri.fw.um.ui.dept;

import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationController;
import jp.co.blueship.tri.fw.um.ui.dept.beans.FlowUcfSectionEntryProsecutor;
import jp.co.blueship.tri.fw.um.ui.dept.beans.FlowUcfSectionListProsecutor;

public class FlowUcfSectionEntry extends PresentationController{

	protected void addPresentationProsecutores(PresentationProsecutorManager ppm) {
		ppm.addPresentationProsecutor(new FlowUcfSectionEntryProsecutor());
	}
	
	protected void addBusinessErrorPresentationProsecutores(PresentationProsecutorManager ppm, BaseBusinessException bbe) {
		ppm.addBusinessErrorPresentationProsecutor(new FlowUcfSectionListProsecutor(bbe));
	}	
	
	@Override
	protected String getForward(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo) {
		return "UcfSectionEntry";
	}

	@Override
	protected String getForwardForBusinessException(PresentationProsecutorManager ppm, 
			IRequestInfo reqInfo, ISessionInfo sesInfo, IApplicationInfo appInfo, TriRuntimeException bbe) {
		return "UcfSectionList";
	}

}
