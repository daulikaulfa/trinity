package jp.co.blueship.tri.fw.um.ui.role.beans;

import jp.co.blueship.tri.fw.act.rb.IBaseResponseBean;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.IGenericTransactionService;
import jp.co.blueship.tri.fw.domain.IService;
import jp.co.blueship.tri.fw.domain.dto.IGeneralServiceBean;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domain.dto.ServiceDto;
import jp.co.blueship.tri.fw.ex.BaseBusinessException;
import jp.co.blueship.tri.fw.session.ISessionInfo;
import jp.co.blueship.tri.fw.session.SessionScopeKeyConsts;
import jp.co.blueship.tri.fw.session.application.IApplicationInfo;
import jp.co.blueship.tri.fw.session.request.IRequestInfo;
import jp.co.blueship.tri.fw.ui.PresentationProsecutor;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleModifyServiceBean;
import jp.co.blueship.tri.fw.um.ui.role.rb.UcfRoleModifyResponseBean;

public class FlowUcfRoleModifyProsecutor extends PresentationProsecutor {

	public static final String FLOW_ACTION_ID = "FlowUcfRoleModifyService";

	public FlowUcfRoleModifyProsecutor() {
		super(null);
	}

	public FlowUcfRoleModifyProsecutor(BaseBusinessException bbe) {
		super(bbe);
	}

	@Override
	protected IGeneralServiceBean callApplicationService(IService service,
			IGeneralServiceBean info) throws Exception {
		IServiceDto<IGeneralServiceBean> serviceDto = new ServiceDto<IGeneralServiceBean>();
		serviceDto.setServiceBean( info );

		return ((IGenericTransactionService)service).execute(FLOW_ACTION_ID, serviceDto).getServiceBean();
	}

	@Override
	protected void getApplicationInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected IGeneralServiceBean getBussinessInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		if ( this.getLog().isDebugEnabled() ) {
			this.getLog().debug("★★★roleId:" + reqInfo.getParameter("roleId"));
		}

		FlowUcfRoleModifyServiceBean bean = new FlowUcfRoleModifyServiceBean();
		String userId = (String)sesInfo.getAttribute(SessionScopeKeyConsts.FLWC00_USER_ID);
		bean.setUserId( userId );
		bean.setUserName(userName());

		bean.setInRoleId(reqInfo.getParameter("roleId"));
		bean.setInRoleName(reqInfo.getParameter("roleName"));

		return bean;
	}

	@Override
	protected IBaseResponseBean getRequestInfo(IApplicationInfo appInfo,
			ISessionInfo sesInfo, IRequestInfo reqInfo) throws Exception {

		FlowUcfRoleModifyServiceBean bean = (FlowUcfRoleModifyServiceBean)getServiceReturnInformation();
		UcfRoleModifyResponseBean resBean = new UcfRoleModifyResponseBean();

		resBean.setRoleId(bean.getRoleId());
		resBean.setRoleName(bean.getRoleName());
		resBean.setActionList(bean.getActionViewBeanList());
		resBean.setActionSelected(bean.getActionSelected());
		resBean.new MessageUtility().reflectMessage(getBussinessException());
		return resBean;
	}

	@Override
	protected String getResponseName() {
		return "responseBean";
	}

	@Override
	protected IService getService(
			IApplicationInfo bbe, ISessionInfo sesInfo, IRequestInfo reqInfo ) {

		IContextAdapter ca = ContextAdapterFactory.getContextAdapter();
		return (IService)ca.getBean("generalService");
	}

	@Override
	protected void getSessionInfo(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) throws Exception {

	}

	@Override
	protected void postProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

	@Override
	protected void preProcessor(IApplicationInfo arg0, ISessionInfo arg1, IRequestInfo arg2) {

	}

}
