package jp.co.blueship.tri.fw.um.ui.role.rb;

import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;

public class UcfRoleEntryResponseBean extends BaseResponseBean{

	private String roleId ;
	private String roleName ;
	private List<ActionListViewBean> actionList;

	public String getRoleId() {
		return roleId;
	}

	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<ActionListViewBean> getActionList() {
		return actionList;
	}
	public void setActionList(List<ActionListViewBean> actionList) {
		this.actionList = actionList;
	}

}
