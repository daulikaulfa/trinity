package jp.co.blueship.tri.fw.um.ui.group.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;

public class UcfGroupModifyResponseBean extends BaseResponseBean{

	private String groupId;
	private String groupName;
	private String roleId;
	private String roleName;
	private List<RoleListViewBean> roleList;
	private List<String> roleSelected = new ArrayList<String>();

	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getRoleId() {
		return roleId;
	}
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public List<RoleListViewBean> getRoleList() {
		return roleList;
	}
	public void setRoleList(List<RoleListViewBean> roleList) {
		this.roleList = roleList;
	}
	public List<String> getRoleSelected() {
		return roleSelected;
	}
	public void setRoleSelected(List<String> roleSelected) {
		this.roleSelected = roleSelected;
	}

}
