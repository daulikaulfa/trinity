package jp.co.blueship.tri.fw.um.ui.group.rb;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.act.rb.BaseResponseBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;

public class UcfGroupEntryResponseBean extends BaseResponseBean{
	
	private String groupId ;
	private String groupName ;
	private String roleId;
	private String roleName;
	private List<RoleListViewBean> roleList = new ArrayList<RoleListViewBean>();
	
	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public String getRoleId() {
		return roleId;
	}
	
	public void setRoleId(String roleId) {
		this.roleId = roleId;
	}
	
	public String getRoleName() {
		return roleName;
	}
	
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
		
	public List<RoleListViewBean> getRoleList() {
		return roleList;
	}
	
	public void setRoleList(List<RoleListViewBean> roleList) {
		this.roleList = roleList;
	}
}
