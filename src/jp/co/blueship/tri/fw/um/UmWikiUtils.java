package jp.co.blueship.tri.fw.um;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class UmWikiUtils {
	private static String RegexOfTag = "\\[.+?\\]";
	private static final String TagBegin = "[" ;

	/**
	 * Gets the string of tag.
	 *
	 * @param pageName A string containing the tag
	 * @return tags
	 */
	static public String[] extractTags( String pageName ) {
		Set<String> results = new LinkedHashSet<String>();
		if ( TriStringUtils.isEmpty(pageName) )
			return new String[0];

		String name = pageName.trim();

		if ( ! StringUtils.startsWith(name, TagBegin) )
			return new String[0];

		Pattern pattern = Pattern.compile(RegexOfTag);
		Matcher matcher = pattern.matcher(name);

		while ( matcher.find() ) {
			int max = matcher.groupCount();

			for ( int i = 0; i <= max; i++ ) {
				results.add( matcher.group(i) );
			}
		}

		return FluentList.from(results).toArray(new String[0]);
	}

	/**
	 * Format all tags of a String within page name.
	 *
	 * @param pageName A string containing the tag
	 * @return the text with tags sort processed
	 */
	static public String formatPageName( String pageName ) {

		if ( TriStringUtils.isEmpty(pageName) )
			return pageName;

		String[] tags = extractTags( pageName );

		StringBuffer buf = new StringBuffer();
		for ( String tag: tags ) {
			pageName = StringUtils.replace(pageName, tag, "");
			//buf.append(tag);
		}

		buf.append(pageName);

		return buf.toString();
	}
	
	/**
	 * Check empty for pageNm and content
	 * 
	 * @param pageNm Wiki page name
	 * @param content Wiki page content
	 */
	public static void validateWiki( String pageNm, String content ) {
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();
		
		if( TriStringUtils.isEmpty( pageNm ) ) {
			messageList.add(UmMessageId.UM001082E);
			messageArgsList.add	( new String[] {} );
		}
		if( TriStringUtils.isEmpty( content ) ) {
			messageList.add(UmMessageId.UM001083E);
			messageArgsList.add	( new String[] {} );
		}
		
		if ( messageList.size() > 0 )
			throw new ContinuableBusinessException( messageList, messageArgsList );
	}
}