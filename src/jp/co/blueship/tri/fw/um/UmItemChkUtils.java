
package jp.co.blueship.tri.fw.um;

import java.util.*;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;

import org.apache.commons.lang.math.NumberUtils;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * 画面入力項目の必須チェック、形式チェックの共通処理Class
 * <br>
 * <p>
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class UmItemChkUtils {

	private static final String ID = "id";
	private static final String NAME = "name";
	private static final String OLDNAME = "oldname";
	private static final String PASSWORD = "password";
	private static final String CONFIRM = "confirm";
	private static final String MAIL = "mail";
	private static final String SECTION = "section";
	private static final String TERMID = "termId";
	public static final String ADMIN = "admin";
	public static final String ADMIN_GROUP_ID = "1";
	public static final String ADMIN_ROLE_ID = "1";

	private static final int MAX_USER_ID_LENGTH = 20;
	public static final int MAX_NAME_LENGTH = 80;
	private static final int MAX_ROLE_NAME_LENGTH = 40;
	private static final int MAX_MAIL_ADDRESS_LENGTH = 128;
	private static final int MAX_CATEGORY_NAME_LENGTH = 140;
	private static final int MAX_MILESTONE_NAME_LENGTH = 140;

	public enum Mode {

		INSERT( "INSERT" ),
		UPDATE( "UPDATE" ),
		DELETE( "DELETE" ),
		VIEW( "VIEW" );

		String value ;

		private Mode( String value ) {
			this.value = value ;
		}
	}

	/**
	 * 部署情報の必須チェック、形式チェックを行う。
	 * 必須項目に入力がない、または入力形式が正しくなければ例外をスローする。
	 * @param sectionId 部署ID
	 */

	public static void checkUcfInputSection( String sectionId, String sectionName , String oldSectionName , List<IDeptEntity> entities , Mode mode ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			// セクションID
			// 自動採番に伴いコメントアウト
//			if ( StringAddonUtil.isNothing( sectionId ) ) {
//				messageList.add		( MessageId.MESUCF0001 );
//				messageArgsList.add	( new String[] {} );
//			} else {
//				if (! StringAddonUtil.isDigits( sectionId ) ) {
//					messageList.add		( MessageId.MESUCF0002 );
//					messageArgsList.add	( new String[] {} );
//				} else {
//					if (sectionId.length() > MAX_ID_LENGTH) {
//						messageList.add		( MessageId.MESUCF0023 );
//						messageArgsList.add	( new String[] {} );
//					} else {
//						if (Integer.parseInt(sectionId) == 0) {
//							messageList.add		( MessageId.MESUCF0002 );
//							messageArgsList.add	( new String[] {} );
//						}
//					}
//				}
//			}

			// 部署名
			if ( TriStringUtils.isEmpty( sectionName ) ) {
				messageList.add		( UmMessageId.UM001013E );
				messageArgsList.add	( new String[] {} );
			} else if (sectionName.length() > MAX_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001014E );
				messageArgsList.add	( new String[] {} );
			} else {
				for( IDeptEntity sectionEntity : entities ) {
					if( sectionName.equals( sectionEntity.getDeptNm() ) ) {
						boolean errFlg = false ;
						if( Mode.INSERT.equals( mode ) ) {
							errFlg = true ;
						} else if( Mode.UPDATE.equals( mode ) ) {
							if( null != oldSectionName && ! oldSectionName.equals( sectionName ) ) {
								errFlg = true ;
							}
						}
						if( errFlg ) {
							messageList.add		( UmMessageId.UM001044E );
							messageArgsList.add	( new String[] {} );
							break ;
						}
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ユーザ情報の必須チェック、形式チェックを行う。
	 * 必須項目に入力がない、または入力形式が正しくなければ例外をスローする。
	 * @param userId ユーザID
	 */

	public static void checkUcfInputUser( String userId ,String userName , String oldUserName , String password ,
			String confirm , String mail , String section , String termId ,
			List<IUserEntity> userEntities , Mode mode ) {

		Map<String, String> map = new HashMap<String, String>();
		map.put(ID, userId);
		map.put(NAME, userName);
		map.put(OLDNAME, oldUserName);
		map.put(PASSWORD, password);
		map.put(CONFIRM, confirm);
		map.put(MAIL, mail);
		map.put(SECTION, section);
		map.put(TERMID, termId );

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			String value = null;
			// ユーザID
			value = map.get(ID);
			if ( TriStringUtils.isEmpty( value ) ) {
				messageList.add		( UmMessageId.UM001000E );
				messageArgsList.add	( new String[] {} );

			} else {

				if (value.length() > MAX_USER_ID_LENGTH) {
					messageList.add		( UmMessageId.UM001004E );
					messageArgsList.add	( new String[] {} );
				}

				if ( !TriStringUtils.isAllHankaku( value ) || TriStringUtils.containsHanKana( value ) ) {
					messageList.add		( UmMessageId.UM001034E );
					messageArgsList.add	( new String[] {} );
				}
			}


			// ユーザ名
			value = map.get(NAME);
			if ( TriStringUtils.isEmpty( value ) ) {
				messageList.add		( UmMessageId.UM001005E );
				messageArgsList.add	( new String[] {} );
			} else if (value.length() > MAX_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001006E );
				messageArgsList.add	( new String[] {} );
			} else {
				for( IUserEntity userEntity : userEntities ) {
					if( value.equals( userEntity.getUserNm() ) ) {
						boolean errFlg = false ;
						if( Mode.INSERT.equals( mode ) ) {
							errFlg = true ;
						} else if( Mode.UPDATE.equals( mode ) ) {
							String oldName = map.get(OLDNAME) ;
							if( null != oldName && ! oldName.equals( value ) ) {
								errFlg = true ;
							}
						}
						if( errFlg ) {
							messageList.add		( UmMessageId.UM001045E );
							messageArgsList.add	( new String[] {} );
							break ;
						}
					}
				}
			}

			// パスワード
			String minPasswordLength = DesignSheetUtils.getPasswordMinimumLength();
			String maxPasswordLength = DesignSheetUtils.getPasswordMaxLength();
			int passMinLen = 0;
			int passMaxLen = 0;
			if ( NumberUtils.isNumber( minPasswordLength )) {
				passMinLen = Integer.parseInt( minPasswordLength );
			}
			if ( NumberUtils.isNumber( maxPasswordLength )) {
				passMaxLen = Integer.parseInt( maxPasswordLength );
			}

			value = map.get(PASSWORD);
			if ( TriStringUtils.isEmpty( value ) ) {
				messageList.add		( UmMessageId.UM001007E );
				messageArgsList.add	( new String[] {} );

			} else {
//
//				if ( !StringAddonUtil.isAllHankaku( value ) ) {
//					messageList.add		( MessageId.MESUCF0057 );
//					messageArgsList.add	( new String[] {} );
//				}

				// 新パスワードの入力許可文字のチェック
				if ( !TriStringUtils.matches( password, DesignSheetUtils.getPasswordAllowPattern() ) ) {
					messageList.add		( UmMessageId.UM003010I );
					messageArgsList.add	( new String[] {} );
				}

				if ( 0 < passMinLen && value.length() < passMinLen ) {
					messageList.add		( UmMessageId.UM001036E );
					messageArgsList.add	( new String[] { String.valueOf( passMinLen ) } );
				}

				if ( value.length() > passMaxLen ) {
					messageList.add		( UmMessageId.UM001008E );
					messageArgsList.add	( new String[] { String.valueOf( passMaxLen ) } );
				}
			}

			// 確認パスワード
			String temp = map.get(CONFIRM);
			if ( TriStringUtils.isEmpty( temp ) ) {
				messageList.add		( UmMessageId.UM001009E );
				messageArgsList.add	( new String[] {} );
			}
			if ( !TriStringUtils.isEquals( value, temp ) ) {
				messageList.add		( UmMessageId.UM001010E );
				messageArgsList.add	( new String[] {} );
			}

			// パスワード有効期間
			value = map.get( TERMID );
			if ( TriStringUtils.isEmpty( value ) ) {
				messageList.add		( UmMessageId.UM001035E );
				messageArgsList.add	( new String[] {} );
			}

			// メールアドレス
			value = map.get(MAIL);
			if (value.length() > MAX_MAIL_ADDRESS_LENGTH) {
				messageList.add		( UmMessageId.UM001011E );
				messageArgsList.add	( new String[] {} );
			}
			try {
				if( ! TriStringUtils.isEmpty( value ) ) {
					if( -1 == value.indexOf( "@" ) ) {
						throw new AddressException() ;
					}
					new InternetAddress( value ) ;
				}
			} catch ( AddressException e ) {
				messageList.add		( UmMessageId.UM001043E );
				messageArgsList.add	( new String[] {} );
			}
			// 部署名
			value = map.get(SECTION);
			if ( TriStringUtils.isEmpty( value ) ) {
				messageList.add		( UmMessageId.UM001012E );
				messageArgsList.add	( new String[] {} );
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ロール情報の必須チェック、形式チェックを行う。
	 * 必須項目に入力がない、または入力形式が正しくなければ例外をスローする。
	 * @param roleId ロールID
	 */

	public static void checkUcfInputRole( String roleId, String roleName , String oldRoleName , List<IRoleEntity> roleEntities , Mode mode ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			// ロールID
			// 自動採番に伴いコメントアウト
//			if ( StringAddonUtil.isNothing( roleId ) ) {
//				messageList.add		( MessageId.MESUCF0005 );
//				messageArgsList.add	( new String[] {} );
//			} else {
//				if (! StringAddonUtil.isDigits( roleId ) ) {
//					messageList.add		( MessageId.MESUCF0006 );
//					messageArgsList.add	( new String[] {} );
//				} else {
//					if (roleId.length() > MAX_ID_LENGTH) {
//						messageList.add		( MessageId.MESUCF0027 );
//						messageArgsList.add	( new String[] {} );
//					} else {
//						if (Integer.parseInt(roleId) == 0) {
//							messageList.add		( MessageId.MESUCF0006 );
//							messageArgsList.add	( new String[] {} );
//						}
//					}
//				}
//			}

			// ロール名
			if ( TriStringUtils.isEmpty( roleName ) ) {
				messageList.add		( UmMessageId.UM001016E );
				messageArgsList.add	( new String[] {} );
			} else if (roleName.length() > MAX_ROLE_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001017E );
				messageArgsList.add	( new String[] {} );
			} else {
				for( IRoleEntity roleEntity : roleEntities ) {
					if( roleName.equals( roleEntity.getRoleName() ) ) {
						boolean errFlg = false ;
						if( Mode.INSERT.equals( mode ) ) {
							errFlg = true ;
						} else if( Mode.UPDATE.equals( mode ) ) {
							if( null != oldRoleName && ! oldRoleName.equals( roleName ) ) {
								errFlg = true ;
							}
						}
						if( errFlg ) {
							messageList.add		( UmMessageId.UM001037E );
							messageArgsList.add	( new String[] {} );
							break ;
						}
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * グループ情報の必須チェック、形式チェックを行う。
	 * 必須項目に入力がない、または入力形式が正しくなければ例外をスローする。
	 * @param groupId グループID
	 */

	public static void checkUcfInputGroup( String groupId, String groupName , String oldGroupName , IGrpEntity[] grpEntities , Mode mode ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			// グループID
			// 自動採番に伴いコメントアウト
//			if ( StringAddonUtil.isNothing( groupId ) ) {
//				messageList.add		( MessageId.MESUCF0009 );
//				messageArgsList.add	( new String[] {} );
//			} else {
//				if (! StringAddonUtil.isDigits( groupId ) ) {
//					messageList.add		( MessageId.MESUCF0010 );
//					messageArgsList.add	( new String[] {} );
//				} else {
//					if (groupId.length() > MAX_ID_LENGTH) {
//						messageList.add		( MessageId.MESUCF0034 );
//						messageArgsList.add	( new String[] {} );
//					} else {
//						if (Integer.parseInt(groupId) == 0) {
//							messageList.add		( MessageId.MESUCF0010 );
//							messageArgsList.add	( new String[] {} );
//						}
//					}
//				}
//			}

			// グループ名
			if ( TriStringUtils.isEmpty( groupName ) ) {
				messageList.add		( UmMessageId.UM001022E );
				messageArgsList.add	( new String[] {} );
			} else if (groupName.length() > MAX_ROLE_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001023E );
				messageArgsList.add	( new String[] {} );
			} else {
				for( IGrpEntity grpEntity : grpEntities ) {
					if( groupName.equals( grpEntity.getGrpNm() ) ) {
						boolean errFlg = false ;
						if( Mode.INSERT.equals( mode ) ) {
							errFlg = true ;
						} else if( Mode.UPDATE.equals( mode ) ) {
							if( null != oldGroupName && ! oldGroupName.equals( groupName ) ) {
								errFlg = true ;
							}
						}
						if( errFlg ) {
							messageList.add		( UmMessageId.UM001038E );
							messageArgsList.add	( new String[] {} );
							break ;
						}
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * ユーザが管理者アカウント(admin)かどうかのチェックを行う。
	 *
	 * @param userId ユーザID
	 */

	public static void checkUcfAdminUser( String userId ) {

		if ( ADMIN.equals(userId) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001028E );
		}

	}

	/**
	 * ユーザがログインユーザかどうかのチェックを行う。
	 *
	 * @param userId ユーザID
	 */

	public static void checkUcfLoginUser( String userId, String loginUserId ) {

		if ( loginUserId.equals(userId) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001029E );
		}

	}
	/**
	 * Verify and validate category name before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 */
	public static void checkInputCategory(
			CategoryEditInputBean inputInfo,
			int countSameName ) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			String categoryName = inputInfo.getSubject();

			if ( TriStringUtils.isEmpty( categoryName ) ) {
				messageList.add		( UmMessageId.UM001054E );
				messageArgsList.add	( new String[] {} );
			} else if (categoryName.length() > MAX_CATEGORY_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001055E );
				messageArgsList.add	( new String[] {} );
			} else {
				if ( 0 < countSameName ) {
					messageList.add		( UmMessageId.UM001056E );
					messageArgsList.add	( new String[] {} );
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Verify and validate milestone name before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 */
	public static void checkInputMilestone(
			MilestoneEditInputBean inputInfo,
			int countSameName) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			String milestoneName = inputInfo.getSubject();

			if ( TriStringUtils.isEmpty( milestoneName ) ) {
				messageList.add		( UmMessageId.UM001058E );
				messageArgsList.add	( new String[] {} );
			} else if (milestoneName.length() > MAX_MILESTONE_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001059E );
				messageArgsList.add	( new String[] {} );
			} else {
				if(  0 < countSameName ) {
					messageList.add		( UmMessageId.UM001060E );
					messageArgsList.add	( new String[] {} );
				}
			}

			boolean isDateEnabled = true;
			String startDate = inputInfo.getStartDate();
			if ( TriStringUtils.isNotEmpty( startDate ) ) {
				if ( !TriDateUtils.checkYMD(startDate) ) {
					isDateEnabled = false;
					messageList.add		( UmMessageId.UM001061E );
					messageArgsList.add	( new String[] {} );
				}
			}

			String dueDate = inputInfo.getDueDate();
			if ( TriStringUtils.isNotEmpty( dueDate ) ) {
				if ( !TriDateUtils.checkYMD( dueDate ) ) {
					isDateEnabled = false;
					messageList.add		( UmMessageId.UM001062E );
					messageArgsList.add	( new String[] {} );
				}
			}

			if ( isDateEnabled
					&& TriStringUtils.isNotEmpty( startDate ) && TriStringUtils.isNotEmpty( dueDate ) ) {
				if ( !TriDateUtils.before(startDate, dueDate) ) {
					messageList.add		( UmMessageId.UM001063E );
					messageArgsList.add	( new String[] {} );
				}
			}


		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Verify and validate department name, selected users before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 */
	public static void checkInputDepartment(
			DepartmentEditInputBean inputInfo,
			int countSameName, IUmFinderSupport support) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			String deptName = inputInfo.getDeptNm();

			if ( TriStringUtils.isEmpty( deptName ) ) {
				messageList.add		( UmMessageId.UM001013E );
				messageArgsList.add	( new String[] {} );
			} else if (deptName.length() > MAX_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001014E );
				messageArgsList.add	( new String[] {} );
			} else {
				if(  0 < countSameName ) {
					messageList.add		( UmMessageId.UM001044E );
					messageArgsList.add	( new String[] {} );
				}

				if (TriStringUtils.isNotEmpty(inputInfo.getUserIdSet())) {
					for (String userId : inputInfo.getUserIdSet()) {
						IUserEntity entity = support.findUserByUserId(userId);
						if (null == entity) {
							messageList.add		( UmMessageId.UM001064E );
							messageArgsList.add	( new String[] {userId} );
						}
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Verify and validate group name, selected users, selected roles before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 */
	public static void checkInputGroup(
			GroupEditInputBean inputInfo,
			int countSameName, IUmFinderSupport support) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			String grpNm = inputInfo.getGroupNm();

			if ( TriStringUtils.isEmpty( grpNm ) ) {
				messageList.add		( UmMessageId.UM001022E );
				messageArgsList.add	( new String[] {} );
			} else if (grpNm.length() > MAX_ROLE_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001023E );
				messageArgsList.add	( new String[] {} );
			} else {
				if(  0 < countSameName ) {
					messageList.add		( UmMessageId.UM001038E );
					messageArgsList.add	( new String[] {} );
				}

				if (TriStringUtils.isNotEmpty(inputInfo.getUserIdSet())) {
					for (String userId : inputInfo.getUserIdSet()) {
						IUserEntity entity = support.findUserByUserId(userId);
						if (null == entity) {
							messageList.add		( UmMessageId.UM001064E );
							messageArgsList.add	( new String[] {userId} );
						}
					}
				}

				if (TriStringUtils.isNotEmpty(inputInfo.getRoleIdSet())) {
					for (String roleId : inputInfo.getRoleIdSet()) {
						IRoleEntity entity = support.findRoleByRoleId(roleId);
						if (null == entity) {
							messageList.add		( UmMessageId.UM001065E );
							messageArgsList.add	( new String[] {roleId} );
						}
					}
				}
			}


		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	public static void checkAdminGroup(String groupId,  Set<String> userIdSet, Set<String> roleIdSet) {
		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();
		if (ADMIN_GROUP_ID.equals(groupId)) {

			if(!userIdSet.contains(ADMIN)) {
				messageList.add(UmMessageId.UM001103E);
				messageArgsList.add	( new String[] {} );
			}

			if(!roleIdSet.contains(ADMIN_ROLE_ID)) {
				messageList.add(UmMessageId.UM001104E);
				messageArgsList.add	( new String[] {} );
			}
		}
		if ( 0 != messageList.size() )
			throw new ContinuableBusinessException( messageList, messageArgsList );
	}

	public static void checkAdminRole(String roleId) {
		if( ADMIN_ROLE_ID.equals(roleId) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001105E );
		}
	}

	/**
	 * Verify and validate role name, selected groups before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 */
	public static void checkInputRole(
			RoleEditInputBean inputInfo,
			int countSameName, IUmFinderSupport support) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			String roleNm = inputInfo.getRoleNm();

			if ( TriStringUtils.isEmpty( roleNm ) ) {
				messageList.add		( UmMessageId.UM001016E );
				messageArgsList.add	( new String[] {} );
			} else if (roleNm.length() > MAX_ROLE_NAME_LENGTH) {
				messageList.add		( UmMessageId.UM001017E );
				messageArgsList.add	( new String[] {} );
			} else {
				if(  0 < countSameName ) {
					messageList.add		( UmMessageId.UM001037E );
					messageArgsList.add	( new String[] {} );
				}

				if (TriStringUtils.isNotEmpty(inputInfo.getGroupIdSet())) {
					for (String grpId : inputInfo.getGroupIdSet()) {
						IGrpEntity entity = support.findGroupById(grpId);
						if (null == entity) {
							messageList.add		( UmMessageId.UM001066E );
							messageArgsList.add	( new String[] {grpId} );
						}
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	public static void checkUserId(String userId, List<IMessageId> messageList, List<String[]> messageArgsList) {
		if ( TriStringUtils.isEmpty( userId ) ) {
			messageList.add		( UmMessageId.UM001000E );
			messageArgsList.add	( new String[] {} );
		} else {
			if (userId.length() > MAX_USER_ID_LENGTH) {
				messageList.add		( UmMessageId.UM001004E );
				messageArgsList.add	( new String[] {} );
			}
			if ( !TriStringUtils.isAllHankaku( userId ) || TriStringUtils.containsHanKana( userId )) {
				messageList.add		( UmMessageId.UM001034E );
				messageArgsList.add	( new String[] {} );
			}
		}
	}

	private static void checkUserName(String userName, int count, List<IMessageId> messageList, List<String[]> messageArgsList) {
		checkUserName(userName, count, messageList, messageArgsList, true);
	}

	public static void checkUserName(String userName, int count, List<IMessageId> messageList, List<String[]> messageArgsList, boolean checkEmpty) {
		if (TriStringUtils.isEmpty(userName)) {
			if (checkEmpty) {
				messageList.add(UmMessageId.UM001005E);
				messageArgsList.add(new String[]{});
			}
		} else {
			if (userName.length() > UmItemChkUtils.MAX_NAME_LENGTH) {
				messageList.add(UmMessageId.UM001006E);
				messageArgsList.add(new String[]{});
			}
			if (0 < count) {
				messageList.add(UmMessageId.UM001045E);
				messageArgsList.add(new String[]{});
			}
			if (TriStringUtils.containsHanKana(userName)) {
				messageList.add(UmMessageId.UM001046E);
				messageArgsList.add(new String[]{});
			}
		}
	}

	public static void checkUserPassword(String password, String confirmPassword,
			String passwordExpirationTerm, List<IMessageId> messageList,
			List<String[]> messageArgsList) {

		String minPasswordLength = DesignSheetUtils.getPasswordMinimumLength();
		String maxPasswordLength = DesignSheetUtils.getPasswordMaxLength();
		int passMinLen = 0;
		int passMaxLen = 0;
		if ( NumberUtils.isNumber( minPasswordLength )) {
			passMinLen = Integer.parseInt( minPasswordLength );
		}
		if ( NumberUtils.isNumber( maxPasswordLength )) {
			passMaxLen = Integer.parseInt( maxPasswordLength );
		}
		if ( TriStringUtils.isEmpty( password ) ) {
			messageList.add		( UmMessageId.UM001007E );
			messageArgsList.add	( new String[] {} );

		} else {
			if ( !TriStringUtils.matches( password, DesignSheetUtils.getPasswordAllowPattern() ) ) {
				messageList.add		( UmMessageId.UM003010I );
				messageArgsList.add	( new String[] {} );
			}

			if ( 0 < passMinLen && password.length() < passMinLen ) {
				messageList.add		( UmMessageId.UM001036E );
				messageArgsList.add	( new String[] { String.valueOf( passMinLen ) } );
			}

			if ( password.length() > passMaxLen ) {
				messageList.add		( UmMessageId.UM001008E );
				messageArgsList.add	( new String[] { String.valueOf( passMaxLen ) } );
			}
		}

		if ( TriStringUtils.isEmpty( confirmPassword ) ) {
			messageList.add		( UmMessageId.UM001009E );
			messageArgsList.add	( new String[] {} );
		}
		if ( !TriStringUtils.isEquals( password, confirmPassword ) ) {
			messageList.add		( UmMessageId.UM001010E );
			messageArgsList.add	( new String[] {} );
		}

		if ( TriStringUtils.isEmpty( passwordExpirationTerm ) ) {
			messageList.add		( UmMessageId.UM001035E );
			messageArgsList.add	( new String[] {} );
		}
	}

	public static void checkUserEmailAddress(String mailAddress,
			List<IMessageId> messageList, List<String[]> messageArgsList) {
		if (TriStringUtils.isEmpty(mailAddress)) {
			return;
		}

		if (mailAddress.length() > MAX_MAIL_ADDRESS_LENGTH) {
			messageList.add		( UmMessageId.UM001011E );
			messageArgsList.add	( new String[] {} );
		}
		try {
			if( ! TriStringUtils.isEmpty( mailAddress ) ) {
				if( -1 == mailAddress.indexOf( "@" ) ) {
					throw new AddressException() ;
				}
				new InternetAddress( mailAddress ) ;
			}
		} catch ( AddressException e ) {
			messageList.add		( UmMessageId.UM001043E );
			messageArgsList.add	( new String[] {} );
		}
	}

	public static void checkUserLanguageAndTimeZone(String language, String timeZone,
			List<IMessageId> messageList, List<String[]> messageArgsList) {

		if (TriStringUtils.isEmpty(language)) {
			messageList.add		( UmMessageId.UM001069E );
			messageArgsList.add	( new String[] {} );
		}

		if (TriStringUtils.isEmpty(timeZone)) {
			messageList.add		( UmMessageId.UM001070E );
			messageArgsList.add	( new String[] {} );
		}
	}

	public static void checkUserIcon(String iconPath,
			List<IMessageId> messageList, List<String[]> messageArgsList) {

		if (TriStringUtils.isEmpty(iconPath)) {
			messageList.add		( UmMessageId.UM001068E );
			messageArgsList.add	( new String[] {} );
		}
	}

	/**
	 * Verify and validate user input information before inserting or updating
	 * @param inputInfo input information
	 * @param countSameName
	 * @param support
	 */
	public static void checkInputUser(
			UserEditInputBean inputInfo,
			int countSameName, IUmFinderSupport support) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {
			checkUserId(inputInfo.getUserId(), messageList, messageArgsList);

			checkUserName(inputInfo.getUserNm(), countSameName, messageList, messageArgsList);

			checkUserPassword(inputInfo.getPassword(), inputInfo.getConfirmPassword(),
					inputInfo.getPasswordExpiration(), messageList, messageArgsList);

			checkUserEmailAddress(inputInfo.getMailAddress(), messageList, messageArgsList);

			checkUserLanguageAndTimeZone(inputInfo.getLanguage(), inputInfo.getTimeZone(),
					messageList, messageArgsList);

			checkUserIcon(inputInfo.getIconPath(), messageList, messageArgsList);

			String deptId = inputInfo.getDeptId();
			if (TriStringUtils.isNotEmpty(deptId)) {
				IDeptEntity deptEntity = support.findDeptByDeptId(deptId);
				if (null == deptEntity) {
					messageList.add		( UmMessageId.UM001067E );
					messageArgsList.add	( new String[] {deptId} );
				}
			}
			if (TriStringUtils.isNotEmpty(inputInfo.getGroupIdSet())) {
				for (String grpId : inputInfo.getGroupIdSet()) {
					IGrpEntity entity = support.findGroupById(grpId);
					if (null == entity) {
						messageList.add		( UmMessageId.UM001066E );
						messageArgsList.add	( new String[] {grpId} );
					}
				}
			}

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}

	/**
	 * Verify and validate user input information before inserting or updating
	 * @param user name
	 * @param mail address
	 * @param icon path
	 * @param count user by the same name
	 */
	public static void checkUserPersonalSetting(String userNm, String mailAddrr,
			String iconPath, int countSameName) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]>	messageArgsList	= new ArrayList<String[]>();

		try {

			checkUserName(userNm, countSameName, messageList, messageArgsList);

			checkUserEmailAddress(mailAddrr, messageList, messageArgsList);

			checkUserIcon(iconPath, messageList, messageArgsList);

		} finally {
			if ( 0 != messageList.size() )
				throw new ContinuableBusinessException( messageList, messageArgsList );
		}
	}
}
