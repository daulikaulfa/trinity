package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;

public class FlowUcfUserEntryBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ユーザID **/
	private String inUserId   		  = null;
	/** ユーザ名 **/
	private String inUserName 		  = null;
	/** パスワード **/
	private String inPassword 		  = null;
	/** パスワード有効期限 **/
	private String inPasswordEffectiveTerm = null;
	/** 確認用パスワード **/
	private String inRepassword	  = null;
	/** メールアドレス **/
	private String inMailAddress 	  = null;
	/** 部署ID **/
	private String inSectionId       = null;
	/** 選択グループID **/
	private List<String> inGroupList;
	/** 選択ロールID **/
	private List<String> inRoleList;
	/**
	 * 一覧情報
	 */
	private List<UserListViewBean> userViewBeanList;

	private List<GroupListViewBean> groupViewBeanList;
	
	private List<RoleListViewBean> roleViewBeanList;
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public UserListViewBean newListViewBean() {
		UserListViewBean bean = new FlowUcfUserListServiceBean().newListViewBean();
		return bean;
	}
	
	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<UserListViewBean> getViewBeanList() {
		return userViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<UserListViewBean> list) {
		this.userViewBeanList = list;
	}

	public String getInMailAddress() {
		return inMailAddress;
	}

	public void setInMailAddress(String inMailAddress) {
		this.inMailAddress = inMailAddress;
	}

	public String getInPassword() {
		return inPassword;
	}

	public void setInPassword(String inPassword) {
		this.inPassword = inPassword;
	}
	
	public String getInPasswordEffectiveTerm() {
		return inPasswordEffectiveTerm;
	}

	public void setInPasswordEffectiveTerm( String inPasswordEffectiveTerm ) {
		this.inPasswordEffectiveTerm = inPasswordEffectiveTerm;
	}

	public String getInRepassword() {
		return inRepassword;
	}

	public void setInRepassword(String inRepassword) {
		this.inRepassword = inRepassword;
	}

	public String getInSectionId() {
		return inSectionId;
	}

	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}

	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}

	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String inUserName) {
		this.inUserName = inUserName;
	}

	public List<UserListViewBean> getUserViewBeanList() {
		return userViewBeanList;
	}

	public void setUserViewBeanList(List<UserListViewBean> userViewBeanList) {
		this.userViewBeanList = userViewBeanList;
	}
	
	public List<String> getInGroupList() {
		return inGroupList;
	}

	public void setInGroupList(List<String> inGroupList) {
		this.inGroupList = inGroupList;
	}
	
	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}

	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	
	public List<String> getInRoleList() {
		return inRoleList;
	}

	public void setInRoleList(List<String> inRoleList) {
		this.inRoleList = inRoleList;
	}
	
	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}

	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}
	
	public class RoleListViewBean {
		
		/** ロールID **/
		private String roleId   		  = null;
		/** ロール名 **/
		private String roleName 		  = null;
		public String getRoleId() {
			return roleId;
		}
		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
	}

}
