package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * FlowUcfActionListServiceイベントのサービスClass
 * <br>
 * <p>
 * ユーザ一覧画面の表示情報を設定する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FlowUcfRoleListService implements IDomain<FlowUcfRoleListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfRoleListServiceBean> execute( IServiceDto<FlowUcfRoleListServiceBean> serviceDto ) {

		FlowUcfRoleListServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			List<IRoleEntity> entitys = umFinderSupport.findAllRole();
			List<RoleListViewBean> list = new ArrayList<RoleListViewBean>();

			for ( IRoleEntity entity : entitys ) {
				RoleListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setRoleId(entity.getRoleId());
				viewBean.setRoleName(entity.getRoleName());

				list.add( viewBean );
			}

			paramBean.setViewBeanList(list);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005032S, e );
		}
	}

}