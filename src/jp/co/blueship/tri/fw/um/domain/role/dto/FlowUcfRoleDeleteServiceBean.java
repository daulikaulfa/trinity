package jp.co.blueship.tri.fw.um.domain.role.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfRoleDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	private String inRoleId;
	private String RoleId;
	private String RoleName;
	private String GroupId;
	private String GroupName;
	private List<String> GroupSelected = new ArrayList<String>();
	
	public String getInRoleId() {
		return inRoleId;
	}
	public void setInRoleId(String inRoleId) {
		this.inRoleId = inRoleId;
	}
	public String getRoleId() {
		return RoleId;
	}
	public void setRoleId(String roleId) {
		RoleId = roleId;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public String getGroupId() {
		return GroupId;
	}
	public void setGroupId(String groupId) {
		GroupId = groupId;
	}	
	public String getGroupName() {
		return GroupName;
	}
	public void setGroupName(String groupName) {
		GroupName = groupName;
	}
	public List<String> getGroupSelected() {
		return GroupSelected;
	}
	public void setGroupSelected(List<String> groupSelected) {
		this.GroupSelected = groupSelected;
	}
}
