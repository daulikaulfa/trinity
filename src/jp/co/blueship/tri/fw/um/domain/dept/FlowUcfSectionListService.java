package jp.co.blueship.tri.fw.um.domain.dept;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * FlowUcfSectionListServiceイベントのサービスClass
 * <br>
 * <p>
 * 部署一覧画面の表示情報を設定する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FlowUcfSectionListService implements IDomain<FlowUcfSectionListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionListServiceBean> execute( IServiceDto<FlowUcfSectionListServiceBean> serviceDto ) {

		FlowUcfSectionListServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S);
			}
			paramBean.setViewBeanList( getDeptList( umFinderSupport.findAllDept(), paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005006S, e );
		}
	}

	private List<SectionListViewBean> getDeptList( List<IDeptEntity> entitys, FlowUcfSectionListServiceBean paramBean ){

		List<SectionListViewBean> list = new ArrayList<SectionListViewBean>();
		for ( IDeptEntity entity : entitys ) {

			SectionListViewBean viewBean = paramBean.newListViewBean();
			viewBean.setSectionId(entity.getDeptId() );
			viewBean.setSectionName(entity.getDeptNm() );
			list.add( viewBean );
		}
		return list;
	}

}
