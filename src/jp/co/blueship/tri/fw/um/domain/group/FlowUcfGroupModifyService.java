package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupModifyServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupModifyService implements IDomain<FlowUcfGroupModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupModifyServiceBean> execute( IServiceDto<FlowUcfGroupModifyServiceBean> serviceDto ) {

		FlowUcfGroupModifyServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			if (paramBean.getGroupName() == null) {

				IGrpEntity entity = umFinderSupport.findGroupById( paramBean.getInGroupId() );
				paramBean.setGroupId(entity.getGrpId().toString()) ;
				paramBean.setGroupName(entity.getGrpNm()) ;
			} else {
				paramBean.setGroupId(paramBean.getInGroupId());
			}

			List<IGrpRoleLnkEntity> grpRoleEntityList = umFinderSupport.findGrpRoleLnkByGrpId( paramBean.getInGroupId() );
			List<String> groupRoleList = new ArrayList<String>();

			for ( IGrpRoleLnkEntity entity : grpRoleEntityList ) {
				groupRoleList.add(entity.getRoleId());
			}

			paramBean.setRoleSelected(groupRoleList);

			List<IRoleEntity> roleEntityList = umFinderSupport.findAllRole();
			List<RoleListViewBean> roleList = new ArrayList<RoleListViewBean>();

			for ( IRoleEntity entity : roleEntityList ) {
				RoleListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setRoleId(entity.getRoleId());
				viewBean.setRoleName(entity.getRoleName());

				roleList.add( viewBean );
			}

			paramBean.setRoleViewBeanList(roleList);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005015S, e );
		}
	}


}
