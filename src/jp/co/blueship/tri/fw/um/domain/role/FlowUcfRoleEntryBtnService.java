package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.role.RoleNumberingDao;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfRoleEntryBtnService implements IDomain<FlowUcfRoleEntryBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IRoleDao roleDao;
	public void setRoleDao( IRoleDao roleDao ) {
		this.roleDao = roleDao;
	}

	private IRoleSvcCtgLnkDao roleSvcCtgLnkDao;
	public void setRoleSvcCtgLnkDao( IRoleSvcCtgLnkDao roleSvcCtgLnkDao ) {
		this.roleSvcCtgLnkDao = roleSvcCtgLnkDao;
	}

	private RoleNumberingDao roleNumberingDao;
	public void setRoleNumberingDao( RoleNumberingDao roleNumberingDao ) {
		this.roleNumberingDao = roleNumberingDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfRoleEntryBtnServiceBean> execute( IServiceDto<FlowUcfRoleEntryBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfRoleEntryBtnServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();
			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			// ロールID採番
			// 既存のデータを考慮し、重複がないように採番する
			String roleId = null;
			for (;;) {
				roleId = roleNumberingDao.nextval();
				if ( umFinderSupport.checkDuplicationByNumberingRoleId( roleId ) ) {
					break;
				}
			}

			paramBean.setInRoleId	( roleId );

			// 入力チェック
			List<IRoleEntity> roleEntities = umFinderSupport.findAllRole() ;
			UmItemChkUtils.checkUcfInputRole( paramBean.getInRoleId(), paramBean.getInRoleName() , null , roleEntities , Mode.INSERT );

			// 画面の値をroleEntityセットしてRoleDaoに登録する
			insertRoleDao(paramBean.getInRoleId(), paramBean.getInRoleName());

			if ( paramBean.getInActionList() == null ) {
				;// 編集で削除できるように修正
				//throw new ContinuableBusinessException( MessageId.MESUCF0007, new String[] {} );
			} else {
				for (String svcId : paramBean.getInActionList()) {
					insertRoleSvcCtgLnkDao(paramBean.getInRoleId(), svcId);
				}
			}

			// DB全件検索
			List<IRoleEntity> entitys = umFinderSupport.findAllRole() ;

			// DBに登録されている内容をListに格納する
			List<RoleListViewBean> roleList = new ArrayList<RoleListViewBean>();

			// インスタンス(器)を作成し登録されたデータの件数分データを格納する
			for ( IRoleEntity entity : entitys ) {
				RoleListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setRoleId	( entity.getRoleId() );
				viewBean.setRoleName( entity.getRoleName() );

				roleList.add( viewBean );

				paramBean.setViewBeanList( roleList );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005030S, e );
		}
	}

	/**
	 * ロールIDとロール名をRoleDaoに登録します。
	 * @param roleId
	 * @param roleNm
	 */
	private void insertRoleDao(String roleId, String roleNm ){

		IRoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleId( roleId );
		roleEntity.setRoleName( roleNm );
		// ここでDBに値を登録
		roleDao.insert( roleEntity );
	}
	/**
	 * ロールIDとサービスカテゴリIDをroleSvcCtgLnkDaoに登録します。
	 * @param roleId
	 * @param svcCtgId
	 */
	private void insertRoleSvcCtgLnkDao(String roleId, String svcCtgId){

		IRoleSvcCtgLnkEntity roleSvcCtgEntity = new RoleSvcCtgLnkEntity();
		roleSvcCtgEntity.setRoleId( roleId );
		roleSvcCtgEntity.setSvcCtgId( svcCtgId );
		roleSvcCtgLnkDao.insert( roleSvcCtgEntity );
	}

}
