package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.PasswordEffectiveTermViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.SectionViewBean;

public class FlowUcfUserModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** ユーザID **/
	private String inUserId	= null;
	/** ユーザ名 **/
	private String inUserName	= null;
	/** パスワード **/
	private String password	= null;
	/** 確認用パスワード **/
	private String rePassword	= null;
	/** メールアドレス **/
	private String mailAddress	= null;
	/** 部署ID **/
	private String sectionId	= null;
	/** パスワード有効期間 **/
	private String termId		= null;
	/** パスワード有効期限 **/
	private String termLimit	= null;
	
	
	private List<String> groupSelected;
	private List<GroupListViewBean> groupViewBeanList;
	
	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}

	public String getMailAddress() {
		return mailAddress;
	}

	public void setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRepassword() {
		return rePassword;
	}

	public void setRepassword(String rePassword) {
		this.rePassword = rePassword;
	}

	public String getSectionId() {
		return sectionId;
	}

	public void setSectionId(String sectionId) {
		this.sectionId = sectionId;
	}

	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String userName) {
		inUserName = userName;
	}

	public List<String> getGroupSelected() {
		return groupSelected;
	}
	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	}
	
	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	public GroupListViewBean newListViewBean() {
		GroupListViewBean bean = new FlowUcfUserEntryServiceBean().newGroupListViewBean();
		
		return bean;
	}
	
	/** パスワード有効期間 */
	private List<PasswordEffectiveTermViewBean> termViewBeanList;

	public List<PasswordEffectiveTermViewBean> getTermViewBeanList() {
		return termViewBeanList;
	}

	public void setTermViewBeanList( List<PasswordEffectiveTermViewBean> termViewBeanList ) {
		this.termViewBeanList = termViewBeanList;
	}
	
	public PasswordEffectiveTermViewBean newTermViewBean() {
		return new FlowUcfUserEntryServiceBean().newTermViewBean();
	}
	
	/** 部署 */
	private List<SectionViewBean> sectionViewBeanList;

	public List<SectionViewBean> getSectionViewBeanList() {
		return sectionViewBeanList;
	}

	public void setSectionViewBeanList( List<SectionViewBean> sectionViewBeanList ) {
		this.sectionViewBeanList = sectionViewBeanList;
	}
	
	public SectionViewBean newSectionViewBean() {
		return new FlowUcfUserEntryServiceBean().newSectionViewBean();
	}
	
	public String getTermId() {
		return termId;
	}

	public void setTermId( String termId ) {
		this.termId = termId;
	}
	
	public String getTermLimit() {
		return termLimit;
	}
	public void setTermLimit( String termLimit ) {
		this.termLimit = termLimit;
	}
	
}
