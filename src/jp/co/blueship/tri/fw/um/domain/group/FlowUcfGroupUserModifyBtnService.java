package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupUserModifyBtnService implements IDomain<FlowUcfGroupUserModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpUserLnkDao grpUserLnkDao;
	public void setGrpUserLnkDao( IGrpUserLnkDao grpUserLnkDao ) {
		this.grpUserLnkDao = grpUserLnkDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupUserModifyBtnServiceBean> execute( IServiceDto<FlowUcfGroupUserModifyBtnServiceBean> serviceDto ) {

		FlowUcfGroupUserModifyBtnServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			String value = paramBean.getInGroupId();
			if (value == null || value.equals(""))
				throw new ContinuableBusinessException( UmMessageId.UM001003E );

			List<String> userList = new ArrayList<String>();
			List<String> gRoleList = new ArrayList<String>();

			//画面のグループIDからグループに所属するユーザを取得する
			List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInGroupId() );
			//グループに所属するユーザのユーザIDをリスト（userList）に格納する
			for (IGrpUserLnkEntity gEntity : grpUserLnkEntityList){
				userList.add(gEntity.getUserId());
			}
			//グループに設定されているロールを取得する
			List<IGrpRoleLnkEntity> grpRoleLnkEntityList = umFinderSupport.findGrpRoleLnkByGrpId( paramBean.getInGroupId() );
			for (IGrpRoleLnkEntity groupRoleEntity : grpRoleLnkEntityList){
				gRoleList.add(groupRoleEntity.getRoleId());
			}
			//グループに設定されているユーザIDとロールで、ユーザロールテーブルから削除する
			this.deleteGroupRoleId( userList, gRoleList );

			//チェックボックスにチェックがない場合
			if ( paramBean.getInUserList() == null ){
				//リスト（userList）のユーザIDに紐づくグループのグループIDを、グループユーザテーブルから削除する
				for (int userCount = 0; userCount < userList.size(); userCount++) {
					String userId = userList.get(userCount);
					//リスト（userList）のユーザIDで、ユーザロールテーブルから削除する
					deleteGrpUserLnkDao(paramBean.getInGroupId(), userId);

					this.setGroupRoleId( userId );
				}

			//チェックボックスにチェックがある場合
			}else{
				//選択したグループIDに紐づくユーザIDを、グループユーザテーブルから削除する
				deleteGrpUserLnkDao( paramBean.getInGroupId() );

				IGrpUserLnkEntity groupUserEntity = new GrpUserLnkEntity();

				for (String userId : paramBean.getInUserList()){

					groupUserEntity.setGrpId( paramBean.getInGroupId() );
					groupUserEntity.setUserId( userId );
					grpUserLnkDao.insert( groupUserEntity );
					for (int roleCount = 0; roleCount < gRoleList.size(); roleCount++) {
					String rId = gRoleList.get(roleCount);

					this.insertRoleId( rId, userId ) ;
					}
				}

				for (int userCount = 0; userCount < userList.size(); userCount++) {
					String uId = userList.get(userCount);
					this.setGroupRoleId( uId );
				}
			}

			paramBean.setInUserList(paramBean.getInUserList());
			paramBean.setInGroupId(paramBean.getInGroupId());
			paramBean.setInGroupName(paramBean.getInGroupName());

			// DB全件検索
			List<IGrpEntity> entitys = umFinderSupport.findAllGroup();
			// DBに登録されている内容をListに格納する
			List<GroupListViewBean> list = new ArrayList<GroupListViewBean>();

			// インスタンス(器)を作成し登録されたデータの件数分データを格納する
			for ( IGrpEntity entity : entitys ) {
				GroupListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setGroupId(entity.getGrpId());
				viewBean.setGroupName(entity.getGrpNm());

				list.add( viewBean );
			}

			// for文でlistに格納したBeanをparamBeanに格納する
			paramBean.setViewBeanList(list);

			List<IUserEntity> userEntityList = umFinderSupport.findAllUser();
			List<UserListViewBean> userViewList = new ArrayList<UserListViewBean>();

			for ( IUserEntity entity : userEntityList ) {
				UserListViewBean viewBean = paramBean.newUserListViewBean();

				viewBean.setUserId(entity.getUserId());
				viewBean.setUserName(entity.getUserNm());

				userViewList.add( viewBean );
			}

			paramBean.setUserViewBeanList(userViewList);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005020S, e );
		}
	}


	/**
	 * //チェックの入っていないユーザが他に所属しているグループのロールを取得する
	 * @param userId
	 */
	private void setGroupRoleId( String userId ){

		List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByUserId(userId);
		//ユーザが他に所属するグループのグループIDを、リスト（groupList）に格納する
		for ( IGrpUserLnkEntity gUserEntity : grpUserLnkEntityList ){
			List<IGrpRoleLnkEntity> grpRoleLnkEntityList = umFinderSupport.findGrpRoleLnkByGrpId( gUserEntity.getGrpId() );
			//ユーザが他に所属するグループに設定されているロールのロールIDを、リスト（roleList）に格納する
			for ( IGrpRoleLnkEntity gRoleEntity : grpRoleLnkEntityList ){
			//リスト（roleList）のロールIDをユーザロールテーブルに格納する
				this.insertRoleId( gRoleEntity.getRoleId() , userId) ;
			}
		}
	}

	/**
	 * //取得したロールID、ユーザIDをユーザロールテーブルに登録する
	 * @param roleId
	 * @param userId
	 */
	private void insertRoleId( String roleId , String userId ) {

		IUserRoleLnkEntity userRoleEntity = new UserRoleLnkEntity();
		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setRoleId(roleId);
		condition.setUserId(userId);
		userRoleLnkDao.delete( condition.getCondition() );

		userRoleEntity.setUserId(userId);
		userRoleEntity.setRoleId(roleId);
		userRoleLnkDao.insert(userRoleEntity);
	}

	/**
	 * //選択したグループに設定されているロールを、ユーザロールテーブルから削除
	 * @param userId
	 */
	private void deleteGroupRoleId( List<String> userList, List<String> gRoleList ) {

		for (int userCount = 0; userCount < userList.size(); userCount++) {
			String userId = userList.get(userCount);
			for (int roleCount = 0; roleCount < gRoleList.size(); roleCount++) {
				String roleId = gRoleList.get(roleCount);
				UserRoleLnkCondition condition = new UserRoleLnkCondition();
				condition.setRoleId(roleId);
				condition.setUserId(userId);
				userRoleLnkDao.delete( condition.getCondition() );
			}
		}
	}

	/**
	 *
	 * @param grpId
	 * @param userId
	 */
	private void deleteGrpUserLnkDao(String grpId, String userId ) {

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId( grpId );
		condition.setUserId( userId );
		grpUserLnkDao.delete( condition.getCondition() );
	}

	/**
	 *
	 * @param grpId
	 */
	private void deleteGrpUserLnkDao(String grpId ) {
		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(grpId);
		grpUserLnkDao.delete( condition.getCondition() );
	}
}