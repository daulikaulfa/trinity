package jp.co.blueship.tri.fw.um.domain.role.dto;


import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfRoleEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	private String inRoleId;
	private String inRoleName;
	private List<ActionListViewBean> actionViewBeanList = new ArrayList<ActionListViewBean>();
	
	public String getInRoleId() {
		return inRoleId;
	}

	public void setInRoleId(String inRoleId) {
		this.inRoleId = inRoleId;
	}

	public String getInRoleName() {
		return inRoleName;
	}

	public void setInRoleName(String inRoleName) {
		this.inRoleName = inRoleName;
	}

	public class ActionListViewBean {
		
		/** アクションID **/
		private String actionId   		  = null;
		/** アクション名 **/
		private String actionName 		  = null;
		public String getActionId() {
			return actionId;
		}
		public void setActionId(String actionId) {
			this.actionId = actionId;
		}
		public String getActionName() {
			return actionName;
		}
		public void setActionName(String actionName) {
			this.actionName = actionName;
		}
		
	}

	public List<ActionListViewBean> getActionViewBeanList() {
		return actionViewBeanList;
	}

	public void setActionViewBeanList(List<ActionListViewBean> actionViewBeanList) {
		this.actionViewBeanList = actionViewBeanList;
	}

	public ActionListViewBean newActionListViewBean() {
		ActionListViewBean bean = new ActionListViewBean();
		
		return bean;
	}
	
}
