package jp.co.blueship.tri.fw.um.domain.dept.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;

public class FlowUcfSectionDeleteBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	private String inSectionId;
	
	/**
	 * 一覧情報
	 */
	private List<SectionListViewBean> sectionViewBeanList = new ArrayList<SectionListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public SectionListViewBean newListViewBean() {
		SectionListViewBean bean = new FlowUcfSectionListServiceBean().newListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<SectionListViewBean> getViewBeanList() {
		return sectionViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<SectionListViewBean> list) {
		this.sectionViewBeanList = list;
	}

	public String getInSectionId() {
		return inSectionId;
	}

	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}
			
}
