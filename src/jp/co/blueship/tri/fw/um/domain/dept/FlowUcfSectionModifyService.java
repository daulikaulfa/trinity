package jp.co.blueship.tri.fw.um.domain.dept;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionModifyServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfSectionModifyService implements IDomain<FlowUcfSectionModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionModifyServiceBean> execute( IServiceDto<FlowUcfSectionModifyServiceBean> serviceDto ) {

		FlowUcfSectionModifyServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			if (paramBean.getSectionName() == null) {
				IDeptEntity entity = umFinderSupport.findDeptByDeptId( paramBean.getInSectionId() );
				paramBean.setSectionId( entity.getDeptId() ) ;
				paramBean.setSectionName( entity.getDeptNm() ) ;
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005008S, e );
		}
	}

}
