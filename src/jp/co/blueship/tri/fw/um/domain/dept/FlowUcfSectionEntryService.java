package jp.co.blueship.tri.fw.um.domain.dept;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionEntryServiceBean;

public class FlowUcfSectionEntryService implements IDomain<FlowUcfSectionEntryServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowUcfSectionEntryServiceBean> execute( IServiceDto<FlowUcfSectionEntryServiceBean> serviceDto ) {

		FlowUcfSectionEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005005S, e );
		}
	}

}
