package jp.co.blueship.tri.fw.um.domain.role.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfRoleListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 一覧情報
	 */
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RoleListViewBean newListViewBean() {
		RoleListViewBean bean = new RoleListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<RoleListViewBean> getViewBeanList() {
		return roleViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<RoleListViewBean> list) {
		this.roleViewBeanList = list;
	}
	
	public class RoleListViewBean {
		
		/** ロールID **/
		private String roleId   		  = null;
		/** ロール名 **/
		private String roleName 		  = null;
		public String getRoleId() {
			return roleId;
		}
		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
		
	}
}
