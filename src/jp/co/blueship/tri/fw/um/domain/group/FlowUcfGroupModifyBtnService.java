package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupModifyBtnService implements IDomain<FlowUcfGroupModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpDao grpDao;
	public void setGrpDao( IGrpDao groupDao ) {
		this.grpDao = groupDao;
	}

	private IGrpRoleLnkDao grpRoleLnkDao;
	public void setGrpRoleLnkDao( IGrpRoleLnkDao grpRoleLnkDao ) {
		this.grpRoleLnkDao = grpRoleLnkDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupModifyBtnServiceBean> execute( IServiceDto<FlowUcfGroupModifyBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfGroupModifyBtnServiceBean paramBean;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			if ( TriStringUtils.isEmpty( paramBean.getInGroupId() ) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001002E );
			}
			// 入力チェック
			List<IGrpEntity> grpEntities = umFinderSupport.findAllGroup();
			IGrpEntity curEntity = null ;
			for( IGrpEntity grpEntity : grpEntities ) {
				if( paramBean.getInGroupId().equals( grpEntity.getGrpId() ) ) {
					curEntity = grpEntity ;
				}
			}
			UmItemChkUtils.checkUcfInputGroup(paramBean.getInGroupId(), paramBean.getInGroupName() , curEntity.getGrpNm() , grpEntities.toArray(new IGrpEntity[0]) , Mode.UPDATE );

			updateGrpDao( paramBean.getInGroupId(), paramBean.getInGroupName() );

			//画面のグループIDで、グループロールテーブルから削除
			deleteGrpRoleLnkDao( paramBean.getInGroupId() );

			//グループに所属するユーザのユーザIDをリスト（userList）に格納する
			List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInGroupId() );

			for ( IGrpUserLnkEntity gUserView : grpUserLnkEntityList ){
				deleteUserRoleLnkDao( gUserView.getUserId() );
			}

			if ( paramBean.getInRoleList() == null ) {
				;// 編集で削除できるように修正
				//throw new ContinuableBusinessException( MessageId.MESUCF0004, new String[] {} );
			} else {
				//選択したロールのロールIDをグループロールテーブルに登録する
				IGrpRoleLnkEntity groupRoleEntity = new GrpRoleLnkEntity();

				for (String roleId : paramBean.getInRoleList()) {
					groupRoleEntity.setGrpId( paramBean.getInGroupId() );
					groupRoleEntity.setRoleId( roleId );
					grpRoleLnkDao.insert(groupRoleEntity);
				}

				//グループに所属するユーザのユーザIDをリスト（uList）に登録する
				List<IGrpUserLnkEntity> grpUserLnkEntityList1 = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInGroupId() );
				List<String> uList = new ArrayList<String>();

				for ( IGrpUserLnkEntity view : grpUserLnkEntityList1 ){
					uList.add(view.getUserId());
				}
				//ユーザが他に所属するグループを取得する
				for (String userId : uList ) {
					List<IGrpUserLnkEntity> grpUserLnkEntityList2 = umFinderSupport.findGrpUserLnkByUserId(userId);
					List<String> groupList = new ArrayList<String>();

				//ユーザが他に所属するグループのグループIDを、リスト（groupList）に格納する
					for ( IGrpUserLnkEntity view : grpUserLnkEntityList2 ){
						groupList.add( view.getGrpId() );
					}
				//リスト（groupList）のグループIDに紐づくロールIDを取得する
					for (String grpId : groupList ) {
						List<IGrpRoleLnkEntity> grpRoleLnkEntityList = umFinderSupport.findGrpRoleLnkByGrpId( grpId );
						List<String> roleList = new ArrayList<String>();

				//ユーザが他に所属するグループに設定されているロールIDを、リスト（roleList）に格納する
						for ( IGrpRoleLnkEntity gRoleEntity : grpRoleLnkEntityList ){
							roleList.add(gRoleEntity.getRoleId());
						}
				//リスト（roleList）のロールIDをユーザロールテーブルに格納する
						this.insertRoleId( roleList , userId) ;
					}
				}
			}

			List<IGrpRoleLnkEntity> grpRoleEntityList = umFinderSupport.findGrpRoleLnkByGrpId( paramBean.getInGroupId() );
			paramBean.setInRoleList( TriCollectionUtils.collect(grpRoleEntityList, toRoleId()) );

			// for文でlistに格納したBeanをparamBeanに格納する
			paramBean.setViewBeanList( setGrpList( paramBean ) );

			paramBean.setRoleViewBeanList( setRoleList( paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005014S, e );
		}
	}

	/**
	 *
	 * @param grpId
	 * @param grpNm
	 */
	private void updateGrpDao( String grpId, String grpNm ) {

		// 画面の値をgroupEntityセットする
		GrpEntity grpEntity = new GrpEntity();
		grpEntity.setGrpId( grpId );
		grpEntity.setGrpNm( grpNm );
		// ここでDBに値を登録
		grpDao.update(grpEntity);
	}

	/**
	 *
	 * @param grpId
	 */
	private void deleteGrpRoleLnkDao( String grpId ) {

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		condition.setGrpId( grpId );
		grpRoleLnkDao.delete( condition.getCondition() );
	}

	private void deleteUserRoleLnkDao( String userId ) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);
		userRoleLnkDao.delete( condition.getCondition() );
	}

	private void deleteUserRoleLnkDao( String userId, String roleId ) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);
		condition.setRoleId(roleId);
		userRoleLnkDao.delete( condition.getCondition() );
	}

	private void insertUserRoleLnkDao( String userId, String roleId ){

		IUserRoleLnkEntity userRoleEntity = new UserRoleLnkEntity();
		userRoleEntity.setUserId(userId);
		userRoleEntity.setRoleId(roleId);
		userRoleLnkDao.insert(userRoleEntity);
	}

	/**
	 *
	 * @param roleList
	 * @param userId
	 */
	private void insertRoleId( List<String> roleList ,String userId ) {

		for (String roleId : roleList) {
			deleteUserRoleLnkDao(userId, roleId);
			insertUserRoleLnkDao(userId, roleId);
		}
	}

	private List<GroupListViewBean> setGrpList(FlowUcfGroupModifyBtnServiceBean paramBean) {

		List<IGrpEntity> entitys = umFinderSupport.findAllGroup();
		// DBに登録されている内容をListに格納する
		List<GroupListViewBean> list = new ArrayList<GroupListViewBean>();

		// インスタンス(器)を作成し登録されたデータの件数分データを格納する
		for ( IGrpEntity entity : entitys ) {
			GroupListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setGroupId(entity.getGrpId());
			viewBean.setGroupName(entity.getGrpNm());

			list.add( viewBean );
		}
		return list;
	}

	private List<RoleListViewBean> setRoleList(FlowUcfGroupModifyBtnServiceBean paramBean) {

		List<IRoleEntity> roleEntityList = umFinderSupport.findAllRole();
		List<RoleListViewBean> roleList = new ArrayList<RoleListViewBean>();
		for ( IRoleEntity entity : roleEntityList ) {
			RoleListViewBean viewBean = paramBean.newRoleListViewBean();

			viewBean.setRoleId(entity.getRoleId());
			viewBean.setRoleName(entity.getRoleName());

			roleList.add( viewBean );
		}
		return roleList;
	}

	private TriFunction<IGrpRoleLnkEntity, String> toRoleId() {

		return new TriFunction<IGrpRoleLnkEntity, String>() {

			@Override
			public String apply(IGrpRoleLnkEntity entity) {
				return entity.getRoleId();
			}
		};
	}

}