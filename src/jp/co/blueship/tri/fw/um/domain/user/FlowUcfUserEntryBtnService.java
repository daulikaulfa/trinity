package jp.co.blueship.tri.fw.um.domain.user;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


public class FlowUcfUserEntryBtnService implements IDomain<FlowUcfUserEntryBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUserDao userDao;
	public void setUserDao( IUserDao userDao ) {
		this.userDao = userDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IGrpUserLnkDao grpUserLnkDao;
	public void setGrpUserLnkDao( IGrpUserLnkDao groupUserLnkDao ) {
		this.grpUserLnkDao = groupUserLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfUserEntryBtnServiceBean> execute( IServiceDto<FlowUcfUserEntryBtnServiceBean> serviceDto ) {

		FlowUcfUserEntryBtnServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			{
				//注意書き
				List<IMessageId> commentList	= new ArrayList<IMessageId>();
				List<String[]> commentArgsList	= new ArrayList<String[]>();

				commentList.add		( UmMessageId.UM003010I );
				commentArgsList.add	( new String[]{} );

				paramBean.setInfoCommentIdList		( commentList );
				paramBean.setInfoCommentArgsList	( commentArgsList );
			}

			// 入力チェック
			paramBean.setInSectionId(paramBean.getInSectionId());
			List<IUserEntity> userEntities = umFinderSupport.findAllUser();
			UmItemChkUtils.checkUcfInputUser
				( paramBean.getInUserId(), paramBean.getInUserName(), null , paramBean.getInPassword(),
						paramBean.getInRepassword(), paramBean.getInMailAddress(), paramBean.getInSectionId() , paramBean.getInPasswordEffectiveTerm() , userEntities , Mode.INSERT );

			// 重複チェック
			if ( umFinderSupport.isExistByUserId( paramBean.getInUserId() ) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001024E );
			}


			// ユーザ情報登録
			insertUserBasic( paramBean );

			// 登録済みロールID
			List<String> addRoleId = new ArrayList<String>();


			if ( paramBean.getInGroupList() == null ) {
				;// 編集で削除できるように修正
				//throw new ContinuableBusinessException( MessageId.MESUCF0004, new String[] {} );
			} else {

				for ( String groupId : paramBean.getInGroupList() ) {

					GrpUserLnkEntity groupUserEntity = new GrpUserLnkEntity();

					groupUserEntity.setGrpId	( groupId );
					groupUserEntity.setUserId	( paramBean.getInUserId() );
					grpUserLnkDao.insert( groupUserEntity );

					List<IGrpRoleLnkEntity> groupRoleEntities = umFinderSupport.findGrpRoleLnkByGrpId( groupUserEntity.getGrpId() );

					for ( IGrpRoleLnkEntity gRoleEntity : groupRoleEntities ) {

						if ( addRoleId.contains( gRoleEntity.getRoleId() )) continue;

						IUserRoleLnkEntity userRoleEntity = new UserRoleLnkEntity();

						userRoleEntity.setUserId( paramBean.getInUserId() );
						userRoleEntity.setRoleId( gRoleEntity.getRoleId() );

						userRoleLnkDao.insert( userRoleEntity );

						addRoleId.add( gRoleEntity.getRoleId() );
					}
				}
			}


//			IGroupUserView[] groupUserView = groupUserDao.find( paramBean.getInUserId() );
//
//			List<String> groupUserList = new ArrayList<String>();
//
//			for ( IGroupUserView entity : groupUserView ) {
//				groupUserList.add(entity.getGroupId().toString());
//			}
//
//			paramBean.setInGroupList( groupUserList );


			// ユーザ一覧情報
			setUserBasicList( paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005037S, e );
		}
	}


	/**
	 * ユーザ情報を登録する
	 * @param paramBean
	 */

	private void insertUserBasic( FlowUcfUserEntryBtnServiceBean paramBean ) {

		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId	( paramBean.getInUserId() );
		userEntity.setUserNm	( paramBean.getInUserName() );
		userEntity.setUserPass	(
				EncryptionUtils.encodePasswordInA1Format( paramBean.getInUserId(), paramBean.getInPassword() ));
		userEntity.setPassTimeLimit(
				getPasswordEffectiveDate( paramBean.getInPasswordEffectiveTerm() ));
		userEntity.setEmailAddr( paramBean.getInMailAddress() );

		IDeptEntity secEntity = findDept( paramBean.getInSectionId() );
		if ( null == secEntity ) {
			throw new ContinuableBusinessException( UmMessageId.UM001015E );
		}
		userEntity.setDeptId	( paramBean.getInSectionId() );

		userDao.insert( userEntity );

	}

	/**
	 * 部署情報を取得します。umFinderSupport内のfindDeptByDeptIdと異なり、検索結果がnullでもExceptionとせずそのまま返します。
	 * @param deptId
	 * @return 部署エンティティ
	 */
	private IDeptEntity findDept(String deptId) {

		PreConditions.assertOf(deptId != null, "DepartmentId is not specified");
		DeptCondition condition = new DeptCondition();
		condition.setDeptId( deptId );
		IDeptEntity entity = umFinderSupport.getDeptDao().findByPrimaryKey( condition.getCondition() );
		return entity;

	}
	/**
	 * ユーザ一覧情報を設定する
	 * @param paramBean
	 */
	private void setUserBasicList( FlowUcfUserEntryBtnServiceBean paramBean ) {

		List<IUserEntity> userEntityList = umFinderSupport.findAllUser();
		List<UserListViewBean> userList = new ArrayList<UserListViewBean>();

		for ( IUserEntity entity : userEntityList ) {
			UserListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setUserId	( entity.getUserId().toString() );
			viewBean.setUserName( entity.getUserNm() );
			viewBean.setPassword( entity.getUserPass() );
			viewBean.setMailAddress	( entity.getEmailAddr() );
			viewBean.setSectionId	( entity.getDeptId() );

			if ( TriStringUtils.isEmpty(entity.getDeptId()) ) {
				viewBean.setSectionName( "" );
			} else {
				IDeptEntity deptEntity = umFinderSupport.findDeptByDeptId( entity.getDeptId() );
				viewBean.setSectionName( deptEntity.getDeptNm() );
			}

			userList.add( viewBean );
		}

		paramBean.setViewBeanList( userList );

	}

	/**
	 * パスワード有効日時を取得する
	 * @param paramBean
	 * @return
	 */
	private Timestamp getPasswordEffectiveDate( String termId ) {

		// 期間未指定、またはマイナス値の場合は有効期限の制限なし
		if ( null != termId ) {

			int termInt = Integer.parseInt( termId );

			if ( 0 <= termInt ) {

				Calendar now = Calendar.getInstance();
				now.add( Calendar.DATE, termInt );

				return new Timestamp( now.getTimeInMillis() );
			}
		}

		return null;

	}

}
