package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriClojure;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupDeleteBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupDeleteBtnService implements IDomain<FlowUcfGroupDeleteBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpDao grpDao;
	public void setGrpDao( IGrpDao groupDao ) {
		this.grpDao = groupDao;
	}

	private IGrpRoleLnkDao grpRoleLnkDao;
	public void setGrpRoleLnkDao( IGrpRoleLnkDao grpRoleLnkDao ) {
		this.grpRoleLnkDao = grpRoleLnkDao;
	}

	private IGrpUserLnkDao grpUserLnkDao;
	public void setGrpUserLnkDao( IGrpUserLnkDao grpUserLnkDao ) {
		this.grpUserLnkDao = grpUserLnkDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}


	@Override
	public IServiceDto<FlowUcfGroupDeleteBtnServiceBean> execute( IServiceDto<FlowUcfGroupDeleteBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfGroupDeleteBtnServiceBean paramBean;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			deleteGrpRoleLnkDao( paramBean.getInGroupId() );

			List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInGroupId() );

			List<String> userList = TriCollectionUtils.collect(grpUserLnkEntityList, toUserId() );

			TriCollectionUtils.forEach(userList, deleteUserRoleAndUpdateUserRoleLnkDao() );
			// 削除
			deleteGrpDao( paramBean.getInGroupId() );
			deleteGrpUserDao( paramBean.getInGroupId() );
			deleteGrpRoleLnkDao( paramBean.getInGroupId() );

			paramBean.setViewBeanList( getGrpList( umFinderSupport.findAllGroup(), paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005009S, e );
		}
	}

	/**
	 *
	 * @param roleList
	 * @param userId
	 */
	private void insertRoleId( List<String> roleList ,String userId ) {

		for (String roleId : roleList ) {
			deleteUserRoleLnkDao(userId, roleId);
			insertUserRoleLnkDao(userId, roleId);
		}
	}

	/**
	 * 指定されたユーザID,ロールIDに該当するUserRoleLnkDaoの削除を行います。
	 * @param userId
	 * @param roleId
	 */
	private void deleteUserRoleLnkDao(String userId, String roleId ) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);
		condition.setRoleId(roleId);

		userRoleLnkDao.delete( condition.getCondition() );
	}

	/**
	 * 指定されたユーザIDに該当するUserRoleLnkDaoの削除を行います。
	 * @param userId
	 */
	private void deleteUserRoleLnkDao(String userId) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);

		userRoleLnkDao.delete( condition.getCondition() );
	}

	/**
	 * 指定されたグループIDに該当するGrpDaoの削除を行います。
	 * @param grpId
	 */
	private void deleteGrpDao( String grpId ){

		GrpCondition grpCondition = new GrpCondition();
		grpCondition.setGrpId( grpId );
		grpDao.delete( grpCondition.getCondition() );

	}

	/**
	 * 指定されたグループIDに該当するGrpUserLnkDaoの削除を行います。
	 * @param grpId
	 */
	private void deleteGrpUserDao(String grpId) {

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(grpId);
		grpUserLnkDao.delete( condition.getCondition() );

	}

	/**
	 * 指定されたグループIDに該当するGrpRoleLnkDaoの削除を行います。
	 * @param grpId
	 */
	private void deleteGrpRoleLnkDao( String grpId ) {

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		condition.setGrpId(grpId);
		grpRoleLnkDao.delete( condition.getCondition() ) ;
	}

	/**
	 * 指定されたユーザID,ロールIDの情報をUserRoleLnkDaoに追加します。
	 * @param userId
	 * @param roleId
	 */
	private void insertUserRoleLnkDao(String userId, String roleId ) {

		IUserRoleLnkEntity userRoleEntity = new UserRoleLnkEntity();
		userRoleEntity.setUserId(userId);
		userRoleEntity.setRoleId(roleId);
		userRoleLnkDao.insert(userRoleEntity);

	}

	/**
	 * リスト（userList）のユーザIDでユーザロールテーブルから削除し、ユーザが他に所属するグループを取得します。
	 * その後、グループリストとユーザIDに紐付くUserRoleLnkDaoをdelete,insertを用いて更新します。
	 * @param userId
	 * @return
	 */
	private TriClojure<String> deleteUserRoleAndUpdateUserRoleLnkDao() {

		return new TriClojure<String>() {

			@Override
			public boolean apply(String userId) {

				deleteUserRoleLnkDao( userId );
				List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByUserId(userId);

				if ( TriCollectionUtils.isNotEmpty( grpUserLnkEntityList) ) {
					//ユーザが他に所属するグループのグループIDを、リスト（groupList）に格納する
					List<String> groupList = TriCollectionUtils.collect(grpUserLnkEntityList, toGrpId() );
					//リスト（groupList）のグループIDに紐づくロールIDを取得する
					TriCollectionUtils.forEach(groupList, fromGrpListInsertRoleId( userId ) );
				}
				return true;
			}
		};
	}

	/**
	 * グループリストとユーザIDに紐付くUserRoleLnkDaoをdelete,insertを用いて更新します。
	 * @param userId
	 * @return
	 */
	private TriClojure<String> fromGrpListInsertRoleId(final String userId) {

		return new TriClojure<String>() {

			@Override
			public boolean apply(String grpId) {

				List<IGrpRoleLnkEntity> grpRoleLnkEntityList = umFinderSupport.findGrpRoleLnkByGrpId( grpId );
				//ユーザが他に所属するグループに設定されているロールのロールIDを、リスト（roleList）に格納する
				List<String> roleList = TriCollectionUtils.collect( grpRoleLnkEntityList, toRoleId() );
				//リスト（roleList）のロールIDをユーザロールテーブルに格納する
				insertRoleId( roleList , userId );
				return true;
			}
		};
	}

	/**
	 * DBに登録されている内容をListに格納します。
	 * @param entitys
	 * @param paramBean
	 * @return
	 */
	private List<GroupListViewBean> getGrpList( List<IGrpEntity> entitys, FlowUcfGroupDeleteBtnServiceBean paramBean) {

		List<GroupListViewBean> list = new ArrayList<GroupListViewBean>();

		for ( IGrpEntity entity : entitys ) {
			GroupListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setGroupId(entity.getGrpId().toString());
			viewBean.setGroupName(entity.getGrpNm());

			list.add( viewBean );
		}
		return list;
	}

	/**
	 * GrpUserLnkEntityからユーザIDを取り出すファンクションメソッドです。
	 * @return
	 */
	private TriFunction<IGrpUserLnkEntity,String> toUserId(){

		return new TriFunction<IGrpUserLnkEntity, String>() {

			@Override
			public String apply(IGrpUserLnkEntity entity) {
				return entity.getUserId();
			}
		};
	}

	/**
	 * GrpUserLnkEntityからグループIDを取り出すファンクションメソッドです。
	 * @return
	 */
	private TriFunction<IGrpUserLnkEntity,String> toGrpId(){

		return new TriFunction<IGrpUserLnkEntity, String>() {

			@Override
			public String apply(IGrpUserLnkEntity entity) {
				return entity.getGrpId();
			}
		};
	}

	/**
	 * GrpRoleLnkEntityからロールIDを取り出すファンクションメソッドです。
	 * @return
	 */
	private TriFunction<IGrpRoleLnkEntity,String> toRoleId(){

		return new TriFunction<IGrpRoleLnkEntity, String>() {

			@Override
			public String apply(IGrpRoleLnkEntity entity) {
				return entity.getRoleId();
			}
		};
	}
}
