package jp.co.blueship.tri.fw.um.domain.dept;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfSectionModifyBtnService implements IDomain<FlowUcfSectionModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDeptDao deptDao;
	public void setDeptDao( IDeptDao deptDao ) {
		this.deptDao = deptDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionModifyBtnServiceBean> execute( IServiceDto<FlowUcfSectionModifyBtnServiceBean> serviceDto ) {

		FlowUcfSectionModifyBtnServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			// 入力チェック
			List<IDeptEntity> entities = umFinderSupport.findAllDept();
			IDeptEntity curEntity = null ;
			for( IDeptEntity sectionEntity : entities ) {
				if( paramBean.getInSectionId().equals( sectionEntity.getDeptId() ) ) {
					curEntity = sectionEntity ;
				}
			}
			UmItemChkUtils.checkUcfInputSection(paramBean.getInSectionId() , paramBean.getInSectionName() , curEntity.getDeptNm() , entities , Mode.UPDATE );

			updateDeptDao( paramBean.getInSectionId(), paramBean.getInSectionName() );

			//変更後再検索
			entities = umFinderSupport.findAllDept();
			paramBean.setViewBeanList( getSectionList( entities , paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005007S, e );
		}
	}

	private void updateDeptDao(String deptId, String deptNm ) {

		IDeptEntity deptEntity = new DeptEntity();
		deptEntity.setDeptId( deptId );
		deptEntity.setDeptNm( deptNm );
		deptDao.update(deptEntity);
	}

	private List<SectionListViewBean> getSectionList( List<IDeptEntity> entities,  FlowUcfSectionModifyBtnServiceBean paramBean) {

		List<SectionListViewBean> list = new ArrayList<SectionListViewBean>();
		for ( IDeptEntity entity : entities ) {
			SectionListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setSectionId( entity.getDeptId() );
			viewBean.setSectionName( entity.getDeptNm() );

			list.add( viewBean );
		}
		return list;
	}
}
