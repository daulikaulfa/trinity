package jp.co.blueship.tri.fw.um.domain.user;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.RandomStringUtils;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.PasswordEffectiveTermViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.SectionViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserModifyServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfUserModifyService implements IDomain<FlowUcfUserModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfUserModifyServiceBean> execute( IServiceDto<FlowUcfUserModifyServiceBean> serviceDto ) {

		FlowUcfUserModifyServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}

			//注意書き
			setComment( paramBean );

			// 部署
			paramBean.setSectionViewBeanList( setSectionList( umFinderSupport.findAllDept(), paramBean ) );

			// ユーザ情報
			if ( paramBean.getInUserName() == null ) {
				setUserInfo( paramBean );
			} else {
				paramBean.setUserName( paramBean.getInUserName() );
			}

			// 選択済みグループ
			if( null == paramBean.getGroupSelected() ) {
				List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByUserId( paramBean.getInUserId() );
				paramBean.setGroupSelected( TriCollectionUtils.collect( grpUserLnkEntityList, toGrpId() ) );
			}

			// グループ一覧
			paramBean.setGroupViewBeanList( setGroupList( umFinderSupport.findAllGroup(), paramBean ) );

			// パスワード有効期間
			setPasswordEffectiveTerm( paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005041S, e );
		}
	}


	/**
	 * 注意書きを設定します。
	 * @param paramBean
	 */
	private void setComment( FlowUcfUserModifyServiceBean paramBean ) {

		List<IMessageId> commentList	= new ArrayList<IMessageId>();
		List<String[]> commentArgsList	= new ArrayList<String[]>();

		commentList.add		( UmMessageId.UM003010I );
		commentArgsList.add	( new String[]{} );

		paramBean.setInfoCommentIdList		( commentList );
		paramBean.setInfoCommentArgsList	( commentArgsList );

	}

	/**
	 * ユーザ情報を設定します。
	 * @param paramBean
	 */
	private void setUserInfo( FlowUcfUserModifyServiceBean paramBean ) {

		IUserEntity userEntity = umFinderSupport.findUserByUserId(paramBean.getInUserId());

		paramBean.setUserId		( userEntity.getUserId() );
		paramBean.setUserName	( userEntity.getUserNm() );
		// パスワードはハッシュ化されていてちょー長いため、適当な文字列を生成してセットする。
		// セットした文字列が変更されて戻ってきたら、パスワードが変更されたと判断する。
		String tempPassword = RandomStringUtils.randomAlphanumeric( 10 );
		paramBean.setPassword	( tempPassword );
		paramBean.setRepassword	( tempPassword );
		paramBean.setTermLimit	( getPasswordEffectiveLimit( userEntity.getPassTimeLimit() ) );
		paramBean.setMailAddress( userEntity.getEmailAddr() );
		paramBean.setSectionId	( userEntity.getDeptId() );
	}

	/**
	 * SectionViewBeanに部署情報を設定します。
	 * @param deptEntityList
	 * @param paramBean
	 * @return
	 */
	private List<SectionViewBean> setSectionList (List<IDeptEntity> deptEntityList, FlowUcfUserModifyServiceBean paramBean ) {

		List<SectionViewBean> sectionList = new ArrayList<SectionViewBean>();

		for ( IDeptEntity entity : deptEntityList ) {
			SectionViewBean viewBean = paramBean.newSectionViewBean();

			viewBean.setSectionId	( entity.getDeptId() );
			viewBean.setSectionName	( entity.getDeptNm() );

			sectionList.add( viewBean );
		}
		return sectionList;
	}

	/**
	 * GroupListViewBeanにグループ情報を設定します。
	 * @param grpEntityList
	 * @param paramBean
	 * @return
	 */
	private List<GroupListViewBean> setGroupList(List<IGrpEntity> grpEntityList, FlowUcfUserModifyServiceBean paramBean) {

		List<GroupListViewBean> groupList = new ArrayList<GroupListViewBean>();

		for ( IGrpEntity entity : grpEntityList ) {
			GroupListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setGroupId( entity.getGrpId() );
			viewBean.setGroupName( entity.getGrpNm() );

			groupList.add( viewBean );
		}
		return groupList;
	}

	/**
	 * パスワード有効期間
	 * @param paramBean
	 */
	private void setPasswordEffectiveTerm( FlowUcfUserModifyServiceBean paramBean ) {

		// デフォルト値
		String defaultTerm = DesignSheetUtils.getPasswordDefaultEffectiveTerm();

		List<PasswordEffectiveTermViewBean> beanList = new ArrayList<PasswordEffectiveTermViewBean>();

		if ( !TriStringUtils.isEmpty( defaultTerm )) {
			PasswordEffectiveTermViewBean viewBean = paramBean.newTermViewBean();
			viewBean.setTerm	( defaultTerm );
			viewBean.setComment	( defaultTerm + DesignUtils.getMessageParameter( MessageParameter.DAYS.getKey() ) );

			beanList.add( viewBean );
		}

		// 固定のリスト
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap( UmDesignBeanId.passwordEffectiveTerm );

		for ( Map.Entry<String, String> entry : termMap.entrySet() ) {

			PasswordEffectiveTermViewBean viewBean = paramBean.newTermViewBean();
			viewBean.setTerm	( entry.getKey() );
			viewBean.setComment	( entry.getValue() );

			beanList.add( viewBean );
		}

		paramBean.setTermViewBeanList( beanList );
	}


	/**
	 * パスワード有効期限を取得する
	 * @param termLimit
	 * @return
	 */
	private String getPasswordEffectiveLimit( Timestamp termLimit ) {

		if ( null == termLimit ) return null;

		SimpleDateFormat sdf = new SimpleDateFormat( "yyyy/MM/dd HH:mm:ss" );

		return sdf.format( new Date( termLimit.getTime() ));

	}

	/**
	 * GrpRoleLnkEntityからロールIDを取り出すファンクションメソッドです。
	 * @return
	 */
	private TriFunction<IGrpUserLnkEntity,String> toGrpId() {

		return new TriFunction<IGrpUserLnkEntity, String>() {

			@Override
			public String apply(IGrpUserLnkEntity entity) {
				return entity.getGrpId();
			}
		};
	}
}
