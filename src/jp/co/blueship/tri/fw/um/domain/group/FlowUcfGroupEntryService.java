package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupEntryService implements IDomain<FlowUcfGroupEntryServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupEntryServiceBean> execute( IServiceDto<FlowUcfGroupEntryServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfGroupEntryServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			List<IRoleEntity> roleEntityList = umFinderSupport.findAllRole();
			List<RoleListViewBean> roleList = new ArrayList<RoleListViewBean>();

			for ( IRoleEntity entity : roleEntityList ) {
				RoleListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setRoleId(entity.getRoleId());
				viewBean.setRoleName(entity.getRoleName());

				roleList.add( viewBean );
			}

			paramBean.setRoleViewBeanList(roleList);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005012S, e );
		}
	}


}
