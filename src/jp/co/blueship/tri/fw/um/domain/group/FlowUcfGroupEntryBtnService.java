package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.grp.GrpNumberingDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupEntryBtnService implements IDomain<FlowUcfGroupEntryBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpDao grpDao = null;
	public void setGrpDao( IGrpDao groupDao ) {
		this.grpDao = groupDao;
	}

	private IGrpRoleLnkDao grpRoleLnkDao;
	public void setGrpRoleLnkDao( IGrpRoleLnkDao grpRoleLnkDao ) {
		this.grpRoleLnkDao = grpRoleLnkDao;
	}

	private GrpNumberingDao grpNumberingDao = null;
	public void setGrpNumberingDao( GrpNumberingDao groupIdNumberingDao ) {
		this.grpNumberingDao = groupIdNumberingDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupEntryBtnServiceBean> execute( IServiceDto<FlowUcfGroupEntryBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfGroupEntryBtnServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			// グループID採番
			// 既存のデータを考慮し、重複がないように採番する
			String groupId = null;
			for (;;) {
				groupId = grpNumberingDao.nextval();
				if ( umFinderSupport.checkDuplicationByNumberingGroupId(groupId) ) {
					break;
				}
			}

			paramBean.setInGroupId	( groupId );

			// 入力チェック
			List<IGrpEntity> grpEntities = umFinderSupport.findAllGroup();
			UmItemChkUtils.checkUcfInputGroup( paramBean.getInGroupId(), paramBean.getInGroupName() , null , grpEntities.toArray(new IGrpEntity[0]) , Mode.INSERT );

			//画面の値をgroupEntityセットし、DBに登録
			insertGrpDao(paramBean.getInGroupId(), paramBean.getInGroupName());

			if ( paramBean.getInRoleList() == null ) {
				;// 編集で削除できるように修正
				//throw new ContinuableBusinessException( MessageId.MESUCF0004, new String[] {} );
			} else {
				for ( String roleId : paramBean.getInRoleList() ) {
					insertGrpRoleLnkDao(paramBean.getInGroupId(), roleId);
				}
			}

			// for文でlistに格納したBeanをparamBeanに格納する
			paramBean.setViewBeanList( getGroupList(paramBean) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005011S, e );
		}
	}

	private void insertGrpDao(String grpId, String grpNm ) {

		GrpEntity grpEntity = new GrpEntity();
		grpEntity.setGrpId	( grpId );
		grpEntity.setGrpNm	( grpNm );
		// ここでDBに値を登録
		grpDao.insert( grpEntity );
	}

	private void insertGrpRoleLnkDao(String grpId, String roleId) {

		IGrpRoleLnkEntity groupRoleEntity = new GrpRoleLnkEntity();
		groupRoleEntity.setGrpId	( grpId );
		groupRoleEntity.setRoleId	( roleId );
		grpRoleLnkDao.insert( groupRoleEntity );
	}


	private List<GroupListViewBean> getGroupList(FlowUcfGroupEntryBtnServiceBean paramBean) {
		// DB全件検索
		List<IGrpEntity> entitys = umFinderSupport.findAllGroup();

		// DBに登録されている内容をListに格納する
		List<GroupListViewBean> groupList = new ArrayList<GroupListViewBean>();

		// インスタンス(器)を作成し登録されたデータの件数分データを格納する
		for ( IGrpEntity entity : entitys ) {
			GroupListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setGroupId		( entity.getGrpId() );
			viewBean.setGroupName	( entity.getGrpNm() );

			groupList.add( viewBean );
		}
		return groupList;
	}
}
