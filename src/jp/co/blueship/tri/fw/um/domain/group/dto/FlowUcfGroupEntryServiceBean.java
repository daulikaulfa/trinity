package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfGroupEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	private List<String> sectionViewList;
	private List<String> userViewList;
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();

	public List<String> getSectionViewList() {
		return sectionViewList;
	}

	public void setSectionViewList(List<String> sectionViewList) {
		this.sectionViewList = sectionViewList;
	}
	
	public List<String> getUserViewList() {
		return userViewList;
	}

	public void setUserViewList(List<String> userViewList) {
		this.userViewList = userViewList;
	}
	
	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}
	
	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}
	
	public RoleListViewBean newListViewBean() {
		RoleListViewBean bean = new RoleListViewBean();
		
		return bean;
	}
	
	public class RoleListViewBean {
	
		/** ロールID **/
		private String roleId   		  = null;
		/** ロール名 **/
		private String roleName 		  = null;
		public String getRoleId() {
			return roleId;
		}
		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
	}
	
}
