package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfRoleModifyBtnService implements IDomain<FlowUcfRoleModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IRoleDao roleDao;
	public void setRoleDao( IRoleDao roleDao ) {
		this.roleDao = roleDao;
	}

	private IRoleSvcCtgLnkDao roleSvcCtgLnkDao;
	public void setRoleSvcCtgLnkDao( IRoleSvcCtgLnkDao roleSvcCtgLnkDao ) {
		this.roleSvcCtgLnkDao = roleSvcCtgLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	private ISmFinderSupport smFinderSupport;
	public void setSmFinderSupport( ISmFinderSupport smFinderSupport ) {
		this.smFinderSupport = smFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfRoleModifyBtnServiceBean> execute( IServiceDto<FlowUcfRoleModifyBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfRoleModifyBtnServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			// 入力チェック
			List<IRoleEntity> roleEntities = umFinderSupport.findAllRole();
			IRoleEntity curEntity = null ;
			for( IRoleEntity roleEntity : roleEntities ) {
				if( paramBean.getInRoleId().equals( roleEntity.getRoleId() ) ) {
					curEntity = roleEntity ;
				}
			}
			UmItemChkUtils.checkUcfInputRole(paramBean.getInRoleId(), paramBean.getInRoleName() , curEntity.getRoleName() , roleEntities , Mode.UPDATE );

			// 画面の値をroleEntityセットする
			updateRoleDao(paramBean.getInRoleId(), paramBean.getInRoleName());
			//backup svcIdV4
			Set<String> v4SvcCtgIds = this.getSvcIdV4Set(paramBean.getInRoleId());
			//ロール・サービスカテゴリ(Lnk)を更新するための削除
			deleteRoleSvcCtgLnkDao(paramBean.getInRoleId());

			//insert backup svcIdV4
			for (String svcId : v4SvcCtgIds) {
				insertRoleSvcCtgLnkDao(paramBean.getInRoleId(), svcId);
			}

			if ( paramBean.getInActionList() == null ) {
				;// 編集で削除できるように修正
				//throw new ContinuableBusinessException( MessageId.MESUCF0007, new String[] {} );
			} else {
				for (String svcId : paramBean.getInActionList()) {
					insertRoleSvcCtgLnkDao(paramBean.getInRoleId(), svcId);
				}
			}

			paramBean.setInActionList(this.umFinderSupport.getSvcCtgIdByRoleId(paramBean.getInRoleId()));

			// for文でlistに格納したBeanをparamBeanに格納する
			paramBean.setViewBeanList(getRoleList(paramBean));

			paramBean.setActionViewBeanList(getActionList(paramBean));

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005033S, e );
		}
	}

	private Set<String> getSvcIdV4Set(String roleId){
		List<String> svcCtgIds = this.umFinderSupport.getSvcCtgIdByRoleId(roleId);
		Set<String> v4SvcIds = new HashSet<String>();
		SvcCtgCondition condition = new SvcCtgCondition();
		condition.setSvcCtgIds(svcCtgIds.toArray(new String[0]));
		List<ISvcCtgEntity> entities = this.umFinderSupport.getSvcCtgDao().find(condition.getCondition());
		for (ISvcCtgEntity entity: entities ) {
			if(TriStringUtils.isNotEmpty(entity.getSvcCtgPath())) {
				v4SvcIds.add(entity.getSvcCtgId());
			}
		}
		return v4SvcIds;
	}

	/**
	 * RoleDaoのロールIDとロール名を更新します。
	 * @param roleId
	 * @param roleNm
	 */
	private void updateRoleDao(String roleId, String roleNm){

		IRoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleId(roleId);
		roleEntity.setRoleName(roleNm);
		roleDao.update(roleEntity);
	}

	/**
	 * ロールIDに紐付くロール・サービスカテゴリ(Lnk)を削除します。
	 * @param roleId
	 */
	private void deleteRoleSvcCtgLnkDao(String roleId) {

		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		condition.setRoleId(roleId);
		roleSvcCtgLnkDao.delete(condition.getCondition());
	}

	/**
	 * ロールIDとサービスカテゴリIDをroleSvcCtgLnkDaoに登録します。
	 * @param roleId
	 * @param svcCtgId
	 */
	private void insertRoleSvcCtgLnkDao(String roleId, String svcCtgId){

		IRoleSvcCtgLnkEntity roleSvcCtgEntity = new RoleSvcCtgLnkEntity();
		roleSvcCtgEntity.setRoleId( roleId );
		roleSvcCtgEntity.setSvcCtgId( svcCtgId );
		roleSvcCtgLnkDao.insert( roleSvcCtgEntity );
	}

	/**
	 * ロールIDとロール名を設定したRoleListViewBeanを取得します。
	 * @return RoleListViewBeanのリスト
	 */
	private List<RoleListViewBean> getRoleList(FlowUcfRoleModifyBtnServiceBean paramBean){

		// DB全件検索
		List<IRoleEntity> entities = umFinderSupport.findAllRole();

		// DBに登録されている内容をListに格納する
		List<RoleListViewBean> list = new ArrayList<RoleListViewBean>();

		// インスタンス(器)を作成し登録されたデータの件数分データを格納する
		for ( IRoleEntity entity : entities ) {
			RoleListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setRoleId(entity.getRoleId());
			viewBean.setRoleName(entity.getRoleName());

			list.add( viewBean );
		}
		return list;

	}

	/**
	 * サービスIDとサービス名を設定したActionListViewBeanを取得します。
	 * @return ActionListViewBeanのリスト
	 */
	private List<ActionListViewBean> getActionList(FlowUcfRoleModifyBtnServiceBean paramBean){

		List<IFuncSvcEntity> actionEntitys = smFinderSupport.getFuncSvcDao().find(new FuncSvcCondition().getCondition());

		List<ActionListViewBean> actionList = new ArrayList<ActionListViewBean>();

		for ( IFuncSvcEntity entity : actionEntitys ) {
			ActionListViewBean viewBean = paramBean.newActionListViewBean();

			viewBean.setActionId(entity.getSvcId());
			viewBean.setActionName(entity.getSvcNm());

			actionList.add( viewBean );
		}
		return actionList;
	}
}

