package jp.co.blueship.tri.fw.um.domain.dept.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfSectionListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 一覧情報
	 */
	private List<SectionListViewBean> sectionViewBeanList = new ArrayList<SectionListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public SectionListViewBean newListViewBean() {
		SectionListViewBean bean = new SectionListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<SectionListViewBean> getViewBeanList() {
		return sectionViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<SectionListViewBean> list) {
		this.sectionViewBeanList = list;
	}
	
	public class SectionListViewBean {
		/**
		 * セクションID
		 */
		private String sectionId = null;
		/**
		 * セクション名
		 */
		private String sectionName = null;
		
		public String getSectionId() {
			return sectionId;
		}
		public void setSectionId(String sectionId) {
			this.sectionId = sectionId;
		}
		public String getSectionName() {
			return sectionName;
		}
		public void setSectionName(String sectionName) {
			this.sectionName = sectionName;
		}
	}
}
