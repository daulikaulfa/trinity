package jp.co.blueship.tri.fw.um.domain.user;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserModifyBtnServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V3L10.02
 *
 * @version SP-20150622_V3L13R01
 * @author Norheda Zulkipeli
 *
 */

public class FlowUcfUserModifyBtnService  implements IDomain<FlowUcfUserModifyBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpUserLnkDao grpUserLnkDao;
	public void setGrpUserLnkDao( IGrpUserLnkDao grpUserLnkDao ) {
		this.grpUserLnkDao = grpUserLnkDao;
	}

	private IUserDao userDao;
	public void setUserDao( IUserDao userDao ) {
		this.userDao = userDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private LoginSession loginSession;
	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}
	@Override
	public IServiceDto<FlowUcfUserModifyBtnServiceBean> execute( IServiceDto<FlowUcfUserModifyBtnServiceBean> serviceDto ) {

		FlowUcfUserModifyBtnServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();
			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			{
				//注意書き
				List<IMessageId> commentList	= new ArrayList<IMessageId>();
				List<String[]> commentArgsList	= new ArrayList<String[]>();

				commentList.add		( UmMessageId.UM003010I );
				commentArgsList.add	( new String[]{} );

				paramBean.setInfoCommentIdList		( commentList );
				paramBean.setInfoCommentArgsList	( commentArgsList );
			}

			// 入力チェック
			String inUserId = paramBean.getInUserId() ;
			String inUserName = paramBean.getInUserName() ;
			String inPassword = paramBean.getInPassword() ;
			String inRepassword = paramBean.getInRepassword() ;
			String inMailAddress = paramBean.getInMailAddress() ;
			String inSectionId = paramBean.getInSectionId() ;

			//ログイン中のユーザとの衝突チェック。ログイン中のユーザ情報は変更できない。
			UmItemChkUtils.checkUcfAdminUser( inUserId );
			UmItemChkUtils.checkUcfLoginUser( inUserId , paramBean.getUserId() ) ;

			if ( loginSession.isLoggedInUser(inUserId) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001029E );
			}

			List<IUserEntity> userEntities = umFinderSupport.findAllUser();
			IUserEntity curEntity = null ;
			for( IUserEntity userEntity : userEntities ) {
				if( inUserId.equals( userEntity.getUserId() ) ) {
					curEntity = userEntity ;
				}
			}

			if ( TriStringUtils.isEmpty( paramBean.getInPassword() ) && TriStringUtils.isEmpty( paramBean.getInRepassword() )) {
				String dummy = "abcabcabca";

				inPassword = dummy;
				inRepassword = dummy;
			}

			//パスワード有効期間は編集時は未選択（＝変更なし）がありえるので
			// ダミーを突っ込んでおく
			UmItemChkUtils.checkUcfInputUser( inUserId , inUserName , curEntity.getUserNm() , inPassword ,
						inRepassword , inMailAddress , inSectionId , "termDummy" , userEntities , Mode.UPDATE );

			// ユーザ情報の更新
			this.updateUserBasic( paramBean );

			//グループユーザ情報の削除
			List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByUserId( inUserId );
			for ( IGrpUserLnkEntity view : grpUserLnkEntityList ) {
				deleteGrpUserLnkDao(view.getGrpId(),inUserId );
			}
			//ユーザロール基本情報の削除
			UserRoleLnkCondition condition = new UserRoleLnkCondition();
			condition.setUserId(inUserId);
			userRoleLnkDao.delete( condition.getCondition() );

			List<String> groupList = new ArrayList<String>();

			if ( ! TriStringUtils.isEmpty( paramBean.getInGroupList() ) ) {
				for (String groupId : paramBean.getInGroupList()) {
					//グループユーザ情報の追加

					IGrpUserLnkEntity groupUserEntity = new GrpUserLnkEntity();

					groupUserEntity.setUserId( inUserId );
					groupUserEntity.setGrpId( groupId ) ;

					grpUserLnkDao.insert(groupUserEntity);

					groupList.add( groupUserEntity.getGrpId() ) ;

					for (int groupCount = 0; groupCount < groupList.size(); groupCount++) {
						String gId = groupList.get(groupCount);

						List<IGrpRoleLnkEntity> groupRoleEntity = umFinderSupport.findGrpRoleLnkByGrpId( gId );

						List<String> roleList = new ArrayList<String>();

						if ( ! TriStringUtils.isEmpty( groupRoleEntity ) ) {
							for ( IGrpRoleLnkEntity gRoleEntity : groupRoleEntity ){
								roleList.add(gRoleEntity.getRoleId());
							}

							boolean delFlg = ( TriStringUtils.isEmpty( umFinderSupport.findUserRoleLnkByUserId(inUserId) ) ) ? false : true ;
							for ( String roleId : roleList ) {
								if( delFlg ) {
									deleteUserRoleLnkDao( roleId, inUserId );
								}
								IUserRoleLnkEntity userRoleLnkEntity = new UserRoleLnkEntity();
								userRoleLnkEntity.setUserId( inUserId );
								userRoleLnkEntity.setRoleId(roleId);
								userRoleLnkDao.insert(userRoleLnkEntity);
							}
						}
					}
				}
			}

			List<IGrpUserLnkEntity> groupUserView = umFinderSupport.findGrpUserLnkByUserId( paramBean.getInUserId() );
			this.makeGroupUserViewList( paramBean , groupUserView ) ;

			List<IUserEntity> entities = umFinderSupport.findAllUser();
			this.makeUserListViewBeanList( paramBean , entities ) ;

			List<IGrpEntity> groupEntities = umFinderSupport.findAllGroup();
			this.makeGroupViewBeanList( paramBean , groupEntities ) ;

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005040S, e );
		}
	}

	/**
	 *
	 * @param paramBean
	 * @param groupUserView
	 */
	private void makeGroupUserViewList( FlowUcfUserModifyBtnServiceBean paramBean , List<IGrpUserLnkEntity> groupUserView ) {
		List<String> groupUserList = new ArrayList<String>();

		for ( IGrpUserLnkEntity entity : groupUserView ) {
			groupUserList.add( entity.getGrpId() );
		}
		paramBean.setInGroupList(groupUserList);
	}

	/**
	 *
	 * @param paramBean
	 * @param entities
	 */
	private void makeUserListViewBeanList( FlowUcfUserModifyBtnServiceBean paramBean , List<IUserEntity> entities ) {
		List<UserListViewBean> list = new ArrayList<UserListViewBean>();

		for ( IUserEntity entity : entities ) {
			UserListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setUserId(entity.getUserId().toString());
			viewBean.setUserName(entity.getUserNm());
			viewBean.setPassword(entity.getUserPass());
			viewBean.setMailAddress(entity.getEmailAddr());
			viewBean.setSectionId(entity.getDeptId() );
			if ( TriStringUtils.isEmpty(entity.getDeptId() ) ) {
				viewBean.setSectionName( "" ) ;
			} else {
				viewBean.setSectionName(umFinderSupport.findDeptByDeptId( entity.getDeptId() ).getDeptNm());
			}

			list.add( viewBean );
		}
		paramBean.setViewBeanList(list);
	}

	private void deleteGrpUserLnkDao(String grpId, String userId ){

		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(grpId);
		condition.setUserId(userId);
		grpUserLnkDao.delete( condition.getCondition() );
	}

	private void deleteUserRoleLnkDao(String roleId, String userId) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setRoleId( roleId );
		condition.setUserId( userId );
		userRoleLnkDao.delete( condition.getCondition() );
	}
	/**
	 *
	 * @param paramBean
	 * @param groupEntities
	 */
	private void makeGroupViewBeanList( FlowUcfUserModifyBtnServiceBean paramBean , List<IGrpEntity> groupEntities ) {
		List<GroupListViewBean> gList = new ArrayList<GroupListViewBean>();

		for ( IGrpEntity entity : groupEntities ) {
			GroupListViewBean viewBean = paramBean.newGroupListViewBean();

			viewBean.setGroupId(entity.getGrpId().toString());
			viewBean.setGroupName(entity.getGrpNm());

			gList.add( viewBean );
		}

		paramBean.setGroupViewBeanList(gList);
	}

	/**
	 * パスワード有効日時を取得する
	 * @param paramBean
	 * @return
	 */
	private Timestamp getPasswordEffectiveDate( String termId ) {

		// 期間未指定、またはマイナス値の場合は有効期限の制限なし
		if ( null != termId ) {

			int termInt = Integer.parseInt( termId );

			if ( 0 <= termInt ) {

				Calendar now = Calendar.getInstance();
				now.add( Calendar.DATE, termInt );

				return new Timestamp( now.getTimeInMillis() );
			}
		}

		return null;

	}

	/**
	 * ユーザ情報を更新する
	 * @param paramBean
	 */

	private void updateUserBasic( FlowUcfUserModifyBtnServiceBean paramBean ) {

		IUserEntity userEntity	= new UserEntity();
		userEntity.setUserId	( paramBean.getInUserId() );
		userEntity.setUserNm	( paramBean.getInUserName() );

		// 元の値（ダミーのランダム文字列）と違う値が返ってきたら、パスワードが
		// 変更されたものとする
		if ( TriStringUtils.isNotEmpty( paramBean.getInPassword() )) {
			userEntity.setUserPass	(
					EncryptionUtils.encodePasswordInA1Format( paramBean.getInUserId(), paramBean.getInPassword() ));
		}

		// 値が""の場合は変更なし
		if ( !TriStringUtils.isEmpty( paramBean.getInPasswordEffectiveTerm() )) {
			userEntity.setPassTimeLimit(
					getPasswordEffectiveDate( paramBean.getInPasswordEffectiveTerm() ));
		}

		userEntity.setEmailAddr	( paramBean.getInMailAddress() );
		userEntity.setDeptId			( paramBean.getInSectionId() );

		userDao.update	( userEntity );
	}

}

