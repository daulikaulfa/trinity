package jp.co.blueship.tri.fw.um.domain.user;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.PasswordEffectiveTermViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.SectionViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfUserEntryService implements IDomain<FlowUcfUserEntryServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}
	@Override
	public IServiceDto<FlowUcfUserEntryServiceBean> execute( IServiceDto<FlowUcfUserEntryServiceBean> serviceDto ) {

		FlowUcfUserEntryServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			{
				//注意書き
				List<IMessageId> commentList	= new ArrayList<IMessageId>();
				List<String[]> commentArgsList	= new ArrayList<String[]>();

				commentList.add		( UmMessageId.UM003010I );
				commentArgsList.add	( new String[]{} );

				paramBean.setInfoCommentIdList		( commentList );
				paramBean.setInfoCommentArgsList	( commentArgsList );
			}

			// 部署
			List<IDeptEntity> entityList = umFinderSupport.findAllDept();
			paramBean.setSectionViewBeanList( getSectionViewBeanList(entityList, paramBean) );

			// グループ
			List<IGrpEntity> grpEntitys = umFinderSupport.findAllGroup();
			paramBean.setGroupViewBeanList( getGroupList( grpEntitys, paramBean ) );

			paramBean.setGroupSelected( paramBean.getGroupSelected() ) ;

			// パスワード有効期間
			setPasswordEffectiveTerm( paramBean );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005038S, e );
		}
	}

	/**
	 * パスワード有効期間
	 * @param paramBean
	 */
	private void setPasswordEffectiveTerm( FlowUcfUserEntryServiceBean paramBean ) {

		// デフォルト値
		String defaultTerm = DesignSheetUtils.getPasswordDefaultEffectiveTerm();

		List<PasswordEffectiveTermViewBean> beanList = new ArrayList<PasswordEffectiveTermViewBean>();

		if ( !TriStringUtils.isEmpty( defaultTerm )) {
			PasswordEffectiveTermViewBean viewBean = paramBean.newTermViewBean();
			viewBean.setTerm	( defaultTerm );
			viewBean.setComment	( defaultTerm + DesignUtils.getMessageParameter( MessageParameter.DAYS.getKey() ) );

			beanList.add( viewBean );
		}



		// 固定のリスト
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap( UmDesignBeanId.passwordEffectiveTerm );

		for ( Map.Entry<String, String> entry : termMap.entrySet() ) {

			PasswordEffectiveTermViewBean viewBean = paramBean.newTermViewBean();
			viewBean.setTerm	( entry.getKey() );
			viewBean.setComment	( entry.getValue() );

			beanList.add( viewBean );
		}

		paramBean.setTermViewBeanList( beanList );
	}

	/**
	 * SectionViewBeanに情報を設定する。
	 * @param entitys
	 * @param paramBean
	 * @return
	 */
	private List<SectionViewBean> getSectionViewBeanList( List<IDeptEntity> entitys, FlowUcfUserEntryServiceBean paramBean ){

		List<SectionViewBean> sectionList = new ArrayList<SectionViewBean>();
		for ( IDeptEntity entity : entitys ) {
			SectionViewBean viewBean = paramBean.newSectionViewBean();

			viewBean.setSectionId	( entity.getDeptId() );
			viewBean.setSectionName	( entity.getDeptNm() );

			sectionList.add( viewBean );
		}
		return sectionList;
	}

	/**
	 * GroupListViewBeanに情報を設定する。
	 * @param grpEntitys
	 * @param paramBean
	 * @return
	 */
	private List<GroupListViewBean> getGroupList( List<IGrpEntity> grpEntitys, FlowUcfUserEntryServiceBean paramBean ) {

		List<GroupListViewBean> groupList = new ArrayList<GroupListViewBean>();
		for ( IGrpEntity entity : grpEntitys ) {
			GroupListViewBean viewBean = paramBean.newGroupListViewBean();

			viewBean.setGroupId(entity.getGrpId().toString());
			viewBean.setGroupName(entity.getGrpNm());

			groupList.add( viewBean );
		}
		return groupList;
	}
}
