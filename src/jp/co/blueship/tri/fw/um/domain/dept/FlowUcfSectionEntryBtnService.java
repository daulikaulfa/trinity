package jp.co.blueship.tri.fw.um.domain.dept;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils.Mode;
import jp.co.blueship.tri.fw.um.dao.dept.DeptNumberingDao;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionEntryBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfSectionEntryBtnService implements IDomain<FlowUcfSectionEntryBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDeptDao deptDao;
	public void setDeptDao( IDeptDao deptDao ) {
		this.deptDao = deptDao;
	}

	private DeptNumberingDao deptNumberingDao;
	public void setDeptNumberingDao( DeptNumberingDao deptNumberingDao ) {
		this.deptNumberingDao = deptNumberingDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionEntryBtnServiceBean> execute( IServiceDto<FlowUcfSectionEntryBtnServiceBean> serviceDto ) {

		FlowUcfSectionEntryBtnServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			// 部署ID採番
			// 既存のデータを考慮し、重複がないように採番する
			String deptId = null;
			for (;;) {
				deptId = deptNumberingDao.nextval();
				if ( umFinderSupport.checkDuplicationByNumberingDeptId( deptId ) ) {
					break;
				}
			}

			paramBean.setInSectionId( deptId );

			// 入力チェック
			List<IDeptEntity> entities = umFinderSupport.findAllDept();
			UmItemChkUtils.checkUcfInputSection( paramBean.getInSectionId(), paramBean.getInSectionName() , null , entities , Mode.INSERT );

			//レコード追加
			insertDeptDao( paramBean.getInSectionId(), paramBean.getInSectionName() );

			//追加後再検索
			entities = umFinderSupport.findAllDept();
			//画面表示用のリストを作成
			this.makeSectionViewBeanList( paramBean , entities ) ;

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005004S, e );
		}
	}
	/**
	 *
	 * @param paramBean
	 * @param entities
	 * @param newEntity
	 */
	private void makeSectionViewBeanList( FlowUcfSectionEntryBtnServiceBean paramBean , List<IDeptEntity> entities ) {

		List<SectionListViewBean> list = new ArrayList<SectionListViewBean>();

		for ( IDeptEntity entity : entities ) {
			SectionListViewBean viewBean = paramBean.newListViewBean();
			viewBean.setSectionId	( entity.getDeptId() );
			viewBean.setSectionName	( entity.getDeptNm() );
			list.add( viewBean );
		}

		paramBean.setViewBeanList( list );
	}

	/**
	 *
	 * @param deptId
	 * @param deptNm
	 */
	private void insertDeptDao( String deptId, String deptNm ) {

		IDeptEntity insertEntity = new DeptEntity();
		insertEntity.setDeptId( deptId );
		insertEntity.setDeptNm( deptNm );
		deptDao.insert(insertEntity);
	}
}
