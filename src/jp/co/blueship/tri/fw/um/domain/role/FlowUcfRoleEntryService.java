package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriRuntimeException;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.IFuncSvcDao;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;

public class FlowUcfRoleEntryService implements IDomain<FlowUcfRoleEntryServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFuncSvcDao funcSvcDao = null;
	public void setFuncSvcDao( IFuncSvcDao actionBasicDao ) {
		this.funcSvcDao = actionBasicDao;
	}

	@Override
	public IServiceDto<FlowUcfRoleEntryServiceBean> execute( IServiceDto<FlowUcfRoleEntryServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfRoleEntryServiceBean paramBean = null;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			FuncSvcCondition condition = new FuncSvcCondition();
			List<IFuncSvcEntity> actionEntitys = funcSvcDao.find( condition.getCondition() );

			List<ActionListViewBean> actionList = new ArrayList<ActionListViewBean>();

			for ( IFuncSvcEntity entity : actionEntitys ) {
				ActionListViewBean viewBean = paramBean.newActionListViewBean();

				viewBean.setActionId(entity.getSvcId());
				viewBean.setActionName(entity.getSvcNm());

				actionList.add( viewBean );
			}

			paramBean.setActionViewBeanList(actionList);

			return serviceDto;

		} catch ( TriRuntimeException be ) {
			throw be;
		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005031S, e );
		}
	}


}
