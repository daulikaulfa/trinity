package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean.UserListViewBean;

public class FlowUcfGroupUserModifyBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	/** グループID **/
	private String inGroupId   		  = null;
	private String inGroupName   		  = null;
	private List<UserListViewBean> userViewBeanList = new ArrayList<UserListViewBean>();

	/** 選択ユーザID **/
	private List<String> inUserList;

	/**
	 * 一覧情報
	 */
	private List<GroupListViewBean> groupViewBeanList = new ArrayList<GroupListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 *
	 * @return 取得した情報を戻します。
	 */
	public GroupListViewBean newListViewBean() {
		GroupListViewBean bean = new FlowUcfGroupListServiceBean().newListViewBean();
		return bean;
	}
	public UserListViewBean newUserListViewBean() {
		UserListViewBean bean = new FlowUcfGroupUserModifyServiceBean().newUserListViewBean();

		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 *
	 * @return 取得したリストを戻します。
	 */
	public List<GroupListViewBean> getViewBeanList() {
		return groupViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 *
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<GroupListViewBean> list) {
		this.groupViewBeanList = list;
	}

	public List<String> getInUserList() {
		return inUserList;
	}

	public void setInUserList(List<String> inUserList) {
		this.inUserList = inUserList;
	}

	public String getInGroupId() {
		return inGroupId;
	}

	public void setInGroupId(String inGroupId) {
		this.inGroupId = inGroupId;
	}

	public String getInGroupName() {
		return inGroupName;
	}

	public void setInGroupName(String inGroupName) {
		this.inGroupName = inGroupName;
	}

	public List<UserListViewBean> getUserViewBeanList() {
		return userViewBeanList;
	}

	public void setUserViewBeanList(List<UserListViewBean> userViewBeanList) {
		this.userViewBeanList = userViewBeanList;
	}
}
