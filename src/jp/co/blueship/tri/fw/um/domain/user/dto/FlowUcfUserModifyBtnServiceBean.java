package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserEntryServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;

public class FlowUcfUserModifyBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ユーザID **/
	private String inUserId   		  = null;
	/** ユーザ名 **/
	private String inUserName 		  = null;
	/** パスワード **/
	private String inPassword 		  = null;
	/** 確認用パスワード **/
	private String inRepassword	  = null;
	/** 旧パスワード **/
	private String orgRepassword	  = null;
	/** パスワード有効期限 **/
	private String inPasswordEffectiveTerm = null;
	/** メールアドレス **/
	private String inMailAddress 	  = null;
	/** 部署ID **/
	private String inSectionId       = null;
	/** 部署名 **/
	private String inSectionName     = null;
	/** 選択グループID **/	
	private List<String> inGroupList;
	/**
	 * 一覧情報
	 */
	private List<UserListViewBean> userViewBeanList;
	private List<GroupListViewBean> groupViewBeanList;
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public UserListViewBean newListViewBean() {
		UserListViewBean bean = new FlowUcfUserListServiceBean().newListViewBean();
		return bean;
	}
	
	public GroupListViewBean newGroupListViewBean() {
		GroupListViewBean bean = new FlowUcfUserEntryServiceBean().newGroupListViewBean();
		
		return bean;
	}
	
	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<UserListViewBean> getViewBeanList() {
		return userViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<UserListViewBean> list) {
		this.userViewBeanList = list;
	}

	public String getInMailAddress() {
		return inMailAddress;
	}

	public void setInMailAddress(String inMailAddress) {
		this.inMailAddress = inMailAddress;
	}

	public String getInPassword() {
		return inPassword;
	}

	public void setInPassword(String inPassword) {
		this.inPassword = inPassword;
	}

	public String getInRepassword() {
		return inRepassword;
	}

	public void setInRepassword(String inRepassword) {
		this.inRepassword = inRepassword;
	}
	
	public String getOrgRepassword() {
		return orgRepassword;
	}

	public void setOrgRepassword( String orgRepassword ) {
		this.orgRepassword = orgRepassword;
	}

	public String getInPasswordEffectiveTerm() {
		return inPasswordEffectiveTerm;
	}

	public void setInPasswordEffectiveTerm( String inPasswordEffectiveTerm ) {
		this.inPasswordEffectiveTerm = inPasswordEffectiveTerm;
	}
	
	public String getInSectionId() {
		return inSectionId;
	}

	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}

	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}

	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String inUserName) {
		this.inUserName = inUserName;
	}

	public List<String> getInGroupList() {
		return inGroupList;
	}

	public void setInGroupList(List<String> inGroupList) {
		this.inGroupList = inGroupList;
	}
	
	public List<UserListViewBean> getUserViewBeanList() {
		return userViewBeanList;
	}

	public void setUserViewBeanList(List<UserListViewBean> userViewBeanList) {
		this.userViewBeanList = userViewBeanList;
	}

	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}

	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}

	public String getInSectionName() {
		return inSectionName;
	}

	public void setInSectionName(String inSectionName) {
		this.inSectionName = inSectionName;
	}

}
