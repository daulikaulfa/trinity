package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;

public class FlowUcfUserDeleteBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** ユーザID **/
	private String inUserId   		  = null;

	/**
	 * 一覧情報
	 */
	private List<UserListViewBean> userViewBeanList = new ArrayList<UserListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public UserListViewBean newListViewBean() {
		UserListViewBean bean = new FlowUcfUserListServiceBean().newListViewBean();
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<UserListViewBean> getViewBeanList() {
		return userViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<UserListViewBean> list) {
		this.userViewBeanList = list;
	}

	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}
	
}
