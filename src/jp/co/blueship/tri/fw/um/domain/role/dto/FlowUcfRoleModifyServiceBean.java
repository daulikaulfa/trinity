package jp.co.blueship.tri.fw.um.domain.role.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;

public class FlowUcfRoleModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	private String inRoleId;
	private String RoleId;
	private String inRoleName;
	private String RoleName;
	private List<ActionListViewBean> actionViewBeanList = new ArrayList<ActionListViewBean>();
	private List<String> actionSelected = new ArrayList<String>();
	
	public String getInRoleId() {
		return inRoleId;
	}
	public void setInRoleId(String inRoleId) {
		this.inRoleId = inRoleId;
	}
	public String getRoleId() {
		return RoleId;
	}
	public void setRoleId(String roleId) {
		RoleId = roleId;
	}
	public String getInRoleName() {
		return inRoleName;
	}
	public void setInRoleName(String roleName) {
		inRoleName = roleName;
	}
	public String getRoleName() {
		return RoleName;
	}
	public void setRoleName(String roleName) {
		RoleName = roleName;
	}
	public List<String> getActionSelected() {
		return actionSelected;
	}
	public void setActionSelected(List<String> actionSelected) {
		this.actionSelected = actionSelected;
	}
	public List<ActionListViewBean> getActionViewBeanList() {
		return actionViewBeanList;
	}
	public void setActionViewBeanList(List<ActionListViewBean> actionViewBeanList) {
		this.actionViewBeanList = actionViewBeanList;
	}
	public ActionListViewBean newListViewBean() {
		ActionListViewBean bean = new FlowUcfRoleEntryServiceBean().newActionListViewBean();
		
		return bean;
	}
	
}
