package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfUserListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 一覧情報
	 */
	private List<UserListViewBean> userViewBeanList = new ArrayList<UserListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public UserListViewBean newListViewBean() {
		UserListViewBean bean = new UserListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<UserListViewBean> getViewBeanList() {
		return userViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<UserListViewBean> list) {
		this.userViewBeanList = list;
	}
	
	public class UserListViewBean {
		
		/** ユーザID **/
		private String userId   		  = null;
		/** ユーザ名 **/
		private String userName 		  = null;
		/** パスワード **/
		private String password 		  = null;
		/** 確認用パスワード **/
		private String repassword 		  = null;
		/** メールアドレス **/
		private String mailAddress 	  = null;
		/** 部署ID **/
		private String sectionId 	      = null;
		/** 部署名 **/
		private String sectionName 	  = null;
		
		public String getMailAddress() {
			return mailAddress;
		}
		public void setMailAddress(String mailAddress) {
			this.mailAddress = mailAddress;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getRepassword() {
			return repassword;
		}
		public void setRepassword(String repassword) {
			this.repassword = repassword;
		}
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
		public String getSectionId() {
			return sectionId;
		}
		public void setSectionId(String sectionId) {
			this.sectionId = sectionId;
		}
		public String getSectionName() {
			return sectionName;
		}
		public void setSectionName(String sectionName) {
			this.sectionName = sectionName;
		}
	}
}
