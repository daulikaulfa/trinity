package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfGroupListServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/**
	 * 一覧情報
	 */
	private List<GroupListViewBean> groupViewBeanList = new ArrayList<GroupListViewBean>();
	private List<SelectedListViewBean> selectedViewBeanList = new ArrayList<SelectedListViewBean>();

	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public GroupListViewBean newListViewBean() {
		GroupListViewBean bean = new GroupListViewBean();
		
		return bean;
	}

	public SelectedListViewBean newSelectedListViewBean() {
		SelectedListViewBean bean = new SelectedListViewBean();
		
		return bean;
	}
	
	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<GroupListViewBean> getViewBeanList() {
		return groupViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<GroupListViewBean> list) {
		this.groupViewBeanList = list;
	}
	
	public class GroupListViewBean {
		
		/** グループID **/
		private String groupId   		  = null;
		/** グループ名 **/
		private String groupName 		  = null;
		
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getGroupName() {
			return groupName;
		}
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
	}

	public List<SelectedListViewBean> getSelectedViewBeanList() {
		return selectedViewBeanList;
	}
	
	public void setSelectedViewBeanList(List<SelectedListViewBean> list) {
		this.selectedViewBeanList = list;
	}
	
	public class SelectedListViewBean {
		
		/** ユーザID **/
		private String userId   		  = null;
		/** ユーザ名 **/
		private String userName 		  = null;
		
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
	}
}
