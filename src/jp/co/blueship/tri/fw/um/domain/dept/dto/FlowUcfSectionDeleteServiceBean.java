package jp.co.blueship.tri.fw.um.domain.dept.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfSectionDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	private String inSectionId;
	private String SectionId;
	private String SectionName;
	
	public String getInSectionId() {
		return inSectionId;
	}
	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}
	public String getSectionId() {
		return SectionId;
	}
	public void setSectionId(String sectionId) {
		SectionId = sectionId;
	}
	public String getSectionName() {
		return SectionName;
	}
	public void setSectionName(String sectionName) {
		SectionName = sectionName;
	}
}
