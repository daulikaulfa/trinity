package jp.co.blueship.tri.fw.um.domain.user;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpUserLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserDeleteBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfUserDeleteBtnService implements IDomain<FlowUcfUserDeleteBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpUserLnkDao grpUserLnkDao;
	public void setGrpUserLnkDao(IGrpUserLnkDao grpUserLnkDao) {
		this.grpUserLnkDao = grpUserLnkDao;
	}

	private IUserDao userDao;
	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao(IUserRoleLnkDao userRoleLnkDao) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	private LoginSession loginSession;

	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	@Override
	public IServiceDto<FlowUcfUserDeleteBtnServiceBean> execute( IServiceDto<FlowUcfUserDeleteBtnServiceBean> serviceDto ) {

		FlowUcfUserDeleteBtnServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();

			if (null == paramBean){
				throw new TriSystemException(UmMessageId.UM005001S);
			}
			UmItemChkUtils.checkUcfAdminUser(paramBean.getInUserId());
			UmItemChkUtils.checkUcfLoginUser(paramBean.getInUserId(), paramBean.getUserId());
			if (loginSession.isLoggedInUser(paramBean.getInUserId())) {
				throw new ContinuableBusinessException( UmMessageId.UM001029E);
			}

			deleteUserDao( paramBean.getInUserId() );
			deleteUserRoleLnkDao( paramBean.getInUserId() );

			String uId = paramBean.getInUserId();
			List<IGrpUserLnkEntity> groupUserEntitys = umFinderSupport.findGrpUserLnkByUserId( uId );

			for (IGrpUserLnkEntity view : groupUserEntitys) {
				deleteGrpUserLnkDao(view.getGrpId(), uId);
			}

			List<IUserEntity> entitys = umFinderSupport.findAllUser();
			paramBean.setViewBeanList( setViewBean( entitys, paramBean ) );

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005035S, e);
		}
	}

	private void deleteUserDao( String userId ) {

		UserCondition condition = new UserCondition();
		condition.setUserId( userId );

		userDao.delete( condition.getCondition() );

	}

	private void deleteUserRoleLnkDao( String userId ) {
		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setUserId(userId);

		userRoleLnkDao.delete(condition.getCondition());
	}

	private void deleteGrpUserLnkDao( String grpId, String userId ) {
		GrpUserLnkCondition condition = new GrpUserLnkCondition();
		condition.setGrpId(grpId);
		condition.setUserId(userId);

		grpUserLnkDao.delete(condition.getCondition());
	}

	private List<UserListViewBean> setViewBean( List<IUserEntity> entitys, FlowUcfUserDeleteBtnServiceBean paramBean) {

		List<UserListViewBean> list = new ArrayList<UserListViewBean>();

		for (IUserEntity entity : entitys) {
			UserListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setUserId( entity.getUserId() );
			viewBean.setUserName( entity.getUserNm() );
			viewBean.setPassword( entity.getUserPass() );
			viewBean.setMailAddress( entity.getEmailAddr() );
			viewBean.setSectionId( entity.getDeptId() );

			list.add(viewBean);
		}
		return list;
	}
}
