package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfGroupDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** グループID **/
	private String inGroupId   	  = null;
	private String groupId   		  = null;
	/** グループ名 **/
	private String groupName 		  = null;
	/** ユーザID **/
	private String inUserId   		  = null;
	/** ユーザ名 **/
	private String inUserName 		  = null;
	/** 選択ユーザリスト **/
	private List<String> userSelected = new ArrayList<String>();
	
	public List<String> getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(List<String> userSelected) {
		this.userSelected = userSelected;
	}

	public String getInGroupId() {
		return inGroupId;
	}

	public void setInGroupId(String inId) {
		this.inGroupId = inId;
	}

	public String getGroupId() {
		return groupId;
	}

	public void setGroupId(String id) {
		groupId = id;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String name) {
		groupName = name;
	}

	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}
	
	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String userName) {
		inUserName = userName;
	}
	
}
