package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.role.IRoleDao;
import jp.co.blueship.tri.fw.um.dao.role.IRoleSvcCtgLnkDao;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.IUserRoleLnkDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleDeleteBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


public class FlowUcfRoleDeleteBtnService implements IDomain<FlowUcfRoleDeleteBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IRoleDao roleDao;
	public void setRoleDao( IRoleDao roleDao ) {
		this.roleDao = roleDao;
	}

	private IRoleSvcCtgLnkDao roleSvcCtgLnkDao;
	public void setRoleSvcCtgLnkDao( IRoleSvcCtgLnkDao roleSvcCtgLnkDao ) {
		this.roleSvcCtgLnkDao = roleSvcCtgLnkDao;
	}

	private IGrpRoleLnkDao grpRoleLnkDao;
	public void setGrpRoleLnkDao( IGrpRoleLnkDao grpRoleLnkDao ) {
		this.grpRoleLnkDao = grpRoleLnkDao;
	}

	private IUserRoleLnkDao userRoleLnkDao;
	public void setUserRoleLnkDao( IUserRoleLnkDao userRoleLnkDao ) {
		this.userRoleLnkDao = userRoleLnkDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}
	@Override
	public IServiceDto<FlowUcfRoleDeleteBtnServiceBean> execute( IServiceDto<FlowUcfRoleDeleteBtnServiceBean> serviceDto ) {

		//画面の入力値を取得
		FlowUcfRoleDeleteBtnServiceBean paramBean;

		try {
			//型を変更
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			String inRoleId = paramBean.getInRoleId() ;

			//削除してもよいロールかチェック：管理者のロールかどうか
			checkRole(inRoleId, UmItemChkUtils.ADMIN, UmMessageId.UM001040E );
			//削除してもよいロールかチェック：ログイン中ユーザのロールかどうか
			checkRole(inRoleId, paramBean.getUserId(), UmMessageId.UM001039E );

			// 削除
			deleteRoleDao( inRoleId );
			deleteRoleSvcCtgLnkDao(inRoleId);
			deleteGrpRoleDao( inRoleId );
			deleteUserRoleLnkDao( inRoleId );

			// DB全件検索
			List<IRoleEntity> entityList = umFinderSupport.findAllRole();
			// for文でlistに格納したBeanをparamBeanに格納する
			paramBean.setViewBeanList( getRoleList( entityList, paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005028S, e );
		}
	}
	/**
	 * 削除しても良いロールかチェックする。
	 * @param inRoleId
	 * @param userId
	 * @param messageId
	 */
	private void checkRole(String inRoleId, String userId, IMessageId messageId) {

		List<IUserRoleLnkEntity> userRoleLnkEntityList = umFinderSupport.findUserRoleLnkByUserId( userId );

		for( IUserRoleLnkEntity userRoleEntity : userRoleLnkEntityList ) {
			if( inRoleId.equals( userRoleEntity.getRoleId() ) ) {
				throw new ContinuableBusinessException( messageId );
			}
		}
	}
	/**
	 * データベースに登録されている内容をRoleListViewBeanに設定して、取得します。
	 * @param entityList
	 * @param paramBean
	 * @return RoleListViewBeanのリスト
	 */
	private List<RoleListViewBean> getRoleList(List<IRoleEntity> entityList, FlowUcfRoleDeleteBtnServiceBean paramBean) {

		// DBに登録されている内容をListに格納する
		List<RoleListViewBean> list = new ArrayList<RoleListViewBean>();

		// インスタンス(器)を作成し登録されたデータの件数分データを格納する
		for ( IRoleEntity entity : entityList ) {
			RoleListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setRoleId(entity.getRoleId());
			viewBean.setRoleName(entity.getRoleName());

			list.add( viewBean );
		}
		return list;
	}

	/**
	 * ロールIDに該当するRoleDaoを削除します。
	 * @param inRoleId
	 */
	private void deleteRoleDao( String inRoleId ) {

		RoleCondition condition = new RoleCondition();
		condition.setRoleId(inRoleId);
		roleDao.delete( condition.getCondition() );
	}

	/**
	 * ロールIDに該当するgrpRoleLnkDaoを削除します。
	 * @param inRoleId
	 */
	private void deleteGrpRoleDao( String inRoleId ) {

		GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
		condition.setRoleId(inRoleId);
		grpRoleLnkDao.delete( condition.getCondition() );
	}

	/**
	 * ロールIDに該当するroleSvcCtgLnkDaoを削除します。
	 * @param inRoleId
	 */
	private void deleteRoleSvcCtgLnkDao( String inRoleId ) {

		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		condition.setRoleId(inRoleId);
		roleSvcCtgLnkDao.delete(condition.getCondition());
	}

	/**
	 * ロールIDに該当するUserRoleLnkDaoを削除します。
	 * @param roleId
	 */
	private void deleteUserRoleLnkDao( String roleId ) {

		UserRoleLnkCondition condition = new UserRoleLnkCondition();
		condition.setRoleId(roleId);
		userRoleLnkDao.delete( condition.getCondition() );
	}
}
