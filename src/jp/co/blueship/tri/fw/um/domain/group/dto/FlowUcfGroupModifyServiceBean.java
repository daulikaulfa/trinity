package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;

public class FlowUcfGroupModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** グループID **/
	private String inGroupId   		  = null;
	private String GroupId   		  = null;
	/** グループ名 **/
	private String GroupName 		  = null;
	private String inGroupName 		  = null;
	/** ロールID **/
	private String RoleId 		  = null;
	private String inRoleId 		  = null;
	/** ロール名 **/
	private String RoleName 		  = null;
	private String inRoleName 		  = null;
	
	private List<String> sectionViewList;
	private List<String> roleSelected = new ArrayList<String>();
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();
	
	public List<String> getSectionViewList() {
		return sectionViewList;
	}

	public void setSectionViewList(List<String> sectionViewList) {
		this.sectionViewList = sectionViewList;
	}

	public String getInGroupId() {
		return inGroupId;
	}

	public void setInGroupId(String inGroupId) {
		this.inGroupId = inGroupId;
	}

	public String getGroupId() {
		return GroupId;
	}

	public void setGroupId(String groupId) {
		GroupId = groupId;
	}

	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}

	public String getInGroupName() {
		return inGroupName;
	}

	public void setGroupInName(String groupName) {
		inGroupName = groupName;
	}

	public String getInRoleId() {
		return inRoleId;
	}

	public void setInRoleId(String inRoleId) {
		this.inRoleId = inRoleId;
	}

	public String getroleId() {
		return RoleId;
	}

	public void setroleId(String roleId) {
		RoleId = roleId;
	}

	public String getRoleName() {
		return RoleName;
	}

	public void setRoleName(String roleName) {
		RoleName = roleName;
	}

	public String getInroleName() {
		return inRoleName;
	}

	public void setGroupInRole(String roleName) {
		inRoleName = roleName;
	}
	
	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}
	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}
	public RoleListViewBean newListViewBean() {
		RoleListViewBean bean = new FlowUcfGroupEntryServiceBean().newListViewBean();
		
		return bean;
	}
	
	public List<String> getRoleSelected() {
		return roleSelected;
	}
	public void setRoleSelected(List<String> roleSelected) {
		this.roleSelected = roleSelected;
	}
		
}
