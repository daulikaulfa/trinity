package jp.co.blueship.tri.fw.um.domain.dept;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.dept.IDeptDao;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.user.IUserDao;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionDeleteBtnServiceBean;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionListServiceBean.SectionListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfSectionDeleteBtnService implements IDomain<FlowUcfSectionDeleteBtnServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IDeptDao deptDao;
	public void setDeptDao( IDeptDao deptDao ) {
		this.deptDao = deptDao;
	}

	private IUserDao userDao;
	public void setUserDao( IUserDao userDao ) {
		this.userDao = userDao;
	}

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionDeleteBtnServiceBean> execute( IServiceDto<FlowUcfSectionDeleteBtnServiceBean> serviceDto ) {

		FlowUcfSectionDeleteBtnServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			String inSectionId = paramBean.getInSectionId() ;

			//削除してもよい部署かチェック：管理者の部署かどうか
			IUserEntity userBasicEntity = umFinderSupport.findUserByUserId( UmItemChkUtils.ADMIN );
			if( inSectionId.equals( String.valueOf( userBasicEntity.getDeptId() ) ) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001042E );
			}
			//削除してもよい部署かチェック：ログイン中ユーザの部署かどうか
			IUserEntity userLoginEntity = umFinderSupport.findUserByUserId( paramBean.getUserId() ) ;
			if( inSectionId.equals( String.valueOf( userLoginEntity.getDeptId() ) ) ) {
				throw new ContinuableBusinessException( UmMessageId.UM001041E );
			}

			//部署に紐付くユーザの部署ＩＤを消す
			List<IUserEntity> userEntityList = umFinderSupport.findUserByDept( inSectionId );
			for( IUserEntity userEntity : userEntityList ) {
				userEntity.setDeptId( null ) ;
				userDao.update( userEntity ) ;
			}

			//部署の削除
			deleteDeptDao( inSectionId );

			paramBean.setViewBeanList( setSectionList( umFinderSupport.findAllDept(), paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005002S, e);
		}
	}


	/**
	 * 部署情報をSectionListViewBeanへ設定します。
	 * @param entitys
	 * @param paramBean
	 * @return
	 */
	private List<SectionListViewBean> setSectionList( List<IDeptEntity> entitys, FlowUcfSectionDeleteBtnServiceBean paramBean) {

		List<SectionListViewBean> list = new ArrayList<SectionListViewBean>();

		for ( IDeptEntity entity : entitys ) {
			SectionListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setSectionId( entity.getDeptId() );
			viewBean.setSectionName( entity.getDeptNm() );

			list.add( viewBean );
		}
		return list;
	}

	/**
	 * 指定した部署IDのDeptDaoを削除します。
	 * @param deptId
	 */
	private void deleteDeptDao( String deptId ) {

		DeptCondition condition = new DeptCondition();
		condition.setDeptId(deptId);
		deptDao.delete( condition.getCondition() );

	}
}
