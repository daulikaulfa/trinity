package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupEntryServiceBean.RoleListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;

public class FlowUcfGroupModifyBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	/** グループID **/
	private String inGroupId   		  = null;
	/** グループ名 **/
	private String inGroupName 		  = null;
	/** 選択ロールID **/
	private List<String> inRoleList;
	/**
	 * 一覧情報
	 */
	private List<GroupListViewBean> groupViewBeanList = new ArrayList<GroupListViewBean>();
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public GroupListViewBean newListViewBean() {
		GroupListViewBean bean = new FlowUcfGroupListServiceBean().newListViewBean();
		return bean;
	}
	
	public RoleListViewBean newRoleListViewBean() {
		RoleListViewBean bean = new FlowUcfGroupEntryServiceBean().newListViewBean();
		
		return bean;
	}

	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<GroupListViewBean> getViewBeanList() {
		return groupViewBeanList;
	}

	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<GroupListViewBean> list) {
		this.groupViewBeanList = list;
	}

	public String getInGroupId() {
		return inGroupId;
	}

	public void setInGroupId(String inGroupId) {
		this.inGroupId = inGroupId;
	}

	public String getInGroupName() {
		return inGroupName;
	}

	public void setInGroupName(String inGroupName) {
		this.inGroupName = inGroupName;
	}

	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}

	public List<String> getInRoleList() {
		return inRoleList;
	}

	public void setInRoleList(List<String> inRoleList) {
		this.inRoleList = inRoleList;
	}
	
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}

	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}

	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}
}
