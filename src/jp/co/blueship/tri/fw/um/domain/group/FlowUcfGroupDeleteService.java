package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupDeleteServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupDeleteService implements IDomain<FlowUcfGroupDeleteServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupDeleteServiceBean> execute( IServiceDto<FlowUcfGroupDeleteServiceBean> serviceDto ) {

		FlowUcfGroupDeleteServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			IGrpEntity entity = umFinderSupport.findGroupById( paramBean.getInGroupId() );

			paramBean.setGroupId( entity.getGrpId() ) ;
			paramBean.setGroupName( entity.getGrpNm() ) ;

			List<IGrpUserLnkEntity> grpUserLnkEntity = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInGroupId() );
			paramBean.setUserSelected( getUserList( grpUserLnkEntity ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005010S, e );
		}
	}

	private List<String> getUserList( List<IGrpUserLnkEntity> grpUserLnkEntity ) {

		List<String> userList = new ArrayList<String>();
		for ( IGrpUserLnkEntity entity: grpUserLnkEntity ) {
			IUserEntity userEntity = umFinderSupport.findUserByUserId( entity.getUserId() );
			if( TriStringUtils.isNotEmpty( userEntity ) ) {
				userList.add(userEntity.getUserNm());
			}
		}
		return userList;
	}
}
