package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.FuncSvcCondition;
import jp.co.blueship.tri.fw.sm.dao.funcsvc.eb.IFuncSvcEntity;
import jp.co.blueship.tri.fw.sm.support.ISmFinderSupport;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgSvcLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleModifyServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfRoleModifyService implements IDomain<FlowUcfRoleModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	private ISmFinderSupport smFinderSupport;
	public void setSmFinderSupport( ISmFinderSupport smFinderSupport ) {
		this.smFinderSupport = smFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfRoleModifyServiceBean> execute( IServiceDto<FlowUcfRoleModifyServiceBean> serviceDto ) {

		FlowUcfRoleModifyServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			if ( TriStringUtils.isNotEmpty(paramBean.getInRoleId()) ) {
				paramBean.setActionSelected(getRoleSvcIdList(paramBean.getInRoleId()));
			}

			setParamRoleIdRoleNm(paramBean);
			paramBean.setActionViewBeanList( getActionListViewBean(paramBean) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005034S, e );
		}
	}

	/**
	 * ロールIDからサービスIDのリストを取得します。
	 * @param roleSvcCtgLnkEntityList
	 * @return サービスIDのリスト
	 */
	private List<String> getRoleSvcIdList(String roleId) {

		List<IRoleSvcCtgLnkEntity> roleSvcCtgLnkEntityList = umFinderSupport.findSvcCtgLnkByRoleId( roleId );
		List<String> roleActionList = new ArrayList<String>();
		for ( IRoleSvcCtgLnkEntity entity : roleSvcCtgLnkEntityList ) {
			SvcCtgCondition svcCondition = new SvcCtgCondition();
			svcCondition.setSvcCtgId(entity.getSvcCtgId());
			ISvcCtgEntity svcEntity = umFinderSupport.getSvcCtgDao().findByPrimaryKey(svcCondition.getCondition());
			if( svcEntity!=null ) {
				if( svcEntity.getSvcCtgPath()==null || svcEntity.getSvcCtgPath().isEmpty() ) {
					List<ISvcCtgSvcLnkEntity> svcCtgSvcLnkEntity = umFinderSupport.findSvcCtgSvcLnkBySvcCtgId( entity.getSvcCtgId() ) ;
					roleActionList.addAll( TriCollectionUtils.collect(svcCtgSvcLnkEntity, toSvcId()) ) ;
				}
			}
		}
		return roleActionList;
	}

	/**
	 * FlowUcfRoleModifyServiceBeanにロールIDとロール名を設定します。
	 * @param paramBean
	 */
	private void setParamRoleIdRoleNm( FlowUcfRoleModifyServiceBean paramBean ) {

		if (paramBean.getInRoleName() == null) {
			IRoleEntity entity = umFinderSupport.findRoleByRoleId( paramBean.getInRoleId() );

			paramBean.setRoleId(entity.getRoleId()) ;
			paramBean.setRoleName(entity.getRoleName()) ;
		} else {
			paramBean.setRoleId(paramBean.getInRoleId());
			paramBean.setInRoleName(paramBean.getInRoleName());
		}

	}
	/**
	 * サービスIDとサービス名を設定したActionListViewBeanを取得します。
	 */
	private List<ActionListViewBean> getActionListViewBean( FlowUcfRoleModifyServiceBean paramBean ) {

		List<IFuncSvcEntity> actionEntitys = smFinderSupport.getFuncSvcDao().find(new FuncSvcCondition().getCondition());;
		List<ActionListViewBean> actionList = new ArrayList<ActionListViewBean>();

		for ( IFuncSvcEntity entity : actionEntitys ) {
			SvcCtgCondition svcCondition = new SvcCtgCondition();
			svcCondition.setSvcCtgId(entity.getSvcId());
			ISvcCtgEntity svcEntity = umFinderSupport.getSvcCtgDao().findByPrimaryKey(svcCondition.getCondition());
			if( svcEntity!=null ) {
				if( svcEntity.getSvcCtgPath()==null || svcEntity.getSvcCtgPath().isEmpty() ) {
					ActionListViewBean viewBean = paramBean.newListViewBean();

					viewBean.setActionId(entity.getSvcId());
					viewBean.setActionName(entity.getSvcNm());

					actionList.add( viewBean );
				}
			}
		}
		return actionList;
	}
	/**
	 * サービスカテゴリ・サービス(Lnk)エンティティリストからサービスIdを取り出すファンクションメソッドです。
	 */
	private TriFunction<ISvcCtgSvcLnkEntity, String> toSvcId() {
		return new TriFunction<ISvcCtgSvcLnkEntity, String>() {

			@Override
			public String apply(ISvcCtgSvcLnkEntity entity) {
				return entity.getSvcId();
			}
		};
	}
}
