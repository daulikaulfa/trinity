package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfGroupRoleEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	private List<GroupListViewBean> groupViewBeanList= new ArrayList<GroupListViewBean>();
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();
	
	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	public List<RoleListViewBean> getRoleViewBeanList() {
		return roleViewBeanList;
	}
	public void setRoleViewBeanList(List<RoleListViewBean> roleViewBeanList) {
		this.roleViewBeanList = roleViewBeanList;
	}
	public RoleListViewBean newListViewBean() {
		RoleListViewBean bean = new RoleListViewBean();
		
		return bean;
	}

	public GroupListViewBean newGroupListViewBean() {
		GroupListViewBean bean = new GroupListViewBean();
		
		return bean;
	}
	public class RoleListViewBean {
	
		/** ロールID **/
		private String roleId   		  = null;
		/** ロール名 **/
		private String roleName 		  = null;
		public String getRoleId() {
			return roleId;
		}
		public void setRoleId(String roleId) {
			this.roleId = roleId;
		}
		public String getRoleName() {
			return roleName;
		}
		public void setRoleName(String roleName) {
			this.roleName = roleName;
		}
	}

	public class GroupListViewBean {
		
		/** グループID **/
		private String groupId   		  = null;
		/** グループ名 **/
		private String groupName 		  = null;
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getGroupName() {
			return groupName;
		}
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
	}

}
