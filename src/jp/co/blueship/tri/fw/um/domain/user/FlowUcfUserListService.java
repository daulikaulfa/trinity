package jp.co.blueship.tri.fw.um.domain.user;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserListServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * FlowUcfSectionListServiceイベントのサービスClass
 * <br>
 * <p>
 * ユーザ一覧画面の表示情報を設定する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FlowUcfUserListService implements IDomain<FlowUcfUserListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfUserListServiceBean> execute( IServiceDto<FlowUcfUserListServiceBean> serviceDto ) {

		FlowUcfUserListServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			List<IUserEntity> entityList = umFinderSupport.findAllUser();
			paramBean.setViewBeanList( getUserList( entityList, paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005039S, e );
		}
	}

	private List<UserListViewBean> getUserList( List<IUserEntity> entityList, FlowUcfUserListServiceBean paramBean ) {

		List<UserListViewBean> list = new ArrayList<UserListViewBean>();
		for ( IUserEntity entity : entityList ) {
			UserListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setUserId( entity.getUserId() );
			viewBean.setUserName( entity.getUserNm() );
			viewBean.setPassword( entity.getUserPass() );
			viewBean.setMailAddress( entity.getEmailAddr() );
			viewBean.setSectionId(entity.getDeptId());

			if (entity.getDeptId() == null) {
				viewBean.setSectionName("");
			} else {
				viewBean.setSectionName( umFinderSupport.findDeptByDeptId( entity.getDeptId() ).getDeptNm() );
			}

			list.add( viewBean );
		}
		return list;
	}

}
