package jp.co.blueship.tri.fw.um.domain.role;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleDeleteServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfRoleDeleteService implements IDomain<FlowUcfRoleDeleteServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfRoleDeleteServiceBean> execute( IServiceDto<FlowUcfRoleDeleteServiceBean> serviceDto ) {

		FlowUcfRoleDeleteServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			IRoleEntity entity = umFinderSupport.findRoleByRoleId( paramBean.getInRoleId() );

			paramBean.setRoleId(entity.getRoleId()) ;
			paramBean.setRoleName(entity.getRoleName()) ;

			List<IGrpRoleLnkEntity> groupRoleEntity = umFinderSupport.findGrpRoleLnkByRoleId( paramBean.getInRoleId() );

			List<String> groupList = new ArrayList<String>();

			for ( IGrpRoleLnkEntity gRoleEntity : groupRoleEntity ) {

				IGrpEntity groupEntity = umFinderSupport.findGroupById( gRoleEntity.getGrpId() );
				if( TriStringUtils.isNotEmpty( groupEntity ) ) {
					groupList.add(groupEntity.getGrpNm());
				}
			}

			paramBean.setGroupSelected(groupList);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005029S, e );
		}
	}

}
