package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean.GroupListViewBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupUserModifyServiceBean.UserListViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfGroupUserModifyService implements IDomain<FlowUcfGroupUserModifyServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;

	public void setUmFinderSupport(IUmFinderSupport umFinderSupport) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfGroupUserModifyServiceBean> execute( IServiceDto<FlowUcfGroupUserModifyServiceBean> serviceDto ) {

		FlowUcfGroupUserModifyServiceBean paramBean;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}

			List<IGrpEntity> groupEntitys = umFinderSupport.findAllGroup();
			paramBean.setGroupViewBeanList( getGroupListViewBeanList( groupEntitys, paramBean ) );

			String value = paramBean.getInGroupId();
			if ( TriStringUtils.isNotEmpty(value) ) {

				IGrpEntity gEntity = umFinderSupport.findGroupById( paramBean.getInGroupId() );
				List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByGrpId(gEntity.getGrpId());

				paramBean.setInGroupId(paramBean.getInGroupId());
				if( TriStringUtils.isNotEmpty( gEntity ) ) {
					paramBean.setInGroupName(gEntity.getGrpNm());
				}

				paramBean.setUserSelected( TriCollectionUtils.collect( grpUserLnkEntityList, toUserId() ) );
			}

			List<IUserEntity> userEntitys = umFinderSupport.findAllUser();
			paramBean.setUserViewBeanList( getUserListViewBean( userEntitys, paramBean ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005021S, e );
		}
	}

	/**
	 * グループ・ユーザ(Lnk)からユーザIDを取り出すファンクションです。
	 * @return
	 */
	private TriFunction<IGrpUserLnkEntity, String> toUserId() {

		return new TriFunction<IGrpUserLnkEntity, String>() {

			@Override
			public String apply(IGrpUserLnkEntity entity) {
				return entity.getUserId();
			}
		};
	}

	private List<GroupListViewBean> getGroupListViewBeanList(List<IGrpEntity> groupEntitys, FlowUcfGroupUserModifyServiceBean paramBean){

		List<GroupListViewBean> groupList = new ArrayList<GroupListViewBean>();

		for ( IGrpEntity entity : groupEntitys ) {
			GroupListViewBean viewBean = paramBean.newGroupListViewBean();

			viewBean.setGroupId(entity.getGrpId());
			viewBean.setGroupName(entity.getGrpNm());

			groupList.add( viewBean );
		}
		return groupList;
	}

	private List<UserListViewBean> getUserListViewBean(List<IUserEntity> userEntitys, FlowUcfGroupUserModifyServiceBean paramBean){

		List<UserListViewBean> userList = new ArrayList<UserListViewBean>();

		for ( IUserEntity entity : userEntitys ) {
			UserListViewBean viewBean = paramBean.newListViewBean();

			viewBean.setUserId( entity.getUserId() );
			viewBean.setUserName( entity.getUserNm() );

			userList.add( viewBean );
		}
		return userList;
	}

}
