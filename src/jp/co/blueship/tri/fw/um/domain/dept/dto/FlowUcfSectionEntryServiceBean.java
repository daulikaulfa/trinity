package jp.co.blueship.tri.fw.um.domain.dept.dto;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfSectionEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	private String inSectionId;
	private String inSectionName;
	
	public String getInSectionId() {
		return inSectionId;
	}

	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}

	public String getInSectionName() {
		return inSectionName;
	}

	public void setInSectionName(String inSectionName) {
		this.inSectionName = inSectionName;
	}
			
}
