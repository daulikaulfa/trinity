package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfUserEntryServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** ユーザID **/
	private String inUserId   		  = null;
	/** ユーザ名 **/
	private String inUserName 		  = null;
	/** パスワード **/
	private String inPassword 		  = null;
	/** 確認用パスワード **/
	private String inRepassword	  = null;
	/** メールアドレス **/
	private String inMailAddress 	  = null;
	/** 部署ID **/
	private String inSectionId       = null;
	
	private List<GroupListViewBean> groupViewBeanList;
	private List<String> groupSelected;
	
	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	
	public GroupListViewBean newGroupListViewBean() {
		return new GroupListViewBean();
	}
		
	public String getInMailAddress() {
		return inMailAddress;
	}

	public void setInMailAddress(String inMailAddress) {
		this.inMailAddress = inMailAddress;
	}

	public String getInPassword() {
		return inPassword;
	}

	public void setInPassword(String inPassword) {
		this.inPassword = inPassword;
	}

	public String getInRepassword() {
		return inRepassword;
	}

	public void setInRepassword(String inRepassword) {
		this.inRepassword = inRepassword;
	}

	public String getInSectionId() {
		return inSectionId;
	}

	public void setInSectionId(String inSectionId) {
		this.inSectionId = inSectionId;
	}

	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}

	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String inUserName) {
		this.inUserName = inUserName;
	}

	public class GroupListViewBean {
		
		/** グループID **/
		private String groupId   		  = null;
		/** グループ名 **/
		private String groupName 		  = null;
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getGroupName() {
			return groupName;
		}
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}
	}
	
	
	/** パスワード有効期限 */
	private List<PasswordEffectiveTermViewBean> termViewBeanList;

	public List<PasswordEffectiveTermViewBean> getTermViewBeanList() {
		return termViewBeanList;
	}

	public void setTermViewBeanList( List<PasswordEffectiveTermViewBean> termViewBeanList ) {
		this.termViewBeanList = termViewBeanList;
	}
	
	public PasswordEffectiveTermViewBean newTermViewBean() {
		return new PasswordEffectiveTermViewBean();
	}
	
	/**
	 * パスワード有効期限
	 * @author inoue
	 *
	 */
	public class PasswordEffectiveTermViewBean {
		
		/** 有効期限 **/
		private String term	= null;
		/** コメント **/
		private String comment	= null;
		
		public String getTerm() {
			return term;
		}
		public void setTerm( String term ) {
			this.term = term;
		}
		public String getComment() {
			return comment;
		}
		public void setComment( String comment ) {
			this.comment = comment;
		}
	}
	
	
	/** 部署 */
	private List<SectionViewBean> sectionViewBeanList;

	public List<SectionViewBean> getSectionViewBeanList() {
		return sectionViewBeanList;
	}

	public void setSectionViewBeanList( List<SectionViewBean> sectionViewBeanList ) {
		this.sectionViewBeanList = sectionViewBeanList;
	}
	
	public SectionViewBean newSectionViewBean() {
		return new SectionViewBean();
	}
	
	/**
	 * 部署
	 * @author inoue
	 *
	 */
	public class SectionViewBean {
		
		/** ID **/
		private String sectionId	= null;
		/** 名前 **/
		private String sectionName	= null;
		
		public String getSectionId() {
			return sectionId;
		}
		public void setSectionId( String sectionId ) {
			this.sectionId = sectionId;
		}
		public String getSectionName() {
			return sectionName;
		}
		public void setSectionName( String sectionName ) {
			this.sectionName = sectionName;
		}
	}

	public List<String> getGroupSelected() {
		return groupSelected;
	}

	public void setGroupSelected(List<String> groupSelected) {
		this.groupSelected = groupSelected;
	}
	
	
}
