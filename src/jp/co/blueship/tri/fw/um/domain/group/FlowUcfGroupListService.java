package jp.co.blueship.tri.fw.um.domain.group;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.grp.IGrpDao;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean;
import jp.co.blueship.tri.fw.um.domain.group.dto.FlowUcfGroupListServiceBean.GroupListViewBean;

/**
 * FlowUcfActionListServiceイベントのサービスClass
 * <br>
 * <p>
 * ユーザ一覧画面の表示情報を設定する。
 * </p>
 *
 * <br>
 * All Rights Reserved, Copyright(c) Blueship  2008
 */
public class FlowUcfGroupListService implements IDomain<FlowUcfGroupListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IGrpDao groupDao = null;
	public void setGrpDao( IGrpDao groupDao ) {
		this.groupDao = groupDao;
	}

	@Override
	public IServiceDto<FlowUcfGroupListServiceBean> execute( IServiceDto<FlowUcfGroupListServiceBean> serviceDto ) {

		FlowUcfGroupListServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}
			GrpCondition grpCondition = new GrpCondition();
			List<IGrpEntity> entitys = groupDao.find( grpCondition.getCondition() );

			List<GroupListViewBean> list = new ArrayList<GroupListViewBean>();

			for ( IGrpEntity entity : entitys ) {
				GroupListViewBean viewBean = paramBean.newListViewBean();

				viewBean.setGroupId(entity.getGrpId());
				viewBean.setGroupName(entity.getGrpNm());

				list.add( viewBean );
			}

			paramBean.setViewBeanList(list);

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005013S , e );
		}
	}

}