package jp.co.blueship.tri.fw.um.domain.user;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.cmn.utils.function.TriFunction;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domain.user.dto.FlowUcfUserDeleteServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfUserDeleteService implements IDomain<FlowUcfUserDeleteServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}
	@Override
	public IServiceDto<FlowUcfUserDeleteServiceBean> execute( IServiceDto<FlowUcfUserDeleteServiceBean> serviceDto ) {


		FlowUcfUserDeleteServiceBean paramBean;

		try {
			paramBean = serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}

			IUserEntity userEntity = umFinderSupport.findUserByUserId( paramBean.getInUserId() );
			setParamBean( userEntity, paramBean );

			if ( TriStringUtils.isNotEmpty( userEntity.getDeptId() ) ){
				IDeptEntity deptEntity = umFinderSupport.findDeptByDeptId( userEntity.getDeptId() );
				paramBean.setSectionName( deptEntity.getDeptNm() );
			}

			List<IGrpUserLnkEntity> grpUserLnkEntityList = umFinderSupport.findGrpUserLnkByGrpId( paramBean.getInUserId() );
			paramBean.setGroupSelected( TriCollectionUtils.collect( grpUserLnkEntityList, toGrpNm() ) );

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005036S, e );
		}
	}

	private TriFunction<IGrpUserLnkEntity, String> toGrpNm() {

		return new TriFunction<IGrpUserLnkEntity, String>() {

			@Override
			public String apply(IGrpUserLnkEntity entity) {
				IGrpEntity groupEntity = umFinderSupport.findGroupById( entity.getGrpId() );
				return groupEntity.getGrpNm();
			}
		};
	}

	/**
	 * ユーザエンティティからFlowUcfUserDeleteServiceBeanへ各種データを設定します。
	 * @param userEntity
	 * @param paramBean
	 */
	private void setParamBean( IUserEntity userEntity, FlowUcfUserDeleteServiceBean paramBean) {

		paramBean.setInUserId	( userEntity.getUserId() );
		paramBean.setInUserName	( userEntity.getUserNm() );
		String password = userEntity.getUserPass();
		String dispPassword = null;
		StringBuffer buf = new StringBuffer();
		if ( password != null ) {
			// 暗号化されたパスワードが長すぎるので適当に１０文字
			for ( int i=0; i < 10; i++ ) {
				buf.append("*");
			}
			dispPassword = buf.toString();
		}
		paramBean.setPassword	( dispPassword );
		paramBean.setRepassword	( userEntity.getUserPass() );
		paramBean.setMailAddress( userEntity.getEmailAddr() );
		paramBean.setSectionId	( userEntity.getDeptId() );

	}
}
