package jp.co.blueship.tri.fw.um.domain.group.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfGroupUserModifyServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;

	

	private String inGroupId   		  = null;
	private String inGroupName   		  = null;
	private List<UserListViewBean> userViewBeanList = new ArrayList<UserListViewBean>();
	private List<GroupListViewBean> groupViewBeanList = new ArrayList<GroupListViewBean>();
	private List<UserListViewBean> preservedUserViewBeanList = null ;
	private List<UserListViewBean> selectedUserViewBeanList = null ;
	private String selectedUserIdString = null ;
	private List<String> userSelected = new ArrayList<String>();


	public List<UserListViewBean> getUserViewBeanList() {
		return userViewBeanList;
	}
	public void setUserViewBeanList(List<UserListViewBean> userViewBeanList) {
		this.userViewBeanList = userViewBeanList;
	}
	public UserListViewBean newListViewBean() {
		UserListViewBean bean = newUserListViewBean();

		return bean;
	}
	public List<GroupListViewBean> getGroupViewBeanList() {
		return groupViewBeanList;
	}
	public void setGroupViewBeanList(List<GroupListViewBean> groupViewBeanList) {
		this.groupViewBeanList = groupViewBeanList;
	}
	public GroupListViewBean newGroupListViewBean() {
		GroupListViewBean bean = new GroupListViewBean();

		return bean;
	}
	public String getInGroupName() {
		return inGroupName;
	}
	public void setInGroupName(String inGroupName) {
		this.inGroupName = inGroupName;
	}
	public String getInGroupId() {
		return inGroupId;
	}
	public void setInGroupId(String inGroupId) {
		this.inGroupId = inGroupId;
	}
	public List<String> getUserSelected() {
		return userSelected;
	}
	public void setUserSelected(List<String> userSelected) {
		this.userSelected = userSelected;
	}
	public List<UserListViewBean> getSelectedUserViewBeanList() {
		return selectedUserViewBeanList;
	}
	public void setSelectedUserViewBeanList(
			List<UserListViewBean> selectedUserViewBeanList) {
		this.selectedUserViewBeanList = selectedUserViewBeanList;
	}
	public String getSelectedUserIdString() {
		return selectedUserIdString;
	}
	public void setSelectedUserIdString( String[] selectedUserIdString ) {

		StringBuilder stb = new StringBuilder() ;
		stb.append( "[" ) ;
		stb.append( TriStringUtils.convertArrayToString( selectedUserIdString ) ) ;
		stb.append( "]" ) ;
		this.selectedUserIdString = stb.toString() ;
	}
	public List<UserListViewBean> getPreservedUserViewBeanList() {
		return preservedUserViewBeanList;
	}
	public void setPreservedUserViewBeanList(
			List<UserListViewBean> preservedUserViewBeanList) {
		this.preservedUserViewBeanList = preservedUserViewBeanList;
	}

	public UserListViewBean newUserListViewBean() {
		UserListViewBean bean = new UserListViewBean();

		return bean;
	}

	public class GroupListViewBean {

		/** グループID **/
		private String groupId   		  = null;
		/** グループ名 **/
		private String groupName 		  = null;
		public String getGroupId() {
			return groupId;
		}
		public void setGroupId(String groupId) {
			this.groupId = groupId;
		}
		public String getGroupName() {
			return groupName;
		}
		public void setGroupName(String groupName) {
			this.groupName = groupName;
		}

	}

	public class UserListViewBean {

		/** ユーザID **/
		private String userId   		  = null;
		/** ユーザ名 **/
		private String userName 		  = null;
		public String getUserId() {
			return userId;
		}
		public void setUserId(String userId) {
			this.userId = userId;
		}
		public String getUserName() {
			return userName;
		}
		public void setUserName(String userName) {
			this.userName = userName;
		}
	}

}
