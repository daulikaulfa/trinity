package jp.co.blueship.tri.fw.um.domain.dept;

import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domain.dept.dto.FlowUcfSectionDeleteServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

public class FlowUcfSectionDeleteService implements IDomain<FlowUcfSectionDeleteServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umFinderSupport;
	public void setUmFinderSupport( IUmFinderSupport umFinderSupport ) {
		this.umFinderSupport = umFinderSupport;
	}

	@Override
	public IServiceDto<FlowUcfSectionDeleteServiceBean> execute( IServiceDto<FlowUcfSectionDeleteServiceBean> serviceDto ) {

		FlowUcfSectionDeleteServiceBean paramBean = null;

		try {
			paramBean	= serviceDto.getServiceBean();

			if ( null == paramBean ){
				throw new TriSystemException( UmMessageId.UM005001S );
			}

			IDeptEntity entity = umFinderSupport.findDeptByDeptId( paramBean.getInSectionId() );
			if ( null != entity ) {
				paramBean.setSectionId( entity.getDeptId() );
				paramBean.setSectionName( entity.getDeptNm() );
			}

			return serviceDto;

		} catch ( Exception e ) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException( UmMessageId.UM005003S , e);
		}
	}

}
