package jp.co.blueship.tri.fw.um.domain.user.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;

public class FlowUcfUserDeleteServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	
	
	/** ユーザID **/
	private String inUserId   		  = null;
	/** ユーザ名 **/
	private String inUserName 		  = null;
	/** パスワード **/
	private String Password 		  = null;
	/** 確認用パスワード **/
	private String Repassword	  	  = null;
	/** メールアドレス **/
	private String MailAddress 	  = null;
	/** 部署ID **/
	private String SectionId         = null;
	/** 部署名 **/
	private String SectionName         = null;
	/** グループID **/
	private String GroupId         = null;
	/** グループ名 **/
	private String GroupName         = null;
	/** グループリスト **/
	private List<String> GroupSelected = new ArrayList<String>();
	
	public String getInUserId() {
		return inUserId;
	}

	public void setInUserId(String inUserId) {
		this.inUserId = inUserId;
	}

	public String getMailAddress() {
		return MailAddress;
	}

	public void setMailAddress(String mailAddress) {
		MailAddress = mailAddress;
	}

	public String getPassword() {
		return Password;
	}

	public void setPassword(String password) {
		Password = password;
	}

	public String getRepassword() {
		return Repassword;
	}

	public void setRepassword(String repassword) {
		Repassword = repassword;
	}

	public String getSectionId() {
		return SectionId;
	}

	public void setSectionId(String sectionId) {
		SectionId = sectionId;
	}

	public String getSectionName() {
		return SectionName;
	}

	public void setSectionName(String sectionName) {
		SectionName = sectionName;
	}

	public String getInUserName() {
		return inUserName;
	}

	public void setInUserName(String userName) {
		inUserName = userName;
	}

	public String getGroupId() {
		return GroupId;
	}
	
	public void setGroupId(String groupId) {
		GroupId = groupId;
	}	
		
	public String getGroupName() {
		return GroupName;
	}

	public void setGroupName(String groupName) {
		GroupName = groupName;
	}
	public List<String> getGroupSelected() {
		return GroupSelected;
	}
	public void setGroupSelected(List<String> groupSelected) {
		this.GroupSelected = groupSelected;
	}
}
