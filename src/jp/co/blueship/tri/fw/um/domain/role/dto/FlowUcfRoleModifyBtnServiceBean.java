package jp.co.blueship.tri.fw.um.domain.role.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domain.dto.GenericServiceBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleEntryServiceBean.ActionListViewBean;
import jp.co.blueship.tri.fw.um.domain.role.dto.FlowUcfRoleListServiceBean.RoleListViewBean;

public class FlowUcfRoleModifyBtnServiceBean extends GenericServiceBean {

	private static final long serialVersionUID = 1L;
	
	private String inRoleId;
	private String inRoleName;
	private List<String> inActionList;
	
	// 一覧
	private List<RoleListViewBean> roleViewBeanList = new ArrayList<RoleListViewBean>();
	private List<ActionListViewBean> actionViewBeanList = new ArrayList<ActionListViewBean>();
	
	/**
	 * 新しいインスタンスを取得します。
	 * 
	 * @return 取得した情報を戻します。
	 */
	public RoleListViewBean newListViewBean() {
		RoleListViewBean bean = new FlowUcfRoleListServiceBean().newListViewBean();
		
		return bean;
	}
	public ActionListViewBean newActionListViewBean() {
		ActionListViewBean bean = new FlowUcfRoleEntryServiceBean().newActionListViewBean();
		
		return bean;
	}
	
	/**
	 * 画面のリスト情報を取得します。
	 * 
	 * @return 取得したリストを戻します。
	 */
	public List<RoleListViewBean> getViewBeanList() {
		return roleViewBeanList;
	}
	
	/**
	 * 画面のリスト情報を設定します。
	 * 
	 * @param list 設定するリスト情報で内部を上書きします。
	 */
	public void setViewBeanList(List<RoleListViewBean> list) {
		this.roleViewBeanList = list;
	}

	public String getInRoleId() {
		return inRoleId;
	}

	public void setInRoleId(String inRoleId) {
		this.inRoleId = inRoleId;
	}

	public String getInRoleName() {
		return inRoleName;
	}

	public void setInRoleName(String inRoleName) {
		this.inRoleName = inRoleName;
	}
	
	public List<ActionListViewBean> getActionViewBeanList() {
		return actionViewBeanList;
	}

	public void setActionViewBeanList(List<ActionListViewBean> actionViewBeanList) {
		this.actionViewBeanList = actionViewBeanList;
	}

	public List<String> getInActionList() {
		return inActionList;
	}

	public void setInActionList(List<String> inActionList) {
		this.inActionList = inActionList;
	}
	
}
