package jp.co.blueship.tri.fw.um;

import static jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils.*;
import static jp.co.blueship.tri.fw.security.spec.SecFunctions.*;
import static jp.co.blueship.tri.fw.security.spec.SecPredicates.*;
import static jp.co.blueship.tri.fw.security.spec.SecSpecifications.*;

import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.DesignUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.orm.NullTimestamp;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.sm.contants.PasswordComplexityLevel;
import jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistEntity;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class UmUserEditServiceUtils {

	public static String getTimezoneName(String timezone) {
		List<ItemLabelsBean> timezoneViews = getTimezoneViews();

		for (ItemLabelsBean itemLabel : timezoneViews) {
			if (itemLabel.getValue().equals(timezone)) {
				return itemLabel.getLabel();
			}
		}

		return "";
	}

	public static String getLanguageName(String language) {
		List<ItemLabelsBean> languageViews = getLanguageViews();

		for (ItemLabelsBean itemLabel : languageViews) {
			if (itemLabel.getValue().equals(language)) {
				return itemLabel.getLabel();
			}
		}

		return "";
	}

	public static List<ItemLabelsBean> getPasswordExpirationTerm() {
		List<ItemLabelsBean> passwordExpirationList = new ArrayList<ItemLabelsBean>();

		String defaultTerm = DesignSheetUtils.getPasswordDefaultEffectiveTerm();
		if (!TriStringUtils.isEmpty(defaultTerm)) {
			ItemLabelsBean passwordBean = new ItemLabelsBean();
			passwordBean.setValue(defaultTerm);
			passwordBean.setLabel(defaultTerm + DesignUtils.getMessageParameter(MessageParameter.DAYS.getKey()));

			passwordExpirationList.add(passwordBean);
		}

		Map<String, String> termMap = DesignSheetFactory.getDesignSheet()
				.getKeyMap(UmDesignBeanId.passwordEffectiveTerm);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean passwordBean = new ItemLabelsBean();
			passwordBean.setValue(entry.getKey());
			passwordBean.setLabel(entry.getValue());

			passwordExpirationList.add(passwordBean);
		}

		return passwordExpirationList;
	}

	public static List<ItemLabelsBean> getIconViews(String userId) {
		List<ItemLabelsBean> iconList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.userIcons);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean userIcon = new ItemLabelsBean();
			userIcon.setLabel(entry.getValue());
			String defaultIconPath = UmDesignBusinessRuleUtils.getUserIconParentSharePath(userId, IconSelectionOption.DefaultImage);
			String iconPath = TriStringUtils.linkPathBySlash(defaultIconPath, entry.getKey());
			userIcon.setValue(iconPath);

			iconList.add(userIcon);
		}

		return iconList;
	}

	public static List<ItemLabelsBean> getTimezoneViews() {
		List<ItemLabelsBean> timezoneList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.timezones);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean timezoneBean = new ItemLabelsBean();
			timezoneBean.setValue(entry.getKey());
			timezoneBean.setLabel(entry.getValue());

			timezoneList.add(timezoneBean);
		}

		return timezoneList;
	}

	public static List<ItemLabelsBean> getLanguageViews() {
		List<ItemLabelsBean> languageList = new ArrayList<ItemLabelsBean>();
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.langs);

		for (Map.Entry<String, String> entry : termMap.entrySet()) {

			ItemLabelsBean languageBean = new ItemLabelsBean();
			languageBean.setValue(entry.getKey());
			languageBean.setLabel(entry.getValue());

			languageList.add(languageBean);
		}

		return languageList;
	}

	public static List<ItemLabelsBean> getLookAndFeelViews() {
		LookAndFeel lookAndFeelOld = LookAndFeel.TrinityClassic;
		LookAndFeel lookAndFeelNew = LookAndFeel.Trinity;

		List<ItemLabelsBean> list = new ArrayList<ItemLabelsBean>();
		list.add(new ItemLabelsBean(lookAndFeelNew.name(), lookAndFeelNew.value()));
		list.add(new ItemLabelsBean(lookAndFeelOld.name(), lookAndFeelOld.value()));

		return list;
	}

	/**
	 * パスワード有効期限を取得する
	 * @param termLimit
	 * @return
	 */
	public static String getPasswordEffectiveLimit( Timestamp termLimit, SimpleDateFormat formatDate) {

		if ( null == termLimit ) return null;

		return formatDate.format( new Date( termLimit.getTime() ));

	}

	/**
	 * パスワード有効日時を取得する
	 * @param paramBean
	 * @return
	 */
	public static Timestamp getPasswordEffectiveDate( String termId ) {

		// 期間未指定、またはマイナス値の場合は有効期限の制限なし
		if ( null != termId ) {

			int termInt = Integer.parseInt( termId );

			if ( 0 <= termInt ) {

				Calendar now = Calendar.getInstance();
				now.add( Calendar.DATE, termInt );

				return new Timestamp( now.getTimeInMillis() );
			} else {
				return new NullTimestamp();
			}
		}

		return null;

	}

	public static void checkComplexity( String password, PasswordDetailsView passwordView ) {

		if(!TriStringUtils.isEmpty(password)){
			int complexity = 0;
			int count =0;

			if(password.length()>=8){
				passwordView.setMoreThan8characters(true);
				complexity++;
			} else {
				passwordView.setMoreThan8characters(false);
			}

			boolean hasUpperCase = !password.equals(password.toLowerCase());
			boolean hasLowerCase = !password.equals(password.toUpperCase());

			if(hasUpperCase && hasLowerCase){
				passwordView.setContainsUppercaseAndLowercase(true);
				complexity++;
			} else {
				passwordView.setContainsUppercaseAndLowercase(false);
			}

			if ( TriStringUtils.containsNumerals( password ) ) {
				passwordView.setContainsNumerals(true);
				complexity++;
			} else {
				passwordView.setContainsNumerals(false);
			}

			{
				char[] strArray = password.toCharArray();

				if(strArray.length >= 3) {

					char current = strArray[0];

					for (int i = 1; i < strArray.length; i++) {
						if(current == strArray[i]) {
							count++;
						} else {
							count = 0;
							current = strArray[i];
						}
						if( count == 2) {
							break;
						}
					}
				}

				if(count >= 2){
					passwordView.setSameCharacterLessThan3Times(true);
					complexity = 1;
				} else {
					passwordView.setSameCharacterLessThan3Times(false);
				}
			}

			if(complexity == 0){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.Low);
			}

			if(complexity == 1){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.Low);
			}

			if(complexity == 2){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.Middle);
			}

			if(complexity >= 3){
				passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.High);
			}
		} else {
			passwordView.setPasswordComplexityLevel(PasswordComplexityLevel.none);
			passwordView.setSameCharacterLessThan3Times(false);
			passwordView.setContainsNumerals(false);
			passwordView.setContainsUppercaseAndLowercase(false);
			passwordView.setMoreThan8characters(false);
		}
	}

	public static int getHistoryNumber() {
		String historyNum = DesignSheetUtils.getPasswordHistoryNumber();

		int historyNumber = 0;
		if (NumberUtils.isNumber(historyNum)) {
			historyNumber = Integer.parseInt(historyNum);
		}

		return historyNumber;
	}

	/**
	 * 入力されたパスワードのチェックを行う
	 *
	 * @param paramBean
	 * @param entity
	 * @param passwordEntities
	 * @param historyNumber
	 */
	public static String checkPassword(String oldPassword, String password, String confirmPassword,
			IUserEntity entity, List<IUserPassHistEntity> passwordEntities,
			int historyNumber) {

		List<IMessageId> messageList = new ArrayList<IMessageId>();
		List<String[]> messageArgsList = new ArrayList<String[]>();

		// 旧パスワードでの認証
		authenticateByOldPassword(entity, oldPassword, messageList, messageArgsList);

		// 新パスワードの入力チェック
		if (TriStringUtils.isEmpty(password)) {
			messageList.add(SmMessageId.SM003003I);
			messageArgsList.add(new String[] {});
		} else {
			// 新パスワードの入力許可文字のチェック
			if (!TriStringUtils.matches(password, DesignSheetUtils.getPasswordAllowPattern())) {
				messageList.add(SmMessageId.SM003010I);
				messageArgsList.add(new String[] {});
			}

			// 新パスワードの文字列長チェック
			String minPasswordLength = DesignSheetUtils.getPasswordMinimumLength();
			String maxPasswordLength = DesignSheetUtils.getPasswordMaxLength();
			int passMinLen = 0;
			int passMaxLen = 0;
			if (NumberUtils.isNumber(minPasswordLength)) {
				passMinLen = Integer.parseInt(minPasswordLength);
			}
			if (NumberUtils.isNumber(maxPasswordLength)) {
				passMaxLen = Integer.parseInt(maxPasswordLength);
			}
			if (0 < passMinLen && password.length() < passMinLen) {
				messageList.add(SmMessageId.SM003004I);
				messageArgsList.add(new String[] { String.valueOf(passMinLen) });
			}
			if (password.length() > passMaxLen) {
				messageList.add(SmMessageId.SM003001I);
				messageArgsList.add(new String[] { String.valueOf(passMaxLen) });
			}

			// 新パスワードと確認用新パスワードのチェック
			if (!password.equals(confirmPassword)) {
				messageList.add(SmMessageId.SM003005I);
				messageArgsList.add(new String[] {});
			}

			// 新パスワードと旧パスワードのチェック
			if (password.equals(oldPassword)) {
				messageList.add(SmMessageId.SM003006I);
				messageArgsList.add(new String[] {});
			}

			// 履歴との比較
			if(isExistentInHistory(entity.getUserId(), password, passwordEntities, historyNumber)) {
				messageList.add(SmMessageId.SM003009I);
				messageArgsList.add(new String[] { String.valueOf(historyNumber) });
			}
		}

		if (0 != messageList.size())
			throw new ContinuableBusinessException(messageList, messageArgsList);

		return encodePasswordInA1Format(entity.getUserId(), password);

	}

	private static void authenticateByOldPassword(IUserEntity entity, String oldPassword, List<IMessageId> messageList, List<String[]> messageArgsList) {

		if (specOfMatchingPasswordInSHAWith(entity.getUserPass()).isSpecified(oldPassword)
				|| specOfMatchingPasswordInMD5A1FormatWith(entity.getUserPass(), entity.getUserId()).isSpecified(oldPassword)) {
			return;
		}

		messageList.add(SmMessageId.SM003002I);
		messageArgsList.add(new String[] {});
	}

	private static boolean isExistentInHistory(String userID, String newPassword, List<IUserPassHistEntity> passwordHistories, int historyLimit) {

		if(historyLimit <= 0) {
			return false;
		}

		if (TriStringUtils.isEmpty(passwordHistories)) {
			return false;
		}

		return FluentList.from(passwordHistories).//
				limit(historyLimit).//
				rejectFirst().//
				map(PASS_HIST_TO_PASSWORD).//
				contains(isMatchedInSHAWith(newPassword).or(isMatchedInMD5A1FormatWith(newPassword, userID)));
	}

	/**
	 * 初めてのパスワード変更の場合に現在のパスワードでパスワード履歴テーブルのデータを作成する。
	 *
	 * @param entity
	 * @param passwordEntities
	 * @param historyNumber
	 */
	public static List<IUserPassHistEntity> initUserPasswordHistory(IUserEntity entity, List<IUserPassHistEntity> passwordEntities, int historyNumber) {

		// すでにデータがある場合はパス
		if (0 < passwordEntities.size())
			return passwordEntities;
		List<IUserPassHistEntity> userPassHistEntityList = new ArrayList<IUserPassHistEntity>();
		// 履歴をとらない場合
		if (0 == historyNumber)
			return userPassHistEntityList;

		IUserPassHistEntity passwordEntity = new UserPassHistEntity();
		passwordEntity.setUserId(entity.getUserId());
		passwordEntity.setPassHistSeqNo("0");
		passwordEntity.setUserPass(entity.getUserPass());
		userPassHistEntityList.add(passwordEntity);

		return userPassHistEntityList;
	}
}
