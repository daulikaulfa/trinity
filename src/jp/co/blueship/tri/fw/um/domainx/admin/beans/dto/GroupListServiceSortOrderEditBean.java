package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class GroupListServiceSortOrderEditBean {

	private String groupId = null;
	private int newSortOrder = 0;

	public String getGroupId() {
		return groupId;
	}
	public GroupListServiceSortOrderEditBean setGroupId(String groupId) {
		this.groupId = groupId;
		return this;
	}
	public int getNewSortOrder() {
		return newSortOrder;
	}
	public GroupListServiceSortOrderEditBean setNewSortOrder(int newSortOrder) {
		this.newSortOrder = newSortOrder;
		return this;
	}
}
