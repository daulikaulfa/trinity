package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmWikiUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagEntity;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiEditServiceBean.WikiCreationInputInfo;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiEditService implements IDomain<FlowWikiEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public IServiceDto<FlowWikiEditServiceBean> execute(IServiceDto<FlowWikiEditServiceBean> serviceDto) {

		FlowWikiEditServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String wikiId = paramBean.getParam().getSelectedWikiId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.submitChanges.equals(paramBean.getParam().getRequestType()))
				this.submitChanges(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiEditServiceBean paramBean ){
		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );

		if(wikiEntity == null) return;

		paramBean.setPageNm(wikiEntity.getPageNm());
		paramBean.setIsWikiHome( wikiEntity.isWikiHome().parseBoolean() );

		String pageNm = this.appendTags(wikiEntity.getPageNm(), wikiId);

		paramBean.getParam().getInputInfo()
					.setPageNm	( pageNm )
					.setContent	( wikiEntity.getContent() )
					;

		paramBean.setTagViews( this.getWikiTags( wikiEntity.getLotId() ) );
	}


	private void submitChanges( FlowWikiEditServiceBean paramBean ){
		paramBean.getResult().setCompleted(false);

		this.editWiki(paramBean);

		String pageNm = UmWikiUtils.formatPageName( paramBean.getParam().getInputInfo().getPageNm() );

		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003067I,pageNm);
		paramBean.getResult().setCompleted	(true)
							 .setWikiId		( paramBean.getParam().getSelectedWikiId() )
							 ;
	}


	private void editWiki( FlowWikiEditServiceBean paramBean ){

		WikiCreationInputInfo inputInfo = paramBean.getParam().getInputInfo();

		String wikiId = paramBean.getParam().getSelectedWikiId();
		String content = inputInfo.getContent();
		String[] tags = UmWikiUtils.extractTags( inputInfo.getPageNm() );
		String pageNm = UmWikiUtils.formatPageName(inputInfo.getPageNm());
		
		UmWikiUtils.validateWiki(pageNm, content);

		this.updateWiki(wikiId, pageNm, content);
		this.insertWikiHist(wikiId);
		this.updateWikiTags(wikiId, tags);
		this.insertWikiUpdateHist(wikiId);
	}


	private void updateWiki( String wikiId, String pageNm, String content ){
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		entity.setPageNm(pageNm);
		entity.setContent(content);

		this.umSupport.getWikiDao().update(entity);
	}


	private void updateWikiTags( String wikiId, String[] tags ){
		WikiTagCondition condition = new WikiTagCondition();
		condition.setWikiId(wikiId);
		condition.setDelStsId((StatusFlg)null);

		List<IWikiTagEntity> entityList = this.umSupport.getWikiTagDao().find(condition.getCondition());
		List<String> tagList = this.formatTagNm(tags);

		for(IWikiTagEntity entity : entityList){
			if( tagList.contains( entity.getTagNm() ) ){
				tagList.remove( entity.getTagNm() );

				if( entity.getDelStsId().parseBoolean() ){
					entity.setDelStsId(StatusFlg.off);

				} else {
					continue;
				}

			}else{
				entity.setDelStsId(StatusFlg.on);
			}
			this.umSupport.getWikiTagDao().update(entity);
		}

		for(String tagNm : tagList)
			this.insertTag(wikiId, tagNm);
	}


	private void insertTag(String wikiId, String tagNm){
		IWikiTagEntity entity = new WikiTagEntity();
		entity.setWikiId(wikiId);
		entity.setTagNm(tagNm);

		this.umSupport.getWikiTagDao().insert(entity);
	}


	private List<String> formatTagNm( String[] tags ){
		List<String> tagList = new ArrayList<String>();


		for(String tag : tags){
			tag = StringUtils.removeStart(tag, "[");
			tag = StringUtils.removeEnd(tag, "]");

			tagList.add(tag);
		}



		return tagList;
	}


	private void insertWikiHist( String wikiId ){
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );

		IWikiHistEntity histEntity = new WikiHistEntity();
		histEntity.setWikiId(wikiId);
		histEntity.setWikiVerNo( wikiEntity.getWikiVerNo() );
		histEntity.setPageNm( wikiEntity.getPageNm() );
		histEntity.setContent( wikiEntity.getContent() );

		this.umSupport.getWikiHistDao().insert( histEntity );
	}

	private void insertWikiUpdateHist( String wikiId ){
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );
		IHistEntity histEntity = new HistEntity();
		histEntity.setActSts(UmActStatusId.Edit.getStatusId());
		this.umSupport.getWikiUpdateHistDao().insert( histEntity, wikiEntity );
	}


	private List<String> getWikiTags( String lotId ){
		ViewWikiTagsCondition condition = new ViewWikiTagsCondition();
		condition.setLotId(lotId);

		List<IViewWikiTagsEntity> entityList = this.umSupport.getViewWikiTagsDao().find(condition);
		List<String> tagList = new ArrayList<String>();

		for( IViewWikiTagsEntity entity : entityList)
			tagList.add(entity.getTagNm());

		return tagList;
	}


	private String appendTags( String pageNm, String wikiId ){

		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setWikiId( wikiId);
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		StringBuffer buf = new StringBuffer();

		for(IWikiTagEntity tagEntity : tagEntityList)
			buf.append("[" + tagEntity.getTagNm() + "]");

		buf.append( pageNm );

		return buf.toString();
	}
}
