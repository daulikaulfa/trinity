package jp.co.blueship.tri.fw.um.domainx.lotsettings.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryListServiceSortOrderEditBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryListServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<CategoryView> categoryViews = new ArrayList<CategoryView>();

	public List<CategoryView> getCategoryViews() {
		return categoryViews;
	}
	public FlowCategoryListServiceBean setCategoryViews(List<CategoryView> categoryViews) {
		this.categoryViews = categoryViews;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCategoryListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private CategoryListServiceSortOrderEditBean sortOrderEdit = new CategoryListServiceSortOrderEditBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public CategoryListServiceSortOrderEditBean getSortOrderEdit() {
			return sortOrderEdit;
		}
		public RequestParam setSortOrderEdit(CategoryListServiceSortOrderEditBean sortOrderEdit) {
			this.sortOrderEdit = sortOrderEdit;
			return this;
		}

	}

	/**
	 * Category
	 */
	public class CategoryView {
		private boolean isRemovalEnabled = false;
		private String categoryId = null;
		private String subject = null;
		private int count = 0;
		private int sortOrder = 0;

		public boolean isRemovalEnabled() {
			return isRemovalEnabled;
		}
		public CategoryView setRemovalEnabled(boolean isRemovalEnabled) {
			this.isRemovalEnabled = isRemovalEnabled;
			return this;
		}

		public String getCategoryId() {
			return categoryId;
		}
		public CategoryView setCategoryId(String categoryId) {
			this.categoryId = categoryId;
			return this;
		}

		public int getSortOrder() {
			return sortOrder;
		}
		public CategoryView setSortOrder(int sortOrder) {
			this.sortOrder = sortOrder;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public CategoryView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public int getCount() {
			return count;
		}
		public CategoryView setCount(int count) {
			this.count = count;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
