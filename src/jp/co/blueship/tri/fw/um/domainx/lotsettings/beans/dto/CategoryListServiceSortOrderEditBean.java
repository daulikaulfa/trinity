package jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class CategoryListServiceSortOrderEditBean {
	private String categoryId = null;
	private int newSortOrder = 0;

	public String getCategoryId() {
		return categoryId;
	}
	public CategoryListServiceSortOrderEditBean setCategoryId(String categoryId) {
		this.categoryId = categoryId;
		return this;
	}

	public int getNewSortOrder() {
		return newSortOrder;
	}
	public CategoryListServiceSortOrderEditBean setNewSortOrder(int newSortOrder) {
		this.newSortOrder = newSortOrder;
		return this;
	}

}
