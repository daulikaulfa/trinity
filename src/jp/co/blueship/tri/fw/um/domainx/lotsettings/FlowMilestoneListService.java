package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneListServiceBean.MilestoneView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowMilestoneListService implements IDomain<FlowMilestoneListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowMilestoneListServiceBean> execute(
			IServiceDto<FlowMilestoneListServiceBean> serviceDto) {

		FlowMilestoneListServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String lotId = paramBean.getParam().getSelectedLotId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setMilestoneViews( this.getMilestoneViews(paramBean, lotId) );
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if ( null != paramBean.getParam().getSortOrderEdit() ) {
					this.updateSortOrder( paramBean.getParam().getSortOrderEdit(), lotId );
					paramBean.setMilestoneViews( this.getMilestoneViews(paramBean, lotId) );
					paramBean.getResult().setCompleted(true);
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				paramBean.setMilestoneViews( this.getMilestoneViews(paramBean, lotId) );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private List<MilestoneView> getMilestoneViews(FlowMilestoneListServiceBean paramBean, String lotId) {
		List<MilestoneView> views = new ArrayList<MilestoneView>();

		for ( IMstoneEntity entity : support.findMstoneByLotId( lotId ) ) {
			MstoneLnkCondition condition = new MstoneLnkCondition();
			condition.setMstoneId( entity.getMstoneId() );
			int count = support.getMstoneLnkDao().count( condition.getCondition() );

			MilestoneView viewBean = paramBean.new MilestoneView()
				.setMstoneId( entity.getMstoneId() )
				.setSubject( entity.getMstoneNm() )
				.setDescription( entity.getContent() )
				.setStartDate( entity.getMstoneStDate() )
				.setDueDate( entity.getMstoneEndDate() )
				.setCount( count )
				.setSortOrder( entity.getSortOdr() )
				.setRemovalEnabled( true )
			;

			views.add(viewBean);
		}

		return views;
	}

	private void updateSortOrder( MilestoneListServiceSortOrderEditBean sortOrderBean, String lotId ) {
		String mstoneId = sortOrderBean.getMstoneId();
		int newSortOrder = sortOrderBean.getNewSortOrder();
		int currentRow = 1;

		for ( IMstoneEntity entity : support.findMstoneByLotId( lotId ) ) {

			if ( currentRow == newSortOrder ) {
				currentRow++;
			}

			if ( ! entity.getMstoneId().equals(mstoneId) ) {
				if ( currentRow != entity.getSortOdr() ) {
					IMstoneEntity updEntity = new MstoneEntity();
					updEntity.setMstoneId( entity.getMstoneId() );
					updEntity.setSortOdr( currentRow );
					support.getMstoneDao().update( updEntity );
				}
				currentRow++;
			}
		}

		IMstoneEntity newEntity = new MstoneEntity();
		newEntity.setMstoneId( mstoneId );
		newEntity.setSortOdr( newSortOrder );
		support.getMstoneDao().update( newEntity );
	}
}
