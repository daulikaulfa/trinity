package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiPageListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiTagListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean.AttachFile;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsServiceBean.WikiView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiDetailsService implements IDomain<FlowWikiDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	private WikiPageListService wikiPageListService = null ;
	private WikiTagListService wikiTagListService = null ;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	public void setWikiPageListService( WikiPageListService wikiPageListService ){
		this.wikiPageListService = wikiPageListService;
	}
	public void setWikiTagListService( WikiTagListService wikiTagListService ){
		this.wikiTagListService = wikiTagListService;
	}

	@Override
	public IServiceDto<FlowWikiDetailsServiceBean> execute(IServiceDto<FlowWikiDetailsServiceBean> serviceDto) {

		FlowWikiDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String lotId = paramBean.getParam().getSelectedLotId();
			String wikiId = paramBean.getParam().getSelectedWikiId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId) || TriStringUtils.isNotEmpty(lotId), "SelectedId is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiDetailsServiceBean paramBean ){
		if( RequestOption.none.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);

		else
			this.onChange(paramBean);
	}


	private void onChange( FlowWikiDetailsServiceBean paramBean ){
		String lotId = paramBean.getParam().getSelectedLotId();
		String wikiId = paramBean.getParam().getSelectedWikiId();

		if( RequestOption.All.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);

		if( RequestOption.WikiPageList.equals( paramBean.getParam().getRequestOption() ) ){
			String searchPageNm = paramBean.getParam().getSearchPageNm();

			if( TriStringUtils.isNotEmpty(wikiId) )
				paramBean.setPageViews( this.wikiPageListService.getWikiPageListByWikiId(wikiId, searchPageNm) );
			else
				paramBean.setPageViews( this.wikiPageListService.getWikiPageList(lotId, searchPageNm) );
		}

		if( RequestOption.TagList.equals( paramBean.getParam().getRequestOption() ) ){

			if( TriStringUtils.isNotEmpty(wikiId) )
				paramBean.setTagViews ( this.wikiTagListService.getWikiTagViewListByWikiId(wikiId) );
			else
				paramBean.setTagViews ( this.wikiTagListService.getWikiTagViewList(lotId) );
		}

		if( RequestOption.WikiDetailsService.equals( paramBean.getParam().getRequestOption() ) )
			this.getWikiDetails(paramBean);
	}


	private void requestAll(FlowWikiDetailsServiceBean paramBean){
		String lotId = paramBean.getParam().getSelectedLotId();
		String wikiId = paramBean.getParam().getSelectedWikiId();

		this.getWikiDetails(paramBean);

		List<WikiPageViewBean> wikiPageList;
		List<WikiTagViewBean> wikiTagList;

		if( TriStringUtils.isNotEmpty(wikiId) ){
			wikiPageList = this.wikiPageListService.getWikiPageListByWikiId(wikiId, paramBean.getParam().getSearchPageNm());
			wikiTagList = this.wikiTagListService.getWikiTagViewListByWikiId(wikiId);

			this.umSupport.updateAccsHist(paramBean, UmTables.UM_WIKI, wikiId);
		}else{
			wikiPageList = this.wikiPageListService.getWikiPageList(lotId, paramBean.getParam().getSearchPageNm());
			wikiTagList = this.wikiTagListService.getWikiTagViewList(lotId);
		}

		paramBean.setPageViews( wikiPageList )
				 .setTagViews ( wikiTagList )
				 ;
	}


	private void getWikiDetails( FlowWikiDetailsServiceBean paramBean ){
		SimpleDateFormat format = TriDateUtils.getYMDHMSDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		String lotId = paramBean.getParam().getSelectedLotId();
		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiCondition condition = new WikiCondition();
		IWikiEntity wikiEntity = null;

		if( TriStringUtils.isNotEmpty( wikiId ) ){
			condition.setWikiId(wikiId);
			wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );

		}else{
			condition.setLotId(lotId);
			condition.setIsWikiHome( StatusFlg.on );

			List<IWikiEntity> entityList = this.umSupport.getWikiDao().find(condition.getCondition());

			if(entityList == null || entityList.size() == 0){
				String homeWikiId = this.umSupport.getWikiNumberingDao().nextval();

				IWikiEntity entity = new WikiEntity();
				entity.setWikiId( homeWikiId );
				entity.setLotId( lotId );
				entity.setPageNm("Home");
				entity.setContent("");
				entity.setIsWikiHome(StatusFlg.on);

				this.umSupport.getWikiDao().insert(entity);

				entityList = this.umSupport.getWikiDao().find(condition.getCondition());
			}

			wikiEntity = entityList.get(0);
			if( wikiEntity == null ) return ;

			wikiId = wikiEntity.getWikiId();
		}


		String createDate = TriDateUtils.convertViewDateFormat( wikiEntity.getRegTimestamp(), format);
		String updDate = TriDateUtils.convertViewDateFormat( wikiEntity.getUpdTimestamp(), format);

		WikiView view = paramBean.new WikiView()
				.setWikiId				( wikiEntity.getWikiId() )
				.setWikiNm				( wikiEntity.getPageNm() )
				.setContent				( wikiEntity.getContent() )
				.setIsWikiHome			( wikiEntity.isWikiHome().parseBoolean() )
				.setCreatedBy			( wikiEntity.getRegUserNm() )
				.setCreatedByUserId		( wikiEntity.getRegUserId() )
				.setCreatedIconPath		( this.umSupport.getIconPath(wikiEntity.getRegUserId()) )
				.setCreatedDate			( createDate )
				.setTagViews			( this.getTagViews(wikiId) )
				.setAttachmentFileNms	( this.getAttachedFileList(paramBean, wikiId) )
				;

		if( !createDate.equals( updDate ) ){
			view.setUpdUserNm			( wikiEntity.getUpdUserNm() )
				.setUpdUserId			( wikiEntity.getUpdUserId() )
				.setUpdUserIconPath		( this.umSupport.getIconPath(wikiEntity.getUpdUserId()) )
				.setUpdDate				( updDate )
				;
		}

		paramBean.setDetailsView( view );
	}


	private List<String> getTagViews(String wikiId){
		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setWikiId( wikiId);
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		List<String> tagList = new ArrayList<String>();

		for( IWikiTagEntity entity : tagEntityList )
			tagList.add(entity.getTagNm());

		return tagList;
	}

	private List<AttachFile> getAttachedFileList(FlowWikiDetailsServiceBean paramBean, String wikiId){
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat(paramBean.getLanguage(), paramBean.getTimeZone());

		WikiAttachedFileCondition condition = new WikiAttachedFileCondition();
		condition.setWikiId(wikiId);
		condition.setDelStsId(StatusFlg.off);
		List<IWikiAttachedFileEntity> entityList = this.umSupport.getWikiAttachedFileDao().find(condition.getCondition());
		List<AttachFile> attachedFileList = new ArrayList<AttachFile>();

		for(IWikiAttachedFileEntity entity : entityList) {
			AttachFile attachFile = new FlowWikiDetailsServiceBean().new AttachFile();
			attachFile.setName(entity.getFilePath())
					.setSeqNo(entity.getAttachedFileSeqNo())
					.setCreatedDate(TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMD));

			File attachmentFile = UmDesignBusinessRuleUtils.getAttachmentFile(wikiId, entity.getAttachedFileSeqNo(), entity.getFilePath());
			attachFile.setSize( TriFileUtils.fileSizeOf( TriFileUtils.sizeOf(attachmentFile) ) );

			attachedFileList.add(attachFile);
		}

		return attachedFileList;
	}
}
