package jp.co.blueship.tri.fw.um.domainx.lotsettings.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneListServiceSortOrderEditBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowMilestoneListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<MilestoneView> milestoneViews = new ArrayList<MilestoneView>();

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private MilestoneListServiceSortOrderEditBean sortOrderEdit = new MilestoneListServiceSortOrderEditBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public MilestoneListServiceSortOrderEditBean getSortOrderEdit() {
			return sortOrderEdit;
		}
		public RequestParam setSortOrderEdit(MilestoneListServiceSortOrderEditBean sortOrderEdit) {
			this.sortOrderEdit = sortOrderEdit;
			return this;
		}
	}

	/**
	 * Milestone
	 */
	public class MilestoneView {
		private boolean isRemovalEnabled = false;
		private String mstoneId = null;
		private String subject = null;
		private String description = null;
		private String startDate = null;
		private String dueDate = null;
		private int count = 0;
		private int sortOrder = 0;

		public boolean isRemovalEnabled() {
			return isRemovalEnabled;
		}
		public MilestoneView setRemovalEnabled(boolean isRemovalEnabled) {
			this.isRemovalEnabled = isRemovalEnabled;
			return this;
		}
		public String getMstoneId() {
			return mstoneId;
		}
		public MilestoneView setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}
		public int getSortOrder() {
			return sortOrder;
		}
		public MilestoneView setSortOrder(int sortOrder) {
			this.sortOrder = sortOrder;
			return this;
		}
		public String getSubject() {
			return subject;
		}
		public MilestoneView setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		public String getDescription() {
			return description;
		}
		public MilestoneView setDescription(String description) {
			this.description = description;
			return this;
		}
		public String getStartDate() {
			return startDate;
		}
		public MilestoneView setStartDate(String startDate) {
			this.startDate = startDate;
			return this;
		}
		public String getDueDate() {
			return dueDate;
		}
		public MilestoneView setDueDate(String dueDate) {
			this.dueDate = dueDate;
			return this;
		}
		public int getCount() {
			return count;
		}
		public MilestoneView setCount(int count) {
			this.count = count;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	public List<MilestoneView> getMilestoneViews() {
		return milestoneViews;
	}
	public FlowMilestoneListServiceBean setMilestoneViews(List<MilestoneView> milestoneViews) {
		this.milestoneViews = milestoneViews;
		return this;
	}
	public RequestsCompletion getResult() {
		return result;
	}
	public FlowMilestoneListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	public RequestParam getParam() {
		return param;
	}
}
