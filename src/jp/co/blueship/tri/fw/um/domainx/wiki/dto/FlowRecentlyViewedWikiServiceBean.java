package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowRecentlyViewedWikiServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<DashboardWikiViewBean> wikiView = new ArrayList<DashboardWikiViewBean>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<DashboardWikiViewBean> getWikiView() {
		return wikiView;
	}

	public FlowRecentlyViewedWikiServiceBean setWikiView(List<DashboardWikiViewBean> wikiView) {
		this.wikiView = wikiView;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}

	public FlowRecentlyViewedWikiServiceBean setPage(IPageNoInfo pageInfo) {
		this.page = pageInfo;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private final int linesPerPage = 8;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public void setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {
		private Integer selectedPageNo = 1;
		private String keyword = null;

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	public class DashboardWikiViewBean {
		private String lotId;
		private String wikiId;
		private String wikiNm;

		public String getLotId() {
			return lotId;
		}
		public DashboardWikiViewBean setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getWikiId() {
			return wikiId;
		}
		public DashboardWikiViewBean setWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public String getWikiNm() {
			return wikiNm;
		}
		public DashboardWikiViewBean setWikiNm(String wikiNm) {
			this.wikiNm = wikiNm;
			return this;
		}

	}

}
