package jp.co.blueship.tri.fw.um.domainx.wiki.beans;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiPageListService {

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}


	public List<WikiPageViewBean> getWikiPageListByWikiId( String wikiId ){
		return this.getWikiPageListByWikiId( wikiId, null );
	}


	public List<WikiPageViewBean> getWikiPageListByWikiId( String wikiId, String searchWikiNm ){

		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		if( entity == null) return null;

		return this.getWikiPageList( entity.getLotId() , searchWikiNm);
	}


	public List<WikiPageViewBean> getWikiPageList( String lotId ){
		return this.getWikiPageList(lotId, null );
	}


	public List<WikiPageViewBean> getWikiPageList( String lotId, String searchWikiNm ){

		WikiCondition condition = new WikiCondition();
		condition.setLotId(lotId);

		if( TriStringUtils.isNotEmpty( searchWikiNm ) )
			condition.setPageNm( searchWikiNm );

		List<IWikiEntity> entityList = this.umSupport.getWikiDao().find(condition.getCondition());
		List<WikiPageViewBean> pageViewList = new ArrayList<WikiPageViewBean>();

		for( IWikiEntity entity : entityList ){
			WikiPageViewBean view = new WikiPageViewBean()
					.setWikiId( entity.getWikiId() )
					.setWikiNm( entity.getPageNm() )
					;
			pageViewList.add( view );
		}
		return pageViewList;
	}
}
