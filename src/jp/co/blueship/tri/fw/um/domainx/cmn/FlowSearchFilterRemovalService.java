package jp.co.blueship.tri.fw.um.domainx.cmn;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterCondition;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowSearchFilterRemovalService implements IDomain<FlowSearchFilterRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowSearchFilterRemovalServiceBean> execute(
			IServiceDto<FlowSearchFilterRemovalServiceBean> serviceDto) {

		FlowSearchFilterRemovalServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String filterId = paramBean.getParam().getSelectedFilterId();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(filterId), "SelectedFilterId is not specified");

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				FilterCondition condition = new FilterCondition();
				condition.setFilterId(filterId);

				String filterName = "";
				if ( 0 < support.getFilterDao().count(condition.getCondition())) {
					IFilterEntity filterEntitiy = support.getFilterDao().findByPrimaryKey(condition.getCondition());
					filterName = filterEntitiy.getFilterNm();
					this.removeFilter(filterId);
				}

				paramBean.getResult().setCompleted(true);
				if(TriStringUtils.isNotEmpty(filterName)){
					paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003013I, filterName);
				}

			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void removeFilter(String filterId) {
		IFilterEntity filterEntity = new FilterEntity();
		filterEntity.setFilterId(filterId);
		filterEntity.setDelStsId(StatusFlg.on);
		support.getFilterDao().update(filterEntity);
	}
}






