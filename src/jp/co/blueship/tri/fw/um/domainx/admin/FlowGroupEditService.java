package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupRoleLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupUserLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupEditServiceBean.GroupRequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGroupEditService implements IDomain<FlowGroupEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowGroupEditServiceBean> execute(
			IServiceDto<FlowGroupEditServiceBean> serviceDto) {
		FlowGroupEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String grpId = paramBean.getParam().getSelectedGroupId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(grpId), "SelectedGrpId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean, grpId);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if (GroupRequestOption.ExcludeUser.equals(paramBean.getParam().getRequestOption())) {
					this.removeUserFromViews(paramBean, grpId);
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean, grpId);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowGroupEditServiceBean paramBean, String grpId) {
		String grpNm = support.findGroupById(grpId).getGrpNm();
		Set<String> selectedUserSet = new LinkedHashSet<String>();

		paramBean.setUserViews(this.getSelectedUserViews(grpId, selectedUserSet))
				.getParam().getInputInfo()
				.setGroupNm(grpNm)
				.setRoleIdSet(this.getSelectedRoleIds(grpId))
				.setUserIdSet(selectedUserSet)
				.setRoleViews(this.getRoleViews())
				.setUserViews(this.getUserViews(selectedUserSet));

	}

	private void removeUserFromViews(FlowGroupEditServiceBean paramBean, String grpId) {
		String excludeUserId = paramBean.getParam().getInputInfo().getExcludeUserId();

		if (TriStringUtils.isNotEmpty(excludeUserId)) {
			if(UmItemChkUtils.ADMIN_GROUP_ID.equals(grpId) && UmItemChkUtils.ADMIN.equals(excludeUserId)) {
				throw new ContinuableBusinessException(UmMessageId.UM001103E);
			}
			Set<String> selectedUserSet = paramBean.getParam().getInputInfo().getUserIdSet();
			selectedUserSet.remove(excludeUserId);

			support.cleaningGrpUserLnk(grpId, excludeUserId);

			support.cleaningAndUpdateUserRoleLnk(new String[]{excludeUserId});

			paramBean.setUserViews(this.getSelectedUserViews(grpId, selectedUserSet));
			paramBean.getParam().getInputInfo().setUserViews(this.getUserViews(selectedUserSet));
		}
	}

	private void submitChanges(FlowGroupEditServiceBean paramBean, String grpId) {
		GroupEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		int count = this.countGrpByName(inputInfo.getGroupNm(), grpId);

		UmItemChkUtils.checkInputGroup(inputInfo, count, support);
		UmItemChkUtils.checkAdminGroup(grpId, inputInfo.getUserIdSet(), inputInfo.getRoleIdSet());
		updateGroup(grpId, inputInfo.getGroupNm());

		// Delete grp_role_lnk
		support.cleaningGrpRoleLnk(grpId, null);

		// Delete grp_user_lnk
		support.cleaningGrpUserLnk(grpId, null);

		// Insert Grp_Role_Lnk
		support.registerGrpRoleLnk(new String[]{grpId}, inputInfo.getRoleIdSet().toArray(new String[0]));

		// Insert Grp_User_Lnk and User_Role_Lnk
		support.registerGrpUserLnk(new String[]{grpId}, inputInfo.getUserIdSet().toArray(new String[0]));

		// Delete/ Update/ Insert User_Role_Lnk
		support.cleaningAndUpdateUserRoleLnk(inputInfo.getUserIdSet().toArray(new String[0]));

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003077I,inputInfo.getGroupNm());
	}

	private List<GroupRoleLinkViewBean> getRoleViews() {
		List<GroupRoleLinkViewBean> roleViews = new ArrayList<GroupRoleLinkViewBean>();

		for (IRoleEntity roleEntity : this.support.findAllRole()) {
			GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
			condition.setRoleId(roleEntity.getRoleId());
			// Count the number of group belongs to this role
			int grpCount = this.support.getGrpRoleLnkDao().count(condition.getCondition());

			GroupRoleLinkViewBean roleView = new GroupRoleLinkViewBean()
					.setRoleId( roleEntity.getRoleId() )
					.setRoleNm( roleEntity.getRoleName() )
					.setCount(grpCount);

			roleViews.add(roleView);
		}

		return roleViews;
	}

	private List<GroupUserLinkViewBean> getUserViews( Set<String> selectedUserSet ) {
		List<GroupUserLinkViewBean> userViews = new ArrayList<GroupUserLinkViewBean>();

		for (IUserEntity userEntity : this.support.findAllUser()) {
			if ( selectedUserSet.contains(userEntity.getUserId()) ) {
				continue;
			}

			GroupUserLinkViewBean userView = new GroupUserLinkViewBean()
					.setUserId( userEntity.getUserId() )
					.setUserNm( userEntity.getUserNm() )
					.setMailAddress( userEntity.getEmailAddr() )
					.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
			userViews.add(userView);
		}

		return userViews;
	}

	private Set<String> getSelectedRoleIds(String grpId) {
		Set<String> selectedRoleSet = new LinkedHashSet<String>();
		List<IRoleEntity> roleEntities = support.findRoleByGroup(grpId);

		for (IRoleEntity roleEntity : roleEntities) {
			selectedRoleSet.add(roleEntity.getRoleId());
		}
		return selectedRoleSet;
	}

	private List<GroupUserLinkViewBean> getSelectedUserViews(String grpId, Set<String> selectedUserSet) {
		List<GroupUserLinkViewBean> selectedUserViews = new ArrayList<GroupUserLinkViewBean>();

		List<IUserEntity> userEntities = support.findUserByGroup(grpId);
		for (IUserEntity userEntity : userEntities) {
			selectedUserSet.add(userEntity.getUserId());

			GroupUserLinkViewBean userViewBean = new GroupUserLinkViewBean()
					.setUserId( userEntity.getUserId() )
					.setUserNm( userEntity.getUserNm() )
					.setMailAddress( userEntity.getEmailAddr() )
					.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
			selectedUserViews.add(userViewBean);
		}

		return selectedUserViews;

	}

	private void updateGroup(String grpId, String grpNm) {
		IGrpEntity grpEntity = new GrpEntity();
		grpEntity.setGrpId(grpId);
		grpEntity.setGrpNm(grpNm);

		support.getGrpDao().update(grpEntity);
	}

	private int countGrpByName(String grpName, String grpId) {
		if ( TriStringUtils.isEmpty(grpName) ) {
			return 0;
		}

		GrpCondition condition = new GrpCondition();
		condition.setGrpIdsByNotEquals(grpId);
		condition.setGrpNm(grpName);

		return support.getGrpDao().count(condition.getCondition());
	}

}
