package jp.co.blueship.tri.fw.um.domainx.project.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;

public class ProfileDetailsLatestActivityBean {

	private List<Day> days = new ArrayList<Day>();
	private boolean hasNext = false;
	private LatestActivityRequestOption requestOption = LatestActivityRequestOption.none;
	private int linesPerPage = 5;

	public List<Day> getDays() {
		return days;
	}
	public ProfileDetailsLatestActivityBean setDays(List<Day> days) {
		this.days = days;
		return this;
	}

	public boolean hasNext() {
		return hasNext;
	}
	public ProfileDetailsLatestActivityBean setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
		return this;
	}

	public LatestActivityRequestOption getRequestOption() {
		return requestOption;
	}
	public ProfileDetailsLatestActivityBean setRequestOption(LatestActivityRequestOption requestOption) {
		this.requestOption = requestOption;
		return this;
	}

	public int getLinesPerPage() {
		return linesPerPage;
	}
	public ProfileDetailsLatestActivityBean setLinesPerPage(int linesPerPage) {
		this.linesPerPage = linesPerPage;
		return this;
	}

	public enum LatestActivityRequestOption {
		none( "" ),
		more("more"),
		;

		private String value = null;

		private LatestActivityRequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			LatestActivityRequestOption type = value( value );

			return null != type && this.equals(type);
		}

		public String value() {
			return this.value;
		}

		public static LatestActivityRequestOption value( String value ) {
			if(value == null) return null;
			for ( LatestActivityRequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class Day {
		private String date;
		private String dateSubject;
		private List<RecentUpdateViewBean> recentUpdateViews = new ArrayList<RecentUpdateViewBean>();


		public String getDate() {
			return date;
		}
		public Day setDate(String date) {
			this.date = date;
			return this;
		}

		public String getDateSubject() {
			return dateSubject;
		}
		public Day setDateSubject(String dateSubject) {
			this.dateSubject = dateSubject;
			return this;
		}

		public List<RecentUpdateViewBean> getRecentUpdateViews() {
			return recentUpdateViews;
		}
		public Day setRecentUpdateViews(List<RecentUpdateViewBean> recentUpdateViews) {
			this.recentUpdateViews = recentUpdateViews;
			return this;
		}
	}
}
