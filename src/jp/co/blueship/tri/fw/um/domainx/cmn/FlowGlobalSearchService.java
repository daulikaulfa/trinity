package jp.co.blueship.tri.fw.um.domainx.cmn;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.ViewUserAccsHistCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedMemberView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedReleaseView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedRequestView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.RecentlyViewedWikiView;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.ReleaseViewType;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowGlobalSearchServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowGlobalSearchService implements IDomain<FlowGlobalSearchServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IAmFinderSupport amSupport;
	private IBmFinderSupport bmSupport;
	private IRmFinderSupport rmSupport;
	private IUmFinderSupport umSupport;


	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
		this.bmSupport = amSupport.getBmFinderSupport();
	}

	public void setRmSupport(IRmFinderSupport rmSupport) {
		this.rmSupport = rmSupport;
	}

	@Override
	public IServiceDto<FlowGlobalSearchServiceBean> execute(IServiceDto<FlowGlobalSearchServiceBean> serviceDto) {
		FlowGlobalSearchServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if ( RequestType.init.equals(paramBean.getParam().getRequestType())
					|| RequestType.onChange.equals(paramBean.getParam().getRequestType()) ) {

				this.searchRecentlyViewed( paramBean );
			}
			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());

		}
	}

	private void searchRecentlyViewed( FlowGlobalSearchServiceBean paramBean ) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		String keyword = searchCondition.getKeyword();

		if (TriStringUtils.isEmpty( keyword )) {
			paramBean.setRequestViews( new ArrayList<RecentlyViewedRequestView>() );
			paramBean.setReleaseViews( new ArrayList<RecentlyViewedReleaseView>() );
			paramBean.setMemberViews( new ArrayList<RecentlyViewedMemberView>() );

			return;
		}

		ISqlSort sort = this.getRecentlyViewedSort();

		// Get recently viewed request view (am_areq)
		this.getRecentlyViewedRequestView(paramBean, keyword, sort);

		// Get recently viewed release view (rm_rp, bm_bp, rm_ra, deployment)
		this.getRecentlyViewedReleaseView(paramBean, keyword, sort);

		// Get recently viewed member view (um_user - um_accs_hist)
		this.getRecentlyViewedMemberView(paramBean, keyword, sort);

		// Get recently viewed wiki view ( um_wiki - um_accs_hist)
		this.getRecentlyViewedWikiView(paramBean, keyword, sort);
	}

	private void getRecentlyViewedRequestView( FlowGlobalSearchServiceBean paramBean, String keyword, ISqlSort sort ) {
		List<RecentlyViewedRequestView> requestViews = new ArrayList<RecentlyViewedRequestView>();

		LotCondition lotCondition = this.getLotCondition();

		List<String> disableLinkLotNumbers = new ArrayList<>();
		boolean isSearch = this.amSupport.setAccessableLotNumbers(lotCondition, paramBean,false, disableLinkLotNumbers, true);

		if(isSearch) {
			AreqCondition areqCondition = new AreqCondition();
			areqCondition.setContainsByKeyword(keyword);
			areqCondition.setJoinAccsHist(true);
			areqCondition.setLotIds(lotCondition.getLotIds());

			IEntityLimit<IAreqEntity> areqEntityLimit = this.amSupport.getAreqDao().find(areqCondition.getCondition(), sort, 1, paramBean.getParam().getLinesPerPage());

			if (null != areqEntityLimit.getEntities()) {
				for (IAreqEntity entity : areqEntityLimit.getEntities()) {

					RecentlyViewedRequestView view = paramBean.new RecentlyViewedRequestView()
							.setAreqId(entity.getAreqId())
							.setPjtId(entity.getPjtId())
							.setReferenceId(entity.getChgFactorNo())
							.setSubject(entity.getSummary())
							.setCheckinFilePath(AmDesignBusinessRuleUtils.getSharePublicPathOfCheckoutRequest(entity))
							.setCheckoutFilePath(AmDesignBusinessRuleUtils.getSharePublicPathOfCheckinRequest(entity))
							.setAreqCtgCd(AreqCtgCd.value(entity.getAreqCtgCd()))
							.setShowClipboardIcon(AreqCtgCd.value(entity.getAreqCtgCd()) == AreqCtgCd.LendingRequest);

					requestViews.add(view);
				}
			}

			paramBean.setRequestViews(requestViews);
		}

	}

	private void getRecentlyViewedReleaseView( FlowGlobalSearchServiceBean paramBean, String keyword, ISqlSort sort ) {

		List<RecentlyViewedReleaseView> releaseViews = new ArrayList<RecentlyViewedReleaseView>();
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		LotCondition lotCondition = this.getLotCondition();

		List<String> disableLinkLotNumbers = new ArrayList<>();
		boolean isSearch = this.amSupport.setAccessableLotNumbers(lotCondition, paramBean,false, disableLinkLotNumbers, true);
		if(isSearch) {
			// Release package
			{
				RpCondition rpCondition = new RpCondition();
				rpCondition.setContainsByKeyword( keyword );
				rpCondition.setJoinAccsHist( true );
				rpCondition.setLotIds(lotCondition.getLotIds());

				IEntityLimit<IRpEntity> rpEntityLimit = this.rmSupport.getRpDao().find( rpCondition.getCondition() , sort, 1, linesPerPage );

				if (null != rpEntityLimit.getEntities()) {
					for (IRpEntity rpEntity : rpEntityLimit.getEntities()) {
						RecentlyViewedReleaseView rpView = paramBean.new RecentlyViewedReleaseView()
								.setInternalId( rpEntity.getRpId() )
								.setSubject( rpEntity.getRpNm() )
								.setReleaseType( ReleaseViewType.ReleasePackage );

						releaseViews.add( rpView );
					}
				}
			}

			// Build Package
			{
				BpCondition bpCondition = new BpCondition();
				bpCondition.setContainsByKeyword( keyword );
				bpCondition.setJoinAccsHist( true );
				bpCondition.setLotIds(lotCondition.getLotIds());

				IEntityLimit<IBpEntity> bpEntityLimit = this.bmSupport.getBpDao().find( bpCondition.getCondition(), sort, 1, linesPerPage );

				if (null != bpEntityLimit) {
					for (IBpEntity bpEntity : bpEntityLimit.getEntities()) {
						RecentlyViewedReleaseView rpView = paramBean.new RecentlyViewedReleaseView()
								.setInternalId( bpEntity.getBpId() )
								.setSubject( bpEntity.getBpNm() )
								.setReleaseType( ReleaseViewType.BuildPackage );

						releaseViews.add( rpView );
					}
				}
			}

			// Release Request
			{
				RaCondition raCondition = new RaCondition();
				raCondition.setContainsByKeyword( keyword );
				raCondition.setJoinAccsHist( true );
				raCondition.setLotIds(lotCondition.getLotIds());

				IEntityLimit<IRaEntity> raEntityLimit = this.rmSupport.getRaDao().find( raCondition.getCondition(), sort, 1, linesPerPage );
				if (null != raEntityLimit) {
					for (IRaEntity raEntity : raEntityLimit.getEntities()) {
						RecentlyViewedReleaseView raView = paramBean.new RecentlyViewedReleaseView()
								.setInternalId( raEntity.getRaId() )
								.setSubject( raEntity.getRemarks() )
								.setReleaseType( ReleaseViewType.ReleaseRequest );

						releaseViews.add( raView );
					}
				}
			}

			// Deployment Request
			{
				DmDoCondition dmDoCondition = new DmDoCondition();
				dmDoCondition.setContainsByKeyword( keyword );
				dmDoCondition.setJoinAccsHist( true );
				dmDoCondition.setLotIds(lotCondition.getLotIds());

				IEntityLimit<IDmDoEntity> dmDoEntityLimit = this.rmSupport.getDmDoDao().find( dmDoCondition.getCondition(), sort, 1, linesPerPage );
				if (null != dmDoEntityLimit) {
					for (IDmDoEntity dmDoEntity : dmDoEntityLimit.getEntities()) {
						RecentlyViewedReleaseView dmDoview = paramBean.new RecentlyViewedReleaseView()
								.setInternalId( dmDoEntity.getMgtVer() )
								.setSubject( null )
								.setReleaseType( ReleaseViewType.DeploymentJob );

						releaseViews.add( dmDoview );
					}
				}
			}
		}


		paramBean.setReleaseViews( releaseViews );
	}

	private void getRecentlyViewedMemberView( FlowGlobalSearchServiceBean paramBean, String keyword, ISqlSort sort ) {
		List<RecentlyViewedMemberView> memberViews = new ArrayList<RecentlyViewedMemberView>();
		List<IUserEntity> userSameGroup = this.umSupport.findUsersInSameGroupWithCurrentUser( paramBean.getUserId() );
		List<String> userSameGroupIds = new ArrayList<>();
		for (IUserEntity userEntity: userSameGroup) {
			userSameGroupIds.add(userEntity.getUserId());
		}

		ViewUserAccsHistCondition userCondition = new ViewUserAccsHistCondition();
		userCondition.setKeywords( keyword );

		List<IUserEntity> userEntities = this.umSupport.getViewUserAccsHistDao().find(userCondition, sort, paramBean.getParam().getLinesPerPage());

		if (null != userEntities) {
			for (IUserEntity entity : userEntities) {
				if(userSameGroupIds.contains(entity.getUserId())) {
					RecentlyViewedMemberView memberView = paramBean.new RecentlyViewedMemberView()
							.setUserId( entity.getUserId() )
							.setUserNm( entity.getUserNm() )
							.setIconPath( UmDesignBusinessRuleUtils.getUserIconSharePath( entity ) );

					memberViews.add( memberView );
				}
			}
		}

		paramBean.setMemberViews( memberViews );
	}

	private void getRecentlyViewedWikiView(FlowGlobalSearchServiceBean paramBean, String keyword, ISqlSort sort) {
		List<RecentlyViewedWikiView> wikiViews = new ArrayList<RecentlyViewedWikiView>();

		WikiCondition wikiCondition = new WikiCondition();
		wikiCondition.setJoinAccsHist(true);
		wikiCondition.setKeyword(keyword);

		LotCondition lotCondition = this.getLotCondition();

		List<String> disableLinkLotNumbers = new ArrayList<>();
		boolean isSearch = this.amSupport.setAccessableLotNumbers(lotCondition, paramBean, false, disableLinkLotNumbers, true);

		if(isSearch) {
			wikiCondition.setLotIds(lotCondition.getLotIds());

			IEntityLimit<IWikiEntity> wikiEntities = this.umSupport.getWikiDao().find(wikiCondition.getCondition(), sort, 1, paramBean.getParam().getLinesPerPage());

			if (null != wikiEntities) {
				for(IWikiEntity entity : wikiEntities.getEntities()) {
					RecentlyViewedWikiView wikiView = paramBean.new RecentlyViewedWikiView();
					wikiView.setLotId(entity.getLotId());
					wikiView.setWikiId(entity.getWikiId());
					wikiView.setWikiNm(entity.getPageNm());

					wikiViews.add(wikiView);
				}
			}
		}

		paramBean.setWikiViews(wikiViews);
	}

	private LotCondition getLotCondition() {

		LotCondition lotCondition = new LotCondition();
		List<String> stsIds = new ArrayList<>();
		stsIds.add( AmLotStatusId.LotInProgress.getStatusId() );
		stsIds.add( AmLotStatusId.LotClosed.getStatusId() );
		lotCondition.setStsIds( FluentList.from(stsIds).toArray(new String[0]) );

		return lotCondition;
	}

	private ISqlSort getRecentlyViewedSort() {
		ISqlSort sort = new SortBuilder();

		sort.setElement(AccsHistItems.accsTimestamp, TriSortOrder.Desc, 1);

		return sort;
	}
}
