package jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiPageViewBean {
	private String wikiId;
	private String wikiNm;

	public String getWikiId(){
		return this.wikiId;
	}
	public WikiPageViewBean setWikiId( String wikiId ){
		this.wikiId = wikiId;
		return this;
	}

	public String getWikiNm(){
		return this.wikiNm;
	}
	public WikiPageViewBean setWikiNm( String wikiNm ){
		this.wikiNm = wikiNm;
		return this;
	}
}
