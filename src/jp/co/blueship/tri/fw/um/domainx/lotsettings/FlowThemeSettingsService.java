package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.support.AmFinderSupport;
import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmThemeEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowThemeSettingsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowThemeSettingsServiceBean.ThemeSettingsInputInfo;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class FlowThemeSettingsService implements IDomain<FlowThemeSettingsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private AmFinderSupport support;

	public void setSupport(AmFinderSupport support) {
		this.support = support;
	}

	@Override
	public IServiceDto<FlowThemeSettingsServiceBean> execute(IServiceDto<FlowThemeSettingsServiceBean> serviceDto) {

		FlowThemeSettingsServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String lotId = paramBean.getParam().getSelectedLotId();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.uploadCustomIcon(paramBean, lotId);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowThemeSettingsServiceBean paramBean) {
		ThemeSettingsInputInfo inputInfo = paramBean.getParam().getInputInfo();

		inputInfo.setIconViews( UmThemeEditServiceUtils.getLotIconViews(paramBean.getParam().getSelectedLotId()) );

		LotCondition condition = new LotCondition();
		condition.setLotId(paramBean.getParam().getSelectedLotId());
		ILotEntity lotEntity = support.getLotDao().findByPrimaryKey(condition.getCondition());
		if(IconSelectionOption.CustomImage.value().equals(lotEntity.getIconTyp())) {
			inputInfo.setIconOption(IconSelectionOption.CustomImage);
			inputInfo.setIconFilePath(UmDesignBusinessRuleUtils.getLotIconSharePath(lotEntity));
		} else {
			inputInfo.setIconPath(UmDesignBusinessRuleUtils.getLotIconSharePath(lotEntity));
		}
	}

	private void uploadCustomIcon(FlowThemeSettingsServiceBean paramBean, String lotId) {
		ThemeSettingsInputInfo inputBean = paramBean.getParam().getInputInfo();

		if (null == inputBean.getIconInputStreamBytes()) {
			return;
		}

		BufferedInputStream inBuffer = null;
		BufferedOutputStream outBuffer = null;
		String filePath = null;
		try {

			filePath = UmDesignBusinessRuleUtils.getLotCustomIconLocalPath(lotId,inputBean.getIconFileNm());
			String absolutePath = filePath.substring(0, filePath.lastIndexOf("/"));
			InputStream is = StreamUtils.convertBytesToInputStreamToBytes(inputBean.getIconInputStreamBytes());

			File userIconDir = new File(absolutePath);
			if (!userIconDir.exists()) {
				userIconDir.mkdirs();
			}

			if (null == is)
				return;

			inBuffer = new BufferedInputStream(is);

			FileOutputStream fos = new FileOutputStream(new File(userIconDir, inputBean.getIconFileNm()));
			outBuffer = new BufferedOutputStream(fos);

			int contents = 0;
			while ((contents = inBuffer.read()) != -1) {
				outBuffer.write(contents);
			}

		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe);
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}

		if (new File(filePath).exists()) {
			String relativePath = UmDesignBusinessRuleUtils.getLotIconSharePath(lotId,inputBean.getIconFileNm(),IconSelectionOption.CustomImage);

			inputBean.setIconFilePath(relativePath);
			inputBean.setIconOption(IconSelectionOption.CustomImage);
		}

	}

	private void submitChanges(FlowThemeSettingsServiceBean paramBean) throws IOException {
		ThemeSettingsInputInfo inputInfo = paramBean.getParam().getInputInfo();
		String lotId = paramBean.getParam().getSelectedLotId();
		// get old lot entity
		LotCondition condition = new LotCondition();
		condition.setLotId(lotId);
		ILotEntity entity = support.getLotDao().findByPrimaryKey(condition.getCondition());

		// update lot entity
		entity.setIconTyp(inputInfo.getIconOption().value());

		// default icon path
		if (IconSelectionOption.DefaultImage.equals(inputInfo.getIconOption())) {
			entity.setDefaultIconPath(inputInfo.getIconPath());
			paramBean.getResult().setLotIconPath(inputInfo.getIconPath());
		}

		// custom icon path
		if (IconSelectionOption.CustomImage.equals(inputInfo.getIconOption())) {
			if ( TriStringUtils.isEmpty( inputInfo.getIconFilePath()) ) {
				throw new ContinuableBusinessException(UmMessageId.UM001068E);
			}
			String[] tokens = inputInfo.getIconFilePath().split("\\.(?=[^\\.]+$)");
			if(tokens.length != 2 || !( tokens[1].equalsIgnoreCase("jpg") 
							|| tokens[1].equalsIgnoreCase("png") 
							|| tokens[1].equalsIgnoreCase("gif") ) ) {
				throw new ContinuableBusinessException(UmMessageId.UM001072E);
			}
			entity.setCustomIconPath(inputInfo.getIconFilePath());
			paramBean.getResult().setLotIconPath(inputInfo.getIconFilePath());
		}
		entity.setThemeColor(inputInfo.getThemeColor().id());

		support.getLotDao().update(entity);
		paramBean.getResult().setCompleted(true);
		paramBean.getResult().setLotThemeColor(inputInfo.getThemeColor());
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003055I);
	}

}
