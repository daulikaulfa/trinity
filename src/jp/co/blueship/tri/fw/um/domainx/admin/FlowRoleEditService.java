package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.*;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean.CheckedState;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmResourceSelectionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleGroupLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleEditServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRoleEditService implements IDomain<FlowRoleEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowRoleEditServiceBean> execute(
			IServiceDto<FlowRoleEditServiceBean> serviceDto) {
		FlowRoleEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String roleId = paramBean.getParam().getSelectedRoleId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(roleId), "SelectedRoleId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean, roleId);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean, roleId);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowRoleEditServiceBean paramBean, String roleId) {
		RoleEditInputBean inputBean = paramBean.getParam().getInputInfo();
		Set<String> actionIdSet = inputBean.getActionIdSet();
		IRoleEntity roleEntity = support.findRoleByRoleId(roleId);
		if (null == roleEntity) {
			return;
		}

		SvcCtgCondition condition = new SvcCtgCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement( SvcCtgItems.sortOdr, TriSortOrder.Asc , 1 );

		List<ISvcCtgEntity> svcCtgEntities = support.getSvcCtgDao().find(condition.getCondition(),sort);
		if (!ProductActivate.isDmActive()) {
			Iterator<ISvcCtgEntity> svcCtgEntityIterator = svcCtgEntities.iterator();
			while (svcCtgEntityIterator.hasNext()){

				ISvcCtgEntity svcCtgEntity = svcCtgEntityIterator.next();
				if (svcCtgEntity.getSvcCtgId().startsWith("FlowRelDistribute")
						|| svcCtgEntity.getSvcCtgId().startsWith("FlowDeployment")
						|| svcCtgEntity.getSvcCtgId().startsWith("FlowPrintDeployment")) {
					svcCtgEntityIterator.remove();
				}
			}
		}
		ITreeFolderViewBean tree = UmResourceSelectionUtils.getTreeActionView(svcCtgEntities);
		UmResourceSelectionUtils.convertToLocale( tree, paramBean.getLanguage() );

		paramBean.getActionSelectionView().setFolderView(tree);

		this.setSelectedActions(roleId, actionIdSet, tree);

		inputBean.setRoleNm(roleEntity.getRoleName())
				.setGroupViews(this.getGroupViews())
				.setGroupIdSet(this.getSelectedGroups(roleId))
				.setActionIdSet(actionIdSet);

	}

	private void submitChanges(FlowRoleEditServiceBean paramBean, String roleId) {
		RoleEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		this.setSelectedActions( inputInfo.getActionIdSet(), paramBean.getActionSelectionView().getFolderView());

		int count = this.countRoleByName(inputInfo.getRoleNm(), roleId);

		UmItemChkUtils.checkInputRole(inputInfo, count, support);
		UmItemChkUtils.checkAdminRole(roleId);

		updateRole(roleId, inputInfo.getRoleNm());

		updateRoleSvcCtgLnk(roleId, inputInfo.getActionIdSet());

		// Delete grp_role_lnk
		support.cleaningGrpRoleLnk(null, roleId);

		// Delete User_Role_Lnk
		support.cleaningUserRoleLnk(null, roleId);

		// Insert Grp_Role_Lnk
		support.registerGrpRoleLnk(inputInfo.getGroupIdSet().toArray(new String[0]), new String[]{roleId});

		List<IGrpUserLnkEntity> grpUserLnkEntityList = support.findGrpUserLnkByGrpId( inputInfo.getGroupIdSet().toArray(new String[0]) );
		List<String> userList = FluentList.from(grpUserLnkEntityList).map(UmFluentFunctionUtils.toUserIdFromGrpUserLnkEntity).asList();


		// Delete/ Insert user_role_lnk
		if (TriStringUtils.isNotEmpty(userList)) {
			support.cleaningAndUpdateUserRoleLnk(userList.toArray(new String[0]));
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003080I,inputInfo.getRoleNm());

		this.init(paramBean, roleId);
	}

	private Set<String> getSelectedGroups(String roleId) {

		Set<String> roleIdSet = new LinkedHashSet<String>();
		for (IGrpRoleLnkEntity entity : support.findGrpRoleLnkByRoleId(roleId)) {
			roleIdSet.add(entity.getGrpId());
		}

		return roleIdSet;
	}

	private void setSelectedActions(String roleId, Set<String> actionIdSet, ITreeFolderViewBean tree) {
		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		condition.setRoleId(roleId);

		List<IRoleSvcCtgLnkEntity> roleSvcCtgEntities = support.getRoleSvcCtgLnkDao().find(condition.getCondition());
		if (TriCollectionUtils.isNotEmpty(roleSvcCtgEntities)) {
			for (IRoleSvcCtgLnkEntity entity : roleSvcCtgEntities) {
				actionIdSet.add(entity.getSvcCtgId());

				SvcCtgCondition svcCondition = new SvcCtgCondition();
				svcCondition.setSvcCtgId(entity.getSvcCtgId());

				ISvcCtgEntity svcEntity = support.getSvcCtgDao().findByPrimaryKey(svcCondition.getCondition());
				if (svcEntity != null) {
					ITreeFolderViewBean serviceAction = tree.getFolder(svcEntity.getSvcCtgPath());
					if (null != serviceAction) {
						serviceAction.setCheckedState(CheckedState.Checked);
					}
				}
			}
		}

	}

	private void setSelectedActions(Set<String> actionIdSet, ITreeFolderViewBean tree) {
		tree.setCheckedState(CheckedState.UnChecked);

		if (TriStringUtils.isNotEmpty(actionIdSet)) {

			SvcCtgCondition condition = new SvcCtgCondition();
			condition.setSvcCtgIds(actionIdSet.toArray(new String[0]));
			List<ISvcCtgEntity> svcCtgEntities = support.getSvcCtgDao().find(condition.getCondition());

			for (ISvcCtgEntity entity : svcCtgEntities) {
				ITreeFolderViewBean serviceAction = tree.getFolder(entity.getSvcCtgPath());

				if (null != serviceAction) {
					serviceAction.setCheckedState(CheckedState.Checked);
				}
			}
		}
	}

	private List<RoleGroupLinkViewBean> getGroupViews() {
		List<RoleGroupLinkViewBean> groupViews = new ArrayList<RoleGroupLinkViewBean>();
		for (IGrpEntity grpEntity : this.support.findAllGroup()) {
			GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
			condition.setGrpId(grpEntity.getGrpId());
			int count = this.support.getGrpRoleLnkDao().count(condition.getCondition());

			RoleGroupLinkViewBean grpView = new RoleGroupLinkViewBean()
				.setGrpId( grpEntity.getGrpId() )
				.setGrpNm( grpEntity.getGrpNm())
				.setCount( count );

			groupViews.add(grpView);
		}

		return groupViews;
	}

	private int countRoleByName(String roleNm, String roleId) {
		if (TriStringUtils.isEmpty(roleNm)) {
			return 0;
		}

		RoleCondition condition = new RoleCondition();
		condition.setRoleIdsByNotEquals(roleId);
		condition.setRoleName(roleNm);

		return support.getRoleDao().count(condition.getCondition());
	}

	private void updateRole(String roleId, String roleName) {
		IRoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleId(roleId);
		roleEntity.setRoleName(roleName);

		support.getRoleDao().update(roleEntity);
	}

	private void updateRoleSvcCtgLnk(String roleId, Set<String> svcCtgIdSet) {
		Set<String> v3SvcCtgIds = this.getSvcCtgIdV3Set(roleId);
		v3SvcCtgIds.addAll(svcCtgIdSet);
		// Delete old data in Um_Role_Svc_Ctg_Lnk
		{
			RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
			condition.setRoleId(roleId);

			support.getRoleSvcCtgLnkDao().delete(condition.getCondition());
		}

		// Add new service actions Um_Role_Svc_Ctg_Lnk
		{
			for (String svcCtgId : v3SvcCtgIds) {
				IRoleSvcCtgLnkEntity roleSvcCtgEntity = new RoleSvcCtgLnkEntity();
				roleSvcCtgEntity.setRoleId( roleId );
				roleSvcCtgEntity.setSvcCtgId( svcCtgId );

				support.getRoleSvcCtgLnkDao().insert( roleSvcCtgEntity );
			}
		}
	}

	private Set<String> getSvcCtgIdV3Set(String roleId){
		List<String> svcCtgIds = this.support.getSvcCtgIdByRoleId(roleId);
		Set<String> v3SvcIds = new HashSet<String>();
		SvcCtgCondition condition = new SvcCtgCondition();
		condition.setSvcCtgIds(svcCtgIds.toArray(new String[0]));
		List<ISvcCtgEntity> entities = this.support.getSvcCtgDao().find(condition.getCondition());
		for (ISvcCtgEntity entity: entities ) {
			if(TriStringUtils.isEmpty(entity.getSvcCtgPath())) {
				v3SvcIds.add(entity.getSvcCtgId());
			}
		}
		return v3SvcIds;
	}
}
