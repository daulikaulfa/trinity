package jp.co.blueship.tri.fw.um.domainx.lotsettings.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowMilestoneCreationServiceBean extends DomainServiceBean {
	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private String lotId = null;
		private MilestoneEditInputBean inputInfo = new MilestoneEditInputBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public MilestoneEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(MilestoneEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}
	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String mstoneId = null;
		private boolean completed = false;

		public String getMstoneId() {
			return mstoneId;
		}

		public RequestsCompletion setMstoneId(String mstoneId) {
			this.mstoneId = mstoneId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowMilestoneCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}
	public RequestParam getParam() {
		return param;
	}
}
