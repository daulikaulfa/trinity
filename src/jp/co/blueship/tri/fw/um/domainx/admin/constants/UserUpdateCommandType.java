package jp.co.blueship.tri.fw.um.domainx.admin.constants;

/**
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */

public enum UserUpdateCommandType {
	add("add"),
	edit("edit"),
	delete("delete"),
	;

	private String value = null;
	UserUpdateCommandType(String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}
}