package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentUserViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentEditServiceBean.DepartmentRequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowDepartmentEditService implements IDomain<FlowDepartmentEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowDepartmentEditServiceBean> execute(
			IServiceDto<FlowDepartmentEditServiceBean> serviceDto) {
		FlowDepartmentEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String deptId = paramBean.getParam().getSelectedDeptId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(deptId), "SelectedDeptId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean, deptId);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if (DepartmentRequestOption.ExcludeUser.equals(paramBean.getParam().getRequestOption())) {
					this.removeUser(deptId, paramBean);
				}

			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean, deptId);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowDepartmentEditServiceBean paramBean, String deptId) {
		String deptNm = support.findDeptByDeptId(deptId).getDeptNm();
		Set<String> selectedUserSet = new LinkedHashSet<String>();

		paramBean.setUserViews(this.getSelectedUserViews(deptId, selectedUserSet));

		paramBean.getParam().getInputInfo()
			.setDeptNm(deptNm)
			.setUserIdSet(selectedUserSet)
			.setUserViews(this.getUserViews( selectedUserSet ));
	}

	private List<DepartmentUserViewBean> getSelectedUserViews(String deptId, Set<String> selectedUserSet) {
		List<DepartmentUserViewBean> selectedUserViews = new ArrayList<DepartmentUserViewBean>();
		UserCondition condition = new UserCondition();
		condition.setDeptId(deptId);

		List<IUserEntity> userEntities = support.getUserDao().find(condition.getCondition());

		for (IUserEntity userEntity: userEntities) {
			selectedUserSet.add(userEntity.getUserId());

			DepartmentUserViewBean userViewBean = new DepartmentUserViewBean()
				.setUserId(userEntity.getUserId())
				.setUserNm(userEntity.getUserNm())
				.setMailAddress(userEntity.getEmailAddr())
				.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
			selectedUserViews.add(userViewBean);
		}

		return selectedUserViews;
	}

	private List<DepartmentUserViewBean> getUserViews( Set<String> selectedUserSet ) {
		List<DepartmentUserViewBean> views = new ArrayList<DepartmentUserViewBean>();

		for (IUserEntity entity: this.support.findAllUser()) {
			if ( selectedUserSet.contains(entity.getUserId()) ) {
				continue;
			}

			DepartmentUserViewBean userView = new DepartmentUserViewBean()
				.setUserId(entity.getUserId())
				.setUserNm(entity.getUserNm())
				.setMailAddress(entity.getEmailAddr())
				.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(entity));

			views.add(userView);
		}

		return views;
	}

	private void removeUser(String deptId, FlowDepartmentEditServiceBean paramBean) {
		String excludeUserId = paramBean.getParam().getInputInfo().getExcludeUserId();
		if (TriStringUtils.isNotEmpty(excludeUserId)) {
			Set<String> selectedUserSet = paramBean.getParam().getInputInfo().getUserIdSet();
			selectedUserSet.remove(excludeUserId);

			validateUnselectedUser(deptId, excludeUserId, paramBean.getUserId());
			updateUsers(excludeUserId, "", "");
			paramBean.setUserViews(this.getSelectedUserViews(deptId, selectedUserSet));
			paramBean.getParam().getInputInfo().setUserViews(this.getUserViews( selectedUserSet ));
		}
	}

	private void validateUnselectedUser(String deptId, String userId, String loginUserId) {
		if( UmItemChkUtils.ADMIN.equals(userId) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001042E );
		}

		if( loginUserId.equals( userId ) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001041E );
		}
	}

	private void submitChanges(FlowDepartmentEditServiceBean paramBean, String deptId) {
		DepartmentEditInputBean inputInfo = paramBean.getParam().getInputInfo();
		int count = this.countDeptByName(inputInfo.getDeptNm(), deptId);

		UmItemChkUtils.checkInputDepartment(inputInfo, count, support);
		updateDepartment(inputInfo, deptId);
		for (String userId : inputInfo.getUserIdSet()) {
			updateUsers(userId, deptId, inputInfo.getDeptNm());
		}
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003074I,inputInfo.getDeptNm());
	}

	private void updateDepartment(DepartmentEditInputBean bean, String deptId) {
		IDeptEntity deptEntity = new DeptEntity();
		deptEntity.setDeptId(deptId);
		deptEntity.setDeptNm(bean.getDeptNm());

		support.getDeptDao().update(deptEntity);
	}

	private void updateUsers(String userId, String deptId, String deptNm) {

		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId(userId);
		userEntity.setDeptId(deptId);
		userEntity.setDeptNm(deptNm);
		support.getUserDao().update(userEntity);
	}

	private int countDeptByName(String deptNm, String deptId) {
		if ( TriStringUtils.isEmpty(deptNm) ) {
			return 0;
		}

		DeptCondition condition = new DeptCondition();
		condition.setDeptIdsByNotEquals(deptId);
		condition.setDeptNm(deptNm);

		return support.getDeptDao().count(condition.getCondition());
	}

}
