package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;



/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<WikiView> wikiViews = new ArrayList<WikiView>();

	/**
	 * This is tag list of side bar.
	 */
	private List<WikiTagViewBean> tagViews = new ArrayList<WikiTagViewBean>();
	/**
	 * This is wiki page list of side bar.
	 */
	private List<WikiPageViewBean> pageViews = new ArrayList<WikiPageViewBean>();

	public RequestParam getParam() {
		return this.param;
	}

	public List<WikiView> getWikiViews(){
		return this.wikiViews;
	}
	public FlowWikiListServiceBean setWikiViews( List<WikiView> wikiViews ){
		this.wikiViews = wikiViews;
		return this;
	}

	public List<WikiTagViewBean> getTagViews(){
		return this.tagViews;
	}
	public FlowWikiListServiceBean setTagViews( List<WikiTagViewBean> tagViews ){
		this.tagViews = tagViews;
		return this;
	}

	public List<WikiPageViewBean> getPageViews(){
		return this.pageViews;
	}
	public FlowWikiListServiceBean setPageViews( List<WikiPageViewBean> pageViews ){
		this.pageViews = pageViews;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId;
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();

		private RequestOption requestOption = RequestOption.none;
		/**
		 * This is search word of side bar wiki page list.
		 */
		private String searchPageNm;

		public String getSelectedLotId() {
			return this.lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return this.searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return this.orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public RequestOption getRequestOption(){
			return this.requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption){
			this.requestOption = requestOption;
			return this;
		}

		public String getSearchPageNm() {
			return this.searchPageNm;
		}
		public RequestParam setSearchPageNm(String searchPageNm) {
			this.searchPageNm = searchPageNm;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		private String tagNm;
		private String keyword;

		public String getTagNm() {
			return this.tagNm;
		}
		public SearchCondition setTagNm(String tagNm) {
			this.tagNm = tagNm;
			return this;
		}

		public String getKeyword() {
			return this.keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder wikiNm = TriSortOrder.Asc;
		private TriSortOrder updDate = TriSortOrder.Asc;

		public TriSortOrder getWikiNm() {
			return this.wikiNm;
		}
		public OrderBy setWikiNm(TriSortOrder wikiNm) {
			this.wikiNm = wikiNm;
			return this;
		}

		public TriSortOrder getUpdDate() {
			return this.updDate;
		}
		public OrderBy setUpdDate(TriSortOrder updDate) {
			this.updDate = updDate;
			return this;
		}
	}

	public enum RequestOption {
		none			( "" ),
		All				( "all" ),
		WikiPageList	( "wikiPageList" ),
		TagList			( "tagList" ),
		WikiListService	( "wikiListService" ),//this service
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( !this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) )
				return null;

			for ( RequestOption type : values() )
				if ( type.value().equals( value ) )
					return type;

			return none;
		}
	}

	public class WikiView {
		private String wikiId;
		private String wikiNm;
		private String content;
		private String updDate;
		private String updUserNm;
		private String updUserId;
		private String updUserIconPath;
		private List<String> tags = new ArrayList<String>();

		public String getWikiId(){
			return this.wikiId;
		}
		public WikiView setWikiId( String wikiId ){
			this.wikiId = wikiId;
			return this;
		}

		public String getWikiNm(){
			return this.wikiNm;
		}
		public WikiView setWikiNm( String wikiNm ){
			this.wikiNm = wikiNm;
			return this;
		}

		public String getContent(){
			return this.content;
		}
		public WikiView setContent( String content ){
			this.content = content;
			return this;
		}

		public String getUpdDate(){
			return this.updDate;
		}
		public WikiView setUpdDate( String updDate ){
			this.updDate = updDate;
			return this;
		}

		public String getUpdUserNm(){
			return this.updUserNm;
		}
		public WikiView setUpdUserNm( String updUserNm ){
			this.updUserNm = updUserNm;
			return this;
		}

		public String getUpdUserId(){
			return this.updUserId;
		}
		public WikiView setUpdUserId( String updUserId ){
			this.updUserId = updUserId;
			return this;
		}

		public String getUpdUserIconPath(){
			return this.updUserIconPath;
		}
		public WikiView setUpdUserIconPath( String updUserIconPath ){
			this.updUserIconPath = updUserIconPath;
			return this;
		}

		public List<String> getTags(){
			return this.tags;
		}
		public WikiView setTags( List<String> tags ){
			this.tags = tags;
			return this;
		}
	}
}
