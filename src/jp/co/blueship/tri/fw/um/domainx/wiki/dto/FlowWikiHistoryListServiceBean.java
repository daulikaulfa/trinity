package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiHistoryListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private String wikiNm;
	private List<WikiVersionView> versionViews = new ArrayList<WikiVersionView>();

	/**
	 * This is tag list of side bar.
	 */
	private List<WikiTagViewBean> tagViews = new ArrayList<WikiTagViewBean>();
	/**
	 * This is wiki page list of side bar.
	 */
	private List<WikiPageViewBean> pageViews = new ArrayList<WikiPageViewBean>();

	public RequestParam getParam() {
		return param;
	}

	public String getWikiNm(){
		return this.wikiNm;
	}
	public FlowWikiHistoryListServiceBean setWikiNm( String wikiNm ){
		this.wikiNm = wikiNm;
		return this;
	}

	public List<WikiVersionView> getVersionViews(){
		return this.versionViews;
	}
	public FlowWikiHistoryListServiceBean setVersionViews( List<WikiVersionView> versionViews ){
		this.versionViews = versionViews;
		return this;
	}

	public List<WikiTagViewBean> getTagViews(){
		return this.tagViews;
	}
	public FlowWikiHistoryListServiceBean setTagViews( List<WikiTagViewBean> tagViews ){
		this.tagViews = tagViews;
		return this;
	}

	public List<WikiPageViewBean> getPageViews(){
		return this.pageViews;
	}
	public FlowWikiHistoryListServiceBean setPageViews( List<WikiPageViewBean> pageViews ){
		this.pageViews = pageViews;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private String wikiId;
		private RequestOption requestOption = RequestOption.none;
		/**
		 * This is search word of Wiki page list.
		 */
		private String searchPageNm;

		public String getSelectedWikiId() {
			return this.wikiId;
		}
		public RequestParam setSelectedWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public RequestOption getRequestOption(){
			return this.requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption){
			this.requestOption = requestOption;
			return this;
		}

		public String getSearchPageNm() {
			return this.searchPageNm;
		}
		public RequestParam setSearchPageNm(String searchPageNm) {
			this.searchPageNm = searchPageNm;
			return this;
		}
	}

	public enum RequestOption {
		none				( "" ),
		All					( "all" ),
		WikiPageList		( "wikiPageList" ),
		TagList				( "tagList" ),
		WistoryListService	( "historyListService" ),//this service
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( !this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) )
				return null;

			for ( RequestOption type : values() )
				if ( type.value().equals( value ) )
					return type;

			return none;
		}
	}

	public class WikiVersionView {
		private Integer wikiVerNo;
		private String updUserNm;
		private String updUserId;
		private String updUserIconPath;
		private String updDate;

		public Integer getWikiVerNo(){
			return this.wikiVerNo;
		}
		public WikiVersionView setWikiVerNo( Integer wikiVerNo ){
			this.wikiVerNo = wikiVerNo;
			return this;
		}

		public String getUpdUserNm(){
			return this.updUserNm;
		}
		public WikiVersionView setUpdUserNm( String updUserNm ){
			this.updUserNm = updUserNm;
			return this;
		}

		public String getUpdUserId(){
			return this.updUserId;
		}
		public WikiVersionView setUpdUserId( String updUserId ){
			this.updUserId = updUserId;
			return this;
		}

		public String getUpdUserIconPath(){
			return this.updUserIconPath;
		}
		public WikiVersionView setUpdUserIconPath( String updUserIconPath ){
			this.updUserIconPath = updUserIconPath;
			return this;
		}

		public String getUpdDate(){
			return this.updDate;
		}
		public WikiVersionView setUpdDate( String updDate ){
			this.updDate = updDate;
			return this;
		}
	}
}
