package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class RoleGroupLinkViewBean {
	private String grpId = null;
	private String grpNm = null;
	private String iconPath = null;
	private int count = 0;
	
	public String getGrpId() {
		return grpId;
	}
	
	public RoleGroupLinkViewBean setGrpId(String grpId) {
		this.grpId = grpId;
		return this;
	}
	
	public String getGrpNm() {
		return grpNm;
	}
	
	public RoleGroupLinkViewBean setGrpNm(String grpNm) {
		this.grpNm = grpNm;
		return this;
	}
	
	public String getIconPath() {
		return iconPath;
	}
	
	public RoleGroupLinkViewBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}
	
	public int getCount() {
		return count;
	}
	
	public RoleGroupLinkViewBean setCount(int count) {
		this.count = count;
		return this;
	}
}
