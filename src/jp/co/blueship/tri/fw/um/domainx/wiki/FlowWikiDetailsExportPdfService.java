package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.dcm.DcmExportToFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiPageListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiTagListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiDetailsExportPdfServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiDetailsExportPdfService implements IDomain<FlowWikiDetailsExportPdfServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	private WikiPageListService wikiPageListService = null;
	private WikiTagListService wikiTagListService = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	public void setWikiPageListService(WikiPageListService wikiPageListService) {
		this.wikiPageListService = wikiPageListService;
	}

	public void setWikiTagListService(WikiTagListService wikiTagListService) {
		this.wikiTagListService = wikiTagListService;
	}

	@Override
	public IServiceDto<FlowWikiDetailsExportPdfServiceBean> execute(
			IServiceDto<FlowWikiDetailsExportPdfServiceBean> serviceDto) {

		FlowWikiDetailsExportPdfServiceBean paramBean = serviceDto.getServiceBean();

		try {
			String lotId = paramBean.getParam().getLotId();
			String wikiId = paramBean.getParam().getWikiId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId) || TriStringUtils.isNotEmpty(lotId),
					"SelectedId is not specified");
			// get wiki details
			this.createPdf(paramBean);
			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void createPdf(FlowWikiDetailsExportPdfServiceBean paramBean) throws IOException {

		// get wiki entity
		String wikiId = paramBean.getParam().getWikiId();
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);
		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());
		List<String> headers = new ArrayList<String>();
		headers.add(wikiEntity.getPageNm());
		List<String> contents = new ArrayList<String>();
		headers.add(wikiEntity.getContent());

		// export to pdf file
		ExportToFileBean fileBean = new ExportToFileBean();
		fileBean.setTitleHeader(headers);
		fileBean.addRow(contents);
		Path directory = Paths
				.get(DcmDesignBusinessRuleUtils.getDownloadTempPath(paramBean.getUserId()).getAbsolutePath());
		DcmExportToFileUtils.createPdfFile(directory, fileBean);
	}
}
