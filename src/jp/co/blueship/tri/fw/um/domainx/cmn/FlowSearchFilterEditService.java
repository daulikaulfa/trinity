package jp.co.blueship.tri.fw.um.domainx.cmn;


import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterEntity;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterEditServiceBean.SearchFilterEditInput;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowSearchFilterEditService implements IDomain<FlowSearchFilterEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowSearchFilterEditServiceBean> execute(
			IServiceDto<FlowSearchFilterEditServiceBean> serviceDto) {

		FlowSearchFilterEditServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String filterId = paramBean.getParam().getFilterId();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(filterId), "SelectedFilterId is not specified");

//			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
//				IFilterEntity filterEntity = support.findCtgByPrimaryKey(ctgId);
//
//				SearchFilterEditInput inputInfo = new SearchFilterEditInput();
//				inputInfo.setFilterNm(filterEntity.getFilterNm());
//				paramBean.getParam().setInputInfo(inputInfo);
//			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())){
				SearchFilterEditInput inputInfo = paramBean.getParam().getInputInfo();

				this.updateSearchFilter(inputInfo, filterId);
				paramBean.getResult().setCompleted(true);
				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003014I);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}



	private void updateSearchFilter(SearchFilterEditInput inputInfo, String filterId) {
		IFilterEntity filterEntity = new FilterEntity();
		filterEntity.setFilterId(filterId);
		filterEntity.setFilterNm(inputInfo.getFilterNm());
		support.getFilterDao().update(filterEntity);
	}
}






