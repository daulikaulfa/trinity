package jp.co.blueship.tri.fw.um.domainx.project;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistCondition;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserUrlEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlCondition;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.GanttChartService;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttContent;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttHeader;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.domainx.project.beans.RecentUpdateService;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean.GanttChartRequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsGanttChartBean.Span;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean.Day;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsLatestActivityBean.LatestActivityRequestOption;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.ProfileDetailsViewBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileDetailsServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

/**
 * @version V4.00.00
 * @author Thang.vu
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowProfileDetailsService implements IDomain<FlowProfileDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport;
	private IAmFinderSupport amSupport;

	private RecentUpdateService recentUpdateService;
	private GanttChartService ganttChartService = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}

	public void setRecentUpdateService(RecentUpdateService recentUpdateService){
		this.recentUpdateService = recentUpdateService;
	}
	public void setGanttChartService(GanttChartService ganttChartService){
		this.ganttChartService = ganttChartService;
	}

	@Override
	public IServiceDto<FlowProfileDetailsServiceBean> execute(IServiceDto<FlowProfileDetailsServiceBean> serviceDto) {
		FlowProfileDetailsServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if( RequestType.init.equals( paramBean.getParam().getRequestType() ) )
				this.init(paramBean);

			if( RequestType.onChange.equals( paramBean.getParam().getRequestType() ) )
				this.onChange(paramBean);


			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowProfileDetailsServiceBean paramBean) {
		this.getProfileDetails(paramBean);
		this.onChange(paramBean);
	}


	private void onChange( FlowProfileDetailsServiceBean paramBean ){

		RequestOption requestOption = paramBean.getParam().getRequestOption();

		if( RequestOption.latestActivity.equals( requestOption ) ) {
			this.getLatestActivity(paramBean);

		} else if( RequestOption.ganttChart.equals( requestOption ) ) {
			this.getGanttChart(paramBean);
		}
	}

	/**
	 *
	 * @param paramBean
	 */
	private void getProfileDetails(FlowProfileDetailsServiceBean paramBean) {
		// set parameter for finding
		String userId = paramBean.getParam().getSelectedUserId();

		// get user url list
		UserUrlCondition urlCondition = new UserUrlCondition();
		urlCondition.setUserId(userId);
		List<IUserUrlEntity> userUrls = umSupport.getUserUrlDao().find(urlCondition.getCondition());

		// get user entity
		UserCondition condition = new UserCondition();
		condition.setUserId(userId);
		IUserEntity userEntity = umSupport.getUserDao().findByPrimaryKey(condition.getCondition());

		// set result to view
		ProfileDetailsViewBean beanView = new ProfileDetailsViewBean();
		beanView.setBiography(userEntity.getBiography());
		if ( TriStringUtils.isNotEmpty( userEntity.getGenderTyp() ) ) {
			beanView.setGenderType(UmDesignBusinessRuleUtils.getGender(userEntity));
		}
		beanView.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
		beanView.setUrls(getUserUrl(userUrls));
		beanView.setUserId(userId);
		beanView.setUserLocation(userEntity.getUserLocation());
		beanView.setUserNm(userEntity.getUserNm());
		paramBean.setDetailsView(beanView);
	}

	/**
	 *
	 * @param userUrls
	 * @return
	 */
	private List<String> getUserUrl(List<IUserUrlEntity> userUrls) {
		List<String> result = new ArrayList<String>();
		for (IUserUrlEntity userUrl : userUrls) {
			result.add(userUrl.getUrl());
		}
		return result;
	}

	private void getLatestActivity(FlowProfileDetailsServiceBean paramBean) {
		ProfileDetailsLatestActivityBean LatestActBean = paramBean.getLatestActivityView();
		LatestActivityRequestOption latestActReqOption = LatestActBean.getRequestOption();

		int linesPerPage = LatestActBean.getLinesPerPage();
		List<Day> days = LatestActBean.getDays();

		Calendar fromDateCal = Calendar.getInstance( paramBean.getTimeZone() );

		if( LatestActivityRequestOption.none.equals( latestActReqOption ) ){
			fromDateCal.setTime(new Date( TriDateUtils.getSystemTimestamp().getTime() ));
			days = new ArrayList<Day>();

		}else if( LatestActivityRequestOption.more.equals( latestActReqOption ) ){
			if( days == null || days.size() == 0 )
				return;

			Day lastDay = days.get( days.size() -1 );
			fromDateCal = this.getCalendar( lastDay.getDate(), paramBean.getTimeZone());
			fromDateCal.add(Calendar.DATE, -1);
		}

		days.addAll(this.getDaysList(paramBean, LatestActBean, fromDateCal, linesPerPage));
		LatestActBean.setDays( days );

		paramBean.getLatestActivityView().setHasNext( getLatestActivityHasNext(paramBean, days));
	}

	private boolean getLatestActivityHasNext( FlowProfileDetailsServiceBean paramBean,
											  List<Day> days ){
		if( days == null || days.size() == 0 )
			return false;

		Day lastDay = days.get( days.size() -1 );
		Calendar fromDateCal = this.getCalendar( lastDay.getDate(), paramBean.getTimeZone());
		fromDateCal.add(Calendar.DATE, -1);

		return this.getLatestActivityHasNext(paramBean, fromDateCal);
	}

	private boolean getLatestActivityHasNext( FlowProfileDetailsServiceBean paramBean,
											  Calendar fromDateCal ){

		Calendar toDateCal = Calendar.getInstance( paramBean.getTimeZone() );
		toDateCal.setTime(new Date( TriDateUtils.getSystemTimestamp().getTime() ));
		toDateCal.add( Calendar.YEAR, -1 );

		String from = TriDateUtils.convertTimezone( getCalDateString(toDateCal), paramBean.getTimeZone(), TriDateUtils.getDefaultTimeZone());
		String to	= TriDateUtils.convertTimezone( getCalDateString(fromDateCal), paramBean.getTimeZone(), TriDateUtils.getDefaultTimeZone());

		return this.getHistEntityList( paramBean.getParam().getSelectedUserId(), from, to).size() > 0;
	}

	private List<Day> getDaysList( FlowProfileDetailsServiceBean paramBean,
								   ProfileDetailsLatestActivityBean LatestActBean,
								   Calendar fromDateCal,
								   int max){
		List<Day> days = new ArrayList<Day>();

		while( days.size() < max ){
			Day day = this.getDay( paramBean,
								   LatestActBean,
								   getCalDateString(fromDateCal));
			if(day != null)
				days.add(day);

			fromDateCal.add( Calendar.DATE, -1 );

			if( !this.getLatestActivityHasNext(paramBean, fromDateCal) )
				break;
		}
		return days;
	}

	private Day getDay( FlowProfileDetailsServiceBean paramBean,
						ProfileDetailsLatestActivityBean LatestActBean,
						String day){
		List<IHistEntity> entityList = this.getHistEntityList( paramBean.getParam().getSelectedUserId(),
															   TriDateUtils.convertTimezone( day, paramBean.getTimeZone(), TriDateUtils.getDefaultTimeZone()),
															   TriDateUtils.convertTimezone( day, paramBean.getTimeZone(), TriDateUtils.getDefaultTimeZone()));

		if( entityList == null || entityList.size() == 0 )
			return null;

		List<RecentUpdateViewBean> views =
				recentUpdateService.getRecentUpdateViewList( entityList,
															 paramBean.getLanguage(),
															 paramBean.getTimeZone());

		return LatestActBean.new Day().setDate			( day )
									  .setDateSubject	( this.getDateSubject(day, paramBean.getLanguage(), paramBean.getTimeZone()) )
									  .setRecentUpdateViews	( views )
									  ;
	}

	private List<IHistEntity> getHistEntityList( String userId, String from, String to){

		HistCondition condition = new HistCondition();

		String[] dataCtgCds = { AmTables.AM_LOT.name() , AmTables.AM_PJT.name() , AmTables.AM_AREQ.name() , BmTables.BM_BP.name() ,
								RmTables.RM_RA.name() , RmTables.RM_RP.name() , DmTables.DM_DO.name() , DcmTables.DCM_REP.name(),
								UmTables.UM_CTG.name() , UmTables.UM_MSTONE .name()};

		String[] actStsIds = { UmActStatusId.Add.getStatusId(),
							   UmActStatusId.Request.getStatusId(),
							   UmActStatusId.Edit.getStatusId(),
							   UmActStatusId.EditRequest.getStatusId(),
							   UmActStatusId.Remove.getStatusId(),
							   UmActStatusId.Reject.getStatusId(),
							   UmActStatusId.MultipleReject.getStatusId(),
							   UmActStatusId.Approve.getStatusId(),
							   UmActStatusId.MultipleApprove.getStatusId(),
							   UmActStatusId.Cancel.getStatusId(),
							   UmActStatusId.ReturnTo.getStatusId(),
							   UmActStatusId.MultipleReturnTo.getStatusId(),
							   UmActStatusId.Close.getStatusId(),
							   UmActStatusId.MultipleClose.getStatusId(),};

		condition.setDataCtgCds( dataCtgCds );
		condition.setActStsIds( actStsIds );
		condition.setRegUserId( userId );
		condition.setRegTimestampFromTo( from, to);

		ISqlSort sort = new SortBuilder();
		sort.setElement(HistItems.regTimestamp, TriSortOrder.Desc, 1);;

		return this.umSupport.getHistDao().find(condition.getCondition() , sort);
	}

	private String getDateSubject(String date, String language,TimeZone timezone){
		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();

		Calendar cal = this.getCalendar( date, timezone );

		String dateSubject = contextAdapter.getValue( MessageParameter.ProgressNotificationResultTime1.getKey(),
														new String[] {
															cal.get( Calendar.YEAR ) + "",
															(cal.get( Calendar.MARCH ) +1) + "",
															cal.get( Calendar.DATE ) + "" },
														language );
		return dateSubject;
	}

	private Calendar getCalendar(String date, TimeZone timezone){
		String year = date.substring(0,4);
		String month = date.substring(5,7);
		String day = date.substring(8,10);

		Calendar cal = Calendar.getInstance( timezone );
		cal.set(Calendar.YEAR, Integer.parseInt( year ) );
		cal.set(Calendar.MONTH, Integer.parseInt( month ) -1 );
		cal.set(Calendar.DATE, Integer.parseInt( day) );

		cal.get(Calendar.MONTH);

		return cal;
	}

	private String getCalDateString( Calendar cal ){
		String dateString =  cal.get( Calendar.YEAR ) + "/" +
							(cal.get( Calendar.MONTH ) + 1) + "/" +
							 cal.get( Calendar.DATE );

		return TriDateUtils.fillYMD( dateString );
	}

	private void getGanttChart( FlowProfileDetailsServiceBean paramBean ){
		ProfileDetailsGanttChartBean ganttChartView = paramBean.getGanttChartView();
		RequestType requestType = paramBean.getParam().getRequestType();
		GanttChartRequestOption requestOption = ganttChartView.getRequestOption();
		Span span = ganttChartView.getSearchCondition().getSpan();
		Grouping grouping = ganttChartView.getSearchCondition().getGrouping();

		Timestamp ganttStartDate = null;
		if(TriStringUtils.isEmpty( ganttChartView.getSearchCondition().getStartDate() ))
			ganttStartDate = TriDateUtils.getSystemTimestamp();
		else
			ganttStartDate = TriDateUtils.convertStringToTimestamp(
								TriDateUtils.convertDateFormat( ganttChartView.getSearchCondition().getStartDate(), null),
								paramBean.getTimeZone());

		Calendar startCal = Calendar.getInstance( paramBean.getTimeZone() );
		startCal.setTime(new Date(ganttStartDate.getTime()));

		if( GanttChartRequestOption.PreviousWeek.equals( requestOption ) && RequestType.onChange == requestType )
			startCal.add(Calendar.DATE, -7);

		else if( GanttChartRequestOption.NextWeek.equals( requestOption ) && RequestType.onChange == requestType )
			startCal.add(Calendar.DATE, +7);

		ganttStartDate = new Timestamp( startCal.getTime().getTime() );

		Calendar endCal = Calendar.getInstance( paramBean.getTimeZone() );
		endCal.setTime(new Date(ganttStartDate.getTime()));
		endCal.add(Calendar.MONTH , span.value());
		endCal.add(Calendar.DATE , 1);
		Timestamp ganttEndDate = new Timestamp(endCal.getTime().getTime());

		if( Grouping.none.equals(grouping) )
			this.getGanttViewBean(paramBean, ganttStartDate, ganttEndDate);

		if( Grouping.Lot.equals(grouping) )
			this.getGanttViewBeanOfGroupingLot(paramBean, ganttStartDate, ganttEndDate);

		ganttChartView.getSearchCondition().setStartDate( TriDateUtils.convertViewDateFormat(ganttStartDate,
															TriDateUtils.getYMDDateFormat(null, paramBean.getTimeZone())) );
	}

	private void getGanttViewBean( FlowProfileDetailsServiceBean paramBean,
								   Timestamp ganttStartDate,
								   Timestamp ganttEndDate){

		ProfileDetailsGanttChartBean ganttChartView = paramBean.getGanttChartView();
		String userId = paramBean.getParam().getSelectedUserId();
		Status searchStatus = ganttChartView.getSearchCondition().getStatus();
		Span span = ganttChartView.getSearchCondition().getSpan();

		List<IMstoneEntity> entityList = umSupport.findMstoneByLotIds( this.getLotIds(userId).toArray(new String[0]) );

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();

		GanttHeader header = ganttChartService.getGanttHeader( paramBean.getLanguage(),
															   paramBean.getTimeZone(),
															   ganttStartDate,
															   span.value(),
															   entityList);

		List<GanttContent> contents =
				ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																	paramBean.getLanguage(),
																	searchStatus,
																	null,
																	null,
																	null,
																	userId,
																	ganttStartDate,
																	ganttEndDate);
		GanttViewBean view = new GanttViewBean()
				.setGrouping	( Grouping.none )
				.setGroupingNm	( null )
				.setHeader		( header )
				.setContents	( contents )
				;
		groupingViews.add(view);
		ganttChartView.setGroupingViews( groupingViews );
	}

	private void getGanttViewBeanOfGroupingLot( FlowProfileDetailsServiceBean paramBean,
												Timestamp ganttStartDate,
												Timestamp ganttEndDate){

		ProfileDetailsGanttChartBean ganttChartView = paramBean.getGanttChartView();
		String userId = paramBean.getParam().getSelectedUserId();
		Status searchStatus = ganttChartView.getSearchCondition().getStatus();
		Span span = ganttChartView.getSearchCondition().getSpan();

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();
		List<String> lotIds = this.getLotIds( userId );

		for(String lotId : lotIds){
			ILotEntity lotEntity = amSupport.findLotEntity( lotId );
			List<IMstoneEntity> mstoneEntityList = umSupport.findMstoneByLotId( lotId );

			GanttHeader header = ganttChartService.getGanttHeader( paramBean.getLanguage(),
																   paramBean.getTimeZone(),
																   ganttStartDate,
																   span.value(),
																   mstoneEntityList);

			List<GanttContent> contents =
					ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																		paramBean.getLanguage(),
																		searchStatus,
																		lotId,
																		null,
																		null,
																		userId,
																		ganttStartDate,
																		ganttEndDate);
			if( contents.size() == 0 )
				continue;

			GanttViewBean view = new GanttViewBean()
					.setGrouping	( Grouping.Lot )
					.setGroupingNm	( lotEntity.getLotNm() + "(" + lotId + ")" )
					.setHeader		( header )
					.setContents	( contents )
					;

			groupingViews.add(view);
		}
		ganttChartView.setGroupingViews( groupingViews );
	}

	private List<String> getLotIds( String userId ){
		List<IGrpUserLnkEntity> grpUserLnkEntityList =
				this.umSupport.findGrpUserLnkByUserId(userId);

		List<String> grpIds = new ArrayList<String>();

		for(IGrpUserLnkEntity grpUserLnkEntity : grpUserLnkEntityList){
			if(grpIds.contains(grpUserLnkEntity.getGrpId()))
				continue;

			grpIds.add(grpUserLnkEntity.getGrpId());
		}

		if( grpIds.size() == 0 )
			grpIds.add("dummy");

		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setGrpIds(grpIds.toArray(new String[0]));

		List<ILotGrpLnkEntity> LotGrpLnkEntityList =
				this.amSupport.getLotGrpLnkDao().find( condition.getCondition() );

		List<String> lotIds = new ArrayList<String>();

		for(ILotGrpLnkEntity LotGrpLnkEntity : LotGrpLnkEntityList){
			if(lotIds.contains(LotGrpLnkEntity.getLotId()))
				continue;

			lotIds.add(LotGrpLnkEntity.getLotId());
		}
		return lotIds;
	}
}
