package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowUserBatchUpdateServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private FileView fileView = new FileView();
	private List<UploadUserView> userViews = new ArrayList<UploadUserView>();
	private RequestsCompletion result = new RequestsCompletion();
	private String maxUploadUser;

	public RequestParam getParam() {
		return param;
	}

	public List<UploadUserView> getUserViews() {
		return userViews;
	}
	public FlowUserBatchUpdateServiceBean setUserViews(List<UploadUserView> userViews) {
		this.userViews = userViews;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowUserBatchUpdateServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public FileView getFileView() {
		return fileView;
	}

	public FlowUserBatchUpdateServiceBean setFileView(FileView fileView) {
		this.fileView = fileView;
		return this;
	}

	public String getMaxUploadUser() {
		return maxUploadUser;
	}

	public FlowUserBatchUpdateServiceBean setMaxUploadUser(String maxUploadUser) {
		this.maxUploadUser = maxUploadUser;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private RequestOption requestOption = RequestOption.none;
		private UserMultipleEditInputBean inputInfo = new UserMultipleEditInputBean();


		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public UserMultipleEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(UserMultipleEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		downloadTemplate( "downloadTemplate" ),
		fileUpload( "fileUpload" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class UserMultipleEditInputBean {
		private String csvFileNm;
		private String csvFilePath;
		private byte[] csvInputStreamBytes;

		public String getCsvFileNm() {
			return csvFileNm;
		}
		public UserMultipleEditInputBean setCsvFileNm(String csvFileNm) {
			this.csvFileNm = csvFileNm;
			return this;
		}

		public String getCsvFilePath() {
			return csvFilePath;
		}
		public UserMultipleEditInputBean setCsvFilePath(String csvFilePath) {
			this.csvFilePath = csvFilePath;
			return this;
		}

		public byte[] getCsvInputStreamBytes() {
			return csvInputStreamBytes;
		}
		public UserMultipleEditInputBean setCsvInputStreamBytes(byte[] csvInputStreamBytes) {
			this.csvInputStreamBytes = csvInputStreamBytes;
			return this;
		}

	}

	public class FileView {
		private File templateFile;

		public File getTemplateFile() {
			return templateFile;
		}

		public FileView setTemplateFile(File templateFile) {
			this.templateFile = templateFile;
			return this;
		}
	}

	public class UploadUserView {

		private boolean selected = false;
		private int lineNo;
		private String userId;
		private String userNm;
		private String mailAddress;
		private String password;
		private String passwordExpiredPolicy;
		private String command;

		public boolean isSelected() {
			return selected;
		}
		public UploadUserView setSelected(boolean selected) {
			this.selected = selected;
			return this;
		}

		public int getLineNo() {
			return lineNo;
		}
		public UploadUserView setLineNo(int lineNo) {
			this.lineNo = lineNo;
			return this;
		}

		public String getUserId() {
			return userId;
		}
		public UploadUserView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public String getUserNm() {
			return userNm;
		}
		public UploadUserView setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}

		public String getMailAddress() {
			return mailAddress;
		}
		public UploadUserView setMailAddress(String mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}

		public String getPassword() {
			return password;
		}
		public UploadUserView setPassword(String password) {
			this.password = password;
			return this;
		}

		public String getPasswordExpiredPolicy() {
			return passwordExpiredPolicy;
		}
		public UploadUserView setPasswordExpiredPolicy(String passwordExpiredPolicy) {
			this.passwordExpiredPolicy = passwordExpiredPolicy;
			return this;
		}

		public String getCommand() {
			return command;
		}

		public UploadUserView setCommand(String command) {
			this.command = command;
			return this;
		}
	}


	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
