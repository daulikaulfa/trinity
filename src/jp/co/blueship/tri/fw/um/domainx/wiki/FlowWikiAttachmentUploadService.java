package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.File;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.BusinessFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByWikiAttachment;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiAttachedFileItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiAttachedFileEntity;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean.AppendFileBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentUploadServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 * @version V4.00.00
 * @author Sharov.Maksym
 */
public class FlowWikiAttachmentUploadService implements IDomain<FlowWikiAttachmentUploadServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public IServiceDto<FlowWikiAttachmentUploadServiceBean> execute(
			IServiceDto<FlowWikiAttachmentUploadServiceBean> serviceDto) {

		FlowWikiAttachmentUploadServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String wikiId = paramBean.getParam().getSelectedWikiId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowWikiAttachmentUploadServiceBean paramBean) throws IOException {
		String wikiId = paramBean.getParam().getSelectedWikiId();

		if (RequestOption.fileUpload.equals(paramBean.getParam().getRequestOption())) {
			if (TriStringUtils.isEmpty(paramBean.getParam().getAppendFile().getAppendFileInputStreamBytes())) {
				return;
			}
			String attachedFileSeqNo = this.getAttachedFileSeqNo(wikiId);
			paramBean.getParam().getAppendFile().setSeqNo(attachedFileSeqNo);

			int row = this.insertAttachment(paramBean);
			if (row > 0) {
				this.saveAttachment(paramBean);
			}
		}

		if (RequestOption.deleteUplodedFile.equals(paramBean.getParam().getRequestOption())) {
			int row = this.removeAttachment(paramBean);
			if (row > 0) {
				this.removeAttachmentFromFileSystem(paramBean);
			}
		}
	}

	private void removeAttachmentFromFileSystem(FlowWikiAttachmentUploadServiceBean paramBean) throws IOException {
		String basePath = sheet.getValue(UmDesignEntryKeyByWikiAttachment.appendFilePath);

		String wikiId = paramBean.getParam().getSelectedWikiId();
		String wikiIdPath = TriStringUtils.linkPathBySlash(basePath, wikiId);

		String appendFileSeqNo = paramBean.getParam().getAppendFile().getSeqNo();
		String appendFileSeqNoPath = TriStringUtils.linkPathBySlash(wikiIdPath, appendFileSeqNo);

		TriFileUtils.delete(new File(appendFileSeqNoPath));
	}

	private String getAttachedFileSeqNo(String wikiId) {
		ISqlSort sort = new SortBuilder();
		sort.setElement(WikiAttachedFileItems.attachedFileSeqNo, TriSortOrder.Desc, 1);
		WikiAttachedFileCondition wikiCondition = new WikiAttachedFileCondition();
		wikiCondition.setWikiId(wikiId);

		List<IWikiAttachedFileEntity> attachedFileList = this.umSupport.getWikiAttachedFileDao()
				.find(wikiCondition.getCondition(), sort);
		if (TriStringUtils.isEmpty(attachedFileList)) {
			return "0001";
		}
		int attachedFileSeqNo = Integer.parseInt(attachedFileList.get(0).getAttachedFileSeqNo());
		attachedFileSeqNo++;

		DecimalFormat format = new DecimalFormat("0000");
		return format.format(attachedFileSeqNo);
	}

	private int insertAttachment(FlowWikiAttachmentUploadServiceBean paramBean) {
		String wikiId = paramBean.getParam().getSelectedWikiId();
		String filePath = paramBean.getParam().getAppendFile().getAppendFileNm();
		String attachedFileSeqNo = paramBean.getParam().getAppendFile().getSeqNo();

		IWikiAttachedFileEntity entity = new WikiAttachedFileEntity();
		entity.setWikiId(wikiId);
		entity.setAttachedFileSeqNo(attachedFileSeqNo);
		entity.setFilePath(filePath);

		return this.umSupport.getWikiAttachedFileDao().insert(entity);
	}

	private void saveAttachment(FlowWikiAttachmentUploadServiceBean paramBean) {
		String basePath = sheet.getValue(UmDesignEntryKeyByWikiAttachment.appendFilePath);

		String wikiId = paramBean.getParam().getSelectedWikiId();
		String wikiIdPath = TriStringUtils.linkPathBySlash(basePath, wikiId);

		AppendFileBean appendFilebean = paramBean.getParam().getAppendFile();

		BusinessFileUtils.saveAppendFile(wikiIdPath, appendFilebean.getSeqNo(),
				StreamUtils.convertBytesToInputStreamToBytes(appendFilebean.getAppendFileInputStreamBytes()),
				appendFilebean.getAppendFileNm());

		appendFilebean.setAppendFileInputStreamBytes(null);
	}

	private int removeAttachment(FlowWikiAttachmentUploadServiceBean paramBean) {
		WikiAttachedFileCondition wikiCondition = new WikiAttachedFileCondition();
		wikiCondition.setWikiId(paramBean.getParam().getSelectedWikiId());
		wikiCondition.setFilePath(paramBean.getParam().getAppendFile().getAppendFileNm());
		wikiCondition.setAttachedFileSeqNo(paramBean.getParam().getAppendFile().getSeqNo());
		wikiCondition.setDelStsId(StatusFlg.off);

		IWikiAttachedFileEntity wikiEntity = new WikiAttachedFileEntity();
		wikiEntity.setDelStsId( StatusFlg.on );

		return this.umSupport.getWikiAttachedFileDao().update(wikiCondition.getCondition(), wikiEntity);
	}
}
