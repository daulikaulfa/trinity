package jp.co.blueship.tri.fw.um.domainx.project.beans.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class ProfileDetailsGanttChartBean {
	private List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();

	private SearchCondition searchCondition = new SearchCondition();
	private GanttChartRequestOption requestOption = GanttChartRequestOption.none;

	public List<GanttViewBean> getGroupingViews() {
		return groupingViews;
	}
	public ProfileDetailsGanttChartBean setGroupingViews(List<GanttViewBean> groupingViews) {
		this.groupingViews = groupingViews;
		return this;
	}

	public SearchCondition getSearchCondition() {
		return searchCondition;
	}
	public ProfileDetailsGanttChartBean setSearchCondition(SearchCondition searchCondition) {
		this.searchCondition = searchCondition;
		return this;
	}

	public GanttChartRequestOption getRequestOption() {
		return requestOption;
	}
	public ProfileDetailsGanttChartBean setRequestOption(GanttChartRequestOption requestOption) {
		this.requestOption = requestOption;
		return this;
	}

	public class SearchCondition implements ISearchFilter {
		@Expose private String startDate;
		@Expose private Span span = Span.ThreeMonth;
		@Expose private Grouping grouping = Grouping.none;
		@Expose private Status status = Status.All;

		private List<Span> spanViews = FluentList.from(new Span[]{Span.OneMonth, Span.TwoMonth, Span.ThreeMonth, Span.SixMonth}).asList();
		private List<Grouping> groupingViews = FluentList.from( new Grouping[]{Grouping.none,Grouping.Lot}).asList();
		private List<Status> statusViews = FluentList.from(Status.values()).asList();

		public String getStartDate() {
			return startDate;
		}
		public SearchCondition setStartDate(String startDate) {
			this.startDate = startDate;
			return this;
		}

		public Span getSpan() {
			return span;
		}
		public SearchCondition setSpan(Span span) {
			this.span = span;
			return this;
		}

		public Grouping getGrouping() {
			return grouping;
		}
		public SearchCondition setGrouping(Grouping grouping) {
			this.grouping = grouping;
			return this;
		}

		public Status getStatus() {
			return status;
		}
		public SearchCondition setStatus(Status status) {
			this.status = status;
			return this;
		}

		public List<Span> getSpanViews() {
			return spanViews;
		}

		public List<Grouping> getGroupingViews() {
			return groupingViews;
		}

		public List<Status> getStatusViews() {
			return statusViews;
		}
	}

	public enum GanttChartRequestOption {
		none			( "" ),
		Refresh			( "refresh" ),
		PreviousWeek	( "previous" ),
		NextWeek		( "next" ),
		;

		private String value = null;

		private GanttChartRequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			GanttChartRequestOption type = value( value );

			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static GanttChartRequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return none;
			}

			for ( GanttChartRequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}
			return none;
		}
	}

	public enum Span {
		OneMonth	( 1 ),
		TwoMonth	( 2 ),
		ThreeMonth	( 3 ),
		SixMonth	( 6 ),
		;

		private Integer value;

		private Span( int value) {
			this.value = value;
		}

		public boolean equals( int value ) {
			Span type = value( value );

			if ( ! this.equals(type) ) return false;

			return true;
		}

		public int value() {
			return this.value;
		}

		public static Span value( int value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return OneMonth;
			}

			for ( Span type : values() ) {
				if ( type.value() == value ) {
					return type;
				}
			}

			return OneMonth;
		}
	}
}
