package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.JoinType;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowRecentlyViewedWikiServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowRecentlyViewedWikiServiceBean.DashboardWikiViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowRecentlyViewedWikiService implements IDomain<FlowRecentlyViewedWikiServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;
	private IAmFinderSupport amSupport = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = this.amSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowRecentlyViewedWikiServiceBean> execute(
			IServiceDto<FlowRecentlyViewedWikiServiceBean> serviceDto) {

		FlowRecentlyViewedWikiServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(null != serviceBean, "ServiceBean is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				this.init(serviceBean);
			}

			if (RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				this.onChange(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, serviceBean.getFlowAction());
		}
	}

	private void init(FlowRecentlyViewedWikiServiceBean serviceBean) {
		this.onChange(serviceBean);
	}

	private void onChange(FlowRecentlyViewedWikiServiceBean serviceBean) {

		WikiCondition condition = new WikiCondition();
		condition.setJoinAccsHist(true, JoinType.RIGHT );

		if(TriStringUtils.isNotEmpty(serviceBean.getParam().getSearchCondition().getKeyword())) {
			condition.setKeyword(serviceBean.getParam().getSearchCondition().getKeyword());
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(AccsHistItems.accsTimestamp, TriSortOrder.Desc, 1);

		int linesPerPage = serviceBean.getParam().getLinesPerPage();
		int pageNo = serviceBean.getParam().getSearchCondition().getSelectedPageNo();

		IEntityLimit<IWikiEntity> limit = this.umSupport.getWikiDao().find(condition.getCondition(), sort, pageNo, linesPerPage);
		IPageNoInfo page = DcmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		serviceBean.setWikiView(this.getWikiViewFromEntity(limit.getEntities()));
		serviceBean.setPage(page);

	}

	private List<DashboardWikiViewBean> getWikiViewFromEntity(List<IWikiEntity> entities) {
		List<DashboardWikiViewBean> wikiViewList = new ArrayList<DashboardWikiViewBean>();
		if(TriStringUtils.isNotEmpty(entities)) {
			for(IWikiEntity entity : entities) {
				DashboardWikiViewBean wikiView = new FlowRecentlyViewedWikiServiceBean().new DashboardWikiViewBean();
				wikiView.setLotId(entity.getLotId());
				wikiView.setWikiId(entity.getWikiId());
				wikiView.setWikiNm(entity.getPageNm());

				wikiViewList.add(wikiView);
			}
		}
		return wikiViewList;
	}
}
