package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRoleRemovalService implements IDomain<FlowRoleRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowRoleRemovalServiceBean> execute(
			IServiceDto<FlowRoleRemovalServiceBean> serviceDto) {
		FlowRoleRemovalServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String roleId = paramBean.getParam().getSelectedRoleId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(roleId), "SelectedRoleId is not specified");
			paramBean.getResult().setCompleted(false);

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if( UmItemChkUtils.ADMIN_ROLE_ID.equals(roleId) ) {
					throw new ContinuableBusinessException(UmMessageId.UM001105E);
				}
				RoleCondition condition = new RoleCondition();
				condition.setRoleId(roleId);

				String roleName = "";
				if (0 < support.getRoleDao().count(condition.getCondition())) {
					IRoleEntity roleEntity = support.getRoleDao().findByPrimaryKey(condition.getCondition());
					roleName = roleEntity.getRoleName();

					this.removeRole(paramBean, roleId);
				}

				if(TriStringUtils.isNotEmpty( roleName )){
					paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003081I,roleName);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void removeRole(FlowRoleRemovalServiceBean paramBean, String roleId) {

		// Check ADMIN user
		checkRole(roleId, UmItemChkUtils.ADMIN, UmMessageId.UM001040E);

		// Check Login user
		checkRole(roleId, paramBean.getUserId(), UmMessageId.UM001039E );

		// Delete um_role_svc_ctg_lnk
		deleteRoleSvcCtgLnk(roleId);

		// Delete um_grp_role_lnk
		support.cleaningGrpRoleLnk(null, roleId);

		// Delete um_user_role_lnk
		support.cleaningUserRoleLnk(null, roleId);

		// Delete um_role
		deleteRole(roleId);

		paramBean.getResult().setCompleted(true);
	}

	private void deleteRoleSvcCtgLnk(String roleId) {
		RoleSvcCtgLnkCondition condition = new RoleSvcCtgLnkCondition();
		condition.setRoleId(roleId);

		support.getRoleSvcCtgLnkDao().delete(condition.getCondition());
	}

	private void deleteRole(String roleId) {
		RoleCondition condition = new RoleCondition();
		condition.setRoleId(roleId);

		support.getRoleDao().delete( condition.getCondition() );
	}

	private void checkRole(String roleId, String userId, IMessageId messageId) {

		List<IUserRoleLnkEntity> userRoleLnkEntityList = support.findUserRoleLnkByUserId( userId );

		for( IUserRoleLnkEntity userRoleEntity : userRoleLnkEntityList ) {
			if( roleId.equals( userRoleEntity.getRoleId() ) ) {
				throw new ContinuableBusinessException( messageId );
			}
		}
	}

}
