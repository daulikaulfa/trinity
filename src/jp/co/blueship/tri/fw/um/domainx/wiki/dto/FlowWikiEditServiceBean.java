package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;


/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowWikiEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	private String pageNm;
	private boolean isWikiHome = false;
	private List<String> tagViews = new ArrayList<String>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowWikiEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public String getPageNm() {
		return this.pageNm;
	}
	public FlowWikiEditServiceBean setPageNm(String pageNm) {
		this.pageNm = pageNm;
		return this;
	}

	public boolean isWikiHome() {
		return this.isWikiHome;
	}
	public FlowWikiEditServiceBean setIsWikiHome(boolean isWikiHome) {
		this.isWikiHome = isWikiHome;
		return this;
	}

	public List<String> getTagViews() {
		return this.tagViews;
	}
	public FlowWikiEditServiceBean setTagViews(List<String> tagViews) {
		this.tagViews = tagViews;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private String wikiId;
		private WikiCreationInputInfo inputInfo = new WikiCreationInputInfo();

		public String getSelectedWikiId() {
			return this.wikiId;
		}
		public RequestParam setSelectedWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public WikiCreationInputInfo getInputInfo() {
			return this.inputInfo;
		}
		public RequestParam setInputInfo(WikiCreationInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Input Information
	 */
	public class WikiCreationInputInfo {
		private String pageNm;
		private String content;

		public String getPageNm(){
			return this.pageNm;
		}
		public WikiCreationInputInfo setPageNm( String pageNm ){
			this.pageNm = pageNm;
			return this;
		}

		public String getContent(){
			return this.content;
		}
		public WikiCreationInputInfo setContent( String content ){
			this.content = content;
			return this;
		}
	}


	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String wikiId;
		private boolean completed = false;

		public String getWikiId() {
			return this.wikiId;
		}
		public RequestsCompletion setWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
