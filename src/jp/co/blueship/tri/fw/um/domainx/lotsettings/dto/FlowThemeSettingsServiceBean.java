package jp.co.blueship.tri.fw.um.domainx.lotsettings.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.ThemeColor;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowThemeSettingsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowThemeSettingsServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ThemeSettingsInputInfo inputInfo = new ThemeSettingsInputInfo();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public ThemeSettingsInputInfo getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ThemeSettingsInputInfo inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	public class ThemeSettingsInputInfo {
		private IconSelectionOption iconOption = IconSelectionOption.DefaultImage;
		/**
		 * for default
		 */
		private String iconPath;
		/**
		 * for custom
		 */
		private String iconFilePath;
		private List<ItemLabelsBean> iconViews = new ArrayList<ItemLabelsBean>();
		private String iconFileNm;
		private byte[] iconInputStreamBytes;
		private ThemeColor themeColor = ThemeColor.none;

		public IconSelectionOption getIconOption() {
			return iconOption;
		}
		public ThemeSettingsInputInfo setIconOption(IconSelectionOption iconOption) {
			this.iconOption = iconOption;
			return this;
		}

		public String getIconPath() {
			return iconPath;
		}
		public ThemeSettingsInputInfo setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}

		public String getIconFilePath() {
			return iconFilePath;
		}
		public ThemeSettingsInputInfo setIconFilePath(String iconFilePath) {
			this.iconFilePath = iconFilePath;
			return this;
		}

		public List<ItemLabelsBean> getIconViews() {
			return iconViews;
		}
		public ThemeSettingsInputInfo setIconViews(List<ItemLabelsBean> iconViews) {
			this.iconViews = iconViews;
			return this;
		}
		
		public String getIconFileNm() {
			return iconFileNm;
		}
		public ThemeSettingsInputInfo setIconFileNm(String iconFileNm) {
			this.iconFileNm = iconFileNm;
			return this;
		}

		public byte[] getIconInputStreamBytes() {
			return iconInputStreamBytes;
		}
		public ThemeSettingsInputInfo setIconInputStreamBytes(byte[] iconInputStreamBytes) {
			this.iconInputStreamBytes = iconInputStreamBytes;
			return this;
		}

		public ThemeColor getThemeColor() {
			return themeColor;
		}
		public ThemeSettingsInputInfo setThemeColor(ThemeColor themeColor) {
			this.themeColor = themeColor;
			return this;
		}

	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private String lotIconPath;
		private ThemeColor lotThemeColor;

		/**
		 * @return the themeColor
		 */
		public ThemeColor getLotThemeColor() {
			return lotThemeColor;
		}
		/**
		 * @param themeColor the themeColor to set
		 */
		public void setLotThemeColor(ThemeColor lotThemeColor) {
			this.lotThemeColor = lotThemeColor;
		}

		/**
		 * @return the IconPath
		 */
		public String getLotIconPath() {
			return lotIconPath;
		}
		/**
		 * @param lotIconPath the iconPath to set
		 */
		public void setLotIconPath(String lotIconPath) {
			this.lotIconPath = lotIconPath;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
