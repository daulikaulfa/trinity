package jp.co.blueship.tri.fw.um.domainx.admin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmProjectEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean.LanguageAndTimezoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean.ProjectSettingEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowProjectSettingsEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.02.00
 * @author Chung
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowProjectSettingsEditService implements IDomain<FlowProjectSettingsEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowProjectSettingsEditServiceBean> execute(
			IServiceDto<FlowProjectSettingsEditServiceBean> serviceDto) {
		FlowProjectSettingsEditServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {

				if (serviceBean.getParam().isRequestOptionOf(RequestOption.ProjectChanges)) {
					this.initProjectChanges(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.LanguageAndTimezoneChanges)) {
					this.initLanguageTimezoneChanges(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.NotificationChanges)) {
					this.initNotification(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.LookAndFeelChanges)) {
					this.initLookAndFeel(serviceBean);
				}
			}

			if (RequestType.onChange.equals(serviceBean.getParam().getRequestType())) {
				if (serviceBean.getParam().isRequestOptionOf(RequestOption.ProjectChanges)) {
					this.onChangeProjectChanges(serviceBean);
				}
			}

			if (RequestType.submitChanges.equals(serviceBean.getParam().getRequestType())) {

				if (serviceBean.getParam().isRequestOptionOf(RequestOption.ProjectChanges)) {
					this.submitProjectChanges(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.LanguageAndTimezoneChanges)) {
					this.submitLanguageTimezone(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.NotificationChanges)) {
					this.submitNotification(serviceBean);
				} else if (serviceBean.getParam().isRequestOptionOf(RequestOption.LookAndFeelChanges)) {
					this.submitLookAndFeel(serviceBean);
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, serviceBean.getFlowAction());
		}
	}

	private void initProjectChanges(FlowProjectSettingsEditServiceBean serviceBean) {

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);

		ProjectSettingEditInputBean inputInfo = serviceBean.getParam().getProjectInputInfo();
		inputInfo.setProjectNm	(projectEntity.getProjectNm());
		inputInfo.setIconViews( UmProjectEditServiceUtils.getProjectIconViews(serviceBean.getUserId()) );
		inputInfo.setIconOption	(IconSelectionOption.value(projectEntity.getIconTyp()));

		if (IconSelectionOption.CustomImage.equals(projectEntity.getIconTyp())) {
			inputInfo.setIconFilePath ( projectEntity.getCustomIconPath() );

		} else if (IconSelectionOption.DefaultImage.equals(projectEntity.getIconTyp())) {
			inputInfo.setIconPath	( projectEntity.getDefaultIconPath());
		}
	}

	private void onChangeProjectChanges (FlowProjectSettingsEditServiceBean serviceBean) {
		ProjectSettingEditInputBean projectInfo = serviceBean.getParam().getProjectInputInfo();

		if ( (null == projectInfo.getIconInputStreamBytes())
				|| projectInfo.getIconInputStreamBytes().length == 0) {
			throw new TriSystemException(UmMessageId.UM001071E);
		}

		BufferedInputStream inBuffer	= null;
		BufferedOutputStream outBuffer	= null;
		String filePath = null;
		try {

			filePath = UmDesignBusinessRuleUtils.getProjectCustomIconLocalPath(projectInfo.getIconFileNm());
			String absolutePath = filePath.substring(0, filePath.lastIndexOf("/"));

			InputStream iStream = StreamUtils.convertBytesToInputStreamToBytes( projectInfo.getIconInputStreamBytes());

			File projectIconDir = new File(absolutePath);
			if ( !projectIconDir.exists() ) {
				projectIconDir.mkdirs();
			}

			if ( null == iStream ) {
				return;
			}

			inBuffer = new BufferedInputStream(iStream);

			FileOutputStream fileOutStream = new FileOutputStream( new File(projectIconDir, projectInfo.getIconFileNm()));
			outBuffer = new BufferedOutputStream(fileOutStream);

			int contents = 0;
			while ( (contents = inBuffer.read()) != -1 ) {
				outBuffer.write(contents);
			}

		} catch ( IOException ioe ) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe );
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}

		if (new File(filePath).exists()) {

			String relativePath = UmDesignBusinessRuleUtils.getProjectIconSharePath(projectInfo.getIconFileNm(),IconSelectionOption.CustomImage);

			projectInfo.setIconFilePath(relativePath);
			projectInfo.setIconOption(IconSelectionOption.CustomImage);
		}
	}

	private void submitProjectChanges (FlowProjectSettingsEditServiceBean serviceBean) {
		ProjectSettingEditInputBean inputInfo = serviceBean.getParam().getProjectInputInfo();
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);

		if ( TriStringUtils.isEmpty( inputInfo.getProjectNm()) ) {
			throw new ContinuableBusinessException(UmMessageId.UM001081E);
		}

		projectEntity.setProjectNm(inputInfo.getProjectNm());
		serviceBean.getHeader().setProjectName(inputInfo.getProjectNm());
		projectEntity.setIconTyp(inputInfo.getIconOption().value());

		if (IconSelectionOption.CustomImage.equals(inputInfo.getIconOption().value())) {
			if ( TriStringUtils.isEmpty( inputInfo.getIconFilePath()) ) {
				throw new ContinuableBusinessException(UmMessageId.UM001068E);
			}
			String[] tokens = inputInfo.getIconFilePath().split("\\.(?=[^\\.]+$)");
			if(tokens.length != 2 || !( tokens[1].equalsIgnoreCase("jpg")
							|| tokens[1].equalsIgnoreCase("png")
							|| tokens[1].equalsIgnoreCase("gif") ) ) {
				throw new ContinuableBusinessException(UmMessageId.UM001072E);
			}
			projectEntity.setCustomIconPath	( inputInfo.getIconFilePath() );
			serviceBean.getHeader().setProjectIconPath(inputInfo.getIconFilePath());

		} else {
			projectEntity.setDefaultIconPath( inputInfo.getIconPath() );
			serviceBean.getHeader().setProjectIconPath(inputInfo.getIconPath());
		}

		this.support.getProjectDao().update(projectEntity);
		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003069I);
}

	private void initNotification(FlowProjectSettingsEditServiceBean serviceBean) {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);
		serviceBean.getParam().getNotificationInputInfo().setNotification(projectEntity.getNotice());
	}

	private void submitNotification(FlowProjectSettingsEditServiceBean serviceBean) {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);
		projectEntity.setNotice( serviceBean.getParam().getNotificationInputInfo().getNotification() );
		projectEntity.setNoticeTimestamp( TriDateUtils.getSystemTimestamp() );
		this.support.getProjectDao().update(projectEntity);

		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003059I);
	}

	private void initLanguageTimezoneChanges(FlowProjectSettingsEditServiceBean serviceBean) {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);
		//String language = projectEntity.getLanguage();

		LanguageAndTimezoneEditInputBean inputInfo = serviceBean.getParam().getLanguageTimezoneInputInfo();
		inputInfo.setTimezone(projectEntity.getTimezone())
				 .setLanguage(projectEntity.getLanguage())
				 .setLanguageViews(UmProjectEditServiceUtils.getLanguageViews())
				 .setTimezoneViews(UmProjectEditServiceUtils.getTimezoneViews());
	}

	private void submitLanguageTimezone(FlowProjectSettingsEditServiceBean serviceBean) {

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);
		projectEntity.setTimezone(serviceBean.getParam().getLanguageTimezoneInputInfo().getTimezone());
		projectEntity.setLanguage(serviceBean.getParam().getLanguageTimezoneInputInfo().getLanguage());

		this.support.getProjectDao().update(projectEntity);

		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003058I);
	}

	private void initLookAndFeel(FlowProjectSettingsEditServiceBean serviceBean) {
		serviceBean.getParam().getLookAndFeelEditInputInfo().setLookAndFeel(this.support.getAdminLookAndFeel())
															.setLookAndFeelViews(UmProjectEditServiceUtils.getLookAndFeelViews());
	}

	private void submitLookAndFeel(FlowProjectSettingsEditServiceBean serviceBean) {
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);

		projectEntity.setLookAndFeel(serviceBean.getParam().getLookAndFeelEditInputInfo().getLookAndFeel().dbValue());

		this.support.getProjectDao().update(projectEntity);

		serviceBean.getResult().setCompleted(true);
		serviceBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003084I);
	}
}
