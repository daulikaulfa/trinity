package jp.co.blueship.tri.fw.um.domainx.dashboard;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.MessageParameter;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.dao.constants.SmTables;
import jp.co.blueship.tri.fw.sm.dao.plmgt.eb.IPlMgtEntity;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.AccsHistCondition;
import jp.co.blueship.tri.fw.um.dao.accshist.eb.IAccsHistEntity;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.ViewNotificationToUserCondition;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserRoleLnkEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean.NotificationToUserView;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 *
 * @version V4.03.00
 * @author Anh.NguyenDuc
 */
public class FlowDashboardServiceNotificationToUser implements IDomain<FlowDashboardServiceBean>{

	private static final ILog log = TriLogFactory.getInstance();

	private IAmFinderSupport amSupport = null;
	private IRmFinderSupport rmSupport = null;
	private IUmFinderSupport umSupport = null;

	public void setAmSupport( IAmFinderSupport amSupport ){
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}
	public void setRmSupport( IRmFinderSupport rmSupport ){
		this.rmSupport = rmSupport;
	}

	@Override
	public IServiceDto<FlowDashboardServiceBean> execute(IServiceDto<FlowDashboardServiceBean> serviceDto) {

		FlowDashboardServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");
			FlowDashboardServiceNotificationToUserBean paramBean = serviceBean.getNotificationToUser();

			if(paramBean == null) return serviceDto;

			if( paramBean.getParam().getRequestOption().equals( RequestOption.none ))
				this.setNotificationToUserViews( paramBean, serviceBean );

			if( paramBean.getParam().getRequestOption().equals( RequestOption.refresh ))
				;

			if( paramBean.getParam().getRequestOption().equals( RequestOption.more ))
				this.more( paramBean, serviceBean );

			this.setHasNext(paramBean, serviceBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, serviceBean.getFlowAction());
		}
	}


	private void setNotificationToUserViews( FlowDashboardServiceNotificationToUserBean paramBean,
											 FlowDashboardServiceBean serviceBean){

		int linePerPage = paramBean.getParam().getLinesPerPage();

		List<SortNotificationToUserView> sortViews = new ArrayList<SortNotificationToUserView>();

		sortViews.addAll( this.getApprovedNotification( paramBean, serviceBean, null, "" ) );

		SortNotificationToUserView projectNoticeSortView		= this.getProjectNotification( paramBean, serviceBean, null );
		SortNotificationToUserView licenseLimitNoticeSortView	= this.getLicenseLimitNotification( paramBean, serviceBean, null );;
		SortNotificationToUserView passwordLimtNoticeSortView	= this.getUserPasswordNotification( paramBean, serviceBean, null );

		if( projectNoticeSortView != null )
			sortViews.add( projectNoticeSortView );

		if( licenseLimitNoticeSortView != null )
			sortViews.add( licenseLimitNoticeSortView );

		if( passwordLimtNoticeSortView != null )
			sortViews.add( passwordLimtNoticeSortView );

		paramBean.setNotificationViews( this.getSortResultNotificationToUserViews(serviceBean, sortViews, linePerPage ));
	}


	private void more( FlowDashboardServiceNotificationToUserBean paramBean,
					   FlowDashboardServiceBean serviceBean){

		int linePerPage = paramBean.getParam().getLinesPerPage();

		List<SortNotificationToUserView> sortViews = this.getMoreSortView(paramBean, serviceBean);
		List<NotificationToUserView> notificationViews = paramBean.getNotificationViews();

		for( NotificationToUserView view : notificationViews ){
			view.setIsUnread(false);
		}

		notificationViews.addAll( this.getSortResultNotificationToUserViews(serviceBean, sortViews, linePerPage) );

		paramBean.setNotificationViews( notificationViews );
	}


	private void setHasNext( FlowDashboardServiceNotificationToUserBean paramBean,
							 FlowDashboardServiceBean serviceBean){

		paramBean.setHasNext( this.getMoreSortView(paramBean, serviceBean).size() > 0);
	}


	private List<SortNotificationToUserView> getMoreSortView( FlowDashboardServiceNotificationToUserBean paramBean,
			 												  FlowDashboardServiceBean serviceBean){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();

		Timestamp to = null;
		List<NotificationToUserView> notificationViews = paramBean.getNotificationViews();

		boolean hasProjectNotice		= false;
		boolean hasLicenseLimitNotice	= false;
		boolean hasPasswordLimitNotice = false;

		String histId = "";

		for(int viewNo = 0 ; viewNo < notificationViews.size() ; viewNo++){
			NotificationToUserView view = notificationViews.get(notificationViews.size() -viewNo -1);

			histId = view.getHistId();

			if( view.isNewsFromAdmin() ){
				String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
				IProjectEntity entity = this.umSupport.findProjectByPrimaryKey(productId);

				String language = serviceBean.getLanguage();
				String subject = view.getSubject();

				if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003050I, entity.getUpdUserNm()) )) {
					hasProjectNotice = true;

				} else if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003053I) ) ) {
					hasLicenseLimitNotice = true;

				} else if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003051I) ) ) {
					hasPasswordLimitNotice = true;
				}

			}else{
				IHistEntity entity = umSupport.findHistByPrimaryKey(histId);
				to = entity.getRegTimestamp();
				break;
			}
		}

		List<SortNotificationToUserView> sortViews = new ArrayList<SortNotificationToUserView>();

		sortViews.addAll( this.getApprovedNotification( paramBean, serviceBean, to, histId ) );

		SortNotificationToUserView projectNoticeSortView		= this.getProjectNotification( paramBean, serviceBean, to );
		SortNotificationToUserView licenseLimitNoticeSortView	= this.getLicenseLimitNotification( paramBean, serviceBean, to );
		SortNotificationToUserView passwordLimtNoticeSortView	= this.getUserPasswordNotification( paramBean, serviceBean, to );

		if( projectNoticeSortView != null && !hasProjectNotice )
			sortViews.add( projectNoticeSortView );

		if( licenseLimitNoticeSortView != null && !hasLicenseLimitNotice)
			sortViews.add( licenseLimitNoticeSortView );

		if( passwordLimtNoticeSortView != null && !hasPasswordLimitNotice)
			sortViews.add( passwordLimtNoticeSortView );

		return sortViews;
	}


	private List<SortNotificationToUserView> getApprovedNotification( FlowDashboardServiceNotificationToUserBean paramBean,
																	  FlowDashboardServiceBean serviceBean,
																	  Timestamp to,
																	  String exceptionHistId){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormatOfExportFile( null ,serviceBean.getTimeZone());
		String language = serviceBean.getLanguage();

		ViewNotificationToUserCondition condition = new ViewNotificationToUserCondition();

		String[] stsIds = { AmAreqStatusId.CheckinRequested.getStatusId(),
							AmAreqStatusId.RemovalRequested.getStatusId(),
							RmRaStatusId.ReleaseRequested.getStatusId(),};

		Timestamp from = this.yearAdded(TriDateUtils.getSystemTimestamp(), -1);

		condition.setRegTimestampFromTo	( from, to );
		condition.setStsIds				( stsIds );
		condition.setDataCtgCd			( AmTables.AM_AREQ.name() );
		condition.setLotIds				( this.getLotIds( serviceBean.getUserId(),
														  ServiceId.AmChangeApprovalService.value(),
														  ServiceId.AmChangeApprovalServiceMultipleApproval.value()) );

		ISqlSort sort = new SortBuilder();
		sort.setElement( HistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IHistEntity> histEntityList = this.umSupport.getViewNotificationToUserDao().find(condition, sort );

		condition = new ViewNotificationToUserCondition();
		condition.setRegTimestampFromTo	( from, to );
		condition.setStsIds				( stsIds );
		condition.setDataCtgCd			( RmTables.RM_RA.name() );
		condition.setLotIds				( this.getLotIds( serviceBean.getUserId(),
														  ServiceId.RmReleaseRequestApprovalService.value()) );

		histEntityList.addAll( umSupport.getViewNotificationToUserDao().find(condition, sort ));

		List<SortNotificationToUserView> views = new ArrayList<SortNotificationToUserView>();

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for(IHistEntity histEntity : histEntityList){

			String date = TriDateUtils.convertViewDateFormat( histEntity.getRegTimestamp(), format);
			String dataId = histEntity.getDataId();
			String lotId = histEntity.getLotId();
			String lotNm = null;
			String requestUserNm = null;
			String subject = null;
			boolean isUnread = true;

			if( histEntity.getHistId().equals( exceptionHistId ) )
				continue;

			if( TriStringUtils.isEmpty( lotId ) )
				continue;

			lotNm = this.amSupport.findLotEntity( lotMap, lotId ).getLotNm();

			DataAttribute dataAttribute = DataAttribute.none;

			if( histEntity.getDataCtgCd().equals( AmTables.AM_AREQ.name() ) ){
				IAreqEntity areqEntity = this.amSupport.findAreqEntity(dataId, (StatusFlg)null);

				if( areqEntity.getDelStsId().parseBoolean() )
					continue;

				if( !(AmAreqStatusId.CheckinRequested.equals( areqEntity.getStsId() ) ||
						AmAreqStatusId.RemovalRequested.equals( areqEntity.getStsId() )) ){
					continue;
				}
				subject = areqEntity.getSummary();

				if( AreqCtgCd.ReturningRequest.equals( areqEntity.getAreqCtgCd() ) ) {
					requestUserNm = areqEntity.getRtnReqUserNm();
					dataAttribute = DataAttribute.CheckInRequest;
				}
				else if( AreqCtgCd.RemovalRequest.equals( areqEntity.getAreqCtgCd() ) ) {
					requestUserNm = areqEntity.getDelReqUserNm();
					dataAttribute = DataAttribute.RemovalRequest;
				}
				else {
					dataAttribute = DataAttribute.CheckOutRequest;
					continue;
				}

			}else if( histEntity.getDataCtgCd().equals( RmTables.RM_RA.name() ) ){
				RaCondition raCondition = new RaCondition();
				raCondition.setRaId		( dataId );
				raCondition.setDelStsId	((StatusFlg)null);
				raCondition.setJoinAccsHist( true );

				dataAttribute = DataAttribute.ReleaseRequest;

				IRaEntity raEntity = this.rmSupport.getRaDao().findByPrimaryKey( raCondition.getCondition() );

				if( raEntity.getDelStsId().parseBoolean() )
					continue;

				if( !RmRaStatusId.ReleaseRequested.equals( raEntity.getStsId() ) ){
					continue;
				}

				subject = raEntity.getBldEnvNm();
				requestUserNm = raEntity.getReqUser();
			}

			AccsHistCondition accsHistCondition = new AccsHistCondition();
			accsHistCondition.setDataId( dataId );
			accsHistCondition.setDataCtgCd( dataAttribute.getTableAttribute().name() );
			accsHistCondition.setUserId( serviceBean.getUserId() );

			IAccsHistEntity accsHistEntity =
					this.umSupport.getAccsHistDao().findByPrimaryKey(accsHistCondition.getCondition());

			isUnread = ( accsHistEntity == null )? true : false;

			if( !isUnread ){
				isUnread = ( accsHistEntity.getNoticeViewTimestamp() == null
						|| accsHistEntity.getNoticeViewTimestamp().before( histEntity.getRegTimestamp() ) )? true : false;
			}

			NotificationToUserView view = paramBean.new NotificationToUserView()
												.setHistId				( histEntity.getHistId() )
												.setNotificationDate	( contextAdapter.getValue( MessageParameter.ProgressNotificationResultTime1.getKey(),
																		  new String[] {
																			  date.substring(0, 4),
																			  date.substring(4, 6),
																			  date.substring(6, 8) }, language) )
												.setSubject				( contextAdapter.getMessage( language, false, UmMessageId.UM003049I,requestUserNm))
												.setMessage				( lotNm + "(" + lotId +") " + subject + "(" + dataId + ")" )
												.setNewsFromAdmin		( false )
												.setDataAttribute		( dataAttribute )
												.setDataId				( dataId )
												.setIsUnread			( isUnread )
												;

			views.add( new SortNotificationToUserView()
					.setNotificationDay		  ( histEntity.getRegTimestamp() )
					.setNotificationToUserView( view )
					);
		}

		return views;
	}


	private SortNotificationToUserView getProjectNotification( FlowDashboardServiceNotificationToUserBean paramBean,
															   FlowDashboardServiceBean serviceBean,
															   Timestamp to){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormatOfExportFile( null ,serviceBean.getTimeZone());
		String language = serviceBean.getLanguage();

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity entity = this.umSupport.findProjectByPrimaryKey(productId);

		if( entity == null )
			return null;

		if( TriStringUtils.isEmpty(entity.getNotice()))
			return null;

		Timestamp noticeTimestamp = entity.getNoticeTimestamp();

		if( noticeTimestamp == null )
			return null;

		if( to != null && noticeTimestamp.after(to) )
			return null;

		String dateString = TriDateUtils.convertViewDateFormat( noticeTimestamp, format);

		AccsHistCondition accsHistCondition = new AccsHistCondition();
		accsHistCondition.setUserId( serviceBean.getUserId() );
		accsHistCondition.setDataId( entity.getProductId() );
		accsHistCondition.setDataCtgCd( UmTables.UM_PROJECT.name() );

		IAccsHistEntity accsHistEntity = this.umSupport.getAccsHistDao().findByPrimaryKey( accsHistCondition.getCondition() );

		boolean isUnread = ( accsHistEntity == null )? true: false;

		if( !isUnread ){
			isUnread = ( accsHistEntity.getNoticeViewTimestamp() == null
					|| accsHistEntity.getNoticeViewTimestamp().before(noticeTimestamp))? true : false;
		}

		NotificationToUserView view = paramBean.new NotificationToUserView()
										.setHistId				( null )
										.setNotificationDate	( "" )
										.setSubject				( contextAdapter.getMessage( language, false, UmMessageId.UM003050I, entity.getUpdUserNm()))
										.setMessage				( entity.getNotice() )
										.setNewsFromAdmin		( true )
										.setDataAttribute		( DataAttribute.none )
										.setDataId				( entity.getProductId() )
										.setIsUnread			( isUnread )
										;

		SortNotificationToUserView sortView = new SortNotificationToUserView().setNotificationDay( entity.getNoticeTimestamp() )
																			  .setNotificationToUserView( view )
																			  ;
		return sortView;
	}


	private SortNotificationToUserView getLicenseLimitNotification( FlowDashboardServiceNotificationToUserBean paramBean,
																	FlowDashboardServiceBean serviceBean,
																	Timestamp to){

		String[] roleIds = this.getRoleIs( ServiceId.SmProductLicenseEditService.value() );

		UserRoleLnkCondition userRoleLnkCondition = new UserRoleLnkCondition();
		userRoleLnkCondition.setRoleIds( roleIds );
		userRoleLnkCondition.setUserId( serviceBean.getUserId() );

		List<IUserRoleLnkEntity> userList = this.umSupport.getUserRoleLnkDao().find(userRoleLnkCondition.getCondition());

		if(userList == null || userList.size() == 0)
			return null;

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormatOfExportFile( null ,serviceBean.getTimeZone());
		String language = serviceBean.getLanguage();

		IPlMgtEntity entity = amSupport.getSmFinderSupport().findPlMgtEntity(ProductActivate.RM_ID);

		if( entity == null )
			return null;

		Timestamp limit = entity.getPlTimeLimit();
		Timestamp noticeDay = this.daysAdded(limit, -14);

		if( limit == null )
			return null;

		if( to != null && noticeDay.after(to) )
			return null;

		NotificationToUserView view = paramBean.new NotificationToUserView();
		int diffDay = this.getDiffDay( TriDateUtils.getSystemTimestamp(), limit );

		if( diffDay > 13){
			return null;

		}else if( diffDay < 14){
			String noticeDateString = TriDateUtils.convertViewDateFormat( noticeDay, format);
			String limitDateString = TriDateUtils.convertViewDateFormat( limit, format);

			AccsHistCondition accsHistCondition = new AccsHistCondition();
			accsHistCondition.setUserId( serviceBean.getUserId() );
			accsHistCondition.setDataId( entity.getCpNm() );
			accsHistCondition.setDataCtgCd( SmTables.SM_PL_MGT.name() );

			IAccsHistEntity accsHistEntity = this.umSupport.getAccsHistDao().findByPrimaryKey( accsHistCondition.getCondition() );

			boolean isUnread = ( accsHistEntity == null )? true : false ;

			if( !isUnread ){
				isUnread = ( accsHistEntity.getNoticeViewTimestamp() == null
						||  accsHistEntity.getNoticeViewTimestamp().before(noticeDay) )? true : false;
			}

			view.setHistId			( null )
				.setNotificationDate( contextAdapter.getValue( MessageParameter.ProgressNotificationResultTime1.getKey(),
																 new String[] {
																	noticeDateString.substring(0, 4),
																	noticeDateString.substring(4, 6),
																	noticeDateString.substring(6, 8) }, language))
				.setSubject			( contextAdapter.getMessage( language, false, UmMessageId.UM003053I) )
				.setMessage			( contextAdapter.getMessage( language, false, UmMessageId.UM003054I,
																 limitDateString.substring(0, 4),
																 limitDateString.substring(4, 6),
																 limitDateString.substring(6, 8)) )
				.setNewsFromAdmin	( true )
				.setDataAttribute	( DataAttribute.none )
				.setDataId			( entity.getCpNm() )
				.setIsUnread		( isUnread )
			;

		}
		SortNotificationToUserView sortView = new SortNotificationToUserView().setNotificationDay		  ( this.daysAdded(limit, -14) )
																			  .setNotificationToUserView( view )
																			  ;
		return sortView;
	}


	private SortNotificationToUserView getUserPasswordNotification( FlowDashboardServiceNotificationToUserBean paramBean,
																	FlowDashboardServiceBean serviceBean,
																	Timestamp to){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();
		SimpleDateFormat format = TriDateUtils.getYMDHMDateFormatOfExportFile( null ,serviceBean.getTimeZone());
		String language = serviceBean.getLanguage();

		UserCondition userCondition = new UserCondition();
		userCondition.setUserId( serviceBean.getUserId() );

		IUserEntity userEntity = this.umSupport.getUserDao().findByPrimaryKey( userCondition.getCondition() );

		if( userEntity == null )
			return null;

		Timestamp passwordLimit = userEntity.getPassTimeLimit();
		Timestamp noticeDay = this.daysAdded( passwordLimit, -14 );

		if(passwordLimit == null)
			return null;

		if( to != null && noticeDay.after(to) )
			return null;

		NotificationToUserView view = paramBean.new NotificationToUserView();
		int diffDay = this.getDiffDay( TriDateUtils.getSystemTimestamp(), passwordLimit );

		if( diffDay > 13){
			return null;

		}else if( diffDay < 14){
			String noticeDateString = TriDateUtils.convertViewDateFormat( noticeDay, format);
			String passwordLimitString = TriDateUtils.convertViewDateFormat( passwordLimit, format);

			AccsHistCondition accsHistCondition = new AccsHistCondition();
			accsHistCondition.setUserId( serviceBean.getUserId() );
			accsHistCondition.setDataId( userEntity.getUserId() );
			accsHistCondition.setDataCtgCd( UmTables.UM_USER.name() );

			IAccsHistEntity accsHistEntity = this.umSupport.getAccsHistDao().findByPrimaryKey( accsHistCondition.getCondition() );

			boolean isUnread = ( accsHistEntity == null )? true : false;

			if( !isUnread ){
				isUnread = ( accsHistEntity.getNoticeViewTimestamp() == null
						|| accsHistEntity.getNoticeViewTimestamp().before( noticeDay ) )? true : false;
			}

			view.setHistId			( null )
				.setNotificationDate( contextAdapter.getValue( MessageParameter.ProgressNotificationResultTime1.getKey(),
																 new String[] {
																	noticeDateString.substring(0, 4),
																	noticeDateString.substring(4, 6),
																	noticeDateString.substring(6, 8) }, language) )
				.setSubject			( contextAdapter.getMessage( language, false, UmMessageId.UM003051I) )
				.setMessage			( contextAdapter.getMessage( language, false, UmMessageId.UM003052I,
																 passwordLimitString.substring(0, 4),
																 passwordLimitString.substring(4, 6),
																 passwordLimitString.substring(6, 8)) )
				.setNewsFromAdmin	( true )
				.setDataAttribute	( DataAttribute.none )
				.setDataId			( userEntity.getUserId() )
				.setIsUnread		( isUnread )
				;
		}
		return new SortNotificationToUserView().setNotificationDay		 ( this.daysAdded(passwordLimit, -14) )
											   .setNotificationToUserView( view )
											   ;
	}


	private List<NotificationToUserView> getSortResultNotificationToUserViews( FlowDashboardServiceBean serviceBean,
																			   List<SortNotificationToUserView> sortViews,
																			   int linesPerPage){

		List<NotificationToUserView> views = new ArrayList<NotificationToUserView>();

		while( views.size() < linesPerPage && sortViews.size() > 0 ){
			SortNotificationToUserView latestView = sortViews.get(0);

			for( SortNotificationToUserView view : sortViews )
				if( view.getNotificationDay().after( latestView.getNotificationDay() ) )
					latestView = view;

			views.add( latestView.getNotificationToUserView() );

			sortViews.remove( latestView );

			this.readNotification(serviceBean, latestView.getNotificationToUserView() );
		}
		return views;
	}

	private void readNotification( FlowDashboardServiceBean serviceBean, NotificationToUserView view ){

		IContextAdapter contextAdapter = ContextAdapterFactory.getContextAdapter();

		String dataId = view.getDataId();
		ITableAttribute attr = null;

		if( !view.isNewsFromAdmin() ){
			attr = view.getDataAttribute().getTableAttribute();

		}else{
			String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
			IProjectEntity entity = this.umSupport.findProjectByPrimaryKey(productId);

			String language = serviceBean.getLanguage();
			String subject = view.getSubject();

			if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003050I, entity.getUpdUserNm()) )) {
				attr = UmTables.UM_PROJECT;

			} else if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003053I) ) ) {
				attr = SmTables.SM_PL_MGT;

			} else if( subject.equals( contextAdapter.getMessage( language, false, UmMessageId.UM003051I) ) ) {
				attr = UmTables.UM_USER;
			}
		}

		this.umSupport.updateAccsHist(serviceBean, attr, dataId, AccsHistItems.noticeViewTimestamp );
	}

	private String[] getLotIds(String userId, String... svcCtgIds ){

		List<String> roleIdList = Arrays.asList(this.getRoleIs(svcCtgIds));

		List<IGrpRoleLnkEntity> grpRoleLnkEntityList = this.umSupport.findGrpRoleLnkByRoleId(roleIdList.toArray(new String[0]));
		List<String> approvalGrpIds = new ArrayList<String>();
		approvalGrpIds.add( "dummy" );

		for(IGrpRoleLnkEntity grpRoleLnkEntity : grpRoleLnkEntityList)
			approvalGrpIds.add( grpRoleLnkEntity.getGrpId() );

		List<IGrpUserLnkEntity> grpUserLnkEntityList = this.umSupport.findGrpUserLnkByUserId(userId);
		List<String> grpIds = new ArrayList<String>();
		grpIds.add( "dummy" );

		for(IGrpUserLnkEntity grpUserLnkEntity : grpUserLnkEntityList){
			if( grpIds.contains(grpUserLnkEntity.getGrpId()) ||
					!approvalGrpIds.contains(grpUserLnkEntity.getGrpId()))
				continue;

			grpIds.add(grpUserLnkEntity.getGrpId());
		}

		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setGrpIds( grpIds.toArray(new String[0]));
		condition.setDelStsId((StatusFlg)null);
		condition.setLotUpdTimestampFromTo( TriDateUtils.convertTimezone( this.yearAdded(TriDateUtils.getSystemTimestamp(), -1), TriDateUtils.getDefaultTimeZone()), (String)null );

		List<ILotGrpLnkEntity> LotGrpLnkEntityList =
				this.amSupport.getLotGrpLnkDao().find( condition.getCondition() );

		List<String> lotIds = new ArrayList<String>();
		lotIds.add("dummy");

		for(ILotGrpLnkEntity LotGrpLnkEntity : LotGrpLnkEntityList){
			if(lotIds.contains(LotGrpLnkEntity.getLotId()))
				continue;

			lotIds.add(LotGrpLnkEntity.getLotId());
		}

		return lotIds.toArray(new String[0]);
	}


	private String[] getRoleIs(String... svcCtgIds){
		RoleSvcCtgLnkCondition roleSvcLnkCondition = new RoleSvcCtgLnkCondition();
		roleSvcLnkCondition.setSvcCtgIds( svcCtgIds );

		List<IRoleSvcCtgLnkEntity> roleSvcLnkEntityList = this.umSupport.getRoleSvcCtgLnkDao().find( roleSvcLnkCondition.getCondition() );

		List<String> roleIds = new ArrayList<String>();
		roleIds.add("dummy");

		for(IRoleSvcCtgLnkEntity roleSvcCtgLnkEntity : roleSvcLnkEntityList)
			roleIds.add( roleSvcCtgLnkEntity.getRoleId());

		return roleIds.toArray(new String[0]);
	}


	private int getDiffDay( Timestamp dateFrom, Timestamp dateTo ){
		long diff = ( dateTo.getTime() - dateFrom.getTime()) / (1000 * 60 * 60 * 24 );
		return (int)diff;
	}


	private Timestamp daysAdded( Timestamp day, int numberOfDays ){

		if( day == null ) return null;

		Calendar cal = Calendar.getInstance();
		cal.setTime( new Date(day.getTime()) );
		cal.add(Calendar.DATE, numberOfDays);

		return new Timestamp( cal.getTime().getTime() );
	}


	private Timestamp yearAdded( Timestamp day, int numberOfYears ){

		if( day == null ) return null;

		Calendar cal = Calendar.getInstance();
		cal.setTime( new Date(day.getTime()) );
		cal.add(Calendar.YEAR, numberOfYears);

		return new Timestamp( cal.getTime().getTime() );
	}


	private class SortNotificationToUserView{

		Timestamp notificationDay = null;
		NotificationToUserView notificationToUserView = null;

		public Timestamp getNotificationDay(){
			return this.notificationDay;
		}
		public SortNotificationToUserView setNotificationDay( Timestamp notificationDay ){
			this.notificationDay = notificationDay;
			return this;
		}

		public NotificationToUserView getNotificationToUserView(){
			return this.notificationToUserView;
		}
		public SortNotificationToUserView setNotificationToUserView( NotificationToUserView notificationToUserView ){
			this.notificationToUserView = notificationToUserView;
			return this;
		}
	}
}
