package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.grp.GrpNumberingDao;
import jp.co.blueship.tri.fw.um.dao.grp.constants.GrpItems;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupRoleLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupUserLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGroupCreationService implements IDomain<FlowGroupCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	private GrpNumberingDao grpNumberingDao;
	public void setGrpNumberingDao( GrpNumberingDao groupIdNumberingDao ) {
		this.grpNumberingDao = groupIdNumberingDao;
	}

	@Override
	public IServiceDto<FlowGroupCreationServiceBean> execute(IServiceDto<FlowGroupCreationServiceBean> serviceDto) {
		FlowGroupCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowGroupCreationServiceBean paramBean) {
		GroupEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		// Role Views
		inputInfo.setRoleViews(this.getRoleViews());

		// User Views
		inputInfo.setUserViews(this.getUserViews());
	}

	private void submitChanges(FlowGroupCreationServiceBean paramBean) {
		GroupEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		int count = this.countGrpByName(inputInfo.getGroupNm());

		UmItemChkUtils.checkInputGroup(inputInfo, count, support);

		String grpId = grpNumberingDao.nextval();
		insertGroup(grpId, inputInfo.getGroupNm());

		if (TriStringUtils.isNotEmpty(inputInfo.getRoleIdSet())) {
			support.registerGrpRoleLnk(new String[]{grpId}, inputInfo.getRoleIdSet().toArray(new String[0]));
		}

		if (TriStringUtils.isNotEmpty(inputInfo.getUserIdSet())) {
			support.registerGrpUserLnk(new String[]{grpId}, inputInfo.getUserIdSet().toArray(new String[0]));
			support.cleaningAndUpdateUserRoleLnk(inputInfo.getUserIdSet().toArray(new String[0]));
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getResult().setGroupId(grpId);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003076I,inputInfo.getGroupNm());

	}

	private List<GroupRoleLinkViewBean> getRoleViews() {
		List<GroupRoleLinkViewBean> roleViews = new ArrayList<GroupRoleLinkViewBean>();

		for (IRoleEntity roleEntity : this.support.findAllRole()) {
			GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
			condition.setRoleId(roleEntity.getRoleId());
			// Count the number of group belongs to this role
			int grpCount = this.support.getGrpRoleLnkDao().count(condition.getCondition());

			GroupRoleLinkViewBean roleView = new GroupRoleLinkViewBean()
					.setRoleId( roleEntity.getRoleId() )
					.setRoleNm( roleEntity.getRoleName() )
					.setCount(grpCount);

			roleViews.add(roleView);
		}

		return roleViews;
	}

	private List<GroupUserLinkViewBean> getUserViews() {
		List<GroupUserLinkViewBean> userViews = new ArrayList<GroupUserLinkViewBean>();
		for (IUserEntity userEntity : this.support.findAllUser()) {

			GroupUserLinkViewBean userView = new GroupUserLinkViewBean()
					.setUserId( userEntity.getUserId() )
					.setUserNm( userEntity.getUserNm() )
					.setMailAddress( userEntity.getEmailAddr() )
					.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
			userViews.add(userView);
		}

		return userViews;
	}

	private void insertGroup(String grpId, String grpNm) {
		int sortOrder = this.getSortOrder();

		IGrpEntity grpEntity = new GrpEntity();
		grpEntity.setGrpId(grpId);
		grpEntity.setGrpNm(grpNm);
		grpEntity.setSortOdr(sortOrder);

		support.getGrpDao().insert(grpEntity);
	}

	private int countGrpByName(String grpName) {
		if ( TriStringUtils.isEmpty(grpName) ) {
			return 0;
		}

		GrpCondition condition = new GrpCondition();
		condition.setGrpNm(grpName);

		return support.getGrpDao().count(condition.getCondition());
	}

	private int getSortOrder() {
		GrpCondition condition = new GrpCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(GrpItems.sortOdr, TriSortOrder.Desc, 1);

		List<IGrpEntity> grpEntities = support.getGrpDao().find(condition.getCondition(), sort, 1, 1).getEntities();

		if (0 < grpEntities.size()) {
			Integer sortOrder = grpEntities.get(0).getSortOdr();
			sortOrder++;
			return sortOrder;
		} else {
			return 1;
		}
	}

}
