package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupUserLinkViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGroupEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<GroupUserLinkViewBean> userViews = new ArrayList<GroupUserLinkViewBean>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowGroupEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public List<GroupUserLinkViewBean> getUserViews() {
		return userViews;
	}
	public FlowGroupEditServiceBean setUserViews(List<GroupUserLinkViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String groupId = null;
		private GroupEditInputBean inputInfo = new GroupEditInputBean();
		private GroupRequestOption requestOption = GroupRequestOption.Default;

		public String getSelectedGroupId() {
			return groupId;
		}
		public RequestParam setSelectedGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}

		public GroupEditInputBean getInputInfo() {
			return inputInfo;
		}

		public RequestParam setInputInfo(GroupEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public GroupRequestOption getRequestOption() {
			return requestOption;
		}

		public RequestParam setRequestOption(GroupRequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * This is the class of all GroupRequest enumeration types.
	 */
	public enum GroupRequestOption {
		Default( "default" ),
		ExcludeUser( "excludeUser" );

		private String value = null;

		private GroupRequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			GroupRequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static GroupRequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( GroupRequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return Default;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
