package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.constants.Charset;
import jp.co.blueship.tri.fw.cmn.io.diff.DiffResult;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer;
import jp.co.blueship.tri.fw.cmn.io.diff.EqualsContentsSetSameLineDiffer.Status;
import jp.co.blueship.tri.fw.cmn.io.diff.IDiffElement;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.AmDesignBeanId;
import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiHistItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.VersionComparisonView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.VersionComparisonView.VersionLineView;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionComparisonServiceBean.WikiVersionView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiVersionComparisonService implements IDomain<FlowWikiVersionComparisonServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public IServiceDto<FlowWikiVersionComparisonServiceBean> execute(
			IServiceDto<FlowWikiVersionComparisonServiceBean> serviceDto) {

		FlowWikiVersionComparisonServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String wikiId = paramBean.getParam().getSelectedWikiId();
			Integer destVerNo = paramBean.getParam().getDestVersion();
			Integer srcVerNo = paramBean.getParam().getSrcVersion();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");
			PreConditions.assertOf((null != destVerNo), "SelectedWikiVerNo is not specified");
			PreConditions.assertOf((null != srcVerNo), "SelectedWikiVerNo is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init(FlowWikiVersionComparisonServiceBean paramBean){
		this.getVersionViews(paramBean);
		this.getWikiPageNm(paramBean);

		this.onChange(paramBean);
	}

	private void onChange(FlowWikiVersionComparisonServiceBean paramBean){
		this.getVersionComparisonView(paramBean);
	}


	private void getWikiPageNm(FlowWikiVersionComparisonServiceBean paramBean){
		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		if( entity == null ) return ;

		paramBean.setWikiNm( entity.getPageNm() );
	}

	private void getVersionViews(FlowWikiVersionComparisonServiceBean paramBean){

		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiHistCondition histCondition = new WikiHistCondition();
		histCondition.setWikiId(wikiId);

		ISqlSort sort = new SortBuilder();
		sort.setElement( WikiHistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IWikiHistEntity> entityList = this.umSupport.getWikiHistDao().find( histCondition.getCondition(), sort );
		List<WikiVersionView> views = new ArrayList<WikiVersionView>();

		for( IWikiHistEntity entity : entityList )
			views.add( this.getWikiVersionView(paramBean, entity) );

		paramBean.setVersionViews(views);
	}


	private void getVersionComparisonView(FlowWikiVersionComparisonServiceBean paramBean){
		Integer destVerNo = paramBean.getParam().getDestVersion();
		Integer srcVerNo = paramBean.getParam().getSrcVersion();

		WikiVersionView destVersionView = this.getWikiVersionView(paramBean, destVerNo);
		WikiVersionView srcVersionView = this.getWikiVersionView(paramBean, srcVerNo);

		if(destVersionView == null || srcVersionView == null)
			return;

		VersionComparisonView view = paramBean.new VersionComparisonView()
				.setSrcVersionView(srcVersionView)
				.setDestVersionView(destVersionView)
				;
		this.getDiffLines(view);

		paramBean.setVersionComparisonView(view);
	}


	private WikiVersionView getWikiVersionView(FlowWikiVersionComparisonServiceBean paramBean, Integer wikiVerNo){
		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiHistCondition condition = new WikiHistCondition();
		condition.setWikiId(wikiId);
		condition.setWikiVerNo(wikiVerNo);

		IWikiHistEntity entity = this.umSupport.getWikiHistDao().findByPrimaryKey(condition.getCondition());

		if(entity == null) return null;

		return this.getWikiVersionView(paramBean, entity);
	}

	private WikiVersionView getWikiVersionView(FlowWikiVersionComparisonServiceBean paramBean, IWikiHistEntity entity){
		WikiVersionView view = paramBean.new WikiVersionView()
				.setPageNm			( entity.getPageNm() )
				.setWikiVerNo		( entity.getWikiVerNo() )
				.setContent			( entity.getContent() )
				.setUpdUserNm		( entity.getUpdUserNm() )
				.setUpdUserId		( entity.getUpdUserId() )
				.setUpdUserIconPath	( this.umSupport.getIconPath(entity.getUpdUserId()) )
				.setUpdDate			( TriDateUtils.convertViewDateFormat( entity.getRegTimestamp(),
											TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(),
																			 paramBean.getTimeZone())) )
				;
		return view;
	}


	private void getDiffLines(VersionComparisonView versionComparisonView){
		String srcContent = versionComparisonView.getSrcVersionView().getContent();
		String dstContent = versionComparisonView.getDestVersionView().getContent();

		EqualsContentsSetSameLineDiffer differ = new EqualsContentsSetSameLineDiffer(  srcContent, dstContent, this.getCharset(srcContent), this.getCharset(dstContent) ) ;

		differ.setException( sheet.getValueList( AmDesignBeanId.diffNeglectKeyword ) ) ;

		DiffResult diffResult = differ.diff() ;

		List<VersionLineView> lines = new ArrayList<VersionLineView>();

		for(IDiffElement resultElement : diffResult.getDiffElementList()){
			FileStatus fileStatus = null;
			Status status  = resultElement.getStatus();

			if		(Status.ADD.equals(status))		fileStatus = FileStatus.Add;
			else if (Status.DELETE.equals(status))	fileStatus = FileStatus.Delete;
			else if (Status.CHANGE.equals(status))	fileStatus = FileStatus.Edit;
			else if (Status.EQUAL.equals(status))	fileStatus = FileStatus.Same;

			VersionLineView lineView = versionComparisonView.new VersionLineView()
					.setStatus			(fileStatus)
					.setDestLineNo		(resultElement.getDstLineNumber()+"")
					.setSrcLineNo		(resultElement.getSrcLineNumber()+"")
					.setDest			(resultElement.getDstContents())
					.setSrc				(resultElement.getSrcContents())
					;

			lines.add(lineView);
		}
		versionComparisonView.setLines(lines);
	}


	private String getCharset(String contents){
		try {
			for( Charset charset : Charset.values() ){
				byte[] bytes = contents.getBytes(charset.toString());

				if( contents.equals(new String(bytes, charset.toString())))
					return charset.toString();
			}
			return "JISAutoDetect";

		} catch (UnsupportedEncodingException e) {
			throw new TriSystemException( UmMessageId.UM005050S, e );
		}
	}
}
