package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.dept.DeptNumberingDao;
import jp.co.blueship.tri.fw.um.dao.dept.constants.DeptItems;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentUserViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;


/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowDepartmentCreationService implements IDomain<FlowDepartmentCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	private DeptNumberingDao deptNumberingDao;
	public void setDeptNumberingDao( DeptNumberingDao deptNumberingDao ) {
		this.deptNumberingDao = deptNumberingDao;
	}

	@Override
	public IServiceDto<FlowDepartmentCreationServiceBean> execute(
			IServiceDto<FlowDepartmentCreationServiceBean> serviceDto) {
		FlowDepartmentCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.getParam().getInputInfo().setUserViews(this.getUserViews());
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void submitChanges(FlowDepartmentCreationServiceBean paramBean) {
		DepartmentEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		int count = this.countDeptByName(inputInfo.getDeptNm());

		UmItemChkUtils.checkInputDepartment(inputInfo, count, support);

		String deptId = deptNumberingDao.nextval();
		insertDepartment(inputInfo, deptId);

		if (TriStringUtils.isNotEmpty(inputInfo.getUserIdSet())) {
			updateUser(inputInfo.getUserIdSet(), deptId, inputInfo.getDeptNm());
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getResult().setDeptId(deptId);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003073I,inputInfo.getDeptNm());
	}

	private List<DepartmentUserViewBean> getUserViews() {
		List<DepartmentUserViewBean> userViews = new ArrayList<DepartmentUserViewBean>();

		for (IUserEntity entity: this.support.findAllUser()) {

			DepartmentUserViewBean userView = new DepartmentUserViewBean()
				.setUserId(entity.getUserId())
				.setUserNm(entity.getUserNm())
				.setMailAddress(entity.getEmailAddr())
				.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(entity))
				;
			userViews.add(userView);
		}

		return userViews;
	}

	private void insertDepartment(DepartmentEditInputBean inputBean, String deptId) {
		int sortOrder = this.getSortOrder();

		IDeptEntity deptEntity = new DeptEntity();
		deptEntity.setDeptId(deptId);
		deptEntity.setDeptNm(inputBean.getDeptNm());
		deptEntity.setSortOdr(sortOrder);

		support.getDeptDao().insert(deptEntity);
	}

	private void updateUser(Set<String> selectedUsers, String deptId, String deptNm) {
		for (String userId: selectedUsers) {
			UserCondition condition = new UserCondition();
			condition.setUserId(userId);

			IUserEntity userEntity = new UserEntity();
			userEntity.setUserId(userId);
			userEntity.setDeptId(deptId);
			userEntity.setDeptNm(deptNm);

			support.getUserDao().update(condition.getCondition(), userEntity);
		}
	}

	private int countDeptByName(String deptNm) {
		if ( TriStringUtils.isEmpty(deptNm) ) {
			return 0;
		}

		DeptCondition condition = new DeptCondition();
		condition.setDeptNm(deptNm);

		return support.getDeptDao().count(condition.getCondition());
	}

	private int getSortOrder() {
		DeptCondition condition = new DeptCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(DeptItems.sortOdr, TriSortOrder.Desc, 1);

		List<IDeptEntity> entities = support.getDeptDao().find(condition.getCondition(), sort, 1, 1).getEntities();

		if ( 0 < entities.size() ) {
			Integer sortOrder = entities.get(0).getSortOdr();
			sortOrder++;
			return sortOrder;
		} else {
			return 1;
		}
	}
}
