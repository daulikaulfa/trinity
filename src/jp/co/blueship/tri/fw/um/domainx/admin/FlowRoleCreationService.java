package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.BusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.domain.product.ProductActivate;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean.CheckedState;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmResourceSelectionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpRoleLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.RoleNumberingDao;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleSvcCtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.constants.SvcCtgItems;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.ISvcCtgEntity;
import jp.co.blueship.tri.fw.um.dao.svcctg.eb.SvcCtgCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleGroupLinkViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRoleCreationService implements IDomain<FlowRoleCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	private RoleNumberingDao roleNumberingDao;
	public void setRoleNumberingDao( RoleNumberingDao roleNumberingDao ) {
		this.roleNumberingDao = roleNumberingDao;
	}

	@Override
	public IServiceDto<FlowRoleCreationServiceBean> execute(
			IServiceDto<FlowRoleCreationServiceBean> serviceDto) {
		FlowRoleCreationServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowRoleCreationServiceBean paramBean) {
		RoleEditInputBean inputBean = paramBean.getParam().getInputInfo();

		inputBean.setGroupViews(this.getGroupViews());

		SvcCtgCondition condition = new SvcCtgCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement( SvcCtgItems.sortOdr, TriSortOrder.Asc , 1 );
		List<ISvcCtgEntity> svcCtgEntities = support.getSvcCtgDao().find(condition.getCondition(),sort);

		if (!ProductActivate.isDmActive()) {
			Iterator<ISvcCtgEntity> svcCtgEntityIterator = svcCtgEntities.iterator();
			while (svcCtgEntityIterator.hasNext()){
				ISvcCtgEntity svcCtgEntity = svcCtgEntityIterator.next();
				if (svcCtgEntity.getSvcCtgId().startsWith("FlowRelDistribute")
						|| svcCtgEntity.getSvcCtgId().startsWith("FlowDeployment")
						|| svcCtgEntity.getSvcCtgId().startsWith("FlowPrintDeployment")) {
					svcCtgEntityIterator.remove();
				}
			}
		}

		ITreeFolderViewBean tree = UmResourceSelectionUtils.getTreeActionView(svcCtgEntities);
		UmResourceSelectionUtils.convertToLocale( tree, paramBean.getLanguage() );

		paramBean.getActionSelectionView().setFolderView(tree);

	}

	private void submitChanges(FlowRoleCreationServiceBean paramBean) {
		RoleEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		this.setSelectedActions(inputInfo.getActionIdSet(), paramBean.getActionSelectionView().getFolderView());

		int count = this.countRoleByName(inputInfo.getRoleNm());

		UmItemChkUtils.checkInputRole(inputInfo, count, support);

		String roleId = roleNumberingDao.nextval();
		insertRole(roleId, inputInfo.getRoleNm());

		// insert role_svc_ctg
		if (TriStringUtils.isNotEmpty(inputInfo.getActionIdSet())) {
			for (String svcId : inputInfo.getActionIdSet()) {
				insertRoleSvcCtgLnk(roleId, svcId);
			}
		}

		if (TriStringUtils.isNotEmpty(inputInfo.getGroupIdSet())) {

			// insert um_grp_role_lnk
			support.registerGrpRoleLnk(inputInfo.getGroupIdSet().toArray(new String[0]), new String[]{roleId});

			// insert um_user_role_lnk
			List<IGrpUserLnkEntity> grpUserLnkEntityList = support.findGrpUserLnkByGrpId( inputInfo.getGroupIdSet().toArray(new String[0]) );
			List<String> userList = FluentList.from(grpUserLnkEntityList).map(UmFluentFunctionUtils.toUserIdFromGrpUserLnkEntity).asList();
			for (String userId : userList) {
				support.cleaningUserRoleLnk(userId, roleId);
				support.registerUserRoleLnk(new String[]{userId}, new String[]{roleId});
			}
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003079I,inputInfo.getRoleNm());

	}

	private void setSelectedActions(Set<String> actionIdSet, ITreeFolderViewBean tree) {

		if (TriStringUtils.isNotEmpty(actionIdSet)) {

			SvcCtgCondition condition = new SvcCtgCondition();
			condition.setSvcCtgIds(actionIdSet.toArray(new String[0]));
			List<ISvcCtgEntity> svcCtgEntities = support.getSvcCtgDao().find(condition.getCondition());

			for (ISvcCtgEntity entity : svcCtgEntities) {
				ITreeFolderViewBean serviceAction = tree.getFolder(entity.getSvcCtgPath());

				if (null != serviceAction) {
					serviceAction.setCheckedState(CheckedState.Checked);
				}
			}
		}
	}

	private void insertRole(String roleId, String roleNm) {
		int sortOrder = this.getSortOrder();

		IRoleEntity roleEntity = new RoleEntity();
		roleEntity.setRoleId(roleId);
		roleEntity.setRoleName(roleNm);
		roleEntity.setSortOdr(sortOrder);

		support.getRoleDao().insert(roleEntity);
	}

	private void insertRoleSvcCtgLnk(String roleId, String svcCtgId){
		IRoleSvcCtgLnkEntity roleSvcCtgEntity = new RoleSvcCtgLnkEntity();
		roleSvcCtgEntity.setRoleId( roleId );
		roleSvcCtgEntity.setSvcCtgId( svcCtgId );

		support.getRoleSvcCtgLnkDao().insert( roleSvcCtgEntity );
	}

	private List<RoleGroupLinkViewBean> getGroupViews() {
		List<RoleGroupLinkViewBean> groupViews = new ArrayList<RoleGroupLinkViewBean>();
		for (IGrpEntity grpEntity : this.support.findAllGroup()) {
			GrpRoleLnkCondition condition = new GrpRoleLnkCondition();
			condition.setGrpId(grpEntity.getGrpId());
			int count = this.support.getGrpRoleLnkDao().count(condition.getCondition());

			RoleGroupLinkViewBean grpView = new RoleGroupLinkViewBean()
				.setGrpId( grpEntity.getGrpId() )
				.setGrpNm( grpEntity.getGrpNm())
				.setCount( count );

			groupViews.add(grpView);
		}

		return groupViews;
	}

	private int getSortOrder() {
		RoleCondition condition = new RoleCondition();

		ISqlSort sort = new SortBuilder();
		sort.setElement(RoleItems.sortOdr, TriSortOrder.Desc, 1);

		List<IRoleEntity> roleEntities = support.getRoleDao().find(condition.getCondition(), sort, 1, 1).getEntities();

		if (0 < roleEntities.size()) {
			Integer sortOrder = roleEntities.get(0).getSortOdr();
			sortOrder++;
			return sortOrder;
		} else {
			return 1;
		}
	}

	private int countRoleByName(String roleNm) {
		if (TriStringUtils.isEmpty(roleNm)) {
			return 0;
		}

		RoleCondition condition = new RoleCondition();
		condition.setRoleName(roleNm);

		return support.getRoleDao().count(condition.getCondition());
	}
}
