package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiPageListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiTagListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.OrderBy;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiListServiceBean.WikiView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiListService implements IDomain<FlowWikiListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	private WikiPageListService wikiPageListService = null ;
	private WikiTagListService wikiTagListService = null ;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	public void setWikiPageListService( WikiPageListService wikiPageListService ){
		this.wikiPageListService = wikiPageListService;
	}
	public void setWikiTagListService( WikiTagListService wikiTagListService ){
		this.wikiTagListService = wikiTagListService;
	}

	@Override
	public IServiceDto<FlowWikiListServiceBean> execute(IServiceDto<FlowWikiListServiceBean> serviceDto) {

		FlowWikiListServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String lotId = paramBean.getParam().getSelectedLotId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiListServiceBean paramBean ){

		if( RequestOption.none.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);

		else
			this.onChange(paramBean);
	}


	private void onChange( FlowWikiListServiceBean paramBean ){

		RequestOption requestOption = paramBean.getParam().getRequestOption();
		String lotId = paramBean.getParam().getSelectedLotId();

		if( RequestOption.All.equals( requestOption ) )
			this.requestAll(paramBean);

		if( RequestOption.WikiPageList.equals( requestOption ) ){
			String searchPageNm = paramBean.getParam().getSearchPageNm();
			paramBean.setPageViews( this.wikiPageListService.getWikiPageList(lotId, searchPageNm) );
		}

		if( RequestOption.TagList.equals( requestOption ) )
			paramBean.setTagViews ( this.wikiTagListService.getWikiTagViewList(lotId) );

		if( RequestOption.WikiListService.equals( requestOption ) )
			this.getWikiViews(paramBean);

	}

	private void requestAll(FlowWikiListServiceBean paramBean){
		String lotId = paramBean.getParam().getSelectedLotId();

		this.getWikiViews(paramBean);
		List<WikiPageViewBean> wikiPageList = this.wikiPageListService.getWikiPageList(lotId, paramBean.getParam().getSearchPageNm());
		List<WikiTagViewBean> wikiTagList = this.wikiTagListService.getWikiTagViewList(lotId);

		paramBean.setPageViews( wikiPageList )
				 .setTagViews ( wikiTagList )
				 ;
	}


	private void getWikiViews( FlowWikiListServiceBean paramBean ){

		WikiCondition wikiCondition = this.getWikiCondition(paramBean);
		ISqlSort sort = this.getSortOrear( paramBean.getParam().getOrderBy() );

		List<IWikiEntity> wikiEntityList = this.umSupport.getWikiDao().find(wikiCondition.getCondition(), sort);

		List<WikiView> views = new ArrayList<WikiView>();

		for( IWikiEntity wikiEntity : wikiEntityList ){
			WikiView view = paramBean.new WikiView()
					.setWikiId	( wikiEntity.getWikiId() )
					.setWikiNm	( wikiEntity.getPageNm() )
					.setContent	( wikiEntity.getContent() )
					.setUpdUserNm(wikiEntity.getUpdUserNm() )
					.setUpdUserId(wikiEntity.getUpdUserId() )
					.setUpdUserIconPath(this.umSupport.getIconPath(wikiEntity.getUpdUserId()))
					.setUpdDate	( TriDateUtils.convertViewDateFormat( wikiEntity.getUpdTimestamp(),
										TriDateUtils.getYMDHMSDateFormat( paramBean.getLanguage(),
																		  paramBean.getTimeZone())))
					.setTags	( this.getTagList( wikiEntity.getWikiId() ) )
					;

			views.add( view );
		}
		paramBean.setWikiViews( views );
	}


	private WikiCondition getWikiCondition( FlowWikiListServiceBean paramBean ){
		String lotId = paramBean.getParam().getSelectedLotId();
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		WikiCondition wikiCondition = new WikiCondition();
		wikiCondition.setLotId( lotId );

		if( TriStringUtils.isNotEmpty( searchCondition.getKeyword() ) )
			wikiCondition.setKeyword( searchCondition.getKeyword() );

		if( TriStringUtils.isNotEmpty( searchCondition.getTagNm() ) )
			wikiCondition.setWikiIds( this.getWikiIds( searchCondition.getTagNm() ) );

		return wikiCondition;
	}


	private ISqlSort getSortOrear( OrderBy orderBy){
		ISqlSort sort = new SortBuilder();

		if( orderBy == null )
			return sort;

		if( orderBy.getWikiNm() ==  TriSortOrder.Desc ){
			sort.setElement( WikiItems.pageNm, orderBy.getWikiNm(), 1 );

		}else{
			sort.setElement( WikiItems.updTimestamp, orderBy.getUpdDate(), 1 );
		}

		return sort;
	}


	private List<String> getTagList(String wikiId){
		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setWikiId( wikiId );
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		List<String> tagList = new ArrayList<String>();

		for( IWikiTagEntity entity : tagEntityList )
			tagList.add(entity.getTagNm());

		return tagList;
	}


	private String[] getWikiIds(String tagNm){
		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setTagNm( tagNm );
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		List<String> wikiIdList = new ArrayList<String>();
		wikiIdList.add("dummy");

		for( IWikiTagEntity entity : tagEntityList )
			wikiIdList.add( entity.getWikiId() );

		return wikiIdList.toArray(new String[0]);
	}
}
