package jp.co.blueship.tri.fw.um.domainx.admin;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.utils.*;
import jp.co.blueship.tri.fw.cmn.utils.collections.TriCollectionUtils;
import jp.co.blueship.tri.fw.constants.*;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.sm.dao.login.eb.UserLoginCondition;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmUserEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.constants.UserUpdateCommandType;
import jp.co.blueship.tri.fw.um.domainx.admin.constants.UserUpdateCsvItem;
import jp.co.blueship.tri.fw.um.domainx.admin.constants.PasswordExpiration;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserBatchUpdateServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserBatchUpdateServiceBean.UploadUserView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserBatchUpdateServiceBean.UserMultipleEditInputBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import org.apache.commons.lang.math.NumberUtils;

/**
 * @author le.thixuan
 * @author Cuong Nguyen
 * @version V4.03.00
 */

//TODO Move some similar validation methods into UmItemChkUtils (GiAnG)
public class FlowUserBatchUpdateService implements IDomain<FlowUserBatchUpdateServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();
	private static String CSVSuffix = ".csv";
	private static String CSVSeparator = ",";
	private static String groupSeparator = "\\|";

	// Admin language and timezone

	IProjectEntity projectEntity;

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	private LoginSession loginSession;

	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	@Override
	public IServiceDto<FlowUserBatchUpdateServiceBean> execute(
			IServiceDto<FlowUserBatchUpdateServiceBean> serviceDto) {
		FlowUserBatchUpdateServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			//init project entity
			String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
			projectEntity = this.umSupport.findProjectByPrimaryKey(productId);

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setMaxUploadUser(sheet.getValue(UmDesignEntryKeyByUserUpload.maxLines));
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if (FlowUserBatchUpdateServiceBean.RequestOption.fileUpload.equals(paramBean.getParam().getRequestOption())) {
					this.validateCSVFile(paramBean);
				} else if (FlowUserBatchUpdateServiceBean.RequestOption.downloadTemplate.equals(paramBean.getParam().getRequestOption())) {
					this.createTemplateFile(paramBean);
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void createTemplateFile(FlowUserBatchUpdateServiceBean paramBean) {
		List<String> lines = Arrays.asList(UserUpdateCsvItem.getHeader(paramBean.getLanguage()));

		try {
			Path path = Paths.get("BatchUpdateUserTemplate.csv");
			Files.write(path, lines, Charset.forName(sheet.getValue(UmDesignEntryKeyByUserUpload.encoding)));
			paramBean.getFileView().setTemplateFile(path.toFile());
		} catch (Exception e) {
			ExceptionUtils.printStackTrace(e);
		}
	}

	private void validateCSVFile(FlowUserBatchUpdateServiceBean paramBean) {
		paramBean.getUserViews().clear();
		UserMultipleEditInputBean inputBean = paramBean.getParam().getInputInfo();

		if (TriStringUtils.isEmpty(inputBean.getCsvInputStreamBytes())) {
			return;
		}
		if (!inputBean.getCsvFileNm().endsWith(CSVSuffix)) {
			throw new ContinuableBusinessException(UmMessageId.UM001090E);
		}
		List<IMessageId> messageList = new ArrayList<>();
		List<String[]> messageArgsList = new ArrayList<>();
		List<String> lines = new ArrayList<>();
		try {
			lines = this.getCSVLines(inputBean.getCsvInputStreamBytes());
		} catch (IOException ioe) {
			ioe.printStackTrace();
		}

		Set<String> userIdSet = new HashSet<>();
		Set<String> userNameSet = new HashSet<>();
		List<UploadUserView> userCsvViews = new ArrayList<UploadUserView>();

		int lineNo = 1;

		for (String line: lines) {

			String[] columnValues = line.split(CSVSeparator, -1);
			if(columnValues.length != UserUpdateCsvItem.values().length) {
				throw new ContinuableBusinessException(UmMessageId.UM001102E);
			}

			String command = columnValues[UserUpdateCsvItem.command.getIndex()];
			if (TriStringUtils.isEmpty(command)) continue;
			else if (!UserUpdateCommandType.add.value().equalsIgnoreCase(command)
						&& !UserUpdateCommandType.delete.value().equalsIgnoreCase(command)
						&& !UserUpdateCommandType.edit.value().equalsIgnoreCase(command)) {
				messageList.add(UmMessageId.UM001092E);
				messageArgsList.add(new String[]{});
				continue;
			}

			if(UserUpdateCommandType.add.value().equalsIgnoreCase(command)) {
				insertDefaultValue(columnValues);
			}

			UploadUserView userView = paramBean.new UploadUserView()
					.setSelected(true)
					.setUserId(columnValues[UserUpdateCsvItem.userId.getIndex()])
					.setUserNm(columnValues[UserUpdateCsvItem.userName.getIndex()])
					.setLineNo(lineNo)
					.setPassword(columnValues[UserUpdateCsvItem.password.getIndex()])
					.setPasswordExpiredPolicy(this.getPasswordExpiration(columnValues[UserUpdateCsvItem.passwordExpiration.getIndex()]))
					.setMailAddress(columnValues[UserUpdateCsvItem.emailAddress.getIndex()])
					.setCommand(command);

			if (userIdSet.contains(userView.getUserId())) {
				messageList.add(UmMessageId.UM001094E);
				messageArgsList.add(new String[]{userView.getUserId()});
			}
			if(TriStringUtils.isNotEmpty(userView.getUserNm())) {
				if (userNameSet.contains(userView.getUserNm()) && !UserUpdateCommandType.delete.value().equalsIgnoreCase(command)) {
					messageList.add(UmMessageId.UM001096E);
					messageArgsList.add(new String[]{userView.getUserNm()});
				}
			}

			UserCondition condition = new UserCondition();
			condition.setUserId(userView.getUserId());
			IUserEntity userEntity = umSupport.getUserDao().findByPrimaryKey(condition.getCondition());
			if (UserUpdateCommandType.edit.value().equalsIgnoreCase(command)
					|| UserUpdateCommandType.delete.value().equalsIgnoreCase(command)) {
				if (userEntity == null) {
					messageList.add(UmMessageId.UM001095E);
					messageArgsList.add(new String[]{userView.getUserId()});
				}

				this.validateUpdateRemovalUser(userView.getUserId(), paramBean.getUserId(), messageList, messageArgsList);
			} else {
				if (userEntity != null) {
					messageList.add(UmMessageId.UM001024E);
					messageArgsList.add(new String[]{userView.getUserId()});
				}
			}

			if (UserUpdateCommandType.edit.value().equalsIgnoreCase(command) || UserUpdateCommandType.add.value().equalsIgnoreCase(command)) {
				this.checkInputUser(columnValues, messageList, messageArgsList,
						UserUpdateCommandType.add.value().equalsIgnoreCase(command), paramBean.getUserId());
			}

			userIdSet.add(userView.getUserId());

			if(!UserUpdateCommandType.delete.value().equalsIgnoreCase(command)) {
				userNameSet.add(userView.getUserNm());
			}

			userCsvViews.add(userView);
			lineNo++;
		}
		if (userCsvViews.size() > Integer.parseInt(sheet.getValue(UmDesignEntryKeyByUserUpload.maxLines))) {
			messageList.add(UmMessageId.UM001093E);
			messageArgsList.add(new String[]{sheet.getValue(UmDesignEntryKeyByUserUpload.maxLines)});
		}
		if (TriCollectionUtils.isNotEmpty(messageList)) {
			throw new ContinuableBusinessException(messageList, messageArgsList);
		} else {
			paramBean.setUserViews(userCsvViews);
		}
	}

	private void checkInputUser(String[] columnValues, List<IMessageId> messageList, List<String[]> messageArgsList, boolean isAdd, String currentLoginUserId) {
		String userId = columnValues[UserUpdateCsvItem.userId.getIndex()];
		String userNm = columnValues[UserUpdateCsvItem.userName.getIndex()];
		String language = columnValues[UserUpdateCsvItem.language.getIndex()];
		String timezone = columnValues[UserUpdateCsvItem.timezone.getIndex()];
		String password = columnValues[UserUpdateCsvItem.password.getIndex()];
		String passwordExpiration = columnValues[UserUpdateCsvItem.passwordExpiration.getIndex()];
		String emailAddress = columnValues[UserUpdateCsvItem.emailAddress.getIndex()];
		String deptId = columnValues[UserUpdateCsvItem.deptId.getIndex()];
		Set<String> groupIdSet = this.getGroupIds(columnValues);
		String icon = columnValues[UserUpdateCsvItem.icon.getIndex()];

		if (isAdd) {
			UmItemChkUtils.checkUserId(userId, messageList, messageArgsList);

		}
		int count = 0;
		UserCondition userCondition = new UserCondition();
		userCondition.setUserNm(userNm);
		IUserEntity userEntity = umSupport.getUserDao().findByPrimaryKey(userCondition.getCondition());
		if (userEntity != null && !userEntity.getUserId().equals(userId)) {
			count = 1;
		}

		UmItemChkUtils.checkUserName(userNm, count, messageList, messageArgsList, isAdd);

		UmItemChkUtils.checkUserEmailAddress(emailAddress, messageList, messageArgsList);

		this.checkPassword(userId, password, passwordExpiration, messageList, messageArgsList, isAdd);

		this.checkLanguageAndTimezone(language, timezone, messageList, messageArgsList, isAdd);

		this.checkUserIcon(icon, messageList, messageArgsList, isAdd);

		if (TriStringUtils.isNotEmpty(deptId)) {
			DeptCondition condition = new DeptCondition();
			condition.setDeptId(deptId);
			IDeptEntity deptEntity = this.umSupport.getDeptDao().findByPrimaryKey(condition.getCondition());
			if (null == deptEntity) {
				messageList.add(UmMessageId.UM001067E);
				messageArgsList.add(new String[]{deptId});
			}
		}
		if (TriCollectionUtils.isNotEmpty(groupIdSet)) {
			for (String grpId : groupIdSet) {
				GrpCondition condition = new GrpCondition();
				condition.setGrpId(grpId);
				IGrpEntity entity = this.umSupport.getGrpDao().findByPrimaryKey(condition.getCondition());
				if (null == entity) {
					messageList.add(UmMessageId.UM001066E);
					messageArgsList.add(new String[]{grpId});
				}
			}
		}
	}

	private void checkUserIcon( String userIcon, List<IMessageId> messageList, List<String[]> messageArgsList, boolean isAdd) {
		if(isAdd && TriStringUtils.isEmpty(userIcon)) {
			messageList.add(UmMessageId.UM001068E);
			messageArgsList.add(new String[] {});

		}
		if (TriStringUtils.isNotEmpty(userIcon) && TriStringUtils.isEmpty(this.getIconPath(userIcon)) ) {
			messageList.add(UmMessageId.UM001101E);
			messageArgsList.add(new String[] {userIcon});
		}
	}

	private void checkLanguageAndTimezone(String language, String timezone, List<IMessageId> messageList, List<String[]> messageArgsList, boolean isAdd) {

		if(isAdd) {
			if (TriStringUtils.isEmpty(timezone)) {
				messageList.add	( UmMessageId.UM001070E );
				messageArgsList.add	( new String[] {} );
			}
			if (TriStringUtils.isEmpty(language)) {
				messageList.add	( UmMessageId.UM001069E );
				messageArgsList.add	( new String[] {} );
			}
		}
		if( TriStringUtils.isNotEmpty(timezone) && !sheet.getKeyMap(UmDesignBeanId.timezones).containsKey(timezone) ) {
			messageList.add(UmMessageId.UM001100E);
			messageArgsList.add(new String[] {timezone});
		}

		if( TriStringUtils.isNotEmpty(language) && !sheet.getKeyMap(UmDesignBeanId.langs).containsKey(language) ) {
			messageList.add(UmMessageId.UM001099E);
			messageArgsList.add(new String[] {language});
		}
	}

	private void checkPassword(String userId, String password, String passwordExpiration, List<IMessageId> messageList, List<String[]> messageArgsList, boolean isAdd) {

		if (isAdd && TriStringUtils.isEmpty(passwordExpiration)) {
			messageList.add(UmMessageId.UM001097E);
			messageArgsList.add(new String[]{userId});
		}

		if(TriStringUtils.isNotEmpty(passwordExpiration) && !PasswordExpiration.contain(passwordExpiration)) {
			messageList.add(UmMessageId.UM001098E);
			messageArgsList.add(new String[] {sheet.getValue(UmDesignEntryKeyByPassword.passwordDefaultEffectiveTerm), userId});
		}

		String minPasswordLength = DesignSheetUtils.getPasswordMinimumLength();
		String maxPasswordLength = DesignSheetUtils.getPasswordMaxLength();
		int passMinLen = 0;
		int passMaxLen = 0;
		if ( NumberUtils.isNumber( minPasswordLength )) {
			passMinLen = Integer.parseInt( minPasswordLength );
		}
		if ( NumberUtils.isNumber( maxPasswordLength )) {
			passMaxLen = Integer.parseInt( maxPasswordLength );
		}
		if ( isAdd &&TriStringUtils.isEmpty( password ) ) {
				messageList.add		( UmMessageId.UM001007E );
				messageArgsList.add	( new String[] {} );
		}

		if(TriStringUtils.isNotEmpty(password)) {
			if ( !TriStringUtils.matches( password, DesignSheetUtils.getPasswordAllowPattern() ) ) {
				messageList.add		( UmMessageId.UM003010I );
				messageArgsList.add	( new String[] {} );
			}

			if ( 0 < passMinLen && password.length() < passMinLen ) {
				messageList.add		( UmMessageId.UM001036E );
				messageArgsList.add	( new String[] { String.valueOf( passMinLen ) } );
			}

			if ( password.length() > passMaxLen ) {
				messageList.add		( UmMessageId.UM001008E );
				messageArgsList.add	( new String[] { String.valueOf( passMaxLen ) } );
			}
		}
	}

	private void submitChanges(FlowUserBatchUpdateServiceBean paramBean) throws IOException {
		List<String> lines = this.getCSVLines(paramBean.getParam().getInputInfo().getCsvInputStreamBytes());
		List<IMessageId> messageList = new ArrayList<>();
		List<String[]> messageArgsList = new ArrayList<>();
		for(String line : lines) {
			String[] columnValues = line.split(CSVSeparator, -1);
			String command = columnValues[UserUpdateCsvItem.command.getIndex()];
			String userId = columnValues[UserUpdateCsvItem.userId.getIndex()];
			if (UserUpdateCommandType.edit.value().equalsIgnoreCase(command)
					|| UserUpdateCommandType.delete.value().equalsIgnoreCase(command)){
				this.validateUpdateRemovalUser(userId, paramBean.getUserId(), messageList, messageArgsList);
			}
		}

		if(TriCollectionUtils.isNotEmpty(messageList)) {
			throw new ContinuableBusinessException(messageList, messageArgsList);
		}

		for (String line : lines) {
			String[] columnValues = line.split(CSVSeparator, -1);
			String command = columnValues[UserUpdateCsvItem.command.getIndex()];
			String userId = columnValues[UserUpdateCsvItem.userId.getIndex()];
			if (UserUpdateCommandType.add.value().equalsIgnoreCase(command)) {
				insertDefaultValue(columnValues);
				IUserEntity userEntity = new UserEntity();
				mapToUser(userEntity, columnValues);
				umSupport.getUserDao().insert(userEntity);
				this.updateGroupInfo(this.getGroupIds(columnValues), userId);

			} else if (UserUpdateCommandType.edit.value().equalsIgnoreCase(command)) {
				IUserEntity userEntity = umSupport.findUserByUserId(userId);
				mapToUser(userEntity, columnValues);
				umSupport.getUserDao().update(userEntity);
				this.updateGroupInfo(this.getGroupIds(columnValues), userId);

			} else if (UserUpdateCommandType.delete.value().equalsIgnoreCase(command)) {
				this.removeUser(userId);
			}
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003085I);
	}

	private Set<String> getGroupIds(String[] columnValues) {
		String[] groupIds = columnValues[UserUpdateCsvItem.groupIds.getIndex()].split(groupSeparator);
		Set<String> groupIdSet = new HashSet<>();
		for (String groupId : groupIds) {
			if (TriStringUtils.isNotEmpty(groupId)) groupIdSet.add(groupId);
		}
		return groupIdSet;
	}

	private void updateGroupInfo(Set<String> groupIdSet, String userId) {
		if (TriStringUtils.isNotEmpty(groupIdSet)) {
			// Delete um_grp_user_lnk
			umSupport.cleaningGrpUserLnk(null, userId);

			// Insert um_grp_user_lnk
			umSupport.registerGrpUserLnk(groupIdSet.toArray(new String[0]), new String[]{userId});

			// Insert um_user_role_lnk
			umSupport.cleaningAndUpdateUserRoleLnk(new String[]{userId});
		}
	}

	private void mapToUser(IUserEntity userEntity, String[] columnValues) {
		userEntity.setUserId(columnValues[UserUpdateCsvItem.userId.getIndex()]);
		if(TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.userName.getIndex()])) {
			userEntity.setUserNm(columnValues[UserUpdateCsvItem.userName.getIndex()]);
		}

		if(TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.password.getIndex()])) {
			userEntity.setUserPass(
					EncryptionUtils.encodePasswordInA1Format(userEntity.getUserId(),
							columnValues[UserUpdateCsvItem.password.getIndex()]));
		}
		if(TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.passwordExpiration.getIndex()])) {
			userEntity.setPassTimeLimit(UmUserEditServiceUtils.getPasswordEffectiveDate(
					this.getPasswordExpiration(columnValues[UserUpdateCsvItem.passwordExpiration.getIndex()])));
		}
		if(TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.emailAddress.getIndex()])) {
			userEntity.setEmailAddr(
					columnValues[UserUpdateCsvItem.emailAddress.getIndex()]);
		}

		if (TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.deptId.getIndex()])) {
			IDeptEntity deptEntity = umSupport.findDeptByDeptId(
					columnValues[UserUpdateCsvItem.deptId.getIndex()]);
			if (null != deptEntity) {
				userEntity.setDeptId(deptEntity.getDeptId());
				userEntity.setDeptNm(deptEntity.getDeptNm());
			}
		}

		if (TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.language.getIndex()])) {
			userEntity.setLang(
					columnValues[UserUpdateCsvItem.language.getIndex()]);
		}

		if (TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.timezone.getIndex()])) {
			userEntity.setTimeZone(
					columnValues[UserUpdateCsvItem.timezone.getIndex()]);
		}

		if (TriStringUtils.isNotEmpty(columnValues[UserUpdateCsvItem.icon.getIndex()])) {
			userEntity.setIconTyp(IconSelectionOption.DefaultImage.value());
			userEntity.setDefaultIconPath(
					this.getIconPath(columnValues[UserUpdateCsvItem.icon.getIndex()]));
		}
	}

	private String getIconPath(String iconIndex) {
		String icon = "";
		Map<String, String> termMap = DesignSheetFactory.getDesignSheet().getKeyMap(UmDesignBeanId.userIcons);

		for (String key : termMap.keySet()) {
			if (key.contains(iconIndex)) {
				String images = sheet.getValue(UmDesignEntryKeyByWebResources.images);
				String mapping = sheet.getValue(UmDesignEntryKeyByWebResources.defaultParentMapping);
				String path = TriStringUtils.linkPath(mapping, images);

				TriStringUtils.convertPath(path, true);
				icon = TriStringUtils.linkPathBySlash(path, key);
			}
		}

		return icon;
	}

	private String getPasswordExpiration(String currentPasswordExpiration) {
		if (PasswordExpiration.certainDays.value().equals(currentPasswordExpiration)) {
			return sheet.getValue(UmDesignEntryKeyByPassword.passwordDefaultEffectiveTerm);
		} else {
			return currentPasswordExpiration;
		}
	}

	private List<String> getCSVLines(byte[] bytes) throws IOException {
		List<String> lines = new ArrayList<>();
		InputStreamReader stream = new InputStreamReader(
				StreamUtils.convertBytesToInputStreamToBytes(bytes), sheet.getValue(UmDesignEntryKeyByUserUpload.encoding));

		BufferedReader buffer = new BufferedReader(stream);
		String header = buffer.readLine();
		if (!UserUpdateCsvItem.getHeader(LanguageType.EN.getValue()).equals(header) &&
				!UserUpdateCsvItem.getHeader(LanguageType.JA.getValue()).equals(header)) {
			throw new TriSystemException(UmMessageId.UM001091E);
		}
		String line;

		while ((line = buffer.readLine()) != null) {
			if (TriStringUtils.isEmpty(line)) {
				continue;
			}
			byte[] lineByte = line.getBytes();
			line = new String(lineByte, sheet.getValue(UmDesignEntryKeyByUserUpload.encoding));
			lines.add(line);
		}

		return lines;
	}

	private void removeUser(String userId) {

		// Delete um_user
		deleteUser(userId);

		// Delete um_user_role_lnk
		umSupport.cleaningUserRoleLnk(userId, null);

		// Delete um_grp_use_lnk
		umSupport.cleaningGrpUserLnk(null, userId);

		// Delete sm_user_login
		deleteUserLogin(userId);

		// Delete um_user_url
		deleteUserUrl(userId);

	}

	private void validateUpdateRemovalUser(String userId, String loggedInUserId, List<IMessageId> messageList, List<String[]> messageArgsList) {

		// Can't update/delete admin user
		if ( UmItemChkUtils.ADMIN.equals(userId) ) {
			messageList.add(UmMessageId.UM001028E);
			messageArgsList.add(new String[]{});
		}

		// Can't update/delete logged in users
		if (loginSession.isLoggedInUser(userId) || loggedInUserId.equals(userId)) {
			messageList.add(UmMessageId.UM001029E);
			messageArgsList.add(new String[]{});
		}
	}

	private void deleteUser(String userId) {
		UserCondition condition = new UserCondition();
		condition.setUserId(userId);

		umSupport.getUserDao().delete(condition.getCondition());
	}

	private void deleteUserLogin(String userId) {
		UserLoginCondition userLoginCondition = new UserLoginCondition();
		userLoginCondition.setUserId(userId);

		fwSupport.getSmFinderSupport().getUserLoginDao().delete(userLoginCondition.getCondition());
	}

	private void deleteUserUrl(String userId) {
		UserUrlCondition userUrlCondition = new UserUrlCondition();
		userUrlCondition.setUserId(userId);

		umSupport.getUserUrlDao().delete(userUrlCondition.getCondition());

	}

	private void insertDefaultValue(String[] columnValues) {

		if(TriStringUtils.isEmpty(columnValues[UserUpdateCsvItem.language.getIndex()])){
			columnValues[UserUpdateCsvItem.language.getIndex()] = projectEntity.getLanguage();
		}

		if(TriStringUtils.isEmpty(columnValues[UserUpdateCsvItem.icon.getIndex()])){
			columnValues[UserUpdateCsvItem.icon.getIndex()] = "1";
		}

		if(TriStringUtils.isEmpty(columnValues[UserUpdateCsvItem.timezone.getIndex()])){
			columnValues[UserUpdateCsvItem.timezone.getIndex()] = projectEntity.getTimezone();
		}
	}
}
