package jp.co.blueship.tri.fw.um.domainx.admin;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptCondition;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowDepartmentRemovalService implements IDomain<FlowDepartmentRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowDepartmentRemovalServiceBean> execute(
			IServiceDto<FlowDepartmentRemovalServiceBean> serviceDto) {
		FlowDepartmentRemovalServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String deptId = paramBean.getParam().getSelectedDeptId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(deptId), "SelectedDeptId is not specified");
			paramBean.getResult().setCompleted(false);

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				DeptCondition condition = new DeptCondition();
				condition.setDeptId(deptId);
				if (0 < support.getDeptDao().count(condition.getCondition())) {
					this.removeDepartment(deptId, paramBean);
				}
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void validateRemovalDept(String deptId, String userId) {
		IUserEntity userBasicEntity = support.findUserByUserId( UmItemChkUtils.ADMIN );
		if( deptId.equals( String.valueOf( userBasicEntity.getDeptId() ) ) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001042E );
		}

		IUserEntity userLoginEntity = support.findUserByUserId( userId ) ;
		if( deptId.equals( String.valueOf( userLoginEntity.getDeptId() ) ) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001041E );
		}
	}
	private void removeDepartment(String deptId, FlowDepartmentRemovalServiceBean paramBean) {
		String userId = paramBean.getUserId();
		validateRemovalDept(deptId, userId);

		// Get grpEntity
		IDeptEntity deptEntity = support.findDeptByDeptId(deptId);

		DeptCondition condition = new DeptCondition();
		condition.setDeptId( deptId );

		support.getDeptDao().delete( condition.getCondition() );
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable( UmMessageId.UM003075I, deptEntity.getDeptNm() );
	}
}
