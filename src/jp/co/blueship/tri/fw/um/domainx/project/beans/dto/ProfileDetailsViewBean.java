package jp.co.blueship.tri.fw.um.domainx.project.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.um.constants.GenderType;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class ProfileDetailsViewBean {
	private String userId;
	private String userNm;
	private String iconPath;
	private String 	biography;
	private GenderType genderType = GenderType.none;
	private String userLocation;
	private List<String> urls = new ArrayList<String>();

	public String getUserId() {
		return userId;
	}
	public ProfileDetailsViewBean setUserId(String userId) {
		this.userId = userId;
		return this;
	}

	public String getUserNm() {
		return userNm;
	}
	public ProfileDetailsViewBean setUserNm(String userNm) {
		this.userNm = userNm;
		return this;
	}

	public String getIconPath() {
		return iconPath;
	}
	public ProfileDetailsViewBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}

	public String getBiography() {
		return biography;
	}
	public ProfileDetailsViewBean setBiography(String biography) {
		this.biography = biography;
		return this;
	}

	public GenderType getGenderType() {
		return genderType;
	}
	public ProfileDetailsViewBean setGenderType(GenderType genderType) {
		this.genderType = genderType;
		return this;
	}

	public String getUserLocation() {
		return userLocation;
	}
	public ProfileDetailsViewBean setUserLocation(String userLocation) {
		this.userLocation = userLocation;
		return this;
	}

	public List<String> getUrls() {
		return urls;
	}
	public void setUrls(List<String> urls) {
		this.urls = urls;
	}

}
