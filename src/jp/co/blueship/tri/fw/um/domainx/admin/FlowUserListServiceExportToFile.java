package jp.co.blueship.tri.fw.um.domainx.admin;

import jp.co.blueship.tri.dcm.DcmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByUserUpload;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.AmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.constants.PasswordExpiration;
import jp.co.blueship.tri.fw.um.domainx.admin.constants.UserUpdateCsvItem;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceExportToFileBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Cuong Nguyen
 * @version V4.03.00
 */
public class FlowUserListServiceExportToFile implements IDomain<FlowUserListServiceExportToFileBean> {

	private static final ILog log = TriLogFactory.getInstance();
	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowUserListServiceExportToFileBean> execute(
			IServiceDto<FlowUserListServiceExportToFileBean> serviceDto) {
		FlowUserListServiceExportToFileBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				this.submitChanges(paramBean);
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(AmMessageId.AM005158S, e, paramBean.getFlowAction());
		}

	}

	private void submitChanges(FlowUserListServiceExportToFileBean paramBean) throws IOException {
		List<IUserEntity> userEntities = support.getUserDao().find(new UserCondition().getCondition());
		Path directory = Paths.get(DcmDesignBusinessRuleUtils.getDownloadTempPath(paramBean.getUserId()).getAbsolutePath());
		File file = Files.createTempFile(directory, "ServiceId-", ".csv").toFile();
		List<String> fileLines = this.getUserListContent(paramBean, userEntities);

		Files.write(file.toPath(), fileLines, Charset.forName(sheet.getValue(UmDesignEntryKeyByUserUpload.encoding)));
		paramBean.getFileResponse().setFile(file);
	}

	private List<String> getUserListContent(FlowUserListServiceExportToFileBean paramBean, List<IUserEntity> userList) {
		String CSVSeparator = ",";
		Timestamp currentTimestamp = new Timestamp(System.currentTimeMillis());

		List<String> fileLines = new ArrayList<>();
		fileLines.add(UserUpdateCsvItem.getHeader(paramBean.getLanguage()));
		
		for (IUserEntity entity: userList) {
			StringBuilder row = new StringBuilder();
			row.append(""); //command
			row.append(CSVSeparator + entity.getUserId());
			row.append(CSVSeparator + entity.getUserNm());
			row.append(CSVSeparator + entity.getLang());
			row.append(CSVSeparator + entity.getTimeZone());
			row.append(CSVSeparator); //password
			row.append(CSVSeparator + this.getPasswordExpiration(entity.getPassTimeLimit(), currentTimestamp));
			row.append(CSVSeparator + entity.getEmailAddr());
			row.append(CSVSeparator + (entity.getDeptId() == null ? "" : entity.getDeptId()));
			row.append(CSVSeparator + this.getGroupIds(entity.getUserId()));
			row.append(CSVSeparator + this.getUserIcon(entity.getDefaultIconPath()));
			fileLines.add(row.toString());
		}

		return fileLines;
	}

	private String getUserIcon(String defaultIconPath) {
		if(TriStringUtils.isEmpty(defaultIconPath)) return "";
		return Integer.parseInt(defaultIconPath.replaceAll("[^0-9]", "")) + "";
	}

	private String getPasswordExpiration(Timestamp passTimeLimit, Timestamp current) {
		if(passTimeLimit == null) return PasswordExpiration.neverExpire.value();
		if(passTimeLimit.after(current)) return PasswordExpiration.certainDays.value();
		else return PasswordExpiration.changeAtNextLogin.value();
	}

	private String getGroupIds(String userId) {
		String groupIdStr = "";
		List<IGrpEntity> grpEntities = this.support.findGroupByUserId(userId);
		if (null != grpEntities) {
			List<String> groupIds =  FluentList
					.from(grpEntities)
					.map( UmFluentFunctionUtils.toGroupIdFromGrpEntity ).asList();
			for (String groupId: groupIds) {
				groupIdStr += groupId + "|";
			}
		}
		return TriStringUtils.isEmpty(groupIdStr) ? groupIdStr : groupIdStr.substring(0, groupIdStr.length() - 1);
	}
}
