package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.io.File;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserTemplateDownloadServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;
	private RequestParam param = new RequestParam();



	public RequestParam getParam() {
		return param;
	}



	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		File csvTemplate = null;

		public File getCsvTemplate() {
			return csvTemplate;
		}

		public RequestParam setCsvTemplate(File csvTemplate) {
			this.csvTemplate = csvTemplate;
			return this;
		}

	}

}
