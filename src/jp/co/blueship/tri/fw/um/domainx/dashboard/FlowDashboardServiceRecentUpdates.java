package jp.co.blueship.tri.fw.um.domainx.dashboard;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.dcm.dao.constants.DcmTables;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.accshist.constants.AccsHistItems;
import jp.co.blueship.tri.fw.um.dao.constants.UmTables;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.constants.HistItems;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistCondition;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowDashboardServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.RecentUpdateService;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;


/**
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowDashboardServiceRecentUpdates implements IDomain<FlowDashboardServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport;
	private IAmFinderSupport amSupport;

	private RecentUpdateService recentUpdateService;


	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}

	public void setRecentUpdateService(RecentUpdateService recentUpdateService){
		this.recentUpdateService = recentUpdateService;
	}

	@Override
	public IServiceDto<FlowDashboardServiceBean> execute(
			IServiceDto<FlowDashboardServiceBean> serviceDto) {

		FlowDashboardServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(serviceBean != null, "ServiceBean is not specified");
			FlowDashboardServiceRecentUpdatesBean paramBean = serviceBean.getRecentUpdates();

			if(paramBean == null) return serviceDto;

			if(RequestOption.none.equals( paramBean.getParam().getRequestOption()) )
				this.setRecentUpdateViews( serviceBean );

			else if(RequestOption.refresh.equals( paramBean.getParam().getRequestOption()) )
				this.refresh( serviceBean );

			else if(RequestOption.more.equals( paramBean.getParam().getRequestOption()) )
				this.more( serviceBean );

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, serviceBean.getFlowAction());
		}
	}


	private void setRecentUpdateViews(FlowDashboardServiceBean serviceBean){
		FlowDashboardServiceRecentUpdatesBean paramBean = serviceBean.getRecentUpdates();

		int linePerPage = paramBean.getParam().getLinesPerPage();
		List<IHistEntity> entityList = this.getHistEntityList(serviceBean.getUserId(), this.getLotIds(serviceBean.getUserId()), null, null, 1, linePerPage);

		List<RecentUpdateViewBean> views =
				recentUpdateService.getRecentUpdateViewList( entityList,
															 serviceBean.getLanguage(),
															 serviceBean.getTimeZone());

		for(RecentUpdateViewBean view : views){
			this.umSupport.updateAccsHist(serviceBean, UmTables.UM_HIST, view.getHistId(), AccsHistItems.noticeViewTimestamp);
		}

		paramBean.setRecentUpdateViews(views);
		this.setHasNext(paramBean , serviceBean.getUserId());
	}

	private void more(FlowDashboardServiceBean serviceBean){
		FlowDashboardServiceRecentUpdatesBean paramBean = serviceBean.getRecentUpdates();

		List<RecentUpdateViewBean> views = paramBean.getRecentUpdateViews();

		for( RecentUpdateViewBean view : views ){
			view.setIsUnread(false);
		}

		int linePerPage = paramBean.getParam().getLinesPerPage();

		if( views.size() < 1 ) return ;

		String lastHistId = views.get( views.size() -1 ).getHistId();
		IHistEntity lastHistEntity = this.umSupport.findHistByPrimaryKey(lastHistId);

		Calendar cal = Calendar.getInstance();
		cal.setTime( new Date(TriDateUtils.getSystemTimestamp().getTime()));
		cal.add( Calendar.YEAR, -1);

		Timestamp from = new Timestamp(cal.getTime().getTime());
		Timestamp to   = lastHistEntity.getRegTimestamp();

		List<IHistEntity> entityList = this.getHistEntityList(serviceBean.getUserId(), this.getLotIds(serviceBean.getUserId()), from, to, 1, linePerPage +1);

		for(IHistEntity entity : entityList){
			if(entity.getHistId().equals(lastHistEntity.getHistId()))
				continue;

			views.add( recentUpdateService.getRecentUpdateView( entity,
																serviceBean.getLanguage(),
																serviceBean.getTimeZone()));

			this.umSupport.updateAccsHist(serviceBean, UmTables.UM_HIST, entity.getHistId(), AccsHistItems.noticeViewTimestamp);
		}

		paramBean.setRecentUpdateViews(views);
		this.setHasNext(paramBean , serviceBean.getUserId());
	}


	private void refresh(FlowDashboardServiceBean serviceBean){

		FlowDashboardServiceRecentUpdatesBean paramBean = serviceBean.getRecentUpdates();

		List<RecentUpdateViewBean> views = new ArrayList<RecentUpdateViewBean>();

		for(RecentUpdateViewBean view : paramBean.getRecentUpdateViews()){
			IHistEntity entity = this.umSupport.findHistByPrimaryKey(view.getHistId());
			view = recentUpdateService.getRecentUpdateView( entity,
															serviceBean.getLanguage(),
															serviceBean.getTimeZone());
			views.add(view);
		}
		paramBean.setRecentUpdateViews(views);
	}


	private void setHasNext(FlowDashboardServiceRecentUpdatesBean paramBean , String userId){
		List<RecentUpdateViewBean> views = paramBean.getRecentUpdateViews();

		if(views.size() < 1){
			paramBean.setHasNext(false);
			return;
		}

		String lastHistId = views.get(views.size() -1).getHistId();
		IHistEntity entity = this.umSupport.findHistByPrimaryKey(lastHistId);

		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date(TriDateUtils.getSystemTimestamp().getTime()));
		cal.add(Calendar.YEAR, -1);

		Timestamp from = new Timestamp(cal.getTime().getTime());
		Timestamp to = entity.getRegTimestamp();

		List<IHistEntity> entityList = this.getHistEntityList( userId, this.getLotIds(userId), from, to, 1, 2);

		paramBean.setHasNext(entityList.size() > 1);
	}


	private List<IHistEntity> getHistEntityList( String userId, String[] lotIds, Timestamp from, Timestamp to, int pageNo, int viewRows){
		HistCondition condition = new HistCondition();

		String[] dataCtgCds = { AmTables.AM_LOT.name() , AmTables.AM_PJT.name() , AmTables.AM_AREQ.name() , BmTables.BM_BP.name() ,
								RmTables.RM_RA.name() , RmTables.RM_RP.name() , DmTables.DM_DO.name() , DcmTables.DCM_REP.name(),
								UmTables.UM_CTG.name() , UmTables.UM_MSTONE.name() , UmTables.UM_WIKI.name()};

		String[] actStsIds = { UmActStatusId.Add.getStatusId(),
							   UmActStatusId.Request.getStatusId(),
							   UmActStatusId.Edit.getStatusId(),
							   UmActStatusId.EditRequest.getStatusId(),
							   UmActStatusId.Remove.getStatusId(),
							   UmActStatusId.Reject.getStatusId(),
							   UmActStatusId.MultipleReject.getStatusId(),
							   UmActStatusId.Approve.getStatusId(),
							   UmActStatusId.MultipleApprove.getStatusId(),
							   UmActStatusId.Cancel.getStatusId(),
							   UmActStatusId.ReturnTo.getStatusId(),
							   UmActStatusId.MultipleReturnTo.getStatusId(),
							   UmActStatusId.Close.getStatusId(),
							   UmActStatusId.MultipleClose.getStatusId(),};

		condition.setDataCtgCds( dataCtgCds );
		condition.setActStsIds( actStsIds );
		condition.setRegTimestampFromTo( from, to);
		condition.setLotIds( lotIds );

		ISqlSort sort = new SortBuilder();
		sort.setElement(HistItems.regTimestamp, TriSortOrder.Desc, 1);

		return this.umSupport.getHistDao().find(condition.getCondition() , sort ,pageNo, viewRows).getEntities();
	}


	private String[] getLotIds(String userId){
		List<IGrpUserLnkEntity> grpUserLnkEntityList =
				this.umSupport.findGrpUserLnkByUserId(userId);

		List<String> grpIds = new ArrayList<String>();

		for(IGrpUserLnkEntity grpUserLnkEntity : grpUserLnkEntityList){
			if(grpIds.contains(grpUserLnkEntity.getGrpId()))
				continue;

			grpIds.add(grpUserLnkEntity.getGrpId());
		}

		if( grpIds.size() == 0 )
			grpIds.add("dummy");

		Calendar fromCal = Calendar.getInstance();
		fromCal.setTime(new Date(TriDateUtils.getSystemTimestamp().getTime()));
		fromCal.add(Calendar.YEAR, -1);

		LotGrpLnkCondition condition = new LotGrpLnkCondition();
		condition.setGrpIds(grpIds.toArray(new String[0]));
		condition.setDelStsId((StatusFlg)null);
		condition.setLotUpdTimestampFromTo( TriDateUtils.convertTimezone( fromCal.getTime(), TriDateUtils.getDefaultTimeZone()), (String)null );

		List<ILotGrpLnkEntity> LotGrpLnkEntityList =
				this.amSupport.getLotGrpLnkDao().find( condition.getCondition() );

		List<String> lotIds = new ArrayList<String>();

		for(ILotGrpLnkEntity LotGrpLnkEntity : LotGrpLnkEntityList){
			if(lotIds.contains(LotGrpLnkEntity.getLotId()))
				continue;

			lotIds.add(LotGrpLnkEntity.getLotId());
		}

		if( lotIds.size() == 0 )
			lotIds.add( "dummy" );

		return lotIds.toArray(new String[0]);
	}
}
