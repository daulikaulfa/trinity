package jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceBean;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceNotificationToUserBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private List<NotificationToUserView> notificationViews = new ArrayList<NotificationToUserView>();
	private boolean hasNext = false;

	public RequestParam getParam() {
		return param;
	}

	public List<NotificationToUserView> getNotificationViews() {
		return notificationViews;
	}
	public FlowDashboardServiceNotificationToUserBean setNotificationViews( List<NotificationToUserView> notificationViews ) {
		this.notificationViews = notificationViews;
		return this;
	}

	public boolean hasNext() {
		return hasNext;
	}
	public FlowDashboardServiceNotificationToUserBean setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam {
		private RequestOption requestOption = RequestOption.none;
		private int linesPerPage = 5;

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		refresh("refresh"),
		more("more"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class NotificationToUserView {
		private String histId;
		private DataAttribute dataAttribute = DataAttribute.none;
		private String dataId;
		private String notificationDate;
		private String subject;
		private String message;
		private boolean newsFromAdmin = false;
		private boolean isUnread = true;

		public String getHistId() {
			return histId;
		}
		public NotificationToUserView setHistId(String histId) {
			this.histId = histId;
			return this;
		}

		public DataAttribute getDataAttribute() {
			return dataAttribute;
		}
		public NotificationToUserView setDataAttribute(DataAttribute dataAttribute) {
			this.dataAttribute = dataAttribute;
			return this;
		}

		public String getDataId() {
			return dataId;
		}
		public NotificationToUserView setDataId(String dataId) {
			this.dataId = dataId;
			return this;
		}

		public String getNotificationDate() {
			return notificationDate;
		}
		public NotificationToUserView setNotificationDate(String notificationDate) {
			this.notificationDate = notificationDate;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public NotificationToUserView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getMessage() {
			return message;
		}
		public NotificationToUserView setMessage(String message) {
			this.message = message;
			return this;
		}

		public boolean isNewsFromAdmin() {
			return newsFromAdmin;
		}
		public NotificationToUserView setNewsFromAdmin(boolean newsFromAdmin) {
			this.newsFromAdmin = newsFromAdmin;
			return this;
		}

		public boolean isUnread() {
			return isUnread;
		}
		public NotificationToUserView setIsUnread(boolean isUnread) {
			this.isUnread = isUnread;
			return this;
		}
	}

}
