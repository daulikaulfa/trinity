package jp.co.blueship.tri.fw.um.domainx.personal;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByUserIcons;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmUserEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserUrlEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserPassHistEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlEntity;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.LanguageAndTimezoneDetailsView;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.LanguageAndTimezoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.PasswordEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.personal.dto.FlowPersonalSettingsEditServiceBean.UserEditInputBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.02.00
 * @author Chung
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowPersonalSettingsEditService implements IDomain<FlowPersonalSettingsEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowPersonalSettingsEditServiceBean> execute(
			IServiceDto<FlowPersonalSettingsEditServiceBean> serviceDto) {
		FlowPersonalSettingsEditServiceBean paramBean = serviceDto.getServiceBean();

		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String userId = paramBean.getUserId();

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				if (paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges)) {

					this.getUserByInit(paramBean, userId);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.LanguageAndTimezoneChanges)) {

					this.getLanguageTimzoneByInit(paramBean);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges)) {

					this.getPasswordExpirationByInit(paramBean);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.LookAndFeelChanges)) {

					this.lookAndFeelInit(paramBean);
				}
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if (paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges)) {
					UmUserEditServiceUtils.checkComplexity(paramBean.getParam().getPasswordInputInfo().getNewPassword(),
							paramBean.getDetailsView().getPassword());

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges)) {
					this.uploadCustomIcon(paramBean, userId);
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if (paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges)) {

					this.submitUserChanges(paramBean, userId);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges)) {

					this.submitPasswordChanges(paramBean, userId);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.LanguageAndTimezoneChanges)) {

					this.submitLanguageTimezoneChanges(paramBean, userId);

				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.LookAndFeelChanges)) {

					this.lookAndFeelChanges(paramBean, userId);

				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void getUserByInit(FlowPersonalSettingsEditServiceBean paramBean, String userId) {
		IUserEntity userEntity = support.findUserByUserId(userId);

		String defaultIconPath = (TriStringUtils.isEmpty(userEntity.getDefaultIconPath()))?
				UmDesignBusinessRuleUtils.getUserIconSharePath( userId, UmDesignEntryKeyByUserIcons.Default.getKey(), IconSelectionOption.DefaultImage ):
					userEntity.getDefaultIconPath();

		paramBean.getDetailsView().setUserId(userId).getUser()
				.setDefaultIconPath(defaultIconPath)
				.setCustomIconPath(userEntity.getCustomIconPath());

		paramBean.getParam().getUserInputInfo()
				.setUserNm(userEntity.getUserNm())
				.setMailAddress(userEntity.getEmailAddr())
				.setIconOption(IconSelectionOption.value(UmDesignBusinessRuleUtils.getUserIconType(userEntity)))
				.setIconViews( UmUserEditServiceUtils.getIconViews(userId) )
				.setIconPath(defaultIconPath)
				.setIconFilePath(userEntity.getCustomIconPath())
				.setLocation(userEntity.getUserLocation())
				.setBiography(userEntity.getBiography()).setUrl(this.getUsersUrl(userId));

		if (TriStringUtils.isNotEmpty(userEntity.getGenderTyp())) {
			paramBean.getParam().getUserInputInfo().setGenderType(UmDesignBusinessRuleUtils.getGender(userEntity));
		}

		if (paramBean.getParam().getUserInputInfo().getIconOption() == null) {
			paramBean.getParam().getUserInputInfo().setIconOption(IconSelectionOption.DefaultImage);
		}
		if (paramBean.getParam().getUserInputInfo().getGenderType() == null) {
			paramBean.getParam().getUserInputInfo().setGenderType(GenderType.Male);
		}
	}

	private String[] getUsersUrl(String userId) {
		UserUrlCondition condition = new UserUrlCondition();
		condition.setUserId(userId);

		List<IUserUrlEntity> userUrlEntitiess = support.getUserUrlDao().find(condition.getCondition());

		List<String> userUrls = new ArrayList<String>();
		for (IUserUrlEntity userUrlEntity : userUrlEntitiess) {
			userUrls.add(userUrlEntity.getUrl());
		}

		return userUrls.toArray(new String[0]);
	}

	private void getLanguageTimzoneByInit(FlowPersonalSettingsEditServiceBean paramBean) {
		IUserEntity userEntity = support.findUserByUserId(paramBean.getUserId());

		LanguageAndTimezoneDetailsView languageAndTimezoneDetailsView = paramBean.getDetailsView().getLanguageTimezone();

		paramBean.getParam().getLanguageTimezoneInputInfo().setLanguage(userEntity.getLang())
				.setTimezone(userEntity.getTimeZone()).setLanguageViews(UmUserEditServiceUtils.getLanguageViews())
				.setTimezoneViews(UmUserEditServiceUtils.getTimezoneViews());

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);


		languageAndTimezoneDetailsView.getDefaultTimeZone()
			.setLabel(UmUserEditServiceUtils.getTimezoneName(projectEntity.getTimezone()))
			.setValue("");

		languageAndTimezoneDetailsView.getDefaultLanguage()
			.setLabel(UmUserEditServiceUtils.getLanguageName(projectEntity.getLanguage()))
			.setValue("");
	}

	private void lookAndFeelInit(FlowPersonalSettingsEditServiceBean paramBean) {
		LookAndFeel adminLookAndFeel = this.support.getAdminLookAndFeel();

		ItemLabelsBean defaultLookAndFeel = new ItemLabelsBean(adminLookAndFeel.name(), adminLookAndFeel.value());

		IUserEntity userEntity = support.findUserByUserId(paramBean.getUserId());
		LookAndFeel lookAndFeel = LookAndFeel.dbValue(userEntity.getLookAndFeel());

		paramBean.getParam().getLookAndFeelInputInfo().setLookAndFeel(lookAndFeel)
													  .setLookAndFeelViews(UmUserEditServiceUtils.getLookAndFeelViews());

		paramBean.getDetailsView().getLookAndFeel().setDefaultLookAndFeel(defaultLookAndFeel);
	}

	private void getPasswordExpirationByInit(FlowPersonalSettingsEditServiceBean paramBean) {
		IUserEntity userEntity = support.findUserByUserId(paramBean.getUserId());
		SimpleDateFormat formatYMDHMS = TriDateUtils.getYMDHMSDateFormat(paramBean.getLanguage(),
				paramBean.getTimeZone());

		paramBean.getDetailsView().getPassword().setExpirationMsg(
				UmUserEditServiceUtils.getPasswordEffectiveLimit(userEntity.getPassTimeLimit(), formatYMDHMS));

		paramBean.getParam().getPasswordInputInfo()
				.setPasswordExpirationViews(UmUserEditServiceUtils.getPasswordExpirationTerm());

	}

	private void uploadCustomIcon(FlowPersonalSettingsEditServiceBean paramBean, String userId) {
		UserEditInputBean inputBean = paramBean.getParam().getUserInputInfo();

		if (null == inputBean.getIconInputStreamBytes()) {
			return;
		}

		BufferedInputStream inBuffer = null;
		BufferedOutputStream outBuffer = null;
		String filePath = null;
		try {

			filePath = UmDesignBusinessRuleUtils.getUserCustomIconLocalPath(userId, inputBean.getIconFileNm());
			String absolutePath = filePath.substring(0, filePath.lastIndexOf("/"));
			InputStream is = StreamUtils.convertBytesToInputStreamToBytes(inputBean.getIconInputStreamBytes());

			File userIconDir = new File(absolutePath);
			if (!userIconDir.exists()) {
				userIconDir.mkdirs();
			}

			if (null == is)
				return;

			inBuffer = new BufferedInputStream(is);

			FileOutputStream fos = new FileOutputStream(new File(userIconDir, inputBean.getIconFileNm()));
			outBuffer = new BufferedOutputStream(fos);

			int contents = 0;
			while ((contents = inBuffer.read()) != -1) {
				outBuffer.write(contents);
			}

		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe);
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}

		if (new File(filePath).exists()) {
			String relativePath = UmDesignBusinessRuleUtils.getUserCustomIconSharePath(userId,
					inputBean.getIconFileNm());

			inputBean.setIconFilePath(relativePath);
			inputBean.setIconOption(IconSelectionOption.CustomImage);
		}

	}

	private void submitUserChanges(FlowPersonalSettingsEditServiceBean paramBean, String userId) {
		UserEditInputBean inputBean = paramBean.getParam().getUserInputInfo();
		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId(userId);

		int count = this.countUserByName(inputBean.getUserNm(), userId);
		String validatedIconPath = null;
		if (IconSelectionOption.CustomImage.equals(inputBean.getIconOption())) {

			userEntity.setCustomIconPath(inputBean.getIconFilePath());
			validatedIconPath = inputBean.getIconFilePath();

		} else if (IconSelectionOption.DefaultImage.equals(inputBean.getIconOption())) {

			userEntity.setDefaultIconPath(inputBean.getIconPath());
			validatedIconPath = inputBean.getIconPath();
		}
		UmItemChkUtils.checkUserPersonalSetting(inputBean.getUserNm(), inputBean.getMailAddress(), validatedIconPath, count);

		userEntity.setUserNm(inputBean.getUserNm());
		userEntity.setEmailAddr(inputBean.getMailAddress());
		userEntity.setIconTyp(inputBean.getIconOption().value());
		userEntity.setGenderTyp(
				TriStringUtils.isEmpty(inputBean.getGenderType()) ? null : inputBean.getGenderType().value());
		userEntity.setUserLocation(inputBean.getLocation());
		userEntity.setBiography(inputBean.getBiography());

		support.getUserDao().update(userEntity);

		paramBean.getHeader().setUserIconPath(validatedIconPath);
		paramBean.getResult().setUserIconPath(validatedIconPath);
		paramBean.setUserName(inputBean.getUserNm());

		// Delete um_user_url_lnk
		{
			UserUrlCondition urlCondition = new UserUrlCondition();
			urlCondition.setUserId(userId);

			support.getUserUrlDao().delete(urlCondition.getCondition());
		}

		if (TriStringUtils.isNotEmpty(inputBean.getUrl())) {
			int urlCount = 1;
			for (String url : inputBean.getUrl()) {
				if (!TriStringUtils.isNotEmpty(url)) {
					continue;
				}
				IUserUrlEntity urlEntity = new UserUrlEntity();
				urlEntity.setUserId(userId);
				urlEntity.setUrl(url);
				urlEntity.setUrlSeqNo(String.valueOf(urlCount));

				support.getUserUrlDao().insert(urlEntity);

				urlCount++;
			}
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003082I);

	}

	private void submitPasswordChanges(FlowPersonalSettingsEditServiceBean paramBean, String userId) {
		PasswordEditInputBean inputBean = paramBean.getParam().getPasswordInputInfo();

		IUserEntity userEntity = support.findUserByUserId(userId);

		List<IUserPassHistEntity> passwordEntities = support.findUserPassHistByUserId(userId);

		int historyNumber = UmUserEditServiceUtils.getHistoryNumber();

		String encryptedNewPassword = UmUserEditServiceUtils.checkPassword(inputBean.getCurrentPassword(),
				inputBean.getNewPassword(), inputBean.getConfirmPassword(), userEntity, passwordEntities,
				historyNumber);

		passwordEntities = UmUserEditServiceUtils.initUserPasswordHistory(userEntity, passwordEntities, historyNumber);

		// Update user's password
		{
			IUserEntity newUserEntity = new UserEntity();
			newUserEntity.setUserId(userId);
			newUserEntity.setUserPass(encryptedNewPassword);

			if (TriStringUtils.isNotEmpty(inputBean.getPasswordExpiration())) {
				newUserEntity.setPassTimeLimit(
						UmUserEditServiceUtils.getPasswordEffectiveDate(inputBean.getPasswordExpiration()));
			}

			support.getUserDao().update(newUserEntity);
		}

		// Update user password history
		{
			UserPassHistCondition condition = new UserPassHistCondition();
			condition.setUserId(userId);
			support.getUserPassHistDao().delete(condition.getCondition());
			if (0 < historyNumber) {
				List<IUserPassHistEntity> insertEntityList = new ArrayList<IUserPassHistEntity>();

				IUserPassHistEntity passwordEntity = new UserPassHistEntity();
				passwordEntity.setUserId(userId);
				passwordEntity.setPassHistSeqNo("0");
				passwordEntity.setUserPass(encryptedNewPassword);

				insertEntityList.add(passwordEntity);

				if (1 < historyNumber) {

					for (int i = 1; i < historyNumber; i++) {

						if (passwordEntities.size() < i)
							break;

						passwordEntity = new UserPassHistEntity();
						passwordEntity.setUserId(userId);
						passwordEntity.setPassHistSeqNo(Integer.toString(i));
						passwordEntity.setUserPass(passwordEntities.get(i - 1).getUserPass());

						insertEntityList.add(passwordEntity);
					}
				}

				for (IUserPassHistEntity insertEntity : insertEntityList) {
					support.getUserPassHistDao().insert(insertEntity);
				}
			}
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003056I);
	}

	private void submitLanguageTimezoneChanges(FlowPersonalSettingsEditServiceBean paramBean, String userId) {

		LanguageAndTimezoneEditInputBean inputBean = paramBean.getParam().getLanguageTimezoneInputInfo();
		LanguageAndTimezoneDetailsView languageAndTimezoneDetailsView = paramBean.getDetailsView().getLanguageTimezone();
		// Update user's language and timezone
		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId(userId);

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.support.findProjectByPrimaryKey(productId);

		if (TriStringUtils.isEmpty(inputBean.getLanguage())) {
			userEntity.setLang(projectEntity.getLanguage());
		} else {
			userEntity.setLang(inputBean.getLanguage());
		}

		if (TriStringUtils.isEmpty(inputBean.getTimezone())) {
			userEntity.setTimeZone(projectEntity.getTimezone());
		} else {
			userEntity.setTimeZone(inputBean.getTimezone());
		}

		support.getUserDao().update(userEntity);

		languageAndTimezoneDetailsView.getDefaultTimeZone()
			.setLabel(UmUserEditServiceUtils.getTimezoneName(projectEntity.getTimezone()))
			.setValue("");

		languageAndTimezoneDetailsView.getDefaultLanguage()
			.setLabel(UmUserEditServiceUtils.getLanguageName(projectEntity.getLanguage()))
			.setValue("");

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003058I);

	}

	private void lookAndFeelChanges(FlowPersonalSettingsEditServiceBean paramBean, String userId) {
		LookAndFeel lookAndFeel = paramBean.getParam().getLookAndFeelInputInfo().getLookAndFeel();
		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId(userId);
		if (lookAndFeel != null) {
			userEntity.setLookAndFeel(lookAndFeel.dbValue());
		} else {
			userEntity.setLookAndFeel("");
		}
		support.getUserDao().update(userEntity);

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003084I);
	}

	private int countUserByName(String userNm, String userId) {
		if (TriStringUtils.isEmpty(userNm)) {
			return 0;
		}

		UserCondition condition = new UserCondition();
		condition.setUserIdsByNotEquals(userId);
		condition.setUserNm(userNm);

		return support.getUserDao().count(condition.getCondition());
	}
}
