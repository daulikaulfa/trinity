package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.constants.FileStatus;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowWikiVersionComparisonServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private String wikiNm;
	private List<WikiVersionView> versionViews = new ArrayList<WikiVersionView>();
	private VersionComparisonView versionComparisonView = new VersionComparisonView();

	public RequestParam getParam() {
		return param;
	}

	public String getWikiNm(){
		return this.wikiNm;
	}
	public FlowWikiVersionComparisonServiceBean setWikiNm( String wikiNm ){
		this.wikiNm = wikiNm;
		return this;
	}

	public List<WikiVersionView> getVersionViews(){
		return this.versionViews;
	}
	public FlowWikiVersionComparisonServiceBean setVersionViews( List<WikiVersionView> versionViews ){
		this.versionViews = versionViews;
		return this;
	}

	public VersionComparisonView getVersionComparisonView(){
		return this.versionComparisonView;
	}
	public FlowWikiVersionComparisonServiceBean setVersionComparisonView( VersionComparisonView versionComparisonView ){
		this.versionComparisonView = versionComparisonView;
		return this;
	}

	public class RequestParam extends DomainServiceBean.RequestParam {
		private String wikiId;
		private Integer destVersion;
		private Integer srcVersion;

		public String getSelectedWikiId() {
			return this.wikiId;
		}
		public RequestParam setSelectedWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public Integer getDestVersion() {
			return this.destVersion;
		}
		public RequestParam setDestVersion(Integer destVersion) {
			this.destVersion = destVersion;
			return this;
		}

		public Integer getSrcVersion() {
			return this.srcVersion;
		}
		public RequestParam setSrcVersion(Integer srcVersion) {
			this.srcVersion = srcVersion;
			return this;
		}
	}

	public class WikiVersionView {
		private String pageNm;
		private Integer wikiVerNo;
		private String content;
		private String updUserNm;
		private String updUserId;
		private String updUserIconPath;
		private String updDate;

		public String getPageNm(){
			return this.pageNm;
		}
		public WikiVersionView setPageNm( String pageNm ){
			this.pageNm = pageNm;
			return this;
		}

		public Integer getWikiVerNo(){
			return this.wikiVerNo;
		}
		public WikiVersionView setWikiVerNo( Integer wikiVerNo ){
			this.wikiVerNo = wikiVerNo;
			return this;
		}

		public String getContent(){
			return this.content;
		}
		public WikiVersionView setContent( String content ){
			this.content = content;
			return this;
		}

		public String getUpdUserNm(){
			return this.updUserNm;
		}
		public WikiVersionView setUpdUserNm( String updUserNm ){
			this.updUserNm = updUserNm;
			return this;
		}

		public String getUpdUserId(){
			return this.updUserId;
		}
		public WikiVersionView setUpdUserId( String updUserId ){
			this.updUserId = updUserId;
			return this;
		}

		public String getUpdUserIconPath(){
			return this.updUserIconPath;
		}
		public WikiVersionView setUpdUserIconPath( String updUserIconPath ){
			this.updUserIconPath = updUserIconPath;
			return this;
		}

		public String getUpdDate(){
			return this.updDate;
		}
		public WikiVersionView setUpdDate( String updDate ){
			this.updDate = updDate;
			return this;
		}
	}

	public class VersionComparisonView {
		private WikiVersionView destVersionView = new WikiVersionView();
		private WikiVersionView srcVersionView = new WikiVersionView();
		private List<VersionLineView> lines = new ArrayList<VersionLineView>();

		public WikiVersionView getDestVersionView() {
			return this.destVersionView;
		}
		public VersionComparisonView setDestVersionView(WikiVersionView destVersionView) {
			this.destVersionView = destVersionView;
			return this;
		}

		public WikiVersionView getSrcVersionView() {
			return this.srcVersionView;
		}
		public VersionComparisonView setSrcVersionView(WikiVersionView srcVersionView) {
			this.srcVersionView = srcVersionView;
			return this;
		}

		public List<VersionLineView> getLines() {
			return this.lines;
		}
		public VersionComparisonView setLines(List<VersionLineView> lines) {
			this.lines = lines;
			return this;
		}

		public class VersionLineView {
			private FileStatus status = FileStatus.none;
			private String dest;
			private String destLineNo;
			private String src;
			private String srcLineNo;

			public FileStatus getStatus() {
				return status;
			}
			public VersionLineView setStatus(FileStatus status) {
				this.status = status;
				return this;
			}

			public String getDest() {
				return dest;
			}
			public VersionLineView setDest(String dest) {
				this.dest = dest;
				return this;
			}

			public String getDestLineNo() {
				return destLineNo;
			}
			public VersionLineView setDestLineNo(String destLineNumber) {
				this.destLineNo = destLineNumber;
				return this;
			}

			public String getSrc() {
				return src;
			}
			public VersionLineView setSrc(String src) {
				this.src = src;
				return this;
			}

			public String getSrcLineNo() {
				return srcLineNo;
			}
			public VersionLineView setSrcLineNo(String srcLineNumber) {
				this.srcLineNo = srcLineNumber;
				return this;
			}
		}
	}
}
