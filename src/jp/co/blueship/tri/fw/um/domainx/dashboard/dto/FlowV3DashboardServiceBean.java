package jp.co.blueship.tri.fw.um.domainx.dashboard.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * This is a service for the back-end(domain).
 *
 * @version V4.02.00
 * @author Akahoshi
 *
 */
public class FlowV3DashboardServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private V3DashboardDetailsView detailsView = new V3DashboardDetailsView();

	public RequestParam getParam() {
		return param;
	}

	public V3DashboardDetailsView getDetailsView() {
		return this.detailsView;
	}
	public FlowV3DashboardServiceBean setDetailsView( V3DashboardDetailsView detailsView ) {
		this.detailsView = detailsView;
		return this;
	}


	public class V3DashboardDetailsView {
		private boolean userInAdminSettingsRole = false;

		public boolean isUserInAdminSettingsRole() {
			return this.userInAdminSettingsRole;
		}
		public V3DashboardDetailsView setUserInAdminSettingsRole( boolean userInAdminSettingsRole ) {
			this.userInAdminSettingsRole = userInAdminSettingsRole;
			return this;
		}
	}
}
