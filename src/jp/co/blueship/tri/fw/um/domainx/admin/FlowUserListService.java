package jp.co.blueship.tri.fw.um.domainx.admin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignBeanId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.dao.oxm.IDesignSheet;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserRoleLnkCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserListServiceBean.UserView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowUserListService implements IDomain<FlowUserListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private static IDesignSheet sheet = DesignSheetFactory.getDesignSheet();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}


	@Override
	public IServiceDto<FlowUserListServiceBean> execute(
			IServiceDto<FlowUserListServiceBean> serviceDto) {
		FlowUserListServiceBean paramBean = serviceDto.getServiceBean();

		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified!");

			if ( RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.init(paramBean);
			}
			if ( RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				this.onChange(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowUserListServiceBean paramBean) {
		this.onChange(paramBean);
		this.generateDropBox(paramBean);
	}

	private void onChange(FlowUserListServiceBean paramBean) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		// Set User Condition
		UserCondition condition = this.getCondition(searchCondition);

		List<IUserEntity> userEntities = support.getUserDao().find(condition.getCondition());

		if (null == userEntities) {
			paramBean.setUserViews(new ArrayList<UserView>());
		} else {
			paramBean.setUserViews(this.getUserViews(paramBean, userEntities, searchCondition));
		}
	}

	private List<UserView> getUserViews(FlowUserListServiceBean paramBean, List<IUserEntity> userList, SearchCondition searchCondition) {
		List<UserView> userBeanList = new ArrayList<UserView>();
		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		SimpleDateFormat formatYMDHM = TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );
		for (IUserEntity entity: userList) {

			if (TriStringUtils.isNotEmpty(searchCondition.getRoleId())) {

				UserRoleLnkCondition userRoleCondition = new UserRoleLnkCondition();
				userRoleCondition.setRoleId(searchCondition.getRoleId());
				userRoleCondition.setUserId(entity.getUserId());

				if (0 == support.getUserRoleLnkDao().count(userRoleCondition.getCondition())) {
					continue;
				}
			}
			if (TriStringUtils.isNotEmpty(searchCondition.getGroupId())) {
				GrpUserLnkCondition grpUserCondition = new GrpUserLnkCondition();
				grpUserCondition.setGrpId(searchCondition.getGroupId());
				grpUserCondition.setUserId(entity.getUserId());

				if (0 == support.getGrpUserLnkDao().count(grpUserCondition.getCondition())) {
					continue;
				}

			}

			UserView view = paramBean.new UserView()
							.setUserId( entity.getUserId() )
							.setUserNm( entity.getUserNm() )
							.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath( entity ))
							.setGroupNm(this.getGroupNm(entity.getUserId()))
							.setMailAddress( entity.getEmailAddr() )
							.setLanguage( sheet.getValue(UmDesignBeanId.langs, entity.getLang()) )
							.setTimezone( TriStringUtils.timezoneOf( entity.getTimeZone() ) )
							.setLastLogInTime( TriDateUtils.convertViewDateFormat(entity.getLastLoginTimestamp(), formatYMDHM))
							.setCreatedDate( TriDateUtils.convertViewDateFormat(entity.getRegTimestamp(), formatYMD) )
							.setUpdDate( TriDateUtils.convertViewDateFormat(entity.getUpdTimestamp(), formatYMD) );

			userBeanList.add(view);
		}

		return userBeanList;
	}

	private UserCondition getCondition(SearchCondition searchCondition) {
		UserCondition condition = new UserCondition();

		if (TriStringUtils.isNotEmpty(searchCondition.getDeptId())) {
			condition.setDeptId(searchCondition.getDeptId());
		}
		if (TriStringUtils.isNotEmpty(searchCondition.getKeyword())) {
			condition.setContainsByKeyword(searchCondition.getKeyword());
		}

		return condition;
	}

	private List<String> getGroupNm(String userId) {
		List<IGrpEntity> grpEntities = this.support.findGroupByUserId(userId);
		if (null != grpEntities) {
			return FluentList
					.from(grpEntities)
					.map( UmFluentFunctionUtils.toGroupNmFromGrpUserLnkEntity ).asList();
		}

		return new ArrayList<String>();
	}

	private void generateDropBox(FlowUserListServiceBean paramBean) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();

		List<IRoleEntity> roleEntities = this.support.findAllRole();
		searchCondition.setRoleViews(FluentList.from( roleEntities).map( UmFluentFunctionUtils.toItemLabelsFromRoleEntity).asList());

		List<IGrpEntity> grpEntities = this.support.findAllGroup();
		searchCondition.setGroupViews(FluentList.from( grpEntities).map( UmFluentFunctionUtils.toItemLabelsFromGrpEntity).asList());

		List<IDeptEntity> deptEntities = this.support.findAllDept();
		searchCondition.setDepartmentViews(FluentList.from( deptEntities).map( UmFluentFunctionUtils.toItemLabelsFromDeptEntity).asList());
	}

}
