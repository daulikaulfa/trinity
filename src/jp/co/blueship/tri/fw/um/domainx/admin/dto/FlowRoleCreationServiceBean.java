package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.ActionSelectionViewBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRoleCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ActionSelectionViewBean actionSelectionView = new ActionSelectionViewBean();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowRoleCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ActionSelectionViewBean getActionSelectionView() {
		return actionSelectionView;
	}
	public FlowRoleCreationServiceBean setActionSelectionView(ActionSelectionViewBean actionSelectionView) {
		this.actionSelectionView = actionSelectionView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private RoleEditInputBean inputInfo = new RoleEditInputBean();

		public RoleEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(RoleEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private String roleId;
		private boolean completed = false;

		public String getRoleId() {
			return roleId;
		}
		public RequestsCompletion setRoleId(String roleId) {
			this.roleId = roleId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
