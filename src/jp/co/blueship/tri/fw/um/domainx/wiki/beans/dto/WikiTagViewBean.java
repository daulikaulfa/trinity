package jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class WikiTagViewBean {
	private String tagNm;
	private int quantity;

	public String getTagNm(){
		return this.tagNm;
	}
	public WikiTagViewBean setTagNm( String tagNm ){
		this.tagNm = tagNm;
		return this;
	}

	public int getQuantity(){
		return this.quantity;
	}
	public WikiTagViewBean setQuantity( int quantity ){
		this.quantity = quantity;
		return this;
	}
}
