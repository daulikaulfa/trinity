package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.ProcessStatus;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IProgressBar;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowProgressNotificationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private int countOfUnread = 0;
	private List<Notification> notificationViews = new ArrayList<Notification>();

	public RequestParam getParam() {
		return param;
	}

	public int getCountOfUnread() {
		return countOfUnread;
	}
	public FlowProgressNotificationServiceBean setCountOfUnread(int countOfUnread) {
		this.countOfUnread = countOfUnread;
		return this;
	}

	public List<Notification> getNotificationViews() {
		return notificationViews;
	}
	public FlowProgressNotificationServiceBean setNotificationViews(List<Notification> notificationViews) {
		this.notificationViews = notificationViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private int linesPerPage = 30;
		private RequestOption requestOption = RequestOption.none;

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		count( "count" ),
		views( "views" ),
		refreshViews("refresh"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public class Notification {
		private String procId;
		private String infoMessage;
		private String subject;
		private String resultMessage;
		private ProcessStatus result = ProcessStatus.none;
		private IProgressBar bar;
		private String remainingTime;
		private String startDate;
		private String endDate;
		private String serviceId;
		private String lotId;

		public String getProcId() {
			return procId;
		}
		public Notification setProcId(String procId) {
			this.procId = procId;
			return this;
		}

		public String getInfoMessage() {
			return infoMessage;
		}
		public Notification setInfoMessage(String infoMessage) {
			this.infoMessage = infoMessage;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public Notification setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getResultMessage() {
			return resultMessage;
		}
		public Notification setResultMessage(String resultMessage) {
			this.resultMessage = resultMessage;
			return this;
		}

		public boolean isActive() {
			return ProcessStatus.Active.equals( result );
		}
		public boolean isCompleted() {
			return ProcessStatus.Completed.equals( result );
		}
		public boolean isError() {
			return ProcessStatus.Error.equals( result );
		}
		public boolean isWarning() {
			return ProcessStatus.Warning.equals( result );
		}
		public Notification setResult(ProcessStatus result) {
			this.result = result;
			return this;
		}

		public IProgressBar getBar() {
			return bar;
		}
		public Notification setBar(IProgressBar bar) {
			this.bar = bar;
			return this;
		}

		public String getRemainingTime() {
			return remainingTime;
		}
		public Notification setRemainingTime(String remainingTime) {
			this.remainingTime = remainingTime;
			return this;
		}

		public String getStartDate() {
			return startDate;
		}
		public Notification setStartDate(String startDate) {
			this.startDate = startDate;
			return this;
		}

		public String getEndDate() {
			return endDate;
		}
		public Notification setEndDate(String endDate) {
			this.endDate = endDate;
			return this;
		}

		public String getServiceId() {
			return serviceId;
		}
		public Notification setServiceId(String serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		public String getLotId() {
			return lotId;
		}
		public Notification setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
	}

}
