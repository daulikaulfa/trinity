package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.EncryptionUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmUserEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean.GroupView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserCreationServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserCreationService implements IDomain<FlowUserCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowUserCreationServiceBean> execute(
			IServiceDto<FlowUserCreationServiceBean> serviceDto) {
		FlowUserCreationServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.generateDropBox(paramBean);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if ( paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges) ) {
					UmUserEditServiceUtils.checkComplexity(paramBean.getParam().getInputInfo().getPassword(),
							paramBean.getDetailsView().getPassword());
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				PreConditions.assertOf(paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges), "'RequestOption.UserChanges' is not specified");
				PreConditions.assertOf(paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges), "'RequestOption.PasswordChanges' is not specified");
				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void submitChanges( FlowUserCreationServiceBean paramBean ) {
		{
			//注意書き
			List<IMessageId> commentList	= new ArrayList<IMessageId>();
			List<String[]> commentArgsList	= new ArrayList<String[]>();

			commentList.add		( UmMessageId.UM003010I );
			commentArgsList.add	( new String[]{} );

			paramBean.setInfoCommentIdList		( commentList );
			paramBean.setInfoCommentArgsList	( commentArgsList );
		}

		paramBean.getDetailsView().setPassword(new PasswordDetailsView());

		UserEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.umSupport.findProjectByPrimaryKey(productId);

		if (TriStringUtils.isEmpty(inputInfo.getLanguage())) {
			inputInfo.setLanguage(projectEntity.getLanguage());
		}

		if (TriStringUtils.isEmpty(inputInfo.getTimeZone())) {
			inputInfo.setTimeZone(projectEntity.getTimezone());
		}

		int count = this.countUserByName(inputInfo.getUserNm());

		UmItemChkUtils.checkInputUser(inputInfo, count, umSupport);

		if (umSupport.isExistByUserId( inputInfo.getUserId() )) {
			throw new ContinuableBusinessException( UmMessageId.UM001024E );
		}

		// Insert um_user
		insertUser( inputInfo );

		if (TriStringUtils.isNotEmpty( inputInfo.getGroupIdSet() )) {

			// Insert um_grp_user_lnk
			umSupport.registerGrpUserLnk( inputInfo.getGroupIdSet().toArray(new String[0]), new String[]{ inputInfo.getUserId() });

			// Insert um_user_role_lnk
			umSupport.cleaningAndUpdateUserRoleLnk(new String[]{ inputInfo.getUserId() });
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getResult().setUserId( inputInfo.getUserId() );
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003070I,inputInfo.getUserNm());
	}

	private void generateDropBox(FlowUserCreationServiceBean paramBean) {
		UserEditInputBean inputBean = paramBean.getParam().getInputInfo();

		// Department Views
		List<IDeptEntity> deptEntities = this.umSupport.findAllDept();
		inputBean
				.setDeptViews(FluentList.from(deptEntities)
						.map(UmFluentFunctionUtils.toItemLabelsFromDeptEntity)
						.asList());

		// Icon Views
		inputBean.setIconViews(UmUserEditServiceUtils.getIconViews(paramBean.getUserId()));

		// Password Expiration Effect
		inputBean.setPasswordExpirationViews( UmUserEditServiceUtils.getPasswordExpirationTerm() );

		// Group Views
		inputBean.setGroupViews(this.getGroupviews(inputBean));

		// Language Views
		inputBean.setLanguageViews(UmUserEditServiceUtils.getLanguageViews() );

		// TimeZone Views
		inputBean.setTimeZoneViews( UmUserEditServiceUtils.getTimezoneViews() );

		// Admin language and timezone
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.umSupport.findProjectByPrimaryKey(productId);
		paramBean.getDetailsView()
				 .getDefaultTimeZone()
				 .setLabel(UmUserEditServiceUtils.getTimezoneName(projectEntity.getTimezone()));

		paramBean.getDetailsView()
		 		 .getDefaultLanguage()
		 		 .setLabel(UmUserEditServiceUtils.getLanguageName(projectEntity.getLanguage()));
	}

	private List<GroupView> getGroupviews(UserEditInputBean inputBean) {
		List<GroupView> groupViews = new ArrayList<GroupView>();
		for (IGrpEntity grpEntity : this.umSupport.findAllGroup()) {
			GrpUserLnkCondition condition = new GrpUserLnkCondition();
			condition.setGrpId( grpEntity.getGrpId() );
			int count = this.umSupport.getGrpUserLnkDao().count(condition.getCondition());

			GroupView grpView = inputBean.new GroupView()
				.setGroupId( grpEntity.getGrpId() )
				.setGroupNm( grpEntity.getGrpNm() )
				.setCount( count );

			groupViews.add(grpView);
		}

		return groupViews;
	}

	private int countUserByName(String userNm) {
		if ( TriStringUtils.isEmpty(userNm) ) {
			return 0;
		}

		UserCondition condition = new UserCondition();
		condition.setUserNm(userNm);

		return umSupport.getUserDao().count(condition.getCondition());
	}

	private void insertUser( UserEditInputBean inputBean) {
		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId( inputBean.getUserId() );
		userEntity.setUserNm( inputBean.getUserNm() );
		userEntity.setUserPass(
				EncryptionUtils.encodePasswordInA1Format( inputBean.getUserId(), inputBean.getPassword() ));
		userEntity.setPassTimeLimit(
				UmUserEditServiceUtils.getPasswordEffectiveDate( inputBean.getPasswordExpiration() ));
		userEntity.setEmailAddr( inputBean.getMailAddress() );

		if (TriStringUtils.isNotEmpty(inputBean.getDeptId())) {
			IDeptEntity deptEntity = umSupport.findDeptByDeptId(inputBean.getDeptId());
			if ( null != deptEntity ) {
				userEntity.setDeptId(deptEntity.getDeptId());
				userEntity.setDeptNm(deptEntity.getDeptNm());
			}
		}

		userEntity.setLang(inputBean.getLanguage());
		userEntity.setTimeZone(inputBean.getTimeZone());
		userEntity.setIconTyp(IconSelectionOption.DefaultImage.value());
		userEntity.setDefaultIconPath(inputBean.getIconPath());
		userEntity.setIsReceiveMail(inputBean.isReceiveMail()? StatusFlg.on : StatusFlg.off);

		umSupport.getUserDao().insert(userEntity);
	}

}
