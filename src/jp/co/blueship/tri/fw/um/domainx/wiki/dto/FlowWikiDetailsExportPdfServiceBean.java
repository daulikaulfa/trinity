package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean;
import jp.co.blueship.tri.am.domainx.chgproperty.dto.FlowChangePropertyListServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author thang.vu
 */
public class FlowWikiDetailsExportPdfServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private ChangePropertyListFileResponse fileResponse = new ChangePropertyListFileResponse();
	private WikiView detailsView = new WikiView();

	{
		this.setInnerService(new FlowChangePropertyListServiceBean());
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public FlowWikiDetailsExportPdfServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public ChangePropertyListFileResponse getFileResponse() {
		return fileResponse;
	}

	public FlowWikiDetailsExportPdfServiceBean setFileResponse(ChangePropertyListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	/**
	 * @return the detailsView
	 */
	public WikiView getDetailsView() {
		return detailsView;
	}

	/**
	 * @param detailsView
	 *            the detailsView to set
	 */
	public void setDetailsView(WikiView detailsView) {
		this.detailsView = detailsView;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private String wikiId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowChangePropertyListServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		/**
		 * @return the wikiId
		 */
		public String getWikiId() {
			return wikiId;
		}

		/**
		 * @param wikiId the wikiId to set
		 */
		public RequestParam setWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public String getLotId() {
			return lotId;
		}

		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}

		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}

		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}

		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class ChangePropertyListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}

		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	public class WikiView {
		private String wikiId;
		private String wikiNm;
		private String content;
		private boolean isWikiHome = false;
		private String createdBy;
		private String createdByUserId;
		private String createdIconPath;
		private String createdDate;
		private String updUserNm;
		private String updUserId;
		private String updUserIconPath;
		private String updDate;
		private List<String> tagViews = new ArrayList<String>();
		private List<String> attachmentFileNms = new ArrayList<String>();

		public String getWikiId() {
			return this.wikiId;
		}

		public WikiView setWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public String getWikiNm() {
			return this.wikiNm;
		}

		public WikiView setWikiNm(String wikiNm) {
			this.wikiNm = wikiNm;
			return this;
		}

		public String getContent() {
			return this.content;
		}

		public WikiView setContent(String content) {
			this.content = content;
			return this;
		}

		public boolean isWikiHome() {
			return this.isWikiHome;
		}

		public WikiView setIsWikiHome(boolean isWikiHome) {
			this.isWikiHome = isWikiHome;
			return this;
		}

		public String getCreatedBy() {
			return this.createdBy;
		}

		public WikiView setCreatedBy(String createdBy) {
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByUserId() {
			return this.createdByUserId;
		}

		public WikiView setCreatedByUserId(String createdByUserId) {
			this.createdByUserId = createdByUserId;
			return this;
		}

		public String getCreatedIconPath() {
			return this.createdIconPath;
		}

		public WikiView setCreatedIconPath(String createdIconPath) {
			this.createdIconPath = createdIconPath;
			return this;
		}

		public String getCreatedDate() {
			return this.createdDate;
		}

		public WikiView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}

		public String getUpdUserNm() {
			return this.updUserNm;
		}

		public WikiView setUpdUserNm(String updUserNm) {
			this.updUserNm = updUserNm;
			return this;
		}

		public String getUpdUserId() {
			return this.updUserId;
		}

		public WikiView setUpdUserId(String updUserId) {
			this.updUserId = updUserId;
			return this;
		}

		public String getUpdUserIconPath() {
			return this.updUserIconPath;
		}

		public WikiView setUpdUserIconPath(String updUserIconPath) {
			this.updUserIconPath = updUserIconPath;
			return this;
		}

		public String getUpdDate() {
			return this.updDate;
		}

		public WikiView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}

		public List<String> getTagViews() {
			return this.tagViews;
		}

		public WikiView setTagViews(List<String> tagViews) {
			this.tagViews = tagViews;
			return this;
		}

		public List<String> getAttachmentFileNms() {
			return attachmentFileNms;
		}

		public WikiView setAttachmentFileNms(List<String> attachmentFileNms) {
			this.attachmentFileNms = attachmentFileNms;
			return this;
		}
	}
}
