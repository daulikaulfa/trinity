package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.HashSet;
import java.util.Set;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private UserDetailsView detailsView = new UserDetailsView();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowUserEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public UserDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowUserEditServiceBean setDetailsView(UserDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * This is the class of all PersonalSettings enumeration types.
	 */
	public enum RequestOption {
		UserChanges( "userChanges" ),
		PasswordChanges( "passwordChanges" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private Set<String> requestOptions = new HashSet<String>();
		private UserEditInputBean inputInfo = new UserEditInputBean();

		public boolean isRequestOptionOf( RequestOption option ) {
			if ( null == option )
				return false;

			return requestOptions.contains(option.value);
		}
		public RequestParam setRequestOptions(RequestOption... requestOptions) {
			if ( null == requestOptions )
				return this;

			this.requestOptions = new HashSet<String>();
			for ( RequestOption option: requestOptions ) {
				this.requestOptions.add( option.value );
			}

			return this;
		}

		public UserEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(UserEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}
	}

	/**
	 * User Information
	 */
	public class UserDetailsView {
		private ItemLabelsBean defaultLanguage = new ItemLabelsBean();
		private ItemLabelsBean defaultTimeZone = new ItemLabelsBean();
		private String userId = null;
		private PasswordDetailsView password = new PasswordDetailsView();

		public ItemLabelsBean getDefaultLanguage() {
			return defaultLanguage;
		}
		public UserDetailsView setDefaultLanguage(ItemLabelsBean defaultLanguage) {
			this.defaultLanguage = defaultLanguage;
			return this;
		}

		public ItemLabelsBean getDefaultTimeZone() {
			return defaultTimeZone;
		}
		public UserDetailsView setDefaultTimeZone(ItemLabelsBean defaultTimeZone) {
			this.defaultTimeZone = defaultTimeZone;
			return this;
		}

		public String getUserId() {
			return userId;
		}
		public UserDetailsView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public PasswordDetailsView getPassword() {
			return password;
		}
		public UserDetailsView setPassword(PasswordDetailsView password) {
			this.password = password;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
