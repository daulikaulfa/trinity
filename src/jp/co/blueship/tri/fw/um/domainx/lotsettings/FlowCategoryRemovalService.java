package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgLnkEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryRemovalService implements IDomain<FlowCategoryRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowCategoryRemovalServiceBean> execute(
			IServiceDto<FlowCategoryRemovalServiceBean> serviceDto) {

		FlowCategoryRemovalServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String ctgId = paramBean.getParam().getSelectedCategoryId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(ctgId), "SelectedCategoryId is not specified");

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				CtgCondition condition = new CtgCondition();
				condition.setCtgId( ctgId );

				String categoryName = "";
				if ( 0 < support.getCtgDao().count(condition.getCondition()) ) {
					ICtgEntity ctgEntity = support.getCtgDao().findByPrimaryKey( condition.getCondition() );
					categoryName = ctgEntity.getCtgNm();
					this.removeCategory( ctgId , ctgEntity.getLotId() );
				};

				paramBean.getResult().setCompleted(true);
				if( TriStringUtils.isNotEmpty( categoryName ) ){
					paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003060I,categoryName);
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void removeCategory(String ctgId , String lotId) {
		ICtgEntity ctgEntity = new CtgEntity();
		ctgEntity.setCtgId(ctgId);
		ctgEntity.setDelStsId(StatusFlg.on);
		support.getCtgDao().update(ctgEntity);
		
		CtgLnkCondition ctgLnkCondition = new CtgLnkCondition();
		ctgLnkCondition.setCtgId(ctgId);
		List<ICtgLnkEntity> ctgLnkEnties = support.getCtgLnkDao().find(ctgLnkCondition.getCondition());
		if( ctgLnkEnties != null ) {
			for (ICtgLnkEntity ctgLnkEntity : ctgLnkEnties) {
				ctgLnkEntity.setDelStsId(StatusFlg.on);
			}
			support.getCtgLnkDao().update(ctgLnkEnties);
		}
		
		if ( DesignSheetUtils.isRecord() ) {
			ctgEntity.setLotId(lotId);
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Remove.getStatusId());
			support.getCtgHistDao().insert( histEntity , ctgEntity);
		}
	}
}
