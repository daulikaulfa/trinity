package jp.co.blueship.tri.fw.um.domainx.project;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserUrlEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserUrlEntity;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.dto.FlowProfileEditServiceBean.ProfileEditInputBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowProfileEditService implements IDomain<FlowProfileEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowProfileEditServiceBean> execute(IServiceDto<FlowProfileEditServiceBean> serviceDto) {
		FlowProfileEditServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {

				this.submitChanges(paramBean);
			}

			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void submitChanges( FlowProfileEditServiceBean paramBean ) {
		String userId = paramBean.getUserId();
		ProfileEditInputBean inputBean = paramBean.getParam().getInputInfo();

		IUserEntity userEntity = new UserEntity();
		userEntity.setUserId(userId);
		userEntity.setGenderTyp(TriStringUtils.isEmpty(inputBean.getGenderType())? null : inputBean.getGenderType().value());
		userEntity.setUserLocation( inputBean.getUserLocation() );
		userEntity.setBiography( inputBean.getBiography() );

		// Update um_user
		support.getUserDao().update(userEntity);

		// Delete um_user_url
		{
			UserUrlCondition urlCondition = new UserUrlCondition();
			urlCondition.setUserId(userId);

			support.getUserUrlDao().delete(urlCondition.getCondition());
		}

		if (TriStringUtils.isNotEmpty( inputBean.getUrls() )) {
			int urlCount = 1;
			for (String url : inputBean.getUrls() ) {
				if (StringUtils.isNotEmpty(url)) {
					IUserUrlEntity urlEntity = new UserUrlEntity();
					urlEntity.setUserId( userId );
					urlEntity.setUrl( url );
					urlEntity.setUrlSeqNo( String.valueOf(urlCount) );
					support.getUserUrlDao().insert(urlEntity);
					urlCount++;
				}
			}
		}

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003083I);

	}

}
