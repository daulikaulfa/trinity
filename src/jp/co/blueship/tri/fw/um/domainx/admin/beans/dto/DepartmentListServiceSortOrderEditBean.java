package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class DepartmentListServiceSortOrderEditBean {

	private String deptId = null;
	private int newSortOrder = 0;

	public String getDeptId() {
		return deptId;
	}
	public DepartmentListServiceSortOrderEditBean setDeptId(String deptId) {
		this.deptId = deptId;
		return this;
	}
	public int getNewSortOrder() {
		return newSortOrder;
	}
	public DepartmentListServiceSortOrderEditBean setNewSortOrder(int newSortOrder) {
		this.newSortOrder = newSortOrder;
		return this;
	}
}
