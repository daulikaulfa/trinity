package jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.bm.dao.constants.BmTables;
import jp.co.blueship.tri.dm.dao.constants.DmTables;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.dao.orm.constants.ITableAttribute;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.rm.dao.constants.RmTables;

public class GanttViewBean {

	private Grouping grouping = Grouping.none;
	private String groupingNm;
	/**
	 * Assignee Only
	 */
	private String iconPath;
	private GanttHeader header = new GanttHeader();
	private List<GanttContent> contents = new ArrayList<GanttContent>();

	public Grouping getGrouping() {
		return grouping;
	}
	public GanttViewBean setGrouping(Grouping grouping) {
		this.grouping = grouping;
		return this;
	}
	public String getGroupingNm() {
		return groupingNm;
	}
	public GanttViewBean setGroupingNm(String groupingNm) {
		this.groupingNm = groupingNm;
		return this;
	}
	public String getIconPath() {
		return iconPath;
	}
	public GanttViewBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}
	public GanttHeader getHeader() {
		return header;
	}
	public GanttViewBean setHeader(GanttHeader header) {
		this.header = header;
		return this;
	}
	public List<GanttContent> getContents() {
		return contents;
	}
	public GanttViewBean setContents(List<GanttContent> contents) {
		this.contents = contents;
		return this;
	}

	public class GanttHeader {
		private List<GanttPeriod> periods = new ArrayList<GanttPeriod>();

		public List<GanttPeriod> getPeriods() {
			return periods;
		}
		public GanttHeader setPeriods(List<GanttPeriod> periods) {
			this.periods = periods;
			return this;
		}
	}

	public class GanttPeriod {
		private String period;
		private List<GanttDay> days = new ArrayList<GanttDay>();

		public String getPeriod() {
			return period;
		}
		public GanttPeriod setPeriod(String period) {
			this.period = period;
			return this;
		}

		public List<GanttDay> getDays() {
			return days;
		}
		public GanttPeriod setDays(List<GanttDay> days) {
			this.days = days;
			return this;
		}
	}

	public class GanttDay {
		private int seq;
		private String day;
		private boolean today = false;
		private boolean holiday = false;
		private boolean weekBeginning = false;
		private boolean flag = false;
		private String flagNm;

		public int getSeq() {
			return seq;
		}
		public GanttDay setSeq(int seq) {
			this.seq = seq;
			return this;
		}

		public String getDay() {
			return day;
		}
		public GanttDay setDay(String day) {
			this.day = day;
			return this;
		}

		public boolean isToday() {
			return today;
		}
		public GanttDay setToday(boolean today) {
			this.today = today;
			return this;
		}

		public boolean isHoliday() {
			return holiday;
		}
		public GanttDay setHoliday(boolean holiday) {
			this.holiday = holiday;
			return this;
		}

		public boolean isWeekBeginning() {
			return weekBeginning;
		}
		public GanttDay setWeekBeginning(boolean weekBeginning) {
			this.weekBeginning = weekBeginning;
			return this;
		}

		public boolean isFlag() {
			return flag;
		}
		public GanttDay setFlag(boolean flag) {
			this.flag = flag;
			return this;
		}

		public String getFlagNm() {
			return flagNm;
		}
		public GanttDay setFlagNm(String flagNm) {
			this.flagNm = flagNm;
			return this;
		}
	}

	public class GanttContent {
		private GroupingDataType groupingDataType = GroupingDataType.none;
		private DataAttribute dataAttribute = DataAttribute.none;
		private String dataId;
		private String subject;
		private String assigneeNm;
		private String assigneeIconPath;
		private String startDate;
		private String endDate;
		private int seq;
		private int countDays;
		private Status status = Status.All;

		public GroupingDataType getGroupingDataType() {
			return groupingDataType;
		}
		public GanttContent setGroupingDataType(GroupingDataType groupingDataType) {
			this.groupingDataType = groupingDataType;
			return this;
		}

		public DataAttribute getDataAttribute() {
			return dataAttribute;
		}
		public GanttContent setDataAttribute(DataAttribute dataAttribute) {
			this.dataAttribute = dataAttribute;
			return this;
		}

		public String getDataId() {
			return dataId;
		}
		public GanttContent setDataId(String dataId) {
			this.dataId = dataId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public GanttContent setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getAssigneeNm() {
			return assigneeNm;
		}
		public GanttContent setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}

		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public GanttContent setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}

		public String getStartDate() {
			return startDate;
		}
		public GanttContent setStartDate(String startDate) {
			this.startDate = startDate;
			return this;
		}

		public String getEndDate() {
			return endDate;
		}
		public GanttContent setEndDate(String endDate) {
			this.endDate = endDate;
			return this;
		}

		public int getSeq() {
			return seq;
		}
		public GanttContent setSeq(int seq) {
			this.seq = seq;
			return this;
		}

		public int getCountDays() {
			return countDays;
		}
		public GanttContent setCountDays(int countDays) {
			this.countDays = countDays;
			return this;
		}

		public Status getStatus() {
			return status;
		}
		public GanttContent setStatus(Status status) {
			this.status = status;
			return this;
		}
	}

	public enum Grouping {
		none		( "none" ),
		DataType	( "dataType" ),
		Assignee	( "assignee" ),
		Lot			( "lot" ),
		Milestone	( "milestone" ),
		Category	( "category" ),
		;

		private String value = null;

		private Grouping( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			Grouping type = value( value );

			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static Grouping value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return none;
			}

			for ( Grouping type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}
			return none;
		}
	}

	public enum GroupingDataType {
		none			( "", null ),
		CheckOutRequest	( "CheckOutRequest", AmTables.AM_AREQ ),
		CheckInRequest	( "CheckInRequest", AmTables.AM_AREQ ),
		RemovalRequest	( "RemovalRequest", AmTables.AM_AREQ ),
		BuildPackage	( "BuildPackage", BmTables.BM_BP ),
		BuildPackageClose
						( "BuildPackageClose", BmTables.BM_BP ),
		ReleaseRequest	( "ReleaseRequest", RmTables.RM_RA ),
		ReleasePackage	( "ReleasePackage", RmTables.RM_RP ),
		DeploymentJob	( "DeploymentJob", DmTables.DM_DO ),
		DeploymentJobTimer
						( "DeploymentJobTimer", DmTables.DM_DO ),
		ConflictCheck	( "ConflictCheck", AmTables.AM_HEAD_BL ),
		Merge			( "Merge", AmTables.AM_HEAD_BL ),
		;

		private String value = null;
		private ITableAttribute attr = null;

		private GroupingDataType	( String value, ITableAttribute attr) {
			this.value = value;
			this.attr = attr;
		}

		public boolean equals( String value ) {
			GroupingDataType type = value( value );

			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public ITableAttribute getTableAttribute() {
			return this.attr;
		}

		public static GroupingDataType value( String name ) {
			if ( TriStringUtils.isEmpty( name ) ) {
				return none;
			}

			for ( GroupingDataType type : values() ) {
				if ( type.value().equals( name ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public enum Status {
		All			( "all" ),
		InProgress	( "inProgress" ),
		Completed	( "completed" ),
		Closed		( "closed" ),
		NotClosed	( "notClosed" ),
		;

		private String value = null;

		private Status( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			Status type = value( value );

			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static Status value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return All;
			}

			for ( Status type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}
			return All;
		}
	}
}
