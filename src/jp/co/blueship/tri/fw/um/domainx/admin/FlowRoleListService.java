package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.role.constants.RoleItems;
import jp.co.blueship.tri.fw.um.dao.role.eb.IRoleEntity;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleCondition;
import jp.co.blueship.tri.fw.um.dao.role.eb.RoleEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowRoleListServiceBean.RoleView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowRoleListService implements IDomain<FlowRoleListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;


	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowRoleListServiceBean> execute(
			IServiceDto<FlowRoleListServiceBean> serviceDto) {
		FlowRoleListServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setRoleViews(this.getRoleViews(paramBean));
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if (null != paramBean.getParam().getSortOrderEdit()) {
					this.updateSortOrder( paramBean.getParam().getSortOrderEdit() );
					paramBean.setRoleViews(this.getRoleViews(paramBean));
					paramBean.getResult().setCompleted(true);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private List<RoleView> getRoleViews(FlowRoleListServiceBean paramBean) {
		RoleCondition condition = new RoleCondition();
		ISqlSort sort = new SortBuilder();
		sort.setElement(RoleItems.sortOdr, TriSortOrder.Asc, 1);

		List<IRoleEntity> roleEntities = support.getRoleDao().find(condition.getCondition(), sort);
		List<RoleView> views = new ArrayList<RoleView>();
		for (IRoleEntity entity : roleEntities) {
			RoleView viewBean = paramBean.new RoleView()
				.setRoleId(entity.getRoleId())
				.setRoleNm(entity.getRoleName())
				.setSortOrder(entity.getSortOdr());
			views.add(viewBean);
		}

		return views;
	}

	private void updateSortOrder(RoleListServiceSortOrderEditBean sortOrderBean) {
		String roleId = sortOrderBean.getRoleId();
		int newSortOrder = sortOrderBean.getNewSortOrder();
		int currentRow = 1;

		for (IRoleEntity entity : support.findAllRole()) {
			if ( currentRow == newSortOrder ) {
				currentRow++;
			}

			if ( ! entity.getRoleId().equals(roleId) ) {
				if ( currentRow != entity.getSortOdr() ) {
					IRoleEntity updEntity = new RoleEntity();
					updEntity.setRoleId( entity.getRoleId() );
					updEntity.setSortOdr( currentRow );
					support.getRoleDao().update( updEntity );
				}
				currentRow++;
			}
		}

		IRoleEntity newEntity = new RoleEntity();
		newEntity.setRoleId( roleId );
		newEntity.setSortOdr( newSortOrder );
		support.getRoleDao().update( newEntity );
	}
}
