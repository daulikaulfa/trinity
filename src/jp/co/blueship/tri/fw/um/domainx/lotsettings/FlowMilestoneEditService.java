package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneEditServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowMilestoneEditService implements IDomain<FlowMilestoneEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowMilestoneEditServiceBean> execute(
			IServiceDto<FlowMilestoneEditServiceBean> serviceDto) {

		FlowMilestoneEditServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String mstoneId = paramBean.getParam().getSelectedMstoneId();
			String lotId = paramBean.getParam().getSelectedLotId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mstoneId), "SelectedMstoneId is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				IMstoneEntity mstoneEntity = support.findMstoneByPrimaryKey(mstoneId);

				MilestoneEditInputBean inputInfo = new MilestoneEditInputBean();
				inputInfo.setSubject(mstoneEntity.getMstoneNm());
				inputInfo.setDescription(mstoneEntity.getContent());
				inputInfo.setStartDate(mstoneEntity.getMstoneStDate());
				inputInfo.setDueDate(mstoneEntity.getMstoneEndDate());
				paramBean.getParam().setInputInfo(inputInfo);
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				MilestoneEditInputBean inputInfo = paramBean.getParam().getInputInfo();
				int count = this.countMilestoneByName(lotId, mstoneId, inputInfo.getSubject());

				UmItemChkUtils.checkInputMilestone(inputInfo, count);

				updateMilestone(inputInfo, mstoneId , lotId);
				paramBean.getResult().setCompleted(true);
				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003065I,inputInfo.getSubject());
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void updateMilestone(MilestoneEditInputBean bean, String milestoneId , String lotId) {
		IMstoneEntity mstoneEntity = new MstoneEntity();
		mstoneEntity.setMstoneId(milestoneId);
		mstoneEntity.setMstoneNm(bean.getSubject());
		mstoneEntity.setMstoneStDate((null != bean.getStartDate())? TriDateUtils.fillYMD(bean.getStartDate()) : bean.getStartDate());
		mstoneEntity.setMstoneEndDate((null != bean.getDueDate())? TriDateUtils.fillYMD(bean.getDueDate()) : bean.getDueDate());
		mstoneEntity.setContent(bean.getDescription());
		support.getMstoneDao().update(mstoneEntity);

		if ( DesignSheetUtils.isRecord() ) {
			mstoneEntity.setLotId(lotId);
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Edit.getStatusId());
			support.getMstoneHistDao().insert( histEntity , mstoneEntity);
		}
	}

	private int countMilestoneByName(String lotId, String mstoneIdByNotEquals, String mstoneSubject) {
		if ( TriStringUtils.isEmpty(mstoneSubject) ) {
			return 0;
		}

		MstoneCondition condition = new MstoneCondition();
		condition.setLotId(lotId);
		condition.setMstoneIdsByNotEquals(mstoneIdByNotEquals);
		condition.setMstoneNm(mstoneSubject);

		return support.getMstoneDao().count(condition.getCondition());
	}
}
