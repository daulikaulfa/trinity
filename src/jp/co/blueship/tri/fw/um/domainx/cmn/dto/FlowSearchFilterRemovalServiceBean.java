package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 */
public class FlowSearchFilterRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowSearchFilterRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String selectedFilterId = null;

		public String getSelectedFilterId() {
			return selectedFilterId;
		}
		public RequestParam setSelectedFilterId(String selectedFilterId) {
			this.selectedFilterId = selectedFilterId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
