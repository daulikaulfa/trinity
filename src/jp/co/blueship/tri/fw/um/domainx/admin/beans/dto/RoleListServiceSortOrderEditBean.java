package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class RoleListServiceSortOrderEditBean {

	private String roleId = null;
	private int newSortOrder = 0;

	public String getRoleId() {
		return roleId;
	}

	public RoleListServiceSortOrderEditBean setRoleId(String roleId) {
		this.roleId = roleId;
		return this;
	}

	public int getNewSortOrder() {
		return newSortOrder;
	}

	public RoleListServiceSortOrderEditBean setNewSortOrder(int newSortOrder) {
		this.newSortOrder = newSortOrder;
		return this;
	}


}
