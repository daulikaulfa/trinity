package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class RoleEditInputBean {
	private String roleNm;
	private Set<String> actionIdSet = new LinkedHashSet<String>();
	private Set<String> groupIdSet = new LinkedHashSet<String>();
	private List<RoleGroupLinkViewBean> groupViews = new ArrayList<RoleGroupLinkViewBean>();

	public String getRoleNm() {
		return roleNm;
	}
	public RoleEditInputBean setRoleNm(String roleNm) {
		this.roleNm = roleNm;
		return this;
	}

	public Set<String> getActionIdSet() {
		return actionIdSet;
	}
	public RoleEditInputBean setActionIdSet(Set<String> actionIdSet) {
		this.actionIdSet = actionIdSet;
		return this;
	}
	public RoleEditInputBean setActionIds(String... actionIds) {
		actionIdSet.clear();
		actionIdSet.addAll( FluentList.from(actionIds).asList() );
		return this;
	}

	public Set<String> getGroupIdSet() {
		return groupIdSet;
	}
	public RoleEditInputBean setGroupIdSet(Set<String> groupIdSet) {
		this.groupIdSet = groupIdSet;
		return this;
	}
	public RoleEditInputBean setGroupIds(String... groupIds) {
		groupIdSet.clear();
		groupIdSet.addAll( FluentList.from(groupIds).asList() );
		return this;
	}

	public List<RoleGroupLinkViewBean> getGroupViews() {
		return groupViews;
	}
	public RoleEditInputBean setGroupViews(List<RoleGroupLinkViewBean> groupViews) {
		this.groupViews = groupViews;
		return this;
	}
}
