package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class DepartmentUserViewBean {
	private String userId = null;
	private String userNm = null;
	private String iconPath = null;
	private String mailAddress = null;

	public String getUserId() {
		return userId;
	}
	public DepartmentUserViewBean setUserId(String userId) {
		this.userId = userId;
		return this;
	}
	public String getUserNm() {
		return userNm;
	}
	public DepartmentUserViewBean setUserNm(String userNm) {
		this.userNm = userNm;
		return this;
	}
	public String getIconPath() {
		return iconPath;
	}
	public DepartmentUserViewBean setIconPath(String iconPath) {
		this.iconPath = iconPath;
		return this;
	}
	public String getMailAddress() {
		return mailAddress;
	}
	public DepartmentUserViewBean setMailAddress(String mailAddress) {
		this.mailAddress = mailAddress;
		return this;
	}

}
