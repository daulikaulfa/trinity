package jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class ExportToFileBean {

	private List<String> titleHeader = new ArrayList<String>();
	private List<List<String>> contents = new ArrayList<List<String>>();

	public ExportToFileBean setTitleHeader( List<String> titleHeader ){
		this.titleHeader = titleHeader;
		return this;
	}

	public List<String> getTitleHeader(){
		return this.titleHeader;
	}

	public ExportToFileBean addRow( List<String> row ){
		this.contents.add( row );
		return this;
	}

	public List<List<String>> getContents() {
		return this.contents;
	}

}
