package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowSearchFilterCreationServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowSearchFilterCreationServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	@Override
	public RequestParam getParam() {
		return param;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchFilterCreationInput inputInfo = new SearchFilterCreationInput();

		public SearchFilterCreationInput getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(SearchFilterCreationInput inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

	}

	/**
	 * Input Information
	 */
	public class SearchFilterCreationInput {
		private String lotId = null;
		private String filterNm = null;
		private String serviceId = null;
		private ISearchFilter searchFilter = null;

		public String getLotId () {
			return lotId;
		}

		public SearchFilterCreationInput setLotId( String lotId ){
			this.lotId = lotId;
			return this;
		}

		public String getFilterNm() {
			return filterNm;
		}
		public SearchFilterCreationInput setFilterNm(String filterNm) {
			this.filterNm = filterNm;
			return this;
		}

		public String getServiceId() {
			return serviceId;
		}
		public SearchFilterCreationInput setServiceId(String serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		public ISearchFilter getSearchFilter() {
			return searchFilter;
		}
		public SearchFilterCreationInput setSearchFilter(ISearchFilter searchFilter) {
			this.searchFilter = searchFilter;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;
		private String filterId = null;

		public String getFilterId() {
			return filterId;
		}
		public RequestsCompletion setFilterId(String filterId) {
			this.filterId = filterId;
			return this;
		}

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
