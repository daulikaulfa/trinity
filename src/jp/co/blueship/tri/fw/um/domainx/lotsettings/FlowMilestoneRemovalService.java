package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneLnkCondition;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 */
public class FlowMilestoneRemovalService implements IDomain<FlowMilestoneRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowMilestoneRemovalServiceBean> execute(
			IServiceDto<FlowMilestoneRemovalServiceBean> serviceDto) {

		FlowMilestoneRemovalServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String mstoneId = paramBean.getParam().getSelectedMstoneId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(mstoneId), "SelectedMilestoneId is not specified");

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				MstoneCondition condition = new MstoneCondition();
				condition.setMstoneId( mstoneId );

				String milestoneName = "";
				if ( 0 < support.getMstoneDao().count(condition.getCondition()) ) {
					IMstoneEntity mstoneEntity = support.getMstoneDao().findByPrimaryKey( condition.getCondition() );
					milestoneName = mstoneEntity.getMstoneNm();
					this.removeMilestone( mstoneId , mstoneEntity.getLotId() );
				}

				paramBean.getResult().setCompleted(true);
				if(TriStringUtils.isNotEmpty(milestoneName)){
					paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003063I,milestoneName);
				}
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void removeMilestone(String mstoneId , String lotId) {
		IMstoneEntity mstoneEntity = new MstoneEntity();
		mstoneEntity.setMstoneId(mstoneId);
		mstoneEntity.setDelStsId(StatusFlg.on);
		support.getMstoneDao().update(mstoneEntity);

		MstoneLnkCondition mstoneLnkCondition = new MstoneLnkCondition();
		mstoneLnkCondition.setMstoneId(mstoneId);
		List<IMstoneLnkEntity> mstoneLnkEnties = support.getMstoneLnkDao().find(mstoneLnkCondition.getCondition());
		if( mstoneLnkEnties != null ) {
			for (IMstoneLnkEntity mstoneLnkEntity : mstoneLnkEnties) {
				mstoneLnkEntity.setDelStsId(StatusFlg.on);
			}
			support.getMstoneLnkDao().update(mstoneLnkEnties);
		}
		
		if ( DesignSheetUtils.isRecord() ) {
			mstoneEntity.setLotId(lotId);
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Remove.getStatusId());
			support.getMstoneHistDao().insert( histEntity , mstoneEntity);
		}
	}

}
