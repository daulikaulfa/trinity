package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowSearchFilterServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<SearchFilterView> filterViews = new ArrayList<SearchFilterView>();
	private IPageNoInfo page = new PageNoInfo();

	@Override
	public RequestParam getParam() {
		return param;
	}

	public List<SearchFilterView> getFilterViews() {
		return filterViews;
	}
	public FlowSearchFilterServiceBean setFilterViews(List<SearchFilterView> filterViews) {
		this.filterViews = filterViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowSearchFilterServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 8;
		private String selectedUserId = null;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}

		public String getSelectedUserId() {
			return selectedUserId;
		}
		public RequestParam setSelectedUserId(String selectedUserId){
			this.selectedUserId = selectedUserId;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {
		private String keyword = null;
		private Integer selectedPageNo = 1;

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
	}

	public class SearchFilterView {
		private String filterId = null;
		private String filterNm = null;
		private String filterData = null;
		private String serviceId = null;
		private String lotId = null;

		public String getFilterId() {
			return filterId;
		}

		public SearchFilterView setFilterId(String filterId) {
			this.filterId = filterId;
			return this;
		}

		public String getFilterNm() {
			return filterNm;
		}

		public SearchFilterView setFilterNm(String filterNm) {
			this.filterNm = filterNm;
			return this;
		}

		public String getFilterData() {
			return filterData;
		}

		public SearchFilterView setFilterData(String filterData) {
			this.filterData = filterData;
			return this;
		}

		public String getServiceId() {
			return serviceId;
		}

		public SearchFilterView setServiceId(String serviceId) {
			this.serviceId = serviceId;
			return this;
		}

		public String getLotId() {
			return lotId;
		}

		public SearchFilterView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

	}
}
