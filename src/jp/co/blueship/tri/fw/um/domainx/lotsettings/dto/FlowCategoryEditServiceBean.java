package jp.co.blueship.tri.fw.um.domainx.lotsettings.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowCategoryEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private String lotId = null;
		private String categoryId = null;
		private CategoryEditInputBean inputInfo = new CategoryEditInputBean();

		public String getSelectedLotId() {
			return lotId;
		}
		public RequestParam setSelectedLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public CategoryEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(CategoryEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

		public String getSelectedCategoryId() {
			return categoryId;
		}
		public RequestParam setSelectedCategoryId(String selectedCategoryId) {
			this.categoryId = selectedCategoryId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
