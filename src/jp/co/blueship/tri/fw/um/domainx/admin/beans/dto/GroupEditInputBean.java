package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class GroupEditInputBean {
	private String groupNm;
	private String excludeUserId = null;
	private Set<String> roleIdSet = new LinkedHashSet<String>();
	private Set<String> userIdSet = new LinkedHashSet<String>();

	private List<GroupRoleLinkViewBean> roleViews = new ArrayList<GroupRoleLinkViewBean>();
	private List<GroupUserLinkViewBean> userViews = new ArrayList<GroupUserLinkViewBean>();

	public String getGroupNm() {
		return groupNm;
	}
	public GroupEditInputBean setGroupNm(String groupNm) {
		this.groupNm = groupNm;
		return this;
	}
	
	public String getExcludeUserId() {
		return excludeUserId;
	}

	public GroupEditInputBean setExcludeUserId(String excludeUserId) {
		this.excludeUserId = excludeUserId;
		return this;
	}

	public Set<String> getRoleIdSet() {
		return roleIdSet;
	}
	public GroupEditInputBean setRoleIdSet(Set<String> userIdSet) {
		this.roleIdSet = userIdSet;
		return this;
	}
	public GroupEditInputBean setRoleIds(String... roleIds) {
		roleIdSet.clear();
		roleIdSet.addAll( FluentList.from(roleIds).asList() );
		return this;
	}

	public Set<String> getUserIdSet() {
		return userIdSet;
	}
	public GroupEditInputBean setUserIdSet(Set<String> userIdSet) {
		this.userIdSet = userIdSet;
		return this;
	}
	public GroupEditInputBean setUserIds(String... userIds) {
		userIdSet.clear();
		userIdSet.addAll( FluentList.from(userIds).asList() );
		return this;
	}

	public List<GroupRoleLinkViewBean> getRoleViews() {
		return roleViews;
	}
	public GroupEditInputBean setRoleViews(List<GroupRoleLinkViewBean> roleViews) {
		this.roleViews = roleViews;
		return this;
	}

	public List<GroupUserLinkViewBean> getUserViews() {
		return userViews;
	}
	public GroupEditInputBean setUserViews(List<GroupUserLinkViewBean> userViews) {
		this.userViews = userViews;
		return this;
	}
}
