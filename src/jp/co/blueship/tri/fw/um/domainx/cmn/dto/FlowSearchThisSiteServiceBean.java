package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.annotations.Expose;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ISearchFilter;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowSearchThisSiteServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
	private IPageNoInfo page = new PageNoInfo();

	public RequestParam getParam() {
		return param;
	}

	public List<SearchThisSiteView> getSearchSiteViews() {
		return searchSiteViews;
	}
	public FlowSearchThisSiteServiceBean setSearchSiteViews(List<SearchThisSiteView> searchSiteViews) {
		this.searchSiteViews = searchSiteViews;
		return this;
	}

	public IPageNoInfo getPage() {
		return page;
	}
	public FlowSearchThisSiteServiceBean setPage(IPageNoInfo page) {
		this.page = page;
		return this;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private SearchServiceType searchServiceType = SearchServiceType.none;
		private int linesPerPage = 20;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public SearchServiceType getSearchServiceType() {
			return searchServiceType;
		}

		public RequestParam setSearchServiceType(SearchServiceType searchServiceType) {
			this.searchServiceType = searchServiceType;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition implements ISearchFilter {
		@Expose private String[] lotIds;
		@Expose private String mstoneStartDate;
		@Expose private String mstoneDueDate;
		private DataAttribute dataAttribute;
		@Expose private String dataNm;
		@Expose private String[] statusIds;
		@Expose private String[] assigneeIds;
		@Expose private String[] groupIds;
		@Expose private String[] ctgIds;
		@Expose private String[] mstoneIds;
		@Expose private boolean attachedFile = false;
		@Expose private String keyword;
		private Integer selectedPageNo = 1;

		private List<ItemLabelsBean> lotViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> dataViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> statusViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> assigneeViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> ctgViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> mstoneViews = new ArrayList<ItemLabelsBean>();

		public String[] getLotIds() {
			return lotIds;
		}
		public SearchCondition setLotIds(String... lotIds) {
			this.lotIds = lotIds;
			return this;
		}

		public String getMstoneStartDate() {
			return mstoneStartDate;
		}
		public SearchCondition setMstoneStartDate(String mstoneStartDate) {
			this.mstoneStartDate = mstoneStartDate;
			return this;
		}

		public String getMstoneDueDate() {
			return mstoneDueDate;
		}
		public SearchCondition setMstoneDueDate(String mstoneDueDate) {
			this.mstoneDueDate = mstoneDueDate;
			return this;
		}

		public String getDataNm() {
			return dataNm;
		}
		public SearchCondition setTableAttribute(DataAttribute attr) {
			this.dataAttribute = attr;
			dataNm = this.dataAttribute.value();
			return this;
		}

		public String[] getStatusIds() {
			return statusIds;
		}
		public SearchCondition setStatusIds(String[] statusIds) {
			this.statusIds = statusIds;
			return this;
		}

		public String[] getAssigneeIds() {
			return assigneeIds;
		}
		public SearchCondition setAssigneeIds(String[] assigneeIds) {
			this.assigneeIds = assigneeIds;
			return this;
		}

		public String[] getGroupIds() {
			return groupIds;
		}
		public SearchCondition setGroupIds(String[] groupIds) {
			this.groupIds = groupIds;
			return this;
		}

		public String[] getCtgIds() {
			return ctgIds;
		}
		public SearchCondition setCtgIds(String[] ctgIds) {
			this.ctgIds = ctgIds;
			return this;
		}

		public String[] getMstoneIds() {
			return mstoneIds;
		}
		public SearchCondition setMstoneIds(String[] mstoneIds) {
			this.mstoneIds = mstoneIds;
			return this;
		}

		public boolean isAttachedFile() {
			return attachedFile;
		}
		public SearchCondition setAttachedFile(boolean attachedFile) {
			this.attachedFile = attachedFile;
			return this;
		}

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}

		public List<ItemLabelsBean> getLotViews() {
			return lotViews;
		}
		public SearchCondition setLotViews(List<ItemLabelsBean> lotViews) {
			this.lotViews = lotViews;
			return this;
		}

		public List<ItemLabelsBean> getDataViews() {
			return dataViews;
		}
		public SearchCondition setDataViews(List<ItemLabelsBean> dataViews) {
			this.dataViews = dataViews;
			return this;
		}

		public List<ItemLabelsBean> getStatusViews() {
			return statusViews;
		}
		public SearchCondition setStatusViews(List<ItemLabelsBean> statusViews) {
			this.statusViews = statusViews;
			return this;
		}

		public List<ItemLabelsBean> getAssigneeViews() {
			return assigneeViews;
		}
		public SearchCondition setAssigneeViews(List<ItemLabelsBean> assigneeViews) {
			this.assigneeViews = assigneeViews;
			return this;
		}

		public List<ItemLabelsBean> getGroupViews() {
			return groupViews;
		}
		public SearchCondition setGroupViews(List<ItemLabelsBean> groupViews) {
			this.groupViews = groupViews;
			return this;
		}

		public List<ItemLabelsBean> getCtgViews() {
			return ctgViews;
		}
		public SearchCondition setCtgViews(List<ItemLabelsBean> ctgViews) {
			this.ctgViews = ctgViews;
			return this;
		}

		public List<ItemLabelsBean> getMstoneViews() {
			return mstoneViews;
		}
		public SearchCondition setMstoneViews(List<ItemLabelsBean> mstoneViews) {
			this.mstoneViews = mstoneViews;
			return this;
		}

		public Integer getSelectedPageNo() {
			return selectedPageNo;
		}
		public SearchCondition setSelectedPageNo(Integer selectedPageNo) {
			this.selectedPageNo = selectedPageNo;
			return this;
		}
	}

	public class SearchThisSiteView {
		private String lotId;
		private String lotNm;
		private DataAttribute dataAttribute;
		private String id;
		private String subject;
		private String stsId;
		private String status;
		private String assigneeNm;
		private String assigneeIconPath;
		private String assigneeGroup;
		private String latestUpdDate;
		private boolean attachedFile;

		public String getLotId() {
			return lotId;
		}
		public SearchThisSiteView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}
		public String getLotNm() {
			return lotNm;
		}
		public SearchThisSiteView setLotNm(String lotNm) {
			this.lotNm = lotNm;
			return this;
		}
		public DataAttribute getDataAttribute() {
			return dataAttribute;
		}
		public SearchThisSiteView setDataAttribute(DataAttribute dataAttribute) {
			this.dataAttribute = dataAttribute;
			return this;
		}
		public String getId() {
			return id;
		}
		public SearchThisSiteView setId(String id) {
			this.id = id;
			return this;
		}
		public String getSubject() {
			return subject;
		}
		public SearchThisSiteView setSubject(String subject) {
			this.subject = subject;
			return this;
		}
		public String getStsId() {
			return stsId;
		}
		public SearchThisSiteView setStsId(String stsId) {
			this.stsId = stsId;
			return this;
		}
		public String getStatus() {
			return status;
		}
		public SearchThisSiteView setStatus(String status) {
			this.status = status;
			return this;
		}
		public String getAssigneeNm() {
			return assigneeNm;
		}
		public SearchThisSiteView setAssigneeNm(String assigneeNm) {
			this.assigneeNm = assigneeNm;
			return this;
		}
		public String getAssigneeIconPath() {
			return assigneeIconPath;
		}
		public SearchThisSiteView setAssigneeIconPath(String assigneeIconPath) {
			this.assigneeIconPath = assigneeIconPath;
			return this;
		}
		public String getAssigneeGroup() {
			return assigneeGroup;
		}
		public SearchThisSiteView setAssigneeGroup(String assigneeGroup) {
			this.assigneeGroup = assigneeGroup;
			return this;
		}
		public String getLatestUpdDate() {
			return latestUpdDate;
		}
		public SearchThisSiteView setLatestUpdDate(String latestUpdDate) {
			this.latestUpdDate = latestUpdDate;
			return this;
		}
		public boolean isAttachedFile() {
			return attachedFile;
		}
		public SearchThisSiteView setAttachedFile(boolean attachedFile) {
			this.attachedFile = attachedFile;
			return this;
		}
	}

	public enum SearchServiceType {
		none( "" ),
		SearchSite( "SearchSite" ),
		SearchLot( "SearchLot" ),
		;

		private String value = null;

		private SearchServiceType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			SearchServiceType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static SearchServiceType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( SearchServiceType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

}
