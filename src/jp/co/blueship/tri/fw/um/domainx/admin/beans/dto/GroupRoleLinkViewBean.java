package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class GroupRoleLinkViewBean {
	private String roleId;
	private String roleNm;
	private int count = 0;

	public String getRoleId() {
		return roleId;
	}
	public GroupRoleLinkViewBean setRoleId(String roleId) {
		this.roleId = roleId;
		return this;
	}

	public String getRoleNm() {
		return roleNm;
	}
	public GroupRoleLinkViewBean setRoleNm(String roleNm) {
		this.roleNm = roleNm;
		return this;
	}

	public int getCount() {
		return count;
	}
	public GroupRoleLinkViewBean setCount(int count) {
		this.count = count;
		return this;
	}

}
