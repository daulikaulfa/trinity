package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmRecentUpdatesUtils;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiHistItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiPageListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiTagListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiHistoryListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiHistoryListServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiHistoryListServiceBean.WikiVersionView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiHistoryListService implements IDomain<FlowWikiHistoryListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	private WikiPageListService wikiPageListService = null ;
	private WikiTagListService wikiTagListService = null ;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	public void setWikiPageListService( WikiPageListService wikiPageListService ){
		this.wikiPageListService = wikiPageListService;
	}
	public void setWikiTagListService( WikiTagListService wikiTagListService ){
		this.wikiTagListService = wikiTagListService;
	}


	@Override
	public IServiceDto<FlowWikiHistoryListServiceBean> execute(IServiceDto<FlowWikiHistoryListServiceBean> serviceDto) {

		FlowWikiHistoryListServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String wikiId = paramBean.getParam().getSelectedWikiId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiHistoryListServiceBean paramBean ){
		if( RequestOption.none.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);

		else
			this.onChange(paramBean);
	}


	private void onChange( FlowWikiHistoryListServiceBean paramBean ){

		String wikiId = paramBean.getParam().getSelectedWikiId();

		if( RequestOption.All.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);

		if( RequestOption.WikiPageList.equals( paramBean.getParam().getRequestOption() ) ){
			String searchPageNm = paramBean.getParam().getSearchPageNm();
			paramBean.setPageViews( this.wikiPageListService.getWikiPageListByWikiId(wikiId, searchPageNm) );
		}

		if( RequestOption.TagList.equals( paramBean.getParam().getRequestOption() ) )
			paramBean.setTagViews ( this.wikiTagListService.getWikiTagViewListByWikiId(wikiId) );

		if( RequestOption.WistoryListService.equals( paramBean.getParam().getRequestOption() ) )
			this.getWikiVersionView(paramBean);
	}


	private void requestAll(FlowWikiHistoryListServiceBean paramBean){
		String wikiId = paramBean.getParam().getSelectedWikiId();

		this.getWikiVersionView(paramBean);
		List<WikiPageViewBean> wikiPageList = this.wikiPageListService.getWikiPageListByWikiId(wikiId, paramBean.getParam().getSearchPageNm());
		List<WikiTagViewBean> wikiTagList = this.wikiTagListService.getWikiTagViewListByWikiId(wikiId);

		paramBean.setPageViews( wikiPageList )
				 .setTagViews ( wikiTagList )
				 ;
	}


	private void getWikiVersionView( FlowWikiHistoryListServiceBean paramBean ){

		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiCondition wikiCondition = new WikiCondition();
		wikiCondition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey(wikiCondition.getCondition());

		paramBean.setWikiNm( wikiEntity.getPageNm() );

		WikiHistCondition wikiHistCondition = new WikiHistCondition();
		wikiHistCondition.setWikiId(wikiId);

		ISqlSort sort = new SortBuilder();
		sort.setElement( WikiHistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IWikiHistEntity> histEntityList = this.umSupport.getWikiHistDao().find(wikiHistCondition.getCondition(), sort);
		List<WikiVersionView> views = new ArrayList<WikiVersionView>();

		for(IWikiHistEntity histEntity : histEntityList){
			WikiVersionView view = paramBean.new WikiVersionView()
					.setWikiVerNo		( histEntity.getWikiVerNo() )
					.setUpdUserNm		( histEntity.getRegUserNm() )
					.setUpdUserId		( histEntity.getRegUserId() )
					.setUpdUserIconPath	( this.umSupport.getIconPath(histEntity.getRegUserId()) )
					.setUpdDate			( UmRecentUpdatesUtils.stringOf( paramBean.getLanguage(),
																		histEntity.getRegTimestamp() ))
					;
			views.add(view);
		}
		paramBean.setVersionViews(views);
	}

}
