package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.wiki.constants.WikiHistItems;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistCondition;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiPageListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.WikiTagListService;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiVersionDetailsServiceBean.WikiVersionView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiVersionDetailsService implements IDomain<FlowWikiVersionDetailsServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	private WikiPageListService wikiPageListService = null ;
	private WikiTagListService wikiTagListService = null ;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	public void setWikiPageListService( WikiPageListService wikiPageListService ){
		this.wikiPageListService = wikiPageListService;
	}
	public void setWikiTagListService( WikiTagListService wikiTagListService ){
		this.wikiTagListService = wikiTagListService;
	}


	@Override
	public IServiceDto<FlowWikiVersionDetailsServiceBean> execute(
			IServiceDto<FlowWikiVersionDetailsServiceBean> serviceDto) {

		FlowWikiVersionDetailsServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String wikiId = paramBean.getParam().getSelectedWikiId();
			Integer wikiVerNo = paramBean.getParam().getWikiVerNo();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "SelectedWikiId is not specified");
			PreConditions.assertOf((null != wikiVerNo), "SelectedWikiVerNo is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiVersionDetailsServiceBean paramBean ){

		if( RequestOption.none.equals( paramBean.getParam().getRequestOption() ) )
			this.requestAll(paramBean);
		else
			this.onChange(paramBean);
	}


	private void onChange( FlowWikiVersionDetailsServiceBean paramBean ){

		RequestOption requestOption = paramBean.getParam().getRequestOption();
		String wikiId = paramBean.getParam().getSelectedWikiId();

		if( RequestOption.All.equals( requestOption ) )
			this.requestAll(paramBean);

		if( RequestOption.WikiPageList.equals( requestOption ) ){
			String searchPageNm = paramBean.getParam().getSearchPageNm();
			paramBean.setPageViews( this.wikiPageListService.getWikiPageListByWikiId(wikiId, searchPageNm) );
		}

		if( RequestOption.TagList.equals( requestOption ) )
			paramBean.setTagViews ( this.wikiTagListService.getWikiTagViewListByWikiId(wikiId) );

		if( RequestOption.WikiVersionDetails.equals( requestOption ) ){
			this.getVersionDetailsView(paramBean);
			this.getWikiVersionViews(paramBean);
		}

	}


	private void requestAll( FlowWikiVersionDetailsServiceBean paramBean ){

		String wikiId = paramBean.getParam().getSelectedWikiId();

		this.getVersionDetailsView(paramBean);
		this.getWikiVersionViews(paramBean);

		List<WikiPageViewBean> wikiPageList = this.wikiPageListService.getWikiPageListByWikiId(wikiId, paramBean.getParam().getSearchPageNm());
		List<WikiTagViewBean> wikiTagList = this.wikiTagListService.getWikiTagViewListByWikiId(wikiId);

		paramBean.setPageViews( wikiPageList )
				 .setTagViews ( wikiTagList )
				 ;
	}


	private void getVersionDetailsView( FlowWikiVersionDetailsServiceBean paramBean ){
		String wikiId = paramBean.getParam().getSelectedWikiId();
		Integer wikiVerNo = paramBean.getParam().getWikiVerNo();

		WikiHistCondition histCondition = new WikiHistCondition();
		histCondition.setWikiId(wikiId);
		histCondition.setWikiVerNo(wikiVerNo);

		IWikiHistEntity entity = this.umSupport.getWikiHistDao().findByPrimaryKey(histCondition.getCondition());

		if( entity == null ) return;

		paramBean.setVersionDetailsView( this.getWikiVersionView(paramBean, entity) );
	}


	private void getWikiVersionViews( FlowWikiVersionDetailsServiceBean paramBean ){
		String wikiId = paramBean.getParam().getSelectedWikiId();

		WikiHistCondition histCondition = new WikiHistCondition();
		histCondition.setWikiId(wikiId);

		ISqlSort sort = new SortBuilder();
		sort.setElement( WikiHistItems.regTimestamp, TriSortOrder.Desc, 1 );

		List<IWikiHistEntity> entityList = this.umSupport.getWikiHistDao().find( histCondition.getCondition(), sort );
		List<WikiVersionView> views = new ArrayList<WikiVersionView>();

		for( IWikiHistEntity entity : entityList )
			views.add( this.getWikiVersionView(paramBean, entity) );

		paramBean.setWikiVersionView(views);
	}

	private WikiVersionView getWikiVersionView( FlowWikiVersionDetailsServiceBean paramBean, IWikiHistEntity entity ){
		WikiVersionView view = paramBean.new WikiVersionView()
				.setPageNm		( entity.getPageNm() )
				.setWikiVerNo	( entity.getWikiVerNo() )
				.setContent		( entity.getContent() )
				.setUpdUserNm	( entity.getRegUserNm() )
				.setUpdUserNm	( entity.getRegUserId() )
				.setUpdDate		( TriDateUtils.convertViewDateFormat( entity.getRegTimestamp(),
										TriDateUtils.getYMDHMDateFormat( paramBean.getLanguage(),
																		 paramBean.getTimeZone())) )
				;
		return view;
	}
}
