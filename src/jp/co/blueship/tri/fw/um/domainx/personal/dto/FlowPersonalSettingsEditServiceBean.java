package jp.co.blueship.tri.fw.um.domainx.personal.dto;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.constants.GenderType;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.constants.LookAndFeel;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.02.00
 * @author Chung
 *
 */
public class FlowPersonalSettingsEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private PersonalSettingsDetailsView detailsView = new PersonalSettingsDetailsView();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowPersonalSettingsEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public PersonalSettingsDetailsView getDetailsView() {
		return detailsView;
	}
	public FlowPersonalSettingsEditServiceBean setDetailsView(PersonalSettingsDetailsView detailsView) {
		this.detailsView = detailsView;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private Set<String> requestOptions = new HashSet<String>();
		private UserEditInputBean userInputInfo = new UserEditInputBean();
		private PasswordEditInputBean passwordInputInfo = new PasswordEditInputBean();
		private LanguageAndTimezoneEditInputBean languageTimezoneInputInfo = new LanguageAndTimezoneEditInputBean();
		private LookAndFeelEditInputBean lookAndFeelInputInfo = new LookAndFeelEditInputBean();

		public boolean isRequestOptionOf( RequestOption option ) {
			if ( null == option )
				return false;

			return requestOptions.contains(option.value);
		}
		public RequestParam setRequestOptions(RequestOption... requestOptions) {
			if ( null == requestOptions )
				return this;

			this.requestOptions = new HashSet<String>();
			for ( RequestOption option: requestOptions ) {
				this.requestOptions.add( option.value );
			}

			return this;
		}

		public UserEditInputBean getUserInputInfo() {
			return userInputInfo;
		}
		public RequestParam setUserInputInfo(UserEditInputBean userInputInfo) {
			this.userInputInfo = userInputInfo;
			return this;
		}

		public PasswordEditInputBean getPasswordInputInfo() {
			return passwordInputInfo;
		}
		public RequestParam setPasswordInputInfo(PasswordEditInputBean passwordInputInfo) {
			this.passwordInputInfo = passwordInputInfo;
			return this;
		}

		public LanguageAndTimezoneEditInputBean getLanguageTimezoneInputInfo() {
			return languageTimezoneInputInfo;
		}
		public RequestParam seLanguageTimezoneInputInfo(LanguageAndTimezoneEditInputBean languageTimezoneInputInfo) {
			this.languageTimezoneInputInfo = languageTimezoneInputInfo;
			return this;
		}

		public LookAndFeelEditInputBean getLookAndFeelInputInfo() {
			return lookAndFeelInputInfo;
		}
		public RequestParam setLookAndFeelInputInfo(LookAndFeelEditInputBean lookAndFeelInputInfo) {
			this.lookAndFeelInputInfo = lookAndFeelInputInfo;
			return this;
		}
	}

	/**
	 * This is the class of all PersonalSettings enumeration types.
	 */
	public enum RequestOption {
		UserChanges( "userChanges" ),
		PasswordChanges( "passwordChanges" ),
		LanguageAndTimezoneChanges( "languageAndTimezoneChanges" ),
		LookAndFeelChanges( "lookAndFeelChanges" ),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public String value() {
			return this.value;
		}
	}

	/**
	 * This is the user information to be used in the case of RequestOption = {@link RequestOption#UserChanges}
	 */
	public class UserEditInputBean {
		private String userNm;
		private String mailAddress;
		private List<ItemLabelsBean> iconViews = new ArrayList<ItemLabelsBean>();
		private IconSelectionOption iconOption = IconSelectionOption.DefaultImage;
		/**
		 * for default
		 */
		private String iconPath;
		/**
		 * for custom
		 */
		private String iconFilePath;
		private String iconFileNm;
		private byte[] iconInputStreamBytes;
		private GenderType genderType = GenderType.none;
		private String location;
		private String biography;
		private String[] url;

		public String getUserNm() {
			return userNm;
		}
		public UserEditInputBean setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}
		public String getMailAddress() {
			return mailAddress;
		}
		public UserEditInputBean setMailAddress(String mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}
		public List<ItemLabelsBean> getIconViews() {
			return iconViews;
		}
		public UserEditInputBean setIconViews(List<ItemLabelsBean> iconViews) {
			this.iconViews = iconViews;
			return this;
		}
		public IconSelectionOption getIconOption() {
			return iconOption;
		}
		public UserEditInputBean setIconOption(IconSelectionOption iconOption) {
			this.iconOption = iconOption;
			return this;
		}
		public String getIconPath() {
			return iconPath;
		}
		public UserEditInputBean setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}
		public String getIconFilePath() {
			return iconFilePath;
		}
		public UserEditInputBean setIconFilePath(String iconFilePath) {
			this.iconFilePath = iconFilePath;
			return this;
		}
		public String getIconFileNm() {
			return iconFileNm;
		}
		public UserEditInputBean setIconFileNm(String iconFileNm) {
			this.iconFileNm = iconFileNm;
			return this;
		}
		public byte[] getIconInputStreamBytes() {
			return iconInputStreamBytes;
		}
		public UserEditInputBean setIconInputStreamBytes(byte[] iconInputStreamBytes) {
			this.iconInputStreamBytes = iconInputStreamBytes;
			return this;
		}
		public GenderType getGenderType() {
			return genderType;
		}
		public UserEditInputBean setGenderType(GenderType genderType) {
			this.genderType = genderType;
			return this;
		}
		public String getLocation() {
			return location;
		}
		public UserEditInputBean setLocation(String location) {
			this.location = location;
			return this;
		}
		public String getBiography() {
			return biography;
		}
		public UserEditInputBean setBiography(String biography) {
			this.biography = biography;
			return this;
		}
		public String[] getUrl() {
			return url;
		}
		public UserEditInputBean setUrl(String... url) {
			this.url = url;
			return this;
		}
	}

	/**
	 * This is the password information to be used in the case of RequestOption = {@link RequestOption#PasswordChanges}
	 */
	public class PasswordEditInputBean {
		private String currentPassword;
		private String newPassword;
		private String confirmPassword;
		private String passwordExpiration;
		private List<ItemLabelsBean> passwordExpirationViews = new ArrayList<ItemLabelsBean>();

		public String getCurrentPassword() {
			return currentPassword;
		}
		public PasswordEditInputBean setCurrentPassword(String currentPassword) {
			this.currentPassword = currentPassword;
			return this;
		}

		public String getNewPassword() {
			return newPassword;
		}
		public PasswordEditInputBean setNewPassword(String newPassword) {
			this.newPassword = newPassword;
			return this;
		}

		public String getConfirmPassword() {
			return confirmPassword;
		}
		public PasswordEditInputBean setConfirmPassword(String confirmPassword) {
			this.confirmPassword = confirmPassword;
			return this;
		}

		public String getPasswordExpiration() {
			return passwordExpiration;
		}
		public PasswordEditInputBean setPasswordExpiration(String passwordExpiration) {
			this.passwordExpiration = passwordExpiration;
			return this;
		}

		public List<ItemLabelsBean> getPasswordExpirationViews() {
			return passwordExpirationViews;
		}
		public PasswordEditInputBean setPasswordExpirationViews(List<ItemLabelsBean> passwordExpirationViews) {
			this.passwordExpirationViews = passwordExpirationViews;
			return this;
		}

	}

	/**
	 * This is the language and timezone to be used in the case of RequestOption = {@link RequestOption#LanguageAndTimezoneChanges}
	 */
	public class LanguageAndTimezoneEditInputBean {
		private String language;
		private String timezone;
		private List<ItemLabelsBean> languageViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> timezoneViews = new ArrayList<ItemLabelsBean>();

		public String getLanguage() {
			return language;
		}
		public LanguageAndTimezoneEditInputBean setLanguage(String language) {
			this.language = language;
			return this;
		}

		public String getTimezone() {
			return timezone;
		}
		public LanguageAndTimezoneEditInputBean setTimezone(String timezone) {
			this.timezone = timezone;
			return this;
		}

		public List<ItemLabelsBean> getLanguageViews() {
			return languageViews;
		}
		public LanguageAndTimezoneEditInputBean setLanguageViews(List<ItemLabelsBean> languageViews) {
			this.languageViews = languageViews;
			return this;
		}

		public List<ItemLabelsBean> getTimezoneViews() {
			return timezoneViews;
		}
		public LanguageAndTimezoneEditInputBean setTimezoneViews(List<ItemLabelsBean> timezoneViews) {
			this.timezoneViews = timezoneViews;
			return this;
		}
	}

	/**
	 * This is the look and feel to be used in the case of RequestOption = {@link RequestOption#LookAndFeelChanges}
	 */
	public class LookAndFeelEditInputBean {
		private LookAndFeel lookAndFeel = LookAndFeel.none;
		private List<ItemLabelsBean> lookAndFeelViews = new ArrayList<ItemLabelsBean>();

		public LookAndFeel getLookAndFeel() {
			return lookAndFeel;
		}

		public LookAndFeelEditInputBean setLookAndFeel(LookAndFeel lookAndFeel) {
			this.lookAndFeel = lookAndFeel;
			return this;
		}

		public List<ItemLabelsBean> getLookAndFeelViews() {
			return lookAndFeelViews;
		}
		public LookAndFeelEditInputBean setLookAndFeelViews(List<ItemLabelsBean> lookAndFeelViews) {
			this.lookAndFeelViews = lookAndFeelViews;
			return this;
		}
	}

	/**
	 * Personal Settings Details View
	 */
	public class PersonalSettingsDetailsView {
		private String userId;
		private UserDetailsView user = new UserDetailsView();
		private PasswordDetailsView password = new PasswordDetailsView();
		private LanguageAndTimezoneDetailsView languageTimezone = new LanguageAndTimezoneDetailsView();
		private LookAndFeelDetailsView lookAndFeel = new LookAndFeelDetailsView();

		public String getUserId() {
			return userId;
		}
		public PersonalSettingsDetailsView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public UserDetailsView getUser() {
			return user;
		}
		public PersonalSettingsDetailsView setUser(UserDetailsView user) {
			this.user = user;
			return this;
		}

		public PasswordDetailsView getPassword() {
			return password;
		}
		public PersonalSettingsDetailsView setPassword(PasswordDetailsView password) {
			this.password = password;
			return this;
		}

		public LanguageAndTimezoneDetailsView getLanguageTimezone() {
			return languageTimezone;
		}
		public PersonalSettingsDetailsView setLanguageTimezone(LanguageAndTimezoneDetailsView languageTimezone) {
			this.languageTimezone = languageTimezone;
			return this;
		}

		public LookAndFeelDetailsView getLookAndFeel() {
			return lookAndFeel;
		}
		public PersonalSettingsDetailsView setLookAndFeel(LookAndFeelDetailsView lookAndFeel) {
			this.lookAndFeel = lookAndFeel;
			return this;
		}

	}

	/**
	 * User Details View
	 */
	public class UserDetailsView {
		private String defaultIconPath;
		private String customIconPath;

		public String getDefaultIconPath() {
			return defaultIconPath;
		}
		public UserDetailsView setDefaultIconPath(String defaultIconPath) {
			this.defaultIconPath = defaultIconPath;
			return this;
		}
		public String getCustomIconPath() {
			return customIconPath;
		}
		public UserDetailsView setCustomIconPath(String customIconPath) {
			this.customIconPath = customIconPath;
			return this;
		}
	}


	public class PasswordDetailsView extends jp.co.blueship.tri.fw.sm.domainx.account.beans.dto.PasswordDetailsView {
		private String expirationMsg = null;

		public String getExpirationMsg() {
			return expirationMsg;
		}

		public PasswordDetailsView setExpirationMsg(String expirationMsg) {
			this.expirationMsg = expirationMsg;
			return this;
		}

	}

	/**
	 * Language and Timezone View
	 */
	public class LanguageAndTimezoneDetailsView {
		private ItemLabelsBean defaultLanguage = new ItemLabelsBean();
		private ItemLabelsBean defaultTimeZone = new ItemLabelsBean();

		public ItemLabelsBean getDefaultLanguage() {
			return defaultLanguage;
		}
		public LanguageAndTimezoneDetailsView setDefaultLanguage(ItemLabelsBean defaultLanguage) {
			this.defaultLanguage = defaultLanguage;
			return this;
		}

		public ItemLabelsBean getDefaultTimeZone() {
			return defaultTimeZone;
		}
		public LanguageAndTimezoneDetailsView setDefaultTimeZone(ItemLabelsBean defaultTimeZone) {
			this.defaultTimeZone = defaultTimeZone;
			return this;
		}
	}

	/**
	 * Look And Feel View
	 */
	public class LookAndFeelDetailsView {
		private ItemLabelsBean defaultLookAndFeel = new ItemLabelsBean();

		public ItemLabelsBean getDefaultLookAndFeel() {
			return defaultLookAndFeel;
		}

		public LookAndFeelDetailsView setDefaultLookAndFeel(ItemLabelsBean defaultLookAndFeel) {
			this.defaultLookAndFeel = defaultLookAndFeel;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;
		private String userIconPath;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}

		public String getUserIconPath() {
			return userIconPath;
		}

		public RequestsCompletion setUserIconPath(String userIconPath) {
			this.userIconPath = userIconPath;
			return this;
		}
	}


}
