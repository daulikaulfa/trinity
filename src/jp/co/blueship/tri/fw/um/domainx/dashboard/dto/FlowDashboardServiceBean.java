package jp.co.blueship.tri.fw.um.domainx.dashboard.dto;

import jp.co.blueship.tri.am.domainx.checkinout.dto.FlowCheckInOutRequestListServiceBean;
import jp.co.blueship.tri.am.domainx.lot.dto.FlowDashboardServiceLotListBean;
import jp.co.blueship.tri.fw.cmn.utils.TriPropertyUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceNotificationToUserBean;
import jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto.FlowDashboardServiceRecentUpdatesBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version SP-201511-2_V3L14R01
 * @author Yukihiro Eguchi
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private FlowDashboardServiceLotListBean lotListBean;
	private FlowCheckInOutRequestListServiceBean checkInOutRequestListBean;
	private FlowDashboardServiceNotificationToUserBean notificationToUser;
	private FlowDashboardServiceRecentUpdatesBean recentUpdates;

	public RequestParam getParam() {
		return param;
	}

	public FlowDashboardServiceLotListBean getLotListBean() {
		return lotListBean;
	}

	public FlowDashboardServiceBean setLotListBean(FlowDashboardServiceLotListBean lotListBean) {
		this.lotListBean = lotListBean;
		return this;
	}

	public FlowCheckInOutRequestListServiceBean getCheckInOutRequestListBean() {
		return checkInOutRequestListBean;
	}

	public FlowDashboardServiceBean setCheckInOutRequestListBean(FlowCheckInOutRequestListServiceBean checkInOutRequestListBean) {
		if ( null != checkInOutRequestListBean ) {
			TriPropertyUtils.copyProperties(checkInOutRequestListBean, this);
		}
		this.checkInOutRequestListBean = checkInOutRequestListBean;
		return this;
	}

	public FlowDashboardServiceNotificationToUserBean getNotificationToUser() {
		return notificationToUser;
	}
	public FlowDashboardServiceBean setNotificationToUser(FlowDashboardServiceNotificationToUserBean notificationToUser) {
		this.notificationToUser = notificationToUser;
		return this;
	}

	public FlowDashboardServiceRecentUpdatesBean getRecentUpdates() {
		return recentUpdates;
	}
	public FlowDashboardServiceBean setRecentUpdates(FlowDashboardServiceRecentUpdatesBean recentUpdates) {
		this.recentUpdates = recentUpdates;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private RequestOption requestOption = RequestOption.none;

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none,
		All,
		Lots,
		CheckInOutRequest,
		NotificationToUser,
		RecentUpdates,
		;
	}

}
