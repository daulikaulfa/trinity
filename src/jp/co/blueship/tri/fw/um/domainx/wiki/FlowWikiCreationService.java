package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmWikiUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IViewWikiTagsEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.IWikiTagEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.ViewWikiTagsCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiHistEntity;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagCondition;
import jp.co.blueship.tri.fw.um.dao.wiki.eb.WikiTagEntity;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiCreationServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiCreationServiceBean.WikiCreationInputInfo;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 */
public class FlowWikiCreationService implements IDomain<FlowWikiCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;

	public void setUmSupport(IUmFinderSupport umSupport) {
		this.umSupport = umSupport;
	}

	@Override
	public IServiceDto<FlowWikiCreationServiceBean> execute(IServiceDto<FlowWikiCreationServiceBean> serviceDto) {
		FlowWikiCreationServiceBean paramBean = serviceDto.getServiceBean();

		try{

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			if(RequestType.submitChanges.equals(paramBean.getParam().getRequestType()))
				this.submitChanges(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void init( FlowWikiCreationServiceBean paramBean ){

		String lotId = paramBean.getParam().getSelectedLotId();
		paramBean.setTagViews( this.getWikiTags(lotId) );

		String copyWikiId = paramBean.getParam().getCopyWikiId();

		if( TriStringUtils.isNotEmpty( copyWikiId ) )
			this.copyInputInfo( paramBean );
	}


	private void submitChanges( FlowWikiCreationServiceBean paramBean ){
		paramBean.getResult().setCompleted(false);

		WikiCreationInputInfo inputInfo = paramBean.getParam().getInputInfo();

		String lotId = paramBean.getParam().getSelectedLotId();
		String content = inputInfo.getContent();
		String[] tags = UmWikiUtils.extractTags( inputInfo.getPageNm() );
		String pageNm = UmWikiUtils.formatPageName(inputInfo.getPageNm());

		UmWikiUtils.validateWiki(pageNm, content);

		String wikiId = this.insertWiki(lotId, pageNm, content);

		if( TriStringUtils.isEmpty( wikiId ) )
			return;

		this.insertWikiTag(tags, wikiId);
		this.insertWikiHist( wikiId );
		this.insertWikiUpdateHist(wikiId);

		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003066I,pageNm);
		paramBean.getResult().setCompleted	(true)
							 .setWikiId		(wikiId);
	}
	
	


	private String insertWiki( String lotId, String pageNm, String content ){
		String wikiId = this.umSupport.getWikiNumberingDao().nextval();

		IWikiEntity entity = new WikiEntity();
		entity.setWikiId( wikiId );
		entity.setLotId( lotId );
		entity.setPageNm(pageNm);
		entity.setContent(content);

		this.umSupport.getWikiDao().insert(entity);

		return wikiId;
	}


	private void insertWikiTag( String[] tags, String wikiId ){
		for( String tag : tags ){
			tag = StringUtils.removeStart(tag, "[");
			tag = StringUtils.removeEnd(tag, "]");

			WikiTagCondition tagCondition = new WikiTagCondition();
			tagCondition.setWikiId(wikiId);
			tagCondition.setTagNm(tag);

			IWikiTagEntity checkEntity = this.umSupport.getWikiTagDao().findByPrimaryKey( tagCondition.getCondition() );

			if(checkEntity != null)
				continue;

			IWikiTagEntity insertEntity = new WikiTagEntity();
			insertEntity.setWikiId( wikiId );
			insertEntity.setTagNm( tag );

			this.umSupport.getWikiTagDao().insert( insertEntity);
		}
	}


	private void insertWikiHist( String wikiId ){
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );

		IWikiHistEntity histEntity = new WikiHistEntity();
		histEntity.setWikiId(wikiId);
		histEntity.setWikiVerNo( wikiEntity.getWikiVerNo() );
		histEntity.setPageNm( wikiEntity.getPageNm() );
		histEntity.setContent( wikiEntity.getContent() );

		this.umSupport.getWikiHistDao().insert( histEntity );
	}

	private void insertWikiUpdateHist( String wikiId ){
		WikiCondition condition = new WikiCondition();
		condition.setWikiId(wikiId);

		IWikiEntity wikiEntity = this.umSupport.getWikiDao().findByPrimaryKey( condition.getCondition() );
		IHistEntity histEntity = new HistEntity();
		histEntity.setActSts(UmActStatusId.Add.getStatusId());
		this.umSupport.getWikiUpdateHistDao().insert( histEntity, wikiEntity );
	}


	private List<String> getWikiTags( String lotId ){
		ViewWikiTagsCondition condition = new ViewWikiTagsCondition();
		condition.setLotId(lotId);

		List<IViewWikiTagsEntity> entityList = this.umSupport.getViewWikiTagsDao().find(condition);
		List<String> tagList = new ArrayList<String>();

		for( IViewWikiTagsEntity entity : entityList)
			tagList.add(entity.getTagNm());

		return tagList;
	}

	private void copyInputInfo( FlowWikiCreationServiceBean paramBean ){
		WikiCreationInputInfo inputInfo = paramBean.getParam().getInputInfo();
		String copyWikiId = paramBean.getParam().getCopyWikiId();

		WikiCondition condition = new WikiCondition();
		condition.setWikiId( copyWikiId );
		IWikiEntity entity = this.umSupport.getWikiDao().findByPrimaryKey(condition.getCondition());

		if(entity == null) return;

		paramBean.getParam().setSelectedLotId( entity.getLotId() );

		WikiTagCondition tagCondition = new WikiTagCondition();
		tagCondition.setWikiId( entity.getWikiId());
		List<IWikiTagEntity> tagEntityList = this.umSupport.getWikiTagDao().find(tagCondition.getCondition());

		StringBuffer buf = new StringBuffer();

		for(IWikiTagEntity tagEntity : tagEntityList)
			buf.append("[" + tagEntity.getTagNm() + "]");

		buf.append( "Copy - " + entity.getPageNm() );

		inputInfo.setPageNm	( buf.toString() )
				 .setContent( entity.getContent() )
				 ;
	}
}
