package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentListServiceSortOrderEditBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowDepartmentListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<DepartmentView> departmentViews = new ArrayList<DepartmentView>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowDepartmentListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public List<DepartmentView> getDepartmentViews() {
		return departmentViews;
	}
	public FlowDepartmentListServiceBean setDepartmentViews(List<DepartmentView> departmentViews) {
		this.departmentViews = departmentViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private DepartmentListServiceSortOrderEditBean sortOrderEdit = new DepartmentListServiceSortOrderEditBean();

		public DepartmentListServiceSortOrderEditBean getSortOrderEdit() {
			return sortOrderEdit;
		}
		public RequestParam setSortOrderEdit(DepartmentListServiceSortOrderEditBean sortOrderEdit) {
			this.sortOrderEdit = sortOrderEdit;
			return this;
		}
	}

	/**
	 * Department View
	 */
	public class DepartmentView {
		private String deptId = null;
		private String deptNm = null;
		private int sortOrder = 0;

		public String getDeptId() {
			return deptId;
		}
		public DepartmentView setDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}

		public String getDeptNm() {
			return deptNm;
		}
		public DepartmentView setDeptNm(String deptNm) {
			this.deptNm = deptNm;
			return this;
		}

		public int getSortOrder() {
			return sortOrder;
		}
		public DepartmentView setSortOrder(int sortOrder) {
			this.sortOrder = sortOrder;
			return this;
		}

	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
