package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.dept.eb.DeptEntity;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.DepartmentListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowDepartmentListServiceBean.DepartmentView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowDepartmentListService implements IDomain<FlowDepartmentListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;


	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowDepartmentListServiceBean> execute(
			IServiceDto<FlowDepartmentListServiceBean> serviceDto) {
		FlowDepartmentListServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setDepartmentViews(this.getDepartmentViews(paramBean));
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if (null != paramBean.getParam().getSortOrderEdit()) {
					this.updateSortOrder( paramBean.getParam().getSortOrderEdit() );
					paramBean.setDepartmentViews(this.getDepartmentViews(paramBean));
					paramBean.getResult().setCompleted(true);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private List<DepartmentView> getDepartmentViews(FlowDepartmentListServiceBean paramBean) {
		List<IDeptEntity> deptEntities = support.findAllDept();

		List<DepartmentView> views = new ArrayList<DepartmentView>();
		for (IDeptEntity entity: deptEntities) {

			DepartmentView viewBean = paramBean.new DepartmentView()
				.setDeptId(entity.getDeptId())
				.setDeptNm(entity.getDeptNm())
				.setSortOrder(entity.getSortOdr());

			views.add(viewBean);
		}

		return views;
	}

	private void updateSortOrder(DepartmentListServiceSortOrderEditBean sortOrderBean) {
		String deptId = sortOrderBean.getDeptId();
		int newSortOrder = sortOrderBean.getNewSortOrder();
		int currentRow = 1;

		for ( IDeptEntity entity: support.findAllDept()) {

			if ( currentRow == newSortOrder ) {
				currentRow++;
			}

			if ( ! entity.getDeptId().equals(deptId) ) {
				if ( currentRow != entity.getSortOdr() ) {
					IDeptEntity updEntity = new DeptEntity();
					updEntity.setDeptId( entity.getDeptId() );
					updEntity.setSortOdr( currentRow );
					support.getDeptDao().update( updEntity );
				}
				currentRow++;
			}
		}
		IDeptEntity newEntity = new DeptEntity();
		newEntity.setDeptId( deptId );
		newEntity.setSortOdr( newSortOrder );
		support.getDeptDao().update( newEntity );

	}
}
