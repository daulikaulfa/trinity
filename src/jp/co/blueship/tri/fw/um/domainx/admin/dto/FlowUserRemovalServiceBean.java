package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserRemovalServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;
	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowUserRemovalServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public RequestParam getParam() {
		return param;
	}


	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String userId = null;

		public String getSelectedUserId() {
			return userId;
		}
		public RequestParam setSelectedUserId(String userId) {
			this.userId = userId;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}

		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}


}
