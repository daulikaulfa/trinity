package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowUserListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<UserView> userViews = new ArrayList<UserView>();

	public RequestParam getParam() {
		return param;
	}

	public List<UserView> getUserViews() {
		return userViews;
	}
	public FlowUserListServiceBean setUserViews(List<UserView> userViews) {
		this.userViews = userViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {

		private List<ItemLabelsBean> roleViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> departmentViews = new ArrayList<ItemLabelsBean>();

		private String roleId = null;
		private String groupId = null;
		private String deptId = null;
		private String keyword = null;

		public List<ItemLabelsBean> getRoleViews() {
			return roleViews;
		}
		public SearchCondition setRoleViews(List<ItemLabelsBean> roleViews) {
			this.roleViews = roleViews;
			return this;
		}
		public List<ItemLabelsBean> getGroupViews() {
			return groupViews;
		}
		public SearchCondition setGroupViews(List<ItemLabelsBean> groupViews) {
			this.groupViews = groupViews;
			return this;
		}
		public List<ItemLabelsBean> getDepartmentViews() {
			return departmentViews;
		}
		public SearchCondition setDepartmentViews(List<ItemLabelsBean> departmentViews) {
			this.departmentViews = departmentViews;
			return this;
		}
		public String getRoleId() {
			return roleId;
		}
		public SearchCondition setRoleId(String roleId) {
			this.roleId = roleId;
			return this;
		}
		public String getGroupId() {
			return groupId;
		}
		public SearchCondition setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}
		public String getDeptId() {
			return deptId;
		}
		public SearchCondition setDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}
		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder userNm = TriSortOrder.Asc;
		private TriSortOrder mailAddress;
		private TriSortOrder groupNm;
		private TriSortOrder createdDate;

		public TriSortOrder getUserNm() {
			return userNm;
		}
		public OrderBy setUserNm(TriSortOrder userNm) {
			this.userNm = userNm;
			return this;
		}
		public TriSortOrder getMailAddress() {
			return mailAddress;
		}
		public OrderBy setMailAddress(TriSortOrder mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}
		public TriSortOrder getGroupNm() {
			return groupNm;
		}
		public OrderBy setGroupNm(TriSortOrder groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public TriSortOrder getCreatedDate() {
			return createdDate;
		}
		public OrderBy setCreatedDate(TriSortOrder createdDate) {
			this.createdDate = createdDate;
			return this;
		}
	}

	/**
	 *
	 * User View
	 *
	 */
	public class UserView {

		private String userId = null;
		private String userNm = null;
		private String iconPath = null;
		private String mailAddress = null;
		private List<String> groupNm = null;
		private String language = null;
		private String timezone = null;
		private String lastLogInTime = null;
		private String createdDate = null;
		private String updDate = null;

		public String getUserId() {
			return userId;
		}
		public UserView setUserId(String userId) {
			this.userId = userId;
			return this;
		}
		public String getUserNm() {
			return userNm;
		}
		public UserView setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}
		public String getIconPath() {
			return iconPath;
		}
		public UserView setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}
		public String getMailAddress() {
			return mailAddress;
		}
		public UserView setMailAddress(String mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}
		public List<String> getGroupNm() {
			return groupNm;
		}
		public UserView setGroupNm(List<String> groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public String getLanguage() {
			return language;
		}
		public UserView setLanguage(String language) {
			this.language = language;
			return this;
		}
		public String getTimezone() {
			return timezone;
		}
		public UserView setTimezone(String timezone) {
			this.timezone = timezone;
			return this;
		}
		public String getLastLogInTime() {
			return lastLogInTime;
		}
		public UserView setLastLogInTime(String lastLogInTime) {
			this.lastLogInTime = lastLogInTime;
			return this;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public UserView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		public String getUpdDate() {
			return updDate;
		}
		public UserView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}
	}
}
