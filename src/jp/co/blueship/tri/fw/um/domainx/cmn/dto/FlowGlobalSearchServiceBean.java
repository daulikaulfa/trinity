package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGlobalSearchServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<RecentlyViewedRequestView> requestViews = new ArrayList<RecentlyViewedRequestView>();// am_areq: checkout, checkin, removal
	private List<RecentlyViewedReleaseView> releaseViews = new ArrayList<RecentlyViewedReleaseView>();//
	private List<RecentlyViewedMemberView> memberViews = new ArrayList<RecentlyViewedMemberView>(); // Users full access
	private List<RecentlyViewedWikiView> wikiViews = new ArrayList<RecentlyViewedWikiView>();

	public RequestParam getParam() {
		return param;
	}

	public List<RecentlyViewedRequestView> getRequestViews() {
		return requestViews;
	}
	public FlowGlobalSearchServiceBean setRequestViews(List<RecentlyViewedRequestView> requestViews) {
		this.requestViews = requestViews;
		return this;
	}

	public List<RecentlyViewedReleaseView> getReleaseViews() {
		return releaseViews;
	}
	public FlowGlobalSearchServiceBean setReleaseViews(List<RecentlyViewedReleaseView> releaseViews) {
		this.releaseViews = releaseViews;
		return this;
	}

	public List<RecentlyViewedMemberView> getMemberViews() {
		return memberViews;
	}
	public FlowGlobalSearchServiceBean setMemberViews(List<RecentlyViewedMemberView> memberViews) {
		this.memberViews = memberViews;
		return this;
	}

	public List<RecentlyViewedWikiView> getWikiViews() {
		return wikiViews;
	}
	public FlowGlobalSearchServiceBean setWikiViews(List<RecentlyViewedWikiView> wikiViews) {
		this.wikiViews = wikiViews;
		return this;
	}

	/**
	 *
	 * Request Param
	 *
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private int linesPerPage = 10;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {
		private String keyword;

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	public class RecentlyViewedRequestView {
		private AreqCtgCd areqCtgCd = null;
		private String areqId;
		private String pjtId;
		private String referenceId;
		private String subject;
		private String checkinFilePath;
		private String checkoutFilePath;
		private boolean isShowClipboardIcon = false;

		public boolean isShowClipboardIcon() {
			return isShowClipboardIcon;
		}
		public RecentlyViewedRequestView setShowClipboardIcon(boolean isShowClipboardIcon) {
			this.isShowClipboardIcon = isShowClipboardIcon;
			return this;
		}
		
		public AreqCtgCd getAreqCtgCd() {
			return areqCtgCd;
		}
		public RecentlyViewedRequestView setAreqCtgCd(AreqCtgCd areqCtgCd) {
			this.areqCtgCd = areqCtgCd;
			return this;
		}

		public String getAreqId() {
			return areqId;
		}
		public RecentlyViewedRequestView setAreqId(String areqId) {
			this.areqId = areqId;
			return this;
		}

		public String getPjtId() {
			return pjtId;
		}
		public RecentlyViewedRequestView setPjtId(String pjtId) {
			this.pjtId = pjtId;
			return this;
		}

		public String getReferenceId() {
			return referenceId;
		}
		public RecentlyViewedRequestView setReferenceId(String referenceId) {
			this.referenceId = referenceId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public RecentlyViewedRequestView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public String getCheckinFilePath() {
			return checkinFilePath;
		}
		public RecentlyViewedRequestView setCheckinFilePath(String checkinFilePath) {
			this.checkinFilePath = checkinFilePath;
			return this;
		}

		public String getCheckoutFilePath() {
			return checkoutFilePath;
		}
		public RecentlyViewedRequestView setCheckoutFilePath(String checkoutFilePath) {
			this.checkoutFilePath = checkoutFilePath;
			return this;
		}
	}

	public class RecentlyViewedReleaseView {
		private String dataId = null;
		private String subject = null;
		private ReleaseViewType releaseType = ReleaseViewType.none;

		public String getInternalId() {
			return dataId;
		}
		public RecentlyViewedReleaseView setInternalId(String dataId) {
			this.dataId = dataId;
			return this;
		}

		public String getSubject() {
			return subject;
		}
		public RecentlyViewedReleaseView setSubject(String subject) {
			this.subject = subject;
			return this;
		}

		public ReleaseViewType getReleaseType() {
			return releaseType;
		}

		public RecentlyViewedReleaseView setReleaseType(ReleaseViewType releaseType) {
			this.releaseType = releaseType;
			return this;
		}
	}

	public class RecentlyViewedMemberView {
		private String userId = null;
		private String userNm = null;
		private String iconPath = null;

		public String getUserId() {
			return userId;
		}
		public RecentlyViewedMemberView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public String getUserNm() {
			return userNm;
		}
		public RecentlyViewedMemberView setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}

		public String getIconPath() {
			return iconPath;
		}
		public RecentlyViewedMemberView setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}
	}

	public class RecentlyViewedWikiView {
		private String lotId = null;
		private String wikiId = null;
		private String wikiNm = null;

		public String getLotId() {
			return lotId;
		}

		public RecentlyViewedWikiView setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public String getWikiId() {
			return wikiId;
		}

		public RecentlyViewedWikiView setWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public String getWikiNm() {
			return wikiNm;
		}

		public RecentlyViewedWikiView setWikiNm(String wikiNm) {
			this.wikiNm = wikiNm;
			return this;
		}
	}

//	public class FileView {
//		private String fileNm = null;
//		private String filePath = null;
//
//		public String getFileNm() {
//			return fileNm;
//		}
//
//		public FileView setFileNm(String fileNm) {
//			this.fileNm = fileNm;
//			return this;
//		}
//
//		public String getFilePath() {
//			return filePath;
//		}
//
//		public FileView setFilePath(String filePath) {
//			this.filePath = filePath;
//			return this;
//		}
//	}

	public enum RequestViewType {
		none( "" ),
		CheckoutRequest( "CheckoutRequest" ),
		CheckinRequest( "CheckinRequest" ),
		RemovalRequest( "RemovalRequest" ),
		;

		private String value = null;

		private RequestViewType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestViewType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestViewType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestViewType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}

	public enum ReleaseViewType {
		none( "" ),
		ReleaseRequest( "RA" ),
		ReleasePackage( "RP" ),
		BuildPackage( "BP" ),
		DeploymentJob( "DM" ),
		;

		private String value = null;

		private ReleaseViewType( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			ReleaseViewType type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static ReleaseViewType value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( ReleaseViewType type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
