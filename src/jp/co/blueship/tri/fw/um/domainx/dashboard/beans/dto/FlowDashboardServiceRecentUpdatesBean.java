package jp.co.blueship.tri.fw.um.domainx.dashboard.beans.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domain.dto.IServiceBean;
import jp.co.blueship.tri.fw.um.domainx.project.beans.dto.RecentUpdateViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowDashboardServiceRecentUpdatesBean implements IServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private List<RecentUpdateViewBean> recentUpdateViews = new ArrayList<RecentUpdateViewBean>();
	private boolean hasNext = false;

	public RequestParam getParam() {
		return param;
	}

	public List<RecentUpdateViewBean> getRecentUpdateViews() {
		return recentUpdateViews;
	}
	public FlowDashboardServiceRecentUpdatesBean setRecentUpdateViews( List<RecentUpdateViewBean> recentUpdateViews ) {
		this.recentUpdateViews = recentUpdateViews;
		return this;
	}

	public boolean hasNext() {
		return hasNext;
	}
	public FlowDashboardServiceRecentUpdatesBean setHasNext(boolean hasNext) {
		this.hasNext = hasNext;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam {
		private RequestOption requestOption = RequestOption.none;
		private int linesPerPage = 5;

		public RequestOption getRequestOption() {
			return requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption) {
			this.requestOption = requestOption;
			return this;
		}

		public int getLinesPerPage() {
			return linesPerPage;
		}
		public RequestParam setLinesPerPage(int linesPerPage) {
			this.linesPerPage = linesPerPage;
			return this;
		}
	}

	/**
	 * This is the class of enumeration types for 'onChange request'.
	 */
	public enum RequestOption {
		none( "" ),
		refresh("refresh"),
		more("more"),
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( ! this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) ) {
				return null;
			}

			for ( RequestOption type : values() ) {
				if ( type.value().equals( value ) ) {
					return type;
				}
			}

			return none;
		}
	}
}
