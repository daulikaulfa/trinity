package jp.co.blueship.tri.fw.um.domainx.wiki;

import java.io.File;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.domainx.wiki.dto.FlowWikiAttachmentDownloadServiceBean;

/**
 *
 * @version V4.00.00
 * @author Sharov.Maksym
 *
 */
public class FlowWikiAttachmentDownloadService implements IDomain<FlowWikiAttachmentDownloadServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	@Override
	public IServiceDto<FlowWikiAttachmentDownloadServiceBean> execute(
			IServiceDto<FlowWikiAttachmentDownloadServiceBean> serviceDto) {

		FlowWikiAttachmentDownloadServiceBean serviceBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(null != serviceBean, "ServiceBean is not specified");

			if (RequestType.init.equals(serviceBean.getParam().getRequestType())) {
				String wikiId = serviceBean.getParam().getSelectedWikiId();
				PreConditions.assertOf(TriStringUtils.isNotEmpty(wikiId), "Seleceted WikiId is not specified");
				this.init(serviceBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, serviceBean.getFlowAction());
		}
	}

	private void init(FlowWikiAttachmentDownloadServiceBean serviceBean) {
		String wikiId = serviceBean.getParam().getSelectedWikiId();
		String seqNo = serviceBean.getParam().getSeqNo();
		String selectedFileNm = serviceBean.getParam().getSelectedFileNm();

		File attachmentFilePath = UmDesignBusinessRuleUtils.getAttachmentFile(wikiId, seqNo, selectedFileNm);

		serviceBean.getFileResponse().setFile(attachmentFilePath);
		serviceBean.getResult().setCompleted(true);
	}
}
