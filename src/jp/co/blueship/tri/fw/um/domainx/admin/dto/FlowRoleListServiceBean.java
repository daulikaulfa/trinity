package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.RoleListServiceSortOrderEditBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowRoleListServiceBean  extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<RoleView> roleViews = new ArrayList<RoleView>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowRoleListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public List<RoleView> getRoleViews() {
		return roleViews;
	}
	public FlowRoleListServiceBean setRoleViews(List<RoleView> roleViews) {
		this.roleViews = roleViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private RoleListServiceSortOrderEditBean sortOrderEdit = new RoleListServiceSortOrderEditBean();

		public RoleListServiceSortOrderEditBean getSortOrderEdit() {
			return sortOrderEdit;
		}
		public RequestParam setSortOrderEdit(RoleListServiceSortOrderEditBean sortOrderEdit) {
			this.sortOrderEdit = sortOrderEdit;
			return this;
		}
	}

	/**
	 * Role View
	 */
	public class RoleView {
		private String roleId = null;
		private String roleNm = null;
		private int sortOrder = 0;

		public String getRoleId() {
			return roleId;
		}
		public RoleView setRoleId(String roleId) {
			this.roleId = roleId;
			return this;
		}

		public String getRoleNm() {
			return roleNm;
		}
		public RoleView setRoleNm(String roleNm) {
			this.roleNm = roleNm;
			return this;
		}

		public int getSortOrder() {
			return sortOrder;
		}
		public RoleView setSortOrder(int sortOrder) {
			this.sortOrder = sortOrder;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
