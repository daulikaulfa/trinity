package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupListServiceBean.GroupView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowGroupListService implements IDomain<FlowGroupListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;


	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}


	@Override
	public IServiceDto<FlowGroupListServiceBean> execute(
			IServiceDto<FlowGroupListServiceBean> serviceDto) {

		FlowGroupListServiceBean paramBean = serviceDto.getServiceBean();

		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setGroupViews(this.getGroupViews(paramBean));
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if (null != paramBean.getParam().getSortOrderEdit()) {
					this.updateSortOrder(paramBean.getParam().getSortOrderEdit());
					paramBean.setGroupViews(this.getGroupViews(paramBean));
					paramBean.getResult().setCompleted(true);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private List<GroupView> getGroupViews(FlowGroupListServiceBean paramBean) {
		List<IGrpEntity> grpEntities = support.findAllGroup();

		List<GroupView> views = new ArrayList<GroupView>();
		for (IGrpEntity entity : grpEntities) {
			GroupView viewBean = paramBean.new GroupView()
				.setGroupId(entity.getGrpId())
				.setGroupNm(entity.getGrpNm())
				.setSortOrder(entity.getSortOdr());

			views.add(viewBean);
		}

		return views;
	}

	private void updateSortOrder(GroupListServiceSortOrderEditBean sortOrderBean) {
		String grpId = sortOrderBean.getGroupId();
		int newSortOrder = sortOrderBean.getNewSortOrder();
		int currentRow = 1;

		for ( IGrpEntity entity: support.findAllGroup()) {

			if ( currentRow == newSortOrder ) {
				currentRow++;
			}

			if ( ! entity.getGrpId().equals(grpId) ) {
				if ( currentRow != entity.getSortOdr() ) {
					IGrpEntity updEntity = new GrpEntity();
					updEntity.setGrpId( entity.getGrpId() );
					updEntity.setSortOdr( currentRow );
					support.getGrpDao().update( updEntity );
				}
				currentRow++;
			}
		}
		IGrpEntity newEntity = new GrpEntity();
		newEntity.setGrpId( grpId );
		newEntity.setSortOdr( newSortOrder );
		support.getGrpDao().update( newEntity );
	}
}
