package jp.co.blueship.tri.fw.um.domainx.dashboard;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.svc.ServiceId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.security.SecurityService;
import jp.co.blueship.tri.fw.um.domainx.dashboard.dto.FlowV3DashboardServiceBean;

/**
*
* @version V4.02.00
* @author Chung
*/
public class FlowV3DashboardService implements IDomain<FlowV3DashboardServiceBean>{
	private static final ILog log = TriLogFactory.getInstance();

	private SecurityService securityService;

	public void setSecurityService(SecurityService securityService) {
		this.securityService = securityService;
	}

	@Override
	public IServiceDto<FlowV3DashboardServiceBean> execute( IServiceDto<FlowV3DashboardServiceBean> serviceDto ) {

		FlowV3DashboardServiceBean paramBean = serviceDto.getServiceBean();

		try {
			if ( RequestType.init.equals(paramBean.getParam().getRequestType()) ) {
				this.init(paramBean);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(SmMessageId.SM005215S, e, paramBean.getFlowAction());
		} finally {
		}
	}

	private void init( FlowV3DashboardServiceBean paramBean ) {
		boolean userInAdminSettingsRole
					= this.securityService.isValidAccess( paramBean.getUserId(), ServiceId.UmProjectSettingsEditService.value() );

		paramBean.getDetailsView().setUserInAdminSettingsRole( userInAdminSettingsRole );
	}
}
