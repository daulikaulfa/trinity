package jp.co.blueship.tri.fw.um.domainx.project.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowMemberListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private List<MemberView> memberViews = new ArrayList<MemberView>();

	public RequestParam getParam() {
		return param;
	}

	public List<MemberView> getMemberViews() {
		return memberViews;
	}

	public FlowMemberListServiceBean setMemberViews(List<MemberView> memberViews) {
		this.memberViews = memberViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {

		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {

		private String keyword;

		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder logInUser = TriSortOrder.Desc;
		private TriSortOrder userNm = TriSortOrder.Asc;

		public TriSortOrder getLogInUser() {
			return logInUser;
		}
		public OrderBy setLogInUser(TriSortOrder logInUser) {
			this.logInUser = logInUser;
			return this;
		}

		public TriSortOrder getUserNm() {
			return userNm;
		}
		public OrderBy setUserNm(TriSortOrder userNm) {
			this.userNm = userNm;
			return this;
		}
	}

	public class MemberView {
		private String userId;
		private String userNm;
		private String lastLogInTime;
		private String currentTime;
		private String iconPath;

		public String getUserId() {
			return userId;
		}
		public MemberView setUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public String getUserNm() {
			return userNm;
		}
		public MemberView setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}

		public String getLastLogInTime() {
			return lastLogInTime;
		}
		public MemberView setLastLogInTime(String lastLogInTime) {
			this.lastLogInTime = lastLogInTime;
			return this;
		}


		public String getCurrentTime() {
			return currentTime;
		}
		public MemberView setCurrentTime(String currentTime) {
			this.currentTime = currentTime;
			return this;
		}

		public String getIconPath() {
			return iconPath;
		}
		public MemberView setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}

	}
}
