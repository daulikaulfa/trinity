package jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto;

/**
 *
 * @version V4.00.00
 * @author sam
 */
public class CategoryEditInputBean {
	private String subject;

	public String getSubject() {
		return subject;
	}

	public CategoryEditInputBean setSubject(String subject) {
		this.subject = subject;
		return this;
	}
}
