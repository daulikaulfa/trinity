package jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto;

/**
 *
 * @version V4.00.00
 * @author Hai Thach
 *
 */
public class MilestoneListServiceSortOrderEditBean {
	private String mstoneId = null;
	private int newSortOrder = 0;

	public String getMstoneId() {
		return mstoneId;
	}
	public MilestoneListServiceSortOrderEditBean setMstoneId(String mstoneId) {
		this.mstoneId = mstoneId;
		return this;
	}

	public int getNewSortOrder() {
		return newSortOrder;
	}
	public MilestoneListServiceSortOrderEditBean setNewSortOrder(int newSortOrder) {
		this.newSortOrder = newSortOrder;
		return this;
	}
}
