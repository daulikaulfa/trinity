package jp.co.blueship.tri.fw.um.domainx.admin;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowGroupRemovalServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowGroupRemovalService implements IDomain<FlowGroupRemovalServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowGroupRemovalServiceBean> execute(
			IServiceDto<FlowGroupRemovalServiceBean> serviceDto) {
		FlowGroupRemovalServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String grpId = paramBean.getParam().getSelectedGroupId();
			PreConditions.assertOf(TriStringUtils.isNotEmpty(grpId), "SelectedGrpId is not specified");
			paramBean.getResult().setCompleted(false);

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				if( UmItemChkUtils.ADMIN_GROUP_ID.equals(grpId) ) {
					throw new ContinuableBusinessException(UmMessageId.UM001106E);
				}
				GrpCondition condition = new GrpCondition();
				condition.setGrpId(grpId);
				if (0 < support.getGrpDao().count(condition.getCondition())) {
					this.removeGroup(paramBean, grpId);
				}
			}
			return serviceDto;
		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void removeGroup(FlowGroupRemovalServiceBean paramBean, String grpId) {
		// Get grpEntity
		IGrpEntity grpEntity = support.findGroupById(grpId);

		// Delete records in UM_GRP_ROLE_LNK
		support.cleaningGrpRoleLnk(grpId, null);

		// Delete records in UM_USER_ROLE_LNK
		List<IGrpUserLnkEntity> grpUserLnkEntityList = support.findGrpUserLnkByGrpId( grpId );
		List<String> userList = FluentList.from(grpUserLnkEntityList).map(UmFluentFunctionUtils.toUserIdFromGrpUserLnkEntity).asList();
		support.cleaningAndUpdateUserRoleLnk(userList.toArray(new String[0]));

		// Delete records in UM_GRP_USER_LNK
		support.cleaningGrpUserLnk(grpId, null);

		// Delete records UM_GRP
		deleteGrp(grpId);

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable( UmMessageId.UM003078I, grpEntity.getGrpNm() );
	}

	private void deleteGrp( String grpId ) {
		GrpCondition grpCondition = new GrpCondition();
		grpCondition.setGrpId( grpId );
		support.getGrpDao().delete( grpCondition.getCondition() );
	}
}
