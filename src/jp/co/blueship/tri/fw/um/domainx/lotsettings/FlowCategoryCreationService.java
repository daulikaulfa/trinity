package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.ctg.constants.CtgItems;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryCreationService implements IDomain<FlowCategoryCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowCategoryCreationServiceBean> execute(
			IServiceDto<FlowCategoryCreationServiceBean> serviceDto) {

		FlowCategoryCreationServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String lotId = paramBean.getParam().getSelectedLotId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				CategoryEditInputBean inputInfo = paramBean.getParam().getInputInfo();

				int count = this.countCategoryByName(lotId, inputInfo.getSubject());

				UmItemChkUtils.checkInputCategory(inputInfo, count);

				String ctgId = support.getCtgNumberingDao().nextval();
				insertCategory(inputInfo, lotId, ctgId);

				paramBean.getResult().setCtgId(ctgId);
				paramBean.getResult().setCompleted(true);
				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003061I,inputInfo.getSubject());
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void insertCategory(CategoryEditInputBean bean, String lotId, String ctgId) {
		int sortOrder = this.getSortOrder( lotId );

		ICtgEntity ctgEntity = new CtgEntity();
		ctgEntity.setCtgId(ctgId);
		ctgEntity.setCtgNm(bean.getSubject());
		ctgEntity.setSortOdr(sortOrder);
		ctgEntity.setLotId(lotId);
		support.getCtgDao().insert(ctgEntity);


		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Add.getStatusId());
			support.getCtgHistDao().insert( histEntity , ctgEntity);
		}
	}

	private int getSortOrder( String lotId ) {
		CtgCondition condition = new CtgCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement(CtgItems.sortOdr, TriSortOrder.Desc, 1);

		List<ICtgEntity> entities = support.getCtgDao().find(condition.getCondition(), sort, 1, 1).getEntities();

		if ( 0 < entities.size() ) {
			Integer sortOrder = entities.get(0).getSortOdr();
			sortOrder++;
			return sortOrder;
		} else {
			return 1;
		}
	}

	private int countCategoryByName(String lotId, String ctgSubject) {
		if ( TriStringUtils.isEmpty(ctgSubject) ) {
			return 0;
		}

		CtgCondition condition = new CtgCondition();
		condition.setLotId(lotId);
		condition.setCtgNm(ctgSubject);

		return support.getCtgDao().count(condition.getCondition());
	}

}
