package jp.co.blueship.tri.fw.um.domainx.project.dto;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.GenderType;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 */
public class FlowProfileEditServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowProfileEditServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String logInUserId;
		private String userId;
		private ProfileEditInputBean inputInfo = new ProfileEditInputBean();

		public String getLogInUserId() {
			return logInUserId;
		}
		public RequestParam setLogInUserId(String logInUserId) {
			this.logInUserId = logInUserId;
			return this;
		}

		public String getSelectedUserId() {
			return userId;
		}
		public RequestParam setSelectedUserId(String userId) {
			this.userId = userId;
			return this;
		}

		public ProfileEditInputBean getInputInfo() {
			return inputInfo;
		}
		public RequestParam setInputInfo(ProfileEditInputBean inputInfo) {
			this.inputInfo = inputInfo;
			return this;
		}

	}

	public class ProfileEditInputBean {
		private String biography;
		private GenderType genderType = GenderType.none;
		private String userLocation;
		private String[] urls;

		public String getBiography() {
			return biography;
		}
		public ProfileEditInputBean setBiography(String biography) {
			this.biography = biography;
			return this;
		}

		public GenderType getGenderType() {
			return genderType;
		}
		public ProfileEditInputBean setGenderType(GenderType genderType) {
			this.genderType = genderType;
			return this;
		}

		public String getUserLocation() {
			return userLocation;
		}
		public ProfileEditInputBean setUserLocation(String userLocation) {
			this.userLocation = userLocation;
			return this;
		}

		public String[] getUrls() {
			return urls;
		}
		public ProfileEditInputBean setUrls(String... urls) {
			this.urls = urls;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
