package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.DesignSheetUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.UmActStatusId;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.dao.hist.eb.HistEntity;
import jp.co.blueship.tri.fw.um.dao.hist.eb.IHistEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.constants.MstoneItems;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneCondition;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.MstoneEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.MilestoneEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowMilestoneCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class FlowMilestoneCreationService implements IDomain<FlowMilestoneCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport ( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowMilestoneCreationServiceBean> execute(
			IServiceDto<FlowMilestoneCreationServiceBean> serviceDto) {

		FlowMilestoneCreationServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String lotId = paramBean.getParam().getSelectedLotId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				MilestoneEditInputBean inputInfo = paramBean.getParam().getInputInfo();

				int count = this.countMilestoneByName(lotId, inputInfo.getSubject());

				UmItemChkUtils.checkInputMilestone(inputInfo, count);

				String mstoneId = support.getMstoneNumberingDao().nextval();
				insertMilestone(inputInfo, lotId, mstoneId);

				paramBean.getResult().setMstoneId(mstoneId);
				paramBean.getResult().setCompleted(true);
				paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003064I,inputInfo.getSubject());
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void insertMilestone(MilestoneEditInputBean bean, String lotId, String mstoneId) {
		int sortOrder = this.getSortOrder ( lotId );

		IMstoneEntity mstoneEntity = new MstoneEntity();
		mstoneEntity.setMstoneId(mstoneId);
		mstoneEntity.setMstoneNm(bean.getSubject());
		mstoneEntity.setMstoneStDate((null != bean.getStartDate())? TriDateUtils.fillYMD(bean.getStartDate()) : bean.getStartDate());
		mstoneEntity.setMstoneEndDate((null != bean.getDueDate())? TriDateUtils.fillYMD(bean.getDueDate()) : bean.getDueDate());
		mstoneEntity.setContent(bean.getDescription());
		mstoneEntity.setLotId(lotId);
		mstoneEntity.setSortOdr(sortOrder);
		support.getMstoneDao().insert(mstoneEntity);

		if ( DesignSheetUtils.isRecord() ) {
			IHistEntity histEntity = new HistEntity();
			histEntity.setActSts(UmActStatusId.Add.getStatusId());
			support.getMstoneHistDao().insert( histEntity , mstoneEntity);
		}
	}

	private int getSortOrder( String lotId ) {
		MstoneCondition condition = new MstoneCondition();
		condition.setLotId( lotId );

		ISqlSort sort = new SortBuilder();
		sort.setElement(MstoneItems.sortOdr, TriSortOrder.Desc, 1);

		List<IMstoneEntity> entities = support.getMstoneDao().find(condition.getCondition(), sort, 1, 1).getEntities();

		if ( 0 < entities.size() ) {
			Integer sortOrder = entities.get(0).getSortOdr();
			sortOrder++;
			return sortOrder;
		} else {
			return 1;
		}
	}

	private int countMilestoneByName(String lotId, String mstoneSubject) {
		if ( TriStringUtils.isEmpty(mstoneSubject) ) {
			return 0;
		}

		MstoneCondition condition = new MstoneCondition();
		condition.setLotId(lotId);
		condition.setMstoneNm(mstoneSubject);

		return support.getMstoneDao().count(condition.getCondition());
	}


}
