package jp.co.blueship.tri.fw.um.domainx.gantt;

import java.sql.Timestamp;
import java.util.*;

import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.baseline.eb.HeadBlCondition;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotGrpLnkCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.GanttChartService;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttContent;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GanttHeader;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Grouping;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.GroupingDataType;
import jp.co.blueship.tri.fw.um.domainx.gantt.beans.dto.GanttViewBean.Status;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.domainx.gantt.dto.FlowGanttChartServiceBean.Span;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;

/**
 *
 * @version V4.00.00
 * @author Akahoshi
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowGanttChartService implements IDomain<FlowGanttChartServiceBean>  {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport umSupport = null;
	private IAmFinderSupport amSupport = null;

	private GanttChartService ganttChartService = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = amSupport.getUmFinderSupport();
	}

	public void setGanttChartService(GanttChartService ganttChartService){
		this.ganttChartService = ganttChartService;
	}

	@Override
	public IServiceDto<FlowGanttChartServiceBean> execute(IServiceDto<FlowGanttChartServiceBean> serviceDto) {

		FlowGanttChartServiceBean paramBean = serviceDto.getServiceBean();

		try{
			String lotId = paramBean.getParam().getSelectedLotId();

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if(RequestType.init.equals(paramBean.getParam().getRequestType()))
				this.init(paramBean);

			else if(RequestType.onChange.equals(paramBean.getParam().getRequestType()))
				this.onChange(paramBean);

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowGanttChartServiceBean paramBean) {
		this.onChange(paramBean);
	}

	private void onChange(FlowGanttChartServiceBean paramBean) {
		Span span = paramBean.getParam().getSearchCondition().getSpan();
		RequestOption requestOption = paramBean.getParam().getRequestOption();
		RequestType requestType = paramBean.getParam().getRequestType();
		Grouping requestGrouping = paramBean.getParam().getSearchCondition().getGrouping();

		Timestamp ganttStartDate = null;

		if(TriStringUtils.isEmpty(paramBean.getParam().getSearchCondition().getStartDate()))
			ganttStartDate = TriDateUtils.getSystemTimestamp();
		else
			ganttStartDate = TriDateUtils.convertStringToTimestamp(
								TriDateUtils.convertDateFormat( paramBean.getParam().getSearchCondition().getStartDate(), null),
								paramBean.getTimeZone());


		Calendar startCal = Calendar.getInstance( paramBean.getTimeZone() );
		startCal.setTime(new Date(ganttStartDate.getTime()));

		if( RequestOption.PreviousWeek.equals( requestOption ) && RequestType.onChange == requestType )
			startCal.add(Calendar.DATE, -7);

		else if( RequestOption.NextWeek.equals( requestOption ) && RequestType.onChange == requestType  )
			startCal.add(Calendar.DATE, +7);

		ganttStartDate = new Timestamp( startCal.getTime().getTime() );

		Calendar endCal = Calendar.getInstance( paramBean.getTimeZone() );
		endCal.setTime(new Date(ganttStartDate.getTime()));
		endCal.add(Calendar.MONTH , span.value());
		endCal.add(Calendar.DATE , 1);
		Timestamp ganttEndDate = new Timestamp(endCal.getTime().getTime());

		if( Grouping.none.equals( requestGrouping ) )
			this.setGroupingViewsOfGroupingNone( paramBean, ganttStartDate, ganttEndDate, span );

		else if( Grouping.DataType.equals( requestGrouping ) )
			this.setGroupingViewsOfGroupingDataType( paramBean, ganttStartDate, ganttEndDate, span );

		else if( Grouping.Assignee.equals( requestGrouping ) )
			this.setGroupingViewsOfGroupingAssignee( paramBean, ganttStartDate, ganttEndDate, span );

		else if( Grouping.Milestone.equals( requestGrouping ) )
			this.setGroupingViewsOfGroupingMstone( paramBean, ganttStartDate, ganttEndDate, span );

		else if( Grouping.Category.equals( requestGrouping ) )
			this.setGroupingViewsOfGroupingCtg( paramBean, ganttStartDate, ganttEndDate, span );


		paramBean.getParam().getSearchCondition().setStartDate( TriDateUtils.convertViewDateFormat(ganttStartDate,
																	TriDateUtils.getYMDDateFormat(null, paramBean.getTimeZone())) );
	}


	private void setGroupingViewsOfGroupingNone(FlowGanttChartServiceBean paramBean,
												Timestamp ganttStartDate,
												Timestamp ganttEndDate,
												Span span){

		Status searchstatus = paramBean.getParam().getSearchCondition().getStatus();

		List<IMstoneEntity> entityList = umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());

		GanttHeader ganttHeader = ganttChartService.getGanttHeader( paramBean.getLanguage(),
																	paramBean.getTimeZone(),
																	ganttStartDate,
																	span.value(),
																	entityList);

		List<GanttContent> ganttContents =
				ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																	paramBean.getLanguage(),
																	searchstatus,
																	paramBean.getParam().getSelectedLotId(),
																	null,
																	null,
																	null,
																	ganttStartDate,
																	ganttEndDate);

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();
		GanttViewBean gantt = new GanttViewBean()
									.setGrouping	( Grouping.none )
									.setGroupingNm	( null )
									.setHeader		( ganttHeader)
									.setContents	( ganttContents );

		groupingViews.add(gantt);
		paramBean.setGroupingViews(groupingViews);
	}


	private void setGroupingViewsOfGroupingDataType(FlowGanttChartServiceBean paramBean,
													Timestamp ganttStartDate,
													Timestamp ganttEndDate,
													Span span){

		Status searchstatus = paramBean.getParam().getSearchCondition().getStatus();
		String lotId = paramBean.getParam().getSelectedLotId();

		List<IMstoneEntity> mstoneEntityList = umSupport.findMstoneByLotId( paramBean.getParam().getSelectedLotId() );
		GanttHeader ganttHeader = ganttChartService.getGanttHeader( paramBean.getLanguage(),
																	paramBean.getTimeZone(),
																	ganttStartDate,
																	span.value(),
																	mstoneEntityList);

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();

		AreqCondition areqCondition = new AreqCondition();
		areqCondition.setLotId( lotId );
		areqCondition.setAreqCtgCd( AreqCtgCd.LendingRequest.value() );

		GanttViewBean checkOutGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.CheckOutRequest.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getAreqGanttContentList( paramBean.getLanguage(),
																			 paramBean.getTimeZone(),
																			 searchstatus,
																			 areqCondition,
																			 ganttStartDate,
																			 ganttEndDate )
								);

		if(checkOutGantt.getContents().size() > 0)
			groupingViews.add(checkOutGantt);

		areqCondition = new AreqCondition();
		areqCondition.setLotId( lotId );
		areqCondition.setAreqCtgCd( AreqCtgCd.ReturningRequest.value() );

		GanttViewBean checkInGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.CheckInRequest.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getAreqGanttContentList( paramBean.getLanguage(),
																		 	 paramBean.getTimeZone(),
																		 	 searchstatus,
																		 	 areqCondition,
																		 	 ganttStartDate,
																		 	 ganttEndDate )
								);

		if(checkInGantt.getContents().size() > 0)
			groupingViews.add(checkInGantt);

		areqCondition = new AreqCondition();
		areqCondition.setLotId( lotId );
		areqCondition.setAreqCtgCd( AreqCtgCd.RemovalRequest.value() );

		GanttViewBean removalGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.RemovalRequest.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getAreqGanttContentList( paramBean.getLanguage(),
																			 paramBean.getTimeZone(),
																			 searchstatus,
																			 areqCondition,
																			 ganttStartDate,
																			 ganttEndDate )
								);
		if(removalGantt.getContents().size() > 0)
			groupingViews.add(removalGantt);

		BpCondition bpCondition = new BpCondition();
		bpCondition.setLotId( lotId );

		GanttViewBean bpGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.BuildPackage.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getBpGanttContentList( paramBean.getLanguage(),
																		   paramBean.getTimeZone(),
																		   searchstatus,
																		   bpCondition,
																		   ganttStartDate,
																		   ganttEndDate )
								);
		if(bpGantt.getContents().size() > 0)
			groupingViews.add(bpGantt);

		RaCondition raCondition = new RaCondition();
		raCondition.setLotId( lotId );

		GanttViewBean raGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.ReleaseRequest.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getRaGanttContentList( paramBean.getLanguage(),
																		   paramBean.getTimeZone(),
																		   searchstatus,
																		   raCondition,
																		   ganttStartDate,
																		   ganttEndDate )
								);
		if(raGantt.getContents().size() > 0)
			groupingViews.add(raGantt);

		RpCondition rpCondition = new RpCondition();
		rpCondition.setLotId( lotId );

		GanttViewBean rpGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.ReleasePackage.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getRpGanttContentList( paramBean.getLanguage(),
																		   paramBean.getTimeZone(),
																		   searchstatus,
																		   rpCondition,
																		   ganttStartDate,
																		   ganttEndDate )
								);
		if(rpGantt.getContents().size() > 0)
			groupingViews.add(rpGantt);

		bpCondition = new BpCondition();
		bpCondition.setLotId( lotId );

		GanttViewBean bpCloseGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.BuildPackageClose.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getBpCloseGanttContentList( paramBean.getLanguage(),
																				paramBean.getTimeZone(),
																				searchstatus,
																				bpCondition,
																				ganttStartDate,
																				ganttEndDate )
								);
		if(bpCloseGantt.getContents().size() > 0)
			groupingViews.add(bpCloseGantt);

		DmDoCondition dmDoCondition = new DmDoCondition();
		dmDoCondition.setStsId( DmDoStatusId.JobRegistered.getStatusId() );
		GanttViewBean dmDoGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.DeploymentJob.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getDeploymentGanttContentList( paramBean.getLanguage(),
																				   paramBean.getTimeZone(),
																				   searchstatus,
																				   lotId,
																				   dmDoCondition,
																				   ganttStartDate,
																				   ganttEndDate )
								);
		if(dmDoGantt.getContents().size() > 0)
			groupingViews.add(dmDoGantt);

		dmDoCondition = new DmDoCondition();
		dmDoCondition.setStsId( DmDoStatusId.TimerConfigured.getStatusId() );
		GanttViewBean timerGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.DeploymentJobTimer.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getDeploymentGanttContentList( paramBean.getLanguage(),
																				   paramBean.getTimeZone(),
																				   searchstatus,
																				   lotId,
																				   dmDoCondition,
																				   ganttStartDate,
																				   ganttEndDate )
								);
		if(timerGantt.getContents().size() > 0)
			groupingViews.add(timerGantt);

		HeadBlCondition headBlCondition = new HeadBlCondition();
		headBlCondition.setLotId( lotId );

		GanttViewBean conflictCheckGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.ConflictCheck.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getConflictCheckGanttContentList( paramBean.getLanguage(),
																					  paramBean.getTimeZone(),
																					  searchstatus,
																					  headBlCondition,
																					  ganttStartDate,
																					  ganttEndDate )
								);
		if(conflictCheckGantt.getContents().size() > 0)
			groupingViews.add(conflictCheckGantt);

		headBlCondition = new HeadBlCondition();
		headBlCondition.setLotId( lotId );

		GanttViewBean mergeGantt = new GanttViewBean()
				.setGrouping	( Grouping.DataType )
				.setGroupingNm	( GroupingDataType.Merge.value() )
				.setHeader		( ganttHeader )
				.setContents	( ganttChartService.getMergeGanttContentList( paramBean.getLanguage(),
																			  paramBean.getTimeZone(),
																			  searchstatus,
																			  headBlCondition,
																			  ganttStartDate,
																			  ganttEndDate )
								);
		if(mergeGantt.getContents().size() > 0)
			groupingViews.add(mergeGantt);

		paramBean.setGroupingViews(groupingViews);
	}


	private void setGroupingViewsOfGroupingAssignee(FlowGanttChartServiceBean paramBean,
													Timestamp ganttStartDate,
													Timestamp ganttEndDate,
													Span span){
		Status searchstatus = paramBean.getParam().getSearchCondition().getStatus();

		LotGrpLnkCondition lotGrpLnkCondition = new LotGrpLnkCondition();
		lotGrpLnkCondition.setLotId( paramBean.getParam().getSelectedLotId());
		List<ILotGrpLnkEntity> lotGrpLnkEntityList =  this.amSupport.getLotGrpLnkDao().find(lotGrpLnkCondition.getCondition());

		List<IMstoneEntity> mstoneEntityList = umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());

		GanttHeader ganttHeader = ganttChartService.getGanttHeader( paramBean.getLanguage(),
																	paramBean.getTimeZone(),
																	ganttStartDate,
																	span.value(),
																	mstoneEntityList);

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();
		Map<String, GanttViewBean> userNameGroupingViewMap = new HashMap<>();

		List<GanttContent> ganttContents =
				ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																	paramBean.getLanguage(),
																	searchstatus,
																	paramBean.getParam().getSelectedLotId(),
																	null,
																	null,
																	null,
																	ganttStartDate,
																	ganttEndDate);

		for (GanttContent ganttContent : ganttContents) {
			if(userNameGroupingViewMap.containsKey(ganttContent.getAssigneeNm())) {
				userNameGroupingViewMap.get(ganttContent.getAssigneeNm()).getContents().add(ganttContent);
			} else {
				GanttViewBean gantt = new GanttViewBean()
						.setGrouping	( Grouping.Assignee )
						.setGroupingNm	( ganttContent.getAssigneeNm() )
						.setIconPath	( ganttContent.getAssigneeIconPath() )
						.setHeader		( ganttHeader );
				gantt.getContents().add(ganttContent);
				userNameGroupingViewMap.put(ganttContent.getAssigneeNm(), gantt);
			}
		}

		for(String key : userNameGroupingViewMap.keySet()) {
			groupingViews.add(userNameGroupingViewMap.get(key));
		}

		paramBean.setGroupingViews(groupingViews);
	}


	private void setGroupingViewsOfGroupingMstone(FlowGanttChartServiceBean paramBean,
												  Timestamp ganttStartDate,
												  Timestamp ganttEndDate,
												  Span span){
		Status searchstatus = paramBean.getParam().getSearchCondition().getStatus();

		List<IMstoneEntity> entityList = umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();

		GanttHeader nonMstoneGanttHeader =
				ganttChartService.getGanttHeader( paramBean.getLanguage(),
												  paramBean.getTimeZone(),
												  ganttStartDate,
												  span.value(),
												  entityList);

		List<GanttContent> nonMstoneGanttContents =
				ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																	paramBean.getLanguage(),
																	searchstatus,
																	paramBean.getParam().getSelectedLotId(),
																	null,
																	null,
																	null,
																	ganttStartDate,
																	ganttEndDate);

		GanttViewBean nonMsotneGantt = new GanttViewBean()
				.setGrouping	( Grouping.Milestone )
				.setGroupingNm	( "" )
				.setHeader		( nonMstoneGanttHeader )
				.setContents	( nonMstoneGanttContents );

		for(IMstoneEntity entity : entityList){
			List<IMstoneEntity> mstoneEntityList = new ArrayList<IMstoneEntity>();
			mstoneEntityList.add(entity);

			GanttHeader ganttHeader =
					ganttChartService.getGanttHeader( paramBean.getLanguage(),
													  paramBean.getTimeZone(),
													  ganttStartDate,
													  span.value(),
													  mstoneEntityList);

			List<GanttContent> ganttContents =
					ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																		paramBean.getLanguage(),
																		searchstatus,
																		paramBean.getParam().getSelectedLotId(),
																		entity.getMstoneId(),
																		null,
																		null,
																		ganttStartDate,
																		ganttEndDate);

			GanttViewBean gantt = new GanttViewBean()
					.setGrouping	( Grouping.Milestone )
					.setGroupingNm	( entity.getMstoneNm() )
					.setHeader		( ganttHeader )
					.setContents	( ganttContents );

			nonMsotneGantt.setContents(this.ganttContentListRemoval(nonMsotneGantt.getContents(), gantt.getContents()));

			if( gantt.getContents().size() > 0 )
				groupingViews.add(gantt);
		}
		if( nonMsotneGantt.getContents().size() > 0 )
			groupingViews.add(nonMsotneGantt);

		paramBean.setGroupingViews(groupingViews);
	}


	private void setGroupingViewsOfGroupingCtg( FlowGanttChartServiceBean paramBean,
												Timestamp ganttStartDate,
												Timestamp ganttEndDate,
												Span span){
		Status searchstatus = paramBean.getParam().getSearchCondition().getStatus();

		List<ICtgEntity> ctgEntityList = umSupport.findCtgByLotId(paramBean.getParam().getSelectedLotId());
		List<IMstoneEntity> mstoneEntityList = umSupport.findMstoneByLotId(paramBean.getParam().getSelectedLotId());

		GanttHeader ganttHeader =
				ganttChartService.getGanttHeader( paramBean.getLanguage(),
												  paramBean.getTimeZone(),
												  ganttStartDate,
												  span.value(),
												  mstoneEntityList);

		List<GanttViewBean> groupingViews = new ArrayList<GanttViewBean>();

		List<GanttContent> nonCtgGanttContents =
				ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																	paramBean.getLanguage(),
																	searchstatus,
																	paramBean.getParam().getSelectedLotId(),
																	null,
																	null,
																	null,
																	ganttStartDate,
																	ganttEndDate);

		GanttViewBean nonCtgGantt = new GanttViewBean()
				.setGrouping	( Grouping.Category )
				.setGroupingNm	( "" )
				.setHeader		( ganttHeader )
				.setContents	( nonCtgGanttContents );

		for(ICtgEntity entity : ctgEntityList){

			List<GanttContent> ganttContents =
					ganttChartService.getGanttContentListOfAllDataType( paramBean.getTimeZone(),
																		paramBean.getLanguage(),
																		searchstatus,
																		paramBean.getParam().getSelectedLotId(),
																		null,
																		entity.getCtgId(),
																		null,
																		ganttStartDate,
																		ganttEndDate);

			GanttViewBean gantt = new GanttViewBean()
					.setGrouping	( Grouping.Category )
					.setGroupingNm	( entity.getCtgNm() )
					.setHeader		( ganttHeader )
					.setContents	( ganttContents);

			nonCtgGantt.setContents(this.ganttContentListRemoval(nonCtgGantt.getContents(), gantt.getContents()));

			if( gantt.getContents().size() > 0 )
				groupingViews.add(gantt);
		}
		if( nonCtgGantt.getContents().size() > 0 )
			groupingViews.add(nonCtgGantt);

		paramBean.setGroupingViews(groupingViews);
	}


	private List<GanttContent> ganttContentListRemoval(List<GanttContent> ganttContents, List<GanttContent> removeGanttContent){

		List<GanttContent> removeList = new ArrayList<GanttContent>();
		for( GanttContent ganttContent : ganttContents ){
			for( GanttContent removeContent : removeGanttContent ){
				if( ganttContent.getDataAttribute().equals( removeContent.getDataAttribute() ) &&
					ganttContent.getDataId().equals( removeContent.getDataId() ))

					removeList.add( ganttContent );
			}
		}
		ganttContents.removeAll( removeList );

		return ganttContents;
	}

}
