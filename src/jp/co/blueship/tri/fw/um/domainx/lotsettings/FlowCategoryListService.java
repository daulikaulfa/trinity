package jp.co.blueship.tri.fw.um.domainx.lotsettings;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgEntity;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.CtgLnkCondition;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.beans.dto.CategoryListServiceSortOrderEditBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryListServiceBean;
import jp.co.blueship.tri.fw.um.domainx.lotsettings.dto.FlowCategoryListServiceBean.CategoryView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowCategoryListService implements IDomain<FlowCategoryListServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;


	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowCategoryListServiceBean> execute(
			IServiceDto<FlowCategoryListServiceBean> serviceDto) {

		FlowCategoryListServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			String lotId = paramBean.getParam().getSelectedLotId();
			paramBean.getResult().setCompleted(false);

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			PreConditions.assertOf(TriStringUtils.isNotEmpty(lotId), "SelectedLotId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				paramBean.setCategoryViews( this.getCategoryViews(paramBean, lotId) );
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if ( null != paramBean.getParam().getSortOrderEdit() ) {
					this.updateSortOrder( paramBean.getParam().getSortOrderEdit(), lotId );
					paramBean.setCategoryViews( this.getCategoryViews(paramBean, lotId) );
					paramBean.getResult().setCompleted(true);
				}
			}
			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				paramBean.setCategoryViews( this.getCategoryViews(paramBean, lotId) );
			}


			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private List<CategoryView> getCategoryViews(FlowCategoryListServiceBean paramBean, String lotId) {
		List<CategoryView> views = new ArrayList<CategoryView>();

		for ( ICtgEntity entity : support.findCtgByLotId( lotId ) ) {
			CtgLnkCondition condition = new CtgLnkCondition();
			condition.setCtgId( entity.getCtgId() );
			int count = support.getCtgLnkDao().count( condition.getCondition() );

			CategoryView viewBean = paramBean.new CategoryView()
				.setCategoryId( entity.getCtgId() )
				.setSubject( entity.getCtgNm() )
				.setCount( count )
				.setSortOrder( entity.getSortOdr() )
				.setRemovalEnabled( true )
			;

			views.add(viewBean);
		}

		return views;
	}

	private void updateSortOrder( CategoryListServiceSortOrderEditBean sortOrderBean, String lotId ) {
		String ctgId = sortOrderBean.getCategoryId();
		int newSortOrder = sortOrderBean.getNewSortOrder();
		int currentRow = 1;

		for ( ICtgEntity entity : support.findCtgByLotId( lotId ) ) {

			if ( currentRow == newSortOrder ) {
				currentRow++;
			}

			if ( ! entity.getCtgId().equals(ctgId) ) {
				if ( currentRow != entity.getSortOdr() ) {
					ICtgEntity updEntity = new CtgEntity();
					updEntity.setCtgId( entity.getCtgId() );
					updEntity.setSortOdr( currentRow );
					support.getCtgDao().update( updEntity );
				}
				currentRow++;
			}
		}

		ICtgEntity newEntity = new CtgEntity();
		newEntity.setCtgId( ctgId );
		newEntity.setSortOdr( newSortOrder );
		support.getCtgDao().update( newEntity );
	}
}
