package jp.co.blueship.tri.fw.um.domainx.cmn.dto;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.constants.ExportToFileSubmitOption;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchThisSiteView;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 *
 */
public class FlowSearchThisSiteServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private SearchThisSiteListFileResponse fileResponse = new SearchThisSiteListFileResponse();
	private List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();

	{
		this.setInnerService( new FlowSearchThisSiteServiceBean() );
	}

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowSearchThisSiteServiceExportToFileBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public SearchThisSiteListFileResponse getFileResponse() {
		return fileResponse;
	}
	public FlowSearchThisSiteServiceExportToFileBean setFileResponse(SearchThisSiteListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
		return this;
	}

	public List<SearchThisSiteView> getSearchSiteViews() {
		return searchSiteViews;
	}

	public void setSearchSiteViews(List<SearchThisSiteView> searchSiteViews) {
		this.searchSiteViews = searchSiteViews;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private String lotId = null;
		private ExportToFileBean exportBean = null;
		private SearchCondition searchCondition = new FlowSearchThisSiteServiceBean().new SearchCondition();
		private ExportToFileSubmitOption submitOption = ExportToFileSubmitOption.ExportToCsv;

		public String getLotId() {
			return lotId;
		}
		public RequestParam setLotId(String lotId) {
			this.lotId = lotId;
			return this;
		}

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public ExportToFileSubmitOption getSubmitOption() {
			return submitOption;
		}
		public RequestParam setSubmitOption(ExportToFileSubmitOption submitOption) {
			this.submitOption = submitOption;
			return this;
		}
		public ExportToFileBean getExportBean() {
			return exportBean;
		}
		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	public class SearchThisSiteListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
