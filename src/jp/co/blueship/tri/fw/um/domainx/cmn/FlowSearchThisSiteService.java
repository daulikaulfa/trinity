package jp.co.blueship.tri.fw.um.domainx.cmn;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.am.AmFluentFunctionUtils;
import jp.co.blueship.tri.am.dao.areq.constants.AreqItems;
import jp.co.blueship.tri.am.dao.areq.eb.AreqAttachedFileCondition;
import jp.co.blueship.tri.am.dao.areq.eb.AreqCondition;
import jp.co.blueship.tri.am.dao.areq.eb.IAreqEntity;
import jp.co.blueship.tri.am.dao.constants.AmTables;
import jp.co.blueship.tri.am.dao.constants.AreqCtgCd;
import jp.co.blueship.tri.am.dao.lot.constants.LotItems;
import jp.co.blueship.tri.am.dao.lot.eb.ILotDto;
import jp.co.blueship.tri.am.dao.lot.eb.ILotEntity;
import jp.co.blueship.tri.am.dao.lot.eb.ILotGrpLnkEntity;
import jp.co.blueship.tri.am.dao.lot.eb.LotCondition;
import jp.co.blueship.tri.am.dao.pjt.constants.PjtItems;
import jp.co.blueship.tri.am.dao.pjt.eb.IPjtEntity;
import jp.co.blueship.tri.am.dao.pjt.eb.PjtCondition;
import jp.co.blueship.tri.am.support.IAmFinderSupport;
import jp.co.blueship.tri.bm.dao.bp.constants.BpItems;
import jp.co.blueship.tri.bm.dao.bp.eb.BpCondition;
import jp.co.blueship.tri.bm.dao.bp.eb.IBpEntity;
import jp.co.blueship.tri.bm.support.IBmFinderSupport;
import jp.co.blueship.tri.dm.dao.dmdo.eb.DmDoCondition;
import jp.co.blueship.tri.dm.dao.dmdo.eb.IDmDoEntity;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.cmn.utils.StatusFlg;
import jp.co.blueship.tri.fw.cmn.utils.TriDateUtils;
import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusId;
import jp.co.blueship.tri.fw.constants.status.AmAreqStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.AmLotStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusId;
import jp.co.blueship.tri.fw.constants.status.AmPjtStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusId;
import jp.co.blueship.tri.fw.constants.status.BmBpStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.DmDoStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRaStatusIdForExecData;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusId;
import jp.co.blueship.tri.fw.constants.status.RmRpStatusIdForExecData;
import jp.co.blueship.tri.fw.dao.orm.EntityLimit;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ILimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.IExecDataStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.IStatusId;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.PageLimit;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.di.ContextAdapterFactory;
import jp.co.blueship.tri.fw.di.IContextAdapter;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.constants.DataAttribute;
import jp.co.blueship.tri.fw.um.dao.ctg.eb.ICtgEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.mstone.eb.IMstoneEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchCondition;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchServiceType;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchThisSiteServiceBean.SearchThisSiteView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;
import jp.co.blueship.tri.rm.dao.constants.RmTables;
import jp.co.blueship.tri.rm.dao.ra.constants.RaItems;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaDto;
import jp.co.blueship.tri.rm.dao.ra.eb.IRaEntity;
import jp.co.blueship.tri.rm.dao.ra.eb.RaCondition;
import jp.co.blueship.tri.rm.dao.rp.constants.RpItems;
import jp.co.blueship.tri.rm.dao.rp.eb.IRpEntity;
import jp.co.blueship.tri.rm.dao.rp.eb.RpCondition;
import jp.co.blueship.tri.rm.support.IRmFinderSupport;

/**
 * Provide the following backend services.
 * <br> - SearchThisSite
 *
 * @version V4.00.00
 * @author le.thixuan
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 */
public class FlowSearchThisSiteService implements IDomain<FlowSearchThisSiteServiceBean> {

	private static final ILog log = TriLogFactory.getInstance();
	IContextAdapter ca = ContextAdapterFactory.getContextAdapter();

	private IAmFinderSupport amSupport = null;
	private IUmFinderSupport umSupport = null;
	private IBmFinderSupport bmSupport = null;
	private IRmFinderSupport rmSupport = null;

	public void setAmSupport(IAmFinderSupport amSupport) {
		this.amSupport = amSupport;
		this.umSupport = this.amSupport.getUmFinderSupport();
		this.bmSupport = this.amSupport.getBmFinderSupport();
	}

	public void setRmSupport(IRmFinderSupport rmSupport) {
		this.rmSupport = rmSupport;
	}

	@Override
	public IServiceDto<FlowSearchThisSiteServiceBean> execute(IServiceDto<FlowSearchThisSiteServiceBean> serviceDto) {
		FlowSearchThisSiteServiceBean paramBean = serviceDto.getServiceBean();
		try {
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");

			String[] lotIds = paramBean.getParam().getSearchCondition().getLotIds();

			if (SearchServiceType.SearchLot.equals( paramBean.getParam().getSearchServiceType() )) {

				PreConditions.assertOf(TriStringUtils.isNotEmpty(lotIds), "SelectedLotId is not specified");
			}

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {

				this.init(paramBean);

			} else if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {

				this.onChange(paramBean);

			}
			return serviceDto;
		} catch (Exception e) {

			LogHandler.fatal(log, e);
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void init(FlowSearchThisSiteServiceBean paramBean) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();
		String dataNm = searchCondition.getDataNm();
		this.generateDropbox(paramBean, paramBean.getParam().getSearchCondition());
		if ( TriStringUtils.isNotEmpty( dataNm ) ) {
			this.onChange(paramBean);
		}

	}


	private void onChange( FlowSearchThisSiteServiceBean paramBean ) {
		SearchCondition searchCondition = paramBean.getParam().getSearchCondition();
		String dataNm = searchCondition.getDataNm();

		if (TriStringUtils.isEmpty( dataNm )) {
			throw new ContinuableBusinessException( UmMessageId.UM001080E );
		}

		if (TriStringUtils.isNotEmpty( searchCondition.getAssigneeIds() )) {
			if (!DataAttribute.ChangeProperty.value().equals( dataNm )
					&& !DataAttribute.CheckInRequest.value().equals( dataNm )
					&& !DataAttribute.CheckOutRequest.value().equals( dataNm )
					&& !DataAttribute.RemovalRequest.value().equals( dataNm )) {
				paramBean.setSearchSiteViews(new ArrayList<SearchThisSiteView>());
				return;
			}
		}

		if (searchCondition.isAttachedFile()) {
			if (!DataAttribute.CheckInRequest.value().equals( dataNm )
					&& !DataAttribute.ReleaseRequest.value().equals( dataNm )) {
				paramBean.setSearchSiteViews(new ArrayList<SearchThisSiteView>());
				return;
			}
		}

		if (TriStringUtils.isNotEmpty( searchCondition.getGroupIds() )) {
			if (!DataAttribute.CheckInRequest.value().equals( dataNm )
					&& !DataAttribute.RemovalRequest.value().equals( dataNm )
					&& !DataAttribute.CheckOutRequest.value().equals( dataNm )) {
				paramBean.setSearchSiteViews(new ArrayList<SearchThisSiteView>());
				return;
			}
		}

		SimpleDateFormat formatYMD = TriDateUtils.getYMDDateFormat( paramBean.getLanguage(), paramBean.getTimeZone() );

		int selectedPageNo = (0 == searchCondition.getSelectedPageNo()) ? 1 : searchCondition.getSelectedPageNo();
		int viewRows = paramBean.getParam().getLinesPerPage();

		ILimit limit = new PageLimit();

		String[] lotIds = searchCondition.getLotIds();

		if (TriStringUtils.isEmpty( lotIds )) {

			if (SearchServiceType.SearchSite.equals( paramBean.getParam().getSearchServiceType() )) {

				lotIds = this.getLotIds( searchCondition.getLotViews() );
			}
		}

		// Search Change Property
		if (DataAttribute.ChangeProperty.value().equals( dataNm )) {

			limit = this.searchChangeProperty(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search CheckIn Request
		if (DataAttribute.CheckInRequest.value().equals( dataNm )) {

			limit = this.searchCheckinRequest(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search CheckOut Request
		if (DataAttribute.CheckOutRequest.value().equals( dataNm )) {

			limit = this.searchCheckoutRequest(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search Removal Request
		if (DataAttribute.RemovalRequest.value().equals( dataNm )) {

			limit = this.searchRemovalRequest(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search Build Package
		if (DataAttribute.BuildPackage.value().equals( dataNm )) {

			limit = this.searchBuildPackage(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search Release Package
		if (DataAttribute.ReleasePackage.value().equals( dataNm )) {

			limit = this.searchReleasePackage(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search Release Request
		if (DataAttribute.ReleaseRequest.value().equals( dataNm )) {

			limit = this.searchReleaseRequest(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		// Search Deployment Job
		if (DataAttribute.DeploymentJob.value().equals( dataNm )) {

			limit = this.searchDeploymentJob(paramBean, searchCondition, lotIds, formatYMD, selectedPageNo, viewRows);
		}

		IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit);
		paramBean.setPage(page);

	}

	private void generateDropbox( FlowSearchThisSiteServiceBean paramBean, SearchCondition searchCondition ) {

		ISqlSort sort = new SortBuilder();
		List<String> groupIdList;
		List<String> lotIdList = new ArrayList<String>();

		// Group Views
		{
			List<IGrpEntity> grpEntities = this.umSupport.findGroupByUserId( paramBean.getUserId() );
			groupIdList = FluentList.from( grpEntities ).map( UmFluentFunctionUtils.toGroupIdFromGrpEntity).asList();
			searchCondition.setGroupViews( FluentList.from( grpEntities ).map( UmFluentFunctionUtils.toItemLabelsFromGrpEntity ).asList());

		}

		// Assignee Views
		{
			List<IUserEntity> userEntities = this.umSupport.findAllUser();

			searchCondition.setAssigneeViews( FluentList.from( userEntities ).map( UmFluentFunctionUtils.toItemLabelsFromUserEntity ).asList());

		}

		// Lot Views
		{
			if (SearchServiceType.SearchSite.equals(paramBean.getParam().getSearchServiceType())) {

				LotCondition lotCondition = new LotCondition();
				lotCondition.setStsIds( new String[]{ AmLotStatusId.LotInProgress.getStatusId(), AmLotStatusId.LotClosed.getStatusId() });
				List<String> disableLinkLotNumbers = new ArrayList<>();
				this.amSupport.setAccessableLotNumbers(lotCondition, paramBean, false, disableLinkLotNumbers, true);
				sort.setElement(LotItems.regTimestamp, TriSortOrder.Asc, 1);

				List<ILotEntity> lotEntities = this.amSupport.getLotDao().find( lotCondition.getCondition(), sort);
				List<ILotDto> lotDtoList = this.amSupport.findLotDto( lotEntities, AmTables.AM_LOT_GRP_LNK );

				searchCondition.setLotViews( this.getLotItemLabelViews( lotDtoList, groupIdList ));

				for (ItemLabelsBean lotLabelBean : searchCondition.getLotViews()) {
					lotIdList.add( lotLabelBean.getValue() );
				}

			} else {

				lotIdList.add( searchCondition.getLotIds()[0] );
			}

		}

		// Data Views
		{
			searchCondition.setDataViews( this.getDataItemLabelsView() );
		}

		// Status Views(depends from Data Attribute) : Sub value = Data Attribute
		{
			searchCondition.setStatusViews( this.getStatusItemLabelsView(paramBean) );
		}

		// Category Views(depends from lot id) : sub value = lot_id
		{
			if (TriStringUtils.isNotEmpty( lotIdList )) {
				List<ICtgEntity> ctgEntities = this.umSupport.findCtgByLotIds( lotIdList.toArray(new String[0]) );
				searchCondition.setCtgViews( FluentList.from( ctgEntities ).map( UmFluentFunctionUtils.toItemLabelsFromCtgEntityWithSubValue).asList());
			}

		}

		// Milestone Views(depends from lot id) : sub value = lot_id
		{
			if (TriStringUtils.isNotEmpty( lotIdList )) {
				List<IMstoneEntity> mstoneEntities = this.umSupport.findMstoneByLotIds( lotIdList.toArray(new String[0]) );
				searchCondition.setMstoneViews( FluentList.from( mstoneEntities ).map(UmFluentFunctionUtils.toItemLabelsFromMstoneEntityViewSubValue ).asList());
			}
		}
	}

	private ILimit searchChangeProperty(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		PjtCondition condition = new PjtCondition();

		if (TriStringUtils.isNotEmpty( lotIds )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.ChangeProperty ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getAssigneeIds() )) {
			condition.setAssigneeIds( searchCondition.getAssigneeIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(PjtItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IPjtEntity> pjtEntityLimit = this.amSupport.getPjtDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IPjtEntity pjtEntity : pjtEntityLimit.getEntities()) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, pjtEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( pjtEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.ChangeProperty)
					.setId( pjtEntity.getPjtId() )
					.setSubject( pjtEntity.getSummary() )
					.setStsId( pjtEntity.getStsId() )
					.setStatus( getStatusLabel(pjtEntity.getStsId(), AmPjtStatusId.value(pjtEntity.getStsId()), AmPjtStatusIdForExecData.value(pjtEntity.getStsId() ), paramBean) )
					.setAssigneeNm( pjtEntity.getAssigneeNm() )
					.setAssigneeIconPath( pjtEntity.getAssigneeIconPath() )
					.setAssigneeGroup( null )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(pjtEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );
		if (!searchCondition.isAttachedFile()) {
			return pjtEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}

	}

	private ILimit searchCheckinRequest(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		AreqCondition condition = new AreqCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.CheckInRequest ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getAssigneeIds() )) {
			condition.setAssigneeIds( searchCondition.getAssigneeIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getGroupIds() )) {
			condition.setGrpIds( searchCondition.getGroupIds() );
		} else {
			condition.setGrpIds( this.getGroupIds( searchCondition.getGroupViews() ));
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}
		condition.setAreqCtgCd( AreqCtgCd.ReturningRequest.value() );

		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IAreqEntity> areqEntityLimit = new EntityLimit<IAreqEntity>();
		List<IAreqEntity> areqEntities;

		if (!searchCondition.isAttachedFile()) {
			areqEntityLimit = this.amSupport.getAreqDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);
			areqEntities = areqEntityLimit.getEntities();

		} else {

			areqEntities = this.amSupport.getAreqDao().find(condition.getCondition(), sort);
		}

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IAreqEntity areqEntity : areqEntities) {
			boolean isAttachedFile = false;
			AreqAttachedFileCondition attachedFileCondition = new AreqAttachedFileCondition();
			attachedFileCondition.setAreqId( areqEntity.getAreqId() );

			int count = this.amSupport.getAreqAttachedFileDao().count( attachedFileCondition.getCondition() );
			if ( count > 0 ) {
				isAttachedFile = true;
			}

			if (searchCondition.isAttachedFile()
					&& !isAttachedFile) {
				continue;
			}

			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, areqEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( areqEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.CheckInRequest)
					.setId( areqEntity.getAreqId() )
					.setSubject( areqEntity.getSummary() )
					.setStsId( areqEntity.getStsId() )
					.setStatus( getStatusLabel(areqEntity.getStsId(), AmAreqStatusId.value(areqEntity.getStsId()), AmAreqStatusIdForExecData.value(areqEntity.getStsId() ), paramBean) )
					.setAssigneeNm( areqEntity.getAssigneeNm() )
					.setAssigneeIconPath( areqEntity.getAssigneeIconPath() )
					.setAssigneeGroup( areqEntity.getGrpNm() )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMD) )
					.setAttachedFile( isAttachedFile );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return areqEntityLimit.getLimit();

		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}


	}

	private ILimit searchCheckoutRequest(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();

		AreqCondition condition = new AreqCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.CheckOutRequest ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getAssigneeIds() )) {
			condition.setAssigneeIds( searchCondition.getAssigneeIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getGroupIds() )) {
			condition.setGrpIds( searchCondition.getGroupIds() );
		} else {
			condition.setGrpIds( this.getGroupIds( searchCondition.getGroupViews() ));
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		condition.setAreqCtgCds(  AreqCtgCd.LendingRequest.value() );
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IAreqEntity> areqEntityLimit = this.amSupport.getAreqDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IAreqEntity areqEntity : areqEntityLimit.getEntities()) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, areqEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( areqEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.CheckOutRequest)
					.setId( areqEntity.getAreqId() )
					.setSubject( areqEntity.getSummary() )
					.setStsId( areqEntity.getStsId() )
					.setStatus( getStatusLabel(areqEntity.getStsId(), AmAreqStatusId.value(areqEntity.getStsId()), AmAreqStatusIdForExecData.value(areqEntity.getStsId() ), paramBean) )
					.setAssigneeNm( areqEntity.getAssigneeNm() )
					.setAssigneeIconPath( areqEntity.getAssigneeIconPath() )
					.setAssigneeGroup( areqEntity.getGrpNm() )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return areqEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}
	}

	private ILimit searchRemovalRequest(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();

		AreqCondition condition = new AreqCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.RemovalRequest ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getAssigneeIds() )) {
			condition.setAssigneeIds( searchCondition.getAssigneeIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getGroupIds() )) {
			condition.setGrpIds( searchCondition.getGroupIds() );
		} else {
			condition.setGrpIds( this.getGroupIds( searchCondition.getGroupViews() ));
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		condition.setAreqCtgCds(  AreqCtgCd.RemovalRequest.value() );
		ISqlSort sort = new SortBuilder();
		sort.setElement(AreqItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IAreqEntity> areqEntityLimit = this.amSupport.getAreqDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IAreqEntity areqEntity : areqEntityLimit.getEntities()) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, areqEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( areqEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.RemovalRequest)
					.setId( areqEntity.getAreqId() )
					.setSubject( areqEntity.getSummary() )
					.setStsId( areqEntity.getStsId() )
					.setStatus( getStatusLabel(areqEntity.getStsId(), AmAreqStatusId.value(areqEntity.getStsId()), AmAreqStatusIdForExecData.value(areqEntity.getStsId() ), paramBean) )
					.setAssigneeNm( areqEntity.getAssigneeNm() )
					.setAssigneeIconPath( areqEntity.getAssigneeIconPath() )
					.setAssigneeGroup( areqEntity.getGrpNm() )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(areqEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return areqEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}
	}

	private ILimit searchBuildPackage(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		BpCondition condition = new BpCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.BuildPackage ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(BpItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IBpEntity> bpEntityLimit = this.bmSupport.getBpDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IBpEntity bpEntity : bpEntityLimit.getEntities()) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, bpEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( bpEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.BuildPackage)
					.setId( bpEntity.getBpId() )
					.setSubject( bpEntity.getBpNm() )
					.setStsId( bpEntity.getStsId() )
					.setStatus( getStatusLabel(bpEntity.getStsId(), BmBpStatusId.value(bpEntity.getStsId()), BmBpStatusIdForExecData.value(bpEntity.getStsId() ), paramBean) )
					.setAssigneeNm( null )
					.setAssigneeIconPath( null )
					.setAssigneeGroup( null )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(bpEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return bpEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}
	}

	private ILimit searchReleasePackage(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		RpCondition condition = new RpCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.ReleasePackage ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(RpItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IRpEntity> rpEntityLimit = this.rmSupport.getRpDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IRpEntity rpEntity : rpEntityLimit.getEntities()) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, rpEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( rpEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.ReleasePackage)
					.setId( rpEntity.getRpId() )
					.setSubject( rpEntity.getSummary() )
					.setStsId( rpEntity.getStsId() )
					.setStatus( getStatusLabel(rpEntity.getStsId(), RmRpStatusId.value(rpEntity.getStsId()), RmRpStatusIdForExecData.value(rpEntity.getStsId() ), paramBean) )
					.setAssigneeNm( null )
					.setAssigneeIconPath( null )
					.setAssigneeGroup( null )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(rpEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return rpEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}
	}


	private ILimit searchReleaseRequest(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		RaCondition condition = new RaCondition();

		if (TriStringUtils.isNotEmpty( searchCondition.getLotIds() )) {
			condition.setLotIds( lotIds );
		}

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.ReleaseRequest ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}
		
		ISqlSort sort = new SortBuilder();
		sort.setElement(RaItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IRaEntity> raEntityLimit = this.rmSupport.getRaDao().find(condition.getCondition(), sort, selectedPageNo, viewRows);

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IRaEntity raEntity : raEntityLimit.getEntities()) {
			boolean isAttachedFile = false;

			IRaDto raDTO = this.rmSupport.findRaDto(raEntity, RmTables.RM_RA_ATTACHED_FILE);
			if ( raDTO.getRaAttachedFileEntities().size() > 0 ) {
				isAttachedFile = true;
			}

			if (searchCondition.isAttachedFile() && !isAttachedFile) {
				continue;
			}
			
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, raEntity.getLotId() );
			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( raEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.ReleaseRequest)
					.setId( raEntity.getRaId() )
					.setSubject( raEntity.getRemarks() )
					.setStsId( raEntity.getStsId() )
					.setStatus( getStatusLabel(raEntity.getStsId(), RmRaStatusId.value(raEntity.getStsId()), RmRaStatusIdForExecData.value(raEntity.getStsId() ), paramBean) )
					.setAssigneeNm( null )
					.setAssigneeIconPath( null )
					.setAssigneeGroup( null )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat(raEntity.getUpdTimestamp(), formatYMD) )
					.setAttachedFile( isAttachedFile );
			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );

		if (!searchCondition.isAttachedFile()) {
			return raEntityLimit.getLimit();
		} else {
			return this.setPageLimit( paramBean, searchSiteViews );
		}
	}

	private ILimit searchDeploymentJob(FlowSearchThisSiteServiceBean paramBean,
			SearchCondition searchCondition, String[] lotIds, SimpleDateFormat formatYMD, int selectedPageNo, int viewRows) {
		List<SearchThisSiteView> searchSiteViews = new ArrayList<SearchThisSiteView>();
		DmDoCondition condition = new DmDoCondition();

		condition.setMstoneFromTo( searchCondition.getMstoneStartDate(), searchCondition.getMstoneDueDate() );

		if (TriStringUtils.isNotEmpty( searchCondition.getStatusIds() )) {
			condition.setStsIds( searchCondition.getStatusIds() );
		} else {
			condition.setStsIds( this.getStatusIds( searchCondition.getStatusViews(), DataAttribute.DeploymentJob ) );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getCtgIds() )) {
			condition.setCtgIds( searchCondition.getCtgIds());
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getMstoneIds() )) {
			condition.setMstoneIds( searchCondition.getMstoneIds() );
		}
		if (TriStringUtils.isNotEmpty( searchCondition.getKeyword() )) {
			condition.setContainsByKeyword( searchCondition.getKeyword() );
		}

		ISqlSort sort = new SortBuilder();
		sort.setElement(RaItems.regTimestamp, TriSortOrder.Desc, 1);

		List<IDmDoEntity> dmDoEntities = this.rmSupport.getDmDoDao().find(condition.getCondition(), sort);

		if (TriStringUtils.isNotEmpty( lotIds )) {
			condition.setLotIds( lotIds );
		}

		Map<String, ILotEntity> lotMap = new HashMap<String, ILotEntity>();

		for (IDmDoEntity dmDoEntity : dmDoEntities) {
			ILotEntity lotEntity = this.amSupport.findLotEntity( lotMap, dmDoEntity.getLotId() );

			SearchThisSiteView view = paramBean.new SearchThisSiteView()
					.setLotId( dmDoEntity.getLotId() )
					.setLotNm( lotEntity.getLotNm() )
					.setDataAttribute( DataAttribute.DeploymentJob)
					.setId( dmDoEntity.getMgtVer() )
					.setSubject( null )
					.setStsId( dmDoEntity.getStsId() )
					.setStatus( getStatusLabel(dmDoEntity.getStsId(), DmDoStatusId.value(dmDoEntity.getStsId()), null, paramBean) )
					.setAssigneeNm( null )
					.setAssigneeIconPath( null )
					.setAssigneeGroup( null )
					.setLatestUpdDate( TriDateUtils.convertViewDateFormat( dmDoEntity.getUpdTimestamp(), formatYMD) );

			searchSiteViews.add( view );
		}

		paramBean.setSearchSiteViews( searchSiteViews );
		return this.setPageLimit( paramBean, searchSiteViews );
	}

	private List<ItemLabelsBean> getLotItemLabelViews(List<ILotDto> lotDtoList, List<String> groupIdList) {
		List<ILotEntity> accessibleLotList = new ArrayList<ILotEntity>();
		for ( ILotDto lotDto : lotDtoList ) {

			if (TriStringUtils.isEmpty(lotDto.getLotGrpLnkEntities())) {

				accessibleLotList.add( lotDto.getLotEntity() );

			} else {

				if (StatusFlg.on == lotDto.getLotEntity().getAllowListView()) {
					accessibleLotList.add( lotDto.getLotEntity() );
				} else {
					for (ILotGrpLnkEntity group : lotDto.getLotGrpLnkEntities()) {

						if (groupIdList.contains(group.getGrpId())) {
							accessibleLotList.add(lotDto.getLotEntity());
							break;
						}
					}
				}
			}
		}

		return FluentList.from( accessibleLotList ).map( AmFluentFunctionUtils.toItemLabelsFromLotEntity ).asList();
	}

	private List<ItemLabelsBean> getDataItemLabelsView() {
		List<String> dataViews = new ArrayList<String>();
		dataViews.add( DataAttribute.ChangeProperty.value() );
		dataViews.add( DataAttribute.CheckInRequest.value() );
		dataViews.add( DataAttribute.CheckOutRequest.value() );
		dataViews.add( DataAttribute.RemovalRequest.value() );
		dataViews.add( DataAttribute.BuildPackage.value() );
		dataViews.add( DataAttribute.ReleaseRequest.value() );
		dataViews.add( DataAttribute.ReleasePackage.value() );
		dataViews.add( DataAttribute.DeploymentJob.value() );

		return FluentList.from( dataViews ).map( AmFluentFunctionUtils.toItemLabelsFromDataAttributeValues ).asList();
	}

	private List<ItemLabelsBean> getStatusItemLabelsView(FlowSearchThisSiteServiceBean paramBean) {
		List<ItemLabelsBean> itemLabels = new ArrayList<ItemLabelsBean>();

		Map<String, String[]> dataMap = new HashMap<String, String[]>();

		// ChangeProperty: AM_PJT
		dataMap.put(DataAttribute.ChangeProperty.value(), new String[] {
				AmPjtStatusId.ChangePropertyInProgress.getStatusId(),
				AmPjtStatusId.ChangePropertyClosed.getStatusId(),
				AmPjtStatusId.ChangePropertyClosable.getStatusId(),
				AmPjtStatusId.ChangePropertyNotClosed.getStatusId(),
				AmPjtStatusId.ChangePropertyTestCompleted.getStatusId(),
				AmPjtStatusId.Merged.getStatusId()
		});

		// CheckInRequest: AM_AREQ
		dataMap.put(DataAttribute.CheckInRequest.value(), new String[] {
				AmAreqStatusId.CheckinRequested.getStatusId(),
				AmAreqStatusIdForExecData.ReturnToCheckoutRequest.getStatusId(),
				AmAreqStatusIdForExecData.CheckinRequestCancelled.getStatusId(),
				AmAreqStatusId.CheckinRequestApproved.getStatusId(),
				AmAreqStatusId.BuildPackageClosed.getStatusId(),
				AmAreqStatusId.Merged.getStatusId()
		});

		// CheckOutRequest: AM_AREQ
		dataMap.put(DataAttribute.CheckOutRequest.value(), new String[] {
				AmAreqStatusId.CheckoutRequested.getStatusId(),
				AmAreqStatusId.DraftCheckoutRequest.getStatusId()
		});

		// RemovalRequest: AM_AREQ
		dataMap.put(DataAttribute.RemovalRequest.value(), new String[] {
				AmAreqStatusId.RemovalRequested.getStatusId(),
				AmAreqStatusId.DraftRemovalRequest.getStatusId(),
				AmAreqStatusIdForExecData.ReturnToPendingRemovalRequest.getStatusId(),
				AmAreqStatusIdForExecData.RemovalRequestCancelled.getStatusId(),
				AmAreqStatusId.RemovalRequestApproved.getStatusId(),
				AmAreqStatusId.BuildPackageClosed.getStatusId(),
				AmAreqStatusId.Merged.getStatusId()
		});

		// BuildPackage: BM_BP
		dataMap.put(DataAttribute.BuildPackage.value(), new String[] {
				BmBpStatusIdForExecData.CreatingBuildPackage.getStatusId(),
				BmBpStatusId.BuildPackageCreated.getStatusId(),
				BmBpStatusId.BuildPackageClosed.getStatusId(),
				BmBpStatusId.Unprocessed.getStatusId(),
				BmBpStatusIdForExecData.BuildPackageCloseError.getStatusId(),
				BmBpStatusIdForExecData.BuildPackageError.getStatusId()
		});

		// ReleasePackage: RM_RP
		dataMap.put(DataAttribute.ReleasePackage.value(), new String[] {
				RmRpStatusId.ReleasePackageRemoved.getStatusId(),
				RmRpStatusId.ReleasePackageClosed.getStatusId(),
				RmRpStatusId.ReleasePackageCreated.getStatusId(),
				RmRpStatusId.Unprocessed.getStatusId(),
				RmRpStatusIdForExecData.CreatingReleasePackage.getStatusId(),
				RmRpStatusIdForExecData.ReleasePackageError.getStatusId()
		});

		// ReleaseRequest: RM_RA --> TO DO
		dataMap.put(DataAttribute.ReleaseRequest.value(), new String[] {
				RmRaStatusId.ReleaseRequested.getStatusId(),
				RmRaStatusId.ReleaseRequestApproved.getStatusId(),
				RmRaStatusId.DraftReleaseRequest.getStatusId(),
				RmRaStatusId.ReleaseRequestRemoved.getStatusId(),
				RmRaStatusId.PendingReleaseRequest.getStatusId(),
				RmRaStatusId.ReleaseRequestClosed.getStatusId(),
				RmRaStatusId.ReleasePackageCreated.getStatusId(),
				RmRaStatusId.ReleasePackageRemoved.getStatusId(),
				RmRaStatusIdForExecData.ReleaseRequestApprovalCancelled.getStatusId(),
		});

		// DeploymentJob: DM_DO --> TO DO
		dataMap.put(DataAttribute.DeploymentJob.value(), new String[] {
				DmDoStatusId.JobRegistered.getStatusId(),
				DmDoStatusId.TimerConfigured.getStatusId(),
				DmDoStatusId.JobCancelled.getStatusId()
		});

		for (Entry<String, String[]> entry : dataMap.entrySet()) {
			String dataView = entry.getKey();
			for (String stsId : entry.getValue()) {
				String label = "";

				if (DataAttribute.ChangeProperty.value().equals(dataView)) {

					label = getStatusLabel(stsId, AmPjtStatusId.value(stsId), AmPjtStatusIdForExecData.value(stsId), paramBean);

				} else if (DataAttribute.CheckInRequest.value().equals( dataView )
						|| DataAttribute.CheckOutRequest.value().equals( dataView )
						|| DataAttribute.RemovalRequest.value().equals( dataView )) {

					label = getStatusLabel(stsId, AmAreqStatusId.value(stsId), AmAreqStatusIdForExecData.value(stsId), paramBean);

				} else if (DataAttribute.BuildPackage.value().equals( dataView )) {

					label = getStatusLabel(stsId, BmBpStatusId.value(stsId), BmBpStatusIdForExecData.value(stsId), paramBean);

				} else if (DataAttribute.ReleasePackage.value().equals( dataView )) {

					label = getStatusLabel(stsId, RmRpStatusId.value(stsId), RmRpStatusIdForExecData.value(stsId), paramBean);

				} else if (DataAttribute.ReleaseRequest.value().equals( dataView )) {

					label = getStatusLabel(stsId, RmRaStatusId.value(stsId), RmRaStatusIdForExecData.value(stsId), paramBean);

				} else if (DataAttribute.DeploymentJob.value().equals( dataView )) {

					label = getStatusLabel(stsId, DmDoStatusId.value(stsId), null, paramBean);
				}
				ItemLabelsBean labelsBean = new ItemLabelsBean( label, stsId, dataView);

				itemLabels.add(labelsBean);
			}
		}

		return itemLabels;
	}

	private String[] getLotIds(List<ItemLabelsBean> lotViews) {
		List<String> lotIdList = new ArrayList<String>();
		for (ItemLabelsBean lotLabelBean : lotViews) {
			lotIdList.add( lotLabelBean.getValue() );
		}
		return lotIdList.toArray( new String[0] );
	}

	private String[] getStatusIds(List<ItemLabelsBean> statusViews, DataAttribute dataNm) {
		List<String> stsIdList = new ArrayList<String>();

		for (ItemLabelsBean stsLabelBean : statusViews) {
			if (dataNm.value().equals(stsLabelBean.getSubValue())) {
				stsIdList.add( stsLabelBean.getValue() );
			}
		}
		return stsIdList.toArray( new String[0] );
	}

	private String[] getGroupIds(List<ItemLabelsBean> grpViews) {
		List<String> grpIdList = new ArrayList<String>();

		for (ItemLabelsBean grpLabelBean : grpViews) {
			grpIdList.add(grpLabelBean.getValue());
		}

		return grpIdList.toArray( new String[0] );
	}

	private ILimit setPageLimit( FlowSearchThisSiteServiceBean paramBean, List<SearchThisSiteView> searchSiteViews) {

		ILimit limit = new PageLimit();
		int size = searchSiteViews.size();
		int selectedPageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		selectedPageNo = ( 0 == selectedPageNo )? 1 : selectedPageNo;
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		if (0 == size) {
			limit.setMaxRows( size );
			limit.setViewRows( linesPerPage );
			limit.getPageBar().setValue( selectedPageNo );

			return limit;
		}

		if (size <= linesPerPage) {

			selectedPageNo = 1;
			paramBean.setSearchSiteViews( searchSiteViews );

		} else {

			int maxRowRange = (selectedPageNo * linesPerPage);

			if (maxRowRange > size || linesPerPage == 0) {

				maxRowRange = size;
			}

			paramBean.setSearchSiteViews( new ArrayList<SearchThisSiteView>());

			for (int i = ((selectedPageNo - 1) * linesPerPage); i < maxRowRange; i++ ) {
				paramBean.getSearchSiteViews().add( searchSiteViews.get(i) );
			}
		}

		{
			limit.setMaxRows( size );
			limit.setViewRows( linesPerPage );
			limit.getPageBar().setValue( selectedPageNo );
		}

		return limit;
	}

	private String getStatusLabel(String statusId, IStatusId iStsId, IExecDataStatusId iExecStsId, FlowSearchThisSiteServiceBean paramBean) {
		String statusLabel = "";

		if(iStsId != null) {
			statusLabel = ca.getValue(iStsId.getMessageKey(), paramBean.getLanguage());
		} else if (iExecStsId != null) {
			statusLabel = ca.getValue(iExecStsId.getMessageKey(), paramBean.getLanguage());
		}
		return statusLabel;
	}

}
