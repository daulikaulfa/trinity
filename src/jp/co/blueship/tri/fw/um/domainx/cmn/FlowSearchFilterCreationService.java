package jp.co.blueship.tri.fw.um.domainx.cmn;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterCreationServiceBean;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yohei Tanaka
 *
 */
public class FlowSearchFilterCreationService implements IDomain<FlowSearchFilterCreationServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.fwSupport = support;
		this.support = this.fwSupport.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowSearchFilterCreationServiceBean> execute(
			IServiceDto<FlowSearchFilterCreationServiceBean> serviceDto) {

		FlowSearchFilterCreationServiceBean paramBean = null;

		try {
			paramBean = serviceDto.getServiceBean();
			paramBean.getResult().setCompleted(false);

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				submitChange( paramBean );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void submitChange(FlowSearchFilterCreationServiceBean paramBean){

		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();

		String filterId = support.getFilterNumberingDao().nextval();

		FilterEntity entity = new FilterEntity();
		entity.setFilterId(filterId);
		entity.setLotId( paramBean.getParam().getInputInfo().getLotId() );
		entity.setUserId( paramBean.getUserId() );
		entity.setFilterNm( paramBean.getParam().getInputInfo().getFilterNm() );
		entity.setFilterData( gson.toJson(paramBean.getParam().getInputInfo().getSearchFilter()) );
		entity.setSvcId( paramBean.getParam().getInputInfo().getServiceId() );

		support.getFilterDao().insert(entity);
		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003014I);
	}
}





