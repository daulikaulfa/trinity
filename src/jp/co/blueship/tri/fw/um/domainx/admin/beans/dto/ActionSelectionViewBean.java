package jp.co.blueship.tri.fw.um.domainx.admin.beans.dto;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import jp.co.blueship.tri.fw.svc.beans.dto.ITreeFolderViewBean;
import jp.co.blueship.tri.fw.svc.beans.dto.TreeFolderViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Yukihiro Eguchi
 */
public class ActionSelectionViewBean {
	private ITreeFolderViewBean folderView = new TreeFolderViewBean().setName("");

	public ITreeFolderViewBean getFolderView() {
		return folderView;
	}
	public ActionSelectionViewBean setFolderView(ITreeFolderViewBean folderView) {
		this.folderView = folderView;
		return this;
	}

	public String toJsonFromActionSelection() {
		Gson gson = new GsonBuilder().setPrettyPrinting().excludeFieldsWithoutExposeAnnotation().create();
		return gson.toJson(folderView);
	}

}
