package jp.co.blueship.tri.fw.um.domainx.wiki.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.cmn.utils.TriStringUtils;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiPageViewBean;
import jp.co.blueship.tri.fw.um.domainx.wiki.beans.dto.WikiTagViewBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author Akahoshi
 */
public class FlowWikiDetailsServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();

	private WikiView detailsView = new WikiView();

	/**
	 * This is tag list of side bar.
	 */
	private List<WikiTagViewBean> tagViews = new ArrayList<WikiTagViewBean>();
	/**
	 * This is wiki page list of side bar.
	 */
	private List<WikiPageViewBean> pageViews = new ArrayList<WikiPageViewBean>();

	public RequestParam getParam() {
		return this.param;
	}

	public WikiView getDetailsView(){
		return this.detailsView;
	}
	public FlowWikiDetailsServiceBean setDetailsView( WikiView detailsView ){
		this.detailsView = detailsView;
		return this;
	}

	public List<WikiTagViewBean> getTagViews(){
		return this.tagViews;
	}
	public FlowWikiDetailsServiceBean setTagViews( List<WikiTagViewBean> tagViews ){
		this.tagViews = tagViews;
		return this;
	}

	public List<WikiPageViewBean> getPageViews(){
		return this.pageViews;
	}
	public FlowWikiDetailsServiceBean setPageViews( List<WikiPageViewBean> pageViews ){
		this.pageViews = pageViews;
		return this;
	}


	public class RequestParam extends DomainServiceBean.RequestParam {
		/**
		 * View the WikiHome using lotId if wikiId is not specified.
		 */
		private String lotId;
		private String wikiId;
		private RequestOption requestOption = RequestOption.none;
		/**
		 * This is search word of Wiki page list.
		 */
		private String searchPageNm;

		public String getSelectedLotId() {
			return this.lotId;
		}
		public RequestParam setSelectedLotId(String wikiId) {
			this.lotId = wikiId;
			return this;
		}

		public String getSelectedWikiId() {
			return this.wikiId;
		}
		public RequestParam setSelectedWikiId(String wikiId) {
			this.wikiId = wikiId;
			return this;
		}

		public RequestOption getRequestOption(){
			return this.requestOption;
		}
		public RequestParam setRequestOption(RequestOption requestOption){
			this.requestOption = requestOption;
			return this;
		}

		public String getSearchPageNm() {
			return this.searchPageNm;
		}
		public RequestParam setSearchPageNm(String searchPageNm) {
			this.searchPageNm = searchPageNm;
			return this;
		}
	}

	public enum RequestOption {
		none				( "" ),
		All					( "all" ),
		WikiPageList		( "wikiPageList" ),
		TagList				( "tagList" ),
		WikiDetailsService	( "wikiDetailsService" ),//this service
		;

		private String value = null;

		private RequestOption( String value) {
			this.value = value;
		}

		public boolean equals( String value ) {
			RequestOption type = value( value );

			if ( null == type ) return false;
			if ( !this.equals(type) ) return false;

			return true;
		}

		public String value() {
			return this.value;
		}

		public static RequestOption value( String value ) {
			if ( TriStringUtils.isEmpty( value ) )
				return null;

			for ( RequestOption type : values() )
				if ( type.value().equals( value ) )
					return type;

			return none;
		}
	}

	public class WikiView {
		private String wikiId;
		private String wikiNm;
		private String content;
		private boolean isWikiHome = false;
		private String createdBy;
		private String createdByUserId;
		private String createdIconPath;
		private String createdDate;
		private String updUserNm;
		private String updUserId;
		private String updUserIconPath;
		private String updDate;
		private List<String> tagViews = new ArrayList<String>();
		private List<AttachFile> attachmentFileNms = new ArrayList<AttachFile>();

		public String getWikiId(){
			return this.wikiId;
		}
		public WikiView setWikiId( String wikiId ){
			this.wikiId = wikiId;
			return this;
		}

		public String getWikiNm(){
			return this.wikiNm;
		}
		public WikiView setWikiNm( String wikiNm ){
			this.wikiNm = wikiNm;
			return this;
		}

		public String getContent(){
			return this.content;
		}
		public WikiView setContent( String content ){
			this.content = content;
			return this;
		}

		public boolean isWikiHome(){
			return this.isWikiHome;
		}
		public WikiView setIsWikiHome( boolean isWikiHome ){
			this.isWikiHome = isWikiHome;
			return this;
		}

		public String getCreatedBy(){
			return this.createdBy;
		}
		public WikiView setCreatedBy( String createdBy ){
			this.createdBy = createdBy;
			return this;
		}

		public String getCreatedByUserId(){
			return this.createdByUserId;
		}
		public WikiView setCreatedByUserId( String createdByUserId ){
			this.createdByUserId = createdByUserId;
			return this;
		}

		public String getCreatedIconPath(){
			return this.createdIconPath;
		}
		public WikiView setCreatedIconPath( String createdIconPath ){
			this.createdIconPath = createdIconPath;
			return this;
		}

		public String getCreatedDate(){
			return this.createdDate;
		}
		public WikiView setCreatedDate( String createdDate ){
			this.createdDate = createdDate;
			return this;
		}

		public String getUpdUserNm(){
			return this.updUserNm;
		}
		public WikiView setUpdUserNm( String updUserNm ){
			this.updUserNm = updUserNm;
			return this;
		}

		public String getUpdUserId(){
			return this.updUserId;
		}
		public WikiView setUpdUserId( String updUserId ){
			this.updUserId = updUserId;
			return this;
		}

		public String getUpdUserIconPath(){
			return this.updUserIconPath;
		}
		public WikiView setUpdUserIconPath( String updUserIconPath ){
			this.updUserIconPath = updUserIconPath;
			return this;
		}

		public String getUpdDate(){
			return this.updDate;
		}
		public WikiView setUpdDate( String updDate ){
			this.updDate = updDate;
			return this;
		}

		public List<String> getTagViews() {
			return this.tagViews;
		}
		public WikiView setTagViews(List<String> tagViews) {
			this.tagViews = tagViews;
			return this;
		}

		public List<AttachFile> getAttachmentFileNms() {
			return attachmentFileNms;
		}
		public WikiView setAttachmentFileNms(List<AttachFile> attachmentFileNms) {
			this.attachmentFileNms = attachmentFileNms;
			return this;
		}
	}
	
	public class AttachFile {
		/**
		 *  compatibility with Dropzone file name field.
		 */
		private String name;
		private String seqNo;
		private String size;
		private String createdDate;

		public String getName() {
			return name;
		}
		public AttachFile setName(String name) {
			this.name = name;
			return this;
		}

		public String getSeqNo() {
			return seqNo;
		}
		public AttachFile setSeqNo(String seqNo) {
			this.seqNo = seqNo;
			return this;
		}
		
		public String getSize() {
			return size;
		}
		public AttachFile setSize(String size) {
			this.size = size;
			return this;
		}
		
		public String getCreatedDate() {
			return createdDate;
		}
		public AttachFile setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}
	}
}
