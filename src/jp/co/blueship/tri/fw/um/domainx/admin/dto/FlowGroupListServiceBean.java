package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.GroupListServiceSortOrderEditBean;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowGroupListServiceBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<GroupView> groupViews = new ArrayList<GroupView>();

	public RequestParam getParam() {
		return param;
	}

	public RequestsCompletion getResult() {
		return result;
	}
	public FlowGroupListServiceBean setResult(RequestsCompletion result) {
		this.result = result;
		return this;
	}

	public List<GroupView> getGroupViews() {
		return groupViews;
	}
	public FlowGroupListServiceBean setGroupViews(List<GroupView> groupViews) {
		this.groupViews = groupViews;
		return this;
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private GroupListServiceSortOrderEditBean sortOrderEdit = new GroupListServiceSortOrderEditBean();

		public GroupListServiceSortOrderEditBean getSortOrderEdit() {
			return sortOrderEdit;
		}
		public RequestParam setSortOrderEdit(GroupListServiceSortOrderEditBean sortOrderEdit) {
			this.sortOrderEdit = sortOrderEdit;
			return this;
		}

	}

	/**
	 * Group View
	 */
	public class GroupView {
		private String groupId = null;
		private String groupNm;
		private int sortOrder = 0;

		public String getGroupId() {
			return groupId;
		}
		public GroupView setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}
		public String getGroupNm() {
			return groupNm;
		}
		public GroupView setGroupNm(String groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public int getSortOrder() {
			return sortOrder;
		}
		public GroupView setSortOrder(int sortOrder) {
			this.sortOrder = sortOrder;
			return this;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {

		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}
}
