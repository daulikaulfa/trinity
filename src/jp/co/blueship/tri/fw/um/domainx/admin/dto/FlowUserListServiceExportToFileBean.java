package jp.co.blueship.tri.fw.um.domainx.admin.dto;

import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.domainx.dto.DomainServiceBean;
import jp.co.blueship.tri.fw.svc.beans.dto.ItemLabelsBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.beans.dto.ExportToFileBean;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * This is a service for the back-end(domain).
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */
public class FlowUserListServiceExportToFileBean extends DomainServiceBean {

	private static final long serialVersionUID = 1L;

	private RequestParam param = new RequestParam();
	private RequestsCompletion result = new RequestsCompletion();
	private List<UserView> userViews = new ArrayList<>();
	private FlowUserListFileResponse fileResponse = new FlowUserListFileResponse();

	public RequestParam getParam() {
		return param;
	}

	public List<UserView> getUserViews() {
		return userViews;
	}
	public FlowUserListServiceExportToFileBean setUserViews(List<UserView> userViews) {
		this.userViews = userViews;
		return this;
	}

	public RequestsCompletion getResult() {
		return result;
	}

	public void setResult(RequestsCompletion result) {
		this.result = result;
	}

	public FlowUserListFileResponse getFileResponse() {
		return fileResponse;
	}

	public void setFileResponse(FlowUserListFileResponse fileResponse) {
		this.fileResponse = fileResponse;
	}

	public class FlowUserListFileResponse {
		private File file;

		public File getFile() {
			return file;
		}
		public void setFile(File file) {
			this.file = file;
		}
	}

	/**
	 * Request Complete
	 */
	public class RequestsCompletion {
		private boolean completed = false;

		public boolean isCompleted() {
			return completed;
		}
		public RequestsCompletion setCompleted(boolean completed) {
			this.completed = completed;
			return this;
		}
	}

	/**
	 * Request Parameter
	 */
	public class RequestParam extends DomainServiceBean.RequestParam {
		private SearchCondition searchCondition = new SearchCondition();
		private OrderBy orderBy = new OrderBy();
		private ExportToFileBean exportBean = null;

		public SearchCondition getSearchCondition() {
			return searchCondition;
		}
		public RequestParam setSearchCondition(SearchCondition searchCondition) {
			this.searchCondition = searchCondition;
			return this;
		}

		public OrderBy getOrderBy() {
			return orderBy;
		}
		public RequestParam setOrderBy(OrderBy orderBy) {
			this.orderBy = orderBy;
			return this;
		}

		public ExportToFileBean getExportBean() {
			return exportBean;
		}

		public RequestParam setExportBean(ExportToFileBean exportBean) {
			this.exportBean = exportBean;
			return this;
		}
	}

	/**
	 * Search Condition
	 */
	public class SearchCondition {

		private List<ItemLabelsBean> roleViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> groupViews = new ArrayList<ItemLabelsBean>();
		private List<ItemLabelsBean> departmentViews = new ArrayList<ItemLabelsBean>();

		private String roleId = null;
		private String groupId = null;
		private String deptId = null;
		private String keyword = null;

		public List<ItemLabelsBean> getRoleViews() {
			return roleViews;
		}
		public SearchCondition setRoleViews(List<ItemLabelsBean> roleViews) {
			this.roleViews = roleViews;
			return this;
		}
		public List<ItemLabelsBean> getGroupViews() {
			return groupViews;
		}
		public SearchCondition setGroupViews(List<ItemLabelsBean> groupViews) {
			this.groupViews = groupViews;
			return this;
		}
		public List<ItemLabelsBean> getDepartmentViews() {
			return departmentViews;
		}
		public SearchCondition setDepartmentViews(List<ItemLabelsBean> departmentViews) {
			this.departmentViews = departmentViews;
			return this;
		}
		public String getRoleId() {
			return roleId;
		}
		public SearchCondition setRoleId(String roleId) {
			this.roleId = roleId;
			return this;
		}
		public String getGroupId() {
			return groupId;
		}
		public SearchCondition setGroupId(String groupId) {
			this.groupId = groupId;
			return this;
		}
		public String getDeptId() {
			return deptId;
		}
		public SearchCondition setDeptId(String deptId) {
			this.deptId = deptId;
			return this;
		}
		public String getKeyword() {
			return keyword;
		}
		public SearchCondition setKeyword(String keyword) {
			this.keyword = keyword;
			return this;
		}
	}

	/**
	 * Order By
	 */
	public class OrderBy {
		private TriSortOrder userNm = TriSortOrder.Asc;
		private TriSortOrder mailAddress;
		private TriSortOrder groupNm;
		private TriSortOrder createdDate;

		public TriSortOrder getUserNm() {
			return userNm;
		}
		public OrderBy setUserNm(TriSortOrder userNm) {
			this.userNm = userNm;
			return this;
		}
		public TriSortOrder getMailAddress() {
			return mailAddress;
		}
		public OrderBy setMailAddress(TriSortOrder mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}
		public TriSortOrder getGroupNm() {
			return groupNm;
		}
		public OrderBy setGroupNm(TriSortOrder groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public TriSortOrder getCreatedDate() {
			return createdDate;
		}
		public OrderBy setCreatedDate(TriSortOrder createdDate) {
			this.createdDate = createdDate;
			return this;
		}
	}

	/**
	 *
	 * User View
	 *
	 */
	public class UserView {

		private String userId = null;
		private String userNm = null;
		private String iconPath = null;
		private String mailAddress = null;
		private List<String> groupNm = null;
		private String language = null;
		private String timezone = null;
		private String lastLogInTime = null;
		private String createdDate = null;
		private String updDate = null;

		public String getUserId() {
			return userId;
		}
		public UserView setUserId(String userId) {
			this.userId = userId;
			return this;
		}
		public String getUserNm() {
			return userNm;
		}
		public UserView setUserNm(String userNm) {
			this.userNm = userNm;
			return this;
		}
		public String getIconPath() {
			return iconPath;
		}
		public UserView setIconPath(String iconPath) {
			this.iconPath = iconPath;
			return this;
		}
		public String getMailAddress() {
			return mailAddress;
		}
		public UserView setMailAddress(String mailAddress) {
			this.mailAddress = mailAddress;
			return this;
		}
		public List<String> getGroupNm() {
			return groupNm;
		}
		public UserView setGroupNm(List<String> groupNm) {
			this.groupNm = groupNm;
			return this;
		}
		public String getLanguage() {
			return language;
		}
		public UserView setLanguage(String language) {
			this.language = language;
			return this;
		}
		public String getTimezone() {
			return timezone;
		}
		public UserView setTimezone(String timezone) {
			this.timezone = timezone;
			return this;
		}
		public String getLastLogInTime() {
			return lastLogInTime;
		}
		public UserView setLastLogInTime(String lastLogInTime) {
			this.lastLogInTime = lastLogInTime;
			return this;
		}
		public String getCreatedDate() {
			return createdDate;
		}
		public UserView setCreatedDate(String createdDate) {
			this.createdDate = createdDate;
			return this;
		}
		public String getUpdDate() {
			return updDate;
		}
		public UserView setUpdDate(String updDate) {
			this.updDate = updDate;
			return this;
		}
	}
}
