package jp.co.blueship.tri.fw.um.domainx.admin.constants;

/**
 *
 * @version V4.03.00
 * @author Cuong Nguyen
 *
 */

public enum PasswordExpiration {
	certainDays( "1" ),
	neverExpire( "0" ),
	changeAtNextLogin( "-1" ),
	;

	private String value = null;

	PasswordExpiration( String value) {
		this.value = value;
	}

	public String value() {
		return this.value;
	}

	public static boolean contain(String value) {
		for (PasswordExpiration pe: PasswordExpiration.values()) {
			if(pe.value().equals(value))
				return true;
		}
		return false;
	}

}