package jp.co.blueship.tri.fw.um.domainx.admin;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import jp.co.blueship.tri.fw.cmn.io.StreamUtils;
import jp.co.blueship.tri.fw.cmn.io.TriFileUtils;
import jp.co.blueship.tri.fw.cmn.utils.*;
import jp.co.blueship.tri.fw.cmn.utils.collections.FluentList;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.constants.UmDesignEntryKeyByProductId;
import jp.co.blueship.tri.fw.dao.oxm.DesignSheetFactory;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ContinuableBusinessException;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.IMessageId;
import jp.co.blueship.tri.fw.msg.SmMessageId;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.session.LoginSession;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.um.UmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.um.UmFluentFunctionUtils;
import jp.co.blueship.tri.fw.um.UmItemChkUtils;
import jp.co.blueship.tri.fw.um.UmUserEditServiceUtils;
import jp.co.blueship.tri.fw.um.constants.IconSelectionOption;
import jp.co.blueship.tri.fw.um.dao.dept.eb.IDeptEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.GrpUserLnkCondition;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpEntity;
import jp.co.blueship.tri.fw.um.dao.grp.eb.IGrpUserLnkEntity;
import jp.co.blueship.tri.fw.um.dao.project.eb.IProjectEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.IUserEntity;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserCondition;
import jp.co.blueship.tri.fw.um.dao.user.eb.UserEntity;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean;
import jp.co.blueship.tri.fw.um.domainx.admin.beans.dto.UserEditInputBean.GroupView;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean;
import jp.co.blueship.tri.fw.um.domainx.admin.dto.FlowUserEditServiceBean.RequestOption;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author le.thixuan
 */
public class FlowUserEditService implements IDomain<FlowUserEditServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IFwFinderSupport fwSupport;
	private IUmFinderSupport umSupport;

	public void setSupport(IFwFinderSupport support) {
		this.fwSupport = support;
		this.umSupport = this.fwSupport.getUmFinderSupport();
	}

	private LoginSession loginSession;
	public void setLoginSession(LoginSession loginSession) {
		this.loginSession = loginSession;
	}

	@Override
	public IServiceDto<FlowUserEditServiceBean> execute(IServiceDto<FlowUserEditServiceBean> serviceDto) {
		FlowUserEditServiceBean paramBean = serviceDto.getServiceBean();

		try {

			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			String userId = paramBean.getParam().getInputInfo().getUserId();

			PreConditions.assertOf(TriStringUtils.isNotEmpty(userId), "SelectedUserId is not specified");

			if (RequestType.init.equals(paramBean.getParam().getRequestType())) {
				this.setUserDetailsView(paramBean, userId);
			}

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				if ( paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges) ) {
					UmUserEditServiceUtils.checkComplexity(paramBean.getParam().getInputInfo().getPassword(),
							paramBean.getDetailsView().getPassword());
				} else if (paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges)) {
					this.uploadCustomIcon(paramBean);
				}
			}

			if (RequestType.submitChanges.equals(paramBean.getParam().getRequestType())) {
				PreConditions.assertOf(paramBean.getParam().isRequestOptionOf(RequestOption.UserChanges), "'RequestOption.UserChanges' is not specified");
				PreConditions.assertOf(paramBean.getParam().isRequestOptionOf(RequestOption.PasswordChanges), "'RequestOption.PasswordChanges' is not specified");
				this.submitChanges(paramBean, userId);
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}


	private void uploadCustomIcon(FlowUserEditServiceBean paramBean) {
		UserEditInputBean inputBean = paramBean.getParam().getInputInfo();

		if (null == inputBean.getIconInputStreamBytes()) {
			return;
		}

		BufferedInputStream inBuffer = null;
		BufferedOutputStream outBuffer = null;
		String filePath = null;
		try {

			filePath = UmDesignBusinessRuleUtils.getUserCustomIconLocalPath(paramBean.getUserId(), inputBean.getIconFileNm());
			String absolutePath = filePath.substring(0, filePath.lastIndexOf("/"));
			InputStream is = StreamUtils.convertBytesToInputStreamToBytes(inputBean.getIconInputStreamBytes());

			File userIconDir = new File(absolutePath);
			if (!userIconDir.exists()) {
				userIconDir.mkdirs();
			}

			if (null == is)
				return;

			inBuffer = new BufferedInputStream(is);

			FileOutputStream fos = new FileOutputStream(new File(userIconDir, inputBean.getIconFileNm()));
			outBuffer = new BufferedOutputStream(fos);

			int contents = 0;
			while ((contents = inBuffer.read()) != -1) {
				outBuffer.write(contents);
			}

		} catch (IOException ioe) {
			throw new TriSystemException(SmMessageId.SM005149S, ioe);
		} finally {
			TriFileUtils.closeOfOutputStream(outBuffer);
			TriFileUtils.close(inBuffer);
		}

		if (new File(filePath).exists()) {
			String relativePath = UmDesignBusinessRuleUtils.getUserCustomIconSharePath(paramBean.getUserId(),
					inputBean.getIconFileNm());

			inputBean.setIconPath(relativePath);
			inputBean.setIconOption(IconSelectionOption.CustomImage);
		}

	}

	private void submitChanges(FlowUserEditServiceBean paramBean, String userId) {
		{
			//注意書き
			List<IMessageId> commentList	= new ArrayList<IMessageId>();
			List<String[]> commentArgsList	= new ArrayList<String[]>();

			commentList.add		( UmMessageId.UM003010I );
			commentArgsList.add	( new String[]{} );

			paramBean.setInfoCommentIdList		( commentList );
			paramBean.setInfoCommentArgsList	( commentArgsList );
		}

		UmItemChkUtils.checkUcfAdminUser(userId);
		UmItemChkUtils.checkUcfLoginUser(userId, paramBean.getUserId());

		if ( loginSession.isLoggedInUser(userId) ) {
			throw new ContinuableBusinessException( UmMessageId.UM001029E );
		}

		UserEditInputBean inputInfo = paramBean.getParam().getInputInfo();

		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.umSupport.findProjectByPrimaryKey(productId);

		if (TriStringUtils.isEmpty(inputInfo.getLanguage())) {
			inputInfo.setLanguage(projectEntity.getLanguage());
		}

		if (TriStringUtils.isEmpty(inputInfo.getTimeZone())) {
			inputInfo.setTimeZone(projectEntity.getTimezone());
		}

		int count = this.countUserByName(inputInfo.getUserNm(), userId);

		boolean passwordEmpty = false;

		if ( TriStringUtils.isEmpty( inputInfo.getPassword() ) && TriStringUtils.isEmpty( inputInfo.getConfirmPassword() )) {
			passwordEmpty = true;
			String dummy = "abcabcabca";

			inputInfo.setPassword( dummy );
			inputInfo.setConfirmPassword( dummy );
		}

		boolean passwordExpirationEmpty = false;

		if ( TriStringUtils.isEmpty( inputInfo.getPasswordExpiration() )) {
			passwordExpirationEmpty = true;
			inputInfo.setPasswordExpiration("termDummy");
		}

		boolean iconPathEmpty = false;
		if ( TriStringUtils.isEmpty( inputInfo.getIconPath() )) {
			iconPathEmpty = true;
			IUserEntity entity = umSupport.findUserByUserId(userId);
			if (IconSelectionOption.CustomImage.value().equals(UmDesignBusinessRuleUtils.getUserIconType(entity))
					&& TriStringUtils.isNotEmpty(entity.getCustomIconPath())) {
				inputInfo.setIconPath(entity.getCustomIconPath());
			}
		}

		// Validate User Input Information
		UmItemChkUtils.checkInputUser(inputInfo, count, umSupport);

		// Update um_user information
		this.updateUser(inputInfo, userId, passwordEmpty, passwordExpirationEmpty, iconPathEmpty);

		// Delete um_grp_user_lnk
		umSupport.cleaningGrpUserLnk(null, userId);

		// Insert um_grp_user_lnk
		if (TriStringUtils.isNotEmpty( inputInfo.getGroupIdSet() )) {

			umSupport.registerGrpUserLnk( inputInfo.getGroupIdSet().toArray(new String[0]), new String[]{ userId });
		}

		// Cleaning and update um_user_role_lnk
		umSupport.cleaningAndUpdateUserRoleLnk(new String[]{ userId });

		this.setUserDetailsView(paramBean, userId);

		paramBean.getResult().setCompleted(true);
		paramBean.getMessageInfo().addFlashTranslatable(UmMessageId.UM003071I,inputInfo.getUserNm());
	}

	private void setUserDetailsView( FlowUserEditServiceBean paramBean, String userId) {
		UserEditInputBean inputBean = paramBean.getParam().getInputInfo();
		IUserEntity userEntity = umSupport.findUserByUserId(userId);

		inputBean.setUserId(userId);
		inputBean.setUserNm(userEntity.getUserNm());
		inputBean.setMailAddress(userEntity.getEmailAddr());
		inputBean.setDeptId(userEntity.getDeptId());
		inputBean.setIconPath(UmDesignBusinessRuleUtils.getUserIconSharePath(userEntity));
		inputBean.setIconOption(IconSelectionOption.value(UmDesignBusinessRuleUtils.getUserIconType(userEntity)));
		inputBean.setReceiveMail(TriStringUtils.isEmpty(userEntity.getIsReceiveMail())? false : userEntity.getIsReceiveMail().parseBoolean());
		inputBean.setOriginalPasswordExpiration(TriDateUtils.convertViewDateFormat(userEntity.getPassTimeLimit(),
				TriDateUtils.getYMDHMSDateFormat(paramBean.getLanguage(), paramBean.getTimeZone())));
		// um_grp_user_lnk
		{
			Set<String> grpIdSet = inputBean.getGroupIdSet();
			GrpUserLnkCondition grpUserCondition = new GrpUserLnkCondition();
			grpUserCondition.setUserId(userId);

			List<IGrpUserLnkEntity> grpUserEntities = umSupport.getGrpUserLnkDao().find(grpUserCondition.getCondition());
			for (IGrpUserLnkEntity grpUserEntity : grpUserEntities) {
				grpIdSet.add(grpUserEntity.getGrpId());
			}
		}
		inputBean.setLanguage(userEntity.getLang());
		inputBean.setTimeZone(userEntity.getTimeZone());

		generateDropBox(paramBean);
	}

	private void generateDropBox(FlowUserEditServiceBean paramBean) {
		UserEditInputBean inputBean = paramBean.getParam().getInputInfo();

		// Department Views
		List<IDeptEntity> deptEntities = this.umSupport.findAllDept();
		inputBean
				.setDeptViews(FluentList.from(deptEntities)
						.map(UmFluentFunctionUtils.toItemLabelsFromDeptEntity)
						.asList());

		// Icon Views
		inputBean.setIconViews(UmUserEditServiceUtils.getIconViews(paramBean.getUserId()));

		// Password Expiration Effect
		inputBean.setPasswordExpirationViews( UmUserEditServiceUtils.getPasswordExpirationTerm() );

		// Group Views
		inputBean.setGroupViews(this.getGroupviews(inputBean));

		// Language Views
		inputBean.setLanguageViews(UmUserEditServiceUtils.getLanguageViews() );

		// TimeZone Views
		inputBean.setTimeZoneViews( UmUserEditServiceUtils.getTimezoneViews() );

		// Admin language and timezone
		String productId = DesignSheetFactory.getDesignSheet().getValue(UmDesignEntryKeyByProductId.rm);
		IProjectEntity projectEntity = this.umSupport.findProjectByPrimaryKey(productId);
		paramBean.getDetailsView()
				 .getDefaultTimeZone()
				 .setLabel(UmUserEditServiceUtils.getTimezoneName(projectEntity.getTimezone()));

		paramBean.getDetailsView()
				 .getDefaultLanguage()
				 .setLabel(UmUserEditServiceUtils.getLanguageName(projectEntity.getLanguage()));

	}

	private List<GroupView> getGroupviews(UserEditInputBean inputBean) {
		List<GroupView> groupViews = new ArrayList<GroupView>();
		for (IGrpEntity grpEntity : this.umSupport.findAllGroup()) {
			GrpUserLnkCondition condition = new GrpUserLnkCondition();
			condition.setGrpId( grpEntity.getGrpId() );
			int count = this.umSupport.getGrpUserLnkDao().count(condition.getCondition());

			GroupView grpView = inputBean.new GroupView()
				.setGroupId( grpEntity.getGrpId() )
				.setGroupNm( grpEntity.getGrpNm() )
				.setCount( count );

			groupViews.add(grpView);
		}

		return groupViews;
	}

	private int countUserByName(String userNm, String userId) {
		if ( TriStringUtils.isEmpty(userNm) ) {
			return 0;
		}

		UserCondition condition = new UserCondition();
		condition.setUserIdsByNotEquals(userId);
		condition.setUserNm(userNm);

		return umSupport.getUserDao().count(condition.getCondition());
	}

	private void updateUser(UserEditInputBean inputBean, String userId, boolean passwordEmpty, boolean passwordExpirationEmpty, boolean iconPathEmpty) {
		IUserEntity userEntity = new UserEntity();

		userEntity.setUserId(userId);
		userEntity.setUserNm(inputBean.getUserNm());
		if (!passwordEmpty) {
			userEntity.setUserPass	(
					EncryptionUtils.encodePasswordInA1Format( userId, inputBean.getPassword() ));
		}

		if (!passwordExpirationEmpty) {
			userEntity.setPassTimeLimit(
					UmUserEditServiceUtils.getPasswordEffectiveDate( inputBean.getPasswordExpiration() ));
		}

		if (!iconPathEmpty) {
			userEntity.setIconTyp(inputBean.getIconOption().value());
			if (IconSelectionOption.CustomImage.equals(inputBean.getIconOption())) {
				userEntity.setCustomIconPath(inputBean.getIconPath());
			} else if (IconSelectionOption.DefaultImage.equals(inputBean.getIconOption())) {
				userEntity.setDefaultIconPath(inputBean.getIconPath());
			}
		}

		userEntity.setEmailAddr( inputBean.getMailAddress() );

		if (TriStringUtils.isNotEmpty(inputBean.getDeptId())) {
			IDeptEntity deptEntity = umSupport.findDeptByDeptId(inputBean.getDeptId());
			if ( null != deptEntity ) {
				userEntity.setDeptId(deptEntity.getDeptId());
				userEntity.setDeptNm(deptEntity.getDeptNm());
			}
		}

		userEntity.setLang(inputBean.getLanguage());
		userEntity.setTimeZone(inputBean.getTimeZone());
		userEntity.setIsReceiveMail(inputBean.isReceiveMail()? StatusFlg.on : StatusFlg.off);

		umSupport.getUserDao().update(userEntity);
	}

}
