package jp.co.blueship.tri.fw.um.domainx.cmn;

import java.util.ArrayList;
import java.util.List;

import jp.co.blueship.tri.am.AmDesignBusinessRuleUtils;
import jp.co.blueship.tri.fw.cmn.utils.PreConditions;
import jp.co.blueship.tri.fw.constants.RequestType;
import jp.co.blueship.tri.fw.dao.orm.IEntityLimit;
import jp.co.blueship.tri.fw.dao.orm.ISqlSort;
import jp.co.blueship.tri.fw.dao.orm.constants.TriSortOrder;
import jp.co.blueship.tri.fw.dao.orm.psql.SortBuilder;
import jp.co.blueship.tri.fw.domain.dto.IServiceDto;
import jp.co.blueship.tri.fw.domainx.IDomain;
import jp.co.blueship.tri.fw.ex.ExceptionUtils;
import jp.co.blueship.tri.fw.ex.TriSystemException;
import jp.co.blueship.tri.fw.log.ILog;
import jp.co.blueship.tri.fw.log.LogHandler;
import jp.co.blueship.tri.fw.log.TriLogFactory;
import jp.co.blueship.tri.fw.msg.UmMessageId;
import jp.co.blueship.tri.fw.support.IFwFinderSupport;
import jp.co.blueship.tri.fw.svc.beans.dto.IPageNoInfo;
import jp.co.blueship.tri.fw.svc.beans.dto.PageNoInfo;
import jp.co.blueship.tri.fw.um.dao.filter.constants.FilterItems;
import jp.co.blueship.tri.fw.um.dao.filter.eb.FilterCondition;
import jp.co.blueship.tri.fw.um.dao.filter.eb.IFilterEntity;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterServiceBean;
import jp.co.blueship.tri.fw.um.domainx.cmn.dto.FlowSearchFilterServiceBean.SearchFilterView;
import jp.co.blueship.tri.fw.um.support.IUmFinderSupport;

/**
 *
 * @version V4.00.00
 * @author Yuhei Suzuki
 *
 */
public class FlowSearchFilterService implements IDomain<FlowSearchFilterServiceBean> {
	private static final ILog log = TriLogFactory.getInstance();

	private IUmFinderSupport support;

	public void setSupport( IFwFinderSupport support ) {
		this.support = support.getUmFinderSupport();
	}

	@Override
	public IServiceDto<FlowSearchFilterServiceBean> execute(
			IServiceDto<FlowSearchFilterServiceBean> serviceDto) {

		FlowSearchFilterServiceBean paramBean = serviceDto.getServiceBean();

		try {
			paramBean = serviceDto.getServiceBean();
			PreConditions.assertOf(paramBean != null, "ServiceBean is not specified");
			paramBean.getParam().setLinesPerPage(8);

			if (RequestType.onChange.equals(paramBean.getParam().getRequestType())) {
				onChange( paramBean );
			}

			return serviceDto;

		} catch (Exception e) {
			LogHandler.fatal( log , e );
			ExceptionUtils.reThrowIfTrinityException(e);
			throw new TriSystemException(UmMessageId.UM005057S, e, paramBean.getFlowAction());
		}
	}

	private void onChange(FlowSearchFilterServiceBean paramBean){
		String selectedUserId = paramBean.getParam().getSelectedUserId();
		String keyword = paramBean.getParam().getSearchCondition().getKeyword();
		FilterCondition condition = new FilterCondition();
		condition.setUserId(selectedUserId);
		condition.setContainsByKeyword(keyword);

		int pageNo = paramBean.getParam().getSearchCondition().getSelectedPageNo();
		int linesPerPage = paramBean.getParam().getLinesPerPage();

		ISqlSort sort = new SortBuilder();
		sort.setElement(FilterItems.regTimestamp, TriSortOrder.Desc, 1);

		IEntityLimit<IFilterEntity> limit = support.getFilterDao().find(
			condition.getCondition(),
			sort,
			pageNo,
			linesPerPage);
		IPageNoInfo page = AmDesignBusinessRuleUtils.convertPageNoInfo(new PageNoInfo(), limit.getLimit());
		paramBean.setPage(page).setFilterViews(getSearchFilterView(paramBean, limit.getEntities()));
	}

	private List<SearchFilterView> getSearchFilterView(
			FlowSearchFilterServiceBean paramBean,
			List<IFilterEntity> entities) {

		List<SearchFilterView> views = new ArrayList<SearchFilterView>();

		for (IFilterEntity entity : entities) {
			SearchFilterView viewBean = paramBean.new SearchFilterView()
				.setFilterId(entity.getFilterId())
				.setFilterNm(entity.getFilterNm())
				.setFilterData(entity.getFilterData())
				.setLotId(entity.getLotId())
				.setServiceId(entity.getSvcId())
				;
			views.add(viewBean);
		}

		return views;
	}
}






